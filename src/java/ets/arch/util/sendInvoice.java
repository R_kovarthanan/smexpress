/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.arch.util;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.Service;
import com.ServiceSoap;
import ets.domain.trip.business.TripBP;
import ets.domain.trip.business.TripTO;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.commons.mail.EmailException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 *
 * @author Arun
 */
public class sendInvoice extends Thread {

    public sendInvoice() {
    }

    TripBP tripBP;
    TripTO tripTO;

    public TripBP getTripBP() {
        return tripBP;
    }

    public void setTripBP(TripBP tripBP) {
        this.tripBP = tripBP;
    }

    public TripTO getTripTO() {
        return tripTO;
    }

    public void setTripTO(TripTO tripTO) {
        this.tripTO = tripTO;
    }

    String eFS_Id = "";
    String efs_branchId = "";
    String invoice_No = "";
    String invoice_Date = "";
    String invoice_amount = "";
    String total_TaxAmt = "";
    String cgst_Per = "";
    String cgst_Amt = "";
    String sgst_Per = "";
    String sgst_Amt = "";
    String igst_Per = "";
    String igst_Amt = "";
    String net_amount = "";
    String comp_GSTNo = "";
    String shipper_code = "";
    String consignee_code = "";
    int postId = 0;

    public sendInvoice(String eFSId, String invoiceNo, String invoiceDate, String invoiceamount, String totalTaxAmt, String cgstPer, String cgstAmt, String sgstPer, String sgstAmt, String igstPer, String igstAmt, String netamount, String compGSTNo, int postid, String shippercode, String consigneecode) {

        eFS_Id = eFSId;
        invoice_No = invoiceNo;
        invoice_Date = invoiceDate;
        invoice_amount = invoiceamount;
        total_TaxAmt = totalTaxAmt;
        cgst_Per = cgstPer;
        cgst_Amt = cgstAmt;
        sgst_Per = sgstPer;
        sgst_Amt = sgstAmt;
        igst_Per = igstPer;
        igst_Amt = igstAmt;
        net_amount = netamount;
        comp_GSTNo = compGSTNo;
        postId = postid;
        shipper_code = shippercode;
        consignee_code = consigneecode;
    }

    public void run() {

        try {

            String fileName1 = "throttle.properties";
            Properties dbProps1 = new Properties();
            InputStream is1 = getClass().getResourceAsStream("/" + fileName1);
            dbProps1.load(is1);//this may throw IOException
            String branchId = dbProps1.getProperty("branchId");
            String fromMailId = dbProps1.getProperty("fromMailId");
            String fromMailPassword = dbProps1.getProperty("fromMailPassword");
            String SMTP = dbProps1.getProperty("smtpServer");
            String PORT = dbProps1.getProperty("smtpPort");
            String serUser = dbProps1.getProperty("webserviceUsername");
            String serPass = dbProps1.getProperty("webservicePassword");

            String fileName = "jdbc_url.properties";
            Properties dbProps = new Properties();
            InputStream is = getClass().getResourceAsStream("/" + fileName);
            dbProps.load(is);//this may throw IOException
            String dbClassName = dbProps.getProperty("jdbc.driverClassName");
            String dbUrl = dbProps.getProperty("jdbc.url");
            String dbUserName = dbProps.getProperty("jdbc.username");
            String dbPassword = dbProps.getProperty("jdbc.password");

            Connection con = null;
            Class.forName(dbClassName).newInstance();
            con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);

            int updateStatus = 0;
            boolean isExceptionOccured = false;

            Service srv = new Service();
            ServiceSoap srvs = srv.getServiceSoap();
            try {
                String result = "";
                String expStatus = "FALSE";
                String expMsg = "-";
                System.out.println("call before invoice webservice for eFSId : " + eFS_Id);

                if (!eFS_Id.equalsIgnoreCase("0")) {

//                 result= srvs.customerInvoice(eFS_Id, branchId, invoice_No, invoice_Date, invoice_amount, total_TaxAmt, cgst_Per, cgst_Amt, sgst_Per, sgst_Amt, igst_Per, igst_Amt, net_amount, comp_GSTNo, serUser, serPass, shipper_code,consignee_code);
                    System.out.println("result : " + result);

                    if (!result.equals("")) {

                        String xmlRespValue = returnXMLtoString(result);

                        String[] resp = xmlRespValue.split("~");
                        expStatus = resp[0];
                        expMsg = resp[1];

                        if (expStatus.equalsIgnoreCase("TRUE") || expStatus.contains("is already generated")) {

                            try {
                                PreparedStatement updateMailDetails = null;
                                String sql = " UPDATE finance_invoice_posting_details SET \n"
                                        + " posting_status = ?  \n"
                                        + "  WHERE  id = ?";
                                updateMailDetails = con.prepareStatement(sql);
                                updateMailDetails.setInt(1, 1);
                                updateMailDetails.setInt(2, postId);
                                System.out.println(" updateMailDetails " + updateMailDetails.toString());
                                updateStatus = updateMailDetails.executeUpdate();
                                System.out.println(" after updateStatus = " + updateStatus);

                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                }

                PreparedStatement updateMailDetails = null;
                String sql = "insert into ts_cust_invoice_log  \n"
                        + " ( service_status , service_Exception_message, inv_no )"
                        + " values( ? , ?, ? )"
                        + "  ";

                updateMailDetails = con.prepareStatement(sql);
                updateMailDetails.setString(1, expStatus);
                updateMailDetails.setString(2, expMsg);
                updateMailDetails.setString(3, invoice_No);

                updateStatus = updateMailDetails.executeUpdate();
                System.out.println("updateStatus = " + updateStatus);

                isExceptionOccured = false;

            } catch (Exception e) {

                isExceptionOccured = true;

            } finally {
                con.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String returnXMLtoString(String resultVal) {

        String result = "";
        String resultResp = "";

        try {

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            result = resultVal;

            Document doc;
            doc = docBuilder.parse(new InputSource(new StringReader(result)));

            // normalize text representation
            doc.getDocumentElement().normalize();
            System.out.println("Root element of the doc is : " + doc.getDocumentElement().getNodeName());

            NodeList listOfResp = doc.getElementsByTagName("Result");

            for (int s = 0; s < listOfResp.getLength(); s++) {

                Node firstNode = listOfResp.item(s);

                if (firstNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element ExecpMsgList = (Element) firstNode;
//-------
                    NodeList statusList = ExecpMsgList.getElementsByTagName("Status");
                    Element statusElement = (Element) statusList.item(0);

                    NodeList textStatusList = statusElement.getChildNodes();
                    System.out.println("Status : " + ((Node) textStatusList.item(0)).getNodeValue().trim());

//------- 
                    NodeList ExcepOccList = ExecpMsgList.getElementsByTagName("Exception_Occured");
                    Element occrdElement = (Element) ExcepOccList.item(0);

                    NodeList textLNList = occrdElement.getChildNodes();
                    System.out.println("Exception_Occured : " + ((Node) textLNList.item(0)).getNodeValue().trim());

//----
                    NodeList ExecpMsgLists = ExecpMsgList.getElementsByTagName("Exception_Message");
                    Element exeMsg = (Element) ExecpMsgLists.item(0);

                    NodeList resExeList = exeMsg.getChildNodes();
                    System.out.println("Exception_Message : " + ((Node) resExeList.item(0)).getNodeValue().trim());

                    resultResp = ((Node) textStatusList.item(0)).getNodeValue().trim() + "~" + ((Node) resExeList.item(0)).getNodeValue().trim();

//------
                }//end of if clause

            }//end of for loop with s var

        } catch (SAXParseException err) {
            System.out.println("** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId());
            System.out.println(" " + err.getMessage());

        } catch (SAXException e) {
            Exception x = e.getException();
            ((x == null) ? e : x).printStackTrace();

        } catch (Throwable t) {
            t.printStackTrace();
        }

        return resultResp;
    }

    public static void main(String args[]) throws IOException, EmailException {

        // new sendInvoice("C014361", "543", "7", "500", "APPROVED", "TEST","").start();
    }

}
