/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.arch.util;

import com.Service;
import com.ServiceSoap;
import ets.domain.operation.business.OperationBP;
import ets.domain.operation.business.OperationTO;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.commons.mail.EmailException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 *
 * @author Arun
 */
public class sendService extends Thread {

    public sendService() {
    }

    OperationBP operationBP;
    OperationTO operationTO;

    public OperationTO getOperationTO() {
        return operationTO;
    }

    public void setOperationTO(OperationTO operationTO) {
        this.operationTO = operationTO;
    }

    public OperationBP getOperationBP() {
        return operationBP;
    }

    public void setOperationBP(OperationBP operationBP) {
        this.operationBP = operationBP;
    }

    String Tripid = "";
    String Advid = "";
    String Advancerequestamt = "";
    String ApprovalStatusText = "";
    String Remarks = "";
    String eFSId = "";
    String emailContent = "";
    int postId = 0;

    public sendService(String eFSid, String tripid, String advid, String advancerequestamt, String approvalStatusText, String remarks, String emailDetails, int postid) {

        Tripid = tripid;
        Advid = advid;
        Advancerequestamt = advancerequestamt;
        ApprovalStatusText = approvalStatusText;
        Remarks = remarks;
        eFSId = eFSid;
        emailContent = emailDetails;
        postId = postid;
    }

    public void run() {

        try {

            String fileName1 = "throttle.properties";
            Properties dbProps1 = new Properties();
            InputStream is1 = getClass().getResourceAsStream("/" + fileName1);
            dbProps1.load(is1);//this may throw IOException
            String branchId = dbProps1.getProperty("VendorbranchId");
            String serUsername = dbProps1.getProperty("webserviceUsername");
            String serPassword = dbProps1.getProperty("webservicePassword");
            String fromMailId = dbProps1.getProperty("fromMailId");
            String fromMailPassword = dbProps1.getProperty("fromMailPassword");
            String SMTP = dbProps1.getProperty("smtpServer");
            String PORT = dbProps1.getProperty("smtpPort");

            String fileName = "jdbc_url.properties";
            Properties dbProps = new Properties();
            InputStream is = getClass().getResourceAsStream("/" + fileName);
            dbProps.load(is);//this may throw IOException
            String dbClassName = dbProps.getProperty("jdbc.driverClassName");
            String dbUrl = dbProps.getProperty("jdbc.url");
            String dbUserName = dbProps.getProperty("jdbc.username");
            String dbPassword = dbProps.getProperty("jdbc.password");

            Connection con = null;
            Class.forName(dbClassName).newInstance();
            con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);

            int updateStatus = 0;
            boolean isExceptionOccured = false;

            try {

                System.out.println("call before webservice for eFSId 1 : " + eFSId);
                
                Service srv = new Service();
                ServiceSoap srvs = srv.getServiceSoap();

                String result = "";
                String expStatus = "FALSE";
                String expMsg = "-";

                System.out.println("call before webservice for eFSId 2 : " + eFSId);

                if (!eFSId.equalsIgnoreCase("0")) {

                //result= srvs.loadTripAdvanceDetail(eFSId, branchId, Tripid, Advid, Advancerequestamt, ApprovalStatusText, Remarks, serUsername, serPassword);
                    System.out.println("result : " + result);

                    String xmlRespValue = returnXMLtoString(result);

                    System.out.println("xmlRespValue : " + xmlRespValue);

                    String[] resp = xmlRespValue.split("~");
                    expStatus = resp[0];
                    expMsg = resp[1];

                    if (expStatus.equalsIgnoreCase("TRUE") || expStatus.contains("is already generated")) {

                        try {
                            PreparedStatement updateMailDetails = null;
                            String sql = " UPDATE finance_advance_posting_details SET \n"
                                    + " posting_status = ?  \n"
                                    + "  WHERE  id = ?";
                            updateMailDetails = con.prepareStatement(sql);
                            updateMailDetails.setInt(1, 1);
                            updateMailDetails.setInt(2, postId);
                            System.out.println(" updateMailDetails " + updateMailDetails.toString());
                            updateStatus = updateMailDetails.executeUpdate();
                            System.out.println(" after updateStatus = " + updateStatus);
                            

                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }

                }

                PreparedStatement updateMailDetails = null;
                String sql = "UPDATE ts_trip_advance set \n"
                        + " service_status = ?, service_Exception_message = ? where trip_advance_id=?  ";

                updateMailDetails = con.prepareStatement(sql);
                updateMailDetails.setString(1, expStatus);
                updateMailDetails.setString(2, expMsg);
                updateMailDetails.setString(3, Advid);

                updateStatus = updateMailDetails.executeUpdate();
                System.out.println("updateStatus = " + updateStatus);

                isExceptionOccured = false;

            } catch (Exception e) {

//                PreparedStatement updateMailDetails = null;
//                String sql = "UPDATE ts_trip_advance set \n"
//                        + " service_status = ?, service_Exception_message = ? where trip_advance_id=?  ";
//
//                updateMailDetails = con.prepareStatement(sql);
//                updateMailDetails.setString(1, "FALSE");
//                updateMailDetails.setString(2, "Service Failure");
//                updateMailDetails.setString(3, Advid);
//
//                updateStatus = updateMailDetails.executeUpdate();
//                System.out.println("updateStatus = " + updateStatus);

                isExceptionOccured = true;

            } finally {
                con.close();
            }

            /////////////////////////////////////////////Email part///////////////////////
            
            if(!isExceptionOccured && !emailContent.equals("")){
            
            
            String Toemailid = " arun@entitlesolutions.com";
            Properties props = new Properties();

            props.put("mail.smtp.starttls.enable", "true"); // added this line
            props.put("mail.smtp.host", SMTP);
            props.put("mail.smtp.port", PORT);
            props.put("mail.smtp.user", fromMailId);
            props.put("mail.smtp.password", fromMailPassword);
            props.put("mail.smtp.auth", "true");
            props.put("mail.debug", "true");

            String[] to = Toemailid.split(","); // added this line
            Session session = Session.getDefaultInstance(props, null);
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(fromMailId));
            InternetAddress[] toAddress = new InternetAddress[to.length];

            //////System.out.println("Toemailid:" + Toemailid);
            //////System.out.println("CCemailid:" + CCemailid);
            if (!Toemailid.equals("") && !Toemailid.equals("-")) {
                for (int i = 0; i < to.length; i++) { // changed from a while loop
                    toAddress[i] = new InternetAddress(to[i]);
                }

                for (int i = 0; i < toAddress.length; i++) { // changed from a while loop
                    message.addRecipient(Message.RecipientType.TO, toAddress[i]);
                }
            }

            String subject = "";
            String content = "";

            String[] emaildetails = emailContent.split("~");

            String customerName = emaildetails[0];
            String cnotename = emaildetails[1];
            String tripCode = emaildetails[2];
            String consignmentDate = emaildetails[3];
            String vehicletype = emaildetails[4];
            String vehicle = emaildetails[5];
            String route = emaildetails[6];
            String driver = emaildetails[7];
            String planneddate = emaildetails[8];
            String advancerequestamt = emaildetails[9];
            String estiexpense = emaildetails[10];
            String requeston = emaildetails[11];
            String remarks = emaildetails[12];

            String reqType = "Manual";
            if (Remarks.equalsIgnoreCase("Auto Approval")) {
                reqType = Remarks;
            }

            String msg = "";
            String subLine = "";

            if (!eFSId.equalsIgnoreCase("0")) {
                msg = "Response For Advance Approval Request";
                subLine = "Confirmation of Advance Approval for Vehicle No : ";
            } else {
                msg = "Please Make a Manual Approval For this Request";
                subLine = "Maximum Request Limit Exceeded for Vehicle No : ";
            }

            if (!isExceptionOccured) {

                String emailFormat = "<html>"
                        + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                        + "<tr>"
                        + "<th colspan='2'><p>Hi, <b>" + msg + "</b></p></th>"
                        + "</tr>"
                        + "<tr><td>&nbsp;&nbsp;Customer Name</td><td>&nbsp;&nbsp;" + customerName + "</td></tr>"
                        + "<tr><td>&nbsp;&nbsp;Trip Code</td><td>&nbsp;&nbsp;" + tripCode + "</td></tr>"
                        + "<tr><td>&nbsp;&nbsp;Cnote Name</td><td>&nbsp;&nbsp;" + cnotename + "</td></tr>"
                        + "<tr><td>&nbsp;&nbsp;Cnote Date</td><td>&nbsp;&nbsp;" + consignmentDate + "</td></tr>"
                        + "<tr><td>&nbsp;&nbsp;Vehicle Type</td><td>&nbsp;&nbsp;" + vehicletype + "</td></tr>"
                        + "<tr><td>&nbsp;&nbsp;Vehicle No</td><td>&nbsp;&nbsp;" + vehicle + "</td></tr>"
                        + "<tr><td>&nbsp;&nbsp;Route Name</td><td>&nbsp;&nbsp;" + route + "</td></tr>"
                        //                    + "<tr><td>&nbsp;&nbsp;Driver Name</td><td>&nbsp;&nbsp;" + driver + "</td></tr>"
                        + "<tr><td>&nbsp;&nbsp;Planned Date</td><td>&nbsp;&nbsp;" + planneddate + "</td></tr>"
                        + "<tr><td>&nbsp;&nbsp;Actual Advance Approved</td><td>&nbsp;&nbsp;" + advancerequestamt + "</td></tr>"
                        + "<tr><td>&nbsp;&nbsp;Estimated Expense</td><td>&nbsp;&nbsp;" + estiexpense + "</td></tr>"
                        + "<tr><td>&nbsp;&nbsp;Remarks</td><td>&nbsp;&nbsp;" + remarks + "</td></tr>"
                        + "<tr><td>&nbsp;&nbsp;Request On</td><td>&nbsp;&nbsp;" + requeston + "</td></tr>"
                        + "<tr><td>&nbsp;&nbsp;Approval Type</td><td>&nbsp;&nbsp;" + reqType + "</td></tr>";

                emailFormat = emailFormat + "</table></body></html>";

                subject = subLine + vehicle;
                content = emailFormat;

            } 
            
//            else {
//
//                String emailFormat = "<html>"
//                        + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
//                        + "<tr>"
//                        + "<th colspan='2'><p>Hi, <b>Response For Advance Approval Request</b>"
//                        + "<br/><b>System Caught Unexpected Exception, Your last request was failed. "
//                        + "<br/>Please contact system administrator!.</b></p></th>"
//                        + "</tr>"
//                        + "<tr><td>&nbsp;&nbsp;Customer Name</td><td>&nbsp;&nbsp;" + customerName + "</td></tr>"
//                        + "<tr><td>&nbsp;&nbsp;Trip Code</td><td>&nbsp;&nbsp;" + tripCode + "</td></tr>"
//                        + "<tr><td>&nbsp;&nbsp;Cnote Name</td><td>&nbsp;&nbsp;" + cnotename + "</td></tr>"
//                        + "<tr><td>&nbsp;&nbsp;Cnote Date</td><td>&nbsp;&nbsp;" + consignmentDate + "</td></tr>"
//                        + "<tr><td>&nbsp;&nbsp;Vehicle Type</td><td>&nbsp;&nbsp;" + vehicletype + "</td></tr>"
//                        + "<tr><td>&nbsp;&nbsp;Vehicle No</td><td>&nbsp;&nbsp;" + vehicle + "</td></tr>"
//                        + "<tr><td>&nbsp;&nbsp;Route Name</td><td>&nbsp;&nbsp;" + route + "</td></tr>"
//                        //                    + "<tr><td>&nbsp;&nbsp;Driver Name</td><td>&nbsp;&nbsp;" + driver + "</td></tr>"
//                        + "<tr><td>&nbsp;&nbsp;Planned Date</td><td>&nbsp;&nbsp;" + planneddate + "</td></tr>"
//                        + "<tr><td>&nbsp;&nbsp;Actual Advance Approved</td><td>&nbsp;&nbsp;" + advancerequestamt + "</td></tr>"
//                        + "<tr><td>&nbsp;&nbsp;Estimated Expense</td><td>&nbsp;&nbsp;" + estiexpense + "</td></tr>"
//                        + "<tr><td>&nbsp;&nbsp;Remarks</td><td>&nbsp;&nbsp;" + remarks + "</td></tr>"
//                        + "<tr><td>&nbsp;&nbsp;Request On</td><td>&nbsp;&nbsp;" + requeston + "</td></tr>"
//                        + "<tr><td>&nbsp;&nbsp;Approval Type</td><td>&nbsp;&nbsp;" + reqType + "</td></tr>";
//
//                emailFormat = emailFormat
//                        + ""
//                        + "</table></body></html>";
//
//                subject = "Failure: Your Advance Approval Request StatusFor Vehicle - " + vehicle;
//                content = emailFormat;
//
//            }

            message.setSubject(subject);
            message.setContent(content, "text/html");
            Transport transport = session.getTransport("smtp");
            transport.connect(SMTP, fromMailId, fromMailPassword);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String returnXMLtoString(String resultVal) {

        String result = "";
        String resultResp = "";

        try {

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            result = resultVal;

            Document doc;
            doc = docBuilder.parse(new InputSource(new StringReader(result)));

            // normalize text representation
            doc.getDocumentElement().normalize();
            System.out.println("Root element of the doc is : " + doc.getDocumentElement().getNodeName());

            NodeList listOfResp = doc.getElementsByTagName("Result");

            for (int s = 0; s < listOfResp.getLength(); s++) {

                Node firstNode = listOfResp.item(s);

                if (firstNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element ExecpMsgList = (Element) firstNode;
//-------
                    NodeList statusList = ExecpMsgList.getElementsByTagName("Status");
                    Element statusElement = (Element) statusList.item(0);

                    NodeList textStatusList = statusElement.getChildNodes();
                    System.out.println("Status : " + ((Node) textStatusList.item(0)).getNodeValue().trim());

//------- 
                    NodeList ExcepOccList = ExecpMsgList.getElementsByTagName("Exception_Occured");
                    Element occrdElement = (Element) ExcepOccList.item(0);

                    NodeList textLNList = occrdElement.getChildNodes();
                    System.out.println("Exception_Occured : " + ((Node) textLNList.item(0)).getNodeValue().trim());

//----
                    NodeList ExecpMsgLists = ExecpMsgList.getElementsByTagName("Exception_Message");
                    Element exeMsg = (Element) ExecpMsgLists.item(0);

                    NodeList resExeList = exeMsg.getChildNodes();
                    System.out.println("Exception_Message : " + ((Node) resExeList.item(0)).getNodeValue().trim());

                    resultResp = ((Node) textStatusList.item(0)).getNodeValue().trim() + "~" + ((Node) resExeList.item(0)).getNodeValue().trim();

//------
                }//end of if clause

            }//end of for loop with s var

        } catch (SAXParseException err) {
            System.out.println("** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId());
            System.out.println(" " + err.getMessage());

        } catch (SAXException e) {
            Exception x = e.getException();
            ((x == null) ? e : x).printStackTrace();

        } catch (Throwable t) {
            t.printStackTrace();
        }

        return resultResp;
    }

    public static void main(String args[]) throws IOException, EmailException {

        //new sendService("C014361", "543", "7", "500", "APPROVED", "TEST","").start();
    }

}
