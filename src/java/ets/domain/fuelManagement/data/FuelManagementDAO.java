/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.fuelManagement.data;

/**
 *
 * @author vinoth
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPRuntimeException;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import ets.domain.fuelManagement.business.FuelManagementTO;
import ets.domain.users.web.CryptoLibrary;

public class FuelManagementDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "LoginDAO";

    public FuelManagementDAO() {
    }

    public ArrayList getVehCompList(FuelManagementTO fuelTO) {
        Map map = new HashMap();
        ArrayList companyList = new ArrayList();
        map.put("regNo", fuelTO.getRegNo());
        map.put("typeId", fuelTO.getVehTypeId());
        map.put("owningCompId", fuelTO.getOwningCompId());
        map.put("usingCompId", fuelTO.getServicePtId());
        //////System.out.println("owningCompId" + fuelTO.getOwningCompId());
        //////System.out.println("fuelTO.getServicePtId()" + fuelTO.getServicePtId());
        try {
            companyList = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.getVehCompList", map);
            //////System.out.println("companyList" + companyList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return companyList;
    }

    public ArrayList getTankDetails(FuelManagementTO fuelTO) {
        Map map = new HashMap();
        ArrayList companyList = new ArrayList();
        if (fuelTO.getCompanyId() != "") {
            map.put("companyId", fuelTO.getCompanyId());
        } else {
            map.put("companyId", "0");
        }
        map.put("fromDate", fuelTO.getFromDate());
        map.put("toDate", fuelTO.getToDate());
        //////System.out.println("companyId" + fuelTO.getCompanyId());
        //////System.out.println("fuelTO.getFromDate()" + fuelTO.getFromDate());
        //////System.out.println("fuelTO.getToDate()" + fuelTO.getToDate());
        try {
            companyList = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.getTankDetails", map);
            //////System.out.println("getTankDetails" + companyList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return companyList;
    }

    public ArrayList getCompanyList() {
        Map map = new HashMap();
        ArrayList companyList = new ArrayList();

        try {
            companyList = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.getCompanyList", map);
            //////System.out.println("companyList" + companyList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return companyList;
    }

    public ArrayList getUsingCompanyList() {
        Map map = new HashMap();
        ArrayList companyList = new ArrayList();

        try {
            companyList = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.getUsingCompanyList", map);
            //////System.out.println("companyList" + companyList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return companyList;
    }

    public ArrayList getUsingCompanyList(FuelManagementTO fuelTO) {
        Map map = new HashMap();
        ArrayList companyList = new ArrayList();
        map.put("usingCompId", fuelTO.getUsingCompId());
        try {
            companyList = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.getUsingCompany", map);
            //////System.out.println("companyList" + companyList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return companyList;
    }

    public int doInsertVehicleCompany(FuelManagementTO fuelTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("regNo", fuelTO.getRegNo());
        map.put("typeId", fuelTO.getVehTypeId());
        map.put("owningCompId", fuelTO.getOwningCompId());
        map.put("usingCompId", fuelTO.getUsingCompId());
        map.put("mfrId", fuelTO.getMfrId());
        map.put("modelId", fuelTO.getModelId());
        map.put("usageId", fuelTO.getUsageTypeId());
        map.put("activeInd", fuelTO.getActiveInd());
        String[] dat = fuelTO.getDate().split("-");
        String date = dat[2] + "-" + dat[1] + "-" + dat[0];
        map.put("date", date);
        String modelId = "";
        try {
            String vehicleId = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getVehicleId", map);
            map.put("vehicleId", vehicleId);
            //////System.out.println("vehicleId=" + vehicleId);
            status = (Integer) getSqlMapClientTemplate().update("fuelManagement.updateVehMaster", map);

            modelId = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getModelId", map);
            map.put("modelId", modelId);
            status = (Integer) getSqlMapClientTemplate().update("fuelManagement.updateVehType", map);
            status = (Integer) getSqlMapClientTemplate().update("fuelManagement.updateUsageType", map);
            status = (Integer) getSqlMapClientTemplate().update("fuelManagement.updateOwningComp", map);
            status = (Integer) getSqlMapClientTemplate().update("fuelManagement.insertVehComp", map);
            //////System.out.println("status" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public int doInsertUsingCompany(FuelManagementTO fuelTO) {
        Map map = new HashMap();
        int status = 0;

        map.put("companyName", fuelTO.getCompanyName());
        map.put("activeInd", fuelTO.getActiveInd());

        try {

            status = (Integer) getSqlMapClientTemplate().update("fuelManagement.insertUsingComp", map);

            //////System.out.println("status" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public int doUpdateUsingComp(FuelManagementTO fuelTO) {
        Map map = new HashMap();
        int status = 0;

        map.put("usingCompId", fuelTO.getUsingCompId());
        map.put("companyName", fuelTO.getCompanyName());
        map.put("activeInd", fuelTO.getActiveInd());

        try {

            status = (Integer) getSqlMapClientTemplate().update("fuelManagement.updateUsingComp", map);

            //////System.out.println("status" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public int doInsertTankDetails(FuelManagementTO fuelTO, int userId) {
        Map map = new HashMap();
        int status = 0;

        String dat = fuelTO.getDate();
        String dat1[] = dat.split("-");
        String date = dat1[2] + "-" + dat1[1] + "-" + dat1[0];
        //////System.out.println("date=" + date);
        map.put("date", date);
        map.put("fuelFilled", fuelTO.getFuelFilled());
        map.put("companyId", fuelTO.getCompanyId());
        map.put("present", fuelTO.getPresent());
        map.put("previous", fuelTO.getPrevious());
        map.put("rate", fuelTO.getRate());
        map.put("minLevel", fuelTO.getMinLevel());

        try {

            status = (Integer) getSqlMapClientTemplate().update("fuelManagement.insertTankDetail", map);
            status = (Integer) getSqlMapClientTemplate().update("fuelManagement.insertTankFilledDetail", map);

            //////System.out.println("status" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public int doInsertPrice(FuelManagementTO fuelTO, int userId) {
        Map map = new HashMap();
        int status = 0;

        String dat = fuelTO.getDate();
        String dat1[] = dat.split("-");
        String date = dat1[2] + "-" + dat1[1] + "-" + dat1[0];
        //////System.out.println("date=" + date);
        map.put("date", date);
        map.put("companyId", fuelTO.getCompanyId());
        map.put("rate", fuelTO.getRate());

        try {

            status = (Integer) getSqlMapClientTemplate().update("fuelManagement.insertPriceDetail", map);
            //////System.out.println("status" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public int doInsertMilleage(FuelManagementTO fuelTO) {
        Map map = new HashMap();
        int status = 0;

        map.put("mfrId", fuelTO.getMfrId());
        map.put("modelId", fuelTO.getModelId());
        map.put("fromYear", fuelTO.getFromYear());
        map.put("toYear", fuelTO.getToYear());
        map.put("milleage", fuelTO.getMilleage());

        try {

            status = (Integer) getSqlMapClientTemplate().update("fuelManagement.insertMilleage", map);
            //////System.out.println("status" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public int doUpdateMilleage(FuelManagementTO fuelTO) {
        Map map = new HashMap();
        int status = 0;

        map.put("mfrId", fuelTO.getMfrId());
        map.put("modelId", fuelTO.getModelId());
        map.put("fromYear", fuelTO.getFromYear());
        map.put("toYear", fuelTO.getToYear());
        map.put("milleage", fuelTO.getMilleage());

        try {

            status = (Integer) getSqlMapClientTemplate().update("fuelManagement.updateMilleage", map);
            //////System.out.println("status" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public int doUpdatePrice(FuelManagementTO fuelTO, int userId) {
        Map map = new HashMap();
        int status = 0;

        String dat = fuelTO.getDate();
        String dat1[] = dat.split("-");
        String date = dat1[2] + "-" + dat1[1] + "-" + dat1[0];
        //////System.out.println("date=" + date);
        map.put("date", date);
        map.put("companyId", fuelTO.getCompanyId());
        map.put("rate", fuelTO.getRate());

        try {

            status = (Integer) getSqlMapClientTemplate().update("fuelManagement.updateFuelPrice", map);
            //////System.out.println("status" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public int doDeletePrice(FuelManagementTO fuelTO, int userId) {
        Map map = new HashMap();
        int status = 0;

        String dat = fuelTO.getDate();
        String dat1[] = dat.split("-");
        String date = dat1[2] + "-" + dat1[1] + "-" + dat1[0];
        //////System.out.println("date=" + date);
        map.put("date", date);
        map.put("companyId", fuelTO.getCompanyId());

        try {

            status = (Integer) getSqlMapClientTemplate().delete("fuelManagement.deleteFuelPrice", map);
            //////System.out.println("status" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public int doInsertFuelFilling(FuelManagementTO fuelTO, int userId, String companyId) {
        Map map = new HashMap();
        int status = 0;
        float current = 0.0f;
        float filled = 0.0f;
        float currentReading = 0.0f;

        String outFuel = "0.00";
        String fuel = "0.00";
        String dat = fuelTO.getDate();
        String dat1[] = dat.split("-");
        String date = dat1[2] + "-" + dat1[1] + "-" + dat1[0];
        //////System.out.println("date=" + date);
        map.put("date", date);
        map.put("regNo", fuelTO.getRegNo());
        map.put("fuelFilled", fuelTO.getFuelFilled());
        map.put("couponNo", fuelTO.getCouponNo());
        map.put("present", fuelTO.getPresent());
        map.put("previous", fuelTO.getPrevious());
        map.put("amount", fuelTO.getAmount());
        map.put("tankReading", fuelTO.getTankReading());
        map.put("runReading", fuelTO.getRunReading());
        map.put("totalKm", fuelTO.getTotalKm());
        map.put("avgKm", fuelTO.getAvgKm());
        map.put("outFillKm", fuelTO.getOutFillKm());
        //////System.out.println("date" + date);
        map.put("place", fuelTO.getPlace());
        //////System.out.println("fuelTO.getFuel()" + fuelTO.getFuel());
        map.put("fuel", fuelTO.getFuel());
        map.put("companyId", companyId);
        try {

            String vehicleId = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getVehicleId", map);
            map.put("vehicleId", vehicleId);
            String chekExist = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getVehicleExistance", map);
            if (chekExist == null) {
                int fillingId = (Integer) getSqlMapClientTemplate().insert("fuelManagement.insertFuelFilling", map);
                map.put("current", fuelTO.getTankReading());

                map.put("fillingId", fillingId);
                status = (Integer) getSqlMapClientTemplate().update("fuelManagement.insertTankDetail", map);
                //////System.out.println("status" + status);
            } else {
                status = 1;
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public int doInsertOutFillFuel(ArrayList List, int userId, String companyId) {
        Map map = new HashMap();
        int status = 0;
        try {
            Iterator itr = List.iterator();
            FuelManagementTO fuelTO = null;
            while (itr.hasNext()) {
                fuelTO = new FuelManagementTO();
                fuelTO = (FuelManagementTO) itr.next();

                String dat = fuelTO.getDate();
                String dat1[] = dat.split("-");
                String date = dat1[2] + "-" + dat1[1] + "-" + dat1[0];
                //////System.out.println("date=" + date);
                map.put("date", date);
                map.put("regNo", fuelTO.getRegNo());
                map.put("outFillKm", fuelTO.getOutFillKm());
                map.put("place", fuelTO.getPlace());
                //////System.out.println("fuelTO.getFuel()" + fuelTO.getFuel());
                map.put("fuel", fuelTO.getFuel());
                map.put("amount", fuelTO.getAmount());
                map.put("companyId", companyId);

                //////System.out.println("fuelTO.getRate()" + fuelTO.getAmount());
                String vehicleId = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getVehicleId", map);
                map.put("vehicleId", vehicleId);
                status = (Integer) getSqlMapClientTemplate().update("fuelManagement.insertOutFillFuel", map);
                //////System.out.println("status" + status);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public int doUpdateVehComp(FuelManagementTO fuelTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        String modelId = "";
        try {
            map.put("vehicleId", fuelTO.getVehicleId());
            map.put("typeId", fuelTO.getVehTypeId());
            map.put("owningCompId", fuelTO.getOwningCompId());
            map.put("usingCompId", fuelTO.getUsingCompId());
            map.put("mfrId", fuelTO.getMfrId());
            map.put("modelId", fuelTO.getModelId());
            map.put("usageId", fuelTO.getUsageTypeId());
            map.put("activeInd", fuelTO.getActiveInd());
            map.put("regNo", fuelTO.getRegNo());
            //////System.out.println("fuelTO.getVehicleId()" + fuelTO.getActiveInd());
            //////System.out.println("fuelTO.getUsingCompId()" + fuelTO.getUsingCompId());
            String[] dat = fuelTO.getDate().split("-");
            String date = dat[2] + "-" + dat[1] + "-" + dat[0];
            map.put("date", date);
            String vehId = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getVehicleId", map);
            map.put("vehId", vehId);
            //////System.out.println("vehicleId=" + vehId);

            status = (Integer) getSqlMapClientTemplate().update("fuelManagement.updateVehMaster", map);
            modelId = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getModelId", map);
            map.put("modelId", modelId);
            status = (Integer) getSqlMapClientTemplate().update("fuelManagement.updateVehType", map);
            status = (Integer) getSqlMapClientTemplate().update("fuelManagement.updateUsageType", map);
            status = (Integer) getSqlMapClientTemplate().update("fuelManagement.updateOwningComp", map);
            status = (Integer) getSqlMapClientTemplate().update("fuelManagement.updateVehComp", map);
            //////System.out.println("status" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public int doDeleteVehComp(ArrayList List, int userId) {
        Map map = new HashMap();
        int status = 0;
        FuelManagementTO fuelTO = null;
        try {
            Iterator itr = List.iterator();
            while (itr.hasNext()) {
                fuelTO = new FuelManagementTO();
                fuelTO = (FuelManagementTO) itr.next();
                map.put("vehicleId", fuelTO.getVehicleId());

                status = (Integer) getSqlMapClientTemplate().delete("fuelManagement.deleteVehComp", map);
                //////System.out.println("status" + status);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public int doUpdateTankDetails(ArrayList List, int userId) {
        Map map = new HashMap();
        int status = 0;
        FuelManagementTO fuelTO = null;
        try {
            Iterator itr = List.iterator();
            while (itr.hasNext()) {
                fuelTO = new FuelManagementTO();
                fuelTO = (FuelManagementTO) itr.next();
                String dat = fuelTO.getDate();
                String dat1[] = dat.split("-");
                String date = dat1[2] + "-" + dat1[1] + "-" + dat1[0];
                //////System.out.println("date=" + date);
                map.put("date", date);
                map.put("fuelFilled", fuelTO.getFuelFilled());
                map.put("companyId", fuelTO.getCompanyId());
                map.put("present", fuelTO.getPresent());
                map.put("rate", fuelTO.getRate());
                map.put("servicePtId", fuelTO.getServicePtId());

                status = (Integer) getSqlMapClientTemplate().update("fuelManagement.updateTankDetail", map);
                status = (Integer) getSqlMapClientTemplate().update("fuelManagement.updateTankFilledDetail", map);

                //////System.out.println("status" + status);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public String getFuelPrice(String companyId) {
        Map map = new HashMap();
        String status = "";
        FuelManagementTO fuelTO = null;
        try {

            map.put("companyId", companyId);

            status = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getFuelPrice", map);
            //////System.out.println("status" + status);
            if (status == null) {
                status = "0";
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public ArrayList getVehDetails(String regNo) {
        Map map = new HashMap();
        ArrayList List = new ArrayList();
        try {
            map.put("regNo", regNo);
            map.put("typeId", "1");
            map.put("mfrId", "0");
            map.put("usageId", "0");
            map.put("usingCompId", "0");
            map.put("owningCompId", "0");

            //////System.out.println("regNo" + regNo);
            List = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.getVehCompList", map);
            //////System.out.println("List" + List.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return List;
    }

    public String getVehComp(String regNo) {
        Map map = new HashMap();
        String vehComp = "";
        try {
            map.put("regNo", regNo);
            //////System.out.println("regNo" + regNo);
            vehComp = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getAjaxVehComp", map);
            //////System.out.println("vehComp" + vehComp);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return vehComp;
    }

    public String getPreviousKm(String regNo, String companyId) {
        Map map = new HashMap();
        String status = "";
        try {

            map.put("regNo", regNo);
            map.put("companyId", companyId);

            status = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getPreKm", map);
            if (status == null) {
                status = "0";
            }
            //////System.out.println("status" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public String getPrevious(String vehicleId, String date) {
        Map map = new HashMap();
        String status = "";
        try {
            String[] dat = date.split("-");
            String dat1 = dat[2] + "-" + dat[1] + "-" + dat[0];
            map.put("vehicleId", vehicleId);
            map.put("date", dat1);

            status = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getPrevious", map);
            if (status == null) {
                status = "0";
            }
            //////System.out.println("status" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public String getCurrentCapacity(String vehicleId, String date) {
        Map map = new HashMap();
        String status = "";
        try {
            String[] dat = date.split("-");
            String dat1 = dat[2] + "-" + dat[1] + "-" + dat[0];
            map.put("vehicleId", vehicleId);
            map.put("date", dat1);

            status = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getCurrentCapacity", map);
            if (status == null) {
                status = "0";
            }
            //////System.out.println("status" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public String getCurrentRunReading(String vehicleId, String date) {
        Map map = new HashMap();
        String status = "";
        try {
            String[] dat = date.split("-");
            String dat1 = dat[2] + "-" + dat[1] + "-" + dat[0];
            map.put("vehicleId", vehicleId);
            map.put("date", dat1);

            status = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getCurrentRunReading", map);
            if (status == null) {
                status = "0";
            }
            //////System.out.println("status" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public String chekMilleage(String mfrId, String modelId, String fromYear, String toYear) {
        Map map = new HashMap();
        String status = "";
        try {

            map.put("mfrId", mfrId);
            map.put("modelId", modelId);
            map.put("fromYear", fromYear);
            map.put("toYear", toYear);

            status = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.chekMilleage", map);
            //////System.out.println("status" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public String getPreviousFuel(String regNo, String companyId) {
        Map map = new HashMap();
        String status = "";
        ArrayList list = new ArrayList();
        try {

            map.put("regNo", regNo);
            map.put("companyId", companyId);
            //////System.out.println("regNo" + regNo);
            //////System.out.println("companyId=" + companyId);
            status = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getPreFuel", map);
            //////System.out.println("status" + status);
//            if (status.equalsIgnoreCase("0.00")) {
//                status = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getOutFilledFuel", map);
//            }
            //////System.out.println("status" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public ArrayList getMilleageActual(FuelManagementTO fuelTO, int userId) {
        Map map = new HashMap();
        ArrayList List = new ArrayList();
        try {
            map.put("fromDate", fuelTO.getFromDate());
            map.put("toDate", fuelTO.getToDate());
            map.put("fromKm", fuelTO.getFromKm());
            map.put("toKm", fuelTO.getToKM());
            map.put("regNo", fuelTO.getRegNo());
            map.put("companyId", fuelTO.getCompanyId());
            map.put("servicePtId", fuelTO.getServicePtId());
            map.put("typeId", fuelTO.getTypeId());
            map.put("outFill", fuelTO.getOutFill());
            map.put("couponNo", fuelTO.getCouponNo());
            //////System.out.println("fuelTO.getOutFill()" + fuelTO.getOutFill());
            //////System.out.println("fuelTO.getTypeId()" + fuelTO.getTypeId());
            //////System.out.println("fuelTO.getServicePtId()" + fuelTO.getServicePtId());
            //////System.out.println("fuelTO.getCompanyId()" + fuelTO.getCompanyId());
            //////System.out.println("fuelTO.getTodate()" + fuelTO.getToDate());
            //////System.out.println("fuelTO.getFromdate()" + fuelTO.getFromDate());
            //////System.out.println("fuelTO.getToKM()" + fuelTO.getToKM());
            //////System.out.println("fuelTO.getFromKm()" + fuelTO.getFromKm());
            //////System.out.println("fuelTO.getRegNo()" + fuelTO.getRegNo());
            List = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.getMilleageActual", map);
            //////System.out.println("List" + List.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return List;
    }

    public ArrayList getFuelFillingList(FuelManagementTO fuelTO, int userId, int startIndex, int endIndex) {
        Map map = new HashMap();
        ArrayList List = new ArrayList();
        try {
            map.put("fromDate", fuelTO.getFromDate());
            map.put("toDate", fuelTO.getToDate());
            map.put("fromKm", fuelTO.getFromKm());
            map.put("toKm", fuelTO.getToKM());
            map.put("regNo", fuelTO.getRegNo());
            map.put("companyId", fuelTO.getCompanyId());
            map.put("servicePtId", fuelTO.getServicePtId());
            map.put("typeId", fuelTO.getTypeId());
            map.put("outFill", fuelTO.getOutFill());
            map.put("couponNo", fuelTO.getCouponNo());
            map.put("startIndex", startIndex);
            map.put("endIndex", endIndex);
            if (fuelTO.getOwningCompId() != "") {
                map.put("owningCompId", fuelTO.getOwningCompId());

            } else {
                map.put("owningCompId", "0");

            }
            if (fuelTO.getUsingCompId() != "") {
                map.put("usingCompId", fuelTO.getUsingCompId());
            } else {
                map.put("usingCompId", "0");
            }
            //////System.out.println("using" + fuelTO.getUsingCompId());
            //////System.out.println("using" + fuelTO.getOwningCompId());
            List = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.getFuelFillingList", map);
            //////System.out.println("List" + List.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return List;
    }

    public ArrayList getFuelPriceList(FuelManagementTO fuelTO) {
        Map map = new HashMap();
        ArrayList List = new ArrayList();
        try {
            if (fuelTO.getCompanyId() != "") {
                map.put("companyId", fuelTO.getCompanyId());
            } else {
                map.put("companyId", "0");
            }
            map.put("fromDate", fuelTO.getFromDate());
            map.put("toDate", fuelTO.getToDate());
            //////System.out.println("fuelTO.getCompanyId()" + fuelTO.getCompanyId());
            //////System.out.println("fuelTO.getFromDate()" + fuelTO.getFromDate());
            //////System.out.println("fuelTO.getToDate()" + fuelTO.getToDate());
            List = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.getFuelPriceList", map);
            //////System.out.println("List Price" + List.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return List;
    }

    public ArrayList getTankReading(String companyId) {
        Map map = new HashMap();
        ArrayList List = new ArrayList();
        try {
            map.put("companyId", companyId);
            //////System.out.println("companyId" + companyId);
            List = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.getTankReading", map);
            //////System.out.println("List Price" + List.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return List;
    }

    public String getRunReading(String companyId) {
        Map map = new HashMap();
        String reading = "";
        try {
            map.put("companyId", companyId);
            //////System.out.println("companyId" + companyId);
            reading = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getRunReading", map);
            if (reading == null) {
                reading = "0";
            }
            //////System.out.println(" getRunReading" + reading);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRunReading Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getRunReading", sqlException);
        }
        return reading;
    }

    public ArrayList getMilleageList(FuelManagementTO fuelTO, String companyId) {
        Map map = new HashMap();
        ArrayList mileageList = new ArrayList();
        try {
            map.put("companyId", companyId);
            map.put("vehicleTypeId", fuelTO.getVehicleTypeId());
            mileageList = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.getMilleageList", map);
            //////System.out.println("mileageList" + mileageList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return mileageList;
    }

    public int updateMileage(String[] vehicleTypeIds, String[] vehicleMileage, String[] reeferMileage, String[] selected, FuelManagementTO fuelTO, int userId) {
        Map map = new HashMap();
        int insertMileage = 0;
        int updateMileage = 0;
        int status = 1;
        int checkMileageConfig = 0;
        try {
            int index = 0;
            int vehicleTypeId = 0;
            float mileage = 0.0f;
            float reeferConsum = 0.0f;
            String[] selectMfrId = new String[vehicleTypeIds.length];
            String[] selectMileage = new String[vehicleMileage.length];
            String[] selectReeferMileage = new String[reeferMileage.length];
            map.put("userId", userId);
            for (int i = 0; i < vehicleTypeIds.length; i++) {
                for (int j = 0; j < selected.length; j++) {
                    index = Integer.parseInt(selected[j]);
                    selectMfrId[index] = vehicleTypeIds[index];
                    selectMileage[index] = vehicleMileage[index];
                    selectReeferMileage[index] = reeferMileage[index];
                }
            }
            for (int i = 0; i < selectMfrId.length; i++) {
                //////System.out.println("selectMfrId[i] = " + selectMfrId[i]);
                if (selectMfrId[i] != null && selectMileage[i] != null && selectReeferMileage[i] != null) {
                    vehicleTypeId = Integer.parseInt(selectMfrId[i]);
                    if (vehicleMileage[i] != "") {
                        mileage = Float.parseFloat(vehicleMileage[i]);
                    } else {
                        mileage = 0;
                    }
                    if (reeferMileage[i] != "") {
                        reeferConsum = Float.parseFloat(reeferMileage[i]);
                    } else {
                        reeferConsum = 0;
                    }
                    map.put("vehicleTypeId", vehicleTypeId);
                    map.put("mileage", mileage);
                    map.put("reeferConsum", reeferConsum);
                    //////System.out.println("map = " + map);
                    checkMileageConfig = (Integer) getSqlMapClientTemplate().queryForObject("fuelManagement.checkMileageConfig", map);
                    if (checkMileageConfig == 0) {
                        checkMileageConfig = (Integer) getSqlMapClientTemplate().queryForObject("fuelManagement.checkMileageConfigId", map);
                        if (checkMileageConfig == 0) {
                            insertMileage = (Integer) getSqlMapClientTemplate().update("fuelManagement.insertMilleage", map);
                        } else {
                            updateMileage = (Integer) getSqlMapClientTemplate().update("fuelManagement.updateMilleage", map);
                            insertMileage = (Integer) getSqlMapClientTemplate().update("fuelManagement.insertMilleage", map);
                        }
                        status++;
                    }
                }
            }

            //updateMileage = (Integer) getSqlMapClientTemplate().update("fuelManagement.getMilleageList", map);
            //////System.out.println("updateMileage" + updateMileage);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateMileage Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateMileage", sqlException);
        }
        return status;
    }

    public String getTankCapacity(String companyId) {
        Map map = new HashMap();
        String status = "";

        map.put("companyId", companyId);

        try {

            status = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getTankMinLevel", map);
            //////System.out.println("status" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public String getRegNo(String companyId) {
        Map map = new HashMap();
        String status = "";

        map.put("companyId", companyId);

        try {

            status = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getRegNo", map);
            //////System.out.println("status" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public String getTotalRecords(FuelManagementTO fuelTO) {
        Map map = new HashMap();
        String status = "";

        map.put("fromDate", fuelTO.getFromDate());
        map.put("toDate", fuelTO.getToDate());
        map.put("fromKm", fuelTO.getFromKm());
        map.put("toKm", fuelTO.getToKM());
        map.put("regNo", fuelTO.getRegNo());
        map.put("companyId", fuelTO.getCompanyId());
        map.put("servicePtId", fuelTO.getServicePtId());
        map.put("typeId", fuelTO.getTypeId());
        map.put("outFill", fuelTO.getOutFill());
        map.put("couponNo", fuelTO.getCouponNo());

        try {
            status = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getTotalRecords", map);
            if (status == null) {
                status = "0";
            }
            //////System.out.println("status" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public int deleteMilleage(FuelManagementTO fuelTO) {
        Map map = new HashMap();
        int status = 0;
        map.put("mfrId", fuelTO.getMfrId());
        map.put("modelId", fuelTO.getModelId());
        map.put("fromYear", fuelTO.getFromYear());
        map.put("toYear", fuelTO.getToYear());

        try {
            status = (Integer) getSqlMapClientTemplate().delete("fuelManagement.deleteMilleage", map);

            //////System.out.println("status" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public ArrayList getMilleage(String vehicleId, String from, String to) {
        Map map = new HashMap();
        ArrayList List = new ArrayList();
        try {

            map.put("vehicleId", vehicleId);
            map.put("from", from);
            map.put("to", to);

            List = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.getMilleageReport", map);
            //////System.out.println("List" + List.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return List;
    }

    public String getTotalKm(String vehicleId, String from, String to) {
        Map map = new HashMap();
        String totalKm = "";
        try {

            map.put("vehicleId", vehicleId);
            map.put("from", from);
            map.put("to", to);
            //////System.out.println("fromdate km" + from);
            //////System.out.println("todate km" + to);
            totalKm = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getTotalKm", map);
            //////System.out.println("totalKm" + totalKm);
            if (totalKm == null) {
                totalKm = "0";
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTotalKm Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return totalKm;
    }

    public String getFuelFilled(String vehicleId, String from, String to) {
        Map map = new HashMap();
        String fuel = "";
        try {

            map.put("vehicleId", vehicleId);
            map.put("from", from);
            map.put("to", to);
            //////System.out.println("from" + from);
            //////System.out.println("to" + to);
            fuel = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getFuelFilled", map);
            //////System.out.println("fuel" + fuel);
            if (fuel == null) {
                fuel = "0";
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTotalKm Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return fuel;
    }

    public String getTotalOutFill(String vehicleId, String from, String to) {
        Map map = new HashMap();
        String totalOutFill = "";
        try {

            map.put("vehicleId", vehicleId);
            map.put("from", from);
            map.put("to", to);

            totalOutFill = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getTotalOutFill", map);
            //////System.out.println("totalOutFill" + totalOutFill);
            if (totalOutFill == null) {
                totalOutFill = "0";
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTotalOutFill Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return totalOutFill;
    }

    public ArrayList getVehicleMonthlyReport(FuelManagementTO fuelTO, int userId, String from, String to) {
        Map map = new HashMap();
        ArrayList List = new ArrayList();
        try {

            map.put("regNo", fuelTO.getRegNo());
            map.put("from", from);
            map.put("to", to);
            //////System.out.println("from" + from);
            //////System.out.println("to" + to);
            List = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.getVehicleReport", map);
            //////System.out.println("List" + List.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return List;
    }

    public ArrayList getMonthlyAvgReport(FuelManagementTO fuelTO, int userId, String from, String to) {
        Map map = new HashMap();
        ArrayList List = new ArrayList();
        try {
            map.put("regNo", fuelTO.getRegNo());
            map.put("from", from);
            map.put("to", to);
            //////System.out.println("from" + from);
            //////System.out.println("to" + to);
            List = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.getAvgReport", map);
            //////System.out.println("List" + List.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return List;
    }

    public ArrayList getFuelPriceNew() {
        Map map = new HashMap();
        ArrayList fuelPriceList = new ArrayList();
        try {
            fuelPriceList = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.getFuelPriceNew", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getFuelPrice", sqlException);
        }
        return fuelPriceList;
    }

    public ArrayList getBunkList() {
        Map map = new HashMap();
        ArrayList bunkList = new ArrayList();
        try {
            if (getSqlMapClientTemplate().queryForList("fuelManagement.getBunkList", map) != null) {
                bunkList = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.getBunkList", map);
                //////System.out.println("bunkList in DAO = " + bunkList.size());
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBunkList Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBunkList", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBunkList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBunkList", sqlException);
        }
        return bunkList;
    }

    public int insertNewfuelPrice(FuelManagementTO fuelTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("bunkId", fuelTO.getBunkId());
        map.put("rate", fuelTO.getRate());

        String[] sdat = fuelTO.getStartDate().split("-");
        String sdate = sdat[2] + "-" + sdat[1] + "-" + sdat[0];
        map.put("startdate", sdate);

        String[] edat = fuelTO.getEndDate().split("-");
        String edate = edat[2] + "-" + edat[1] + "-" + edat[0];
        map.put("enddate", edate);

        //////System.out.println("map = " + map);

        try {
            status = (Integer) getSqlMapClientTemplate().update("fuelManagement.insertNewfuelPrice", map);
            //////System.out.println("status" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertNewfuelPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertNewfuelPrice", sqlException);
        }
        return status;
    }

    public ArrayList getVehicleTypeCostList(FuelManagementTO fuelTO) {
        Map map = new HashMap();
        ArrayList vehicleTypeCostList = new ArrayList();
        try {
            map.put("countryId", fuelTO.getCountryId());
            vehicleTypeCostList = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.getVehicleTypeCostList", map);
            //////System.out.println("vehicleTypeCostList in DAO = " + vehicleTypeCostList.size());

        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleTypeCostList Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBunkList", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleTypeCostList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleTypeCostList", sqlException);
        }
        return vehicleTypeCostList;
    }

    public ArrayList getCountryList() {
        Map map = new HashMap();
        ArrayList countryList = new ArrayList();
        try {

            countryList = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.getCountryMasterList", map);
            //////System.out.println("countryList in DAO = " + countryList.size());

        } catch (NullPointerException ne) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("countryList Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBunkList", ne);
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("countryList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleTypeCostList", sqlException);
        }
        return countryList;
    }

    public ArrayList getVehicleTypeList() {
        Map map = new HashMap();
        ArrayList vehicleTypeList = new ArrayList();
        try {

            vehicleTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.getVehicleTypeList", map);
            //////System.out.println("vehicleTypeList in DAO = " + vehicleTypeList.size());

        } catch (NullPointerException ne) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleTypeList Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBunkList", ne);
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleTypeList", sqlException);
        }
        return vehicleTypeList;
    }

    public ArrayList editVehicleTypeList(FuelManagementTO fuelTO) {
        Map map = new HashMap();
        ArrayList editVehicleTypeList = new ArrayList();
        try {
            map.put("vehicleTypeId", fuelTO.getVehicleTypeId());
            editVehicleTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.editVehicleTypeList", map);
            //////System.out.println("editVehicleTypeList in DAO = " + editVehicleTypeList.size());

        } catch (NullPointerException ne) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("editVehicleTypeList Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBunkList", ne);
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("editVehicleTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "editVehicleTypeList", sqlException);
        }
        return editVehicleTypeList;
    }

    public int saveVehicleTypeOperatingCost(FuelManagementTO fuelTO) {
        Map map = new HashMap();
        int insertStatus = 0;
        try {
            String[] vehicleTypeId = fuelTO.getVehicletypeid();
            if (vehicleTypeId.length > 0) {
                for (int i = 0; i < vehicleTypeId.length; i++) {
                    map.put("countryId", fuelTO.getCountryId());
                    map.put("vehicleTypeId", vehicleTypeId[i]);
                    map.put("costPerKm", fuelTO.getCostPerKm()[i]);
                    map.put("tollGateCostPerKm", fuelTO.getTollGateCostPerKm()[i]);
                    map.put("miscAmt", fuelTO.getMiscAmt()[i]);
                    map.put("driverVehicleIncentive", fuelTO.getDriverVehicleIncentive()[i]);
                    if (fuelTO.getEtcAmount()[i].equals("")) {
                        map.put("etcAmount", "0");
                    } else {
                        map.put("etcAmount", fuelTO.getEtcAmount()[i]);
                    }
                    map.put("userId", fuelTO.getUserId());
                    if (fuelTO.getVehicleTypeOperatingCostId().length >= i) {
                        map.put("vehicleTypeOperatingCostId", fuelTO.getVehicleTypeOperatingCostId()[i]);
                        //////System.out.println("map Edit" + map);
                        insertStatus = (Integer) getSqlMapClientTemplate().update("fuelManagement.updateVehicleTypeOperatingCost", map);
                        //////System.out.println("insertStatus in DAO = " + insertStatus);
                    } else {
                        //////System.out.println("map insert " + map);
                        insertStatus = (Integer) getSqlMapClientTemplate().insert("fuelManagement.saveVehicleTypeOperatingCost", map);
                        //////System.out.println("insertStatus in DAO = " + insertStatus);
                    }

                }
            }

        } catch (NullPointerException ne) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveVehicleTypeOperatingCost Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBunkList", ne);
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveVehicleTypeOperatingCost Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveVehicleTypeOperatingCost", sqlException);
        }
        return insertStatus;
    }

    public ArrayList getFuelConfigVehicleTypeList(String countryId) {
        Map map = new HashMap();
        ArrayList fuelConfigVehicleTypeList = new ArrayList();
        try {
            map.put("countryId", countryId);
            //////System.out.println("my map value" + map);
            fuelConfigVehicleTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.getFuelConfigVehicleTypeList", map);
            //////System.out.println("fuelConfigVehicleTypeList in DAO = " + fuelConfigVehicleTypeList.size());

        } catch (NullPointerException ne) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("fuelConfigVehicleTypeList Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBunkList", ne);
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("fuelConfigVehicleTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getfuelConfigVehicleTypeList", sqlException);
        }
        return fuelConfigVehicleTypeList;
    }

    public ArrayList getCheckFuelList(String countryId) {
        Map map = new HashMap();
        ArrayList checkFuelList = new ArrayList();
        try {
            map.put("countryId", countryId);
            //////System.out.println("my map value is " + map);
            checkFuelList = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.getCheckFuelList", map);
            //////System.out.println("checkFuelList in DAO = " + checkFuelList.size());

        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkFuelList Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBunkList", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkFuelList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleTypeList", sqlException);
        }
        return checkFuelList;
    }

    public ArrayList viewMfrMilleageAgeing(FuelManagementTO fuelTO) {
        Map map = new HashMap();
        ArrayList editVehicleTypeList = new ArrayList();
        try {
            map.put("mfrId", fuelTO.getMfrId());
            //////System.out.println("map = " + map);
            editVehicleTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.viewMfrMilleageAgeing", map);
            //////System.out.println("editVehicleTypeList in DAO = " + editVehicleTypeList.size());

        } catch (NullPointerException ne) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("editVehicleTypeList Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBunkList", ne);
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("editVehicleTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "editVehicleTypeList", sqlException);
        }
        return editVehicleTypeList;
    }

    public int saveMfrMilleageAgeing(FuelManagementTO fuelTO, int userId) {
        Map map = new HashMap();
        int insertStatus = 0;
        try {
            map.put("mfrId", fuelTO.getMfrId());
            map.put("mfrAge", fuelTO.getMfrAge());
            map.put("mfrToAge", fuelTO.getMfrToAge());
            map.put("userId", userId);
            String ageStatus = "";
            ageStatus = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getMfrMilleageAgeing", map);
            if(ageStatus == null){
            insertStatus = (Integer) getSqlMapClientTemplate().update("fuelManagement.saveMfrMilleageAgeing", map);
            }else{
            map.put("ageingId", fuelTO.getAgeingId());
            map.put("status", fuelTO.getStatus());
            insertStatus = (Integer) getSqlMapClientTemplate().update("fuelManagement.updateMfrMilleageAgeing", map);
            insertStatus = 2;
            }

        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveMfrMilleageAgeing Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveMfrMilleageAgeing", sqlException);
        }
        return insertStatus;
    }
    
    
    public ArrayList viewMfrModelList(FuelManagementTO fuelTO) {
        Map map = new HashMap();
        ArrayList viewMfrModelList = new ArrayList();
        try {
            map.put("mfrId", fuelTO.getMfrId());
            //////System.out.println("map = " + map);
            viewMfrModelList = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.viewMfrModelList", map);
            //////System.out.println("viewMfrMilleageConfigDetails in DAO = " + viewMfrModelList.size());

        
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("viewMfrModelList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "viewMfrModelList", sqlException);
        }
        return viewMfrModelList;
    }
    
    public ArrayList viewMfrMilleageConfigDetails(FuelManagementTO fuelTO) {
        Map map = new HashMap();
        ArrayList editVehicleTypeList = new ArrayList();
        try {
            map.put("mfrId", fuelTO.getMfrId());
            map.put("modelId", fuelTO.getModelId());
            map.put("vehicleTypeId", fuelTO.getVehicleTypeId());
            //////System.out.println("map = " + map);
            editVehicleTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("fuelManagement.viewMfrMilleageConfigDetails", map);
            //////System.out.println("viewMfrMilleageConfigDetails in DAO = " + editVehicleTypeList.size());

        
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("viewMfrMilleageConfigDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "viewMfrMilleageConfigDetails", sqlException);
        }
        return editVehicleTypeList;
    }
    
    
    
    public int updateMfrMilleage(FuelManagementTO fuelTO, int userId) {
        Map map = new HashMap();
        int insertStatus = 0;
        try {
            String ageingId[] = null;
            String vehicleTypeId[] = null;
            String modelId[] = null;
            String mfrId[] = null;
            String kmPerLitter[] = null;
            String kmPerLitterLoaded[] = null;
            ageingId = fuelTO.getAgeingId().split("~");
            vehicleTypeId = fuelTO.getVehicleTypeId().split("~");
            modelId = fuelTO.getModelId().split("~");
            mfrId = fuelTO.getMfrId().split("~");
            kmPerLitter = fuelTO.getKmPerLitter().split("~");
            kmPerLitterLoaded = fuelTO.getKmPerLitterLoaded().split("~");
            for(int i=0; i<ageingId.length; i++){
            map.put("ageingId", ageingId[i]);
            map.put("vehicleTypeId", vehicleTypeId[i]);
            map.put("modelId", modelId[i]);
            map.put("mfrId", mfrId[i]);
            map.put("kmPerLitter", kmPerLitter[i]);
            map.put("kmPerLitterLoaded", kmPerLitterLoaded[i]);
            map.put("userId", userId);
            String ageStatus = "";
            ageStatus = (String) getSqlMapClientTemplate().queryForObject("fuelManagement.getMfrMilleage", map);
            if(ageStatus == null){
            insertStatus = (Integer) getSqlMapClientTemplate().update("fuelManagement.saveMfrMilleage", map);
            }else{
//            map.put("ageingId", ageingId[i]);
            map.put("status", fuelTO.getStatus());
            insertStatus = (Integer) getSqlMapClientTemplate().update("fuelManagement.updateMfrMilleage", map);
            insertStatus = 2;
            }
                
            }

        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveMfrMilleageAgeing Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveMfrMilleageAgeing", sqlException);
        }
        return insertStatus;
    }
}
