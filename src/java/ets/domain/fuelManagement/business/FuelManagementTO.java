/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.fuelManagement.business;

import java.util.ArrayList;

/**
 *
 * @author vinoth
 */
public class FuelManagementTO {

    private String kmPerLitter = "";
    private String kmPerLitterLoaded = "";
    private String status = "";
    private String ageingId = "";
    private String mfrAge = "";
    private String mfrToAge = "";
    private String countryName = "";
    private String fuelCostPerKm = "";
    private String tollCostPerKm = "";
    private String miscCostPerKm = "";
    private String etcCost = "";
    private String driverIncentive = "";
    private String vehicleTYpeCostId = "";
    private String countryId = "";
    private String fuelPriceDate = "";
    private String vehicletypeid[];
    private String costPerKm[];
    private String tollGateCostPerKm[];
    private String miscAmt[];
    private String driverVehicleIncentive[];
    private String userId = "";
    private String etcAmount[];
    ///18 april 2015
    private String vehicleTypeOperatingCostId[];
    private String vehTypeId = "1";
    private String companyId = "0";
    private String servicePtId = "0";
    private String owningCompId = "";
    private String usingCompId = "";
    private String vehicleId = "";
    private String usageTypeId = "0";
    private String runReading = "";
    private String regNo = "";
    private String servicePtName = "";
    private String companyName = "";
    private String typeName = "";
    private String usageType = "";
    private String typeId = "1";
    private String activeInd = "";
    private String mfr = "";
    private String model = "";
    private String present = "";
    private String previous = "";
    private String rate = "";
    private String date = "";
    private String fuelFilled = "";
    private String total = "";
    private String fuel = "";
    private String totalKm = "";
    private String avgKm = "";
    private String outFillKm = "";
    private String place = "";
    private String couponNo = "";
    private String amount = "";
    private String tankReading = "";
    private String takRead = "";
    private String outFill = "0";
    private String fromDate = "";
    private String toDate = "";
    private String toKM = "";
    private String fromKm = "";
    private String capacity = "";
    private String minLevel = "";
    private String toYear = "";
    private String fromYear = "";
    private String mfrName = "";
    private String modelName = "";
    private String modelId = "0";
    private String mfrId = "0";
    private String milleage = "";
    private String owningComp = "";
//    endDate startDate fuelPrice fuelPriceId fuelLocatName fillingStationId
    private String endDate = "";
    private String startDate = "";
    private String fuelPrice = "";
    private String fuelPriceId = "";
    private String fuelLocatName = "";
    private String fillingStationId = "";
    private String reeferMileagePerKm = "";
    private String vehicleTypeId = "0";
    private String vehicleTypeName = "";

    private String bunkName = "";
    private String bunkId = "";

    ArrayList avg = new ArrayList();
    ArrayList fuelMonth = new ArrayList();
    ArrayList fuelOut = new ArrayList();

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getOutFill() {
        return outFill;
    }

    public void setOutFill(String outFill) {
        this.outFill = outFill;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getServicePtId() {
        return servicePtId;
    }

    public void setServicePtId(String servicePtId) {
        this.servicePtId = servicePtId;
    }

    public String getVehTypeId() {
        return vehTypeId;
    }

    public void setVehTypeId(String vehTypeId) {
        this.vehTypeId = vehTypeId;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getServicePtName() {
        return servicePtName;
    }

    public void setServicePtName(String servicePtName) {
        this.servicePtName = servicePtName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFuelFilled() {
        return fuelFilled;
    }

    public void setFuelFilled(String fuelFilled) {
        this.fuelFilled = fuelFilled;
    }

    public String getPresent() {
        return present;
    }

    public void setPresent(String present) {
        this.present = present;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAvgKm() {
        return avgKm;
    }

    public void setAvgKm(String avgKm) {
        this.avgKm = avgKm;
    }

    public String getCouponNo() {
        return couponNo;
    }

    public void setCouponNo(String couponNo) {
        this.couponNo = couponNo;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getTotalKm() {
        return totalKm;
    }

    public void setTotalKm(String totalKm) {
        this.totalKm = totalKm;
    }

    public String getOutFillKm() {
        return outFillKm;
    }

    public void setOutFillKm(String outFillKm) {
        this.outFillKm = outFillKm;
    }

    public String getTankReading() {
        return tankReading;
    }

    public void setTankReading(String tankReading) {
        this.tankReading = tankReading;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getFromKm() {
        return fromKm;
    }

    public void setFromKm(String fromKm) {
        this.fromKm = fromKm;
    }

    public String getToKM() {
        return toKM;
    }

    public void setToKM(String toKM) {
        this.toKM = toKM;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getMinLevel() {
        return minLevel;
    }

    public void setMinLevel(String minLevel) {
        this.minLevel = minLevel;
    }

    public String getMilleage() {
        return milleage;
    }

    public void setMilleage(String milleage) {
        this.milleage = milleage;
    }

    public String getFromYear() {
        return fromYear;
    }

    public void setFromYear(String fromYear) {
        this.fromYear = fromYear;
    }

    public String getMfrName() {
        return mfrName;
    }

    public void setMfrName(String mfrName) {
        this.mfrName = mfrName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getToYear() {
        return toYear;
    }

    public void setToYear(String toYear) {
        this.toYear = toYear;
    }

    public String getMfrId() {
        return mfrId;
    }

    public void setMfrId(String mfrId) {
        this.mfrId = mfrId;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getUsageTypeId() {
        return usageTypeId;
    }

    public void setUsageTypeId(String usageTypeId) {
        this.usageTypeId = usageTypeId;
    }

    public String getOwningCompId() {
        return owningCompId;
    }

    public void setOwningCompId(String owningCompId) {
        this.owningCompId = owningCompId;
    }

    public String getUsingCompId() {
        return usingCompId;
    }

    public void setUsingCompId(String usingCompId) {
        this.usingCompId = usingCompId;
    }

    public String getUsageType() {
        return usageType;
    }

    public void setUsageType(String usageType) {
        this.usageType = usageType;
    }

    public ArrayList getAvg() {
        return avg;
    }

    public void setAvg(ArrayList avg) {
        this.avg = avg;
    }

    public String getMfr() {
        return mfr;
    }

    public void setMfr(String mfr) {
        this.mfr = mfr;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getTakRead() {
        return takRead;
    }

    public void setTakRead(String takRead) {
        this.takRead = takRead;
    }

    public ArrayList getFuelMonth() {
        return fuelMonth;
    }

    public void setFuelMonth(ArrayList fuelMonth) {
        this.fuelMonth = fuelMonth;
    }

    public ArrayList getFuelOut() {
        return fuelOut;
    }

    public void setFuelOut(ArrayList fuelOut) {
        this.fuelOut = fuelOut;
    }

    public String getRunReading() {
        return runReading;
    }

    public void setRunReading(String runReading) {
        this.runReading = runReading;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getOwningComp() {
        return owningComp;
    }

    public void setOwningComp(String owningComp) {
        this.owningComp = owningComp;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getFillingStationId() {
        return fillingStationId;
    }

    public void setFillingStationId(String fillingStationId) {
        this.fillingStationId = fillingStationId;
    }

    public String getFuelLocatName() {
        return fuelLocatName;
    }

    public void setFuelLocatName(String fuelLocatName) {
        this.fuelLocatName = fuelLocatName;
    }

    public String getFuelPrice() {
        return fuelPrice;
    }

    public void setFuelPrice(String fuelPrice) {
        this.fuelPrice = fuelPrice;
    }

    public String getFuelPriceId() {
        return fuelPriceId;
    }

    public void setFuelPriceId(String fuelPriceId) {
        this.fuelPriceId = fuelPriceId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getBunkId() {
        return bunkId;
    }

    public void setBunkId(String bunkId) {
        this.bunkId = bunkId;
    }

    public String getBunkName() {
        return bunkName;
    }

    public void setBunkName(String bunkName) {
        this.bunkName = bunkName;
    }

    public String getReeferMileagePerKm() {
        return reeferMileagePerKm;
    }

    public void setReeferMileagePerKm(String reeferMileagePerKm) {
        this.reeferMileagePerKm = reeferMileagePerKm;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public String[] getCostPerKm() {
        return costPerKm;
    }

    public void setCostPerKm(String[] costPerKm) {
        this.costPerKm = costPerKm;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getDriverIncentive() {
        return driverIncentive;
    }

    public void setDriverIncentive(String driverIncentive) {
        this.driverIncentive = driverIncentive;
    }

    public String[] getDriverVehicleIncentive() {
        return driverVehicleIncentive;
    }

    public void setDriverVehicleIncentive(String[] driverVehicleIncentive) {
        this.driverVehicleIncentive = driverVehicleIncentive;
    }

    public String[] getEtcAmount() {
        return etcAmount;
    }

    public void setEtcAmount(String[] etcAmount) {
        this.etcAmount = etcAmount;
    }

    public String getEtcCost() {
        return etcCost;
    }

    public void setEtcCost(String etcCost) {
        this.etcCost = etcCost;
    }

    public String getFuelCostPerKm() {
        return fuelCostPerKm;
    }

    public void setFuelCostPerKm(String fuelCostPerKm) {
        this.fuelCostPerKm = fuelCostPerKm;
    }

    public String getFuelPriceDate() {
        return fuelPriceDate;
    }

    public void setFuelPriceDate(String fuelPriceDate) {
        this.fuelPriceDate = fuelPriceDate;
    }

    public String[] getMiscAmt() {
        return miscAmt;
    }

    public void setMiscAmt(String[] miscAmt) {
        this.miscAmt = miscAmt;
    }

    public String getMiscCostPerKm() {
        return miscCostPerKm;
    }

    public void setMiscCostPerKm(String miscCostPerKm) {
        this.miscCostPerKm = miscCostPerKm;
    }

    public String getTollCostPerKm() {
        return tollCostPerKm;
    }

    public void setTollCostPerKm(String tollCostPerKm) {
        this.tollCostPerKm = tollCostPerKm;
    }

    public String[] getTollGateCostPerKm() {
        return tollGateCostPerKm;
    }

    public void setTollGateCostPerKm(String[] tollGateCostPerKm) {
        this.tollGateCostPerKm = tollGateCostPerKm;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getVehicleTYpeCostId() {
        return vehicleTYpeCostId;
    }

    public void setVehicleTYpeCostId(String vehicleTYpeCostId) {
        this.vehicleTYpeCostId = vehicleTYpeCostId;
    }

    public String[] getVehicleTypeOperatingCostId() {
        return vehicleTypeOperatingCostId;
    }

    public void setVehicleTypeOperatingCostId(String[] vehicleTypeOperatingCostId) {
        this.vehicleTypeOperatingCostId = vehicleTypeOperatingCostId;
    }

    public String[] getVehicletypeid() {
        return vehicletypeid;
    }

    public void setVehicletypeid(String[] vehicletypeid) {
        this.vehicletypeid = vehicletypeid;
    }

    public String getAgeingId() {
        return ageingId;
    }

    public void setAgeingId(String ageingId) {
        this.ageingId = ageingId;
    }

    public String getMfrAge() {
        return mfrAge;
    }

    public void setMfrAge(String mfrAge) {
        this.mfrAge = mfrAge;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKmPerLitter() {
        return kmPerLitter;
    }

    public void setKmPerLitter(String kmPerLitter) {
        this.kmPerLitter = kmPerLitter;
    }

    public String getMfrToAge() {
        return mfrToAge;
    }

    public void setMfrToAge(String mfrToAge) {
        this.mfrToAge = mfrToAge;
    }

    public String getKmPerLitterLoaded() {
        return kmPerLitterLoaded;
    }

    public void setKmPerLitterLoaded(String kmPerLitterLoaded) {
        this.kmPerLitterLoaded = kmPerLitterLoaded;
    }

}
