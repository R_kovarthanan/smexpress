/*---------------------------------------------------------------------------
 * ContractController.java
 * Mar 3, 2009
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
-------------------------------------------------------------------------*/
package ets.domain.contract.web;

import ets.arch.exception.FPRuntimeException;
import ets.arch.web.BaseController;
import ets.domain.contract.business.ContractBP;
import ets.domain.contract.business.ContractTO;
import ets.domain.users.business.LoginBP;
import ets.domain.util.FPLogUtils;
import ets.domain.util.ParveenErrorConstants;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;

/****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver              Date                      Author                    Change
 * ---------------------------------------------------------------------------
 * 1.0           Mar 3, 2009              vijay			       Created
 *
 ******************************************************************************/
public class ContractController extends BaseController {

    ContractCommand contractCommand;
    ContractBP contractBP;
    LoginBP loginBP;

    public ContractBP getContractBP() {
        return contractBP;
    }

    public void setContractBP(ContractBP contractBP) {
        this.contractBP = contractBP;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    /**
     * This method used to bind the request values to the command object.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();
           initialize(request);


    }

    /**
     * This method caters to get dept subjects
     *
     * @param 	request - Http request object
     *
     * @param 	response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public ModelAndView viewContracts(HttpServletRequest request, HttpServletResponse reponse, ContractCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Customer >>Contract Details >> Manage Contract";
        String pageTitle = "Manage Contract";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            path = "content/contract/manageContract.jsp";

            ArrayList contractDetails = new ArrayList();
            contractDetails = contractBP.getContractDetails();
            System.out.println("size in cont" + contractDetails.size());
            if (contractDetails.size() != 0) {
                request.setAttribute("contractDetails", contractDetails);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Contract Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method caters to get dept subjects
     *
     * @param 	request - Http request object
     *
     * @param 	response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public ModelAndView alterContractPage(HttpServletRequest request, HttpServletResponse reponse, ContractCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Customer >>Contract Details >> Alter Contract";
        String pageTitle = "Alter Contract";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/contract/alterContract.jsp";
            System.out.println("custId1" + request.getParameter("custId"));
            //System.out.println("custId2"+contractCommand.getCustId());
            String cust = request.getParameter("custId");
            System.out.println("cust2" + cust);
            int custId = Integer.parseInt(cust);
            System.out.println("cust3" + custId);
            ArrayList custContractDetails = new ArrayList();
            ContractTO contractTo = null;
            ArrayList mfrList = new ArrayList();

            custContractDetails = contractBP.custContractDetails(custId);
            mfrList = contractBP.getMfrList();
            int mfrId = 0;
            ArrayList modlist = new ArrayList();

            modlist = contractBP.getModList(mfrId);
            request.setAttribute("custContractDetails", custContractDetails);
            request.setAttribute("mfrList", mfrList);
            request.setAttribute("modList", modlist);


        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Contract Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method caters to get dept subjects
     *
     * @param 	request - Http request object
     *
     * @param 	response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public void getModels(HttpServletRequest request, HttpServletResponse response, ContractCommand command) throws IOException {
        String path = "";

        HttpSession session = request.getSession();
        PrintWriter pw = response.getWriter();

        ContractTO contaractTO = null;
        ArrayList modDetails = new ArrayList();
        try {
            int mfrId = 0;
            mfrId = Integer.parseInt(request.getParameter("mfrId"));
            System.out.println("mfrId" + mfrId);
            String returnValue = "";
            modDetails = contractBP.getModList(mfrId);
            Iterator itr = modDetails.iterator();
            while (itr.hasNext()) {
                contaractTO = (ContractTO) itr.next();
                returnValue = returnValue + "," + contaractTO.getModelDetails();
                System.out.println("returnValue" + returnValue);
            }
            pw.print(returnValue);

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to search Course sems--> " + exception);
            exception.printStackTrace();
        }

    }

    /**
     * This method caters to add contract's
     *
     * @param 	request - Http request object
     *
     * @param 	response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public ModelAndView addContractPage(HttpServletRequest request, HttpServletResponse reponse, ContractCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Customer >>Contract Details >> Add Contract";
        String pageTitle = "Add Contract";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/contract/addContract.jsp";
            ArrayList custList = new ArrayList();
            ArrayList mfrList = new ArrayList();

            custList = contractBP.getCustList();
            mfrList = contractBP.getMfrList();
            request.setAttribute("mfrList", mfrList);
            request.setAttribute("custList", custList);
            request.removeAttribute("freeServiceDetails");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Contract Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView insertContract(HttpServletRequest request, HttpServletResponse reponse, ContractCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Customer >>Contract Details >> Manage Contract";
        String pageTitle = "Manage Contract";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            int status = 0;
            path = "content/contract/manageContract.jsp";
            int userId = (Integer) session.getAttribute("userId");
            String sdate = request.getParameter("sdate");
            System.out.println("sdate" + sdate);

            String edate = request.getParameter("edate");
            System.out.println("edate" + edate);

            String custId = request.getParameter("custId");
            System.out.println("custId" + custId);
            String[] mfr = request.getParameterValues("hiddenElement1");
            String[] mod = request.getParameterValues("hiddenElement2");
            String[] spare = request.getParameterValues("hiddenElement3");
            String[] labour = request.getParameterValues("hiddenElement4");

            String mfrvalues = null;
            String modvalues = null;
            String sparvalues = null;
            String labovalues = null;
            int contractId = 0;
            contractId = contractBP.getContractId(sdate, edate, custId, userId);
            System.out.println("contractId in cont" + contractId);
            System.out.println("mfr length" + mfr.length);
            for (int i = 0; i < mfr.length; i++) {
                mfrvalues = request.getParameter(mfr[i]);
                modvalues = request.getParameter(mod[i]);
                sparvalues = request.getParameter(spare[i]);
                labovalues = request.getParameter(labour[i]);
                status = contractBP.insertContractDetails(mfrvalues, modvalues, sparvalues, labovalues, contractId, userId);
            }

            ArrayList contractDetails = new ArrayList();
            contractDetails = contractBP.getContractDetails();
            request.setAttribute("contractDetails", contractDetails);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Contract Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView updateContract(HttpServletRequest request, HttpServletResponse reponse, ContractCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        contractCommand = command;
        HttpSession session = request.getSession();
        ContractTO contractTO = null;
        String path = "";
        String menuPath = "";
        menuPath = "Customer >>Contract Details >> Manage Contract";
        String pageTitle = "Manage Contract";
        request.setAttribute("pageTitle", pageTitle);
        //int userId=(Integer)session.getAttribute("userId");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            path = "content/contract/manageContract.jsp";
            int userId = (Integer) session.getAttribute("userId");
            String sdate = request.getParameter("sdate");
            System.out.println("sdate" + sdate);

            String edate = request.getParameter("edate");
            System.out.println("edate" + edate);

            int custId = Integer.parseInt(request.getParameter("custId"));
            System.out.println("custId" + custId);

            String[] check = request.getParameterValues("check");

//            if (mfrInsert != null) {
//                System.out.println("vijaykanth");
//                insertContract(request, reponse, command);
//            }

            String[] mfr = request.getParameterValues("mfrIds");
            String[] mod = request.getParameterValues("modIds");
            String[] spare = request.getParameterValues("sparePers");
            String[] labour = request.getParameterValues("labourPers");
            String[] status = request.getParameterValues("activeInds");
            String[] index = request.getParameterValues("selectedindex");

            if (index != null) {
                ArrayList contracts = new ArrayList();
                System.out.println("lenght:" + index.length);
                System.out.println("lenght val:" + index[0]);
                 System.out.println("mfr lenght:" + mfr.length);

                int i = 0;
                int temp = 0;
                while (i < index.length) {
                    temp = Integer.parseInt(index[i]);
                    contractTO = new ContractTO();
                    contractTO.setCheck((check[temp]));
                    contractTO.setMfrId(Integer.parseInt(mfr[temp]));
                    contractTO.setModId(Integer.parseInt(mod[temp]));
                    contractTO.setSparePers(Float.parseFloat(spare[temp]));
                    contractTO.setLabourPers(Float.parseFloat(labour[temp]));
                    contractTO.setStatus(status[temp]);
                    contracts.add(contractTO);
                    System.out.println("mod[i]=" + mod[temp]);
                    System.out.println("spare[i]=" + spare[temp]);
                    System.out.println("labour[i]=" + labour[temp]);
                    i++;
                }
                int modify = 0;

                modify = contractBP.modifyContractDetails(custId, sdate, edate, contracts, userId);
            }
            ArrayList contractDetails = new ArrayList();
            contractDetails = contractBP.getContractDetails();
            request.setAttribute("contractDetails", contractDetails);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Contract Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //Rathimeena Contract Start
    /**
     * This method caters to get dept subjects
     *
     * @param 	request - Http request object
     *
     * @param 	response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public ModelAndView viewContractDetails(HttpServletRequest request, HttpServletResponse reponse, ContractCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Customer >>Contract Details >> Manage Contract";
        String pageTitle = "Manage Contract";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            path = "content/contract/manageContractDetails.jsp";

            ArrayList contractDetails = new ArrayList();
            contractDetails = contractBP.getContractDetails();
            System.out.println("size in cont" + contractDetails.size());
            if (contractDetails.size() != 0) {
                request.setAttribute("contractDetails", contractDetails);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Contract Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method caters to add contract's
     *
     * @param 	request - Http request object
     *
     * @param 	response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public ModelAndView addCustContractPage(HttpServletRequest request, HttpServletResponse reponse, ContractCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Customer >>Contract Details >> Add Contract";
        String pageTitle = "Add Contract";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/contract/addCustContract.jsp";
            ArrayList custList = new ArrayList();
            ArrayList routeList = new ArrayList();

            custList = contractBP.getCustList();
            request.setAttribute("custList", custList);
            routeList = contractBP.getRouteList();
            request.setAttribute("routeList", routeList);
            request.removeAttribute("freeServiceDetails");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to add Cust Contract Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView insertCustContract(HttpServletRequest request, HttpServletResponse reponse, ContractCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Customer >>Customer Contract >> Manage Contract";
        String pageTitle = "Manage Contract";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            int status = 0;
            path = "content/contract/manageContractDetails.jsp";
            int userId = (Integer) session.getAttribute("userId");
            String sdate = request.getParameter("sdate");
            //System.out.println("sdate" + sdate);

            String edate = request.getParameter("edate");
            //System.out.println("edate" + edate);

            String custId = request.getParameter("custId");
            //System.out.println("custId" + custId);
            String[] rId = request.getParameterValues("hiddenElement1");
            String[] tonRate = request.getParameterValues("hiddenElement2");

            String rIdValues = null;
            String tonRateValues = null;
            int contractId = 0;
            contractId = contractBP.getContractId(sdate, edate, custId, userId);

            System.out.println("contractId in cont" + contractId);
            System.out.println("mfr length" + rId.length);
            for (int i = 0; i < rId.length; i++) {
                rIdValues = request.getParameter(rId[i]);
                tonRateValues = request.getParameter(tonRate[i]);
                System.out.println("insertCustContract...");
                System.out.println("rIdValues: "+rIdValues);
                System.out.println("tonRateValues: "+tonRateValues);
                status = contractBP.insertCustContractDetails(rIdValues, tonRateValues, contractId, userId);
            }

            ArrayList contractDetails = new ArrayList();
            contractDetails = contractBP.getContractDetails();
            request.setAttribute("contractDetails", contractDetails);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Cust Contract Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method caters to get dept subjects
     *
     * @param 	request - Http request object
     *
     * @param 	response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public ModelAndView alterCustContractPage(HttpServletRequest request, HttpServletResponse reponse, ContractCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Customers >> Customer Contract >> Alter Contract Details";
        String pageTitle = "Alter Contract";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/contract/alterCustContract.jsp";
            //System.out.println("custId1" + request.getParameter("custId"));
            //System.out.println("custId2"+contractCommand.getCustId());
            String cust = request.getParameter("custId");
            //System.out.println("cust2" + cust);
            int custId = Integer.parseInt(cust);
            //System.out.println("cust3" + custId);
            ArrayList custContractDetails = new ArrayList();
            ArrayList routeList = new ArrayList();

            custContractDetails = contractBP.rathiCustContractDetails(custId);
            request.setAttribute("custContractDetails", custContractDetails);

            routeList = contractBP.getRouteList();
            request.setAttribute("routeList", routeList);
            //request.setAttribute("modList", modlist);


        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to alter Cust Contract Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView updateCustContract(HttpServletRequest request, HttpServletResponse reponse, ContractCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        contractCommand = command;
        HttpSession session = request.getSession();
        ContractTO contractTO = null;
        String path = "";
        String menuPath = "";
        menuPath = "Customer >>Contract Details >> Manage Contract";
        String pageTitle = "Manage Contract";
        request.setAttribute("pageTitle", pageTitle);
        //int userId=(Integer)session.getAttribute("userId");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            path = "content/contract/manageContractDetails.jsp";
            int userId = (Integer) session.getAttribute("userId");
            String sdate = request.getParameter("sdate");
            System.out.println("sdate" + sdate);

            String edate = request.getParameter("edate");
            System.out.println("edate" + edate);

            int custId = Integer.parseInt(request.getParameter("custId"));
            System.out.println("custId" + custId);

            String[] check = request.getParameterValues("check");

//            if (mfrInsert != null) {
//                System.out.println("vijaykanth");
//                insertContract(request, reponse, command);
//            }

            String[] rId = request.getParameterValues("routeId");
            String[] tRate = request.getParameterValues("tonnageRate");
            String[] status = request.getParameterValues("activeInds");
            String[] index = request.getParameterValues("selectedindex");
            System.out.println("index con: : "+index);
            if (index != null) {
                ArrayList contracts = new ArrayList();                
                int i = 0;
                int temp = 0;
                while (i < index.length) {
                    temp = Integer.parseInt(index[i]);
                    contractTO = new ContractTO();
                    contractTO.setCheck((check[temp]));
                    contractTO.setRouteId(rId[temp]);
                    contractTO.setTonnageRate(tRate[temp]);
                    contractTO.setStatus(status[temp]);
                    contracts.add(contractTO);
                    i++;
                }
                int modify = 0;
                modify = contractBP.modifyCustContractDetails(custId, sdate, edate, contracts, userId);
            }
            ArrayList contractDetails = new ArrayList();
            contractDetails = contractBP.getContractDetails();
            request.setAttribute("contractDetails", contractDetails);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to updateCustContract --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    //Rathimeena Contract End
}
