/*---------------------------------------------------------------------------
 * ContractBP.java
 * Mar 3, 2009
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
-------------------------------------------------------------------------*/

package ets.domain.contract.business;

import java.io.Serializable;

/****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver              Date                      Author                    Change
 * ---------------------------------------------------------------------------
 * 1.0           Mar 3, 2009              vijay			       Created
 *
 ******************************************************************************/
public class ContractTO implements Serializable {
    
        private int custId=0;
        private String status="";
        private String custName="";
        private String sdate="";        
        private String edate="";
        private String date="";
        private int mfrId=0;
        private int modId=0;
        private String modName="";
        private float sparePers=0;
        private float labourPers=0;
        private String modelDetails="";
        private int contId=0;
        private String mfrName="";
        private String check="";

        //Rathimeena Cust Contract Start
        private String routeId="";
        private String rId="";
        private String routeName="";
        private String tonnageRate="";
        //Rathimeena Cust Contract End

    public String getCheck() {
        return check;
    }

    public void setCheck(String check) {
        this.check = check;
    }
        

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
        

    public String getModName() {
        return modName;
    }

    public void setModName(String modName) {
        this.modName = modName;
    }
        

    public int getContId() {
        return contId;
    }

    public void setContId(int contId) {
        this.contId = contId;
    }

    public String getModelDetails() {
        return modelDetails;
    }

    public void setModelDetails(String modelDetails) {
        this.modelDetails = modelDetails;
    }


    public int getCustId() {
        return custId;
    }

    public void setCustId(int custId) {
        this.custId = custId;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }



    public float getLabourPers() {
        return labourPers;
    }

    public void setLabourPers(float labourPers) {
        this.labourPers = labourPers;
    }

    public int getMfrId() {
        return mfrId;
    }

    public void setMfrId(int mfrId) {
        this.mfrId = mfrId;
    }

    public int getModId() {
        return modId;
    }

    public void setModId(int modId) {
        this.modId = modId;
    }

   



    public float getSparePers() {
        return sparePers;
    }

    public void setSparePers(float sparePers) {
        this.sparePers = sparePers;
    }

    public String getEdate() {
        return edate;
    }

    public void setEdate(String edate) {
        this.edate = edate;
    }

    public String getSdate() {
        return sdate;
    }

    public void setSdate(String sdate) {
        this.sdate = sdate;
    }

    public String getMfrName() {
        return mfrName;
    }

    public void setMfrName(String mfrName) {
        this.mfrName = mfrName;
    }
    
    //Rathimeena Contract Start
    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getTonnageRate() {
        return tonnageRate;
    }

    public void setTonnageRate(String tonnageRate) {
        this.tonnageRate = tonnageRate;
    }
    public String getrId() {
        return rId;
    }

    public void setrId(String rId) {
        this.rId = rId;
    }
    //Rathimeena Contract End
    
    

}
