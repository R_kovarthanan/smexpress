/*---------------------------------------------------------------------------
 * ContractBP.java
 * Mar 3, 2009
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
-------------------------------------------------------------------------*/
package ets.domain.contract.business;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.contract.data.ContractDAO;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;

/****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver              Date                      Author                    Change
 * ---------------------------------------------------------------------------
 * 1.0           Mar 3, 2009              vijay			       Created
 *
 ******************************************************************************/
public class ContractBP {

    private ContractDAO contractDAO;

    public ContractDAO getContractDAO() {
        return contractDAO;
    }

    public void setContractDAO(ContractDAO contractDAO) {
        this.contractDAO = contractDAO;
    }

    public ArrayList getContractDetails() throws FPRuntimeException, FPBusinessException {
        ArrayList contractDetails = new ArrayList();


        contractDetails = contractDAO.getContractDetails();
        if (contractDetails.size() == 0) {
//           throw new FPBusinessException("EM-GEN-01");
        }

        return contractDetails;
    }   

    public ArrayList getMfrList() throws FPRuntimeException, FPBusinessException {
        ArrayList contractDetails = new ArrayList();
        contractDetails = contractDAO.getMfrList();
        if (contractDetails.size() == 0) {
            // throw new FPBusinessException("EM-SER-8");
        }

        return contractDetails;
    }

    public ArrayList getModList(int mfrId) throws FPRuntimeException, FPBusinessException {
        ArrayList contractDetails = new ArrayList();
        contractDetails = contractDAO.getModList(mfrId);
        if (contractDetails.size() == 0) {
            //throw new FPBusinessException("EM-CON-03");
        }

        return contractDetails;
    }

    public ArrayList getCustList() throws FPRuntimeException, FPBusinessException {
        ArrayList contractDetails = new ArrayList();
        contractDetails = contractDAO.getCustList();
        if (contractDetails.size() == 0) {
            // throw new FPBusinessException("EM-CON-04");
        }
        return contractDetails;
    }

    public int insertContractDetails(String mfrvalues, String modvalues, String sparvalues, String labovalues, int contractId, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        int mfr = Integer.parseInt(mfrvalues);
        int mod = Integer.parseInt(modvalues);
        status = contractDAO.insertContractDetails(mfr, mod, sparvalues, labovalues, contractId, userId);
        if (status == 0) {
            // throw new FPBusinessException("EM-CON-05");
        }
        return status;
    }

    public int getContractId(String sdate, String edate, String custId, int userId) {
        int status = 0;
        String[] date = sdate.split("-");
        String startDate = date[2] + "-" + date[1] + "-" + date[0];
        date = edate.split("-");
        String endDate = date[2] + "-" + date[1] + "-" + date[0];
        status = contractDAO.getContractId(startDate, endDate, custId, userId);
        return status;
    }

    public int modifyContractDetails(int custId, String sdate, String edate, ArrayList contracts, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        String[] date = sdate.split("-");
        String startDate = date[2] + "-" + date[1] + "-" + date[0];
        date = edate.split("-");
        String endDate = date[2] + "-" + date[1] + "-" + date[0];
        ContractTO contractTO = null;
        int contractId = 0;
        contractId = contractDAO.modifyContractDetails(custId, startDate, endDate, userId);

        Iterator itr = contracts.iterator();
        float spare = 0;
        float labour = 0;
        int modId = 0;
        int mfrId = 0;
        String aStatus = "";
        String check = "";
        while (itr.hasNext()) {
            contractTO = new ContractTO();
            contractTO = (ContractTO) itr.next();
            spare = contractTO.getSparePers();
            labour = contractTO.getLabourPers();
            modId = contractTO.getModId();
            mfrId = contractTO.getMfrId();
            aStatus = contractTO.getStatus();
            check = contractTO.getCheck();
            if (check.equalsIgnoreCase("update")) {
                status = contractDAO.updateContractDetails(custId, userId, spare, labour, modId, mfrId, aStatus, contractId);
            } else if (check.equalsIgnoreCase("insert")) {
                status = contractDAO.insertContractDetails(mfrId, modId, spare, labour, contractId, userId);
            }
        }
        if (status == 0) {
            // throw new FPBusinessException("EM-CON-06");
        }

        return status;
    }

    //Rathimeena Contract Start
    public ArrayList getRouteList() throws FPRuntimeException, FPBusinessException {
        ArrayList routeList = new ArrayList();
        routeList = contractDAO.getRouteList();
        if (routeList.size() == 0) {
            // throw new FPBusinessException("EM-SER-8");
        }

        return routeList;
    }
    public ArrayList custContractDetails(int custId) throws FPRuntimeException, FPBusinessException {
         ArrayList contractDetails = new ArrayList();
         ContractTO contractTo = null;
         contractDetails = contractDAO.custContractDetails(custId);
         if (contractDetails.size() == 0) {
             //throw new FPBusinessException("EM-CON-02");
         }

         return contractDetails;
     }

     public int insertCustContractDetails(String rIdValues, String tonRateValues, int contractId, int userId) throws FPRuntimeException, FPBusinessException {
         int status = 0;
         int rId = Integer.parseInt(rIdValues);
         int tonRate = Integer.parseInt(tonRateValues);
         status = contractDAO.insertCustContractDetails(rId, tonRate, contractId, userId);
         if (status == 0) {
             // throw new FPBusinessException("EM-CON-05");
         }
         return status;
     }

     public ArrayList rathiCustContractDetails(int custId) throws FPRuntimeException, FPBusinessException {
         ArrayList custContractDetails = new ArrayList();
         ContractTO contractTo = null;
         custContractDetails = contractDAO.rathiCustContractDetails(custId);
         if (custContractDetails.size() == 0) {
             //throw new FPBusinessException("EM-CON-02");
         }

         return custContractDetails;
     }

     public int modifyCustContractDetails(int custId, String sdate, String edate, ArrayList contracts, int userId) throws FPRuntimeException, FPBusinessException {
         int status = 0;
         String[] date = sdate.split("-");
         String startDate = date[2] + "-" + date[1] + "-" + date[0];
         date = edate.split("-");
         String endDate = date[2] + "-" + date[1] + "-" + date[0];
         ContractTO contractTO = null;
         int contractId = 0;
         contractId = contractDAO.modifyContractDetails(custId, startDate, endDate, userId);

         Iterator itr = contracts.iterator();
         String rId = "";
         String tonRate = "";
         String aStatus = "";
         String check = "";
         while (itr.hasNext()) {
             contractTO = new ContractTO();
             contractTO = (ContractTO) itr.next();
             rId = contractTO.getRouteId();
             tonRate = contractTO.getTonnageRate();
             aStatus = contractTO.getStatus();
             check = contractTO.getCheck();
             
             /*System.out.println("modifyCustContractDetails....");
             System.out.println("rId: "+rId);
             System.out.println("tonRate: "+tonRate);
             System.out.println("contractId: "+contractId);
             System.out.println("aStatus: "+aStatus);*/

             if (check.equalsIgnoreCase("update")) {
                 status = contractDAO.updateCustContractDetails(custId, userId, rId, tonRate, aStatus, contractId);
             } else if (check.equalsIgnoreCase("insert")) {
                 status = contractDAO.insertCustContractDetails(rId, tonRate, contractId, userId);
             }
         }
         if (status == 0) {
             // throw new FPBusinessException("EM-CON-06");
         }

         return status;
    }
    //Rathimeena Contract End

}
