/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.employee.data;

import ets.arch.exception.FPRuntimeException;
import ets.domain.designation.business.DesignationTO;
import ets.domain.employee.business.EmployeeTO;
import ets.domain.users.web.CryptoLibrary;
import ets.domain.util.FPLogUtils;
import ets.domain.util.ThrottleConstants;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

/**
 *
 * @author karudaiyar Subramaniam
 */
public class EmployeeDAO extends SqlMapClientDaoSupport {

    public EmployeeDAO() {
    }
    private final static String CLASS = "EmployeeDAO";

    public ArrayList getDeptList() {

        Map map = new HashMap();
        ArrayList DeptList = new ArrayList();
        //////System.out.println("i am in getdeptlist - jan");
        try {
            DeptList = (ArrayList) getSqlMapClientTemplate().queryForList("employee.getDeptList", map);
            //////System.out.println("DeptList-" + DeptList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "DeptList", sqlException);
        }
        return DeptList;

    }

    public ArrayList getDriverVendorList() {

        Map map = new HashMap();
         map.put("driverVendorTypeId", ThrottleConstants.driverVendorTypeId);
        ArrayList vendorList = new ArrayList();
        //////System.out.println("i am in getDriverVendorList - jan");
        try {
            vendorList = (ArrayList) getSqlMapClientTemplate().queryForList("employee.getDriverVendor", map);
            //////System.out.println("DeptList-" + vendorList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverVendorList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getDriverVendorList", sqlException);
        }
        return vendorList;

    }

   public int addNewUser(EmployeeTO employeeTO, int UserId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", UserId);
        map.put("userName", employeeTO.getUserName());
        CryptoLibrary cLib = new CryptoLibrary();
        String password = employeeTO.getPassword();
        password = cLib.encrypt(password);
        map.put("password", password);
        map.put("roleId", employeeTO.getRoleIds());
        int status = 0;
        int userid = 0;
        try {
            if (employeeTO.getRoleIds() != "0" || employeeTO.getRoleIds() != "") {
                userid = (Integer) getSqlMapClientTemplate().insert("employee.insertUser", map);
                map.put("empUserId", userid);
                status = (Integer) getSqlMapClientTemplate().update("employee.insertUserRole", map);
                //////System.out.println("userId hd = " + userid);
                //////System.out.println("userId = " + userid);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("addNewUser Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "addNewUser", sqlException);
        }
        return userid;
    }
        public int updateUser(EmployeeTO employeeTO, int UserId, int staffUserId, int roleId) {
            Map map = new HashMap();
            /*
             * set the parameters in the map for sending to ORM
             */
            map.put("userId", UserId);
            map.put("userName", employeeTO.getUserName());
            CryptoLibrary cLib = new CryptoLibrary();
            String password = employeeTO.getPassword();
            password = cLib.encrypt(password);
            map.put("password", password);
            map.put("roleId", employeeTO.getRoleId());
            map.put("staffUserId", staffUserId);
            map.put("activeInd", employeeTO.getStatus());
            int status = 0;
            int userid = 0;
            System.out.println("map in update employee user roles = " + map);
            try {
                userid = (Integer) getSqlMapClientTemplate().update("employee.updateUser", map);
                map.put("empUserId", userid);
                System.out.println("map details:"+map);
                status = (Integer) getSqlMapClientTemplate().update("employee.updateUserRole", map);
                
                System.out.println("update user role status:"+status);
                if(status == 0){
                    //value (#staffUserId#,#roleId#,curdate(),'Y',#userId#)
                    int insertRole = (Integer) getSqlMapClientTemplate().update("employee.insertRole", map);
                }
                //////System.out.println("userId = " + userid);
    //            if (status > 0) {
    //                userid = (Integer) getSqlMapClientTemplate().queryForObject("employee.getUserId", map);
    //            }
    
            } catch (Exception sqlException) {
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("addNewUser Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-SYS-01", CLASS, "addNewUser", sqlException);
            }
            return status;
    }

   public int insertEmployeeDetails(EmployeeTO employeeTO, int UserId, int staffUserId) {
        Map map = new HashMap();
        int status = 0;
        int staffId = 0;

        int statusLedger = 0;
        int statusBranch = 0;
        String code = "";
        String[] temp;
        int vehicle_id = 0;
        int currentYear = 0;
        String newYear = "";
        int empLedgerId = 0;
        FileInputStream fis = null;

        /*
         * set the parameters in the map for sending to ORM
         */
        try {
            map.put("userId", UserId);
            //System.out.println("staffUserId in dao" + staffUserId);
            map.put("staffUserId", staffUserId);
            map.put("Name", employeeTO.getName());
            map.put("orgEmpId", employeeTO.getOrgEmpId());
            map.put("Qualification", employeeTO.getQualification());
            //System.out.println("date " + employeeTO.getDateOfBirth());
            //System.out.println("datej " + employeeTO.getDOJ());
            String[] DOBdate = employeeTO.getDateOfBirth().split("-");

            String DOB = DOBdate[2] + "-" + DOBdate[1] + "-" + DOBdate[0];
            String[] DOJdate = employeeTO.getDOJ().split("-");
            String DOJ = DOJdate[2] + "-" + DOJdate[1] + "-" + DOJdate[0];

            map.put("DOB", DOB);
            map.put("DOJ", DOJ);

            map.put("Gender", employeeTO.getGender());
            map.put("Father_Name", employeeTO.getFatherName());
            map.put("Blood_Grp", employeeTO.getBloodGrp());
            map.put("Martial_Status", employeeTO.getMartialStatus());
            map.put("Mobile", employeeTO.getMobile());
            map.put("Phone", employeeTO.getPhone());
            map.put("Email", employeeTO.getEmail());

            map.put("Addr", employeeTO.getAddr());
            map.put("City", employeeTO.getCity());
            map.put("State", employeeTO.getState());
            map.put("Pincode", employeeTO.getPincode());
            map.put("Addr1", employeeTO.getAddr1());
            map.put("City1", employeeTO.getCity1());
            map.put("State1", employeeTO.getState1());
            map.put("Pincode1", employeeTO.getPincode1());
            map.put("cmpId", employeeTO.getCmpId());
            map.put("roleId", employeeTO.getRoleId());
            map.put("gradeId", employeeTO.getGradeId());
            map.put("desigId", employeeTO.getDesigId());
            map.put("deptId", employeeTO.getDeptId());
            map.put("referenceName", employeeTO.getReferenceName());
            map.put("userMovementType", employeeTO.getUserMovementType());
            map.put("contractDriver", employeeTO.getContractDriver());

            map.put("licenseExpDate", employeeTO.getLicenseExpDate());
            map.put("drivingLicenseNo", employeeTO.getDrivingLicenseNo());
            map.put("licenseType", employeeTO.getLicenseType());
            map.put("licenseState", employeeTO.getLicenseState());
            map.put("driverType", employeeTO.getDriverType());

            map.put("salaryType", employeeTO.getSalaryType());
            map.put("basicSalary", employeeTO.getBasicSalary());
            map.put("esiEligible", employeeTO.getEsiEligible());
            map.put("pfEligible", employeeTO.getPfEligible());
            map.put("nomineeName", employeeTO.getNomineeName());
            map.put("nomineeRelation", employeeTO.getNomineeRelation());
            map.put("nomineeAge", employeeTO.getNomineeAge());

            map.put("yearOfExperience", employeeTO.getYearOfExperience());
            map.put("preEmpDesignation", employeeTO.getPreEmpDesignation());
            map.put("prevCompanyName", employeeTO.getPrevCompanyName());
            map.put("prevCompanyAddress", employeeTO.getPrevCompanyAddress());
            map.put("prevCompanyCity", employeeTO.getPrevCompanyCity());
            map.put("preEmpState", employeeTO.getPreEmpState());
            map.put("preEmpPincode", employeeTO.getPreEmpPincode());
            map.put("prevCompanyContact", employeeTO.getPrevCompanyContact());
            map.put("nomineeDob", employeeTO.getNomineeDob());
            map.put("vendorCompany", employeeTO.getVendorCompany());
            String check = "N";
            if (employeeTO.getChec() != null && employeeTO.getChec() != "") {
                check = employeeTO.getChec();
            }
            map.put("chec", check);
            map.put("bankAccountNo", employeeTO.getBankAccountNo());
            String[] relationName = null;
            String[] relation = null;
            String[] relationDOB = null;
            String[] relationAge = null;

            relationName = employeeTO.getRelationName();

            map.put("bankName", employeeTO.getBankName());
            map.put("bankBranchName", employeeTO.getBankBranchName());
//            map.put("branchId", employeeTO.getBranchId());
            map.put("accountHolderName", employeeTO.getAccountHolderName());
            int empCount = 1;
            Object o = getSqlMapClientTemplate().queryForObject("employee.getEmployeeCount", map);
            if(o != null){
                empCount = (Integer)o;
            }
            if (empCount <= 9) {
                map.put("empCode", "EMP00" + empCount);
            } else if (empCount > 9 && empCount < 100) {
                map.put("empCode", "EMP0" + empCount);
            } else {
                map.put("empCode", "EMP" + empCount);
            }
            System.out.println("employeeTO.getPhotoFile() = " + employeeTO.getPhotoFile());
            System.out.println("employeeTO.getLicenseFile() = " + employeeTO.getLicenseFile());
            System.out.println("employeeTO.getPassportFile() = " + employeeTO.getPassportPhotoFile());
            byte[] photoFile = null;
            File file = null;
            if (!"".equals(employeeTO.getPhotoFile())) {
                file = new File(employeeTO.getPhotoFile());
                System.out.println("file"+file);
                fis = new FileInputStream(file);
                photoFile = new byte[(int) file.length()];
                fis.read(photoFile);
                fis.close();
                map.put("photoFile", photoFile);
            } else {
                map.put("photoFile", photoFile);
            }
            byte[] licenseFile = null;
            if (!"".equals(employeeTO.getLicenseFile())) {
                file = new File(employeeTO.getLicenseFile());
                fis = new FileInputStream(file);
                licenseFile = new byte[(int) file.length()];
                fis.read(licenseFile);
                fis.close();
                map.put("licenseFile", licenseFile);
            } else {
                map.put("licenseFile", licenseFile);
            }

            System.out.println(" insertEmployee map:" + map);
            staffId = (Integer) getSqlMapClientTemplate().insert("employee.insertEmployee", map);
            System.out.println(" staffId is " + staffId);

            //          staffId = (Integer) getSqlMapClientTemplate().queryForObject("employee.lastStaffId", map);
            map.put("staffId", staffId);

            if (staffId != 0) {
//                if (staffUserId != 0) {
//                    int insertRole = (Integer) getSqlMapClientTemplate().update("employee.insertRole", map);
//                }
                int desigDetails = (Integer) getSqlMapClientTemplate().update("employee.insertEmpDesig", map);
                int deptDetails = (Integer) getSqlMapClientTemplate().update("employee.insertEmpDept", map);
                int compDetails = (Integer) getSqlMapClientTemplate().update("employee.insertEmpComp", map);


                FileInputStream uploadFile = null;
                byte[] passportPhotoFile = null;
                if (!"".equals(employeeTO.getPassportPhotoFile())) {
                file= new File(employeeTO.getPassportPhotoFile());
                System.out.println("passportfile"+file);
                uploadFile = new FileInputStream(file);
                System.out.println("uploadFile"+uploadFile);
                passportPhotoFile = new byte[(int) file.length()];
                uploadFile.read(passportPhotoFile);
                uploadFile.close();
                map.put("passportFile", passportPhotoFile);
            } else {
                map.put("passportFile", passportPhotoFile);
            }
                map.put("remarks", employeeTO.getRemarks());
                map.put("passportNo", employeeTO.getPassportNo());
                map.put("passportName", employeeTO.getPassportName());
                map.put("nationality", employeeTO.getNationality());
                map.put("passportType", employeeTO.getPassportType());
                map.put("palceOfIssue", employeeTO.getPalceOfIssue());
                map.put("palceOfBirth", employeeTO.getPalceOfBirth());
                map.put("dateOfIssue", employeeTO.getDateOfIssue());
                map.put("dateOfExpiry", employeeTO.getDateOfExpiry());
                   System.out.println(" saveEmpPassportDetails map:" + map);
            int passport = (Integer) getSqlMapClientTemplate().update("employee.saveEmpPassportDetails", map);

            //visaDetails
                byte[] visaPhoto = null;
                if (!"".equals(employeeTO.getVisaPhoto())) {
                file= new File(employeeTO.getVisaPhoto());
                System.out.println("getVisaPhoto"+file);
                uploadFile = new FileInputStream(file);
                System.out.println("uploadFile"+uploadFile);
                visaPhoto = new byte[(int) file.length()];
                uploadFile.read(visaPhoto);
                uploadFile.close();
                map.put("visaPhoto", visaPhoto);
            } else {
                map.put("visaPhoto", visaPhoto);
            }
                map.put("visaProvider", employeeTO.getVisaProvider());
                map.put("visaType", employeeTO.getVisaType());
                map.put("visaDateOfIssue", employeeTO.getVisaDateOfIssue());
                map.put("visaDateOfValidity", employeeTO.getVisaDateOfValidity());
                map.put("countryEnteringDate", employeeTO.getCountryEnteringDate());
                map.put("visaValidity", employeeTO.getVisaValidity());
                map.put("visaRemarks", employeeTO.getVisaRemarks());
                System.out.println(" saveEmpVisaDetails map:" + map);
                int visa = (Integer) getSqlMapClientTemplate().update("employee.saveEmpVisaDetails", map);
            }
            if (employeeTO.getRelationName() != null) {
                relation = employeeTO.getRelationName();
                if (relation.length != 0) {
                    for (int i = 0; i < employeeTO.getRelationName().length; i++) {
                        map.put("relationName", employeeTO.getRelationName()[i]);
                        if (employeeTO.getRelation()[i] != null) {
                            map.put("relation", employeeTO.getRelation()[i]);
                        } else {
                            map.put("relation", 0);
                        }
                        map.put("relationDob", employeeTO.getRelationDOB()[i]);
                        if (!"".equals(employeeTO.getRelationAge()[i])) {
                            map.put("relationAge", employeeTO.getRelationAge()[i]);
                        } else {
                            map.put("relationAge", 0);
                        }
                        System.out.println("map = " + map);
                        if (employeeTO.getRelationName()[i] != null) {
                            int empRelationDetails = (Integer) getSqlMapClientTemplate().update("employee.insertEmpRelationDetails", map);
                        }
                    }
                }
            }

            //Ledger Code start
            code = (String) getSqlMapClientTemplate().queryForObject("employee.getLedgerCode", map);
            temp = code.split("-");
            int codeval = Integer.parseInt(temp[1]);
            int codev = codeval + 1;
            String ledgercode = "LEDGER-" + codev;
            //System.out.println("ledgercode = " + ledgercode);
            map.put("ledgercode", ledgercode);
            //Ledger Code end

            String nameVal = employeeTO.getName();
            String ledgerName = nameVal + "-" + staffId;
            //System.out.println("ledgerName = " + ledgerName);
            map.put("ledgerName", ledgerName);
            map.put("groupCode", "ACC-0002");

            //current year and month start
            String accYear = (String) getSqlMapClientTemplate().queryForObject("employee.accYearVal", map);
            //System.out.println("accYear:" + accYear);
            map.put("accYear", accYear);
            //current year end

            empLedgerId = (Integer) getSqlMapClientTemplate().insert("employee.insertEmpLedger", map);
            //System.out.println("empLedgerId......." + empLedgerId);
            if (empLedgerId != 0) {
                map.put("ledgerId", empLedgerId);
                //System.out.println("map update ::::=> " + map);
                statusLedger = (Integer) getSqlMapClientTemplate().update("employee.updateEmpMaster", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertEmployeeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertEmployeeDetails", sqlException);
        }
        return staffId;

    }

    public int doUpdateEmpDetails(EmployeeTO empTO, int UserId, int staffUserId) throws FileNotFoundException, IOException {
        Map map = new HashMap();
        FileInputStream fis = null;
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", UserId);
        map.put("staffId", empTO.getStaffId());
        System.out.println("staffId" + empTO.getStaffId());
        map.put("Name", empTO.getName());
        map.put("orgEmpId", empTO.getOrgEmpId());
        map.put("Qualification", empTO.getQualification());
        String[] DOBdate = empTO.getDateOfBirth().split("-");
        String DOB = DOBdate[2] + "-" + DOBdate[1] + "-" + DOBdate[0];
        String[] DOJdate = empTO.getDOJ().split("-");
        String DOJ = DOJdate[2] + "-" + DOJdate[1] + "-" + DOJdate[0];
        map.put("DOB", DOB);
        map.put("DOJ", DOJ);
        map.put("Gender", empTO.getGender());
        map.put("Father_Name", empTO.getFatherName());
        map.put("Blood_Grp", empTO.getBloodGrp());
        map.put("Martial_Status", empTO.getMartialStatus());
        map.put("Mobile", empTO.getMobile());
        map.put("Phone", empTO.getPhone());
        map.put("Email", empTO.getEmail());
        map.put("Addr", empTO.getAddr());
        map.put("City", empTO.getCity());
        map.put("State", empTO.getState());
        map.put("Pincode", empTO.getPincode());
        map.put("Addr1", empTO.getAddr1());
        map.put("City1", empTO.getCity1());
        map.put("State1", empTO.getState1());
        map.put("Pincode1", empTO.getPincode1());
        map.put("subjectId", empTO.getSubjectId());
        map.put("desigId", empTO.getDesigId());
        map.put("gradeId", empTO.getGradeId());
        map.put("status", empTO.getStatus());
        map.put("cmpId", empTO.getCmpId());
        map.put("empUserId", empTO.getEmpUserId());
        map.put("referenceName", empTO.getReferenceName());
        map.put("userMovementType", empTO.getUserMovementType());
        map.put("referenceName", empTO.getReferenceName());
        map.put("terminateFlag", empTO.getTerminateFlag());

        map.put("drivingLicenseNo", empTO.getDrivingLicenseNo());
        map.put("licenseType", empTO.getLicenseType());
        map.put("licenseState", empTO.getLicenseState());
        map.put("licenseExpDate", empTO.getLicenseExpDate());
        map.put("driverType", empTO.getDriverType());
        map.put("salaryType", empTO.getSalaryType());
        map.put("basicSalary", empTO.getBasicSalary());
        map.put("esiElligible", empTO.getEsiEligible());
        map.put("pfElligible", empTO.getPfEligible());
        map.put("vendorCompany", empTO.getVendorCompany());
        map.put("nomineeName", empTO.getNomineeName());
        map.put("nomineeRelation", empTO.getNomineeRelation());
        map.put("nomineeDob", empTO.getNomineeDob());
        map.put("nomineeAge", empTO.getNomineeAge());
        map.put("yearOfExperience", empTO.getYearOfExperience());
        map.put("preEmpDesig", empTO.getPreEmpDesignation());
        map.put("preCompanyName", empTO.getPrevCompanyName());
        map.put("preCompanyAddr", empTO.getPrevCompanyAddress());
        map.put("preCompanyCity", empTO.getPrevCompanyCity());
        map.put("preEmpState", empTO.getPreEmpState());
        map.put("preEmpPinCode", empTO.getPreEmpPincode());
        map.put("preCompanyContact", empTO.getPrevCompanyContact());
        map.put("roleId", empTO.getRoleIds());
        map.put("preCompanyContact", empTO.getPrevCompanyContact());

        String[] relationName = null;
        String[] relation = null;
        String[] relationDOB = null;
        String[] relationAge = null;
        int relationSize = 0;

        String chec = "N";
        if (empTO.getChec() != null && empTO.getChec() != "") {
            if (empTO.getChec().contains("on")) {
                chec = "Y";
            } else {
                chec = "Y";
            }
        }
        byte[] photoFile = null;
        File file = null;
        if (!"".equals(empTO.getPhotoFile())) {
            file = new File(empTO.getPhotoFile());
            fis = new FileInputStream(file);
            photoFile = new byte[(int) file.length()];
            fis.read(photoFile);
            fis.close();
            map.put("photoFile", photoFile);
            int empPhotoUploadFile = (Integer) getSqlMapClientTemplate().update("employee.updateEmpPhoto", map);
        } else {
            map.put("photoFile", photoFile);
        }
        byte[] licenseFile = null;
        if (!"".equals(empTO.getLicenseFile())) {
            file = new File(empTO.getLicenseFile());
            fis = new FileInputStream(file);
            licenseFile = new byte[(int) file.length()];
            fis.read(licenseFile);
            fis.close();
            map.put("licenseFile", licenseFile);
            int licensePhotoUploadFile = (Integer) getSqlMapClientTemplate().update("employee.updateLicensePhoto", map);
        } else {
            map.put("licenseFile", licenseFile);
        }


        map.put("checking", chec);
        map.put("bankAccountNo", empTO.getBankAccountNo());

        map.put("bankName", empTO.getBankName());
        map.put("bankBranchName", empTO.getBankBranchName());
        map.put("accountHolderName", empTO.getAccountHolderName());
        int status = 0;
        System.out.println("map doUpdateEmpDetails = " + map);
        try {
            ////System.out.println("in lect DAO");
            //System.out.println("empTO.getStaffId() " + empTO.getStaffId());

            status = (Integer) getSqlMapClientTemplate().update("employee.updateEmp", map);
            //System.out.println("A status in dao after update Perconal Details: " + status);
            System.out.println("status change staff master  "+status);

//            FileInputStream uploadFile = null;
                byte[] passportPhotoFile = null;
                if (!"".equals(empTO.getPassportPhotoFile())) {
                file= new File(empTO.getPassportPhotoFile());
                System.out.println("passportfile"+file);
                fis = new FileInputStream(file);
                System.out.println("uploadFile"+fis);
                passportPhotoFile = new byte[(int) file.length()];
                fis.read(passportPhotoFile);
                fis.close();
                map.put("passportFile", passportPhotoFile);
                map.put("uploadId", empTO.getUploadId());
                int updateUploadFile = (Integer) getSqlMapClientTemplate().update("employee.updateEmpPassportDetail", map);
                System.out.println("updateUploadFile"+updateUploadFile);
            } else {
                map.put("passportFile", passportPhotoFile);
                }
                map.put("remarks", empTO.getRemarks());
                map.put("passportNo", empTO.getPassportNo());
                map.put("passportName", empTO.getPassportName());
                map.put("nationality", empTO.getNationality());
                map.put("passportType", empTO.getPassportType());
                map.put("palceOfIssue", empTO.getPalceOfIssue());
                map.put("palceOfBirth", empTO.getPalceOfBirth());
                map.put("dateOfIssue", empTO.getDateOfIssue());
                map.put("dateOfExpiry", empTO.getDateOfExpiry());
                System.out.println("mapPassport"+map);
            int passport = (Integer) getSqlMapClientTemplate().update("employee.updateEmpPassportDetails", map);
            System.out.println("passportFileAlter"+passport);
            //visa File Upload
             byte[] visaPhoto = null;
                if (!"".equals(empTO.getVisaPhoto())) {
                file= new File(empTO.getVisaPhoto());
                System.out.println("getVisaPhoto"+file);
                fis = new FileInputStream(file);
                System.out.println("fisVisa"+fis);
                visaPhoto = new byte[(int) file.length()];
                fis.read(visaPhoto);
                fis.close();
                map.put("visaPhoto", visaPhoto);
                map.put("visaId", empTO.getVisaId());
                int updateVisaFile = (Integer) getSqlMapClientTemplate().update("employee.updateEmpVisaDetail", map);
                System.out.println("updateVisaFile"+updateVisaFile);
            } else {
                map.put("visaPhoto", visaPhoto);
            }
                map.put("visaProvider", empTO.getVisaProvider());
                map.put("visaType", empTO.getVisaType());
                map.put("visaDateOfIssue", empTO.getVisaDateOfIssue());
                map.put("visaDateOfValidity", empTO.getVisaDateOfValidity());
                map.put("countryEnteringDate", empTO.getCountryEnteringDate());
                map.put("visaValidity", empTO.getVisaValidity());
                map.put("visaRemarks", empTO.getVisaRemarks());
                int visa = (Integer) getSqlMapClientTemplate().update("employee.updateEmpVisaDetails", map);
                System.out.println("visaFileDao"+visa);
            ArrayList getStatus = new ArrayList();
            int updateStatus = 0;
            int insertStatus = 0;
            int check = 0;
            ////System.out.println("getStatus  change staffDept "+getStatus);
            ////System.out.println("staffId "+empTO.getStaffId());
            ////System.out.println("getDeptId "+empTO.getDeptId());
            map.put("deptId", empTO.getDeptId());
//            map.put("staffId", empTO.getStaffId());
            getStatus = (ArrayList) getSqlMapClientTemplate().queryForList("employee.getEmpDept", map);
            //System.out.println("getStatus  change staffDept " + getStatus.size());
            if (getStatus.size() == 0) {
                updateStatus = (Integer) getSqlMapClientTemplate().update("employee.updateEmpDept", map);
                //System.out.println("update status " + updateStatus);
                insertStatus = (Integer) getSqlMapClientTemplate().update("employee.insertdeptDetails", map);
                //System.out.println("insert Status" + insertStatus);
            }
            ArrayList statusDes = new ArrayList();
            statusDes = (ArrayList) getSqlMapClientTemplate().queryForList("employee.getEmpDesig", map);
            //System.out.println("getStatus  change statusDes " + statusDes);
            if (statusDes.size() == 0) {
                updateStatus = (Integer) getSqlMapClientTemplate().update("employee.updateDesigStaff", map);
                //System.out.println("update status " + updateStatus);
                insertStatus = (Integer) getSqlMapClientTemplate().update("employee.insertdesigDetails", map);
                //System.out.println("insert Status" + insertStatus);
            }

            check = (Integer) getSqlMapClientTemplate().queryForObject("employee.checkEmpComp", map);
            //System.out.println("getStatus  change statusDes " + check);
            if (check == 0) {
                updateStatus = (Integer) getSqlMapClientTemplate().update("employee.updateEmpComp", map);
                //System.out.println("update status " + updateStatus);
                insertStatus = (Integer) getSqlMapClientTemplate().update("employee.insertEmpComp", map);
                System.out.println("insert Status" + insertStatus);
            }
            System.out.println("empTO.getEmpRelationSize() Status" + empTO.getEmpRelationSize());
            int statusBranch = 0;
            if (empTO.getEmpRelationSize() != null) {
                relationSize = Integer.parseInt(empTO.getEmpRelationSize());
                relationName = empTO.getRelationName();

                relation = empTO.getRelation();
                System.out.println("empTO.relation.length() Status" + relation.length);
                if (relation != null && relation.length == relationSize) {
                    //System.out.println("i am in for loop to update the falmily details ");
                    //System.out.println("relationSize" + relationSize);
                    for (int i = 0; i < relationSize; i++) {
                        map.put("empRelationId", empTO.getEmpRelationIds()[i]);
                        map.put("relationName", empTO.getRelationName()[i]);
                        System.out.println("empTO.getRelation()[i]"+empTO.getRelation()[i]);
                        map.put("relation", empTO.getRelation()[i]);
                        map.put("relationDob", empTO.getRelationDOB()[i]);
                        map.put("relationAge", empTO.getRelationAge()[i]);
                        System.out.println("updateEmpRelationDetails map = " + map);
                        int updateEmpRelationDetails = (Integer) getSqlMapClientTemplate().update("employee.updateEmpRelationDetails", map);
                        System.out.println("updateEmpRelationDetails = " + updateEmpRelationDetails);
                    }
                } else if (relation.length != relationSize) {
                    int deleteEmpRelationDetails = (Integer) getSqlMapClientTemplate().update("employee.deleteEmpRelationDetails", map);
                    System.out.println("relation.length = " + relation.length);
                    if (deleteEmpRelationDetails != 0) {
                        if (relation.length != 0) {
                            for (int i = 0; i < relationSize; i++) {
                                map.put("relationName", empTO.getRelationName()[i]);
                                System.out.println("empTO.getRelation()[i]"+empTO.getRelation()[i]);
                                map.put("relation", empTO.getRelation()[i]);
                                map.put("relationDob", empTO.getRelationDOB()[i]);
                                map.put("relationAge", empTO.getRelationAge()[i]);
                                System.out.println("empRelationDetails map = " + map);
                                int empRelationDetails = (Integer) getSqlMapClientTemplate().update("employee.insertEmpRelationDetails", map);
                                System.out.println("empRelationDetails = " + empRelationDetails);
                            }
                        }
                    }
                }
            }

//            map.put("branchId", empTO.getBranchId());
//            //System.out.println("map update ::::=> " + map);
//            statusBranch = (Integer) getSqlMapClientTemplate().update("employee.updateUserBranch", map);
//            //System.out.println("map update ::::=> " + statusBranch);
            /*
             if (empTO.getEmpRelationSize() != null) {
             relationSize = Integer.parseInt(empTO.getEmpRelationSize());
             relationName = empTO.getRelationName();

             relation = empTO.getRelation();
             if (relation != null && relation.length == relationSize) {
             //System.out.println("i am in for loop to update the falmily details ");
             //System.out.println("relationSize" + relationSize);
             for (int i = 0; i < relationSize; i++) {
             map.put("empRelationId", empTO.getEmpRelationIds()[i]);
             map.put("relationName", empTO.getRelationName()[i]);
             map.put("relation", empTO.getRelation()[i]);
             map.put("relationDob", empTO.getRelationDOB()[i]);
             map.put("relationAge", empTO.getRelationAge()[i]);
             //System.out.println("map = " + map);
             int updateEmpRelationDetails = (Integer) getSqlMapClientTemplate().update("employee.updateEmpRelationDetails", map);
             //System.out.println("updateEmpRelationDetails = " + updateEmpRelationDetails);
             }
             } else if (relation.length != relationSize) {
             int deleteEmpRelationDetails = (Integer) getSqlMapClientTemplate().update("employee.deleteEmpRelationDetails", map);
             //System.out.println("deleteEmpRelationDetails = " + deleteEmpRelationDetails);
             if (deleteEmpRelationDetails != 0) {
             if (relation.length != 0) {
             for (int i = 0; i < empTO.getRelation().length; i++) {
             map.put("relationName", empTO.getRelationName()[i]);
             map.put("relation", empTO.getRelation()[i]);
             map.put("relationDob", empTO.getRelationDOB()[i]);
             map.put("relationAge", empTO.getRelationAge()[i]);
             //System.out.println("map = " + map);
             int empRelationDetails = (Integer) getSqlMapClientTemplate().update("employee.insertEmpRelationDetails", map);
             //System.out.println("empRelationDetails = " + empRelationDetails);
             }
             }
             }
             }
             } */
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doUpdateEmpDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "doUpdateEmpDetails", sqlException);
        }
        return status;

    }



    public ArrayList getEmpDetails(String staffId, String staffName) {
        Map map = new HashMap();
        EmployeeTO empTO = new EmployeeTO();
        ArrayList List = new ArrayList();
        map.put("staffId", staffId);
        map.put("staffName", staffName);
        try {
            System.out.println("staffId--" + staffId);
            System.out.println("staffName--" + staffName);
            List = (ArrayList) getSqlMapClientTemplate().queryForList("employee.EmpList", map);
            System.out.println("List.size=" + List.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmpDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getEmpDetails", sqlException);
        }
        return List;
    }

    public String getEmpNameSuggestions(String staffName) {
        Map map = new HashMap();
        String name = staffName + "%";
        map.put("staffName", name);
        String suggestions = "";
        try {
            ArrayList getStaffList = new ArrayList();
            getStaffList = (ArrayList) getSqlMapClientTemplate().queryForList("employee.getEmpName", map);
            Iterator itr = getStaffList.iterator();
            EmployeeTO empTO = null;
            while (itr.hasNext()) {
                empTO = (EmployeeTO) itr.next();
                suggestions = empTO.getName() + "~" + suggestions;
            }
            //////System.out.println("StaffName  " + suggestions);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmpNameSuggestions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getEmpNameSuggestions", sqlException);
        }
        return suggestions;

    }

    public String getEmpCode(String empCode) {
        Map map = new HashMap();
        empCode = empCode + "%";
        map.put("empCode", empCode);
        String suggestions = "";
        //////System.out.println("map = " + map);
        try {
            ArrayList getStaffList = new ArrayList();
            getStaffList = (ArrayList) getSqlMapClientTemplate().queryForList("employee.getEmpCode", map);
            Iterator itr = getStaffList.iterator();
            EmployeeTO empTO = null;
            while (itr.hasNext()) {
                empTO = (EmployeeTO) itr.next();
                suggestions = empTO.getEmpCode() + "~" + suggestions;
            }
            //////System.out.println("StaffName  " + suggestions);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmpCode Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getEmpCode", sqlException);
        }
        return suggestions;

    }

    public ArrayList processEmpFamilyDetails(String staffId, String staffName) {
        Map map = new HashMap();
        ArrayList familyDetails = new ArrayList();
        map.put("staffId", staffId);
        map.put("staffName", staffName);
        try {
            //////System.out.println("map = " + map);
            familyDetails = (ArrayList) getSqlMapClientTemplate().queryForList("employee.getProcessEmpFamilyDetails", map);
            //////System.out.println("familyDetails=" + familyDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processEmpFamilyDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "processEmpFamilyDetails", sqlException);
        }
        return familyDetails;
    }

    /**
     * This method used to Get StaffId.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getStaffId(String staffCode, String staffName) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("staffCode", staffCode);
        map.put("staffName", staffName);
        String staffId = "";
        //////System.out.println("map getStaffId= " + map);
        try {
            staffId = (String) getSqlMapClientTemplate().queryForObject("employee.getStaffId", map);
            //////System.out.println("staffId = " + staffId);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStaffId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getStaffId", sqlException);
        }
        return staffId;
    }

    public ArrayList getEmployeeDetails(EmployeeTO empTO) {
        Map map = new HashMap();
        ArrayList employeeDetails = new ArrayList();
        try {
            map.put("desigId", empTO.getDesigId());
            //////System.out.println("map = " + map);
            employeeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("employee.getEmployeeDetails", map);
            //////System.out.println("getEmployeeDetails=" + employeeDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmployeeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getEmployeeDetails", sqlException);
        }
        return employeeDetails;
    }

    public ArrayList getPrimaryDriverDetails(EmployeeTO empTO) {
        Map map = new HashMap();
        ArrayList employeeDetails = new ArrayList();
        try {
            map.put("desigId", empTO.getDesigId());
            //////System.out.println("map = " + map);
            employeeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("employee.getPrimaryDriverDetails", map);
            //////System.out.println("getEmployeeDetails=" + employeeDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmployeeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getEmployeeDetails", sqlException);
        }
        return employeeDetails;
    }

    public int saveEmployeeStatus(EmployeeTO employeeTO) {
        Map map = new HashMap();
        map.put("staffId", employeeTO.getStaffId());
        map.put("activeInd", employeeTO.getActiveInd());
        map.put("remarks", employeeTO.getRemarks());
        int status = 0;
        //////System.out.println("map in update employee user status = " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("employee.saveEmployeeStatus", map);
            //////System.out.println("map in update employee user status = " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("addNewUser Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "addNewUser", sqlException);
        }
        return status;
    }

    public String checkEmployeeDetails(EmployeeTO empTO) {
        Map map = new HashMap();
        String statuss = "";
        try {
            map.put("employeeName", empTO.getName());
            //////System.out.println("the employefgsdfjbs" + empTO.getName());
            String[] temp = null;
            String date = "";
            String time = "";
            map.put("dateOfBirth", empTO.getDateOfBirth());
            map.put("deptName", empTO.getDeptName());
            map.put("desigName", empTO.getDesigName());
            map.put("companyName", empTO.getCompanyName());
            map.put("gradeName", empTO.getGradeName());
            map.put("roleName", empTO.getRoleName());
            //////System.out.println("map = " + map);
            String status1 = (String) getSqlMapClientTemplate().queryForObject("employee.checkEmployeeDeptName", map);
            map.put("deptId", status1);
            String status2 = (String) getSqlMapClientTemplate().queryForObject("employee.checkEmployeeDesigName", map);
            map.put("desigId", status2);
            String status3 = (String) getSqlMapClientTemplate().queryForObject("employee.checkEmployeeCompanyName", map);
            map.put("compId", status3);
            String status4 = (String) getSqlMapClientTemplate().queryForObject("employee.checkEmployeeGradeName", map);
            map.put("gradeId", status4);
            String status5 = (String) getSqlMapClientTemplate().queryForObject("employee.checkEmployeeRoleName", map);
            map.put("roleId", status5);
            String status = "";
            //////System.out.println("map in the employee controller = " + map);
            status = (String) getSqlMapClientTemplate().queryForObject("employee.checkEmployeeDetails", map);
            if (status == null) {
                statuss = null;
            } else {
                statuss = "yes";
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkEmployeeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkEmployeeDetails", sqlException);
        }
        return statuss;

    }

    public String getDeptId(EmployeeTO empTO) {
        Map map = new HashMap();
        String statuss = "";
        try {

            map.put("deptName", empTO.getDeptName());
            map.put("desigName", empTO.getDesigName());
            map.put("companyName", empTO.getCompanyName());
            map.put("gradeName", empTO.getGradeName());
//                map.put("roleName", empTO.getRoleName());
            //////System.out.println("map = " + map);
            String status1 = (String) getSqlMapClientTemplate().queryForObject("employee.checkEmployeeDeptName", map);
            String status2 = (String) getSqlMapClientTemplate().queryForObject("employee.checkEmployeeDesigName", map);
            String status3 = (String) getSqlMapClientTemplate().queryForObject("employee.checkEmployeeCompanyName", map);
            String status4 = (String) getSqlMapClientTemplate().queryForObject("employee.checkEmployeeGradeName", map);
//                String status5 = (String) getSqlMapClientTemplate().queryForObject("employee.checkEmployeeRoleName", map);
            //////System.out.println("status1 = " + status1);
            //////System.out.println("status2 = " + status2);
            //////System.out.println("status3 = " + status3);
            //////System.out.println("status4 = " + status4);
            statuss = status1 + "-" + status2 + "-" + status3 + "-" + status4;

            //////System.out.println("statuss = " + statuss);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkEmployeeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkEmployeeDetails", sqlException);
        }
        return statuss;

    }

    public String checkDriverStatus(String staffId) {
        Map map = new HashMap();
        String status = "";
        try {
            map.put("staffId", staffId);
            //////System.out.println("map = " + map);
            status = (String) getSqlMapClientTemplate().queryForObject("employee.checkDriverStatus", map);
            //////System.out.println("status status = " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkEmployeeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkEmployeeDetails", sqlException);
        }
        return status;

    }

   




public ArrayList getPassportDetails(String staffId) {
        Map map = new HashMap();
        String status = "";
        ArrayList DeptList = new ArrayList();
        try {
            map.put("staffId", staffId);
            //////System.out.println("map = " + map);
            DeptList = (ArrayList) getSqlMapClientTemplate().queryForList("employee.getPassportDetails", map);
            System.out.println("status status = " + DeptList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkEmployeeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkEmployeeDetails", sqlException);
        }
        return DeptList;

    }
    public ArrayList getVisaDetails(String staffId) {
        Map map = new HashMap();
        String status = "";
        ArrayList DeptList = new ArrayList();
        try {
            map.put("staffId", staffId);
            //////System.out.println("map = " + map);
            DeptList = (ArrayList) getSqlMapClientTemplate().queryForList("employee.getVisaDetails", map);
            System.out.println("status status = " + DeptList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkEmployeeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkEmployeeDetails", sqlException);
        }
        return DeptList;

    }

public ArrayList getDisplayLogoBlobData(String uploadId) {
            Map map = new HashMap();
            ArrayList profileList = new ArrayList();

            try {

                map.put("uploadId", uploadId);
                profileList = (ArrayList) getSqlMapClientTemplate().queryForList("employee.getDisplayLogoBlobData", map);
            } catch (Exception sqlException) {
                FPLogUtils.fpDebugLog("getDisplayLogoBlobData Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-SYS-01", CLASS, "getDisplayLogoBlobData", sqlException);
            }
            return profileList;
    }
    public ArrayList getDisplayLogoBlobDataEmp(String staffId) {
            Map map = new HashMap();
            ArrayList profileList = new ArrayList();

            try {

                map.put("staffId", staffId);
                profileList = (ArrayList) getSqlMapClientTemplate().queryForList("employee.getDisplayLogoBlobDataEmp", map);
            } catch (Exception sqlException) {
                FPLogUtils.fpDebugLog("getDisplayLogoBlobData Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-SYS-01", CLASS, "getDisplayLogoBlobData", sqlException);
            }
            return profileList;
    }
    public ArrayList getDisplayLogoBlobDataLicense(String staffId) {
            Map map = new HashMap();
            ArrayList profileList = new ArrayList();

            try {

                map.put("staffId", staffId);
                profileList = (ArrayList) getSqlMapClientTemplate().queryForList("employee.getDisplayLogoBlobDataLicense", map);
            } catch (Exception sqlException) {
                FPLogUtils.fpDebugLog("getDisplayLogoBlobData Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-SYS-01", CLASS, "getDisplayLogoBlobData", sqlException);
            }
            return profileList;
    }
    public ArrayList getDisplayLogoBlobDataVisa(String visaId) {
            Map map = new HashMap();
            ArrayList profileList = new ArrayList();

            try {

                map.put("visaId", visaId);
                profileList = (ArrayList) getSqlMapClientTemplate().queryForList("employee.getDisplayLogoBlobDataVisa", map);
            } catch (Exception sqlException) {
                FPLogUtils.fpDebugLog("getDisplayLogoBlobData Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-SYS-01", CLASS, "getDisplayLogoBlobData", sqlException);
            }
            return profileList;
    }

}
