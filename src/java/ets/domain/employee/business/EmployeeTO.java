/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.employee.business;

import java.sql.Blob;

/**
 *
 * @author karudaiyar Subramaniam
 */
public class EmployeeTO {

    public EmployeeTO() {
    }
    private byte profileImg[]  ;
    private byte empImg[]  ;
    private byte licenseImg[]  ;
    private byte visaImg[]  ;
    private String companyId = "";
    private String branchId = "";
    private String branchName = "";
    //passport details
    private String userMovementType = "";
    private String passportPhotoFileName = "";
    private String passportPhotoFile = "";
    private String uploadId = "";
    private String passportNo = "";
    private String passportName = "";
    private String passportType = "";
    private String nationality = "";
    private String palceOfIssue = "";
    private String palceOfBirth = "";
    private String dateOfExpiry = "";
    private String dateOfIssue = "";
    //visa deatils
    private String visaId = "";
    private String visaProvider = "";
    private String visaType = "";
    private String countryEnteringDate = "";
    private String visaDateOfValidity = "";
    private String visaDateOfIssue = "";
    private String visaValidity = "";
    private String visaPhoto = "";
    private String visaRemarks = "";
    private String statusId = "";    
    private String hourlyRate = "";
    private String tripCode = "";    
    private String statusName = "";    
    private String mappedVehicle = "";    
    private String RoleIds = "";
    private String remarks = "";
    private String userAccess = "";
    private byte[] driverPhotoFileAsByteArray;
    private byte[] driverLicenseFileAsByteArray;
    private Blob driverLicenseFile;
    private Blob driverPhotoFile;
    private String photoFile = "";
    private String licenseFile = "";
    private String bankName = "";
    private String bankBranchName = "";
    private String accountHolderName = "";
    private String deptCode = "";
    private String empCode = "";
    private String status = "";
    private String fromMonth = "";
    private String endMonth = "";
    private String semName = "";
    private int semId = 0;
    private int totSems = 0;
    private String courseCode = "";
    private String courseName = "";
    private int courseId = 0;
    private String subjectCode = "";
    private String subjectName = "";
    private String subjectType = "";
    private int subjectId = 0;
    private String desigName = "";
    private int batchId = 0;
    private String batchName = "";
    private int userId = 0;
    private int selectedIndex = 0;
    private String staffId = "";
    private String Name = "";
    private String fatherName = "";
    private String Qualification = "";
    private String DOB = "";
    private String DOJ = "";
    private String Gender = "";
    private String bloodGrp = "";
    private String martialStatus = "";
    private String Mobile = "";
    private String Phone = "";
    private String Email = "";
    private String Addr = "";
    private String City = "";
    private String State = "";
    private String Pincode = "";
    private String Addr1 = "";
    private String City1 = "";
    private String State1 = "";
    private String Pincode1 = "";
    private int desigId = 0;
    private int gradeId = 0;
    private String staffDetail = "";
    private String gradeName = "";
    private String userName = "";
    private String password = "";
    private String dateOfBirth = "";
    private String orgEmpId = "";
    private int subTypeId = 0;
    private String subTypeName = "";
    private String subDetail = "";
    private int mandatory = 0;
    private int elective = 0;
    private int practical = 0;
    private int project = 0;
    private int roleId = 0;
    private String roleName = "";
    private String cmpId = "";
    private String empUserId = "";
    
    
    private int deptId = 0;
    private String deptName = "";
    private String description = "";
    private String referenceName = "";
    private String terminateFlag = "";
    
    private String licenseExpDate = "";
    private String drivingLicenseNo = "";
    private String licenseType = "";
    private String licenseState = "";
    private String driverType = "";



    private String salaryType = "";
    private String basicSalary = "0";
    private String esiEligible = "";
    private String pfEligible = "";
    private String nomineeName = "";
    private String nomineeRelation = "";
    private String nomineeAge = "0";


    private String yearOfExperience = "0";
    private String preEmpDesignation = "";
    private String prevCompanyName = "";
    private String prevCompanyAddress = "";
    private String prevCompanyCity = "";
    private String preEmpState = "";
    private String preEmpPincode = "";
    private String prevCompanyContact = "";

     private String nomineeDob = "";
    private String vendorCompany = "";
    private String vendorId = "";
    private String vendorName = "";
    private String[] relationName = null;
    private String[] relation = null;
    private String[] relationDOB = null;
    private String[] relationAge = null;
    private String prevEmpDesignation = "";
    private String prevEmpState = "";
    private String prevEmployeePincode = "";
    private String licenseDate = "";
    private String staffUserId = "";
    private String activeInd = "";
    private String relationId = "";
    private String relationNames = "";
    private String relationDobs = "";
    private String relationAges = "";
    private String chec = "";
    private String empRelationId = "";
    private String[] empRelationIds = null;
    private String empRelationSize = "";
    private String vehicleType = "";
    private String vehicleCategory = "";
    private String tripType = "";
    private String salaryStructureId = "";
    private String fromKm = "";
    private String toKm = "";
    private String kmAmount = "";
    private String fromHours = "";
    private String toHours = "";
    private String hoursAmount = "";
    private String perDayAmount = "";
    private String extraAmountPerKm = "";
    private String extraAmountPerHour = "";
    private String companyName = "";
    private String relationType = "";


    private String contractDriver = "";
    private String bankAccountNo = "";


    public String getReferenceName() {
        return referenceName;
    }

    public void setReferenceName(String referenceName) {
        this.referenceName = referenceName;
    }

    public String getTerminateFlag() {
        return terminateFlag;
    }

    public void setTerminateFlag(String terminateFlag) {
        this.terminateFlag = terminateFlag;
    }

    

    public String getAddr() {
        return Addr;
    }

    public void setAddr(String Addr) {
        this.Addr = Addr;
    }

    public String getAddr1() {
        return Addr1;
    }

    public void setAddr1(String Addr1) {
        this.Addr1 = Addr1;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String City) {
        this.City = City;
    }

    public String getCity1() {
        return City1;
    }

    public void setCity1(String City1) {
        this.City1 = City1;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getDOJ() {
        return DOJ;
    }

    public void setDOJ(String DOJ) {
        this.DOJ = DOJ;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String Mobile) {
        this.Mobile = Mobile;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    public String getPincode() {
        return Pincode;
    }

    public void setPincode(String Pincode) {
        this.Pincode = Pincode;
    }

    public String getPincode1() {
        return Pincode1;
    }

    public void setPincode1(String Pincode1) {
        this.Pincode1 = Pincode1;
    }

    public String getQualification() {
        return Qualification;
    }

    public void setQualification(String Qualification) {
        this.Qualification = Qualification;
    }

    public String getState() {
        return State;
    }

    public void setState(String State) {
        this.State = State;
    }

    public String getState1() {
        return State1;
    }

    public void setState1(String State1) {
        this.State1 = State1;
    }

    public int getBatchId() {
        return batchId;
    }

    public void setBatchId(int batchId) {
        this.batchId = batchId;
    }

    public String getBatchName() {
        return batchName;
    }

    public void setBatchName(String batchName) {
        this.batchName = batchName;
    }

    public String getBloodGrp() {
        return bloodGrp;
    }

    public void setBloodGrp(String bloodGrp) {
        this.bloodGrp = bloodGrp;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public int getDeptId() {
        return deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDesigId() {
        return desigId;
    }

    public void setDesigId(int desigId) {
        this.desigId = desigId;
    }

    public String getDesigName() {
        return desigName;
    }

    public void setDesigName(String desigName) {
        this.desigName = desigName;
    }

    public int getElective() {
        return elective;
    }

    public void setElective(int elective) {
        this.elective = elective;
    }

    public String getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(String endMonth) {
        this.endMonth = endMonth;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getFromMonth() {
        return fromMonth;
    }

    public void setFromMonth(String fromMonth) {
        this.fromMonth = fromMonth;
    }

    public int getGradeId() {
        return gradeId;
    }

    public void setGradeId(int gradeId) {
        this.gradeId = gradeId;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public int getMandatory() {
        return mandatory;
    }

    public void setMandatory(int mandatory) {
        this.mandatory = mandatory;
    }

    public String getMartialStatus() {
        return martialStatus;
    }

    public void setMartialStatus(String martialStatus) {
        this.martialStatus = martialStatus;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPractical() {
        return practical;
    }

    public void setPractical(int practical) {
        this.practical = practical;
    }

    public int getProject() {
        return project;
    }

    public void setProject(int project) {
        this.project = project;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public int getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(int selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public int getSemId() {
        return semId;
    }

    public void setSemId(int semId) {
        this.semId = semId;
    }

    public String getSemName() {
        return semName;
    }

    public void setSemName(String semName) {
        this.semName = semName;
    }

    public String getStaffDetail() {
        return staffDetail;
    }

    public void setStaffDetail(String staffDetail) {
        this.staffDetail = staffDetail;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubDetail() {
        return subDetail;
    }

    public void setSubDetail(String subDetail) {
        this.subDetail = subDetail;
    }

    public int getSubTypeId() {
        return subTypeId;
    }

    public void setSubTypeId(int subTypeId) {
        this.subTypeId = subTypeId;
    }

    public String getSubTypeName() {
        return subTypeName;
    }

    public void setSubTypeName(String subTypeName) {
        this.subTypeName = subTypeName;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(String subjectType) {
        this.subjectType = subjectType;
    }

    public int getTotSems() {
        return totSems;
    }

    public void setTotSems(int totSems) {
        this.totSems = totSems;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCmpId() {
        return cmpId;
    }

    public void setCmpId(String cmpId) {
        this.cmpId = cmpId;
    }

    public String getEmpUserId() {
        return empUserId;
    }

    public void setEmpUserId(String empUserId) {
        this.empUserId = empUserId;
    }

  
    public String getDriverType() {
        return driverType;
    }

    public void setDriverType(String driverType) {
        this.driverType = driverType;
    }

    public String getLicenseExpDate() {
        return licenseExpDate;
    }

    public void setLicenseExpDate(String licenseExpDate) {
        this.licenseExpDate = licenseExpDate;
    }
        
    public String getDrivingLicenseNo() {
        return drivingLicenseNo;
    }

    public void setDrivingLicenseNo(String drivingLicenseNo) {
        this.drivingLicenseNo = drivingLicenseNo;
    }

    public String getEsiEligible() {
        return esiEligible;
    }

    public void setEsiEligible(String esiEligible) {
        this.esiEligible = esiEligible;
    }

    public String getLicenseState() {
        return licenseState;
    }

    public void setLicenseState(String licenseState) {
        this.licenseState = licenseState;
    }

    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType;
    }

    public String getNomineeAge() {
        return nomineeAge;
    }

    public void setNomineeAge(String nomineeAge) {
        this.nomineeAge = nomineeAge;
    }


    public String getNomineeName() {
        return nomineeName;
    }

    public void setNomineeName(String nomineeName) {
        this.nomineeName = nomineeName;
    }

    public String getNomineeRelation() {
        return nomineeRelation;
    }

    public void setNomineeRelation(String nomineeRelation) {
        this.nomineeRelation = nomineeRelation;
    }

    public String getPfEligible() {
        return pfEligible;
    }

    public void setPfEligible(String pfEligible) {
        this.pfEligible = pfEligible;
    }

    public String getPreEmpDesignation() {
        return preEmpDesignation;
    }

    public void setPreEmpDesignation(String preEmpDesignation) {
        this.preEmpDesignation = preEmpDesignation;
    }

    public String getPreEmpPincode() {
        return preEmpPincode;
    }

    public void setPreEmpPincode(String preEmpPincode) {
        this.preEmpPincode = preEmpPincode;
    }

    public String getPreEmpState() {
        return preEmpState;
    }

    public void setPreEmpState(String preEmpState) {
        this.preEmpState = preEmpState;
    }

    public String getPrevCompanyAddress() {
        return prevCompanyAddress;
    }

    public void setPrevCompanyAddress(String prevCompanyAddress) {
        this.prevCompanyAddress = prevCompanyAddress;
    }

    public String getPrevCompanyCity() {
        return prevCompanyCity;
    }

    public void setPrevCompanyCity(String prevCompanyCity) {
        this.prevCompanyCity = prevCompanyCity;
    }

    public String getPrevCompanyContact() {
        return prevCompanyContact;
    }

    public void setPrevCompanyContact(String prevCompanyContact) {
        this.prevCompanyContact = prevCompanyContact;
    }

    public String getPrevCompanyName() {
        return prevCompanyName;
    }

    public void setPrevCompanyName(String prevCompanyName) {
        this.prevCompanyName = prevCompanyName;
    }

    public String getSalaryType() {
        return salaryType;
    }

    public void setSalaryType(String salaryType) {
        this.salaryType = salaryType;
    }

    public String getBasicSalary() {
        return basicSalary;
    }

    public void setBasicSalary(String basicSalary) {
        this.basicSalary = basicSalary;
    }

    public String getYearOfExperience() {
        return yearOfExperience;
    }

    public void setYearOfExperience(String yearOfExperience) {
        this.yearOfExperience = yearOfExperience;
    }



    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getChec() {
        return chec;
    }

    public void setChec(String chec) {
        this.chec = chec;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getEmpRelationId() {
        return empRelationId;
    }

    public void setEmpRelationId(String empRelationId) {
        this.empRelationId = empRelationId;
    }

    public String[] getEmpRelationIds() {
        return empRelationIds;
    }

    public void setEmpRelationIds(String[] empRelationIds) {
        this.empRelationIds = empRelationIds;
    }

    public String getEmpRelationSize() {
        return empRelationSize;
    }

    public void setEmpRelationSize(String empRelationSize) {
        this.empRelationSize = empRelationSize;
    }

    public String getExtraAmountPerHour() {
        return extraAmountPerHour;
    }

    public void setExtraAmountPerHour(String extraAmountPerHour) {
        this.extraAmountPerHour = extraAmountPerHour;
    }

    public String getExtraAmountPerKm() {
        return extraAmountPerKm;
    }

    public void setExtraAmountPerKm(String extraAmountPerKm) {
        this.extraAmountPerKm = extraAmountPerKm;
    }

    public String getFromHours() {
        return fromHours;
    }

    public void setFromHours(String fromHours) {
        this.fromHours = fromHours;
    }

    public String getFromKm() {
        return fromKm;
    }

    public void setFromKm(String fromKm) {
        this.fromKm = fromKm;
    }

    public String getHoursAmount() {
        return hoursAmount;
    }

    public void setHoursAmount(String hoursAmount) {
        this.hoursAmount = hoursAmount;
    }

    public String getKmAmount() {
        return kmAmount;
    }

    public void setKmAmount(String kmAmount) {
        this.kmAmount = kmAmount;
    }

    public String getLicenseDate() {
        return licenseDate;
    }

    public void setLicenseDate(String licenseDate) {
        this.licenseDate = licenseDate;
    }

    public String getNomineeDob() {
        return nomineeDob;
    }

    public void setNomineeDob(String nomineeDob) {
        this.nomineeDob = nomineeDob;
    }

    public String getPerDayAmount() {
        return perDayAmount;
    }

    public void setPerDayAmount(String perDayAmount) {
        this.perDayAmount = perDayAmount;
    }

    public String getPrevEmpDesignation() {
        return prevEmpDesignation;
    }

    public void setPrevEmpDesignation(String prevEmpDesignation) {
        this.prevEmpDesignation = prevEmpDesignation;
    }

    public String getPrevEmpState() {
        return prevEmpState;
    }

    public void setPrevEmpState(String prevEmpState) {
        this.prevEmpState = prevEmpState;
    }

    public String getPrevEmployeePincode() {
        return prevEmployeePincode;
    }

    public void setPrevEmployeePincode(String prevEmployeePincode) {
        this.prevEmployeePincode = prevEmployeePincode;
    }

    public String[] getRelation() {
        return relation;
    }

    public void setRelation(String[] relation) {
        this.relation = relation;
    }

    public String[] getRelationAge() {
        return relationAge;
    }

    public void setRelationAge(String[] relationAge) {
        this.relationAge = relationAge;
    }

    public String getRelationAges() {
        return relationAges;
    }

    public void setRelationAges(String relationAges) {
        this.relationAges = relationAges;
    }

    public String[] getRelationDOB() {
        return relationDOB;
    }

    public void setRelationDOB(String[] relationDOB) {
        this.relationDOB = relationDOB;
    }

    public String getRelationDobs() {
        return relationDobs;
    }

    public void setRelationDobs(String relationDobs) {
        this.relationDobs = relationDobs;
    }

    public String getRelationId() {
        return relationId;
    }

    public void setRelationId(String relationId) {
        this.relationId = relationId;
    }

    public String[] getRelationName() {
        return relationName;
    }

    public void setRelationName(String[] relationName) {
        this.relationName = relationName;
    }

    public String getRelationNames() {
        return relationNames;
    }

    public void setRelationNames(String relationNames) {
        this.relationNames = relationNames;
    }

    public String getRelationType() {
        return relationType;
    }

    public void setRelationType(String relationType) {
        this.relationType = relationType;
    }

    public String getSalaryStructureId() {
        return salaryStructureId;
    }

    public void setSalaryStructureId(String salaryStructureId) {
        this.salaryStructureId = salaryStructureId;
    }

    public String getStaffUserId() {
        return staffUserId;
    }

    public void setStaffUserId(String staffUserId) {
        this.staffUserId = staffUserId;
    }

    public String getToHours() {
        return toHours;
    }

    public void setToHours(String toHours) {
        this.toHours = toHours;
    }

    public String getToKm() {
        return toKm;
    }

    public void setToKm(String toKm) {
        this.toKm = toKm;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getVehicleCategory() {
        return vehicleCategory;
    }

    public void setVehicleCategory(String vehicleCategory) {
        this.vehicleCategory = vehicleCategory;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVendorCompany() {
        return vendorCompany;
    }

    public void setVendorCompany(String vendorCompany) {
        this.vendorCompany = vendorCompany;
    }

    public String getContractDriver() {
        return contractDriver;
    }

    public void setContractDriver(String contractDriver) {
        this.contractDriver = contractDriver;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getBankAccountNo() {
        return bankAccountNo;
    }

    public void setBankAccountNo(String bankAccountNo) {
        this.bankAccountNo = bankAccountNo;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    public String getBankBranchName() {
        return bankBranchName;
    }

    public void setBankBranchName(String bankBranchName) {
        this.bankBranchName = bankBranchName;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Blob getDriverLicenseFile() {
        return driverLicenseFile;
    }

    public void setDriverLicenseFile(Blob driverLicenseFile) {
        this.driverLicenseFile = driverLicenseFile;
    }

    public byte[] getDriverLicenseFileAsByteArray() {
        return driverLicenseFileAsByteArray;
    }

    public void setDriverLicenseFileAsByteArray(byte[] driverLicenseFileAsByteArray) {
        this.driverLicenseFileAsByteArray = driverLicenseFileAsByteArray;
    }

    public Blob getDriverPhotoFile() {
        return driverPhotoFile;
    }

    public void setDriverPhotoFile(Blob driverPhotoFile) {
        this.driverPhotoFile = driverPhotoFile;
    }

    public byte[] getDriverPhotoFileAsByteArray() {
        return driverPhotoFileAsByteArray;
    }

    public void setDriverPhotoFileAsByteArray(byte[] driverPhotoFileAsByteArray) {
        this.driverPhotoFileAsByteArray = driverPhotoFileAsByteArray;
    }

    public String getLicenseFile() {
        return licenseFile;
    }

    public void setLicenseFile(String licenseFile) {
        this.licenseFile = licenseFile;
    }

    public String getPhotoFile() {
        return photoFile;
    }

    public void setPhotoFile(String photoFile) {
        this.photoFile = photoFile;
    }

    public String getRoleIds() {
        return RoleIds;
    }

    public void setRoleIds(String RoleIds) {
        this.RoleIds = RoleIds;
    }

    public String getUserAccess() {
        return userAccess;
    }

    public void setUserAccess(String userAccess) {
        this.userAccess = userAccess;
    }

    public String getMappedVehicle() {
        return mappedVehicle;
    }

    public void setMappedVehicle(String mappedVehicle) {
        this.mappedVehicle = mappedVehicle;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getTripCode() {
        return tripCode;
    }

    public void setTripCode(String tripCode) {
        this.tripCode = tripCode;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(String hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCountryEnteringDate() {
        return countryEnteringDate;
    }

    public void setCountryEnteringDate(String countryEnteringDate) {
        this.countryEnteringDate = countryEnteringDate;
    }

    public String getDateOfExpiry() {
        return dateOfExpiry;
    }

    public void setDateOfExpiry(String dateOfExpiry) {
        this.dateOfExpiry = dateOfExpiry;
    }

    public String getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDateOfIssue(String dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }

    public byte[] getEmpImg() {
        return empImg;
    }

    public void setEmpImg(byte[] empImg) {
        this.empImg = empImg;
    }

    public byte[] getLicenseImg() {
        return licenseImg;
    }

    public void setLicenseImg(byte[] licenseImg) {
        this.licenseImg = licenseImg;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getPalceOfBirth() {
        return palceOfBirth;
    }

    public void setPalceOfBirth(String palceOfBirth) {
        this.palceOfBirth = palceOfBirth;
    }

    public String getPalceOfIssue() {
        return palceOfIssue;
    }

    public void setPalceOfIssue(String palceOfIssue) {
        this.palceOfIssue = palceOfIssue;
    }

    public String getPassportName() {
        return passportName;
    }

    public void setPassportName(String passportName) {
        this.passportName = passportName;
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    public String getPassportPhotoFile() {
        return passportPhotoFile;
    }

    public void setPassportPhotoFile(String passportPhotoFile) {
        this.passportPhotoFile = passportPhotoFile;
    }

    public String getPassportPhotoFileName() {
        return passportPhotoFileName;
    }

    public void setPassportPhotoFileName(String passportPhotoFileName) {
        this.passportPhotoFileName = passportPhotoFileName;
    }

    public String getPassportType() {
        return passportType;
    }

    public void setPassportType(String passportType) {
        this.passportType = passportType;
    }

    public byte[] getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(byte[] profileImg) {
        this.profileImg = profileImg;
    }

    public String getUploadId() {
        return uploadId;
    }

    public void setUploadId(String uploadId) {
        this.uploadId = uploadId;
    }

    public String getVisaDateOfIssue() {
        return visaDateOfIssue;
    }

    public void setVisaDateOfIssue(String visaDateOfIssue) {
        this.visaDateOfIssue = visaDateOfIssue;
    }

    public String getVisaDateOfValidity() {
        return visaDateOfValidity;
    }

    public void setVisaDateOfValidity(String visaDateOfValidity) {
        this.visaDateOfValidity = visaDateOfValidity;
    }

    public String getVisaId() {
        return visaId;
    }

    public void setVisaId(String visaId) {
        this.visaId = visaId;
    }

    public byte[] getVisaImg() {
        return visaImg;
    }

    public void setVisaImg(byte[] visaImg) {
        this.visaImg = visaImg;
    }

    public String getVisaPhoto() {
        return visaPhoto;
    }

    public void setVisaPhoto(String visaPhoto) {
        this.visaPhoto = visaPhoto;
    }

    public String getVisaProvider() {
        return visaProvider;
    }

    public void setVisaProvider(String visaProvider) {
        this.visaProvider = visaProvider;
    }

    public String getVisaRemarks() {
        return visaRemarks;
    }

    public void setVisaRemarks(String visaRemarks) {
        this.visaRemarks = visaRemarks;
    }

    public String getVisaType() {
        return visaType;
    }

    public void setVisaType(String visaType) {
        this.visaType = visaType;
    }

    public String getVisaValidity() {
        return visaValidity;
    }

    public void setVisaValidity(String visaValidity) {
        this.visaValidity = visaValidity;
    }

    public String getOrgEmpId() {
        return orgEmpId;
    }

    public void setOrgEmpId(String orgEmpId) {
        this.orgEmpId = orgEmpId;
    }

    public String getUserMovementType() {
        return userMovementType;
    }

    public void setUserMovementType(String userMovementType) {
        this.userMovementType = userMovementType;
    }

    
}
