/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ets.domain.employee.business;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.employee.data.EmployeeDAO;
import java.util.ArrayList;

/**
 *
 * @author karudaiyar Subramaniam
 */
public class EmployeeBP {
    private EmployeeDAO employeeDAO;

    public EmployeeDAO getEmployeeDAO() {
        return employeeDAO;
    }

    public void setEmployeeDAO(EmployeeDAO employeeDAO) {
        this.employeeDAO = employeeDAO;
    }

    public ArrayList getDeptList() throws FPRuntimeException, FPBusinessException {
        ArrayList DeptList = new ArrayList();
        DeptList = employeeDAO.getDeptList();
//        if (DeptList.size() == 0) {
//            throw new FPBusinessException("EM-MD-01");
//        }
        return DeptList;
    }
    public ArrayList getDriverVendorList() throws FPRuntimeException, FPBusinessException {
        ArrayList vendorList = new ArrayList();
        vendorList = employeeDAO.getDriverVendorList();
        return vendorList;
    }

     public int addNewUser(EmployeeTO employeeTO, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = getEmployeeDAO().addNewUser(employeeTO, UserId);
        return insertStatus;
    }
     public int updateUser(EmployeeTO employeeTO, int UserId, int staffUserId , int roleId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = getEmployeeDAO().updateUser(employeeTO, UserId, staffUserId , roleId);
        return insertStatus;
    }

      public int insertEmployeeDetails(EmployeeTO employeeTO, int UserId, int staffUserId ) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = getEmployeeDAO().insertEmployeeDetails(employeeTO, UserId, staffUserId  );
        return insertStatus;
    }
    public int processUpdateEmpDetails(EmployeeTO empTO, int UserId , int staffUserId) throws Exception {
        int status = 0;
        status = employeeDAO.doUpdateEmpDetails(empTO, UserId, staffUserId);
        return status;

    }
    public ArrayList processEmpDetails(String staffId, String staffName) throws FPRuntimeException, FPBusinessException {
        ArrayList List = new ArrayList();
        List = employeeDAO.getEmpDetails(staffId, staffName);
        return List;
    }

    public String processEmpNameSuggestions(String staffName) {
        String suggestion = "";
        suggestion = employeeDAO.getEmpNameSuggestions(staffName);
        return suggestion;
    }

    public String processEmpCode(String empCode) {
        String suggestion = "";
        suggestion = employeeDAO.getEmpCode(empCode);
        return suggestion;
    }

public ArrayList processEmpFamilyDetails(String staffId, String staffName) throws FPRuntimeException, FPBusinessException {
        ArrayList fsmilyDetails = new ArrayList();
        fsmilyDetails = employeeDAO.processEmpFamilyDetails(staffId, staffName);
        return fsmilyDetails;
    }

/**
     * This method used to Get Staff Id.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
      public String getStaffId(String staffCode,String staffName) throws FPRuntimeException, FPBusinessException {
        String staffId = "";
            staffId = employeeDAO.getStaffId(staffCode,staffName);
        return staffId;

    }
  
public int saveEmployeeStatus(EmployeeTO employeeTO) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = employeeDAO.saveEmployeeStatus(employeeTO);
        return insertStatus;
    }
  public ArrayList getEmployeeDetails(EmployeeTO empTO) throws FPRuntimeException, FPBusinessException {
        ArrayList employeeDetails = new ArrayList();
        employeeDetails = employeeDAO.getEmployeeDetails(empTO);
        return employeeDetails;
    }
  public ArrayList getPrimaryDriverDetails(EmployeeTO empTO) throws FPRuntimeException, FPBusinessException {
        ArrayList employeeDetails = new ArrayList();
        employeeDetails = employeeDAO.getPrimaryDriverDetails(empTO);
        return employeeDetails;
    }
 public String checkEmployeeDetails(EmployeeTO empTO) {
        String status = "";
        status = employeeDAO.checkEmployeeDetails(empTO);
        return status;
    }
  public String getDeptId(EmployeeTO empTO) {
        String status = "";
        status = employeeDAO.getDeptId(empTO);
        return status;
    }
  public String checkDriverStatus(String staffId ) {
        String status = "";
        status = employeeDAO.checkDriverStatus(staffId);
        return status;
    }

  public ArrayList getPassportDetails(String staffId) throws FPRuntimeException, FPBusinessException {
        ArrayList employeeDetails = new ArrayList();
        employeeDetails = employeeDAO.getPassportDetails(staffId);
        return employeeDetails;
    }
  public ArrayList getVisaDetails(String staffId) throws FPRuntimeException, FPBusinessException {
        ArrayList employeeDetails = new ArrayList();
        employeeDetails = employeeDAO.getVisaDetails(staffId);
        return employeeDetails;
    }
public ArrayList  getDisplayLogoBlobData(String uploadId) {
        ArrayList profileList=new ArrayList();
        profileList = employeeDAO.getDisplayLogoBlobData(uploadId);
        return profileList;
    }
  public ArrayList  getDisplayLogoBlobDataEmp(String staffId) {
        ArrayList profileList=new ArrayList();
        profileList = employeeDAO.getDisplayLogoBlobDataEmp(staffId);
        return profileList;
    }
  public ArrayList  getDisplayLogoBlobDataLicense(String staffId) {
        ArrayList profileList=new ArrayList();
        profileList = employeeDAO.getDisplayLogoBlobDataLicense(staffId);
        return profileList;
    }
  public ArrayList  getDisplayLogoBlobDataVisa(String visaId) {
        ArrayList profileList=new ArrayList();
        profileList = employeeDAO.getDisplayLogoBlobDataVisa(visaId);
        return profileList;
    }
}
