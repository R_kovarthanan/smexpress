package ets.domain.vendor.business;

import com.ibatis.sqlmap.client.SqlMapClient;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.vendor.data.VendorDAO;
import ets.domain.operation.business.OperationTO;

import java.util.ArrayList;
import java.util.Iterator;

public class VendorBP {

    public VendorBP() {
    }
    VendorDAO vendorDAO;

    public VendorDAO getVendorDAO() {
        return vendorDAO;
    }

    public void setVendorDAO(VendorDAO vendorDAO) {
        this.vendorDAO = vendorDAO;
    }

    /**
     * This method used to Get Vendor Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises ..
     */
    public ArrayList processGetVendorList() throws FPRuntimeException, FPBusinessException {

        ArrayList vendorList = new ArrayList();
        vendorList = vendorDAO.getVendorList();
        if (vendorList.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        }
        return vendorList;
    }

    // processGetVendorTypeList
    /**
     * This method used to Get Vendor Type Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processGetVendorTypeList() {

        ArrayList vendorTypeList = new ArrayList();
        vendorTypeList = vendorDAO.getVendorTypeList();
        return vendorTypeList;
    }

    /**
     * This method used to Insert Vendor Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises ===
     */
    public int processInsertVendorDetails(VendorTO vendorTO, String index, int UserId, String[] actualFilePath, String tripSheetId1, String[] fileSaved) throws FPRuntimeException, FPBusinessException {
        //    public int processInsertVendorDetails(VendorTO vendorTO,String[] mfrids,String index,int UserId) throws FPRuntimeException, FPBusinessException {
        int vendorId = 0;
        int status = 0;
        int mfrId = 0;
        vendorId = vendorDAO.doVendorDetails(vendorTO, UserId, actualFilePath, tripSheetId1, fileSaved);

        int temp = 0;

        if (index != null) {

            //        for(int i=0;i<index.length;i++){
            //        temp=Integer.parseInt(index[i]);
            //        mfrId=Integer.parseInt(mfrids[temp]);
            //       // status = vendorDAO.insertVendorMfrDetails(vendorId,mfrId,UserId);
            //        }
        }
        return vendorId;
    }

    /**
     * This method used to get Designation Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processVendorGetsDetail(int vendorId) throws FPRuntimeException, FPBusinessException {
        ArrayList vendorDetail = new ArrayList();
        vendorDetail = (ArrayList) vendorDAO.getAlterVendorDetail(vendorId);
        return vendorDetail;
    }

    /**
     * This method used to Update vendor Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int processUpdateVendorDetails(VendorTO vendorTO, String[] mfrids, String[] index, int UserId) throws FPRuntimeException, FPBusinessException {
        int mfrId = 0;
        int status = 0;
        int status1 = 0;
        int vendorId = 0;
        status = vendorDAO.doVendorUpdateDetails(vendorTO, UserId);
        //inner updation used to update mfr_config
        status1 = vendorDAO.updateVendorMfrDetails(vendorTO, mfrids, index, UserId);
        return status;
    }

    //getAjaxModelList
    /**
     * This method used to Get parts throw ajax Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String processGetVendor(int vendorTypeId) throws FPRuntimeException, FPBusinessException {
        ArrayList models = new ArrayList();
        int counter = 0;
        VendorTO vendor = null;
        models = vendorDAO.getAjaxVendorList(vendorTypeId);

        Iterator itr;
        String model = "";
        if (models.size() == 0) {
            model = "";
        } else {
            itr = models.iterator();
            while (itr.hasNext()) {
                vendor = new VendorTO();
                vendor = (VendorTO) itr.next();
                if (counter == 0) {
                    model = vendor.getVendorId() + "-" + vendor.getVendorName();
                    counter++;
                } else {
                    model = model + "~" + vendor.getVendorId() + "-" + vendor.getVendorName();
                }
            }
        }
        return model;
    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processGetItemList(int vendorId) throws FPRuntimeException, FPBusinessException {

        ArrayList itemList = new ArrayList();
        itemList = vendorDAO.getItemList(vendorId);

        return itemList;
    }

    //processGetAssignedList
    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processGetAssignedList(int vendorId) throws FPRuntimeException, FPBusinessException {

        ArrayList assignedList = new ArrayList();
        assignedList = vendorDAO.getAssignedList(vendorId);

        return assignedList;
    }

    //makeAllNoToActiveInd
    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int makeAllNoToActiveInd(int vendorId) throws FPRuntimeException, FPBusinessException {

        int status = 0;
        status = vendorDAO.doAllNoToActiveInd(vendorId);
        return status;
    }
    //resultAssignedList

    /**
     * This method used to Update vendor Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int resultAssignedList(String[] ItemIds, int vendorId, int userId) throws FPRuntimeException, FPBusinessException {

        int count = 0;

        if (ItemIds != null) {
            for (int i = 0; i < ItemIds.length; i++) {
                int itemId = Integer.parseInt(ItemIds[i]);
                System.out.println("itemId=" + ItemIds[i]);
                count = vendorDAO.getCount(itemId, vendorId, userId);
            }
        }

        return count;
    }

    public int processVendorCatConfig(String[] categoryIds, int vendorId, int userId) throws FPRuntimeException, FPBusinessException {

        int count = 0;
        if (categoryIds != null) {
            for (int i = 0; i < categoryIds.length; i++) {
                int itemId = Integer.parseInt(categoryIds[i]);
                System.out.println("categoryIds=" + categoryIds[i]);
                count = vendorDAO.doVendorCategoryConfig(itemId, vendorId, userId);
            }
        }

        return count;
    }

    public int processVendorCatInactive(int vendorId) throws FPRuntimeException, FPBusinessException {

        int status = 0;
        status = vendorDAO.doVendorCatInactive(vendorId);
        return status;
    }

    public ArrayList processGetCatAssignedList(int vendorId) throws FPRuntimeException, FPBusinessException {

        ArrayList assignedList = new ArrayList();
        assignedList = vendorDAO.getCategoryAssignedList(vendorId);
        return assignedList;
    }

    public ArrayList processGetNotAssisgnedList(int vendorId) throws FPRuntimeException, FPBusinessException {

        ArrayList itemList = new ArrayList();
        itemList = vendorDAO.getCatNotAssignedList(vendorId);

        return itemList;
    }

    public ArrayList processGetVendorLists(VendorTO vendorTO) {
        ArrayList vendorList = new ArrayList();
        vendorList = vendorDAO.getVendorLists(vendorTO);
        if (vendorList.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        }
        return vendorList;
    }
    //Hari

    public ArrayList processGetRcVendorList() {
        ArrayList rcVendorList = new ArrayList();
        rcVendorList = vendorDAO.getRcVendorList();
        if (rcVendorList.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        }
        return rcVendorList;
    }

    public int processInsertVehicleVendorContract(VendorTO vendorTO, int UserId) throws FPRuntimeException, FPBusinessException {
        int contractId = 0;
        int status = 0;
        SqlMapClient session = vendorDAO.getSqlMapClient();
        try {
            session.startTransaction();
            String vendorName = vendorTO.getVendorName();
            String contractTypeId = vendorTO.getContractTypeId();
            contractId = vendorDAO.processInsertVehicleVendor(vendorTO, UserId, session);
            String[] vehicleTypeIdDedicate = vendorTO.getVehicleTypeIdDedicates();
            String[] vehicleUnitsDedicate = vendorTO.getVehicleUnitsDedicates();
            String[] contractCategory = vendorTO.getContractCategorys();
            String[] fixedCost = vendorTO.getFixedCosts();
            String[] fixedHrs = vendorTO.getFixedHrss();
            String[] fixedMin = vendorTO.getFixedMins();
            String[] totalFixedCost = vendorTO.getTotalFixedCosts();
            String[] rateCost = vendorTO.getRateCosts();
            String[] rateLimit = vendorTO.getRateLimits();
            String[] maxAllowableKM = vendorTO.getMaxAllowableKMs();
            String[] workingDays = vendorTO.getWorkingDayss();
            String[] holidays = vendorTO.getHolidayss();
            String[] addCostDedicate = vendorTO.getAddCostDedicates();
            String[] iCnt = vendorTO.getiCnt();
            String[] iCnt1 = vendorTO.getiCnt1();
            String[] vehicleTypeId = vendorTO.getVehicleTypeIds();
            String[] vehicleUnits = vendorTO.getVehicleUnitss();

            String[] fromDate = vendorTO.getFromDates();
            String[] toDate = vendorTO.getToDates();

            String[] spotCost = vendorTO.getSpotCosts();
            String[] additionalCost = vendorTO.getAdditionalCosts();
            String[] lHCNO = vendorTO.getlHCNO();
            String[] marketRate = vendorTO.getMarketRate();
            String[] loadTypeIds = vendorTO.getLoadTypeIds();
//            String[] containerTypeIds = vendorTO.getContainerTypeIds();
//            String[] containerQtys = vendorTO.getContainerQtys();
            String[] originIdFullTruck = vendorTO.getOriginIdFullTrucks();
            String[] originNameFullTruck = vendorTO.getOriginNameFullTrucks();
            String[] pointsId1 = vendorTO.getPointsId1();
            String[] pointsId2 = vendorTO.getPointsId2();
            String[] pointsId3 = vendorTO.getPointsId3();
            String[] pointsId4 = vendorTO.getPointsId4();
            String[] destinationIdFullTruck = vendorTO.getDestinationIdFullTrucks();
            String[] destinationNameFullTruck = vendorTO.getDestinationNameFullTrucks();
            String[] travelKmFullTruck = vendorTO.getTravelKmFullTrucks();
            String[] travelHourFullTruck = vendorTO.getTravelHourFullTrucks();
            String[] travelMinuteFullTruck = vendorTO.getTravelMinuteFullTrucks();

            String[] AdvanceMode = vendorTO.getAdvanceMode();
            String[] ModeRate = vendorTO.getModeRate();
            String[] InitialAdvance = vendorTO.getInitialAdvance();
            String[] EndAdvance = vendorTO.getEndAdvance();

            boolean processStatus = true;
            boolean contractStatus = true;
            
            System.out.println("contract : "+contractId);
            
            if (contractId > 0) {
                
                System.out.println("contractTypeId : "+contractId);
                
                if (contractTypeId.equals("3") || contractTypeId.equals("2")) {
                    
                    System.out.println("originIdFullTruck.length : "+originIdFullTruck.length);
                    
                    if (originIdFullTruck.length > 0) {
                        for (int i = 0; i < originIdFullTruck.length; i++) {
                            String originIdFullTrucks = originIdFullTruck[i];
                            String originNameFullTrucks = originNameFullTruck[i];
                            String pointId1 = pointsId1[i];
                            String pointId2 = pointsId2[i];
                            String pointId3 = pointsId3[i];
                            String pointId4 = pointsId4[i];
                            String destinationNameFullTrucks = destinationNameFullTruck[i];
                            String destinationIdFullTrucks = destinationIdFullTruck[i];
                            String travelKmFullTrucks = travelKmFullTruck[i];
                            String travelHourFullTrucks = travelHourFullTruck[i];
                            String travelMinuteFullTrucks = travelMinuteFullTruck[i];
                            //String point1Name = vendorTO.getPointName1A()[i];
                            
                            System.out.println("vehicleTypeId.length : "+vehicleTypeId.length);
                            
                            if (vehicleTypeId.length > 0) {
                                for (int j = 0; j < vehicleTypeId.length; j++) {
                                    System.out.println("Throttle here........step 1 ");
                                    
                                    System.out.println("vehicleTypeId.length : "+iCnt1[j]);
                                    System.out.println("vehicleTypeId.length : "+iCnt[i]);
                                    
                                    if (iCnt[i] == null ? iCnt1[j] == null : iCnt[i].equals(iCnt1[j])) {
                                        System.out.println("Throttle here........Step 2 ");
                                        String vehicleTypeIds = vehicleTypeId[j];
                                        System.out.println("vehicleTypeIds :  "+vehicleTypeIds);
                                        String vehicleUnit = vehicleUnits[j];
                                        System.out.println("vehicleUnit :  "+vehicleUnit);

                                        String fromDates = fromDate[j];
                                        String toDates = toDate[j];

                                        String spotCosts = spotCost[j];
                                        System.out.println("spotCosts :  "+spotCosts);
                                        String additionalCosts = additionalCost[j];
                                        int lHCNOs = Integer.parseInt(lHCNO[j]);
                                        String loadTypeId = loadTypeIds[j];
    //                                  String containerTypeId = containerTypeIds[j];
                                        //                                  String containerQty = containerQtys[j];
                                        System.out.println("originIdFullTruck : "+originIdFullTruck);
                                        
                                        if (!"".equals(originIdFullTruck)) {
                                        String tripAdvanceMode = AdvanceMode[j];
                                        String tripModeRate = ModeRate[j];
                                        String InitialtripAdvance = InitialAdvance[j];
                                        String EndtripAdvance = EndAdvance[j];

                                            System.out.println("Throttle here........Step 3 ");
                                            if (processStatus) {
                                                vendorTO.setOriginIdFullTruck(originIdFullTrucks);
                                                vendorTO.setPointId1(pointId1);
                                                vendorTO.setPointId2(pointId2);
                                                vendorTO.setPointId3(pointId3);
                                                vendorTO.setPointId4(pointId4);
                                                vendorTO.setDestinationIdFullTruck(destinationIdFullTrucks);
                                                vendorTO.setVehicleTypeId(vehicleTypeIds);
                                                vendorTO.setFromDate(fromDates);
                                                vendorTO.setToDate(toDates);
//                                              vendorTO.setContainerTypeId(containerTypeId);
                                                vendorTO.setLoadTypeId(loadTypeId);
//                                              vendorTO.setContainerQty(containerQty);
                                                String marketHireRate = vendorDAO.getMarketHireRate(vendorTO, session);
                                                marketHireRate = "1";
                                                boolean appSts = true;
                                                if (!"0".equals(marketHireRate)) {
//                                                if (Double.parseDouble(marketHireRate) <= Double.parseDouble(additionalCosts)) {
//                                                    appSts = false;
////                                                    if (Double.parseDouble(marketHireRate) > Double.parseDouble(additionalCosts)) {
////                                                        appSts = true;
////                                                    } else {
////                                                        appSts = false;
////                                                    }
//                                                } else {
//                                                    appSts = true;
//                                                }
                                                    if (appSts) {
                                                        vendorTO.setApprovalStatus(1);
                                                        vendorTO.setRequestOn(null);
                                                        vendorTO.setRequestBy(null);
                                                        vendorTO.setRequestRemarks(null);
                                                        vendorTO.setApprovalOn("1");
                                                        vendorTO.setApprovalBy(UserId + "");
                                                        vendorTO.setApprovalRemarks("Auto Approval");
                                                        contractStatus = true;

                                                    } else {
                                                        vendorTO.setApprovalStatus(2);
                                                        vendorTO.setRequestOn("1");
                                                        vendorTO.setRequestBy(UserId + "");
                                                        vendorTO.setRequestRemarks("Auto Request and waiting for approval");
                                                        vendorTO.setApprovalOn(null);
                                                        vendorTO.setApprovalBy(null);
                                                        vendorTO.setApprovalRemarks(null);
                                                        contractStatus = false;
                                                    }
                                                    //}else{
                                                    //}
                                                    
                                                    int checkDuplicate = vendorDAO.checkRouteExists(vendorTO, session);

                                                   if (checkDuplicate == 0) {
                                                    status = vendorDAO.processInsertVehicleVendorRoute(vendorTO, UserId, contractId, originIdFullTrucks, originNameFullTrucks, destinationIdFullTrucks, destinationNameFullTrucks, travelKmFullTrucks, travelHourFullTrucks, travelMinuteFullTrucks,
                                                            vehicleTypeIds, vehicleUnit, fromDates, toDates, spotCosts, additionalCosts, loadTypeId, pointId1, pointId2, pointId3, pointId4, tripAdvanceMode, tripModeRate, InitialtripAdvance, EndtripAdvance, session);

                                                    vendorTO.setVehicleTypeId(vehicleTypeIds);
                                                    vendorTO.setContractHireId(status + "");
                                                    vendorTO.setAdditionalCost(additionalCosts);

                                                    System.out.println("vehicleTypeIds : " + vehicleTypeIds);
                                                    System.out.println("status : " + status);
                                                    System.out.println("additionalCosts : " + additionalCosts);
                                                    System.out.println(" total lhc's : " + lHCNO);

                                                    for (int ii = 0; ii < lHCNOs; ii++) {
                                                        int lhcid = vendorDAO.processInsertLHCDetails(vendorTO, UserId, session);
                                                        System.out.println("lhcid : " + lhcid);
                                                    }
                                                    }

                                                    VendorTO mailTO = new VendorTO();
                                                    if (!appSts) {
                                                        System.out.println("approvalStatus" + vendorTO.getApprovalStatus());
                                                        mailTO.setContractHireId(status + "");
                                                        mailTO.setOriginNameFullTruck(originNameFullTrucks);
                                                        mailTO.setDestinationNameFullTruck(destinationNameFullTrucks);
                                                        //mailTO.setPoint1Name(point1Name);
                                                        mailTO.setLoadTypeId(loadTypeId);
//                                                        mailTO.setContainerTypeId(containerTypeId);
//                                                        mailTO.setContainerQty(containerQty);
                                                        mailTO.setVehicleType(vehicleTypeIds);
                                                        mailTO.setSpotCost(spotCosts);
                                                        mailTO.setAdditionalCost(additionalCosts);
                                                        mailTO.setVendorName(vendorName);
                                                        mailTO.setFromDate(fromDates);
                                                        mailTO.setVendorName(toDates);
                                                        System.out.println("userID@@@" + UserId);
                                                       // int status3 = vendorDAO.sendContractApprovalMail(mailTO, UserId, session);
                                                    }
                                                    processStatus = true;
                                                } else {

                                                    processStatus = false;
                                                    contractId = -100;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //if (contractTypeId.equals("3") || contractTypeId.equals("1")) {
                    if (!"".equals(vendorTO.getAgreedFuelPrice())) {
                        if (processStatus) {
                            status = vendorDAO.processInsertVehicleVendorDedicate(vendorTO, UserId, contractId, vehicleTypeIdDedicate, vehicleUnitsDedicate,
                                    contractCategory, fixedCost, fixedHrs, fixedMin, totalFixedCost,
                                    rateCost, rateLimit, maxAllowableKM, workingDays, holidays, addCostDedicate, session);
                        }
                    }
                //}
            }
            if (!contractStatus) {
                contractId = -200;
            }
            if (processStatus) {
                session.commitTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return contractId;
    }

    /**
     * This method used to Insert Vendor Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises ===
     */
    public ArrayList getVehicleTypeList(VendorTO vendorTO) {

        ArrayList activeTypeList = new ArrayList();
        activeTypeList = vendorDAO.getVehicleTypeList(vendorTO);
        return activeTypeList;
    }
    
       public ArrayList getPastVehicleTypeList(VendorTO vendorTO) {

        ArrayList activeTypeList = new ArrayList();
        activeTypeList = vendorDAO.getPastVehicleTypeList(vendorTO);
        return activeTypeList;
    }

    /**
     * This method used to Insert Vendor Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises ===
     */
    public ArrayList getVehicleList(VendorTO vendorTO) {
        ArrayList vehicleList = new ArrayList();
        vehicleList = vendorDAO.getVehicleList(vendorTO);
        if (vehicleList.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        }
        return vehicleList;
    }

    /**
     * This method used to Get Vehicle List
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises ===
     */
    public ArrayList getVehicleLists(VendorTO vendorTO) {
        ArrayList vehicleList = new ArrayList();
        vehicleList = vendorDAO.getVehicleLists(vendorTO);
        if (vehicleList.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        }
        return vehicleList;
    }

    /**
     * This method used to Vendor Vehicle Contract List
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises ===
     */
    public ArrayList vendorVehicleContractList(VendorTO vendorTO) {
        ArrayList vehicleList = new ArrayList();
        vehicleList = vendorDAO.vendorVehicleContractList(vendorTO);
        if (vehicleList.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        }
        return vehicleList;

    }

    /**
     * This method used to Vendor Vehicle Contract List
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises ===
     */
    public ArrayList viewVendorVehicleContract(VendorTO vendorTO) {
        ArrayList viewVendorVehicleContract = new ArrayList();
        viewVendorVehicleContract = vendorDAO.viewVendorVehicleContract(vendorTO);
        if (viewVendorVehicleContract.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        }
        return viewVendorVehicleContract;
    }

    public ArrayList processGetUomList() throws FPRuntimeException, FPBusinessException {

        ArrayList MfrList = new ArrayList();
        MfrList = vendorDAO.getUomList();

        return MfrList;
    }

    public ArrayList getCityToList(OperationTO opTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getCityToList = new ArrayList();
        getCityToList = vendorDAO.getCityToList(opTO);
        return getCityToList;
    }

    public int updateVendorContractDetails(VendorTO vendorTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 1;
        int contractId = 0;
        int status = 0;
        VendorTO mailTO = new VendorTO();
        SqlMapClient session = vendorDAO.getSqlMapClient();
        try {
            session.startTransaction();
            String vendorName = vendorTO.getVendorName();
            vendorDAO.updateVendorContractDetails(vendorTO, userId, session);
            boolean processStatus = true;
            boolean contractStatus = true;
            if (updateStatus > 0) {
                if (vendorTO.getContractDedicateIds() != null) {
                    contractId = vendorDAO.updateVendorVehicleDedicate(vendorTO, userId, vendorTO.getContractDedicateIds(), vendorTO.getFixedCostE(),
                            vendorTO.getFixedHrsE(), vendorTO.getFixedMinE(), vendorTO.getTotalFixedCostE(), vendorTO.getRateCostE(),
                            vendorTO.getRateLimitE(), vendorTO.getWorkingDaysE(), vendorTO.getHolidaysE(), vendorTO.getAddCostDedicateE(),  vendorTO.getMaxAllowableKME(),  vendorTO.getVehicleUnitsDedicateE(), vendorTO.getActiveIndE(), session);
                }
                if (vendorTO.getContractHireIds() != null) {
                    if (vendorTO.getContractHireIds().length > 0) {
                        // Checck Approval
                        contractId = vendorDAO.updateVendorVehicleRoute(vendorTO, userId, vendorTO.getContractHireIds(), vendorTO.getFromDateE(), vendorTO.getToDateE(), vendorTO.getSpotCostE(),
                                vendorTO.getAdditionalCostE(), vendorTO.getActiveIndD(), vendorName, vendorTO.getAdvanceModeE(), vendorTO.getModeRateE(), vendorTO.getInitialAdvanceE(), vendorTO.getEndAdvanceE(), session);
                        System.out.println("updateStatus=@@@" + updateStatus);
                    }
                }
//                    String pointId1 = "0", pointId2 = "0", pointId3 = "0", pointId4 = "0" ;
                String[] marketRate = vendorTO.getMarketRate();
                String[] pointsId1 = vendorTO.getPointsId1();
                String[] pointsId2 = vendorTO.getPointsId2();
                String[] pointsId3 = vendorTO.getPointsId3();
                String[] pointsId4 = vendorTO.getPointsId4();

                String[] AdvanceMode = vendorTO.getAdvanceMode();
                String[] ModeRate = vendorTO.getModeRate();
                String[] InitialAdvance = vendorTO.getInitialAdvance();
                String[] EndAdvance = vendorTO.getEndAdvance();
                String[] lHCNO = vendorTO.getlHCNO();

                if (Integer.parseInt(vendorTO.getVendorId()) > 0) {
                    if (!"".equals(vendorTO.getOriginIdFullTrucks()) && vendorTO.getOriginIdFullTrucks() != null) {
                        if (vendorTO.getOriginIdFullTrucks().length > 0) {
                            System.out.println("vendorTO.getOriginIdFullTrucks().length@@" + vendorTO.getOriginIdFullTrucks().length);
                            for (int i = 0; i < vendorTO.getOriginIdFullTrucks().length; i++) {
                                String originIdFullTrucks = vendorTO.getOriginIdFullTrucks()[i];
                                String originNameFullTrucks = vendorTO.getOriginNameFullTrucks()[i];
                                String destinationNameFullTrucks = vendorTO.getDestinationNameFullTrucks()[i];
                                String destinationIdFullTrucks = vendorTO.getDestinationIdFullTrucks()[i];
                                String travelKmFullTrucks = vendorTO.getTravelKmFullTrucks()[i];
                                String travelHourFullTrucks = vendorTO.getTravelHourFullTrucks()[i];
                                String travelMinuteFullTrucks = vendorTO.getTravelMinuteFullTrucks()[i];
                                String pointId1 = pointsId1[i];
                                String pointId2 = pointsId2[i];
                                String pointId3 = pointsId3[i];
                                String pointId4 = pointsId4[i];

                                if (vendorTO.getVehicleTypeIds().length > 0) {
                                    for (int j = 0; j < vendorTO.getVehicleTypeIds().length; j++) {
                                        if (vendorTO.getiCnt()[i] == null ? vendorTO.getiCnt1()[j] == null : vendorTO.getiCnt()[i].equals(vendorTO.getiCnt1()[j])) {
                                            String vehicleTypeIds = vendorTO.getVehicleTypeIds()[j];
                                            String fromDates = vendorTO.getFromDates()[j];
                                            String toDates = vendorTO.getToDates()[j];
                                            String vehicleUnit = vendorTO.getVehicleUnitss()[j];
                                            String spotCosts = vendorTO.getSpotCosts()[j];
                                            String additionalCosts = vendorTO.getAdditionalCosts()[j];
                                            String loadTypeId = vendorTO.getLoadTypeIds()[j];
                                            int lHCNOs = Integer.parseInt(lHCNO[j]);

//                                          String containerTypeId = vendorTO.getContainerTypeIds()[j];
//                                          String containerQty = vendorTO.getContainerQtys()[j];
                                            
                                            System.out.println("vehicleTypeIds : "+vehicleTypeIds);
                                            System.out.println("fromDates  : "+fromDates);
                                            System.out.println("toDates : "+toDates);
                                            System.out.println("additionalCosts : "+additionalCosts);
                                            System.out.println("loadTypeId : "+loadTypeId);
                                            
                                            String tripAdvanceMode = AdvanceMode[j];
                                            String tripModeRate = ModeRate[j];
                                            String InitialtripAdvance = InitialAdvance[j];
                                            String EndtripAdvance = EndAdvance[j];

                                            String point1Name = "";

                                            if (!"".equals(vendorTO.getOriginIdFullTrucks())) {
                                                // Checck Approval
                                                vendorTO.setVehicleTypeId(vehicleTypeIds);
                                                vendorTO.setLoadTypeId(loadTypeId);
//                                                vendorTO.setContainerTypeId(containerTypeId);
//                                                vendorTO.setContainerQty(containerQty);
                                                vendorTO.setOriginIdFullTruck(originIdFullTrucks);
                                                vendorTO.setPointId1(pointId1);
                                                vendorTO.setPointId2(pointId2);
                                                vendorTO.setPointId3(pointId3);
                                                vendorTO.setPointId4(pointId4);
                                                vendorTO.setPointId4(pointId4);

                                                vendorTO.setDestinationIdFullTruck(destinationIdFullTrucks);
                                                if (processStatus) {
                                                    String marketHireRate = vendorDAO.getMarketHireRate(vendorTO, session);
                                                    System.out.println("marketHireRateBP@@@" + marketHireRate);
                                                    marketHireRate = "1";
                                                    boolean appSts = true;
                                                    if (!"".equals(marketHireRate)) {
//                                                        if (Double.parseDouble(marketHireRate) <= Double.parseDouble(additionalCosts)) {
//                                                            appSts = true;
////                                                            if (Double.parseDouble(marketHireRate) > Double.parseDouble(additionalCosts)) {
////                                                                appSts = true;
////                                                            } else {
////                                                                appSts = false;
////                                                            }
//                                                        } else {
//                                                            appSts = false;
//                                                        }
                                                        if (appSts) {
                                                            vendorTO.setApprovalStatus(1);
                                                            vendorTO.setRequestOn(null);
                                                            vendorTO.setRequestBy(null);
                                                            vendorTO.setRequestRemarks(null);
                                                            vendorTO.setApprovalOn("1");
                                                            vendorTO.setApprovalBy(userId + "");
                                                            vendorTO.setApprovalRemarks("Auto Approval");
                                                            contractStatus = true;
                                                        } else {
                                                            vendorTO.setApprovalStatus(2);
                                                            vendorTO.setRequestOn("1");
                                                            vendorTO.setRequestBy(userId + "");
                                                            vendorTO.setRequestRemarks("Auto Request and waiting for approval");
                                                            vendorTO.setApprovalOn(null);
                                                            vendorTO.setApprovalBy(null);
                                                            vendorTO.setApprovalRemarks(null);
                                                            contractStatus = false;
                                                        }

                                                        //////////////////// arun edit for duplicate contract ////////////////
                                                        int checkDuplicate = 100;

                                                        System.out.println("check duplicate " + checkDuplicate);

                                                        checkDuplicate = vendorDAO.checkRouteExists(vendorTO, session);

                                                        if (checkDuplicate == 0) {

                                                            contractId = vendorDAO.processInsertVehicleVendorRoute(vendorTO, userId, Integer.parseInt(vendorTO.getContractId()), originIdFullTrucks, originNameFullTrucks, destinationIdFullTrucks, destinationNameFullTrucks, travelKmFullTrucks, travelHourFullTrucks, travelMinuteFullTrucks, vehicleTypeIds, vehicleUnit, fromDates, toDates, spotCosts, additionalCosts, loadTypeId, pointId1, pointId2, pointId3, pointId4, tripAdvanceMode, tripModeRate, InitialtripAdvance, EndtripAdvance, session);
                                                            
                                                                vendorTO.setVehicleTypeId(vehicleTypeIds);
                                                                vendorTO.setContractHireId(contractId + "");
                                                                vendorTO.setAdditionalCost(additionalCosts);

                                                                System.out.println("contractId : " + contractId);
                                                                System.out.println("vehicleTypeIds : " + vehicleTypeIds);
                                                                System.out.println("status : " + updateStatus);
                                                                System.out.println("additionalCosts : " + additionalCosts);
                                                                System.out.println(" total lhc's : " + lHCNO);

                                                                for (int ii = 0; ii < lHCNOs; ii++) {
                                                                    int lhcid = vendorDAO.processInsertLHCDetails(vendorTO, userId, session);
                                                                    System.out.println("lhcid : " + lhcid);
                                                                }
                                                        }else{
                                                        
                                                            contractId = -300;
                                                            
                                                        }

                                                        System.out.println("insertStatus " + updateStatus);

//                                                        if (!appSts) {
//                                                            System.out.println("approvalStatus" + vendorTO.getApprovalStatus());
//                                                            mailTO.setContractHireId(updateStatus + "");
//                                                            mailTO.setOriginNameFullTruck(originNameFullTrucks);
//                                                            mailTO.setDestinationNameFullTruck(destinationNameFullTrucks);
//                                                            mailTO.setPoint1Name(point1Name);
//                                                            mailTO.setLoadTypeId(loadTypeId);
//                                                            //mailTO.setContainerTypeId(containerTypeId);
//                                                            //mailTO.setContainerQty(containerQty);
//                                                            mailTO.setVehicleType(vehicleTypeIds);
//                                                            mailTO.setFromDate(fromDates);
//                                                            mailTO.setToDate(toDates);
//                                                            mailTO.setSpotCost(spotCosts);
//                                                            mailTO.setAdditionalCost(additionalCosts);
//                                                            mailTO.setVendorName(vendorName);
//                                                            System.out.println("userID@@@" + userId);
//                                                            int status3 = vendorDAO.sendContractApprovalMail(mailTO, userId, session);
//                                                        }

                                                    } else {
                                                        processStatus = false;
                                                        contractId = -100;
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                    
                    System.out.println("inn processInsertVehicleVendorDedicate part called ");
                    if (!"".equals(vendorTO.getContractCategorys()) && vendorTO.getContractCategorys() != null) {
                        updateStatus = vendorDAO.processInsertVehicleVendorDedicate(vendorTO, userId, Integer.parseInt(vendorTO.getContractId()), vendorTO.getVehicleTypeIdDedicates(), vendorTO.getVehicleUnitsDedicates(), vendorTO.getContractCategorys(), vendorTO.getFixedCosts(), vendorTO.getFixedHrss(), vendorTO.getFixedMins(), vendorTO.getTotalFixedCosts(), vendorTO.getRateCosts(), vendorTO.getRateLimits(), vendorTO.getMaxAllowableKMs(), vendorTO.getWorkingDayss(), vendorTO.getHolidayss(), vendorTO.getAddCostDedicates(), session);
                    }
                }
            }
            if (!contractStatus) {
                contractId = -200;
            }
            if (processStatus) {
                session.commitTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return contractId;
    }

    public String viewVendorVehicle(VendorTO vendorTO) {
        String vendorContractLists = "";
        vendorContractLists = vendorDAO.viewVendorVehicle(vendorTO);
        return vendorContractLists;
    }

    public ArrayList getDedicateList(VendorTO vendorTO) throws FPRuntimeException, FPBusinessException {
        ArrayList dedicateList = new ArrayList();
        dedicateList = vendorDAO.getDedicateList(vendorTO);
        return dedicateList;
    }

    public ArrayList getHireList(VendorTO vendorTO) throws FPRuntimeException, FPBusinessException {
        ArrayList hireList = new ArrayList();
        hireList = vendorDAO.getHireList(vendorTO);
        return hireList;
    }

//    public int updateVendorVehicleDedicate(VendorTO vendorTO, int userId, String[] contractDedicateId, String[] fixedCostE, String[] fixedHrsE, String[] fixedMinE, String[] totalFixedCostE, String[] rateCostE,
//            String[] rateLimitE, String[] workingDaysE, String[] holidaysE, String[] addCostDedicateE, String[] activeIndE) throws FPRuntimeException, FPBusinessException {
//        int vendorId = 0;
//        vendorId = vendorDAO.updateVendorVehicleDedicate(vendorTO, userId, contractDedicateId, fixedCostE, fixedHrsE, fixedMinE, totalFixedCostE, rateCostE,
//                rateLimitE, workingDaysE, holidaysE, addCostDedicateE, activeIndE);
//        return vendorId;
//    }
//    public int updateVendorVehicleRoute(VendorTO vendorTO, int userId, String[] contractHireId, String[] spotCostE, String[] additionalCostE, String[] activeIndD) throws FPRuntimeException, FPBusinessException {
//        int vendorId = 0;
//        vendorId = vendorDAO.updateVendorVehicleRoute(vendorTO, userId, contractHireId, spotCostE, additionalCostE, activeIndD);
//        return vendorId;
//    }
    public int processInsertVehicleContract(VendorTO vendorTO, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertVehicleContract = 0;
        int trailerId = 0;

        int status = 0;

        status = vendorDAO.processInsertVehicleContract(vendorTO, UserId);
        //       trailerId = vendorDAO.processInsertTrailerContract(vendorTO, UserId);

        return status;
    }

    public int processInsertTrailerContract(VendorTO vendorTO, String[] trailerType, String[] trailerNo, String[] trailerRemarks, int UserId) throws FPRuntimeException, FPBusinessException {
        int trailerId = 0;
        SqlMapClient session = vendorDAO.getSqlMapClient();
        try {
            session.startTransaction();
            trailerId = vendorDAO.processInsertTrailerContract(vendorTO, trailerType, trailerNo, trailerRemarks, UserId, session);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return trailerId;
    }

    public int processUpdateTrailerContract(VendorTO vendorTO, String[] trailerNo, String[] activeIndTrailor, String[] trailerRemarks, int userId) throws FPRuntimeException, FPBusinessException {
        int vendorId = 0;
        int trailerId = 0;

        int status = 0;

        // vendorId = vendorDAO.processInsertVehicleContract(vendorTO, UserId);
        trailerId = vendorDAO.processUpdateTrailerContract(vendorTO, trailerNo, activeIndTrailor, trailerRemarks, userId);

        return status;
    }

    public ArrayList getVehicleConfig(VendorTO vendorTO) {

        ArrayList vehicles = new ArrayList();
        vehicles = vendorDAO.getVehicleConfig(vendorTO);
        return vehicles;
    }

    public ArrayList getTrailerList(VendorTO vendorTO) {

        ArrayList trailerList = new ArrayList();
        trailerList = vendorDAO.getTrailerConfig(vendorTO);
        return trailerList;
    }

    public int updateTrailer(VendorTO vendorTO, int userId, String[] contractDedicateId, String[] trailerType, String[] trailerNo, String[] trailerRemarks, String[] activeInd) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = vendorDAO.updateTrailer(contractDedicateId, trailerType, trailerNo, trailerRemarks, activeInd);
        return insertStatus;
    }

    public int updateVehicles(VendorTO vendorTO, int userId, String[] contractDedicateIdVehicle, String[] vehicleRegNo, String[] agreedDate, String[] mfr, String[] model, String[] remarks, String[] activeInd) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = vendorDAO.updateVehicles(contractDedicateIdVehicle, vehicleRegNo, agreedDate, mfr, model, remarks, activeInd);
        return insertStatus;
    }

    public ArrayList processTrailerList(VendorTO vendorTO) throws FPRuntimeException, FPBusinessException {

        ArrayList vehicleList = new ArrayList();
        System.out.println("i m here 3");
        vehicleList = vendorDAO.getTrailerDetail(vendorTO);

        return vehicleList;
    }

    public ArrayList getCityFromList(VendorTO vendorTO) throws FPRuntimeException, FPBusinessException {

        ArrayList cityList = new ArrayList();
        System.out.println("i m here 3");
        cityList = vendorDAO.getCityFromList(vendorTO);

        return cityList;
    }

    public int updatesaveVehicles(VendorTO vendorTO, int userId, String[] vehicleRegNo1, String[] vehicleTypeIdcheck1, String[] mfr1, String[] model1, String[] agreedDate, String[] remarks) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        SqlMapClient session = vendorDAO.getSqlMapClient();
        try {
            session.startTransaction();
            insertStatus = vendorDAO.updatesaveVehicles(vendorTO, userId, vehicleRegNo1, vehicleTypeIdcheck1, mfr1, model1, agreedDate, remarks, session);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }

        return insertStatus;
    }

    public String processGetModels(int mfr, String vehicleTypeId) throws FPRuntimeException, FPBusinessException {
        ArrayList models = new ArrayList();
        int counter = 0;
        VendorTO rack = null;
        models = vendorDAO.getAjaxModelList(mfr, vehicleTypeId);

        Iterator itr;
        String model = "";
        if (models.size() == 0) {
            model = "";
        } else {
            itr = models.iterator();
            while (itr.hasNext()) {
                rack = new VendorTO();
                rack = (VendorTO) itr.next();
                if (counter == 0) {
                    model = rack.getModel() + "-" + rack.getModelName();
                    counter++;
                } else {
                    model = model + "~" + rack.getModel() + "-" + rack.getModelName();
                }
            }
        }
        return model;
    }

    public String getVehicleContractDedicateId(VendorTO vendorTO, int userId) throws FPRuntimeException, FPBusinessException {
        String contractDedicateId = "";
        contractDedicateId = vendorDAO.getVehicleContractDedicateId(vendorTO, userId);

        return contractDedicateId;
    }

    public int processUpdateVehicleContract(VendorTO vendorTO, String[] configureId, String[] configureIds, String[] replaceVehicleId, String[] vehicleRegNo,
            String[] agreedDate, String[] mfr, String[] model, String[] vehicleRemarks, String[] activeIndVehicle,
            String[] existVehicle, String[] inDate, String[] replacementStatus, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        SqlMapClient session = vendorDAO.getSqlMapClient();
        System.out.println("bp>>>>>>>>>>>>>>>>>");
        try {
            session.startTransaction();
            status = vendorDAO.processUpdateVehicleContract(vendorTO, configureId, configureIds, replaceVehicleId, vehicleRegNo,
                    agreedDate, mfr, model, vehicleRemarks, activeIndVehicle, existVehicle, inDate, replacementStatus, userId, session);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                session.getSession().close();
            }
        }

        return status;
    }

    public ArrayList getFuelHike(VendorTO vendorTO) throws FPRuntimeException, FPBusinessException {
        ArrayList fuelHike = new ArrayList();
        fuelHike = vendorDAO.getFuelHike(vendorTO);
        return fuelHike;
    }

    public String checkVehicleRegNo(String vehicleRegNo) {
        String mess = "";
        mess = vendorDAO.checkVehicleRegNo(vehicleRegNo);
        return mess;
    }

    public String checkTrailerNo(String trailerNo) {
        String mess = "";
        mess = vendorDAO.checkTrailerNo(trailerNo);
        return mess;
    }

    public int processUpdateVendorDetails(VendorTO vendorTO, String index, int UserId, String[] actualFilePath, String tripSheetId1, String[] fileSaved) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = vendorDAO.doVendorUpdateDetails(vendorTO, UserId, actualFilePath, tripSheetId1, fileSaved);
        return status;
    }

    public int updateVendorApprovedStatus(int UserId, String contractHireId, String approvalStatus) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = vendorDAO.updateVendorApprovedStatus(UserId, contractHireId, approvalStatus);
        return status;
    }

    public ArrayList getVendorNameDetails(VendorTO vendorTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vendorList = new ArrayList();
        vendorList = vendorDAO.getVendorNameDetails(vendorTO);
        return vendorList;
    }

    public int checkVendorNameExists(VendorTO vendorTO) {
        int status = 0;
        status = vendorDAO.checkVendorNameExists(vendorTO);
        return status;
    }

    public String getMarketHireRate(VendorTO vendorTO) {
        String marketHireRate = "";
        marketHireRate = vendorDAO.getMarketHireRate(vendorTO);
        return marketHireRate;
    }

    public ArrayList getReplaceVehicle(VendorTO vendorTO) throws FPRuntimeException, FPBusinessException {

        ArrayList getReplaceVehicle = new ArrayList();
        System.out.println("i m here 3");
        getReplaceVehicle = vendorDAO.getReplaceVehicle(vendorTO);

        return getReplaceVehicle;
    }

    public ArrayList getVehTypeList() throws FPRuntimeException, FPBusinessException {
        ArrayList VehTypeList = new ArrayList();
        VehTypeList = vendorDAO.getVehTypeList();
        return VehTypeList;
    }

    public ArrayList getLHCList(VendorTO vendorTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getLHCList = new ArrayList();
        getLHCList = vendorDAO.getLHCList(vendorTO);
        return getLHCList;
    }

    public int processInsertLHCDetails(VendorTO vendorTO, int UserId, String[] fileSaved) throws FPRuntimeException, FPBusinessException {
        int lhcid = 0;
        lhcid = vendorDAO.processInsertLHCDetails(vendorTO, UserId, fileSaved);
        return lhcid;
    }

    public ArrayList getPastContractDetails(VendorTO vendorTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getPastContractDetails = new ArrayList();
        getPastContractDetails = vendorDAO.getPastContractDetails(vendorTO);
        return getPastContractDetails;
    }

    public ArrayList pastContractMinDetails(String vendorId) throws FPRuntimeException, FPBusinessException {
        ArrayList pastContractMinDetails = new ArrayList();
        pastContractMinDetails = vendorDAO.pastContractMinDetails(vendorId);
        return pastContractMinDetails;
    }

    public ArrayList pastContractMaxDetails(String vendorId) throws FPRuntimeException, FPBusinessException {
        ArrayList pastContractMaxDetails = new ArrayList();
        pastContractMaxDetails = vendorDAO.pastContractMaxDetails(vendorId);
        return pastContractMaxDetails;
    }

    public int insertVendorPenalitycharges(VendorTO vendorTO, String penality, String chargeamount, String pcmremarks, String pcmunit) {
        int insertStatus = 0;
        insertStatus = vendorDAO.insertVendorPenalitycharges(vendorTO, penality, chargeamount, pcmremarks, pcmunit);
        return insertStatus;
    }

    public int insertVendorDetentioncharges(VendorTO vendorTO, String detention, String dcmunit, String dcmToUnit, String chargeamt, String dcmremarks) {
        int insertStatus = 0;
        insertStatus = vendorDAO.insertVendorDetentioncharges(vendorTO, detention, dcmunit, dcmToUnit, chargeamt, dcmremarks);
        return insertStatus;
    }

    public ArrayList getviewpenalitycharge(VendorTO vendorTO) {
        ArrayList viewpenalitycharge = new ArrayList();
        viewpenalitycharge = vendorDAO.getviewpenalitycharge(vendorTO);
        System.out.println("contract BP");
        return viewpenalitycharge;
    }

    public ArrayList getviewdetentioncharge(VendorTO vendorTO) {
        ArrayList viewdetentioncharge = new ArrayList();
        viewdetentioncharge = vendorDAO.getviewdetentioncharge(vendorTO);
        return viewdetentioncharge;
    }

    public ArrayList getDetaintionTimeSlot() throws FPBusinessException, FPRuntimeException {
        ArrayList detaintionTimeSlot = new ArrayList();
        detaintionTimeSlot = vendorDAO.getDetaintionTimeSlot();
        return detaintionTimeSlot;
    }

     public int updateVendorContract() {
        int status = 0;
        status = vendorDAO.updateVendorContract();
        return status;
    }
     
      public int getRouteCheck(VendorTO vendorTO) throws FPBusinessException, FPRuntimeException {
        int getRouteCheck = 0;
        getRouteCheck = vendorDAO.getRouteCheck(vendorTO);
        return getRouteCheck;
    }
    
       public int insertContractUpload(VendorTO vendorTO) {
        int status = 0;
        status = vendorDAO.insertContractUpload(vendorTO);
        return status;
    }
       
       
        public ArrayList getContractUploadIncorect(VendorTO vendorTO) throws FPBusinessException, FPRuntimeException {
        ArrayList getConsignmentExcelUploadIncorect = new ArrayList();
        getConsignmentExcelUploadIncorect = vendorDAO.getContractUploadIncorect(vendorTO);
        return getConsignmentExcelUploadIncorect;
    }

    public ArrayList getContractExcelUpload(VendorTO vendorTO) throws FPBusinessException, FPRuntimeException {
        ArrayList getConsignmentExcelUpload = new ArrayList();
        getConsignmentExcelUpload = vendorDAO.getContractExcelUpload(vendorTO);
        return getConsignmentExcelUpload;
    }
    
      public int insertVendorContractExcel(int UserId) {
        int status = 0;
        status = vendorDAO.insertVendorContractExcel(UserId);
        return status;
    }
      
      public ArrayList getviewpenalitychargeList( ) {
        ArrayList viewpenalitycharge = new ArrayList();
        viewpenalitycharge = vendorDAO.getviewpenalitychargeList();
        return viewpenalitycharge;
    }
        public ArrayList getMarketHireList( ) {
        ArrayList viewpenalitycharge = new ArrayList();
        viewpenalitycharge = vendorDAO.getMarketHireList();
        return viewpenalitycharge;
    }
}
