package ets.domain.vendor.business;

/**
 *
 * @author vidya
 */
public class VendorTO {

    public VendorTO() {
    }

    private String origin = "";
    private String touchPoint1 = "";
    private String destination = "";
    private String vehicleType = "";
    private String contractAmount = "";
    private String counts = "";
    private String flag = "";
    private String originId = "";
    private String touchPoint1Id = "";
    private String destinationId = "";
    private String routeId = "";

    private String fcmPerc = null;
    private String annualTurnOver = null;
    private String gstExemp = null;
    private String penality = "";
    private String id = "";
    private String dcmremarks = "";
    private String dcmunit = "";
    private String chargeamt = "";
    private String detention = "";
    private String dcmunitTo = "";
    private String chargeamount = "";
    private String pcmunit = "";
    private String pcmremarks = "";
    private String timeSlot = "";
    private String timeSlotTo = "";
    private String stateId = "";
    private String gstNo = "";
    private String billingTypeId = "";
    private String[] originNameFullTruckE = null;
    private String[] destinationNameFullTruckE = null;
    private String[] pointName1E = null;
    private String[] pointName2E = null;
    private String[] pointName3E = null;
    private String[] pointName4E = null;
    private String[] approvalStatusE = null;
    private String[] loadTypeE = null;
    private String[] vehicleTypeE = null;
    private String[] containerTypeIdE1 = null;
    private String[] containerQtyE1 = null;

    private String[] pointName1A = null;
    private String[] pointName2A = null;
    private String[] pointName3A = null;
    private String[] pointName4A = null;

    private String[] approvalStatusA = null;
    private String[] vehicleTypeA = null;
    private String[] loadTypeA = null;
    private String[] containerTypeA = null;
    private String[] containerQtyA = null;

    private int userId = 0;
    private String contractRateId = "";
    private String status = "";
    private String tripCode = "";
    private String outDate = "";
    private String configureId = "";
    private String containerNo = "";
    private String totRunKM = "";
    private String allowedKM = "";
    private String totExtraRunKm = "";
    private String totExtraKMRate = "";
    private String actualRatePerKM = "";
    private String point1Name = "";
    private String point2Name = "";
    private String point3Name = "";
    private String point4Name = "";

    private String container = "";
    private String expense = "";
    private String route = "";
    private String containerType = "";
    private String containerQuty = "";
    private String tripId = "";
    private String virId = "";
    private String invoiceAmount = "";
    private String statusName = "";

    private int approvalStatus = 1;
    private String requestBy = "";
    private String requestOn = "";
    private String requestRemarks = "";
    private String approvalBy = "";
    private String approvalOn = "";
    private String approvalRemarks = "";
    private String[] contractHireIds = null;
    private String[] spotCostE = null;
    private String[] additionalCostE = null;
    private String[] activeIndD = null;
    private String[] fixedCostE = null;
    private String[] fixedHrsE = null;
    private String[] fixedMinE = null;
    private String[] totalFixedCostE = null;
    private String[] rateCostE = null;
    private String[] rateLimitE = null;
    private String[] workingDaysE = null;
    private String[] holidaysE = null;
    private String[] addCostDedicateE = null;
    private String[] trailorTypeUnitss = null;
    private String[] trailerTypes = null;
    private String[] activeIndE = null;
    private String[] contractDedicateIds = null;
    private String[] vehicleTypeIdDedicates = null;
    private String[] vehicleUnitsDedicates = null;
    private String[] vehicleUnitsDedicateE = null;
    private String[] contractCategorys = null;
    private String[] fixedCosts = null;
    private String[] fixedHrss = null;
    private String[] fixedMins = null;
    private String[] totalFixedCosts = null;
    private String[] rateCosts = null;
    private String[] rateLimits = null;
    private String[] maxAllowableKMs = null;
    private String[] maxAllowableKME = null;
    private String[] workingDayss = null;
    private String[] holidayss = null;
    private String[] addCostDedicates = null;
    private String[] iCnt = null;
    private String[] iCnt1 = null;
    private String[] vehicleTypeIds = null;
    private String[] vehicleUnitss = null;
    private String[] spotCosts = null;
    private String[] additionalCosts = null;
    private String[] marketRate = null;
    private String[] loadTypeIds = null;
    private String[] containerTypeIds = null;
    private String[] containerQtys = null;
    private String[] originIdFullTrucks = null;
    private String[] originNameFullTrucks = null;
    private String[] destinationIdFullTrucks = null;
    private String[] destinationNameFullTrucks = null;
    private String[] travelKmFullTrucks = null;
    private String[] travelHourFullTrucks = null;
    private String[] travelMinuteFullTrucks = null;
    private String[] pointsId1 = null;
    private String[] pointsId2 = null;
    private String[] pointsId3 = null;
    private String[] pointsId4 = null;

    private String pointId1 = "";
    private String pointId2 = "";
    private String pointId3 = "";
    private String pointId4 = "";
    private String contactName = "";
    private String loadTypeId = null;
    private String containerTypeId = null;
    private String containerQty = null;

    private String designation = "";
    private String emailId = "";

    private String teleNo = "";
    private String mobileNo = "";
    private String faxNo = "";

    private String bankName = "";
    private String branch = "";
    private String branchCode = "";
    private String accountNo = "";

    private String ifscCode = "";
    private String micrNo = "";
    private String msmeId = "";

    private String nstcId = "";
    private String gstId = "";
    private String dgsdId = "";
    private String rocId = "";
    private String eepcId = "";

    private String serViceTax = "";
    private String ecId = "";
    private String exciseDuty = "";

    private String vatId = "";
    private String cstId = "";
    private String panNo = "";

    private String fileName = "";
    private String fileName1 = "";
    private String fileName2 = "";
    private String fileName3 = "";
    private String fileName4 = "";
//    private String mfrName = "";
    private String fuelHikePercentage = "";
    private String trailerTypeAxles = "";
    private String trailorTypeId = "";
    private String trailerTypeId = "";
    private String trailorTypeIdDedicate = "";
    private String cityId = "";
    private String cityName = "";
    private String contractHireId = "";
    private String contractDedicateId = "";
    private String hikeFuelPrice = "";
    private String agreedFuelPrice = "";
    private String uom = "";
    private String description = "";
    private String vehicleTypeName = "";
    private String ratePerKm = "";
    private String ratePerExtraKm = "";
    private String driverResponsibility = "";
    private String fuelExpenseResponsibility = "";
    private String contractId = "";
    private String[] contractId1 = null;
    private String vendorContractStatus = "";
    private String startDate = "";
    private String endDate = "";
    private String VehicleType = "";
    private String vehicleTypeId = "";
    private String vehicleNo = "";
    private String vehicleId = "";
    private String contractTypeId = "";
    private String operationType = "";
    private String fixedKm = "";
    private String fixedAmount = "";
    private String rateExtraKm = "";
    private String driverReponsibility = "";
    private String fuleExpenseby = "";
    private String paymentType = "";
    private String paymentScheduleDays = "";
    private String vendorId = "";
    private String settlementType = "";
    private String vendorName = "";
    private String vendorTypeValue = "";
    private String vendorAddress = "";
    private String vendorPhoneNo = "";
    private String vendorMailId = "";
    private String isCreditableVendor = "";
    private String activeInd = "";
    private String vendorTypeId = "";
    private String creditDays = "";
    private String priceType = "";
    private String[] selectedIndex;
    private String mfrNames = "";
    private String[] mfrIds = null;
    private String mfrId = "";
    private String mfrName = "";
    private String itemId = "";
    private String itemName = "";
    private String catName = "";
    private String catId = "";
    private String tinNo = "";
    int startIndex = 0;
    int endIndex = 0;
    //23-04-2015  createvehiclevendor start here//
    private String originIdFullTruck = "";
    private String originNameFullTruck = "";
    private String destinationIdFullTruck = "";
    private String destinationNameFullTruck = "";
    private String routeIdFullTruck = "";
    private String travelKmFullTruck = "";
    private String travelHourFullTruck = "";
    private String travelMinuteFullTruck = "";
    private String vehicleIdFullTruck = "";
    private String dateWithReeferFullTruck = "";
    private String dateWithoutReeferFullTruck = "";
    //private String vehicleTypeId="";
    private String vehicleUnits = "";
    private String trailerType = "";
    private String tType = "";
    private String spotCost = "";
    private String additionalCost = "";
    //dedicate
    private String vehicleTypeIddeD = "";
    private String units = "";
    private String trailType = "";
    private String tUnits = "";
    private String fCost = "";
    private String fHrs = "";
    private String fMin = "";
    private String tFixed = "";
    private String vCost = "";
    private String mAllow = "";
    private String cCategory = "";
    private String rLimit = "";
    private String oTime = "";
    private String time = "";
    private String aCost = "";
    private String aeddeD = "0";
    private String uomdeD = "";
    private String fueldeD = "0";
    private String endDateold = "";
    private String contactId = "";
    private String vehicleRegNo = "";
    private String agreedDate = "";
    private String remarks = "";
    private String mfr = "";
    private String model = "";
    private String modelName = "";

    private String trailerType1 = "";
    private String trailerNo = "";
    private String trailerRemarks = "";
    private int contrctId = 0;
    private String vehicleTypeIdDedicate = "";
    private String trailorTypeDedicate = "";
    private String vehicleUnitsDedicate = "";
    private String trailorUnitsDedicate = "";
    private String contractCategory = "";
    private String fixedCost = "";
    private String fixedHrs = "";
    private String fixedMin = "";
    private String totalFixedCost = "";
    private String rateCost = "";
    private String rateLimit = "";
    private String maxAllowableKM = "";
    private String workingDays = "";
    private String holidays = "";
    private String trailorTypeUnits = "";
    private String addCostDedicate = "";
    private String existingVehicleUnit = "";
    private String trailerUnits = "";
//private String activeInd = "";

    private String noOfTrips = "";
    private String extraKm = "";
    private String otherVendorTypeIds = "";
//ram
    private String[] toDates = null;
    private String[] fromDates = null;
    private String[] toDateA = null;
    private String[] fromDateA = null;
    private String[] fromDateE = null;
    private String[] toDateE = null;
    private String fromDate = "";
    private String toDate = "";
    private String fromId = "";
    private String toId = "";

    private String[] advanceMode = null;
    private String[] modeRate = null;
    private String[] initialAdvance = null;
    private String[] endAdvance = null;

    private String advanceTripMode = "";
    private String modeTripRate = "";
    private String initialTripAdvance = "";
    private String endTripAdvance = "";

    private String[] advanceModeE = null;
    private String[] modeRateE = null;
    private String[] initialAdvanceE = null;
    private String[] endAdvanceE = null;

    private String lhcdocType[] = null;
    private String driverName = "";
    private String driverMobile = "";
    private String docType = "";
    private String lHCNo = "";
    private String lHCId = "";

    private String insurance = "";
    private String permit = "";
    private String fC = "";
    private String license = "";
    private String lhcUsed = "";
    private String lhcNotUsed = "";
    private String eFSId = "";

    private String insuranceDate = "";
    private String roadTax = "";
    private String roadTaxDate = "";
    private String fcDate = "";
    private String permitDate = "";
    private String licenseNo = "";
    private String[] lHCNO = null;

    public String[] getlHCNO() {
        return lHCNO;
    }

    public void setlHCNO(String[] lHCNO) {
        this.lHCNO = lHCNO;
    }

    public String getRoadTax() {
        return roadTax;
    }

    public void setRoadTax(String roadTax) {
        this.roadTax = roadTax;
    }

    public String getInsuranceDate() {
        return insuranceDate;
    }

    public void setInsuranceDate(String insuranceDate) {
        this.insuranceDate = insuranceDate;
    }

    public String getRoadTaxDate() {
        return roadTaxDate;
    }

    public void setRoadTaxDate(String roadTaxDate) {
        this.roadTaxDate = roadTaxDate;
    }

    public String getFcDate() {
        return fcDate;
    }

    public void setFcDate(String fcDate) {
        this.fcDate = fcDate;
    }

    public String getPermitDate() {
        return permitDate;
    }

    public void setPermitDate(String permitDate) {
        this.permitDate = permitDate;
    }

    public String getLicenseNo() {
        return licenseNo;
    }

    public void setLicenseNo(String licenseNo) {
        this.licenseNo = licenseNo;
    }

    public String geteFSId() {
        return eFSId;
    }

    public void seteFSId(String eFSId) {
        this.eFSId = eFSId;
    }

    public String getLhcUsed() {
        return lhcUsed;
    }

    public void setLhcUsed(String lhcUsed) {
        this.lhcUsed = lhcUsed;
    }

    public String getLhcNotUsed() {
        return lhcNotUsed;
    }

    public void setLhcNotUsed(String lhcNotUsed) {
        this.lhcNotUsed = lhcNotUsed;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getPermit() {
        return permit;
    }

    public void setPermit(String permit) {
        this.permit = permit;
    }

    public String getfC() {
        return fC;
    }

    public void setfC(String fC) {
        this.fC = fC;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String[] getLhcdocType() {
        return lhcdocType;
    }

    public void setLhcdocType(String[] lhcdocType) {
        this.lhcdocType = lhcdocType;
    }

    public String getlHCNo() {
        return lHCNo;
    }

    public void setlHCNo(String lHCNo) {
        this.lHCNo = lHCNo;
    }

    public String getlHCId() {
        return lHCId;
    }

    public void setlHCId(String lHCId) {
        this.lHCId = lHCId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverMobile() {
        return driverMobile;
    }

    public void setDriverMobile(String driverMobile) {
        this.driverMobile = driverMobile;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String[] getAdvanceModeE() {
        return advanceModeE;
    }

    public void setAdvanceModeE(String[] advanceModeE) {
        this.advanceModeE = advanceModeE;
    }

    public String[] getModeRateE() {
        return modeRateE;
    }

    public void setModeRateE(String[] modeRateE) {
        this.modeRateE = modeRateE;
    }

    public String[] getInitialAdvanceE() {
        return initialAdvanceE;
    }

    public void setInitialAdvanceE(String[] initialAdvanceE) {
        this.initialAdvanceE = initialAdvanceE;
    }

    public String[] getEndAdvanceE() {
        return endAdvanceE;
    }

    public void setEndAdvanceE(String[] endAdvanceE) {
        this.endAdvanceE = endAdvanceE;
    }

    public String[] getAdvanceMode() {
        return advanceMode;
    }

    public void setAdvanceMode(String[] advanceMode) {
        this.advanceMode = advanceMode;
    }

    public String[] getModeRate() {
        return modeRate;
    }

    public void setModeRate(String[] modeRate) {
        this.modeRate = modeRate;
    }

    public String[] getInitialAdvance() {
        return initialAdvance;
    }

    public void setInitialAdvance(String[] initialAdvance) {
        this.initialAdvance = initialAdvance;
    }

    public String[] getEndAdvance() {
        return endAdvance;
    }

    public void setEndAdvance(String[] endAdvance) {
        this.endAdvance = endAdvance;
    }

    public String getAdvanceTripMode() {
        return advanceTripMode;
    }

    public void setAdvanceTripMode(String advanceTripMode) {
        this.advanceTripMode = advanceTripMode;
    }

    public String getModeTripRate() {
        return modeTripRate;
    }

    public void setModeTripRate(String modeTripRate) {
        this.modeTripRate = modeTripRate;
    }

    public String getInitialTripAdvance() {
        return initialTripAdvance;
    }

    public void setInitialTripAdvance(String initialTripAdvance) {
        this.initialTripAdvance = initialTripAdvance;
    }

    public String getEndTripAdvance() {
        return endTripAdvance;
    }

    public void setEndTripAdvance(String endTripAdvance) {
        this.endTripAdvance = endTripAdvance;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getMfrName() {
        return mfrName;
    }

    public void setMfrName(String mfrName) {
        this.mfrName = mfrName;
    }

    public String getMfrId() {
        return mfrId;
    }

    public void setMfrId(String mfrId) {
        this.mfrId = mfrId;
    }

    public String[] getMfrIds() {
        return mfrIds;
    }

    public void setMfrIds(String[] mfrIds) {
        this.mfrIds = mfrIds;
    }

    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String[] selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public String getMfrNames() {
        return mfrNames;
    }

    public void setMfrNames(String mfrNames) {
        this.mfrNames = mfrNames;
    }

    public String getCreditDays() {
        return creditDays;
    }

    public void setCreditDays(String creditDays) {
        this.creditDays = creditDays;
    }

    public String getVendorTypeId() {
        return vendorTypeId;
    }

    public void setVendorTypeId(String vendorTypeId) {
        this.vendorTypeId = vendorTypeId;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getIsCreditableVendor() {
        return isCreditableVendor;
    }

    public void setIsCreditableVendor(String isCreditableVendor) {
        this.isCreditableVendor = isCreditableVendor;
    }

    public String getVendorAddress() {
        return vendorAddress;
    }

    public void setVendorAddress(String vendorAddress) {
        this.vendorAddress = vendorAddress;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorMailId() {
        return vendorMailId;
    }

    public void setVendorMailId(String vendorMailId) {
        this.vendorMailId = vendorMailId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorPhoneNo() {
        return vendorPhoneNo;
    }

    public void setVendorPhoneNo(String vendorPhoneNo) {
        this.vendorPhoneNo = vendorPhoneNo;
    }

    public String getVendorTypeValue() {
        return vendorTypeValue;
    }

    public void setVendorTypeValue(String vendorTypeValue) {
        this.vendorTypeValue = vendorTypeValue;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getTinNo() {
        return tinNo;
    }

    public void setTinNo(String tinNo) {
        this.tinNo = tinNo;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public String getSettlementType() {
        return settlementType;
    }

    public void setSettlementType(String settlementType) {
        this.settlementType = settlementType;
    }

    public String getVehicleType() {
        return VehicleType;
    }

    public void setVehicleType(String VehicleType) {
        this.VehicleType = VehicleType;
    }

    public String getContractTypeId() {
        return contractTypeId;
    }

    public void setContractTypeId(String contractTypeId) {
        this.contractTypeId = contractTypeId;
    }

    public String getDriverReponsibility() {
        return driverReponsibility;
    }

    public void setDriverReponsibility(String driverReponsibility) {
        this.driverReponsibility = driverReponsibility;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getFixedAmount() {
        return fixedAmount;
    }

    public void setFixedAmount(String fixedAmount) {
        this.fixedAmount = fixedAmount;
    }

    public String getFixedKm() {
        return fixedKm;
    }

    public void setFixedKm(String fixedKm) {
        this.fixedKm = fixedKm;
    }

    public String getFuleExpenseby() {
        return fuleExpenseby;
    }

    public void setFuleExpenseby(String fuleExpenseby) {
        this.fuleExpenseby = fuleExpenseby;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getPaymentScheduleDays() {
        return paymentScheduleDays;
    }

    public void setPaymentScheduleDays(String paymentScheduleDays) {
        this.paymentScheduleDays = paymentScheduleDays;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getRateExtraKm() {
        return rateExtraKm;
    }

    public void setRateExtraKm(String rateExtraKm) {
        this.rateExtraKm = rateExtraKm;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getVendorContractStatus() {
        return vendorContractStatus;
    }

    public void setVendorContractStatus(String vendorContractStatus) {
        this.vendorContractStatus = vendorContractStatus;
    }

    public String getRatePerKm() {
        return ratePerKm;
    }

    public void setRatePerKm(String ratePerKm) {
        this.ratePerKm = ratePerKm;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getDriverResponsibility() {
        return driverResponsibility;
    }

    public void setDriverResponsibility(String driverResponsibility) {
        this.driverResponsibility = driverResponsibility;
    }

    public String getFuelExpenseResponsibility() {
        return fuelExpenseResponsibility;
    }

    public void setFuelExpenseResponsibility(String fuelExpenseResponsibility) {
        this.fuelExpenseResponsibility = fuelExpenseResponsibility;
    }

    public String getRatePerExtraKm() {
        return ratePerExtraKm;
    }

    public void setRatePerExtraKm(String ratePerExtraKm) {
        this.ratePerExtraKm = ratePerExtraKm;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateWithReeferFullTruck() {
        return dateWithReeferFullTruck;
    }

    public void setDateWithReeferFullTruck(String dateWithReeferFullTruck) {
        this.dateWithReeferFullTruck = dateWithReeferFullTruck;
    }

    public String getDateWithoutReeferFullTruck() {
        return dateWithoutReeferFullTruck;
    }

    public void setDateWithoutReeferFullTruck(String dateWithoutReeferFullTruck) {
        this.dateWithoutReeferFullTruck = dateWithoutReeferFullTruck;
    }

    public String getDestinationIdFullTruck() {
        return destinationIdFullTruck;
    }

    public void setDestinationIdFullTruck(String destinationIdFullTruck) {
        this.destinationIdFullTruck = destinationIdFullTruck;
    }

    public String getDestinationNameFullTruck() {
        return destinationNameFullTruck;
    }

    public void setDestinationNameFullTruck(String destinationNameFullTruck) {
        this.destinationNameFullTruck = destinationNameFullTruck;
    }

    public String getOriginIdFullTruck() {
        return originIdFullTruck;
    }

    public void setOriginIdFullTruck(String originIdFullTruck) {
        this.originIdFullTruck = originIdFullTruck;
    }

    public String getOriginNameFullTruck() {
        return originNameFullTruck;
    }

    public void setOriginNameFullTruck(String originNameFullTruck) {
        this.originNameFullTruck = originNameFullTruck;
    }

    public String getRouteIdFullTruck() {
        return routeIdFullTruck;
    }

    public void setRouteIdFullTruck(String routeIdFullTruck) {
        this.routeIdFullTruck = routeIdFullTruck;
    }

    public String getTravelHourFullTruck() {
        return travelHourFullTruck;
    }

    public void setTravelHourFullTruck(String travelHourFullTruck) {
        this.travelHourFullTruck = travelHourFullTruck;
    }

    public String getTravelKmFullTruck() {
        return travelKmFullTruck;
    }

    public void setTravelKmFullTruck(String travelKmFullTruck) {
        this.travelKmFullTruck = travelKmFullTruck;
    }

    public String getTravelMinuteFullTruck() {
        return travelMinuteFullTruck;
    }

    public void setTravelMinuteFullTruck(String travelMinuteFullTruck) {
        this.travelMinuteFullTruck = travelMinuteFullTruck;
    }

    public String getVehicleIdFullTruck() {
        return vehicleIdFullTruck;
    }

    public void setVehicleIdFullTruck(String vehicleIdFullTruck) {
        this.vehicleIdFullTruck = vehicleIdFullTruck;
    }

    public String getAdditionalCost() {
        return additionalCost;
    }

    public void setAdditionalCost(String additionalCost) {
        this.additionalCost = additionalCost;
    }

    public String getSpotCost() {
        return spotCost;
    }

    public void setSpotCost(String spotCost) {
        this.spotCost = spotCost;
    }

    public String gettType() {
        return tType;
    }

    public void settType(String tType) {
        this.tType = tType;
    }

    public String getTrailerType() {
        return trailerType;
    }

    public void setTrailerType(String trailerType) {
        this.trailerType = trailerType;
    }

    public String getVehicleUnits() {
        return vehicleUnits;
    }

    public void setVehicleUnits(String vehicleUnits) {
        this.vehicleUnits = vehicleUnits;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getaCost() {
        return aCost;
    }

    public void setaCost(String aCost) {
        this.aCost = aCost;
    }

    public String getfCost() {
        return fCost;
    }

    public void setfCost(String fCost) {
        this.fCost = fCost;
    }

    public String getmAllow() {
        return mAllow;
    }

    public void setmAllow(String mAllow) {
        this.mAllow = mAllow;
    }

    public String getoTime() {
        return oTime;
    }

    public void setoTime(String oTime) {
        this.oTime = oTime;
    }

    public String getrLimit() {
        return rLimit;
    }

    public void setrLimit(String rLimit) {
        this.rLimit = rLimit;
    }

    public String getcCategory() {
        return cCategory;
    }

    public void setcCategory(String cCategory) {
        this.cCategory = cCategory;
    }

    public String gettFixed() {
        return tFixed;
    }

    public void settFixed(String tFixed) {
        this.tFixed = tFixed;
    }

    public String gettUnits() {
        return tUnits;
    }

    public void settUnits(String tUnits) {
        this.tUnits = tUnits;
    }

    public String getTrailType() {
        return trailType;
    }

    public void setTrailType(String trailType) {
        this.trailType = trailType;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getvCost() {
        return vCost;
    }

    public void setvCost(String vCost) {
        this.vCost = vCost;
    }

    public String getVehicleTypeIddeD() {
        return vehicleTypeIddeD;
    }

    public void setVehicleTypeIddeD(String vehicleTypeIddeD) {
        this.vehicleTypeIddeD = vehicleTypeIddeD;
    }

    public String getAgreedDate() {
        return agreedDate;
    }

    public void setAgreedDate(String agreedDate) {
        this.agreedDate = agreedDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTrailerNo() {
        return trailerNo;
    }

    public void setTrailerNo(String trailerNo) {
        this.trailerNo = trailerNo;
    }

    public String getTrailerRemarks() {
        return trailerRemarks;
    }

    public void setTrailerRemarks(String trailerRemarks) {
        this.trailerRemarks = trailerRemarks;
    }

    public String getVehicleRegNo() {
        return vehicleRegNo;
    }

    public void setVehicleRegNo(String vehicleRegNo) {
        this.vehicleRegNo = vehicleRegNo;
    }

    public String getTrailerType1() {
        return trailerType1;
    }

    public void setTrailerType1(String trailerType1) {
        this.trailerType1 = trailerType1;
    }

//    public String getAeddeD() {
//        return aeddeD;
//    }
//
//    public void setAeddeD(String aeddeD) {
//        this.aeddeD = aeddeD;
//    }
//    public String getFueldeD() {
//        return fueldeD;
//    }
//
//    public void setFueldeD(String fueldeD) {
//        this.fueldeD = fueldeD;
//    }
    public String getUomdeD() {
        return uomdeD;
    }

    public void setUomdeD(String uomdeD) {
        this.uomdeD = uomdeD;
    }

    public String getfHrs() {
        return fHrs;
    }

    public void setfHrs(String fHrs) {
        this.fHrs = fHrs;
    }

    public String getfMin() {
        return fMin;
    }

    public void setfMin(String fMin) {
        this.fMin = fMin;
    }

    public String getAeddeD() {
        return aeddeD;
    }

    public void setAeddeD(String aeddeD) {
        this.aeddeD = aeddeD;
    }

    public String getFueldeD() {
        return fueldeD;
    }

    public void setFueldeD(String fueldeD) {
        this.fueldeD = fueldeD;
    }

    public String getEndDateold() {
        return endDateold;
    }

    public void setEndDateold(String endDateold) {
        this.endDateold = endDateold;
    }

    public int getContrctId() {
        return contrctId;
    }

    public void setContrctId(int contrctId) {
        this.contrctId = contrctId;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getAgreedFuelPrice() {
        return agreedFuelPrice;
    }

    public void setAgreedFuelPrice(String agreedFuelPrice) {
        this.agreedFuelPrice = agreedFuelPrice;
    }

    public String getHikeFuelPrice() {
        return hikeFuelPrice;
    }

    public void setHikeFuelPrice(String hikeFuelPrice) {
        this.hikeFuelPrice = hikeFuelPrice;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getContractCategory() {
        return contractCategory;
    }

    public void setContractCategory(String contractCategory) {
        this.contractCategory = contractCategory;
    }

    public String getFixedCost() {
        return fixedCost;
    }

    public void setFixedCost(String fixedCost) {
        this.fixedCost = fixedCost;
    }

    public String getFixedHrs() {
        return fixedHrs;
    }

    public void setFixedHrs(String fixedHrs) {
        this.fixedHrs = fixedHrs;
    }

    public String getFixedMin() {
        return fixedMin;
    }

    public void setFixedMin(String fixedMin) {
        this.fixedMin = fixedMin;
    }

    public String getHolidays() {
        return holidays;
    }

    public void setHolidays(String holidays) {
        this.holidays = holidays;
    }

    public String getMaxAllowableKM() {
        return maxAllowableKM;
    }

    public void setMaxAllowableKM(String maxAllowableKM) {
        this.maxAllowableKM = maxAllowableKM;
    }

    public String getRateCost() {
        return rateCost;
    }

    public void setRateCost(String rateCost) {
        this.rateCost = rateCost;
    }

    public String getRateLimit() {
        return rateLimit;
    }

    public void setRateLimit(String rateLimit) {
        this.rateLimit = rateLimit;
    }

    public String getTotalFixedCost() {
        return totalFixedCost;
    }

    public void setTotalFixedCost(String totalFixedCost) {
        this.totalFixedCost = totalFixedCost;
    }

    public String getTrailorTypeDedicate() {
        return trailorTypeDedicate;
    }

    public void setTrailorTypeDedicate(String trailorTypeDedicate) {
        this.trailorTypeDedicate = trailorTypeDedicate;
    }

    public String getTrailorUnitsDedicate() {
        return trailorUnitsDedicate;
    }

    public void setTrailorUnitsDedicate(String trailorUnitsDedicate) {
        this.trailorUnitsDedicate = trailorUnitsDedicate;
    }

    public String getVehicleTypeIdDedicate() {
        return vehicleTypeIdDedicate;
    }

    public void setVehicleTypeIdDedicate(String vehicleTypeIdDedicate) {
        this.vehicleTypeIdDedicate = vehicleTypeIdDedicate;
    }

    public String getVehicleUnitsDedicate() {
        return vehicleUnitsDedicate;
    }

    public void setVehicleUnitsDedicate(String vehicleUnitsDedicate) {
        this.vehicleUnitsDedicate = vehicleUnitsDedicate;
    }

    public String getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(String workingDays) {
        this.workingDays = workingDays;
    }

    public String getTrailorTypeUnits() {
        return trailorTypeUnits;
    }

    public void setTrailorTypeUnits(String trailorTypeUnits) {
        this.trailorTypeUnits = trailorTypeUnits;
    }

    public String getAddCostDedicate() {
        return addCostDedicate;
    }

    public void setAddCostDedicate(String addCostDedicate) {
        this.addCostDedicate = addCostDedicate;
    }

    public String getContractDedicateId() {
        return contractDedicateId;
    }

    public void setContractDedicateId(String contractDedicateId) {
        this.contractDedicateId = contractDedicateId;
    }

    public String getContractHireId() {
        return contractHireId;
    }

    public void setContractHireId(String contractHireId) {
        this.contractHireId = contractHireId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getMfr() {
        return mfr;
    }

    public void setMfr(String mfr) {
        this.mfr = mfr;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getExistingVehicleUnit() {
        return existingVehicleUnit;
    }

    public void setExistingVehicleUnit(String existingVehicleUnit) {
        this.existingVehicleUnit = existingVehicleUnit;
    }

    public String getTrailerUnits() {
        return trailerUnits;
    }

    public void setTrailerUnits(String trailerUnits) {
        this.trailerUnits = trailerUnits;
    }

    public String getTrailorTypeIdDedicate() {
        return trailorTypeIdDedicate;
    }

    public void setTrailorTypeIdDedicate(String trailorTypeIdDedicate) {
        this.trailorTypeIdDedicate = trailorTypeIdDedicate;
    }

    public String getTrailorTypeId() {
        return trailorTypeId;
    }

    public void setTrailorTypeId(String trailorTypeId) {
        this.trailorTypeId = trailorTypeId;
    }

    public String getTrailerTypeAxles() {
        return trailerTypeAxles;
    }

    public void setTrailerTypeAxles(String trailerTypeAxles) {
        this.trailerTypeAxles = trailerTypeAxles;
    }

    public String getFuelHikePercentage() {
        return fuelHikePercentage;
    }

    public void setFuelHikePercentage(String fuelHikePercentage) {
        this.fuelHikePercentage = fuelHikePercentage;
    }

    public String[] getContractId1() {
        return contractId1;
    }

    public void setContractId1(String[] contractId1) {
        this.contractId1 = contractId1;
    }

    public String getTrailerTypeId() {
        return trailerTypeId;
    }

    public void setTrailerTypeId(String trailerTypeId) {
        this.trailerTypeId = trailerTypeId;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getTeleNo() {
        return teleNo;
    }

    public void setTeleNo(String teleNo) {
        this.teleNo = teleNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getFaxNo() {
        return faxNo;
    }

    public void setFaxNo(String faxNo) {
        this.faxNo = faxNo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getMicrNo() {
        return micrNo;
    }

    public void setMicrNo(String micrNo) {
        this.micrNo = micrNo;
    }

    public String getMsmeId() {
        return msmeId;
    }

    public void setMsmeId(String msmeId) {
        this.msmeId = msmeId;
    }

    public String getNstcId() {
        return nstcId;
    }

    public void setNstcId(String nstcId) {
        this.nstcId = nstcId;
    }

    public String getGstId() {
        return gstId;
    }

    public void setGstId(String gstId) {
        this.gstId = gstId;
    }

    public String getDgsdId() {
        return dgsdId;
    }

    public void setDgsdId(String dgsdId) {
        this.dgsdId = dgsdId;
    }

    public String getRocId() {
        return rocId;
    }

    public void setRocId(String rocId) {
        this.rocId = rocId;
    }

    public String getEepcId() {
        return eepcId;
    }

    public void setEepcId(String eepcId) {
        this.eepcId = eepcId;
    }

    public String getSerViceTax() {
        return serViceTax;
    }

    public void setSerViceTax(String serViceTax) {
        this.serViceTax = serViceTax;
    }

    public String getEcId() {
        return ecId;
    }

    public void setEcId(String ecId) {
        this.ecId = ecId;
    }

    public String getExciseDuty() {
        return exciseDuty;
    }

    public void setExciseDuty(String exciseDuty) {
        this.exciseDuty = exciseDuty;
    }

    public String getVatId() {
        return vatId;
    }

    public void setVatId(String vatId) {
        this.vatId = vatId;
    }

    public String getCstId() {
        return cstId;
    }

    public void setCstId(String cstId) {
        this.cstId = cstId;
    }

    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName1() {
        return fileName1;
    }

    public void setFileName1(String fileName1) {
        this.fileName1 = fileName1;
    }

    public String getFileName2() {
        return fileName2;
    }

    public void setFileName2(String fileName2) {
        this.fileName2 = fileName2;
    }

    public String getFileName3() {
        return fileName3;
    }

    public void setFileName3(String fileName3) {
        this.fileName3 = fileName3;
    }

    public String getFileName4() {
        return fileName4;
    }

    public void setFileName4(String fileName4) {
        this.fileName4 = fileName4;
    }

    public String getLoadTypeId() {
        return loadTypeId;
    }

    public void setLoadTypeId(String loadTypeId) {
        this.loadTypeId = loadTypeId;
    }

    public String getContainerTypeId() {
        return containerTypeId;
    }

    public void setContainerTypeId(String containerTypeId) {
        this.containerTypeId = containerTypeId;
    }

    public String getContainerQty() {
        return containerQty;
    }

    public void setContainerQty(String containerQty) {
        this.containerQty = containerQty;
    }

    public String getNoOfTrips() {
        return noOfTrips;
    }

    public void setNoOfTrips(String noOfTrips) {
        this.noOfTrips = noOfTrips;
    }

    public String getExtraKm() {
        return extraKm;
    }

    public void setExtraKm(String extraKm) {
        this.extraKm = extraKm;
    }

    public String getOtherVendorTypeIds() {
        return otherVendorTypeIds;
    }

    public void setOtherVendorTypeIds(String otherVendorTypeIds) {
        this.otherVendorTypeIds = otherVendorTypeIds;
    }

    public int getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(int approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getRequestBy() {
        return requestBy;
    }

    public void setRequestBy(String requestBy) {
        this.requestBy = requestBy;
    }

    public String getRequestOn() {
        return requestOn;
    }

    public void setRequestOn(String requestOn) {
        this.requestOn = requestOn;
    }

    public String getRequestRemarks() {
        return requestRemarks;
    }

    public void setRequestRemarks(String requestRemarks) {
        this.requestRemarks = requestRemarks;
    }

    public String getApprovalBy() {
        return approvalBy;
    }

    public void setApprovalBy(String approvalBy) {
        this.approvalBy = approvalBy;
    }

    public String getApprovalOn() {
        return approvalOn;
    }

    public void setApprovalOn(String approvalOn) {
        this.approvalOn = approvalOn;
    }

    public String getApprovalRemarks() {
        return approvalRemarks;
    }

    public void setApprovalRemarks(String approvalRemarks) {
        this.approvalRemarks = approvalRemarks;
    }

    public String[] getContractHireIds() {
        return contractHireIds;
    }

    public void setContractHireIds(String[] contractHireIds) {
        this.contractHireIds = contractHireIds;
    }

    public String[] getSpotCostE() {
        return spotCostE;
    }

    public void setSpotCostE(String[] spotCostE) {
        this.spotCostE = spotCostE;
    }

    public String[] getAdditionalCostE() {
        return additionalCostE;
    }

    public void setAdditionalCostE(String[] additionalCostE) {
        this.additionalCostE = additionalCostE;
    }

    public String[] getActiveIndD() {
        return activeIndD;
    }

    public void setActiveIndD(String[] activeIndD) {
        this.activeIndD = activeIndD;
    }

    public String[] getFixedCostE() {
        return fixedCostE;
    }

    public void setFixedCostE(String[] fixedCostE) {
        this.fixedCostE = fixedCostE;
    }

    public String[] getFixedHrsE() {
        return fixedHrsE;
    }

    public void setFixedHrsE(String[] fixedHrsE) {
        this.fixedHrsE = fixedHrsE;
    }

    public String[] getFixedMinE() {
        return fixedMinE;
    }

    public void setFixedMinE(String[] fixedMinE) {
        this.fixedMinE = fixedMinE;
    }

    public String[] getTotalFixedCostE() {
        return totalFixedCostE;
    }

    public void setTotalFixedCostE(String[] totalFixedCostE) {
        this.totalFixedCostE = totalFixedCostE;
    }

    public String[] getRateCostE() {
        return rateCostE;
    }

    public void setRateCostE(String[] rateCostE) {
        this.rateCostE = rateCostE;
    }

    public String[] getRateLimitE() {
        return rateLimitE;
    }

    public void setRateLimitE(String[] rateLimitE) {
        this.rateLimitE = rateLimitE;
    }

    public String[] getWorkingDaysE() {
        return workingDaysE;
    }

    public void setWorkingDaysE(String[] workingDaysE) {
        this.workingDaysE = workingDaysE;
    }

    public String[] getHolidaysE() {
        return holidaysE;
    }

    public void setHolidaysE(String[] holidaysE) {
        this.holidaysE = holidaysE;
    }

    public String[] getAddCostDedicateE() {
        return addCostDedicateE;
    }

    public void setAddCostDedicateE(String[] addCostDedicateE) {
        this.addCostDedicateE = addCostDedicateE;
    }

    public String[] getTrailorTypeUnitss() {
        return trailorTypeUnitss;
    }

    public void setTrailorTypeUnitss(String[] trailorTypeUnitss) {
        this.trailorTypeUnitss = trailorTypeUnitss;
    }

    public String[] getTrailerTypes() {
        return trailerTypes;
    }

    public void setTrailerTypes(String[] trailerTypes) {
        this.trailerTypes = trailerTypes;
    }

    public String[] getActiveIndE() {
        return activeIndE;
    }

    public void setActiveIndE(String[] activeIndE) {
        this.activeIndE = activeIndE;
    }

    public String[] getContractDedicateIds() {
        return contractDedicateIds;
    }

    public void setContractDedicateIds(String[] contractDedicateIds) {
        this.contractDedicateIds = contractDedicateIds;
    }

    public String[] getVehicleTypeIdDedicates() {
        return vehicleTypeIdDedicates;
    }

    public void setVehicleTypeIdDedicates(String[] vehicleTypeIdDedicates) {
        this.vehicleTypeIdDedicates = vehicleTypeIdDedicates;
    }

    public String[] getVehicleUnitsDedicates() {
        return vehicleUnitsDedicates;
    }

    public void setVehicleUnitsDedicates(String[] vehicleUnitsDedicates) {
        this.vehicleUnitsDedicates = vehicleUnitsDedicates;
    }

    public String[] getContractCategorys() {
        return contractCategorys;
    }

    public void setContractCategorys(String[] contractCategorys) {
        this.contractCategorys = contractCategorys;
    }

    public String[] getFixedCosts() {
        return fixedCosts;
    }

    public void setFixedCosts(String[] fixedCosts) {
        this.fixedCosts = fixedCosts;
    }

    public String[] getFixedHrss() {
        return fixedHrss;
    }

    public void setFixedHrss(String[] fixedHrss) {
        this.fixedHrss = fixedHrss;
    }

    public String[] getFixedMins() {
        return fixedMins;
    }

    public void setFixedMins(String[] fixedMins) {
        this.fixedMins = fixedMins;
    }

    public String[] getTotalFixedCosts() {
        return totalFixedCosts;
    }

    public void setTotalFixedCosts(String[] totalFixedCosts) {
        this.totalFixedCosts = totalFixedCosts;
    }

    public String[] getRateCosts() {
        return rateCosts;
    }

    public void setRateCosts(String[] rateCosts) {
        this.rateCosts = rateCosts;
    }

    public String[] getRateLimits() {
        return rateLimits;
    }

    public void setRateLimits(String[] rateLimits) {
        this.rateLimits = rateLimits;
    }

    public String[] getMaxAllowableKMs() {
        return maxAllowableKMs;
    }

    public void setMaxAllowableKMs(String[] maxAllowableKMs) {
        this.maxAllowableKMs = maxAllowableKMs;
    }

    public String[] getWorkingDayss() {
        return workingDayss;
    }

    public void setWorkingDayss(String[] workingDayss) {
        this.workingDayss = workingDayss;
    }

    public String[] getHolidayss() {
        return holidayss;
    }

    public void setHolidayss(String[] holidayss) {
        this.holidayss = holidayss;
    }

    public String[] getAddCostDedicates() {
        return addCostDedicates;
    }

    public void setAddCostDedicates(String[] addCostDedicates) {
        this.addCostDedicates = addCostDedicates;
    }

    public String[] getiCnt() {
        return iCnt;
    }

    public void setiCnt(String[] iCnt) {
        this.iCnt = iCnt;
    }

    public String[] getiCnt1() {
        return iCnt1;
    }

    public void setiCnt1(String[] iCnt1) {
        this.iCnt1 = iCnt1;
    }

    public String[] getVehicleTypeIds() {
        return vehicleTypeIds;
    }

    public void setVehicleTypeIds(String[] vehicleTypeIds) {
        this.vehicleTypeIds = vehicleTypeIds;
    }

    public String[] getVehicleUnitss() {
        return vehicleUnitss;
    }

    public void setVehicleUnitss(String[] vehicleUnitss) {
        this.vehicleUnitss = vehicleUnitss;
    }

    public String[] getSpotCosts() {
        return spotCosts;
    }

    public void setSpotCosts(String[] spotCosts) {
        this.spotCosts = spotCosts;
    }

    public String[] getAdditionalCosts() {
        return additionalCosts;
    }

    public void setAdditionalCosts(String[] additionalCosts) {
        this.additionalCosts = additionalCosts;
    }

    public String[] getMarketRate() {
        return marketRate;
    }

    public void setMarketRate(String[] marketRate) {
        this.marketRate = marketRate;
    }

    public String[] getLoadTypeIds() {
        return loadTypeIds;
    }

    public void setLoadTypeIds(String[] loadTypeIds) {
        this.loadTypeIds = loadTypeIds;
    }

    public String[] getContainerTypeIds() {
        return containerTypeIds;
    }

    public void setContainerTypeIds(String[] containerTypeIds) {
        this.containerTypeIds = containerTypeIds;
    }

    public String[] getContainerQtys() {
        return containerQtys;
    }

    public void setContainerQtys(String[] containerQtys) {
        this.containerQtys = containerQtys;
    }

    public String[] getOriginIdFullTrucks() {
        return originIdFullTrucks;
    }

    public void setOriginIdFullTrucks(String[] originIdFullTrucks) {
        this.originIdFullTrucks = originIdFullTrucks;
    }

    public String[] getOriginNameFullTrucks() {
        return originNameFullTrucks;
    }

    public void setOriginNameFullTrucks(String[] originNameFullTrucks) {
        this.originNameFullTrucks = originNameFullTrucks;
    }

    public String[] getDestinationIdFullTrucks() {
        return destinationIdFullTrucks;
    }

    public void setDestinationIdFullTrucks(String[] destinationIdFullTrucks) {
        this.destinationIdFullTrucks = destinationIdFullTrucks;
    }

    public String[] getDestinationNameFullTrucks() {
        return destinationNameFullTrucks;
    }

    public void setDestinationNameFullTrucks(String[] destinationNameFullTrucks) {
        this.destinationNameFullTrucks = destinationNameFullTrucks;
    }

    public String[] getTravelKmFullTrucks() {
        return travelKmFullTrucks;
    }

    public void setTravelKmFullTrucks(String[] travelKmFullTrucks) {
        this.travelKmFullTrucks = travelKmFullTrucks;
    }

    public String[] getTravelHourFullTrucks() {
        return travelHourFullTrucks;
    }

    public void setTravelHourFullTrucks(String[] travelHourFullTrucks) {
        this.travelHourFullTrucks = travelHourFullTrucks;
    }

    public String[] getTravelMinuteFullTrucks() {
        return travelMinuteFullTrucks;
    }

    public void setTravelMinuteFullTrucks(String[] travelMinuteFullTrucks) {
        this.travelMinuteFullTrucks = travelMinuteFullTrucks;
    }

    public String[] getPointsId1() {
        return pointsId1;
    }

    public void setPointsId1(String[] pointsId1) {
        this.pointsId1 = pointsId1;
    }

    public String[] getPointsId2() {
        return pointsId2;
    }

    public void setPointsId2(String[] pointsId2) {
        this.pointsId2 = pointsId2;
    }

    public String[] getPointsId3() {
        return pointsId3;
    }

    public void setPointsId3(String[] pointsId3) {
        this.pointsId3 = pointsId3;
    }

    public String[] getPointsId4() {
        return pointsId4;
    }

    public void setPointsId4(String[] pointsId4) {
        this.pointsId4 = pointsId4;
    }

    public String getPointId1() {
        return pointId1;
    }

    public void setPointId1(String pointId1) {
        this.pointId1 = pointId1;
    }

    public String getPointId2() {
        return pointId2;
    }

    public void setPointId2(String pointId2) {
        this.pointId2 = pointId2;
    }

    public String getPointId3() {
        return pointId3;
    }

    public void setPointId3(String pointId3) {
        this.pointId3 = pointId3;
    }

    public String getPointId4() {
        return pointId4;
    }

    public void setPointId4(String pointId4) {
        this.pointId4 = pointId4;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getVirId() {
        return virId;
    }

    public void setVirId(String virId) {
        this.virId = virId;
    }

    public String getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(String invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getContainer() {
        return container;
    }

    public void setContainer(String container) {
        this.container = container;
    }

    public String getExpense() {
        return expense;
    }

    public void setExpense(String expense) {
        this.expense = expense;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getContainerType() {
        return containerType;
    }

    public void setContainerType(String containerType) {
        this.containerType = containerType;
    }

    public String getContainerQuty() {
        return containerQuty;
    }

    public void setContainerQuty(String containerQuty) {
        this.containerQuty = containerQuty;
    }

    public String getPoint1Name() {
        return point1Name;
    }

    public void setPoint1Name(String point1Name) {
        this.point1Name = point1Name;
    }

    public String getPoint2Name() {
        return point2Name;
    }

    public void setPoint2Name(String point2Name) {
        this.point2Name = point2Name;
    }

    public String getPoint3Name() {
        return point3Name;
    }

    public void setPoint3Name(String point3Name) {
        this.point3Name = point3Name;
    }

    public String getPoint4Name() {
        return point4Name;
    }

    public void setPoint4Name(String point4Name) {
        this.point4Name = point4Name;
    }

    public String getTotRunKM() {
        return totRunKM;
    }

    public void setTotRunKM(String totRunKM) {
        this.totRunKM = totRunKM;
    }

    public String getAllowedKM() {
        return allowedKM;
    }

    public void setAllowedKM(String allowedKM) {
        this.allowedKM = allowedKM;
    }

    public String getTotExtraRunKm() {
        return totExtraRunKm;
    }

    public void setTotExtraRunKm(String totExtraRunKm) {
        this.totExtraRunKm = totExtraRunKm;
    }

    public String getTotExtraKMRate() {
        return totExtraKMRate;
    }

    public void setTotExtraKMRate(String totExtraKMRate) {
        this.totExtraKMRate = totExtraKMRate;
    }

    public String getActualRatePerKM() {
        return actualRatePerKM;
    }

    public void setActualRatePerKM(String actualRatePerKM) {
        this.actualRatePerKM = actualRatePerKM;
    }

    public String getContainerNo() {
        return containerNo;
    }

    public void setContainerNo(String containerNo) {
        this.containerNo = containerNo;
    }

    public String getOutDate() {
        return outDate;
    }

    public void setOutDate(String outDate) {
        this.outDate = outDate;
    }

    public String getConfigureId() {
        return configureId;
    }

    public void setConfigureId(String configureId) {
        this.configureId = configureId;
    }

    public String getTripCode() {
        return tripCode;
    }

    public void setTripCode(String tripCode) {
        this.tripCode = tripCode;
    }

    public String[] getOriginNameFullTruckE() {
        return originNameFullTruckE;
    }

    public void setOriginNameFullTruckE(String[] originNameFullTruckE) {
        this.originNameFullTruckE = originNameFullTruckE;
    }

    public String[] getDestinationNameFullTruckE() {
        return destinationNameFullTruckE;
    }

    public void setDestinationNameFullTruckE(String[] destinationNameFullTruckE) {
        this.destinationNameFullTruckE = destinationNameFullTruckE;
    }

    public String[] getPointName1E() {
        return pointName1E;
    }

    public void setPointName1E(String[] pointName1E) {
        this.pointName1E = pointName1E;
    }

    public String[] getPointName2E() {
        return pointName2E;
    }

    public void setPointName2E(String[] pointName2E) {
        this.pointName2E = pointName2E;
    }

    public String[] getPointName3E() {
        return pointName3E;
    }

    public void setPointName3E(String[] pointName3E) {
        this.pointName3E = pointName3E;
    }

    public String[] getPointName4E() {
        return pointName4E;
    }

    public void setPointName4E(String[] pointName4E) {
        this.pointName4E = pointName4E;
    }

    public String[] getApprovalStatusE() {
        return approvalStatusE;
    }

    public void setApprovalStatusE(String[] approvalStatusE) {
        this.approvalStatusE = approvalStatusE;
    }

    public String[] getLoadTypeE() {
        return loadTypeE;
    }

    public void setLoadTypeE(String[] loadTypeE) {
        this.loadTypeE = loadTypeE;
    }

    public String[] getVehicleTypeE() {
        return vehicleTypeE;
    }

    public void setVehicleTypeE(String[] vehicleTypeE) {
        this.vehicleTypeE = vehicleTypeE;
    }

    public String[] getPointName1A() {
        return pointName1A;
    }

    public void setPointName1A(String[] pointName1A) {
        this.pointName1A = pointName1A;
    }

    public String[] getPointName2A() {
        return pointName2A;
    }

    public void setPointName2A(String[] pointName2A) {
        this.pointName2A = pointName2A;
    }

    public String[] getPointName3A() {
        return pointName3A;
    }

    public void setPointName3A(String[] pointName3A) {
        this.pointName3A = pointName3A;
    }

    public String[] getPointName4A() {
        return pointName4A;
    }

    public void setPointName4A(String[] pointName4A) {
        this.pointName4A = pointName4A;
    }

    public String[] getContainerTypeIdE1() {
        return containerTypeIdE1;
    }

    public void setContainerTypeIdE1(String[] containerTypeIdE1) {
        this.containerTypeIdE1 = containerTypeIdE1;
    }

    public String[] getContainerQtyE1() {
        return containerQtyE1;
    }

    public void setContainerQtyE1(String[] containerQtyE1) {
        this.containerQtyE1 = containerQtyE1;
    }

    public String[] getApprovalStatusA() {
        return approvalStatusA;
    }

    public void setApprovalStatusA(String[] approvalStatusA) {
        this.approvalStatusA = approvalStatusA;
    }

    public String[] getVehicleTypeA() {
        return vehicleTypeA;
    }

    public void setVehicleTypeA(String[] vehicleTypeA) {
        this.vehicleTypeA = vehicleTypeA;
    }

    public String[] getLoadTypeA() {
        return loadTypeA;
    }

    public void setLoadTypeA(String[] loadTypeA) {
        this.loadTypeA = loadTypeA;
    }

    public String[] getContainerTypeA() {
        return containerTypeA;
    }

    public void setContainerTypeA(String[] containerTypeA) {
        this.containerTypeA = containerTypeA;
    }

    public String[] getContainerQtyA() {
        return containerQtyA;
    }

    public void setContainerQtyA(String[] containerQtyA) {
        this.containerQtyA = containerQtyA;
    }

    public String getContractRateId() {
        return contractRateId;
    }

    public void setContractRateId(String contractRateId) {
        this.contractRateId = contractRateId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String[] getToDates() {
        return toDates;
    }

    public void setToDates(String[] toDates) {
        this.toDates = toDates;
    }

    public String[] getFromDates() {
        return fromDates;
    }

    public void setFromDates(String[] fromDates) {
        this.fromDates = fromDates;
    }

    public String[] getToDateA() {
        return toDateA;
    }

    public void setToDateA(String[] toDateA) {
        this.toDateA = toDateA;
    }

    public String[] getFromDateA() {
        return fromDateA;
    }

    public void setFromDateA(String[] fromDateA) {
        this.fromDateA = fromDateA;
    }

    public String[] getFromDateE() {
        return fromDateE;
    }

    public void setFromDateE(String[] fromDateE) {
        this.fromDateE = fromDateE;
    }

    public String[] getToDateE() {
        return toDateE;
    }

    public void setToDateE(String[] toDateE) {
        this.toDateE = toDateE;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    public String getToId() {
        return toId;
    }

    public void setToId(String toId) {
        this.toId = toId;
    }

    public String getBillingTypeId() {
        return billingTypeId;
    }

    public void setBillingTypeId(String billingTypeId) {
        this.billingTypeId = billingTypeId;
    }

    public String getPenality() {
        return penality;
    }

    public void setPenality(String penality) {
        this.penality = penality;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDcmremarks() {
        return dcmremarks;
    }

    public void setDcmremarks(String dcmremarks) {
        this.dcmremarks = dcmremarks;
    }

    public String getDcmunit() {
        return dcmunit;
    }

    public void setDcmunit(String dcmunit) {
        this.dcmunit = dcmunit;
    }

    public String getChargeamt() {
        return chargeamt;
    }

    public void setChargeamt(String chargeamt) {
        this.chargeamt = chargeamt;
    }

    public String getDetention() {
        return detention;
    }

    public void setDetention(String detention) {
        this.detention = detention;
    }

    public String getDcmunitTo() {
        return dcmunitTo;
    }

    public void setDcmunitTo(String dcmunitTo) {
        this.dcmunitTo = dcmunitTo;
    }

    public String getChargeamount() {
        return chargeamount;
    }

    public void setChargeamount(String chargeamount) {
        this.chargeamount = chargeamount;
    }

    public String getPcmunit() {
        return pcmunit;
    }

    public void setPcmunit(String pcmunit) {
        this.pcmunit = pcmunit;
    }

    public String getPcmremarks() {
        return pcmremarks;
    }

    public void setPcmremarks(String pcmremarks) {
        this.pcmremarks = pcmremarks;
    }

    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    public String getTimeSlotTo() {
        return timeSlotTo;
    }

    public void setTimeSlotTo(String timeSlotTo) {
        this.timeSlotTo = timeSlotTo;
    }

    public String getAnnualTurnOver() {
        return annualTurnOver;
    }

    public void setAnnualTurnOver(String annualTurnOver) {
        this.annualTurnOver = annualTurnOver;
    }

    public String getGstExemp() {
        return gstExemp;
    }

    public void setGstExemp(String gstExemp) {
        this.gstExemp = gstExemp;
    }

    public String getFcmPerc() {
        return fcmPerc;
    }

    public void setFcmPerc(String fcmPerc) {
        this.fcmPerc = fcmPerc;
    }

    public String[] getMaxAllowableKME() {
        return maxAllowableKME;
    }

    public void setMaxAllowableKME(String[] maxAllowableKME) {
        this.maxAllowableKME = maxAllowableKME;
    }

    public String[] getVehicleUnitsDedicateE() {
        return vehicleUnitsDedicateE;
    }

    public void setVehicleUnitsDedicateE(String[] vehicleUnitsDedicateE) {
        this.vehicleUnitsDedicateE = vehicleUnitsDedicateE;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getTouchPoint1() {
        return touchPoint1;
    }

    public void setTouchPoint1(String touchPoint1) {
        this.touchPoint1 = touchPoint1;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getContractAmount() {
        return contractAmount;
    }

    public void setContractAmount(String contractAmount) {
        this.contractAmount = contractAmount;
    }

    public String getCounts() {
        return counts;
    }

    public void setCounts(String counts) {
        this.counts = counts;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getOriginId() {
        return originId;
    }

    public void setOriginId(String originId) {
        this.originId = originId;
    }

    public String getTouchPoint1Id() {
        return touchPoint1Id;
    }

    public void setTouchPoint1Id(String touchPoint1Id) {
        this.touchPoint1Id = touchPoint1Id;
    }

    public String getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    
    
}
