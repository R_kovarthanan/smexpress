// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3)
// Source File Name:   MrsController.java
package ets.domain.mrs.web;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.web.BaseController;
import ets.domain.contract.business.ContractBP;
import ets.domain.mrs.business.MrsBP;
import ets.domain.mrs.business.MrsTO;
import ets.domain.purchase.business.PurchaseBP;
import ets.domain.recondition.business.ReconditionBP;
import ets.domain.users.business.LoginBP;
import ets.domain.util.FPLogUtils;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.http.*;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;

// Referenced classes of package ets.domain.mrs.web:
//            MrsCommand
public class MrsController extends BaseController {

    MrsCommand mrsCommand;
    MrsBP mrsBP;
    LoginBP loginBP;
    PurchaseBP purchaseBP;
    ReconditionBP reconditionBP;
    ContractBP contractBP;

    public ContractBP getContractBP() {
        return contractBP;
    }

    public void setContractBP(ContractBP contractBP) {
        this.contractBP = contractBP;
    }


    public MrsController() {
    }

    public PurchaseBP getPurchaseBP() {
        return purchaseBP;
    }

    public void setPurchaseBP(PurchaseBP purchaseBP) {
        this.purchaseBP = purchaseBP;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public MrsBP getMrsBP() {
        return mrsBP;
    }

    public void setMrsBP(MrsBP mrsBP) {
        this.mrsBP = mrsBP;
    }

    public MrsCommand getMrsCommand() {
        return mrsCommand;
    }

    public void setMrsCommand(MrsCommand mrsCommand) {
        this.mrsCommand = mrsCommand;
    }

    public ReconditionBP getReconditionBP() {
        return reconditionBP;
    }

    public void setReconditionBP(ReconditionBP reconditionBP) {
        this.reconditionBP = reconditionBP;
    }


    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        System.out.println("in bind func");
        System.out.println((new StringBuilder()).append("request=").append(request).toString());
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog((new StringBuilder()).append("command -->").append(command).toString());
        binder.closeNoCatch();
                   initialize(request);
    }

    public ModelAndView handleViewApprovalMrs(HttpServletRequest request, HttpServletResponse response, MrsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        mrsCommand = command;
        System.out.println("view MRS");
        String menuPath = "";
        String pageTitle = " MRS List";
        request.setAttribute("pageTitle", pageTitle);
        ArrayList mrsUnApproval = new ArrayList();
        menuPath = "Stores >>  MRS Approval";
        request.setAttribute("menuPath", menuPath);
        String fromDate = request.getParameter("fromDate");
        String toDate = request.getParameter("toDate");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Mrs-ApprovalList")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                if (fromDate == null) {
                    fromDate = (String) session.getAttribute("currentDate");
                }
                if (toDate == null) {
                    toDate = (String) session.getAttribute("currentDate");
                }
                request.setAttribute("fromDate", fromDate);
                request.setAttribute("toDate", toDate);
                path = "content/stores/MRSApproval.jsp";
                MrsTO compTO = new MrsTO();
                compTO.setCompanyId(companyId);
                mrsUnApproval = mrsBP.processApprovalList(compTO, fromDate, toDate);
                if (mrsUnApproval.size() != 0) {
                    request.setAttribute("mrsLists", mrsUnApproval);
                }
                System.out.println((new StringBuilder()).append("mrsUnApproval list size").append(mrsUnApproval.size()).toString());
            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to viewMrsUnApprovalList --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewSpecificApprovalMrs(HttpServletRequest request, HttpServletResponse response, MrsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        System.out.println((new StringBuilder()).append("companyId=").append(companyId).toString());
        mrsCommand = command;
        System.out.println("view MRS");
        String menuPath = "";
        String pageTitle = "View MRS";
        request.setAttribute("pageTitle", pageTitle);
        ArrayList vehicleDetail = new ArrayList();
        ArrayList mrsDetail = new ArrayList();
        menuPath = "Stores >> MrsApproval >> View ";
        request.setAttribute("menuPath", menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int mrsId = Integer.parseInt(request.getParameter("mrsId"));
            System.out.println((new StringBuilder()).append("mrsId").append(mrsId).toString());
            path = "content/stores/viewMRS2.jsp";
            vehicleDetail = mrsBP.processVehicleDetails(mrsId);
            request.setAttribute("vehicleDetails", vehicleDetail);
            System.out.println((new StringBuilder()).append("vehicleDetail size").append(vehicleDetail.size()).toString());
            mrsDetail = mrsBP.processMrsDetails(mrsId, companyId);
            request.setAttribute("mrsDetails", mrsDetail);
            System.out.println((new StringBuilder()).append("mrsDetail size").append(mrsDetail.size()).toString());
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to viewMrsDetails --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewMrsApprovalScreen(HttpServletRequest request, HttpServletResponse response, MrsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        mrsCommand = command;
        System.out.println("view Approval");
        String menuPath = "";
        String pageTitle = "MRS Approval";
        String mrsIdArray[] = new String[1];
        request.setAttribute("pageTitle", pageTitle);
        ArrayList vehicleDetail = new ArrayList();
        ArrayList mrsDetail = new ArrayList();
        menuPath = "Stores >>MRS Approval >> Approve ";
        request.setAttribute("menuPath", menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Mrs-Approve")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                int mrsId = Integer.parseInt(request.getParameter("mrsId"));
                System.out.println((new StringBuilder()).append("mrsId").append(mrsId).toString());
                path = "content/stores/MRSDetail.jsp";
                vehicleDetail = mrsBP.processVehicleDetails(mrsId);
                System.out.println((new StringBuilder()).append("vehicleDetail size").append(vehicleDetail.size()).toString());
                MrsTO mrsTO = new MrsTO();
                mrsIdArray[0] = String.valueOf(mrsId);
                mrsTO.setMrsId(mrsIdArray);
                mrsDetail = mrsBP.processMrsItemDetails(mrsTO, companyId);
                System.out.println((new StringBuilder()).append("mrsDetail size").append(mrsDetail.size()).toString());
                request.setAttribute("vehicleDetails", vehicleDetail);
                request.setAttribute("mrsDetails", mrsDetail);
            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to viewMrsDetails --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewMrsDetail(HttpServletRequest request, HttpServletResponse response, MrsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println("handleViewMrsDetail");
        String path = "";
        HttpSession session = request.getSession();
        int companyId = 0;
        int counterId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        mrsCommand = command;
        String menuPath = "";
        String pageTitle = "View MRS";
        request.setAttribute("pageTitle", pageTitle);
        ArrayList vehicleDetail = new ArrayList();
        ArrayList mrsDetail = new ArrayList();
        ArrayList rcMrsDetails = new ArrayList();
        ArrayList counterMrsDetails = new ArrayList();
        menuPath = "Stores >> View Mrs >> MRS No ";
        request.setAttribute("menuPath", menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int mrsId = Integer.parseInt(request.getParameter("mrsId"));
            counterId = Integer.parseInt(request.getParameter("counterId"));
            path = "content/stores/ViewMrsDetail.jsp";
            System.out.println("handleViewMrsDetail");

            vehicleDetail = mrsBP.processVehicleDetails(mrsId);

            if (vehicleDetail.size() != 0) {
                request.setAttribute("vehicleDetails", vehicleDetail);
            }
            if (counterId != 0) {
                counterMrsDetails = mrsBP.processCounterMrsDetails(mrsId);
                if (counterMrsDetails.size() != 0) {
                    request.setAttribute("counterMrsDetails", counterMrsDetails);
                }
                //request.setAttribute("counter", "Y");
            }

            rcMrsDetails = mrsBP.processRcMrsDetails(mrsId);
            if (rcMrsDetails.size() != 0) {
                request.setAttribute("rcMrsDetails", rcMrsDetails);
                System.out.println("rcMrsDetails size-->" + rcMrsDetails.size());
            }

            mrsDetail = mrsBP.processMrsDetails(mrsId, companyId);
            request.setAttribute("mrsDetails", mrsDetail);

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to viewMRSList --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewMrsList(HttpServletRequest request, HttpServletResponse response, MrsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        mrsCommand = command;
        String fromDate = request.getParameter("fromDate");
        String toDate = request.getParameter("toDate");
        System.out.println("view MRS List");
        String menuPath = "";
        String pageTitle = "MRS List";
        request.setAttribute("pageTitle", pageTitle);
        ArrayList mrsList = new ArrayList();
        menuPath = "Stores >> View MRS ";
        request.setAttribute("menuPath", menuPath);
        try {
            if (fromDate == null) {
                fromDate = (String) session.getAttribute("currentDate");
            }
            if (toDate == null) {
                toDate = (String) session.getAttribute("currentDate");
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            System.out.println((new StringBuilder()).append("fromDate=").append(fromDate).toString());
            System.out.println((new StringBuilder()).append("toDate=").append(toDate).toString());
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            ArrayList servicePtList = new ArrayList();

            if (!loginBP.checkAuthorisation(userFunctions, "Mrs-List")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "content/stores/MRSList.jsp";
//                if(companyId==1025)
//                {
//                servicePtList = mrsBP.processServicePtList();
//                request.setAttribute("servicePtList",servicePtList);
//                if(request.getParameter("compId")!=null)
//                companyId = Integer.parseInt(request.getParameter("compId"));
//                request.setAttribute("compId",companyId);
//                mrsList = mrsBP.processViewMrsList(companyId, fromDate, toDate);
//                }
//                else
                {
                mrsList = mrsBP.processViewMrsList(companyId, fromDate, toDate);
                }
                if (mrsList.size() != 0) {
                    request.setAttribute("mrsLists", mrsList);
                }
                System.out.println((new StringBuilder()).append("mrsList size").append(mrsList.size()).toString());
            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to viewMRSList --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAddMrsApprovedQuantity(HttpServletRequest request, HttpServletResponse response, MrsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        mrsCommand = command;
        ArrayList mrsList = new ArrayList();
        String path = "";
        try {
            System.out.println("in MRS Add Quantity  ");
            String menuPath = "Stores  >>  MRS Approval List";
            String pageTitle = "MRS List";
            request.setAttribute("pageTitle", pageTitle);
            request.setAttribute("menuPath", menuPath);
            System.out.println("in MRS Add Quantity  ");
            int status = 0;
            String mrsApprovalStatus = "";
            mrsApprovalStatus = mrsCommand.getMrsApprovalStatus();
            mrsApprovalStatus = request.getParameter("mrsApprovalStatus");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            if (fromDate == null) {
                fromDate = (String) session.getAttribute("currentDate");
            }
            if (toDate == null) {
                toDate = (String) session.getAttribute("currentDate");
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            MrsTO mrsTO = new MrsTO();
            System.out.println((new StringBuilder()).append("mrsApprovalStatus=").append(mrsApprovalStatus).toString());
            System.out.println((new StringBuilder()).append("issue").append(mrsCommand.getMrsIssueQuantity()).toString());
            System.out.println((new StringBuilder()).append("mrsid").append(mrsCommand.getMrsNumber()).toString());
            System.out.println((new StringBuilder()).append("itemqty").append(mrsCommand.getMrsRequestedItemNumber()).toString());
            System.out.println((new StringBuilder()).append("itemid").append(mrsCommand.getMrsItemId()).toString());
            System.out.println((new StringBuilder()).append("remarks ").append(mrsCommand.getMrsApprovalRemarks()).toString());
            mrsTO.setMrsApprovalStatus(mrsApprovalStatus);
            if (mrsCommand.getMrsNumber() != null && mrsCommand.getMrsNumber() != "") {
                System.out.println((new StringBuilder()).append("mrsCommand.getMrsNumber()").append(mrsCommand.getMrsNumber()).toString());
                mrsTO.setMrsNumber(mrsCommand.getMrsNumber());
            }
            if (mrsCommand.getMrsApprovalRemarks() != null && mrsCommand.getMrsApprovalRemarks() != "") {
                mrsTO.setMrsApprovalRemarks(mrsCommand.getMrsApprovalRemarks());
            }
            int index = 0;
            String itemIds[] = mrsCommand.getItemIds();
            String requestedQtys[] = mrsCommand.getRequestedQtys();
            String issueQtys[] = mrsCommand.getIssueQtys();
            String selectedIndex[] = mrsCommand.getSelectedIndex();
            String positionId[] = mrsCommand.getPositionIds();
            ArrayList List = new ArrayList();
            MrsTO listTO = null;
            for (int i = 0; i < selectedIndex.length; i++) {
                listTO = new MrsTO();
                index = Integer.parseInt(selectedIndex[i]);
                System.out.println((new StringBuilder()).append("index").append(index).toString());
                listTO.setMrsItemId(itemIds[index]);
                listTO.setMrsRequestedItemNumber(requestedQtys[index]);
                listTO.setMrsIssueQuantity(issueQtys[index]);
                listTO.setPositionId(positionId[index]);
                listTO.setMrsApprovalRemarks(mrsCommand.getMrsApprovalRemarks());
                listTO.setMrsApprovalStatus(mrsCommand.getMrsApprovalStatus());
                listTO.setMrsNumber(mrsCommand.getMrsNumber());
                System.out.println((new StringBuilder()).append("mrsCommand.getMrsNumber()=").append(mrsCommand.getMrsNumber()).toString());
                List.add(listTO);
            }

            int userId = ((Integer) session.getAttribute("userId")).intValue();
            int companyId = 0;
            companyId = Integer.parseInt((String) session.getAttribute("companyId"));
            System.out.println((new StringBuilder()).append("userId").append(userId).toString());
            path = "content/stores/MRSApproval.jsp";
            mrsTO = new MrsTO();
            mrsTO.setCompanyId(companyId);
            System.out.println((new StringBuilder()).append("list size=").append(List.size()).toString());
            status = mrsBP.processAddMrsApprovedQuantity(userId, mrsTO, List);
            mrsList = mrsBP.processApprovalList(mrsTO, fromDate, toDate);
            if (mrsList.size() != 0) {
                request.setAttribute("mrsLists", mrsList);
            }
            request.setAttribute("successMessage", "MRS Quantity Details Modified Successfully");
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to ADD data  --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleOtherServicePointDetails(HttpServletRequest request, HttpServletResponse response, MrsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList servicePointItemList = new ArrayList();
        HttpSession session = request.getSession();
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        mrsCommand = command;
        System.out.println("view Other Service Point Item List");
        String menuPath = "";
        String pageTitle = "View Other Service Point Item List";
        menuPath = "Stores  >>MRS Approval >> Other Service Point  ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        try {
            int itemId = Integer.parseInt(request.getParameter("itemId"));
            System.out.println((new StringBuilder()).append("ItemId").append(itemId).toString());
            path = "content/stores/OtherServiceStockAvailability.jsp";
            servicePointItemList = mrsBP.processOtherServicePointDetails(itemId, companyId);
            request.setAttribute("servicePointItemLists", servicePointItemList);
            System.out.println((new StringBuilder()).append("servicePointItemList size").append(servicePointItemList.size()).toString());
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to view servicePointItemList --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewMrsIssue(HttpServletRequest request, HttpServletResponse response) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList mrsDetail = new ArrayList();
        ArrayList mrsTyreDetail = new ArrayList();
        ArrayList vehicleDetail = new ArrayList();
        ArrayList rcMrsDetails = new ArrayList();
        ArrayList counterMrsDetails = new ArrayList();
        ArrayList vatList = new ArrayList();
        HttpSession session = request.getSession();
        int companyId = 0;
        int counterId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        int mrsId = Integer.parseInt(request.getParameter("mrsId"));
        if (request.getParameter("counterId") != null && !"".equals(request.getParameter("counterId"))) {
            counterId = Integer.parseInt(request.getParameter("counterId"));
        }
        System.out.println("view Issue handleViewMrsIssue");
        String menuPath = "";
        String pageTitle = "View Issue mrscontroller";
        menuPath = "View MRS  >>Issue ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Mrs-IssueList")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "content/stores/viewMRS.jsp";
                System.out.println("CounterId" + counterId);
                mrsDetail = mrsBP.processMrsDetails(mrsId, companyId);
                System.out.println("After Mrs Details");
                //request.setAttribute("counter", "N");
                request.setAttribute("counterId", counterId);
                if (counterId != 0) {
                    counterMrsDetails = mrsBP.processCounterMrsDetails(mrsId);
                    if (counterMrsDetails.size() != 0) {
                        request.setAttribute("counterMrsDetails", counterMrsDetails);
                    }
                    //request.setAttribute("counter", "Y");
                    vatList = purchaseBP.processVatList();
                    System.out.println("in Mrs processVatList" + vatList.size());
                    request.setAttribute("vatList", vatList);
                } else {
                    rcMrsDetails = mrsBP.processRcMrsDetails(mrsId);
                    //System.out.println("After RCMrs Details");

                    if (rcMrsDetails.size() != 0) {
                        request.setAttribute("rcMrsDetails", rcMrsDetails);
                    }
                }

                System.out.println("rcMrsDetails size-->" + rcMrsDetails.size());
                System.out.println("counterMrsDetails size-->" + counterMrsDetails.size());

                vehicleDetail = mrsBP.processVehicleDetails(mrsId);
                if (vehicleDetail.size() != 0) {
                    request.setAttribute("vehicleDetails", vehicleDetail);
                }

                System.out.println((new StringBuilder()).append("vehicleDetail size").append(vehicleDetail.size()).toString());
                request.setAttribute("mrsDetails", mrsDetail);
                System.out.println((new StringBuilder()).append("mrsDetail size").append(mrsDetail.size()).toString());
            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to view Issue --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void handlePriceList(HttpServletRequest request, HttpServletResponse response, MrsCommand command) {
        System.out.println("i am in handlePriceList  ajax");
        mrsCommand = command;
        MrsTO mrsTO = new MrsTO();
        HttpSession session = request.getSession();
        int companyId = 0;
        companyId = Integer.parseInt(session.getAttribute("companyId").toString());
        System.out.println((new StringBuilder()).append("request values=").append(request).toString());
        int itemType = Integer.parseInt(request.getParameter("itemType"));
        String priceList = "";
        try {
            System.out.println((new StringBuilder()).append("itemTypeId  ").append(itemType).toString());
            System.out.println("in New issue ");
            System.out.println((new StringBuilder()).append("itemId=").append(request.getParameter("mrsItemId")).toString());
            mrsTO.setMrsItemId(request.getParameter("mrsItemId"));
            mrsTO.setCompanyId(companyId);
            priceList = mrsBP.processPriceList(itemType, mrsTO);
            PrintWriter writer = response.getWriter();
            System.out.println((new StringBuilder()).append("priceList size   :").append(priceList).toString());
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(priceList);
            writer.close();
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to retrieve PriceList data in Ajax --> ").append(exception).toString());
        }
    }

    public void handleRcPriceList(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("i am in handleRcPriceList Ajax");
        MrsTO mrsTO = new MrsTO();
        HttpSession session = request.getSession();
        int companyId = 1;
        System.out.println((new StringBuilder()).append("request values=").append(request).toString());
        String priceList = "";
        try {
            companyId = Integer.parseInt((String) session.getAttribute("companyId"));
            System.out.println("in Rc issue " + companyId);
            System.out.println((new StringBuilder()).append("itemId=").append(request.getParameter("mrsItemId")).toString());
            String categoryId = request.getParameter("categoryId");
            mrsTO.setMrsItemId(request.getParameter("mrsItemId"));
            mrsTO.setCompanyId(companyId);
            if (categoryId.equals("1011")) {
                priceList = mrsBP.processRtTyrePriceList(mrsTO);
            } else {
                priceList = mrsBP.processRcPriceList(mrsTO);
            }
            PrintWriter writer = response.getWriter();
            System.out.println((new StringBuilder()).append("priceList size   :").append(priceList).toString());
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(priceList);
            writer.close();
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to retrieve PriceList data in Ajax --> ").append(exception).toString());
        }
    }

    public void handleVehicleRcItems(HttpServletRequest request, HttpServletResponse response, MrsCommand command) {
        System.out.println("in Ajax handleVehicleRcItems");
        mrsCommand = command;
        MrsTO mrsTO = new MrsTO();
        HttpSession session = request.getSession();
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        System.out.println((new StringBuilder()).append("request values=").append(request).toString());
        int itemType = Integer.parseInt(request.getParameter("itemType"));
        String rcItems = "";
        try {
            System.out.println((new StringBuilder()).append("itemTypeId  ").append(itemType).toString());
            System.out.println("in Rc Receive");
            System.out.println((new StringBuilder()).append("vehicleId=").append(request.getParameter("vehicleId")).toString());
            mrsTO.setMrsItemId(request.getParameter("mrsItemId"));
            mrsTO.setVehicleId(Integer.parseInt(request.getParameter("vehicleId")));
            rcItems = mrsBP.processVehicleRcItems(mrsTO);
            PrintWriter writer = response.getWriter();
            System.out.println((new StringBuilder()).append("rcItems size   :").append(rcItems).toString());
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(rcItems);
            writer.close();
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to retrieve PriceList data in Ajax --> ").append(exception).toString());
        }
    }

    public ModelAndView handleMrsSummary(HttpServletRequest request, HttpServletResponse response, MrsCommand command) {
        mrsCommand = command;
        MrsTO mrsTO = new MrsTO();
        String menuPath = "Stores >> View MRS ";
        String pageTitle = "MRS List";
        request.setAttribute("pageTitle", pageTitle);
        int companyId = 0;
        String path = "/content/stores/MRSList.jsp";
        ArrayList mrsList = new ArrayList();
        ArrayList mrsSummary = new ArrayList();
        ArrayList itemVedors = new ArrayList();
        ArrayList jobCardList = new ArrayList();
        HttpSession session = request.getSession();
        request.setAttribute("menuPath", menuPath);
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String selectedIndex[] = request.getParameterValues("selectedIndex");
        String mrsIds[] = new String[selectedIndex.length];
        String fromDate = request.getParameter("fromDate");
        String toDate = request.getParameter("toDate");
        int index = 0;
        try {
            if (fromDate == null) {
                fromDate = (String) session.getAttribute("currentDate");
            }
            if (toDate == null) {
                toDate = (String) session.getAttribute("currentDate");
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            System.out.println((new StringBuilder()).append("fromDate=").append(fromDate).toString());
            System.out.println((new StringBuilder()).append("toDate=").append(toDate).toString());
            for (int i = 0; i < selectedIndex.length; i++) {
                index = Integer.parseInt(selectedIndex[i]);
                mrsIds[i] = mrsCommand.getMrsIds()[index];
            }

            mrsTO.setMrsId(mrsIds);
            mrsList = mrsBP.processViewMrsList(companyId, fromDate, toDate);
            System.out.println((new StringBuilder()).append("mrsList size in controller=").append(mrsList.size()).toString());
            if (mrsList.size() != 0) {
                request.setAttribute("mrsLists", mrsList);
            }
            mrsSummary = mrsBP.processMrsSummary(mrsTO, companyId);
            System.out.println((new StringBuilder()).append("final mrsSummary-->").append(mrsSummary.size()).toString());
            request.setAttribute("mrsSummaryList", mrsSummary);
            jobCardList = mrsBP.getJobCardList(companyId);
            request.setAttribute("jobCardList", jobCardList);
            System.out.println((new StringBuilder()).append("jobCardList size=").append(jobCardList.size()).toString());
            itemVedors = purchaseBP.processVendorMfrCat(1011);
            System.out.println((new StringBuilder()).append("itemVedors -->").append(itemVedors.size()).toString());
            request.setAttribute("vendorList", itemVedors);
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to mrsSummary --> ").append(exception).toString());
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleMrsApprovalSummary(HttpServletRequest request, HttpServletResponse response, MrsCommand command) {
        mrsCommand = command;
        MrsTO mrsTO = new MrsTO();
        String menuPath = "Stores >> MRS Approval ";
        String pageTitle = "MRS Approval List";
        request.setAttribute("pageTitle", pageTitle);
        int companyId = 0;
        String path = "/content/stores/MRSApproval.jsp";
        ArrayList mrsList = new ArrayList();
        ArrayList mrsSummary = new ArrayList();
        ArrayList itemVedors = new ArrayList();
        HttpSession session = request.getSession();
        request.setAttribute("menuPath", menuPath);
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String selectedIndex[] = request.getParameterValues("selectedIndex");
        String mrsIds[] = new String[selectedIndex.length];
        int index = 0;
        try {
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            if (fromDate == null) {
                fromDate = (String) session.getAttribute("currentDate");
            }
            if (toDate == null) {
                toDate = (String) session.getAttribute("currentDate");
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            for (int i = 0; i < selectedIndex.length; i++) {
                index = Integer.parseInt(selectedIndex[i]);
                mrsIds[i] = mrsCommand.getMrsIds()[index];
            }

            mrsTO.setMrsId(mrsIds);
            MrsTO compTO = new MrsTO();
            compTO.setCompanyId(companyId);
            mrsList = mrsBP.processApprovalList(compTO, fromDate, toDate);
            if (mrsList.size() != 0) {
                request.setAttribute("mrsLists", mrsList);
            }
            mrsSummary = mrsBP.processMrsSummary(mrsTO, companyId);
            System.out.println((new StringBuilder()).append("final mrsSummary-->").append(mrsSummary.size()).toString());
            request.setAttribute("mrsSummaryList", mrsSummary);
            itemVedors = mrsBP.processVendorsForItem(mrsTO);
            System.out.println((new StringBuilder()).append("itemVedors -->").append(itemVedors.size()).toString());
            request.setAttribute("vendorList", itemVedors);
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to mrsSummary --> ").append(exception).toString());
        }
        return new ModelAndView(path);
    }

    public ModelAndView chooseJobCard(HttpServletRequest request, HttpServletResponse response, MrsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList jobCardlist = new ArrayList();
        ArrayList mrsList = new ArrayList();
        HttpSession session = request.getSession();
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String menuPath = "";
        String pageTitle = "Genearte MRS";
        menuPath = "Render Service >>Genearte MRS ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            System.out.println((new StringBuilder()).append("size is  ").append(userFunctions.size()).toString());
            if (!loginBP.checkAuthorisation(userFunctions, "RenderService-GenerateMrs")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                if(companyId!=1025)
                jobCardlist = mrsBP.getJobCardList(companyId);
                else
                jobCardlist = reconditionBP.getJobCardList();
                request.setAttribute("jobCardlist", jobCardlist);
                request.removeAttribute("mrsList");
                path = "content/RenderService/chooseJobCard.jsp";
            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to view jobcard --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView searchMrsDetails(HttpServletRequest request, HttpServletResponse response, MrsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList jobCardlist = new ArrayList();
        ArrayList mrsList = new ArrayList();
        HttpSession session = request.getSession();
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String menuPath = "";
        String pageTitle = "Genearte MRS";
        menuPath = "Render Service >>Genearte MRS ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/RenderService/chooseJobCard.jsp";
            int jobcardId = Integer.parseInt(request.getParameter("mrsJobCardNumber"));
            mrsList = mrsBP.getMrsList1(jobcardId);
            System.out.println((new StringBuilder()).append("mrsList vijay size").append(mrsList.size()).toString());
            jobCardlist = mrsBP.getJobCardList(companyId);
            request.setAttribute("jobCardlist", jobCardlist);
            request.setAttribute("mrsList", mrsList);
            request.setAttribute("jobcardId", Integer.valueOf(jobcardId));
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to search mrs details --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView generateMrsPage(HttpServletRequest request, HttpServletResponse response, MrsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        mrsCommand = command;
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Genearte MRS";
        menuPath = "Render Service >>Genearte MRS ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        try {
            int jobCardId = 0;
            if (mrsCommand.getMrsJobCardNumber() != null && mrsCommand.getMrsJobCardNumber() != "") {
                jobCardId = Integer.parseInt(mrsCommand.getMrsJobCardNumber());
            }
            int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
            ArrayList vehicleDetails = new ArrayList();
            ArrayList technicians = new ArrayList();
            vehicleDetails = mrsBP.getVehicleDetails(jobCardId);
            technicians = mrsBP.jobCardtechList(companyId, jobCardId);

            ArrayList tyrePostions = new ArrayList();
            tyrePostions = mrsBP.getTyrePostions();

            request.setAttribute("technicians", technicians);
            request.setAttribute("vehicleDetails", vehicleDetails);
            request.setAttribute("tyrePostions", tyrePostions);

            ArrayList mfrList = contractBP.getMfrList();
            request.setAttribute("mfrList", mfrList);
            path = "content/RenderService/generateMrs.jsp";
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to view Issue --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getList(HttpServletRequest request, HttpServletResponse response, MrsCommand command)
            throws IOException {
        System.out.println("i am in ajax ");
        HttpSession session = request.getSession();
        mrsCommand = command;
        String mfrCode = mrsCommand.getMfrCode();
        String itemCode = mrsCommand.getItemCode();
        String itemName = mrsCommand.getItemName();
        System.out.println((new StringBuilder()).append("mfrCode  ").append(mfrCode).toString());
        System.out.println((new StringBuilder()).append("itemCode  ").append(itemCode).toString());
        System.out.println((new StringBuilder()).append("itemName  ").append(itemName).toString());
        String suggestions = mrsBP.getSuggestions(mfrCode, itemCode, itemName);
        System.out.println((new StringBuilder()).append("suggestions").append(suggestions).toString());
        PrintWriter writer = response.getWriter();
        System.out.println(suggestions);
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.println(suggestions);
        writer.close();
    }



    public ModelAndView handleMrsIssueHistory(HttpServletRequest request, HttpServletResponse response, MrsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        mrsCommand = command;
        ArrayList jobCardlist = new ArrayList();
        ArrayList itemList = new ArrayList();
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "MRS";
        menuPath = "STORES >> MRS >> ISSUE HISTORY";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/stores/issueHistory.jsp";
            itemList = mrsBP.processMrsIssueHistory(Integer.parseInt(request.getParameter("mrsId")));
            request.setAttribute("itemList", itemList);
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to view Issue --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleScrapItems(HttpServletRequest request, HttpServletResponse response) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println((new StringBuilder()).append("request=").append(request).toString());
        String path = "";
        ArrayList scrapList = new ArrayList();
        ArrayList mrsList = new ArrayList();
        MrsTO mrsTo = null;
        HttpSession session = request.getSession();
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String menuPath = "";
        String pageTitle = "MRS";
        menuPath = "STORES >> MRS >> ISSUE";
        int index = 0;
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/stores/MRSList.jsp";
            int userId = ((Integer) session.getAttribute("userId")).intValue();
            String mrsId = request.getParameter("mrsId");
            String item_id[] = request.getParameterValues("mrsItemId");
            String faultQty[] = request.getParameterValues("faultQty");
            String selectedIndex[] = request.getParameterValues("selectedIndex1");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            if (fromDate == null) {
                fromDate = (String) session.getAttribute("currentDate");
            }
            if (toDate == null) {
                toDate = (String) session.getAttribute("currentDate");
            }
            System.out.println((new StringBuilder()).append("fromDate=").append(fromDate).toString());
            System.out.println((new StringBuilder()).append("toDate=").append(toDate).toString());
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            for (int i = 0; i < selectedIndex.length; i++) {
                index = Integer.parseInt(selectedIndex[i]);
                mrsTo = new MrsTO();
                mrsTo.setMrsItemId(item_id[index]);
                mrsTo.setMrsNumber(mrsId);
                mrsTo.setFaultQty(faultQty[index]);
                mrsTo.setRcItemId("0");
                mrsTo.setQty("1");
                scrapList.add(mrsTo);
            }

            mrsList = mrsBP.processViewMrsList(companyId, fromDate, toDate);
            if (mrsList.size() != 0) {
                request.setAttribute("mrsLists", mrsList);
            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to view Issue --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleGenerateMrsGdn(HttpServletRequest request, HttpServletResponse response, MrsCommand command)throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println((new StringBuilder()).append("request=").append(request).toString());
        String path = "";
        ArrayList scrapList = new ArrayList();
        ArrayList mrsList = new ArrayList();
        ArrayList counterItems = new ArrayList();
        ArrayList billHeaderInfo = new ArrayList();
        ArrayList billItems = new ArrayList();
        MrsTO mrsTo = null;
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "MRS";
        menuPath = "STORES >> MRS >> ISSUE";
        int index = 0;
        String itemIds[] = null;
        String tax[] = null;
        String quantity[] = null;
        String prices[] = null;
        int i= 0;
        int billNo=0;
        int status=0;
        float counterBillAmount=0.0f;
        float discountAmount=0.0f;
        float spareAmount=0.0f;
        float nettAmount=0.0f;
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        try {
            mrsCommand = command;
            System.out.println("getMrsId()"+mrsCommand.getMrsId());

            int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
            int userId = ((Integer) session.getAttribute("userId")).intValue();
            String mrsId = request.getParameter("mrsId");
            String remarks = request.getParameter("remarks");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            int counterId = Integer.parseInt(request.getParameter("counterId"));
            String reqFor= request.getParameter("reqFor");
            System.out.println("Generate GDN CounterId" + counterId+"reqFor-->"+reqFor);
            if (fromDate == null) {
                fromDate = (String) session.getAttribute("currentDate");
            }
            if (toDate == null) {
                toDate = (String) session.getAttribute("currentDate");
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            System.out.println((new StringBuilder()).append("fromDate=").append(fromDate).toString());
            System.out.println((new StringBuilder()).append("toDate=").append(toDate).toString());
            
            //ReqFor Block only for Print the Counter Bill
            if(reqFor!=null)
            {
            billHeaderInfo = mrsBP.getCounterBillHeaderInfo(counterId);
            billItems = mrsBP.getCounterBillItems(counterId);
            if(billHeaderInfo.size() > 0)
            request.setAttribute("billHeaderInfo",billHeaderInfo);
            if(billItems.size() > 0)
            request.setAttribute("billItems",billItems);
            path = "content/counter/counterBill.jsp";
            }
            else
            {
            mrsTo = new MrsTO();
            mrsTo.setMrsNumber(mrsId);
            mrsTo.setMrsApprovalRemarks(remarks);
            mrsBP.processGenerateMrsGdn(mrsTo, userId);
            mrsTo=new MrsTO();
            //Hari Start
            if (counterId != 0) {
                System.out.println("Mrs Id" + request.getParameter("mrsId"));
//                System.out.println("sparePercentage" + request.getParameter("hike"));
//                System.out.println("spareAmount" + request.getParameter("spareAmount"));
//                System.out.println("Discount" + request.getParameter("discount"));
//                System.out.println("Spare Ret Amount" + request.getParameter("spareRetAmount"));
//                System.out.println("Nett AMount" + request.getParameter("nettAmount"));
//                System.out.println("Remarks" + request.getParameter("remarks"));

                mrsTo.setMrsId1(Integer.parseInt(mrsCommand.getMrsId()));
                mrsTo.setHike(Float.parseFloat(mrsCommand.getHike()));
                mrsTo.setDiscount(Float.parseFloat(mrsCommand.getDiscount()));
                mrsTo.setSpareAmount(Float.parseFloat(mrsCommand.getSpareAmount()));
                mrsTo.setTax(Float.parseFloat(mrsCommand.getTax()));
                mrsTo.setSpareTaxAmount(Float.parseFloat(mrsCommand.getSpareTaxAmount()));

                mrsTo.setSpareRetAmount(Float.parseFloat(mrsCommand.getSpareRetAmount()));
                mrsTo.setNettAmount(Float.parseFloat(mrsCommand.getNettAmount()));
                mrsTo.setRemarks(mrsCommand.getRemarks());
                mrsTo.setUserId(userId);
                spareAmount = Float.parseFloat(mrsCommand.getSpareAmount())+Float.parseFloat(mrsCommand.getSpareTaxAmount());
                nettAmount = Float.parseFloat(mrsCommand.getNettAmount())+Float.parseFloat(mrsCommand.getSpareRetAmount());
                discountAmount = spareAmount - nettAmount;
                System.out.println("DiscountAMount -->"+discountAmount);
                mrsTo.setDiscountAmount(discountAmount);
                counterBillAmount = Float.parseFloat(mrsCommand.getNettAmount());

                billNo = mrsBP.insertCounterBillMaster(mrsTo);

                itemIds = request.getParameterValues("mrsItemId");
                tax = request.getParameterValues("itemTax");
                prices = request.getParameterValues("price");
                quantity = request.getParameterValues("totalIssued");
                System.out.println("Item Id Length" + itemIds.length);
                System.out.println("itemTax Length" + tax.length);
                mrsTo=new MrsTO();
            for(i=0;i<itemIds.length;i++)
            {
                mrsTo= new MrsTO();
                mrsTo.setCounterId(counterId);
                mrsTo.setIssuedItemsId(Integer.parseInt(itemIds[i]));
                mrsTo.setQuantityRI(Float.parseFloat(quantity[i]));
                mrsTo.setTax(Float.parseFloat(tax[i]));
                mrsTo.setPrice(prices[i]);
                mrsTo.setNettAmount((Float.parseFloat(prices[i])*Float.parseFloat(quantity[i])));
//                System.out.println("Item Id["+i+"]-->"+itemIds[i]);
//                System.out.println("Quantity["+i+"]-->"+quantity[i]);
//                System.out.println("tax["+i+"]-->"+tax[i]);
//                System.out.println("Prices["+i+"]-->"+prices[i]);
//                System.out.println("Nettamount["+i+"]-->"+(Float.parseFloat(prices[i])*Float.parseFloat(quantity[i])));
                counterItems.add(mrsTo);
            }
            //System.out.println("counterItems Size"+counterItems.size());

            status = mrsBP.insertCounterBillItems(counterItems,billNo,counterBillAmount);

            billHeaderInfo = mrsBP.getCounterBillHeaderInfo(counterId);
            billItems = mrsBP.getCounterBillItems(counterId);

            if(billHeaderInfo.size() > 0)
            request.setAttribute("billHeaderInfo",billHeaderInfo);

            if(billItems.size() > 0)
            request.setAttribute("billItems",billItems);

            path = "content/counter/counterBill.jsp";
                    }
             else {
                path = "content/stores/MRSList.jsp";
                mrsList = mrsBP.processViewMrsList(companyId, fromDate, toDate);
                if (mrsList.size() != 0) {
                    request.setAttribute("mrsLists", mrsList);
                }
            }
            }
            //Hari End
            request.setAttribute("successMessage", "Goods Delivery Generated Successfully");
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to view Issue --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleItemIssuePopup(HttpServletRequest request, HttpServletResponse response) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println((new StringBuilder()).append("request=").append(request).toString());
        String path = "";
        ArrayList scrapList = new ArrayList();
        ArrayList itemList = new ArrayList();
        MrsTO mrsTo = null;
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "MRS";
        menuPath = "STORES >> MRS >> ISSUE";
        int index = 0;
        int mrsId = Integer.parseInt(request.getParameter("mrsId"));
        int itemId = Integer.parseInt(request.getParameter("itemId"));
        int positionId = Integer.parseInt(request.getParameter("positionId"));
        String categoryId = request.getParameter("categoryId");
        //Hari
        //String counterFlag = request.getParameter("counterFlag");
        String counterId = request.getParameter("counterId");
        System.out.println("CounterId" + counterId);
        //Hari End
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        try {
            request.setAttribute("itemName", request.getParameter("itemName"));
            request.setAttribute("itemId", request.getParameter("itemId"));
            request.setAttribute("categoryId", request.getParameter("categoryId"));
            request.setAttribute("mrsId", request.getParameter("mrsId"));
            request.setAttribute("positionId", request.getParameter("positionId"));
            request.setAttribute("rcStatus", request.getParameter("rcStatus"));
            request.setAttribute("approvedQty", request.getParameter("approvedQty"));
            request.setAttribute("rcWorkId", request.getParameter("rcWorkId"));
            request.setAttribute("rcItemsId", request.getParameter("rcItemsId"));
            request.setAttribute("mrsItemId", request.getParameter("mrsItemId"));
            //request.setAttribute("counterFlag", counterFlag);
            request.setAttribute("counterId", counterId);
            //System.out.println("request" + request.getAttribute("counterFlag"));
            System.out.println("request mrsItemId" + request.getAttribute("mrsItemId"));
            System.out.println("mrsItemId" + request.getParameter("mrsItemId"));
            System.out.println("rcItemsId" + request.getParameter("rcItemsId"));
            if (categoryId.equals("1011")) {
                itemList = mrsBP.processTyresIsuedDetails(mrsId, itemId, positionId);
            } else {
                itemList = mrsBP.processItemsIsuedDetails(mrsId, itemId);
            }
            System.out.println((new StringBuilder()).append("for mrsId and itemId").append(mrsId).append(" -").append(itemId).toString());
            System.out.println((new StringBuilder()).append("itemList details size=").append(itemList.size()).toString());
            if (itemList.size() != 0) {
                request.setAttribute("itemList", itemList);
            }
            path = "content/stores/itemIssuePopup.jsp";
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to view Issue --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getQuandity(HttpServletRequest request, HttpServletResponse response, MrsCommand command)
            throws IOException {
        HttpSession session = request.getSession();
        mrsCommand = command;
        String mfrCode = mrsCommand.getMfrCode();
        String itemCode = mrsCommand.getItemCode();
        String itemName = mrsCommand.getItemName();
        String vendorId = (String)request.getParameter("vendorId");
        String reqType = (String)request.getParameter("reqType");
        System.out.println("vendorId:"+vendorId);
        System.out.println("reqType:"+reqType);
        if(reqType == null){
            reqType = "";
        }
        System.out.println((new StringBuilder()).append("mfrCode  ").append(mfrCode).toString());
        System.out.println((new StringBuilder()).append("itemCode  ").append(itemCode).toString());
        System.out.println((new StringBuilder()).append("itemName  ").append(itemName).toString());
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String suggestions = mrsBP.getQuandity(mfrCode, itemCode, itemName, companyId, vendorId,reqType);
        System.out.println((new StringBuilder()).append("suggestions").append(suggestions).toString());
        PrintWriter writer = response.getWriter();
        System.out.println(suggestions);
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.print(suggestions);
        writer.close();
    }

    public ModelAndView handleMrsTyresIssue(HttpServletRequest request, HttpServletResponse response) {
        int companyId = 0;
        MrsTO mrsTO = new MrsTO();
        MrsTO retMrsTO = new MrsTO();
        HttpSession session = request.getSession();
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        int userId = ((Integer) session.getAttribute("userId")).intValue();
        System.out.println("In Tyres Issue ");
        System.out.println((new StringBuilder()).append("userId").append(userId).toString());
        System.out.println((new StringBuilder()).append("request values=").append(request).toString());
        int index = 0;
        int status = 0;
        String path = "content/stores/refreshScript.jsp";
        System.out.println((new StringBuilder()).append("rc item ids test=").append(request.getParameter("rcItemIds")).toString());
        try {
            String mrsId = request.getParameter("mrsId");
            String itemId = request.getParameter("itemId");
            String tyreId = request.getParameter("tyreId");
            String actionType = "1011";
            String itemType = request.getParameter("itemType");
            //Hari
            System.out.println("IIIIIIIIIIIIIItem Type handleMrsTyresIssue-->" + itemType);
            //Hari
            String positionId = request.getParameter("positionId");
            String priceId = request.getParameter("priceId");
            String quantity = request.getParameter("issueQty");
            String categoryId = request.getParameter("categoryId");
            String faultQty = request.getParameter("faultQty");
            String selectedItem = request.getParameter("selectedItem");
            String rcStatus = request.getParameter("rcStatus");
            //Hari Start
            String type = "new";
            String rcWork = request.getParameter("rcWorkId");
            String rcItems = request.getParameter("rcItemsId");
            String issued = request.getParameter("mrsItemId");
            String rcPriceWoId = request.getParameter("rcPriceWoId");
            System.out.println("rcWork" + rcWork);
            System.out.println("rcItemsId" + rcItems);
            System.out.println("mrsItemId" + issued);
            int rcWorkId = Integer.parseInt(rcWork);
            int rcItemsId = Integer.parseInt(rcItems);
            int mrsItemId = Integer.parseInt(issued);
            System.out.println("RCWorkId-->" + rcWorkId);
            System.out.println("rcPriceWoId in Tyre Method-->" + rcPriceWoId);
            //For Counter Bill
            String tax = "";
            String hikePercentage = "";
            String discount = "";
            String nettAmount = "";
            String counterId = "";
            String amount = "";
            //Hari End
            System.out.println((new StringBuilder()).append("issue qty in jsp=").append(request.getParameter("issueQty")).toString());
            String retIssueId[] = request.getParameterValues("retIssueId");
            String retPriceId[] = request.getParameterValues("retPriceId");
            String retRcItemId[] = request.getParameterValues("retRcItemId");
            String retItemType[] = request.getParameterValues("retItemType");
            String retReturnedQty[] = request.getParameterValues("retReturnedQty");
            String retPrice[] = request.getParameterValues("retPrice");
            String retTyreId[] = request.getParameterValues("retTyreId");
            String retPositionIds[] = request.getParameterValues("retTyreId");
            String retselectedIndex[] = request.getParameterValues("retselectedIndex");
            System.out.println((new StringBuilder()).append("faultQty = ").append(faultQty).toString());
            mrsTO.setMrsItemId(itemId);
            mrsTO.setMrsNumber(mrsId);
            mrsTO.setTyreId(tyreId);
            mrsTO.setActionType(actionType);
           if(positionId!=null){
            mrsTO.setPositionId(positionId);
           }
            if(companyId!=0){
            mrsTO.setCompanyId(companyId);
            }
            if(priceId!=null){
            mrsTO.setPriceIds(priceId);
            }
            mrsTO.setFaultStatus("N");
            mrsTO.setFaultQty(faultQty);
            mrsTO.setRcStatus(rcStatus);
            mrsTO.setItemType("NEW");
            //Hari
            mrsTO.setType("NEW");
            mrsTO.setRcWorkId(rcWorkId);
            mrsTO.setRcItemsId(rcItemsId);
            mrsTO.setIssuedItemsId(mrsItemId);
            if (selectedItem != null) {
                if (categoryId.equals("1011")) {
                    mrsTO.setQuantityRI(-1F);
                    mrsBP.processIssueTyres(mrsTO, userId, companyId);
                    System.out.println("start------------------");
                    mrsBP.processMrsVehicleTyreUpdate(mrsTO, userId, companyId);
                    System.out.println("End------------------");
                } else if (!categoryId.equals("1011")) {
                    mrsTO.setQuantityRI(Float.parseFloat(quantity));
                    mrsBP.processIssueItems(mrsTO, userId);
                }
                //Hari
                if (rcWorkId != 0) {
                    mrsTO.setType(itemType);
                    mrsTO.setQty(quantity);
                    mrsTO.setCategoryId(categoryId);
                    mrsBP.updatePriceForSpareItems(mrsTO, userId);
                }
                //Hari End
            }




            if (retselectedIndex != null && retselectedIndex.length != 0) {

                for (int i = 0; i < retselectedIndex.length; i++) {
                    index = Integer.parseInt(retselectedIndex[i]);
                    retMrsTO = new MrsTO();
                    retMrsTO.setMrsItemId(itemId);
                    retMrsTO.setMrsNumber(mrsId);
                    retMrsTO.setCompanyId(companyId);
                    retMrsTO.setPriceIds(retPriceId[index]);
                    retMrsTO.setRcItemIds(retRcItemId[index]);
                    retMrsTO.setQuantityRI(Float.parseFloat(retReturnedQty[index]));
                    retMrsTO.setTyreId(retTyreId[index]);
                    retMrsTO.setIssueId(retIssueId[index]);
                    retMrsTO.setPositionId(positionId);
                    //Hari
                    retMrsTO.setPrice(retPrice[index]);
                    retMrsTO.setCategoryId(categoryId);
                    System.out.println("retMrsTO.getPrice()" + retMrsTO.getPrice());
                    //Hari End
                    retMrsTO.setActionType("1012");
                    retMrsTO.setFaultStatus("N");
                    if (categoryId.equals("1011")) {
                        if (retItemType[index].equals("NEW")) {
                            System.out.println("tyre Disabled");
                            mrsBP.processReturnIssuedTyre(retMrsTO);
                        } else {
                            mrsBP.processReturnRtTyre(retMrsTO);
                        }
                        System.out.println("return start---------");
                        mrsBP.processUpdateReturnTyre(retMrsTO);
                        System.out.println("return End---------");
                        continue;
                    }
                    if (retItemType[index].equals("NEW")) {
                        mrsBP.processNewItemsReturn(retMrsTO, userId);
                    } else {
                        mrsBP.processRcItemsReturn(retMrsTO, userId);
                    }

                    //Hari Start
                    if (rcWorkId != 0) {
                        System.out.println("Return type in handleMrsTyresIssue " + retItemType[index]);
                        retMrsTO.setType(retItemType[index]);
                        retMrsTO.setRcWorkId(rcWorkId);
                        retMrsTO.setRcItemsId(rcItemsId);
                        mrsBP.processReturnSpareItems(retMrsTO, userId);
                    }
                    //Hari End

                }

            }
            
            if (!"".equals(faultQty) && rcStatus.equals("Y")) {
                System.out.println("Rc case");
                mrsBP.processInsertRcQueue(mrsTO, userId);
            } else if (!"".equals(faultQty) && rcStatus.equals("N")) {
                System.out.println("scrap case");
                mrsBP.processScrapItems(mrsTO, companyId, userId);
            }
            request.setAttribute("counterId", request.getParameter("counterId"));
//            System.out.println("counterFlag-->" + request.getParameter("counterFlag"));
//            if (request.getParameter("counterFlag").equalsIgnoreCase("Y")) {
//                System.out.println("tax-->" + request.getParameter("tax"));
//                System.out.println("hikePercentage-->" + request.getParameter("hikePercentage"));
//                System.out.println("discount-->" + request.getParameter("discount"));
//                System.out.println("nettAmount-->" + request.getParameter("nettAmount"));
//                System.out.println("Amount-->" + request.getParameter("amount"));
//                tax = request.getParameter("tax");
//                hikePercentage = request.getParameter("hikePercentage");
//                discount = request.getParameter("discount");
//                nettAmount = request.getParameter("nettAmount");
//                amount = request.getParameter("amount");
//
//                counterId = request.getParameter("counterId");
//                //status = mrsBP.insertCounterMrsBillDetails(counterId,tax,hikePercentage,discount,nettAmount,userId,itemId,quantity,amount);
//                System.out.println("Counter Finished the job");
//            } else {
//                System.out.println("No Counter Bill Generated");
//            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleMrsTyresIssue in Ajax --> ").append(exception).toString());
        }
        return new ModelAndView(path);
    }

public void getItemDetails(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("i am in getItemDetails Ajax");
        MrsTO mrsTO = new MrsTO();
        HttpSession session = request.getSession();
        int companyId = 1;
        System.out.println((new StringBuilder()).append("request values=").append(request).toString());
        String itemDetails = "";
        try {
            companyId = Integer.parseInt((String) session.getAttribute("companyId"));
            System.out.println("in Rc issue " + companyId);
            System.out.println((new StringBuilder()).append("itemId=").append(request.getParameter("mrsItemId")).toString());
            String mfrCode = request.getParameter("mfrcode");
            String typeId = request.getParameter("typeId");
            String jcno = request.getParameter("jcno");

            if (typeId.equals("1011")) {
                itemDetails = mrsBP.getItemDetails(companyId,mfrCode);
            } else {
                itemDetails = mrsBP.getIssuedItemDetails(companyId,jcno,mfrCode);
            }
            PrintWriter writer = response.getWriter();
            System.out.println((new StringBuilder()).append("itemDetails size   :").append(itemDetails).toString());
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(itemDetails);
            writer.close();
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to retrieve PriceList data in Ajax --> ").append(exception).toString());
        }
    }
    public ModelAndView handleMrsRcItemsIssue(HttpServletRequest request, HttpServletResponse response) {
        int companyId = 0;
        MrsTO mrsTO = new MrsTO();
        MrsTO retMrsTO = new MrsTO();
        HttpSession session = request.getSession();
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        int userId = ((Integer) session.getAttribute("userId")).intValue();
        int index = 0;
        System.out.println("In Tyres Issue ");
        System.out.println((new StringBuilder()).append("userId").append(userId).toString());
        System.out.println((new StringBuilder()).append("request values=").append(request).toString());
        String path = "content/stores/refreshScript.jsp";
        int status = 0;
        System.out.println((new StringBuilder()).append("rc item ids test=").append(request.getParameter("rcItemIds")).toString());
        try {
            String mrsId = request.getParameter("mrsId");
            String itemId = request.getParameter("itemId");
            String tyreId = request.getParameter("tyreId");
            String rcItemId = request.getParameter("rcItemId");
            String selectedItem = request.getParameter("selectedItem");
            String actionType = "1011";
            String positionId = request.getParameter("positionId");
            String priceId = request.getParameter("price");

            String quantity = "1";
            String faultQty = request.getParameter("faultQty");
            String categoryId = request.getParameter("categoryId");
            String itemType = request.getParameter("itemType");
            System.out.println("IIIIIIIIIIIIIItem Type-->" + itemType);

            String retIssueId[] = request.getParameterValues("retIssueId");
            String retPriceId[] = request.getParameterValues("retPriceId");
            String retRcItemId[] = request.getParameterValues("retRcItemId");
            String retItemType[] = request.getParameterValues("retItemType");
            String retReturnedQty[] = request.getParameterValues("retReturnedQty");
            String retPrice[] = request.getParameterValues("retPrice");
            String retTyreId[] = request.getParameterValues("retTyreId");
            String retselectedIndex[] = request.getParameterValues("retselectedIndex");
            System.out.println((new StringBuilder()).append("price=").append(priceId).toString());
            System.out.println((new StringBuilder()).append("rc ItemId=").append(rcItemId).toString());
            System.out.println((new StringBuilder()).append("tyreId=").append(tyreId).toString());
            System.out.println((new StringBuilder()).append("faultQty = ").append(faultQty).toString());
            //Hari Start
            String type = "rc";
            String rcWork = request.getParameter("rcWorkId");
            String rcItems = request.getParameter("rcItemsId");
            String issued = request.getParameter("mrsItemId");

            System.out.println("rcWork123" + rcWork);
            System.out.println("rcItemsId123" + rcItems);
            System.out.println("mrsItemId123" + issued);

            int rcWorkId = Integer.parseInt(rcWork);
            int rcItemsId = Integer.parseInt(rcItems);
            int mrsItemId = Integer.parseInt(issued);
            int rcPriceWoId = 0;

            System.out.println("RCWorkId-->" + rcWorkId);

            if (request.getParameter("rcPriceWoId") != null && !"".equals(request.getParameter("rcPriceWoId"))) {
                rcPriceWoId = Integer.parseInt(request.getParameter("rcPriceWoId"));
            }

            System.out.println(" rcPriceWoId" + rcPriceWoId);

            mrsTO.setRcWorkId(rcWorkId);
            mrsTO.setRcItemsId(rcItemsId);
            mrsTO.setIssuedItemsId(mrsItemId);
            mrsTO.setRcPriceWoId(rcPriceWoId);
            //Hari End
            //For Counter Bill
            String tax = "";
            String hikePercentage = "";
            String discount = "";
            String nettAmount = "";
            String counterId = "";
            String amount = "";
            //End
            mrsTO.setMrsItemId(itemId);
            mrsTO.setMrsNumber(mrsId);
            mrsTO.setTyreId(tyreId);
            mrsTO.setRcItemIds(rcItemId);
            mrsTO.setActionType("1011");
            mrsTO.setPositionId(positionId);
            mrsTO.setCompanyId(companyId);
            mrsTO.setPriceIds(priceId);
            mrsTO.setFaultStatus("N");
            mrsTO.setFaultQty(faultQty);

            if (selectedItem != null) {
                if (categoryId.equals("1011")) {
                    mrsTO.setQuantityRI(-Float.parseFloat(quantity));
                    mrsBP.processRtTyreIssue(mrsTO, userId);
                    System.out.println("start------------------");
                    mrsBP.processMrsVehicleTyreUpdate(mrsTO, userId, companyId);
                    System.out.println("End------------------");
                } else if (!categoryId.equals("1011")) {
                    mrsTO.setQuantityRI(Float.parseFloat(quantity));
                    mrsBP.processRcMrsItems(mrsTO, userId);
                }

                //Hari
                if (rcWorkId != 0) {
                    mrsTO.setType(itemType);
                    mrsTO.setType(type);
                    mrsTO.setQty(quantity);
                    mrsTO.setCategoryId(categoryId);
                    mrsBP.updatePriceForSpareItems(mrsTO, userId);
                }
                //Hari End
            }
            if (retselectedIndex != null && retselectedIndex.length != 0) {
                for (int i = 0; i < retselectedIndex.length; i++) {
                    index = Integer.parseInt(retselectedIndex[i]);
                    retMrsTO = new MrsTO();
                    retMrsTO.setMrsItemId(itemId);
                    retMrsTO.setMrsNumber(mrsId);
                    retMrsTO.setCompanyId(companyId);
                    retMrsTO.setPriceIds(retPriceId[index]);
                    retMrsTO.setRcItemIds(retRcItemId[index]);
                    retMrsTO.setQuantityRI(Float.parseFloat(retReturnedQty[index]));
                    retMrsTO.setTyreId(retTyreId[index]);
                    retMrsTO.setIssueId(retIssueId[index]);
                    retMrsTO.setPositionId(positionId);
                    retMrsTO.setActionType("1012");
                    retMrsTO.setFaultStatus("N");
                    if (categoryId.equals("1011")) {
                        if (retItemType[index].equalsIgnoreCase("NEW")) {
                            System.out.println("tyre Disabled");
                            mrsBP.processReturnIssuedTyre(retMrsTO);
                        } else {
                            mrsBP.processUpdateReturnTyre(retMrsTO);
                        }
                        System.out.println("return start---------");
                        mrsBP.processUpdateReturnTyre(retMrsTO);
                        System.out.println("return End---------");
                        continue;
                    }
                    if (retItemType[index].equalsIgnoreCase("NEW")) {
                        mrsBP.processNewItemsReturn(retMrsTO, userId);
                    } else {
                        mrsBP.processRcItemsReturn(retMrsTO, userId);
                    }

                    //Hari Start
                    if (rcWorkId != 0) {
                        System.out.println("Return type in handleMrsRcItemsIssue " + retItemType[index]);
                        retMrsTO.setType(retItemType[index]);
                        retMrsTO.setRcWorkId(rcWorkId);
                        retMrsTO.setRcItemsId(rcItemsId);
                        mrsBP.processReturnSpareItems(retMrsTO, userId);
                    }
                    //Hari End
                }

            }
            if (!"".equals(faultQty)) {
                mrsBP.processInsertRcQueue(mrsTO, userId);
            }
            System.out.println("In Rc Item counterFlag-->" + request.getParameter("counterFlag"));
            request.setAttribute("counterId", request.getParameter("counterId"));
//            if (request.getParameter("counterFlag").equalsIgnoreCase("Y")) {
//                System.out.println("tax-->" + request.getParameter("tax"));
//                System.out.println("hikePercentage-->" + request.getParameter("hikePercentage"));
//                System.out.println("discount-->" + request.getParameter("discount"));
//                System.out.println("nettAmount-->" + request.getParameter("nettAmount"));
//                tax = request.getParameter("tax");
//                hikePercentage = request.getParameter("hikePercentage");
//                discount = request.getParameter("discount");
//                nettAmount = request.getParameter("nettAmount");
//                amount = request.getParameter("amount");
//
//                counterId = request.getParameter("counterId");
//                status = mrsBP.insertCounterMrsBillDetails(counterId,tax,hikePercentage,discount,nettAmount,userId,itemId,quantity,amount);
//                System.out.println("Rc Counter Finished the job");
//            } else {
//                System.out.println("No Rc Counter Bill Generated");
//            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleMrsTyresIssue in Ajax --> ").append(exception).toString());
        }
        return new ModelAndView(path);
    }

    public void handleNewTyresStock(HttpServletRequest request, HttpServletResponse response, MrsCommand command) {
        System.out.println("in Ajax handleNewTyresStock");
        mrsCommand = command;
        MrsTO mrsTO = new MrsTO();
        HttpSession session = request.getSession();
        int companyId = 0;
        int itemId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        System.out.println((new StringBuilder()).append("request values=").append(request).toString());
        String tyres = "";
        try {
            System.out.println("in handleNewTyresStock");
            itemId = Integer.parseInt(request.getParameter("mrsItemId"));
            tyres = mrsBP.processNewTyresList(itemId, companyId);
            PrintWriter writer = response.getWriter();
            System.out.println((new StringBuilder()).append("tyres are   :").append(tyres).toString());
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(tyres);
            writer.close();
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleNewTyresStock data in Ajax --> ").append(exception).toString());
        }
    }

    public ModelAndView generateMrs(HttpServletRequest request, HttpServletResponse response, MrsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList jobCardlist = new ArrayList();
        ArrayList mrsList = new ArrayList();
        HttpSession session = request.getSession();
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String menuPath = "";
        String pageTitle = "";
        try {
            int userId = ((Integer) session.getAttribute("userId")).intValue();
            int jobCardId = Integer.parseInt(request.getParameter("mrsJobCardNumber"));
            int technicianId = Integer.parseInt(request.getParameter("technicianId"));
            String remarks = request.getParameter("remarks");
            String manualMrsDate = "";
            System.out.println("Remarks-->" + remarks + "ManualMrsDate" + manualMrsDate);
            String temp1[] = request.getParameterValues("hiddenElement1");
            String temp2[] = request.getParameterValues("hiddenElement2");
            String position[] = request.getParameterValues("hiddenElement3");
            int itemId = 0;
            float qty = 0.0F;
            int mrsId = 0;
            int status = 0;
            int posId = 0;
            //Hari
            int rcWorkId = 0;
            int rcItemsId = 0;
            int counterId = 0;
            int custId = 0;
            int spId = 0;
            ArrayList tyrePostions = new ArrayList();
            MrsTO mrsTO = new MrsTO();
            if (command.getReqFor() != null && !"".equals(command.getReqFor())) {
                pageTitle = "Genearte Counter MRS";
                if (command.getCustomerName() != null && !"".equals(command.getCustomerName())) {
                    mrsTO.setName(command.getCustomerName());
                }
                if (command.getAddress() != null && !"".equals(command.getAddress())) {
                    mrsTO.setAddress(command.getAddress());
                }
                if (command.getDate() != null && !"".equals(command.getDate())) {
                    mrsTO.setDate(command.getDate());
                    manualMrsDate = command.getDate();
                }
                if (command.getMobileNo() != null && !"".equals(command.getMobileNo())) {
                    mrsTO.setMobileNo(command.getMobileNo());
                }
                if (command.getRemarks() != null && !"".equals(command.getRemarks())) {
                    mrsTO.setRemarks(command.getRemarks());
                }
                mrsTO.setUserId(userId);
                mrsTO.setCompanyId(companyId);

                menuPath = "Stores >> Genearte Counter Sale MRS ";
                request.setAttribute("pageTitle", pageTitle);
                request.setAttribute("menuPath", menuPath);
                counterId = mrsBP.insertCounterMrs(mrsTO);
                System.out.println("CounterId" + counterId);
                mrsId = mrsBP.insertMRS(0, 0, 0, counterId, remarks,companyId, manualMrsDate, userId);
            } else {
                pageTitle = "Genearte MRS";
                menuPath = "Render Service >>Genearte MRS ";
                request.setAttribute("pageTitle", pageTitle);
                request.setAttribute("menuPath", menuPath);
                manualMrsDate = request.getParameter("manualMrsDate");
                mrsId = mrsBP.insertMRS(technicianId, jobCardId, rcWorkId, counterId, remarks,companyId, manualMrsDate, userId);
            }
            System.out.println((new StringBuilder()).append("mrsId").append(mrsId).toString());
            for (int i = 0; i < temp1.length; i++) {
                System.out.println((new StringBuilder()).append("temp1[i]=").append(request.getParameter(temp1[i])).toString());
                if (request.getParameter(temp1[i]) != null && !request.getParameter(temp1[i]).equals("")) {
                    itemId = Integer.parseInt(request.getParameter(temp1[i]));
                    qty = Float.parseFloat(request.getParameter(temp2[i]));
                    posId = Integer.parseInt(request.getParameter(position[i]));
                    status = mrsBP.insertMrsItems(itemId, qty, mrsId, posId, rcItemsId, userId);
                }
            }

            //Redirect Only For Counter Sale Bill
            if (counterId != 0) {

                tyrePostions = mrsBP.getTyrePostions();
                request.setAttribute("tyrePostions", tyrePostions);
                request.setAttribute("successMessage", "Counter Mrs Generated Successfully -"+counterId);
                path = "content/counter/generateCounterBill.jsp";
            } else {
                jobCardlist = mrsBP.getJobCardList(companyId);
                request.setAttribute("jobCardlist", jobCardlist);
                request.setAttribute("mrsList", mrsList);
                request.setAttribute("successMessage", "MRS No:"+mrsId+" Generated Successfully");
                path = "content/RenderService/chooseJobCard.jsp";
            }

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to view Issue --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView getMrsDetails(HttpServletRequest request, HttpServletResponse response, MrsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        mrsCommand = command;
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Genearte MRS";
        menuPath = "Render Service >>Genearte MRS ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        try {
            String status = request.getParameter("status");
            if (status.equals("Hold") || status.equals("Requested")) {
                path = "content/RenderService/alterMrs.jsp";
            } else if (status.equals("Approved") || status.equalsIgnoreCase("Rejected")) {
                path = "content/RenderService/viewMrs.jsp";
            }
            int jobCardId = Integer.parseInt(request.getParameter("mrsJobCardNumber"));
            int mrsId = Integer.parseInt(request.getParameter("mrsId"));
            int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
            ArrayList mrsDetails = new ArrayList();
            mrsDetails = mrsBP.getMrsItems(mrsId, status);

            ArrayList vehicleDetails = new ArrayList();
            ArrayList technicians = new ArrayList();
            vehicleDetails = mrsBP.getVehicleDetails(jobCardId);

            technicians = mrsBP.jobCardtechList(companyId, jobCardId);

            Iterator itrP = mrsDetails.iterator();
            String manualMrsNo = "";
            if (itrP.hasNext()) {
                MrsTO mrs = (MrsTO) itrP.next();
                manualMrsNo = mrs.getMrsApprovalRemarks();
            }
            request.setAttribute("manualMrsNo", manualMrsNo);
            request.setAttribute("vehicleDetails", vehicleDetails);
            request.setAttribute("technicians", technicians);
            request.setAttribute("mrsDetails", mrsDetails);
            request.setAttribute("mrsId", Integer.valueOf(mrsId));
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to view Issue --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView updateMrsDetails(HttpServletRequest request, HttpServletResponse response, MrsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        mrsCommand = command;
        HttpSession session = request.getSession();
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String menuPath = "";
        String pageTitle = "Genearte MRS";
        menuPath = "Render Service >>Alter MRS ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/RenderService/alterMrs.jsp";
            ArrayList jobCardlist = new ArrayList();
            int mrsId = Integer.parseInt(request.getParameter("mrsId"));
            String selectedIndex[] = request.getParameterValues("selectedindex");
            String qty[] = request.getParameterValues("qty");
            String action[] = request.getParameterValues("option");
            String itemId[] = request.getParameterValues("itemId");
            String catId[] = request.getParameterValues("catId");
            int updateStatus = 0;
            updateStatus = mrsBP.updateMrsItems(mrsId, itemId, selectedIndex, qty, action, catId);
            jobCardlist = mrsBP.getJobCardList(companyId);
            request.setAttribute("jobCardlist", jobCardlist);
            request.removeAttribute("mrsList");
            path = "content/RenderService/chooseJobCard.jsp";
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to view Issue --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView printMrs(HttpServletRequest request, HttpServletResponse response, MrsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList mrsDetail = new ArrayList();
        ArrayList mrsTyreDetail = new ArrayList();
        ArrayList vehicleDetail = new ArrayList();
        HttpSession session = request.getSession();
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        int mrsId = Integer.parseInt(request.getParameter("mrsNumber"));
        System.out.println("view Issue");
        String menuPath = "";
        String pageTitle = "View Issue";
        menuPath = "View MRS  >>Issue ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Mrs-IssueList")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "content/stores/mrsSlip.jsp";
                mrsDetail = mrsBP.processMrsDetails(mrsId, companyId);
                vehicleDetail = mrsBP.processVehicleDetails(mrsId);
                request.setAttribute("vehicleDetails", vehicleDetail);
                System.out.println((new StringBuilder()).append("vehicleDetail size").append(vehicleDetail.size()).toString());
                request.setAttribute("mrsDetails", mrsDetail);
            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to view Issue --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //Hari Start
//handleGenerateCounterPage
    public ModelAndView handleGenerateCounterPage(HttpServletRequest request, HttpServletResponse response, MrsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        mrsCommand = command;
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Genearte MRS";
        menuPath = "Stores >>Generate Counter Mrs";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        ArrayList tyrePostions = new ArrayList();
        try {

            tyrePostions = mrsBP.getTyrePostions();
            request.setAttribute("tyrePostions", tyrePostions);
            path = "content/counter/generateCounterBill.jsp";
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to view Issue --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
}
