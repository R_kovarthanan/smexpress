// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3)
// Source File Name:   MrsBP.java
package ets.domain.mrs.business;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.mrs.data.MrsDAO;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;

// Referenced classes of package ets.domain.mrs.business:
//            MrsTO
public class MrsBP {

    public MrsBP() {
    }

    public MrsDAO getMrsDAO() {
        return mrsDAO;
    }

    public void setMrsDAO(MrsDAO mrsDAO) {
        this.mrsDAO = mrsDAO;
    }

    public ArrayList processApprovalList(MrsTO mrsTO, String fromDate, String toDate)
            throws FPBusinessException, FPRuntimeException {
        ArrayList mrsList = new ArrayList();
        mrsList = mrsDAO.getApprovalList(mrsTO, fromDate, toDate);
        if (mrsList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        } else {
            return mrsList;
        }
    }
    public String getItemDetails(int compId, String mfrCode)
            throws FPBusinessException, FPRuntimeException {
        ArrayList itemDetailList = new ArrayList();
        String details = "";
        MrsTO mrsTO = new MrsTO();
        int counter = 0;
        itemDetailList = mrsDAO.getItemDetails(compId, mfrCode);
        if (itemDetailList.size() == 0) {
            details = "";
        } else {
            for (Iterator itr = itemDetailList.iterator(); itr.hasNext();) {
                mrsTO = new MrsTO();
                mrsTO = (MrsTO) itr.next();

                details = mrsTO.getMrsItemId()+"~"+mrsTO.getMfrCode()+"~"+mrsTO.getItemName()+"~"+
                            mrsTO.getPrice()+"~"+mrsTO.getTax()+"~"+mrsTO.getNewStock();

            }

        }
        return details;
    }
    public String getIssuedItemDetails(int compId,String jcno,String mfrCode)
            throws FPBusinessException, FPRuntimeException {
        ArrayList itemDetailList = new ArrayList();
        String details = "";
        MrsTO mrsTO = new MrsTO();
        int counter = 0;
        itemDetailList = mrsDAO.getIssuedItemDetails(compId,jcno,mfrCode);
        if (itemDetailList.size() == 0) {
            details = "";
        } else {
            for (Iterator itr = itemDetailList.iterator(); itr.hasNext();) {
                mrsTO = new MrsTO();
                mrsTO = (MrsTO) itr.next();
                details = mrsTO.getMrsItemId()+"~"+mrsTO.getMfrCode()+"~"+mrsTO.getItemName()+"~"+
                            mrsTO.getPrice()+"~"+mrsTO.getTax()+"~"+mrsTO.getNewStock()+"~"+mrsTO.getMrsIssueQuantity();
            }

        }
        return details;
    }
    public ArrayList processVehicleDetails(int mrsId)
            {
        ArrayList vehicleDetails = new ArrayList();
        vehicleDetails = mrsDAO.getVehicleDetails(mrsId);
//        if (vehicleDetails.size() == 0) {
//          //  throw new FPBusinessException("EM-VEH-01");
//        } else {
//            return vehicleDetails;
//        }
        return vehicleDetails;
    }

    public ArrayList processMrsDetails(int mrsId, int companyId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList mrsDetails = new ArrayList();
        String mrsRCItems = "";
        String mrsOtherReq = "";
        String otherServicePointQuantity = "";
        String localServicePointQuantity = "";
        String totalIssued = "";
        String price = "";
        String tax = "0.00";
        int totalTyresIssued = 0;
        int counterId=0;
        float vendorPoQty = 0.0F;
        float localPoQty = 0.0F;
        String faultQty = "";
        ArrayList filteredList = new ArrayList();
        ArrayList poRaisedQty = new ArrayList();
        ArrayList tyreList = new ArrayList();
        int item_id = 0;
        MrsTO detailTO = new MrsTO();
        System.out.println("-----------------");
        mrsDetails = mrsDAO.getMrsDetails(mrsId);
        if (mrsDetails.size() == 0) {
            throw new FPBusinessException("EM-MRS-01");
        }
        System.out.println((new StringBuilder()).append("mrs items size=").append(mrsDetails.size()).toString());
        Iterator itr = mrsDetails.iterator();
        MrsTO mrsTO = null;
        MrsTO filterTo;
        for (; itr.hasNext(); filteredList.add(filterTo)) {
            System.out.println("filtered Loop");
            mrsTO = new MrsTO();
            mrsTO = (MrsTO) itr.next();
            filterTo = new MrsTO();
            counterId = mrsTO.getCounterId();
            item_id = Integer.parseInt(mrsTO.getMrsItemId());
            mrsRCItems = mrsDAO.getLocalCenterRcItemsStock(item_id, companyId);
            mrsOtherReq = mrsDAO.getOtherMrsRequest(mrsId, item_id);
            mrsTO.setMrsNumber(String.valueOf(mrsId));
            faultQty = mrsDAO.getFaultItemsReceived(mrsTO);
            filterTo.setApprovedQty(mrsTO.getApprovedQty());
            filterTo.setQty(mrsRCItems);
            filterTo.setCount(mrsOtherReq);
            if (mrsTO.getCategoryId().equals("1011")) {
                totalTyresIssued = mrsDAO.getTotalTyresIssued(mrsId, item_id, Integer.parseInt(mrsTO.getPositionId()));
                totalIssued = String.valueOf(totalTyresIssued);
                //For Counter Sale Only
                if(counterId!=0)
                {
                price = mrsDAO.getTotalIssuedQuantityPrice(mrsId, item_id);
                if(Float.parseFloat(price)!=0.00 || Float.parseFloat(price)!=0)
                tax = mrsDAO.getTotalIssuedQuantityTax(mrsId, item_id);
                System.out.println("Price Calculated"+item_id+"Price-->"+price+"tax-->"+tax);
                }
            } else {
                totalIssued = mrsDAO.getTotalIssuedQuantity(mrsId, item_id);
                //For Counter Sale Only
                if(counterId!=0)
                {
                price = mrsDAO.getTotalIssuedQuantityPrice(mrsId, item_id);
                if(Float.parseFloat(price)!=0.00 || Float.parseFloat(price)!=0)
                tax = mrsDAO.getTotalIssuedQuantityTax(mrsId, item_id);
                System.out.println("Price Calculated"+item_id+"Price-->"+price+"tax-->"+tax);
                }
            }
            MrsTO puchTO = new MrsTO();
            puchTO.setMrsItemId(String.valueOf(item_id));
            puchTO.setCompanyId(companyId);
            poRaisedQty = mrsDAO.getPoRaisedQty(puchTO);
            Iterator poItr = poRaisedQty.iterator();
            do {
                if (!poItr.hasNext()) {
                    break;
                }
                MrsTO puchTO1 = new MrsTO();
                puchTO1 = (MrsTO) poItr.next();
                if (puchTO1.getPurchaseType().equals("1011")) {
                    localPoQty = Float.parseFloat(puchTO1.getQty());
                } else if (puchTO1.getPurchaseType().equals("1012")) {
                    vendorPoQty = Float.parseFloat(puchTO1.getQty());
                }
            } while (true);
            filterTo.setFaultQty(faultQty);
            filterTo.setTotalIssued(totalIssued);
            if(counterId!=0)
            {
            filterTo.setPrice(price);
            filterTo.setTax(Float.parseFloat(tax));
            }
            filterTo.setMrsPaplCode(mrsTO.getMrsPaplCode());
            filterTo.setMrsItemMfrCode(mrsTO.getMrsItemMfrCode());
            filterTo.setMrsItemName(mrsTO.getMrsItemName());
            filterTo.setRcStatus(mrsTO.getRcStatus());
            filterTo.setCategoryId(mrsTO.getCategoryId());
            filterTo.setMrsItemQuantity(mrsTO.getMrsItemQuantity());
            filterTo.setMrsRequestedItemNumber(mrsTO.getMrsRequestedItemNumber());
            filterTo.setLocalPoQty(String.valueOf(localPoQty));
            filterTo.setVendorPoQty(String.valueOf(vendorPoQty));
            float poQty = localPoQty + vendorPoQty;
            System.out.println((new StringBuilder()).append("poqty").append(poQty).toString());
            filterTo.setPoQuantity(String.valueOf(poQty));
            //Hari
            filterTo.setRcItemsId(mrsTO.getRcItemsId());
            //Hari End
            filterTo.setMrsItemId(mrsTO.getMrsItemId());
            filterTo.setMrsNumber(mrsTO.getMrsNumber());
            filterTo.setUomName(mrsTO.getUomName());
            filterTo.setPositionId(mrsTO.getPositionId());
            filterTo.setPositionName(mrsTO.getPositionName());
            otherServicePointQuantity = mrsDAO.getOtherServicePointQuantity(item_id, companyId);
            filterTo.setMrsOtherServiceItemSum(otherServicePointQuantity);
            localServicePointQuantity = mrsDAO.getLocalServicePointQuantity(item_id, companyId);
            filterTo.setMrsLocalServiceItemSum(localServicePointQuantity);
        }

        if (filteredList.size() == 0) {
            throw new FPBusinessException("EM-MRS-01");
        } else {
            System.out.println("AM return Filtered List now()");
            return filteredList;
        }
    }

    public ArrayList processMrsTyreDetails(int mrsId, int companyId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList mrsDetails = new ArrayList();
        String mrsRCItems = "";
        String mrsOtherReq = "";
        String otherServicePointQuantity = "";
        String localServicePointQuantity = "";
        int totalIssued = 0;
        float vendorPoQty = 0.0F;
        float localPoQty = 0.0F;
        int faultQty = 0;
        ArrayList filteredList = new ArrayList();
        ArrayList poRaisedQty = new ArrayList();
        ArrayList tyreList = new ArrayList();
        int item_id = 0;
        MrsTO detailTO = new MrsTO();
        System.out.println("-----------------");
        mrsDetails = mrsDAO.getMrsApprovedTyres(mrsId);
        if (mrsDetails.size() != 0);
        System.out.println((new StringBuilder()).append("mrs tyre items size=").append(mrsDetails.size()).toString());
        Iterator itr = mrsDetails.iterator();
        MrsTO mrsTO = null;
        MrsTO filterTo;
        for (; itr.hasNext(); filteredList.add(filterTo)) {
            mrsTO = new MrsTO();
            mrsTO = (MrsTO) itr.next();
            filterTo = new MrsTO();
            item_id = Integer.parseInt(mrsTO.getMrsItemId());
            mrsRCItems = mrsDAO.getLocalCenterRcItemsStock(item_id, companyId);
            totalIssued = mrsDAO.getTotalTyresIssued(mrsId, item_id, Integer.parseInt(mrsTO.getPositionId()));
            otherServicePointQuantity = mrsDAO.getOtherServicePointQuantity(item_id, companyId);
            localServicePointQuantity = mrsDAO.getLocalServicePointQuantity(item_id, companyId);
            mrsOtherReq = mrsDAO.getOtherMrsRequest(mrsId, item_id);
            MrsTO puchTO = new MrsTO();
            puchTO.setMrsItemId(String.valueOf(item_id));
            puchTO.setCompanyId(companyId);
            poRaisedQty = mrsDAO.getPoRaisedQty(puchTO);
            Iterator poItr = poRaisedQty.iterator();
            do {
                if (!poItr.hasNext()) {
                    break;
                }
                MrsTO puchTO1 = new MrsTO();
                puchTO1 = (MrsTO) poItr.next();
                if (puchTO1.getPurchaseType().equals("1011")) {
                    localPoQty = Float.parseFloat(puchTO1.getQty());
                } else if (puchTO1.getPurchaseType().equals("1012")) {
                    vendorPoQty = Float.parseFloat(puchTO1.getQty());
                }
            } while (true);
            mrsTO.setMrsNumber(String.valueOf(mrsId));
            filterTo.setMrsItemMfrCode(mrsTO.getMrsItemMfrCode());
            filterTo.setMrsPaplCode(mrsTO.getMrsPaplCode());
            filterTo.setMrsItemId(mrsTO.getMrsItemId());
            filterTo.setMrsItemName(mrsTO.getMrsItemName());
            filterTo.setCategoryId(mrsTO.getCategoryId());
            filterTo.setPositionId(mrsTO.getPositionId());
            filterTo.setPositionName(mrsTO.getPositionName());
            filterTo.setRcStatus(mrsTO.getRcStatus());
            filterTo.setUomName(mrsTO.getUomName());
            filterTo.setMrsRequestedItemNumber("1");
            filterTo.setApprovedQty("1");
            filterTo.setMrsItemQuantity(mrsTO.getMrsItemQuantity());
            filterTo.setCount(mrsOtherReq);
            filterTo.setQty(mrsRCItems);
            filterTo.setTotalIssued(String.valueOf(totalIssued));
            filterTo.setLocalPoQty(String.valueOf(localPoQty));
            filterTo.setVendorPoQty(String.valueOf(vendorPoQty));
            filterTo.setMrsOtherServiceItemSum(otherServicePointQuantity);
            filterTo.setMrsLocalServiceItemSum(localServicePointQuantity);
        }

        if (filteredList.size() == 0) {
            throw new FPBusinessException("EM-MRS-01");
        } else {
            return filteredList;
        }
    }

    public int processAddMrsApprovedQuantity(int userId, MrsTO mrsTO, ArrayList List)
            throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = mrsDAO.doAddMrsApprovedQuantity(userId, mrsTO, List);
        if (status == 0) {
            throw new FPBusinessException("EM-MRS-02");
        } else {
            return status;
        }
    }

    public ArrayList processOtherServicePointDetails(int ItemId, int companyId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList otherServicePointDetails = new ArrayList();
        ArrayList RcStockDetails = new ArrayList();
        ArrayList newStockDetails = new ArrayList();
        MrsTO mrsTo = new MrsTO();
        MrsTO rcTO = new MrsTO();
        RcStockDetails = mrsDAO.getRcStockDetails(ItemId, companyId);
        newStockDetails = mrsDAO.getNewStockDetails(ItemId, companyId);
        label0:
        for (Iterator newItr = newStockDetails.iterator(); newItr.hasNext(); otherServicePointDetails.add(mrsTo)) {
            mrsTo = (MrsTO) newItr.next();
            Iterator rcItr = RcStockDetails.iterator();
            do {
                if (!rcItr.hasNext()) {
                    continue label0;
                }
                rcTO = (MrsTO) rcItr.next();
            } while (!mrsTo.getMrsItemId().equals(rcTO.getMrsItemId()));
            mrsTo.setRcStock(rcTO.getRcStock());
        }

        if (otherServicePointDetails.size() == 0) {
            throw new FPBusinessException("EM-MRS-01");
        } else {
            return otherServicePointDetails;
        }
    }

    public ArrayList processViewMrsList(int companyId, String fromDate, String toDate)
            throws FPBusinessException, FPRuntimeException {
        ArrayList mrsList = new ArrayList();
        mrsList = mrsDAO.getMrsList(companyId, fromDate, toDate);
        if (mrsList.size() != 0);
        return mrsList;
    }
    public ArrayList processServicePtList()
            throws FPBusinessException, FPRuntimeException {
        ArrayList servicePtList = new ArrayList();
        servicePtList = mrsDAO.processServicePtList();
        if (servicePtList.size() != 0);
        return servicePtList;
    }

    public String processPriceList(int itemType, MrsTO mrsTO)
            throws FPBusinessException, FPRuntimeException {
        ArrayList priceList = new ArrayList();
        String price = "";
        int counter = 0;
        priceList = mrsDAO.getNewPriceList(mrsTO);
        if (priceList.size() == 0) {
            price = "";
        } else {
            for (Iterator itr = priceList.iterator(); itr.hasNext();) {
                mrsTO = new MrsTO();
                mrsTO = (MrsTO) itr.next();
                if (counter == 0) {
                    System.out.println((new StringBuilder()).append(" mrsTO.getPrice()").append(mrsTO.getPrice()).toString());
                    System.out.println((new StringBuilder()).append("mrsTO.getPriceId()").append(mrsTO.getPriceId()).toString());
                    price = (new StringBuilder()).append(mrsTO.getPriceId()).append("-").append(mrsTO.getPrice()).append("-").append(mrsTO.getQty()).append("-").append(mrsTO.getPriceType()).toString();
                    counter++;
                } else {
                    price = (new StringBuilder()).append(price).append("~").append(mrsTO.getPriceId()).append("-").append(mrsTO.getPrice()).append("-").append(mrsTO.getQty()).append("-").append(mrsTO.getPriceType()).toString();
                }
            }

        }
        return price;
    }

    public String processRcPriceList(MrsTO mrsTO)
            throws FPBusinessException, FPRuntimeException {
        ArrayList priceList = new ArrayList();
        String price = "";
        int counter = 0;
        priceList = mrsDAO.getRcPriceList(mrsTO);
        if (priceList.size() == 0) {
            price = "";
        } else {
            for (Iterator itr = priceList.iterator(); itr.hasNext();) {
                mrsTO = new MrsTO();
                mrsTO = (MrsTO) itr.next();
                if (counter == 0) {
                    price = (new StringBuilder()).append(mrsTO.getPrice()).append("-").append(mrsTO.getRcItemId()).append("-").append(mrsTO.getRcWorkId()).toString();
                    counter++;
                } else {
                    System.out.println((new StringBuilder()).append("mrsTO.getRcItemId()").append(mrsTO.getRcItemId()).toString());
                    price = (new StringBuilder()).append(price).append("~").append(mrsTO.getPrice()).append("-").append(mrsTO.getRcItemId()).append("-").append(mrsTO.getRcWorkId()).toString();
                }
            }

        }
        return price;
    }

    public String processRtTyrePriceList(MrsTO mrsTO)
            throws FPBusinessException, FPRuntimeException {
        ArrayList priceList = new ArrayList();
        String price = "";
        int counter = 0;
        System.out.println("In Bp");
        priceList = mrsDAO.getRtTyrePriceList(mrsTO);
        if (priceList.size() == 0) {
            price = "";
        } else {
            for (Iterator itr = priceList.iterator(); itr.hasNext();) {
                mrsTO = new MrsTO();
                mrsTO = (MrsTO) itr.next();
                if (counter == 0) {
                    price = (new StringBuilder()).append(mrsTO.getPrice()).append("-").append(mrsTO.getRcItemId()).append("-").append(mrsTO.getTyreNo()).append("-").append(mrsTO.getRcWorkId()).toString();
                    counter++;
                } else {
                    System.out.println((new StringBuilder()).append("mrsTO.getRcItemId()").append(mrsTO.getRcItemId()).toString());
                    price = (new StringBuilder()).append(price).append("~").append(mrsTO.getPrice()).append("-").append(mrsTO.getRcItemId()).append("-").append(mrsTO.getTyreNo()).append("-").append(mrsTO.getRcWorkId()).toString();
                }
            }

        }
        return price;
    }

    public String processVehicleRcItems(MrsTO mrsTo)
            throws FPBusinessException, FPRuntimeException {
        ArrayList itemList = new ArrayList();
        String rcItems = "";
        int counter = 0;
        MrsTO itemTO = new MrsTO();
        itemList = mrsDAO.getVehicleRcItems(mrsTo);
        if (itemList.size() != 0) {
            for (Iterator itr = itemList.iterator(); itr.hasNext();) {
                itemTO = (MrsTO) itr.next();
                if (counter == 0) {
                    rcItems = itemTO.getRcItemId();
                    counter++;
                } else {
                    rcItems = (new StringBuilder()).append(rcItems).append("~").append(itemTO.getRcItemId()).toString();
                }
            }

        } else {
            rcItems = "";
        }
        return rcItems;
    }

    public ArrayList processMrsSummary(MrsTO mrsTO, int companyId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList issuedQty = new ArrayList();
        ArrayList poRaisedQty = new ArrayList();
        ArrayList mrsItems = new ArrayList();
        ArrayList mrsItemSummary = new ArrayList();
        String totalRequested = "";
        float totalIssued = 0.0F;
        float totalRequiredQty = 0.0F;
        float localPoQty = 0.0F;
        float vendorPoQty = 0.0F;
        String otherServicePointQuantity = "";
        String localServicePointQuantity = "";
        String rcQuantity = "";
        MrsTO mrs = null;
        MrsTO mrsItemsTo = null;
        MrsTO ItemSummaryTO = null;
        mrsItems = mrsDAO.getMultipleMrsItems(mrsTO);
        mrsTO.setCompanyId(companyId);
        System.out.println((new StringBuilder()).append("in mrsbp mrsItems = ").append(mrsItems.size()).toString());
        for (Iterator items = mrsItems.iterator(); items.hasNext();) {
            mrsItemsTo = new MrsTO();
            mrsItemsTo = (MrsTO) items.next();
            mrsTO.setMrsItemId(mrsItemsTo.getMrsItemId());
            System.out.println((new StringBuilder()).append("mrsId-->").append(mrsTO.getMrsId().length).toString());
            ItemSummaryTO = new MrsTO();
            totalRequested = mrsDAO.getSelectedMrsReqQuantity(mrsTO);
            issuedQty = mrsDAO.getIssuedQty4SelecMrs(mrsTO);
            System.out.println((new StringBuilder()).append("issuedQty --> ").append(issuedQty.size()).toString());
            Iterator itr;
            for (itr = issuedQty.iterator(); itr.hasNext();) {
                mrs = new MrsTO();
                mrs = (MrsTO) itr.next();
                totalIssued += Float.parseFloat(mrs.getQty());
            }

            totalRequiredQty = Float.parseFloat(totalRequested) - totalIssued;
            System.out.println((new StringBuilder()).append("itemId=").append(mrsItemsTo.getMrsItemId()).toString());
            System.out.print((new StringBuilder()).append("--totalIssued=").append(totalIssued).toString());
            System.out.print((new StringBuilder()).append("--totalRequested=").append(totalRequested).toString());
            System.out.print((new StringBuilder()).append("--totalRequiredQty=").append(totalRequiredQty).toString());
            poRaisedQty = mrsDAO.getPoRaisedQty(mrsTO);
            itr = poRaisedQty.iterator();
            do {
                if (!itr.hasNext()) {
                    break;
                }
                mrs = new MrsTO();
                mrs = (MrsTO) itr.next();
                if (mrs.getPurchaseType().equals("1011")) {
                    localPoQty = Float.parseFloat(mrs.getQty());
                } else if (mrs.getPurchaseType().equals("1012")) {
                    vendorPoQty = Float.parseFloat(mrs.getQty());
                }
            } while (true);
            otherServicePointQuantity = mrsDAO.getOtherServicePointQuantity(Integer.parseInt(mrsTO.getMrsItemId()), companyId);
            localServicePointQuantity = mrsDAO.getLocalServicePointQuantity(Integer.parseInt(mrsTO.getMrsItemId()), companyId);
            System.out.println((new StringBuilder()).append("mrs Item ID = ").append(mrsTO.getMrsItemId()).toString());
            rcQuantity = mrsDAO.getLocalCenterRcItemsStock(Integer.parseInt(mrsTO.getMrsItemId()), companyId);
            ItemSummaryTO.setMrsItemId(mrsItemsTo.getMrsItemId());
            ItemSummaryTO.setMrsItemMfrCode(mrsItemsTo.getMrsItemMfrCode());
            ItemSummaryTO.setMrsPaplCode(mrsItemsTo.getMrsPaplCode());
            ItemSummaryTO.setMrsItemName(mrsItemsTo.getMrsItemName());
            ItemSummaryTO.setMrsLocalServiceItemSum(localServicePointQuantity);
            ItemSummaryTO.setRcQty(rcQuantity);
            ItemSummaryTO.setMrsOtherServiceItemSum(otherServicePointQuantity);
            ItemSummaryTO.setRequiredQty(String.valueOf(totalRequiredQty));
            ItemSummaryTO.setLocalPoQty(String.valueOf(localPoQty));
            ItemSummaryTO.setVendorPoQty(String.valueOf(vendorPoQty));
            mrsItemSummary.add(ItemSummaryTO);
            totalRequested = "";
            totalIssued = 0.0F;
            totalRequiredQty = 0.0F;
        }

        if (mrsItemSummary.size() == 0) {
            throw new FPBusinessException("EM-MRS-01");
        } else {
            return mrsItemSummary;
        }
    }

    public ArrayList processVendorsForItem(MrsTO mrsTO)
            throws FPBusinessException, FPRuntimeException {
        ArrayList itemVendors = new ArrayList();
        itemVendors = mrsDAO.getVendorsForItems(mrsTO);
        return itemVendors;
    }

    public ArrayList getJobCardList(int companyId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList tempList = new ArrayList();
        tempList = mrsDAO.getJobCardList(companyId);
        if (tempList.size() != 0);
        return tempList;
    }

    public ArrayList getJobCardNoVehNo(int companyId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList tempList = new ArrayList();
        tempList = mrsDAO.getJobCardNoVehNo(companyId);
        if (tempList.size() != 0);
        return tempList;
    }

    public ArrayList getVehicleDetails(int jobCardId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList tempList = new ArrayList();
        tempList = mrsDAO.vehicleDetails(jobCardId);
        if (tempList.size() != 0);
        return tempList;
    }

    public ArrayList techniciansList(int compId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList tempList = new ArrayList();
        tempList = mrsDAO.techniciansList(compId);
        if (tempList.size() != 0);
        return tempList;
    }

    public ArrayList jobCardtechList(int companyId, int jobCardId) {
        ArrayList tempList = new ArrayList();
        tempList = mrsDAO.jobCardtechList(companyId, jobCardId);
        if (tempList.size() != 0);
        return tempList;
    }

    public String getSuggestions(String mfrCode, String itemCode, String itemName) {
        String values = "";
        if (mfrCode != "" && mfrCode != null) {
            mfrCode = (new StringBuilder()).append(mfrCode).append("%").toString();
            values = mrsDAO.mfrCodeSuggestions(mfrCode);
        } else if (itemCode != "" && itemCode != null) {
            itemCode = (new StringBuilder()).append(itemCode).append("%").toString();
            values = mrsDAO.itemCodeSuggestions(itemCode);
        } else if (itemName != "" && itemName != null) {
            itemName = (new StringBuilder()).append(itemName).append("%").toString();
            values = mrsDAO.itemNameSuggestions(itemName);
        }
        return values;
    }

    public int insertMRS(int technicianId, int jobCardId, int rcWorkId,int counterId,String remarks,int companyId,String manualMrsDate, int userId) {
        int status = 0;
        status = mrsDAO.insertMRS(technicianId, jobCardId, rcWorkId,counterId,remarks,companyId,manualMrsDate,userId);
        return status;
    }

    public int insertMrsItems(int itemId, float qty, int mrsId, int userId) {
        int status = 0;
        status = mrsDAO.insertMrsItems(itemId, qty, mrsId,0, userId);
        return status;
    }

    public ArrayList getMrsList1(int jobcardId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList tempList = new ArrayList();
        tempList = mrsDAO.getMrsList1(jobcardId);
        if (tempList.size() != 0);
        return tempList;
    }

    public ArrayList processMrsItemDetails(MrsTO mrsTO, int companyId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList issuedQty = new ArrayList();
        ArrayList poRaisedQty = new ArrayList();
        ArrayList mrsItems = new ArrayList();
        ArrayList mrsItemSummary = new ArrayList();
        System.out.println((new StringBuilder()).append("mrsId is =").append(mrsTO.getMrsId()[0]).toString());
        System.out.println((new StringBuilder()).append("companyId is  =").append(companyId).toString());
        String totalRequested = "";
        float totalIssued = 0.0F;
        float totalRequiredQty = 0.0F;
        float localPoQty = 0.0F;
        float vendorPoQty = 0.0F;
        String otherServicePointQuantity = "";
        String localServicePointQuantity = "";
        String rcQuantity = "";
        String mrsOtherReq = "";
        MrsTO mrs = null;
        MrsTO mrsItemsTo = null;
        MrsTO ItemSummaryTO = null;
        mrsItems = mrsDAO.getMrsItems(Integer.parseInt(mrsTO.getMrsId()[0]));
        mrsTO.setCompanyId(companyId);
        System.out.println((new StringBuilder()).append("in mrsbp mrsItems = ").append(mrsItems.size()).toString());
        for (Iterator items = mrsItems.iterator(); items.hasNext();) {
            mrsItemsTo = new MrsTO();
            mrsItemsTo = (MrsTO) items.next();
            mrsTO.setMrsItemId(mrsItemsTo.getMrsItemId());
            System.out.println((new StringBuilder()).append("mrsId-->").append(mrsTO.getMrsId().length).toString());
            ItemSummaryTO = new MrsTO();
            totalRequested = mrsDAO.getSelectedMrsReqQuantity(mrsTO);
            issuedQty = mrsDAO.getIssuedQty4SelecMrs(mrsTO);
            mrsOtherReq = mrsDAO.getOtherMrsRequest(Integer.parseInt(mrsTO.getMrsId()[0]), Integer.parseInt(mrsTO.getMrsItemId()));
            System.out.println((new StringBuilder()).append("issuedQty --> ").append(issuedQty.size()).toString());
            Iterator itr;
            for (itr = issuedQty.iterator(); itr.hasNext();) {
                mrs = new MrsTO();
                mrs = (MrsTO) itr.next();
                totalIssued += Float.parseFloat(mrs.getQty());
            }

            totalRequiredQty = Float.parseFloat(totalRequested) - totalIssued;
            System.out.println((new StringBuilder()).append("itemId=").append(mrsItemsTo.getMrsItemId()).toString());
            System.out.print((new StringBuilder()).append("--totalIssued=").append(totalIssued).toString());
            System.out.print((new StringBuilder()).append("--totalRequested=").append(totalRequested).toString());
            System.out.print((new StringBuilder()).append("--totalRequiredQty=").append(totalRequiredQty).toString());
            poRaisedQty = mrsDAO.getPoRaisedQty(mrsTO);
            itr = poRaisedQty.iterator();
            do {
                if (!itr.hasNext()) {
                    break;
                }
                mrs = new MrsTO();
                mrs = (MrsTO) itr.next();
                if (mrs.getPurchaseType().equals("1011")) {
                    localPoQty = Float.parseFloat(mrs.getQty());
                } else if (mrs.getPurchaseType().equals("1012")) {
                    vendorPoQty = Float.parseFloat(mrs.getQty());
                }
            } while (true);
            otherServicePointQuantity = mrsDAO.getOtherServicePointQuantity(Integer.parseInt(mrsTO.getMrsItemId()), companyId);
            localServicePointQuantity = mrsDAO.getLocalServicePointQuantity(Integer.parseInt(mrsTO.getMrsItemId()), companyId);
            System.out.println((new StringBuilder()).append("mrs Item ID = ").append(mrsTO.getMrsItemId()).toString());
            rcQuantity = mrsDAO.getLocalCenterRcItemsStock(Integer.parseInt(mrsTO.getMrsItemId()), companyId);
            System.out.println((new StringBuilder()).append("mrsId test =").append(mrsTO.getMrsId()[0]).toString());
            ItemSummaryTO.setMrsNumber(mrsTO.getMrsId()[0]);
            ItemSummaryTO.setMrsItemId(mrsItemsTo.getMrsItemId());
            ItemSummaryTO.setMrsItemMfrCode(mrsItemsTo.getMrsItemMfrCode());
            ItemSummaryTO.setMrsPaplCode(mrsItemsTo.getMrsPaplCode());
            ItemSummaryTO.setMrsItemName(mrsItemsTo.getMrsItemName());
            System.out.println((new StringBuilder()).append("mrsLocalServiceItemSum=").append(localServicePointQuantity).toString());
            System.out.println((new StringBuilder()).append("rcQuantity=").append(rcQuantity).toString());
            ItemSummaryTO.setMrsLocalServiceItemSum(localServicePointQuantity);
            ItemSummaryTO.setRcQty(rcQuantity);
            ItemSummaryTO.setMrsOtherServiceItemSum(otherServicePointQuantity);
            ItemSummaryTO.setRequiredQty(String.valueOf(totalRequiredQty));
            ItemSummaryTO.setLocalPoQty(String.valueOf(localPoQty));
            ItemSummaryTO.setVendorPoQty(String.valueOf(vendorPoQty));
            ItemSummaryTO.setCount(mrsOtherReq);
            ItemSummaryTO.setMrsRequestedItemNumber(mrsItemsTo.getMrsRequestedItemNumber());
            ItemSummaryTO.setPositionId(mrsItemsTo.getPositionId());
            ItemSummaryTO.setPositionName(mrsItemsTo.getPositionName());
            mrsItemSummary.add(ItemSummaryTO);
            totalRequested = "";
            totalIssued = 0.0F;
            totalRequiredQty = 0.0F;
        }

        if (mrsItemSummary.size() == 0) {
            throw new FPBusinessException("EM-MRS-01");
        } else {
            return mrsItemSummary;
        }
    }

    public ArrayList processMrsIssueHistory(int mrsId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList itemList = new ArrayList();
        itemList = mrsDAO.getMrsIssueHistory(mrsId);
        if (itemList.size() != 0);
        return itemList;
    }

    public ArrayList processJcIssueHistory(int jcId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList itemList = new ArrayList();
        itemList = mrsDAO.getJobcardIssueHistory(jcId);
        if (itemList.size() != 0);
        return itemList;
    }
    public ArrayList processMrsItemStatus(int jcId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList itemList = new ArrayList();
        itemList = mrsDAO.processMrsItemStatus(jcId);
        if (itemList.size() != 0);
        return itemList;
    }

    public int processScrapItems(MrsTO mrs, int companyId, int userId)
            throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = mrsDAO.doInsertScrapItems(mrs, companyId, userId);
        if (status != 0);
        return status;
    }

    public int processGenerateMrsGdn(MrsTO mrsTo, int userId)
            throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = mrsDAO.doGenerateMrsGdn(mrsTo, userId);
        if (status != 0);
        return status;
    }

    public int processIssueMrsItems(MrsTO mrsTo, int userId)
            throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = mrsDAO.doGenerateMrsGdn(mrsTo, userId);
        if (status != 0);
        return status;
    }

    public String processNewTyresList(int itemId, int companyId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList tyreList = new ArrayList();
        String tyres = "";
        int counter = 0;
        MrsTO mrsTO = null;
        tyreList = mrsDAO.getNewTyresList(itemId, companyId);
        if (tyreList.size() == 0) {
            tyres = "";
        } else {
            for (Iterator itr = tyreList.iterator(); itr.hasNext();) {
                mrsTO = new MrsTO();
                mrsTO = (MrsTO) itr.next();
                if (counter == 0) {
                    System.out.println((new StringBuilder()).append(" mrsTO.getTyreId()").append(mrsTO.getTyreId()).toString());
                    System.out.println((new StringBuilder()).append("mrsTO.getTyreNo()").append(mrsTO.getTyreNo()).toString());
                    System.out.println((new StringBuilder()).append("mrsTO.getPurchaseId()").append(mrsTO.getPurchaseId()).toString());
                    tyres = (new StringBuilder()).append(mrsTO.getTyreId()).append("-").append(mrsTO.getTyreNo()).append("-").append(mrsTO.getPriceId()).append("-").append(mrsTO.getPrice()).append("-").append(mrsTO.getPriceType()).append("-").append(mrsTO.getPurchaseId()).toString();
                    //tyres = (new StringBuilder()).append(mrsTO.getTyreId()).append("-").append(mrsTO.getTyreNo()).append("-").append(mrsTO.getPriceId()).append("-").append(mrsTO.getPrice()).append("-").append(mrsTO.getPriceType()).toString();
                    counter++;
                } else {
                    tyres = (new StringBuilder()).append(tyres).append("~").append(mrsTO.getTyreId()).append("-").append(mrsTO.getTyreNo()).append("-").append(mrsTO.getPriceId()).append("-").append(mrsTO.getPrice()).append("-").append(mrsTO.getPriceType()).append("-").append(mrsTO.getPurchaseId()).toString();
                    //tyres = (new StringBuilder()).append(tyres).append("~").append(mrsTO.getTyreId()).append("-").append(mrsTO.getTyreNo()).append("-").append(mrsTO.getPriceId()).append("-").append(mrsTO.getPrice()).append("-").append(mrsTO.getPriceType()).toString();
                }
            }

        }
        return tyres;
    }

    public String processIssueTyres(MrsTO mrsTo, int userId, int companyId)
            throws FPBusinessException, FPRuntimeException {
        System.out.println("////////////////In processIssueTyres");
        int status = 0;
        int status1=0;
        String ajaxData = "";
        status = mrsDAO.doTyresIssue(mrsTo, userId);
        status = mrsDAO.updateNewItemStock(userId, mrsTo);
        return ajaxData;
    }

    public String processIssueItems(MrsTO mrsTo, int userId)
            throws FPBusinessException, FPRuntimeException {
        int status = 0;
        String ajaxData = "";
        int status1 = 0;
        int status2 = 0;
        int status3 = 0;
        status1 = mrsDAO.insertNewItemHistory(userId, mrsTo);
        status2 = mrsDAO.insertNewItemMaster(userId, mrsTo);
        mrsTo.setQuantityRI(-mrsTo.getQuantityRI());
        status3 = mrsDAO.updateNewItemStock(userId, mrsTo);
        status = status1 + status2 + status3;
        System.out.println((new StringBuilder()).append("status1=").append(status1).toString());
        System.out.println((new StringBuilder()).append("status2=").append(status2).toString());
        System.out.println((new StringBuilder()).append("status3=").append(status3).toString());
        System.out.println((new StringBuilder()).append("result status = ").append(status).toString());
        return ajaxData;
    }

    public void processRcMrsItems(MrsTO mrsTO, int userId)
            throws FPBusinessException, FPRuntimeException {
        int status1 = 0;
        int status2 = 0;
        int status3 = 0;
        int status4 = 0;
        status1 = mrsDAO.insertRcItemHistory(userId, mrsTO);
        status2 = mrsDAO.insertRcItemIssueMaster(userId, mrsTO);
        status3 = mrsDAO.updateRcItemMaster(userId, mrsTO);
        mrsTO.setQuantityRI(-mrsTO.getQuantityRI());
        System.out.println((new StringBuilder()).append("make negative by =").append(mrsTO.getQuantityRI()).toString());
        status4 = mrsDAO.updateRcItemStock(userId, mrsTO);
        System.out.println((new StringBuilder()).append(">status1=").append(status1).append(">status2=").append(status2).append(">status3=").append(status3).toString());
    }

    public void processRtTyreIssue(MrsTO mrsTO, int userId)
            throws FPBusinessException, FPRuntimeException {
        int status1 = 0;
        int status2 = 0;
        int status3 = 0;
        int tyreId = Integer.parseInt(mrsTO.getTyreId());
        status1 = mrsDAO.doTyresIssue(mrsTO, userId);
        status2 = mrsDAO.doRtStockUpdate(tyreId, "N");
        System.out.println((new StringBuilder()).append("make negative by =").append(mrsTO.getQuantityRI()).toString());
        status3 = mrsDAO.updateRcItemStock(userId, mrsTO);
        System.out.println((new StringBuilder()).append(">status1=").append(status1).append(">status2=").append(status2).append(">status3=").append(status3).toString());
    }

    public int processInsertRcQueue(MrsTO mrsTo, int userId)
            throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = mrsDAO.doInsertRcQueue(mrsTo, userId);
        if (status != 0);
        return status;
    }

    public ArrayList processItemsIsuedDetails(int mrsId, int itemId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList itemList = new ArrayList();
        itemList = mrsDAO.getItemsIsuedDetails(mrsId, itemId);
        return itemList;
    }

    public ArrayList processTyresIsuedDetails(int mrsId, int itemId, int positionId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList itemList = new ArrayList();
        itemList = mrsDAO.getTyresIsuedDetails(mrsId, itemId, positionId);
        return itemList;
    }

    public void processRcItemsReturn(MrsTO mrsTO, int userId)
            throws FPBusinessException, FPRuntimeException {
        int status1 = 0;
        int status2 = 0;
        int status3 = 0;
        int status4 = 0;
        status1 = mrsDAO.insertRcItemHistory(userId, mrsTO);
        status2 = mrsDAO.updateRcItemIssueMaster(userId, mrsTO);
        status3 = mrsDAO.updateRcItemMaster(userId, mrsTO);
        status4 = mrsDAO.updateRcItemStock(userId, mrsTO);
    }

    public void processNewItemsReturn(MrsTO mrsTO, int userId)
            throws FPBusinessException, FPRuntimeException {
        System.out.println("/////////////processNewItemsReturn");
        int status1 = 0;
        int status2 = 0;
        int status3 = 0;
        int status4 = 0;
        status1 = mrsDAO.insertNewItemHistory(userId, mrsTO);
        mrsTO.setQuantityRI(-mrsTO.getQuantityRI());
        status2 = mrsDAO.insertNewItemMaster(userId, mrsTO);
        mrsTO.setQuantityRI(-mrsTO.getQuantityRI());
        status3 = mrsDAO.updateNewItemStock(userId, mrsTO);
        //



    }

    public int processReturnIssuedTyre(MrsTO mrsTo)
            throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = mrsDAO.doReturnIssuedTyre(mrsTo);
        return status;
    }

    public int processReturnRtTyre(MrsTO mrsTo)
            throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = mrsDAO.doReturnRtTyre(mrsTo);
        return status;
    }

    public ArrayList getTyrePostions() {
        ArrayList itemList = new ArrayList();
        itemList = mrsDAO.getTyrePostions();
        if (itemList.size() != 0);
        return itemList;
    }

    public String getQuandity(String mfrCode, String itemCode, String itemName, int companyId, String vendorId, String reqType) {
        String values = "";
        String temp[] = null;
        String Quandity = "";
        int itemId = 0;
        String rcCount = "";
        String localCount = "";
        float totlQty = 0.0F;
        if (mfrCode != "" && mfrCode != null) {
            values = mrsDAO.getMfrItemId(mfrCode, vendorId,reqType);
        } else if (itemCode != "" && itemCode != null) {
            values = mrsDAO.getCodeItemId(itemCode,vendorId,reqType);
        } else if (itemName != "" && itemName != null) {
            values = mrsDAO.getNameItemId(itemName,vendorId,reqType);
        }
        String bestBuy = "";
        if (values != null) {
            temp = values.split("~");
            itemId = Integer.parseInt(temp[0]);
            System.out.println("1");
            rcCount = mrsDAO.getLocalCenterRcItemsStock(itemId, companyId);
            System.out.println("2");
            localCount = mrsDAO.getLocalServicePointQuantity(itemId, companyId);
            System.out.println("3");
            totlQty = Float.parseFloat(rcCount) + Float.parseFloat(localCount);
            if(vendorId != null){
                //unitPrice,'~',discount,'~',vat
                Quandity = (new StringBuilder()).append(temp[0]).append("~").append(String.valueOf(totlQty)).append("~").append(temp[1]).append("~").append(temp[2]).append("~").append(temp[3]).append("~").append(temp[4]).append("~").append(temp[5]).append("~").append(temp[6]).append("~").append(temp[7]).append("~").append(temp[8]).append("~").append(temp[9]).append("~").append(temp[10]).toString();
                bestBuy = mrsDAO.getBestBuy(itemId);
                Quandity = Quandity + "~" + bestBuy;
                System.out.println("Quandity1:"+Quandity);
            }else {
                Quandity = (new StringBuilder()).append(temp[0]).append("~").append(String.valueOf(totlQty)).append("~").append(temp[1]).append("~").append(temp[2]).append("~").append(temp[3]).append("~").append(temp[4]).append("~").append(temp[5]).append("~").append(temp[6]).toString();
            }   System.out.println("Quandity2:"+Quandity);
        }
        return Quandity;
    }

    public int insertMrsItems(int itemId, float qty, int mrsId, int posId,int rcItemsId,int userId) {
        int status = 0;
        if (posId == 0) {
            status = mrsDAO.insertMrsItems(itemId, qty, mrsId,rcItemsId,userId);
        } else {
            status = mrsDAO.insertMrsTyreItems(itemId, mrsId, posId,userId,rcItemsId);
        }
        return status;
    }

    public ArrayList getMrsItems(int mrsId, String status)
            throws FPBusinessException, FPRuntimeException {
        ArrayList tempList = new ArrayList();
        if (status.equalsIgnoreCase("Hold") || status.equalsIgnoreCase("Requested")) {
            tempList = mrsDAO.getMrsItems(mrsId);
        } else if (status.equalsIgnoreCase("Approved") || status.equalsIgnoreCase("Rejected")) {
            tempList = mrsDAO.getApprovedMrsItems(mrsId);
        }
        if (tempList.size() != 0);
        return tempList;
    }

    public int updateMrsItems(int mrsId, String itemId[], String selectedIndex[], String qtys[], String action[], String catId[]) {
        int status = 0;
        int item = 0;
        int temp = 0;
        int qty = 0;
        int cat = 0;
        int option = 0;
        for (int i = 0; i < selectedIndex.length; i++) {
            temp = Integer.parseInt(selectedIndex[i]);
            item = Integer.parseInt(itemId[temp]);
            qty = Integer.parseInt(qtys[temp]);
            cat = Integer.parseInt(catId[temp]);
            option = Integer.parseInt(action[temp]);
            if (option == 1) {
                status = mrsDAO.updateItem(mrsId, item, qty, cat);
                continue;
            }
            if (option == 2) {
                status = mrsDAO.deleteItem(mrsId, item, cat);
            }
        }

        return status;
    }

    public MrsTO getMrsUpdatesAfterIssue(int mrsId, int companyId, int itemId, int categoryId) {
        MrsTO mrs = new MrsTO();
        String totalIssued = "";
        String mrsRCItems = mrsDAO.getLocalCenterRcItemsStock(itemId, companyId);
        String localServicePointQuantity = mrsDAO.getLocalServicePointQuantity(itemId, companyId);
        String mrsOtherReq = mrsDAO.getOtherMrsRequest(mrsId, itemId);
        if (categoryId == 1011) {
            totalIssued = mrsDAO.getTotalIssuedQuantity(mrsId, itemId);
        } else {
            totalIssued = mrsDAO.getTotalIssuedQuantity(mrsId, itemId);
        }
        mrs.setMrsLocalServiceItemSum(localServicePointQuantity);
        mrs.setRcQty(mrsRCItems);
        mrs.setTotalIssued(totalIssued);
        return mrs;
    }

    public int processMrsVehicleTyreUpdate(MrsTO tyreTO, int UserId, int companyId) {
        int status = 0;
        int vehicleId = 0;
        vehicleId = mrsDAO.getMrsVehicleId(Integer.parseInt(tyreTO.getMrsNumber()));
        tyreTO.setVehicleId(vehicleId);
        status = mrsDAO.doInsertVehicleTyre(tyreTO, UserId, companyId);
        if (status != 0);
        return status;
    }

    public int processUpdateReturnTyre(MrsTO tyreTO) {
        int status = 0;
        int vehicleId = 0;
        vehicleId = mrsDAO.getMrsVehicleId(Integer.parseInt(tyreTO.getMrsNumber()));
        tyreTO.setVehicleId(vehicleId);
        status = mrsDAO.doUpdateReturnVehicleTyre(tyreTO);
        if (status != 0);
        return status;
    }
    //Hari

    public ArrayList processRcMrsDetails(int mrsId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList rcMrsDetails = new ArrayList();
        rcMrsDetails = mrsDAO.getRcMrsDetails(mrsId);
        return rcMrsDetails;
    }


public int insertRcSpareDetails(int rcWorkId,int rcItem, int itemId, int userId,float qty) {
        int status = 0;

            status = mrsDAO.insertRcSpareDetails(rcWorkId, rcItem,itemId,userId,qty);

        return status;
    }



public int processReturnSpareItems(MrsTO mrsTO,int userId) {
        int status = 0;

            status = mrsDAO.processReturnSpareItems( mrsTO,userId);

        return status;
    }


public int updatePriceForSpareItems(MrsTO mrsTO,int userId) {
        int status = 0;

            status = mrsDAO.updatePriceForSpareItems( mrsTO,userId);

        return status;
    }
public ArrayList getRcMrsItems(int mrsId, String status)
            throws FPBusinessException, FPRuntimeException {
        ArrayList tempList = new ArrayList();
         if (status.equalsIgnoreCase("Approved") || status.equalsIgnoreCase("Rejected")) {
            tempList = mrsDAO.getRcApprovedMrsItems(mrsId);
        }
        if (tempList.size() != 0);
        return tempList;
    }
//For Counter Bill
public int insertCounterMrs(MrsTO mrsTO) {
        int status = 0;

            status = mrsDAO.insertCounterMrs(mrsTO);

        return status;
    }
public int insertCounterBillMaster(MrsTO mrsTO) {
        int billNo = 0;
            billNo = mrsDAO.insertCounterBillMaster(mrsTO);

        return billNo;
    }
public int insertCounterBillItems(ArrayList counterItems,int billNo,float counterBillAmount) {
        int status = 0;
            status = mrsDAO.insertCounterBillItems(counterItems,billNo,counterBillAmount);
        return status;
    }

public ArrayList getCounterBillHeaderInfo(int counterId) {
        ArrayList billHeader = new ArrayList();
            billHeader = mrsDAO.getCounterBillHeaderInfo(counterId);
        return billHeader;
    }

public ArrayList getCounterBillItems(int counterId) {
        ArrayList billItems = new ArrayList();
            billItems = mrsDAO.getCounterBillItems(counterId);
        return billItems;
    }


    public ArrayList processCounterMrsDetails(int mrsId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList counterMrsDetails = new ArrayList();
        counterMrsDetails = mrsDAO.getCounterMrsDetails(mrsId);
        return counterMrsDetails;
    }
    public ArrayList processCounterCustomer()
            throws FPBusinessException, FPRuntimeException {
        ArrayList counterCustomer = new ArrayList();
        counterCustomer = mrsDAO.getCounterCustomer();
        return counterCustomer;
    }

    private MrsDAO mrsDAO;
}
