// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3)
// Source File Name:   MrsTO.java
package ets.domain.mrs.business;

public class MrsTO {

    public MrsTO() {
        mrsNumber = "";
        mrsJobCardNumber = "";
        mrsVehicleNumber = "";
        mrsCreatedDate = "";
        mrsStatus = "";
        mrsVehicleKm = "";
        mrsVehicleType = "";
        mrsVehicleUsageType = "";
        mrsVehicleMfr = "";
        mrsVehicleModel = "";
        mrsVehicleEngineNumber = "";
        mrsVehicleChassisNumber = "";
        mrsItemMfrCode = "";
        mrsPaplCode = "";
        mrsItemName = "";
        mrsItemMax = "";
        mrsItemQuantity = "";
        mrsRequestedItemNumber = "";
        mrsIssueQuantity = "";
        mrsItemId = "";
        mrsApprovalRemarks = "";
        mrsApprovalStatus = "";
        mrsOtherServiceItemSum = "";
        mrsLocalServiceItemSum = "";
        Qty = "";
        mrsCompanyName = "";
        mrsItemSum = "";
        count = "";
        newStock = "";
        rcStock = "";
        poQuantity = "";
        approvedQty = "";
        uomName = "";
        totalIssued = "";
        priceId = "";
        price = "";
        rcItemId = "";
        companyId = 0;
        priceIds = "";
        rcItemIds = "";
        faultStatus = "";
        actionType = "";
        quantityRI = 0.0F;
        itemType = "";
        mrsId = null;
        purchaseType = "";
        rcQty = "";
        requiredQty = "";
        localPoQty = "";
        vendorPoQty = "";
        mrsTechnician = "";
        vendorId = "";
        vendorName = "";
        itemIds = null;
        requestedQtys = null;
        issueQtys = null;
        selectedIndex = null;
        jobCardId = 0;
        workOrderId = 0;
        vehicleId = 0;
        serviceTypeId = 0;
        serviceTypeName = "";
        usageTypeId = 0;
        empId = 0;
        mrsId1 = 0;
        empName = "";
        mfrCode = "";
        itemCode = "";
        itemName = "";
        status = "";
        issuedOn = "";
        faultQty = "";
        rcStatus = "";
        categoryId = "";
        positionId = "";
        positionName = "";
        tyreId = "";
        tyreNo = "";
        issueId = "";
        posId = 0;
        catId = 0;
        posName = "";
        jcMYFormatNo = "";
        priceType = "";
        //Hari
        rcWorkId = 0;
        rcItemsId = 0;
        issuedItemsId = 0;
        priceWithTax = 0.0F;
        rcItems = 0;
        type = null;
        counterId = 0;
        rcPriceWoId = 0;
        name = null;
        address = null;
        mobileNo = null;
        date = null;
        remarks = null;
        userId = 0;
        hike = 0.0F;
        discount = 0.0F;
        discountAmount = 0.0F;
        spareAmount = 0.0F;
        spareRetAmount = 0.0F;
        spareTaxAmount = 0.0F;
        nettAmount = 0.0F;
        tax = 0.0F;
        invoiceNo = null;
        billNo = null;
//        bala
        purchaseId="";


    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(String purchaseId) {
        this.purchaseId = purchaseId;
    }

    public float getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(float discountAmount) {
        this.discountAmount = discountAmount;
    }

    public Float getSpareTaxAmount() {
        return spareTaxAmount;
    }

    public void setSpareTaxAmount(Float spareTaxAmount) {
        this.spareTaxAmount = spareTaxAmount;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public Float getNettAmount() {
        return nettAmount;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public void setNettAmount(Float nettAmount) {
        this.nettAmount = nettAmount;
    }

    public float getTax() {
        return tax;
    }

    public void setTax(float tax) {
        this.tax = tax;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public float getHike() {
        return hike;
    }

    public void setHike(float hike) {
        this.hike = hike;
    }

    public Float getSpareAmount() {
        return spareAmount;
    }

    public void setSpareAmount(Float spareAmount) {
        this.spareAmount = spareAmount;
    }

    public Float getSpareRetAmount() {
        return spareRetAmount;
    }

    public void setSpareRetAmount(Float spareRetAmount) {
        this.spareRetAmount = spareRetAmount;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public int getRcPriceWoId() {
        return rcPriceWoId;
    }

    public void setRcPriceWoId(int rcPriceWoId) {
        this.rcPriceWoId = rcPriceWoId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public int getCounterId() {
        return counterId;
    }

    public void setCounterId(int counterId) {
        this.counterId = counterId;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getRcItems() {
        return rcItems;
    }

    public void setRcItems(int rcItems) {
        this.rcItems = rcItems;
    }

    public int getIssuedItemsId() {
        return issuedItemsId;
    }

    public void setIssuedItemsId(int issuedItemsId) {
        this.issuedItemsId = issuedItemsId;
    }

    public int getRcItemsId() {
        return rcItemsId;
    }

    public void setRcItemsId(int rcItemsId) {
        this.rcItemsId = rcItemsId;
    }

    public float getPriceWithTax() {
        return priceWithTax;
    }

    public void setPriceWithTax(float priceWithTax) {
        this.priceWithTax = priceWithTax;
    }

    public int getRcWorkId() {
        return rcWorkId;
    }

    public void setRcWorkId(int rcWorkId) {
        this.rcWorkId = rcWorkId;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public int getMrsId1() {
        return mrsId1;
    }

    public void setMrsId1(int mrsId1) {
        this.mrsId1 = mrsId1;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getJobCardId() {
        return jobCardId;
    }

    public void setJobCardId(int jobCardId) {
        this.jobCardId = jobCardId;
    }

    public String getMfrCode() {
        return mfrCode;
    }

    public void setMfrCode(String mfrCode) {
        this.mfrCode = mfrCode;
    }

    public int getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(int serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public String getServiceTypeName() {
        return serviceTypeName;
    }

    public void setServiceTypeName(String serviceTypeName) {
        this.serviceTypeName = serviceTypeName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getUsageTypeId() {
        return usageTypeId;
    }

    public void setUsageTypeId(int usageTypeId) {
        this.usageTypeId = usageTypeId;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public int getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(int workOrderId) {
        this.workOrderId = workOrderId;
    }

    public String getMrsCreatedDate() {
        return mrsCreatedDate;
    }

    public void setMrsCreatedDate(String mrsCreatedDate) {
        this.mrsCreatedDate = mrsCreatedDate;
    }

    public String getMrsJobCardNumber() {
        return mrsJobCardNumber;
    }

    public void setMrsJobCardNumber(String mrsJobCardNumber) {
        this.mrsJobCardNumber = mrsJobCardNumber;
    }

    public String getMrsNumber() {
        return mrsNumber;
    }

    public void setMrsNumber(String mrsNumber) {
        this.mrsNumber = mrsNumber;
    }

    public String getMrsStatus() {
        return mrsStatus;
    }

    public void setMrsStatus(String mrsStatus) {
        this.mrsStatus = mrsStatus;
    }

    public String getMrsVehicleNumber() {
        return mrsVehicleNumber;
    }

    public void setMrsVehicleNumber(String mrsVehicleNumber) {
        this.mrsVehicleNumber = mrsVehicleNumber;
    }

    public String getMrsVehicleChassisNumber() {
        return mrsVehicleChassisNumber;
    }

    public void setMrsVehicleChassisNumber(String mrsVehicleChassisNumber) {
        this.mrsVehicleChassisNumber = mrsVehicleChassisNumber;
    }

    public String getMrsVehicleEngineNumber() {
        return mrsVehicleEngineNumber;
    }

    public void setMrsVehicleEngineNumber(String mrsVehicleEngineNumber) {
        this.mrsVehicleEngineNumber = mrsVehicleEngineNumber;
    }

    public String getMrsVehicleKm() {
        return mrsVehicleKm;
    }

    public void setMrsVehicleKm(String mrsVehicleKm) {
        this.mrsVehicleKm = mrsVehicleKm;
    }

    public String getMrsVehicleMfr() {
        return mrsVehicleMfr;
    }

    public void setMrsVehicleMfr(String mrsVehicleMfr) {
        this.mrsVehicleMfr = mrsVehicleMfr;
    }

    public String getMrsVehicleModel() {
        return mrsVehicleModel;
    }

    public void setMrsVehicleModel(String mrsVehicleModel) {
        this.mrsVehicleModel = mrsVehicleModel;
    }

    public String getMrsVehicleType() {
        return mrsVehicleType;
    }

    public void setMrsVehicleType(String mrsVehicleType) {
        this.mrsVehicleType = mrsVehicleType;
    }

    public String getMrsVehicleUsageType() {
        return mrsVehicleUsageType;
    }

    public void setMrsVehicleUsageType(String mrsVehicleUsageType) {
        this.mrsVehicleUsageType = mrsVehicleUsageType;
    }

    public String getMrsItemMfrCode() {
        return mrsItemMfrCode;
    }

    public void setMrsItemMfrCode(String mrsItemMfrCode) {
        this.mrsItemMfrCode = mrsItemMfrCode;
    }

    public String getMrsItemName() {
        return mrsItemName;
    }

    public void setMrsItemName(String mrsItemName) {
        this.mrsItemName = mrsItemName;
    }

    public String getMrsPaplCode() {
        return mrsPaplCode;
    }

    public void setMrsPaplCode(String mrsPaplCode) {
        this.mrsPaplCode = mrsPaplCode;
    }

    public String getMrsItemMax() {
        return mrsItemMax;
    }

    public void setMrsItemMax(String mrsItemMax) {
        this.mrsItemMax = mrsItemMax;
    }

    public String getMrsItemQuantity() {
        return mrsItemQuantity;
    }

    public void setMrsItemQuantity(String mrsItemQuantity) {
        this.mrsItemQuantity = mrsItemQuantity;
    }

    public String getMrsRequestedItemNumber() {
        return mrsRequestedItemNumber;
    }

    public void setMrsRequestedItemNumber(String mrsRequestedItemNumber) {
        this.mrsRequestedItemNumber = mrsRequestedItemNumber;
    }

    public String getMrsIssueQuantity() {
        return mrsIssueQuantity;
    }

    public void setMrsIssueQuantity(String mrsIssueQuantity) {
        this.mrsIssueQuantity = mrsIssueQuantity;
    }

    public String getMrsItemId() {
        return mrsItemId;
    }

    public void setMrsItemId(String mrsItemId) {
        this.mrsItemId = mrsItemId;
    }

    public String getMrsApprovalRemarks() {
        return mrsApprovalRemarks;
    }

    public void setMrsApprovalRemarks(String mrsApprovalRemarks) {
        this.mrsApprovalRemarks = mrsApprovalRemarks;
    }

    public String getMrsApprovalStatus() {
        return mrsApprovalStatus;
    }

    public void setMrsApprovalStatus(String mrsApprovalStatus) {
        this.mrsApprovalStatus = mrsApprovalStatus;
    }

    public String getMrsOtherServiceItemSum() {
        return mrsOtherServiceItemSum;
    }

    public void setMrsOtherServiceItemSum(String mrsOtherServiceItemSum) {
        this.mrsOtherServiceItemSum = mrsOtherServiceItemSum;
    }

    public String getMrsCompanyName() {
        return mrsCompanyName;
    }

    public void setMrsCompanyName(String mrsCompanyName) {
        this.mrsCompanyName = mrsCompanyName;
    }

    public String getQty() {
        return Qty;
    }

    public void setQty(String Qty) {
        this.Qty = Qty;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getMrsItemSum() {
        return mrsItemSum;
    }

    public void setMrsItemSum(String mrsItemSum) {
        this.mrsItemSum = mrsItemSum;
    }

    public String getMrsLocalServiceItemSum() {
        return mrsLocalServiceItemSum;
    }

    public void setMrsLocalServiceItemSum(String mrsLocalServiceItemSum) {
        this.mrsLocalServiceItemSum = mrsLocalServiceItemSum;
    }

    public String getNewStock() {
        return newStock;
    }

    public void setNewStock(String newStock) {
        this.newStock = newStock;
    }

    public String getRcStock() {
        return rcStock;
    }

    public void setRcStock(String rcStock) {
        this.rcStock = rcStock;
    }

    public String getPoQuantity() {
        return poQuantity;
    }

    public void setPoQuantity(String poQuantity) {
        this.poQuantity = poQuantity;
    }

    public String getApprovedQty() {
        return approvedQty;
    }

    public void setApprovedQty(String approvedQty) {
        this.approvedQty = approvedQty;
    }

    public String getTotalIssued() {
        return totalIssued;
    }

    public void setTotalIssued(String totalIssued) {
        this.totalIssued = totalIssued;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPriceId() {
        return priceId;
    }

    public void setPriceId(String priceId) {
        this.priceId = priceId;
    }

    public String getRcItemId() {
        return rcItemId;
    }

    public void setRcItemId(String rcItemId) {
        this.rcItemId = rcItemId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getFaultStatus() {
        return faultStatus;
    }

    public void setFaultStatus(String faultStatus) {
        this.faultStatus = faultStatus;
    }

    public String getPriceIds() {
        return priceIds;
    }

    public void setPriceIds(String priceIds) {
        this.priceIds = priceIds;
    }

    public float getQuantityRI() {
        return quantityRI;
    }

    public void setQuantityRI(float quantityRI) {
        this.quantityRI = quantityRI;
    }

    public String getRcItemIds() {
        return rcItemIds;
    }

    public void setRcItemIds(String rcItemIds) {
        this.rcItemIds = rcItemIds;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String[] getMrsId() {
        return mrsId;
    }

    public void setMrsId(String mrsId[]) {
        this.mrsId = mrsId;
    }

    public String getPurchaseType() {
        return purchaseType;
    }

    public void setPurchaseType(String purchaseType) {
        this.purchaseType = purchaseType;
    }

    public String getLocalPoQty() {
        return localPoQty;
    }

    public void setLocalPoQty(String localPoQty) {
        this.localPoQty = localPoQty;
    }

    public String getRcQty() {
        return rcQty;
    }

    public void setRcQty(String rcQty) {
        this.rcQty = rcQty;
    }

    public String getRequiredQty() {
        return requiredQty;
    }

    public void setRequiredQty(String requiredQty) {
        this.requiredQty = requiredQty;
    }

    public String getVendorPoQty() {
        return vendorPoQty;
    }

    public void setVendorPoQty(String vendorPoQty) {
        this.vendorPoQty = vendorPoQty;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public Integer[] getMrsidsArray() {
        return mrsidsArray;
    }

    public void setMrsidsArray(Integer mrsidsArray[]) {
        this.mrsidsArray = mrsidsArray;
    }

    public String getIssuedOn() {
        return issuedOn;
    }

    public void setIssuedOn(String issuedOn) {
        this.issuedOn = issuedOn;
    }

    public String[] getIssueQtys() {
        return issueQtys;
    }

    public void setIssueQtys(String issueQtys[]) {
        this.issueQtys = issueQtys;
    }

    public String[] getItemIds() {
        return itemIds;
    }

    public void setItemIds(String itemIds[]) {
        this.itemIds = itemIds;
    }

    public String[] getRequestedQtys() {
        return requestedQtys;
    }

    public void setRequestedQtys(String requestedQtys[]) {
        this.requestedQtys = requestedQtys;
    }

    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String selectedIndex[]) {
        this.selectedIndex = selectedIndex;
    }

    public String getFaultQty() {
        return faultQty;
    }

    public void setFaultQty(String faultQty) {
        this.faultQty = faultQty;
    }

    public String getRcStatus() {
        return rcStatus;
    }

    public void setRcStatus(String rcStatus) {
        this.rcStatus = rcStatus;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getPositionId() {
        return positionId;
    }

    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getTyreId() {
        return tyreId;
    }

    public void setTyreId(String tyreId) {
        this.tyreId = tyreId;
    }

    public String getTyreNo() {
        return tyreNo;
    }

    public void setTyreNo(String tyreNo) {
        this.tyreNo = tyreNo;
    }

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public int getPosId() {
        return posId;
    }

    public void setPosId(int posId) {
        this.posId = posId;
    }

    public String getPosName() {
        return posName;
    }

    public void setPosName(String posName) {
        this.posName = posName;
    }

    public String getMrsTechnician() {
        return mrsTechnician;
    }

    public void setMrsTechnician(String mrsTechnician) {
        this.mrsTechnician = mrsTechnician;
    }

    public String getJcMYFormatNo() {
        return jcMYFormatNo;
    }

    public void setJcMYFormatNo(String jcMYFormatNo) {
        this.jcMYFormatNo = jcMYFormatNo;
    }
    private String mrsNumber;
    private String mrsJobCardNumber;
    private String mrsVehicleNumber;
    private String mrsCreatedDate;
    private String mrsStatus;
    private String mrsVehicleKm;
    private String mrsVehicleType;
    private String mrsVehicleUsageType;
    private String mrsVehicleMfr;
    private String mrsVehicleModel;
    private String mrsVehicleEngineNumber;
    private String mrsVehicleChassisNumber;
    private String mrsItemMfrCode;
    private String mrsPaplCode;
    private String mrsItemName;
    private String mrsItemMax;
    private String mrsItemQuantity;
    private String mrsRequestedItemNumber;
    private String mrsIssueQuantity;
    private String mrsItemId;
    private String mrsApprovalRemarks;
    private String mrsApprovalStatus;
    private String mrsOtherServiceItemSum;
    private String mrsLocalServiceItemSum;
    private String Qty;
    private String mrsCompanyName;
    private String mrsItemSum;
    private String count;
    private String newStock;
    private String rcStock;
    private String poQuantity;
    private String approvedQty;
    private String uomName;
    private String totalIssued;
    private String priceId;
    private String price;
    private String rcItemId;
    private int companyId;
    private String priceIds;
    private String rcItemIds;
    private String faultStatus;
    private String actionType;
    private float quantityRI;
    private String itemType;
    private String mrsId[];
    private String purchaseType;
    private String rcQty;
    private String requiredQty;
    private String localPoQty;
    private String vendorPoQty;
    private String mrsTechnician;
    private String vendorId;
    private String vendorName;
    private Integer mrsidsArray[];
    private String itemIds[];
    private String requestedQtys[];
    private String issueQtys[];
    private String selectedIndex[];
    private int jobCardId;
    private int workOrderId;
    private int vehicleId;
    private int serviceTypeId;
    private String serviceTypeName;
    private int usageTypeId;
    private int empId;
    int mrsId1;
    private String empName;
    private String mfrCode;
    private String itemCode;
    private String itemName;
    private String status;
    private String issuedOn;
    private String faultQty;
    private String rcStatus;
    private String categoryId;
    private String positionId;
    private String positionName;
    private String tyreId;
    private String tyreNo;
    private String issueId;
    private int posId;
    private int catId;
    private String posName;
    private String jcMYFormatNo;
    private String priceType;
    //Hari
    private int rcWorkId;
    private int rcItemsId;
    private int rcItems;
    private int issuedItemsId;
    private float priceWithTax;
    private String type;
    private int counterId;
    private int rcPriceWoId;
    private String name;
    private String address;
    private String mobileNo;
    private String date;
    private String remarks;
    private int userId;
    private float hike;
    private float discount;
    private float discountAmount;
    private float tax;
    private Float spareAmount;
    private Float spareRetAmount;
    private Float spareTaxAmount;
    private Float nettAmount;
    private String invoiceNo;
    private String billNo;
//    bala
    private String purchaseId;
}
