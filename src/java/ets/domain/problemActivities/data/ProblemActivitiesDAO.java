package ets.domain.problemActivities.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPRuntimeException;
import ets.domain.problemActivities.business.ProblemActivitiesTO;
import ets.domain.section.business.SectionTO;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
 

/**
 *
 * @author vidya
 */
public class ProblemActivitiesDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "ProblemActivitiesDAO";

    
     //getSectionList
     /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getpaList(ProblemActivitiesTO problemActivitiesTO) {
        Map map = new HashMap();
        ArrayList paList = new ArrayList();
        try {
            map.put("startIndex", problemActivitiesTO.getStartIndex());
            map.put("endIndex", problemActivitiesTO.getEndIndex());
            
            paList = (ArrayList) getSqlMapClientTemplate().queryForList("probAct.ProblemActivities", map);
            // System.out.println("i am in getMfrList :"+MfrList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "SectionList", sqlException);
        }
        return paList;

    }
   
     /**
     * This  method used to Alter Parts List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception Model
     */
     public ArrayList getAlterProblemDetail(int problemId) {
        Map map = new HashMap();
        map.put("problemId", problemId);
        System.out.println("vendorId in dao" + problemId);
        ArrayList problemDetail = null;
        
        try {
            problemDetail = (ArrayList) getSqlMapClientTemplate().queryForList("probAct.GetProblemDetailUnique", map);
            // mfrDetail = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.GetVendorAllMfrList", map);
            System.out.println("GetVendorDetailUnique in dao " + problemDetail.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GetVendorDetailUnique", sqlException);
        }
        return problemDetail;
    }//doVendorUpdateDetails
               /**
     * This method used to Insert MFR Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doProblemUpdateDetails(ProblemActivitiesTO problemActivitiesTO, int problemId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
     
        map.put("problemId", problemId);
        map.put("problemName", problemActivitiesTO.getProblemName());
        map.put("discription", problemActivitiesTO.getDiscription());
        map.put("activeInd", problemActivitiesTO.getActiveInd());
        map.put("sectionId", problemActivitiesTO.getSectionId());
          
              
              

        int status = 0;
        int mfrstatus = 0;
     //   int vendorId = 0;
        try {
            System.out.println("doa- problem");
             System.out.println("problemId-problemId"+problemId);
             System.out.println("problemActivitiesTO.getProblemName()"+problemActivitiesTO.getProblemName());
             System.out.println("problemActivitiesTO.getDiscription()"+problemActivitiesTO.getDiscription());
             System.out.println("problemActivitiesTO.getActiveInd()"+problemActivitiesTO.getActiveInd());
             System.out.println("problemActivitiesTO.getSectionId()"+problemActivitiesTO.getSectionId());
             status = (Integer) getSqlMapClientTemplate().update("probAct.updateProblem", map);
   
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */  
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "updateVendor", sqlException);
        }
        return status;

    }
    //doProblemAddDetails
    
          /**
     * This method used to Insert MFR Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doProblemAddDetails(ProblemActivitiesTO problemActivitiesTO, int userId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
     
        map.put("userId", userId);
        map.put("problemName", problemActivitiesTO.getProblemName());
        map.put("discription", problemActivitiesTO.getDiscription());
        map.put("sectionId", problemActivitiesTO.getSectionId());
          
              
              

        int status = 0;
        int mfrstatus = 0;
     //   int vendorId = 0;
        try {
           
             status = (Integer) getSqlMapClientTemplate().update("probAct.addProblem", map);
   
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */  
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "updateVendor", sqlException);
        }
        return status;

    }
     
}
