/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.vehicle.data;

import com.ibatis.sqlmap.client.SqlMapClient;
import ets.arch.exception.FPRuntimeException;
import ets.domain.util.FPLogUtils;
import ets.domain.util.ThrottleConstants;
import ets.domain.vehicle.business.VehicleTO;
import java.io.File;
import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Date;
import java.util.Calendar;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

/**
 *
 * @author karudaiyar Subramaniam
 */
public class VehicleDAO extends SqlMapClientDaoSupport {

    private Object vendorId;

    public VehicleDAO() {
    }
    private final static String CLASS = "VehicleDAO";

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getMfrList() {
        Map map = new HashMap();
        ArrayList MfrList = new ArrayList();
        try {

            MfrList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.MfrList", map);
            System.out.println("MFRLIST:" + MfrList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "MfrList", sqlException);
        }
        return MfrList;

    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getModelList(VehicleTO vehicleTO) {
        Map map = new HashMap();
        ArrayList ModelList = new ArrayList();
        map.put("fleetTypeId", vehicleTO.getFleetTypeId());
        System.out.println("map..." + map);
        try {
            ModelList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.ModelList", map);
            System.out.println("MOdelLISt SIze:" + ModelList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ModelList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "ModelList", sqlException);
        }
        return ModelList;

    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getTypeList(VehicleTO vehicleTO) {
        Map map = new HashMap();
        ArrayList TypeList = new ArrayList();
        try {
            if (vehicleTO.getMfrId() == null) {
                vehicleTO.setMfrId("");
            }
            map.put("mfrId", vehicleTO.getMfrId());
            System.out.println("map = " + map);
            if ("1".equals(vehicleTO.getFleetTypeId())) {
                TypeList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.TypeList", map);
            } else if ("2".equals(vehicleTO.getFleetTypeId())) {
                TypeList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getTrailerTypeList", map);
            } else {
                TypeList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.TypeList", map);
            }
            System.out.println("TypeList  = " + TypeList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "MfrList", sqlException);
        }
        return TypeList;

    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getFuelList() {
        Map map = new HashMap();
        ArrayList FuelList = new ArrayList();
        try {

            FuelList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.FuelList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "MfrList", sqlException);
        }
        return FuelList;

    }

    /**
     * This method used to Insert MFR Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doMfrDetails(VehicleTO vehicleTO, int UserId) {
        int mfrId = 0;
        int status = 0;
        int vehicleMfr = 0;
        int tyreMfr = 0;
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", UserId);
        map.put("mfrName", vehicleTO.getMfrName());
        map.put("percentage", vehicleTO.getPercentage());
        map.put("mfrDesc", vehicleTO.getDescription());
        map.put("activtyGroupId", vehicleTO.getActivityGroupId());
        map.put("bodyGroupId", vehicleTO.getBodyWorkGroupId());
        map.put("status", vehicleTO.getStatus());
//        map.put("vehicleMfr", vehicleTO.getVehicleMfr());
        System.out.println("vehicleTO.getTyreMfr()" + vehicleTO.getTyreMfr());
        System.out.println("");
        if ("".equals(vehicleTO.getVehicleMfr())) {
            map.put("vehicleMfr", 0);
        } else {
            map.put("vehicleMfr", 1);
        }
        if ("".equals(vehicleTO.getTyreMfr())) {
            map.put("tyreMfr", 0);
        } else {
            map.put("tyreMfr", 1);
        }
        if ("".equals(vehicleTO.getTrailerMfr())) {
            map.put("trailerMfr", 0);
        } else {
            map.put("trailerMfr", 1);
        }
        if ("".equals(vehicleTO.getBatteryMfr())) {
            map.put("batteryMfr", 0);
        } else {
            map.put("batteryMfr", 1);
        }

        System.out.println("map" + map);
        try {
            if (vehicleTO.getMfrId() != null && vehicleTO.getMfrId() != "") {
                System.out.println("ifffff");
                map.put("mfrId", vehicleTO.getMfrId());
                status = (Integer) getSqlMapClientTemplate().update("mfr.updateMfr", map);
            } else {
                System.out.println("elseeee");
                mfrId = (Integer) getSqlMapClientTemplate().insert("mfr.insertMfr", map);

                map.put("mfrId", mfrId);
                map.put("typeId", 0);
                map.put("fuelId", 1);
                map.put("modelName", "ALL MODEL");
                map.put("Description", "ALL MODEL");
                status = (Integer) getSqlMapClientTemplate().update("mfr.insertModel", map);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;

    }

    public int saveResetVehicleKM(VehicleTO vehicleTO, int UserId) {

        int status = 0;
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", UserId);
        map.put("regNo", vehicleTO.getRegno());
        map.put("actualKm", vehicleTO.getActualKm());
        map.put("kmReading", vehicleTO.getKmReading());
        map.put("totalKm", vehicleTO.getTotalKm());
        map.put("reason", vehicleTO.getReason());

        try {
            int vehicleId = (Integer) getSqlMapClientTemplate().queryForObject("mfr.getTheVehicleId", map);
            map.put("vehicleId", vehicleId);
            System.out.println("vehid:" + vehicleId);
            System.out.println("actualKm:" + vehicleTO.getActualKm());
            System.out.println("kmReading:" + vehicleTO.getKmReading());
            System.out.println("totalKm:" + vehicleTO.getTotalKm());
            System.out.println("reason:" + map.get("reason"));
            status = (Integer) getSqlMapClientTemplate().update("mfr.saveResetKm", map);
            System.out.println("status:" + status);
            status = (Integer) getSqlMapClientTemplate().update("mfr.udpateResetKM", map);
            System.out.println("status:" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;

    }

    /**
     * This method used to Modify MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doMfrDetailsModify(ArrayList List, int UserId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        try {
            Iterator itr = List.iterator();
            VehicleTO vehicleTO = null;
            while (itr.hasNext()) {
                vehicleTO = (VehicleTO) itr.next();
                map.put("mfrId", vehicleTO.getMfrId());
                map.put("mfrName", vehicleTO.getMfrName());
                map.put("activityGroupId", vehicleTO.getActivityGroupId());
                map.put("bodyGroupId", vehicleTO.getBodyWorkGroupId());
                map.put("percentage", vehicleTO.getPercentage());
                map.put("Description", vehicleTO.getDescription());
                map.put("Active_Ind", vehicleTO.getActiveInd());
                map.put("userId", UserId);
                map.put("vehicleMfr", "vehicle");
                map.put("tyreMfr", "tyre");
                map.put("trailerMfr", "trailer");
                map.put("batteryMfr", "battery");
                System.out.println("map" + map);
                status = (Integer) getSqlMapClientTemplate().update("mfr.updateMfr", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;

    }

    public int insertModelDetails(VehicleTO vehicleTO, int UserId) {
        Map map = new HashMap();
        int status = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        try {
            map.put("userId", UserId);
            map.put("mfrId", vehicleTO.getMfrId());
            map.put("modelName", vehicleTO.getModelName());
            map.put("typeId", vehicleTO.getTypeId());
            map.put("fuelId", vehicleTO.getFuelId());
            map.put("Description", vehicleTO.getDescription());
            status = (Integer) getSqlMapClientTemplate().update("mfr.insertModel", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;

    }
    //doTypeDetails

    /**
     * This method used to Insert MFR Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doTypeDetails(VehicleTO vehicleTO, int UserId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", UserId);
        map.put("typeName", vehicleTO.getTypeName());
        map.put("tonnage", vehicleTO.getTonnage());
        map.put("capacity", vehicleTO.getCapacity());
        map.put("typeDesc", vehicleTO.getDescription());
        map.put("axleTypeId", vehicleTO.getAxleTypeId());

        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("mfr.insertType", map);
            System.out.println("status dao type= " + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;

    }

    /**
     * This method used to Modify MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doTypeDetailsModify(ArrayList List, int UserId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        try {
            Iterator itr = List.iterator();
            VehicleTO vehicleTO = null;
            while (itr.hasNext()) {
                vehicleTO = (VehicleTO) itr.next();
                map.put("typeId", vehicleTO.getTypeId());
                map.put("typeName", vehicleTO.getTypeName());
                map.put("tonnage", vehicleTO.getTonnage());
                map.put("capacity", vehicleTO.getCapacity());
                map.put("Description", vehicleTO.getDescription());
                map.put("Active_Ind", vehicleTO.getActiveInd());
                map.put("userId", UserId);
                status = (Integer) getSqlMapClientTemplate().update("mfr.updateType", map);
                System.out.println("altertype" + status);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;

    }

    /**
     * This method used to Modify MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doModelDetailsModify(ArrayList List, int UserId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        try {
            Iterator itr = List.iterator();
            VehicleTO vehicleTO = null;
            while (itr.hasNext()) {
                vehicleTO = new VehicleTO();
                vehicleTO = (VehicleTO) itr.next();
                map.put("userId", UserId);
                map.put("mfrId", vehicleTO.getMfrId());
                map.put("modelName", vehicleTO.getModelName());
                map.put("typeId", vehicleTO.getTypeId());
                map.put("fuelId", vehicleTO.getFuelId());
                map.put("Active_Ind", vehicleTO.getActiveInd());
                map.put("modelId", vehicleTO.getModelId());
                map.put("Description", vehicleTO.getDescription());
                status = (Integer) getSqlMapClientTemplate().update("mfr.updateModel", map);
                System.out.println("ststus=" + status);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;

    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getUsageList() {
        Map map = new HashMap();
        ArrayList usageList = new ArrayList();
        try {

            usageList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.UsageList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "usageList", sqlException);
        }
        return usageList;

    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getClassList() {
        Map map = new HashMap();
        ArrayList usageList = new ArrayList();
        try {

            usageList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.ClassList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "usageList", sqlException);
        }
        return usageList;

    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getopList() {
        Map map = new HashMap();
        ArrayList operationPointList = new ArrayList();
        try {

            operationPointList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.OperationPointList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "OperationPointList", sqlException);
        }
        return operationPointList;

    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getspList() {
        Map map = new HashMap();
        ArrayList servicePointList = new ArrayList();
        try {

            servicePointList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.ServicePointList", map);
            System.out.println("servicePointList" + servicePointList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "ServicePointList", sqlException);
        }
        return servicePointList;

    }

    //doTypeDetails
    /**
     * This method used to Insert MFR Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String doAddVehicleDetails(VehicleTO vehicleTO, int UserId) {
        System.out.println("vehicle DAO Throttle Here ");
        Map map = new HashMap();

        /*
         * set the parameters in the map for sending to ORM
         */
        String ownerS = "";
        map.put("regNo", vehicleTO.getRegNo());
        map.put("userId", UserId);
        map.put("mfrId", vehicleTO.getMfrId());
        map.put("groupId", vehicleTO.getGroupId());
        map.put("modelId", vehicleTO.getModelId());
        map.put("dateOfSale", vehicleTO.getDateOfSale());
        map.put("chassisNo", vehicleTO.getChassisNo());
        map.put("description", vehicleTO.getDescription());
        map.put("seats", vehicleTO.getSeatCapacity());
        if ("3".equals(vehicleTO.getOwnerShips()) || "4".equals(vehicleTO.getOwnerShips()) || "2".equals(vehicleTO.getOwnerShips())) {
            map.put("war_period", 0);
            map.put("hourMeter", 0);
            map.put("kiloMeter", 0);
            map.put("dailyRunKm", 0);
            map.put("dailyRunHm", 0);
            map.put("gpsSystemId", 0);
        } else {
            map.put("war_period", vehicleTO.getWar_period());
            map.put("hourMeter", vehicleTO.getHmReading());
            map.put("kiloMeter", vehicleTO.getKmReading());
            map.put("dailyRunKm", vehicleTO.getDailyKm());
            map.put("dailyRunHm", vehicleTO.getDailyHm());
            map.put("gpsSystemId", vehicleTO.getGpsSystemId());
        }
        map.put("fservice", vehicleTO.getFservice());
        map.put("vehicleCost", vehicleTO.getVehicleCost());
        map.put("vehicleDepreciation", vehicleTO.getVehicleDepreciation());
        map.put("vehicleColor", vehicleTO.getVehicleColor());
//      map.put("ownership", vehicleTO.getLeasingCustId());
        map.put("leasingCustId", vehicleTO.getLeasingCustId());
        map.put("ownership", vehicleTO.getAsset());
        map.put("typeId", vehicleTO.getTypeId());
        map.put("vendorId", vehicleTO.getVendorId());
        ownerS = vehicleTO.getAsset();
        if (ownerS.equalsIgnoreCase("Own")) {
            map.put("groupCode", "ACC-0003");
        } else {
            map.put("groupCode", "");
        }
        map.put("gpsSystem", vehicleTO.getGpsSystem());
        map.put("warrantyDate", vehicleTO.getWarrantyDate());
        map.put("axles", vehicleTO.getAxles());
        map.put("ownerShips", vehicleTO.getOwnerShips());
        System.out.println("map first:::::: = " + map);
        int status = 0;
        String code = "";
        String[] temp;
        int vehicle_id = 0;
        int currentYear = 0;
        String newYear = "";
        int VehicleLedgerId = 0;
        String truckNo = "";
        try {

            int vehicleNo = (Integer) getSqlMapClientTemplate().insert("mfr.insertAddVehicleNo", map);
            truckNo = ThrottleConstants.tahoosTruckNoPrefix + vehicleNo;
            map.put("truckNo", truckNo);
            vehicle_id = (Integer) getSqlMapClientTemplate().insert("mfr.insertAddVehicle", map);
            System.out.println("vehicleId = " + vehicle_id);
            //srini commented on 21 march 13
            /*
             if (vehicle_id != 0) {

             //Ledger Code start
             code = (String) getSqlMapClientTemplate().queryForObject("mfr.getLedgerCode", map);
             temp = code.split("-");
             int codeval = Integer.parseInt(temp[1]);
             int codev = codeval + 1;
             String ledgercode = "LEDGER-" + codev;
             map.put("ledgercode", ledgercode);
             map.put("regNo", vehicleTO.getRegNo());
             //Ledger Code end

             //current year and month start
             String accYear = (String) getSqlMapClientTemplate().queryForObject("mfr.accYearVal", map);
             System.out.println("accYear:" + accYear);
             map.put("accYear", accYear);

             //                currentYear = Calendar.getInstance().get(Calendar.YEAR);
             //                System.out.println("currentYear = " +currentYear);
             //
             //                int currentMonth = Calendar.getInstance().get(Calendar.MONTH);
             //                System.out.println("currentMonth = " +currentMonth);
             //                if(currentMonth > 4 ){
             //                     newYear = currentYear+"-"+(currentYear + 1);
             //                }else{
             //                    newYear = (currentYear - 1)+"-"+currentYear;
             //                }
             //                System.out.println("newYear = " + newYear);
             //                map.put("newYear", newYear);

             //current year end

             VehicleLedgerId = (Integer) getSqlMapClientTemplate().insert("mfr.insertVehicleLedger", map);
             System.out.println("VehicleLedgerId......."+VehicleLedgerId);
             if(VehicleLedgerId != 0){
             map.put("vehicle_id", vehicle_id);
             System.out.println("map update ::::=> " + map);
             status = (Integer) getSqlMapClientTemplate().update("mfr.updateVehicleMaster", map);
             }
             }
             */
            //srini commented on 21 march 13

            //Ledger Code start
            if (vehicle_id != 0) {
                code = (String) getSqlMapClientTemplate().queryForObject("vendor.getLedgerCode", map);
                temp = code.split("-");
                int codeval = Integer.parseInt(temp[1]);
                int codev = codeval + 1;
                String ledgercode = "LEDGER-" + codev;
                map.put("ledgercode", ledgercode);

                //current year and month start
                String accYear = (String) getSqlMapClientTemplate().queryForObject("vendor.accYearVal", map);
                System.out.println("accYear:" + accYear);
                map.put("accYear", ThrottleConstants.financialYear);
                //current year end

                map.put("groupCode", ThrottleConstants.fleetGroupCode);
                map.put("levelId", ThrottleConstants.fleetLevelId);

                VehicleLedgerId = (Integer) getSqlMapClientTemplate().insert("mfr.insertVehicleLedger", map);
                System.out.println("VehicleLedgerId......." + VehicleLedgerId);
                if (VehicleLedgerId != 0) {

                    map.put("ledgerId", VehicleLedgerId);
                    map.put("vehicle_id", vehicle_id);
                    System.out.println("map update ::::=> " + map);
                    status = (Integer) getSqlMapClientTemplate().update("mfr.updateVehicleMaster", map);
                }

            }
            //Ledger Code end

//            vehicle_id = (Integer) getSqlMapClientTemplate().queryForObject("mfr.getVehicleId", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */

            FPLogUtils.fpDebugLog("doAddVehicleDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "doAddVehicleDetails", sqlException);
        }
        return vehicle_id + "~" + truckNo;
    }

    public int doAlterVehicleDetails(VehicleTO vehicleTO, int UserId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
//        String[] vehicleTypeId=vehicleTO.getVehicleTypeId().split("~");

//        map.put("vehicleTypeId", vehicleTypeId[0]);
        map.put("regNo", vehicleTO.getRegNo());
        map.put("userId", UserId);
        map.put("mfrId", vehicleTO.getMfrId());
        map.put("groupId", vehicleTO.getGroupId());
        map.put("modelId", vehicleTO.getModelId());
        map.put("dateOfSale", vehicleTO.getDateOfSale());
        map.put("chassisNo", vehicleTO.getChassisNo());
        map.put("war_period", vehicleTO.getWar_period());
        map.put("seatCapacity", vehicleTO.getSeatCapacity());
        map.put("vehicleCost", vehicleTO.getVehicleCost());
        map.put("hourMeter", vehicleTO.getHmReading());
        map.put("kiloMeter", vehicleTO.getKmReading());
        map.put("currentKm", vehicleTO.getDailyKm());
        map.put("currentHm", vehicleTO.getDailyHm());
        map.put("description", vehicleTO.getDescription());
        map.put("activeInd", vehicleTO.getActiveInd());
        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("remarks", vehicleTO.getDescription());
        map.put("fservice", vehicleTO.getFservice());
        map.put("gpsSystem", vehicleTO.getGpsSystem());
        map.put("typeId", vehicleTO.getTypeId());
        map.put("ownership", vehicleTO.getOwnerShips());
        map.put("vehicleDepreciation", vehicleTO.getVehicleDepreciation());
        System.out.println("vehicle map====" + map);
        System.out.println("vehicleTO.getLeasingCustId() ============== " + vehicleTO.getLeasingCustId());
        if (vehicleTO.getOwnership().equalsIgnoreCase("1")) {
            map.put("VendorId", "");
        } else {
            System.out.println("else");
            map.put("VendorId", vehicleTO.getLeasingCustId());

        }

        System.out.println("map uuuuuuuuuuuuuuu => " + map);
        int status = 0;
        try {

            status = (Integer) getSqlMapClientTemplate().update("mfr.updateVehicle", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return Integer.parseInt(vehicleTO.getVehicleId());
    }

    public int doAddVehicleClass(VehicleTO vehicleTO, int UserId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("classId", vehicleTO.getClassId());
        map.put("userId", UserId);
        map.put("usageId", vehicleTO.getTypeId());

        String[] DOSdate = vehicleTO.getDateOfSale().split("-");
        String DOS = DOSdate[2] + "-" + DOSdate[1] + "-" + DOSdate[0];
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("mfr.insertVehClass", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public int doInsertVehicleClass(VehicleTO vehicleTO, int UserId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("classId", vehicleTO.getClassId());
        map.put("userId", UserId);
        int status = 0;
        int insertStatus = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().queryForObject("mfr.checkVehClass", map);
            if (status == 0) {
                System.out.println("i m in if...");
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehClass", map);
            } else {
                System.out.println("i m in else...");
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.updateVehClass", map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehClass", map);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return insertStatus;
    }

    public int doInsertVehicleEngine(VehicleTO vehicleTO, int UserId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("engineNo", vehicleTO.getEngineNo());
        map.put("userId", UserId);

        int status = 0;
        int insertStatus = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().queryForObject("mfr.checkVehEngine", map);
            if (status == 0) {
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehEngine", map);
            } else {
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.updateVehEngine", map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehEngine", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doInsertVehicleEngine Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "doInsertVehicleEngine", sqlException);
        }
        return insertStatus;
    }

    public int doInsertVehicleOper(VehicleTO vehicleTO, int UserId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("operationId", vehicleTO.getOpId());
        map.put("userId", UserId);

        int status = 0;
        int insertStatus = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().queryForObject("mfr.checkVehOperation", map);
            if (status == 0) {
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehOperation", map);
            } else {
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.updateVehOperation", map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehOperation", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doInsertVehicleOper Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "doInsertVehicleOper", sqlException);
        }
        return insertStatus;
    }

    public int doInsertVehicleFc(VehicleTO vehicleTO, int UserId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("fcDate", vehicleTO.getNextFCDate());
        map.put("userId", UserId);

        int status = 0;
        int insertStatus = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().queryForObject("mfr.checkVehFc", map);
            if (status == 0) {
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehFc", map);
            } else {
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.updateVehFc", map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehFc", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doInsertVehicleFc Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "doInsertVehicleFc", sqlException);
        }
        return insertStatus;
    }

    public int doInsertVehicleReg(VehicleTO vehicleTO, int UserId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("regNo", vehicleTO.getRegNo());
        map.put("userId", UserId);
        map.put("typeId", vehicleTO.getTypeId());
        System.out.println("map for regNo:" + map);
        int status = 0;
        int insertStatus = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().queryForObject("mfr.checkVehReg", map);
            if (status == 0) {
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehReg", map);
            } else {
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.updateVehReg", map);
                System.out.println("update reg:" + insertStatus);
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehReg", map);
                System.out.println("insert reg:" + insertStatus);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doInsertVehicleReg Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "doInsertVehicleReg", sqlException);
        }
        return insertStatus;
    }

    public int doInsertVehicleTyre(VehicleTO vehicleTO, int UserId, int companyId) {

//        String MfrIds[] = vehicleTO.getMfrIds();
//        String Modelds[] = vehicleTO.getModelIds();
        String[] positionIds = vehicleTO.getPositionId();
//        String tyreType[] = vehicleTO.getTyreType();
        String[] itemIds = vehicleTO.getItemIds();
        String[] tyreIds = vehicleTO.getTyreIds();
        String[] tyreDate = vehicleTO.getTyreDate();

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        int insertStatus = 0;
        String vehicleId = "";
        int tyreId = 0;
        Integer tyreNo;
        try {
            System.out.println("check2  :");
            if (tyreIds != null) {
                System.out.println("check3  :");
                for (int i = 0; i < tyreIds.length; i++) {
                    if (!itemIds[i].equals("0")) {
                        map.put("tyreNo", tyreIds[i]);
                        map.put("positionId", positionIds[i]);
                        map.put("itemId", itemIds[i]);
                        map.put("companyId", companyId);
                        map.put("tyreDate", tyreDate[i]);
                        map.put("userId", UserId);
                        System.out.println("map:11" + map);
                        if (!vehicleTO.getVehicleId().equalsIgnoreCase("0")) {
                            map.put("vehicleId", vehicleTO.getVehicleId());
                        } else {
                            map.put("regNo", vehicleTO.getRegNos()[i]);
                            vehicleId = (String) getSqlMapClientTemplate().queryForObject("mfr.vehicleId", map);
                            map.put("vehicleId", vehicleId);
                        }

                        tyreNo = (Integer) getSqlMapClientTemplate().queryForObject("mfr.checkTyreNo", map);

                        if (tyreNo == null) {
                            tyreId = (Integer) getSqlMapClientTemplate().insert("mfr.insertVehicleTyre", map);
                            map.put("tyreId", tyreId);
                            if (tyreId != 0) {
                                System.out.println("map tessssssssssssssssssssssss => " + map);
                                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehTyre", map);
                            }
                        } else {
                            map.put("tyreId", tyreNo);
                            insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.updateVehTyre", map);
                            insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehTyre", map);
                        }

                    }
                }
//                status = (Integer) getSqlMapClientTemplate().queryForObject("mfr.checkVehTyre",map);
//                else{
//                    insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.updateVehTyre", map);
//                    insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehTyre", map);
//                }
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doInsertVehicleTyre Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "doInsertVehicleTyre", sqlException);
        }
        return insertStatus;
    }

    public ArrayList doInsertVehicleTyre1(VehicleTO vehicleTO, int UserId, int companyId) {

//        String MfrIds[] = vehicleTO.getMfrIds();
//        String Modelds[] = vehicleTO.getModelIds();
        String[] positionIds = vehicleTO.getPositionId();
//        String tyreType[] = vehicleTO.getTyreType();
        String[] itemIds = vehicleTO.getItemIds();
        String[] tyreIds = vehicleTO.getTyreIds();
        String[] tyreDate = vehicleTO.getTyreDate();
        VehicleTO vehiTO = null;
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList tyreIdList = new ArrayList();

        int status = 0;
        int insertStatus = 0;
        String vehicleId = "";
        int tyreId = 0;
        Integer tyreNo;
        try {
            if (tyreIds != null) {
                for (int i = 0; i < tyreIds.length; i++) {
                    if (!itemIds[i].equals("0")) {
                        vehiTO = new VehicleTO();
                        map.put("tyreNo", tyreIds[i]);
                        map.put("positionId", positionIds[i]);
                        map.put("itemId", itemIds[i]);
                        map.put("companyId", companyId);
                        map.put("tyreDate", tyreDate[i]);
                        map.put("userId", UserId);
                        if (!vehicleTO.getVehicleId().equalsIgnoreCase("0")) {
                            map.put("vehicleId", vehicleTO.getVehicleId());
                        } else {
                            map.put("regNo", vehicleTO.getRegNos()[i]);
                            vehicleId = (String) getSqlMapClientTemplate().queryForObject("mfr.vehicleId", map);
                            map.put("vehicleId", vehicleId);
                        }

                        tyreNo = (Integer) getSqlMapClientTemplate().queryForObject("mfr.checkTyreNo", map);

                        if (tyreNo == null) {
                            tyreId = (Integer) getSqlMapClientTemplate().insert("mfr.insertVehicleTyre", map);
                            map.put("tyreId", tyreId);
                            if (tyreId != 0) {
                                vehiTO.setTypeId(String.valueOf(tyreId));

                                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehTyre", map);
                            }
                        } else {
                            map.put("tyreId", tyreNo);
                            vehiTO.setTypeId(String.valueOf(tyreNo));
                            insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.updateVehTyre", map);
                            insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehTyre", map);
                        }
                        tyreIdList.add(vehiTO);
                    }
                }
//                status = (Integer) getSqlMapClientTemplate().queryForObject("mfr.checkVehTyre",map);
//                else{
//                    insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.updateVehTyre", map);
//                    insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehTyre", map);
//                }
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doInsertVehicleTyre Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "doInsertVehicleTyre", sqlException);
        }
        return tyreIdList;
    }

    public int doAlterVehicleTyre(VehicleTO vehicleTO, int companyId, int userId) {

        String positionIds[] = vehicleTO.getPositionId();
        String itemIds[] = vehicleTO.getItemIds();
        String tyreIds[] = vehicleTO.getTyreIds();
        String tyreNos[] = vehicleTO.getTyreNos();
        String tyreDate[] = vehicleTO.getTyreDate();

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        int status = 0;
        int insertStatus = 0;
        int tyreId = 0;
        Integer tyreNo;
        try {
            map.put("vehicleId", vehicleTO.getVehicleId());
            map.put("userId", userId);
            map.put("companyId", companyId);
            for (int i = 0; i < tyreIds.length; i++) {
                if (!itemIds[i].equals("0")) {
                    map.put("tyreId", tyreIds[i]);
                    map.put("tyreNo", tyreNos[i]);
                    map.put("positionId", positionIds[i]);
                    map.put("itemId", itemIds[i]);
                    map.put("tyreDate", tyreDate[i]);

//                tyreNo = (Integer) getSqlMapClientTemplate().queryForObject("mfr.checkTyreNo",map);              
                    if (tyreIds[i].equals("0")) {
                        tyreId = (Integer) getSqlMapClientTemplate().insert("mfr.insertVehicleTyre", map);
                        map.put("tyreId", tyreId);
                        if (tyreId != 0) {
                            insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehTyre", map);
                        }
                    } else {
                        map.put("tyreId", tyreIds[i]);
                        insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.alterTyreDetails", map);
//                    insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehTyre", map);                    
                    }
                }
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doInsertVehicleTyre Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "doInsertVehicleTyre", sqlException);
        }
        return insertStatus;
    }

    public int doInsertVehicleUsage(VehicleTO vehicleTO, int UserId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("usageTypeId", vehicleTO.getUsageId());
        map.put("userId", UserId);

        int status = 0;
        int insertStatus = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().queryForObject("mfr.checkVehUsageType", map);
            if (status == 0) {
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehUsageType", map);
            } else {
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.updateVehUsageType", map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehUsageType", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doInsertVehicleUsage Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "doInsertVehicleUsage", sqlException);
        }
        return insertStatus;
    }

    public int doInsertVehicleCust(VehicleTO vehicleTO, int UserId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("custId", vehicleTO.getCustId());
        map.put("userId", UserId);

        int status = 0;
        int insertStatus = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().queryForObject("mfr.checkVehCust", map);
            if (status == 0) {
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehCust", map);
            } else {
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.updateVehCust", map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehCust", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doInsertVehicleCust Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "doInsertVehicleCust", sqlException);
        }
        return insertStatus;
    }

    public int doInsertVehicleHist(VehicleTO vehicleTO, int UserId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vehicleId", vehicleTO.getVehicleId());
        if ("".equals(vehicleTO.getDailyKm())) {
            map.put("km", 0);
        } else {
            map.put("km", vehicleTO.getDailyKm());
        }
        if ("".equals(vehicleTO.getDailyHm())) {
            map.put("hm", 0);
        } else {
            map.put("hm", vehicleTO.getDailyHm());
        }
        map.put("userId", UserId);
        System.out.println("map..." + map);
        int status = 0;
        int insertStatus = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().queryForObject("mfr.checkVehHistory", map);
            if (status == 0) {
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehHistory", map);
            } else {
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.updateVehHistory", map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehHistory", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doInsertVehicleHist Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "doInsertVehicleHist", sqlException);
        }
        return insertStatus;
    }

    public ArrayList getVehicleList(VehicleTO vehicleTO) {
        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        if (vehicleTO.getRoleId().equals(null)) {
            map.put("roleId", vehicleTO.getRoleId());
            System.out.println("1");
        } else {
            map.put("roleId", "");
            System.out.println("2");
        }
        if (vehicleTO.getCompanyId() != null) {
            map.put("companyId", vehicleTO.getCompanyId());
        } else {
            map.put("companyId", "0");
        }
        map.put("regNo", vehicleTO.getRegNo());
        map.put("mfrId", vehicleTO.getMfrId());
        map.put("groupId", vehicleTO.getGroupId());
        map.put("usageId", vehicleTO.getUsageId());
        map.put("typeId", vehicleTO.getTypeId());

        map.put("custId", vehicleTO.getCustId());
        map.put("startIndex", vehicleTO.getStartIndex());
        map.put("endIndex", vehicleTO.getEndIndex());
        map.put("ownership", vehicleTO.getOwnership());

        System.out.println("vehicleTO.getRegNo():" + vehicleTO.getRegNo());
        System.out.println("vehicleTO.getMfrId():" + vehicleTO.getMfrId());
        System.out.println("vehicleTO.getGroupId():" + vehicleTO.getGroupId());
        System.out.println("vehicleTO.getUsageId():" + vehicleTO.getUsageId());
        System.out.println("vehicleTO.getTypeId():" + vehicleTO.getTypeId());
        System.out.println("vehicleTO.getCompanyId():" + vehicleTO.getCompanyId());
        System.out.println("vehicleTO.getCustId():" + vehicleTO.getCustId());
        System.out.println("vehicleTO.getStartIndex():" + vehicleTO.getStartIndex());
        System.out.println("vehicleTO.getEndIndex():" + vehicleTO.getEndIndex());
        System.out.println("vehicleTO.getRoleId():" + vehicleTO.getRoleId());
        System.out.println("vehicleTO.roleId():" + vehicleTO.getRoleId());

        try {
            System.out.println("map:getVehicleList  *********" + map);
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.searchVehicles", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getVehicleList", sqlException);
        }
        return vehicleList;
    }

    public ArrayList getVehicleDetail(VehicleTO vehicleTO) {

        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */

        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("typeId", vehicleTO.getTypeId());

//        System.out.println("vehicleList12345678900= = " + vehicleList);
        try {

            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.vehicleDetails", map);
            System.out.println("vehicleList 0967456352= " + vehicleList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getVehicleDetail", sqlException);
        }
        return vehicleList;
    }

    public ArrayList getTyreItems() {

        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        try {
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getTyreItems", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTyreItems Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getTyreItems", sqlException);
        }
        return vehicleList;
    }

    public ArrayList getGroupList() {

        Map map = new HashMap();
        ArrayList groupList = new ArrayList();
        try {
            groupList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getGroupList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getGroupList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getGroupList", sqlException);
        }
        return groupList;
    }

    public ArrayList getAvailableTyres(int itemId) {

        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        try {
            map.put("itemId", itemId);
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getAvailableTyres", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAvailableTyres Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getAvailableTyres", sqlException);
        }
        return vehicleList;
    }

    public ArrayList getVehicleTyreDetails(int vehicleId) {

        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();

        map.put("vehicleId", vehicleId);

        try {

            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.vehicleTyreDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getVehicleDetail", sqlException);
        }
        return vehicleList;
    }

    public ArrayList getTyrePostions() {
        Map map = new HashMap();
        int qty = 0;
        ArrayList itemList = new ArrayList();
        try {

            itemList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getTyrePostions", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTyrePostions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-02", CLASS, "getTyrePostions", sqlException);
        }
        return itemList;
    }

    public String checkVehicleTyre(String tyreNo) {
        Map map = new HashMap();
        String status = "";

        String mess = "";
        try {

            map.put("tyreNo", tyreNo);
            status = (String) getSqlMapClientTemplate().queryForObject("mfr.checkVehTyreExists", map);
            if (status != null) {
                mess = status;
            }

            System.out.println("checkVehicleTyre" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkVehicleTyre Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkVehicleTyre", sqlException);
        }

        return mess;
    }

    public int vehicleKmUpdate(int vehicleId, String km, String hm, int userId) {
        Map map = new HashMap();
        String status = "";
        int stat = 0;
        int lastKm = 0;
        int diffKm = 0;

        String mess = "";
        try {
            System.out.println("i m here");
            if ("".equals(km) || km == null) {

                map.put("km", 0);
            } else {
                map.put("km", km);
            }
            map.put("vehicleId", vehicleId);
            if ("".equals(hm) || hm == null) {
                map.put("hm", 0);
            } else {
                map.put("hm", hm);
            }
            map.put("userId", userId);
            System.out.println("map := " + map);
            status = (String) getSqlMapClientTemplate().queryForObject("mfr.checkVehicleLastKm", map);
            if (status == null) {
                stat = (Integer) getSqlMapClientTemplate().update("mfr.insertVehicleKm", map);
            } else {
                lastKm = Integer.parseInt(status);
                diffKm = Integer.parseInt(km) - lastKm;
                map.put("diffKm", diffKm);
                stat = (Integer) getSqlMapClientTemplate().update("mfr.updateLastKm", map);
            }

            System.out.println("checkVehicleTyre" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkVehicleTyre Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkVehicleTyre", sqlException);
        }

        return stat;
    }

    public int saveVehicleFinance(VehicleTO vehicleTO, int companyId, int userId) {
        int count = 0;
        int status = 0;
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        map.put("userId", userId);
        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("bankerName", vehicleTO.getBankerName());
        map.put("bankerAddress", vehicleTO.getBankerAddress());
        map.put("financeAmount", vehicleTO.getFinanceAmount());
        map.put("roi", vehicleTO.getRoi());
        map.put("interestType", vehicleTO.getInterestType());
        map.put("emiMonths", vehicleTO.getEmiMonths());
        map.put("emiAmount", vehicleTO.getEmiAmount());
        map.put("emiStartDate", vehicleTO.getEmiStartDate());
        map.put("emiEndDate", vehicleTO.getEmiEndDate());
        map.put("emiPayDay", vehicleTO.getEmiPayDay());
        map.put("payMode", vehicleTO.getPayMode());

        try {
            status = (Integer) getSqlMapClientTemplate().update("mfr.updateVehicleFinance", map);
            System.out.println("status in vehicle finance " + status);
            if (status == 0) {
                status = (Integer) getSqlMapClientTemplate().update("mfr.insertVehicleFinance", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "saveVehicleInsurance", sqlException);
        }
        return status;

    }

    public int saveFleetPurchase(VehicleTO vehicleTO, int companyId, int userId) {
        int count = 0;
        int status = 0;
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        map.put("userId", userId);
        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("bankId", vehicleTO.getBankId());

        map.put("trailerId", vehicleTO.getTrailerId());
        map.put("vendorId", vehicleTO.getVendorId());
        map.put("fleetCost", vehicleTO.getFleetCost());
        map.put("otherCost", vehicleTO.getOtherCost());
        map.put("totalCost", vehicleTO.getTotalCost());
        map.put("financeStatus", vehicleTO.getFinanceStatus());
        map.put("initialAmount", vehicleTO.getInitialAmount());
        map.put("processingFee", vehicleTO.getProcessingFee());
        map.put("otherCharges", vehicleTO.getOtherCharges());

        map.put("financeAmount", vehicleTO.getFinanceAmount());
        map.put("roi", vehicleTO.getRoi());
        map.put("interestType", vehicleTO.getInterestType());
        map.put("emiMonths", vehicleTO.getEmiMonths());
        map.put("emiAmount", vehicleTO.getEmiAmount());
        map.put("emiStartDate", vehicleTO.getEmiStartDate());
        map.put("emiEndDate", vehicleTO.getEmiEndDate());
        map.put("emiPayDay", vehicleTO.getEmiPayDay());
        map.put("payMode", vehicleTO.getPayMode());
        map.put("remarks", vehicleTO.getRemarks());
        System.out.println("fleet purchase map:" + map);
        int purchaseId = 0;
        try {
//            status = (Integer) getSqlMapClientTemplate().update("mfr.updateVehicleFinance", map);
//            System.out.println("status in vehicle finance " + status);
//            if (status == 0) {

            status = (Integer) getSqlMapClientTemplate().insert("mfr.insertFleetPurchase", map);
            purchaseId = status;
//            }
            String reference = "";
            String ledgerDetails = "";
            String fleetLedgerId = "";
            String fleetLedgerCode = "";
            if (!"0".equals(vehicleTO.getTrailerId())) {
                reference = "Trailer No";
                ledgerDetails = (String) getSqlMapClientTemplate().queryForObject("mfr.getTrailerLedgerId", map);
                String[] temp2 = ledgerDetails.split("~");
                fleetLedgerId = temp2[0];
                fleetLedgerCode = temp2[1];
            } else {
                ledgerDetails = (String) getSqlMapClientTemplate().queryForObject("mfr.getTruckLedgerId", map);
                String[] temp2 = ledgerDetails.split("~");
                fleetLedgerId = temp2[0];
                fleetLedgerCode = temp2[1];
                reference = "Truck No";
            }
            int formId = 0;
            int voucherNo = 0;
            map.put("vendorNameId", vehicleTO.getVendorId());
            String vendorLedgerDetails = (String) getSqlMapClientTemplate().queryForObject("purchase.getVendorLedgerId", map);
            String[] temp2 = vendorLedgerDetails.split("~");
            String vendorLedgerId = temp2[0];
            String vendorLedgerCode = temp2[1];
            if (status > 0) {
                formId = Integer.parseInt(ThrottleConstants.purchaseFormId);
                map.put("formId", formId);
                voucherNo = (Integer) getSqlMapClientTemplate().insert("purchase.getFormVoucherNo", map);
                map.put("voucherNo", voucherNo);
                map.put("voucherCodeNo", ThrottleConstants.purchaseVoucherCode + voucherNo);

                String voucherCode = "PURCHASE-" + voucherNo;
                map.put("detailCode", "1");
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "PAYMENT");
                map.put("ledgerId", fleetLedgerId);
                map.put("particularsId", fleetLedgerCode);
                map.put("amount", vehicleTO.getTotalCost());
                map.put("accountsType", "DEBIT");
                map.put("narration", reference);
                map.put("searchCode", vehicleTO.getRegNo());
                map.put("reference", reference);

                System.out.println("map1 updateClearnceDate=---------------------> " + map);
                status = (Integer) getSqlMapClientTemplate().update("mfr.insertAccountEntry", map);
                System.out.println("status1 = " + status);
                //--------------------------------- acc 2nd row start --------------------------
                if (status > 0) {
                    map.put("detailCode", "2");
                    map.put("ledgerId", vendorLedgerId);
                    map.put("particularsId", vendorLedgerCode);
                    map.put("accountsType", "CREDIT");
                    System.out.println("map2 updateClearnceDate=---------------------> " + map);
                    status = (Integer) getSqlMapClientTemplate().update("mfr.insertAccountEntry", map);
                    System.out.println("status2 = " + status);
                }

            //papl_payment_entry start
//                papl_payment_details(payment_code, reference_id,reference_name,payment_Amount,vendor_id,payment_Date,Created_On)
//        values(#paymentReferenceCode#,#paymentReferenceId#,#paymentReferenceName#,#paymentAmount#,#vendorNameId#,
//                        STR_TO_DATE(#invoiceDate#,'%d-%m-%Y'),convert_tz(now(),@@session.time_zone,'+05:30'));
                String paymentReferenceCode = ThrottleConstants.fleetPurchaseFormCode + purchaseId;
                map = new HashMap();
                map.put("paymentReferenceCode", paymentReferenceCode);
                map.put("paymentReferenceId", purchaseId);
                map.put("paymentReferenceName", "Fleet Purchase");
                map.put("paymentAmount", vehicleTO.getTotalCost());
                map.put("vendorNameId", vehicleTO.getVendorId());
                System.out.println("papl_payment_entry map:" + map);
                status = (Integer) getSqlMapClientTemplate().update("mfr.insertPaymentDetails", map);
                System.out.println("papl_payment_entry statu:" + status);
                //papl_payment_entry end

            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "saveVehicleInsurance", sqlException);
        }
        return status;

    }
    //-----------Vehicle Insurance Updates starts here---------------

    public String getVehicleDetails(String regNo) {
        System.out.println("..........ajax in DAO.....................");
        Map map = new HashMap();
        String vehicleDetails = "";
        try {
            map.put("regNo", regNo);
            vehicleDetails = (String) getSqlMapClientTemplate().queryForObject("mfr.vehicleDeailsInsurance", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkVehicleTyre Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleDetails", sqlException);
        }

        return vehicleDetails;
    }

    //-----------Vehicle Insurance Updates starts here---------------
    public String getVehicleExisting(String regNo, String vPAge) {
        Map map = new HashMap();
        String vehicleDetails = "";
        try {
            map.put("regNo", regNo);
            if (vPAge.equals("Finance")) {
                vehicleDetails = (String) getSqlMapClientTemplate().queryForObject("mfr.getVehicleFinExisting", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkVehicleTyre Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleDetails", sqlException);
        }

        return vehicleDetails;
    }

    /**
     * This method used to Insert MFR Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int saveVehicleInsurance(VehicleTO vehicleTO, int companyId, int userId) {
        int count = 0;
        int status = 0;
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", userId);
        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("insuranceCompanyName", vehicleTO.getInsuranceCompanyName());
        map.put("premiumNo", vehicleTO.getPremiumNo());
        map.put("premiumPaidDate", vehicleTO.getPremiumPaidDate());
        map.put("premiumPaidAmount", vehicleTO.getPremiumPaidAmount());
        map.put("vehicleValue", vehicleTO.getVehicleValue());
        map.put("premiumExpiryDate", vehicleTO.getPremiumExpiryDate());
        map.put("insuarnceNCB", vehicleTO.getInsuarnceNCB());
        map.put("insuarnceRemarks", vehicleTO.getInsuarnceRemarks());

        System.out.println("userId: " + userId);
        System.out.println("vehicleTO.getVehicleId(): " + vehicleTO.getVehicleId());
        System.out.println("vehicleTO.getInsuranceCompanyName(): " + vehicleTO.getInsuranceCompanyName());
        System.out.println("vehicleTO.getPremiumNo(): " + vehicleTO.getPremiumNo());
        System.out.println("vehicleTO.getPremiumPaidDate(): " + vehicleTO.getPremiumPaidDate());
        System.out.println("vehicleTO.getPremiumPaidAmount(): " + vehicleTO.getPremiumPaidAmount());
        System.out.println("vehicleTO.getVehicleValue(): " + vehicleTO.getVehicleValue());
        System.out.println("vehicleTO.getPremiumExpiryDate(): " + vehicleTO.getPremiumExpiryDate());
        System.out.println("vehicleTO.getInsuarnceNCB(): " + vehicleTO.getInsuarnceNCB());
        System.out.println("vehicleTO.getInsuarnceRemarks(): " + vehicleTO.getInsuarnceRemarks());
        try {
            count = (Integer) getSqlMapClientTemplate().queryForObject("mfr.vehicleInsuranceCount", map);
            System.out.println("count in insurance " + count);
            if (count == 0) {
                status = (Integer) getSqlMapClientTemplate().update("mfr.insertVehicleInsurance", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "saveVehicleInsurance", sqlException);
        }
        return status;

    }

    /**
     * This method used to Insert MFR Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int saveVehicleAMC(VehicleTO vehicleTO, int companyId, int userId) {
        int count = 0;
        int status = 0;
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", userId);
        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("amcCompanyName", vehicleTO.getAmcCompanyName());
        map.put("amcAmount", vehicleTO.getAmcAmount());
        map.put("amcDuration", vehicleTO.getAmcDuration());
        map.put("amcChequeNo", vehicleTO.getAmcChequeNo());
        map.put("amcChequeDate", vehicleTO.getAmcChequeDate());
        map.put("amcFromDate", vehicleTO.getAmcFromDate());
        map.put("amcToDate", vehicleTO.getAmcToDate());
        map.put("amcRemarks", vehicleTO.getAmcRemarks());

        try {
            count = (Integer) getSqlMapClientTemplate().queryForObject("mfr.vehicleAMCCount", map);
            System.out.println("count in AMC " + count);
            if (count == 0) {
                status = (Integer) getSqlMapClientTemplate().update("mfr.insertVehicleAMC", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "saveVehicleInsurance", sqlException);
        }
        return status;

    }

    /**
     * This method used to Vehicle Accident
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int saveVehicleAccident(VehicleTO vehicleTO, int companyId, int userId) {
        int count = 0;
        int status = 0;
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", userId);
        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("accidentSpot", vehicleTO.getAccidentSpot());
        map.put("accidentDate", vehicleTO.getAccidentDate());
        map.put("driverName", vehicleTO.getDriverName());
        map.put("policeStation", vehicleTO.getPoliceStation());
        map.put("firNo", vehicleTO.getFirNo());
        map.put("firDate", vehicleTO.getFirDate());
        map.put("firPreparedBy", vehicleTO.getFirPreparedBy());
        map.put("accidentRemarks", vehicleTO.getAccidentRemarks());

        try {
            count = (Integer) getSqlMapClientTemplate().queryForObject("mfr.vehicleAccidentCount", map);
            System.out.println("count in insurance " + count);
            if (count == 0) {
                status = (Integer) getSqlMapClientTemplate().update("mfr.insertVehicleAccident", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "saveVehicleAccident", sqlException);
        }
        return status;

    }

    public ArrayList getAccidentVehicleList(String regno) {
        Map map = new HashMap();
        map.put("regno", regno);
        ArrayList AccidentVehicleList = new ArrayList();
        try {
            AccidentVehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.AccidentVehicleList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "AccidentVehicleList", sqlException);
        }
        return AccidentVehicleList;

    }

    public ArrayList getAccidentVehicleDetails() {
        Map map = new HashMap();
        ArrayList AccidentVehicleList = new ArrayList();
        try {
            AccidentVehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getAccidentVehicleList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "AccidentVehicleList", sqlException);
        }
        return AccidentVehicleList;

    }

    public ArrayList alterAccidentVehicleList(String regNo) {
        Map map = new HashMap();
        map.put("regNo", regNo);
        ArrayList AccidentVehicleList = new ArrayList();
        try {
            AccidentVehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.alterAccVehicleList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "AccidentVehicleList", sqlException);
        }
        return AccidentVehicleList;

    }

    /**
     * This method used to Vehicle Accident
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int updateVehicleAccident(VehicleTO vehicleTO, int companyId, int userId) {
        int count = 0;
        int status = 0;
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", userId);
        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("accidentSpot", vehicleTO.getAccidentSpot());
        map.put("accidentDate", vehicleTO.getAccidentDate());
        map.put("driverName", vehicleTO.getDriverName());
        map.put("policeStation", vehicleTO.getPoliceStation());
        map.put("firNo", vehicleTO.getFirNo());
        map.put("firDate", vehicleTO.getFirDate());
        map.put("firPreparedBy", vehicleTO.getFirPreparedBy());
        map.put("accidentRemarks", vehicleTO.getAccidentRemarks());

        try {
            status = (Integer) getSqlMapClientTemplate().update("mfr.updateVehicleAccident", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "updateVehicleAccident", sqlException);
        }
        return status;

    }

    /**
     * This method used to Save Vehicle Road Txx
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int saveVehicleRoadTax(VehicleTO vehicleTO, int companyId, int userId) {
        int count = 0;
        int status = 0;
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", userId);
        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("roadTaxReceiptNo", vehicleTO.getRoadTaxReceiptNo());
        map.put("roadTaxReceiptDate", vehicleTO.getRoadTaxReceiptDate());
        map.put("roadTaxPaidLocation", vehicleTO.getRoadTaxPaidLocation());
        map.put("roadTaxPaidOffice", vehicleTO.getRoadTaxPaidOffice());
        map.put("roadTaxPeriod", vehicleTO.getRoadTaxPeriod());
        map.put("roadTaxPaidAmount", vehicleTO.getRoadTaxPaidAmount());
        map.put("roadTaxFromDate", vehicleTO.getRoadTaxFromDate());
        map.put("nextRoadTaxDate", vehicleTO.getNextRoadTaxDate());
        map.put("roadTaxRemarks", vehicleTO.getRoadTaxRemarks());

        try {
            count = (Integer) getSqlMapClientTemplate().queryForObject("mfr.vehicleRoadTaxCount", map);
            System.out.println("count in road tax " + count);
            if (count == 0) {
                status = (Integer) getSqlMapClientTemplate().update("mfr.insertRoadTax", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "saveVehicleRoadTax", sqlException);
        }
        return status;

    }

    /**
     * This method used to Save Fc
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int saveVehicleFc(VehicleTO vehicleTO, int companyId, int userId) {
        int count = 0;
        int status = 0;
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", userId);
        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("rtoDetail", vehicleTO.getRtoDetail());
        map.put("fcDate", vehicleTO.getFcDate());
        map.put("fcReceiptNo", vehicleTO.getFcReceiptNo());
        map.put("fcAmount", vehicleTO.getFcAmount());
        map.put("fcExpiryDate", vehicleTO.getFcExpiryDate());
        map.put("fcRemarks", vehicleTO.getFcRemarks());
        System.out.println("map in FC " + map);
        try {
            count = (Integer) getSqlMapClientTemplate().queryForObject("mfr.vehicleFcCount", map);
            System.out.println("count in FC " + count);
            if (count == 0) {
                status = (Integer) getSqlMapClientTemplate().update("mfr.insertFc", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "saveVehicleFc", sqlException);
        }
        return status;

    }

    /**
     * This method used to getVehicleProfileDetails
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleProfileDetails(VehicleTO vehicleTO) throws NullPointerException {

        ArrayList profileList = new ArrayList();
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vehicleId", vehicleTO.getVehicleId());
        System.out.println("map vehicle profile-->" + map);

        try {
            if (vehicleTO.getVehicleId() != null) {
                profileList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehicleProfile", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "getVehicleProfileDetails", sqlException);
        }
        return profileList;

    }

    /**
     * This method used to getVehicleProfileTyre
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleProfileTyre(VehicleTO vehicleTO) {

        ArrayList profileList = new ArrayList();
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM        */

        map.put("vehicleId", vehicleTO.getVehicleId());
        try {
            profileList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehicleProfileTyre", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "getVehicleProfileTyre", sqlException);
        }
        return profileList;
    }

    /**
     * This method used to get FCAmount
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public double getFcAmount(VehicleTO vehicleTO) throws NullPointerException {
        double fcAmount = 0;
        String fcAmountStr = "";
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vehicleId", vehicleTO.getVehicleId());
        try {
            fcAmountStr = (String) getSqlMapClientTemplate().queryForObject("mfr.getFcAmount", map);
            if (fcAmountStr != null) {
                fcAmount = Double.parseDouble(fcAmountStr);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getFcAmount", sqlException);
        }
        return fcAmount;

    }

    /**
     * This method used to get getInsuranceAmount
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public double getInsuranceAmount(VehicleTO vehicleTO) throws NullPointerException {
        double insuranceAmount = 0;
        String insuranceAmountStr = "";
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vehicleId", vehicleTO.getVehicleId());
        try {
            insuranceAmountStr = (String) getSqlMapClientTemplate().queryForObject("mfr.getInsuranceAmount", map);
            if (insuranceAmountStr != null) {
                insuranceAmount = Double.parseDouble(insuranceAmountStr);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getInsuranceAmount", sqlException);
        }
        return insuranceAmount;

    }

    /**
     * This method used to get getInsuranceAmount
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public double getRoadTaxAmount(VehicleTO vehicleTO) throws NullPointerException {
        double roadTaxAmount = 0;
        String roadTaxAmountStr = "";
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vehicleId", vehicleTO.getVehicleId());
        try {
            roadTaxAmountStr = (String) getSqlMapClientTemplate().queryForObject("mfr.getRoadTaxAmount", map);
            if (roadTaxAmountStr != null) {
                roadTaxAmount = Double.parseDouble(roadTaxAmountStr);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getInsuranceAmount", sqlException);
        }
        return roadTaxAmount;
    }

    /**
     * This method used to get service Cost
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public double getServiceCost(VehicleTO vehicleTO) throws NullPointerException {
        String serviceCostStr = "";
        double serviceCost = 0;
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vehicleId", vehicleTO.getVehicleId());
        try {
            serviceCostStr = (String) getSqlMapClientTemplate().queryForObject("mfr.getServiceCost", map);
            if (serviceCostStr != null) {
                serviceCost = Double.parseDouble(serviceCostStr);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getServiceCost", sqlException);
        }
        return serviceCost;
    }

    /**
     * This method used to getTripExpenses
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public double getTripExpenses(VehicleTO vehicleTO) throws NullPointerException {
        String tripExpStr = "";
        double tripExp = 0;
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vehicleId", vehicleTO.getVehicleId());
        try {
            tripExpStr = (String) getSqlMapClientTemplate().queryForObject("mfr.getTripExpenses", map);
            if (tripExpStr != null) {
                tripExp = Double.parseDouble(tripExpStr);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getServiceCost", sqlException);
        }
        return tripExp;
    }

    /**
     * This method used to getTripFuelDetail
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getTripFuelDetail(VehicleTO vehicleTO) throws NullPointerException {
        String tripFuelDetails = "";

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vehicleId", vehicleTO.getVehicleId());
        try {
            tripFuelDetails = (String) getSqlMapClientTemplate().queryForObject("mfr.getTripFuelDetail", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getServiceCost", sqlException);
        }
        return tripFuelDetails;
    }

    /**
     * This method used to getVehicleRegNos
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleRegNos() {

        ArrayList vehicleRegNos = new ArrayList();
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM        */
        try {
            vehicleRegNos = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehicleRegNos", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "getVehicleProfileTyre", sqlException);
        }
        return vehicleRegNos;
    }

    public ArrayList getRouteListNew() {

        ArrayList routeList = new ArrayList();
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM        */
        try {
            routeList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getRouteListNew", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRouteList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "getRouteList", sqlException);
        }
        return routeList;
    }

    public ArrayList getCompanyList() {

        ArrayList companyList = new ArrayList();
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM        */
        try {
            companyList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getCompanyList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCompanyList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "getCompanyList", sqlException);
        }
        return companyList;
    }

    /**
     * This method used to Get Route Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getRouteList() {
        Map map = new HashMap();
        ArrayList RouteList = new ArrayList();
        try {

            RouteList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.RouteList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "MfrList", sqlException);
        }
        return RouteList;
    }

    /**
     * This method used to Get getCusList Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getCusList() {
        Map map = new HashMap();
        ArrayList custList = new ArrayList();
        try {
            custList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getCusList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCusList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getCusList", sqlException);
        }
        return custList;
    }

    public ArrayList getLPSProductList() {
        Map map = new HashMap();
        ArrayList productList = new ArrayList();
        try {
            productList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getLPSProductList", map);
            System.out.println("productList = " + productList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getLPSProductList", sqlException);
        }
        return productList;
    }

    /**
     * This method used to Get Route Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList alterRouteDetails(String routeId) {
        Map map = new HashMap();
        map.put("routeId", routeId);
        ArrayList routeDetails = new ArrayList();
        try {
            System.out.println("DAO----------------------->" + map);
            routeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.RouteListDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "MfrList", sqlException);
        }
        return routeDetails;
    }

    /**
     * This method used to Get Route Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int insertRouteDetails(VehicleTO vehicleTO, int UserId) {
        Map map = new HashMap();
        String[] tempVal;
        int routeCheck = 0;
        int status = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        try {
            map.put("userId", UserId);
            map.put("routeCode", vehicleTO.getRouteCode());
            map.put("custId", vehicleTO.getCustId());
            map.put("fromLocation", vehicleTO.getFromLocation());
            map.put("fromLocationId", vehicleTO.getFromLocationId());
            map.put("toLocation", vehicleTO.getToLocation());
            map.put("toLocationId", vehicleTO.getToLocationId());
            map.put("productId", vehicleTO.getProductId());
            map.put("Depot", vehicleTO.getDepot());

            map.put("viaRoute", vehicleTO.getViaRoute());
            map.put("km", vehicleTO.getKm());
            map.put("tollAmount", vehicleTO.getTollAmount());
            map.put("tonnageRate", vehicleTO.getTonnageRate());
            map.put("tonnageRateMarket", vehicleTO.getTonnageRateMarket());
            map.put("eligibleTrip", vehicleTO.getEligibleTrip());
            map.put("drivrBata", vehicleTO.getDriverBata());
            map.put("Description", vehicleTO.getDescription());
            map.put("toCity", vehicleTO.getToCity());
            map.put("state", vehicleTO.getState());
            map.put("sla", vehicleTO.getSla());
            map.put("marketVehiclePercentage", vehicleTO.getMarketVehiclePercentage());
            System.out.println("vehicleTO.getFromLocation(): " + vehicleTO.getFromLocation());
            System.out.println("vehicleTO.getToLocation(): " + vehicleTO.getToLocation());

            int locationId = 0;
            if ((Integer) getSqlMapClientTemplate().queryForObject("mfr.getTheLocationId", map) != null) {
                locationId = (Integer) getSqlMapClientTemplate().queryForObject("mfr.getTheLocationId", map);
            }
            System.out.println("location Id 1 : " + locationId);
            if (locationId == 0) {
                String locationCode = vehicleTO.getFromLocation().substring(0, 1) + "01";
                System.out.println("location Code: " + locationCode);
                map.put("locationCode", locationCode);
                status = (Integer) getSqlMapClientTemplate().update("mfr.insertLocation", map);
                System.out.println("status: " + status);
                if (status == 1) {
                    locationId = (Integer) getSqlMapClientTemplate().queryForObject("mfr.getTheLocationId", map);
                    System.out.println("location Id 2 : " + locationId);
                }
            }
            System.out.println("location Id 3 : " + locationId);
            map.put("locationId", locationId);

            if ((Integer) getSqlMapClientTemplate().queryForObject("mfr.getRouteCheck", map) != null) {
                routeCheck = (Integer) getSqlMapClientTemplate().queryForObject("mfr.getRouteCheck", map);
                System.out.println("routeCheck = " + routeCheck);
            }
            if (routeCheck == 0) {
                System.out.println("routeCheck inside if= " + routeCheck);
                System.out.println("map route--------------=> " + map);
                status = (Integer) getSqlMapClientTemplate().update("mfr.insertRoute", map);
            } else {
                status = 2;
            }
        } catch (NullPointerException sqlNullException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlNullException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlNullException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getUserAuthorisedFunctions", sqlNullException);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getRouteListDetails() {
        Map map = new HashMap();
        ArrayList routeList = new ArrayList();
        try {

            routeList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.RouteListDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "MfrList", sqlException);
        }
        return routeList;

    }

    /**
     * This method used to Modify MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doRouteDetailsModify(ArrayList List, int UserId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        try {
            Iterator itr = List.iterator();
            VehicleTO vehicleTO = null;
            while (itr.hasNext()) {
                vehicleTO = new VehicleTO();
                vehicleTO = (VehicleTO) itr.next();
                map.put("userId", UserId);
                map.put("routeId", vehicleTO.getRouteId());
                map.put("routeCode", vehicleTO.getRouteCode());
                map.put("fromLocation", vehicleTO.getFromLocation());
                map.put("toLocation", vehicleTO.getToLocation());
                map.put("viaRoute", vehicleTO.getViaRoute());
                map.put("km", vehicleTO.getKm());
                map.put("tollAmount", vehicleTO.getTollAmount());
                map.put("driverBata", vehicleTO.getDriverBata());
                map.put("Active_Ind", vehicleTO.getActiveInd());

                status = (Integer) getSqlMapClientTemplate().update("mfr.updateRoute", map);
                System.out.println("ststus=" + status);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    /**
     * This method used to Get Route Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int ModifyRouteDetails(VehicleTO vehicleTO, int UserId) {
        Map map = new HashMap();
        int status = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        try {

            map.put("userId", UserId);
            map.put("routeCode", vehicleTO.getRouteCode());
            map.put("custId", vehicleTO.getCustId());
            map.put("fromLocation", vehicleTO.getFromLocation());
            map.put("fromLocationId", vehicleTO.getFromLocationId());
            map.put("toLocation", vehicleTO.getToLocation());
            map.put("toLocationId", vehicleTO.getToLocationId());
            map.put("DepotVal", vehicleTO.getDepotVal());

            map.put("userId", UserId);
            map.put("routeId", vehicleTO.getRouteId());
            map.put("routeCode", vehicleTO.getRouteCode());
            map.put("fromLocation", vehicleTO.getFromLocation());
            map.put("toLocation", vehicleTO.getToLocation());
            map.put("productId", vehicleTO.getProductId());
            map.put("viaRoute", vehicleTO.getViaRoute());
            map.put("km", vehicleTO.getKm());
            map.put("tollAmount", vehicleTO.getTollAmount());
            map.put("tonnageRate", vehicleTO.getTonnageRate());
            map.put("tonnageRateMarket", vehicleTO.getTonnageRateMarket());
            map.put("eligibleTrip", vehicleTO.getEligibleTrip());
            map.put("driverBata", vehicleTO.getDriverBata());
            map.put("Description", vehicleTO.getDescription());
            map.put("toCity", vehicleTO.getToCity());
            map.put("state", vehicleTO.getState());
            map.put("sla", vehicleTO.getSla());
            map.put("tonnageRate", vehicleTO.getTonnageRate());
            map.put("marketVehiclePercentage", vehicleTO.getMarketVehiclePercentage());
            System.out.println("map ```````````````=> " + map);
            status = (Integer) getSqlMapClientTemplate().update("mfr.updateRoute", map);
        } catch (NullPointerException sqlNullException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlNullException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlNullException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getUserAuthorisedFunctions", sqlNullException);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList viewVehicleLubrication(String vehicleNo) {
        Map map = new HashMap();
        map.put("vehicleNo", vehicleNo);
        ArrayList vehicleLubri = new ArrayList();
        try {

            vehicleLubri = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehicleLubrication", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "MfrList", sqlException);
        }
        return vehicleLubri;

    }

    /**
     * This method used to Get insurance Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleFinanceList(VehicleTO vehicleTO, int UserId) {
        Map map = new HashMap();
        ArrayList VehicleInsList = new ArrayList();

        map.put("userId", UserId);
        map.put("regNo", vehicleTO.getRegNo());
        try {

            VehicleInsList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehicleFinanceList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "MfrList", sqlException);
        }
        return VehicleInsList;

    }

    /**
     * This method used to Get insurance Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleInsuranceList(VehicleTO vehicleTO) {
        Map map = new HashMap();
        ArrayList VehicleInsList = new ArrayList();

//        map.put("userId", UserId);
        map.put("regNo", vehicleTO.getRegNo());
        try {

            VehicleInsList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehicleInsuranceList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "MfrList", sqlException);
        }
        return VehicleInsList;

    }

    /**
     * This method used to Get insurance Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleAMCList(VehicleTO vehicleTO) {
        Map map = new HashMap();
        ArrayList vehicleAmcList = new ArrayList();

//        map.put("userId", UserId);
        map.put("regNo", vehicleTO.getRegNo());
        try {

            vehicleAmcList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehicleAMCList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "MfrList", sqlException);
        }
        return vehicleAmcList;

    }

    public ArrayList getvehicleInsurance(VehicleTO vehicleTO) {
        System.out.println("in dao====");
        Map map = new HashMap();
        ArrayList vehicleInsurance = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */

        map.put("insuranceid", vehicleTO.getInsuranceid());
        System.out.println("map = " + map);
        try {

            vehicleInsurance = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehicleDetails", map);
            System.out.println("vehicleInsuranceList = " + vehicleInsurance.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleInsuranceDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getVehicleInsuranceDetail", sqlException);
        }
        return vehicleInsurance;
    }

    public ArrayList getvehicleAMC(VehicleTO vehicleTO) {
        Map map = new HashMap();
        ArrayList vehicleAMC = new ArrayList();
        map.put("amcId", vehicleTO.getAmcId());
        map.put("vehicleId", vehicleTO.getVehicleId());
        System.out.println("map = " + map);
        try {
            vehicleAMC = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehicleAMCDetails", map);
            System.out.println("vehicleInsuranceList = " + vehicleAMC.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleAMCDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getVehicleAMCDetail", sqlException);
        }
        return vehicleAMC;
    }

    public ArrayList vehicleFinanceDetail(VehicleTO vehicleTO) {
        Map map = new HashMap();
        ArrayList vehicleFinance = new ArrayList();
        map.put("financeId", vehicleTO.getFinanceId());
        System.out.println("map = " + map);
        try {
            vehicleFinance = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehicleFinance", map);
            System.out.println("vehicleFinanceList = " + vehicleFinance.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleInsuranceDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getVehicleInsuranceDetail", sqlException);
        }
        return vehicleFinance;
    }

    public ArrayList getUpdateVehicleDetail(VehicleTO vehicleTO) {
        Map map = new HashMap();
        ArrayList vehicleDetail = new ArrayList();

        // map.put("insuranceid", vehicleTO.getInsuranceid());
        map.put("vehicleId", vehicleTO.getVehicleId());
        System.out.println("map = " + map);
        try {

            vehicleDetail = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.UpdateVehicleDetail", map);
            System.out.println("vehicleDetail = " + vehicleDetail.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getVehicleInsuranceDetail", sqlException);
        }
        return vehicleDetail;
    }

    public int processUpdateVehicleInsurance(VehicleTO vehicleTO, int userId) {
        Map map = new HashMap();
        map.put("userId", userId);
        map.put("insuranceCompanyName", vehicleTO.getInsuranceCompanyName());
        map.put("premiumNo", vehicleTO.getPremiumNo());
        map.put("premiumPaidAmount", vehicleTO.getPremiumPaidAmount());
        map.put("premiumPaidDate", vehicleTO.getPremiumPaidDate());
        map.put("vehicleValue", vehicleTO.getVehicleValue());
        map.put("premiumExpiryDate", vehicleTO.getPremiumExpiryDate());
        map.put("insuarnceNCB", vehicleTO.getInsuarnceNCB());
        map.put("insuarnceRemarks", vehicleTO.getInsuarnceRemarks());
        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("insuranceid", vehicleTO.getInsuranceid());
        System.out.println("map = " + map);

        int status = 0;
        try {

            status = (Integer) getSqlMapClientTemplate().update("mfr.updateVehicleInsurance", map);
            System.out.println("status = " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public int processUpdateVehicleAMC(VehicleTO vehicleTO, int userId) {
        Map map = new HashMap();
        map.put("userId", userId);
        map.put("amcCompanyName", vehicleTO.getAmcCompanyName());
        map.put("amcAmount", vehicleTO.getAmcAmount());
        map.put("amcDuration", vehicleTO.getAmcDuration());
        map.put("amcChequeDate", vehicleTO.getAmcChequeDate());
        map.put("amcChequeNo", vehicleTO.getAmcChequeNo());
        map.put("amcFromDate", vehicleTO.getAmcFromDate());
        map.put("amcToDate", vehicleTO.getAmcToDate());
        map.put("amcRemarks", vehicleTO.getRemarks());
        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("amcId", vehicleTO.getAmcId());
        System.out.println("map = " + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("mfr.updateVehicleAMC", map);
            System.out.println("status = " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public ArrayList getRoadTaxList(VehicleTO vehicleTO) {
        Map map = new HashMap();
        ArrayList RoadTaxList = new ArrayList();
//        map.put("userId", UserId);
        map.put("regNo", vehicleTO.getRegNo());
        map.put("vehicleId", vehicleTO.getVehicleId());
        if (vehicleTO.getFromDate() != null) {
            map.put("fromDate", vehicleTO.getFromDate());
        } else {
            map.put("fromDate", "");
        }
        if (vehicleTO.getToDate() != null) {
            map.put("toDate", vehicleTO.getToDate());
        } else {
            map.put("toDate", "");
        }

        System.out.println("map = " + map);
        try {
            RoadTaxList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getRoadTaxList", map);
            System.out.println("RoadTaxList = " + RoadTaxList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "MfrList", sqlException);
        }
        return RoadTaxList;

    }

    public ArrayList getvehicleRoadTax(VehicleTO vehicleTO) {
        System.out.println("in dao====");
        Map map = new HashMap();
        ArrayList vehicleTax = new ArrayList();
        map.put("id", vehicleTO.getId());
        System.out.println("map 1= " + map);
        try {

            vehicleTax = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehicleTax", map);
            System.out.println("vehicleInsuranceList = " + vehicleTax.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleInsuranceDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getVehicleInsuranceDetail", sqlException);
        }
        return vehicleTax;
    }

    public ArrayList getUpdateTax(VehicleTO vehicleTO) {
        Map map = new HashMap();
        ArrayList vehicleDetail = new ArrayList();

        // map.put("insuranceid", vehicleTO.getInsuranceid());
        map.put("vehicleId", vehicleTO.getVehicleId());
        System.out.println("map 2= " + map);
        try {

            vehicleDetail = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.UpdateVehicleDetail", map);
            System.out.println("vehicleDetail = " + vehicleDetail.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getVehicleInsuranceDetail", sqlException);
        }
        return vehicleDetail;
    }

    public int processUpdateVehicleTax(VehicleTO vehicleTO, int userId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        map.put("userId", userId);
        map.put("roadTaxReceiptNo", vehicleTO.getRoadTaxReceiptNo());
        map.put("roadTaxReceiptDate", vehicleTO.getRoadTaxReceiptDate());
        map.put("roadTaxPaidAmount", vehicleTO.getRoadTaxPaidAmount());
        map.put("roadTaxPaidLocation", vehicleTO.getRoadTaxPaidLocation());
        map.put("roadTaxPeriod", vehicleTO.getRoadTaxPeriod());
        map.put("roadTaxFromDate", vehicleTO.getRoadTaxFromDate());
        map.put("nextRoadTaxDate", vehicleTO.getNextRoadTaxDate());
        map.put("roadTaxRemarks", vehicleTO.getRoadTaxRemarks());

        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("id", vehicleTO.getId());
        System.out.println("map = " + map);

        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("mfr.updateVehicleTax", map);
            System.out.println("status = " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public ArrayList getFCList(VehicleTO vehicleTO) {
        Map map = new HashMap();
        ArrayList FCList = new ArrayList();

//        map.put("userId", UserId);
        map.put("regNo", vehicleTO.getRegNo());
        System.out.println("map = " + map);
        try {

            FCList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getFCList", map);
            System.out.println("FCList = " + FCList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "MfrList", sqlException);
        }
        return FCList;

    }

    public ArrayList getvehicleFC(VehicleTO vehicleTO) {
        System.out.println("in dao====");
        Map map = new HashMap();
        ArrayList vehicleFC = new ArrayList();
        map.put("vehicleId", vehicleTO.getVehicleId());

        System.out.println("map 1= " + map);
        try {

            vehicleFC = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.UpdateVehicleDetail", map);
            System.out.println("vehicleInsuranceList = " + vehicleFC.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleInsuranceDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getVehicleInsuranceDetail", sqlException);
        }
        return vehicleFC;
    }

    public ArrayList getUpdateFCDetail(VehicleTO vehicleTO) {
        System.out.println("in dao====");
        Map map = new HashMap();
        ArrayList vehicleFCDetail = new ArrayList();
        map.put("fcid", vehicleTO.getFcid());

        System.out.println("map 2= " + map);
        try {

            vehicleFCDetail = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getFCDetail", map);
            System.out.println("vehicleFCDetail = " + vehicleFCDetail.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleInsuranceDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "vehicleFCDetail", sqlException);
        }
        return vehicleFCDetail;
    }

    public int processUpdateVehicleFC(VehicleTO vehicleTO, int userId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        map.put("userId", userId);
        map.put("rtoDetail", vehicleTO.getRtoDetail());
        map.put("fcDate", vehicleTO.getFcDate());
        map.put("fcReceiptNo", vehicleTO.getFcReceiptNo());
        map.put("fcAmount", vehicleTO.getFcAmount());
        map.put("fcExpiryDate", vehicleTO.getFcExpiryDate());
        map.put("fcRemarks", vehicleTO.getFcRemarks());

        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("fcid", vehicleTO.getFcid());
        System.out.println("map = " + map);

        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("mfr.updateVehicleFC", map);
            System.out.println("status = " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public int saveVehiclePermit(VehicleTO vehicleTO, int companyId, int userId) {
        int count = 0;
        int status = 0;
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        map.put("userId", userId);
        map.put("vehicleId", vehicleTO.getVehicleId());

        map.put("permitType", vehicleTO.getPermitType());
        map.put("permitNo", vehicleTO.getPermitNo());
        map.put("permitAmount", vehicleTO.getPermitAmount());
        map.put("permitPaidDate", vehicleTO.getPermitPaidDate());
        map.put("permitExpiryDate", vehicleTO.getPermitExpiryDate());
        map.put("permitRemarks", vehicleTO.getPermitRemarks());
        System.out.println("map = " + map);
        try {
            count = (Integer) getSqlMapClientTemplate().queryForObject("mfr.vehiclePermitCount", map);
            System.out.println("count in insurance " + count);
            if (count == 0) {
                status = (Integer) getSqlMapClientTemplate().update("mfr.insertVehiclePermit", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "saveVehicleInsurance", sqlException);
        }
        System.out.println("status = " + status);
        return status;

    }

    public ArrayList getVehiclePermitList(VehicleTO vehicleTO) {
        Map map = new HashMap();
        ArrayList VehiclePermitList = new ArrayList();

//        map.put("userId", UserId);
        map.put("regNo", vehicleTO.getRegNo());
        System.out.println("map=================== = " + map);
        try {

            VehiclePermitList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehiclePermitList", map);
            System.out.println("VehiclePermitList = " + VehiclePermitList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "MfrList", sqlException);
        }
        return VehiclePermitList;

    }

    public ArrayList getvehiclePermit(VehicleTO vehicleTO) {
        System.out.println("in dao====");
        Map map = new HashMap();
        ArrayList vehiclePermit = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */

        map.put("permitid", vehicleTO.getPermitid());
        System.out.println("map = " + map);
        try {

            vehiclePermit = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehiclePermit", map);
            System.out.println("vehiclePermitList = " + vehiclePermit.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehiclePermitList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "vehiclePermitList", sqlException);
        }
        return vehiclePermit;
    }

    public int processUpdateVehiclePermit(VehicleTO vehicleTO, int userId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        map.put("userId", userId);
        map.put("permitType", vehicleTO.getPermitType());
        map.put("permitNo", vehicleTO.getPermitNo());
        map.put("permitAmount", vehicleTO.getPermitAmount());
        map.put("permitPaidDate", vehicleTO.getPermitPaidDate());
        map.put("permitExpiryDate", vehicleTO.getPermitExpiryDate());
        map.put("remarks", vehicleTO.getRemarks());
        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("permitid", vehicleTO.getPermitid());
        System.out.println("map = " + map);

        int status = 0;
        try {

            status = (Integer) getSqlMapClientTemplate().update("mfr.updateVehiclePermit", map);
            System.out.println("status = " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public ArrayList getLeasingCustomerList() {

        Map map = new HashMap();
        ArrayList leasingCustList = new ArrayList();
        try {
            leasingCustList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getLeasingCustomerList", map);
            System.out.println("leasingCustList    " + leasingCustList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTyreItems Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getLeasingCustomerList", sqlException);
        }
        return leasingCustList;
    }

    public String getToLocSugg(String toLocation) {
        Map map = new HashMap();
        map.put("toLocation", toLocation);
        String suggestions = "";
        VehicleTO repTO = new VehicleTO();
        try {
            ArrayList toLoc = new ArrayList();
            System.out.println("map = " + map);
            toLoc = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getToLocSugg", map);
            Iterator itr = toLoc.iterator();
            while (itr.hasNext()) {
                repTO = new VehicleTO();
                repTO = (VehicleTO) itr.next();
                suggestions = repTO.getToLocation() + "^" + repTO.getToLocationId() + "~" + suggestions;
                System.out.println("suggestions = " + suggestions);
            }
            if ("".equals(suggestions)) {
                suggestions = "no match found";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerSuggestion Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCustomerSuggestion", sqlException);
        }
        return suggestions;
    }

    /**
     * This method used to Get Vehicle Type List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleTypeList() {
        Map map = new HashMap();
        ArrayList vehicleTypeList = new ArrayList();
        try {

            vehicleTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehicleTypeList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getVehicleTypeList", sqlException);
        }
        return vehicleTypeList;

    }

    public ArrayList getTrailerDetail(VehicleTO vehicleTO) {

        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */

        //    map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("regNo", vehicleTO.getRegNo());
        map.put("mfrId", vehicleTO.getMfrId());
        map.put("typeId", vehicleTO.getTypeId());
        map.put("ownership", vehicleTO.getOwnership());
        try {
            System.out.println("i m here 4");
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getTrailerDetails", map);
            System.out.println("trailer size:" + vehicleList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getVehicleDetail", sqlException);
        }
        return vehicleList;
    }

    public int doAddTrailerDetails(VehicleTO vehicleTO, int UserId) {
        System.out.println("vehicle DAO");
        Map map = new HashMap();

        /*
         * set the parameters in the map for sending to ORM
         */
        String ownerS = "";
        map.put("userId", UserId);
        map.put("trailerNo", vehicleTO.getTrailerNo());
        map.put("mfrId", vehicleTO.getMfrId());
        map.put("groupId", vehicleTO.getGroupId());
        map.put("modelId", vehicleTO.getModelId());
        map.put("dateOfSale", vehicleTO.getDateOfSale());
        map.put("chassisNo", vehicleTO.getChassisNo());
        map.put("war_period", vehicleTO.getWar_period());
        map.put("description", vehicleTO.getDescription());
        map.put("seats", vehicleTO.getSeatCapacity());
        map.put("hourMeter", vehicleTO.getHmReading());
        map.put("kiloMeter", vehicleTO.getKmReading());
        map.put("fservice", vehicleTO.getFservice());
        map.put("vehicleCost", vehicleTO.getVehicleCost());
        map.put("vehicleDepreciation", vehicleTO.getVehicleDepreciation());
        map.put("vehicleColor", vehicleTO.getVehicleColor());
//      map.put("ownership", vehicleTO.getLeasingCustId());
        map.put("leasingCustId", vehicleTO.getLeasingCustId());
        map.put("ownership", vehicleTO.getAsset());
        ownerS = vehicleTO.getAsset();
        if (ownerS.equalsIgnoreCase("Own")) {
            map.put("groupCode", "ACC-0003");
        } else {
            map.put("groupCode", "");
        }
        map.put("gpsSystem", vehicleTO.getGpsSystem());
        map.put("warrantyDate", vehicleTO.getWarrantyDate());
        map.put("axles", vehicleTO.getAxles());
        map.put("dailyRunKm", vehicleTO.getDailyKm());
        map.put("dailyRunHm", vehicleTO.getDailyHm());
        map.put("gpsSystemId", vehicleTO.getGpsSystemId());
        System.out.println("map first:::::: = " + map);
        int status = 0;
        String code = "";
        String[] temp;
        int vehicle_id = 0;
        int currentYear = 0;
        String newYear = "";
        int VehicleLedgerId = 0;
        try {

            vehicle_id = (Integer) getSqlMapClientTemplate().insert("mfr.insertAddTrailer", map);
            System.out.println("vehicleId = " + vehicle_id);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */

            FPLogUtils.fpDebugLog("insertAddTrailer Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "insertAddTrailer", sqlException);
        }
        return vehicle_id;
    }

    public int doInsertTrailerTyre(VehicleTO vehicleTO, int UserId, int companyId, int vehicle_id) {

//        String MfrIds[] = vehicleTO.getMfrIds();
//        String Modelds[] = vehicleTO.getModelIds();
        String[] positionIds = vehicleTO.getPositionId();
//        String tyreType[] = vehicleTO.getTyreType();
        String[] itemIds = vehicleTO.getItemIds();
        String[] tyreIds = vehicleTO.getTyreIds();
        String[] tyreDate = vehicleTO.getTyreDate();

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        int insertStatus = 0;
        String vehicleId = "";
        int tyreId = 0;
        Integer tyreNo;
        try {
            if (tyreIds != null) {
                for (int i = 0; i < tyreIds.length; i++) {
                    if (!itemIds[i].equals("0")) {
                        map.put("tyreNo", tyreIds[i]);
                        map.put("positionId", positionIds[i]);
                        map.put("itemId", itemIds[i]);
                        map.put("companyId", companyId);
                        map.put("tyreDate", tyreDate[i]);
                        map.put("userId", UserId);
//                        if (!vehicleTO.getVehicleId().equalsIgnoreCase("0")) {
//                            map.put("vehicleId", vehicleTO.getVehicleId());
//                        } else {
//                            map.put("trailerNo", vehicleTO.getRegNos()[i]);
//                            vehicleId = (String) getSqlMapClientTemplate().queryForObject("mfr.trailerId", map);
//                            map.put("vehicleId", vehicleId);
//                        }
                        map.put("vehicleId", vehicle_id);
                        System.out.println("map  " + map);
                        tyreNo = (Integer) getSqlMapClientTemplate().queryForObject("mfr.checkTyreNo", map);

                        if (tyreNo == null) {
                            tyreId = (Integer) getSqlMapClientTemplate().insert("mfr.insertTrailerTyre", map);
                            map.put("tyreId", tyreId);
                            if (tyreId != 0) {
                                System.out.println("map tessssssssssssssssssssssss => " + map);
                                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertTrailTyre", map);
                            }
                        } else {
                            map.put("tyreId", tyreNo);
                            System.out.println("map tessssssssssssssssssssssss22222222222222222 => " + map);
                            insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.updateTrailTyre", map);
                            insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertTrailTyre", map);
                        }

                    }
                }
//                status = (Integer) getSqlMapClientTemplate().queryForObject("mfr.checkVehTyre",map);
//                else{
//                    insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.updateVehTyre", map);
//                    insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehTyre", map);
//                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doInsertVehicleTyre Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "doInsertVehicleTyre", sqlException);
        }
        return insertStatus;
    }

    public ArrayList getTrailerList(VehicleTO vehicleTO) {

        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
//        map.put("vendorId", vendorId);
        map.put("trailerId", vehicleTO.getVehicleId());
        System.out.println("map 1:" + map);
        try {

            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.trailerDetails", map);
            System.out.println("trailer Detail siaze:" + vehicleList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getVehicleDetail", sqlException);
        }
        return vehicleList;
    }

    public int doAlterTrailerDetails(VehicleTO vehicleTO, int UserId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", UserId);
        map.put("mfrId", vehicleTO.getMfrId());
        map.put("groupId", vehicleTO.getGroupId());
        map.put("modelId", vehicleTO.getModelId());
        map.put("dateOfSale", vehicleTO.getDateOfSale());
        map.put("chassisNo", vehicleTO.getChassisNo());
        map.put("war_period", vehicleTO.getWar_period());
        map.put("seatCapacity", vehicleTO.getSeatCapacity());
        map.put("currentKm", vehicleTO.getKmReading());
        map.put("currentHm", vehicleTO.getHmReading());
        map.put("description", vehicleTO.getDescription());
        map.put("activeInd", vehicleTO.getActiveInd());
        map.put("trailerId", vehicleTO.getVehicleId());
        map.put("remarks", vehicleTO.getDescription());
        map.put("fservice", vehicleTO.getFservice());
        map.put("gpsSystem", vehicleTO.getGpsSystem());
        map.put("warrantyDate", vehicleTO.getWarrantyDate());

        System.out.println("vehicleTO.getLeasingCustId() ============== " + vehicleTO.getLeasingCustId());
        if (vehicleTO.getLeasingCustId().equalsIgnoreCase("1")) {
            System.out.println("if");
            map.put("ownership", "1");
            map.put("VendorId", "");
        } else {
            System.out.println("else");
            map.put("ownership", "2");
            map.put("VendorId", vehicleTO.getLeasingCustId());

        }

        System.out.println("map uuuuuuuuuuuuuuu => " + map);
        int status = 0;
        try {

            status = (Integer) getSqlMapClientTemplate().update("mfr.updateTrailer", map);
            System.out.println("111:" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */

            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public ArrayList getVehicleReplacementList() {

        Map map = new HashMap();
        ArrayList vehicleReplacementList = new ArrayList();
        try {
            try {
                vehicleReplacementList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehicleReplacementList", map);
                System.out.println("getVehicleReplacementList= " + vehicleReplacementList.size());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleReplacementList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getVehicleReplacementList", sqlException);
        }
        return vehicleReplacementList;

    }

    public int insertVehicleReplacement(VehicleTO vehicleTO, int userId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", userId);
        map.put("vName", vehicleTO.getvName());
        map.put("cType", vehicleTO.getcType());
        map.put("eRegNo", vehicleTO.geteRegNo());
        map.put("rRegNo", vehicleTO.getrRegNo());
        map.put("rDate", vehicleTO.getrDate());
        System.out.println("map insertVehicleReplacement = " + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("mfr.insertVehicleReplacement", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;

    }

    public int updateVehicleReplacement(VehicleTO vehicleTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        map.put("vName", vehicleTO.getvName());
        map.put("cType", vehicleTO.getcType());
        map.put("eRegNo", vehicleTO.geteRegNo());
        map.put("rRegNo", vehicleTO.getrRegNo());
        map.put("rDate", vehicleTO.getrDate());

        System.out.println("map updateVehicleReplacement = " + map);
        try {
            System.out.println("updateVehicleReplacement status isrtrtrtrrr" + status);
            status = (Integer) getSqlMapClientTemplate().update("mfr.updateVehicleReplacement", map);
            System.out.println("updateVehicleReplacement status is" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateVehicleReplacement Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS,
                    "updateVehicleReplacement", sqlException);
        }
        return status;
    }

    public ArrayList getVendorList() {
        Map map = new HashMap();
        // map.put("vendorType", Integer.valueOf(vendorTypeId));
        ArrayList vendorList = new ArrayList();
        try {
            vendorList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVendorList", map);
            System.out.println((new StringBuilder()).append("vendorList size=").append(vendorList.size()).toString());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "vendorList", sqlException);
        }
        return vendorList;
    }

    public ArrayList getVendorVehicleRegNo(VehicleTO vehicleTO) {
        Map map = new HashMap();
        ArrayList regNoList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vendorId", vehicleTO.getVendorId());
        System.out.println("map =----------- " + map);
        try {
            regNoList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getRegNoAjax", map);
            System.out.println((new StringBuilder()).append("regNoList size=").append(regNoList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getVendorVehcileRegNo Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "regNoList", sqlException);
        }
        return regNoList;
    }

    public ArrayList getVendorVehicleRegNos(VehicleTO vehicleTO) {
        Map map = new HashMap();
        ArrayList regNosList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vendorId", vehicleTO.getVendorId());
        System.out.println("map =----------- " + map);
        try {
            regNosList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getRegNos", map);
            System.out.println((new StringBuilder()).append("regNosList size=").append(regNosList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getVendorVehcileRegNos Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "regNosList", sqlException);
        }
        return regNosList;
    }

    public ArrayList getRegNo(VehicleTO vehicleTO) {
        Map map = new HashMap();
        ArrayList regNoList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vehicleId", vehicleTO.getVehicleId());
        System.out.println("map =----------- " + map);
        try {
            regNoList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getRegNo", map);
            System.out.println((new StringBuilder()).append("regNoList size=").append(regNoList.size()).toString());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "regNoList", sqlException);
        }
        return regNoList;
    }

    public ArrayList getRegNos(VehicleTO vehicleTO) {
        Map map = new HashMap();
        ArrayList regNosList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vehicleId", vehicleTO.getVehicleId());
        System.out.println("map =----------- " + map);
        try {
            regNosList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getRegNos", map);
            System.out.println((new StringBuilder()).append("regNosList size=").append(regNosList.size()).toString());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "regNosList", sqlException);
        }
        return regNosList;
    }

    public ArrayList getTrailerTypeList(VehicleTO vehicleTO) {

        Map map = new HashMap();
        ArrayList trailerTypeList = new ArrayList();
        map.put("mfrId", "");
        /*
         * set the parameters in the map for sending to ORM
         */

        //    map.put("vehicleId", vehicleTO.getVehicleId());
        try {
            System.out.println("i m here 4");
            trailerTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getTrailerTypeList", map);
            System.out.println("trailerTypeList size:" + trailerTypeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("gettrailerTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getTrailerTypeList", sqlException);
        }
        return trailerTypeList;
    }

    public ArrayList vendorNameList(String venId) {
        System.out.println("in DAO  getting vendor name for popup");
        Map map = new HashMap();
        ArrayList vendorNameList = new ArrayList();

        try {

            map.put("venId", venId);
            System.out.println("the venId is===>" + venId);
            System.out.println("map:" + map);
            vendorNameList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.vendorNameList", map);
            System.out.println("vendorNameList size:" + vendorNameList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vendorNameList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "vendorNameList", sqlException);
        }
        return vendorNameList;
    }

    public ArrayList GetAxleList() {
        Map map = new HashMap();
        ArrayList AxleList = new ArrayList();

        try {

            AxleList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.GetAxleList", map);
            System.out.println("AxleList  = " + AxleList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "MfrList", sqlException);
        }
        return AxleList;

    }

    public int insertVehicleTyre(VehicleTO vehicleTO, int UserId, int companyId, String vehicleFront, String leftTyreIds, String leftItemIds, String rightTyreIds, String rightItemIds) {
        Map map = new HashMap();
        int status = 0;
        int insertStatus = 0;
        String vehicleId = "";
        String Left = "";
        String Right = "";
        int tyreId = 0;
        Integer tyreNo;
        try {
            map.put("axleId", vehicleFront);
            map.put("companyId", companyId);
            map.put("userId", UserId);
            System.out.println("map" + map);
            for (int i = 0; i < 2; i++) {
                if (i == 0) {
                    map.put("leftTyreIds", leftTyreIds);
                    map.put("leftItemIds", leftItemIds);
                    map.put("positionName", "Left");
                    map.put("position", "1");
                    insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehicleAxleTyre", map);
                } else {
                    map.put("leftTyreIds", rightTyreIds);
                    map.put("leftItemIds", rightItemIds);
                    map.put("positionName", "Right");
                    map.put("position", "2");
                    insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehicleAxleTyre", map);
                }
                System.out.println("axleId222222" + insertStatus);
            }
//            tyreNo = (Integer) getSqlMapClientTemplate().queryForObject("mfr.checkTyreNo", map);
//            if (tyreNo == null) {
//                tyreId = (Integer) getSqlMapClientTemplate().insert("mfr.insertVehicleTyre", map);
//                map.put("tyreId", tyreId);
//                if (tyreId != 0) {
//                    System.out.println("map tessssssssssssssssssssssss => " + map);
//                    insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehTyre", map);
//                }
//            } else {
//                map.put("tyreId", tyreNo);
//                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.updateVehTyre", map);
//                insertStatus = (Integer) getSqlMapClientTemplate().update("mfr.insertVehTyre", map);
//            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doInsertVehicleTyre Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "doInsertVehicleTyre", sqlException);
        }
        return insertStatus;
    }

    public int insertAxletypeMaster(VehicleTO vehicleTO, int userId, String vehicleFront, String axleRight, String axleLeft, int lastinsertedId) {

        Map map = new HashMap();

        int axleId = 0;
        int axleDetails = 0;
        /*
         * set the parameters in the map for sending to ORM
         */

        try {

            map.put("axleId", lastinsertedId);
            map.put("vehicleFront", vehicleFront);
            map.put("axleLeft", axleLeft);
            map.put("axleRight", axleRight);
            map.put("userId", userId);
            System.out.println("map Axle 2 = " + map);
            axleDetails = (Integer) getSqlMapClientTemplate().update("mfr.insertAxleDetails", map);
            System.out.println("axleId222222" + axleDetails);
//          
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return axleDetails;

    }

    public int insertAxleMaster(VehicleTO vehicleTO, int userId) {

        Map map = new HashMap();

        int axleId = 0;
        int axleDetails = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        try {
            map.put("axleTypeName", vehicleTO.getAxleTypeName());
            map.put("axleCount", vehicleTO.getAxleCount());
            map.put("typeFor", vehicleTO.getTypeFor());
            map.put("userId", userId);
//            map.put("userId", userId);
            System.out.println("map Axle = " + map);
            axleId = (Integer) getSqlMapClientTemplate().insert("mfr.insertAxleMaster", map);

            System.out.println("axleId = " + axleId);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return axleId;

    }

    public ArrayList getVehAxleName(int axleId) {
        Map map = new HashMap();
        ArrayList insuranceDetails = new ArrayList();
        map.put("axleId", axleId);
        System.out.println("map" + map);

        try {

            insuranceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehAxleName", map);
            System.out.println("insuranceDetails size:" + insuranceDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getInsuranceDetails", sqlException);
        }
        return insuranceDetails;

    }

    public ArrayList getVehAxleTyreNo(VehicleTO vehTO) {
        Map map = new HashMap();
        ArrayList insuranceDetails = new ArrayList();
        map.put("axleDetailId", vehTO.getAxleDetailId());
        map.put("positionName", vehTO.getPositionName());
        map.put("positionNo", vehTO.getPositionNo());
        map.put("vehicleId", vehTO.getVehicleId());
        map.put("axleTypeId", vehTO.getAxleTypeId());
        System.out.println("map" + map);
        try {
            insuranceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehAxleTyreNo", map);
            System.out.println("insuranceDetails size:" + insuranceDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getInsuranceDetails", sqlException);
        }
        return insuranceDetails;

    }

    public int vehicleTyreDetails(VehicleTO vehicleTO, int vehicleId, int userId, int madhavaramSp) {
        Map map = new HashMap();
        int axleId = 0;
        int axleDetails = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        try {
            map.put("axleId", vehicleTO.getAxleTypeId());
            map.put("vehicleId", vehicleId);
            map.put("userId", userId);
            map.put("companyId", madhavaramSp);
            System.out.println("map Axle 2 Throttle Here  = " + map);
            ArrayList axleList = new ArrayList();
            axleList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehAxleName", map);
            Iterator itr = axleList.iterator();
            VehicleTO vehTO = new VehicleTO();
            int left = 0;
            int right = 0;
            while (itr.hasNext()) {
                vehTO = (VehicleTO) itr.next();
                left = Integer.parseInt(vehTO.getAxleLeft());
                map.put("axleTypeName", vehTO.getAxleTypeName());
                map.put("axleDetailId", vehTO.getVehicleAxleId());
                for (int i = 0; i < left; i++) {
                    map.put("positionName", "Left");
                    map.put("positionNo", i + 1);
                    axleDetails = (Integer) getSqlMapClientTemplate().update("mfr.insertTyreDetails", map);
                }
                right = Integer.parseInt(vehTO.getAxleRight());
                for (int j = 0; j < right; j++) {
                    map.put("positionName", "Right");
                    map.put("positionNo", j + 1);
                    axleDetails = (Integer) getSqlMapClientTemplate().update("mfr.insertTyreDetails", map);
                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return axleDetails;
    }

    public int updateVehicleTyreDetails(VehicleTO vehicleTO, int userId, int madhavaramSp) {
        Map map = new HashMap();
        int axleId = 0;
        int axleDetails = 0;
        try {
            map.put("axleDetailId", vehicleTO.getAxleDetailId());
            map.put("positionName", vehicleTO.getPositionName());
            map.put("positionNo", vehicleTO.getPositionNo());
            map.put("updateValue", vehicleTO.getUpdateValue());
            map.put("itemId", vehicleTO.getUpdateValue());
            map.put("tyreNo", vehicleTO.getTyreNo());
            map.put("vehicleId", vehicleTO.getVehicleId());
            map.put("depthVal", vehicleTO.getDepthVal());
            map.put("kmReading", vehicleTO.getKmReading());
            map.put("companyId", madhavaramSp);
            System.out.println("map Axle 2 = " + map);
            int tyreId = 0;
            Object tyreIdStr = getSqlMapClientTemplate().queryForObject("mfr.checkTyreNo", map);
            if (tyreIdStr != null) {
                tyreId = (Integer) tyreIdStr;
                System.out.println("tyreId:111111::" + tyreId);
            } else {
                tyreId = (Integer) getSqlMapClientTemplate().insert("mfr.insertVehicleTyre", map);
                System.out.println("tyreId:222222::" + tyreId);
            }

            map.put("tyreId", tyreId);
            // update tyre no
            int checkTyreStatus = 0;
            checkTyreStatus = (Integer) getSqlMapClientTemplate().queryForObject("mfr.checkTyreNoStatus", map);
            System.out.println("checkTyreStatus" + checkTyreStatus);
            if (checkTyreStatus == 0) {
                axleDetails = (Integer) getSqlMapClientTemplate().update("mfr.updateVehicleTyreMfr", map);
            } else if (vehicleTO.getUpdateType().equals("0")) {
                // update tyre mfr
                axleDetails = (Integer) getSqlMapClientTemplate().update("mfr.updateVehicleTyreMfr", map);
                System.out.println("axleDetails" + axleDetails);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("updateVehicleTyreDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "updateVehicleTyreDetails", sqlException);
        }
        return axleDetails;
    }

    public int updateInsuranceDetails(VehicleTO vehicleTO, int userId, int madhavaramSp, SqlMapClient session) {
        System.out.println("updateInsuranceDetails entered BP");
        Map map = new HashMap();
        int axleId = 0;
        int insuranceIds = 0;
        int insuranceId = 0;
        int updateinsuranceId = 0;
        String Insurance = "vehicle Insurance";
        try {
            String[] bankBranch = vehicleTO.getBankBranchId().split("~");
            String[] ledgerIdAndCode = vehicleTO.getLedgerId().split("~");
//            System.out.println("ledgerIdAndCode0="+ledgerIdAndCode[0]);
//            System.out.println("ledgerIdAndCode1="+ledgerIdAndCode[1]);
            map.put("fleetTypeId", vehicleTO.getFleetTypeId());
            System.out.println("fleetTypeId@#@@@@" + vehicleTO.getFleetTypeId());
            map.put("insPolicy", vehicleTO.getInsPolicy());
            map.put("prevPolicy", vehicleTO.getPrevPolicy());
            map.put("packageType", vehicleTO.getPackageType());
            map.put("insName", vehicleTO.getInsName());
            map.put("insAddress", vehicleTO.getInsAddress());
            map.put("insMobileNo", vehicleTO.getInsMobileNo());
            map.put("insPolicyNo", vehicleTO.getInsPolicyNo());
            map.put("insIdvValue", vehicleTO.getInsIdvValue());
            map.put("fromDate1", vehicleTO.getFromDate1());
            map.put("toDate1", vehicleTO.getToDate1());
            map.put("premiunAmount", vehicleTO.getPremiunAmount());
            map.put("paymentDate", vehicleTO.getPaymentDate());
//            map.put("insuranceid", vehicleTO.getInsuranceid());

            map.put("paymentType", vehicleTO.getPaymentType());
            map.put("bankId", vehicleTO.getBankId());
            map.put("chequeDate", vehicleTO.getChequeDate());
            map.put("chequeNo", vehicleTO.getChequeNo());
            map.put("chequeAmount", vehicleTO.getChequeAmount());
            map.put("debitLedgerId", ledgerIdAndCode[1]);
            map.put("reference", Insurance);

            map.put("vendorId", vehicleTO.getVendorId());
            map.put("vehicleId", vehicleTO.getVehicleId());
            map.put("userId", userId);
            map.put("companyId", madhavaramSp);

            map.put("issuingOffice", vehicleTO.getIssuingOffice());
            System.out.println("map Insurance 2 = " + map);

            if ("1".equals(vehicleTO.getFleetTypeId())) {

                if (!"".equals(vehicleTO.getInsuranceid())) {
                    System.out.println("ENTERED DAO updateinsuranceId");
                    map.put("insuranceid", vehicleTO.getInsuranceid());
                    map.put("vendorId", ledgerIdAndCode[0]);
                    System.out.println("map update=" + map);
                    insuranceId = (Integer) session.update("mfr.updateInsurance", map);
                    map.put("clearanceId", vehicleTO.getClearanceId());
//                updateinsuranceId = (Integer) getSqlMapClientTemplate().update("mfr.updateInsuranceChequeType", map);
                    System.out.println("updateinsuranceId" + insuranceId);

                } else {
                    if ("2".equals(vehicleTO.getPaymentType())) {
                        map.put("bankBranchId", bankBranch[0]);
                        map.put("creditLedgerId", bankBranch[1]);
                        map.put("formReferId", ThrottleConstants.vehicleInsuranceFormId);
                        insuranceId = (Integer) session.insert("mfr.insertInsuranceChequeType", map);
                        map.put("clearanceId", insuranceId);
                    }

                    map.put("vendorId", ledgerIdAndCode[0]);
                    insuranceId = (Integer) session.insert("mfr.insertInsurance", map);
                    map.put("insuranceid", insuranceId);

                    if (!"".equals(insuranceId)) {
                        System.out.println("insuranceidmap" + map);
                        updateinsuranceId = (Integer) session.update("mfr.updateVehicleInsuranceId", map);
                        System.out.println("updateinsuranceId" + updateinsuranceId);

                        if ("2".equals(vehicleTO.getPaymentType())) {
                            map.put("formReferenceCode", ThrottleConstants.vehicleInsuranceFormCode + insuranceId);
                            updateinsuranceId = (Integer) session.update("mfr.updateInsuranceFormCode", map);
                            System.out.println("updateinsuranceId" + updateinsuranceId);
                        }
                    }

                    Integer formId = 0;
                    Integer voucherNo = 0;
                    Integer status = 0;
                    if ("1".equals(vehicleTO.getPaymentType())) {
                    } else {
                        // BankPayment
                        formId = Integer.parseInt(ThrottleConstants.BankPaymentFormId);
                        map.put("formId", formId);
                        voucherNo = (Integer) session.insert("finance.getFormVoucherNo", map);
                        map.put("voucherNo", voucherNo);
                        map.put("voucherCodeNo", ThrottleConstants.BankPaymentVoucherCode + voucherNo);
                        map.put("voucherType", "%PAYMENT%");
                        String code = "";
                        String[] temp;
                        code = (String) session.queryForObject("finance.getVoucerCode", map);
                        temp = code.split("-");
                        int codeval2 = Integer.parseInt(temp[1]);
                        int codev2 = codeval2 + 1;
                        String voucherCode = "PAYMENT-" + codev2;
                        System.out.println("voucherCode = " + voucherCode);
                        map.put("detailCode", "1");
                        map.put("voucherCode", voucherCode);
                        map.put("accountEntryDate", vehicleTO.getPaymentDate());
                        map.put("mainEntryType", "VOUCHER");
                        map.put("entryType", "PAYMENT");
                        map.put("ledgerId", ledgerIdAndCode[1]);
                        map.put("particularsId", ledgerIdAndCode[2]);
                        map.put("amount", vehicleTO.getPremiunAmount());
                        map.put("accountsType", "CREDIT");
                        map.put("narration", "vehicle Insurance");
                        map.put("reference", "Compliance");

                        System.out.println("map1 updateClearnceDate=---------------------> " + map);
                        status = (Integer) session.update("finance.insertAccountEntry", map);
                        System.out.println("status1 = " + status);
                        //--------------------------------- acc 2nd row start --------------------------
                        if (status > 0) {
                            map.put("detailCode", "2");
                            map.put("accountEntryDate", vehicleTO.getPaymentDate());
                            map.put("ledgerId", ThrottleConstants.vehicleInsuranceLedgerId);
                            map.put("particularsId", ThrottleConstants.vehicleInsuranceLedgerCode);
                            map.put("accountsType", "DEBIT");
                            System.out.println("map2 updateClearnceDate=---------------------> " + map);
                            status = (Integer) session.update("finance.insertAccountEntry", map);
                            System.out.println("status2 = " + status);
                        }

                    }
                }
            } //            trailer PAYMENT
            else if ("2".equals(vehicleTO.getFleetTypeId())) {
                System.out.println("ENTERED TRAILER PAYMENT ");
                if ("2".equals(vehicleTO.getPaymentType())) {
                    map.put("bankBranchId", bankBranch[0]);
                    map.put("creditLedgerId", bankBranch[1]);
                    map.put("formReferId", ThrottleConstants.trailerInsuranceFormId);
                    insuranceId = (Integer) session.insert("mfr.insertTrailerInsuranceChequeType", map);
                    map.put("clearanceId", insuranceId);
                }

                map.put("vendorId", ledgerIdAndCode[0]);
                insuranceId = (Integer) session.insert("mfr.insertTrailerInsurance", map);
                map.put("insuranceid", insuranceId);

                if (!"".equals(insuranceId)) {
                    System.out.println("insuranceidmap" + map);
                    updateinsuranceId = (Integer) session.update("mfr.updateTrailerInsuranceId", map);
                    System.out.println("updateinsuranceId" + updateinsuranceId);

                    if ("2".equals(vehicleTO.getPaymentType())) {
                        map.put("formReferenceCode", ThrottleConstants.trailerInsuranceFormCode + insuranceId);
                        updateinsuranceId = (Integer) session.update("mfr.updateTrailerInsuranceFormCode", map);
                        System.out.println("updateinsuranceId" + updateinsuranceId);
                    }
                }

                Integer formId = 0;
                Integer voucherNo = 0;
                Integer status = 0;
                if ("1".equals(vehicleTO.getPaymentType())) {
                } else {
                    // BankPayment 
                    formId = Integer.parseInt(ThrottleConstants.BankPaymentFormId);
                    map.put("formId", formId);
                    voucherNo = (Integer) session.insert("finance.getFormVoucherNo", map);
                    map.put("voucherNo", voucherNo);
                    map.put("voucherCodeNo", ThrottleConstants.BankPaymentVoucherCode + voucherNo);
                    map.put("voucherType", "%PAYMENT%");
                    String code = "";
                    String[] temp;
                    code = (String) session.queryForObject("finance.getVoucerCode", map);
                    temp = code.split("-");
                    int codeval2 = Integer.parseInt(temp[1]);
                    int codev2 = codeval2 + 1;
                    String voucherCode = "PAYMENT-" + codev2;
                    System.out.println("voucherCode = " + voucherCode);
                    map.put("detailCode", "1");
                    map.put("voucherCode", voucherCode);
                    map.put("accountEntryDate", vehicleTO.getPaymentDate());
                    map.put("mainEntryType", "VOUCHER");
                    map.put("entryType", "PAYMENT");
                    map.put("ledgerId", ledgerIdAndCode[1]);
                    map.put("particularsId", ledgerIdAndCode[2]);
                    map.put("amount", vehicleTO.getPremiunAmount());
                    map.put("accountsType", "CREDIT");
                    map.put("narration", "Trailer Insurance");
                    map.put("reference", "Compliance");

                    System.out.println("map1 updateClearnceDate=---------------------> " + map);
                    status = (Integer) session.update("finance.insertAccountEntry", map);
                    System.out.println("status1 = " + status);
                    //--------------------------------- acc 2nd row start --------------------------
                    if (status > 0) {
                        map.put("detailCode", "2");
                        map.put("accountEntryDate", vehicleTO.getPaymentDate());
                        map.put("ledgerId", ThrottleConstants.trailerInsuranceLedgerId);
                        map.put("particularsId", ThrottleConstants.trailerInsuranceLedgerCode);
                        map.put("accountsType", "DEBIT");
                        System.out.println("map2 updateClearnceDate=---------------------> " + map);
                        status = (Integer) session.update("finance.insertAccountEntry", map);
                        System.out.println("status2 = " + status);
                    }

                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("updateVehicleTyreDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "updateVehicleTyreDetails", sqlException);
        }
        return insuranceId;
    }

    public int insertRoadTaxDetails(VehicleTO vehicleTO, int userId, int madhavaramSp, SqlMapClient session) {
        Map map = new HashMap();
        int axleId = 0;
        int axleDetails = 0;
        int roadTaxId = 0;
        int clearanceIdRT = 0;
        String roadTax = "vehicle RoadTax";
        try {
            System.out.println("getBankBranchIdRT hai*********");
            String[] bankBranch = vehicleTO.getBankBranchIdRT().split("~");
            String[] ledgerIdAndCode = vehicleTO.getLedgerId().split("~");

            map.put("roadTaxReceiptNo", vehicleTO.getRoadTaxReceiptNo());
            map.put("roadTaxReceiptDate", vehicleTO.getRoadTaxReceiptDate());
            map.put("roadTaxPaidLocation", vehicleTO.getRoadTaxPaidLocation());
            map.put("roadTaxPeriod", vehicleTO.getRoadTaxPeriod());
            map.put("roadTaxPaidAmount", vehicleTO.getRoadTaxPaidAmount());
            map.put("roadTaxFromDate", vehicleTO.getRoadTaxFromDate());
            map.put("nextRoadTaxDate", vehicleTO.getNextRoadTaxDate());
            map.put("roadTaxRemarks", vehicleTO.getRoadTaxRemarks());

            map.put("paymentType", vehicleTO.getPaymentTypeRT());
            map.put("bankId", vehicleTO.getBankIdRT());
            map.put("chequeDate", vehicleTO.getChequeDateRT());
            map.put("chequeNo", vehicleTO.getChequeNoRT());
            map.put("chequeAmount", vehicleTO.getChequeAmountRT());
            map.put("paymentDateRT", vehicleTO.getPaymentDateRT());
//            map.put("debitLedgerId", ledgerIdAndCode[1]);
            map.put("debitLedgerId", 0);
//            map.put("clearanceDate", vehicleTO.getClearanceDateRT());
            map.put("reference", roadTax);

            map.put("vehicleId", vehicleTO.getVehicleId());
            map.put("userId", userId);
            map.put("companyId", madhavaramSp);
            System.out.println("insertRoadTaxDetails dao" + map);
            if (!"".equals(vehicleTO.getRoadTaxId()) && vehicleTO.getRoadTaxId() != null) {
                map.put("roadTaxId", vehicleTO.getRoadTaxId());
                map.put("vendorId", ledgerIdAndCode[0]);
                axleDetails = (Integer) session.update("mfr.updateVehicleRoadTax", map);
                map.put("clearanceIdRT", vehicleTO.getClearanceIdRT());

//                axleDetails = (Integer) getSqlMapClientTemplate().update("mfr.updateRoadTaxChequeType", map);
            } else {
                if ("2".equals(vehicleTO.getPaymentTypeRT())) {
                    map.put("bankBranchId", bankBranch[0]);
                    map.put("creditLedgerId", bankBranch[1]);
                    clearanceIdRT = (Integer) session.insert("mfr.insertRoadTaxChequeType", map);
                    map.put("clearanceIdRT", clearanceIdRT);
                }

                map.put("vendorId", 0);
//                map.put("vendorId", ledgerIdAndCode[0]);
//                System.out.println("vendorId=" + ledgerIdAndCode[0]);
//                System.out.println("ledgerId=" + ledgerIdAndCode[1]);
//                System.out.println("ledgerCode=" + ledgerIdAndCode[2]);
                roadTaxId = (Integer) session.insert("mfr.insertVehicleRoadTax", map);
                map.put("roadTaxId", roadTaxId);
                System.out.println("insertRoadTaxDetails insertRoadTaxDetails 2= " + map);
                System.out.println("insertRoadTaxDetails insertRoadTaxDetails3= " + roadTaxId);

                if ("2".equals(vehicleTO.getPaymentTypeRT())) {
                    if (roadTaxId != 0) {
                        System.out.println("test");
                        map.put("formReferenceCode", "VRT/16-17/00" + roadTaxId);
                        axleDetails = (Integer) session.update("mfr.updateRoadTaxFormCode", map);
                        System.out.println("insertRoadTaxDetails" + axleDetails);
                    }
                }

                Integer formId = 0;
                Integer voucherNo = 0;
                Integer status = 0;
                if ("1".equals(vehicleTO.getPaymentTypeRT())) {
                    System.out.println("entered cash payment");
                    formId = Integer.parseInt(ThrottleConstants.CashPaymentFormId);
                    map.put("formId", formId);
                    voucherNo = (Integer) session.insert("finance.getFormVoucherNo", map);
                    map.put("voucherNo", voucherNo);
                    map.put("voucherCodeNo", ThrottleConstants.CashPaymentVoucherCode + voucherNo);
                    map.put("voucherType", "%PAYMENT%");
                    System.out.println("cash map 1=" + map);
                    String code = "";
                    String[] temp;
                    code = (String) session.queryForObject("finance.getVoucerCode", map);
                    temp = code.split("-");
                    int codeval2 = Integer.parseInt(temp[1]);
                    int codev2 = codeval2 + 1;
                    String voucherCode = "PAYMENT-" + codev2;
                    System.out.println("voucherCode = " + voucherCode);

                    map.put("detailCode", "1");
                    map.put("voucherCode", voucherCode);
                    map.put("accountEntryDate", vehicleTO.getPaymentDateRT());
                    map.put("mainEntryType", "VOUCHER");
                    map.put("entryType", "PAYMENT");
                    map.put("ledgerId", ledgerIdAndCode[1]);
                    map.put("particularsId", ledgerIdAndCode[2]);
                    map.put("amount", vehicleTO.getRoadTaxPaidAmount());

                    map.put("accountsType", "DEBIT");
                    map.put("narration", "vehicle Road Tax");
                    map.put("reference", "Compliance");

                    System.out.println("cash map 2=---------------------> " + map);
                    status = (Integer) session.update("finance.insertAccountEntry", map);
                    System.out.println("debit status1 = " + status);
                    //--------------------------------- acc 2nd row start --------------------------
                    if (status > 0) {
                        map.put("detailCode", "2");
                        map.put("accountEntryDate", vehicleTO.getPaymentDateRT());
                        map.put("ledgerId", vehicleTO.getCashOnHandLedgerId());
                        map.put("particularsId", vehicleTO.getCashOnHandLedgerCode());
                        map.put("accountsType", "CREDIT");
                        System.out.println("cash map3 =---------------------> " + map);
                        status = (Integer) session.update("finance.insertAccountEntry", map);
                        System.out.println("credit status2 = " + status);
                    }

                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("updateVehicleTyreDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "updateVehicleTyreDetails", sqlException);
        }
        return roadTaxId;
    }

    public int insertFCDetails(VehicleTO vehicleTO, int userId, int madhavaramSp, SqlMapClient session) {
        Map map = new HashMap();
        int axleId = 0;
        int fcID = 0;
        String FC = "vehicle Fittness Certificate";
        try {
            String[] bankBranch = vehicleTO.getBankBranchIdFC().split("~");
            String[] ledgerIdAndCode = vehicleTO.getLedgerId().split("~");
            map.put("userId", userId);
            map.put("vehicleId", vehicleTO.getVehicleId());
            map.put("rtoDetail", vehicleTO.getRtoDetail());
            map.put("fcDate", vehicleTO.getFcDate());
            map.put("fcReceiptNo", vehicleTO.getFcReceiptNo());
            map.put("fcAmount", vehicleTO.getFcAmount());
            map.put("fcExpiryDate", vehicleTO.getFcExpiryDate());
            map.put("fcRemarks", vehicleTO.getFcRemarks());
            map.put("companyId", madhavaramSp);

            map.put("fleetTypeId", vehicleTO.getFleetTypeId());
            map.put("paymentType", vehicleTO.getPaymentTypeFC());
            map.put("bankId", vehicleTO.getBankIdFC());
            map.put("paymentDateFC", vehicleTO.getPaymentDateFC());
            map.put("chequeDate", vehicleTO.getChequeDateFC());
            map.put("chequeNo", vehicleTO.getChequeNoFC());
            map.put("chequeAmount", vehicleTO.getChequeAmountFC());
            map.put("odometer", vehicleTO.getOdometer());
            map.put("reportNo", vehicleTO.getReportNo());
            map.put("reference", FC);
            map.put("debitLedgerId", "0");
//            map.put("debitLedgerId", ledgerIdAndCode[1]);

            System.out.println("map1=" + map);
            if ("1".equals(vehicleTO.getFleetTypeId())) {

//            if (!"".equals(vehicleTO.getFcid()) && vehicleTO.getFcid() != null) {
//                map.put("fcid", vehicleTO.getFcid());
//                map.put("vendorId", ledgerIdAndCode[0]);
//                System.out.println("map2="+map);
//                fcID = (Integer) session.update("mfr.updateFCDetails", map);
//                 map.put("clearanceIdFC", vehicleTO.getClearanceIdFC());
////                fcID = (Integer) getSqlMapClientTemplate().update("mfr.updateFCChequeType", map);
//            } else {
                if ("2".equals(vehicleTO.getPaymentTypeFC())) {
                    System.out.println("entered bank payment inert checquetype 1");
                    map.put("bankBranchId", bankBranch[0]);
                    map.put("creditLedgerId", bankBranch[1]);
                    map.put("formReferId", ThrottleConstants.vehicleFCFormId);
                    fcID = (Integer) session.insert("mfr.insertFCChequeType", map);
                    map.put("clearanceIdFC", fcID);
                    System.out.println("map3=" + map);
                }
//                map.put("vendorId", ledgerIdAndCode[0]);
                map.put("vendorId", 0);
                fcID = (Integer) session.insert("mfr.insertFCDetails", map);
                map.put("fcID", fcID);
                System.out.println("map4=" + map);

                if ("2".equals(vehicleTO.getPaymentTypeFC())) {
                    System.out.println("entered bank payment2");
                    if (!"".equals(fcID)) {
                        map.put("formReferenceCode", ThrottleConstants.vehicleFCFormCode + fcID);
                        System.out.println("map5:::" + map);
                        fcID = (Integer) session.update("mfr.updateFCFormCode", map);
                    }
                }
                Integer formId = 0;
                Integer voucherNo = 0;
                Integer status = 0;
                if ("2".equals(vehicleTO.getPaymentTypeFC())) {
                    // BankPayment 
                    formId = Integer.parseInt(ThrottleConstants.BankPaymentFormId);
                    map.put("formId", formId);
                    voucherNo = (Integer) session.insert("finance.getFormVoucherNo", map);
                    map.put("voucherNo", voucherNo);
                    map.put("voucherCodeNo", ThrottleConstants.BankPaymentVoucherCode + voucherNo);
                    map.put("voucherType", "%PAYMENT%");
                    String code = "";
                    String[] temp;
                    code = (String) session.queryForObject("finance.getVoucerCode", map);
                    temp = code.split("-");
                    int codeval2 = Integer.parseInt(temp[1]);
                    int codev2 = codeval2 + 1;
                    String voucherCode = "PAYMENT-" + codev2;
                    System.out.println("voucherCode = " + voucherCode);
                    map.put("detailCode", "1");
                    map.put("voucherCode", voucherCode);
                    map.put("accountEntryDate", vehicleTO.getPaymentDateFC());
                    map.put("mainEntryType", "VOUCHER");
                    map.put("entryType", "PAYMENT");
                    map.put("ledgerId", ledgerIdAndCode[1]);
                    map.put("particularsId", ledgerIdAndCode[2]);
                    map.put("amount", vehicleTO.getFcAmount());
                    map.put("accountsType", "CREDIT");
                    map.put("narration", "vehicle FC");
                    map.put("reference", "Compliance");

                    System.out.println("map1 updateClearnceDate=---------------------> " + map);
                    status = (Integer) session.update("finance.insertAccountEntry", map);
                    System.out.println("status1 = " + status);
                    //--------------------------------- acc 2nd row start --------------------------
                    if (status > 0) {
                        map.put("detailCode", "2");
                        map.put("ledgerId", ThrottleConstants.vehicleFCLedgerId);
                        map.put("particularsId", ThrottleConstants.vehicleFCLedgerCode);
                        map.put("accountsType", "DEBIT");
                        System.out.println("map2 updateClearnceDate=---------------------> " + map);
                        status = (Integer) session.update("finance.insertAccountEntry", map);
                        System.out.println("status2 = " + status);
                    }

                }
//            }
            }
            /* else if ("2".equals(vehicleTO.getFleetTypeId())) {
             System.out.println("entered trailer");
             //              if (!"".equals(vehicleTO.getFcid()) && vehicleTO.getFcid() != null) {
             //                map.put("fcid", vehicleTO.getFcid());
             //                map.put("vendorId", ledgerIdAndCode[0]);
             //                System.out.println("map2="+map);
             //                fcID = (Integer) session.update("mfr.updateTrailerFCDetails", map);
             //                 map.put("clearanceIdFC", vehicleTO.getClearanceIdFC());
             ////                fcID = (Integer) getSqlMapClientTemplate().update("mfr.updateFCChequeType", map);
             //            } else {
             if ("2".equals(vehicleTO.getPaymentTypeFC())) {
             System.out.println("entered bank payment inert checquetype 1");
             map.put("bankBranchId", bankBranch[0]);
             map.put("creditLedgerId", bankBranch[1]);
             map.put("formReferId", ThrottleConstants.trailerFCFormId);
             fcID = (Integer) session.insert("mfr.insertTrailerFCChequeType", map);
             map.put("clearanceIdFC", fcID);
             System.out.println("map3=" + map);
             }
             map.put("vendorId", ledgerIdAndCode[0]);
             fcID = (Integer) session.insert("mfr.insertTrailerFCDetails", map);
             map.put("fcID", fcID);
             System.out.println("map4=" + map);

             if ("2".equals(vehicleTO.getPaymentTypeFC())) {
             System.out.println("entered bank payment2");
             if (!"".equals(fcID)) {
             map.put("formReferenceCode", ThrottleConstants.trailerFCFormCode + fcID);
             System.out.println("map5:::" + map);
             fcID = (Integer) session.update("mfr.updateTrailerFCFormCode", map);
             }
             }
             Integer formId = 0;
             Integer voucherNo = 0;
             Integer status = 0;
             if ("2".equals(vehicleTO.getPaymentTypeFC())) {
             // BankPayment 
             formId = Integer.parseInt(ThrottleConstants.BankPaymentFormId);
             map.put("formId", formId);
             voucherNo = (Integer) session.insert("finance.getFormVoucherNo", map);
             map.put("voucherNo", voucherNo);
             map.put("voucherCodeNo", ThrottleConstants.BankPaymentVoucherCode + voucherNo);
             map.put("voucherType", "%PAYMENT%");
             String code = "";
             String[] temp;
             code = (String) session.queryForObject("finance.getVoucerCode", map);
             temp = code.split("-");
             int codeval2 = Integer.parseInt(temp[1]);
             int codev2 = codeval2 + 1;
             String voucherCode = "PAYMENT-" + codev2;
             System.out.println("voucherCode = " + voucherCode);
             map.put("detailCode", "1");
             map.put("voucherCode", voucherCode);
             map.put("accountEntryDate", vehicleTO.getPaymentDateFC());
             map.put("mainEntryType", "VOUCHER");
             map.put("entryType", "PAYMENT");
             map.put("ledgerId", ledgerIdAndCode[1]);
             map.put("particularsId", ledgerIdAndCode[2]);
             map.put("amount", vehicleTO.getFcAmount());
             map.put("accountsType", "CREDIT");
             map.put("narration", "trailer FC");
             map.put("reference", "Compliance");

             System.out.println("map1 updateClearnceDate=---------------------> " + map);
             status = (Integer) session.update("finance.insertAccountEntry", map);
             System.out.println("status1 = " + status);
             //--------------------------------- acc 2nd row start --------------------------
             if (status > 0) {
             map.put("detailCode", "2");
             map.put("ledgerId", ThrottleConstants.trailerFCLedgerId);
             map.put("particularsId", ThrottleConstants.trailerFCLedgerCode);
             map.put("accountsType", "DEBIT");
             System.out.println("map2 updateClearnceDate=---------------------> " + map);
             status = (Integer) session.update("finance.insertAccountEntry", map);
             System.out.println("status2 = " + status);
             }


             }
             //            }  
             }*/

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("updateVehicleTyreDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "updateVehicleTyreDetails", sqlException);
        }
        return fcID;
    }

    public int insertPermitDetails(VehicleTO vehicleTO, int userId, int madhavaramSp, SqlMapClient session) {
        Map map = new HashMap();
        int axleId = 0;
        int permitId = 0;
        int clearanceIdPermit = 0;
        String permit = "vehicle Permit";
        try {
            String[] bankBranch = vehicleTO.getBankBranchIdPermit().split("~");
            String[] ledgerIdAndCode = vehicleTO.getLedgerId().split("~");
            map.put("permitType", vehicleTO.getPermitType());
            map.put("permitNo", vehicleTO.getPermitNo());
            map.put("permitAmount", vehicleTO.getPermitAmount());
            map.put("permitPaidDate", vehicleTO.getPermitPaidDate());
            map.put("permitExpiryDate", vehicleTO.getPermitExpiryDate());
            map.put("permitRemarks", vehicleTO.getPermitRemarks());

            map.put("paymentType", vehicleTO.getPaymentTypePermit());
//            map.put("paymentDatePermit", vehicleTO.getPaymentDatePermit());
            map.put("bankId", vehicleTO.getBankIdPermit());

            map.put("chequeDate", vehicleTO.getChequeDatePermit());
            map.put("chequeNo", vehicleTO.getChequeNoPermit());
            map.put("chequeAmount", vehicleTO.getChequeAmountPermit());
            map.put("debitLedgerId", ledgerIdAndCode[1]);
            map.put("reference", permit);

            map.put("vehicleId", vehicleTO.getVehicleId());
            map.put("companyId", madhavaramSp);
            map.put("userId", userId);
            if (!"".equals(vehicleTO.getPermitid()) && vehicleTO.getPermitid() != null) {
                map.put("permitId", vehicleTO.getPermitid());
                map.put("vendorId", ledgerIdAndCode[0]);
                permitId = (Integer) getSqlMapClientTemplate().update("mfr.updatePermitDetails", map);

                map.put("clearanceIdPermit", vehicleTO.getClearanceIdPermit());

//                permitId = (Integer) getSqlMapClientTemplate().update("mfr.updatePermitChequeType", map);
//                System.out.println("permitId "+permitId);
            } else {
                if ("2".equals(vehicleTO.getPaymentTypePermit())) {
                    map.put("bankBranchId", bankBranch[0]);
                    map.put("creditLedgerId", bankBranch[1]);
                    clearanceIdPermit = (Integer) getSqlMapClientTemplate().insert("mfr.insertPermitChequeType", map);
                    map.put("clearanceIdPermit", clearanceIdPermit);
                }
                map.put("vendorId", ledgerIdAndCode[0]);
                permitId = (Integer) getSqlMapClientTemplate().insert("mfr.insertPermitDetails", map);
                map.put("permitId", permitId);

                if ("2".equals(vehicleTO.getPaymentTypePermit())) {
                    if (!"".equals(permitId)) {
                        map.put("formReferenceCode", "VPermit/16-17/00" + permitId);
                        permitId = (Integer) getSqlMapClientTemplate().update("mfr.updatePermitFormCode", map);
                    }
                }
                Integer formId = 0;
                Integer voucherNo = 0;
                Integer status = 0;
                if ("1".equals(vehicleTO.getPaymentTypePermit())) {
                    System.out.println("entered cash payment");
                    formId = Integer.parseInt(ThrottleConstants.CashPaymentFormId);
                    map.put("formId", formId);
                    voucherNo = (Integer) session.insert("finance.getFormVoucherNo", map);
                    map.put("voucherNo", voucherNo);
                    map.put("voucherCodeNo", ThrottleConstants.CashPaymentVoucherCode + voucherNo);
                    map.put("voucherType", "%PAYMENT%");
                    System.out.println("cash map 1=" + map);
                    String code = "";
                    String[] temp;
                    code = (String) session.queryForObject("finance.getVoucerCode", map);
                    temp = code.split("-");
                    int codeval2 = Integer.parseInt(temp[1]);
                    int codev2 = codeval2 + 1;
                    String voucherCode = "PAYMENT-" + codev2;
                    System.out.println("voucherCode = " + voucherCode);

                    map.put("detailCode", "1");
                    map.put("voucherCode", voucherCode);
                    map.put("accountEntryDate", vehicleTO.getPermitPaidDate());
                    map.put("mainEntryType", "VOUCHER");
                    map.put("entryType", "PAYMENT");
                    map.put("ledgerId", ledgerIdAndCode[1]);
                    map.put("particularsId", ledgerIdAndCode[2]);
                    map.put("amount", vehicleTO.getPermitAmount());
                    map.put("accountsType", "DEBIT");
                    map.put("narration", "Vehicle Permit");
                    map.put("reference", "Compliance");

                    System.out.println("cash map 2=---------------------> " + map);
                    status = (Integer) session.update("finance.insertAccountEntry", map);
                    System.out.println("debit status1 = " + status);
                    //--------------------------------- acc 2nd row start --------------------------
                    if (status > 0) {
                        map.put("detailCode", "2");
                        map.put("accountEntryDate", vehicleTO.getPaymentDatePermit());
                        map.put("ledgerId", vehicleTO.getCashOnHandLedgerId());
                        map.put("particularsId", vehicleTO.getCashOnHandLedgerCode());
                        map.put("accountsType", "CREDIT");
                        System.out.println("cash map3 =---------------------> " + map);
                        status = (Integer) session.update("finance.insertAccountEntry", map);
                        System.out.println("credit status2 = " + status);
                    }

                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("updateVehicleTyreDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "updateVehicleTyreDetails", sqlException);
        }
        return permitId;
    }

    public int insertVehicleDepreciation(VehicleTO vehicleTO, int userId, int madhavaramSp) {
        Map map = new HashMap();
        int status = 0;
        int insertVehicleDepreciation = 0;
        try {
            String vehicleDepreciationCost = vehicleTO.getVehicleDepreciationCost();
            String depreciationType = vehicleTO.getDepreciationType();
            if (!"".equals(depreciationType)) {
                map.put("vehicleId", vehicleTO.getVehicleId());
                map.put("vehicleDepreciationCost", vehicleDepreciationCost);
                map.put("depreciationType", depreciationType);
                status = (Integer) getSqlMapClientTemplate().update("mfr.updateVehicleDepreciationDetails", map);
            }
            String[] vehicleYear = vehicleTO.getVehicleYear().split("~");
            String[] depreciation = vehicleTO.getDepreciation().split("~");
            String[] yearCost = vehicleTO.getYearCost().split("~");
            String[] perMonth = vehicleTO.getPerMonth().split("~");
            for (int i = 0; i < vehicleYear.length; i++) {
                map.put("vehicleYear", vehicleYear[i]);
                map.put("depreciation", depreciation[i]);
                map.put("yearCost", yearCost[i]);
                map.put("perMonth", perMonth[i]);
                map.put("vehicleId", vehicleTO.getVehicleId());
                map.put("companyId", madhavaramSp);
                map.put("userId", userId);
                insertVehicleDepreciation = (Integer) getSqlMapClientTemplate().update("mfr.insertVehicleDepreciation", map);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("updateVehicleTyreDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "updateVehicleTyreDetails", sqlException);
        }
        return insertVehicleDepreciation;
    }

//    gulshan 08/12/15
    public int saveVehicleFinanceMaster(VehicleTO vehicleTO, int userId) {
        int count = 0;
        int status = 0;
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        System.out.println("===saveVehicleFinanceMaster_DAO===");
        map.put("userId", userId);
        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("bankerName", vehicleTO.getBankerName());
        map.put("bankerAddress", vehicleTO.getBankerAddress());
        map.put("vehicleAmt", vehicleTO.getVehicleAmt());
        map.put("roi", vehicleTO.getRoi());
        map.put("emiMonths", vehicleTO.getEmiMonths());
        map.put("emiAmount", vehicleTO.getEmiAmount());
        map.put("monthlyAmount", vehicleTO.getMonthlyAmount());
        map.put("emiStartDate", vehicleTO.getEmiStartDate());
        map.put("emiEndDate", vehicleTO.getEmiEndDate());
        map.put("emiPayDay", vehicleTO.getEmiPayDay());
        map.put("payMode", vehicleTO.getPayMode());
        System.out.println("map===" + map);

        try {
            status = (Integer) getSqlMapClientTemplate().update("mfr.updateVehicleFinanceMaster", map);
            System.out.println("status in update finance " + status);
            if (status == 0) {
                status = (Integer) getSqlMapClientTemplate().update("mfr.insertVehicleFinanceMaster", map);

                map.put("bankerName", vehicleTO.getBankerName());
                map.put("vehicleId", vehicleTO.getVehicleId());
                map.put("emiPayDay", vehicleTO.getEmiPayDay());
                map.put("monthlyAmount", vehicleTO.getMonthlyAmount());
                map.put("Status", 'N');
                map.put("userId", userId);

                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                Date startEmiDate = new Date();
                Date endEmiDate = new Date();
                String startDate = "";
                String endDate = "";
                startEmiDate = (Date) dateFormat.parse(vehicleTO.getEmiStartDate().toString());
                endEmiDate = (Date) dateFormat.parse(vehicleTO.getEmiEndDate().toString());
//                System.out.println("startEmiDate" + startEmiDate);
//                System.out.println("endEmiDate" + endEmiDate);

                String[] temp = null;
                for (int i = 0; i < Integer.parseInt(vehicleTO.getEmiMonths()); i++) {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(startEmiDate);
                    cal.add(Calendar.MONTH, 1);
                    startDate = dateFormat.format(cal.getTime());
                    map.put("emiDate", startDate);
//                    System.out.println("startDate in map is--------------- " + startDate);
                    status = (Integer) getSqlMapClientTemplate().update("mfr.insertmonthlyEmiMaster", map);
                    startEmiDate = (Date) dateFormat.parse(startDate.toString());
                    System.out.println("status in insert finance " + status);
                }

            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "saveVehicleInsurance", sqlException);
        }
        return status;

    }

    public String getVehicleDetailsInsuranceMas(String regNo) {
        System.out.println("..........ajax in DAO.....................");
        Map map = new HashMap();
        String vehicleDetails = "";
        try {
            map.put("regNo", regNo);
            vehicleDetails = (String) getSqlMapClientTemplate().queryForObject("mfr.VehicleDetailsInsuranceMas", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkVehicleTyre Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleDetails", sqlException);
        }

        return vehicleDetails;
    }

    public ArrayList getVehicledue(VehicleTO vehicleTO) {

        Map map = new HashMap();
        ArrayList Vehicledue = new ArrayList();

        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("regNo", vehicleTO.getRegNo());
        map.put("emiStartDate", vehicleTO.getEmiStartDate());
        map.put("EmiEndDate", vehicleTO.getEmiEndDate());
        map.put("financeId", vehicleTO.getFinanceId());
        System.out.println("getVehicledue map:" + map);

        try {
            Vehicledue = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehicledue", map);
            System.out.println("getVehicledue size= " + Vehicledue.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicledue Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getVehicledue", sqlException);
        }
        return Vehicledue;

    }

    public int SavevehicleDuePayment(VehicleTO vehicleTO, int userId) {
        int count = 0;
        int status = 0;
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", userId);
        map.put("PaymentId", vehicleTO.getPaymentId());
        map.put("AmountPaid", vehicleTO.getAmountPaid());
        map.put("PaymentMadeOn", vehicleTO.getPaymentMadeOn());
        map.put("BalanceAmount", vehicleTO.getBalanceAmount());
        map.put("Status", 'Y');
        map.put("EmiPayer", vehicleTO.getEmiPayer());
        map.put("Remarks", vehicleTO.getRemarks());
        System.out.println("map1===" + map);

        status = (Integer) getSqlMapClientTemplate().update("mfr.UpdatevehicleDuePayment", map);
        try {

            System.out.println("con_status==" + status);

            map.put("TotalBalance", vehicleTO.getTotalBalance());
            map.put("financeId", vehicleTO.getFinanceId());
            System.out.println("map2===" + map);
            status = (Integer) getSqlMapClientTemplate().update("mfr.UpdatelastEmiBalance", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "saveVehicleInsurance", sqlException);
        }
        return status;
    }

    public String getVehicleRegDetails(String regNo) {
        System.out.println("..........ajax in DAO.....................");
        Map map = new HashMap();
        String VehicleRegDetails = "";
        try {
            map.put("regNo", regNo);
            VehicleRegDetails = (String) getSqlMapClientTemplate().queryForObject("mfr.getVehicleRegDetails", map);
            System.out.println("VehicleRegDetails" + VehicleRegDetails);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkVehicleTyre Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleDetails", sqlException);
        }

        return VehicleRegDetails;
    }

    public ArrayList getVehicledetails(VehicleTO vehicleTO) {

        Map map = new HashMap();
        ArrayList Duedetails = new ArrayList();

        map.put("vehicleId", vehicleTO.getVehicleId());

        System.out.println("map:" + map);

        try {
            Duedetails = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehicledueDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Vehicledetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "Vehicledetails", sqlException);
        }
        return Duedetails;
    }

    public ArrayList getPaidDuedetails(VehicleTO vehicleTO) {

        Map map = new HashMap();
        ArrayList PaidDuedetails = new ArrayList();
        map.put("vehicleId", vehicleTO.getVehicleId());
        map.put("financeId", vehicleTO.getFinanceId());
        System.out.println("map:" + map);

        try {
            PaidDuedetails = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getPaidDuedetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Vehicledetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "Vehicledetails", sqlException);
        }
        return PaidDuedetails;

    }

    public ArrayList getVehiclesDetails(String vehicleId) {

        Map map = new HashMap();
        ArrayList vehiclesDetails = new ArrayList();
        map.put("vehicleId", vehicleId);
        System.out.println("map:" + map);

        try {
            vehiclesDetails = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehiclesDetails", map);
            System.out.println("vehiclesDetails" + vehiclesDetails);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Vehicledetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "Vehicledetails", sqlException);
        }
        return vehiclesDetails;

    }

    public ArrayList getVehiclesInsuranceList(VehicleTO vehicleTO) {
        Map map = new HashMap();
        ArrayList vehiclesList = new ArrayList();
        map.put("regNo", vehicleTO.getRegNo());
        map.put("vendorId", vehicleTO.getVendorId());
//        map.put("fromDate", vehicleTO.getFromDate());
//        map.put("toDate", vehicleTO.getToDate());
        System.out.println("getVehiclesInsuranceList map:" + map);

        try {
            vehiclesList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehiclesInsuranceList", map);
            System.out.println("getVehiclesInsuranceList" + vehiclesList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehiclesList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "vehiclesList", sqlException);
        }
        return vehiclesList;

    }

    public ArrayList getFleetPurchaseList(VehicleTO vehicleTO) {
        Map map = new HashMap();
        ArrayList vehiclesList = new ArrayList();
        map.put("regNo", vehicleTO.getRegNo());
        map.put("companyId", vehicleTO.getVendorId());
        map.put("fromDate", vehicleTO.getFromDate());
        map.put("toDate", vehicleTO.getToDate());
        System.out.println("getFleetPurchaseList map:" + map);

        try {
            vehiclesList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getFleetPurchaseList", map);
            System.out.println("getFleetPurchaseList = " + vehiclesList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFleetPurchaseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getFleetPurchaseList", sqlException);
        }
        return vehiclesList;

    }

    public ArrayList getVehiclesFCList(VehicleTO vehicleTO) {

        Map map = new HashMap();
        ArrayList vehiclesFCList = new ArrayList();
        map.put("regNo", vehicleTO.getRegNo());
        map.put("companyId", vehicleTO.getCompanyId());
        map.put("fromDate", vehicleTO.getFromDate());
        map.put("toDate", vehicleTO.getToDate());
        System.out.println("getVehiclesFCList map:" + map);

        try {
            vehiclesFCList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehiclesFCList", map);
            System.out.println("getVehiclesFCList" + vehiclesFCList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehiclesList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "vehiclesList", sqlException);
        }
        return vehiclesFCList;

    }

    public ArrayList getTrailerInsuranceList(VehicleTO vehicleTO) {

        Map map = new HashMap();
        ArrayList vehiclesList = new ArrayList();
        map.put("regNo", vehicleTO.getRegNo());
        map.put("companyId", vehicleTO.getCompanyId());
        map.put("fromDate", vehicleTO.getFromDate());
        map.put("toDate", vehicleTO.getToDate());
        System.out.println("getVehiclesInsuranceList map:" + map);

        try {
            vehiclesList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getTrailerInsuranceList", map);
            System.out.println("getVehiclesInsuranceList" + vehiclesList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehiclesList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "vehiclesList", sqlException);
        }
        return vehiclesList;

    }

    public ArrayList getTrailerFCList(VehicleTO vehicleTO) {

        Map map = new HashMap();
        ArrayList vehiclesFCList = new ArrayList();
        map.put("regNo", vehicleTO.getRegNo());
        map.put("companyId", vehicleTO.getCompanyId());
        map.put("fromDate", vehicleTO.getFromDate());
        map.put("toDate", vehicleTO.getToDate());
        System.out.println("getVehiclesFCList map:" + map);

        try {
            vehiclesFCList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getTrailerFCList", map);
            System.out.println("getVehiclesFCList" + vehiclesFCList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehiclesList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "vehiclesList", sqlException);
        }
        return vehiclesFCList;

    }

    public ArrayList getVehicleDepreciationList(String vehicleId) {

        Map map = new HashMap();
        ArrayList vehicleDepreciation = new ArrayList();
        map.put("vehicleId", vehicleId);
        System.out.println("map:" + map);

        try {
            vehicleDepreciation = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehicleDepreciationList", map);
            System.out.println("vehiclesDetails" + vehicleDepreciation);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDepreciationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getVehicleDepreciationList", sqlException);
        }
        return vehicleDepreciation;

    }

    public int saveFileUploadDetails(String actualFilePath, String fileSaved, String remarks, VehicleTO vehicleTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        FileInputStream fis = null;
        int tripId = 0;
        int cityId = 0;
        try {

            map.put("fileName", fileSaved);
            map.put("userId", userId);
            map.put("vehicleID", vehicleTO.getVehicleId());
            map.put("remarks", remarks);
            System.out.println("map1111 = " + map);
            System.out.println("actualFilePath = " + actualFilePath);
            File file = new File(actualFilePath);
            System.out.println("file = " + file);
            fis = new FileInputStream(file);
            System.out.println("fis = " + fis);
            byte[] uploadfile = new byte[(int) file.length()];
//           System.out.println("uploadfile = " + uploadfile);
            fis.read(uploadfile);
            fis.close();
            map.put("uploadfile", uploadfile);
            System.out.println("the saveFileUploadDetails123455" + map);
            status = (Integer) getSqlMapClientTemplate().update("mfr.saveFileUploadDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveFileUploadDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveFileUploadDetails List", sqlException);
        }

        return status;
    }

    public int insertOemDetails(VehicleTO vehicleTO, String oemDetailsId, String oemMfr, String oemTyreNo, String oemDepth, String oemBattery, String oemId, int userId) {
        Map map = new HashMap();
        int oemId1 = 0;
        int axleDetails = 0;
        try {
            map.put("userId", userId);
            map.put("vehicleID", vehicleTO.getVehicleId());
            map.put("oemDetailsId", oemDetailsId);
            map.put("oemMfr", oemMfr);
            map.put("oemTyreNo", oemTyreNo);
            map.put("oemDepth", oemDepth);
            map.put("oemBattery", oemBattery);
            int oemInsert = Integer.parseInt(oemId);
            map.put("oemId", oemInsert);
            System.out.println("mapInsert" + map);
            map.put("companyId", 1011);
	//            if(oemInsert > 0){
            //            oemId1 = (Integer) getSqlMapClientTemplate().update("mfr.updateOemDetails", map);
            //                System.out.println("oemId1daoupdate"+oemId1);
            //            }else{
            //            }
            if (oemDetailsId.equals("2")) {
                oemId1 = (Integer) getSqlMapClientTemplate().insert("mfr.insertOemDetails", map);
            } else {
                int tyreId = 0;
                Object tyreIdStr = getSqlMapClientTemplate().queryForObject("mfr.checkTyreNo", map);
                map.put("vehicleID", vehicleTO.getVehicleId());
                map.put("itemId", oemMfr);
                map.put("tyreNo", oemTyreNo);
                map.put("kmReading", vehicleTO.getKmReading());
                map.put("oemDepth", oemDepth);
                if (tyreIdStr != null) {
                    tyreId = (Integer) tyreIdStr;
                    System.out.println("tyreId:111111::" + tyreId);
                } else {
                    tyreId = (Integer) getSqlMapClientTemplate().update("mfr.insertVehicleTyre", map);
                    System.out.println("tyreId:222222::" + tyreId);
                }
                map.put("tyreId", tyreId);
                // update tyre no
                int checkTyreStatus = 0;
                checkTyreStatus = (Integer) getSqlMapClientTemplate().queryForObject("mfr.checkTyreNoStatus", map);
                System.out.println("checkTyreStatus" + checkTyreStatus);
                if (checkTyreStatus == 0) {
                    axleDetails = (Integer) getSqlMapClientTemplate().update("mfr.insertVehicleStepni", map);
                } else if (vehicleTO.getUpdateType().equals("0")) {
                    axleDetails = 0;
                }
            }
            System.out.println("oemId" + oemId1);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveFileUploadDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveFileUploadDetails List", sqlException);
        }

        return oemId1;
    }

    public ArrayList getOemList(VehicleTO vehicleTO) {

        Map map = new HashMap();
        ArrayList oemList = new ArrayList();
        map.put("vehicleId", vehicleTO.getVehicleId());
        System.out.println("getOemList:" + map);

        try {
            oemList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getOemList", map);
            System.out.println("getOemList oemListdao" + oemList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Vehicledetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "Vehicledetails", sqlException);
        }
        return oemList;

    }

    public ArrayList getTrailerNoList(VehicleTO vehicleTO) {
        Map map = new HashMap();
        ArrayList trailerList = new ArrayList();
        map.put("trailerNo", vehicleTO.getTrailerNo() + "%");
        System.out.println("map+" + map);
        try {
            trailerList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getTrailerListForAvailability", map);
            System.out.println(" trailerList =" + trailerList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleList", sqlException);
        }

        return trailerList;
    }

    public int insertVessel(VehicleTO vehicleTO, int userId) {
        Map map = new HashMap();

        int status = 0;
        try {
            map.put("vesselName", vehicleTO.getVesselName());
            map.put("vesselCode", vehicleTO.getVesselCode());
            map.put("vesselType", vehicleTO.getVesselType());
            map.put("linerCode", vehicleTO.getLinerCode());
            map.put("userId", userId);
            System.out.println("map Vessel 2 = " + map);
            status = (Integer) getSqlMapClientTemplate().insert("mfr.insertVessel", map);
            System.out.println("status" + status);
//            if (!"".equals(vehicleTO.getInsuranceid()) && vehicleTO.getInsuranceid() != null) {
//                map.put("insuranceid", vehicleTO.getInsuranceid());
//                System.out.println("map Second:::::: = " + map);
//                insuranceId = (Integer) getSqlMapClientTemplate().update("mfr.updateInsurance", map);
//                System.out.println("insuranceId11111 = " + insuranceId);
//            } else {
//
//                insuranceId = (Integer) getSqlMapClientTemplate().insert("mfr.insertInsurance", map);
//                map.put("insuranceid", insuranceId);
//                System.out.println("insuranceidmap"+map);
//                updateinsuranceId = (Integer) getSqlMapClientTemplate().update("mfr.updateVehicleInsuranceId", map);
//                System.out.println("updateinsuranceId"+updateinsuranceId);
//            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("updateVehicleTyreDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "updateVehicleTyreDetails", sqlException);
        }
        return status;
    }

    public ArrayList getVesselNameList(VehicleTO vehicleTO) {
        Map map = new HashMap();
        ArrayList getVesselName = new ArrayList();
        map.put("vesselName", vehicleTO.getDischargeVesselName() + "%");
        System.out.println("map+" + map);
        try {
            getVesselName = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVesselName", map);
            System.out.println(" getVesselName =" + getVesselName.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleList", sqlException);
        }

        return getVesselName;
    }

    public int insertDischargeVessel(VehicleTO vehicleTO, int userId) {
        Map map = new HashMap();

        int status = 0;
        try {
            map.put("vesselName", vehicleTO.getDischargeVesselName());
            map.put("vesselId", vehicleTO.getVesselId());
            map.put("voyageNo", vehicleTO.getVoyageNo());
            map.put("linersName", vehicleTO.getLinersName());
            map.put("agentName", vehicleTO.getAgentName());
            map.put("plannedArrivalDate", vehicleTO.getPlannedArrivalDate());
            map.put("plannedArrivalPort", vehicleTO.getPlannedArrivalPort());
            map.put("plannedDischargeDate", vehicleTO.getPlannedDischargeDate());
            map.put("plannedDischargePort", vehicleTO.getPlannedDischargePort());
            map.put("status", vehicleTO.getStatus());
            map.put("description", vehicleTO.getDescription());
            map.put("userId", userId);
            System.out.println("map Discharge Vessel 2 = " + map);
            status = (Integer) getSqlMapClientTemplate().insert("mfr.insertDischargeVessel", map);
            System.out.println("status" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("updateVehicleTyreDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "updateVehicleTyreDetails", sqlException);
        }
        return status;
    }

    public int processInsertTrailerTypeDetails(VehicleTO vehicleTO, int UserId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", UserId);
        map.put("typeName", vehicleTO.getTypeName());
        map.put("tonnage", vehicleTO.getTonnage());
        map.put("capacity", vehicleTO.getCapacity());
        map.put("typeDesc", vehicleTO.getDescription());
        map.put("axleTypeId", vehicleTO.getAxleTypeId());
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("mfr.insertTrailerType", map);
            System.out.println("status dao type= " + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertTrailerType Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "insertTrailerType", sqlException);
        }
        return status;

    }

    public int processModifyTrailerTypeDetails(ArrayList List, int UserId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        int status = 0;
        try {
            Iterator itr = List.iterator();
            VehicleTO vehicleTO = null;
            while (itr.hasNext()) {
                vehicleTO = (VehicleTO) itr.next();
                map.put("typeId", vehicleTO.getTypeId());
                map.put("typeName", vehicleTO.getTypeName());
                map.put("tonnage", vehicleTO.getTonnage());
                map.put("capacity", vehicleTO.getCapacity());
                map.put("Description", vehicleTO.getDescription());
                map.put("Active_Ind", vehicleTO.getActiveInd());
                map.put("userId", UserId);
                status = (Integer) getSqlMapClientTemplate().update("mfr.updateTrailerType", map);
                System.out.println("altertype" + status);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processModifyTrailerTypeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "processModifyTrailerTypeDetails", sqlException);
        }
        return status;

    }

    public ArrayList getMfrList(VehicleTO vehicleTO) {
        Map map = new HashMap();
        map.put("fleetTypeId", vehicleTO.getFleetTypeId());
        ArrayList MfrList = new ArrayList();
        try {
            MfrList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getMfrList", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMfrList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getMfrList", sqlException);
        }
        return MfrList;

    }

    public String doAddTrailereDetails(VehicleTO vehicleTO, int UserId, SqlMapClient session) {
        System.out.println("vehicle DAO Throttle Here ");
        Map map = new HashMap();

        /*
         * set the parameters in the map for sending to ORM
         */
        String ownerS = "";
        map.put("userId", UserId);
        map.put("regNo", vehicleTO.getRegNo());
        map.put("mfrId", vehicleTO.getMfrId());
        map.put("groupId", vehicleTO.getGroupId());
        map.put("modelId", vehicleTO.getModelId());
        map.put("dateOfSale", vehicleTO.getDateOfSale());
        map.put("chassisNo", vehicleTO.getChassisNo());
        map.put("description", vehicleTO.getDescription());
        map.put("seats", vehicleTO.getSeatCapacity());
        if ("3".equals(vehicleTO.getOwnerShips()) || "4".equals(vehicleTO.getOwnerShips()) || "2".equals(vehicleTO.getOwnerShips())) {
            map.put("war_period", 0);
            map.put("hourMeter", 0);
            map.put("kiloMeter", 0);
            map.put("dailyRunKm", 0);
            map.put("dailyRunHm", 0);
            map.put("gpsSystemId", 0);
        } else {
            map.put("war_period", vehicleTO.getWar_period());
            map.put("hourMeter", vehicleTO.getHmReading());
            map.put("kiloMeter", vehicleTO.getKmReading());
            map.put("dailyRunKm", vehicleTO.getDailyKm());
            map.put("dailyRunHm", vehicleTO.getDailyHm());
            map.put("gpsSystemId", vehicleTO.getGpsSystemId());
        }
        map.put("fservice", vehicleTO.getFservice());
        map.put("vehicleCost", vehicleTO.getVehicleCost());
        map.put("vehicleDepreciation", vehicleTO.getVehicleDepreciation());
        map.put("vehicleColor", vehicleTO.getVehicleColor());
//      map.put("ownership", vehicleTO.getLeasingCustId());
        map.put("leasingCustId", vehicleTO.getLeasingCustId());
        map.put("ownership", vehicleTO.getAsset());
        map.put("typeId", vehicleTO.getTypeId());
        map.put("vendorId", vehicleTO.getVendorId());
        ownerS = vehicleTO.getAsset();
        if (ownerS.equalsIgnoreCase("Own")) {
            map.put("groupCode", "ACC-0003");
        } else {
            map.put("groupCode", "");
        }
        map.put("gpsSystem", vehicleTO.getGpsSystem());
        map.put("warrantyDate", vehicleTO.getWarrantyDate());
        map.put("axles", vehicleTO.getAxles());
        map.put("ownerShips", vehicleTO.getOwnerShips());
        System.out.println("map first:::::: = " + map);
        int status = 0;
        String code = "";
        String[] temp;
        int vehicle_id = 0;
        int currentYear = 0;
        String newYear = "";
        int VehicleLedgerId = 0;
        String trailerNo = "";
        int serialNo = 0;
        try {
            String catId = vehicleTO.getCatId();
            System.out.println("catId = " + catId);
            if ("1".equals(catId)) {
                serialNo = (Integer) getSqlMapClientTemplate().insert("mfr.insertAddFlatBedTrailerNo", map);
                trailerNo = ThrottleConstants.tahoosFlatBedTrailerNoPrefix + serialNo;
            }
            if ("2".equals(catId)) {
                serialNo = (Integer) getSqlMapClientTemplate().insert("mfr.insertAddLowBedTrailerNo", map);
                trailerNo = ThrottleConstants.tahoosLowBedTrailerNoPrefix + serialNo;
            }
            if ("3".equals(catId)) {
                serialNo = (Integer) getSqlMapClientTemplate().insert("mfr.insertAddWireRugTrailerNo", map);
                trailerNo = ThrottleConstants.tahoosWireRugTrailerNoPrefix + serialNo;
            }
            if ("4".equals(catId)) {
                serialNo = (Integer) getSqlMapClientTemplate().insert("mfr.insertAddWaterTankTrailerNo", map);
                trailerNo = ThrottleConstants.tahoosWaterTankTrailerNoPrefix + serialNo;
            }
            if ("5".equals(catId)) {
                serialNo = (Integer) getSqlMapClientTemplate().insert("mfr.insertAddDieselTankTrailerNo", map);
                trailerNo = ThrottleConstants.tahoosDieselTankTrailerNoPrefix + serialNo;
            }
            if ("6".equals(catId)) {
                serialNo = (Integer) getSqlMapClientTemplate().insert("mfr.insertAddHydrolicLowBedTrailerNo", map);
                trailerNo = ThrottleConstants.tahoosHydrolicLowBedTrailerNoPrefix + serialNo;
            }

            System.out.println("trailerNo = " + trailerNo);
            map.put("trailerNo", trailerNo);
            map.put("regNo", trailerNo);

            vehicle_id = (Integer) session.insert("mfr.insertTrailer", map);
            System.out.println("vehicleId = " + vehicle_id);

            //Ledger Code start
            if (vehicle_id != 0) {
                map.put("trailerId", vehicle_id);
                status = (Integer) getSqlMapClientTemplate().update("mfr.insertTrailerLastKm", map);
                code = (String) getSqlMapClientTemplate().queryForObject("vendor.getLedgerCode", map);
                temp = code.split("-");
                int codeval = Integer.parseInt(temp[1]);
                int codev = codeval + 1;
                String ledgercode = "LEDGER-" + codev;
                map.put("ledgercode", ledgercode);

                //current year and month start
                String accYear = (String) getSqlMapClientTemplate().queryForObject("vendor.accYearVal", map);
                System.out.println("accYear:" + accYear);
                map.put("accYear", ThrottleConstants.financialYear);
                //current year end

                map.put("groupCode", ThrottleConstants.fleetGroupCode);
                map.put("levelId", ThrottleConstants.fleetLevelId);

                VehicleLedgerId = (Integer) getSqlMapClientTemplate().insert("mfr.insertVehicleLedger", map);
                System.out.println("VehicleLedgerId......." + VehicleLedgerId);
                if (VehicleLedgerId != 0) {
                    map.put("ledgerId", VehicleLedgerId);
                    map.put("vehicle_id", vehicle_id);
                    System.out.println("map update ::::=> " + map);
                    status = (Integer) getSqlMapClientTemplate().update("mfr.updateTrailerMaster", map);
                }

            }
            //Ledger Code end

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */

            FPLogUtils.fpDebugLog("insertTrailer Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "insertTrailer", sqlException);
        }
        return vehicle_id + "~" + trailerNo;
    }

    public int doInsertTrailerClass(VehicleTO vehicleTO, int UserId, SqlMapClient session) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("trailerId", vehicleTO.getVehicleId());
        map.put("classId", vehicleTO.getClassId());
        map.put("userId", UserId);
        int status = 0;
        int insertStatus = 0;
        try {
            status = (Integer) session.queryForObject("mfr.checkTrailerClass", map);
            if (status == 0) {
                System.out.println("i m in if...");
                insertStatus = (Integer) session.update("mfr.insertTrailerClass", map);
            } else {
                System.out.println("i m in else...");
                insertStatus = (Integer) session.update("mfr.updateTrailerClass", map);
                insertStatus = (Integer) session.update("mfr.insertTrailerClass", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return insertStatus;
    }

    public int doInsertTrailerEngine(VehicleTO vehicleTO, int UserId, SqlMapClient session) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("trailerId", vehicleTO.getVehicleId());
        map.put("engineNo", vehicleTO.getEngineNo());
        map.put("userId", UserId);

        int status = 0;
        int insertStatus = 0;
        try {
            status = (Integer) session.queryForObject("mfr.checkTrailerEngine", map);
            if (status == 0) {
                insertStatus = (Integer) session.update("mfr.insertTrailerEngine", map);
            } else {
                insertStatus = (Integer) session.update("mfr.updateTrailerEngine", map);
                insertStatus = (Integer) session.update("mfr.insertTrailerEngine", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doInsertTrailerEngine Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "doInsertTrailerEngine", sqlException);
        }
        return insertStatus;
    }

    public int doInsertTrailerOper(VehicleTO vehicleTO, int UserId, SqlMapClient session) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("trailerId", vehicleTO.getVehicleId());
        map.put("operationId", vehicleTO.getOpId());
        map.put("userId", UserId);

        int status = 0;
        int insertStatus = 0;
        try {
            status = (Integer) session.queryForObject("mfr.checkTrailerOperation", map);
            if (status == 0) {
                insertStatus = (Integer) session.update("mfr.insertTrailerOperation", map);
            } else {
                insertStatus = (Integer) session.update("mfr.updateTrailerOperation", map);
                insertStatus = (Integer) session.update("mfr.insertTrailerOperation", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doInsertTrailerOper Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "doInsertTrailerOper", sqlException);
        }
        return insertStatus;
    }

    public int doInsertTrailerFc(VehicleTO vehicleTO, int UserId, SqlMapClient session) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("trailerId", vehicleTO.getVehicleId());
        map.put("fcDate", vehicleTO.getNextFCDate());
        map.put("userId", UserId);

        int status = 0;
        int insertStatus = 0;
        try {
            status = (Integer) session.queryForObject("mfr.checkTrailerFc", map);
            if (status == 0) {
                insertStatus = (Integer) session.update("mfr.insertTrailerFc", map);
            } else {
                insertStatus = (Integer) session.update("mfr.updateTrailerFc", map);
                insertStatus = (Integer) session.update("mfr.insertTrailerFc", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doInsertTrailerFc Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "doInsertTrailerFc", sqlException);
        }
        return insertStatus;
    }

    public int doInsertTrailerReg(VehicleTO vehicleTO, int UserId, SqlMapClient session) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("trailerId", vehicleTO.getVehicleId());
        map.put("regNo", vehicleTO.getRegNo());
        map.put("userId", UserId);
        map.put("typeId", vehicleTO.getTypeId());
        System.out.println("map for regNo:" + map);
        int status = 0;
        int insertStatus = 0;
        try {
            status = (Integer) session.queryForObject("mfr.checkTrailerReg", map);
            if (status == 0) {
                insertStatus = (Integer) session.update("mfr.insertTrailerReg", map);
            } else {
                insertStatus = (Integer) session.update("mfr.updateTrailerReg", map);
                System.out.println("update reg:" + insertStatus);
                insertStatus = (Integer) session.update("mfr.insertTrailerReg", map);
                System.out.println("insert reg:" + insertStatus);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doInsertTrailerReg Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "doInsertTrailerReg", sqlException);
        }
        return insertStatus;
    }

    public int doInsertTrailerUsage(VehicleTO vehicleTO, int UserId, SqlMapClient session) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("trailerId", vehicleTO.getVehicleId());
        map.put("usageTypeId", vehicleTO.getUsageId());
        map.put("userId", UserId);

        int status = 0;
        int insertStatus = 0;
        try {
            status = (Integer) session.queryForObject("mfr.checkTrailerUsageType", map);
            if (status == 0) {
                insertStatus = (Integer) session.update("mfr.insertTrailerUsageType", map);
            } else {
                insertStatus = (Integer) session.update("mfr.updateTrailerUsageType", map);
                insertStatus = (Integer) session.update("mfr.insertTrailerUsageType", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doInsertTrailerUsage Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "doInsertTrailerUsage", sqlException);
        }
        return insertStatus;
    }

    public int doInsertTrailerCust(VehicleTO vehicleTO, int UserId, SqlMapClient session) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("trailerId", vehicleTO.getVehicleId());
        map.put("custId", vehicleTO.getCustId());
        map.put("userId", UserId);

        int status = 0;
        int insertStatus = 0;
        try {
            status = (Integer) session.queryForObject("mfr.checkTrailerCust", map);
            if (status == 0) {
                insertStatus = (Integer) session.update("mfr.insertTrailerCust", map);
            } else {
                insertStatus = (Integer) session.update("mfr.updateTrailerCust", map);
                insertStatus = (Integer) session.update("mfr.insertTrailerCust", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doInsertTrailerCust Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "doInsertTrailerCust", sqlException);
        }
        return insertStatus;
    }

    public int doInsertTrailerHist(VehicleTO vehicleTO, int UserId, SqlMapClient session) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("trailerId", vehicleTO.getVehicleId());
        if ("".equals(vehicleTO.getDailyKm())) {
            map.put("km", 0);
        } else {
            map.put("km", vehicleTO.getDailyKm());
        }
        if ("".equals(vehicleTO.getDailyHm())) {
            map.put("hm", 0);
        } else {
            map.put("hm", vehicleTO.getDailyHm());
        }
        map.put("userId", UserId);
        System.out.println("map..." + map);
        int status = 0;
        int insertStatus = 0;
        try {
            status = (Integer) session.queryForObject("mfr.checkTrailerHistory", map);
            if (status == 0) {
                insertStatus = (Integer) session.update("mfr.insertTrailerHistory", map);
            } else {
                insertStatus = (Integer) session.update("mfr.updateTrailerHistory", map);
                insertStatus = (Integer) session.update("mfr.insertTrailerHistory", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doInsertTrailerHist Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "doInsertTrailerHist", sqlException);
        }
        return insertStatus;
    }

    public int trailerTyreDetails(VehicleTO vehicleTO, int vehicleId, int userId, int madhavaramSp) {
        Map map = new HashMap();
        int axleId = 0;
        int axleDetails = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        try {
            map.put("axleId", vehicleTO.getAxleTypeId());
            map.put("trailerId", vehicleId);
            map.put("userId", userId);
            map.put("companyId", madhavaramSp);
            System.out.println("map Axle 2 Throttle Here  = " + map);
            ArrayList axleList = new ArrayList();
            axleList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehAxleName", map);
            Iterator itr = axleList.iterator();
            VehicleTO vehTO = new VehicleTO();
            int left = 0;
            int right = 0;
            while (itr.hasNext()) {
                vehTO = (VehicleTO) itr.next();
                left = Integer.parseInt(vehTO.getAxleLeft());
                map.put("axleTypeName", vehTO.getAxleTypeName());
                map.put("axleDetailId", vehTO.getVehicleAxleId());
                for (int i = 0; i < left; i++) {
                    map.put("positionName", "Left");
                    map.put("positionNo", i + 1);
                    axleDetails = (Integer) getSqlMapClientTemplate().update("mfr.insertTrailerTyreDetails", map);
                }
                right = Integer.parseInt(vehTO.getAxleRight());
                for (int j = 0; j < right; j++) {
                    map.put("positionName", "Right");
                    map.put("positionNo", j + 1);
                    axleDetails = (Integer) getSqlMapClientTemplate().update("mfr.insertTrailerTyreDetails", map);
                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("trailerTyreDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "trailerTyreDetails", sqlException);
        }
        return axleDetails;
    }

    public int doAlterTrailer(VehicleTO vehicleTO, int UserId, SqlMapClient session) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", UserId);
        map.put("regNo", vehicleTO.getRegNo());
        map.put("mfrId", vehicleTO.getMfrId());
        map.put("groupId", vehicleTO.getGroupId());
        map.put("modelId", vehicleTO.getModelId());
        map.put("dateOfSale", vehicleTO.getDateOfSale());
        map.put("chassisNo", vehicleTO.getChassisNo());
        map.put("war_period", vehicleTO.getWar_period());
        map.put("seatCapacity", vehicleTO.getSeatCapacity());
        map.put("vehicleCost", vehicleTO.getVehicleCost());
        map.put("currentKm", vehicleTO.getKmReading());
        map.put("currentHm", vehicleTO.getHmReading());
        map.put("description", vehicleTO.getDescription());
        map.put("activeInd", vehicleTO.getActiveInd());
        map.put("trailerId", vehicleTO.getVehicleId());
        map.put("remarks", vehicleTO.getDescription());
        map.put("fservice", vehicleTO.getFservice());
        map.put("gpsSystem", vehicleTO.getGpsSystem());
        map.put("typeId", vehicleTO.getTypeId());
        map.put("ownership", vehicleTO.getOwnerShips());

        System.out.println("vehicleTO.getLeasingCustId() ============== " + vehicleTO.getLeasingCustId());
        if (vehicleTO.getOwnership().equalsIgnoreCase("1")) {
            map.put("VendorId", "");
        } else {
            System.out.println("else");
            map.put("VendorId", vehicleTO.getLeasingCustId());

        }

        System.out.println("map uuuuuuuuuuuuuuu => " + map);
        int status = 0;
        try {

            status = (Integer) session.update("mfr.updateTrailerDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */

            FPLogUtils.fpDebugLog("updateTrailerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "updateTrailerDetails", sqlException);
        }
        return Integer.parseInt(vehicleTO.getVehicleId());
    }

    public ArrayList vendorList(String vendorTypeId) {
        System.out.println("in DAO  getting vendor name for popup");
        Map map = new HashMap();
        ArrayList vendorList = new ArrayList();

        try {

            map.put("vendorTypeId", vendorTypeId);
            System.out.println("the vendorTypeId is===>" + vendorTypeId);
            System.out.println("map:" + map);
            vendorList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.vendorList", map);
            System.out.println("vendorList size:" + vendorList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vendorList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "vendorList", sqlException);
        }
        return vendorList;
    }

    public ArrayList vendorListCompliance(String vendorTypeIdCompliance) {
        System.out.println("in DAO  getting vendor name for popup");
        Map map = new HashMap();
        ArrayList vendorListCompliance = new ArrayList();

        try {

            map.put("vendorTypeId", vendorTypeIdCompliance);
            System.out.println("the vendorTypeId is===>" + vendorTypeIdCompliance);
            System.out.println("map:" + map);
            vendorListCompliance = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.vendorList", map);
            System.out.println("vendorList size:" + vendorListCompliance.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vendorList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "vendorList", sqlException);
        }
        return vendorListCompliance;
    }

    public ArrayList getTrailerAxleTyreNo(VehicleTO vehTO) {
        Map map = new HashMap();
        ArrayList insuranceDetails = new ArrayList();
        map.put("axleDetailId", vehTO.getAxleDetailId());
        map.put("positionName", vehTO.getPositionName());
        map.put("positionNo", vehTO.getPositionNo());
        map.put("trailerId", vehTO.getVehicleId());
        map.put("axleTypeId", vehTO.getAxleTypeId());
        System.out.println("map" + map);
        try {
            insuranceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getTrailerAxleTyreNo", map);
            System.out.println("insuranceDetails size:" + insuranceDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getInsuranceDetails", sqlException);
        }
        return insuranceDetails;

    }

    public int updateTrailerTyreDetails(VehicleTO vehicleTO, int userId, int madhavaramSp) {
        Map map = new HashMap();
        int axleId = 0;
        int axleDetails = 0;
        try {
            map.put("axleDetailId", vehicleTO.getAxleDetailId());
            map.put("positionName", vehicleTO.getPositionName());
            map.put("positionNo", vehicleTO.getPositionNo());
            map.put("updateValue", vehicleTO.getUpdateValue());
            map.put("itemId", vehicleTO.getUpdateValue());
            map.put("tyreNo", vehicleTO.getTyreNo());
            map.put("trailerId", vehicleTO.getVehicleId());
            map.put("depthVal", vehicleTO.getDepthVal());
            map.put("companyId", madhavaramSp);
            System.out.println("map Axle 2 = " + map);

            int tyreId = 0;
            Object tyreIdStr = getSqlMapClientTemplate().queryForObject("mfr.checkTyreNo", map);
            if (tyreIdStr != null) {
                tyreId = (Integer) tyreIdStr;
                System.out.println("tyreId:111111::" + tyreId);
            } else {
                tyreId = (Integer) getSqlMapClientTemplate().update("mfr.insertVehicleTyre", map);
                System.out.println("tyreId:222222::" + tyreId);
            }

            map.put("tyreId", tyreId);
            // update tyre no
            int checkTyreStatus = 0;
            checkTyreStatus = (Integer) getSqlMapClientTemplate().queryForObject("mfr.checkTrailerTyreNoStatus", map);
            System.out.println("checkTyreStatus" + checkTyreStatus);
            if (checkTyreStatus == 0) {
                axleDetails = (Integer) getSqlMapClientTemplate().update("mfr.updateTrailerTyreMfr", map);
            } else if (vehicleTO.getUpdateType().equals("0")) {
                // update tyre mfr
                axleDetails = (Integer) getSqlMapClientTemplate().update("mfr.updateTrailerTyreMfr", map);
                System.out.println("axleDetails" + axleDetails);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("updateVehicleTyreDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "updateVehicleTyreDetails", sqlException);
        }
        return axleDetails;
    }

    public int insertTrailerOemDetails(VehicleTO vehicleTO, String oemDetailsId, String oemMfr, String oemTyreNo, String oemDepth, String oemBattery, String oemId, int userId) {
        Map map = new HashMap();
        int oemId1 = 0;

        try {

            map.put("userId", userId);
            map.put("trailerId", vehicleTO.getVehicleId());
            map.put("oemDetailsId", oemDetailsId);
            map.put("oemMfr", oemMfr);
            map.put("oemTyreNo", oemTyreNo);
            map.put("oemDepth", oemDepth);
            map.put("oemBattery", oemBattery);
            int oemInsert = Integer.parseInt(oemId);
            map.put("oemId", oemInsert);
            System.out.println("mapInsert" + map);
//            if(oemInsert > 0){
//            oemId1 = (Integer) getSqlMapClientTemplate().update("mfr.updateOemDetails", map);
//                System.out.println("oemId1daoupdate"+oemId1);
//            }else{
//            }
            oemId1 = (Integer) getSqlMapClientTemplate().insert("mfr.insertTrailerOemDetails", map);
            System.out.println("oemId" + oemId1);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveFileUploadDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveFileUploadDetails List", sqlException);
        }

        return oemId1;
    }

    public ArrayList getTrailerOemList(VehicleTO vehicleTO) {

        Map map = new HashMap();
        ArrayList oemList = new ArrayList();
        map.put("vehicleId", vehicleTO.getVehicleId());
        System.out.println("mapdfedff:" + map);

        try {
            oemList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getTrailerOemList", map);
            System.out.println("oemListdao" + oemList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTrailerOemList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getTrailerOemList", sqlException);
        }
        return oemList;

    }

    public ArrayList getTrailerDetails(String trailerId) {

        Map map = new HashMap();
        ArrayList vehiclesDetails = new ArrayList();
        map.put("trailerId", trailerId);
        System.out.println("map:" + map);

        try {
            vehiclesDetails = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getTrailerDetails", map);
            System.out.println("vehiclesDetails" + vehiclesDetails);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Vehicledetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "Vehicledetails", sqlException);
        }
        return vehiclesDetails;

    }

    public ArrayList getPrimarybankList() {
        Map map = new HashMap();
        ArrayList primarybankList = new ArrayList();
        try {
            primarybankList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getPrimaryBankList", map);
            //////System.out.println("getBankPaymentList = " + bankPaymentList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("primarybankList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "primarybankList", sqlException);
        }
        return primarybankList;
    }

    public ArrayList getVehiclesRegistrationList(VehicleTO vehicleTO) {

        Map map = new HashMap();
        ArrayList vehiclesRegistrationList = new ArrayList();
        map.put("regNo", vehicleTO.getRegNo());
        map.put("companyId", vehicleTO.getVendorId());
        map.put("fromDate", vehicleTO.getFromDate());
        map.put("toDate", vehicleTO.getToDate());
        System.out.println("vehiclesRegistrationList map:" + map);

        try {
            vehiclesRegistrationList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getVehiclesRegistrationList", map);
            System.out.println("vehiclesRegistrationList" + vehiclesRegistrationList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehiclesList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "vehiclesList", sqlException);
        }
        return vehiclesRegistrationList;

    }

    public ArrayList getvehicleUploadsDetails(VehicleTO vehicleTO) {
        Map map = new HashMap();

        ArrayList vehicleUploadsDetails = new ArrayList();
        try {
            map.put("vehicleId", vehicleTO.getVehicleId());
            System.out.println("getvehicleUploadsDetails = " + map);
            vehicleUploadsDetails = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getvehicleUploadsDetails", map);
            System.out.println("vehicleUploadsDetails dao size :" + vehicleUploadsDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getvehicleUploadsDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "customerContractUploadsDetails", sqlException);
        }
        return vehicleUploadsDetails;

    }

    public ArrayList getDisplayLogoBlobData(String uploadId) {
        Map map = new HashMap();
        ArrayList profileList = new ArrayList();

        try {

            map.put("uploadId", uploadId);
            profileList = (ArrayList) getSqlMapClientTemplate().queryForList("mfr.getDisplayLogoBlobData", map);
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getDisplayLogoBlobData Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getDisplayLogoBlobData", sqlException);
        }
        return profileList;
    }

    public int saveRegistrationDetails(VehicleTO vehicleTO, int userId, int madhavaramSp, SqlMapClient session) {
        System.out.println("saveRegistrationDetails entered BP");
        Map map = new HashMap();

        int registrationId = 0;
        String registration = "vehicle Registration";
        try {

            map.put("truckNo", vehicleTO.getTruckNo());
            map.put("transportionType", vehicleTO.getTransportionType());
            map.put("ownerName", vehicleTO.getOwnerName());
            map.put("ownerId", vehicleTO.getOwnerId());
            map.put("dateOfReg", vehicleTO.getDateOfReg());
            map.put("noOfPersons", vehicleTO.getNoOfPersons());
            map.put("color", vehicleTO.getColor());
            map.put("vehicleId", vehicleTO.getVehicleId());
            map.put("companyId", madhavaramSp);
            map.put("userId", userId);
            registrationId = (Integer) session.insert("mfr.insertRegistration", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("updateVehicleTyreDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "updateVehicleTyreDetails", sqlException);
        }
        return registrationId;
    }
}
