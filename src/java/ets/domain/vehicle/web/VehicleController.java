/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.vehicle.web;

import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.Part;
import ets.arch.business.PaginationHelper;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.web.BaseController;
import ets.domain.company.business.CompanyTO;
import ets.domain.company.business.CompanyBP;
import ets.domain.operation.business.OperationBP;
import ets.domain.users.business.LoginBP;
import ets.domain.customer.business.CustomerBP;
import ets.domain.finance.business.FinanceBP;
import ets.domain.finance.business.FinanceTO;
import ets.domain.mrs.business.MrsBP;

import ets.domain.util.ParveenErrorConstants;
import ets.domain.util.FPLogUtils;
import ets.domain.util.ThrottleConstants;
import ets.domain.vehicle.business.VehicleBP;
import ets.domain.vehicle.business.VehicleTO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import ets.domain.vendor.business.VendorTO;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.UnknownHostException;
import java.util.List;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONArray;
import org.json.JSONObject;

public class VehicleController extends BaseController {

    /**
     * Creates a new instance of __NAME__
     */
    public VehicleController() {
    }
    VehicleCommand vehicleCommand;
    VehicleBP vehicleBP;
    LoginBP loginBP;
    CustomerBP customerBP;
    MrsBP mrsBP;
    OperationBP operationBP;
    FinanceBP financeBP;

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public VehicleBP getVehicleBP() {
        return vehicleBP;
    }

    public void setVehicleBP(VehicleBP vehicleBP) {
        this.vehicleBP = vehicleBP;
    }

    public CustomerBP getCustomerBP() {
        return customerBP;
    }

    public void setCustomerBP(CustomerBP customerBP) {
        this.customerBP = customerBP;
    }

    public MrsBP getMrsBP() {
        return mrsBP;
    }

    public void setMrsBP(MrsBP mrsBP) {
        this.mrsBP = mrsBP;
    }

    public OperationBP getOperationBP() {
        return operationBP;
    }

    public void setOperationBP(OperationBP operationBP) {
        this.operationBP = operationBP;
    }

    public FinanceBP getFinanceBP() {
        return financeBP;
    }

    public void setFinanceBP(FinanceBP financeBP) {
        this.financeBP = financeBP;
    }

    /**
     * This method used to bind the request values to the command object.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();
        initialize(request);

    }

    /**
     * This method used to View MFR Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleViewMfrPage(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String menuPath = "Vehicle  >>  MFR >>  View MFR";

        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Mfr-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "content/MFR/addMfr.jsp";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String fleetTypeId = request.getParameter("fleetType");
                String pageTitle = "View Manufacturers";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList MfrList = new ArrayList();
                VehicleTO vehicleTO = new VehicleTO();
                vehicleTO.setFleetTypeId(fleetTypeId);
                MfrList = vehicleBP.getMfrList(vehicleTO);
                request.setAttribute("MfrList", MfrList);
                path = "content/MFR/addMfr.jsp";

            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to Add MFR Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAddMfrPage(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        path = "content/MFR/addMfr.jsp";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Mfr-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                vehicleCommand = command;
                String menuPath = "Vehicle  >>  MFR >>  Add";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "Add Manufacturer";
                request.setAttribute("pageTitle", pageTitle);

                ArrayList MfrList = new ArrayList();
                MfrList = vehicleBP.processGetMfrList();
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                request.setAttribute("MfrList", MfrList);
                request.setAttribute("pageTitle", pageTitle);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to Insert MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAddMfr(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        try {
            String menuPath = "Vehicle  >>  MFR >>  View MFR";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();

            if (vehicleCommand.getMfrName() != null && vehicleCommand.getMfrName() != "") {
                vehicleTO.setMfrName(vehicleCommand.getMfrName());
            }
            if (vehicleCommand.getDescription() != null && vehicleCommand.getDescription() != "") {
                vehicleTO.setDescription(vehicleCommand.getDescription());
            }
            if (vehicleCommand.getPerc() != null && vehicleCommand.getPerc() != "") {
                vehicleTO.setPercentage(vehicleCommand.getPerc());
            }
            if (vehicleCommand.getGroupId() != null && vehicleCommand.getGroupId() != "") {
                vehicleTO.setActivityGroupId(vehicleCommand.getGroupId());
            }
            if (vehicleCommand.getBodyGroupId() != null && vehicleCommand.getBodyGroupId() != "") {
                vehicleTO.setBodyWorkGroupId(vehicleCommand.getBodyGroupId());
            }
            if (vehicleCommand.getVehicleMfr() != null && vehicleCommand.getVehicleMfr() != "") {
                vehicleTO.setVehicleMfr(vehicleCommand.getVehicleMfr());
            }
            if (vehicleCommand.getTyreMfr() != null && vehicleCommand.getTyreMfr() != "") {
                vehicleTO.setTyreMfr(vehicleCommand.getTyreMfr());
            }
            if (vehicleCommand.getTrailerMfr() != null && vehicleCommand.getTrailerMfr() != "") {
                vehicleTO.setTrailerMfr(vehicleCommand.getTrailerMfr());
            }
            if (vehicleCommand.getBatteryMfr() != null && vehicleCommand.getBatteryMfr() != "") {
                vehicleTO.setBatteryMfr(vehicleCommand.getBatteryMfr());
            }
            if (vehicleCommand.getMfrId() != null && vehicleCommand.getMfrId() != "") {
                vehicleTO.setMfrId(vehicleCommand.getMfrId());
            }

            vehicleTO.setStatus(request.getParameter("status"));
            String pageTitle = "View Manufacturers";
            request.setAttribute("pageTitle", pageTitle);

            int insertStatus = 0;
            path = "content/MFR/addMfr.jsp";

            insertStatus = vehicleBP.processInsertMfrDetails(vehicleTO, userId);
            request.removeAttribute("MfrList");
            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);

               request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Manufacturer Added Successfully");  
             //    }
          //   }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to View Alter MFR page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAlterMfrPage(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        path = "content/MFR/alterMfr.jsp";

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        try {

            if (!loginBP.checkAuthorisation(userFunctions, "Mfr-Alter")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                vehicleCommand = command;
                String menuPath = "Vehicle  >>  MFR >>  Alter";
                ArrayList MfrList = new ArrayList();
                MfrList = vehicleBP.processGetMfrList();
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "Alter Manufacturer";
                request.setAttribute("MfrList", MfrList);
                request.setAttribute("pageTitle", pageTitle);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to Modify MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAlterMfr(HttpServletRequest request, HttpServletResponse reponse, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        vehicleCommand = command;
        String path = "";

        try {
            String menuPath = "  Vehicle  >>  MFR >>  View MFR  ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-Modify")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            int index = 0;
            int modify = 0;
            String[] mfrids = vehicleCommand.getMfrIds();
            String[] mfrnames = vehicleCommand.getMfrNames();
            String[] groupIds = vehicleCommand.getGroupIds();
            String[] bodyGroupIds = vehicleCommand.getBodyGroupIds();
            String[] percentage = vehicleCommand.getPercentage();
            String[] activeStatus = vehicleCommand.getActiveInds();
            String[] selectedIndex = vehicleCommand.getSelectedIndex();
            String[] desciption = vehicleCommand.getDescriptions();
            int UserId = (Integer) session.getAttribute("userId");

            ArrayList List = new ArrayList();
            VehicleTO vehicleTO = null;
            for (int i = 0; i < selectedIndex.length; i++) {
                vehicleTO = new VehicleTO();
                index = Integer.parseInt(selectedIndex[i]);

                vehicleTO.setMfrId(mfrids[index]);
                vehicleTO.setMfrName(mfrnames[index]);
                vehicleTO.setActivityGroupId(groupIds[index]);
                vehicleTO.setBodyWorkGroupId(bodyGroupIds[index]);
                vehicleTO.setPercentage(percentage[index]);
                vehicleTO.setActiveInd(activeStatus[index]);
                vehicleTO.setDescription(desciption[index]);
                List.add(vehicleTO);
            }
            String pageTitle = "View Manufacturers";
            path = "content/MFR/manageMfr.jsp";
            request.setAttribute("pageTitle", pageTitle);
            modify = vehicleBP.processModifyMfrDetails(List, UserId);
            request.removeAttribute("MfrList");
            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Manufacturer Details Modified Successfully");
//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve designation --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //handleModelPage 
    /**
     * This method used to View MFR Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleModelPage(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String menuPath = "Vehicle  >>  Model >>  View Model";

        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Model-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "content/MFR/manageModel.jsp";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

                String pageTitle = "View Manufacturers";
                request.setAttribute("pageTitle", pageTitle);
                String fleetTypeId = request.getParameter("fleetType");
                VehicleTO vehicleTO = new VehicleTO();
                vehicleTO.setFleetTypeId(fleetTypeId);
                ArrayList ModelList = new ArrayList();
                ModelList = vehicleBP.processActiveMfrList();
                request.setAttribute("MfrList", ModelList);
                ArrayList TypeList = new ArrayList();
                TypeList = vehicleBP.processActiveTypeList(vehicleTO);
                request.setAttribute("TypeList", TypeList);
                ArrayList FuelList = new ArrayList();
                FuelList = vehicleBP.processGetFuelList();
                request.setAttribute("FuelList", FuelList);
                ModelList = vehicleBP.processGetModelList(vehicleTO);
                request.setAttribute("ModelList", ModelList);
                request.setAttribute("fleetTypeId", fleetTypeId);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method caters to view add lecture page
     *
     * @param request - Http request object
     *
     * @param response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public ModelAndView handleModeladdPage(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String menuPath = "HRMS  >>  Manage Model  >>  Add";

        String pageTitle = "Add Model";
        request.setAttribute("pageTitle", pageTitle);
        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Model-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                path = "content/MFR/addModel.jsp";
                VehicleTO vehicleTO = new VehicleTO();
                String fleetTypeId = request.getParameter("fleetTypeId");
                vehicleTO.setFleetTypeId(fleetTypeId);
                request.setAttribute("fleetTypeId", fleetTypeId);
                ArrayList ModelList = new ArrayList();
                ModelList = vehicleBP.getMfrList(vehicleTO);
                request.setAttribute("MfrList", ModelList);
                ArrayList TypeList = new ArrayList();
                TypeList = vehicleBP.processActiveTypeList(vehicleTO);
                request.setAttribute("TypeList", TypeList);

                ArrayList FuelList = new ArrayList();
                FuelList = vehicleBP.processGetFuelList();
                request.setAttribute("FuelList", FuelList);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to add lecture data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    //handleAddModel
    /**
     * This method caters to add lecture
     *
     * @param request - Http request object
     *
     * @param response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public ModelAndView handleAddModel(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        String pageTitle = "View Model";
        request.setAttribute("pageTitle", pageTitle);
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String menuPath = "";
        menuPath = "MFR  >>  Manage Model >> Add  ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {

            VehicleTO vehicleTO = new VehicleTO();

            if (vehicleCommand.getMfrId() != null && vehicleCommand.getMfrId() != "") {
                vehicleTO.setMfrId(vehicleCommand.getMfrId());

            }
            if (vehicleCommand.getModelName() != null && vehicleCommand.getModelName() != "") {
                vehicleTO.setModelName(vehicleCommand.getModelName());

            }
            if (vehicleCommand.getTypeId() != null && vehicleCommand.getTypeId() != "") {
                vehicleTO.setTypeId(vehicleCommand.getTypeId());

            }
            if (vehicleCommand.getFuelId() != null && vehicleCommand.getFuelId() != "") {
                vehicleTO.setFuelId(vehicleCommand.getFuelId());

            }
            if (vehicleCommand.getDescription() != null && vehicleCommand.getDescription() != "") {
                vehicleTO.setDescription(vehicleCommand.getDescription());

            }

            request.setAttribute("pageTitle", pageTitle);

            int insertStatus = 0;
            path = "content/MFR/manageModel.jsp";

            insertStatus = vehicleBP.insertModelDetails(vehicleTO, userId);
            request.removeAttribute("ModelList");
            ArrayList ModelList = new ArrayList();
            String fleetTypeId = request.getParameter("fleetTypeId");
            vehicleTO.setFleetTypeId(fleetTypeId);
            ModelList = vehicleBP.processGetModelList(vehicleTO);
            request.setAttribute("ModelList", ModelList);
            request.setAttribute("fleetTypeId", fleetTypeId);
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Model Added Successfully");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //handleAlterModelPage
    /**
     * l
     * This method used to View Alter MFR page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAlterModelPage(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        path = "content/MFR/alterModel.jsp";

        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Model-Alter")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                vehicleCommand = command;
                String menuPath = "Vehicle  >>  MODEL >>  Alter";
                VehicleTO vehicleTO = new VehicleTO();
                String fleetTypeId = request.getParameter("fleetTypeId");
                vehicleTO.setFleetTypeId(fleetTypeId);
                ArrayList modelList = new ArrayList();
                modelList = vehicleBP.processGetModelList(vehicleTO);
                request.setAttribute("ModelList", modelList);
                ArrayList MfrList = new ArrayList();
                MfrList = vehicleBP.getMfrList(vehicleTO);
                request.setAttribute("MfrList", MfrList);

                ArrayList TypeList = new ArrayList();
                TypeList = vehicleBP.getTypeList(vehicleTO);
                request.setAttribute("TypeList", TypeList);

                ArrayList FuelList = new ArrayList();
                FuelList = vehicleBP.processGetFuelList();
                request.setAttribute("FuelList", FuelList);

                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "Alter Model";
                request.setAttribute("fleetTypeId", fleetTypeId);
                request.setAttribute("pageTitle", pageTitle);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /* vehicle type  */
    /**
     * This method used to View MFR Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleViewTypePage(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String menuPath = "Vehicle  >>  Vehicle >>  View Type";

        try {
            if (!loginBP.checkAuthorisation(userFunctions, "VehicleType-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "content/MFR/manageVehicleType.jsp";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

                String pageTitle = "View Vehicle Type";
                request.setAttribute("pageTitle", pageTitle);

                ArrayList TypeList = new ArrayList();

                TypeList = vehicleBP.processGetTypeList();

                request.setAttribute("TypeList", TypeList);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Vehicle Type  data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //handleViewTypeAddPage
    /**
     * ''' This method used to Add MFR Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleViewTypeAddPage(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        try {
            if (!loginBP.checkAuthorisation(userFunctions, "VehicleType-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "content/MFR/addVehicleType.jsp";
                String pageTitle = "Add Vehicle Type";
                request.setAttribute("pageTitle", pageTitle);
                String menuPath = "Vehicle  >>  Vehicle Type >>  Add";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

                ArrayList TypeList = new ArrayList();
                TypeList = vehicleBP.processGetTypeList();
                request.setAttribute("TypeList", TypeList);
                ArrayList AxleList = new ArrayList();
                AxleList = vehicleBP.GetAxleList();
                System.out.print("AxleList-----cgbb" + AxleList.size());
                request.setAttribute("AxleList", AxleList);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Vehicle Type  data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //handleVehicleTypeAdd
    /**
     * This method used to Insert MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleVehicleTypeAdd(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        try {
            String menuPath = "Vehicle  >>  Vehicle Type >>  View  ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();

            if (vehicleCommand.getTypeName() != null && vehicleCommand.getTypeName() != "") {
                vehicleTO.setTypeName(vehicleCommand.getTypeName());

            }
            if (vehicleCommand.getTonnage() != null && vehicleCommand.getTonnage() != "") {
                vehicleTO.setTonnage(vehicleCommand.getTonnage());

            }
            if (vehicleCommand.getCapacity() != null && vehicleCommand.getCapacity() != "") {
                vehicleTO.setCapacity(vehicleCommand.getCapacity());

            }
            if (vehicleCommand.getDescription() != null && vehicleCommand.getDescription() != "") {
                vehicleTO.setDescription(vehicleCommand.getDescription());

            }
            if (vehicleCommand.getAxleTypeId() != null && vehicleCommand.getAxleTypeId() != "") {
                vehicleTO.setAxleTypeId(vehicleCommand.getAxleTypeId());

            }

            String pageTitle = "View VehicleType";
            request.setAttribute("pageTitle", pageTitle);

            int insertStatus = 0;
            path = "content/MFR/manageVehicleType.jsp";

            insertStatus = vehicleBP.processInsertTypeDetails(vehicleTO, userId);
            System.out.println("insertStatus types= " + insertStatus);
            request.removeAttribute("TypeList");
            ArrayList TypeList = new ArrayList();
            TypeList = vehicleBP.processGetTypeList();
            request.setAttribute("TypeList", TypeList);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Type Added Successfully");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //handleViewTypeAlterPage
    /**
     * This method used to View Alter MFR page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleViewTypeAlterPage(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        path = "content/MFR/alterTypePage.jsp";

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        try {
            if (!loginBP.checkAuthorisation(userFunctions, "VehicleType-Alter")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                vehicleCommand = command;
                String menuPath = "Vehicle  >>  Vehicle Type >>  Alter";
                ArrayList TypeList = new ArrayList();
                TypeList = vehicleBP.processGetTypeList();
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "Alter Vehicle type";
                request.setAttribute("TypeList", TypeList);
                request.setAttribute("pageTitle", pageTitle);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

//handleAlterType 
    /**
     * This method used to Modify MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAlterType(HttpServletRequest request, HttpServletResponse reponse, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        vehicleCommand = command;
        String path = "";

        try {
            String menuPath = "  Vehicle  >> Vehicle >>  View Type  ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-Modify")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            int index = 0;
            int modify = 0;
            String[] typeids = vehicleCommand.getTypeIds();
            String[] typenames = vehicleCommand.getTypeNames();
            String[] tonnages = vehicleCommand.getTonnages();
            String[] capacities = vehicleCommand.getCapacities();
            String[] activeStatus = vehicleCommand.getActiveInds();
            String[] selectedIndex = vehicleCommand.getSelectedIndex();
            String[] desciption = vehicleCommand.getDescriptions();
            int UserId = (Integer) session.getAttribute("userId");

            ArrayList List = new ArrayList();
            VehicleTO vehicleTO = null;
            for (int i = 0; i < selectedIndex.length; i++) {
                vehicleTO = new VehicleTO();
                index = Integer.parseInt(selectedIndex[i]);

                vehicleTO.setTypeId(typeids[index]);
                vehicleTO.setTypeName(typenames[index]);
                vehicleTO.setTonnage(tonnages[index]);
                vehicleTO.setCapacity(capacities[index]);
                vehicleTO.setActiveInd(activeStatus[index]);
                vehicleTO.setDescription(desciption[index]);
                List.add(vehicleTO);
            }
            String pageTitle = "View Vehicle Type";
            path = "content/MFR/manageVehicleType.jsp";
            request.setAttribute("pageTitle", pageTitle);
            modify = vehicleBP.processModifyTypeDetails(List, UserId);
            System.out.println("modify the type= " + modify);
            request.removeAttribute("TypeList");
            ArrayList TypeList = new ArrayList();
            TypeList = vehicleBP.processGetTypeList();
            request.setAttribute("TypeList", TypeList);
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "vehicle Details Modified Successfully");
//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve designation --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //handleAlterModel  
    /**
     * This method used to Modify MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAlterModel(HttpServletRequest request, HttpServletResponse reponse, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        ModelAndView mv = new ModelAndView();
        HttpSession session = request.getSession();
        vehicleCommand = command;
        String path = "";
        String fleetTypeId = request.getParameter("fleetTypeId");
        try {
            String menuPath = "  Vehicle  >>  VEHICLE >>  View    ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-Modify")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            int index = 0;
            int modify = 0;
            System.out.println("1");
            String[] mfrids = vehicleCommand.getMfrIds();
            String[] modelNames = vehicleCommand.getModelNames();
            String[] typeIds = vehicleCommand.getTypeIds();
            String[] fuelIds = vehicleCommand.getFuelIds();
            String[] modelId = vehicleCommand.getModelIds();
            String[] desciption = vehicleCommand.getDescriptions();
            String[] activeStatus = vehicleCommand.getActiveInds();
            String[] selectedIndex = vehicleCommand.getSelectedIndex();

            System.out.println("2");
            int UserId = (Integer) session.getAttribute("userId");

            System.out.println("3");
            ArrayList List = new ArrayList();
            VehicleTO vehicleTO = null;
            for (int i = 0; i < selectedIndex.length; i++) {
                System.out.println("4");
                vehicleTO = new VehicleTO();
                System.out.println("5");
                index = Integer.parseInt(selectedIndex[i]);
                System.out.println("6");

                vehicleTO.setMfrId(mfrids[index]);
                System.out.println("7");
                vehicleTO.setModelName(modelNames[index]);
                vehicleTO.setModelId(modelId[index]);
                vehicleTO.setTypeId(typeIds[index]);
                vehicleTO.setFuelId(fuelIds[index]);
                vehicleTO.setActiveInd(activeStatus[index]);
                vehicleTO.setDescription(desciption[index]);
                List.add(vehicleTO);
            }
            String pageTitle = "View model";
            request.setAttribute("pageTitle", pageTitle);
            request.setAttribute("fleetTypeId", fleetTypeId);
            modify = vehicleBP.processModifyModelDetails(List, UserId);
//            mv = "handleModelAlterPage.do?fleetTypeId="+fleetTypeId;
//            mv = handleAlterModelPage(request, reponse, command);
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Model Details Modified Successfully");


            menuPath = "Vehicle  >>  MODEL >>  Alter";
            vehicleTO.setFleetTypeId(fleetTypeId);
            ArrayList modelList = new ArrayList();
            modelList = vehicleBP.processGetModelList(vehicleTO);
            request.setAttribute("ModelList", modelList);
            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.getMfrList(vehicleTO);
            request.setAttribute("MfrList", MfrList);

            ArrayList TypeList = new ArrayList();
            TypeList = vehicleBP.getTypeList(vehicleTO);
            request.setAttribute("TypeList", TypeList);

            ArrayList FuelList = new ArrayList();
            FuelList = vehicleBP.processGetFuelList();
            request.setAttribute("FuelList", FuelList);

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            pageTitle = "Alter Model";
            request.setAttribute("pageTitle", pageTitle);
            path = "content/MFR/alterModel.jsp";
//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve designation --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //handleAddVehiclePage
    /**
     * This method used to View MFR Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAddVehiclePage(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String menuPath = "Vehicle  >> Add New Vehicle";

        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Vehicle-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "content/Vehicle/addVehicle.jsp";
//                path = "BrattleFoods/vehicleMaster.jsp";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "Add Vehicle";
                request.setAttribute("pageTitle", pageTitle);

                request.setAttribute("fleetTypeId", 1);


                request.setAttribute("fleetTypeId", request.getParameter("fleetTypeId"));

                String listId = request.getParameter("listId");
                System.out.println("listId==" + listId);
                request.setAttribute("listId", listId);


                ArrayList MfrList = new ArrayList();
                MfrList = vehicleBP.processGetMfrList();
                request.setAttribute("MfrList", MfrList);

                ArrayList usageList = new ArrayList();
                usageList = vehicleBP.processGetUsageList();
                request.setAttribute("UsageList", usageList);

                ArrayList classList = new ArrayList();
                classList = vehicleBP.processGetClassList();
                request.setAttribute("ClassList", classList);

                //OperationPointList
                ArrayList operationPointList = new ArrayList();
                operationPointList = vehicleBP.processGetopList();
                request.setAttribute("OperationPointList", operationPointList);

                ArrayList servicePointList = new ArrayList();
                servicePointList = vehicleBP.processGetspList();
                request.setAttribute("ServicePointList", servicePointList);

                ArrayList customerList = new ArrayList();
                customerList = customerBP.processActiveCustomerList();
                request.setAttribute("customerList", customerList);

                ArrayList tyreItemList = new ArrayList();
                tyreItemList = vehicleBP.processTyreItems();
                request.setAttribute("tyreItemList", tyreItemList);

                ArrayList groupList = new ArrayList();
                groupList = vehicleBP.getGroupList();
                request.setAttribute("groupList", groupList);

                ArrayList positionList = new ArrayList();
                positionList = mrsBP.getTyrePostions();
                request.setAttribute("positionList", positionList);


//                ArrayList leasingCustList = new ArrayList();
//                leasingCustList = vehicleBP.getLeasingCustomerList();
//                request.setAttribute("leasingCustList", leasingCustList);
                VehicleTO vehicleTo = new VehicleTO();
                ArrayList TypeList = new ArrayList();
                TypeList = vehicleBP.processActiveTypeList(vehicleTo);
                request.setAttribute("TypeList", TypeList);

                String venId = request.getParameter("vendorId");
//                String vendorTypeId = request.getParameter("vendorTypeId");
                System.out.println("the vendor name====>" + venId);
                if (venId != null) {
                    System.out.println("in if condition===>vickys1" + venId);
                    System.out.println("the vendor name====>" + venId);

                    ArrayList vendorNameList = new ArrayList();
                    vendorNameList = vehicleBP.vendorNameList(venId);
                    request.setAttribute("vendorNameList", vendorNameList);
                } else {
                    System.out.println("in else condition======>>vickys2");
                    ArrayList leasingCustList = new ArrayList();
                    leasingCustList = vehicleBP.getLeasingCustomerList();
                    request.setAttribute("leasingCustList", leasingCustList);
                }

                ArrayList vendorNameList = new ArrayList();
                vendorNameList = vehicleBP.vendorNameList(venId);
                request.setAttribute("vendorNameList", vendorNameList);

                String vendorTypeId = ThrottleConstants.vehicleInsuranceVendorTypeId;
                ArrayList vendorList = new ArrayList();
                vendorList = vehicleBP.vendorList(vendorTypeId);
                request.setAttribute("vendorList", vendorList);

                String vendorTypeIdCompliance = ThrottleConstants.vehicleComplianceVendorTypeId;
                ArrayList vendorListCompliance = new ArrayList();
                vendorListCompliance = vehicleBP.vendorListCompliance(vendorTypeIdCompliance);
                request.setAttribute("vendorListCompliance", vendorListCompliance);

                ArrayList primarybankList = new ArrayList();
                primarybankList = vehicleBP.getPrimarybankList();
                request.setAttribute("primarybankList", primarybankList);
                System.out.println("primarybankList.size()=" + primarybankList.size());

                ArrayList oemList = new ArrayList();
                oemList = vehicleBP.getOemList(vehicleTo);
                System.out.println("oemListcontrol***************************" + oemList.size());
                request.setAttribute("oemListSize", oemList.size());
                request.setAttribute("oemList", oemList);
//            
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //handleAddVehicle   
    /**
     * This method used to Insert MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAddVehicle(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {

//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        int madhavaramSp = 1011;
        ModelAndView mv = new ModelAndView();
        try {
            path = "content/Vehicle/addVehicle.jsp";
            String menuPath = "Vehicle  >>  ADD  ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            CompanyTO companyTO = new CompanyTO();
            if (vehicleCommand.getUsageId() != null && vehicleCommand.getUsageId() != "") {
                vehicleTO.setUsageId(vehicleCommand.getUsageId());
            }
            if (vehicleCommand.getWar_period() != null && vehicleCommand.getWar_period() != "") {
                vehicleTO.setWar_period(vehicleCommand.getWar_period());
            }
            if (vehicleCommand.getMfrId() != null && vehicleCommand.getMfrId() != "") {
                vehicleTO.setMfrId(vehicleCommand.getMfrId());
            }
            if (vehicleCommand.getClassId() != null && vehicleCommand.getClassId() != "") {
                vehicleTO.setClassId(vehicleCommand.getClassId());
            }
            if (vehicleCommand.getModelId() != null && vehicleCommand.getModelId() != "") {
                vehicleTO.setModelId(vehicleCommand.getModelId());
            }
            if (vehicleCommand.getDateOfSale() != null && vehicleCommand.getDateOfSale() != "") {
                vehicleTO.setDateOfSale(vehicleCommand.getDateOfSale());
            }
            if (vehicleCommand.getRegistrationDate() != null && vehicleCommand.getRegistrationDate() != "") {
                vehicleTO.setRegistrationDate(vehicleCommand.getRegistrationDate());
            }
            if (vehicleCommand.getRegNo() != null && vehicleCommand.getRegNo() != "") {
                vehicleTO.setRegNo(vehicleCommand.getRegNo());
            }
            if (vehicleCommand.getDescription() != null && vehicleCommand.getDescription() != "") {
                vehicleTO.setDescription(vehicleCommand.getDescription());
            }
            if (vehicleCommand.getEngineNo() != null && vehicleCommand.getEngineNo() != "") {
                vehicleTO.setEngineNo(vehicleCommand.getEngineNo());
            }
            if (vehicleCommand.getChassisNo() != null && vehicleCommand.getChassisNo() != "") {
                vehicleTO.setChassisNo(vehicleCommand.getChassisNo());
            }
            if (vehicleCommand.getOpId() != null && vehicleCommand.getOpId() != "") {
                vehicleTO.setOpId(vehicleCommand.getOpId());
            }
//            if (vehicleCommand.getSpId() != null && vehicleCommand.getSpId() != "") {
//                vehicleTO.setSpId(vehicleCommand.getSpId());
//            }
            if (vehicleCommand.getSeatCapacity() != null && vehicleCommand.getSeatCapacity() != "") {
                vehicleTO.setSeatCapacity(vehicleCommand.getSeatCapacity());
            }
//            if (vehicleCommand.getNextFCDate() != null && vehicleCommand.getNextFCDate() != "") {
//                vehicleTO.setNextFCDate(vehicleCommand.getNextFCDate());
//            }
            if (vehicleCommand.getKmReading() != null && vehicleCommand.getKmReading() != "") {
                vehicleTO.setKmReading(vehicleCommand.getKmReading());
            }
//            if (vehicleCommand.getHmReading() != null && vehicleCommand.getHmReading() != "") {
//                vehicleTO.setHmReading(vehicleCommand.getHmReading());
//            }
            if (vehicleCommand.getVehicleColor() != null && vehicleCommand.getVehicleColor() != "") {
                vehicleTO.setVehicleColor(vehicleCommand.getVehicleColor());
            }
            if (vehicleCommand.getGroupId() != null && vehicleCommand.getGroupId() != "") {
                vehicleTO.setGroupId(vehicleCommand.getGroupId());
            }
            if (vehicleCommand.getLeasingCustId() != null && vehicleCommand.getLeasingCustId() != "") {
                vehicleTO.setLeasingCustId(vehicleCommand.getLeasingCustId());
            }
            if (vehicleCommand.getGpsSystem() != null && vehicleCommand.getGpsSystem() != "") {
                vehicleTO.setGpsSystem(vehicleCommand.getGpsSystem());
            }
            if (vehicleCommand.getWarrantyDate() != null && vehicleCommand.getWarrantyDate() != "") {
                vehicleTO.setWarrantyDate(vehicleCommand.getWarrantyDate());
            }
            if (vehicleCommand.getAsset() != null && vehicleCommand.getAsset() != "") {
                vehicleTO.setAsset(vehicleCommand.getAsset());
            }
            if (vehicleCommand.getAxles() != null && vehicleCommand.getAxles() != "") {
                vehicleTO.setAxles(vehicleCommand.getAxles());
            }
            if (vehicleCommand.getVehicleDepreciation() != null && vehicleCommand.getVehicleDepreciation() != "") {
                vehicleTO.setVehicleDepreciation(vehicleCommand.getVehicleDepreciation());
            }
            if (vehicleCommand.getDailyKm() != null && vehicleCommand.getDailyKm() != "") {
                vehicleTO.setDailyKm(vehicleCommand.getDailyKm());
            }
            if (vehicleCommand.getDailyHm() != null && vehicleCommand.getDailyHm() != "") {
                vehicleTO.setDailyHm(vehicleCommand.getDailyHm());
            }
            if (vehicleCommand.getGpsSystemId() != null && vehicleCommand.getGpsSystemId() != "") {
                vehicleTO.setGpsSystemId(vehicleCommand.getGpsSystemId());
            }
            if (vehicleCommand.getOwnerShips() != null && vehicleCommand.getOwnerShips() != "") {
                vehicleTO.setOwnerShips(vehicleCommand.getOwnerShips());
            }
            if (vehicleCommand.getTypeId() != null && vehicleCommand.getTypeId() != "") {
                vehicleTO.setTypeId(vehicleCommand.getTypeId());
            }
            if (vehicleCommand.getVendorId() != null && vehicleCommand.getVendorId() != "") {
                vehicleTO.setVendorId(vehicleCommand.getVendorId());
            }

            String fservice = request.getParameter("selectedindex");
            if (fservice != null) {
                vehicleTO.setFservice(fservice);
            } else {
                fservice = "N";
                vehicleTO.setFservice(fservice);
            }

            vehicleTO.setTyreIds(vehicleCommand.getTyreIds());
            vehicleTO.setPositionId(vehicleCommand.getPositionIds());
//            vehicleTO.setTyreType(vehicleCommand.getTyreType());
            vehicleTO.setItemIds(vehicleCommand.getItemIds());
            vehicleTO.setTyreDate(vehicleCommand.getTyreDate());
            String pageTitle = "View Vehicles";

            ArrayList ModelList = new ArrayList();
            ModelList = vehicleBP.processActiveMfrList();
            request.setAttribute("MfrList", ModelList);

            VehicleTO vehicleTo = new VehicleTO();
            ArrayList TypeList = new ArrayList();
            TypeList = vehicleBP.processActiveTypeList(vehicleTo);
            request.setAttribute("TypeList", TypeList);

            ArrayList usageList = new ArrayList();
            usageList = vehicleBP.processActiveUsageList();
            request.setAttribute("usageList", usageList);

            ArrayList customerList = new ArrayList();
            customerList = customerBP.processActiveCustomerList();
            request.setAttribute("customerList", customerList);

            ArrayList groupList = new ArrayList();
            groupList = vehicleBP.getGroupList();
            request.setAttribute("groupList", groupList);

            request.setAttribute("pageTitle", pageTitle);
            int vehicleId = 0;

            vehicleId = vehicleBP.processAddVehicleDetails(vehicleTO, userId, madhavaramSp);
            vehicleBP.vehicleKmUpdate(vehicleId, vehicleTO.getKmReading(), vehicleTO.getHmReading(), userId);

            path = "content/Vehicle/viewVehicle.jsp";

            vehicleTO = new VehicleTO();
            if (companyType == 1011) {
                vehicleTO.setCompanyId(companyId);
            } else {
                vehicleTO.setCompanyId("0");
            }

            ArrayList vehicleList = new ArrayList();
            vehicleList = vehicleBP.processVehicleList(vehicleTO);
            request.setAttribute("vehicleList", vehicleList);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Added Successfully");
            //request.removeAttribute("AddVehicleList");
            // ArrayList AddVehicleList = new ArrayList();
            // AddVehicleList = vehicleBP.processGetAddVehicleList();
            // request.setAttribute("AddVehicleList", AddVehicleList);  

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Added Successfully");
            mv = handleSearchVehicle(request, response, command);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    //handleSearchVehicle
    /**
     * This method used to Vehicle List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleSearchVehiclePage(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        
        if (request.getSession().isNew()) {
            System.out.println("2");
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "Vehicle  >>  View Vehicles";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        vehicleCommand = command;
        VehicleTO vehicleTO = new VehicleTO();
        //int companyType = (Integer) session.getAttribute("companyTypeId");
        int companyType = 1011;
        //int UserId = (Integer) session.getAttribute("userId");
        System.out.println("hai");
        try {
        String companyId = (String) session.getAttribute("companyId");
            if (!loginBP.checkAuthorisation(userFunctions, "Vehicle-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "View Vehicles";
                request.setAttribute("pageTitle", pageTitle);
                String fleetTypeId = request.getParameter("fleetTypeId");
                System.out.println("fleetTypeId==" + fleetTypeId);
                request.setAttribute("fleetTypeId", fleetTypeId);

                String listId = request.getParameter("listId");
                vehicleTO.setListId(listId);
                System.out.println("listId==" + listId);
                request.setAttribute("listId", listId);

                String regNo = request.getParameter("regno");
                if (regNo == null) {
                    regNo = "";
                }
                vehicleTO.setRegNo(regNo);

                String vendorIds = request.getParameter("vendorId");
                if (vendorIds == null) {
                    vendorIds = "";
                }
                vehicleTO.setVendorId(vendorIds);

                String fromDate = request.getParameter("fromDate");
                vehicleTO.setFromDate(fromDate);

                String toDate = request.getParameter("toDate");
                vehicleTO.setToDate(toDate);
                System.out.println("listId+--------------------------    "+ listId);
                
                if ("1".equals(listId)) {
                    path = "content/Vehicle/searchVehicleInsurance.jsp";
                    ArrayList vehiclesInsuranceList = new ArrayList();
                    System.out.println("22222222222222222222222222222222");
                    vehiclesInsuranceList = vehicleBP.getVehiclesInsuranceList(vehicleTO);
                    System.out.println("vehiclesList" + vehiclesInsuranceList);
                    request.setAttribute("vehiclesList", vehiclesInsuranceList);
                    request.setAttribute("vendorId", vendorIds);
                    request.setAttribute("regNo", regNo);
//                    request.setAttribute("fromDate", fromDate);
//                    request.setAttribute("toDate", toDate);

                } else if ("5".equals(listId)) {
                    System.out.println("am here in searchFleetPurchase.....");
                    path = "content/Vehicle/searchFleetPurchase.jsp";
                    ArrayList fleetPurchaseList = new ArrayList();
                    fleetPurchaseList = vehicleBP.getFleetPurchaseList(vehicleTO);
                    System.out.println("fleetPurchaseList" + fleetPurchaseList.size());
                    request.setAttribute("fleetPurchaseList", fleetPurchaseList);
                    request.setAttribute("vendorId", vendorIds);
                    request.setAttribute("fromDate", fromDate);
                    request.setAttribute("toDate", toDate);

                } else if ("3".equals(listId)) {
                    String vendorTypeIdCompliance = ThrottleConstants.vehicleComplianceVendorTypeId;
                    ArrayList vendorListCompliance = new ArrayList();
                    vendorListCompliance = vehicleBP.vendorListCompliance(vendorTypeIdCompliance);
                    request.setAttribute("vendorListCompliance", vendorListCompliance);
                    path = "content/Vehicle/searchVehicleFC.jsp";
                    request.setAttribute("vendorId", vendorIds);
                    request.setAttribute("regNo", regNo);
                    request.setAttribute("fromDate", fromDate);
                    request.setAttribute("toDate", toDate);

                    ArrayList vehiclesFCList = new ArrayList();
                    vehiclesFCList = vehicleBP.getVehiclesFCList(vehicleTO);
                    request.setAttribute("vehiclesFCList", vehiclesFCList);
                } else if ("6".equals(listId)) {

                    path = "content/Vehicle/searchVehicleRegistration.jsp";
                    request.setAttribute("vendorId", vendorIds);
                    request.setAttribute("regNo", regNo);
                    request.setAttribute("fromDate", fromDate);
                    request.setAttribute("toDate", toDate);

                    ArrayList vehiclesRegistrationList = new ArrayList();
                    vehiclesRegistrationList = vehicleBP.getVehiclesRegistrationList(vehicleTO);
                    request.setAttribute("vehiclesRegistrationList", vehiclesRegistrationList);
                } else if ("2".equals(listId)) {

                    path = "content/Vehicle/viewVehicleTax.jsp";
//                    path = "content/Vehicle/searchVehicleRoadPermit.jsp";
                    request.setAttribute("vendorId", vendorIds);
                    request.setAttribute("regNo", regNo);
                    request.setAttribute("fromDate", fromDate);
                    request.setAttribute("toDate", toDate);

                    ArrayList RoadTaxList = new ArrayList();

                    RoadTaxList = vehicleBP.getRoadTaxList(vehicleTO);
                    request.setAttribute("RoadTaxList", RoadTaxList);
                    System.out.println("RoadTaxList in cont = " + RoadTaxList.size());
                } else {
                    System.out.println("I'M IN");
                    path = "content/Vehicle/viewVehicle.jsp";
                }
//                int totalRecords = 0;
//                int pageNo = 0;
//                int totalPages = 0;
//
//                session.setAttribute("totalRecords", totalRecords);
//                request.setAttribute("pageNo", pageNo);
//                request.setAttribute("totalPages", totalPages);



                String vendorTypeId = ThrottleConstants.vehicleInsuranceVendorTypeId;
                ArrayList vendorList = new ArrayList();
                vendorList = vehicleBP.vendorList(vendorTypeId);
                request.setAttribute("vendorList", vendorList);



                ArrayList ModelList = new ArrayList();
                ModelList = vehicleBP.processActiveMfrList();
                request.setAttribute("MfrList", ModelList);
                VehicleTO vehicleTo = new VehicleTO();
                ArrayList TypeList = new ArrayList();
                TypeList = vehicleBP.processActiveTypeList(vehicleTo);
                request.setAttribute("TypeList", TypeList);
                ArrayList usageList = new ArrayList();
                usageList = vehicleBP.processActiveUsageList();
                request.setAttribute("usageList", usageList);
                ArrayList customerList = new ArrayList();
                customerList = customerBP.processActiveCustomerList();
                request.setAttribute("customerList", customerList);
                ArrayList groupList = new ArrayList();
                groupList = vehicleBP.getGroupList();
                request.setAttribute("groupList", groupList);
                if (companyType == 1011) {
                    vehicleTO.setCompanyId(companyId);
                } else {
                    vehicleTO.setCompanyId("0");
                }
                ArrayList vehicleList = new ArrayList();
//                PaginationHelper pagenation = new PaginationHelper();
//                int startIndex = 0;
//                int endIndex = 0;
//
//                vehicleTO.setStartIndex(startIndex);
//                vehicleTO.setEndIndex(endIndex);
                String roleId = "" + (Integer) session.getAttribute("RoleId");
                System.out.println("roleId****************************" + roleId);
                vehicleTO.setRoleId(roleId);
                vehicleList = vehicleBP.processVehicleList(vehicleTO);
//                totalRecords = vehicleList.size();
//                pagenation.setTotalRecords(totalRecords);
                String buttonClicked = "";
                if (request.getParameter("button") != null && request.getParameter("button") != "") {
                    buttonClicked = request.getParameter("button");
                }
//                if (request.getParameter("pageNo") != null && request.getParameter("pageNo") != "") {
//                    pageNo = Integer.parseInt(request.getParameter("pageNo"));
//                }
//                pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
//                totalPages = pagenation.getTotalNoOfPages();
//                request.setAttribute("pageNo", pageNo);
//                System.out.println("pageNo" + pageNo);
//                request.setAttribute("totalPages", totalPages);
//                startIndex = pagenation.getStartIndex();
//                endIndex = pagenation.getEndIndex();

//                vehicleTO.setStartIndex(startIndex);
//                vehicleTO.setEndIndex(endIndex);
                vehicleList = vehicleBP.processVehicleList(vehicleTO);
//                if (totalRecords <= 0) {
//                    vehicleList = null;
//                }
                ArrayList leasingCustList = new ArrayList();
                leasingCustList = vehicleBP.getLeasingCustomerList();
                request.setAttribute("leasingCustList", leasingCustList);
                request.setAttribute("vehicleList", vehicleList);
                System.out.println("start Index=" + vehicleTO.getStartIndex());
                System.out.println("end Index=" + vehicleTO.getEndIndex());

                request.setAttribute("vehicleUploadsDetails", null);

            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleResetVehicleKM(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "Vehicle  >>  Reset Vehicle KM";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        vehicleCommand = command;
        VehicleTO vehicleTO = new VehicleTO();
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Vehicle-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "View Vehicles";
                request.setAttribute("pageTitle", pageTitle);
                path = "content/Vehicle/resetVehicleKM.jsp";

            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveResetVehicleKM(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        try {
            String menuPath = "Vehicle  >>  Reset Vehicle KM";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            VehicleTO vehicleTO = new VehicleTO();

            if (vehicleCommand.getRegno() != null && vehicleCommand.getRegno() != "") {
                vehicleTO.setRegno(vehicleCommand.getRegno());
            }
            if (vehicleCommand.getKmReading() != null && vehicleCommand.getKmReading() != "") {
                vehicleTO.setKmReading(vehicleCommand.getKmReading());
            }
            if (vehicleCommand.getActualKm() != null && vehicleCommand.getActualKm() != "") {
                vehicleTO.setActualKm(vehicleCommand.getActualKm());
            }
            if (vehicleCommand.getTotalKm() != null && vehicleCommand.getTotalKm() != "") {
                vehicleTO.setTotalKm(vehicleCommand.getTotalKm());
            }
            if (vehicleCommand.getReason() != null && vehicleCommand.getReason() != "") {
                vehicleTO.setReason(vehicleCommand.getReason());
            }
            String pageTitle = "Reset Vehicle KM";
            request.setAttribute("pageTitle", pageTitle);

            int insertStatus = 0;
            path = "content/Vehicle/resetVehicleKM.jsp";

            insertStatus = vehicleBP.saveResetVehicleKM(vehicleTO, userId);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "KM Reset Successfully");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //handleVehicleList
    /**
     * This method used to Vehicle List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleSearchVehicle(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String temp = "";
        ArrayList vehicleList = new ArrayList();
        String menuPath = "Vehicle  >>  View Vehicles";
        vehicleCommand = command;
        VehicleTO vehicleTO = new VehicleTO();
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        try {
            path = "content/Vehicle/viewVehicle.jsp";
            String fleetTypeId = request.getParameter("fleetTypeId");
            request.setAttribute("fleetTypeId", fleetTypeId);
            if (vehicleCommand.getUsageId().equalsIgnoreCase("0")) {
                vehicleTO.setUsageId("");
            } else {
                vehicleTO.setUsageId(vehicleCommand.getUsageId());
            }
            if (vehicleCommand.getMfrId().equalsIgnoreCase("0")) {
                vehicleTO.setMfrId("");
            } else {
                vehicleTO.setMfrId(vehicleCommand.getMfrId());
            }
            if (vehicleCommand.getRegNo() != null && vehicleCommand.getRegNo() != "") {
                vehicleTO.setRegNo(vehicleCommand.getRegNo());
            } else {
                vehicleTO.setRegNo("");
            }

            if (vehicleCommand.getCustId() != null && !vehicleCommand.getCustId().equalsIgnoreCase("0")) {
                vehicleTO.setCustId(vehicleCommand.getCustId());
            } else {
                vehicleTO.setCustId("");
            }

            if (vehicleCommand.getGroupId() != null && !vehicleCommand.getGroupId().equalsIgnoreCase("0")) {
                vehicleTO.setGroupId(vehicleCommand.getGroupId());
            } else {
                vehicleTO.setGroupId("");
            }

            if (vehicleCommand.getTypeId().equalsIgnoreCase("0")) {
                vehicleTO.setTypeId("");
            } else {
                vehicleTO.setTypeId(vehicleCommand.getTypeId());
            }
            if (companyType == 1011) {
                vehicleTO.setCompanyId(companyId);
            } else {
                vehicleTO.setCompanyId("0");
            }
            if (vehicleCommand.getOwnership() != null && !"".equals(vehicleCommand.getOwnership())) {
                vehicleTO.setOwnership(vehicleCommand.getOwnership());
                request.setAttribute("ownership", vehicleCommand.getOwnership());
            }
            ArrayList ModelList = new ArrayList();
            ModelList = vehicleBP.processActiveMfrList();
            request.setAttribute("MfrList", ModelList);

            VehicleTO vehicleTo = new VehicleTO();
            ArrayList TypeList = new ArrayList();
            TypeList = vehicleBP.processActiveTypeList(vehicleTo);
            request.setAttribute("TypeList", TypeList);

            ArrayList usageList = new ArrayList();
            usageList = vehicleBP.processActiveUsageList();
            request.setAttribute("usageList", usageList);

            ArrayList customerList = new ArrayList();
            customerList = customerBP.processActiveCustomerList();
            request.setAttribute("customerList", customerList);

            ArrayList groupList = new ArrayList();
            groupList = vehicleBP.getGroupList();
            request.setAttribute("groupList", groupList);

            request.setAttribute("regNo", vehicleCommand.getRegNo());
            request.setAttribute("typeId", vehicleCommand.getTypeId());
            request.setAttribute("mfrId", vehicleCommand.getMfrId());
            request.setAttribute("usageId", vehicleCommand.getUsageId());
            request.setAttribute("custId", vehicleCommand.getCustId());
            request.setAttribute("groupId", vehicleCommand.getGroupId());
            String roleId = "" + (Integer) session.getAttribute("RoleId");
            vehicleTO.setRoleId(roleId);
            vehicleList = vehicleBP.processVehicleList(vehicleTO);
            request.setAttribute("vehicleList", vehicleList);
            System.out.println("start Index=" + vehicleTO.getStartIndex());
            System.out.println("end Index=" + vehicleTO.getEndIndex());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            String pageTitle = "View Vehicles";
            request.setAttribute("pageTitle", pageTitle);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewVehicle(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String pageTitle = "";
        String menuPath = "Vehicle  >>  View Vehicle Detail";
        vehicleCommand = command;
        ArrayList vehicleDetail = new ArrayList();
        VehicleTO vehicleTO = null;
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        int UserId = (Integer) session.getAttribute("userId");
        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "Vehicle-Add")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                pageTitle = "Alter Vehicle";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                request.setAttribute("pageTitle", pageTitle);
                path = "content/Vehicle/viewVehicleDetail.jsp";
                String vehicleId = request.getParameter("vehicleId");

                vehicleTO = new VehicleTO();
                if (vehicleId != null && vehicleId != "") {
                    vehicleTO.setVehicleId(vehicleId);
                }

                ArrayList MfrList = new ArrayList();
                MfrList = vehicleBP.processGetMfrList();
                request.setAttribute("MfrList", MfrList);

                ArrayList usageList = new ArrayList();
                usageList = vehicleBP.processGetUsageList();
                request.setAttribute("UsageList", usageList);

                ArrayList classList = new ArrayList();
                classList = vehicleBP.processGetClassList();
                request.setAttribute("ClassList", classList);

                ArrayList modelList = new ArrayList();
                modelList = vehicleBP.processActiveModelList();
                request.setAttribute("modelList", modelList);

                ArrayList customerList = new ArrayList();
                customerList = customerBP.processActiveCustomerList();
                request.setAttribute("customerList", customerList);

                ArrayList groupList = new ArrayList();
                groupList = vehicleBP.getGroupList();
                request.setAttribute("groupList", groupList);

                //OperationPointList
                ArrayList operationPointList = new ArrayList();
                operationPointList = vehicleBP.processGetopList();
                request.setAttribute("OperationPointList", operationPointList);

                // get Vehicle tyre items list
                ArrayList tyreItemList = new ArrayList();
                tyreItemList = vehicleBP.processTyreItems();
                request.setAttribute("tyreItemList", tyreItemList);

                //            ArrayList positionList = new ArrayList();
                //            positionList = mrsBP.getTyrePostions();
                //            request.setAttribute("positionList", positionList);
                //get positions list
                ArrayList positionList = new ArrayList();
                positionList = vehicleBP.getTyrePostions();
                request.setAttribute("positionList", positionList);
                System.out.println("positionList size=" + positionList.size());

                // get Vehicle TYre positions
                ArrayList vehTyreList = new ArrayList();
                vehTyreList = vehicleBP.processTyrePosition(Integer.parseInt(vehicleId));
                request.setAttribute("vehTyreList", vehTyreList);

                ArrayList leasingCustList = new ArrayList();
                leasingCustList = vehicleBP.getLeasingCustomerList();
                request.setAttribute("leasingCustList", leasingCustList);

                vehicleDetail = vehicleBP.processVehicleDetail(vehicleTO);
                request.setAttribute("vehicleDetail", vehicleDetail);
                ArrayList vehicleAMC = new ArrayList();
                vehicleAMC = vehicleBP.vehicleAMCDetail(vehicleTO);
                request.setAttribute("vehicleAMC", vehicleAMC);
                ArrayList RoadTaxList = new ArrayList();
                RoadTaxList = vehicleBP.getRoadTaxList(vehicleTO);
                request.setAttribute("RoadTaxList", RoadTaxList);

                VehicleTO vehicleTo = new VehicleTO();
                ArrayList TypeList = new ArrayList();
                TypeList = vehicleBP.processActiveTypeList(vehicleTo);
                request.setAttribute("TypeList", TypeList);

           // }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVehicleDetail(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String pageTitle = "";
        String menuPath = "Vehicle  >>  Search Vehicles";
        vehicleCommand = command;
        ArrayList vehicleDetail = new ArrayList();
        VehicleTO vehicleTO = null;
        FinanceTO financeTO = null;
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Vehicle-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                pageTitle = "Alter Vehicle";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                request.setAttribute("pageTitle", pageTitle);
                String listId = request.getParameter("listId");
                System.out.println("listId==" + listId);
                request.setAttribute("listId", listId);
                String vehicleId = request.getParameter("vehicleId");
                String fleetTypeId = request.getParameter("fleetTypeId");

                request.setAttribute("fleetTypeId", fleetTypeId);
                System.out.println("fleetTypeId==="+fleetTypeId);
                
                if ("1".equals(listId)) {
                    String editStatus = request.getParameter("editStatus");
                    if ("0".equals(editStatus)) {//view
                        path = "content/Vehicle/editVehiclePage.jsp";
                    } else if ("1".equals(editStatus)) {//edit
                        path = "content/Vehicle/viewVehiclePage.jsp";
                    } else if ("2".equals(editStatus)) {//renew
                        path = "content/Vehicle/viewVehiclePage.jsp";
                    } else if ("3".equals(editStatus)) {//postinsurance
                        path = "content/Vehicle/viewVehiclePage.jsp";
                    } else {//add
                        path = "content/Vehicle/insuranceVehicleDetail.jsp";
                    }

                } else if ("2".equals(listId)) {
                    path = "content/Vehicle/roadTaxVehicleDetail.jsp";
                } else if ("3".equals(listId)) {
                    path = "content/Vehicle/fCVehicleDetail.jsp";
                } else if ("4".equals(listId)) {
                    path = "content/Vehicle/fCVehicleDetail.jsp";
                } else if ("5".equals(listId)) {
                    path = "content/Vehicle/viewVehicleAMC.jsp";
                } else if ("6".equals(listId)) {
                    path = "content/Vehicle/registrationVehicleDetail.jsp";
                } else {
                    String editStatus = request.getParameter("editStatus");
                    System.out.println("editStatus="+editStatus);
                    if ("1".equals(editStatus)) {//edit
                        path = "content/Vehicle/editVehiclePage.jsp";
                    } else if ("0".equals(editStatus)) {//view
                        path = "content/Vehicle/viewVehiclePage.jsp";
                    } else {//add
                        path = "content/Vehicle/addVehicle.jsp";
                    }
                }

                vehicleTO = new VehicleTO();
                if (vehicleId != null && vehicleId != "") {
                    vehicleTO.setVehicleId(vehicleId);
                }


                ArrayList oemList = new ArrayList();
                oemList = vehicleBP.getOemList(vehicleTO);
                System.out.println("oemListcontrol" + oemList.size());
                request.setAttribute("oemListSize", oemList.size());
                request.setAttribute("oemList", oemList);
                VehicleTO vTO = new VehicleTO();

                Iterator itr1 = oemList.iterator();

                while (itr1.hasNext()) {
                    vTO = (VehicleTO) itr1.next();

                    request.setAttribute("oemId", vTO.getOemId());
                    request.setAttribute("oemMfr", vTO.getOemMfr());
                    request.setAttribute("oemDepth", vTO.getOemDepth());
                    request.setAttribute("oemTyreNo", vTO.getOemTyreNo());
                    request.setAttribute("oemBattery", vTO.getOemBattery());
                    request.setAttribute("oemDetailsId", vTO.getOemDetailsId());
                    System.out.println("getOemDetailsId" + vTO.getOemDetailsId());
                }
                ArrayList vehiclesDetails = new ArrayList();
                vehiclesDetails = vehicleBP.getVehiclesDetails(vehicleId);
                System.out.println("vehiclesDetails" + vehiclesDetails);
                request.setAttribute("vehiclesDetails", vehiclesDetails.size());



                if (vehiclesDetails.size() == 0) {
                    request.setAttribute("vehicleId", 0);
                    System.out.println("request.setAttribute(vehicleId)" + request.getAttribute("vehicleId"));
                }
                Iterator itr = vehiclesDetails.iterator();

                while (itr.hasNext()) {
                    vTO = (VehicleTO) itr.next();
//                request.setAttribute("vehicleId",vTO.getVehicleId());
                    if (vTO.getVehicleId() != null && vTO.getVehicleId() != "") {
                        System.out.println("vehicleCommand.getVehicleId()" + vTO.getVehicleId());
                        request.setAttribute("vehicleId", vehicleCommand.getVehicleId());
                    }
                    request.setAttribute("regNo", vTO.getRegNo());
                    request.setAttribute("mfrId", vTO.getMfrId());
                    request.setAttribute("axleTypeId", vTO.getAxleTypeId());
                    request.setAttribute("modelId", vTO.getModelId());
                    request.setAttribute("typeId", vTO.getTypeId());
                    request.setAttribute("war_period", vTO.getWar_period());
                    request.setAttribute("dateOfSale", vTO.getDateOfSale());
                    request.setAttribute("chassisNo", vTO.getChassisNo());
                    request.setAttribute("seatCapacity", vTO.getSeatCapacity());
                    request.setAttribute("engineNo", vTO.getEngineNo());
                    request.setAttribute("vehicleDepreciation", vTO.getVehicleDepreciation());
                    request.setAttribute("classId", vTO.getClassId());
                    request.setAttribute("kmReading", vTO.getKmReading());
                    request.setAttribute("dailyKm", vTO.getDailyKm());
                    request.setAttribute("dailyHm", vTO.getDailyHm());
                    request.setAttribute("vehicleCost", vTO.getVehicleCost());
                    request.setAttribute("vehicleColor", vTO.getVehicleColor());
                    request.setAttribute("usageId", vTO.getUsageId());
                    request.setAttribute("gpsSystemId", vTO.getGpsSystemId());
                    request.setAttribute("warrantyDate", vTO.getWarrantyDate());
                    request.setAttribute("description", vTO.getDescription());
                    request.setAttribute("ownership", vTO.getOwnership());
                    request.setAttribute("warPeriod", vTO.getWar_period());
                    request.setAttribute("opId", vTO.getOpId());


                    request.setAttribute("insuranceid", vTO.getInsuranceid());
                    request.setAttribute("insPolicy", vTO.getInsPolicy());
                    request.setAttribute("prevPolicy", vTO.getPrevPolicy());
                    request.setAttribute("packageType", vTO.getPackageType());
                    request.setAttribute("insName", vTO.getInsName());
                    System.out.println("insNameinsNameinsName" + vTO.getInsName());
                    request.setAttribute("insAddress", vTO.getInsAddress());
                    request.setAttribute("insMobileNo", vTO.getInsMobileNo());
                    request.setAttribute("insPolicyNo", vTO.getInsPolicyNo());
                    request.setAttribute("insIdvValue", vTO.getInsIdvValue());
                    request.setAttribute("fromDate1", vTO.getFromDate1());
                    request.setAttribute("toDate1", vTO.getToDate1());
                    request.setAttribute("premiunAmount", vTO.getPremiunAmount());
                    request.setAttribute("paymentType", vTO.getPaymentType());
                    request.setAttribute("bankId", vTO.getBankId());
                    request.setAttribute("bankBranchId", vTO.getBankBranchId());
                    request.setAttribute("bankBranchIdIns", vTO.getBankBranchIdIns());
                    request.setAttribute("chequeNo", vTO.getChequeNo());
                    request.setAttribute("chequeDate", vTO.getChequeDate());
                    request.setAttribute("chequeAmount", vTO.getChequeAmount());
                    request.setAttribute("clearanceDate", vTO.getClearanceDate());
                    request.setAttribute("clearanceId", vTO.getClearanceId());
                    request.setAttribute("vendorId1", vTO.getVendorId1());
                    request.setAttribute("paymentTypeIns", vTO.getPaymentTypeIns());
                    request.setAttribute("paymentDateIns", vTO.getPaymentDateIns());

                    request.setAttribute("roadTaxId", vTO.getRoadTaxId());
                    request.setAttribute("roadTaxReceiptNo", vTO.getRoadTaxReceiptNo());
                    request.setAttribute("roadTaxReceiptDate", vTO.getRoadTaxReceiptDate());
                    request.setAttribute("roadTaxPaidLocation", vTO.getRoadTaxPaidLocation());
                    request.setAttribute("roadTaxPeriod", vTO.getRoadTaxPeriod());
                    request.setAttribute("roadTaxAmount", vTO.getRoadTaxAmount());
                    request.setAttribute("roadTaxFromDate", vTO.getRoadTaxFromDate());
                    request.setAttribute("nextRoadTaxDate", vTO.getNextRoadTaxDate());
                    request.setAttribute("roadTaxRemarks", vTO.getRoadTaxRemarks());
                    request.setAttribute("clearanceIdRT", vTO.getClearanceIdRT());
                    request.setAttribute("vendorIdRT", vTO.getVendorIdRT());
                    request.setAttribute("paymentTypeRT", vTO.getPaymentTypeRT());
                    request.setAttribute("paymentDateRT", vTO.getPaymentDateRT());

                    request.setAttribute("bankIdRT", vTO.getBankIdRT());
                    request.setAttribute("bankBranchIdRT", vTO.getBankBranchIdRT());
                    request.setAttribute("chequeNoRT", vTO.getChequeNoRT());
                    request.setAttribute("chequeDateRT", vTO.getChequeDateRT());
                    request.setAttribute("chequeAmountRT", vTO.getChequeAmountRT());

                    request.setAttribute("fcid", vTO.getFcid());
                    request.setAttribute("rtoDetail", vTO.getRtoDetail());
                    request.setAttribute("fcDate", vTO.getFcDate());
                    request.setAttribute("fcReceiptNo", vTO.getFcReceiptNo());
                    request.setAttribute("fcAmount", vTO.getFcAmount());
                    request.setAttribute("fcExpiryDate", vTO.getFcExpiryDate());
                    request.setAttribute("fcRemarks", vTO.getFcRemarks());
                    request.setAttribute("clearanceIdFC", vTO.getClearanceIdFC());
                    request.setAttribute("vendorIdFC", vTO.getVendorIdFC());
                    request.setAttribute("paymentTypeFC", vTO.getPaymentTypeFC());
                    request.setAttribute("paymentDateFC", vTO.getPaymentDateFC());

                    request.setAttribute("bankIdFC", vTO.getBankIdFC());
                    request.setAttribute("bankBranchIdFC", vTO.getBankBranchIdFC());
                    request.setAttribute("chequeNoFC", vTO.getChequeNoFC());
                    request.setAttribute("chequeDateFC", vTO.getChequeDateFC());
                    request.setAttribute("chequeAmountFC", vTO.getChequeAmountFC());


                    request.setAttribute("permitid", vTO.getPermitid());
                    request.setAttribute("permitType", vTO.getPermitType());
                    request.setAttribute("permitNo", vTO.getPermitNo());
                    request.setAttribute("permitAmount", vTO.getPermitAmount());
                    request.setAttribute("permitPaidDate", vTO.getPermitPaidDate());
                    request.setAttribute("permitExpiryDate", vTO.getPermitExpiryDate());
                    request.setAttribute("remarks", vTO.getRemarks());
                    request.setAttribute("clearanceIdPermit", vTO.getClearanceIdPermit());
                    request.setAttribute("vendorIdPermit", vTO.getVendorIdPermit());
                    request.setAttribute("paymentTypePermit", vTO.getPaymentTypePermit());

                    request.setAttribute("bankIdPermit", vTO.getBankIdPermit());
                    request.setAttribute("bankBranchIdPermit", vTO.getBankBranchIdPermit());
                    request.setAttribute("chequeNoPermit", vTO.getChequeNoPermit());
                    request.setAttribute("chequeDatePermit", vTO.getChequeDatePermit());
                    request.setAttribute("chequeAmountPermit", vTO.getChequeAmountPermit());
                    request.setAttribute("opId", vTO.getOpId());
                    request.setAttribute("doorNo", vTO.getDoorNo());
                }
//                request.setAttribute("vehiclesDetails", vehiclesDetails);
                if (vehicleCommand.getRegNo() != null && vehicleCommand.getRegNo() != "") {
                    vehicleTO.setRegNo(vehicleCommand.getRegNo());
                } else {
                    vehicleTO.setRegNo("");
                }
                if (vehicleCommand.getVehicleId() != null && vehicleCommand.getVehicleId() != "") {
                    vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
                }

                ArrayList VehicleInsList = new ArrayList();
                VehicleInsList = vehicleBP.getVehicleInsuranceList(vehicleTO);
                request.setAttribute("VehicleInsList", VehicleInsList);

                ArrayList RoadTaxList = new ArrayList();
                RoadTaxList = vehicleBP.getRoadTaxList(vehicleTO);
                request.setAttribute("RoadTaxList", RoadTaxList);
                System.out.println("RoadTaxList in cont = " + RoadTaxList.size());
                request.setAttribute("regNo", vehicleCommand.getRegNo());

                ArrayList FCList = new ArrayList();
                FCList = vehicleBP.getFCList(vehicleTO);
                request.setAttribute("FCList", FCList);
                System.out.println("FCList in cont = " + FCList.size());

                ArrayList VehiclePermitList = new ArrayList();
                VehiclePermitList = vehicleBP.getVehiclePermitList(vehicleTO);
                request.setAttribute("VehiclePermitList", VehiclePermitList);

                ArrayList vehicleAmcList = new ArrayList();
                vehicleAmcList = vehicleBP.getVehicleAMCList(vehicleTO);
                request.setAttribute("vehicleAmcList", vehicleAmcList);

                ArrayList MfrList = new ArrayList();
                MfrList = vehicleBP.processGetMfrList();
                request.setAttribute("MfrList", MfrList);

                ArrayList bankBranchList = new ArrayList();
//                bankBranchList = financeBP.getBankBranchLists();
                request.setAttribute("bankBranchList", bankBranchList);

                ArrayList usageList = new ArrayList();
                usageList = vehicleBP.processGetUsageList();
                request.setAttribute("UsageList", usageList);

                ArrayList classList = new ArrayList();
                classList = vehicleBP.processGetClassList();
                request.setAttribute("ClassList", classList);

                ArrayList modelList = new ArrayList();
                modelList = vehicleBP.processActiveModelList();
                request.setAttribute("modelList", modelList);

                ArrayList customerList = new ArrayList();
                customerList = customerBP.processActiveCustomerList();
                request.setAttribute("customerList", customerList);

                ArrayList groupList = new ArrayList();
                groupList = vehicleBP.getGroupList();
                request.setAttribute("groupList", groupList);

                //OperationPointList
                ArrayList operationPointList = new ArrayList();
                operationPointList = vehicleBP.processGetopList();
                request.setAttribute("OperationPointList", operationPointList);

                // get Vehicle tyre items list
                ArrayList tyreItemList = new ArrayList();
                tyreItemList = vehicleBP.processTyreItems();
                request.setAttribute("tyreItemList", tyreItemList);
                System.out.println("size of tyrelist===" + tyreItemList.size());

                VehicleTO vehicleTo = new VehicleTO();
                ArrayList TypeList = new ArrayList();
                TypeList = vehicleBP.processActiveTypeList(vehicleTo);
                request.setAttribute("TypeList", TypeList);

                //            ArrayList positionList = new ArrayList();
                //            positionList = mrsBP.getTyrePostions();
                //            request.setAttribute("positionList", positionList);
                //get positions list
                ArrayList positionList = new ArrayList();
                positionList = vehicleBP.getTyrePostions();
                request.setAttribute("positionList", positionList);
                System.out.println("positionList size=" + positionList.size());

                // get Vehicle TYre positions
                ArrayList vehTyreList = new ArrayList();
                vehTyreList = vehicleBP.processTyrePosition(Integer.parseInt(vehicleId));
                request.setAttribute("vehTyreList", vehTyreList);

                ArrayList leasingCustList = new ArrayList();
                leasingCustList = vehicleBP.getLeasingCustomerList();
                request.setAttribute("leasingCustList", leasingCustList);

                ArrayList primarybankList = new ArrayList();
                primarybankList = vehicleBP.getPrimarybankList();
                request.setAttribute("primarybankList", primarybankList);
                System.out.println("primarybankList.size()=" + primarybankList.size());

                String vendorTypeId = ThrottleConstants.vehicleInsuranceVendorTypeId;
                ArrayList vendorList = new ArrayList();
                vendorList = vehicleBP.vendorList(vendorTypeId);
                request.setAttribute("vendorList", vendorList);

                String vendorTypeIdCompliance = ThrottleConstants.vehicleComplianceVendorTypeId;
                ArrayList vendorListCompliance = new ArrayList();
                vendorListCompliance = vehicleBP.vendorListCompliance(vendorTypeIdCompliance);
                request.setAttribute("vendorListCompliance", vendorListCompliance);

                ArrayList depreciationList = new ArrayList();
                depreciationList = vehicleBP.getVehicleDepreciationList(vehicleId);
                if (depreciationList.size() > 0) {
                    request.setAttribute("depreciationList", depreciationList);
                }
                request.setAttribute("depreciationSize", depreciationList.size());

                vehicleDetail = vehicleBP.processVehicleDetail(vehicleTO);
                request.setAttribute("vehicleDetail", vehicleDetail);
            }

            request.setAttribute("editStatus", request.getParameter("editStatus"));

            ArrayList vehicleUploadsDetails = new ArrayList();
            vehicleUploadsDetails = vehicleBP.getvehicleUploadsDetails(vehicleTO);
            request.setAttribute("vehicleUploadsDetails", vehicleUploadsDetails);
            
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    //handleAddVehicle   

    /**
     * This method used to Insert MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAlterVehicle(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        System.out.println("handleAlterVehicle ");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        String pageTitle = "View Vehicles";
        ModelAndView mv = new ModelAndView();
        //         ArrayList vehicleDetail = new ArrayList();

        try {
            String menuPath = "Vehicle  >>  View Vehicles  ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            CompanyTO companyTO = new CompanyTO();
            path = "content/Vehicle/viewVehicle.jsp";
            if (vehicleCommand.getUsageId() != null && vehicleCommand.getUsageId() != "") {
                vehicleTO.setUsageId(vehicleCommand.getUsageId());
            }
            if (vehicleCommand.getWar_period() != null && vehicleCommand.getWar_period() != "") {
                vehicleTO.setWar_period(vehicleCommand.getWar_period());
            }
            if (vehicleCommand.getMfrId() != null && vehicleCommand.getMfrId() != "") {
                vehicleTO.setMfrId(vehicleCommand.getMfrId());
            }
            if (vehicleCommand.getClassId() != null && vehicleCommand.getClassId() != "") {
                vehicleTO.setClassId(vehicleCommand.getClassId());
            }
            if (vehicleCommand.getModelId() != null && vehicleCommand.getModelId() != "") {
                vehicleTO.setModelId(vehicleCommand.getModelId());
            }
            if (vehicleCommand.getGpsSystem() != null && vehicleCommand.getGpsSystem() != "") {
                vehicleTO.setGpsSystem(vehicleCommand.getGpsSystem());
            }
            if (vehicleCommand.getDateOfSale() != null && vehicleCommand.getDateOfSale() != "") {
                vehicleTO.setDateOfSale(vehicleCommand.getDateOfSale());
            }
            if (vehicleCommand.getRegistrationDate() != null && vehicleCommand.getRegistrationDate() != "") {
                vehicleTO.setRegistrationDate(vehicleCommand.getRegistrationDate());
            }
            if (vehicleCommand.getRegNo() != null && vehicleCommand.getRegNo() != "") {
                vehicleTO.setRegNo(vehicleCommand.getRegNo());
            }
            if (vehicleCommand.getDescription() != null && vehicleCommand.getDescription() != "") {
                vehicleTO.setDescription(vehicleCommand.getDescription());
            }
            if (vehicleCommand.getEngineNo() != null && vehicleCommand.getEngineNo() != "") {
                vehicleTO.setEngineNo(vehicleCommand.getEngineNo());
            }
            if (vehicleCommand.getChassisNo() != null && vehicleCommand.getChassisNo() != "") {
                vehicleTO.setChassisNo(vehicleCommand.getChassisNo());
            }
            if (vehicleCommand.getOpId() != null && vehicleCommand.getOpId() != "") {
                vehicleTO.setOpId(vehicleCommand.getOpId());
            }
            if (vehicleCommand.getSeatCapacity() != null && vehicleCommand.getSeatCapacity() != "") {
                vehicleTO.setSeatCapacity(vehicleCommand.getSeatCapacity());
            }
            if (vehicleCommand.getNextFCDate() != null && vehicleCommand.getNextFCDate() != "") {
                vehicleTO.setNextFCDate(vehicleCommand.getNextFCDate());
            }
            if (vehicleCommand.getKmReading() != null && vehicleCommand.getKmReading() != "") {
                vehicleTO.setKmReading(vehicleCommand.getKmReading());
            }
            if (vehicleCommand.getHmReading() != null && vehicleCommand.getHmReading() != "") {
                vehicleTO.setHmReading(vehicleCommand.getHmReading());
            }
            if (vehicleCommand.getDescription() != null && vehicleCommand.getDescription() != "") {
                vehicleTO.setDescription(vehicleCommand.getDescription());
            }
            if (vehicleCommand.getFservice() != null && vehicleCommand.getFservice() != "") {
                vehicleTO.setFservice(vehicleCommand.getFservice());
            }
            //          if (vehicleCommand.getGroupId() != null && vehicleCommand.getGroupId() != "") {
            //               vehicleTO.setGroupId(vehicleCommand.getGroupId());
            //            }
            if (vehicleCommand.getLeasingCustId() != null && vehicleCommand.getLeasingCustId() != "") {
                vehicleTO.setLeasingCustId(vehicleCommand.getLeasingCustId());
            }
            if (vehicleCommand.getTypeId() != null && vehicleCommand.getTypeId() != "") {
                vehicleTO.setTypeId(vehicleCommand.getTypeId());
            }
            if (vehicleCommand.getVehicleDepreciation() != null && vehicleCommand.getVehicleDepreciation() != "") {
                vehicleTO.setVehicleDepreciation(vehicleCommand.getVehicleDepreciation());
            }

            vehicleTO.setActiveInd(vehicleCommand.getActiveInd());
            vehicleTO.setMfrIds(vehicleCommand.getMfrIds());
            vehicleTO.setModelIds(vehicleCommand.getModelIds());
            //            vehicleTO.setTyreType(vehicleCommand.getTyreType());
            vehicleTO.setSeatCapacity(vehicleCommand.getSeatCapacity());
            vehicleTO.setCustId(vehicleCommand.getCustId());
            vehicleTO.setSeatCapacity(vehicleCommand.getSeatCapacity());
            vehicleTO.setDailyKm(vehicleCommand.getDailyKm());
            vehicleTO.setDailyHm(vehicleCommand.getDailyHm());

            vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            vehicleTO.setItemIds(vehicleCommand.getItemIds());
            vehicleTO.setPositionId(vehicleCommand.getPosIds());
            vehicleTO.setTyreIds(vehicleCommand.getTyreIds());
            vehicleTO.setTyreNos(vehicleCommand.getTyreNos());
            System.out.println("setOwnershipsetOwnershipsetOwnership=" + request.getParameter("ownerShips"));
            vehicleTO.setOwnership(request.getParameter("ownerShips"));
            vehicleTO.setTyreDate(vehicleCommand.getTyreDate());

            request.setAttribute("pageTitle", pageTitle);
            int insertStatus = 0;

            System.out.println("vehicleTO.setItemIds=" + vehicleTO.getItemIds());

            insertStatus = vehicleBP.processUpdateVehicleDetails(vehicleTO, Integer.parseInt(companyId), userId);
            vehicleBP.vehicleKmUpdate(Integer.parseInt(vehicleTO.getVehicleId()), vehicleTO.getKmReading(), vehicleTO.getHmReading(), userId);

            vehicleTO = new VehicleTO();
            if (companyType == 1011) {
                vehicleTO.setCompanyId(companyId);
            } else {
                vehicleTO.setCompanyId("0");
            }

            ArrayList ModelList = new ArrayList();
            ModelList = vehicleBP.processActiveMfrList();
            request.setAttribute("MfrList", ModelList);

            VehicleTO vehicleTo = new VehicleTO();
            ArrayList TypeList = new ArrayList();
            TypeList = vehicleBP.processActiveTypeList(vehicleTo);
            request.setAttribute("TypeList", TypeList);

            ArrayList usageList = new ArrayList();
            usageList = vehicleBP.processActiveUsageList();
            request.setAttribute("usageList", usageList);

            ArrayList customerList = new ArrayList();
            customerList = customerBP.processActiveCustomerList();
            request.setAttribute("customerList", customerList);

            ArrayList primarybankList = new ArrayList();
            primarybankList = vehicleBP.getPrimarybankList();
            request.setAttribute("primarybankList", primarybankList);
            System.out.println("primarybankList.size()=" + primarybankList.size());

            String vendorTypeId = ThrottleConstants.vehicleInsuranceVendorTypeId;
            ArrayList vendorList = new ArrayList();
            vendorList = vehicleBP.vendorList(vendorTypeId);
            request.setAttribute("vendorList", vendorList);

            ArrayList vehicleList = new ArrayList();
            vehicleList = vehicleBP.processVehicleList(vehicleTO);
            request.setAttribute("vehicleList", vehicleList);
            if (insertStatus > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Details Modified Successfully");
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                mv = handleSearchVehicle(request, response, command);
            }
            //request.removeAttribute("AddVehicleList");
            // ArrayList AddVehicleList = new ArrayList();
            // AddVehicleList = vehicleBP.processGetAddVehicleList();
            // request.setAttribute("AddVehicleList", AddVehicleList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public void handleGetTyreList(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String tyreNo = "";

        try {
            String tyreId = request.getParameter("tyreId");

            tyreNo = vehicleBP.processAvailableTyres(Integer.parseInt(tyreId), companyId);
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(tyreNo);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve PriceList data in Ajax --> " + exception);
        }
    }

    public void checkVehicleTyreNo(HttpServletRequest request, HttpServletResponse response) throws IOException, FPBusinessException {
        System.out.println("i am in ajax ");
        HttpSession session = request.getSession();
        String regNo = request.getParameter("tyreNo");
        System.out.println("regNo" + regNo);
        String suggestions = "";
        suggestions = vehicleBP.checkVehicleTyreNo(regNo);
        System.out.println("suggestions" + suggestions);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.print(suggestions);
        writer.close();
    }

    //-----------Vehicle Insurance Updates starts here---------------
    public void getVehicleDetails(HttpServletRequest request, HttpServletResponse response) throws IOException, FPBusinessException {
        System.out.println("..........ajax in controller.....................");
        HttpSession session = request.getSession();
        String regNo = request.getParameter("regNo1");
        System.out.println("regNo--->" + regNo);
        String vehicleDetails = "";
        vehicleDetails = vehicleBP.getVehicleDetails(regNo);
        System.out.println("vehicleDetails" + vehicleDetails);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.print(vehicleDetails);
        writer.close();
    }

    //-----------Vehicle Existing starts here---------------
    public void getVehicleExisting(HttpServletRequest request, HttpServletResponse response) throws IOException, FPBusinessException {

        HttpSession session = request.getSession();
        String regNo = request.getParameter("regNo1");
        System.out.println("regNo--->" + regNo);
        String vPAge = request.getParameter("vPAge");
        System.out.println("vPAge--->" + vPAge);
        String vehicleDetails = "";
        vehicleDetails = vehicleBP.getVehicleExisting(regNo, vPAge);
        System.out.println("vehicleDetails" + vehicleDetails);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.print(vehicleDetails);
        writer.close();
    }

    /**
     * This method used to handle Vehicle Insurance
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleVehicleInsurance(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        String pageTitle = "View Vehicles";
        ModelAndView mv = new ModelAndView();
        try {
            String menuPath = "Vehicle  >>  Insurance ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            path = "content/Vehicle/VehicleInsuranceUpdate.jsp";
            request.setAttribute("pageTitle", pageTitle);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle Vehicle Insurance
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView searchVehicleAMCPage(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        String pageTitle = "View Vehicles";
        ModelAndView mv = new ModelAndView();
        try {
            String menuPath = "Vehicle  >>  Insurance ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            path = "content/Vehicle/viewVehicleAMC.jsp";
            request.setAttribute("pageTitle", pageTitle);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle Vehicle Insurance
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleVehicleAMC(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        String pageTitle = "View Vehicles";
        ModelAndView mv = new ModelAndView();
        try {
            String menuPath = "Vehicles  >>  AMC Add";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            path = "content/Vehicle/VehicleAMCUpdate.jsp";

            request.setAttribute("pageTitle", pageTitle);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to add vehicle AMC page --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView vehicleAMCDetail(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        System.out.println("Test=======");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";
        String pageTitle = "";
        String menuPath = "Vehicle  >>  Vehicle AMC Update";
        vehicleCommand = command;
        ArrayList vehicleAMC = new ArrayList();
        ArrayList vehicleDetail = new ArrayList();
        VehicleTO vehicleTO = null;
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Vehicle-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                pageTitle = "Alter Vehicle Insurance";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                request.setAttribute("pageTitle", pageTitle);
                path = "content/Vehicle/alterVehicleAMC.jsp";

                String amcId = request.getParameter("amcid");
                System.out.println("amcId = " + amcId);
                String vehicleId = request.getParameter("vehicleId");
                System.out.println("vehicleId = " + vehicleId);

                vehicleTO = new VehicleTO();
                if (vehicleId != null && vehicleId != "") {
                    System.out.println("===in vehicle===" + vehicleId);
                    vehicleTO.setVehicleId(vehicleId);
                }
                if (amcId != null && amcId != "") {
                    System.out.println("===in vehicle===" + amcId);
                    vehicleTO.setAmcId(amcId);
                }
                vehicleDetail = vehicleBP.updateVehicleDetail(vehicleTO);
                request.setAttribute("vehicleDetail", vehicleDetail);
                System.out.println("vehicleDetail.size() in cont= " + vehicleDetail.size());

                vehicleAMC = vehicleBP.vehicleAMCDetail(vehicleTO);
                request.setAttribute("vehicleAMC", vehicleAMC);
                System.out.println("vehicleAMC.size() in cont= " + vehicleAMC.size());
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to update Vehicle Insurance
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView saveVehicleInsurance(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        String pageTitle = "View Vehicles";
        ModelAndView mv = new ModelAndView();
        try {
            String menuPath = "Vehicle  >>  Insurance ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            path = "content/Vehicle/VehicleInsuranceUpdate.jsp";

            if (vehicleCommand.getVehicleId() != null && !vehicleCommand.getVehicleId().equals("")) {
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }
            if (vehicleCommand.getInsuranceCompanyName() != null && !vehicleCommand.getInsuranceCompanyName().equals("")) {
                vehicleTO.setInsuranceCompanyName(vehicleCommand.getInsuranceCompanyName());
            }
            if (vehicleCommand.getPremiumNo() != null && !vehicleCommand.getPremiumNo().equals("")) {
                vehicleTO.setPremiumNo(vehicleCommand.getPremiumNo());
            }
            if (vehicleCommand.getPremiumPaidAmount() != 0) {
                vehicleTO.setPremiumPaidAmount(vehicleCommand.getPremiumPaidAmount());
            }
            if (vehicleCommand.getPremiumPaidDate() != null && !vehicleCommand.getPremiumPaidDate().equals("")) {
                vehicleTO.setPremiumPaidDate(vehicleCommand.getPremiumPaidDate());
            }
            if (vehicleCommand.getVehicleValue() != 0) {
                vehicleTO.setVehicleValue(vehicleCommand.getVehicleValue());
            }
            if (vehicleCommand.getPremiumExpiryDate() != null && !vehicleCommand.getPremiumExpiryDate().equals("")) {
                vehicleTO.setPremiumExpiryDate(vehicleCommand.getPremiumExpiryDate());
            }
            if (vehicleCommand.getInsuarnceNCB() != null && !vehicleCommand.getInsuarnceNCB().equals("")) {
                vehicleTO.setInsuarnceNCB(vehicleCommand.getInsuarnceNCB());
            }
            if (vehicleCommand.getInsuarnceRemarks() != null && !vehicleCommand.getInsuarnceRemarks().equals("")) {
                vehicleTO.setInsuarnceRemarks(vehicleCommand.getInsuarnceRemarks());
            }
            request.setAttribute("pageTitle", pageTitle);
            int insertStatus = 0;
            insertStatus = vehicleBP.saveVehicleInsurance(vehicleTO, Integer.parseInt(companyId), userId);
            System.out.println("Vehicle Insu insert status: " + insertStatus);
            if (insertStatus == 1) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Insurance Updated Successfully");
            }
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } /*catch (FPBusinessException exception) {
        // run time exception has occurred. Directed to error page.
        FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
        request.setAttribute(ParveenErrorConstants.ERROR_KEY,
        exception.getErrorMessage());
        } */ catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to update Vehicle Insurance
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView saveVehicleAMC(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        String pageTitle = "View Vehicles";
        ModelAndView mv = new ModelAndView();
        try {
            String menuPath = "Vehicle  >>  Insurance ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            path = "content/Vehicle/viewVehicleAMC.jsp";

            if (vehicleCommand.getVehicleId() != null && !vehicleCommand.getVehicleId().equals("")) {
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }
            if (vehicleCommand.getAmcCompanyName() != null && !vehicleCommand.getAmcCompanyName().equals("")) {
                vehicleTO.setAmcCompanyName(vehicleCommand.getAmcCompanyName());
            }
            if (vehicleCommand.getAmcAmount() != null && !vehicleCommand.getAmcAmount().equals("")) {
                vehicleTO.setAmcAmount(vehicleCommand.getAmcAmount());
            }
            if (vehicleCommand.getAmcDuration() != null && !vehicleCommand.getAmcDuration().equals("")) {
                vehicleTO.setAmcDuration(vehicleCommand.getAmcDuration());
            }
            if (vehicleCommand.getAmcChequeDate() != null && !vehicleCommand.getAmcChequeDate().equals("")) {
                vehicleTO.setAmcChequeDate(vehicleCommand.getAmcChequeDate());
            }
            if (vehicleCommand.getAmcFromDate() != null && !vehicleCommand.getAmcFromDate().equals("")) {
                vehicleTO.setAmcFromDate(vehicleCommand.getAmcFromDate());
            }
            if (vehicleCommand.getAmcToDate() != null && !vehicleCommand.getAmcToDate().equals("")) {
                vehicleTO.setAmcToDate(vehicleCommand.getAmcToDate());
            }
            if (vehicleCommand.getAmcRemarks() != null && !vehicleCommand.getAmcRemarks().equals("")) {
                vehicleTO.setAmcRemarks(vehicleCommand.getAmcRemarks());
            }

            request.setAttribute("pageTitle", pageTitle);
            int insertStatus = 0;
            insertStatus = vehicleBP.saveVehicleAMC(vehicleTO, Integer.parseInt(companyId), userId);
            System.out.println("Vehicle Insu insert status: " + insertStatus);
            if (insertStatus == 1) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Insurance Updated Successfully");
            }
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } /*catch (FPBusinessException exception) {
        // run time exception has occurred. Directed to error page.
        FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
        request.setAttribute(ParveenErrorConstants.ERROR_KEY,
        exception.getErrorMessage());
        } */ catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVehicleAccident(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        ArrayList accidentVehicleList = new ArrayList();
        String pageTitle = "View Vehicles";
        ModelAndView mv = new ModelAndView();
        try {
            String menuPath = "Vehicle  >>  Accident ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            path = "content/Vehicle/manageVehicleAccident.jsp";
            request.setAttribute("pageTitle", pageTitle);
            accidentVehicleList = vehicleBP.getAccidentVehicleDetails();
            
            if (accidentVehicleList.size() > 0) {
                request.setAttribute("accidentVehicleList", accidentVehicleList);
            }
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to update Vehicle Accident
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView saveVehicleAccident(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        ArrayList accidentVehicleList = new ArrayList();
        userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        String pageTitle = "View Vehicles";
        ModelAndView mv = new ModelAndView();
        try {
            String menuPath = "Vehicle  >>  Accident ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            path = "content/Vehicle/manageVehicleAccident.jsp";
            if (vehicleCommand.getVehicleId() != null && !vehicleCommand.getVehicleId().equals("")) {
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }
            if (vehicleCommand.getAccidentDate() != null && !vehicleCommand.getAccidentDate().equals("")) {
                vehicleTO.setAccidentDate(vehicleCommand.getAccidentDate());
            }
            if (vehicleCommand.getAccidentSpot() != null && !vehicleCommand.getAccidentSpot().equals("")) {
                vehicleTO.setAccidentSpot(vehicleCommand.getAccidentSpot());
            }
            if (vehicleCommand.getDriverName() != null && !vehicleCommand.getDriverName().equals("")) {
                vehicleTO.setDriverName(vehicleCommand.getDriverName());
            }
            if (vehicleCommand.getPoliceStation() != null && !vehicleCommand.getPoliceStation().equals("")) {
                vehicleTO.setPoliceStation(vehicleCommand.getPoliceStation());
            }
            if (vehicleCommand.getFirNo() != null && !vehicleCommand.getFirNo().equals("")) {
                vehicleTO.setFirNo(vehicleCommand.getFirNo());
            }
            if (vehicleCommand.getFirDate() != null && !vehicleCommand.getFirDate().equals("")) {
                vehicleTO.setFirDate(vehicleCommand.getFirDate());
            }
            if (vehicleCommand.getFirPreparedBy() != null && !vehicleCommand.getFirPreparedBy().equals("")) {
                vehicleTO.setFirPreparedBy(vehicleCommand.getFirPreparedBy());
            }
            if (vehicleCommand.getAccidentRemarks() != null && !vehicleCommand.getAccidentRemarks().equals("")) {
                vehicleTO.setAccidentRemarks(vehicleCommand.getAccidentRemarks());
            }

            request.setAttribute("pageTitle", pageTitle);
            int insertStatus = 0;
            insertStatus = vehicleBP.saveVehicleAccident(vehicleTO, Integer.parseInt(companyId), userId);
            if (insertStatus != 0) {
                accidentVehicleList = vehicleBP.getAccidentVehicleDetails();
            }
            if (accidentVehicleList.size() > 0) {
                request.setAttribute("accidentVehicleList", accidentVehicleList);
            }
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Accident Added Successfully");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } /*catch (FPBusinessException exception) {
        // run time exception has occurred. Directed to error page.
        FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
        request.setAttribute(ParveenErrorConstants.ERROR_KEY,
        exception.getErrorMessage());
        } */ catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //Vehicle Accident Start
    public ModelAndView handleVehicleAccidentDetail(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        ArrayList accidentVehicleList = new ArrayList();
        String pageTitle = "View Vehicles";
        ModelAndView mv = new ModelAndView();
        try {
            String menuPath = "Vehicle  >>  Accident ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //VehicleTO vehicleTO = new VehicleTO();
            path = "content/Vehicle/VehicleAccidentDetails.jsp";

            //request.setAttribute("accidentVehicleList", accidentVehicleList);
            request.setAttribute("pageTitle", pageTitle);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewVehicleAccidentDetails(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        //        if (request.getSession().isNew()) {
        //            return new ModelAndView("content/common/login.jsp");
        //        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        String pageTitle = "View Accident Vehicles Details";
        String regno = request.getParameter("regno");
        ArrayList accidentVehicleList = new ArrayList();
        ModelAndView mv = new ModelAndView();
        VehicleTO vehicleTO = new VehicleTO();
        try {
            String menuPath = "Vehicle  >>  Accident ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            accidentVehicleList = vehicleBP.processGetAccidentVehicleList(regno);
            request.setAttribute("accidentVehicleList", accidentVehicleList);
            path = "content/Vehicle/manageVehicleAccident.jsp";
            request.setAttribute("pageTitle", pageTitle);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Accident vehicle Details page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView alterVehicleAccidentDetails(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {

//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        String pageTitle = "View Accident Vehicles Details";
        String regNo = request.getParameter("regNo");
        ArrayList accidentVehicleList = new ArrayList();
        ModelAndView mv = new ModelAndView();
        VehicleTO vehicleTO = new VehicleTO();
        try {
            String menuPath = "Vehicle  >>  Accident ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            accidentVehicleList = vehicleBP.processAlterAccidentVehicleList(regNo);
            request.setAttribute("accidentVehicleList", accidentVehicleList);
            path = "content/Vehicle/VehicleAccidentDetails.jsp";
            request.setAttribute("pageTitle", pageTitle);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Accident vehicle Details page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView updateVehicleAccident(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        ArrayList accidentVehicleList = new ArrayList();
        userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        String pageTitle = "View Vehicles";
        ModelAndView mv = new ModelAndView();
        try {
            String menuPath = "Vehicle  >>  Accident ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            path = "content/Vehicle/manageVehicleAccident.jsp";
            if (vehicleCommand.getVehicleId() != null && !vehicleCommand.getVehicleId().equals("")) {
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }
            if (vehicleCommand.getAccidentDate() != null && !vehicleCommand.getAccidentDate().equals("")) {
                vehicleTO.setAccidentDate(vehicleCommand.getAccidentDate());
            }
            if (vehicleCommand.getAccidentSpot() != null && !vehicleCommand.getAccidentSpot().equals("")) {
                vehicleTO.setAccidentSpot(vehicleCommand.getAccidentSpot());
            }
            if (vehicleCommand.getDriverName() != null && !vehicleCommand.getDriverName().equals("")) {
                vehicleTO.setDriverName(vehicleCommand.getDriverName());
            }
            if (vehicleCommand.getPoliceStation() != null && !vehicleCommand.getPoliceStation().equals("")) {
                vehicleTO.setPoliceStation(vehicleCommand.getPoliceStation());
            }
            if (vehicleCommand.getFirNo() != null && !vehicleCommand.getFirNo().equals("")) {
                vehicleTO.setFirNo(vehicleCommand.getFirNo());
            }
            if (vehicleCommand.getFirDate() != null && !vehicleCommand.getFirDate().equals("")) {
                vehicleTO.setFirDate(vehicleCommand.getFirDate());
            }
            if (vehicleCommand.getFirPreparedBy() != null && !vehicleCommand.getFirPreparedBy().equals("")) {
                vehicleTO.setFirPreparedBy(vehicleCommand.getFirPreparedBy());
            }
            if (vehicleCommand.getAccidentRemarks() != null && !vehicleCommand.getAccidentRemarks().equals("")) {
                vehicleTO.setAccidentRemarks(vehicleCommand.getAccidentRemarks());
            }

            request.setAttribute("pageTitle", pageTitle);
            int insertStatus = 0;
            insertStatus = vehicleBP.updateVehicleAccident(vehicleTO, Integer.parseInt(companyId), userId);
            if (insertStatus != 0) {
                accidentVehicleList = vehicleBP.getAccidentVehicleDetails();
            }
            if (accidentVehicleList.size() > 0) {
                request.setAttribute("accidentVehicleList", accidentVehicleList);
            }
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Accident Updated Successfully");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } /*catch (FPBusinessException exception) {
        // run time exception has occurred. Directed to error page.
        FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
        request.setAttribute(ParveenErrorConstants.ERROR_KEY,
        exception.getErrorMessage());
        } */ catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //Vehicle Accident End
    /**
     * This method used to handle Vehicle Road Tax
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleVehicleRoadTax(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        String pageTitle = "View Vehicles";
        ModelAndView mv = new ModelAndView();
        try {
            String menuPath = "Vehicle  >>  Accident ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            path = "content/Vehicle/VehicleRoadTaxUpdate.jsp";
            request.setAttribute("pageTitle", pageTitle);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to update Vehicle RoadTax
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView saveVehicleRoadTax(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        String pageTitle = "View Vehicles";
        ModelAndView mv = new ModelAndView();
        try {
            String menuPath = "Vehicle  >>  Accident ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            path = "content/Vehicle/VehicleRoadTaxUpdate.jsp";
            if (vehicleCommand.getVehicleId() != null && !vehicleCommand.getVehicleId().equals("")) {
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }
            if (vehicleCommand.getRoadTaxReceiptNo() != null && !vehicleCommand.getRoadTaxReceiptNo().equals("")) {
                vehicleTO.setRoadTaxReceiptNo(vehicleCommand.getRoadTaxReceiptNo());
            }
            if (vehicleCommand.getRoadTaxReceiptDate() != null && !vehicleCommand.getRoadTaxReceiptDate().equals("")) {
                vehicleTO.setRoadTaxReceiptDate(vehicleCommand.getRoadTaxReceiptDate());
            }
            if (vehicleCommand.getRoadTaxPaidLocation() != null && !vehicleCommand.getRoadTaxPaidLocation().equals("")) {
                vehicleTO.setRoadTaxPaidLocation(vehicleCommand.getRoadTaxPaidLocation());
            }
            if (vehicleCommand.getRoadTaxPaidOffice() != null && !vehicleCommand.getRoadTaxPaidOffice().equals("")) {
                vehicleTO.setRoadTaxPaidOffice(vehicleCommand.getRoadTaxPaidOffice());
            }
            if (vehicleCommand.getRoadTaxPeriod() != null && !vehicleCommand.getRoadTaxPeriod().equals("")) {
                vehicleTO.setRoadTaxPeriod(vehicleCommand.getRoadTaxPeriod());
            }
            if (vehicleCommand.getRoadTaxPaidAmount() != 0) {
                vehicleTO.setRoadTaxPaidAmount(vehicleCommand.getRoadTaxPaidAmount());
            }
            if (vehicleCommand.getRoadTaxFromDate() != null && !vehicleCommand.getRoadTaxFromDate().equals("")) {
                vehicleTO.setRoadTaxFromDate(vehicleCommand.getRoadTaxFromDate());
            }
            if (vehicleCommand.getNextRoadTaxDate() != null && !vehicleCommand.getNextRoadTaxDate().equals("")) {
                vehicleTO.setNextRoadTaxDate(vehicleCommand.getNextRoadTaxDate());
            }
            if (vehicleCommand.getRoadTaxRemarks() != null && !vehicleCommand.getRoadTaxRemarks().equals("")) {
                vehicleTO.setRoadTaxRemarks(vehicleCommand.getRoadTaxRemarks());
            }

            request.setAttribute("pageTitle", pageTitle);
            int insertStatus = 0;
            insertStatus = vehicleBP.saveVehicleRoadTax(vehicleTO, Integer.parseInt(companyId), userId);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Road Tax Updated Successfully");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } /*catch (FPBusinessException exception) {
        // run time exception has occurred. Directed to error page.
        FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
        request.setAttribute(ParveenErrorConstants.ERROR_KEY,
        exception.getErrorMessage());
        } */ catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle Vehicle FC detail
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleVehicleFc(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        String pageTitle = "View Vehicles";
        ModelAndView mv = new ModelAndView();
        try {
            String menuPath = "Vehicle  >>  Accident ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            path = "content/Vehicle/VehicleFcDetails.jsp";
            request.setAttribute("pageTitle", pageTitle);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to update Vehicle RoadTax
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView saveVehicleFc(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        String pageTitle = "View Vehicles";
        ModelAndView mv = new ModelAndView();
        try {
            String menuPath = "Vehicle  >>  Accident ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            path = "content/Vehicle/VehicleFcDetails.jsp";
            if (vehicleCommand.getVehicleId() != null && !vehicleCommand.getVehicleId().equals("")) {
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }
            if (vehicleCommand.getRtoDetail() != null && !vehicleCommand.getRtoDetail().equals("")) {
                vehicleTO.setRtoDetail(vehicleCommand.getRtoDetail());
            }
            if (vehicleCommand.getFcDate() != null && !vehicleCommand.getFcDate().equals("")) {
                vehicleTO.setFcDate(vehicleCommand.getFcDate());
            }
            if (vehicleCommand.getFcReceiptNo() != null && !vehicleCommand.getFcReceiptNo().equals("")) {
                vehicleTO.setFcReceiptNo(vehicleCommand.getFcReceiptNo());
            }

            if (vehicleCommand.getFcExpiryDate() != null && !vehicleCommand.getFcExpiryDate().equals("")) {
                vehicleTO.setFcExpiryDate(vehicleCommand.getFcExpiryDate());
            }
            if (vehicleCommand.getFcAmount() != 0) {
                vehicleTO.setFcAmount(vehicleCommand.getFcAmount());
            }
            if (vehicleCommand.getFcRemarks() != null && !vehicleCommand.getFcRemarks().equals("")) {
                vehicleTO.setFcRemarks(vehicleCommand.getFcRemarks());
            }

            request.setAttribute("pageTitle", pageTitle);
            int insertStatus = 0;
            insertStatus = vehicleBP.saveVehicleFc(vehicleTO, Integer.parseInt(companyId), userId);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle FC Updated Successfully");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } /*catch (FPBusinessException exception) {
        // run time exception has occurred. Directed to error page.
        FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
        request.setAttribute(ParveenErrorConstants.ERROR_KEY,
        exception.getErrorMessage());
        } */ catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to View Vehicle Profile.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView viewVehicleProfile(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        String pageTitle = "View Vehicles";
        ModelAndView mv = new ModelAndView();

        try {
            String menuPath = "Vehicle  >>  View Vehicles  ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            CompanyTO companyTO = new CompanyTO();
            path = "content/Vehicle/VehicleProfile.jsp";

            vehicleTO.setVehicleId(request.getParameter("vehicleId"));
            System.out.println("Vehicle Id: " + vehicleTO.getVehicleId());
            ArrayList vehicleRegNos = new ArrayList();
            vehicleRegNos = vehicleBP.getVehicleRegNos();
            request.setAttribute("vehicleRegNos", vehicleRegNos);

            ArrayList vehicleProfileList = new ArrayList();
            vehicleProfileList = vehicleBP.getVehicleProfileDetails(vehicleTO);
            request.setAttribute("vehicleProfileList", vehicleProfileList);

            ArrayList tyreList = new ArrayList();
            tyreList = vehicleBP.getVehicleProfileTyre(vehicleTO);
            request.setAttribute("tyreList", tyreList);

            request.setAttribute("vehicleId", request.getParameter("vehicleId"));

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to vehicle Tracking System.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView vehicleTrackingSystem(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        System.out.println("------------------------");
        //        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        String userName = (String) session.getAttribute("userName");
        String companyId = (String) session.getAttribute("companyId");
        String pageTitle = "View Vehicles";
        ModelAndView mv = new ModelAndView();

        try {
            String menuPath = "Vehicle  >>  View Vehicles  ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            CompanyTO companyTO = new CompanyTO();
            path = "content/Vehicle/gpsPage.jsp";

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //handleRoutePage
    /**
     * This method used to Manage Route Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleRoutePage(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String menuPath = "Vehicle  >>  Route >>  View Route";

        try {
            /*if (!loginBP.checkAuthorisation(userFunctions, "Model-View")) {
            path = "content/common/NotAuthorized.jsp";
            } else {*/
            path = "content/route/manageRoute.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            String pageTitle = "View Route";
            request.setAttribute("pageTitle", pageTitle);

            ArrayList RouteList = new ArrayList();
            RouteList = vehicleBP.processActiveRouteList();
            request.setAttribute("RouteList", RouteList);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //alterRouteDetails
    /**
     * This method used to Manage Route Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView alterRoutes(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        System.out.println("alter");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "content/route/alterRoutes.jsp";

        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String menuPath = "Vehicle  >>  Route >>  View Route";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            String pageTitle = "View Route";
            request.setAttribute("pageTitle", pageTitle);

            String routeId = request.getParameter("routeId");
            System.out.println("v cont:=====--------> " + routeId);

            ArrayList routeDetails = new ArrayList();
            routeDetails = vehicleBP.alterRouteDetails(routeId);
            request.setAttribute("routeDetails", routeDetails);

            ArrayList cuslist = new ArrayList();
            cuslist = vehicleBP.getCusList();
            System.out.println("cuslist = " + cuslist.size());
            request.setAttribute("cuslist", cuslist);

            ArrayList PList = new ArrayList();
            PList = vehicleBP.getLPSProductList();
            request.setAttribute("PList", PList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method caters to view add route page
     *
     * @param request - Http request object
     *
     * @param response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public ModelAndView handleRouteAddPage(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String menuPath = "HRMS  >>  Manage Route  >>  Add";

        String pageTitle = "Add Model";
        request.setAttribute("pageTitle", pageTitle);
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            /*if (!loginBP.checkAuthorisation(userFunctions, "Model-Add")) {
            path = "content/common/NotAuthorized.jsp";
            } else {*/
            path = "content/route/addRoute.jsp";
            //}

            ArrayList cuslist = new ArrayList();
            cuslist = vehicleBP.getCusList();
            request.setAttribute("cuslist", cuslist);

            ArrayList PList = new ArrayList();
            PList = vehicleBP.getLPSProductList();
            request.setAttribute("PList", PList);

//            ArrayList productList = new ArrayList();
//            System.out.println("---------------2222222222");
//            productList = operationBP.getLPSProductList();
//            System.out.println("productList = " + productList.size());
//            request.setAttribute("productList", productList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to add lecture data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //handleAddModel
    /**
     * This method caters to add lecture
     *
     * @param request - Http request object
     *
     * @param response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public ModelAndView handleAddRoute(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        String pageTitle = "View Route";
        request.setAttribute("pageTitle", pageTitle);
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String menuPath = "";
        menuPath = "Vehicle  >>  Manage Route >> Add  ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            VehicleTO vehicleTO = new VehicleTO();

            if (vehicleCommand.getRouteCode() != null && vehicleCommand.getRouteCode() != "") {
                vehicleTO.setRouteCode(vehicleCommand.getRouteCode());
            }
//            if (vehicleCommand.getFromLocation() != null && vehicleCommand.getFromLocation() != "") {
//                vehicleTO.setFromLocation(vehicleCommand.getFromLocation());
//            }
//            if (vehicleCommand.getToLocation() != null && vehicleCommand.getToLocation() != "") {
//                vehicleTO.setToLocation(vehicleCommand.getToLocation());
//            }
            if (vehicleCommand.getViaRoute() != null && vehicleCommand.getViaRoute() != "") {
                vehicleTO.setViaRoute(vehicleCommand.getViaRoute());
            }
            if (vehicleCommand.getKm() != null && vehicleCommand.getKm() != "") {
                vehicleTO.setKm(vehicleCommand.getKm());
            }
            if (vehicleCommand.getTollAmount() != null && vehicleCommand.getTollAmount() != "") {
                vehicleTO.setTollAmount(vehicleCommand.getTollAmount());
            }
            if (vehicleCommand.getTonnageRate() != null && vehicleCommand.getTonnageRate() != "") {
                vehicleTO.setTonnageRate(vehicleCommand.getTonnageRate());
            }
            if (vehicleCommand.getTonnageRateMarket() != null && vehicleCommand.getTonnageRateMarket() != "") {
                vehicleTO.setTonnageRateMarket(vehicleCommand.getTonnageRateMarket());
            }
            if (vehicleCommand.getEligibleTrip() != null && vehicleCommand.getEligibleTrip() != "") {
                vehicleTO.setEligibleTrip(vehicleCommand.getEligibleTrip());
            }
            if (vehicleCommand.getDriverBata() != null && vehicleCommand.getDriverBata() != "") {
                vehicleTO.setDriverBata(vehicleCommand.getDriverBata());
            }
            if (vehicleCommand.getDescription() != null && vehicleCommand.getDescription() != "") {
                vehicleTO.setDescription(vehicleCommand.getDescription());
            }
            if (vehicleCommand.getToCity() != null && vehicleCommand.getToCity() != "") {
                vehicleTO.setToCity(vehicleCommand.getToCity());
            }
            if (vehicleCommand.getState() != null && vehicleCommand.getState() != "") {
                vehicleTO.setState(vehicleCommand.getState());
            }
            if (vehicleCommand.getState() != null && vehicleCommand.getState() != "") {
                vehicleTO.setState(vehicleCommand.getState());
            }
            if (vehicleCommand.getSla() != null && vehicleCommand.getSla() != "") {
                vehicleTO.setSla(vehicleCommand.getSla());
            }
            if (vehicleCommand.getTonnageRate() != null && vehicleCommand.getTonnageRate() != "") {
                vehicleTO.setTonnageRate(vehicleCommand.getTonnageRate());
            }
            if (vehicleCommand.getMarketVehiclePercentage() != null && vehicleCommand.getMarketVehiclePercentage() != "") {
                vehicleTO.setMarketVehiclePercentage(vehicleCommand.getMarketVehiclePercentage());
            }
            vehicleTO.setFromLocation(request.getParameter("floc"));
            vehicleTO.setFromLocationId(request.getParameter("flocID"));
            vehicleTO.setToLocation(request.getParameter("tloc"));
            System.out.println("request.getParameter= " + request.getParameter("tlocID"));
            vehicleTO.setToLocationId(request.getParameter("tlocID"));
            vehicleTO.setCustId(request.getParameter("custID"));
            vehicleTO.setProductId(request.getParameter("productId"));
            System.out.println("productId = " + request.getParameter("productId"));
            vehicleTO.setDepot(request.getParameter("Depot"));
            System.out.println("Depot = " + request.getParameter("Depot"));
            request.setAttribute("pageTitle", pageTitle);
            int insertStatus = 0;
            path = "content/route/manageRoute.jsp";
            insertStatus = vehicleBP.insertRouteDetails(vehicleTO, userId);
            System.out.println("insertStatus = " + insertStatus);
            request.removeAttribute("RouteList");
            ArrayList RouteList = new ArrayList();
            RouteList = vehicleBP.processActiveRouteList();
            request.setAttribute("RouteList", RouteList);
            if (insertStatus == 2) {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Route Already Exists ");
            } else {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Route Added Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //handleAlterModelPage
    /**
     * l
     * This method used to View Alter MFR page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAlterRoutePage(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        path = "content/route/alterRoute.jsp";
        try {
            /*if (!loginBP.checkAuthorisation(userFunctions, "Model-Alter")) {
            path = "content/common/NotAuthorized.jsp";
            } else {*/
            vehicleCommand = command;
            String menuPath = "Vehicle  >>  Route >>  Alter";

            ArrayList getRouteList = new ArrayList();
            getRouteList = vehicleBP.processGetRouteList();
            request.setAttribute("getRouteList", getRouteList);

            ArrayList PList = new ArrayList();
            PList = vehicleBP.getLPSProductList();
            request.setAttribute("PList", PList);

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Alter Route";
            request.setAttribute("pageTitle", pageTitle);

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //handleAlterModel
    /**
     * This method used to Modify MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAlterRoute(HttpServletRequest request, HttpServletResponse reponse, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        ModelAndView mv = new ModelAndView();
        HttpSession session = request.getSession();
        vehicleCommand = command;
        String path = "";
        path = "content/route/manageRoute.jsp";
        try {
            String menuPath = "  Vehicle  >>  Route >>  View    ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-Modify")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            int index = 0;
            int modify = 0;
            String[] routeIds = vehicleCommand.getRouteIds();
            String[] routeCodes = vehicleCommand.getRouteCodes();
            String[] fromocations = vehicleCommand.getFromLocations();
            String[] toLocations = vehicleCommand.getToLocations();
            String[] viaRoutes = vehicleCommand.getViaRoutes();
            String[] kms = vehicleCommand.getKms();
            String[] tollAmounts = vehicleCommand.getTollAmounts();
            String[] driverBatas = vehicleCommand.getDriverBatas();
            String[] activeStatus = vehicleCommand.getActiveInds();
            String[] selectedIndex = vehicleCommand.getSelectedIndex();

            int UserId = (Integer) session.getAttribute("userId");

            ArrayList List = new ArrayList();
            VehicleTO vehicleTO = null;
            for (int i = 0; i < selectedIndex.length; i++) {
                vehicleTO = new VehicleTO();
                index = Integer.parseInt(selectedIndex[i]);
                vehicleTO.setRouteId(routeIds[index]);
                System.out.println("routeIds[index]: " + routeIds[index]);
                vehicleTO.setRouteCode(routeCodes[index]);
                vehicleTO.setFromLocation(fromocations[index]);
                vehicleTO.setToLocation(toLocations[index]);
                vehicleTO.setViaRoute(viaRoutes[index]);
                vehicleTO.setKm(kms[index]);
                vehicleTO.setTollAmount(tollAmounts[index]);
                vehicleTO.setDriverBata(driverBatas[index]);
                vehicleTO.setActiveInd(activeStatus[index]);
                List.add(vehicleTO);
            }
            String pageTitle = "View model";
            request.setAttribute("pageTitle", pageTitle);
            modify = vehicleBP.processModifyRouteDetails(List, UserId);
            //mv = handleAlterRoutePage(request, reponse, command);
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Route Details Modified Successfully");
//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve designation --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //handleAlterModel
    /**
     * This method used to Modify MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView alterRouteInfo(HttpServletRequest request, HttpServletResponse reponse, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        ModelAndView mv = new ModelAndView();
        HttpSession session = request.getSession();
        vehicleCommand = command;
        String path = "";
        path = "content/route/manageRoute.jsp";
        String menuPath = "  Vehicle  >>  Route >>  View    ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        int userId = (Integer) session.getAttribute("userId");
        try {
            VehicleTO vehicleTO = new VehicleTO();
            if (vehicleCommand.getRouteId() != null && vehicleCommand.getRouteId() != "") {
                vehicleTO.setRouteId(vehicleCommand.getRouteId());
            }
            if (vehicleCommand.getRouteCode() != null && vehicleCommand.getRouteCode() != "") {
                vehicleTO.setRouteCode(vehicleCommand.getRouteCode());
            }
            if (vehicleCommand.getFromLocation() != null && vehicleCommand.getFromLocation() != "") {
                vehicleTO.setFromLocation(vehicleCommand.getFromLocation());
            }
            if (vehicleCommand.getToLocation() != null && vehicleCommand.getToLocation() != "") {
                vehicleTO.setToLocation(vehicleCommand.getToLocation());
            }
            if (vehicleCommand.getViaRoute() != null && vehicleCommand.getViaRoute() != "") {
                vehicleTO.setViaRoute(vehicleCommand.getViaRoute());
            }
            if (vehicleCommand.getKm() != null && vehicleCommand.getKm() != "") {
                vehicleTO.setKm(vehicleCommand.getKm());
            }
            if (vehicleCommand.getTollAmount() != null && vehicleCommand.getTollAmount() != "") {
                vehicleTO.setTollAmount(vehicleCommand.getTollAmount());
            }
            if (vehicleCommand.getTonnageRate() != null && vehicleCommand.getTonnageRate() != "") {
                vehicleTO.setTonnageRate(vehicleCommand.getTonnageRate());
            }
            if (vehicleCommand.getTonnageRateMarket() != null && vehicleCommand.getTonnageRateMarket() != "") {
                vehicleTO.setTonnageRateMarket(vehicleCommand.getTonnageRateMarket());
            }
            if (vehicleCommand.getEligibleTrip() != null && vehicleCommand.getEligibleTrip() != "") {
                vehicleTO.setEligibleTrip(vehicleCommand.getEligibleTrip());
            }
            if (vehicleCommand.getDriverBata() != null && vehicleCommand.getDriverBata() != "") {
                vehicleTO.setDriverBata(vehicleCommand.getDriverBata());
            }
            if (vehicleCommand.getDescription() != null && vehicleCommand.getDescription() != "") {
                vehicleTO.setDescription(vehicleCommand.getDescription());
            }
            if (vehicleCommand.getToCity() != null && vehicleCommand.getToCity() != "") {
                vehicleTO.setToCity(vehicleCommand.getToCity());
            }
            if (vehicleCommand.getState() != null && vehicleCommand.getState() != "") {
                vehicleTO.setState(vehicleCommand.getState());
            }
            if (vehicleCommand.getSla() != null && vehicleCommand.getSla() != "") {
                vehicleTO.setSla(vehicleCommand.getSla());
            }
            if (vehicleCommand.getTonnageRate() != null && vehicleCommand.getTonnageRate() != "") {
                vehicleTO.setTonnageRate(vehicleCommand.getTonnageRate());
            }
            if (vehicleCommand.getMarketVehiclePercentage() != null && vehicleCommand.getMarketVehiclePercentage() != "") {
                vehicleTO.setMarketVehiclePercentage(vehicleCommand.getMarketVehiclePercentage());
            }
            vehicleTO.setFromLocation(request.getParameter("floc"));
            vehicleTO.setFromLocationId(request.getParameter("flocID"));
            vehicleTO.setToLocation(request.getParameter("tloc"));
            vehicleTO.setToLocationId(request.getParameter("tlocID"));
            System.out.println("request.getParameter= " + request.getParameter("tlocID"));
            vehicleTO.setCustId(request.getParameter("custID"));
            vehicleTO.setProductId(request.getParameter("productId"));
            vehicleTO.setDepotVal(request.getParameter("Depot"));
            int mStatus = 0;
            path = "content/route/manageRoute.jsp";

            String pageTitle = "View model";
            request.setAttribute("pageTitle", pageTitle);
            mStatus = vehicleBP.ModifyRouteDetails(vehicleTO, userId);

            request.removeAttribute("RouteList");
            ArrayList RouteList = new ArrayList();
            RouteList = vehicleBP.processActiveRouteList();
            request.setAttribute("RouteList", RouteList);
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Route Modified Successfully");
//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve designation --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //viewVehicleLubrication
    /**
     * This method used to Modify MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView viewVehicleLubrication(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        path = "content/Vehicle/manageLubrication.jsp";
        try {
            String pageTitle = "Alter Route";
            request.setAttribute("pageTitle", pageTitle);

            vehicleCommand = command;
            String menuPath = "Vehicle  >>  Lubrication >>  Summary";

            String vehicleNo = request.getParameter("vehicleNo");

            ArrayList vehicleLubri = new ArrayList();
            vehicleLubri = vehicleBP.viewVehicleLubrication(vehicleNo);
            request.setAttribute("vehicleLubri", vehicleLubri);

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //searchVehicleFinance
    /**
     * This method used to Vehicle List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView searchVehicleFinance(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "Vehicle  >>  View Vehicle Finance";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        vehicleCommand = command;
        VehicleTO vehicleTO = new VehicleTO();
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Vehicle-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "View Vehicle Finance";
                request.setAttribute("pageTitle", pageTitle);
                path = "content/Vehicle/viewVehicleFinance.jsp";

            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //vehicleFinanceList
    /**
     * This method used to Vehicle Insurances List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView vehicleFinanceList(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String temp = "";
        ArrayList vehicleList = new ArrayList();
        String menuPath = "Vehicle  >>  View Vehicle Finance List";
        vehicleCommand = command;
        VehicleTO vehicleTO = new VehicleTO();
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        try {
            path = "content/Vehicle/viewVehicleFinance.jsp";
            if (vehicleCommand.getUsageId().equalsIgnoreCase("0")) {
                vehicleTO.setUsageId("");
            } else {
                vehicleTO.setUsageId(vehicleCommand.getUsageId());
            }
            if (vehicleCommand.getRegNo() != null && vehicleCommand.getRegNo() != "") {
                vehicleTO.setRegNo(vehicleCommand.getRegNo());
            } else {
                vehicleTO.setRegNo("");
            }
            int UserId = (Integer) session.getAttribute("userId");
            int totalRecords = 0;
            int pageNo = 0;
            int totalPages = 0;
            PaginationHelper pagenation = new PaginationHelper();
            int startIndex = 0;
            int endIndex = 0;

            vehicleTO.setStartIndex(startIndex);
            vehicleTO.setEndIndex(endIndex);

            ArrayList VehicleInsList = new ArrayList();
            VehicleInsList = vehicleBP.getVehicleFinanceList(vehicleTO, UserId);
            request.setAttribute("VehicleInsList", VehicleInsList);

            request.setAttribute("regNo", vehicleCommand.getRegNo());
            request.setAttribute("companyname", vehicleCommand.getCompanyname());
            request.setAttribute("premiumno", vehicleCommand.getPremiumno());
            request.setAttribute("premiumpaiddate", vehicleCommand.getPremiumpaiddate());
            request.setAttribute("premiumamount", vehicleCommand.getPremiumamount());
            request.setAttribute("vehiclevalue", vehicleCommand.getVehiclevalue());
            request.setAttribute("expirydate", vehicleCommand.getExpirydate());
            request.setAttribute("ncb", vehicleCommand.getNcb());
            request.setAttribute("remarks", vehicleCommand.getRemarks());

            vehicleList = vehicleBP.processVehicleList(vehicleTO);
            totalRecords = vehicleList.size();
            pagenation.setTotalRecords(totalRecords);

            String buttonClicked = "";
            if (request.getParameter("button") != null && request.getParameter("button") != "") {
                buttonClicked = request.getParameter("button");
            }
            if (request.getParameter("pageNo") != null && request.getParameter("pageNo") != "") {
                pageNo = Integer.parseInt(request.getParameter("pageNo"));
            }
            pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
            totalPages = pagenation.getTotalNoOfPages();
            request.setAttribute("pageNo", pageNo);
            System.out.println("pageNo" + pageNo);
            request.setAttribute("totalPages", totalPages);
            startIndex = pagenation.getStartIndex();
            endIndex = pagenation.getEndIndex();

            vehicleTO.setStartIndex(startIndex);
            vehicleTO.setEndIndex(endIndex);
            vehicleList = vehicleBP.processVehicleList(vehicleTO);
            if (totalRecords <= 0) {
                vehicleList = null;
            }
            request.setAttribute("vehicleList", vehicleList);
            System.out.println("start Index=" + vehicleTO.getStartIndex());
            System.out.println("end Index=" + vehicleTO.getEndIndex());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            String pageTitle = "View Vehicles";
            request.setAttribute("pageTitle", pageTitle);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVehicleFinance(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        String pageTitle = "View Vehicles";
        ModelAndView mv = new ModelAndView();
        try {
            String menuPath = "Vehicle  >>  Finance Details ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            path = "content/Vehicle/VehicleFinanceUpdate.jsp";
            request.setAttribute("pageTitle", pageTitle);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle finance page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleFleetPurchase(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        String pageTitle = "View Vehicles";
        ModelAndView mv = new ModelAndView();
        try {
            String menuPath = "Vehicle  >>  Finance Details ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            path = "content/Vehicle/fleetPurchase.jsp";
            request.setAttribute("pageTitle", pageTitle);

            String addStatus = request.getParameter("addStatus");
            String make = request.getParameter("make");
            String model = request.getParameter("model");
            String vehicleId = request.getParameter("vehicleId");
            String trailerId = request.getParameter("trailerId");
            String regNo = request.getParameter("regNo");
            String doorNo = request.getParameter("doorNo");


            System.out.println("regNo =" + regNo);
            System.out.println("make =" + make);
            System.out.println("model =" + model);
            System.out.println("doorNo =" + doorNo);
            System.out.println("trailerId =" + trailerId);
            request.setAttribute("regNo", regNo);
            request.setAttribute("doorNo", doorNo);
            request.setAttribute("make", make);
            request.setAttribute("model", model);
            request.setAttribute("vehicleId", vehicleId);
            request.setAttribute("trailerId", trailerId);
            if ("1".equals(addStatus)) {
                ArrayList primarybankList = new ArrayList();
                primarybankList = vehicleBP.getPrimarybankList();
                request.setAttribute("primarybankList", primarybankList);
                System.out.println("primarybankList.size()=" + primarybankList.size());

                String vendorTypeId = ThrottleConstants.fleetPurchaseVendorTypeId;
                ArrayList vendorList = new ArrayList();
                vendorList = vehicleBP.vendorList(vendorTypeId);
                request.setAttribute("vendorList", vendorList);
                path = "content/Vehicle/fleetPurchase.jsp";
            } else if ("2".equals(addStatus)) {
                path = "content/Vehicle/fleetPurchaseView.jsp";
                //fetch view details
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle finance page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveVehicleFinance(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        String pageTitle = "View Vehicles";
        ModelAndView mv = new ModelAndView();
        try {
            String menuPath = "Vehicle  >>  Finance ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            path = "content/Vehicle/VehicleFinanceUpdate.jsp";
            if (vehicleCommand.getVehicleId() != null && !vehicleCommand.getVehicleId().equals("")) {
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }
            if (vehicleCommand.getBankerName() != null && !vehicleCommand.getBankerName().equals("")) {
                vehicleTO.setBankerName(vehicleCommand.getBankerName());
            }
            if (vehicleCommand.getBankerAddress() != null && !vehicleCommand.getBankerAddress().equals("")) {
                vehicleTO.setBankerAddress(vehicleCommand.getBankerAddress());
            }

            if (vehicleCommand.getFinanceAmount() != null && !vehicleCommand.getFinanceAmount().equals("")) {
                vehicleTO.setFinanceAmount(vehicleCommand.getFinanceAmount());
            }
            if (vehicleCommand.getRoi() != null && !vehicleCommand.getRoi().equals("")) {
                vehicleTO.setRoi(vehicleCommand.getRoi());
            }
            if (vehicleCommand.getInterestType() != null && !vehicleCommand.getInterestType().equals("")) {
                vehicleTO.setInterestType(vehicleCommand.getInterestType());
            }
            if (vehicleCommand.getEmiMonths() != null && !vehicleCommand.getEmiMonths().equals("")) {
                vehicleTO.setEmiMonths(vehicleCommand.getEmiMonths());
            }
            if (vehicleCommand.getEmiAmount() != null && !vehicleCommand.getEmiAmount().equals("")) {
                vehicleTO.setEmiAmount(vehicleCommand.getEmiAmount());
            }
            if (vehicleCommand.getEmiStartDate() != null && !vehicleCommand.getEmiStartDate().equals("")) {
                vehicleTO.setEmiStartDate(vehicleCommand.getEmiStartDate());
            }
            if (vehicleCommand.getEmiEndDate() != null && !vehicleCommand.getEmiEndDate().equals("")) {
                vehicleTO.setEmiEndDate(vehicleCommand.getEmiEndDate());
            }
            if (vehicleCommand.getEmiPayDay() != null && !vehicleCommand.getEmiPayDay().equals("")) {
                vehicleTO.setEmiPayDay(vehicleCommand.getEmiPayDay());
            }
            if (vehicleCommand.getPayMode() != null && !vehicleCommand.getPayMode().equals("")) {
                vehicleTO.setPayMode(vehicleCommand.getPayMode());
            }
            request.setAttribute("pageTitle", pageTitle);
            int insertStatus = 0;
            insertStatus = vehicleBP.saveVehicleFinance(vehicleTO, Integer.parseInt(companyId), userId);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Finance Details Updated Successfully");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } /*catch (FPBusinessException exception) {
        // run time exception has occurred. Directed to error page.
        FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
        request.setAttribute(ParveenErrorConstants.ERROR_KEY,
        exception.getErrorMessage());
        } */ catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveFleetPurchase(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        String pageTitle = "View Vehicles";
        ModelAndView mv = new ModelAndView();
        try {
            String menuPath = "Vehicle  >>  Finance ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            path = "content/Vehicle/VehicleFinanceUpdate.jsp";
            if (vehicleCommand.getVehicleId() != null && !vehicleCommand.getVehicleId().equals("")) {
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }

            if (vehicleCommand.getFinanceAmount() != null && !vehicleCommand.getFinanceAmount().equals("")) {
                vehicleTO.setFinanceAmount(vehicleCommand.getFinanceAmount());
            }
            if (vehicleCommand.getRoi() != null && !vehicleCommand.getRoi().equals("")) {
                vehicleTO.setRoi(vehicleCommand.getRoi());
            }
            if (vehicleCommand.getInterestType() != null && !vehicleCommand.getInterestType().equals("")) {
                vehicleTO.setInterestType(vehicleCommand.getInterestType());
            }
            if (vehicleCommand.getEmiMonths() != null && !vehicleCommand.getEmiMonths().equals("")) {
                vehicleTO.setEmiMonths(vehicleCommand.getEmiMonths());
            }
            if (vehicleCommand.getEmiAmount() != null && !vehicleCommand.getEmiAmount().equals("")) {
                vehicleTO.setEmiAmount(vehicleCommand.getEmiAmount());
            }
            if (vehicleCommand.getEmiStartDate() != null && !vehicleCommand.getEmiStartDate().equals("")) {
                vehicleTO.setEmiStartDate(vehicleCommand.getEmiStartDate());
            }
            if (vehicleCommand.getEmiEndDate() != null && !vehicleCommand.getEmiEndDate().equals("")) {
                vehicleTO.setEmiEndDate(vehicleCommand.getEmiEndDate());
            }
            if (vehicleCommand.getEmiPayDay() != null && !vehicleCommand.getEmiPayDay().equals("")) {
                vehicleTO.setEmiPayDay(vehicleCommand.getEmiPayDay());
            }
            if (vehicleCommand.getPayMode() != null && !vehicleCommand.getPayMode().equals("")) {
                vehicleTO.setPayMode(vehicleCommand.getPayMode());
            }

            String trailerId = request.getParameter("trailerId");
            String vendorId = request.getParameter("vendorId");
            String fleetCost = request.getParameter("fleetCost");
            String otherCost = request.getParameter("otherCost");
            String totalCost = request.getParameter("totalCost");
            String financeStatus = request.getParameter("financeStatus");
            String bankId = request.getParameter("bankId");
            String initialAmount = request.getParameter("initialAmount");
            String processingFee = request.getParameter("processingFee");
            String otherCharges = request.getParameter("otherCharges");
            String remarks = request.getParameter("remarks");
            String regno = request.getParameter("regNo");

            vehicleTO.setRegno(regno);
            vehicleTO.setRemarks(remarks);
            vehicleTO.setTrailerId(trailerId);
            vehicleTO.setVendorId(vendorId);
            vehicleTO.setFleetCost(fleetCost);
            vehicleTO.setOtherCost(otherCost);
            vehicleTO.setTotalCost(totalCost);
            vehicleTO.setFinanceStatus(financeStatus);
            vehicleTO.setBankId(bankId);
            vehicleTO.setInitialAmount(initialAmount);
            vehicleTO.setProcessingFee(processingFee);
            vehicleTO.setOtherCharges(otherCharges);

            request.setAttribute("pageTitle", pageTitle);
            int insertStatus = 0;
            insertStatus = vehicleBP.saveFleetPurchase(vehicleTO, Integer.parseInt(companyId), userId);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Purchase Details Updated Successfully");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            //forward to view screen
            request.setAttribute("listId", "5");
            mv = handleSearchVehiclePage(request, response, command);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } /*catch (FPBusinessException exception) {
        // run time exception has occurred. Directed to error page.
        FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
        request.setAttribute(ParveenErrorConstants.ERROR_KEY,
        exception.getErrorMessage());
        } */ catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        //return new ModelAndView(path);
        return mv;
    }

    public ModelAndView vehicleFinanceDetail(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        System.out.println("Test=======");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String pageTitle = "";
        String menuPath = "Vehicle  >>  Vehicle Finance Update";
        vehicleCommand = command;
        ArrayList vehicleFinance = new ArrayList();
        ArrayList vehicleDetail = new ArrayList();
        VehicleTO vehicleTO = null;
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Vehicle-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                pageTitle = "Alter Vehicle Finance";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                request.setAttribute("pageTitle", pageTitle);
                path = "content/Vehicle/alterVehicleFinance.jsp";

                String financeId = request.getParameter("financeId");
                System.out.println("financeId = " + financeId);
                String vehicleId = request.getParameter("vehicleId");
                System.out.println("vehicleId = " + vehicleId);

                vehicleTO = new VehicleTO();
                if (vehicleId != null && vehicleId != "") {
                    System.out.println("===in vehicle===" + vehicleId);
                    vehicleTO.setVehicleId(vehicleId);
                }
                if (financeId != null && financeId != "") {
                    System.out.println("===in vehicle===" + financeId);
                    vehicleTO.setFinanceId(financeId);
                }
                vehicleDetail = vehicleBP.updateVehicleDetail(vehicleTO);
                request.setAttribute("vehicleDetail", vehicleDetail);
                System.out.println("vehicleDetail.size() in cont= " + vehicleDetail.size());

                vehicleFinance = vehicleBP.vehicleFinanceDetail(vehicleTO);
                request.setAttribute("vehicleFinance", vehicleFinance);
                System.out.println("vehicleFinance.size() in cont= " + vehicleFinance.size());
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //updateVehicleFinance
    /**
     * This method used to Insert MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView updateVehicleFinance(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String pageTitle = "View Vehicles Finanace";

        try {
            String menuPath = "Vehicle  >>  View Vehicle's Finance";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            CompanyTO companyTO = new CompanyTO();
            path = "content/Vehicle/viewVehicleFinance.jsp";

            if (vehicleCommand.getVehicleId() != null && !vehicleCommand.getVehicleId().equals("")) {
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }
            if (vehicleCommand.getBankerName() != null && !vehicleCommand.getBankerName().equals("")) {
                vehicleTO.setBankerName(vehicleCommand.getBankerName());
            }
            if (vehicleCommand.getBankerAddress() != null && !vehicleCommand.getBankerAddress().equals("")) {
                vehicleTO.setBankerAddress(vehicleCommand.getBankerAddress());
            }

            if (vehicleCommand.getFinanceAmount() != null && !vehicleCommand.getFinanceAmount().equals("")) {
                vehicleTO.setFinanceAmount(vehicleCommand.getFinanceAmount());
            }
            if (vehicleCommand.getRoi() != null && !vehicleCommand.getRoi().equals("")) {
                vehicleTO.setRoi(vehicleCommand.getRoi());
            }
            if (vehicleCommand.getInterestType() != null && !vehicleCommand.getInterestType().equals("")) {
                vehicleTO.setInterestType(vehicleCommand.getInterestType());
            }
            if (vehicleCommand.getEmiMonths() != null && !vehicleCommand.getEmiMonths().equals("")) {
                vehicleTO.setEmiMonths(vehicleCommand.getEmiMonths());
            }
            if (vehicleCommand.getEmiAmount() != null && !vehicleCommand.getEmiAmount().equals("")) {
                vehicleTO.setEmiAmount(vehicleCommand.getEmiAmount());
            }
            if (vehicleCommand.getEmiStartDate() != null && !vehicleCommand.getEmiStartDate().equals("")) {
                vehicleTO.setEmiStartDate(vehicleCommand.getEmiStartDate());
            }
            if (vehicleCommand.getEmiEndDate() != null && !vehicleCommand.getEmiEndDate().equals("")) {
                vehicleTO.setEmiEndDate(vehicleCommand.getEmiEndDate());
            }
            if (vehicleCommand.getEmiPayDay() != null && !vehicleCommand.getEmiPayDay().equals("")) {
                vehicleTO.setEmiPayDay(vehicleCommand.getEmiPayDay());
            }
            if (vehicleCommand.getPayMode() != null && !vehicleCommand.getPayMode().equals("")) {
                vehicleTO.setPayMode(vehicleCommand.getPayMode());
            }
            String companyId = "0";
            request.setAttribute("pageTitle", pageTitle);
            int insertStatus = 0;
            insertStatus = vehicleBP.saveVehicleFinance(vehicleTO, Integer.parseInt(companyId), userId);

            if (insertStatus == 1) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Finance Modified Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //handleSearchVehicleInsuranct
    /**
     * This method used to Vehicle List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView searchVehicleInsurance(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "Vehicle  >>  View Vehicles Insurance";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        vehicleCommand = command;
        VehicleTO vehicleTO = new VehicleTO();
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Vehicle-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "View Vehicles Insurance";
                request.setAttribute("pageTitle", pageTitle);
                path = "content/Vehicle/viewVehicleInsurance.jsp";

            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //vehicleInsuranceList
    /**
     * This method used to Vehicle Insurances List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView vehicleInsuranceList(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String temp = "";
        ArrayList vehicleList = new ArrayList();
        String menuPath = "Vehicle  >>  View Vehicle Insurance List";
        vehicleCommand = command;
        VehicleTO vehicleTO = new VehicleTO();
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        try {
            path = "content/Vehicle/viewVehicleInsurance.jsp";
            if (vehicleCommand.getUsageId().equalsIgnoreCase("0")) {
                vehicleTO.setUsageId("");
            } else {
                vehicleTO.setUsageId(vehicleCommand.getUsageId());
            }
            if (vehicleCommand.getRegNo() != null && vehicleCommand.getRegNo() != "") {
                vehicleTO.setRegNo(vehicleCommand.getRegNo());
            } else {
                vehicleTO.setRegNo("");
            }
            int UserId = (Integer) session.getAttribute("userId");
            int totalRecords = 0;
            int pageNo = 0;
            int totalPages = 0;
            PaginationHelper pagenation = new PaginationHelper();
            int startIndex = 0;
            int endIndex = 0;

            vehicleTO.setStartIndex(startIndex);
            vehicleTO.setEndIndex(endIndex);

            ArrayList VehicleInsList = new ArrayList();
            VehicleInsList = vehicleBP.getVehicleInsuranceList(vehicleTO);
            request.setAttribute("VehicleInsList", VehicleInsList);

            request.setAttribute("regNo", vehicleCommand.getRegNo());
            request.setAttribute("companyname", vehicleCommand.getCompanyname());
            request.setAttribute("premiumno", vehicleCommand.getPremiumno());
            request.setAttribute("premiumpaiddate", vehicleCommand.getPremiumpaiddate());
            request.setAttribute("premiumamount", vehicleCommand.getPremiumamount());
            request.setAttribute("vehiclevalue", vehicleCommand.getVehiclevalue());
            request.setAttribute("expirydate", vehicleCommand.getExpirydate());
            request.setAttribute("ncb", vehicleCommand.getNcb());
            request.setAttribute("remarks", vehicleCommand.getRemarks());

            vehicleList = vehicleBP.processVehicleList(vehicleTO);
            totalRecords = vehicleList.size();
            pagenation.setTotalRecords(totalRecords);

            String buttonClicked = "";
            if (request.getParameter("button") != null && request.getParameter("button") != "") {
                buttonClicked = request.getParameter("button");
            }
            if (request.getParameter("pageNo") != null && request.getParameter("pageNo") != "") {
                pageNo = Integer.parseInt(request.getParameter("pageNo"));
            }
            pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
            totalPages = pagenation.getTotalNoOfPages();
            request.setAttribute("pageNo", pageNo);
            System.out.println("pageNo" + pageNo);
            request.setAttribute("totalPages", totalPages);
            startIndex = pagenation.getStartIndex();
            endIndex = pagenation.getEndIndex();

            vehicleTO.setStartIndex(startIndex);
            vehicleTO.setEndIndex(endIndex);
            vehicleList = vehicleBP.processVehicleList(vehicleTO);
            if (totalRecords <= 0) {
                vehicleList = null;
            }
            request.setAttribute("vehicleList", vehicleList);
            System.out.println("start Index=" + vehicleTO.getStartIndex());
            System.out.println("end Index=" + vehicleTO.getEndIndex());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            String pageTitle = "View Vehicles";
            request.setAttribute("pageTitle", pageTitle);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //vehicleAMCList
    /**
     * This method used to Vehicle Insurances List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView vehicleAMCList(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String temp = "";
        ArrayList vehicleList = new ArrayList();
        String menuPath = "Vehicle  >>  View Vehicle AMC List";
        vehicleCommand = command;
        VehicleTO vehicleTO = new VehicleTO();
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        try {
            path = "content/Vehicle/viewVehicleAMC.jsp";
            if (vehicleCommand.getUsageId().equalsIgnoreCase("0")) {
                vehicleTO.setUsageId("");
            } else {
                vehicleTO.setUsageId(vehicleCommand.getUsageId());
            }
            if (vehicleCommand.getRegNo() != null && vehicleCommand.getRegNo() != "") {
                vehicleTO.setRegNo(vehicleCommand.getRegNo());
            } else {
                vehicleTO.setRegNo("");
            }
            int UserId = (Integer) session.getAttribute("userId");
            int totalRecords = 0;
            int pageNo = 0;
            int totalPages = 0;
            PaginationHelper pagenation = new PaginationHelper();
            int startIndex = 0;
            int endIndex = 0;

            vehicleTO.setStartIndex(startIndex);
            vehicleTO.setEndIndex(endIndex);

            ArrayList vehicleAmcList = new ArrayList();
            vehicleAmcList = vehicleBP.getVehicleAMCList(vehicleTO);
            request.setAttribute("vehicleAmcList", vehicleAmcList);

            request.setAttribute("regNo", vehicleCommand.getRegNo());
            request.setAttribute("companyname", vehicleCommand.getCompanyname());
            request.setAttribute("premiumno", vehicleCommand.getPremiumno());
            request.setAttribute("premiumpaiddate", vehicleCommand.getPremiumpaiddate());
            request.setAttribute("premiumamount", vehicleCommand.getPremiumamount());
            request.setAttribute("vehiclevalue", vehicleCommand.getVehiclevalue());
            request.setAttribute("expirydate", vehicleCommand.getExpirydate());
            request.setAttribute("ncb", vehicleCommand.getNcb());
            request.setAttribute("remarks", vehicleCommand.getRemarks());

            vehicleList = vehicleBP.processVehicleList(vehicleTO);
            totalRecords = vehicleList.size();
            pagenation.setTotalRecords(totalRecords);

            String buttonClicked = "";
            if (request.getParameter("button") != null && request.getParameter("button") != "") {
                buttonClicked = request.getParameter("button");
            }
            if (request.getParameter("pageNo") != null && request.getParameter("pageNo") != "") {
                pageNo = Integer.parseInt(request.getParameter("pageNo"));
            }
            pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
            totalPages = pagenation.getTotalNoOfPages();
            request.setAttribute("pageNo", pageNo);
            System.out.println("pageNo" + pageNo);
            request.setAttribute("totalPages", totalPages);
            startIndex = pagenation.getStartIndex();
            endIndex = pagenation.getEndIndex();

            vehicleTO.setStartIndex(startIndex);
            vehicleTO.setEndIndex(endIndex);
            vehicleList = vehicleBP.processVehicleList(vehicleTO);
            if (totalRecords <= 0) {
                vehicleList = null;
            }
            request.setAttribute("vehicleList", vehicleList);
            System.out.println("start Index=" + vehicleTO.getStartIndex());
            System.out.println("end Index=" + vehicleTO.getEndIndex());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            String pageTitle = "View Vehicles";
            request.setAttribute("pageTitle", pageTitle);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //updateVehicleAMC
    /**
     * This method used to Insert MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView updateVehicleAMC(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String pageTitle = "View Vehicles AMC";

        try {
            String menuPath = "Vehicle  >>  View Vehicle's AMC";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            CompanyTO companyTO = new CompanyTO();
            path = "content/Vehicle/viewVehicleAMC.jsp";

            if (vehicleCommand.getAmcCompanyName() != null && vehicleCommand.getAmcCompanyName() != "") {
                vehicleTO.setAmcCompanyName(vehicleCommand.getAmcCompanyName());
            }
            if (vehicleCommand.getAmcAmount() != null && vehicleCommand.getAmcAmount() != "") {
                vehicleTO.setAmcAmount(vehicleCommand.getAmcAmount());
            }
            if (vehicleCommand.getAmcDuration() != null && vehicleCommand.getAmcDuration() != "") {
                vehicleTO.setAmcDuration(vehicleCommand.getAmcDuration());
            }
            String[] temp = null;
            String frDate = "", tDt = "";
            if (vehicleCommand.getAmcChequeDate() != null && vehicleCommand.getAmcChequeDate() != "") {
                temp = vehicleCommand.getAmcChequeDate().split("-");
                String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
                vehicleTO.setAmcChequeDate(sDate);
            }
            if (vehicleCommand.getAmcFromDate() != null && vehicleCommand.getAmcFromDate() != "") {
                temp = vehicleCommand.getAmcFromDate().split("-");
                String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
                vehicleTO.setAmcFromDate(sDate);

            }
            if (vehicleCommand.getAmcToDate() != null && vehicleCommand.getAmcToDate() != "") {
                temp = vehicleCommand.getAmcToDate().split("-");
                String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
                vehicleTO.setAmcToDate(sDate);
            }
            if (vehicleCommand.getRemarks() != null && vehicleCommand.getRemarks() != "") {
                vehicleTO.setRemarks(vehicleCommand.getRemarks());
            }
            if (vehicleCommand.getVehicleId() != null && vehicleCommand.getVehicleId() != "") {
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }
            if (vehicleCommand.getAmcId() != null && vehicleCommand.getAmcId() != "") {
                vehicleTO.setAmcId(vehicleCommand.getAmcId());
            }

            request.setAttribute("pageTitle", pageTitle);
            int insertStatus = 0;
            System.out.println("userId====1" + userId);

            insertStatus = vehicleBP.processUpdateVehicleAMC(vehicleTO, userId);
            System.out.println("userId====2" + userId);
            if (insertStatus == 1) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle AMC Modified Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView vehicleInsuranceDetail(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        System.out.println("Test=======");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";
        String pageTitle = "";
        String menuPath = "Vehicle  >>  Vehicle Insurance Update";
        vehicleCommand = command;
        ArrayList vehicleInsurance = new ArrayList();
        ArrayList vehicleDetail = new ArrayList();
        VehicleTO vehicleTO = null;
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Vehicle-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                pageTitle = "Alter Vehicle Insurance";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                request.setAttribute("pageTitle", pageTitle);
                path = "content/Vehicle/alterVehicleInsurance.jsp";

                String insuranceid = request.getParameter("insuranceid");
                System.out.println("insuranceid = " + insuranceid);
                String vehicleId = request.getParameter("vehicleId");
                System.out.println("vehicleId = " + vehicleId);

                vehicleTO = new VehicleTO();
                if (vehicleId != null && vehicleId != "") {
                    System.out.println("===in vehicle===" + vehicleId);
                    vehicleTO.setVehicleId(vehicleId);
                }

//                vehicleTO = new VehicleTO();
                if (insuranceid != null && insuranceid != "") {
                    System.out.println("===in vehicle===" + insuranceid);
                    vehicleTO.setInsuranceid(insuranceid);
                }

//
                vehicleDetail = vehicleBP.updateVehicleDetail(vehicleTO);
                request.setAttribute("vehicleDetail", vehicleDetail);
                System.out.println("vehicleDetail.size() in cont= " + vehicleDetail.size());

                vehicleInsurance = vehicleBP.vehicleInsuranceDetail(vehicleTO);
                request.setAttribute("vehicleInsurance", vehicleInsurance);
                System.out.println("vehicleInsurance.size() in cont= " + vehicleInsurance.size());
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //updateVehicleInsurance
    /**
     * This method used to Insert MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView updateVehicleInsurance(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        System.out.println("in test demo-----------------------------");
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String pageTitle = "View Vehicles Insurance";
        ModelAndView mv = new ModelAndView();

        try {
            String menuPath = "Vehicle  >>  View Vehicles insurance  ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            CompanyTO companyTO = new CompanyTO();
            path = "content/Vehicle/viewVehicleInsurance.jsp";
            if (vehicleCommand.getUsageId() != null && vehicleCommand.getUsageId() != "") {
                vehicleTO.setUsageId(vehicleCommand.getUsageId());
            }
            if (vehicleCommand.getInsuranceCompanyName() != null && vehicleCommand.getInsuranceCompanyName() != "") {
                vehicleTO.setInsuranceCompanyName(vehicleCommand.getInsuranceCompanyName());
            }
            if (vehicleCommand.getPremiumNo() != null && vehicleCommand.getPremiumNo() != "") {
                vehicleTO.setPremiumNo(vehicleCommand.getPremiumNo());
            }
            if (vehicleCommand.getPremiumPaidAmount() != 0) {
                vehicleTO.setPremiumPaidAmount(vehicleCommand.getPremiumPaidAmount());
            }
            String[] temp = null;
            String frDate = "", tDt = "";
            if (vehicleCommand.getPremiumPaidDate() != "" && vehicleCommand.getPremiumExpiryDate() != "") {
                temp = vehicleCommand.getPremiumPaidDate().split("-");
                String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
                frDate = sDate;

                temp = vehicleCommand.getPremiumExpiryDate().split("-");
                String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
                tDt = eDate;
            }
            if (frDate != null && frDate != "") {
                vehicleTO.setPremiumPaidDate(frDate);
            }
            if (vehicleCommand.getVehicleValue() != 0) {
                vehicleTO.setVehicleValue(vehicleCommand.getVehicleValue());
            }
            if (tDt != null && tDt != "") {
                vehicleTO.setPremiumExpiryDate(tDt);
            }
            if (vehicleCommand.getInsuarnceNCB() != null && vehicleCommand.getRegNo() != "") {
                vehicleTO.setRegNo(vehicleCommand.getRegNo());
            }
            if (vehicleCommand.getInsuarnceRemarks() != null && vehicleCommand.getInsuarnceRemarks() != "") {
                vehicleTO.setInsuarnceRemarks(vehicleCommand.getInsuarnceRemarks());
            }
            if (vehicleCommand.getVehicleId() != null && vehicleCommand.getVehicleId() != "") {
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }
            if (vehicleCommand.getInsuranceid() != null && vehicleCommand.getInsuranceid() != "") {
                vehicleTO.setInsuranceid(vehicleCommand.getInsuranceid());
                System.out.println("vehicleCommand.getInsuranceid() 1= " + vehicleCommand.getInsuranceid());
            }
            System.out.println("vehicleTO.getInsuranceid() 2= " + vehicleTO.getInsuranceid());

            request.setAttribute("pageTitle", pageTitle);
            int insertStatus = 0;

//            System.out.println("vehicleTO.setItemIds=" + vehicleTO.getItemIds());
            System.out.println("userId====1" + userId);

            insertStatus = vehicleBP.processUpdateVehicleInsurance(vehicleTO, userId);
            System.out.println("userId====2" + userId);
            if (insertStatus == 1) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Insurance Modified Successfully");
            }
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            mv = searchVehicleInsurance(request, response, command);
            //request.removeAttribute("AddVehicleList");
            // ArrayList AddVehicleList = new ArrayList();
            // AddVehicleList = vehicleBP.processGetAddVehicleList();
            // request.setAttribute("AddVehicleList", AddVehicleList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView searchVehicleRoadTax(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "Vehicle  >>  View Vehicles Insurance";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        vehicleCommand = command;
        VehicleTO vehicleTO = new VehicleTO();
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Vehicle-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "View Vehicles Insurance";
                request.setAttribute("pageTitle", pageTitle);
                path = "content/Vehicle/viewVehicleTax.jsp";

            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //vehicleInsuranceList
    /**
     * This method used to Vehicle Insurances List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView vehicleTaxList(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String temp = "";
        ArrayList vehicleList = new ArrayList();
        String menuPath = "Vehicle  >>  View Road Tax List";
        vehicleCommand = command;
        VehicleTO vehicleTO = new VehicleTO();
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        try {
            path = "content/Vehicle/viewVehicleTax.jsp";
            if (vehicleCommand.getUsageId().equalsIgnoreCase("0")) {
                vehicleTO.setUsageId("");
            } else {
                vehicleTO.setUsageId(vehicleCommand.getUsageId());
            }
            if (vehicleCommand.getRegNo() != null && vehicleCommand.getRegNo() != "") {
                vehicleTO.setRegNo(vehicleCommand.getRegNo());
            } else {
                vehicleTO.setRegNo("");
            }
            int UserId = (Integer) session.getAttribute("userId");
            int totalRecords = 0;
            int pageNo = 0;
            int totalPages = 0;
            PaginationHelper pagenation = new PaginationHelper();
            int startIndex = 0;
            int endIndex = 0;

            vehicleTO.setStartIndex(startIndex);
            vehicleTO.setEndIndex(endIndex);

            ArrayList RoadTaxList = new ArrayList();
            RoadTaxList = vehicleBP.getRoadTaxList(vehicleTO);
            request.setAttribute("RoadTaxList", RoadTaxList);
            System.out.println("RoadTaxList in cont = " + RoadTaxList.size());
            request.setAttribute("regNo", vehicleCommand.getRegNo());

//            vehicleList = vehicleBP.processVehicleList(vehicleTO);
//            totalRecords = vehicleList.size();
//            pagenation.setTotalRecords(totalRecords);
            String buttonClicked = "";
            if (request.getParameter("button") != null && request.getParameter("button") != "") {
                buttonClicked = request.getParameter("button");
            }
            if (request.getParameter("pageNo") != null && request.getParameter("pageNo") != "") {
                pageNo = Integer.parseInt(request.getParameter("pageNo"));
            }
            pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
            totalPages = pagenation.getTotalNoOfPages();
            request.setAttribute("pageNo", pageNo);
            System.out.println("pageNo" + pageNo);
            request.setAttribute("totalPages", totalPages);
            startIndex = pagenation.getStartIndex();
            endIndex = pagenation.getEndIndex();

            vehicleTO.setStartIndex(startIndex);
            vehicleTO.setEndIndex(endIndex);
            vehicleList = vehicleBP.processVehicleList(vehicleTO);
            if (totalRecords <= 0) {
                vehicleList = null;
            }
            request.setAttribute("vehicleList", vehicleList);
            System.out.println("start Index=" + vehicleTO.getStartIndex());
            System.out.println("end Index=" + vehicleTO.getEndIndex());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            String pageTitle = "View Vehicles";
            request.setAttribute("pageTitle", pageTitle);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView vehicleTaxDetail(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        System.out.println("Test=======");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";
        String pageTitle = "";
        String menuPath = "Vehicle  >>  Road Tax Update";
        vehicleCommand = command;
        ArrayList vehicleTax = new ArrayList();
        ArrayList vehicleRoadTax = new ArrayList();
        VehicleTO vehicleTO = null;
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Vehicle-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                pageTitle = "Alter Vehicle Insurance";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                request.setAttribute("pageTitle", pageTitle);
                path = "content/Vehicle/alterVehicleTax.jsp";

                String id = request.getParameter("id");
                System.out.println("id = " + id);
                String vehicleId = request.getParameter("vehicleId");
                System.out.println("vehicleId = " + vehicleId);

                vehicleTO = new VehicleTO();
                if (vehicleId != null && vehicleId != "") {
                    System.out.println("===in vehicleid===" + vehicleId);
                    vehicleTO.setVehicleId(vehicleId);
                }

                if (id != null && id != "") {
                    System.out.println("==={{{{{{{{{{{ id===" + id);
                    vehicleTO.setId(id);
                }

//
                vehicleRoadTax = vehicleBP.updateTax(vehicleTO);
                request.setAttribute("vehicleRoadTax", vehicleRoadTax);
                System.out.println("vehicleRoadTax.size() in cont= " + vehicleRoadTax.size());

                vehicleTax = vehicleBP.vehicleRoadTax(vehicleTO);
                request.setAttribute("vehicleTax", vehicleTax);
                System.out.println("vehicleRoadTax.size() in cont= " + vehicleTax.size());
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView alterVehicleTax(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        System.out.println("in test demo-----------------------------");
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String pageTitle = "View Vehicles Insurance";
        ModelAndView mv = new ModelAndView();

        try {
            String menuPath = "Vehicle  >>  View Vehicles insurance  ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            CompanyTO companyTO = new CompanyTO();
            path = "content/Vehicle/viewVehicleTax.jsp";

            if (vehicleCommand.getRoadTaxReceiptNo() != null && vehicleCommand.getRoadTaxReceiptNo() != "") {
                vehicleTO.setRoadTaxReceiptNo(vehicleCommand.getRoadTaxReceiptNo());
            }
            if (vehicleCommand.getRoadTaxReceiptDate() != null && vehicleCommand.getRoadTaxReceiptDate() != "") {
                vehicleTO.setRoadTaxReceiptDate(vehicleCommand.getRoadTaxReceiptDate());
            }
            if (vehicleCommand.getRoadTaxPaidAmount() != 0) {
                vehicleTO.setRoadTaxPaidAmount(vehicleCommand.getRoadTaxPaidAmount());
            }
            if (vehicleCommand.getRoadTaxPaidLocation() != null && vehicleCommand.getRoadTaxPaidLocation() != "") {
                vehicleTO.setRoadTaxPaidLocation(vehicleCommand.getRoadTaxPaidLocation());
            }
            if (vehicleCommand.getRoadTaxPeriod() != null && vehicleCommand.getRoadTaxPeriod() != "") {
                vehicleTO.setRoadTaxPeriod(vehicleCommand.getRoadTaxPeriod());
            }
            if (vehicleCommand.getRoadTaxFromDate() != null && vehicleCommand.getRoadTaxFromDate() != "") {
                vehicleTO.setRoadTaxFromDate(vehicleCommand.getRoadTaxFromDate());
            }
            if (vehicleCommand.getNextRoadTaxDate() != null && vehicleCommand.getNextRoadTaxDate() != "") {
                vehicleTO.setNextRoadTaxDate(vehicleCommand.getNextRoadTaxDate());
            }
            if (vehicleCommand.getRoadTaxRemarks() != null && vehicleCommand.getRoadTaxRemarks() != "") {
                vehicleTO.setRoadTaxRemarks(vehicleCommand.getRoadTaxRemarks());
            }
            if (vehicleCommand.getVehicleId() != null && vehicleCommand.getVehicleId() != "") {
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }
            if (vehicleCommand.getId() != null && vehicleCommand.getId() != "") {
                vehicleTO.setId(vehicleCommand.getId());
            }

            request.setAttribute("pageTitle", pageTitle);
            int insertStatus = 0;

//            System.out.println("vehicleTO.setItemIds=" + vehicleTO.getItemIds());
            System.out.println("userId====1" + userId);

            insertStatus = vehicleBP.processUpdateVehicleTax(vehicleTO, userId);
            System.out.println("userId====2" + userId);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Road Tax Modified Successfully");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            mv = searchVehicleRoadTax(request, response, command);
            //request.removeAttribute("AddVehicleList");
            // ArrayList AddVehicleList = new ArrayList();
            // AddVehicleList = vehicleBP.processGetAddVehicleList();
            // request.setAttribute("AddVehicleList", AddVehicleList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView searchVehicleFc(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "Vehicle  >>  View Vehicles FC";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        vehicleCommand = command;
        VehicleTO vehicleTO = new VehicleTO();
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Vehicle-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "View Vehicles FC";
                request.setAttribute("pageTitle", pageTitle);
                path = "content/Vehicle/searchVehicleFC.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //vehicleInsuranceList
    /**
     * This method used to Vehicle Insurances List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView vehicleFCList(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String temp = "";
        ArrayList vehicleList = new ArrayList();
        String menuPath = "Vehicle  >>  View Road FC List";
        vehicleCommand = command;
        VehicleTO vehicleTO = new VehicleTO();
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        try {
//            path = "content/Vehicle/viewVehicleFC.jsp";
            path = "content/Vehicle/searchVehicleFC.jsp";
            if (vehicleCommand.getUsageId().equalsIgnoreCase("0")) {
                vehicleTO.setUsageId("");
            } else {
                vehicleTO.setUsageId(vehicleCommand.getUsageId());
            }
            if (vehicleCommand.getRegNo() != null && vehicleCommand.getRegNo() != "") {
                vehicleTO.setRegNo(vehicleCommand.getRegNo());
            } else {
                vehicleTO.setRegNo("");
            }
            int UserId = (Integer) session.getAttribute("userId");
            int totalRecords = 0;
            int pageNo = 0;
            int totalPages = 0;
            PaginationHelper pagenation = new PaginationHelper();
            int startIndex = 0;
            int endIndex = 0;

            vehicleTO.setStartIndex(startIndex);
            vehicleTO.setEndIndex(endIndex);

            ArrayList FCList = new ArrayList();
            FCList = vehicleBP.getFCList(vehicleTO);
            request.setAttribute("FCList", FCList);
            System.out.println("FCList in cont = " + FCList.size());
            request.setAttribute("regNo", vehicleCommand.getRegNo());

//            vehicleList = vehicleBP.processVehicleList(vehicleTO);
//            totalRecords = vehicleList.size();
//            pagenation.setTotalRecords(totalRecords);
            String buttonClicked = "";
            if (request.getParameter("button") != null && request.getParameter("button") != "") {
                buttonClicked = request.getParameter("button");
            }
            if (request.getParameter("pageNo") != null && request.getParameter("pageNo") != "") {
                pageNo = Integer.parseInt(request.getParameter("pageNo"));
            }
            pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
            totalPages = pagenation.getTotalNoOfPages();
            request.setAttribute("pageNo", pageNo);
            System.out.println("pageNo" + pageNo);
            request.setAttribute("totalPages", totalPages);
            startIndex = pagenation.getStartIndex();
            endIndex = pagenation.getEndIndex();

            vehicleTO.setStartIndex(startIndex);
            vehicleTO.setEndIndex(endIndex);
            vehicleList = vehicleBP.processVehicleList(vehicleTO);
            if (totalRecords <= 0) {
                vehicleList = null;
            }
            request.setAttribute("vehicleList", vehicleList);
            System.out.println("start Index=" + vehicleTO.getStartIndex());
            System.out.println("end Index=" + vehicleTO.getEndIndex());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            String pageTitle = "View Vehicles";
            request.setAttribute("pageTitle", pageTitle);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView vehicleFCDetail(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        System.out.println("Test=======");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";
        String pageTitle = "";
        String menuPath = "Vehicle  >> Vehicle FC Update";
        vehicleCommand = command;
        ArrayList vehicleFC = new ArrayList();
        ArrayList vehicleFCDetail = new ArrayList();
        VehicleTO vehicleTO = null;
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Vehicle-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                pageTitle = "Alter Vehicle FC ";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                request.setAttribute("pageTitle", pageTitle);
                path = "content/Vehicle/alterVehicleFC.jsp";

                String fcid = request.getParameter("fcid");
                System.out.println("fcid = " + fcid);
                String vehicleId = request.getParameter("vehicleId");
                System.out.println("vehicleId = " + vehicleId);

                vehicleTO = new VehicleTO();
                if (vehicleId != null && vehicleId != "") {
                    System.out.println("===in vehicleid===" + vehicleId);
                    vehicleTO.setVehicleId(vehicleId);
                }

                if (fcid != null && fcid != "") {
                    System.out.println("==={{{{{{{{{{{ id===" + fcid);
                    vehicleTO.setFcid(fcid);
                }

//
                vehicleFC = vehicleBP.updateFC(vehicleTO);
                request.setAttribute("vehicleFC", vehicleFC);
                System.out.println("vehicleFC.size() in cont= " + vehicleFC.size());

                vehicleFCDetail = vehicleBP.vehicleFCDetail(vehicleTO);
                request.setAttribute("vehicleFCDetail", vehicleFCDetail);
                System.out.println("vehicleFCDetail.size() in cont= " + vehicleFCDetail.size());
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView alterVehicleFC(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        System.out.println("in test demo-----------------------------");
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String pageTitle = "View Vehicles Insurance";
        ModelAndView mv = new ModelAndView();

        try {
            String menuPath = "Vehicle  >>  View Vehicles insurance  ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            CompanyTO companyTO = new CompanyTO();
            path = "content/Vehicle/viewVehicleFC.jsp";

            if (vehicleCommand.getRtoDetail() != null && vehicleCommand.getRtoDetail() != "") {
                vehicleTO.setRtoDetail(vehicleCommand.getRtoDetail());
            }
            if (vehicleCommand.getFcDate() != null && vehicleCommand.getFcDate() != "") {
                vehicleTO.setFcDate(vehicleCommand.getFcDate());
            }
            if (vehicleCommand.getFcAmount() != 0) {
                vehicleTO.setFcAmount(vehicleCommand.getFcAmount());
            }
            if (vehicleCommand.getFcReceiptNo() != null && vehicleCommand.getFcReceiptNo() != "") {
                vehicleTO.setFcReceiptNo(vehicleCommand.getFcReceiptNo());
            }
            if (vehicleCommand.getFcExpiryDate() != null && vehicleCommand.getFcExpiryDate() != "") {
                vehicleTO.setFcExpiryDate(vehicleCommand.getFcExpiryDate());
            }
            if (vehicleCommand.getFcRemarks() != null && vehicleCommand.getFcRemarks() != "") {
                vehicleTO.setFcRemarks(vehicleCommand.getFcRemarks());
            }
            if (vehicleCommand.getVehicleId() != null && vehicleCommand.getVehicleId() != "") {
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }
            if (vehicleCommand.getFcid() != null && vehicleCommand.getFcid() != "") {
                vehicleTO.setFcid(vehicleCommand.getFcid());
            }

            request.setAttribute("pageTitle", pageTitle);
            int insertStatus = 0;

//            System.out.println("vehicleTO.setItemIds=" + vehicleTO.getItemIds());
            System.out.println("userId====1" + userId);

            insertStatus = vehicleBP.processUpdateVehicleFC(vehicleTO, userId);
            System.out.println("userId====2" + userId);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle FC Modified Successfully");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            mv = searchVehicleFc(request, response, command);
            //request.removeAttribute("AddVehicleList");
            // ArrayList AddVehicleList = new ArrayList();
            // AddVehicleList = vehicleBP.processGetAddVehicleList();
            // request.setAttribute("AddVehicleList", AddVehicleList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView searchVehiclePermit(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "Vehicle  >>  View Vehicles Permit";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        vehicleCommand = command;
        VehicleTO vehicleTO = new VehicleTO();
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Vehicle-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "View Vehicles Permit";
                request.setAttribute("pageTitle", pageTitle);
                path = "content/Vehicle/viewVehiclePermit.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle Vehicle Insurance
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView addVehiclePermit(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        String pageTitle = "View Vehicles";
        ModelAndView mv = new ModelAndView();
        try {
            String menuPath = "Vehicle  >>  Permit ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            path = "content/Vehicle/addVehiclePermit.jsp";
            request.setAttribute("pageTitle", pageTitle);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveAddVehiclePermit(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        String pageTitle = "View Vehicle Permit";
        ModelAndView mv = new ModelAndView();
        try {
            String menuPath = "Vehicle  >>  Vehicle Permit ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            path = "content/Vehicle/viewVehiclePermit.jsp";
            if (vehicleCommand.getVehicleId() != null && !vehicleCommand.getVehicleId().equals("")) {
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }
            if (vehicleCommand.getPermitType() != null && !vehicleCommand.getPermitType().equals("")) {
                vehicleTO.setPermitType(vehicleCommand.getPermitType());
            }
            if (vehicleCommand.getPermitNo() != null && !vehicleCommand.getPermitNo().equals("")) {
                vehicleTO.setPermitNo(vehicleCommand.getPermitNo());
            }
            if (vehicleCommand.getPermitAmount() != 0) {
                vehicleTO.setPermitAmount(vehicleCommand.getPermitAmount());
            }
            if (vehicleCommand.getPermitPaidDate() != null && !vehicleCommand.getPermitPaidDate().equals("")) {
                vehicleTO.setPermitPaidDate(vehicleCommand.getPermitPaidDate());
            }
            if (vehicleCommand.getPermitExpiryDate() != null && !vehicleCommand.getPermitExpiryDate().equals("")) {
                vehicleTO.setPermitExpiryDate(vehicleCommand.getPermitExpiryDate());
            }
            if (vehicleCommand.getPermitRemarks() != null && !vehicleCommand.getPermitRemarks().equals("")) {
                vehicleTO.setPermitRemarks(vehicleCommand.getPermitRemarks());
            }

            request.setAttribute("pageTitle", pageTitle);
            int insertStatus = 0;
            insertStatus = vehicleBP.saveVehiclePermit(vehicleTO, Integer.parseInt(companyId), userId);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Permit Added Successfully");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } /*catch (FPBusinessException exception) {
        // run time exception has occurred. Directed to error page.
        FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
        request.setAttribute(ParveenErrorConstants.ERROR_KEY,
        exception.getErrorMessage());
        } */ catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView vehiclePermitList(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String temp = "";
        ArrayList vehicleList = new ArrayList();
        String menuPath = "Vehicle  >>  View Vehicle Permit List";
        vehicleCommand = command;
        VehicleTO vehicleTO = new VehicleTO();
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        try {
            path = "content/Vehicle/viewVehiclePermit.jsp";
            if (vehicleCommand.getUsageId().equalsIgnoreCase("0")) {
                vehicleTO.setUsageId("");
            } else {
                vehicleTO.setUsageId(vehicleCommand.getUsageId());
            }
            if (vehicleCommand.getRegNo() != null && vehicleCommand.getRegNo() != "") {
                vehicleTO.setRegNo(vehicleCommand.getRegNo());
            } else {
                vehicleTO.setRegNo("");
            }
            int UserId = (Integer) session.getAttribute("userId");
            int totalRecords = 0;
            int pageNo = 0;
            int totalPages = 0;
            PaginationHelper pagenation = new PaginationHelper();
            int startIndex = 0;
            int endIndex = 0;

            vehicleTO.setStartIndex(startIndex);
            vehicleTO.setEndIndex(endIndex);

            ArrayList VehiclePermitList = new ArrayList();
            VehiclePermitList = vehicleBP.getVehiclePermitList(vehicleTO);
            request.setAttribute("VehiclePermitList", VehiclePermitList);

            request.setAttribute("permitid", vehicleCommand.getPermitid());
            request.setAttribute("vehicleId", vehicleCommand.getVehicleId());
            request.setAttribute("regNo", vehicleCommand.getRegNo());
            request.setAttribute("permitType", vehicleCommand.getPermitType());
            request.setAttribute("permitNo", vehicleCommand.getPermitNo());
            request.setAttribute("permitPaidDate", vehicleCommand.getPermitPaidDate());
            request.setAttribute("permitAmount", vehicleCommand.getPermitAmount());
            request.setAttribute("permitExpiryDate", vehicleCommand.getPermitExpiryDate());
            request.setAttribute("remarks", vehicleCommand.getRemarks());
            request.setAttribute("activeInd", vehicleCommand.getActiveInd());

            vehicleList = vehicleBP.processVehicleList(vehicleTO);
            totalRecords = vehicleList.size();
            pagenation.setTotalRecords(totalRecords);

            String buttonClicked = "";
            if (request.getParameter("button") != null && request.getParameter("button") != "") {
                buttonClicked = request.getParameter("button");
            }
            if (request.getParameter("pageNo") != null && request.getParameter("pageNo") != "") {
                pageNo = Integer.parseInt(request.getParameter("pageNo"));
            }
            pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
            totalPages = pagenation.getTotalNoOfPages();
            request.setAttribute("pageNo", pageNo);
            System.out.println("pageNo" + pageNo);
            request.setAttribute("totalPages", totalPages);
            startIndex = pagenation.getStartIndex();
            endIndex = pagenation.getEndIndex();

            vehicleTO.setStartIndex(startIndex);
            vehicleTO.setEndIndex(endIndex);
            vehicleList = vehicleBP.processVehicleList(vehicleTO);
            if (totalRecords <= 0) {
                vehicleList = null;
            }
            request.setAttribute("vehicleList", vehicleList);
            System.out.println("start Index=" + vehicleTO.getStartIndex());
            System.out.println("end Index=" + vehicleTO.getEndIndex());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            String pageTitle = "View Vehicles";
            request.setAttribute("pageTitle", pageTitle);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView vehiclePermitDetail(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";
        String pageTitle = "";
        String menuPath = "Vehicle  >>  Vehicle Permit Update";
        vehicleCommand = command;
        ArrayList vehiclePermit = new ArrayList();
        ArrayList vehicleDetail = new ArrayList();
        VehicleTO vehicleTO = null;
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Vehicle-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                pageTitle = "Alter Vehicle Permit";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                request.setAttribute("pageTitle", pageTitle);
                path = "content/Vehicle/alterVehiclePermit.jsp";

                String permitid = request.getParameter("permitid");
                System.out.println("permitid = " + permitid);
                String vehicleId = request.getParameter("vehicleId");
                System.out.println("vehicleId = " + vehicleId);

                vehicleTO = new VehicleTO();
                if (vehicleId != null && vehicleId != "") {
                    System.out.println("===in vehicle===" + vehicleId);
                    vehicleTO.setVehicleId(vehicleId);
                }
//                vehicleTO = new VehicleTO();
                if (permitid != null && permitid != "") {
                    System.out.println("===in vehicle permitid===" + permitid);
                    vehicleTO.setPermitid(permitid);
                }

//
                vehicleDetail = vehicleBP.updateVehicleDetail(vehicleTO);
                request.setAttribute("vehicleDetail", vehicleDetail);
                System.out.println("vehicleDetail.size() in cont= " + vehicleDetail.size());

                vehiclePermit = vehicleBP.vehiclePermitDetail(vehicleTO);
                request.setAttribute("vehiclePermit", vehiclePermit);
                System.out.println("vehiclePermit.size() in cont= " + vehiclePermit.size());
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView alterVehiclePermit(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        System.out.println("in test demo-----------------------------");
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String pageTitle = "View Vehicles Permit";
        ModelAndView mv = new ModelAndView();

        try {
            String menuPath = "Vehicle  >>  View Vehicles Permit  ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            CompanyTO companyTO = new CompanyTO();
            path = "content/Vehicle/viewVehiclePermit.jsp";
            if (vehicleCommand.getUsageId() != null && vehicleCommand.getUsageId() != "") {
                vehicleTO.setUsageId(vehicleCommand.getUsageId());
            }
            if (vehicleCommand.getPermitType() != null && vehicleCommand.getPermitType() != "") {
                vehicleTO.setPermitType(vehicleCommand.getPermitType());
            }
            if (vehicleCommand.getPermitNo() != null && vehicleCommand.getPermitNo() != "") {
                vehicleTO.setPermitNo(vehicleCommand.getPermitNo());
            }
            if (vehicleCommand.getPermitAmount() != 0) {
                vehicleTO.setPermitAmount(vehicleCommand.getPermitAmount());
            }
            if (vehicleCommand.getPermitPaidDate() != null && vehicleCommand.getPermitPaidDate() != "") {
                vehicleTO.setPermitPaidDate(vehicleCommand.getPermitPaidDate());
            }
            if (vehicleCommand.getPermitExpiryDate() != null && vehicleCommand.getPermitExpiryDate() != "") {
                vehicleTO.setPermitExpiryDate(vehicleCommand.getPermitExpiryDate());
            }
            if (vehicleCommand.getInsuarnceNCB() != null && vehicleCommand.getRegNo() != "") {
                vehicleTO.setRegNo(vehicleCommand.getRegNo());
            }
            if (vehicleCommand.getRemarks() != null && vehicleCommand.getRemarks() != "") {
                vehicleTO.setRemarks(vehicleCommand.getRemarks());
            }
            if (vehicleCommand.getVehicleId() != null && vehicleCommand.getVehicleId() != "") {
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }
            if (vehicleCommand.getPermitid() != null && vehicleCommand.getPermitid() != "") {
                vehicleTO.setPermitid(vehicleCommand.getPermitid());
                System.out.println("vehicleCommand.getPermitid() 1= " + vehicleCommand.getPermitid());
            }
            System.out.println("vehicleCommand.getPermitid() 2= " + vehicleCommand.getPermitid());

            request.setAttribute("pageTitle", pageTitle);
            int insertStatus = 0;

//            System.out.println("vehicleTO.setItemIds=" + vehicleTO.getItemIds());
            System.out.println("userId====1" + userId);

            insertStatus = vehicleBP.processUpdateVehiclePermit(vehicleTO, userId);
            System.out.println("userId====2" + userId);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Permit Modified Successfully");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            mv = searchVehiclePermit(request, response, command);
            //request.removeAttribute("AddVehicleList");
            // ArrayList AddVehicleList = new ArrayList();
            // AddVehicleList = vehicleBP.processGetAddVehicleList();
            // request.setAttribute("AddVehicleList", AddVehicleList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public void getToLocSugg(HttpServletRequest request, HttpServletResponse response) throws IOException {
        System.out.println("i am in ajax ");
        HttpSession session = request.getSession();
        String sno = request.getParameter("sno");
        System.out.println("sno" + sno);
        String paramName = "toLocation" + request.getParameter("sno");
        System.out.println("paramName = " + paramName);
        String toLocation = request.getParameter(paramName);
        System.out.println("toLocation" + toLocation);

        String suggestions = vehicleBP.getToLocSugg(toLocation);
        System.out.println("suggestions" + suggestions);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.println(suggestions);
        writer.close();
    }

    public ModelAndView handleAddTrailerPage(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String menuPath = "Vehicle  >> Add New Trailer";

        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Vehicle-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
//                path = "content/Vehicle/addTrailer.jsp";
                path = "content/Vehicle/addTrailerDetails.jsp";
//                path = "BrattleFoods/vehicleMaster.jsp";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "Add Vehicle";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList MfrList = new ArrayList();
                VehicleTO vehicleTO = new VehicleTO();
                String fleetTypeId = request.getParameter("fleetTypeId");
                vehicleTO.setFleetTypeId(fleetTypeId);
                MfrList = vehicleBP.getMfrList(vehicleTO);
                request.setAttribute("fleetTypeId", fleetTypeId);
                request.setAttribute("MfrList", MfrList);

                ArrayList usageList = new ArrayList();
                usageList = vehicleBP.processGetUsageList();
                request.setAttribute("UsageList", usageList);

                ArrayList classList = new ArrayList();
                classList = vehicleBP.processGetClassList();
                request.setAttribute("ClassList", classList);

                //OperationPointList
                ArrayList operationPointList = new ArrayList();
                operationPointList = vehicleBP.processGetopList();
                request.setAttribute("OperationPointList", operationPointList);

                ArrayList servicePointList = new ArrayList();
                servicePointList = vehicleBP.processGetspList();
                request.setAttribute("ServicePointList", servicePointList);

                ArrayList customerList = new ArrayList();
                customerList = customerBP.processActiveCustomerList();
                request.setAttribute("customerList", customerList);

                ArrayList tyreItemList = new ArrayList();
                tyreItemList = vehicleBP.processTyreItems();
                request.setAttribute("tyreItemList", tyreItemList);

                ArrayList groupList = new ArrayList();
                groupList = vehicleBP.getGroupList();
                request.setAttribute("groupList", groupList);

                ArrayList positionList = new ArrayList();
                positionList = mrsBP.getTyrePostions();
                request.setAttribute("positionList", positionList);

                ArrayList leasingCustList = new ArrayList();
                leasingCustList = vehicleBP.getLeasingCustomerList();
                request.setAttribute("leasingCustList", leasingCustList);

                //path = "content/Vehicle/addVehicle.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleSearchTrailerPage(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        System.out.println("1");
        if (request.getSession().isNew()) {
            System.out.println("2");
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "Trailer  >>  View Trailer";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        vehicleCommand = command;
        VehicleTO vehicleTO = new VehicleTO();
        //int companyType = (Integer) session.getAttribute("companyTypeId");
        int companyType = 1011;
        String companyId = (String) session.getAttribute("companyId");
        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Vehicle-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "View Trailer";
                request.setAttribute("pageTitle", pageTitle);
//                int totalRecords = 0;
//                int pageNo = 0;
//                int totalPages = 0;
//
//                session.setAttribute("totalRecords", totalRecords);
//                request.setAttribute("pageNo", pageNo);
//                request.setAttribute("totalPages", totalPages);


                String fleetTypeId = request.getParameter("fleetTypeId");
                ArrayList ModelList = new ArrayList();
                vehicleTO.setFleetTypeId(fleetTypeId);
                ModelList = vehicleBP.getMfrList(vehicleTO);
                request.setAttribute("MfrList", ModelList);
                VehicleTO vehicleTo = new VehicleTO();
                ArrayList TypeList = new ArrayList();
                TypeList = vehicleBP.getTrailerTypeList();
                request.setAttribute("TypeList", TypeList);
                ArrayList usageList = new ArrayList();
                usageList = vehicleBP.processActiveUsageList();
                request.setAttribute("usageList", usageList);
                ArrayList customerList = new ArrayList();
                customerList = customerBP.processActiveCustomerList();
                request.setAttribute("customerList", customerList);
//                ArrayList groupList = new ArrayList();
//                groupList = vehicleBP.getGroupList();
//                request.setAttribute("groupList", groupList);
//                if (companyType == 1011) {
//                    vehicleTO.setCompanyId(companyId);
//                } else {
//                    vehicleTO.setCompanyId("0");
//                }
                System.out.println("i m here");
                ArrayList vehicleList = new ArrayList();
//                PaginationHelper pagenation = new PaginationHelper();
//                int startIndex = 0;
//                int endIndex = 0;
//
//                vehicleTO.setStartIndex(startIndex);
//                vehicleTO.setEndIndex(endIndex);
                if (vehicleCommand.getMfrId().equalsIgnoreCase("0")) {
                    vehicleTO.setMfrId("");
                } else {
                    vehicleTO.setMfrId(vehicleCommand.getMfrId());
                }
                if (vehicleCommand.getRegNo() != null && vehicleCommand.getRegNo() != "") {
                    vehicleTO.setRegNo(vehicleCommand.getRegNo());
                } else {
                    vehicleTO.setRegNo("");
                }

                if (vehicleCommand.getTypeId().equalsIgnoreCase("0")) {
                    vehicleTO.setTypeId("");
                } else {
                    vehicleTO.setTypeId(vehicleCommand.getTypeId());
                }
                if (vehicleCommand.getOwnership() != null && !"".equals(vehicleCommand.getOwnership())) {
                    vehicleTO.setOwnership(vehicleCommand.getOwnership());
                    request.setAttribute("ownership", vehicleCommand.getOwnership());
                }
                String roleId = "" + (Integer) session.getAttribute("RoleId");
                vehicleTO.setRoleId(roleId);
                vehicleList = vehicleBP.processTrailerList(vehicleTO);
                System.out.println("i m here 2");
                //   vehicleList = vehicleBP.processVehicleList(vehicleTO);
//                totalRecords = vehicleList.size();
//                pagenation.setTotalRecords(totalRecords);
                String buttonClicked = "";
                if (request.getParameter("button") != null && request.getParameter("button") != "") {
                    buttonClicked = request.getParameter("button");
                }
//                if (request.getParameter("pageNo") != null && request.getParameter("pageNo") != "") {
//                    pageNo = Integer.parseInt(request.getParameter("pageNo"));
//                }
//                pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
//                totalPages = pagenation.getTotalNoOfPages();
//                request.setAttribute("pageNo", pageNo);
//                System.out.println("pageNo" + pageNo);
//                request.setAttribute("totalPages", totalPages);
//                startIndex = pagenation.getStartIndex();
//                endIndex = pagenation.getEndIndex();

//                vehicleTO.setStartIndex(startIndex);
//                vehicleTO.setEndIndex(endIndex);
//                if (totalRecords <= 0) {
//                    vehicleList = null;
//                }
                ArrayList leasingCustList = new ArrayList();
                leasingCustList = vehicleBP.getLeasingCustomerList();
                request.setAttribute("leasingCustList", leasingCustList);
                request.setAttribute("vehicleList", vehicleList);
                System.out.println("start Index=" + vehicleTO.getStartIndex());
                System.out.println("end Index=" + vehicleTO.getEndIndex());
                String regNo = request.getParameter("regno");
                vehicleTO.setRegNo(regNo);

                String companyIds = request.getParameter("companyId");
                vehicleTO.setCompanyId(companyIds);

                String fromDate = request.getParameter("fromDate");
                vehicleTO.setFromDate(fromDate);

                String toDate = request.getParameter("toDate");
                vehicleTO.setToDate(toDate);
                String listId = request.getParameter("listId");
                if (listId.equals("1")) {
                    path = "content/Vehicle/searchTrailerInsurance.jsp";
                    ArrayList vehiclesInsuranceList = new ArrayList();
                    vehiclesInsuranceList = vehicleBP.getTrailerInsuranceList(vehicleTO);
                    System.out.println("vehiclesList" + vehiclesInsuranceList);
                    request.setAttribute("vehiclesList", vehiclesInsuranceList);
                    request.setAttribute("companyId", companyIds);
                    request.setAttribute("regNo", regNo);
                    request.setAttribute("fromDate", fromDate);
                    request.setAttribute("toDate", toDate);
                } else if (listId.equals("3")) {
                    path = "content/Vehicle/searchTrailerFC.jsp";
                    request.setAttribute("companyId", companyIds);
                    request.setAttribute("regNo", regNo);
                    request.setAttribute("fromDate", fromDate);
                    request.setAttribute("toDate", toDate);

                    ArrayList vehiclesFCList = new ArrayList();
                    vehiclesFCList = vehicleBP.getTrailerFCList(vehicleTO);
                    request.setAttribute("vehiclesFCList", vehiclesFCList);
                } else {
                    path = "content/Vehicle/viewTrailer.jsp";
                }
                request.setAttribute("regNo", vehicleCommand.getRegNo());
                request.setAttribute("typeId", vehicleCommand.getTypeId());
                request.setAttribute("mfrId", vehicleCommand.getMfrId());


            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAddTrailer(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {

//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        int madhavaramSp = 1011;
        ModelAndView mv = new ModelAndView();
        try {
            path = "content/Vehicle/addTrailer.jsp";
            String menuPath = "Trailer  >>  ADD trailer ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            CompanyTO companyTO = new CompanyTO();
            if (vehicleCommand.getUsageId() != null && vehicleCommand.getUsageId() != "") {
                vehicleTO.setUsageId(vehicleCommand.getUsageId());
            }
            if (vehicleCommand.getWar_period() != null && vehicleCommand.getWar_period() != "") {
                vehicleTO.setWar_period(vehicleCommand.getWar_period());
            }
            if (vehicleCommand.getMfrId() != null && vehicleCommand.getMfrId() != "") {
                vehicleTO.setMfrId(vehicleCommand.getMfrId());
            }
            if (vehicleCommand.getClassId() != null && vehicleCommand.getClassId() != "") {
                vehicleTO.setClassId(vehicleCommand.getClassId());
            }
            if (vehicleCommand.getModelId() != null && vehicleCommand.getModelId() != "") {
                vehicleTO.setModelId(vehicleCommand.getModelId());
            }
            if (vehicleCommand.getDateOfSale() != null && vehicleCommand.getDateOfSale() != "") {
                vehicleTO.setDateOfSale(vehicleCommand.getDateOfSale());
            }
            if (vehicleCommand.getRegistrationDate() != null && vehicleCommand.getRegistrationDate() != "") {
                vehicleTO.setRegistrationDate(vehicleCommand.getRegistrationDate());
            }
            if (vehicleCommand.getRegNo() != null && vehicleCommand.getRegNo() != "") {
                vehicleTO.setRegNo(vehicleCommand.getRegNo());
            }
            if (vehicleCommand.getTrailerNo() != null && vehicleCommand.getTrailerNo() != "") {
                vehicleTO.setTrailerNo(vehicleCommand.getTrailerNo());
            }
            if (vehicleCommand.getDescription() != null && vehicleCommand.getDescription() != "") {
                vehicleTO.setDescription(vehicleCommand.getDescription());
            }
            if (vehicleCommand.getEngineNo() != null && vehicleCommand.getEngineNo() != "") {
                vehicleTO.setEngineNo(vehicleCommand.getEngineNo());
            }
            if (vehicleCommand.getChassisNo() != null && vehicleCommand.getChassisNo() != "") {
                vehicleTO.setChassisNo(vehicleCommand.getChassisNo());
            }
            if (vehicleCommand.getOpId() != null && vehicleCommand.getOpId() != "") {
                vehicleTO.setOpId(vehicleCommand.getOpId());
            }
//            if (vehicleCommand.getSpId() != null && vehicleCommand.getSpId() != "") {
//                vehicleTO.setSpId(vehicleCommand.getSpId());
//            }
            if (vehicleCommand.getSeatCapacity() != null && vehicleCommand.getSeatCapacity() != "") {
                vehicleTO.setSeatCapacity(vehicleCommand.getSeatCapacity());
            }
//            if (vehicleCommand.getNextFCDate() != null && vehicleCommand.getNextFCDate() != "") {
//                vehicleTO.setNextFCDate(vehicleCommand.getNextFCDate());
//            }
            if (vehicleCommand.getKmReading() != null && vehicleCommand.getKmReading() != "") {
                vehicleTO.setKmReading(vehicleCommand.getKmReading());
            }
//            if (vehicleCommand.getHmReading() != null && vehicleCommand.getHmReading() != "") {
//                vehicleTO.setHmReading(vehicleCommand.getHmReading());
//            }
            if (vehicleCommand.getVehicleColor() != null && vehicleCommand.getVehicleColor() != "") {
                vehicleTO.setVehicleColor(vehicleCommand.getVehicleColor());
            }
            if (vehicleCommand.getGroupId() != null && vehicleCommand.getGroupId() != "") {
                vehicleTO.setGroupId(vehicleCommand.getGroupId());
            }
            if (vehicleCommand.getLeasingCustId() != null && vehicleCommand.getLeasingCustId() != "") {
                vehicleTO.setLeasingCustId(vehicleCommand.getLeasingCustId());
            }
            if (vehicleCommand.getGpsSystem() != null && vehicleCommand.getGpsSystem() != "") {
                vehicleTO.setGpsSystem(vehicleCommand.getGpsSystem());
            }
            if (vehicleCommand.getWarrantyDate() != null && vehicleCommand.getWarrantyDate() != "") {
                vehicleTO.setWarrantyDate(vehicleCommand.getWarrantyDate());
            }
            if (vehicleCommand.getAsset() != null && vehicleCommand.getAsset() != "") {
                vehicleTO.setAsset(vehicleCommand.getAsset());
            }
            if (vehicleCommand.getAxles() != null && vehicleCommand.getAxles() != "") {
                vehicleTO.setAxles(vehicleCommand.getAxles());
            }
            if (vehicleCommand.getVehicleDepreciation() != null && vehicleCommand.getVehicleDepreciation() != "") {
                vehicleTO.setVehicleDepreciation(vehicleCommand.getVehicleDepreciation());
            }
            if (vehicleCommand.getDailyKm() != null && vehicleCommand.getDailyKm() != "") {
                vehicleTO.setDailyKm(vehicleCommand.getDailyKm());
            }
            if (vehicleCommand.getDailyHm() != null && vehicleCommand.getDailyHm() != "") {
                vehicleTO.setDailyHm(vehicleCommand.getDailyHm());
            }
            if (vehicleCommand.getGpsSystemId() != null && vehicleCommand.getGpsSystemId() != "") {
                vehicleTO.setGpsSystemId(vehicleCommand.getGpsSystemId());
            }

            String fservice = request.getParameter("selectedindex");
            if (fservice != null) {
                vehicleTO.setFservice(fservice);
            } else {
                fservice = "N";
                vehicleTO.setFservice(fservice);
            }

            vehicleTO.setTyreIds(vehicleCommand.getTyreIds());
            vehicleTO.setPositionId(vehicleCommand.getPositionIds());
//            vehicleTO.setTyreType(vehicleCommand.getTyreType());
            vehicleTO.setItemIds(vehicleCommand.getItemIds());
            vehicleTO.setTyreDate(vehicleCommand.getTyreDate());
            String pageTitle = "View Vehicles";

            ArrayList ModelList = new ArrayList();
            ModelList = vehicleBP.processActiveMfrList();
            request.setAttribute("MfrList", ModelList);

            VehicleTO vehicleTo = new VehicleTO();
            ArrayList TypeList = new ArrayList();
            TypeList = vehicleBP.processActiveTypeList(vehicleTo);
            request.setAttribute("TypeList", TypeList);

            ArrayList usageList = new ArrayList();
            usageList = vehicleBP.processActiveUsageList();
            request.setAttribute("usageList", usageList);

            ArrayList customerList = new ArrayList();
            customerList = customerBP.processActiveCustomerList();
            request.setAttribute("customerList", customerList);

            ArrayList groupList = new ArrayList();
            groupList = vehicleBP.getGroupList();
            request.setAttribute("groupList", groupList);

            request.setAttribute("pageTitle", pageTitle);
            int vehicleId = 0;

            vehicleId = vehicleBP.processAddTrailerDetails(vehicleTO, userId, madhavaramSp);
            //  vehicleBP.vehicleKmUpdate(vehicleId, vehicleTO.getKmReading(), vehicleTO.getHmReading(), userId);

            path = "content/Vehicle/viewTrailer.jsp";

            vehicleTO = new VehicleTO();
            if (companyType == 1011) {
                vehicleTO.setCompanyId(companyId);
            } else {
                vehicleTO.setCompanyId("0");
            }

            ArrayList vehicleList = new ArrayList();
            vehicleList = vehicleBP.processVehicleList(vehicleTO);
            request.setAttribute("vehicleList", vehicleList);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Trailer Added Successfully");
            //request.removeAttribute("AddVehicleList");
            // ArrayList AddVehicleList = new ArrayList();
            // AddVehicleList = vehicleBP.processGetAddVehicleList();
            // request.setAttribute("AddVehicleList", AddVehicleList);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Trailer Added Successfully");
            mv = handleSearchTrailer(request, response, command);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView handleViewTrailer(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String pageTitle = "";
        String menuPath = "Vehicle  >>  View Trailer Detail";
        vehicleCommand = command;
        ArrayList vehicleDetail = new ArrayList();
        VehicleTO vehicleTO = null;
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        int UserId = (Integer) session.getAttribute("userId");
        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Vehicle-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                pageTitle = "view Trailer";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                request.setAttribute("pageTitle", pageTitle);
                path = "content/Vehicle/viewTrailerDetail.jsp";
                String vehicleId = request.getParameter("trailerId");

                vehicleTO = new VehicleTO();
                if (vehicleId != null && vehicleId != "") {
                    vehicleTO.setVehicleId(vehicleId);
                }

                ArrayList MfrList = new ArrayList();
                MfrList = vehicleBP.processGetMfrList();
                request.setAttribute("MfrList", MfrList);

                ArrayList usageList = new ArrayList();
                usageList = vehicleBP.processGetUsageList();
                request.setAttribute("UsageList", usageList);

                ArrayList classList = new ArrayList();
                classList = vehicleBP.processGetClassList();
                request.setAttribute("ClassList", classList);

                ArrayList modelList = new ArrayList();
                modelList = vehicleBP.processActiveModelList();
                request.setAttribute("modelList", modelList);

                ArrayList customerList = new ArrayList();
                customerList = customerBP.processActiveCustomerList();
                request.setAttribute("customerList", customerList);

                ArrayList groupList = new ArrayList();
                groupList = vehicleBP.getGroupList();
                request.setAttribute("groupList", groupList);

                //OperationPointList
                ArrayList operationPointList = new ArrayList();
                operationPointList = vehicleBP.processGetopList();
                request.setAttribute("OperationPointList", operationPointList);

                // get Vehicle tyre items list
                ArrayList tyreItemList = new ArrayList();
                tyreItemList = vehicleBP.processTyreItems();
                request.setAttribute("tyreItemList", tyreItemList);

                //            ArrayList positionList = new ArrayList();
                //            positionList = mrsBP.getTyrePostions();
                //            request.setAttribute("positionList", positionList);
                //get positions list
                ArrayList positionList = new ArrayList();
                positionList = vehicleBP.getTyrePostions();
                request.setAttribute("positionList", positionList);
                System.out.println("positionList size=" + positionList.size());

                // get Vehicle TYre positions
                ArrayList vehTyreList = new ArrayList();
                vehTyreList = vehicleBP.processTyrePosition(Integer.parseInt(vehicleId));
                request.setAttribute("vehTyreList", vehTyreList);

                ArrayList leasingCustList = new ArrayList();
                leasingCustList = vehicleBP.getLeasingCustomerList();
                request.setAttribute("leasingCustList", leasingCustList);

                vehicleDetail = vehicleBP.processTrailerDetail(vehicleTO);
                request.setAttribute("vehicleDetail", vehicleDetail);
                ArrayList vehicleAMC = new ArrayList();
                vehicleAMC = vehicleBP.vehicleAMCDetail(vehicleTO);
                request.setAttribute("vehicleAMC", vehicleAMC);
                ArrayList RoadTaxList = new ArrayList();
                RoadTaxList = vehicleBP.getRoadTaxList(vehicleTO);
                request.setAttribute("RoadTaxList", RoadTaxList);

            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleSearchTrailer(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String temp = "";
        ArrayList vehicleList = new ArrayList();
        String menuPath = "Trailer  >>  View Trailer";
        vehicleCommand = command;
        VehicleTO vehicleTO = new VehicleTO();
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        try {
            path = "content/Vehicle/viewTrailer.jsp";
            if (vehicleCommand.getUsageId().equalsIgnoreCase("0")) {
                vehicleTO.setUsageId("");
            } else {
                vehicleTO.setUsageId(vehicleCommand.getUsageId());
            }
            if (vehicleCommand.getMfrId().equalsIgnoreCase("0")) {
                vehicleTO.setMfrId("");
            } else {
                vehicleTO.setMfrId(vehicleCommand.getMfrId());
            }
            if (vehicleCommand.getRegNo() != null && vehicleCommand.getRegNo() != "") {
                vehicleTO.setRegNo(vehicleCommand.getRegNo());
            } else {
                vehicleTO.setRegNo("");
            }

            if (vehicleCommand.getCustId() != null && !vehicleCommand.getCustId().equalsIgnoreCase("0")) {
                vehicleTO.setCustId(vehicleCommand.getCustId());
            } else {
                vehicleTO.setCustId("");
            }

            if (vehicleCommand.getGroupId() != null && !vehicleCommand.getGroupId().equalsIgnoreCase("0")) {
                vehicleTO.setGroupId(vehicleCommand.getGroupId());
            } else {
                vehicleTO.setGroupId("");
            }

            if (vehicleCommand.getTypeId().equalsIgnoreCase("0")) {
                vehicleTO.setTypeId("");
            } else {
                vehicleTO.setTypeId(vehicleCommand.getTypeId());
            }
            if (companyType == 1011) {
                vehicleTO.setCompanyId(companyId);
            } else {
                vehicleTO.setCompanyId("0");
            }
            if (vehicleCommand.getOwnership() != null && !"".equals(vehicleCommand.getOwnership())) {
                vehicleTO.setOwnership(vehicleCommand.getOwnership());
                request.setAttribute("ownership", vehicleCommand.getOwnership());
            }
            ArrayList ModelList = new ArrayList();
            ModelList = vehicleBP.processActiveMfrList();
            request.setAttribute("MfrList", ModelList);

            VehicleTO vehicleTo = new VehicleTO();
            ArrayList TypeList = new ArrayList();
            TypeList = vehicleBP.processActiveTypeList(vehicleTo);
            request.setAttribute("TypeList", TypeList);

            ArrayList usageList = new ArrayList();
            usageList = vehicleBP.processActiveUsageList();
            request.setAttribute("usageList", usageList);

            ArrayList customerList = new ArrayList();
            customerList = customerBP.processActiveCustomerList();
            request.setAttribute("customerList", customerList);

            ArrayList groupList = new ArrayList();
            groupList = vehicleBP.getGroupList();
            request.setAttribute("groupList", groupList);

            request.setAttribute("regNo", vehicleCommand.getRegNo());
            request.setAttribute("typeId", vehicleCommand.getTypeId());
            request.setAttribute("mfrId", vehicleCommand.getMfrId());
            request.setAttribute("usageId", vehicleCommand.getUsageId());
            request.setAttribute("custId", vehicleCommand.getCustId());
            request.setAttribute("groupId", vehicleCommand.getGroupId());
            String roleId = "" + (Integer) session.getAttribute("RoleId");
            vehicleTO.setRoleId(roleId);
            vehicleList = vehicleBP.processTrailerList(vehicleTO);
            request.setAttribute("vehicleList", vehicleList);
            System.out.println("start Index=" + vehicleTO.getStartIndex());
            System.out.println("end Index=" + vehicleTO.getEndIndex());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            String pageTitle = "View Vehicles";
            request.setAttribute("pageTitle", pageTitle);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAlterTrailer(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = 0;
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        String pageTitle = "View Trailer";
        ModelAndView mv = new ModelAndView();

        try {
            String menuPath = "Trailer  >>  View Trailer  ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            CompanyTO companyTO = new CompanyTO();
            path = "content/Vehicle/viewTrailer.jsp";
            if (vehicleCommand.getUsageId() != null && vehicleCommand.getUsageId() != "") {
                vehicleTO.setUsageId(vehicleCommand.getUsageId());
            }
            if (vehicleCommand.getWar_period() != null && vehicleCommand.getWar_period() != "") {
                vehicleTO.setWar_period(vehicleCommand.getWar_period());
            }
            if (vehicleCommand.getWarrantyDate() != null && vehicleCommand.getWarrantyDate() != "") {
                vehicleTO.setWarrantyDate(vehicleCommand.getWarrantyDate());
            }
            if (vehicleCommand.getMfrId() != null && vehicleCommand.getMfrId() != "") {
                vehicleTO.setMfrId(vehicleCommand.getMfrId());
            }
            if (vehicleCommand.getClassId() != null && vehicleCommand.getClassId() != "") {
                vehicleTO.setClassId(vehicleCommand.getClassId());
            }
            if (vehicleCommand.getModelId() != null && vehicleCommand.getModelId() != "") {
                vehicleTO.setModelId(vehicleCommand.getModelId());
            }
            if (vehicleCommand.getGpsSystem() != null && vehicleCommand.getGpsSystem() != "") {
                vehicleTO.setGpsSystem(vehicleCommand.getGpsSystem());
            }
            if (vehicleCommand.getDateOfSale() != null && vehicleCommand.getDateOfSale() != "") {
                vehicleTO.setDateOfSale(vehicleCommand.getDateOfSale());
            }
            if (vehicleCommand.getRegistrationDate() != null && vehicleCommand.getRegistrationDate() != "") {
                vehicleTO.setRegistrationDate(vehicleCommand.getRegistrationDate());
            }
            if (vehicleCommand.getRegNo() != null && vehicleCommand.getRegNo() != "") {
                vehicleTO.setRegNo(vehicleCommand.getRegNo());
            }
            if (vehicleCommand.getTrailerNo() != null && vehicleCommand.getTrailerNo() != "") {
                vehicleTO.setTrailerNo(vehicleCommand.getTrailerNo());
            }
            if (vehicleCommand.getDescription() != null && vehicleCommand.getDescription() != "") {
                vehicleTO.setDescription(vehicleCommand.getDescription());
            }
            if (vehicleCommand.getEngineNo() != null && vehicleCommand.getEngineNo() != "") {
                vehicleTO.setEngineNo(vehicleCommand.getEngineNo());
            }
            if (vehicleCommand.getChassisNo() != null && vehicleCommand.getChassisNo() != "") {
                vehicleTO.setChassisNo(vehicleCommand.getChassisNo());
            }
            if (vehicleCommand.getOpId() != null && vehicleCommand.getOpId() != "") {
                vehicleTO.setOpId(vehicleCommand.getOpId());
            }
            if (vehicleCommand.getSeatCapacity() != null && vehicleCommand.getSeatCapacity() != "") {
                vehicleTO.setSeatCapacity(vehicleCommand.getSeatCapacity());
            }
            if (vehicleCommand.getNextFCDate() != null && vehicleCommand.getNextFCDate() != "") {
                vehicleTO.setNextFCDate(vehicleCommand.getNextFCDate());
            }
            if (vehicleCommand.getKmReading() != null && vehicleCommand.getKmReading() != "") {
                vehicleTO.setKmReading(vehicleCommand.getKmReading());
            }
            if (vehicleCommand.getHmReading() != null && vehicleCommand.getHmReading() != "") {
                vehicleTO.setHmReading(vehicleCommand.getHmReading());
            }
            if (vehicleCommand.getDescription() != null && vehicleCommand.getDescription() != "") {
                vehicleTO.setDescription(vehicleCommand.getDescription());
            }
            if (vehicleCommand.getFservice() != null && vehicleCommand.getFservice() != "") {
                vehicleTO.setFservice(vehicleCommand.getFservice());
            }
            if (vehicleCommand.getGroupId() != null && vehicleCommand.getGroupId() != "") {
                vehicleTO.setGroupId(vehicleCommand.getGroupId());
            }
            if (vehicleCommand.getLeasingCustId() != null && vehicleCommand.getLeasingCustId() != "") {
                vehicleTO.setLeasingCustId(vehicleCommand.getLeasingCustId());
            }

            vehicleTO.setActiveInd(vehicleCommand.getActiveInd());
            vehicleTO.setMfrIds(vehicleCommand.getMfrIds());
            vehicleTO.setModelIds(vehicleCommand.getModelIds());
//            vehicleTO.setTyreType(vehicleCommand.getTyreType());
            vehicleTO.setSeatCapacity(vehicleCommand.getSeatCapacity());
            vehicleTO.setCustId(vehicleCommand.getCustId());
            vehicleTO.setSeatCapacity(vehicleCommand.getSeatCapacity());
            vehicleTO.setDailyKm(vehicleCommand.getDailyKm());
            vehicleTO.setDailyHm(vehicleCommand.getDailyHm());

            vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            vehicleTO.setItemIds(vehicleCommand.getItemIds());
            vehicleTO.setPositionId(vehicleCommand.getPosIds());
            vehicleTO.setTyreIds(vehicleCommand.getTyreIds());
            vehicleTO.setTyreNos(vehicleCommand.getTyreNos());

            vehicleTO.setTyreDate(vehicleCommand.getTyreDate());

            request.setAttribute("pageTitle", pageTitle);
            int insertStatus = 0;

            System.out.println("vehicleTO.setItemIds=" + vehicleTO.getItemIds());

            insertStatus = vehicleBP.processUpdateTrailerDetails(vehicleTO, Integer.parseInt(companyId), userId);
            System.out.println("check1");
            //  vehicleBP.vehicleKmUpdate(Integer.parseInt(vehicleTO.getVehicleId()), vehicleTO.getKmReading(), vehicleTO.getHmReading(), userId);
            vehicleTO = new VehicleTO();
            if (companyType == 1011) {
                vehicleTO.setCompanyId(companyId);
            } else {
                vehicleTO.setCompanyId("0");
            }

            ArrayList ModelList = new ArrayList();
            ModelList = vehicleBP.processActiveMfrList();
            request.setAttribute("MfrList", ModelList);

            VehicleTO vehicleTo = new VehicleTO();
            ArrayList TypeList = new ArrayList();
            TypeList = vehicleBP.processActiveTypeList(vehicleTo);
            request.setAttribute("TypeList", TypeList);

            ArrayList usageList = new ArrayList();
            usageList = vehicleBP.processActiveUsageList();
            request.setAttribute("usageList", usageList);

            ArrayList customerList = new ArrayList();
            customerList = customerBP.processActiveCustomerList();
            request.setAttribute("customerList", customerList);

            ArrayList vehicleList = new ArrayList();
            vehicleList = vehicleBP.processVehicleList(vehicleTO);
            request.setAttribute("vehicleList", vehicleList);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Trailer Details Modified Successfully");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            mv = handleSearchTrailer(request, response, command);
            //request.removeAttribute("AddVehicleList");
            // ArrayList AddVehicleList = new ArrayList();
            // AddVehicleList = vehicleBP.processGetAddVehicleList();
            // request.setAttribute("AddVehicleList", AddVehicleList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView handleVehicleReplacementPage(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/Login.jsp");
        }

        HttpSession session = request.getSession();
        vehicleCommand = command;
        String path = "";
        VehicleTO vehicleTO = new VehicleTO();
        VendorTO vendorTO = new VendorTO();

//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Employee-Add")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
        //
        try {

            String menuPath = "Vehicle  >>  VehicleReplacement >> View ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Add Designation";
            request.setAttribute("pageTitle", pageTitle);
            int UserId = (Integer) session.getAttribute("userId");

            ArrayList regNoList = new ArrayList();
            regNoList = vehicleBP.getRegNo(vehicleTO);
            request.setAttribute("regNo", regNoList);

            ArrayList vendorList = new ArrayList();
            vendorList = vehicleBP.processVendorList();
            request.setAttribute("vendorList", vendorList);
            ArrayList vehicleReplacementList = new ArrayList();
            vehicleReplacementList = vehicleBP.getVehicleReplacement();
            System.out.println("array list size" + vehicleReplacementList.size());
            System.out.println("vehicleReplacementList= " + vehicleReplacementList.size());
            request.setAttribute("vehicleReplacementList", vehicleReplacementList);

            path = "content/Vehicle/VehicleReplacementPage.jsp";
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to handleLogout --> " + exception);
            exception.printStackTrace();
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle DhcChargeMaster Add.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView insertVehicleReplacementPage(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        try {
            String menuPath = "Vehicle  >>  VendorType >>  View  ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();
            String temp[] = null;
            if (vehicleCommand.getvName() != null && vehicleCommand.getvName() != "") {
                temp = vehicleCommand.getvName().split("~");
                vehicleTO.setvName(temp[1]);

            }
            if (vehicleCommand.getcType() != null && vehicleCommand.getcType() != "") {
                vehicleTO.setcType(vehicleCommand.getcType());

            }
            if (vehicleCommand.geteRegNo() != null && vehicleCommand.geteRegNo() != "") {
                vehicleTO.seteRegNo(vehicleCommand.geteRegNo());

            }
            if (vehicleCommand.getrRegNo() != null && vehicleCommand.getrRegNo() != "") {
                vehicleTO.setrRegNo(vehicleCommand.getrRegNo());

            }
            if (vehicleCommand.getrDate() != null && vehicleCommand.getrDate() != "") {
                vehicleTO.setrDate(vehicleCommand.getrDate());

            }

            String pageTitle = "View VehicleType";
            request.setAttribute("pageTitle", pageTitle);

            int insertStatus = 0;

            insertStatus = vehicleBP.insertVehicleReplacement(vehicleTO, userId);
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "VehicleReplace Successfully");

            path = "content/Vehicle/VehicleReplacementPage.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getRegNoAjax(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        vehicleCommand = command;

        String vName = request.getParameter("vName");
        System.out.println("vendorid:" + vName);
        String suggestions = "";
        VehicleTO vehicleTO = new VehicleTO();
        try {
            //Hari
            vehicleTO.setVendorId(vName);
            //Hari End
            PrintWriter pw = response.getWriter();
            ArrayList vehicleNos = new ArrayList();
            response.setContentType("text/html");
            vehicleNos = vehicleBP.getVendorVehicleRegNo(vehicleTO);
            System.out.println("vehicleNos.size() = " + vehicleNos.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = vehicleNos.iterator();
            int cntr = 0;
            VehicleTO vTO = new VehicleTO();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                vTO = (VehicleTO) itr.next();
                jsonObject.put("Name", vTO.getRegNo());
                jsonObject.put("Id", vTO.getVehicleId());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
                cntr++;
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Model data in Ajax --> " + exception);
        }
    }

    public void getRegNos(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        vehicleCommand = command;

        String vName = request.getParameter("vName");
        System.out.println("vendorid:" + vName);
        String suggestions = "";
        VehicleTO vehicleTO = new VehicleTO();
        try {
            //Hari
            vehicleTO.setVendorId(vName);
            //Hari End
            PrintWriter pw = response.getWriter();
            ArrayList vehicleNos = new ArrayList();
            response.setContentType("text/html");
            vehicleNos = vehicleBP.getVendorVehicleRegNos(vehicleTO);
            System.out.println("vehicleNos.size() = " + vehicleNos.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = vehicleNos.iterator();
            int cntr = 0;
            VehicleTO vTO = new VehicleTO();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                vTO = (VehicleTO) itr.next();
                jsonObject.put("Name", vTO.getRegNo());
                jsonObject.put("Id", vTO.getVehicleId());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
                cntr++;
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Model data in Ajax --> " + exception);
        }
    }

    public ModelAndView saveVehicleTyre(HttpServletRequest request, HttpServletResponse reponse, VehicleCommand command) throws IOException {
        System.out.println("ServiceTypeMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        vehicleCommand = command;
        String path = "";
        int madhavaramSp = 1011;
        int insertStatus = 0;
        int userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");

        String companyId = (String) session.getAttribute("companyId");
        //        int cityMasterList = 0;
        VehicleTO vehicleTO = new VehicleTO();
        String menuPath = "";
        menuPath = "Service Plan  >> Axle Type Master ";
        String pageTitle = "ServiceTypeMaster ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {

            String vehicleFrontAxle[] = request.getParameterValues("vehicleFrontAxle");
            String position[] = request.getParameterValues("position");
            String leftTyreIds[] = request.getParameterValues("leftTyreIds");
            String leftItemIds[] = request.getParameterValues("leftItemIds");
            String rightTyreIds[] = request.getParameterValues("rightTyreIds");
            String rightItemIds[] = request.getParameterValues("rightItemIds");
            String positionNo[] = request.getParameterValues("positionNo");
            System.out.println("positionNo" + positionNo);
            System.out.println("position length" + position.length);
            int k = 0;
            System.out.println("vehicleFrontAxle length" + position.length);
            for (int i = 0; i < position.length; i++) {
                StringBuffer AxleType = new StringBuffer(vehicleFrontAxle[i]);
                System.out.println("AxleType" + AxleType);
                for (int j = 0; j < Integer.parseInt(position[i]); j++) {
                    StringBuffer LeftTyre = new StringBuffer(leftTyreIds[k]);
                    System.out.println("LeftTyre" + LeftTyre);
                    StringBuffer LeftMFR = new StringBuffer(leftItemIds[k]);
                    System.out.println("LeftMFR" + LeftMFR);
                    StringBuffer RightTyre = new StringBuffer(rightTyreIds[k]);
                    System.out.println("RightTyre" + RightTyre);
                    StringBuffer RightMFR = new StringBuffer(rightItemIds[k]);
                    System.out.println("RightMFR" + RightMFR);
                    k++;

                    int vehicleTyreId = 0;

                    vehicleTyreId = vehicleBP.insertVehicleTyre(vehicleTO, userId, madhavaramSp, AxleType.toString(), LeftTyre.toString(), LeftMFR.toString(), RightTyre.toString(), RightMFR.toString());
                    LeftTyre.setLength(0);
                    LeftMFR.setLength(0);
                    RightTyre.setLength(0);
                    RightMFR.setLength(0);
                }
                AxleType.setLength(0);
            }

            path = "content/Vehicle/addVehicle.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView handleVehicleAxleTyre(HttpServletRequest request, HttpServletResponse reponse, VehicleCommand command) throws IOException {
        System.out.println("ServiceTypeMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        vehicleCommand = command;
        String path = "";
        //        int userId = 0;
        //                (Integer) session.getAttribute("userId");
        //        int cityMasterList = 0;
        VehicleTO vehicleTO = new VehicleTO();
        String menuPath = "";
        menuPath = "Axle  >> Add New Axle ";
        String pageTitle = "New Axle ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {

            path = "content/Vehicle/vehicleAxleTyreMaster.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView handleSaveVehicleAxleMaster(HttpServletRequest request, HttpServletResponse reponse, VehicleCommand command) throws IOException {
        System.out.println("ServiceTypeMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        vehicleCommand = command;
        String path = "";
        int insertStatus = 0;
        int userId = (Integer) session.getAttribute("userId");
        //        int cityMasterList = 0;
        VehicleTO vehicleTO = new VehicleTO();
        String menuPath = "";
        menuPath = "Service Plan  >> Axle Type Master ";
        String pageTitle = "ServiceTypeMaster ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String axleTypeName1 = request.getParameter("axleTypeName");
        System.out.println("axleTypeName1" + axleTypeName1);
        String axleCount1 = request.getParameter("axleCount");
        System.out.println("axleCount1" + axleCount1);
        String typeFor = request.getParameter("typeFor");
        System.out.println("typeFor" + typeFor);

        try {

            String vehicleFront[] = request.getParameterValues("vehicleFront");
            System.out.println("vehicleFront" + vehicleFront);
            String axleRight[] = request.getParameterValues("axleRight");
            System.out.println("axleRight" + axleRight);
            String axleLeft[] = request.getParameterValues("axleLeft");
            System.out.println("axleLeft" + axleLeft);
            vehicleTO.setAxleTypeName(axleTypeName1);
            vehicleTO.setAxleCount(axleCount1);
            vehicleTO.setTypeFor(typeFor);
            int lastinsertedId = 0;
            lastinsertedId = vehicleBP.insertAxleMaster(vehicleTO, userId);
            System.out.println("lastinsertedId in control for axle matser" + lastinsertedId);

            System.out.println("axleRight.length" + axleRight.length);
            int axleId = 0;
            if (lastinsertedId > 0) {
//              if (axleRight == null && !"".equals("axleRight") ) {
                System.out.println("hai controler");
                for (int i = 0; i < axleRight.length; i++) {
                    System.out.println("axleRight" + i + "=" + axleRight[i]);
                    System.out.println("vehicleFront" + i + "=" + vehicleFront[i]);
                    System.out.println("axleLeft" + i + "=" + axleLeft[i]);
                    //                    if (vehicleFront[i] == null && axleLeft[i] != null) {
                    axleId = vehicleBP.insertAxletypeMaster(vehicleTO, userId, vehicleFront[i], axleRight[i], axleLeft[i], lastinsertedId);

                }
            }

            path = "content/Vehicle/vehicleAxleTyreMaster.jsp";
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Axle Added Successfully");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public void getVehAxleName(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws IOException {
        System.out.println("entered in  ajaxmodel controller");
        HttpSession session = request.getSession();

        vehicleCommand = command;
        VehicleTO vehicleTO = new VehicleTO();
        ArrayList vehicleAxleDetail = new ArrayList();
        PrintWriter pw = response.getWriter();
        try {
            response.setContentType("text/html");
            int typeId = Integer.parseInt(request.getParameter("typeId"));
            System.out.println("typeId" + typeId);
            vehicleAxleDetail = vehicleBP.getVehAxleName(typeId);
            System.out.println("vehicleAxleDetail.size() = " + vehicleAxleDetail.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = vehicleAxleDetail.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                vehicleTO = (VehicleTO) itr.next();
                jsonObject.put("AxleDetailId", vehicleTO.getVehicleAxleId());
                jsonObject.put("AxleTypeName", vehicleTO.getAxleTypeName());
                jsonObject.put("LeftSideTyreCount", vehicleTO.getAxleLeft());
                jsonObject.put("RightSideTyreCount", vehicleTO.getAxleRight());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);
            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }

    }

    public void getVehAxleTyreNo(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws IOException {
	        System.out.println("entered in  ajaxmodel controller");
	        HttpSession session = request.getSession();

	        vehicleCommand = command;
	        VehicleTO vehicleTO = new VehicleTO();
	        VehicleTO vehTO = new VehicleTO();
	        ArrayList vehicleAxleDetail = new ArrayList();
	        PrintWriter pw = response.getWriter();
	        try {
	            response.setContentType("text/html");
	            String axleDetailId = request.getParameter("axleDetailId");
	            String positionName = request.getParameter("positionName");
	            String positionNo = request.getParameter("positionNo");
	            String vehicleId = request.getParameter("vehicleId");
	            String axleTypeId = request.getParameter("axleTypeId");
	            vehTO.setAxleDetailId(axleDetailId);
	            vehTO.setPositionName(positionName);
	            vehTO.setPositionNo(positionNo);
	            vehTO.setVehicleId(vehicleId);
	            vehTO.setAxleTypeId(axleTypeId);
	            vehicleAxleDetail = vehicleBP.getVehAxleTyreNo(vehTO);
	            System.out.println("vehicleAxleDetail.size() = " + vehicleAxleDetail.size());
	            JSONArray jsonArray = new JSONArray();
	            Iterator itr = vehicleAxleDetail.iterator();
	            while (itr.hasNext()) {
	                JSONObject jsonObject = new JSONObject();
	                vehicleTO = (VehicleTO) itr.next();
	                jsonObject.put("AxleTyreId", vehicleTO.getAxleTyreId());
	                jsonObject.put("TyreNo", vehicleTO.getTyreNo());
	                jsonObject.put("TyreDepth", vehicleTO.getTyreDepth());
	                jsonObject.put("TyreMfr", vehicleTO.getTyreMfr());
	                System.out.println("jsonObject = " + jsonObject);
	                jsonArray.put(jsonObject);
	            }
	            System.out.println("jsonArray = " + jsonArray);
	            pw.print(jsonArray);
	        } catch (FPRuntimeException excp) {
	            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
	        } catch (Exception exception) {
	            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
	            exception.printStackTrace();
	        }

    }


    public void saveVehicleDetails(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");
        //int companyType = (Integer) session.getAttribute("companyTypeId");
        //String companyId = (String) session.getAttribute("companyId");

        int madhavaramSp = 1011;
        VehicleTO vehicleTO = new VehicleTO();
        CompanyTO companyTO = new CompanyTO();
//        String vehicleId = request.getParameter("vehicleId");
        try {
            if (vehicleCommand.getUsageId() != null && vehicleCommand.getUsageId() != "") {
                vehicleTO.setUsageId(vehicleCommand.getUsageId());
            }
            if (vehicleCommand.getWar_period() != null && vehicleCommand.getWar_period() != "") {
                vehicleTO.setWar_period(vehicleCommand.getWar_period());
            }
            if (vehicleCommand.getMfrId() != null && vehicleCommand.getMfrId() != "") {
                vehicleTO.setMfrId(vehicleCommand.getMfrId());
            }
            if (vehicleCommand.getClassId() != null && vehicleCommand.getClassId() != "") {
                vehicleTO.setClassId(vehicleCommand.getClassId());
            }
            if (vehicleCommand.getModelId() != null && vehicleCommand.getModelId() != "") {
                vehicleTO.setModelId(vehicleCommand.getModelId());
            }
            if (vehicleCommand.getDateOfSale() != null && vehicleCommand.getDateOfSale() != "") {
                vehicleTO.setDateOfSale(vehicleCommand.getDateOfSale());
            }
            if (vehicleCommand.getRegistrationDate() != null && vehicleCommand.getRegistrationDate() != "") {
                vehicleTO.setRegistrationDate(vehicleCommand.getRegistrationDate());
            }
            if (vehicleCommand.getRegNo() != null && vehicleCommand.getRegNo() != "") {
                vehicleTO.setRegNo(vehicleCommand.getRegNo());
            }
            if (vehicleCommand.getDescription() != null && vehicleCommand.getDescription() != "") {
                vehicleTO.setDescription(vehicleCommand.getDescription());
            }
            if (vehicleCommand.getEngineNo() != null && vehicleCommand.getEngineNo() != "") {
                vehicleTO.setEngineNo(vehicleCommand.getEngineNo());
            }
            if (vehicleCommand.getChassisNo() != null && vehicleCommand.getChassisNo() != "") {
                vehicleTO.setChassisNo(vehicleCommand.getChassisNo());
            }
            if (vehicleCommand.getOpId() != null && vehicleCommand.getOpId() != "") {
                vehicleTO.setOpId(vehicleCommand.getOpId());
            }
//            if (vehicleCommand.getSpId() != null && vehicleCommand.getSpId() != "") {
//                vehicleTO.setSpId(vehicleCommand.getSpId());
//            }
            if (vehicleCommand.getSeatCapacity() != null && vehicleCommand.getSeatCapacity() != "") {
                vehicleTO.setSeatCapacity(vehicleCommand.getSeatCapacity());
            }
//            if (vehicleCommand.getNextFCDate() != null && vehicleCommand.getNextFCDate() != "") {
//                vehicleTO.setNextFCDate(vehicleCommand.getNextFCDate());
//            }
            if (vehicleCommand.getKmReading() != null && vehicleCommand.getKmReading() != "") {
                vehicleTO.setKmReading(vehicleCommand.getKmReading());
            }
//            if (vehicleCommand.getHmReading() != null && vehicleCommand.getHmReading() != "") {
//                vehicleTO.setHmReading(vehicleCommand.getHmReading());
//            }
            if (vehicleCommand.getVehicleColor() != null && vehicleCommand.getVehicleColor() != "") {
                vehicleTO.setVehicleColor(vehicleCommand.getVehicleColor());
            }
            if (vehicleCommand.getVehicleCost() != null && vehicleCommand.getVehicleCost() != "") {
                vehicleTO.setVehicleCost(vehicleCommand.getVehicleCost());
            }
            if (vehicleCommand.getGroupId() != null && vehicleCommand.getGroupId() != "") {
                vehicleTO.setGroupId(vehicleCommand.getGroupId());
            }
            if (vehicleCommand.getGpsSystem() != null && vehicleCommand.getGpsSystem() != "") {
                vehicleTO.setGpsSystem(vehicleCommand.getGpsSystem());
            }
            if (vehicleCommand.getWarrantyDate() != null && vehicleCommand.getWarrantyDate() != "") {
                vehicleTO.setWarrantyDate(vehicleCommand.getWarrantyDate());
            }
            if (vehicleCommand.getAsset() != null && vehicleCommand.getAsset() != "") {
                vehicleTO.setAsset(vehicleCommand.getAsset());
            }
            if (vehicleCommand.getAxles() != null && vehicleCommand.getAxles() != "") {
                vehicleTO.setAxles(vehicleCommand.getAxles());
            }
            if (vehicleCommand.getVehicleDepreciation() != null && vehicleCommand.getVehicleDepreciation() != "") {
                vehicleTO.setVehicleDepreciation(vehicleCommand.getVehicleDepreciation());
            }
            if (vehicleCommand.getDailyKm() != null && vehicleCommand.getDailyKm() != "") {
                vehicleTO.setDailyKm(vehicleCommand.getDailyKm());
            }
            if (vehicleCommand.getDailyHm() != null && vehicleCommand.getDailyHm() != "") {
                vehicleTO.setDailyHm(vehicleCommand.getDailyHm());
            }
            if (vehicleCommand.getGpsSystemId() != null && vehicleCommand.getGpsSystemId() != "") {
                vehicleTO.setGpsSystemId(vehicleCommand.getGpsSystemId());
            }
            if (vehicleCommand.getOwnerShips() != null && vehicleCommand.getOwnerShips() != "") {
                vehicleTO.setOwnerShips(vehicleCommand.getOwnerShips());
            }
            if (vehicleCommand.getTypeId() != null && vehicleCommand.getTypeId() != "") {
                vehicleTO.setTypeId(vehicleCommand.getTypeId());
            }
            if (vehicleCommand.getVendorId() != null && vehicleCommand.getVendorId() != "") {
                vehicleTO.setVendorId(vehicleCommand.getVendorId());
            }
            if (vehicleCommand.getAxleTypeId() != null && vehicleCommand.getAxleTypeId() != "") {
                vehicleTO.setAxleTypeId(vehicleCommand.getAxleTypeId());
            }
            if (vehicleCommand.getVehicleId() != null && vehicleCommand.getVehicleId() != "") {
                System.out.println("vehicleCommand.getVehicleId()" + vehicleCommand.getVehicleId());
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }
//             if (vehicleCommand.getVehicleId() != null && vehicleCommand.getVehicleId() != "") {
//                 System.out.println("vehicleCommand.getVehicleId()"+vehicleCommand.getVehicleId());
//
//                 request.setAttribute("vehicleId", vehicleCommand.getVehicleId());
//            }else{
//                request.setAttribute("vehicleId", 0);
//            }

            int vehicleId = 0;
            String vehicleDetails = vehicleBP.addVehicleDetails(vehicleTO, userId, madhavaramSp);
            String[] temp = vehicleDetails.split("~");
            vehicleId = Integer.parseInt(temp[0]);
            vehicleBP.vehicleKmUpdate(vehicleId, vehicleTO.getKmReading(), vehicleTO.getHmReading(), userId);
            if (vehicleId > 0) {
                vehicleBP.vehicleTyreDetails(vehicleTO, vehicleId, userId, madhavaramSp);
            }

            response.setContentType("text/css");
            if (vehicleId > 0) {
                response.getWriter().println(vehicleDetails);
            } else {
                response.getWriter().println(0);
            }
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
    }

    public void updateVehicleDetails(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws IOException, FPRuntimeException, FPBusinessException {

        System.out.println("entered updateVehicleDetails");
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");

        int madhavaramSp = 1011;
        VehicleTO vehicleTO = new VehicleTO();
        CompanyTO companyTO = new CompanyTO();
        try {
            if (vehicleCommand.getUsageId() != null && vehicleCommand.getUsageId() != "") {
                vehicleTO.setUsageId(vehicleCommand.getUsageId());
            }
            if (vehicleCommand.getWar_period() != null && vehicleCommand.getWar_period() != "") {
                vehicleTO.setWar_period(vehicleCommand.getWar_period());
            }
            if (vehicleCommand.getMfrId() != null && vehicleCommand.getMfrId() != "") {
                vehicleTO.setMfrId(vehicleCommand.getMfrId());
            }
            if (vehicleCommand.getClassId() != null && vehicleCommand.getClassId() != "") {
                vehicleTO.setClassId(vehicleCommand.getClassId());
            }
            if (vehicleCommand.getModelId() != null && vehicleCommand.getModelId() != "") {
                vehicleTO.setModelId(vehicleCommand.getModelId());
            }
            if (vehicleCommand.getDateOfSale() != null && vehicleCommand.getDateOfSale() != "") {
                vehicleTO.setDateOfSale(vehicleCommand.getDateOfSale());
            }
            if (vehicleCommand.getRegistrationDate() != null && vehicleCommand.getRegistrationDate() != "") {
                vehicleTO.setRegistrationDate(vehicleCommand.getRegistrationDate());
            }
            if (vehicleCommand.getRegNo() != null && vehicleCommand.getRegNo() != "") {
                vehicleTO.setRegNo(vehicleCommand.getRegNo());
            }
            if (vehicleCommand.getDescription() != null && vehicleCommand.getDescription() != "") {
                vehicleTO.setDescription(vehicleCommand.getDescription());
            }
            if (vehicleCommand.getEngineNo() != null && vehicleCommand.getEngineNo() != "") {
                vehicleTO.setEngineNo(vehicleCommand.getEngineNo());
            }
            if (vehicleCommand.getChassisNo() != null && vehicleCommand.getChassisNo() != "") {
                vehicleTO.setChassisNo(vehicleCommand.getChassisNo());
            }
            if (vehicleCommand.getOpId() != null && vehicleCommand.getOpId() != "") {
                vehicleTO.setOpId(vehicleCommand.getOpId());
            }
//            if (vehicleCommand.getSpId() != null && vehicleCommand.getSpId() != "") {
//                vehicleTO.setSpId(vehicleCommand.getSpId());
//            }
            if (vehicleCommand.getSeatCapacity() != null && vehicleCommand.getSeatCapacity() != "") {
                vehicleTO.setSeatCapacity(vehicleCommand.getSeatCapacity());
            }
//            if (vehicleCommand.getNextFCDate() != null && vehicleCommand.getNextFCDate() != "") {
//                vehicleTO.setNextFCDate(vehicleCommand.getNextFCDate());
//            }
            if (vehicleCommand.getKmReading() != null && vehicleCommand.getKmReading() != "") {
                vehicleTO.setKmReading(vehicleCommand.getKmReading());
            }
//            if (vehicleCommand.getHmReading() != null && vehicleCommand.getHmReading() != "") {
//                vehicleTO.setHmReading(vehicleCommand.getHmReading());
//            }
            if (vehicleCommand.getVehicleColor() != null && vehicleCommand.getVehicleColor() != "") {
                vehicleTO.setVehicleColor(vehicleCommand.getVehicleColor());
            }
            if (vehicleCommand.getVehicleCost() != null && vehicleCommand.getVehicleCost() != "") {
                vehicleTO.setVehicleCost(vehicleCommand.getVehicleCost());
            }
            if (vehicleCommand.getGroupId() != null && vehicleCommand.getGroupId() != "") {
                vehicleTO.setGroupId(vehicleCommand.getGroupId());
            }
            if (vehicleCommand.getGpsSystem() != null && vehicleCommand.getGpsSystem() != "") {
                vehicleTO.setGpsSystem(vehicleCommand.getGpsSystem());
            }
            if (vehicleCommand.getWarrantyDate() != null && vehicleCommand.getWarrantyDate() != "") {
                vehicleTO.setWarrantyDate(vehicleCommand.getWarrantyDate());
            }
            if (vehicleCommand.getAsset() != null && vehicleCommand.getAsset() != "") {
                vehicleTO.setAsset(vehicleCommand.getAsset());
            }
            if (vehicleCommand.getAxles() != null && vehicleCommand.getAxles() != "") {
                vehicleTO.setAxles(vehicleCommand.getAxles());
            }
            if (vehicleCommand.getVehicleDepreciation() != null && vehicleCommand.getVehicleDepreciation() != "") {
                vehicleTO.setVehicleDepreciation(vehicleCommand.getVehicleDepreciation());
            }
            if (vehicleCommand.getDailyKm() != null && vehicleCommand.getDailyKm() != "") {
                vehicleTO.setDailyKm(vehicleCommand.getDailyKm());
            }
            if (vehicleCommand.getDailyHm() != null && vehicleCommand.getDailyHm() != "") {
                vehicleTO.setDailyHm(vehicleCommand.getDailyHm());
            }
            if (vehicleCommand.getGpsSystemId() != null && vehicleCommand.getGpsSystemId() != "") {
                vehicleTO.setGpsSystemId(vehicleCommand.getGpsSystemId());
            }
//            if (vehicleCommand.getVehicleTypeId() != null && vehicleCommand.getVehicleTypeId() != "") {
//                vehicleTO.setVehicleTypeId(vehicleCommand.getVehicleTypeId());
//            }
            if (vehicleCommand.getTypeId() != null && vehicleCommand.getTypeId() != "") {
                vehicleTO.setTypeId(vehicleCommand.getTypeId());
            }
            if (vehicleCommand.getVendorId() != null && vehicleCommand.getVendorId() != "") {
                vehicleTO.setVendorId(vehicleCommand.getVendorId());
            }
            if (vehicleCommand.getAxleTypeId() != null && vehicleCommand.getAxleTypeId() != "") {
                vehicleTO.setAxleTypeId(vehicleCommand.getAxleTypeId());
            }
            if (vehicleCommand.getActiveInd() != null && vehicleCommand.getActiveInd() != "") {
                vehicleTO.setActiveInd(vehicleCommand.getActiveInd());
            }
            if (vehicleCommand.getVehicleId() != null && vehicleCommand.getVehicleId() != "") {
                System.out.println("vehicleCommand.getVehicleId()" + vehicleCommand.getVehicleId());
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }
            vehicleTO.setOwnerShips(request.getParameter("ownerShips"));
//            System.out.println("setOwnershipsetOwnershipsetOwnership="+request.getParameter("ownerShips"));

            int vehicleId = 0;
            vehicleId = vehicleBP.updateVehicleDetails(vehicleTO, userId, madhavaramSp);
            response.setContentType("text/css");
            if (vehicleId > 0) {
                response.getWriter().println(vehicleCommand.getVehicleId());
            } else {
                response.getWriter().println(0);
            }
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
    }

    public void updateVehicleTyreNo(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");

        int madhavaramSp = 1011;
        VehicleTO vehicleTO = new VehicleTO();
        try {
            if (vehicleCommand.getAxleDetailId() != null && vehicleCommand.getAxleDetailId() != "") {
                vehicleTO.setAxleDetailId(vehicleCommand.getAxleDetailId());
            }
            if (vehicleCommand.getPositionName() != null && vehicleCommand.getPositionName() != "") {
                vehicleTO.setPositionName(vehicleCommand.getPositionName());
            }
            if (vehicleCommand.getPositionNo() != null && vehicleCommand.getPositionNo() != "") {
                vehicleTO.setPositionNo(vehicleCommand.getPositionNo());
            }
            if (vehicleCommand.getUpdateValue() != null && vehicleCommand.getUpdateValue() != "") {
                vehicleTO.setUpdateValue(vehicleCommand.getUpdateValue());
            }
            if (vehicleCommand.getUpdateType() != null && vehicleCommand.getUpdateType() != "") {
                vehicleTO.setUpdateType(vehicleCommand.getUpdateType());
            }
            if (vehicleCommand.getDepthVal() != null && vehicleCommand.getDepthVal() != "") {
                vehicleTO.setDepthVal(vehicleCommand.getDepthVal());
            }
            if (vehicleCommand.getTyreNo() != null && vehicleCommand.getTyreNo() != "") {
                vehicleTO.setTyreNo(vehicleCommand.getTyreNo());
            }
            if (vehicleCommand.getVehicleId() != null && vehicleCommand.getVehicleId() != "") {
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }
            if (vehicleCommand.getKmReading() != null && vehicleCommand.getKmReading() != "") {
                vehicleTO.setKmReading(vehicleCommand.getKmReading());
            }
            int updateStatus = 0;
            updateStatus = vehicleBP.updateVehicleTyreDetails(vehicleTO, userId, madhavaramSp);
            response.setContentType("text/css");
            if (updateStatus > 0) {
                response.getWriter().println(updateStatus);
            } else {
                response.getWriter().println(0);
            }
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
    }


    public void updateInsuranceDetails(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        System.out.println("entered updateInsuranceDetails");
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");
//        int companyType = (Integer) session.getAttribute("companyTypeId");
//        String companyId = (String) session.getAttribute("companyId");
//
        int madhavaramSp = 1011;
        VehicleTO vehicleTO = new VehicleTO();
        System.out.println("bsdh");
        try {
            if (vehicleCommand.getInsPolicy() != null && vehicleCommand.getInsPolicy() != "") {
                vehicleTO.setInsPolicy(vehicleCommand.getInsPolicy());
            }
            if (vehicleCommand.getPrevPolicy() != null && vehicleCommand.getPrevPolicy() != "") {
                vehicleTO.setPrevPolicy(vehicleCommand.getPrevPolicy());
            }
            if (vehicleCommand.getPackageType() != null && vehicleCommand.getPackageType() != "") {
                vehicleTO.setPackageType(vehicleCommand.getPackageType());
            }
            if (vehicleCommand.getInsName() != null && vehicleCommand.getInsName() != "") {
                vehicleTO.setInsName(vehicleCommand.getInsName());
            }
            if (vehicleCommand.getInsAddress() != null && vehicleCommand.getInsAddress() != "") {
                vehicleTO.setInsAddress(vehicleCommand.getInsAddress());
            }
            if (vehicleCommand.getInsMobileNo() != null && vehicleCommand.getInsMobileNo() != "") {
                vehicleTO.setInsMobileNo(vehicleCommand.getInsMobileNo());
            }
            if (vehicleCommand.getInsPolicyNo() != null && vehicleCommand.getInsPolicyNo() != "") {
                vehicleTO.setInsPolicyNo(vehicleCommand.getInsPolicyNo());
            }
            if (vehicleCommand.getInsIdvValue() != null && vehicleCommand.getInsIdvValue() != "") {
                vehicleTO.setInsIdvValue(vehicleCommand.getInsIdvValue());
            }
            if (vehicleCommand.getFromDate1() != null && vehicleCommand.getFromDate1() != "") {
                vehicleTO.setFromDate1(vehicleCommand.getFromDate1());
            }
            if (vehicleCommand.getToDate1() != null && vehicleCommand.getToDate1() != "") {
                vehicleTO.setToDate1(vehicleCommand.getToDate1());
            }
            if (vehicleCommand.getPremiunAmount() != null && vehicleCommand.getPremiunAmount() != "") {
                vehicleTO.setPremiunAmount(vehicleCommand.getPremiunAmount());
            }
            if (vehicleCommand.getVehicleId() != null && vehicleCommand.getVehicleId() != "") {
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }

            if (vehicleCommand.getPaymentType() != null && vehicleCommand.getPaymentType() != "") {
                vehicleTO.setPaymentType(vehicleCommand.getPaymentType());
            }
            if (vehicleCommand.getBankId() != null && vehicleCommand.getBankId() != "") {
                vehicleTO.setBankId(vehicleCommand.getBankId());
            }
            if (vehicleCommand.getBankBranchId() != null && vehicleCommand.getBankBranchId() != "") {
                vehicleTO.setBankBranchId(vehicleCommand.getBankBranchId());
            }
//            String bankBranchId[] =vehicleCommand.getBankBranchId().split("~");
//            vehicleTO.setBankBranchId(bankBranchId[0]);
//            vehicleTO.setCreditLedgerId(bankBranchId[1]);
//            System.out.println("");
            System.out.println("1");
            if (vehicleCommand.getChequeDate() != null && vehicleCommand.getChequeDate() != "") {
                vehicleTO.setChequeDate(vehicleCommand.getChequeDate());
            }
            if (vehicleCommand.getChequeNo() != null && vehicleCommand.getChequeNo() != "") {
                vehicleTO.setChequeNo(vehicleCommand.getChequeNo());
            }
            if (vehicleCommand.getChequeAmount() != null && vehicleCommand.getChequeAmount() != "") {
                vehicleTO.setChequeAmount(vehicleCommand.getChequeAmount());
            }
            if (vehicleCommand.getClearanceDate() != null && vehicleCommand.getClearanceDate() != "") {
                vehicleTO.setClearanceDate(vehicleCommand.getClearanceDate());
            }
            if (vehicleCommand.getLedgerId() != null && vehicleCommand.getLedgerId() != "") {
                vehicleTO.setLedgerId(vehicleCommand.getLedgerId());
            }

            if (vehicleCommand.getPaymentDate() != null && vehicleCommand.getPaymentDate() != "") {
                vehicleTO.setPaymentDate(vehicleCommand.getPaymentDate());
            }
            if (vehicleCommand.getCashOnHandLedgerId() != null && vehicleCommand.getCashOnHandLedgerId() != "") {
                vehicleTO.setCashOnHandLedgerId(vehicleCommand.getCashOnHandLedgerId());
            }
            if (vehicleCommand.getCashOnHandLedgerCode() != null && vehicleCommand.getCashOnHandLedgerCode() != "") {
                vehicleTO.setCashOnHandLedgerCode(vehicleCommand.getCashOnHandLedgerCode());
            }
            System.out.println("2");

            if (vehicleCommand.getInsuranceid() != null && vehicleCommand.getInsuranceid() != "") {
                vehicleTO.setInsuranceid(vehicleCommand.getInsuranceid());
            }
            if (vehicleCommand.getClearanceId() != null && vehicleCommand.getClearanceId() != "") {
                vehicleTO.setClearanceId(vehicleCommand.getClearanceId());
            }
            if (vehicleCommand.getVendorId1() != null && vehicleCommand.getVendorId1() != "") {
                vehicleTO.setVendorId1(vehicleCommand.getVendorId1());
            }
            if (vehicleCommand.getFleetTypeId() != null && vehicleCommand.getFleetTypeId() != "") {
                vehicleTO.setFleetTypeId(vehicleCommand.getFleetTypeId());
            }
            if (vehicleCommand.getIssuingOffice() != null && vehicleCommand.getIssuingOffice() != "") {
                vehicleTO.setIssuingOffice(vehicleCommand.getIssuingOffice());
            }

            System.out.println("FleetTypeIdcontroller" + vehicleCommand.getFleetTypeId());

            int updateStatus = 0;
            updateStatus = vehicleBP.updateInsuranceDetails(vehicleTO, userId, madhavaramSp);
            System.out.println("updateStatus controller" + updateStatus);
            response.setContentType("text/css");
            if (updateStatus > 0) {
                response.getWriter().println(updateStatus);
            } else {
                response.getWriter().println(0);
            }
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
    }

    public void insertRoadTaxDetails(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");
//        int companyType = (Integer) session.getAttribute("companyTypeId");
//        String companyId = (String) session.getAttribute("companyId");

        int madhavaramSp = 1011;
        VehicleTO vehicleTO = new VehicleTO();
        try {
            if (vehicleCommand.getRoadTaxId() != null && !vehicleCommand.getRoadTaxId().equals("")) {
                vehicleTO.setRoadTaxId(vehicleCommand.getRoadTaxId());
            }
            if (vehicleCommand.getRoadTaxReceiptNo() != null && !vehicleCommand.getRoadTaxReceiptNo().equals("")) {
                vehicleTO.setRoadTaxReceiptNo(vehicleCommand.getRoadTaxReceiptNo());
            }
            if (vehicleCommand.getRoadTaxReceiptDate() != null && !vehicleCommand.getRoadTaxReceiptDate().equals("")) {
                vehicleTO.setRoadTaxReceiptDate(vehicleCommand.getRoadTaxReceiptDate());
            }
            if (vehicleCommand.getRoadTaxPaidLocation() != null && !vehicleCommand.getRoadTaxPaidLocation().equals("")) {
                vehicleTO.setRoadTaxPaidLocation(vehicleCommand.getRoadTaxPaidLocation());
            }

            if (vehicleCommand.getRoadTaxPeriod() != null && !vehicleCommand.getRoadTaxPeriod().equals("")) {
                vehicleTO.setRoadTaxPeriod(vehicleCommand.getRoadTaxPeriod());
            }
            if (vehicleCommand.getRoadTaxPaidAmount() != 0) {
                vehicleTO.setRoadTaxPaidAmount(vehicleCommand.getRoadTaxPaidAmount());
            }
            if (vehicleCommand.getRoadTaxFromDate() != null && !vehicleCommand.getRoadTaxFromDate().equals("")) {
                vehicleTO.setRoadTaxFromDate(vehicleCommand.getRoadTaxFromDate());
            }
            if (vehicleCommand.getNextRoadTaxDate() != null && !vehicleCommand.getNextRoadTaxDate().equals("")) {
                vehicleTO.setNextRoadTaxDate(vehicleCommand.getNextRoadTaxDate());
            }
            if (vehicleCommand.getRoadTaxRemarks() != null && !vehicleCommand.getRoadTaxRemarks().equals("")) {
                vehicleTO.setRoadTaxRemarks(vehicleCommand.getRoadTaxRemarks());
            }
            if (vehicleCommand.getVehicleId() != null && vehicleCommand.getVehicleId() != "") {
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }
            System.out.println("vehicleCommand.getVehicleId()=" + vehicleCommand.getVehicleId());

            if (vehicleCommand.getPaymentTypeRT() != null && vehicleCommand.getPaymentTypeRT() != "") {
                vehicleTO.setPaymentTypeRT(vehicleCommand.getPaymentTypeRT());
            }
            if (vehicleCommand.getBankIdRT() != null && vehicleCommand.getBankIdRT() != "") {
                vehicleTO.setBankIdRT(vehicleCommand.getBankIdRT());
            }
            if (vehicleCommand.getBankBranchIdRT() != null && vehicleCommand.getBankBranchIdRT() != "") {
                vehicleTO.setBankBranchIdRT(vehicleCommand.getBankBranchIdRT());
            }
            if (vehicleCommand.getChequeDateRT() != null && vehicleCommand.getChequeDateRT() != "") {
                vehicleTO.setChequeDateRT(vehicleCommand.getChequeDateRT());
            }
            if (vehicleCommand.getChequeNoRT() != null && vehicleCommand.getChequeNoRT() != "") {
                vehicleTO.setChequeNoRT(vehicleCommand.getChequeNoRT());
            }
            if (vehicleCommand.getChequeAmountRT() != null && vehicleCommand.getChequeAmountRT() != "") {
                vehicleTO.setChequeAmountRT(vehicleCommand.getChequeAmountRT());
            }
            if (vehicleCommand.getClearanceDateRT() != null && vehicleCommand.getClearanceDateRT() != "") {
                vehicleTO.setClearanceDateRT(vehicleCommand.getClearanceDateRT());
            }
            if (vehicleCommand.getClearanceIdRT() != null && vehicleCommand.getClearanceIdRT() != "") {
                vehicleTO.setClearanceIdRT(vehicleCommand.getClearanceIdRT());
            }

            if (vehicleCommand.getLedgerId() != null && vehicleCommand.getLedgerId() != "") {
                vehicleTO.setLedgerId(vehicleCommand.getLedgerId());
            }

            if (vehicleCommand.getPaymentDateRT() != null && vehicleCommand.getPaymentDateRT() != "") {
                vehicleTO.setPaymentDateRT(vehicleCommand.getPaymentDateRT());
            }
            if (vehicleCommand.getCashOnHandLedgerId() != null && vehicleCommand.getCashOnHandLedgerId() != "") {
                vehicleTO.setCashOnHandLedgerId(vehicleCommand.getCashOnHandLedgerId());
            }
            if (vehicleCommand.getCashOnHandLedgerCode() != null && vehicleCommand.getCashOnHandLedgerCode() != "") {
                vehicleTO.setCashOnHandLedgerCode(vehicleCommand.getCashOnHandLedgerCode());
            }
            System.out.println("vehicleCommand.getVehicleId()11=" + vehicleCommand.getVehicleId());
            int updateStatus = 0;
            updateStatus = vehicleBP.insertRoadTaxDetails(vehicleTO, userId, madhavaramSp);
            response.setContentType("text/css");
            if (updateStatus > 0) {
                response.getWriter().println(updateStatus);
            } else {
                response.getWriter().println(0);
            }
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
    }

    public void insertFCDetails(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");
//        int companyType = (Integer) session.getAttribute("companyTypeId");
//        String companyId = (String) session.getAttribute("companyId");

        int madhavaramSp = 1011;
        VehicleTO vehicleTO = new VehicleTO();
        try {
            if (vehicleCommand.getFcid() != null && !vehicleCommand.getFcid().equals("")) {
                vehicleTO.setFcid(vehicleCommand.getFcid());
            }
            if (vehicleCommand.getRtoDetail() != null && !vehicleCommand.getRtoDetail().equals("")) {
                vehicleTO.setRtoDetail(vehicleCommand.getRtoDetail());
            }
            if (vehicleCommand.getFcDate() != null && !vehicleCommand.getFcDate().equals("")) {
                vehicleTO.setFcDate(vehicleCommand.getFcDate());
            }
            if (vehicleCommand.getFcReceiptNo() != null && !vehicleCommand.getFcReceiptNo().equals("")) {
                vehicleTO.setFcReceiptNo(vehicleCommand.getFcReceiptNo());
            }

            if (vehicleCommand.getFcExpiryDate() != null && !vehicleCommand.getFcExpiryDate().equals("")) {
                vehicleTO.setFcExpiryDate(vehicleCommand.getFcExpiryDate());
            }
            if (vehicleCommand.getFcAmount() != 0) {
                vehicleTO.setFcAmount(vehicleCommand.getFcAmount());
            }
            if (vehicleCommand.getFcRemarks() != null && !vehicleCommand.getFcRemarks().equals("")) {
                vehicleTO.setFcRemarks(vehicleCommand.getFcRemarks());
            }
            if (vehicleCommand.getVehicleId() != null && vehicleCommand.getVehicleId() != "") {
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }

            if (vehicleCommand.getPaymentTypeFC() != null && vehicleCommand.getPaymentTypeFC() != "") {
                vehicleTO.setPaymentTypeFC(vehicleCommand.getPaymentTypeFC());
            }
            if (vehicleCommand.getBankIdFC() != null && vehicleCommand.getBankIdFC() != "") {
                vehicleTO.setBankIdFC(vehicleCommand.getBankIdFC());
            }
            if (vehicleCommand.getBankBranchIdFC() != null && vehicleCommand.getBankBranchIdFC() != "") {
                vehicleTO.setBankBranchIdFC(vehicleCommand.getBankBranchIdFC());
            }
            if (vehicleCommand.getChequeDateFC() != null && vehicleCommand.getChequeDateFC() != "") {
                vehicleTO.setChequeDateFC(vehicleCommand.getChequeDateFC());
            }

            if (vehicleCommand.getChequeNoFC() != null && vehicleCommand.getChequeNoFC() != "") {
                vehicleTO.setChequeNoFC(vehicleCommand.getChequeNoFC());
            }
            if (vehicleCommand.getChequeAmountFC() != null && vehicleCommand.getChequeAmountFC() != "") {
                vehicleTO.setChequeAmountFC(vehicleCommand.getChequeAmountFC());
            }
            if (vehicleCommand.getClearanceDateFC() != null && vehicleCommand.getClearanceDateFC() != "") {
                vehicleTO.setClearanceDateFC(vehicleCommand.getClearanceDateFC());
            }
            if (vehicleCommand.getClearanceIdFC() != null && vehicleCommand.getClearanceIdFC() != "") {
                vehicleTO.setClearanceIdFC(vehicleCommand.getClearanceIdFC());
            }
            if (vehicleCommand.getLedgerId() != null && vehicleCommand.getLedgerId() != "") {
                vehicleTO.setLedgerId(vehicleCommand.getLedgerId());
            }
            if (vehicleCommand.getCashOnHandLedgerId() != null && vehicleCommand.getCashOnHandLedgerId() != "") {
                vehicleTO.setCashOnHandLedgerId(vehicleCommand.getCashOnHandLedgerId());
            }
            if (vehicleCommand.getCashOnHandLedgerCode() != null && vehicleCommand.getCashOnHandLedgerCode() != "") {
                vehicleTO.setCashOnHandLedgerCode(vehicleCommand.getCashOnHandLedgerCode());
            }
            if (vehicleCommand.getPaymentDateFC() != null && vehicleCommand.getPaymentDateFC() != "") {
                vehicleTO.setPaymentDateFC(vehicleCommand.getPaymentDateFC());
            }
            if (vehicleCommand.getFleetTypeId() != null && vehicleCommand.getFleetTypeId() != "") {
                vehicleTO.setFleetTypeId(vehicleCommand.getFleetTypeId());
            }
            if (vehicleCommand.getReportNo() != null && vehicleCommand.getReportNo() != "") {
                vehicleTO.setReportNo(vehicleCommand.getReportNo());
            }
            if (vehicleCommand.getOdometer() != null && vehicleCommand.getOdometer() != "") {
                vehicleTO.setOdometer(vehicleCommand.getOdometer());
            }
            int updateStatus = 0;
            updateStatus = vehicleBP.insertFCDetails(vehicleTO, userId, madhavaramSp);
            response.setContentType("text/css");
            if (updateStatus > 0) {
                response.getWriter().println(updateStatus);
            } else {
                response.getWriter().println(0);
            }
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
    }

    public void insertPermitDetails(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");

        int madhavaramSp = 1011;
        VehicleTO vehicleTO = new VehicleTO();
        try {
            if (vehicleCommand.getPermitid() != null && !vehicleCommand.getPermitid().equals("")) {
                vehicleTO.setPermitid(vehicleCommand.getPermitid());
            }
            if (vehicleCommand.getPermitType() != null && !vehicleCommand.getPermitType().equals("")) {
                vehicleTO.setPermitType(vehicleCommand.getPermitType());
            }
            if (vehicleCommand.getPermitNo() != null && !vehicleCommand.getPermitNo().equals("")) {
                vehicleTO.setPermitNo(vehicleCommand.getPermitNo());
            }
            if (vehicleCommand.getPermitAmount() != 0) {
                vehicleTO.setPermitAmount(vehicleCommand.getPermitAmount());
            }
            if (vehicleCommand.getPermitPaidDate() != null && !vehicleCommand.getPermitPaidDate().equals("")) {
                vehicleTO.setPermitPaidDate(vehicleCommand.getPermitPaidDate());
            }
            if (vehicleCommand.getPermitExpiryDate() != null && !vehicleCommand.getPermitExpiryDate().equals("")) {
                vehicleTO.setPermitExpiryDate(vehicleCommand.getPermitExpiryDate());
            }
            if (vehicleCommand.getPermitRemarks() != null && !vehicleCommand.getPermitRemarks().equals("")) {
                vehicleTO.setPermitRemarks(vehicleCommand.getPermitRemarks());
            }
            if (vehicleCommand.getVehicleId() != null && vehicleCommand.getVehicleId() != "") {
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }


            if (vehicleCommand.getPaymentTypePermit() != null && vehicleCommand.getPaymentTypePermit() != "") {
                vehicleTO.setPaymentTypePermit(vehicleCommand.getPaymentTypePermit());
            }
            if (vehicleCommand.getBankIdPermit() != null && vehicleCommand.getBankIdPermit() != "") {
                vehicleTO.setBankIdPermit(vehicleCommand.getBankIdPermit());
            }
            if (vehicleCommand.getBankBranchIdPermit() != null && vehicleCommand.getBankBranchIdPermit() != "") {
                vehicleTO.setBankBranchIdPermit(vehicleCommand.getBankBranchIdPermit());
            }
            if (vehicleCommand.getChequeDatePermit() != null && vehicleCommand.getChequeDatePermit() != "") {
                vehicleTO.setChequeDatePermit(vehicleCommand.getChequeDatePermit());
            }

            if (vehicleCommand.getChequeNoPermit() != null && vehicleCommand.getChequeNoPermit() != "") {
                vehicleTO.setChequeNoPermit(vehicleCommand.getChequeNoPermit());
            }
            if (vehicleCommand.getChequeAmountPermit() != null && vehicleCommand.getChequeAmountPermit() != "") {
                vehicleTO.setChequeAmountPermit(vehicleCommand.getChequeAmountPermit());
            }
            if (vehicleCommand.getClearanceDatePermit() != null && vehicleCommand.getClearanceDatePermit() != "") {
                vehicleTO.setClearanceDatePermit(vehicleCommand.getClearanceDatePermit());
            }
            if (vehicleCommand.getClearanceIdPermit() != null && vehicleCommand.getClearanceIdPermit() != "") {
                vehicleTO.setClearanceIdPermit(vehicleCommand.getClearanceIdPermit());
            }
            if (vehicleCommand.getPermitid() != null && vehicleCommand.getPermitid() != "") {
                vehicleTO.setPermitid(vehicleCommand.getPermitid());
            }

            if (vehicleCommand.getLedgerId() != null && vehicleCommand.getLedgerId() != "") {
                vehicleTO.setLedgerId(vehicleCommand.getLedgerId());
            }

//            if (vehicleCommand.getPaymentDatePermit() != null && vehicleCommand.getPaymentDatePermit() != "") {
//                vehicleTO.setPaymentDatePermit(vehicleCommand.getPaymentDatePermit());
//            }
            if (vehicleCommand.getCashOnHandLedgerId() != null && vehicleCommand.getCashOnHandLedgerId() != "") {
                vehicleTO.setCashOnHandLedgerId(vehicleCommand.getCashOnHandLedgerId());
            }
            if (vehicleCommand.getCashOnHandLedgerCode() != null && vehicleCommand.getCashOnHandLedgerCode() != "") {
                vehicleTO.setCashOnHandLedgerCode(vehicleCommand.getCashOnHandLedgerCode());
            }

            int updateStatus = 0;
            updateStatus = vehicleBP.insertPermitDetails(vehicleTO, userId, madhavaramSp);
            response.setContentType("text/css");
            if (updateStatus > 0) {
                response.getWriter().println(updateStatus);
            } else {
                response.getWriter().println(0);
            }
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
    }

    public void saveVehicleDepreciation(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");

        int madhavaramSp = 1011;
        VehicleTO vehicleTO = new VehicleTO();
        try {
            if (vehicleCommand.getVehicleYear() != null && !vehicleCommand.getVehicleYear().equals("")) {
                vehicleTO.setVehicleYear(vehicleCommand.getVehicleYear());
            }
            if (vehicleCommand.getDepreciation() != null && !vehicleCommand.getDepreciation().equals("")) {
                vehicleTO.setDepreciation(vehicleCommand.getDepreciation());
            }
            if (vehicleCommand.getYearCost() != null && !vehicleCommand.getYearCost().equals("")) {
                vehicleTO.setYearCost(vehicleCommand.getYearCost());
            }
            if (vehicleCommand.getPerMonth() != null && !vehicleCommand.getPerMonth().equals("")) {
                vehicleTO.setPerMonth(vehicleCommand.getPerMonth());
            }
            if (vehicleCommand.getVehicleId() != null && vehicleCommand.getVehicleId() != "") {
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }
            if (vehicleCommand.getVehicleDepreciationCost() != null && vehicleCommand.getVehicleDepreciationCost() != "") {
                vehicleTO.setVehicleDepreciationCost(vehicleCommand.getVehicleDepreciationCost());
            }
            if (vehicleCommand.getDepreciationType() != null && vehicleCommand.getDepreciationType() != "") {
                vehicleTO.setDepreciationType(vehicleCommand.getDepreciationType());
            }
            int updateStatus = 0;
            updateStatus = vehicleBP.insertVehicleDepreciation(vehicleTO, userId, madhavaramSp);
            response.setContentType("text/css");
            if (updateStatus > 0) {
                response.getWriter().println(updateStatus);
            } else {
                response.getWriter().println(0);
            }
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
    }

    public void uploadVehicleDocuments(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        vehicleCommand = command;
        HttpSession session = request.getSession();
        System.out.println("uploadVehicleDocuments");
        int userId = (Integer) session.getAttribute("userId");
//        int companyType = (Integer) session.getAttribute("companyTypeId");
//        String companyId = (String) session.getAttribute("companyId");
        String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        String newFileName = "", actualFilePath = "";
        int madhavaramSp = 1011;
        int i = 0;
        int j = 0;
        VehicleTO vehicleTO = new VehicleTO();
        String[] fileSaved = new String[10];
        String[] uploadedFileName = new String[10];
        String[] tempFilePath = new String[10];
        String vehicleId = request.getParameter("vehicleId");
        System.out.println("vehicleId" + vehicleId);
        String narration = request.getParameter("narrationVal");
        System.out.println("narration" + narration);
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        Part partObj = null;
        FilePart fPart = null;
        if (isMultipart) {
            // Create a factory for disk-based file items
            FileItemFactory factory = new DiskFileItemFactory();

            // Create a new file upload handler
            ServletFileUpload upload = new ServletFileUpload(factory);
            try {
                // Parse the request
                List /* FileItem */ items = upload.parseRequest(request);
                Iterator iterator = items.iterator();
//                    String remarks[]=null;
//                    String remrk = "";
//                    remarks=narration.split(",");
                while (iterator.hasNext()) {
                    FileItem item = (FileItem) iterator.next();
                    if (!item.isFormField()) {
                        fileName = item.getName();
                        System.out.println("fileName" + fileName);
                        String root = getServletContext().getRealPath("/");
                        File path = new File(root + "/uploads");
                        if (!path.exists()) {
                            boolean status = path.mkdirs();
                        }

                        File uploadedFile = new File(path + "/" + fileName);
                        System.out.println("uploadedFile" + uploadedFile);
                        item.write(uploadedFile);
                        response.setContentType("text/css");
                        response.getWriter().println(uploadedFile);
                        tempFilePath[j] = tempServerFilePath + "//" + uploadedFile;
                        actualFilePath = actualServerFilePath + "//" + tempFilePath;
                        System.out.println("tempPath..." + tempFilePath);
                        System.out.println("actPath..." + actualFilePath);
//                            long fileSize = fPart.writeTo(new java.io.File(tempFilePath[j]));
//                            System.out.println("fileSize..." + fileSize);
                        File f1 = new File(actualFilePath);
                        System.out.println("check " + f1.isFile());
                        f1.renameTo(new File(tempFilePath[j]));
                        System.out.println("tempPath = " + tempFilePath);
//                            System.out.println("actPath = " + actualFilePath)
                        System.out.println(uploadedFile.getAbsolutePath());
                    }
                    vehicleTO.setVehicleId(vehicleId);
                    String file = "";
                    String saveFile = "";
                    file = tempFilePath[j];
                    saveFile = fileSaved[j];
                    vehicleTO.setVehicleId(vehicleId);


//                    if(remarks[j] != null && remarks[j] !=""){
//                    remrk=remarks[j];
//                            }else{
//                        remrk="";
//                    }
                    int status = 0;
                    status = vehicleBP.saveFileUploadDetails(file, fileName, narration, vehicleTO, userId);
                }
            } catch (FileUploadException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void getMfrVehicleType(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws IOException {
        HttpSession session = request.getSession();
        vehicleCommand = command;
        VehicleTO vehicleTO = new VehicleTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList list = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String mfrId = "";
            String fleetTypeId = "";
            response.setContentType("text/html");
            mfrId = request.getParameter("mfrId");
            fleetTypeId = request.getParameter("fleetTypeId");
            vehicleTO.setMfrId(mfrId);
            vehicleTO.setFleetTypeId(fleetTypeId);
            list = vehicleBP.processActiveTypeList(vehicleTO);
            System.out.println("list.size() = " + list.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = list.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                vehicleTO = (VehicleTO) itr.next();
                jsonObject.put("Id", vehicleTO.getTypeId());
                jsonObject.put("Name", vehicleTO.getTypeName());
                jsonObject.put("AxleTypeId", vehicleTO.getAxleTypeId());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void insertOemDetails(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws IOException, FPRuntimeException, FPBusinessException {
	        System.out.println("hi i m OEMcontroller");
	        vehicleCommand = command;
	        VehicleTO vehicleTO = new VehicleTO();
	        HttpSession session = request.getSession();
	        String path = "";

	        int userId = (Integer) session.getAttribute("userId");
	        int companyType = (Integer) session.getAttribute("companyTypeId");
	        String companyId = (String) session.getAttribute("companyId");
	        System.out.println("in controller");
	        int madhavaramSp = 1011;
	        PrintWriter pw = response.getWriter();
	        response.setContentType("text/html");
	        if (vehicleCommand.getVehicleId() != null && vehicleCommand.getVehicleId() != "") {
	            vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
	        }
	        if (vehicleCommand.getKmReading() != null && vehicleCommand.getKmReading() != "") {
	            vehicleTO.setKmReading(vehicleCommand.getKmReading());
	        }
	        String[] oemDetailsId = request.getParameterValues("oemDetailsIdVal[]");
	        String[] oemMfr = request.getParameterValues("oemMfrVal[]");
	        String[] oemTyreNo = request.getParameterValues("oemTyreNoVal[]");
	        String[] oemDepth = request.getParameterValues("oemDepthVal[]");
	        String[] oemBattery = request.getParameterValues("oemBatteryVal[]");
	        String[] oemId = request.getParameterValues("oemIdVal[]");
	        String oemIds = null;
	        JSONArray jsonArray = new JSONArray();
	        for (int i = 0; i < oemDetailsId.length; i++) {
	            System.out.println("oemDetailsIdVal" + oemDetailsId[i]);
	            System.out.println("oemMfrVal" + oemMfr[i]);
	            System.out.println("oemTyreNoVal" + oemTyreNo[i]);
	            System.out.println("oemBatteryVal" + oemBattery[i]);
	            int oemId1 = 0;
	//                System.out.println("oemId Throttle Here = " + oemId);
	            if (oemId[i].equals("0")) {
	                oemId1 = vehicleBP.insertOemDetails(vehicleTO, oemDetailsId[i], oemMfr[i], oemTyreNo[i], oemDepth[i], oemBattery[i], oemId[i], userId);
	            } else {
	                oemId1 = Integer.parseInt(oemId[i]);
	            }
	            JSONObject jsonObject = new JSONObject();
//	            jsonObject.put("OemId", oemId1);
	            jsonArray.put(jsonObject);
	            System.out.println("oemIds" + oemIds);
	        }
	        System.out.println("jsonArray = " + jsonArray);
	        pw.print(jsonArray);
    }

    public void getTrailerNo(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws IOException {
        HttpSession session = request.getSession();
        vehicleCommand = command;
        VehicleTO vehicleTO = new VehicleTO();
        System.out.println("jaiiiiii");
        //        JsonTO jsonTO = new JsonTO();
        ArrayList vehicleList = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String trailerNo = "";
            response.setContentType("text/html");
            trailerNo = request.getParameter("trailerNo");
            System.out.println("trailerNo" + trailerNo);
            vehicleTO.setTrailerNo(trailerNo);
            vehicleList = vehicleBP.getTrailerNoList(vehicleTO);
            //////System.out.println("vehicleList.size() = " + vehicleList.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = vehicleList.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                vehicleTO = (VehicleTO) itr.next();
                jsonObject.put("Name", vehicleTO.getTrailerId() + "-" + vehicleTO.getTrailerNo());
                //System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            //////System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView viewVehicleTrailer(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String pageTitle = "";
        String menuPath = "Trailer  >>  Search Trailer";
        vehicleCommand = command;
        ArrayList vehicleDetail = new ArrayList();
        VehicleTO vehicleTO = null;
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Vehicle-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                pageTitle = "Alter Trailer";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                request.setAttribute("pageTitle", pageTitle);
                path = "content/Vehicle/viewVehicleTrailer.jsp";
                String vehicleId = request.getParameter("trailerId");

                vehicleTO = new VehicleTO();
                if (vehicleId != null && vehicleId != "") {
                    vehicleTO.setVehicleId(vehicleId);
                }

                ArrayList MfrList = new ArrayList();
                MfrList = vehicleBP.processGetMfrList();
                request.setAttribute("MfrList", MfrList);

                ArrayList usageList = new ArrayList();
                usageList = vehicleBP.processGetUsageList();
                request.setAttribute("UsageList", usageList);

                ArrayList classList = new ArrayList();
                classList = vehicleBP.processGetClassList();
                request.setAttribute("ClassList", classList);

                ArrayList modelList = new ArrayList();
                modelList = vehicleBP.processActiveModelList();
                request.setAttribute("modelList", modelList);

                ArrayList customerList = new ArrayList();
                customerList = customerBP.processActiveCustomerList();
                request.setAttribute("customerList", customerList);

                ArrayList groupList = new ArrayList();
                groupList = vehicleBP.getGroupList();
                request.setAttribute("groupList", groupList);

                //OperationPointList
                ArrayList operationPointList = new ArrayList();
                operationPointList = vehicleBP.processGetopList();
                request.setAttribute("OperationPointList", operationPointList);

                // get Vehicle tyre items list
                ArrayList tyreItemList = new ArrayList();
                tyreItemList = vehicleBP.processTyreItems();
                request.setAttribute("tyreItemList", tyreItemList);

                //            ArrayList positionList = new ArrayList();
                //            positionList = mrsBP.getTyrePostions();
                //            request.setAttribute("positionList", positionList);
                //get positions list
                ArrayList positionList = new ArrayList();
                positionList = vehicleBP.getTyrePostions();
                request.setAttribute("positionList", positionList);
                System.out.println("positionList size=" + positionList.size());

                // get Vehicle TYre positions
                ArrayList vehTyreList = new ArrayList();
                vehTyreList = vehicleBP.processTyrePosition(Integer.parseInt(vehicleId));
                request.setAttribute("vehTyreList", vehTyreList);

                ArrayList leasingCustList = new ArrayList();
                leasingCustList = vehicleBP.getLeasingCustomerList();
                request.setAttribute("leasingCustList", leasingCustList);

                vehicleDetail = vehicleBP.processTrailerDetail(vehicleTO);
                request.setAttribute("vehicleDetail", vehicleDetail);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewManageVessel(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String menuPath = "Vehicle  >>  Vehicle >>  View Vessel";

        try {
            if (!loginBP.checkAuthorisation(userFunctions, "VehicleType-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "content/Vehicle/manageVessel.jsp";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

                String pageTitle = "View Vehicle Type";
                request.setAttribute("pageTitle", pageTitle);

                ArrayList TypeList = new ArrayList();
                TypeList = vehicleBP.processGetTypeList();
                request.setAttribute("TypeList", TypeList);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Vehicle Type  data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void saveVessel(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");

        int madhavaramSp = 1011;
        VehicleTO vehicleTO = new VehicleTO();
        try {
            if (vehicleCommand.getVesselName() != null && vehicleCommand.getVesselName() != "") {
                vehicleTO.setVesselName(vehicleCommand.getVesselName());
            }
            if (vehicleCommand.getVesselCode() != null && vehicleCommand.getVesselCode() != "") {
                vehicleTO.setVesselCode(vehicleCommand.getVesselCode());
            }
            if (vehicleCommand.getVesselType() != null && vehicleCommand.getVesselType() != "") {
                vehicleTO.setVesselType(vehicleCommand.getVesselType());
            }
            if (vehicleCommand.getLinerCode() != null && vehicleCommand.getLinerCode() != "") {
                vehicleTO.setLinerCode(vehicleCommand.getLinerCode());
            }

            int updateStatus = 0;
            updateStatus = vehicleBP.insertVessel(vehicleTO, userId);
            response.setContentType("text/css");
            if (updateStatus > 0) {
                response.getWriter().println(updateStatus);
            } else {
                response.getWriter().println(0);
            }
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
    }

    public ModelAndView handleDischargeVessel(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String menuPath = "Vehicle  >>  Vehicle >>  View Vessel";

        try {
            if (!loginBP.checkAuthorisation(userFunctions, "VehicleType-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "content/Vehicle/dischargeVessel.jsp";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

                String pageTitle = "View Vehicle Type";
                request.setAttribute("pageTitle", pageTitle);

                ArrayList TypeList = new ArrayList();
                TypeList = vehicleBP.processGetTypeList();
                request.setAttribute("TypeList", TypeList);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Vehicle Type  data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getVesselName(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws IOException {
        HttpSession session = request.getSession();
        vehicleCommand = command;
        VehicleTO vehicleTO = new VehicleTO();
        System.out.println("jaiiiiii");
        //        JsonTO jsonTO = new JsonTO();
        ArrayList vesselNameList = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String vesselName = "";
            response.setContentType("text/html");
            vesselName = request.getParameter("dischargeVesselName");
            System.out.println("vesselName" + vesselName);
            vehicleTO.setDischargeVesselName(vesselName);
            vesselNameList = vehicleBP.getVesselNameList(vehicleTO);
            //////System.out.println("vehicleList.size() = " + vehicleList.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = vesselNameList.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                vehicleTO = (VehicleTO) itr.next();
                jsonObject.put("Name", vehicleTO.getVesselId() + "-" + vehicleTO.getDischargeVesselName());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            //////System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void savedischargeVessel(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");

        int madhavaramSp = 1011;
        VehicleTO vehicleTO = new VehicleTO();
        try {
            if (vehicleCommand.getDischargeVesselName() != null && vehicleCommand.getDischargeVesselName() != "") {
                vehicleTO.setDischargeVesselName(vehicleCommand.getDischargeVesselName());
            }
            if (vehicleCommand.getVesselId() != null && vehicleCommand.getVesselId() != "") {
                vehicleTO.setVesselId(vehicleCommand.getVesselId());
            }
            if (vehicleCommand.getVoyageNo() != null && vehicleCommand.getVoyageNo() != "") {
                vehicleTO.setVoyageNo(vehicleCommand.getVoyageNo());
            }
            if (vehicleCommand.getLinersName() != null && vehicleCommand.getLinersName() != "") {
                vehicleTO.setLinersName(vehicleCommand.getLinersName());
            }
            if (vehicleCommand.getAgentName() != null && vehicleCommand.getAgentName() != "") {
                vehicleTO.setAgentName(vehicleCommand.getAgentName());
            }
            if (vehicleCommand.getPlannedArrivalDate() != null && vehicleCommand.getPlannedArrivalDate() != "") {
                vehicleTO.setPlannedArrivalDate(vehicleCommand.getPlannedArrivalDate());
            }
            if (vehicleCommand.getPlannedArrivalPort() != null && vehicleCommand.getPlannedArrivalPort() != "") {
                vehicleTO.setPlannedArrivalPort(vehicleCommand.getPlannedArrivalPort());
            }
            if (vehicleCommand.getPlannedDischargeDate() != null && vehicleCommand.getPlannedDischargeDate() != "") {
                vehicleTO.setPlannedDischargeDate(vehicleCommand.getPlannedDischargeDate());
            }
            if (vehicleCommand.getPlannedDischargePort() != null && vehicleCommand.getPlannedDischargePort() != "") {
                vehicleTO.setPlannedDischargePort(vehicleCommand.getPlannedDischargePort());
            }
            if (vehicleCommand.getStatus() != null && vehicleCommand.getStatus() != "") {
                vehicleTO.setStatus(vehicleCommand.getStatus());
            }

            if (vehicleCommand.getDescription() != null && vehicleCommand.getDescription() != "") {
                vehicleTO.setDescription(vehicleCommand.getDescription());
            }

            int updateStatus = 0;
            updateStatus = vehicleBP.insertDischargeVessel(vehicleTO, userId);
            response.setContentType("text/css");
            if (updateStatus > 0) {
                response.getWriter().println(updateStatus);
            } else {
                response.getWriter().println(0);
            }
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
    }

    public ModelAndView handleViewTrailerTypePage(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String menuPath = "Trailer  >>  View Type";

        try {
            if (!loginBP.checkAuthorisation(userFunctions, "VehicleType-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "content/MFR/manageTrailerType.jsp";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

                String pageTitle = "View Vehicle Type";
                request.setAttribute("pageTitle", pageTitle);

                ArrayList TypeList = new ArrayList();

                TypeList = vehicleBP.getTrailerTypeList();

                request.setAttribute("TypeList", TypeList);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Vehicle Type  data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewTrailerTypeAddPage(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        try {
            if (!loginBP.checkAuthorisation(userFunctions, "VehicleType-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "content/MFR/addTrailerType.jsp";
                String pageTitle = "Add Trailer Type";
                request.setAttribute("pageTitle", pageTitle);
                String menuPath = "Trailer  >>  Trailer Type >>  Add";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

                ArrayList TypeList = new ArrayList();
                TypeList = vehicleBP.getTrailerTypeList();
                request.setAttribute("TypeList", TypeList);
                ArrayList AxleList = new ArrayList();
                AxleList = vehicleBP.GetAxleList();
                System.out.print("AxleList-----cgbb" + AxleList.size());
                request.setAttribute("AxleList", AxleList);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Vehicle Type  data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewTrailerTypeAlterPage(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        path = "content/MFR/alterTrailerTypePage.jsp";

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        try {
            if (!loginBP.checkAuthorisation(userFunctions, "VehicleType-Alter")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                vehicleCommand = command;
                String menuPath = "Trailer  >>  Trailer Type >>  Alter";
                ArrayList TypeList = new ArrayList();
                TypeList = vehicleBP.getTrailerTypeList();
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "Alter Trailer type";
                request.setAttribute("TypeList", TypeList);
                request.setAttribute("pageTitle", pageTitle);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleTrailerTypeAdd(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        try {
            String menuPath = "Trailer  >>  Trailer Type >>  View  ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VehicleTO vehicleTO = new VehicleTO();

            if (vehicleCommand.getTypeName() != null && vehicleCommand.getTypeName() != "") {
                vehicleTO.setTypeName(vehicleCommand.getTypeName());

            }
            if (vehicleCommand.getTonnage() != null && vehicleCommand.getTonnage() != "") {
                vehicleTO.setTonnage(vehicleCommand.getTonnage());

            }
            if (vehicleCommand.getCapacity() != null && vehicleCommand.getCapacity() != "") {
                vehicleTO.setCapacity(vehicleCommand.getCapacity());

            }
            if (vehicleCommand.getDescription() != null && vehicleCommand.getDescription() != "") {
                vehicleTO.setDescription(vehicleCommand.getDescription());

            }
            if (vehicleCommand.getAxleTypeId() != null && vehicleCommand.getAxleTypeId() != "") {
                vehicleTO.setAxleTypeId(vehicleCommand.getAxleTypeId());

            }

            String pageTitle = "View Tralier Type";
            request.setAttribute("pageTitle", pageTitle);

            int insertStatus = 0;
            path = "content/MFR/manageTrailerType.jsp";

            insertStatus = vehicleBP.processInsertTrailerTypeDetails(vehicleTO, userId);
            System.out.println("insertStatus types= " + insertStatus);
            request.removeAttribute("TypeList");
            ArrayList TypeList = new ArrayList();
            TypeList = vehicleBP.getTrailerTypeList();
            request.setAttribute("TypeList", TypeList);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Trailer Type Added Successfully");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAlterTrailerType(HttpServletRequest request, HttpServletResponse reponse, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        vehicleCommand = command;
        String path = "";

        try {
            String menuPath = "  Trailer  >> Trailer >>  View Type  ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-Modify")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            int index = 0;
            int modify = 0;
            String[] typeids = vehicleCommand.getTypeIds();
            String[] typenames = vehicleCommand.getTypeNames();
            String[] tonnages = vehicleCommand.getTonnages();
            String[] capacities = vehicleCommand.getCapacities();
            String[] activeStatus = vehicleCommand.getActiveInds();
            String[] selectedIndex = vehicleCommand.getSelectedIndex();
            String[] desciption = vehicleCommand.getDescriptions();
            int UserId = (Integer) session.getAttribute("userId");

            ArrayList List = new ArrayList();
            VehicleTO vehicleTO = null;
            for (int i = 0; i < selectedIndex.length; i++) {
                vehicleTO = new VehicleTO();
                index = Integer.parseInt(selectedIndex[i]);

                vehicleTO.setTypeId(typeids[index]);
                vehicleTO.setTypeName(typenames[index]);
                vehicleTO.setTonnage(tonnages[index]);
                vehicleTO.setCapacity(capacities[index]);
                vehicleTO.setActiveInd(activeStatus[index]);
                vehicleTO.setDescription(desciption[index]);
                List.add(vehicleTO);
            }
            String pageTitle = "View Trailer Type";
            path = "content/MFR/manageTrailerType.jsp";
            request.setAttribute("pageTitle", pageTitle);
            modify = vehicleBP.processModifyTrailerTypeDetails(List, UserId);
            System.out.println("modify the type= " + modify);
            request.removeAttribute("TypeList");
            ArrayList TypeList = new ArrayList();
            TypeList = vehicleBP.getTrailerTypeList();
            request.setAttribute("TypeList", TypeList);
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Trailer Type Details Modified Successfully");
//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve designation --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void saveTrailerDetails(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");

        int madhavaramSp = 1011;
        VehicleTO vehicleTO = new VehicleTO();
        CompanyTO companyTO = new CompanyTO();
//        String vehicleId = request.getParameter("vehicleId");
        try {
            if (vehicleCommand.getUsageId() != null && vehicleCommand.getUsageId() != "") {
                vehicleTO.setUsageId(vehicleCommand.getUsageId());
            }
            if (vehicleCommand.getWar_period() != null && vehicleCommand.getWar_period() != "") {
                vehicleTO.setWar_period(vehicleCommand.getWar_period());
            }
            if (vehicleCommand.getMfrId() != null && vehicleCommand.getMfrId() != "") {
                vehicleTO.setMfrId(vehicleCommand.getMfrId());
            }
            if (vehicleCommand.getClassId() != null && vehicleCommand.getClassId() != "") {
                vehicleTO.setClassId(vehicleCommand.getClassId());
            }
            if (vehicleCommand.getModelId() != null && vehicleCommand.getModelId() != "") {
                vehicleTO.setModelId(vehicleCommand.getModelId());
            }
            if (vehicleCommand.getDateOfSale() != null && vehicleCommand.getDateOfSale() != "") {
                vehicleTO.setDateOfSale(vehicleCommand.getDateOfSale());
            }
            if (vehicleCommand.getRegistrationDate() != null && vehicleCommand.getRegistrationDate() != "") {
                vehicleTO.setRegistrationDate(vehicleCommand.getRegistrationDate());
            }
            if (vehicleCommand.getRegNo() != null && vehicleCommand.getRegNo() != "") {
                vehicleTO.setRegNo(vehicleCommand.getRegNo());
            }
            if (vehicleCommand.getDescription() != null && vehicleCommand.getDescription() != "") {
                vehicleTO.setDescription(vehicleCommand.getDescription());
            }
            if (vehicleCommand.getEngineNo() != null && vehicleCommand.getEngineNo() != "") {
                vehicleTO.setEngineNo(vehicleCommand.getEngineNo());
            }
            if (vehicleCommand.getChassisNo() != null && vehicleCommand.getChassisNo() != "") {
                vehicleTO.setChassisNo(vehicleCommand.getChassisNo());
            }
            if (vehicleCommand.getOpId() != null && vehicleCommand.getOpId() != "") {
                vehicleTO.setOpId(vehicleCommand.getOpId());
            }
//            if (vehicleCommand.getSpId() != null && vehicleCommand.getSpId() != "") {
//                vehicleTO.setSpId(vehicleCommand.getSpId());
//            }
            if (vehicleCommand.getSeatCapacity() != null && vehicleCommand.getSeatCapacity() != "") {
                vehicleTO.setSeatCapacity(vehicleCommand.getSeatCapacity());
            }
//            if (vehicleCommand.getNextFCDate() != null && vehicleCommand.getNextFCDate() != "") {
//                vehicleTO.setNextFCDate(vehicleCommand.getNextFCDate());
//            }
            if (vehicleCommand.getKmReading() != null && vehicleCommand.getKmReading() != "") {
                vehicleTO.setKmReading(vehicleCommand.getKmReading());
            }
//            if (vehicleCommand.getHmReading() != null && vehicleCommand.getHmReading() != "") {
//                vehicleTO.setHmReading(vehicleCommand.getHmReading());
//            }
            if (vehicleCommand.getVehicleColor() != null && vehicleCommand.getVehicleColor() != "") {
                vehicleTO.setVehicleColor(vehicleCommand.getVehicleColor());
            }
            if (vehicleCommand.getVehicleCost() != null && vehicleCommand.getVehicleCost() != "") {
                vehicleTO.setVehicleCost(vehicleCommand.getVehicleCost());
            }
            if (vehicleCommand.getGroupId() != null && vehicleCommand.getGroupId() != "") {
                vehicleTO.setGroupId(vehicleCommand.getGroupId());
            }
            if (vehicleCommand.getGpsSystem() != null && vehicleCommand.getGpsSystem() != "") {
                vehicleTO.setGpsSystem(vehicleCommand.getGpsSystem());
            }
            if (vehicleCommand.getWarrantyDate() != null && vehicleCommand.getWarrantyDate() != "") {
                vehicleTO.setWarrantyDate(vehicleCommand.getWarrantyDate());
            }
            if (vehicleCommand.getAsset() != null && vehicleCommand.getAsset() != "") {
                vehicleTO.setAsset(vehicleCommand.getAsset());
            }
            if (vehicleCommand.getAxles() != null && vehicleCommand.getAxles() != "") {
                vehicleTO.setAxles(vehicleCommand.getAxles());
            }
            if (vehicleCommand.getVehicleDepreciation() != null && vehicleCommand.getVehicleDepreciation() != "") {
                vehicleTO.setVehicleDepreciation(vehicleCommand.getVehicleDepreciation());
            }
            if (vehicleCommand.getDailyKm() != null && vehicleCommand.getDailyKm() != "") {
                vehicleTO.setDailyKm(vehicleCommand.getDailyKm());
            }
            if (vehicleCommand.getDailyHm() != null && vehicleCommand.getDailyHm() != "") {
                vehicleTO.setDailyHm(vehicleCommand.getDailyHm());
            }
            if (vehicleCommand.getGpsSystemId() != null && vehicleCommand.getGpsSystemId() != "") {
                vehicleTO.setGpsSystemId(vehicleCommand.getGpsSystemId());
            }
            if (vehicleCommand.getOwnerShips() != null && vehicleCommand.getOwnerShips() != "") {
                vehicleTO.setOwnerShips(vehicleCommand.getOwnerShips());
            }
            if (vehicleCommand.getTypeId() != null && vehicleCommand.getTypeId() != "") {
                vehicleTO.setTypeId(vehicleCommand.getTypeId());
            }
            if (vehicleCommand.getVendorId() != null && vehicleCommand.getVendorId() != "") {
                vehicleTO.setVendorId(vehicleCommand.getVendorId());
            }
            if (vehicleCommand.getAxleTypeId() != null && vehicleCommand.getAxleTypeId() != "") {
                vehicleTO.setAxleTypeId(vehicleCommand.getAxleTypeId());
            }
            if (vehicleCommand.getVehicleId() != null && vehicleCommand.getVehicleId() != "") {
                System.out.println("vehicleCommand.getVehicleId()" + vehicleCommand.getVehicleId());
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }
            String catId = request.getParameter("catId");
            vehicleTO.setCatId(catId);
            System.out.println("catId 11 = " + catId);
//             if (vehicleCommand.getVehicleId() != null && vehicleCommand.getVehicleId() != "") {
//                 System.out.println("vehicleCommand.getVehicleId()"+vehicleCommand.getVehicleId());
//
//                 request.setAttribute("vehicleId", vehicleCommand.getVehicleId());
//            }else{
//                request.setAttribute("vehicleId", 0);
//            }

            int vehicleId = 0;
            String vehicleDetails = vehicleBP.addTrailerDetails(vehicleTO, userId, madhavaramSp);
            String[] temp = vehicleDetails.split("~");
            vehicleId = Integer.parseInt(temp[0]);
//            vehicleBP.vehicleKmUpdate(vehicleId, vehicleTO.getKmReading(), vehicleTO.getHmReading(), userId);
            if (vehicleId > 0) {
                vehicleBP.trailerTyreDetails(vehicleTO, vehicleId, userId, madhavaramSp);
            }

            response.setContentType("text/css");
            if (vehicleId > 0) {
                response.getWriter().println(vehicleDetails);
            } else {
                response.getWriter().println(0);
            }
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
    }

    public void updateTrailerDetails(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");

        int madhavaramSp = 1011;
        VehicleTO vehicleTO = new VehicleTO();
        CompanyTO companyTO = new CompanyTO();
        try {
            if (vehicleCommand.getUsageId() != null && vehicleCommand.getUsageId() != "") {
                vehicleTO.setUsageId(vehicleCommand.getUsageId());
            }
            if (vehicleCommand.getWar_period() != null && vehicleCommand.getWar_period() != "") {
                vehicleTO.setWar_period(vehicleCommand.getWar_period());
            }
            if (vehicleCommand.getMfrId() != null && vehicleCommand.getMfrId() != "") {
                vehicleTO.setMfrId(vehicleCommand.getMfrId());
            }
            if (vehicleCommand.getClassId() != null && vehicleCommand.getClassId() != "") {
                vehicleTO.setClassId(vehicleCommand.getClassId());
            }
            if (vehicleCommand.getModelId() != null && vehicleCommand.getModelId() != "") {
                vehicleTO.setModelId(vehicleCommand.getModelId());
            }
            if (vehicleCommand.getDateOfSale() != null && vehicleCommand.getDateOfSale() != "") {
                vehicleTO.setDateOfSale(vehicleCommand.getDateOfSale());
            }
            if (vehicleCommand.getRegistrationDate() != null && vehicleCommand.getRegistrationDate() != "") {
                vehicleTO.setRegistrationDate(vehicleCommand.getRegistrationDate());
            }
            if (vehicleCommand.getRegNo() != null && vehicleCommand.getRegNo() != "") {
                vehicleTO.setRegNo(vehicleCommand.getRegNo());
            }
            if (vehicleCommand.getDescription() != null && vehicleCommand.getDescription() != "") {
                vehicleTO.setDescription(vehicleCommand.getDescription());
            }
            if (vehicleCommand.getEngineNo() != null && vehicleCommand.getEngineNo() != "") {
                vehicleTO.setEngineNo(vehicleCommand.getEngineNo());
            }
            if (vehicleCommand.getChassisNo() != null && vehicleCommand.getChassisNo() != "") {
                vehicleTO.setChassisNo(vehicleCommand.getChassisNo());
            }
            if (vehicleCommand.getOpId() != null && vehicleCommand.getOpId() != "") {
                vehicleTO.setOpId(vehicleCommand.getOpId());
            }
//            if (vehicleCommand.getSpId() != null && vehicleCommand.getSpId() != "") {
//                vehicleTO.setSpId(vehicleCommand.getSpId());
//            }
            if (vehicleCommand.getSeatCapacity() != null && vehicleCommand.getSeatCapacity() != "") {
                vehicleTO.setSeatCapacity(vehicleCommand.getSeatCapacity());
            }
//            if (vehicleCommand.getNextFCDate() != null && vehicleCommand.getNextFCDate() != "") {
//                vehicleTO.setNextFCDate(vehicleCommand.getNextFCDate());
//            }
            if (vehicleCommand.getKmReading() != null && vehicleCommand.getKmReading() != "") {
                vehicleTO.setKmReading(vehicleCommand.getKmReading());
            }
//            if (vehicleCommand.getHmReading() != null && vehicleCommand.getHmReading() != "") {
//                vehicleTO.setHmReading(vehicleCommand.getHmReading());
//            }
            if (vehicleCommand.getVehicleColor() != null && vehicleCommand.getVehicleColor() != "") {
                vehicleTO.setVehicleColor(vehicleCommand.getVehicleColor());
            }
            if (vehicleCommand.getVehicleCost() != null && vehicleCommand.getVehicleCost() != "") {
                vehicleTO.setVehicleCost(vehicleCommand.getVehicleCost());
            }
            if (vehicleCommand.getGroupId() != null && vehicleCommand.getGroupId() != "") {
                vehicleTO.setGroupId(vehicleCommand.getGroupId());
            }
            if (vehicleCommand.getGpsSystem() != null && vehicleCommand.getGpsSystem() != "") {
                vehicleTO.setGpsSystem(vehicleCommand.getGpsSystem());
            }
            if (vehicleCommand.getWarrantyDate() != null && vehicleCommand.getWarrantyDate() != "") {
                vehicleTO.setWarrantyDate(vehicleCommand.getWarrantyDate());
            }
            if (vehicleCommand.getAsset() != null && vehicleCommand.getAsset() != "") {
                vehicleTO.setAsset(vehicleCommand.getAsset());
            }
            if (vehicleCommand.getAxles() != null && vehicleCommand.getAxles() != "") {
                vehicleTO.setAxles(vehicleCommand.getAxles());
            }
            if (vehicleCommand.getVehicleDepreciation() != null && vehicleCommand.getVehicleDepreciation() != "") {
                vehicleTO.setVehicleDepreciation(vehicleCommand.getVehicleDepreciation());
            }
            if (vehicleCommand.getDailyKm() != null && vehicleCommand.getDailyKm() != "") {
                vehicleTO.setDailyKm(vehicleCommand.getDailyKm());
            }
            if (vehicleCommand.getDailyHm() != null && vehicleCommand.getDailyHm() != "") {
                vehicleTO.setDailyHm(vehicleCommand.getDailyHm());
            }
            if (vehicleCommand.getGpsSystemId() != null && vehicleCommand.getGpsSystemId() != "") {
                vehicleTO.setGpsSystemId(vehicleCommand.getGpsSystemId());
            }
            if (vehicleCommand.getOwnerShips() != null && vehicleCommand.getOwnerShips() != "") {
                vehicleTO.setOwnerShips(vehicleCommand.getOwnerShips());
            }
            if (vehicleCommand.getTypeId() != null && vehicleCommand.getTypeId() != "") {
                vehicleTO.setTypeId(vehicleCommand.getTypeId());
            }
            if (vehicleCommand.getVendorId() != null && vehicleCommand.getVendorId() != "") {
                vehicleTO.setVendorId(vehicleCommand.getVendorId());
            }
            if (vehicleCommand.getAxleTypeId() != null && vehicleCommand.getAxleTypeId() != "") {
                vehicleTO.setAxleTypeId(vehicleCommand.getAxleTypeId());
            }
            if (vehicleCommand.getActiveInd() != null && vehicleCommand.getActiveInd() != "") {
                vehicleTO.setActiveInd(vehicleCommand.getActiveInd());
            }
            if (vehicleCommand.getVehicleId() != null && vehicleCommand.getVehicleId() != "") {
                System.out.println("vehicleCommand.getVehicleId()" + vehicleCommand.getVehicleId());
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }
//             if (vehicleCommand.getVehicleId() != null && vehicleCommand.getVehicleId() != "") {
//                 System.out.println("vehicleCommand.getVehicleId()"+vehicleCommand.getVehicleId());
//
//                 request.setAttribute("vehicleId", vehicleCommand.getVehicleId());
//            }else{
//                request.setAttribute("vehicleId", 0);
//            }
            int vehicleId = 0;
            vehicleId = vehicleBP.updateTrailerDetails(vehicleTO, userId, madhavaramSp);
            response.setContentType("text/css");
            if (vehicleId > 0) {
                response.getWriter().println(vehicleCommand.getVehicleId());
            } else {
                response.getWriter().println(0);
            }
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
    }

    public void getTrailerAxleTyreNo(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws IOException {
        System.out.println("entered in  ajaxmodel controller");
        HttpSession session = request.getSession();

        vehicleCommand = command;
        VehicleTO vehicleTO = new VehicleTO();
        VehicleTO vehTO = new VehicleTO();
        ArrayList vehicleAxleDetail = new ArrayList();
        PrintWriter pw = response.getWriter();
        try {
            response.setContentType("text/html");
            String axleDetailId = request.getParameter("axleDetailId");
            String positionName = request.getParameter("positionName");
            String positionNo = request.getParameter("positionNo");
            String vehicleId = request.getParameter("vehicleId");
            String axleTypeId = request.getParameter("axleTypeId");
            vehTO.setAxleDetailId(axleDetailId);
            vehTO.setPositionName(positionName);
            vehTO.setPositionNo(positionNo);
            vehTO.setVehicleId(vehicleId);
            vehTO.setAxleTypeId(axleTypeId);
            vehicleAxleDetail = vehicleBP.getTrailerAxleTyreNo(vehTO);
            System.out.println("vehicleAxleDetail.size() = " + vehicleAxleDetail.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = vehicleAxleDetail.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                vehicleTO = (VehicleTO) itr.next();
                jsonObject.put("AxleTyreId", vehicleTO.getAxleTyreId());
                jsonObject.put("TyreNo", vehicleTO.getTyreNo());
                jsonObject.put("TyreDepth", vehicleTO.getTyreDepth());
                jsonObject.put("TyreMfr", vehicleTO.getTyreMfr());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);
            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }

    }

    public void updateTrailerTyreNo(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");

        int madhavaramSp = 1011;
        VehicleTO vehicleTO = new VehicleTO();
        try {
            if (vehicleCommand.getAxleDetailId() != null && vehicleCommand.getAxleDetailId() != "") {
                vehicleTO.setAxleDetailId(vehicleCommand.getAxleDetailId());
            }
            if (vehicleCommand.getPositionName() != null && vehicleCommand.getPositionName() != "") {
                vehicleTO.setPositionName(vehicleCommand.getPositionName());
            }
            if (vehicleCommand.getPositionNo() != null && vehicleCommand.getPositionNo() != "") {
                vehicleTO.setPositionNo(vehicleCommand.getPositionNo());
            }
            if (vehicleCommand.getUpdateValue() != null && vehicleCommand.getUpdateValue() != "") {
                vehicleTO.setUpdateValue(vehicleCommand.getUpdateValue());
            }
            if (vehicleCommand.getUpdateType() != null && vehicleCommand.getUpdateType() != "") {
                vehicleTO.setUpdateType(vehicleCommand.getUpdateType());
            }
            if (vehicleCommand.getDepthVal() != null && vehicleCommand.getDepthVal() != "") {
                vehicleTO.setDepthVal(vehicleCommand.getDepthVal());
            }
            if (vehicleCommand.getTyreNo() != null && vehicleCommand.getTyreNo() != "") {
                vehicleTO.setTyreNo(vehicleCommand.getTyreNo());
            }
            if (vehicleCommand.getVehicleId() != null && vehicleCommand.getVehicleId() != "") {
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }
            int updateStatus = 0;
            updateStatus = vehicleBP.updateTrailerTyreDetails(vehicleTO, userId, madhavaramSp);
            response.setContentType("text/css");
            if (updateStatus > 0) {
                response.getWriter().println(updateStatus);
            } else {
                response.getWriter().println(0);
            }
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
    }

    public void insertTrailerOemDetails(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        System.out.println("hi i m OEMcontroller");
        vehicleCommand = command;
        VehicleTO vehicleTO = new VehicleTO();
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        System.out.println("in controller");
        int madhavaramSp = 1011;
        PrintWriter pw = response.getWriter();
        response.setContentType("text/html");
        if (vehicleCommand.getVehicleId() != null && vehicleCommand.getVehicleId() != "") {
            vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
        }
        String[] oemDetailsId = request.getParameterValues("oemDetailsIdVal[]");
        String[] oemMfr = request.getParameterValues("oemMfrVal[]");
        String[] oemTyreNo = request.getParameterValues("oemTyreNoVal[]");
        String[] oemDepth = request.getParameterValues("oemDepthVal[]");
        String[] oemBattery = request.getParameterValues("oemBatteryVal[]");
        String[] oemId = request.getParameterValues("oemIdVal[]");
        String oemIds = null;
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < oemDetailsId.length; i++) {
            System.out.println("oemDetailsIdVal" + oemDetailsId[i]);
            System.out.println("oemMfrVal" + oemMfr[i]);
            System.out.println("oemTyreNoVal" + oemTyreNo[i]);
            System.out.println("oemBatteryVal" + oemBattery[i]);
            int oemId1 = 0;
//                System.out.println("oemId Throttle Here = " + oemId);
            if (oemId[i].equals("0")) {
                oemId1 = vehicleBP.insertTrailerOemDetails(vehicleTO, oemDetailsId[i], oemMfr[i], oemTyreNo[i], oemDepth[i], oemBattery[i], oemId[i], userId);
            } else {
                oemId1 = Integer.parseInt(oemId[i]);
            }
            JSONObject jsonObject = new JSONObject();
          ///  jsonObject.put("OemId", oemId1);
            jsonArray.put(jsonObject);
            System.out.println("oemIds" + oemIds);
        }
        System.out.println("jsonArray = " + jsonArray);
        pw.print(jsonArray);
    }

    public ModelAndView handleTrailerDetail(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String pageTitle = "";
        String menuPath = "Vehicle  >>  Search Vehicles";
        vehicleCommand = command;
        ArrayList vehicleDetail = new ArrayList();
        VehicleTO vehicleTO = null;
        FinanceTO financeTO = null;
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Vehicle-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                pageTitle = "Alter Vehicle";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                request.setAttribute("pageTitle", pageTitle);
                path = "content/Vehicle/addTrailerDetails.jsp";
                String trailerId = request.getParameter("vehicleId");
                String fleetTypeId = request.getParameter("fleetTypeId");
                String listId = request.getParameter("listId");
                request.setAttribute("listId", listId);

                vehicleTO = new VehicleTO();
                if (trailerId != null && trailerId != "") {
                    vehicleTO.setVehicleId(trailerId);
                }



                ArrayList oemList = new ArrayList();
                oemList = vehicleBP.getTrailerOemList(vehicleTO);
                System.out.println("oemListcontrol" + oemList);
                request.setAttribute("oemListSize", oemList.size());
                request.setAttribute("oemList", oemList);
                request.setAttribute("fleetTypeId", fleetTypeId);
                VehicleTO vTO = new VehicleTO();

                Iterator itr1 = oemList.iterator();

                while (itr1.hasNext()) {
                    vTO = (VehicleTO) itr1.next();

                    request.setAttribute("oemId", vTO.getOemId());
                    request.setAttribute("oemMfr", vTO.getOemMfr());
                    request.setAttribute("oemDepth", vTO.getOemDepth());
                    request.setAttribute("oemTyreNo", vTO.getOemTyreNo());
                    request.setAttribute("oemBattery", vTO.getOemBattery());
                    request.setAttribute("oemDetailsId", vTO.getOemDetailsId());
                    System.out.println("getOemDetailsId" + vTO.getOemDetailsId());
                }
                ArrayList vehiclesDetails = new ArrayList();
                vehiclesDetails = vehicleBP.getTrailerDetails(trailerId);
                System.out.println("vehiclesDetails" + vehiclesDetails);
                request.setAttribute("vehiclesDetails", vehiclesDetails.size());
                if (vehiclesDetails.size() == 0) {
                    request.setAttribute("vehicleId", 0);
                    System.out.println("request.setAttribute(vehicleId)" + request.getAttribute("vehicleId"));
                }
                Iterator itr = vehiclesDetails.iterator();
                String mfrId = "";
                while (itr.hasNext()) {
                    vTO = (VehicleTO) itr.next();
//                request.setAttribute("vehicleId",vTO.getVehicleId());
                    if (vTO.getVehicleId() != null && vTO.getVehicleId() != "") {
                        System.out.println("vehicleCommand.getVehicleId()" + vTO.getVehicleId());
                        request.setAttribute("vehicleId", vehicleCommand.getVehicleId());
                    }
                    request.setAttribute("regNo", vTO.getTrailerNo());
                    request.setAttribute("mfrId", vTO.getMfrId());
                    mfrId = vTO.getMfrId();
                    request.setAttribute("axleTypeId", vTO.getAxleTypeId());
                    request.setAttribute("modelId", vTO.getModelId());
                    request.setAttribute("typeId", vTO.getTypeId());
                    request.setAttribute("war_period", vTO.getWar_period());
                    request.setAttribute("dateOfSale", vTO.getDateOfSale());
                    request.setAttribute("chassisNo", vTO.getChassisNo());
                    request.setAttribute("seatCapacity", vTO.getSeatCapacity());
                    request.setAttribute("engineNo", vTO.getEngineNo());
                    request.setAttribute("vehicleDepreciation", vTO.getVehicleDepreciation());
                    request.setAttribute("classId", vTO.getClassId());
                    request.setAttribute("kmReading", vTO.getKmReading());
                    request.setAttribute("dailyKm", vTO.getDailyKm());
                    request.setAttribute("vehicleCost", vTO.getVehicleCost());
                    request.setAttribute("vehicleColor", vTO.getVehicleColor());
                    request.setAttribute("usageId", vTO.getUsageId());
                    request.setAttribute("gpsSystemId", vTO.getGpsSystemId());
                    request.setAttribute("warrantyDate", vTO.getWarrantyDate());
                    request.setAttribute("description", vTO.getDescription());
                    request.setAttribute("ownership", vTO.getOwnership());
                    request.setAttribute("warPeriod", vTO.getWar_period());
                    request.setAttribute("insuranceid", vTO.getInsuranceid());
                    request.setAttribute("insPolicy", vTO.getInsPolicy());
                    request.setAttribute("prevPolicy", vTO.getPrevPolicy());
                    request.setAttribute("packageType", vTO.getPackageType());
                    request.setAttribute("insName", vTO.getInsName());
                    System.out.println("insNameinsNameinsName" + vTO.getInsName());
                    request.setAttribute("insAddress", vTO.getInsAddress());
                    request.setAttribute("insMobileNo", vTO.getInsMobileNo());
                    request.setAttribute("insPolicyNo", vTO.getInsPolicyNo());
                    request.setAttribute("insIdvValue", vTO.getInsIdvValue());
                    request.setAttribute("fromDate1", vTO.getFromDate1());
                    request.setAttribute("toDate1", vTO.getToDate1());
                    request.setAttribute("premiunAmount", vTO.getPremiunAmount());
                    request.setAttribute("paymentType", vTO.getPaymentType());
                    request.setAttribute("bankId", vTO.getBankId());
                    request.setAttribute("bankBranchId", vTO.getBankBranchId());
                    request.setAttribute("chequeNo", vTO.getChequeNo());
                    request.setAttribute("chequeDate", vTO.getChequeDate());
                    request.setAttribute("chequeAmount", vTO.getChequeAmount());
                    request.setAttribute("clearanceDate", vTO.getClearanceDate());
                    request.setAttribute("clearanceId", vTO.getClearanceId());

                    request.setAttribute("roadTaxId", vTO.getRoadTaxId());
                    request.setAttribute("roadTaxReceiptNo", vTO.getRoadTaxReceiptNo());
                    request.setAttribute("roadTaxReceiptDate", vTO.getRoadTaxReceiptDate());
                    request.setAttribute("roadTaxPaidLocation", vTO.getRoadTaxPaidLocation());
                    request.setAttribute("roadTaxPeriod", vTO.getRoadTaxPeriod());
                    request.setAttribute("roadTaxPaidAmount", vTO.getRoadTaxPaidAmount());
                    request.setAttribute("roadTaxFromDate", vTO.getRoadTaxFromDate());
                    request.setAttribute("nextRoadTaxDate", vTO.getNextRoadTaxDate());
                    request.setAttribute("roadTaxRemarks", vTO.getRoadTaxRemarks());

                    request.setAttribute("fcid", vTO.getFcid());
                    request.setAttribute("rtoDetail", vTO.getRtoDetail());
                    request.setAttribute("fcDate", vTO.getFcDate());
                    request.setAttribute("fcReceiptNo", vTO.getFcReceiptNo());
                    request.setAttribute("fcAmount", vTO.getFcAmount());
                    request.setAttribute("fcExpiryDate", vTO.getFcExpiryDate());
                    request.setAttribute("fcRemarks", vTO.getFcRemarks());
                    request.setAttribute("permitid", vTO.getPermitid());
                    request.setAttribute("permitType", vTO.getPermitType());
                    request.setAttribute("permitNo", vTO.getPermitNo());
                    request.setAttribute("permitAmount", vTO.getPermitAmount());
                    request.setAttribute("permitPaidDate", vTO.getPermitPaidDate());
                    request.setAttribute("permitExpiryDate", vTO.getPermitExpiryDate());
                    request.setAttribute("remarks", vTO.getRemarks());
                    request.setAttribute("opId", vTO.getOpId());
                }
//                request.setAttribute("vehiclesDetails", vehiclesDetails);

                ArrayList MfrList = new ArrayList();
                MfrList = vehicleBP.processGetMfrList();
                request.setAttribute("MfrList", MfrList);

                ArrayList bankBranchList = new ArrayList();
//                bankBranchList = financeBP.getBankBranchLists();
                request.setAttribute("bankBranchList", bankBranchList);

                ArrayList usageList = new ArrayList();
                usageList = vehicleBP.processGetUsageList();
                request.setAttribute("UsageList", usageList);

                ArrayList classList = new ArrayList();
                classList = vehicleBP.processGetClassList();
                request.setAttribute("ClassList", classList);

                ArrayList modelList = new ArrayList();
                modelList = vehicleBP.processActiveModelList();
                request.setAttribute("modelList", modelList);

                ArrayList customerList = new ArrayList();
                customerList = customerBP.processActiveCustomerList();
                request.setAttribute("customerList", customerList);

                ArrayList groupList = new ArrayList();
                groupList = vehicleBP.getGroupList();
                request.setAttribute("groupList", groupList);

                //OperationPointList
                ArrayList operationPointList = new ArrayList();
                operationPointList = vehicleBP.processGetopList();
                request.setAttribute("OperationPointList", operationPointList);

                // get Vehicle tyre items list
                ArrayList tyreItemList = new ArrayList();
                tyreItemList = vehicleBP.processTyreItems();
                request.setAttribute("tyreItemList", tyreItemList);
                System.out.println("size of tyrelist===" + tyreItemList.size());

                VehicleTO vehicleTo = new VehicleTO();
                ArrayList TypeList = new ArrayList();
                vehicleTO.setFleetTypeId("2");
                vehicleTo.setMfrId(mfrId);
                TypeList = vehicleBP.processActiveTypeList(vehicleTO);
                request.setAttribute("TypeList", TypeList);

                //            ArrayList positionList = new ArrayList();
                //            positionList = mrsBP.getTyrePostions();
                //            request.setAttribute("positionList", positionList);
                //get positions list
                ArrayList positionList = new ArrayList();
                positionList = vehicleBP.getTyrePostions();
                request.setAttribute("positionList", positionList);
                System.out.println("positionList size=" + positionList.size());

                // get Vehicle TYre positions
                ArrayList vehTyreList = new ArrayList();
                vehTyreList = vehicleBP.processTyrePosition(Integer.parseInt(trailerId));
                request.setAttribute("vehTyreList", vehTyreList);

                ArrayList leasingCustList = new ArrayList();
                leasingCustList = vehicleBP.getLeasingCustomerList();
                request.setAttribute("leasingCustList", leasingCustList);

                ArrayList primarybankList = new ArrayList();
                primarybankList = vehicleBP.getPrimarybankList();
                request.setAttribute("primarybankList", primarybankList);
                System.out.println("primarybankList.size()=" + primarybankList.size());

                String vendorTypeId = ThrottleConstants.vehicleInsuranceVendorTypeId;
                ArrayList vendorList = new ArrayList();
                vendorList = vehicleBP.vendorList(vendorTypeId);
                request.setAttribute("vendorList", vendorList);

                String vendorTypeIdCompliance = ThrottleConstants.vehicleComplianceVendorTypeId;
                ArrayList vendorListCompliance = new ArrayList();
                vendorListCompliance = vehicleBP.vendorListCompliance(vendorTypeIdCompliance);
                request.setAttribute("vendorListCompliance", vendorListCompliance);

                ArrayList depreciationList = new ArrayList();
                depreciationList = vehicleBP.getVehicleDepreciationList(trailerId);
                if (depreciationList.size() > 0) {
                    request.setAttribute("depreciationList", depreciationList);
                }
                request.setAttribute("depreciationSize", depreciationList.size());

                vehicleDetail = vehicleBP.processVehicleDetail(vehicleTO);
                request.setAttribute("vehicleDetail", vehicleDetail);
            }

            request.setAttribute("editStatus", request.getParameter("editStatus"));
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleAddVehiclePage data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewAxle(HttpServletRequest request, HttpServletResponse reponse, VehicleCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        vehicleCommand = command;
        String path = "";
        //        int userId = 0;
        //                (Integer) session.getAttribute("userId");
        //        int cityMasterList = 0;
        VehicleTO vehicleTO = new VehicleTO();
        String menuPath = "";
        menuPath = "Axle >> View Axle Details";
        String pageTitle = "AxleDetails ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            ArrayList axleDetails = new ArrayList();
            axleDetails = vehicleBP.GetAxleList();
            if (axleDetails.size() > 0) {
                request.setAttribute("axleDetails", axleDetails);
            }
            path = "content/Vehicle/manageAxle.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView handleViewAxleDetails(HttpServletRequest request, HttpServletResponse reponse, VehicleCommand command) throws IOException {
        System.out.println("ServiceTypeMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        vehicleCommand = command;
        String path = "";
        //        int userId = 0;
        //                (Integer) session.getAttribute("userId");
        //        int cityMasterList = 0;
        VehicleTO vehicleTO = new VehicleTO();
        String menuPath = "";
        menuPath = "Axle  >> Add New Axle ";
        String pageTitle = "New Axle ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            int axleId = 0;
            axleId = Integer.parseInt(request.getParameter("axleId"));
            request.setAttribute("axleCount", request.getParameter("axleCount"));
            request.setAttribute("axleTypeName", request.getParameter("axleTypeName"));
            request.setAttribute("typeFor", request.getParameter("typeFor"));
            ArrayList axleDetails = new ArrayList();
            axleDetails = vehicleBP.getVehAxleName(axleId);
            if (axleDetails.size() > 0) {
                request.setAttribute("axleDetails", axleDetails);
                request.setAttribute("saveStatus", 1);
            }
            path = "content/Vehicle/vehicleAxleTyreDetails.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView fleetPurchasePage(HttpServletRequest request, HttpServletResponse reponse, VehicleCommand command) throws IOException {
        System.out.println("ServiceTypeMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        vehicleCommand = command;
        String path = "";
        //        int userId = 0;
        //                (Integer) session.getAttribute("userId");
        //        int cityMasterList = 0;
        VehicleTO vehicleTO = new VehicleTO();
        String menuPath = "";
        menuPath = "Axle  >> Add New Axle ";
        String pageTitle = "New Axle ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            ArrayList primarybankList = new ArrayList();
            primarybankList = vehicleBP.getPrimarybankList();
            request.setAttribute("primarybankList", primarybankList);
            System.out.println("primarybankList.size()=" + primarybankList.size());

            String vendorTypeId = ThrottleConstants.fleetPurchaseVendorTypeId;
            ArrayList vendorList = new ArrayList();
            vendorList = vehicleBP.vendorList(vendorTypeId);
            request.setAttribute("vendorList", vendorList);
            path = "content/Vehicle/fleetPurchase.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public void displayVehicleLogoBlobData(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) {
        String uploadId = "";
        try {
            uploadId = request.getParameter("uploadId");
            System.out.println("uploadId" + uploadId);
            ArrayList profileList = new ArrayList();
            profileList = vehicleBP.getDisplayLogoBlobData(uploadId);
            if (profileList.size() > 0) {
//        VehicleTO vehicleTO = new VehicleTO();
                VehicleTO vehicleTO = (VehicleTO) profileList.get(0);
                byte[] profileImg = vehicleTO.getProfileImg();
                response.setContentType("image/jpeg");
                OutputStream out = response.getOutputStream();
                out.write(profileImg);
                out.close();
            }
        } catch (FPRuntimeException exception) {
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    public void saveRegistrationDetails(HttpServletRequest request, HttpServletResponse response, VehicleCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        System.out.println("entered updateInsuranceDetails");
        vehicleCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");
//        int companyType = (Integer) session.getAttribute("companyTypeId");
//        String companyId = (String) session.getAttribute("companyId");
        int madhavaramSp = 1011;
        VehicleTO vehicleTO = new VehicleTO();
        System.out.println("bsdh");
        try {
            if (vehicleCommand.getTruckNo() != null && vehicleCommand.getTruckNo() != "") {
                vehicleTO.setTruckNo(vehicleCommand.getTruckNo());
            }
            if (vehicleCommand.getTransportionType() != null && vehicleCommand.getTransportionType() != "") {
                vehicleTO.setTransportionType(vehicleCommand.getTransportionType());
            }
            if (vehicleCommand.getOwnerName() != null && vehicleCommand.getOwnerName() != "") {
                vehicleTO.setOwnerName(vehicleCommand.getOwnerName());
            }
            if (vehicleCommand.getOwnerId() != null && vehicleCommand.getOwnerId() != "") {
                vehicleTO.setOwnerId(vehicleCommand.getOwnerId());
            }
            if (vehicleCommand.getDateOfReg() != null && vehicleCommand.getDateOfReg() != "") {
                vehicleTO.setDateOfReg(vehicleCommand.getDateOfReg());
            }
            if (vehicleCommand.getNoOfPersons() != null && vehicleCommand.getNoOfPersons() != "") {
                vehicleTO.setNoOfPersons(vehicleCommand.getNoOfPersons());
            }
            if (vehicleCommand.getColor() != null && vehicleCommand.getColor() != "") {
                vehicleTO.setColor(vehicleCommand.getColor());
            }
            if (vehicleCommand.getVehicleId() != null && vehicleCommand.getVehicleId() != "") {
                vehicleTO.setVehicleId(vehicleCommand.getVehicleId());
            }



            int updateStatus = 0;
            updateStatus = vehicleBP.saveRegistrationDetails(vehicleTO, userId, madhavaramSp);
            System.out.println("updateStatus controller" + updateStatus);
            response.setContentType("text/css");
            if (updateStatus > 0) {
                response.getWriter().println(updateStatus);
            } else {
                response.getWriter().println(0);
            }
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
    }
}
