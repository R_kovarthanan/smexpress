/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ets.domain.mis.business;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.mis.data.MisDAO;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author admin
 */
public class MisBP {

public MisBP() {
    }
    MisDAO misDAO;

    public MisDAO getMisDAO() {
        return misDAO;
    }

    public void setMisDAO(MisDAO misDAO) {
        this.misDAO = misDAO;
    }


    public ArrayList getcustomerList() throws FPRuntimeException, FPBusinessException {
        ArrayList getcustomerList = new ArrayList();        
        getcustomerList = misDAO.getcustomerList();
        return  getcustomerList;
    }

    public ArrayList getFCList() throws FPRuntimeException, FPBusinessException {
        ArrayList getFCList = new ArrayList();
        getFCList = misDAO.getFCList();
        return  getFCList;
    }
    public ArrayList getVehicleList() throws FPRuntimeException, FPBusinessException {
        ArrayList VehicleList = new ArrayList();
        VehicleList = misDAO.getVehicleList();
        return  VehicleList;
    }

    public ArrayList getVehicleTypeList() throws FPRuntimeException, FPBusinessException {
        ArrayList VehicleTypeList = new ArrayList();
        VehicleTypeList = misDAO.getVehicleTypeList();
        return  VehicleTypeList;
    }
    
}
