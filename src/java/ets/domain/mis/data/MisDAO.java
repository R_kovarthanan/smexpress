package ets.domain.mis.data;

import ets.arch.exception.FPRuntimeException;
import ets.domain.mrs.business.MrsTO;
import ets.domain.util.FPLogUtils;
import java.io.PrintStream;
import java.util.*;
import org.springframework.orm.ibatis.SqlMapClientTemplate;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class MisDAO extends SqlMapClientDaoSupport {

    public MisDAO() {
    }
    
    private final static String CLASS = "MisDAO";

    public ArrayList getcustomerList() {
        Map map = new HashMap();
        ArrayList getcustomerList = new ArrayList();

        try {           
            getcustomerList = (ArrayList) getSqlMapClientTemplate().queryForList("mis.getCustomerList", map);
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MIS-01", CLASS, "Customer List", sqlException);
        }
        return getcustomerList;
    }

    public ArrayList getFCList() {
        Map map = new HashMap();
        ArrayList FCList = new ArrayList();

        try {
            FCList = (ArrayList) getSqlMapClientTemplate().queryForList("mis.getFCList", map);
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MIS-01", CLASS, "FC List", sqlException);
        }
        return FCList;
    }

    public ArrayList getVehicleList() {
        Map map = new HashMap();
        ArrayList VehicleList = new ArrayList();

        try {
            VehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("mis.VehicleList", map);
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MIS-01", CLASS, "Vehicle List", sqlException);
        }
        return VehicleList;
    }
    
    public ArrayList getVehicleTypeList() {
        Map map = new HashMap();
        ArrayList VehicleTypeList = new ArrayList();

        try {
            VehicleTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("mis.VehicleTypeList", map);
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MIS-01", CLASS, "Vehicle  Type List", sqlException);
        }
        return VehicleTypeList;
    }


    
}
