// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   PurchaseController.java

package ets.domain.purchase.web;

import ets.arch.business.PaginationHelper;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.web.BaseController;
import ets.domain.mrs.business.MrsBP;
import ets.domain.purchase.business.PurchaseBP;
import ets.domain.purchase.business.PurchaseTO;
import ets.domain.section.business.SectionBP;
import ets.domain.users.business.LoginBP;
import ets.domain.util.FPLogUtils;
import ets.domain.vehicle.business.VehicleBP;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import javax.servlet.http.*;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;

// Referenced classes of package ets.domain.purchase.web:
//            PurchaseCommand

public class PurchaseController extends BaseController
{

    public PurchaseController()
    {
    }

    public SectionBP getSectionBP()
    {
        return sectionBP;
    }

    public void setSectionBP(SectionBP sectionBP)
    {
        this.sectionBP = sectionBP;
    }

    public LoginBP getLoginBP()
    {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP)
    {
        this.loginBP = loginBP;
    }

    public PurchaseBP getPurchaseBP()
    {
        return purchaseBP;
    }

    public void setPurchaseBP(PurchaseBP purchaseBP)
    {
        this.purchaseBP = purchaseBP;
    }

    public MrsBP getMrsBP()
    {
        return mrsBP;
    }

    public void setMrsBP(MrsBP mrsBP)
    {
        this.mrsBP = mrsBP;
    }

    public VehicleBP getVehicleBP()
    {
        return vehicleBP;
    }

    public void setVehicleBP(VehicleBP vehicleBP)
    {
        this.vehicleBP = vehicleBP;
    }

    public PurchaseCommand getPurchaseCommand()
    {
        return purchaseCommand;
    }

    public void setPurchaseCommand(PurchaseCommand purchaseCommand)
    {
        this.purchaseCommand = purchaseCommand;
    }

    protected void bind(HttpServletRequest request, Object command)
        throws Exception
    {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog((new StringBuilder()).append("command -->").append(command).toString());
        binder.closeNoCatch();
                   initialize(request);
    }

    public ModelAndView handleGenerateMpr(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        purchaseCommand = command;
        HttpSession session = request.getSession();
        PurchaseTO purchaseTO = new PurchaseTO();
        String menuPath = "Stores >> MPR List ";
        String pageTitle = "MPR List";
        request.setAttribute("pageTitle", pageTitle);
        int selectedInd = 0;
        String fromDate = request.getParameter("fromDate");
        String toDate = request.getParameter("toDate");
        String companyId = (String)session.getAttribute("companyId");
        String path = "/content/stores/mprRaisedList.jsp";
        ArrayList generateMpr = new ArrayList();
        ArrayList mprList = new ArrayList();
        ArrayList mrsSummary = new ArrayList();
        ArrayList itemVedors = new ArrayList();
        ArrayList vendorList = new ArrayList();
        request.setAttribute("menuPath", menuPath);
        int userId = ((Integer)session.getAttribute("userId")).intValue();
        System.out.println((new StringBuilder()).append("user id is=").append(session.getAttribute("userId")).append("-").toString());
        String selectedIndex[] = request.getParameterValues("selectedIndex1");
        String itemIds[] = request.getParameterValues("itemIds");
        String vendorIds[] = request.getParameterValues("vendorIds");
        String reqQty[] = request.getParameterValues("reqQtys");
        String purchaseTypeId = request.getParameter("purchaseType");
        try
        {
            ArrayList userFunctions = new ArrayList();
            userFunctions = (ArrayList)session.getAttribute("userFunction");
            if(!loginBP.checkAuthorisation(userFunctions, "Mpr-GenerateMPR"))
            {
                path = "content/common/NotAuthorized.jsp";
            } else
            {
                if(fromDate == null)
                    fromDate = (String)session.getAttribute("currentDate");
                if(toDate == null)
                    toDate = (String)session.getAttribute("currentDate");
                request.setAttribute("fromDate", fromDate);
                request.setAttribute("toDate", toDate);
                for(int i = 0; i < selectedIndex.length; i++)
                {
                    purchaseTO = new PurchaseTO();
                    selectedInd = Integer.parseInt(selectedIndex[i]);
                    purchaseTO.setItemId(Integer.parseInt(itemIds[selectedInd]));
                    purchaseTO.setVendorId(Integer.parseInt(vendorIds[selectedInd]));
                    purchaseTO.setReqQty(reqQty[selectedInd]);
                    purchaseTO.setUserId(userId);
                    purchaseTO.setCompanyId(companyId);
                    purchaseTO.setJobCardId(request.getParameter("mrsJobCardNumber"));
                    generateMpr.add(purchaseTO);
                }

                purchaseBP.processGenerateMpr(generateMpr, Integer.parseInt(purchaseTypeId));
                vendorList = purchaseBP.processVendorList(1011);
                System.out.println("Updated list");
                purchaseTO = new PurchaseTO();
                mprList = purchaseBP.processMprStatusList(purchaseTO, fromDate, toDate);
                if(mprList.size() != 0)
                    request.setAttribute("mprList", mprList);
                request.setAttribute("vendorList", vendorList);
            }
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleGenerateMpr --> ").append(exception).toString());
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleGenerateMprForReqItems(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        System.out.println((new StringBuilder()).append("request=").append(request).toString());
        purchaseCommand = command;
        HttpSession session = request.getSession();
        PurchaseTO purchaseTO = new PurchaseTO();
        String menuPath = "Stores >> Required Items ";
        String pageTitle = "Required Items List";
        request.setAttribute("pageTitle", pageTitle);
        int selectedInd = 0;
        String companyId = (String)session.getAttribute("companyId");
        String path = "/content/stores/mprRaisedList.jsp";
        ArrayList generateMpr = new ArrayList();
        ArrayList mprList = new ArrayList();
        ArrayList mrsSummary = new ArrayList();
        ArrayList itemVedors = new ArrayList();
        ArrayList vendorList = new ArrayList();
        String fromDate = request.getParameter("fromDate");
        String toDate = request.getParameter("toDate");
        request.setAttribute("menuPath", menuPath);
        int userId = ((Integer)session.getAttribute("userId")).intValue();
        String selectedIndex[] = request.getParameterValues("selectedIndex");
        String itemIds[] = request.getParameterValues("itemIds");
        String vendorIds[] = request.getParameterValues("vendorIds");
        String reqQty[] = request.getParameterValues("reqQtys");
        String jobCardId = request.getParameter("mrsJobCardNumber");
        String purchaseTypeId = request.getParameter("purchaseType");
        System.out.println((new StringBuilder()).append("vendorIds=").append(vendorIds).toString());
        System.out.println((new StringBuilder()).append("vendorIds=").append(vendorIds[0]).toString());
        try
        {
            ArrayList userFunctions = new ArrayList();
            userFunctions = (ArrayList)session.getAttribute("userFunction");
            if(!loginBP.checkAuthorisation(userFunctions, "Mpr-GenerateMPR"))
            {
                path = "content/common/NotAuthorized.jsp";
            } else
            {
                if(fromDate == null)
                    fromDate = (String)session.getAttribute("currentDate");
                if(toDate == null)
                    toDate = (String)session.getAttribute("currentDate");
                request.setAttribute("fromDate", fromDate);
                request.setAttribute("toDate", toDate);
                for(int i = 0; i < selectedIndex.length; i++)
                {
                    purchaseTO = new PurchaseTO();
                    selectedInd = Integer.parseInt(selectedIndex[i]);
                    purchaseTO.setItemId(Integer.parseInt(itemIds[selectedInd]));
                    System.out.println((new StringBuilder()).append("selectedInd = ").append(selectedInd).toString());
                    System.out.println((new StringBuilder()).append("itemIds[selectedInd]=").append(itemIds[selectedInd]).toString());
                    purchaseTO.setVendorId(Integer.parseInt(vendorIds[selectedInd]));
                    purchaseTO.setReqQty(reqQty[selectedInd]);
                    purchaseTO.setUserId(userId);
                    purchaseTO.setCompanyId(companyId);
                    purchaseTO.setJobCardId(jobCardId);
                    generateMpr.add(purchaseTO);
                }

                purchaseBP.processGenerateMpr(generateMpr, Integer.parseInt(purchaseTypeId));
                vendorList = purchaseBP.processVendorList(1011);
                System.out.println("Updated list");
                purchaseTO = new PurchaseTO();
                mprList = purchaseBP.processMprStatusList(purchaseTO, fromDate, toDate);
                if(mprList.size() != 0)
                    request.setAttribute("mprList", mprList);
                request.setAttribute("vendorList", vendorList);
            }
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleGenerateMpr --> ").append(exception).toString());
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleMprApprovalList(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        purchaseCommand = command;
        PurchaseTO purchaseTO = new PurchaseTO();
        String menuPath = "Stores >> MPR Approval ";
        String pageTitle = "MPR Approval List";
        request.setAttribute("pageTitle", pageTitle);
        int companyId = 0;
        String path = "/content/stores/mprList.jsp";
        ArrayList mprList = new ArrayList();
        ArrayList vendorList = new ArrayList();
        HttpSession session = request.getSession();
        request.setAttribute("menuPath", menuPath);
        companyId = Integer.parseInt((String)session.getAttribute("companyId"));
        int userId = ((Integer)session.getAttribute("userId")).intValue();
        String fromDate = request.getParameter("fromDate");
        String toDate = request.getParameter("toDate");
        System.out.println((new StringBuilder()).append("reques=").append(request).toString());
        try
        {
            ArrayList userFunctions = new ArrayList();
            userFunctions = (ArrayList)session.getAttribute("userFunction");
            if(!loginBP.checkAuthorisation(userFunctions, "Mpr-MPRApprovalList"))
            {
                path = "content/common/NotAuthorized.jsp";
            } else
            {
                if(fromDate == null)
                    fromDate = (String)session.getAttribute("currentDate");
                if(toDate == null)
                    toDate = (String)session.getAttribute("currentDate");
                request.setAttribute("fromDate", fromDate);
                request.setAttribute("toDate", toDate);
                if(purchaseCommand.getFromDate() != null || purchaseCommand.getFromDate() != "")
                    purchaseTO.setFromDate(purchaseCommand.getFromDate());
                if(purchaseCommand.getToDate() != null || purchaseCommand.getToDate() != "")
                    purchaseTO.setToDate(purchaseCommand.getToDate());
                if(purchaseCommand.getVendorId() != null || purchaseCommand.getVendorId() != "")
                    purchaseTO.setVendorId(0);
                mprList = purchaseBP.processMprStatusList(purchaseTO, fromDate, toDate);
                System.out.println((new StringBuilder()).append("mprList size=").append(mprList.size()).toString());
                if(mprList.size() != 0)
                    request.setAttribute("mprList", mprList);
                vendorList = purchaseBP.processVendorList(1011);
                System.out.println((new StringBuilder()).append("vendorList").append(vendorList.size()).toString());
                request.setAttribute("vendorList", vendorList);
            }
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleGenerateMpr --> ").append(exception).toString());
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleStoresMprList(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        purchaseCommand = command;
        PurchaseTO purchaseTO = new PurchaseTO();
        String menuPath = "Stores >> MPR List ";
        System.out.println("Stores >> MPR List ");
        String pageTitle = "MPR List";
        request.setAttribute("pageTitle", pageTitle);
        int companyId = 0;
        String path = "/content/stores/storesMprList.jsp";
        ArrayList mprList = new ArrayList();
        HttpSession session = request.getSession();
        request.setAttribute("menuPath", menuPath);
        companyId = Integer.parseInt((String)session.getAttribute("companyId"));
        String fromDate = "";
        String toDate = "";
        if(request.getParameter("fromDate") == null)
        {
            
            fromDate = (String)session.getAttribute("currentDate");
            toDate = (String)session.getAttribute("currentDate");
            System.out.println("Have  Date"+fromDate+"To"+toDate);
        } else
        {
            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");
            System.out.println("Have No Date"+fromDate+"To"+toDate);
        }
        int userId = ((Integer)session.getAttribute("userId")).intValue();
        System.out.println((new StringBuilder()).append("reques=").append(request).toString());
        try
        {
            ArrayList userFunctions = new ArrayList();
            userFunctions = (ArrayList)session.getAttribute("userFunction");
            System.out.println((new StringBuilder()).append("fromDate=").append(fromDate).toString());
            System.out.println((new StringBuilder()).append("toDate=").append(toDate).toString());
            if(!loginBP.checkAuthorisation(userFunctions, "Mpr-StoresMprList"))
            {
                path = "content/common/NotAuthorized.jsp";
            } else
            {
                mprList = purchaseBP.processMprPoList(purchaseTO, fromDate, toDate);
                System.out.println("Mpr List Size-->"+mprList.size());
                request.setAttribute("mprList", mprList);
                request.setAttribute("fromDate", fromDate);
                request.setAttribute("toDate", toDate);
            }
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleGenerateMpr --> ").append(exception).toString());
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleApproveMprPage(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        purchaseCommand = command;
        String menuPath = "Stores >> View MPR ";
        String pageTitle = "APPROVE MPR";
        request.setAttribute("pageTitle", pageTitle);
        String path = "/content/stores/approveMpr.jsp";
        int mprId = Integer.parseInt(request.getParameter("mprId"));
        ArrayList mprList = new ArrayList();
        HttpSession session = request.getSession();
        request.setAttribute("menuPath", menuPath);
        try
        {
            path = "content/stores/approveMpr.jsp";
            mprList = purchaseBP.processMprDetail(mprId);
            System.out.println("Exception handling Failed0");
            request.setAttribute("mprList", mprList);
        }
        catch(FPRuntimeException exception)
        {
            System.out.println("Exception handling Failed1");
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        }
        catch(FPBusinessException exception1)
        {
            System.out.println("Exception handling Failed2");
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception1.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception1.getErrorMessage());
        }
        catch(Exception exception2)
        {
            System.out.println("Exception handling Failed3");
            exception2.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleGenerateMpr --> ").append(exception2).toString());
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewMprPage(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        purchaseCommand = command;
        String menuPath = "Stores >> View MPR ";
        String pageTitle = "APPROVE MPR";
        request.setAttribute("pageTitle", pageTitle);
        String path = "/content/stores/approveMpr.jsp";
        int mprId = Integer.parseInt(request.getParameter("mprId"));
        ArrayList mprList = new ArrayList();
        HttpSession session = request.getSession();
        request.setAttribute("menuPath", menuPath);
        try
        {
            path = "content/stores/approveMpr.jsp";
            mprList = purchaseBP.processMprDetail(mprId);
            System.out.println("Exception handling Failed0");
            request.setAttribute("mprList", mprList);
        }
        catch(FPRuntimeException exception)
        {
            System.out.println("Exception handling Failed1");
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        }
        catch(FPBusinessException exception1)
        {
            System.out.println("Exception handling Failed2");
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception1.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception1.getErrorMessage());
        }
        catch(Exception exception2)
        {
            System.out.println("Exception handling Failed3");
            exception2.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleGenerateMpr --> ").append(exception2).toString());
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleApproveMpr(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        ModelAndView mv = null;
        purchaseCommand = command;
        PurchaseTO approveTO = new PurchaseTO();
        String menuPath = "Stores >> View MPR ";
        String pageTitle = "APPROVE MPR";
        request.setAttribute("pageTitle", pageTitle);
        String path = "/content/stores/approveMpr.jsp";
        ArrayList vendorList = new ArrayList();
        ArrayList mprList = new ArrayList();
        HttpSession session = request.getSession();
        System.out.println((new StringBuilder()).append("session.getAttribute()").append(session.getAttribute("userId")).toString());
        int userId = ((Integer)session.getAttribute("userId")).intValue();
        System.out.println((new StringBuilder()).append("temp=").append(userId).toString());
        System.out.println((new StringBuilder()).append("userId=").append(userId).toString());
        request.setAttribute("menuPath", menuPath);
        try
        {
            String mprId = request.getParameter("mprId");
            String desc = request.getParameter("desc");
            String status = request.getParameter("status");
            String itemId[] = request.getParameterValues("itemIds");
            String approvedQtys[] = request.getParameterValues("approvedQtys");
            for(int i = 0; i < approvedQtys.length; i++)
                System.out.println((new StringBuilder()).append("approvedQtys==").append(approvedQtys[i]).toString());

            approveTO.setMprId(Integer.parseInt(mprId));
            approveTO.setDesc(desc);
            approveTO.setStatus(status);
            approveTO.setItemIds(itemId);
            approveTO.setApprovedQtys(approvedQtys);
            approveTO.setUserId(userId);
            purchaseBP.processMprApprove(approveTO);
            vendorList = purchaseBP.processVendorList(1011);
            request.setAttribute("mprList", mprList);
            request.setAttribute("vendorList", vendorList);
            path = "content/stores/approveMpr.jsp";
            mprList = purchaseBP.processMprDetail(Integer.parseInt(mprId));
            request.setAttribute("mprList", mprList);
            mv = handleMprApprovalList(request, response, command);
            request.setAttribute("successMessage", "Approved Successfully");
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleGenerateMpr --> ").append(exception).toString());
        }
        return mv;
    }

    public ModelAndView handleGeneratePo(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        purchaseCommand = command;
        String menuPath = "Stores >> View MPR ";
        String pageTitle = "MPR List";
        request.setAttribute("pageTitle", pageTitle);
        String path = "/content/stores/generatePO.jsp";
        int mprId = Integer.parseInt(request.getParameter("mprId"));
        ArrayList poDetail = new ArrayList();
        HttpSession session = request.getSession();
        int companyId = Integer.parseInt((String)session.getAttribute("companyId"));
        System.out.println("handleGeneratePo Company Id-->"+companyId);
        int userId = ((Integer)session.getAttribute("userId")).intValue();
        request.setAttribute("menuPath", menuPath);
        try
        {
            path = "content/stores/generatePO.jsp";
            poDetail = purchaseBP.processGeneratePO(mprId, userId,companyId);
            System.out.println((new StringBuilder()).append("poDetail=").append(poDetail.size()).toString());
            request.setAttribute("poDetail", poDetail);
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        }
        catch(FPBusinessException exception1)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception1.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception1.getErrorMessage());
        }
        catch(Exception exception2)
        {
            exception2.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleGenerateMpr --> ").append(exception2).toString());
        }
        return new ModelAndView(path);
    }
    //Hari
    public ModelAndView handleModifyPoPage(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        purchaseCommand = command;
        String menuPath = "Stores >> ModifyPO ";
        String pageTitle = "ModifyPO";
        System.out.println("In handleModifyPoPage");
        String path = "/content/stores/getPo.jsp";
        try{
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        }

        catch(Exception exception2)
        {
            exception2.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleGenerateMpr --> ").append(exception2).toString());
        }
        return new ModelAndView(path);
    }
    //Hari End

    public ModelAndView handlePoDetail(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        purchaseCommand = command;
        String menuPath = "Stores >> View Purchase Order";
        String pageTitle = "Purchase Order";
        request.setAttribute("pageTitle", pageTitle);
        String path = "/content/stores/generatePO.jsp";
        ArrayList poDetail = new ArrayList();
        HttpSession session = request.getSession();
        int poId = Integer.parseInt(purchaseCommand.getPoId());
        String company = session.getAttribute("companyId").toString();
        int companyId = Integer.parseInt(company);
        int userId = ((Integer)session.getAttribute("userId")).intValue();
        request.setAttribute("menuPath", menuPath);
        try
        {
            path = "content/stores/generatePO.jsp";
            poDetail = purchaseBP.processPoDetail(poId,companyId);
            System.out.println((new StringBuilder()).append("poDetail=").append(poDetail.size()).toString());
            request.setAttribute("poDetail", poDetail);
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        }
        catch(FPBusinessException exception1)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception1.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception1.getErrorMessage());
        }
        catch(Exception exception2)
        {
            exception2.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handlePoDetail--> ").append(exception2).toString());
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleCancelPo(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        purchaseCommand = command;
        String menuPath = "Stores >> View Purchase Order";
        String pageTitle = "Purchase Order";
        request.setAttribute("pageTitle", pageTitle);
        String path = "/content/stores/storesMprList.jsp";
        ArrayList poDetail = new ArrayList();
        HttpSession session = request.getSession();
        ModelAndView mv = null;
        PurchaseTO purch = new PurchaseTO();
        int status = 0;
        int poId = Integer.parseInt(purchaseCommand.getPoId());
        int userId = ((Integer)session.getAttribute("userId")).intValue();
        request.setAttribute("menuPath", menuPath);
        try
        {
            path = "content/stores/storesMprList.jsp";
            status = purchaseBP.processCancelPo(poId);
            if(status == 0)
                request.setAttribute("errorMessage", "Purchase Order Cancellation Failed");
            else
                request.setAttribute("successMessage", "Purchase Order Cancelled Successfully");
            mv = new ModelAndView();
            mv = handleStoresMprList(request, response, command);
            //request.setAttribute("mprList", poDetail);
            System.out.println((new StringBuilder()).append("poDetail=").append(poDetail.size()).toString());
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        }
        catch(FPBusinessException exception1)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception1.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception1.getErrorMessage());
        }
        catch(Exception exception2)
        {
            exception2.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handlePoDetail--> ").append(exception2).toString());
        }
        return mv;
    }

    public ModelAndView handleRequiredItemsPage(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        purchaseCommand = command;
        String menuPath = "Stores >> Required Items ";
        String pageTitle = "Required Items";
        request.setAttribute("pageTitle", pageTitle);
        String path = "/content/stores/generatePO.jsp";
        HttpSession session = request.getSession();
        int servicePtId = Integer.parseInt((String)session.getAttribute("companyId"));
        String companyId = (String)session.getAttribute("companyId");
        int compId = Integer.parseInt(companyId);
        System.out.println((new StringBuilder()).append("companyID=").append(companyId).toString());
        request.setAttribute("menuPath", menuPath);
        ArrayList vendorItemList = new ArrayList();
        ArrayList requiredItemsList = new ArrayList();
        ArrayList jobCardList = new ArrayList();
        try
        {
            ArrayList userFunctions = new ArrayList();
            userFunctions = (ArrayList)session.getAttribute("userFunction");
            if(!loginBP.checkAuthorisation(userFunctions, "Purchase-ShowRequiredItems"))
            {
                path = "content/common/NotAuthorized.jsp";
            } else
            {
                path = "content/stores/requiredItems.jsp";
                PurchaseTO compTO = new PurchaseTO();
                compTO.setCompanyId(companyId);
                ArrayList categoryList = new ArrayList();
                categoryList = sectionBP.processGetCategoryList();
                System.out.println((new StringBuilder()).append("jobCardList=").append(jobCardList.size()).toString());
                request.setAttribute("jobCardlist", jobCardList);
                request.setAttribute("CategoryList", categoryList);
                System.out.println((new StringBuilder()).append("requiredItemsList size=").append(requiredItemsList.size()).toString());
                System.out.println((new StringBuilder()).append("requiredItemsList=").append(requiredItemsList.size()).toString());
                System.out.println((new StringBuilder()).append("vendorItemList=").append(vendorItemList.size()).toString());
                int pageNo = 0;
                int totalPages = 0;
                int startIndex = 0;
                int endIndex = 0;
                int totalRecords = 0;
                totalRecords = requiredItemsList.size();
                PaginationHelper pagenation = new PaginationHelper();
                pagenation.setTotalRecords(totalRecords);
                String buttonClicked = "";
                session.setAttribute("totalRecords", Integer.valueOf(totalRecords));
                pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
                totalPages = pagenation.getTotalNoOfPages();
                request.setAttribute("pageNo", Integer.valueOf(pageNo));
                System.out.println((new StringBuilder()).append("pageNo").append(pageNo).toString());
                request.setAttribute("totalPages", Integer.valueOf(totalPages));
                startIndex = pagenation.getStartIndex();
                endIndex = pagenation.getEndIndex();
            }
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        }
        catch(FPBusinessException exception1)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception1.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception1.getErrorMessage());
        }
        catch(Exception exception2)
        {
            exception2.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleGenerateMpr --> ").append(exception2).toString());
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleSearchRequiredItems(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        long currentTimeInMillis = System.currentTimeMillis();
        Date today = new Date(currentTimeInMillis);
        System.out.println((new StringBuilder()).append("start:").append(today).toString());
        purchaseCommand = command;
        String menuPath = "Stores >> Required Items ";
        String pageTitle = "Required Items";
        request.setAttribute("pageTitle", pageTitle);
        String path = "";
        HttpSession session = request.getSession();
        int servicePtId = Integer.parseInt((String)session.getAttribute("companyId"));
        String companyId = (String)session.getAttribute("companyId");
        int compId = Integer.parseInt(companyId);
        System.out.println((new StringBuilder()).append("companyID=").append(companyId).toString());
        request.setAttribute("menuPath", menuPath);
        ArrayList vendorItemList = new ArrayList();
        ArrayList requiredItemsList = new ArrayList();
        ArrayList recordsWithLimit = new ArrayList();
        ArrayList jobCardlist = new ArrayList();
        try
        {
            ArrayList userFunctions = new ArrayList();
            userFunctions = (ArrayList)session.getAttribute("userFunction");
            if(!loginBP.checkAuthorisation(userFunctions, "Purchase-ShowRequiredItems"))
            {
                path = "content/common/NotAuthorized.jsp";
            } else
            {
                int pageNo = 0;
                int totalPages = 0;
                int startIndex = 0;
                int endIndex = 0;
                int totalRecords = 0;
                path = "content/stores/requiredItems.jsp";
                PurchaseTO compTO = new PurchaseTO();
                compTO.setCompanyId(companyId);
                compTO.setMfrCode(purchaseCommand.getMfrCode());
                compTO.setPaplCode(purchaseCommand.getPaplCode());
                compTO.setCategoryId(purchaseCommand.getCategoryId());
                compTO.setSearchAll(purchaseCommand.getSearchAll());
                request.setAttribute("mfrCode", compTO.getMfrCode());
                request.setAttribute("paplCode", compTO.getPaplCode());
                request.setAttribute("categoryId", compTO.getCategoryId());
                request.setAttribute("searchAll", compTO.getSearchAll());
                currentTimeInMillis = System.currentTimeMillis();
                today = new Date(currentTimeInMillis);
                System.out.println((new StringBuilder()).append("start1:").append(today).toString());
                requiredItemsList = purchaseBP.processTotalGetRequiredItems(compTO, compId, 0, 0);
                currentTimeInMillis = System.currentTimeMillis();
                today = new Date(currentTimeInMillis);
                System.out.println((new StringBuilder()).append("start2:").append(today).toString());
                totalRecords = requiredItemsList.size();
                PaginationHelper pagenation = new PaginationHelper();
                pagenation.setTotalRecords(totalRecords);
                String buttonClicked = "";
                if(request.getParameter("button") != null)
                    buttonClicked = request.getParameter("button");
                pageNo = Integer.parseInt(request.getParameter("pageNo"));
                session.setAttribute("totalRecords", Integer.valueOf(totalRecords));
                pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
                totalPages = pagenation.getTotalNoOfPages();
                request.setAttribute("pageNo", Integer.valueOf(pageNo));
                request.setAttribute("totalPages", Integer.valueOf(totalPages));
                startIndex = pagenation.getStartIndex();
                endIndex = pagenation.getEndIndex();
                currentTimeInMillis = System.currentTimeMillis();
                today = new Date(currentTimeInMillis);
                System.out.println((new StringBuilder()).append("start3:").append(today).toString());
                requiredItemsList = purchaseBP.processGetRequiredItems(compTO, compId, startIndex, endIndex);
                currentTimeInMillis = System.currentTimeMillis();
                today = new Date(currentTimeInMillis);
                System.out.println((new StringBuilder()).append("start4:").append(today).toString());
                ArrayList categoryList = new ArrayList();
                categoryList = sectionBP.processGetCategoryList();
                request.setAttribute("CategoryList", categoryList);
                currentTimeInMillis = System.currentTimeMillis();
                today = new Date(currentTimeInMillis);
                System.out.println((new StringBuilder()).append("start5:").append(today).toString());
                request.setAttribute("requiredItemsList", requiredItemsList);
            }
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        }
        catch(FPBusinessException exception1)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception1.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception1.getErrorMessage());
        }
        catch(Exception exception2)
        {
            exception2.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleGenerateMpr --> ").append(exception2).toString());
        }
        currentTimeInMillis = System.currentTimeMillis();
        today = new Date(currentTimeInMillis);
        System.out.println((new StringBuilder()).append("start6:").append(today).toString());
        return new ModelAndView(path);
    }

    public ModelAndView handleReceiveInvoice(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        String path = "";
        ArrayList vendorList = new ArrayList();
        HttpSession session = request.getSession();
        purchaseCommand = command;
        System.out.println("view InVoice");
        String menuPath = "";
        String pageTitle = "Receive InVoice";
        menuPath = "Stores  >>Receive InVoice ";
        request.setAttribute("pageTitle", pageTitle);
        int vendorType = 1011;
        request.setAttribute("menuPath", menuPath);
        try
        {
            ArrayList userFunctions = (ArrayList)session.getAttribute("userFunction");
            if(!loginBP.checkAuthorisation(userFunctions, "Purchase-InvoiceView"))
            {
                path = "content/common/NotAuthorized.jsp";
            } else
            {
                path = "content/stores/receiveInvoice.jsp";
                vendorList = purchaseBP.processVendorList(vendorType);
                request.setAttribute("vendorLists", vendorList);
                System.out.println((new StringBuilder()).append("vendorList size").append(vendorList.size()).toString());
            }
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to viewvendorList --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void handlePurchaseOrderList(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        System.out.println("i am in ajax ");
        System.out.println("i am in ajax1 ");
        purchaseCommand = command;
        PurchaseTO purchaseTO = new PurchaseTO();
        HttpSession session = request.getSession();
        String companyId = "";
        companyId = (String)session.getAttribute("companyId");
        System.out.println((new StringBuilder()).append("request values=").append(request).toString());
        String vendorId = request.getParameter("vendorId");
        String poList = "";
        try
        {
            System.out.println((new StringBuilder()).append("vendorId  ").append(vendorId).toString());
            purchaseTO.setVendorId(Integer.parseInt(vendorId));
            purchaseTO.setCompanyId(companyId);
            poList = purchaseBP.processPoList(purchaseTO);
            PrintWriter writer = response.getWriter();
            System.out.println((new StringBuilder()).append("poList size   :").append(poList).toString());
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(poList);
            writer.close();
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to retrieve PriceList data in Ajax --> ").append(exception).toString());
        }
    }

    public ModelAndView handlePurchaseItems(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command){
        System.out.println((new StringBuilder()).append("request:").append(request).toString());
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        System.out.println("view InVoice");
        String path = "";
        ArrayList itemList = new ArrayList();
        ArrayList vendorList = new ArrayList();
        ArrayList vatList = new ArrayList();
        HttpSession session = request.getSession();
        int userId = ((Integer)session.getAttribute("userId")).intValue();
        purchaseCommand = command;
        PurchaseTO purchaseTO = new PurchaseTO();
        String menuPath = "";
        int vendorType = 1011;
        int status = 0;
        String pageTitle = "Receive InVoice";
        menuPath = "Stores  >>Receive InVoice ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        try
        {
            purchaseTO.setDcNumber(purchaseCommand.getDcNumber());
            purchaseTO.setInVoiceNumber(purchaseCommand.getInVoiceNumber());
            purchaseTO.setInVoiceDate(purchaseCommand.getInVoiceDate());
            purchaseTO.setInVoiceAmount(purchaseCommand.getInVoiceAmount());
            purchaseTO.setVendorId(Integer.parseInt(request.getParameter("vendorId")));
            purchaseTO.setInventoryStatus(request.getParameter("inventoryStatus"));
            purchaseTO.setPoId(Integer.parseInt(request.getParameter("poIds")));
            path = "content/stores/receiveInvoice.jsp";
            itemList = purchaseBP.processItemList(purchaseTO);
            //itemList = purchaseBP.processItemList(purchaseTO);
            vendorList = purchaseBP.processVendorList(vendorType);
            vatList = purchaseBP.processActiveVatList();
            ArrayList positionList = new ArrayList();
            positionList = mrsBP.getTyrePostions();
            request.setAttribute("positionList", positionList);
            request.setAttribute("ItemList", itemList);
            request.setAttribute("vatList", vatList);
            request.setAttribute("vendorLists", vendorList);
            System.out.println((new StringBuilder()).append("itemList size").append(itemList.size()).toString());
            request.setAttribute("inVoiceNumber", purchaseTO.getInVoiceNumber());
            request.setAttribute("inVoiceDate", purchaseTO.getInVoiceDate());
            request.setAttribute("inVoiceAmount", purchaseTO.getInVoiceAmount());
            request.setAttribute("getPO", request.getParameter("poIds"));
            request.setAttribute("dcNumber", purchaseTO.getDcNumber());
            request.setAttribute("vendorId", Integer.valueOf(purchaseTO.getVendorId()));
            request.setAttribute("inventoryStatus", purchaseTO.getInventoryStatus());
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to viewItemList --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleInsertItems(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        System.out.println((new StringBuilder()).append("req=").append(request).toString());
        HttpSession session = request.getSession();
        String companyId = "";
        companyId = (String)session.getAttribute("companyId");
        int userId = ((Integer)session.getAttribute("userId")).intValue();
        System.out.println((new StringBuilder()).append("companyId").append(companyId).toString());
        purchaseCommand = command;
        ArrayList List = new ArrayList();
        ArrayList tyreList = new ArrayList();
        int vendorType = 1011;
        String path = "";
        ModelAndView mv = new ModelAndView();
        try
        {
            String menuPath = "Stores  >>  Receive InVoice";
            request.setAttribute("menuPath", menuPath);
            ArrayList userFunctions = (ArrayList)session.getAttribute("userFunction");
            System.out.println("in In voice items insert");
            int index = 0;
            int insert = 0;
            PurchaseTO purchaseTO = new PurchaseTO();
            PurchaseTO tyreTO = new PurchaseTO();
            String poId = request.getParameter("poIds");
            String vendorId = request.getParameter("vendorId");
            String invoiceDate = request.getParameter("inVoiceDate");
            String invoiceAmount = request.getParameter("inVoiceAmount");
            String purchaseType = request.getParameter("purchaseType");
            String inventoryStatus = request.getParameter("inventoryStatus");
            purchaseTO.setPoId(Integer.parseInt(poId));
            purchaseTO.setVendorId(Integer.parseInt(vendorId));
            purchaseTO.setInVoiceDate(invoiceDate);
            purchaseTO.setFreight(purchaseCommand.getFreight());
            purchaseTO.setInVoiceAmount(invoiceAmount);
            purchaseTO.setInventoryStatus(inventoryStatus);
            purchaseTO.setPurchaseType(purchaseType);
            purchaseTO.setRemarks(purchaseCommand.getRemarks());
            purchaseTO.setTransactionType(purchaseCommand.getTransactionType());
            String mrps[] = purchaseCommand.getMrps();
            String itemMrps[] = purchaseCommand.getItemMrp();
            String unitPrice[] = purchaseCommand.getUnitPrice();
            String tax[] = purchaseCommand.getTax();
            String discount[] = purchaseCommand.getDiscount();
            String receivedQtys[] = purchaseCommand.getReceivedQtys();
            String acceptedQtys[] = purchaseCommand.getAcceptedQtys();
            String itemIds[] = purchaseCommand.getItemIds();
            String itemAmounts[] = purchaseCommand.getItemAmounts();
            String selectedIndex[] = purchaseCommand.getSelectedIndex();
            String tyreItemId[] = request.getParameterValues("tyreItemId");
            String tyreNo[] = request.getParameterValues("tyreNo");
            String posIds[] = request.getParameterValues("posIds");
            int supplyId = 0;
            purchaseTO.setCompanyId(companyId);
            System.out.println((new StringBuilder()).append("itemAmounts").append(itemAmounts).toString());
            purchaseTO.setInVoiceNumber(purchaseCommand.getInVoiceNumber());
            purchaseTO.setDcNumber(purchaseCommand.getDcNumber());
            PurchaseTO listTO = null;
            System.out.println((new StringBuilder()).append("getInVoiceNumber = ").append(purchaseTO.getInVoiceNumber()).toString());
            System.out.println((new StringBuilder()).append("acceptedQtys=").append(acceptedQtys.length).toString());
            for(int i = 0; i < selectedIndex.length; i++)
            {
                listTO = new PurchaseTO();
                index = Integer.parseInt(selectedIndex[i]);
                if(!acceptedQtys[index].equals("0"))
                {
                    listTO.setTax(tax[index]);
                    listTO.setDiscount(discount[index]);
                    listTO.setUnitPrice(unitPrice[index]);
                    listTO.setMrp(mrps[index]);
                    listTO.setItemMrp(itemMrps[index]);
                    System.out.println("itemMrp:"+listTO.getItemMrp());
                    System.out.println((new StringBuilder()).append("mrp").append(listTO.getMrp()).toString());
                    listTO.setReceivedQty(receivedQtys[index]);
                    System.out.println((new StringBuilder()).append("rqty").append(listTO.getReceivedQty()).toString());
                    listTO.setAcceptedQty(acceptedQtys[index]);
                    System.out.println((new StringBuilder()).append("aqty").append(listTO.getAcceptedQty()).toString());
                    listTO.setItemId(Integer.parseInt(itemIds[index]));
                    System.out.println((new StringBuilder()).append("itemId").append(listTO.getItemId()).toString());
                    listTO.setItemAmount(itemAmounts[index]);
                    System.out.println((new StringBuilder()).append("itemAmount").append(listTO.getItemAmount()).toString());
                    List.add(listTO);
                }
            }

            System.out.println((new StringBuilder()).append("userId").append(userId).toString());
            path = "content/stores/receiveInvoice.jsp";
            System.out.println((new StringBuilder()).append("purchaseCommand.getInventoryStatus()").append(purchaseCommand.getInventoryStatus()).toString());
            supplyId = purchaseBP.processInsertInVoice(purchaseTO, userId);
            purchaseTO.setSupplyId(String.valueOf(supplyId));
            if(purchaseCommand.getTransactionType().equals("DC"))
                insert = purchaseBP.processInsertItem(List, userId, purchaseTO);
                purchaseBP.processItemList(purchaseTO);
            if(tyreItemId != null && tyreItemId.length != 0)
            {
                for(int i = 0; i < tyreItemId.length; i++)
                    if(!tyreItemId[i].equals("") && !tyreNo[i].equals(""))
                    {
                        tyreTO = new PurchaseTO();
                        tyreTO.setPoId(Integer.parseInt(poId));
                        tyreTO.setCompanyId(companyId);
                        tyreTO.setSupplyId(String.valueOf(supplyId));
                        tyreTO.setItemId(Integer.parseInt(tyreItemId[i]));
                        tyreTO.setTyreNo(tyreNo[i]);
                        tyreTO.setIsRt("N");
                        tyreTO.setStatus("Y");
                        tyreTO.setPosId(posIds[i]);
                        tyreList.add(tyreTO);
                    }

                purchaseBP.processTyreSupply(tyreList);
                if(inventoryStatus.equalsIgnoreCase("Y"))
                    purchaseBP.processTyreDetail(tyreList, userId);
            }
            String pageTitle = "Receive Invoice";
            request.setAttribute("pageTitle", pageTitle);
            ArrayList vendorList = new ArrayList();
            vendorList = purchaseBP.processVendorList(vendorType);
            request.setAttribute("vendorLists", vendorList);
            request.setAttribute("supplyId", Integer.valueOf(supplyId));
            request.setAttribute("successMessage", (new StringBuilder()).append("DC Details Added  Successfully and GRN No is").append(supplyId).toString());
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to update data  --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleModifyGrnPage(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        System.out.println("handleGrnDetails");
        String path = "";
        ArrayList supplyList = new ArrayList();
        purchaseCommand = command;
        PurchaseTO supplyTO = new PurchaseTO();
        String menuPath = "";
        String pageTitle = "Modify GRN";
        menuPath = "Stores  >> Modify GRN";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        HttpSession session = request.getSession();
        try
        {
            ArrayList userFunctions = (ArrayList)session.getAttribute("userFunction");
            if(!loginBP.checkAuthorisation(userFunctions, "Purchase-ModifyGrn"))
                path = "content/common/NotAuthorized.jsp";
            else
                path = "content/stores/getGrn.jsp";
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to viewItemList --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleGrnDetails(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        System.out.println("handleGrnDetails");
        String path = "";
        ArrayList supplyList = new ArrayList();
        ArrayList tyreList = new ArrayList();
        purchaseCommand = command;
        PurchaseTO supplyTO = new PurchaseTO();
        String menuPath = "";
        int status = 0;
        String pageTitle = "Modify GRN";
        String reqFor="";
        menuPath = "Stores  >>Modify GRN";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        
        try
        {
            reqFor= purchaseCommand.getReqFor();
            System.out.println("Request For"+reqFor);
            supplyTO.setSupplyId(purchaseCommand.getSupplyId());
            supplyTO.setInVoiceNumber(purchaseCommand.getInVoiceNumber());
            supplyTO.setDcNumber(purchaseCommand.getDcNumber());
            supplyTO.setInVoiceAmount(purchaseCommand.getInVoiceAmount());
            System.out.println((new StringBuilder()).append("invoice amount=").append(purchaseCommand.getInVoiceAmount()).toString());
            supplyTO.setAcceptedQtys(purchaseCommand.getAcceptedQtys());
            supplyTO.setItemIds(purchaseCommand.getItemIds());
            supplyTO.setItemAmounts(purchaseCommand.getItemAmounts());
            supplyList = purchaseBP.processGrnDetails(supplyTO);
            if(supplyList.size() == 0 && reqFor==null)
            {
                path = "content/stores/getGrn.jsp";
                request.setAttribute("errorMessage", "Invoice Not Found");
            } else if(reqFor==null) {
                Iterator supplyItr = supplyList.iterator();
                PurchaseTO purchTO;
                if(supplyItr.hasNext()) {
                    purchTO = new PurchaseTO();
                    purchTO = (PurchaseTO) supplyItr.next();

                    if(!"".equals(purchTO.getInVoiceNumber())){

                        path = "content/stores/getGrn.jsp";
                        request.setAttribute("errorMessage", "Invoice Already Updated");                        

                    }else{
                        path = "content/stores/modifyGRN.jsp";
                    }
                }

                
            }
            else if(supplyList.size()!= 0)
            {
                System.out.println("In print Block");
                path = "content/stores/printGrn.jsp";
            }
            else
            {
                path = "content/stores/getGrn.jsp";
                request.setAttribute("errorMessage", "Invoice Not Found");
            }
            request.setAttribute("supplyList", supplyList);
            request.setAttribute("tyreList", tyreList);
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to viewItemList --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

//    public ModelAndView handlePrintGrnDetails(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
//    {
//        if(request.getSession().isNew())
//            return new ModelAndView("content/common/login.jsp");
//        System.out.println("handlePrintGrnDetails");
//        String path = "";
//        ArrayList supplyList = new ArrayList();
//        ArrayList tyreList = new ArrayList();
//        purchaseCommand = command;
//        PurchaseTO supplyTO = new PurchaseTO();
//        String menuPath = "";
//        int status = 0;
//        String pageTitle = "Modify GRN";
//        menuPath = "Stores  >>Modify GRN";
//        request.setAttribute("pageTitle", pageTitle);
//        request.setAttribute("menuPath", menuPath);
//        try
//        {
//            supplyTO.setSupplyId(purchaseCommand.getSupplyId());
//
//            System.out.println((new StringBuilder()).append("invoice amount=").append(purchaseCommand.getInVoiceAmount()).toString());
//            supplyList = purchaseBP.processGrnDetails(supplyTO);
//            if(supplyList.size() == 0)
//            {
//                path = "content/stores/getGrn.jsp";
//                request.setAttribute("errorMessage", "Invoice Not Found");
//            } else
//            {
//                path = "content/stores/modifyGRN.jsp";
//            }
//            request.setAttribute("supplyList", supplyList);
//            request.setAttribute("tyreList", tyreList);
//        }
//        catch(FPRuntimeException exception)
//        {
//            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
//            return new ModelAndView("content/common/error.jsp");
//        }
//        catch(FPBusinessException exception)
//        {
//            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
//            request.setAttribute("errorMessage", exception.getErrorMessage());
//        }
//        catch(Exception exception)
//        {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to viewItemList --> ").append(exception).toString());
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }

    public ModelAndView handleGrnDetailsFromBill(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        System.out.println("handleGrnDetailsFromBill");
        String path = "";
        ArrayList supplyList = new ArrayList();
        purchaseCommand = command;
        PurchaseTO supplyTO = new PurchaseTO();
        String menuPath = "";
        int status = 0;
        String pageTitle = "Modify GRN";
        menuPath = "Stores  >>Modify GRN";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        try
        {
            System.out.println((new StringBuilder()).append("grnId servlet = ").append(request.getParameter("supplyId")).toString());
            if(request.getParameter("supplyId") != null)
                supplyTO.setSupplyId(request.getParameter("supplyId"));
            else
                supplyTO.setSupplyId(String.valueOf(0));
            supplyList = purchaseBP.processGrnDetails(supplyTO);
            if(supplyList.size() == 0)
            {
                path = "content/stores/getGrn.jsp";
                request.setAttribute("errorMessage", "Invoice Not Found");
            } else
            {
                path = "content/stores/modifyGRN.jsp";
            }
            request.setAttribute("supplyList", supplyList);
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to viewItemList --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleUpdateGrn(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        System.out.println((new StringBuilder()).append("request=").append(request).toString());
        System.out.println("handleGrnDetails");
        HttpSession session = request.getSession();
        String path = "";
        ArrayList supplyList = new ArrayList();
        purchaseCommand = command;
        PurchaseTO supplyTO = new PurchaseTO();
        String menuPath = "";
        int status = 0;
        String pageTitle = "Modify GRN";
        menuPath = "Stores  >>Modify GRN";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        try
        {
            int userId = ((Integer)session.getAttribute("userId")).intValue();
            String companyId = (String)session.getAttribute("companyId");
            supplyTO.setSupplyId(purchaseCommand.getSupplyId());
            supplyTO.setPoId(Integer.parseInt(purchaseCommand.getPoId()));
            supplyTO.setItemIds(purchaseCommand.getItemIds());
            supplyTO.setInVoiceNumber(purchaseCommand.getInVoiceNumber());
            supplyTO.setDcNumber(purchaseCommand.getDcNumber());
            supplyTO.setInVoiceAmount(purchaseCommand.getInVoiceAmount());
            supplyTO.setFreight(purchaseCommand.getFreight());
            supplyTO.setInVoiceDate(purchaseCommand.getInVoiceDate());
            supplyTO.setTaxs(purchaseCommand.getTax());
            supplyTO.setDiscounts(purchaseCommand.getDiscount());
            supplyTO.setUnitPrices(purchaseCommand.getUnitPrice());
            supplyTO.setMrps(purchaseCommand.getMrps());
            supplyTO.setRetQtys(purchaseCommand.getRetQtys());
            supplyTO.setItemAmounts(purchaseCommand.getItemAmounts());
            supplyTO.setAcceptedQtys(purchaseCommand.getAcceptedQtys());
//            bala
            supplyTO.setTotalQtys(purchaseCommand.getTotalQtys());
//            bala ends
            supplyTO.setCompanyId(companyId);
            path = "content/stores/getGrn.jsp";
            purchaseBP.processUpdateGrn(supplyTO, userId);
            path = "content/stores/getGrn.jsp";
            request.setAttribute("supplyList", supplyList);
            request.setAttribute("successMessage", "Invoice Modified Successfully");
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time handleUpdateGrn --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleUpdateGrn --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleDirectPurchaseRetQty(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        System.out.println((new StringBuilder()).append("req=").append(request).toString());
        HttpSession session = request.getSession();
        String companyId = "";
        companyId = (String)session.getAttribute("companyId");
        System.out.println((new StringBuilder()).append("companyId").append(companyId).toString());
        purchaseCommand = command;
        ArrayList List = new ArrayList();
        ArrayList tyreList = new ArrayList();
        int vendorType = 1011;
        String path = "";
        try
        {
            String menuPath = "Stores  >>  Receive InVoice";
            request.setAttribute("menuPath", menuPath);
            ArrayList userFunctions = (ArrayList)session.getAttribute("userFunction");
            System.out.println("in In voice items insert");
            int index = 0;
            int insert = 0;
            String grnNo = request.getParameter("supplyId");
            String mrps[] = purchaseCommand.getMrps();
            String returnedQtys[] = purchaseCommand.getReturnedQtys();
            String itemIds[] = purchaseCommand.getItemIds();
            String selectedIndex[] = purchaseCommand.getSelectedIndex();
            PurchaseTO listTO = null;
            System.out.println((new StringBuilder()).append("returnedQtys=").append(returnedQtys.length).toString());
            for(int i = 0; i < selectedIndex.length; i++)
            {
                listTO = new PurchaseTO();
                index = Integer.parseInt(selectedIndex[i]);
                listTO.setMrp(mrps[index]);
                System.out.println((new StringBuilder()).append("mrp").append(listTO.getMrp()).toString());
                System.out.println((new StringBuilder()).append("returnedQtys[index]").append(returnedQtys[index]).toString());
                listTO.setItemId(Integer.parseInt(itemIds[index]));
                listTO.setReturnedQty(returnedQtys[index]);
                listTO.setSupplyId(grnNo);
                listTO.setCompanyId(companyId);
                List.add(listTO);
            }

            int userId = ((Integer)session.getAttribute("userId")).intValue();
            System.out.println((new StringBuilder()).append("userId").append(userId).toString());
            path = "content/stores/getGrn.jsp";
            insert = purchaseBP.insertInVoiceReturnedQty(List, userId);
            String pageTitle = "Modify GRN";
            request.setAttribute("pageTitle", pageTitle);
            request.setAttribute("successMessage", "GRN Modified Successfully");
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to update data  --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleGenerateDirectMprPage(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        purchaseCommand = command;
        String menuPath = "Stores >> Generate MPR";
        String pageTitle = "Generate MPR";
        request.setAttribute("pageTitle", pageTitle);
        String path = "/content/stores/generateDirectMPR.jsp";
        HttpSession session = request.getSession();
        int servicePtId = Integer.parseInt((String)session.getAttribute("companyId"));
        String companyId = (String)session.getAttribute("companyId");
        int compId = Integer.parseInt(companyId);
        System.out.println((new StringBuilder()).append("companyID=").append(companyId).toString());
        request.setAttribute("menuPath", menuPath);
        ArrayList vendorList = new ArrayList();
        ArrayList jobCardList = new ArrayList();
        ArrayList servicePointList = new ArrayList();
        try
        {
            ArrayList userFunctions = new ArrayList();
            userFunctions = (ArrayList)session.getAttribute("userFunction");
            if(!loginBP.checkAuthorisation(userFunctions, "Purchase-ShowRequiredItems"))
            {
                path = "content/common/NotAuthorized.jsp";
            } else
            {
                path = "content/stores/generateDirectMPR.jsp";
                servicePointList = vehicleBP.processGetspList();
                vendorList = purchaseBP.processVendorList(1011);
                request.setAttribute("vendorList", vendorList);
                request.setAttribute("servicePointList", servicePointList);
            }
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        }
        catch(FPBusinessException exception1)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception1.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception1.getErrorMessage());
        }
        catch(Exception exception2)
        {
            exception2.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleGenerateMpr --> ").append(exception2).toString());
        }
        return new ModelAndView(path);
    }
    public ModelAndView handleVendorItemConfigPage(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        purchaseCommand = command;
        String menuPath = "Vendor >> Item Configure";
        String pageTitle = "Generate Vendor Item";
        request.setAttribute("pageTitle", pageTitle);
        String path = "/content/stores/vendorItemConfigure.jsp";
        HttpSession session = request.getSession();
        int servicePtId = Integer.parseInt((String)session.getAttribute("companyId"));
        String companyId = (String)session.getAttribute("companyId");
        int compId = Integer.parseInt(companyId);
        System.out.println((new StringBuilder()).append("companyID=").append(companyId).toString());
        request.setAttribute("menuPath", menuPath);
        ArrayList vendorList = new ArrayList();
        ArrayList vendorItemList = new ArrayList();
        try
        {
            ArrayList userFunctions = new ArrayList();
            userFunctions = (ArrayList)session.getAttribute("userFunction");
            if(!loginBP.checkAuthorisation(userFunctions, "Purchase-ShowRequiredItems"))
            {
                path = "content/common/NotAuthorized.jsp";
            } else
            {
                path = "content/stores/vendorItemConfigure.jsp";

                String vendorId = request.getParameter("vendorId");
                if(vendorId != null && !"0".equals(vendorId)){
                    vendorItemList = purchaseBP.processVendorItemList(vendorId);
                    request.setAttribute("vendorItemList", vendorItemList);
                    request.setAttribute("vendorId", vendorId);

                    ArrayList vatList = new ArrayList();
                    vatList = purchaseBP.processVatList();
                    request.setAttribute("vatList", vatList);
                }
                vendorList = purchaseBP.processVendorList(1011);
                request.setAttribute("vendorList", vendorList);
                
            }
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        }
        catch(FPBusinessException exception1)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception1.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception1.getErrorMessage());
        }
        catch(Exception exception2)
        {
            exception2.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleGenerateMpr --> ").append(exception2).toString());
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveVendorItems(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        HttpSession session = request.getSession();
        PurchaseTO purchaseTO = new PurchaseTO();
        String menuPath = "Vendor >> Item Configure";
        String pageTitle = "MPR List";
        request.setAttribute("pageTitle", pageTitle);
        int selectedInd = 0;

        String companyId = (String)session.getAttribute("companyId");
        String path = "";
        request.setAttribute("menuPath", menuPath);
        int userId = ((Integer)session.getAttribute("userId")).intValue();
        String itemIds[] = request.getParameterValues("itemIds");
        String vendorId = request.getParameter("vendorId");
        String buyPrice[] = request.getParameterValues("buyPrice");
        String discounts[] = request.getParameterValues("discount");
        String unitPrice[] = request.getParameterValues("unitPrice");
        String vatId[] = request.getParameterValues("vatId");

        try
        {
            ArrayList userFunctions = new ArrayList();
            userFunctions = (ArrayList)session.getAttribute("userFunction");
            if(!loginBP.checkAuthorisation(userFunctions, "Mpr-GenerateMPR"))
            {
                path = "content/common/NotAuthorized.jsp";
            } else
            {
                for(int i = 0; i < itemIds.length; i++){
                    if(!"".equals(itemIds[i]) && itemIds[i] != null){
                        purchaseBP.processVendorItem(vendorId, itemIds[i],unitPrice[i],vatId[i],buyPrice[i],discounts[i],userId);
                    }
                }

                path = "content/stores/vendorItemConfigure.jsp";

                vendorId = request.getParameter("vendorId");
                if(vendorId != null && !"0".equals(vendorId)){
                    ArrayList vendorItemList = purchaseBP.processVendorItemList(vendorId);
                    request.setAttribute("vendorItemList", vendorItemList);
                    request.setAttribute("vendorId", vendorId);
                    ArrayList vatList = new ArrayList();
                    vatList = purchaseBP.processVatList();
                    request.setAttribute("vatList", vatList);
                }
                ArrayList vendorList = purchaseBP.processVendorList(1011);
                request.setAttribute("vendorList", vendorList);


            }
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleGenerateMpr --> ").append(exception).toString());
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleGenerateDirectMpr(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        HttpSession session = request.getSession();
        PurchaseTO purchaseTO = new PurchaseTO();
        String menuPath = "Stores >> MPR List";
        String pageTitle = "MPR List";
        request.setAttribute("pageTitle", pageTitle);
        int selectedInd = 0;
        String fromDate = request.getParameter("fromDate");
        String toDate = request.getParameter("toDate");
        String companyId = (String)session.getAttribute("companyId");
        String path = "/content/stores/mprRaisedList.jsp";
        ArrayList generateMpr = new ArrayList();
        ArrayList mprList = new ArrayList();
        ArrayList mrsSummary = new ArrayList();
        ArrayList itemVedors = new ArrayList();
        ArrayList vendorList = new ArrayList();
        request.setAttribute("menuPath", menuPath);
        int userId = ((Integer)session.getAttribute("userId")).intValue();
        System.out.println((new StringBuilder()).append("user id is=").append(session.getAttribute("userId")).append("-").toString());
        String itemIds[] = request.getParameterValues("itemIds");
        String vendorId = request.getParameter("vendorId");
        String desc = request.getParameter("desc");
        String reqQty[] = request.getParameterValues("reqQtys");
        String buyPrice[] = request.getParameterValues("buyPrice");
        String unitPrice[] = request.getParameterValues("unitPrice");
        String discount[] = request.getParameterValues("discount");
        String vat[] = request.getParameterValues("vat");
        String purchaseTypeId = "1012";
        if(!request.getParameter("mrsJobCardNumber").equals("0"))
            purchaseTypeId = "1013";
        try
        {
            ArrayList userFunctions = new ArrayList();
            userFunctions = (ArrayList)session.getAttribute("userFunction");
            if(!loginBP.checkAuthorisation(userFunctions, "Mpr-GenerateMPR"))
            {
                path = "content/common/NotAuthorized.jsp";
            } else
            {
                if(fromDate == null)
                    fromDate = (String)session.getAttribute("currentDate");
                if(toDate == null)
                    toDate = (String)session.getAttribute("currentDate");
                request.setAttribute("fromDate", fromDate);
                request.setAttribute("toDate", toDate);
                for(int i = 0; i < itemIds.length; i++)
                {
                    purchaseTO = new PurchaseTO();
                    selectedInd = i;
                    if(itemIds[selectedInd] != null && itemIds[selectedInd] != "")
                    {
                        purchaseTO.setItemId(Integer.parseInt(itemIds[selectedInd]));
                        purchaseTO.setVendorId(Integer.parseInt(vendorId));
                        purchaseTO.setReqQty(reqQty[selectedInd]);
                        purchaseTO.setBuyPrice(buyPrice[selectedInd]);
                        purchaseTO.setUnitPrice(unitPrice[selectedInd]);
                        purchaseTO.setDiscount(discount[selectedInd]);
                        purchaseTO.setVat(vat[selectedInd]);

                        System.out.println("Re Qty-->"+purchaseTO.getReqQty());
                        purchaseTO.setUserId(userId);
                        purchaseTO.setCompanyId(companyId);
                        purchaseTO.setDesc(desc);
                        purchaseTO.setJobCardId(request.getParameter("mrsJobCardNumber"));
                        generateMpr.add(purchaseTO);
                    }
                }

                purchaseBP.processGenerateMpr(generateMpr, Integer.parseInt(purchaseTypeId));
                vendorList = purchaseBP.processVendorList(1011);
                System.out.println("Updated list");
                purchaseTO = new PurchaseTO();
                mprList = purchaseBP.processMprStatusList(purchaseTO, fromDate, toDate);
                if(mprList.size() != 0)
                    request.setAttribute("mprList", mprList);
                request.setAttribute("vendorList", vendorList);
            }
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleGenerateMpr --> ").append(exception).toString());
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAddVatPage(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        HttpSession session = request.getSession();
        String path = "";
        path = "content/stores/addVat.jsp";
        ArrayList userFunctions = (ArrayList)session.getAttribute("userFunction");
        try
        {
            if(!loginBP.checkAuthorisation(userFunctions, "Mfr-Add"))
            {
                path = "content/common/NotAuthorized.jsp";
            } else
            {
                String menuPath = "Stores  >>  VAT >>  Add";
                request.setAttribute("menuPath", menuPath);
                String pageTitle = "Add VAT";
                request.setAttribute("pageTitle", pageTitle);
            }
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to retrieve Designation data --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAddVat(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        HttpSession session = request.getSession();
        String path = "";
        int userId = ((Integer)session.getAttribute("userId")).intValue();
        try
        {
            String menuPath = "Stores  >> VAT >>  View VAT";
            request.setAttribute("menuPath", menuPath);
            ArrayList userFunctions = (ArrayList)session.getAttribute("userFunction");
            PurchaseTO purchTO = new PurchaseTO();
            if(command.getVat() != null && command.getVat() != "")
                purchTO.setVat(command.getVat());
            if(command.getEffectiveDate() != null && command.getEffectiveDate() != "")
                purchTO.setEffectiveDate(command.getEffectiveDate());
            if(command.getDesc() != null && command.getDesc() != "")
                purchTO.setDesc(command.getDesc());
            String pageTitle = "View VAT";
            request.setAttribute("pageTitle", pageTitle);
            int insertStatus = 0;
            path = "content/stores/alterVat.jsp";
            insertStatus = purchaseBP.processInsertVat(purchTO, userId);
            request.removeAttribute("vatList");
            ArrayList vatList = new ArrayList();
            vatList = purchaseBP.processVatList();
            request.setAttribute("vatList", vatList);
            request.setAttribute("successMessage", "Vat Added Successfully");
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to retrieve acad data --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAlterVatPage(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        String path = "";
        path = "content/stores/alterVat.jsp";
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList)session.getAttribute("userFunction");
        try
        {
            if(!loginBP.checkAuthorisation(userFunctions, "Mpr-MPRApprovalList"))
            {
                path = "content/common/NotAuthorized.jsp";
            } else
            {
                String menuPath = "Stores  >>  VAT >>  Alter";
                ArrayList vatList = new ArrayList();
                vatList = purchaseBP.processVatList();
                request.setAttribute("menuPath", menuPath);
                String pageTitle = "Alter VAT";
                request.setAttribute("vatList", vatList);
                request.setAttribute("pageTitle", pageTitle);
            }
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to retrieve acad data --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAlterVat(HttpServletRequest request, HttpServletResponse reponse, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        HttpSession session = request.getSession();
        String path = "";
        try
        {
            String menuPath = "  Stores  >>  VAT >>  View VAT ";
            request.setAttribute("menuPath", menuPath);
            int index = 0;
            int modify = 0;
            PurchaseCommand purchaseCommand = command;
            String vatIds[] = request.getParameterValues("vatIds");
            String vats[] = request.getParameterValues("vats");
            String descs[] = request.getParameterValues("descs");
            String effectiveDate[] = request.getParameterValues("effectiveDate");
            String activeStatus[] = request.getParameterValues("activeInds");
            String selectedIndex[] = request.getParameterValues("selectedIndex");
            int UserId = ((Integer)session.getAttribute("userId")).intValue();
            ArrayList List = new ArrayList();
            PurchaseTO purchTO = null;
            for(int i = 0; i < selectedIndex.length; i++)
            {
                purchTO = new PurchaseTO();
                index = Integer.parseInt(selectedIndex[i]);
                purchTO.setVatId(vatIds[index]);
                purchTO.setVat(vats[index]);
                purchTO.setDesc(descs[index]);
                purchTO.setEffectiveDate(effectiveDate[index]);
                purchTO.setActiveInd(activeStatus[index]);
                List.add(purchTO);
            }

            String pageTitle = "View VAT";
            path = "content/stores/alterVat.jsp";
            request.setAttribute("pageTitle", pageTitle);
            modify = purchaseBP.processModifyVatDetails(List, UserId);
            request.removeAttribute("vatList");
            ArrayList vatList = new ArrayList();
            vatList = purchaseBP.processVatList();
            request.setAttribute("vatList", vatList);
            request.setAttribute("successMessage", "VAT Details Modified Successfully");
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to retrieve designation --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handlePurhcaseorderItems(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        String path = "";
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList)session.getAttribute("userFunction");
        ArrayList vendorList = new ArrayList();
        try
        {
            if(!loginBP.checkAuthorisation(userFunctions, "Mpr-MPRApprovalList"))
            {
                path = "content/common/NotAuthorized.jsp";
            } else
            {
                String menuPath = "Stores  >>  Purchase Order >>  Modify PO";
                ArrayList poItems = new ArrayList();
                poItems = purchaseBP.getPurchaseorderItems(Integer.parseInt(request.getParameter("poId")));
                if(poItems.size() != 0)
                {
                    path = "content/stores/modifyPO.jsp";
                } else
                {
                    path = "content/stores/getPo.jsp";
                    request.setAttribute("errorMessage", "Purchase order not found");
                }
                request.setAttribute("menuPath", menuPath);
                vendorList = purchaseBP.processVendorList(1011);
                String pageTitle = "Modify PO";
                request.setAttribute("poItems", poItems);
                request.setAttribute("pageTitle", pageTitle);
                request.setAttribute("vendorId", command.getVendorId());
                request.setAttribute("vendorList", vendorList);
            }
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to retrieve handlePurhcaseorderItems--> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleModifyPo(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        String path = "";
        path = "content/stores/modifyPO.jsp";
        ModelAndView mv = new ModelAndView();
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList)session.getAttribute("userFunction");
        try
        {
            if(!loginBP.checkAuthorisation(userFunctions, "Mpr-MPRApprovalList"))
            {
                path = "content/common/NotAuthorized.jsp";
            } else
            {
                String menuPath = "Stores  >>  Purchase Order >>  Modify PO";
                ArrayList poItems = new ArrayList();
                PurchaseTO purch = null;
                String itemId[] = request.getParameterValues("itemId");
                String qty[] = request.getParameterValues("qty");
                String price[] = request.getParameterValues("price");
                String unitPrice[] = request.getParameterValues("unitPrice");
                String discount[] = request.getParameterValues("discount");
                String tax[] = request.getParameterValues("tax");
                purch = new PurchaseTO();
                purch.setVendorId(Integer.parseInt(command.getVendorId()));
                purch.setRemarks(command.getRemarks());
                purch.setReqDate(command.getReqDate());
                purch.setMprId(Integer.parseInt(command.getMprId()));
                purch.setPoId(Integer.parseInt(command.getPoId()));
                purchaseBP.modifyPOAndMprHeader(purch);
                for(int i = 0; i < itemId.length; i++)
                    if(!itemId[i].equals("") && itemId[i] != null)
                    {
                        purch = new PurchaseTO();
                        purch.setItemId(Integer.parseInt(itemId[i]));
                        purch.setReqQty(qty[i]);
                        purch.setMprId(Integer.parseInt(command.getMprId()));
                        purch.setPoId(Integer.parseInt(command.getPoId()));
                        purch.setBuyPrice(price[i]);
                        purch.setUnitPrice(unitPrice[i]);
                        purch.setDiscount(discount[i]);
                        
                        purch.setTax(tax[i]);
                        System.out.println("price:"+price[i]+":"+"unitprice:"+unitPrice[i]+":");
                        System.out.println("discount:"+discount[i]+":"+"tax:"+tax[i]+":");
                        System.out.println((new StringBuilder()).append("itemId").append(itemId[i]).toString());
                        System.out.println((new StringBuilder()).append("qty=").append(qty[i]).toString());
                        System.out.println((new StringBuilder()).append("command.getMprId()=").append(command.getMprId()).toString());
                        System.out.println((new StringBuilder()).append("command.getPoId()=").append(command.getPoId()).toString());
                        purchaseBP.doPoModify(purch);
                    }

                request.setAttribute("menuPath", menuPath);
                String pageTitle = "Modify PO";
                request.setAttribute("poItems", poItems);
                request.setAttribute("pageTitle", pageTitle);
                mv = handlePoDetail(request, response, command);
            }
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to retrieve handlePurhcaseorderItems--> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView handleDeletePoItems(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        String path = "";
        path = "content/stores/modifyPO.jsp";
        ModelAndView mv = new ModelAndView();
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList)session.getAttribute("userFunction");
        try
        {
            if(!loginBP.checkAuthorisation(userFunctions, "Mpr-MPRApprovalList"))
            {
                path = "content/common/NotAuthorized.jsp";
            } else
            {
                String menuPath = "Stores  >>  Purchase Order >>  Modify PO";
                ArrayList poItems = new ArrayList();
                PurchaseTO purch = null;
                String itemId[] = request.getParameterValues("itemId");
                String deleteInd[] = request.getParameterValues("deleteInd");
                purch = new PurchaseTO();
                purch.setVendorId(Integer.parseInt(command.getVendorId()));
                purch.setMprId(Integer.parseInt(command.getMprId()));
                purch.setPoId(Integer.parseInt(command.getPoId()));
                int index = 0;
                for(int i = 0; i < deleteInd.length; i++)
                {
                    index = Integer.parseInt(deleteInd[i]);
                    purch = new PurchaseTO();
                    purch.setItemId(Integer.parseInt(itemId[index]));
                    purch.setPoId(Integer.parseInt(command.getPoId()));
                    System.out.println((new StringBuilder()).append("itemId").append(itemId[index]).toString());
                    System.out.println((new StringBuilder()).append("command.getPoId()=").append(command.getPoId()).toString());
                    purchaseBP.deletePoItems(purch);
                }

                request.setAttribute("menuPath", menuPath);
                String pageTitle = "Modify PO";
                request.setAttribute("poItems", poItems);
                request.setAttribute("pageTitle", pageTitle);
                mv = handlePoDetail(request, response, command);
            }
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to retrieve handlePurhcaseorderItems--> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView handleShowGrn(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        System.out.println("handleGrnDetailsFromBill");
        String path = "";
        ArrayList supplyList = new ArrayList();
        purchaseCommand = command;
        PurchaseTO supplyTO = new PurchaseTO();
        String menuPath = "";
        int status = 0;
        String pageTitle = "Modify GRN";
        menuPath = "Stores  >>Modify GRN";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        try
        {
            supplyTO.setSupplyId(command.getSupplyId());
            supplyList = purchaseBP.processGrnDetails(supplyTO);
            if(supplyList.size() == 0)
            {
                path = "content/stores/getGrn1.jsp";
                request.setAttribute("errorMessage", "Invoice Not Found");
            } else
            {
                path = "content/stores/showGRN.jsp";
            }
            request.setAttribute("supplyList", supplyList);
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to viewItemList --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAlterGrnHeaderInfoPage(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        System.out.println("handleGrnDetailsFromBill");
        String path = "";
        ArrayList supplyList = new ArrayList();
        purchaseCommand = command;
        PurchaseTO supplyTO = new PurchaseTO();
        String menuPath = "";
        int status = 0;
        String pageTitle = "Modify GRN";
        menuPath = "Stores  >>Modify GRN";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        try
        {
            System.out.println((new StringBuilder()).append("grnId servlet = ").append(request.getParameter("supplyId")).toString());
            if(request.getParameter("supplyId") != null)
                supplyTO.setSupplyId(request.getParameter("supplyId"));
            else
                supplyTO.setSupplyId(String.valueOf(0));
            supplyList = purchaseBP.processGrnDetails(supplyTO);
            if(supplyList.size() == 0)
            {
                path = "content/stores/getGrn1.jsp";
                request.setAttribute("errorMessage", "Invoice Not Found");
            } else
            {
                path = "content/stores/modifyGRNHeader.jsp";
            }
            request.setAttribute("supplyList", supplyList);
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to viewItemList --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAlterGrnHeaderInfo(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        String path = "";
        path = "content/stores/getGrn1.jsp";
        ModelAndView mv = new ModelAndView();
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList)session.getAttribute("userFunction");
        try
        {
            if(!loginBP.checkAuthorisation(userFunctions, "Mpr-MPRApprovalList"))
            {
                path = "content/common/NotAuthorized.jsp";
            } else
            {
                String menuPath = "Stores  >>  Purchase Order >>  Modify PO";
                PurchaseTO purch = null;
                purch = new PurchaseTO();
                purch.setInVoiceNumber(command.getInVoiceNumber());
                purch.setDcNumber(command.getDcNumber());
                purch.setInVoiceDate(command.getInVoiceDate());
                purch.setRemarks(command.getRemarks());
                purch.setSupplyId(command.getSupplyId());
                purchaseBP.alterGrnHeaderInfo(purch);
                request.setAttribute("menuPath", menuPath);
                request.setAttribute("successMessage", "GRN altered successfully");
                String pageTitle = "Modify Invoice";
                request.setAttribute("pageTitle", pageTitle);
                mv = handleAlterGrnHeaderInfoPage(request, response, command);
            }
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to retrieve handlePurhcaseorderItems--> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView handleVendorPaymentPage(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        String path = "";
        path = "content/vendor/vendorPayment.jsp";
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList)session.getAttribute("userFunction");
        ArrayList vendorList = new ArrayList();
        try
        {
            if(!loginBP.checkAuthorisation(userFunctions, "Mpr-MPRApprovalList"))
            {
                path = "content/common/NotAuthorized.jsp";
            } else
            {
                String menuPath = "Manage Vendor >> Vendor Payments";
                vendorList = purchaseBP.processVendorList(0);
                request.setAttribute("menuPath", menuPath);
                String pageTitle = "Vendor Payment Details";
                request.setAttribute("pageTitle", pageTitle);
                request.setAttribute("vendorList", vendorList);
            }
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to retrieve handleVendorPayments--> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVendorPaymentDetails(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        String path = "";
        path = "content/vendor/vendorPayment.jsp";
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList)session.getAttribute("userFunction");
        ArrayList invoiceList = new ArrayList();
        ArrayList vendorList = new ArrayList();
        try
        {
            if(!loginBP.checkAuthorisation(userFunctions, "Mpr-MPRApprovalList"))
            {
                path = "content/common/NotAuthorized.jsp";
            } else
            {
                String menuPath = "Manage Vendor >> Vendor Payments";
                PurchaseTO purch = null;
                purch = new PurchaseTO();
                purch.setVendorId(Integer.parseInt(command.getVendorId()));
                purch.setFromDate(command.getFromDate());
                purch.setToDate(command.getToDate());
                invoiceList = purchaseBP.getVendorPayments(purch);
                vendorList = purchaseBP.processVendorList(0);
                request.setAttribute("menuPath", menuPath);
                String pageTitle = "Vendor Payment Details";
                request.setAttribute("pageTitle", pageTitle);
                request.setAttribute("invoiceList", invoiceList);
                request.setAttribute("vendorList", vendorList);
                request.setAttribute("fromDate", command.getFromDate());
                request.setAttribute("toDate", command.getToDate());
                request.setAttribute("vendorId", command.getVendorId());
            }
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to retrieve handleVendorPayments--> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAddVendorPayments(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command)
    {
        if(request.getSession().isNew())
            return new ModelAndView("content/common/login.jsp");
        ModelAndView mv = new ModelAndView();
        String path = "";
        path = "content/vendor/vendorPayment.jsp";
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList)session.getAttribute("userFunction");
        ArrayList invoiceList = new ArrayList();
        ArrayList vendorList = new ArrayList();
        PurchaseTO purch = null;
        int index = 0;
        try
        {
            if(!loginBP.checkAuthorisation(userFunctions, "Mpr-MPRApprovalList"))
            {
                path = "content/common/NotAuthorized.jsp";
            } else
            {
                String menuPath = "Manage Vendor >> Vendor Payments";
                String vendorId = command.getVendorId();
                String grnIds[] = command.getGrnIds();
                String orderType[] = command.getOrderTypes();
                String payDates[] = command.getPayDates();
                String invoiceAmnt[] = command.getInvoiceAmounts();
                String paidAmnt[] = command.getPaidAmounts();
                String remarks[] = command.getRemarkss();
                String paymentId[] = command.getPaymentIds();
                String selectedInd[] = command.getSelectedInd();
                for(int i = 0; i < selectedInd.length; i++)
                {
                    index = Integer.parseInt(selectedInd[i]);
                    purch = new PurchaseTO();
                    purch.setSupplyId(grnIds[index]);
                    purch.setPaymentDate(payDates[index]);
                    purch.setInVoiceAmount(invoiceAmnt[index]);
                    purch.setPaidAmnt(paidAmnt[index]);
                    purch.setRemarks(remarks[index]);
                    purch.setOrderType(orderType[index]);
                    purch.setVendorId(Integer.parseInt(vendorId));
                    purch.setPaymentId(paymentId[index]);
                    System.out.println((new StringBuilder()).append("paymentId=").append(paymentId[index]).toString());
                    if(paymentId[index].equals("0"))
                    {
                        purchaseBP.addPaymentDetails(purch);
                        continue;
                    }
                    if(!paymentId[index].equals("0"))
                        purchaseBP.updatePaymentDetails(purch);
                }

                request.setAttribute("menuPath", menuPath);
                String pageTitle = "Vendor Payment Details";
                request.setAttribute("pageTitle", pageTitle);
                mv = handleVendorPaymentDetails(request, response, command);
            }
        }
        catch(FPRuntimeException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        catch(FPBusinessException exception)
        {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to retrieve handleVendorPayments--> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    PurchaseCommand purchaseCommand;
    PurchaseBP purchaseBP;
    LoginBP loginBP;
    MrsBP mrsBP;
    SectionBP sectionBP;
    VehicleBP vehicleBP;
}
