/*---------------------------------------------------------------------------
 * ServiceCommand.java
 * Mar 5, 2009
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
-------------------------------------------------------------------------*/
package ets.domain.stockTransfer.web;

/****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver              Date                      Author                    Change
 * ---------------------------------------------------------------------------
 * 1.0           Mar 3, 2009              vijay			       Created
 *
 ******************************************************************************/
public class StockTransferCommand {

    private String companyId = "";
    private String companyName = "";
     private String servicePtName = "";
    private String approvedQty = "";
    private String requiredDate = "";
    private String requestId = "";
    private String itemName = "";
    private String itemId = "";
    private String mfrCode = "";
    private String rcQty = "";
    private String paplCode = "";
    private String requestedQty = "";
    private String approvalStatus = "";
    private String remarks = "";
    private String requestPtRcItem = "";
    private String requestPtNewItem = "";
    private String approvePtRcItem = "";
    private String approvePtNewItem = "";
    private String requestPtId = "";
    private String servicePtId = "";
    private String otherStockReq = "";
    private String price = "";
    private String priceId = "";
    private String approvedId = "";   
    private String userId = "";
    private String amount = "";
    private String issuedQty="";
    private String acceptedQty="";
    private String gdId = "";
    private String[] itemIds = null;
     private String[] prices = null;
     private String[] rcQtys = null;
    private String[] requestedQtys = null;
    private String[] approvedQtys = null;
    private String[] selectedIndex = null;
    private String[] issuedQtys=null; 
     private String[] arrLength = null;
   // private String[] priceIds = null;
    private String[] acceptedQtys=null;
    
    
    //receive rc wo
    
    private String vendorId="";
    private String woId="";
    private String rcItemId="";
    private String rcStatus="";
     private String rcPrice="";
    private String[] itemAmounts = null;
    //private String[] rcItemIds = null;
    // private String[] rcPrices = null;
      private String[] rcItemIds = null;
     private String[] priceIds = null;
     private String[] rcStatuses=null;
     private String[] categoryId=null;
     private String[] tyreId=null;
     private String[] itemType=null;
     private String[] tyreNo=null;
     private String[] taxs=null;
     private String[] priceTypes=null;
    
    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getApprovedQty() {
        return approvedQty;
    }

    public void setApprovedQty(String approvedQty) {
        this.approvedQty = approvedQty;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getMfrCode() {
        return mfrCode;
    }

    public void setMfrCode(String mfrCode) {
        this.mfrCode = mfrCode;
    }

    public String getPaplCode() {
        return paplCode;
    }

    public void setPaplCode(String paplCode) {
        this.paplCode = paplCode;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequiredDate() {
        return requiredDate;
    }

    public void setRequiredDate(String requiredDate) {
        this.requiredDate = requiredDate;
    }

    public String getRequestedQty() {
        return requestedQty;
    }

    public void setRequestedQty(String requestedQty) {
        this.requestedQty = requestedQty;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String[] getApprovedQtys() {
        return approvedQtys;
    }

    public void setApprovedQtys(String[] approvedQtys) {
        this.approvedQtys = approvedQtys;
    }

    public String[] getItemIds() {
        return itemIds;
    }

    public void setItemIds(String[] itemIds) {
        this.itemIds = itemIds;
    }

    public String[] getRequestedQtys() {
        return requestedQtys;
    }

    public void setRequestedQtys(String[] requestedQtys) {
        this.requestedQtys = requestedQtys;
    }

    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String[] selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public String getApprovePtNewItem() {
        return approvePtNewItem;
    }

    public void setApprovePtNewItem(String approvePtNewItem) {
        this.approvePtNewItem = approvePtNewItem;
    }

    public String getApprovePtRcItem() {
        return approvePtRcItem;
    }

    public void setApprovePtRcItem(String approvePtRcItem) {
        this.approvePtRcItem = approvePtRcItem;
    }

    public String getRequestPtNewItem() {
        return requestPtNewItem;
    }

    public void setRequestPtNewItem(String requestPtNewItem) {
        this.requestPtNewItem = requestPtNewItem;
    }

    public String getRequestPtRcItem() {
        return requestPtRcItem;
    }

    public void setRequestPtRcItem(String requestPtRcItem) {
        this.requestPtRcItem = requestPtRcItem;
    }

    public String getRequestPtId() {
        return requestPtId;
    }

    public void setRequestPtId(String requestPtId) {
        this.requestPtId = requestPtId;
    }

    public String getOtherStockReq() {
        return otherStockReq;
    }

    public void setOtherStockReq(String otherStockReq) {
        this.otherStockReq = otherStockReq;
    }

    public String getApprovedId() {
        return approvedId;
    }

    public void setApprovedId(String approvedId) {
        this.approvedId = approvedId;
    }

    public String[] getIssuedQtys() {
        return issuedQtys;
    }

    public void setIssuedQtys(String[] issuedQtys) {
        this.issuedQtys = issuedQtys;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPriceId() {
        return priceId;
    }

    public void setPriceId(String priceId) {
        this.priceId = priceId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

   

  
    public String getIssuedQty() {
        return issuedQty;
    }

    public void setIssuedQty(String issuedQty) {
        this.issuedQty = issuedQty;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getGdId() {
        return gdId;
    }

    public void setGdId(String gdId) {
        this.gdId = gdId;
    }

    public String getAcceptedQty() {
        return acceptedQty;
    }

    public void setAcceptedQty(String acceptedQty) {
        this.acceptedQty = acceptedQty;
    }

    public String[] getAcceptedQtys() {
        return acceptedQtys;
    }

    public void setAcceptedQtys(String[] acceptedQtys) {
        this.acceptedQtys = acceptedQtys;
    }

    public String getServicePtId() {
        return servicePtId;
    }

    public void setServicePtId(String servicePtId) {
        this.servicePtId = servicePtId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getWoId() {
        return woId;
    }

    public void setWoId(String woId) {
        this.woId = woId;
    }

    public String getRcItemId() {
        return rcItemId;
    }

    public void setRcItemId(String rcItemId) {
        this.rcItemId = rcItemId;
    }

    public String[] getItemAmounts() {
        return itemAmounts;
    }

    public void setItemAmounts(String[] itemAmounts) {
        this.itemAmounts = itemAmounts;
    }

    public String[] getPriceIds() {
        return priceIds;
    }

    public void setPriceIds(String[] priceIds) {
        this.priceIds = priceIds;
    }

    public String[] getRcItemIds() {
        return rcItemIds;
    }

    public void setRcItemIds(String[] rcItemIds) {
        this.rcItemIds = rcItemIds;
    }

   

   

    public String[] getRcStatuses() {
        return rcStatuses;
    }

    public void setRcStatuses(String[] rcStatuses) {
        this.rcStatuses = rcStatuses;
    }

    public String getRcStatus() {
        return rcStatus;
    }

    public void setRcStatus(String rcStatus) {
        this.rcStatus = rcStatus;
    }

    public String getServicePtName() {
        return servicePtName;
    }

    public void setServicePtName(String servicePtName) {
        this.servicePtName = servicePtName;
    }

    public String getRcPrice() {
        return rcPrice;
    }

    public void setRcPrice(String rcPrice) {
        this.rcPrice = rcPrice;
    }

    public String[] getArrLength() {
        return arrLength;
    }

    public void setArrLength(String[] arrLength) {
        this.arrLength = arrLength;
    }

    public String[] getPrices() {
        return prices;
    }

    public void setPrices(String[] prices) {
        this.prices = prices;
    }

    public String getRcQty() {
        return rcQty;
    }

    public void setRcQty(String rcQty) {
        this.rcQty = rcQty;
    }

    public String[] getRcQtys() {
        return rcQtys;
    }

    public void setRcQtys(String[] rcQtys) {
        this.rcQtys = rcQtys;
    }

    public String[] getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String[] categoryId) {
        this.categoryId = categoryId;
    }

    public String[] getTyreId() {
        return tyreId;
    }

    public void setTyreId(String[] tyreId) {
        this.tyreId = tyreId;
    }

    public String[] getItemType() {
        return itemType;
    }

    public void setItemType(String[] itemType) {
        this.itemType = itemType;
    }

    public String[] getTyreNo() {
        return tyreNo;
    }

    public void setTyreNo(String[] tyreNo) {
        this.tyreNo = tyreNo;
    }

    public String[] getTaxs() {
        return taxs;
    }

    public void setTaxs(String[] taxs) {
        this.taxs = taxs;
    }

    public String[] getPriceTypes() {
        return priceTypes;
    }

    public void setPriceTypes(String[] priceTypes) {
        this.priceTypes = priceTypes;
    }

   
    
}
