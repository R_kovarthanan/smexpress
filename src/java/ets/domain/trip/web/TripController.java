/*-------------------------------------------------------------------------
 * __NAME__.java
 * __DATE__
 *
 * Copyright (c) Entitle.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Entitle ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Entitle.
 -------------------------------------------------------------------------*/
package ets.domain.trip.web; 

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.web.BaseController;
import ets.domain.trip.business.TripBP;
import ets.domain.trip.business.TripTO;
import ets.domain.programmingfree.excelexamples.CreateBarChartExample;
import ets.domain.operation.business.OperationBP;
import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;
import com.oreilly.servlet.multipart.Part;
import ets.arch.util.SendMail;
import ets.domain.trip.web.TripCommand;
import java.io.File;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import ets.domain.vehicle.business.VehicleBP;
import ets.domain.company.business.CompanyBP;
import ets.domain.users.business.LoginBP;
import ets.domain.util.ParveenErrorConstants;
import ets.domain.util.FPLogUtils;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.json.JSONArray;
import org.json.JSONObject;

import jxl.Workbook;
import jxl.*;
import jxl.WorkbookSettings;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;
import com.oreilly.servlet.multipart.Part;
import ets.arch.util.sendInvoice;
import ets.domain.operation.business.OperationTO;
import ets.domain.finance.business.FinanceBP;
import ets.domain.util.ThrottleConstants;

/**
 * ****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver Date Author Change
 * ----------------------------------------------------------------------------
 * 1.0 __DATE__ Your_Name ,Entitle Created
 *
 *****************************************************************************
 */
public class TripController extends BaseController {

    /**
     * Creates a new instance of __NAME__
     */
    public TripController() {
    }
    TripCommand tripCommand;
    TripBP tripBP;
    //   LeaveBP leaveBP;
    LoginBP loginBP;
    CompanyBP companyBP;
    VehicleBP vehicleBP;
    OperationBP operationBP;
    FinanceBP financeBP;

    public FinanceBP getFinanceBP() {
        return financeBP;
    }

    public void setFinanceBP(FinanceBP financeBP) {
        this.financeBP = financeBP;
    }

    public OperationBP getOperationBP() {
        return operationBP;
    }

    public void setOperationBP(OperationBP operationBP) {
        this.operationBP = operationBP;
    }

    public VehicleBP getVehicleBP() {
        return vehicleBP;
    }

    public void setVehicleBP(VehicleBP vehicleBP) {
        this.vehicleBP = vehicleBP;
    }

    public CompanyBP getCompanyBP() {
        return companyBP;
    }

    public void setCompanyBP(CompanyBP companyBP) {
        this.companyBP = companyBP;
    }

    public TripCommand getTripCommand() {
        return tripCommand;
    }

    public void setTripCommand(TripCommand tripCommand) {
        this.tripCommand = tripCommand;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public TripBP getTripBP() {
        return tripBP;
    }

    public void setTripBP(TripBP tripBP) {
        this.tripBP = tripBP;
    }

    /**
     * This method used to bind the request values to the command object.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();
        initialize(request);
    }

    /**
     * This method used to View Desgintion Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView createTripSheet(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Operation  >>  Trip Planning ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Create Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            if (command.getConsignmentOrderNos() != null && command.getConsignmentOrderNos() != "") {
                tripTO.setConsignmentOrderNos(command.getConsignmentOrderNos());
            }

            ArrayList consignmentList = new ArrayList();
            ArrayList consignmentList1 = new ArrayList();
            ArrayList consignmentList2 = new ArrayList();
            consignmentList = tripBP.getConsignmentList(tripTO);

            ArrayList requestedLHC = tripBP.getRequestedLHC(tripTO);
            request.setAttribute("LHCStatus", requestedLHC);
            System.out.println("requestedLHC :" + requestedLHC.size());

            String vehicleTypeId = "";
            //calculate revenue if possible
            //check if vehicle type id is given in consignment order for revenue calculation
            Iterator itr = consignmentList.iterator();
            TripTO tripTONew = null;
            TripTO trpTO = null;
            float totalRevenue = 0.00F;
            String revenue = "0";
            float totalExpense = 0.00F;
            String expense = "0";
            System.out.println(" consignmentList.size()1111" + consignmentList.size());
            if (consignmentList.size() > 1) {
                while (itr.hasNext()) {
                    tripTONew = new TripTO();
                    tripTONew = (TripTO) itr.next();
                    String billingTypeId = tripTONew.getBillingId();
                    String reeferRequired = tripTONew.getReeferRequired();
                    String totalWeight = tripTONew.getTotalWeight();
                    String customerType = tripTONew.getCustomerTypeId();
//                  tripTONew.setOrderRevenue(revenue);
                    consignmentList1.add(tripTONew);
                }
                System.out.println(" 1111111111111111111111" + consignmentList.size());
                System.out.println(" consignmentList1" + consignmentList.size());
            } else {
                while (itr.hasNext()) {
                    tripTONew = new TripTO();
                    tripTONew = (TripTO) itr.next();
                    String billingTypeId = tripTONew.getBillingId();
                    String reeferRequired = tripTONew.getReeferRequired();
                    String totalWeight = tripTONew.getTotalWeight();
                    String customerType = tripTONew.getCustomerTypeId();
//                    revenue = tripBP.getConsignmentOrderRevenue(tripTONew.getOrderId(), billingTypeId, vehicleTypeId, reeferRequired, customerType);
//                    System.out.println("oiiiii" + revenue);
//                    if (customerType.equals("2")) {
//                        revenue = revenue;
//                    } else {
//                        if (billingTypeId.equals("2")) {
//                            if (revenue != null) {
//                                String[] temp = revenue.split("-");
//                                if (reeferRequired.equals("No")) {
//                                    System.out.println(" rebenooooon" + revenue);
//                                    if (Float.parseFloat(totalWeight) <= Float.parseFloat(temp[2])) {
//                                        revenue = Float.parseFloat(totalWeight) * Float.parseFloat(temp[2]) + "";
//                                    } else {
//                                        revenue = Float.parseFloat(totalWeight) * Float.parseFloat(temp[1]) + "";
//
//                                    }
//                                } else {
//                                    if (Float.parseFloat(totalWeight) <= Float.parseFloat(temp[2])) {
//                                        revenue = Float.parseFloat(totalWeight) * Float.parseFloat(temp[2]) + "";
//                                    } else {
//                                        revenue = Float.parseFloat(totalWeight) * Float.parseFloat(temp[0]) + "";
//
//                                    }
//                                    System.out.println(" yesssss" + revenue);
//                                }
//                            }
//                        }
//                    }
//                    System.out.println(" reben" + revenue);
//                    if (revenue == null || "NA".equals(revenue)) {
//                        revenue = "0";
//                    }
//                    tripTONew.setOrderRevenue(revenue);
                    consignmentList1.add(tripTONew);
//                    totalRevenue = totalRevenue + Float.parseFloat(revenue);
                }
            }
            if (consignmentList.size() > 1) {

                itr = consignmentList1.iterator();
                while (itr.hasNext()) {
                    tripTONew = new TripTO();
                    tripTONew = (TripTO) itr.next();
                    vehicleTypeId = tripTONew.getVehicleTypeId();
                    System.out.println("vehicleTypeId");
                    String customerTypeId = tripTONew.getCustomerTypeId();
                    expense = tripBP.getConsignmentOrderExpense(tripTONew.getOrderId(), tripTONew.getBillingId(), vehicleTypeId, tripTONew.getReeferRequired(), customerTypeId);
                    if (expense == null || "NA".equals(expense)) {
                        expense = "0";
                    }
                    tripTONew.setOrderExpense(expense);
                    consignmentList2.add(tripTONew);
                    totalExpense = totalExpense + Float.parseFloat(expense);
                    System.out.println("tripTONew.setOrderExpense" + tripTONew.getOrderExpense());
                }

// more than one consignment
                //route cost expense cannot be derived unless route plan is fixed
            } else { // single consingment is selected for trip planning
                itr = consignmentList1.iterator();
                while (itr.hasNext()) {
                    tripTONew = new TripTO();
                    tripTONew = (TripTO) itr.next();
                    vehicleTypeId = tripTONew.getVehicleTypeId();
                    System.out.println("vehicleTypeId");
                    String customerTypeId = tripTONew.getCustomerTypeId();
                    expense = tripBP.getConsignmentOrderExpense(tripTONew.getOrderId(), tripTONew.getBillingId(), vehicleTypeId, tripTONew.getReeferRequired(), customerTypeId);
                    if (expense == null || "NA".equals(expense)) {
                        expense = "0";
                    }
                    tripTONew.setOrderExpense(expense);
                    consignmentList2.add(tripTONew);
                    totalExpense = totalExpense + Float.parseFloat(expense);
                }

            }
            System.out.println("consignmentListconsignmentListconsignmentListconsignmentList7777" + consignmentList.size());
            System.out.println("consignmentListconsignmentListconsignmentListconsignmentList9999" + consignmentList1.size());
            System.out.println("consignmentListconsignmentListconsignmentListconsignmentList888" + consignmentList2.size());
            request.setAttribute("consignmentList", consignmentList);
            request.setAttribute("orderExpense", "0");

            String vehicleId = request.getParameter("vehicleId");
            String vehicleNo = request.getParameter("vehicleNo");
            String tripType = "";
            tripType = request.getParameter("tripType");
            if (tripType != null) {
                tripTO.setTripType(tripType);
                request.setAttribute("tripType", tripType);
            }
            String cbt = request.getParameter("cbt");
            if (cbt != null) {
                tripTO.setCbt(cbt);
                request.setAttribute("cbt", cbt);
            }
            if (vehicleId == null) {
                vehicleId = "";
            }
            String roleId = "" + (Integer) session.getAttribute("RoleId");
            String companyId = (String) session.getAttribute("companyId");
            tripTO.setRoleId(roleId);
            tripTO.setCompanyId(companyId);
            if (!"".equals(vehicleNo)) {
                tripTO.setVehicleNo(vehicleNo);
                tripTO.setVehicleId(vehicleId);
                ArrayList vehicleDetails = new ArrayList();
                vehicleDetails = tripBP.getVehicleRegNosForUpload(tripTO);
                System.out.println("vehicleDetails.size() = " + vehicleDetails.size());
                trpTO = new TripTO();
                if (vehicleDetails.size() > 0) {
                    Iterator itr1 = vehicleDetails.iterator();
                    while (itr1.hasNext()) {
                        trpTO = (TripTO) itr1.next();
                        System.out.println("trpTO.getVehicleId() = " + trpTO.getVehicleId());
                        System.out.println("trpTO.getVehicleNo() = " + trpTO.getVehicleNo());
                        request.setAttribute("vehicleId", trpTO.getVehicleId());
                        request.setAttribute("vehicleNo", trpTO.getVehicleNo());
                        request.setAttribute("vehicleTonnage", trpTO.getVehicleTonnage());
                        request.setAttribute("primaryDriverId", trpTO.getPrimaryDriverId());
                        request.setAttribute("primaryDriverName", trpTO.getPrimaryDriverName());
                        request.setAttribute("secondaryDriver1Id", trpTO.getSecondaryDriver1Id());
                        request.setAttribute("secondaryDriver1Name", trpTO.getSecondaryDriver1Name());
                        request.setAttribute("secondaryDriver2Id", trpTO.getSecondaryDriver2Id());
                        request.setAttribute("secondaryDriver2Name", trpTO.getSecondaryDriver2Name());
                    }
                }

            }
            String vehicleMileageAndTollRate = tripBP.getVehicleMileageAndTollRate(vehicleTypeId);
            ArrayList orderPointDetails = new ArrayList();
            orderPointDetails = tripBP.getOrderPointDetails(tripTO);

            ArrayList loadDetails = new ArrayList();
            loadDetails = tripBP.getLoadDetails(tripTO);
            request.setAttribute("loadDetails", loadDetails);

            request.setAttribute("vehicleMileageAndTollRate", vehicleMileageAndTollRate);
            request.setAttribute("orderPointDetails", orderPointDetails);

            //getVehicleRegNos
            roleId = "" + (Integer) session.getAttribute("RoleId");
            companyId = (String) session.getAttribute("companyId");
            tripTO.setRoleId(roleId);
            tripTO.setCompanyId(companyId);
            tripTO.setVehicleNo("");
            tripTO.setVehicleId("");
            ArrayList vehicleNos = tripBP.getVehicleRegNos(tripTO);
            System.out.println("vehicleNos.size() = " + vehicleNos.size());
            request.setAttribute("vehicleNos", vehicleNos);
            ArrayList statusList = tripBP.getStatusList(tripTO);
            System.out.println("tripStatus.size() = " + statusList.size());
            request.setAttribute("statusList", statusList);

            request.setAttribute("roleId", roleId);

            ArrayList vendorList = tripBP.getVendorList();
            System.out.println("vendorList.size() = " + vendorList.size());
            request.setAttribute("vendorList", vendorList);

            TripTO tripVendor = null;
            Iterator itrVen = vendorList.iterator();
            while (itrVen.hasNext()) {
                tripVendor = (TripTO) itrVen.next();
                System.out.println("tripVendor.getVendorName() = " + tripVendor.getVendorName());
            }

            path = "content/trip/createTripSheet.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void selectTripVehicle(HttpServletRequest request, HttpServletResponse response, TripCommand command) throws IOException {
        HttpSession session = request.getSession();
        tripCommand = command;
        TripTO tripTO = new TripTO();

        ArrayList vehicleNos = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String statusId = "";
            String tripType = "";
            String customerName = "";
            response.setContentType("text/html");
            statusId = request.getParameter("statusId");
            tripType = request.getParameter("tripType");
            customerName = request.getParameter("customerName");
            tripTO.setStatusId(statusId);
            tripTO.setTripType(tripType);
            tripTO.setCustomerName(customerName);
            String roleId = "" + (Integer) session.getAttribute("RoleId");
            String companyId = (String) session.getAttribute("companyId");
            tripTO.setRoleId(roleId);
            tripTO.setCompanyId(companyId);
            vehicleNos = tripBP.getTripVehicleNo(tripTO);
            System.out.println("vehicleNos.size() = " + vehicleNos.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = vehicleNos.iterator();
            int cntr = 0;
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                tripTO = (TripTO) itr.next();
                jsonObject.put("Name", tripTO.getVehicleNo());
                jsonObject.put("Id", tripTO.getVehicleId() + "-" + tripTO.getVehicleNo() + "-" + tripTO.getVehicleTonnage()
                        + "-" + tripTO.getPrimaryDriverId() + "-" + tripTO.getPrimaryDriverName()
                        + "-" + tripTO.getSecondaryDriver1Id() + "-" + tripTO.getSecondaryDriver1Name()
                        + "-" + tripTO.getSecondaryDriver2Id() + "-" + tripTO.getSecondaryDriver2Name()
                        + "-" + tripTO.getFcExpiryDate() + "-" + tripTO.getInsuranceExpiryDate()
                        + "-" + tripTO.getPermitExpiryDate() + "-" + tripTO.getRoadTaxExpiryDate());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
                cntr++;
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView viewTripSheetDetails(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Operation  >>  View TripSheet ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            String tripId = request.getParameter("tripId");
            tripTO.setTripId(tripId);
            tripTO.setTripSheetId(tripId);
            String vehicleId = request.getParameter("vehicleId");
            tripTO.setVehicleId(vehicleId);

            String roleId = "" + (Integer) session.getAttribute("RoleId");
            request.setAttribute("roleId", roleId);

            String tripSheetId = "";
            if (tripCommand != null) {
                tripSheetId = tripCommand.getTripSheetId();
            }

            if (tripSheetId != null && !tripSheetId.equals("")) {
                tripSheetId = request.getParameter("tripId");
                tripTO.setTripSheetId(tripSheetId);
            }

            String tripDetail = tripBP.getTripDetails(tripSheetId);
            ArrayList tripDetails = new ArrayList();
            tripDetails = tripBP.getTripDetails(tripTO);

            TripTO tripTO1 = new TripTO();
            String vehicleTypeId = "";
            ArrayList totalExpenseDetails = new ArrayList();
            totalExpenseDetails = tripBP.getTotalExpenseDetails(tripTO);
            Iterator itr1 = totalExpenseDetails.iterator();
            while (itr1.hasNext()) {
                tripTO1 = (TripTO) itr1.next();
                request.setAttribute("tollAmount", tripTO1.getTollAmount());
                request.setAttribute("driverBatta", tripTO1.getDriverBatta());
                request.setAttribute("driverIncentive", tripTO1.getDriverIncentive());
                request.setAttribute("fuelPrice", tripTO1.getFuelPrice());
                request.setAttribute("milleage", tripTO1.getMilleage());
                request.setAttribute("reeferConsumption", tripTO1.getReeferConsumption());
                request.setAttribute("vehicleTypeId", tripTO1.getVehicleTypeId());
                vehicleTypeId = tripTO1.getVehicleTypeId();
            }

            TripTO tpTO = new TripTO();
            ArrayList tripSheetDetails = new ArrayList();
            tripSheetDetails = tripBP.getEndTripSheetDetails(tripTO);
            Iterator itr = tripSheetDetails.iterator();
            while (itr.hasNext()) {
                tpTO = (TripTO) itr.next();
                request.setAttribute("tripSheetId", tpTO.getTripSheetId());
                request.setAttribute("cNotes", tpTO.getcNotes());
                request.setAttribute("vehicleNo", tpTO.getVehicleNo());
                request.setAttribute("tripCode", tpTO.getTripCode());
                request.setAttribute("startDate", tpTO.getTripScheduleDate());
                request.setAttribute("startTime", tpTO.getTripScheduleTime());
                request.setAttribute("estimatedExpense", tpTO.getOrderExpense());
                request.setAttribute("rcmExpense", tpTO.getRcmExpense());
                request.setAttribute("origin", tpTO.getOrigin());
                request.setAttribute("destination", tpTO.getDestination());
                request.setAttribute("tripEndDate", tpTO.getTripEndDate());
                request.setAttribute("tripEndTime", tpTO.getTripEndTime());
                request.setAttribute("startKm", tpTO.getStartKm());
                request.setAttribute("startHm", tpTO.getStartHm());
                request.setAttribute("endKm", tpTO.getEndKm());
                request.setAttribute("endHm", tpTO.getEndHm());
                request.setAttribute("runKm", tpTO.getRunKm());
                request.setAttribute("runHm", tpTO.getRunHm());
                request.setAttribute("totaldays", tpTO.getTotalDays());
                request.setAttribute("totalHours", tpTO.getDurationHours());
                request.setAttribute("driverCount", tpTO.getDriverCount());
                request.setAttribute("extraExpenseValue", tpTO.getExtraExpenseValue());
                String[] temp = null;
                if (!"".equals(tpTO.getDriverName()) && tpTO.getDriverName().contains(",")) {
                    temp = tpTO.getDriverName().split(",");
                    request.setAttribute("driverName1", temp[0]);
                    request.setAttribute("driverName2", temp[1]);
                } else {
                    request.setAttribute("driverName1", tpTO.getDriverName());
                }
                request.setAttribute("driverName", tpTO.getDriverName());
            }
            request.setAttribute("tripSheetDetails", tripSheetDetails);
            String gpsKm = tripBP.getGpsKm(tripSheetId);
            if (gpsKm != null) {
                String[] temp = gpsKm.split("-");
                String gpsKmVal = "0";
                if (!"0".equals(temp[0])) {
                    gpsKmVal = temp[0];
                }
                request.setAttribute("gpsKm", "" + gpsKmVal);
                //use above calculations
                request.setAttribute("gpsKm", gpsKmVal);
                request.setAttribute("gpsHm", temp[1]);
            } else {
                request.setAttribute("gpsKm", 0);
                request.setAttribute("gpsHm", 0);

            }
            System.out.println("gpsKm = " + request.getAttribute("gpsKm"));
            System.out.println("gpsHm = " + request.getAttribute("gpsHm"));
            //Throttle Starts As On 12-12-2013
            String miscValue = tripBP.getMiscValue(vehicleTypeId);
            request.setAttribute("miscValue", miscValue);

            String[] temp1 = null;
            if (tripDetail != null && !"".equals(tripDetail)) {
                temp1 = tripDetail.split("~");
                request.setAttribute("customerName", temp1[0]);
                request.setAttribute("routeName", temp1[1]);
                request.setAttribute("tripStartDate", temp1[2]);
                request.setAttribute("tripStartTime", temp1[3]);
            }
            String tripInformation = tripBP.getTripDetails(tripSheetId);
            String driverCount = tripBP.getDriverCount(tripSheetId, tripTO);
            String tripAdvance = tripBP.getTripAdvance(tripSheetId, tripTO);
            String tripExpense = tripBP.getTripExpense(tripSheetId, tripTO);
            request.setAttribute("tripAdvance", tripAdvance);
            request.setAttribute("tripExpense", tripExpense);

            String[] temp = null;
            if (tripInformation != null && !"".equals(tripInformation)) {
                temp = tripInformation.split("~");
                request.setAttribute("totalRunKm", temp[0]);
                request.setAttribute("transitDays", temp[1]);
                request.setAttribute("tripRemarks", temp[2]);
            }
            float advance = 0.0f;
            float expense = 0.0f;
            advance = Float.parseFloat(tripAdvance);
            expense = Float.parseFloat(tripExpense);
            float amountDifference = 0.0f;
            if (advance > expense) {
                amountDifference = advance - expense;
            } else {
                amountDifference = expense - advance;
            }
            request.setAttribute("driverCount", driverCount);
            request.setAttribute("tripAdvance", tripAdvance);
            request.setAttribute("tripExpense", tripExpense);
            request.setAttribute("amountDifference", amountDifference);
            ArrayList tripAdvanceDetails = new ArrayList();
            tripAdvanceDetails = tripBP.getTripAdvanceDetails(request.getParameter("tripId"));

            request.setAttribute("tripAdvanceDetails", tripAdvanceDetails);
            ArrayList tripFuelDetails = new ArrayList();
            tripFuelDetails = tripBP.getTripFuelDetails(request.getParameter("tripId"));
            request.setAttribute("tripFuelDetails", tripFuelDetails);
            ArrayList tripExpenseDetails = new ArrayList();
            tripExpenseDetails = tripBP.getTripExpenseDetails(request.getParameter("tripId"));
            request.setAttribute("tripExpenseDetails", tripExpenseDetails);
            ArrayList tripUnPackDetails = new ArrayList();
            tripUnPackDetails = tripBP.getTripUnPackDetails(tripTO);
            request.setAttribute("tripUnPackDetails", tripUnPackDetails);
            ArrayList viewPODDetails = new ArrayList();
            viewPODDetails = tripBP.getPODDetails(tripTO);
            if (viewPODDetails.size() > 0) {
                request.setAttribute("viewPODDetails", viewPODDetails);
            }

            ArrayList expiryDateDetails = new ArrayList();
            expiryDateDetails = tripBP.getExpiryDateDetails(tripTO);
            request.setAttribute("expiryDateDetails", expiryDateDetails);
            ArrayList statusDetails = new ArrayList();
            statusDetails = tripBP.getTripStausDetails(tripTO);
            if (statusDetails.size() > 0) {
                request.setAttribute("statusDetails", statusDetails);
            }
            ArrayList settlementDetails = new ArrayList();
            settlementDetails = tripBP.getDriverSettlementDetails(tripTO);
            if (settlementDetails.size() > 0) {
                request.setAttribute("settlementDetails", settlementDetails);
            }

            ArrayList tripDetailsNew = new ArrayList();
            tripDetailsNew = tripBP.getTripDetails(tripTO);
            ArrayList tripPointDetails = new ArrayList();
            ArrayList tripPreStartDetails = new ArrayList();
            ArrayList tripStartDetails = new ArrayList();
            ArrayList tripEndDetails = new ArrayList();
            tripPointDetails = tripBP.getTripPointDetails(tripTO);
            tripPreStartDetails = tripBP.getPreStartTripDetails(tripTO);
            tripStartDetails = tripBP.getStartedTripDetails(tripTO);
            tripEndDetails = tripBP.getEndTripDetails(tripTO);

            TripTO tTO = new TripTO();
            ArrayList bpclTransactionHistory = new ArrayList();
            double bpclAmount = 0.0f;
            bpclTransactionHistory = tripBP.getBPCLTransactionHistory(tripTO);
            if (bpclTransactionHistory.size() > 0) {
                request.setAttribute("bpclTransactionHistory", bpclTransactionHistory);
                Iterator itr3 = bpclTransactionHistory.iterator();
                while (itr3.hasNext()) {
                    tTO = (TripTO) itr3.next();
                    bpclAmount += (Double) Double.parseDouble(tTO.getAmount());
                }

            } else {
                bpclAmount = 0;
            }
            request.setAttribute("bpclAmount", bpclAmount);

            ArrayList tripAdvanceDetailsStatus = new ArrayList();
            tripAdvanceDetailsStatus = tripBP.getTripAdvanceDetailsStatus(request.getParameter("tripId"));

            request.setAttribute("tripAdvanceDetailsStatus", tripAdvanceDetailsStatus);

            request.setAttribute("tripDetails", tripDetails);
            request.setAttribute("tripPointDetails", tripPointDetails);
            request.setAttribute("tripPreStartDetails", tripPreStartDetails);
            request.setAttribute("tripStartDetails", tripStartDetails);
            request.setAttribute("tripEndDetails", tripEndDetails);
            request.setAttribute("tripDetails", tripDetailsNew);

            ArrayList vehicleChangeAdvanceDetails = new ArrayList();
            vehicleChangeAdvanceDetails = tripBP.getVehicleChangeTripAdvanceDetails(request.getParameter("tripId"));
            request.setAttribute("vehicleChangeAdvanceDetails", vehicleChangeAdvanceDetails);
            request.setAttribute("vehicleChangeAdvanceDetailsSize", vehicleChangeAdvanceDetails.size());
            String tripUnclearedBalance = "";
            tripUnclearedBalance = tripBP.getTripUnclearedBalance(tripTO);
            request.setAttribute("tripUnclearedBalance", tripUnclearedBalance);

            ArrayList loadDetails = new ArrayList();
            loadDetails = tripBP.getLoadDetails(tripTO);
            request.setAttribute("loadDetails", loadDetails);
            System.out.println("loadDetails++=" + loadDetails.size());

            path = "content/trip/viewTripSheetDetails.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView showinvoicedetail(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Finance  >>  Generate Bill ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            int userId = (Integer) session.getAttribute("userId");
            String invoiceId = request.getParameter("invoiceId");
            String tripId = request.getParameter("tripId");
            tripTO.setInvoiceId(invoiceId);
            tripTO.setTripSheetId(tripId);
            tripTO.setTripId(tripId);
            request.setAttribute("tripId", tripId);
            ArrayList courierDetails = tripBP.getCourierDetails(tripTO);
            TripTO tripTONew = new TripTO();
            if (courierDetails.size() > 0) {
                request.setAttribute("courierDetails", courierDetails);
                Iterator itr = courierDetails.iterator();
                while (itr.hasNext()) {
                    tripTONew = (TripTO) itr.next();
                    request.setAttribute("courierNo", tripTONew.getCourierNo());
                    request.setAttribute("courierRemarks", tripTONew.getCourierRemarks());

                }
            }
            int billSubmitStatus = tripBP.checkBillSubmittedStatus(tripTO, userId);
            request.setAttribute("billSubmitStatus", billSubmitStatus);

            ArrayList invoiceHeader = tripBP.getInvoiceHeader(tripTO);
            ArrayList invoiceDetails = tripBP.getInvoiceDetails(tripTO);
            ArrayList invoiceDetailExpenses = tripBP.getInvoiceDetailExpenses(tripTO);

            ArrayList viewPODDetails = new ArrayList();
            viewPODDetails = tripBP.getPODDetails(tripTO);
            request.setAttribute("viewPODDetails", viewPODDetails);

            ArrayList viewTempLogDetails = new ArrayList();
            viewTempLogDetails = tripBP.getTempLogDetails(tripTO);
            request.setAttribute("viewTempLogDetails", viewTempLogDetails);

            ArrayList bpclTransactionHistory = new ArrayList();
            bpclTransactionHistory = tripBP.getBPCLTransactionHistory(tripTO);
            request.setAttribute("bpclTransactionHistory", bpclTransactionHistory);

            request.setAttribute("invoiceHeader", invoiceHeader);
            request.setAttribute("invoiceDetails", invoiceDetails);
            if (invoiceDetailExpenses.size() > 0) {
                request.setAttribute("invoiceDetailExpenses", invoiceDetailExpenses);
            }

            path = "content/trip/billingInvoice.jsp";
            ArrayList tripDetails = new ArrayList();
            tripDetails = tripBP.getTripDetails(tripTO);

            request.setAttribute("tripDetails", tripDetails);
            /*
             ArrayList tripDetails = new ArrayList();
             ArrayList tripPointDetails = new ArrayList();
             tripDetails = tripBP.getTripDetails(tripTO);
             tripPointDetails = tripBP.getTripPointDetails(tripTO);

             request.setAttribute("tripDetails", tripDetails);
             request.setAttribute("tripPointDetails", tripPointDetails);
             path = "content/trip/viewTripSheetDetails.jsp";
             */
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView submitBillCourierDetails(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Finance  >>  Generate Bill ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            String invoiceId = request.getParameter("invoiceId");
            String tripId = request.getParameter("tripId");
            tripTO.setInvoiceId(invoiceId);
            tripTO.setTripSheetId(tripId);
            tripTO.setTripId(tripId);
            request.setAttribute("tripId", tripId);
            request.setAttribute("invoiceId", invoiceId);
            request.setAttribute("tripId", tripId);
            int userId = (Integer) session.getAttribute("userId");
            int tempGraphApprovedStatus = tripBP.checkTempGraphApprovedStatus(tripTO, userId);
            /////  raju
            if (tempGraphApprovedStatus == 0) {
                int status = operationBP.submitBill(invoiceId, userId);
            }
            int billSubmitStatus = tripBP.checkBillSubmittedStatus(tripTO, userId);
            request.setAttribute("billSubmitStatus", billSubmitStatus);

            ArrayList courierDetails = tripBP.getCourierDetails(tripTO);
            TripTO tripTONew = new TripTO();
            if (courierDetails.size() > 0) {
                request.setAttribute("courierDetails", courierDetails);
                Iterator itr = courierDetails.iterator();
                while (itr.hasNext()) {
                    tripTONew = (TripTO) itr.next();
                    request.setAttribute("courierNo", tripTONew.getCourierNo());
                    request.setAttribute("courierRemarks", tripTONew.getCourierRemarks());

                }
            }
            ArrayList invoiceHeader = tripBP.getInvoiceHeader(tripTO);
            ArrayList invoiceDetails = tripBP.getInvoiceDetails(tripTO);
            ArrayList invoiceDetailExpenses = tripBP.getInvoiceDetailExpenses(tripTO);

            ArrayList viewPODDetails = new ArrayList();
            viewPODDetails = tripBP.getPODDetails(tripTO);
            request.setAttribute("viewPODDetails", viewPODDetails);

            ArrayList viewTempLogDetails = new ArrayList();
            viewTempLogDetails = tripBP.getTempLogDetails(tripTO);
            request.setAttribute("viewTempLogDetails", viewTempLogDetails);

            ArrayList bpclTransactionHistory = new ArrayList();
            bpclTransactionHistory = tripBP.getBPCLTransactionHistory(tripTO);
            request.setAttribute("bpclTransactionHistory", bpclTransactionHistory);

            request.setAttribute("invoiceHeader", invoiceHeader);
            request.setAttribute("invoiceDetails", invoiceDetails);
            if (invoiceDetailExpenses.size() > 0) {
                request.setAttribute("invoiceDetailExpenses", invoiceDetailExpenses);
            }

            path = "content/trip/billingInvoice.jsp";
            ArrayList tripDetails = new ArrayList();
            tripDetails = tripBP.getTripDetails(tripTO);

            request.setAttribute("tripDetails", tripDetails);
            /*
             ArrayList tripDetails = new ArrayList();
             ArrayList tripPointDetails = new ArrayList();
             tripDetails = tripBP.getTripDetails(tripTO);
             tripPointDetails = tripBP.getTripPointDetails(tripTO);

             request.setAttribute("tripDetails", tripDetails);
             request.setAttribute("tripPointDetails", tripPointDetails);
             path = "content/trip/viewTripSheetDetails.jsp";
             */
            //}
        } catch (FPRuntimeException exception) {
            exception.printStackTrace();
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            exception.printStackTrace();
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveCourierDetails(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Finance  >>  Generate Bill ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            String invoiceId = request.getParameter("invoiceId");
            String tripId = request.getParameter("tripId");
            tripTO.setInvoiceId(invoiceId);
            tripTO.setTripSheetId(tripId);
            tripTO.setTripId(tripId);
            request.setAttribute("tripId", tripId);
            int userId = (Integer) session.getAttribute("userId");

            String courierNo = "";
            String courierRemarks = "";
            courierNo = request.getParameter("courierNo");
            courierRemarks = request.getParameter("courierRemarks");
            tripTO.setCourierNo(courierNo);
            tripTO.setCourierRemarks(courierRemarks);
            if (!"".equals(courierNo)) {
                int insertCourierDetails = tripBP.insertCourierDetails(tripTO, userId);
            }
            ArrayList courierDetails = tripBP.getCourierDetails(tripTO);
            TripTO tripTONew = new TripTO();
            if (courierDetails.size() > 0) {
                request.setAttribute("courierDetails", courierDetails);
                Iterator itr = courierDetails.iterator();
                while (itr.hasNext()) {
                    tripTONew = (TripTO) itr.next();
                    request.setAttribute("courierNo", tripTONew.getCourierNo());
                    request.setAttribute("courierRemarks", tripTONew.getCourierRemarks());

                }
            }
            int billSubmitStatus = tripBP.checkBillSubmittedStatus(tripTO, userId);
            request.setAttribute("billSubmitStatus", billSubmitStatus);
            ArrayList invoiceHeader = tripBP.getInvoiceHeader(tripTO);
            ArrayList invoiceDetails = tripBP.getInvoiceDetails(tripTO);
            ArrayList invoiceDetailExpenses = tripBP.getInvoiceDetailExpenses(tripTO);

            ArrayList viewPODDetails = new ArrayList();
            viewPODDetails = tripBP.getPODDetails(tripTO);
            request.setAttribute("viewPODDetails", viewPODDetails);

            ArrayList viewTempLogDetails = new ArrayList();
            viewTempLogDetails = tripBP.getTempLogDetails(tripTO);
            request.setAttribute("viewTempLogDetails", viewTempLogDetails);

            ArrayList bpclTransactionHistory = new ArrayList();
            bpclTransactionHistory = tripBP.getBPCLTransactionHistory(tripTO);
            request.setAttribute("bpclTransactionHistory", bpclTransactionHistory);

            request.setAttribute("invoiceHeader", invoiceHeader);
            request.setAttribute("invoiceDetails", invoiceDetails);
            if (invoiceDetailExpenses.size() > 0) {
                request.setAttribute("invoiceDetailExpenses", invoiceDetailExpenses);
            }

            path = "content/trip/billingInvoice.jsp";
            ArrayList tripDetails = new ArrayList();
            tripDetails = tripBP.getTripDetails(tripTO);

            request.setAttribute("tripDetails", tripDetails);
            /*
             ArrayList tripDetails = new ArrayList();
             ArrayList tripPointDetails = new ArrayList();
             tripDetails = tripBP.getTripDetails(tripTO);
             tripPointDetails = tripBP.getTripPointDetails(tripTO);

             request.setAttribute("tripDetails", tripDetails);
             request.setAttribute("tripPointDetails", tripPointDetails);
             path = "content/trip/viewTripSheetDetails.jsp";
             */
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView showGSTNo(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "Finance  >>  Generate Bill ";

        try {
            PrintWriter pw = response.getWriter();
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            String gstDetails = new String();
            gstDetails = tripBP.showGSTNo(tripTO);
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();

            if (gstDetails != null) {
                jsonObject.put("Name", gstDetails.split("~")[0]);
                jsonObject.put("Id", gstDetails.split("~")[0]);
            }
            jsonArray.put(jsonObject);
            pw.print(jsonArray);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView gererateTripBill(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Finance  >>  Generate Bill ";
        String fromDate = "";
        String toDate = "";
        try {
            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            tripTO.setFromDate(fromDate);
            tripTO.setToDate(toDate);

            ArrayList financeStateList = new ArrayList();
            financeStateList = tripBP.getFinanceStateList(tripTO);
            request.setAttribute("financeStateList", financeStateList);

            String[] tripIds = request.getParameterValues("tripId");
            String[] consignmentOrderIds = request.getParameterValues("selectedIndex");

            String InvoiceFlag = request.getParameter("cancelInvoiceFlag");
            System.out.println("InvoiceFlag : " + InvoiceFlag);
            request.setAttribute("InvoiceFlag", InvoiceFlag);
            String selectedCustomerId = request.getParameter("selectedCustomerId");
            String reeferRequired = request.getParameter("reeferRequired");

            tripTO.setInvoiceFlag(InvoiceFlag);
            tripTO.setTripIds(tripIds);
            tripTO.setConsignmentOrderIds(consignmentOrderIds);
            tripTO.setCustomerId(selectedCustomerId);
            ArrayList tripsToBeBilledDetails = tripBP.getTripsToBeBilledDetails(tripTO);
            ArrayList tripsOtherExpenseDetails = tripBP.getTripsOtherExpenseDetails(tripTO);

            ArrayList gstDetails = new ArrayList();
            gstDetails = tripBP.getGSTTaxPercentage(tripTO);

            ArrayList companyList = new ArrayList();
            companyList = tripBP.getCompanyDetailsList(tripTO);
            request.setAttribute("companyList", companyList);

            ArrayList customerList = new ArrayList();
            customerList = tripBP.getCustomerDetailsList(tripTO);
            request.setAttribute("customerList", customerList);

            Iterator itrGst = gstDetails.iterator();
            TripTO gstTO = new TripTO();
            while (itrGst.hasNext()) {
                gstTO = (TripTO) itrGst.next();
                if ("IGST".equalsIgnoreCase(gstTO.getGstName())) {
                    request.setAttribute("igst", gstTO.getGstPercentage());
                    System.out.println("IGST" + gstTO.getGstPercentage());
                }
                if ("SGST".equalsIgnoreCase(gstTO.getGstName())) {
                    request.setAttribute("sgst", gstTO.getGstPercentage());
                    System.out.println("SGST" + gstTO.getGstPercentage());
                }
                if ("CGST".equalsIgnoreCase(gstTO.getGstName())) {
                    request.setAttribute("cgst", gstTO.getGstPercentage());
                    System.out.println("CGST" + gstTO.getGstPercentage());
                }
            }

            request.setAttribute("tripsToBeBilledDetails", tripsToBeBilledDetails);
            request.setAttribute("tripsOtherExpenseDetails", tripsOtherExpenseDetails);

            if ("3".equals(InvoiceFlag) || "4".equals(InvoiceFlag) || "5".equals(InvoiceFlag)) {
                path = "content/trip/viewTripBillDetailsForTspRoyalty.jsp";
            } else {
                path = "content/trip/viewTripBillDetails.jsp";
            }
            ArrayList tripDetails = new ArrayList();
            tripDetails = tripBP.getTripDetails(tripTO);

            request.setAttribute("reeferRequired", reeferRequired);

            request.setAttribute("tripDetails", tripDetails);
            System.out.println("tripIds.length" + tripIds.length);

//            String filePath = "";
            //                    filePath = "/employee";
//            filePath = getServletContext().getRealPath("/uploadFiles/DataLogerXls");
            //                    filePath = "C:/EmployeePhotoCopies";
//            File fileSaveDir = new File(filePath);
//            if (!fileSaveDir.exists()) {
//                fileSaveDir.mkdir();
//            }
//            System.out.println("REAL filePath = " + fileSaveDir);
//            System.out.println("REAL filePath = " + filePath);
//            String filePath1 = filePath.replace("\\", "\\\\");
//            System.out.println("REAL filePath1 = " + filePath1);
//            for (int i = 0; i < tripIds.length; i++) {
//                String tripId = tripIds[i];
//                System.out.println("tripId.length" + tripId);
            //new CreateBarChartExample(tripId,filePath);
//                if (!"No".equals(reeferRequired)) {
//                    if (tripId != null) {
//                        System.out.println("tripID" + tripId);
//                        new CreateBarChartExample(tripId, filePath);
//                        filePath1 = filePath1 + "\\" + "dataLogExcel" + tripId + ".xls";
//                        System.out.println("filePath1 new file path= " + filePath1);
//                        int graphInsertStatus = 0;
//                        graphInsertStatus = tripBP.insertBillingGraph(tripId, filePath1, userId);
//                    }
//                }
//            }

            /*
             ArrayList tripDetails = new ArrayList();
             ArrayList tripPointDetails = new ArrayList();
             tripDetails = tripBP.getTripDetails(tripTO);
             tripPointDetails = tripBP.getTripPointDetails(tripTO);

             request.setAttribute("tripDetails", tripDetails);
             request.setAttribute("tripPointDetails", tripPointDetails);
             path = "content/trip/viewTripSheetDetails.jsp";
             */
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveTripBill(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        Map<String, ArrayList> printData = new HashMap<String, ArrayList>();
        String menuPath = "Finance  >>  Generate Bill ";
        ModelAndView mv = null;
        synchronized (this) {
            try {

                String invoiceNo = "";
                int invoiceId = 0;
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "View Trip Sheet";
                request.setAttribute("pageTitle", pageTitle);

                int userId = (Integer) session.getAttribute("userId");
                String[] tripIds = request.getParameterValues("tripId");
                String[] consignmentOrderIds = request.getParameterValues("consignmentOrderId");

                String customerId = request.getParameter("customerId");
                String noOfTrips = request.getParameter("noOfTrips");
                String billingId = request.getParameter("billingId");
                String companyIdNew = request.getParameter("companyIdNew");
                String invoiceType = request.getParameter("invoiceType");
                String InvoiceFlag = request.getParameter("InvoiceFlag");
                String invoiceDate = request.getParameter("invoiceDate");
                String startDate = request.getParameter("startDate");
                String compGSTNo = request.getParameter("compGSTNo");

                TripTO tripTO = new TripTO();

                tripTO.setTripIds(tripIds);
                tripTO.setConsignmentOrderIds(consignmentOrderIds);
                tripTO.setNoOfTrips(noOfTrips);
                tripTO.setBillingId(billingId);
                tripTO.setCompanyId(companyIdNew);
                tripTO.setUserId(userId);
                tripTO.setCustomerId(customerId);
                tripTO.setInvoiceDate(invoiceDate);
                tripTO.setInvoiceType(invoiceType);
                tripTO.setInvoiceFlag(InvoiceFlag);
                tripTO.setFromDate(startDate);
                tripTO.setGstNo(compGSTNo);

                String totalRevenue = request.getParameter("totalRevenue");
                String totalExpToBeBilled = request.getParameter("totalExpToBeBilled");
                String grandTotal = request.getParameter("grandTotal");

                String igst = request.getParameter("igst");
                String sgst = request.getParameter("sgst");
                String cgst = request.getParameter("cgst");
                String igstAmt = request.getParameter("totalExpIGSTAmt");
                String sgstAmt = request.getParameter("totalExpSGSTAmt");
                String cgstAmt = request.getParameter("totalExpCGSTAmt");
                String gstType = request.getParameter("gstType");

                if (gstType.equalsIgnoreCase("IGST")) {
                    sgst = "0";
                    cgst = "0";
                    cgstAmt = "0";
                    sgstAmt = "0";
                } else {
                    igst = "0";
                    igstAmt = "0";
                }

                tripTO.setIgst(igst);
                tripTO.setCgst(cgst);
                tripTO.setSgst(sgst);

                tripTO.setIgstAmt(igstAmt);
                tripTO.setCgstAmt(cgstAmt);
                tripTO.setSgstAmt(sgstAmt);

                tripTO.setTotalRevenue(totalRevenue);
                tripTO.setTotalExpToBeBilled(totalExpToBeBilled);
                tripTO.setGrandTotal(grandTotal);

                double freightRound = Double.parseDouble(totalRevenue);
                double grandRound = Double.parseDouble(grandTotal);
                double igstRound = Double.parseDouble(igstAmt);
                double cgstRound = Double.parseDouble(cgstAmt);
                double sgstRound = Double.parseDouble(sgstAmt);

                System.out.println("igstRound : " + igstRound);
                System.out.println("sgstRound : " + sgstRound);
                System.out.println("cgstRound : " + cgstRound);

                freightRound = Math.round(freightRound);
                grandRound = Math.round(grandRound);
                igstRound = Math.round(igstRound);
                cgstRound = Math.round(cgstRound);
                sgstRound = Math.round(sgstRound);

                tripTO.setFreightAmountRound(freightRound + "");
                tripTO.setGrandTotalRound(grandRound + "");
                tripTO.setCgstAmtRound(cgstRound + "");
                tripTO.setSgstAmtRound(sgstRound + "");
                tripTO.setIgstAmtRound(igstRound + "");

                invoiceId = tripBP.saveCustomerInvoice(tripTO);
                tripTO.setInvoiceId(invoiceId + "");

                request.setAttribute("invoiceNo", invoiceNo);
                request.setAttribute("invoiceId", invoiceId);

                if (invoiceId > 0) {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Invoice Created Successfully.");
                    path = "content/TripManagement/viewBillForSubmission.jsp";
                    mv = new ModelAndView("viewbillpageforsubmit.do?");
                } else {
                    path = "content/trip/invoicePrint.jsp";
                    mv = new ModelAndView("viewbillpageforsubmit.do?");
                }

            } catch (FPRuntimeException exception) {
                /*
                 * run time exception has occurred. Directed to error page.
                 */
                FPLogUtils.fpErrorLog("Run time exception --> " + exception);
                return new ModelAndView("content/common/error.jsp");
            } catch (FPBusinessException exception) {
                /*
                 * run time exception has occurred. Directed to error page.
                 */
                FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
                request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                        exception.getErrorMessage());
            } catch (Exception exception) {
                exception.printStackTrace();
                FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
                return new ModelAndView("content/common/error.jsp");
            }
        }
        return mv;
//        return new ModelAndView(path);
    }

    public ModelAndView saveTripBillold(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        Map<String, ArrayList> printData = new HashMap<String, ArrayList>();
        String menuPath = "Finance  >>  Generate Bill ";
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            String[] tripIds = request.getParameterValues("tripId");
            String[] consignmentOrderIds = request.getParameterValues("consignmentOrderId");
            String tripSheetId = request.getParameter("tripSheetId");
            tripTO.setTripIds(tripIds);
            tripTO.setConsignmentOrderIds(consignmentOrderIds);

            String billing = request.getParameter("billing");
            String customerId = request.getParameter("customerId");
            String totalRevenue = request.getParameter("totalRevenue");
            String totalExpToBeBilled = request.getParameter("totalExpToBeBilled");
            String grandTotal = request.getParameter("grandTotal");
            String noOfTrips = request.getParameter("noOfTrips");
            String billingId = request.getParameter("billingId");
            String companyIdNew = request.getParameter("companyIdNew");
            String goodsMovement = request.getParameter("goodsMovement");
            String remarks = request.getParameter("remarks");
            String fuelEstInvoiceFlag = request.getParameter("fuelEstInvoiceFlag");
            tripTO.setNoOfTrips(noOfTrips);
            tripTO.setBillingId(billingId);
            tripTO.setCompanyId(companyIdNew);
            tripTO.setGoodsMovement(goodsMovement);
            tripTO.setRemarks(remarks);
            tripTO.setFuelEstInvoiceFlag(fuelEstInvoiceFlag);
            tripTO.setUserId(userId);
            tripTO.setCustomerId(customerId);

            String invoiceType = request.getParameter("invoiceType");
            String invoiceTypeBill = request.getParameter("invoiceTypeBill");
            String invoiceTypeRemb = request.getParameter("invoiceTypeRemb");
            String gstLevy = request.getParameter("gstLevy");
            String gstLevyBill = request.getParameter("gstLevyBill");
            String gstLevyRemb = request.getParameter("gstLevyRemb");
//            sesha
            String fuelEscalation = request.getParameter("fuelEscalation");
            String fuelEstCharge = request.getParameter("fuelEstCharge");
            String fuelType = request.getParameter("fuelType");
            String fuelGstLevy = request.getParameter("fuelGstLevy");
            String fuelPlaceofSupply = request.getParameter("fuelPlaceofSupply");
            String fuelInvoiceNo = request.getParameter("fuelInvoiceNo");
            String fuelInvoiceDate = request.getParameter("fuelInvoiceDate");

            String placeofSupply = request.getParameter("placeofSupply");
            String placeofSupplyBill = request.getParameter("placeofSupplyBill");
            String placeofSupplyRemb = request.getParameter("placeofSupplyRemb");
            String invoiceNo = request.getParameter("invoiceNo");
            String invoiceDate = request.getParameter("invoiceDate");
            String invoiceNoBill = request.getParameter("invoiceNoBill");
            String invoiceNoRemb = request.getParameter("invoiceNoRemb");
            String invoiceDateRemb = request.getParameter("invoiceDateRemb");
            String invoiceDateBill = request.getParameter("invoiceDateBill");

            // cancellation
            String cancelInvoiceFlag = request.getParameter("cancelInvoiceFlag");
            String cancelInvoiceType = request.getParameter("cancelInvoiceType");
            String cancelGstLevy = request.getParameter("cancelGstLevy");
            String cancelPlaceofSupply = request.getParameter("cancelPlaceofSupply");
            String cancelInvoiceNo = request.getParameter("cancelInvoiceNo");
            String cancelInvoiceDate = request.getParameter("cancelInvoiceDate");
            String cancellationCharge = request.getParameter("cancellationCharge");
            String totalCancellationInvoiceAmt = request.getParameter("totalCancellationInvoiceAmt");
            // TSP 
            String tspInvoiceType = request.getParameter("tspInvoiceType");
            String tspGstLevy = request.getParameter("tspGstLevy");
            String tspPlaceofSupply = request.getParameter("tspPlaceofSupply");
            String tspInvoiceNo = request.getParameter("tspInvoiceNo");
            String tspInvoiceDate = request.getParameter("tspInvoiceDate");
            //Royalty
            String royaltyInvoiceType = request.getParameter("royaltyInvoiceType");
            String royaltyGstLevy = request.getParameter("royaltyGstLevy");
            String royaltyPlaceofSupply = request.getParameter("royaltyPlaceofSupply");
            String royaltyInvoiceNo = request.getParameter("royaltyInvoiceNo");
            String royaltyInvoiceDate = request.getParameter("royaltyInvoiceDate");

            String tspRoyaltyAmount = request.getParameter("totalExpToBeBilled");
            System.out.println("totalExpToBeBilled=" + tspRoyaltyAmount);
            String totalTspRoyaltyInvoiceAmt = request.getParameter("totalTspRoyaltyInvoiceAmt");
            System.out.println("totalTspRoyaltyInvoiceAmt=" + totalTspRoyaltyInvoiceAmt);

            tripTO.setCancelInvoiceFlag(cancelInvoiceFlag);
            tripTO.setCancelInvoiceType(cancelInvoiceType);
            tripTO.setCancelGstLevy(cancelGstLevy);
            tripTO.setCancelPlaceofSupply(cancelPlaceofSupply);
            tripTO.setCancelInvoiceNo(cancelInvoiceNo);
            tripTO.setCancelInvoiceDate(cancelInvoiceDate);
            tripTO.setCancellationCharge(cancellationCharge);
            tripTO.setTotalCancellationInvoiceAmt(totalCancellationInvoiceAmt);

            tripTO.setInvoiceDate(invoiceDate);
            tripTO.setInvoiceDateBill(invoiceDateBill);
            tripTO.setInvoiceDateRemb(invoiceDateRemb);
            tripTO.setInvoiceType(invoiceType);
            tripTO.setInvoiceTypeBill(invoiceTypeBill);
            tripTO.setInvoiceTypeRemb(invoiceTypeRemb);
            tripTO.setGstLevy(gstLevy);
            tripTO.setGstLevyBill(gstLevyBill);
            tripTO.setGstLevyRemb(gstLevyRemb);
            tripTO.setPlaceofSupply(placeofSupply);
            tripTO.setPlaceofSupplyBill(placeofSupplyBill);
            tripTO.setPlaceofSupplyRemb(placeofSupplyRemb);

            tripTO.setFuelEscalation(fuelEscalation);
            tripTO.setFuelEstCharge(fuelEstCharge);
            tripTO.setFuelType(fuelType);
            tripTO.setFuelGstLevy(fuelGstLevy);
            tripTO.setFuelPlaceofSupply(fuelPlaceofSupply);
            tripTO.setFuelInvoiceNo(fuelInvoiceNo);
            tripTO.setFuelInvoiceDate(fuelInvoiceDate);

            // TSP
            tripTO.setTspInvoiceType(tspInvoiceType);
            tripTO.setTspGstLevy(tspGstLevy);
            tripTO.setTspPlaceofSupply(tspPlaceofSupply);
            tripTO.setTspInvoiceNo(tspInvoiceNo);
            tripTO.setTspInvoiceDate(tspInvoiceDate);
            //Royalty
            tripTO.setRoyaltyInvoiceType(royaltyInvoiceType);
            tripTO.setRoyaltyGstLevy(royaltyGstLevy);
            tripTO.setRoyaltyPlaceofSupply(royaltyPlaceofSupply);
            tripTO.setRoyaltyInvoiceNo(royaltyInvoiceNo);
            tripTO.setRoyaltyInvoiceDate(royaltyInvoiceDate);

            tripTO.setTspRoyaltyAmount(tspRoyaltyAmount);
            tripTO.setTotalTspRoyaltyInvoiceAmt(totalTspRoyaltyInvoiceAmt);

            String InvoiceFlag = request.getParameter("InvoiceFlag");
            tripTO.setInvoiceFlag(InvoiceFlag);
            System.out.println("InvoiceFlag==" + InvoiceFlag);
            Calendar calendar = Calendar.getInstance();
            int curyear = (int) calendar.get(Calendar.YEAR);
            String currentyear = curyear + "";
            currentyear = currentyear.substring(2);
            int nextyear = Integer.parseInt(currentyear) + 1;
            String accYear = currentyear + "-" + nextyear;

            int invoiceId = 0;

            if ("3".equals(InvoiceFlag) || "5".equals(InvoiceFlag)) { //TSP

                String invoiceCode = "INV/" + accYear + "/";
                String invoiceCodeSequence = tripBP.getInvoiceCodeSequence();
                invoiceCode = invoiceCode + invoiceCodeSequence;
                tripTO.setInvoiceCode(invoiceCode);
                tripTO.setInvoiceNo(invoiceCode);

                ArrayList tripsToBeBilledDetails = tripBP.getTripsToBeBilledDetails(tripTO);
                ArrayList tripsOtherExpenseDetails = new ArrayList();

                //savebillheader
                tripTO.setTotalRevenue(request.getParameter("totalRevenue"));
                tripTO.setTotalExpToBeBilled(request.getParameter("totalExpToBeBilled"));
                tripTO.setGrandTotal(request.getParameter("totalFreightInvoiceAmt"));
                tripTO.setTaxValue(request.getParameter("totalTspRoyaltyTaxAmt"));

                System.out.println("request.getParameter(\"totalRevenue\")=========" + request.getParameter("totalRevenue"));
                System.out.println("request.getParameter(\"totalExpToBeBilled\")=========" + request.getParameter("totalExpToBeBilled"));
                System.out.println("request.getParameter(\"totalFreightInvoiceAmt\")=========" + request.getParameter("totalFreightInvoiceAmt"));
                System.out.println("request.getParameter(\"totalTspRoyaltyTaxAmt\")=========" + request.getParameter("totalTspRoyaltyTaxAmt"));

                invoiceId = tripBP.saveBillHeaderTSP(tripTO);
                int invoiceDetailId = 0;
                int status = 0;
                if (invoiceId > 0) {
                    Iterator itr = tripsToBeBilledDetails.iterator();
                    TripTO tripTONew = null;

                    while (itr.hasNext()) {
                        tripTONew = new TripTO();
                        tripTONew = (TripTO) itr.next();
                        tripTONew.setInvoiceId("" + invoiceId);
                        tripTONew.setInvoiceCode(invoiceCode);

                        invoiceDetailId = tripBP.saveBillDetailsTSP(tripTONew);
                        tripTO.setExpenseType("6");
                        tripsOtherExpenseDetails = tripBP.getTripsOtherExpenseDetails(tripTO);

                        Iterator itr1 = tripsOtherExpenseDetails.iterator();
                        TripTO tripTONew1 = null;

                        while (itr1.hasNext()) {
                            tripTONew1 = new TripTO();
                            tripTONew1 = (TripTO) itr1.next();
                            tripTONew1.setTripId(tripTONew.getTripId());
                            tripTONew1.setInvoiceId("" + invoiceId);
                            tripTONew1.setInvoiceDetailId("" + invoiceDetailId);
                            tripTONew1.setInvoiceCode(invoiceCode);

//                            status = tripBP.saveBillDetailExpense(tripTONew1);
                        }
                        tripTONew.setTripSheetId(tripTONew.getTripId());
//                        tripTONew.setStatusId("1");
                        tripTONew.setInvoiceFlag(InvoiceFlag);
                        tripTONew.setConsignmentOrderIds(consignmentOrderIds);
                        status = tripBP.updateTSPinvoiceStatus(tripTONew, userId);
                        //update trip status to billed**********************************************************************

                    }
                }

                TripTO tspRoyaltyTax = new TripTO();
                tspRoyaltyTax.setInvoiceId(invoiceId + "");
                tspRoyaltyTax.setIgstValue(request.getParameter("totalTspRoyaltyIGSTAmt"));
                tspRoyaltyTax.setCgstValue(request.getParameter("totalTspRoyaltyCGSTAmt"));
                tspRoyaltyTax.setSgstValue(request.getParameter("totalTspRoyaltySGSTAmt"));
                tspRoyaltyTax.setIgstPercentage(request.getParameter("igst"));
                tspRoyaltyTax.setSgstPercentage(request.getParameter("sgst"));
                tspRoyaltyTax.setCgstPercentage(request.getParameter("cgst"));
                int tspTaxTaxUpdate = tripBP.updateInvoiceTspTax(tspRoyaltyTax);
                System.out.println("tspTaxTaxUpdate=" + tspTaxTaxUpdate);

            } else if ("4".equals(InvoiceFlag)) {// Royalty
                String invoiceCode = "INV/" + accYear + "/";
                String invoiceCodeSequence = tripBP.getInvoiceCodeSequence();
                invoiceCode = invoiceCode + invoiceCodeSequence;
                tripTO.setInvoiceCode(invoiceNo);
                tripTO.setInvoiceNo(invoiceNo);

                ArrayList tripsToBeBilledDetails = tripBP.getTripsToBeBilledDetails(tripTO);
                ArrayList tripsOtherExpenseDetails = new ArrayList();

                //savebillheader
                tripTO.setTotalRevenue(request.getParameter("totalRevenue"));
                tripTO.setTotalExpToBeBilled(request.getParameter("totalExpToBeBilled"));
                tripTO.setGrandTotal(request.getParameter("totalFreightInvoiceAmt"));
                tripTO.setTaxValue(request.getParameter("totalTspRoyaltyTaxAmt"));

                int invoiceId2 = tripBP.saveBillHeaderRoyalty(tripTO);
                int invoiceDetailId = 0;
                int status = 0;
                if (invoiceId2 > 0) {
                    Iterator itr = tripsToBeBilledDetails.iterator();
                    TripTO tripTONew = null;

                    while (itr.hasNext()) {
                        tripTONew = new TripTO();
                        tripTONew = (TripTO) itr.next();
                        tripTONew.setInvoiceId("" + invoiceId2);

                        invoiceDetailId = tripBP.saveBillDetailsRoyalty(tripTONew);

                        tripTONew.setTripSheetId(tripTONew.getTripId());
                        tripTONew.setStatusId("1");
                        tripTONew.setConsignmentOrderIds(consignmentOrderIds);
                        status = tripBP.updateRoyaltyStatus(tripTONew, userId);
                    }
                }
                TripTO tspRoyaltyTax = new TripTO();
                tspRoyaltyTax.setInvoiceId(invoiceId2 + "");
                tspRoyaltyTax.setIgstValue(request.getParameter("totalTspRoyaltyIGSTAmt"));
                tspRoyaltyTax.setCgstValue(request.getParameter("totalTspRoyaltyCGSTAmt"));
                tspRoyaltyTax.setSgstValue(request.getParameter("totalTspRoyaltySGSTAmt"));
                tspRoyaltyTax.setIgstPercentage(request.getParameter("igst"));
                tspRoyaltyTax.setSgstPercentage(request.getParameter("sgst"));
                tspRoyaltyTax.setCgstPercentage(request.getParameter("cgst"));
                int tspTaxTaxUpdate = tripBP.updateInvoiceRoyaltyTax(tspRoyaltyTax);
                System.out.println("cancellationTaxUpdate=" + tspRoyaltyTax);

            } else {

                //generate tripcode
                if ("1".equals(request.getParameter("billWithFreight"))) {
                    String invoiceCode = "INV/" + accYear + "/";
                    String invoiceCodeSequence = tripBP.getInvoiceCodeSequence();
                    invoiceCode = invoiceCode + invoiceCodeSequence;
                    tripTO.setInvoiceCode(invoiceCode);
                    tripTO.setInvoiceNo(invoiceCode);

                    ArrayList tripsToBeBilledDetails = tripBP.getTripsToBeBilledDetails(tripTO);
                    ArrayList tripsOtherExpenseDetails = new ArrayList();

                    //savebillheader
                    tripTO.setTotalRevenue(request.getParameter("totalRevenue"));
                    tripTO.setTotalExpToBeBilled(request.getParameter("totalOtherExpesneForFreightAmt"));
                    tripTO.setGrandTotal(request.getParameter("totalFreightInvoiceAmt"));
                    invoiceId = tripBP.saveBillHeader(tripTO);
                    int invoiceDetailId = 0;
                    int status = 0;
                    if (invoiceId > 0) {
                        Iterator itr = tripsToBeBilledDetails.iterator();
                        TripTO tripTONew = null;

                        while (itr.hasNext()) {
                            tripTONew = new TripTO();
                            tripTONew = (TripTO) itr.next();
                            tripTONew.setInvoiceId("" + invoiceId);
                            tripTONew.setInvoiceCode(invoiceCode);
                            // Tax Insert
                            tripTONew.setCgstValue(request.getParameter("gstTaxExpAmountCGST"));
                            tripTONew.setSgstValue(request.getParameter("gstTaxExpAmountSGST"));
                            tripTONew.setIgstValue(request.getParameter("gstTaxExpAmountIGST"));
                            tripTONew.setIgstPercentage(request.getParameter("igst"));
                            tripTONew.setSgstPercentage(request.getParameter("sgst"));
                            tripTONew.setCgstPercentage(request.getParameter("cgst"));
                            int generalTaxUpdate = tripBP.updateGeneralTax(tripTONew);
                            System.out.println("generalTaxUpdate=" + generalTaxUpdate);

                            invoiceDetailId = tripBP.saveBillDetails(tripTONew);
                            tripTO.setExpenseType("3");
                            tripsOtherExpenseDetails = tripBP.getTripsOtherExpenseDetails(tripTO);

                            Iterator itr1 = tripsOtherExpenseDetails.iterator();
                            TripTO tripTONew1 = null;

                            while (itr1.hasNext()) {
                                tripTONew1 = new TripTO();
                                tripTONew1 = (TripTO) itr1.next();
                                tripTONew1.setTripId(tripTONew.getTripId());
                                tripTONew1.setInvoiceId("" + invoiceId);
                                tripTONew1.setInvoiceDetailId("" + invoiceDetailId);
                                tripTONew1.setInvoiceCode(invoiceCode);

                                status = tripBP.saveBillDetailExpense(tripTONew1);

                            }
                            tripTONew.setTripSheetId(tripTONew.getTripId());
                            tripTONew.setStatusId("16");
                            tripTONew.setConsignmentOrderIds(consignmentOrderIds);
//                            status = tripBP.updateStatus(tripTONew, userId);
                            if (!"4".equals(cancelInvoiceFlag)) {
                                status = tripBP.updateStatusForInvoice(tripTONew, userId);

                            }
                            //update trip status to billed**********************************************************************

                            ////////////////////// Invoice Web service Part //////////////////////////////////////////////////////
                            String eFSId = tripBP.getCustomereFSId(customerId);

                            double invoice_amount = Double.parseDouble(request.getParameter("totalFreightInvoiceAmt"));

                            double cgstAmt = Double.parseDouble(request.getParameter("gstTaxExpAmountCGST"));
                            double sgstAmt = Double.parseDouble(request.getParameter("gstTaxExpAmountSGST"));
                            double igstAmt = Double.parseDouble(request.getParameter("gstTaxExpAmountIGST"));

                            double net_amount = invoice_amount + cgstAmt + sgstAmt + igstAmt;
                            double totalTaxAmt = cgstAmt + sgstAmt + igstAmt;

                            String igstPer = request.getParameter("igst");
                            String sgstPer = request.getParameter("sgst");
                            String cgstPer = request.getParameter("cgst");
                            String compGSTNo = request.getParameter("compGSTNo");

                            System.out.println("cgstAmt : " + cgstAmt + " per:" + cgstPer);
                            System.out.println("sgstAmt : " + sgstAmt + " per:" + sgstPer);
                            System.out.println("igstAmt : " + igstAmt + " per:" + igstPer);

                            //    new sendInvoice(eFSId, invoiceNo, invoiceDate, invoice_amount + "", totalTaxAmt + "", cgstPer, cgstAmt + "", sgstPer, sgstAmt + "", igstPer, igstAmt + "", net_amount + "", compGSTNo).start();
                        }
                    }
                }

                tripTO.setInvoiceId(invoiceId + "");
                System.out.println(" request.getParameter(rembType " + request.getParameter("rembType"));
                System.out.println(" request.getParameter(billToCustomerType" + request.getParameter("billToCustomerType"));
                System.out.println(" request.getParameter(billWithFreight" + request.getParameter("billWithFreight"));
                if ("1".equals(request.getParameter("billToCustomerType"))) {
                    String invoiceCode = "INVBILL/17-18/";
                    String invoiceCodeSequence = tripBP.getInvoiceCodeSequenceBill();
                    invoiceCode = invoiceCode + invoiceCodeSequence;
                    tripTO.setInvoiceCode(invoiceNoBill);
                    tripTO.setInvoiceNo(invoiceNoBill);

                    ArrayList tripsToBeBilledDetails = tripBP.getTripsToBeBilledDetails(tripTO);
                    ArrayList tripsOtherExpenseDetails = new ArrayList();

                    //savebillheader
                    tripTO.setTotalRevenue(request.getParameter("totalExpInvoiceAmt"));
                    tripTO.setTotalExpToBeBilled("0");
                    tripTO.setGrandTotal(request.getParameter("totalExpenseInvoiceAmt"));
                    tripTO.setTaxValue(request.getParameter("totalExpTaxAmt"));

                    int invoiceId1 = tripBP.saveBillHeaderBill(tripTO);
                    int invoiceDetailId = 0;
                    int status = 0;
                    if (invoiceId > 0) {
                        Iterator itr = tripsToBeBilledDetails.iterator();
                        TripTO tripTONew = null;

                        while (itr.hasNext()) {
                            tripTONew = new TripTO();
                            tripTONew = (TripTO) itr.next();
                            tripTONew.setInvoiceId("" + invoiceId);
                            tripTONew.setInvoiceCode(invoiceCode);
                            invoiceDetailId = tripBP.saveBillDetailsBill(tripTONew);
                            tripTO.setExpenseType("1");
                            tripsOtherExpenseDetails = tripBP.getTripsOtherExpenseDetails(tripTO);

                            Iterator itr1 = tripsOtherExpenseDetails.iterator();
                            TripTO tripTONew1 = null;

                            while (itr1.hasNext()) {
                                tripTONew1 = new TripTO();
                                tripTONew1 = (TripTO) itr1.next();
                                tripTONew1.setTripId(tripTONew.getTripId());
                                tripTONew1.setInvoiceId("" + invoiceId1);
                                tripTONew1.setInvoiceDetailId("" + invoiceDetailId);
                                tripTONew1.setInvoiceCode(invoiceCode);

                                status = tripBP.saveBillDetailExpenseBill(tripTONew1);

                            }

                            TripTO billTax = new TripTO();
                            billTax.setInvoiceId(invoiceId + "");
                            billTax.setIgstValue(request.getParameter("totalExpIGSTAmt"));
                            billTax.setCgstValue(request.getParameter("totalExpCGSTAmt"));
                            billTax.setSgstValue(request.getParameter("totalExpSGSTAmt"));
                            billTax.setIgstPercentage(request.getParameter("igst"));
                            billTax.setSgstPercentage(request.getParameter("sgst"));
                            billTax.setCgstPercentage(request.getParameter("cgst"));
                            int taxUpdate = tripBP.updateInvoiceBillTax(billTax);
                            tripTONew.setTripSheetId(tripTONew.getTripId());
                            tripTONew.setStatusId("16");
                            tripTONew.setConsignmentOrderIds(consignmentOrderIds);
                            status = tripBP.updateStatusForInvoice(tripTONew, userId);
                            //update trip status to billed**********************************************************************

                        }
                    }
                }

                tripTO.setInvoiceId(invoiceId + "");
                if ("1".equals(request.getParameter("rembType"))) {
                    String invoiceCode = "INVREMB/17-18/";
                    String invoiceCodeSequence = tripBP.getInvoiceCodeSequenceRemb();
                    invoiceCode = invoiceCode + invoiceCodeSequence;
                    tripTO.setInvoiceCode(invoiceNoRemb);
                    tripTO.setInvoiceNo(invoiceNoRemb);

                    ArrayList tripsToBeBilledDetails = tripBP.getTripsToBeBilledDetails(tripTO);
                    ArrayList tripsOtherExpenseDetails = new ArrayList();

                    //savebillheader
                    //savebillheader
                    tripTO.setTotalRevenue(request.getParameter("totalReimbursInvoiceAmt"));
                    tripTO.setTotalExpToBeBilled("0");
                    tripTO.setGrandTotal(request.getParameter("totalReimbusInvoiceAmt"));
                    tripTO.setTaxValue(request.getParameter("totalReimbusTaxAmt"));

                    int invoiceId2 = tripBP.saveBillHeaderRemb(tripTO);
                    int invoiceDetailId = 0;
                    int status = 0;
                    if (invoiceId2 > 0) {
                        Iterator itr = tripsToBeBilledDetails.iterator();
                        TripTO tripTONew = null;

                        while (itr.hasNext()) {
                            tripTONew = new TripTO();
                            tripTONew = (TripTO) itr.next();
                            tripTONew.setInvoiceId("" + invoiceId);
                            tripTONew.setInvoiceCode(invoiceCode);
                            invoiceDetailId = tripBP.saveBillDetailsRemb(tripTONew);
                            tripTO.setExpenseType("4");
                            tripsOtherExpenseDetails = tripBP.getTripsOtherExpenseDetails(tripTO);

                            Iterator itr1 = tripsOtherExpenseDetails.iterator();
                            TripTO tripTONew1 = null;

                            while (itr1.hasNext()) {
                                tripTONew1 = new TripTO();
                                tripTONew1 = (TripTO) itr1.next();
                                tripTONew1.setTripId(tripTONew.getTripId());
                                tripTONew1.setInvoiceId("" + invoiceId);
                                tripTONew1.setInvoiceDetailId("" + invoiceDetailId);
                                tripTONew1.setInvoiceCode(invoiceCode);

                                status = tripBP.saveBillDetailExpenseRemb(tripTONew1);

                            }
                            TripTO reimbusTax = new TripTO();
                            reimbusTax.setInvoiceId(invoiceId + "");
                            reimbusTax.setIgstValue(request.getParameter("totalReimbursIGSTAmt"));
                            reimbusTax.setCgstValue(request.getParameter("totalReimbursCGSTAmt"));
                            reimbusTax.setSgstValue(request.getParameter("totalReimbursSGSTAmt"));
                            reimbusTax.setIgstPercentage(request.getParameter("igst"));
                            reimbusTax.setSgstPercentage(request.getParameter("sgst"));
                            reimbusTax.setCgstPercentage(request.getParameter("cgst"));
                            int reimbusTaxUpdate = tripBP.updateInvoiceReimbusTax(reimbusTax);
                            tripTONew.setTripSheetId(tripTONew.getTripId());
                            tripTONew.setStatusId("16");
                            tripTONew.setConsignmentOrderIds(consignmentOrderIds);
                            status = tripBP.updateStatusForInvoice(tripTONew, userId);
                            //update trip status to billed**********************************************************************

                        }
                    }
                }
                // cancellation Invoice

                if ("4".equals(cancelInvoiceFlag)) {
                    System.out.println("test");
                    ArrayList tripsToBeBilledDetails = tripBP.getTripsToBeBilledDetails(tripTO);
                    System.out.println("tripsToBeBilledDetailscancel===" + tripsToBeBilledDetails.size());
                    ArrayList tripsOtherExpenseDetails = new ArrayList();

                    //savebillheader
                    //savebillheader
                    tripTO.setTotalRevenue(request.getParameter("cancellationCharge"));
                    tripTO.setTotalExpToBeBilled("0");
                    tripTO.setGrandTotal(request.getParameter("totalCancellationInvoiceAmt"));
                    tripTO.setTaxValue(request.getParameter("totalCancellationTaxAmt"));

                    int invoiceId2 = tripBP.saveBillHeaderCancellation(tripTO);
                    int invoiceDetailId = 0;
                    int status = 0;
                    if (invoiceId2 > 0) {
                        Iterator itr = tripsToBeBilledDetails.iterator();
                        TripTO tripTONew = null;

                        while (itr.hasNext()) {
                            tripTONew = new TripTO();
                            tripTONew = (TripTO) itr.next();
                            tripTONew.setInvoiceId("" + invoiceId);

                            invoiceDetailId = tripBP.saveBillDetailsCancellation(tripTONew);
                            System.out.println("invoiceDetailId====" + invoiceDetailId);
                            tripTONew.setTripSheetId(tripTONew.getTripId());
                            tripTONew.setStatusId("16");
                            tripTONew.setConsignmentOrderIds(consignmentOrderIds);
                            status = tripBP.updateCancellationStatus(tripTONew, userId);
                            System.out.println("status=" + status);
                        }
                    }
                    TripTO cancellationTax = new TripTO();
                    cancellationTax.setInvoiceId(invoiceId + "");
                    cancellationTax.setIgstValue(request.getParameter("totalCancellationIGSTAmt"));
                    cancellationTax.setCgstValue(request.getParameter("totalCancellationCGSTAmt"));
                    cancellationTax.setSgstValue(request.getParameter("totalCancellationSGSTAmt"));
                    cancellationTax.setIgstPercentage(request.getParameter("igst"));
                    cancellationTax.setSgstPercentage(request.getParameter("sgst"));
                    cancellationTax.setCgstPercentage(request.getParameter("cgst"));
                    int cancellationTaxUpdate = tripBP.updateInvoiceCancellationTax(cancellationTax);
                    System.out.println("cancellationTaxUpdate=" + cancellationTaxUpdate);
                }
            }

            String reeferRequired = request.getParameter("reeferRequired");
            System.out.println("reeferRequired is thkjshrnkjgnskg" + reeferRequired);
            if (!"No".equals(reeferRequired)) {

                String reeferHour = request.getParameter("reeferHour");
                System.out.println("reeferHour is thkjshrnkjgnskg" + reeferHour);
                String startDate1 = request.getParameter("startDate");
                System.out.println("startDate is thkjshrnkjgnskg" + startDate1);
                String temp[] = startDate1.split("-");
                String startDate = temp[2] + "-" + temp[1] + "-" + temp[0];
                System.out.println("startDate2342 is thkjshrnkjgnskg" + startDate);
                String startTime = request.getParameter("startTime");
                System.out.println("startTime is thkjshrnkjgnskg" + startTime);
                String startDateTime = startDate + " " + startTime;
                System.out.println("startDateTime is thkjshrnkjgnskg" + startDateTime);
                String tripTransitHours = request.getParameter("tripTransitHours");
                System.out.println("tripTransitHours is thkjshrnkjgnskg" + tripTransitHours);
                String tripId = request.getParameter("tripId");

                tripTO.setStartDate(startDateTime);
                tripTO.setTripId(tripId);
                tripTO.setTripTransitHours(tripTransitHours);
                tripTO.setTotalHrs(reeferHour);
                ArrayList vehicleLogDetails = new ArrayList();
//                vehicleLogDetails = tripBP.getVehicleLogDetails(tripTO);

                request.setAttribute("vehicleLogDetails", vehicleLogDetails);

                String filePath = "";
                //                    filePath = "/employee";
                filePath = getServletContext().getRealPath("/uploadFiles/DataLogerXls");
                //                    filePath = "C:/EmployeePhotoCopies";
                File fileSaveDir = new File(filePath);
                if (!fileSaveDir.exists()) {
                    fileSaveDir.mkdir();
                }

                System.out.println("REAL filePath = " + fileSaveDir);
                System.out.println("REAL filePath = " + filePath);
                String filePath1 = filePath.replace("\\", "\\\\");
                System.out.println("REAL filePath1 = " + filePath1);

                String tripId1 = request.getParameter("tripId");
                System.out.println("tripId.length" + tripId1);
                //new CreateBarChartExample(tripId,filePath);
                /*
                 if (tripId1 != null) {
                 System.out.println("tripID" + tripId1);
                 new CreateBarChartExample(tripId1, filePath, vehicleLogDetails);
                 filePath1 = filePath1 + "\\" + "dataLogExcel" + tripId1 + ".xls";
                 System.out.println("filePath1 new file path= " + filePath1);
                 int graphInsertStatus = 0;
                 graphInsertStatus = tripBP.insertBillingGraph(tripId1, filePath1, userId);
                 }
                 */
            }

            request.setAttribute("tripSheetId", tripSheetId);
            System.out.println("tripSheetId in the bill generation = " + tripSheetId);
            request.setAttribute("invoiceNo", invoiceNo);
//            request.setAttribute("invoiceCode", invoiceCode);
            request.setAttribute("invoiceId", invoiceId);
//            request.setAttribute("tripsToBeBilledDetails", tripsToBeBilledDetails);
//            request.setAttribute("tripsOtherExpenseDetails", tripsOtherExpenseDetails);
//            
//            printData.put("invoiceTaxDetails", invoiceTaxDetails);
//            printData.put("invoiceDetails", invoiceDetails);
//            printData.put("invoiceTripDetails", invoiceTripDetails);
            path = "content/trip/invoicePrint.jsp";

            mv = new ModelAndView("viewbillpageforsubmit.do?");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
//        return new ModelAndView(path);
    }

    public ModelAndView viewTripSheetForVehicleAllotment(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        tripCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Operation  >>  View TripSheet ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            String tripId = request.getParameter("tripId");
            String tripType = request.getParameter("tripType");
            String statusId = request.getParameter("statusId");
            String customerName = request.getParameter("customerName");
            String roleId = "" + (Integer) session.getAttribute("RoleId");
            String companyId = (String) session.getAttribute("companyId");
            tripTO.setTripId(tripId);
            tripTO.setTripType(tripType);
            tripTO.setCustomerName(customerName);
            tripTO.setCompanyId(companyId);
            tripTO.setRoleId(roleId);

            ArrayList tripDetails = new ArrayList();
            ArrayList tripPointDetails = new ArrayList();
            tripDetails = tripBP.getTripDetails(tripTO);
            tripPointDetails = tripBP.getTripPointDetails(tripTO);

            Iterator itr = tripDetails.iterator();
            TripTO tripTONew = null;
            String vehicleTypeId = "";
            while (itr.hasNext()) {
                tripTONew = new TripTO();
                tripTONew = (TripTO) itr.next();
                vehicleTypeId = tripTONew.getVehicleTypeId();
            }
            if (tripCommand.getConsignmentOrderNos() != null && tripCommand.getConsignmentOrderNos() != "") {
                tripTO.setConsignmentOrderNos(tripCommand.getConsignmentOrderNos());
            }
            ArrayList consignmentList = new ArrayList();
            consignmentList = tripBP.getConsignmentList(tripTO);
            request.setAttribute("consignmentList", consignmentList);

            String vehicleMileageAndTollRate = tripBP.getVehicleMileageAndTollRate(vehicleTypeId);
            request.setAttribute("vehicleMileageAndTollRate", vehicleMileageAndTollRate);
            tripTO.setTripSheetId(tripId);
            ArrayList expiryDateDetails = new ArrayList();
            expiryDateDetails = tripBP.getExpiryDateDetails(tripTO);
            request.setAttribute("expiryDateDetails", expiryDateDetails);
            request.setAttribute("tripDetails", tripDetails);
            request.setAttribute("tripPointDetails", tripPointDetails);
            request.setAttribute("tripType", tripType);
            request.setAttribute("customerName", customerName);
            request.setAttribute("statusId", statusId);
            ArrayList vehicleNos = tripBP.getVehicleRegNos(tripTO);
            System.out.println("vehicleNos.size() = " + vehicleNos.size());
            request.setAttribute("vehicleNos", vehicleNos);
            ArrayList statusList = tripBP.getStatusList(tripTO);
            System.out.println("tripStatus.size() = " + statusList.size());
            request.setAttribute("statusList", statusList);
            path = "content/trip/viewTripForUpdate.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewTripSheetForVehicleChange(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Operation  >>  View TripSheet ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            String tripId = request.getParameter("tripId");
            tripTO.setTripId(tripId);

            ArrayList tripDetails = new ArrayList();
            ArrayList tripPointDetails = new ArrayList();
            tripDetails = tripBP.getTripDetails(tripTO);
            tripPointDetails = tripBP.getTripPointDetails(tripTO);

            ArrayList consDetails = new ArrayList();
            consDetails = tripBP.getConsDetails(tripTO);
            Iterator itr1 = consDetails.iterator();
            TripTO tripTO1 = null;
            String consid = "";
            String sealnotype = "";
            String sealno = "";

            while (itr1.hasNext()) {
                tripTO1 = new TripTO();
                tripTO1 = (TripTO) itr1.next();
                consid = tripTO1.getOrderId();
            }

            System.out.println("consid :" + consid);
            Iterator itr = tripDetails.iterator();
            TripTO tripTONew = null;
            String vehicleTypeId = "";
            String vendorId = "";
            String origin_id = "";
            String dest_id = "";
            while (itr.hasNext()) {
                tripTONew = new TripTO();
                tripTONew = (TripTO) itr.next();
                vehicleTypeId = tripTONew.getVehicleTypeId();
                vendorId = tripTONew.getVendorId();
                origin_id = tripTONew.getOriginId();
                dest_id = tripTONew.getDestinationId();
                sealnotype = tripTONew.getSealNoType();
                sealno = tripTONew.getSealNo();
            }

            System.out.println("sealno type:" + sealnotype);
            request.setAttribute("sealnotype", sealnotype);
            request.setAttribute("sealno", sealno);

            String tripType = request.getParameter("tripType");
            String statusId = request.getParameter("statusId");
            String vehicleId = request.getParameter("vehicleId");
            String vehicleNo = request.getParameter("vehicleNo");

            String vehicleMileageAndTollRate = tripBP.getVehicleMileageAndTollRate(vehicleTypeId);
            request.setAttribute("vehicleMileageAndTollRate", vehicleMileageAndTollRate);
            tripTO.setTripSheetId(tripId);

            ArrayList expiryDateDetails = new ArrayList();
            expiryDateDetails = tripBP.getExpiryDateDetails(tripTO);

            request.setAttribute("expiryDateDetails", expiryDateDetails);
            request.setAttribute("tripDetails", tripDetails);
            request.setAttribute("tripPointDetails", tripPointDetails);

            request.setAttribute("tripType", tripType);
            request.setAttribute("statusId", statusId);

            ArrayList cityList = new ArrayList();
            cityList = tripBP.getCityList();
            request.setAttribute("cityList", cityList);

            if (tripType != null) {
                tripTO.setTripType(tripType);
                request.setAttribute("tripType", tripType);
            }
            if (vehicleId == null) {
                vehicleId = "";
            }

            String roleId = "" + (Integer) session.getAttribute("RoleId");
            String companyId = (String) session.getAttribute("companyId");

            tripTO.setSealNoType(sealnotype);
            tripTO.setConsignmentId(consid);

            request.setAttribute("consid", consid);

            tripTO.setRoleId(roleId);
            tripTO.setCompanyId(companyId);
            tripTO.setVehicleTypeId(vehicleTypeId);
            tripTO.setVendorId(vendorId);
            tripTO.setDestinationId(dest_id);
            tripTO.setOriginId(origin_id);

            ArrayList vehicleNos = tripBP.getVehicleRegNos(tripTO);
            System.out.println("vehicleNos.size() = " + vehicleNos.size());
            request.setAttribute("vehicleNos", vehicleNos);

            path = "content/trip/viewTripSheetForVehicleChange.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewTripFreezeUnFreeze(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Operation  >>  View TripSheet ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            String tripId = request.getParameter("tripId");
            tripTO.setTripId(tripId);

            ArrayList tripDetails = new ArrayList();
            ArrayList tripPointDetails = new ArrayList();
            tripDetails = tripBP.getTripDetails(tripTO);
            tripPointDetails = tripBP.getTripPointDetails(tripTO);

            Iterator itr = tripDetails.iterator();
            TripTO tripTONew = null;
            String vehicleTypeId = "";
            while (itr.hasNext()) {
                tripTONew = new TripTO();
                tripTONew = (TripTO) itr.next();
                vehicleTypeId = tripTONew.getVehicleTypeId();
            }

            String vehicleMileageAndTollRate = tripBP.getVehicleMileageAndTollRate(vehicleTypeId);
            request.setAttribute("vehicleMileageAndTollRate", vehicleMileageAndTollRate);

            tripTO.setTripSheetId(tripId);
            ArrayList expiryDateDetails = new ArrayList();
            expiryDateDetails = tripBP.getExpiryDateDetails(tripTO);
            request.setAttribute("expiryDateDetails", expiryDateDetails);
            request.setAttribute("tripDetails", tripDetails);
            request.setAttribute("tripPointDetails", tripPointDetails);
            String tripType = request.getParameter("tripType");
            String statusId = request.getParameter("statusId");
            request.setAttribute("tripType", tripType);
            request.setAttribute("statusId", statusId);
            path = "content/trip/viewTripForFreezeUnFreeze.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveTripSheet(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ModelAndView mv = null;
        String menuPath = "Operation  >>  View TripSheet ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            String[] consignmentOrderId = request.getParameterValues("consignmentOrderId");
            for (int j = 0; j < consignmentOrderId.length; j++) {
                System.out.println("j:" + j + " value is=" + consignmentOrderId[j]);
            }
            String regex = "(?<=[\\d])(,)(?=[\\d])";
            //String tripDate = request.getParameter("tripDate");
            String vehicleId = request.getParameter("vehicleId");
            String vehicleNoEmail = request.getParameter("vehicleNoEmail");
            String driver1Id = request.getParameter("driver1Id");
            String driver2Id = request.getParameter("driver2Id");
            String driver3Id = request.getParameter("driver3Id");
            String orderExpense = request.getParameter("orderExpense");
//            String orderExpense1 = request.getParameter("orderExpense");
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(orderExpense);
            orderExpense = m.replaceAll("");
            System.out.println("orderExpense : " + orderExpense);
            String orderRevenue = request.getParameter("orderRevenue");
            String profitMargin = request.getParameter("profitMargin");
            String tripScheduleDate = request.getParameter("tripScheduleDate");
            String tripScheduleTime = request.getParameter("tripScheduleTime");
            String tripScheduleTimeHrs = request.getParameter("tripScheduleTimeHrs");
            String tripScheduleTimeMins = request.getParameter("tripScheduleTimeMins");
            String totalHours = request.getParameter("totalHours");

            String cNotes = request.getParameter("cNotes");
            String billingType = request.getParameter("billingType");
            String customerName = request.getParameter("customerName");
            String customerType = request.getParameter("customerType");
            String routeInfo = request.getParameter("routeInfo");
            String productInfo = request.getParameter("productInfo");
            String reeferRequired = request.getParameter("reeferRequired");
            String totalWeight = request.getParameter("totalWeight");

            String vehicleNo = request.getParameter("vehicleNo");
            String vehicleTonnage = request.getParameter("vehicleTonnage");
            String vehicleCapUtil = request.getParameter("vehicleCapUtil");
            String tripRemarks = request.getParameter("tripRemarks");

            String actionName = request.getParameter("actionName");
            String actionRemarks = request.getParameter("actionRemarks");
            String preStartLocationId = request.getParameter("preStartLocationId");
            String preStartLocationPlanDate = request.getParameter("preStartLocationPlanDate");
            String preStartLocationPlanTimeHrs = request.getParameter("preStartLocationPlanTimeHrs");
            String preStartLocationPlanTimeMins = request.getParameter("preStartLocationPlanTimeMins");
            String preStartLocationDistance = request.getParameter("preStartLocationDistance");
            String preStartLocationDurationHrs = request.getParameter("preStartLocationDurationHrs");
            String preStartLocationDurationMins = request.getParameter("preStartLocationDurationMins");
            String preStartLocationVehicleMileage = request.getParameter("vehicleMileage");
            String preStartLocationTollRate = request.getParameter("tollRate");
            String preStartLocationRouteExpense = request.getParameter("preStartRouteExpense");
            String preStartLocationStatus = request.getParameter("preStartLocationCheckbox");

            String customerId = request.getParameter("customerId");
            String vehicleCategory = request.getParameter("vehicleCategory");
            String hireVehicleNo = request.getParameter("hireVehicleNo");
            String ownVehicleDriver = request.getParameter("driver1Name");
            String hireVehicleDriver = request.getParameter("hireVehicleDriver");
            String cbt = request.getParameter("cbt");
            String totalPackages = request.getParameter("totalPackages");

            System.out.println("hireVehicleDriver=======" + hireVehicleDriver + "=====ownVehicleDriver====" + ownVehicleDriver);
            if (preStartLocationStatus == null) {
                preStartLocationStatus = "0";
            }
            String roleId = "" + (Integer) session.getAttribute("RoleId");
            String companyId = (String) session.getAttribute("companyId");
            tripTO.setRoleId(roleId);
            tripTO.setCbt(cbt);
            tripTO.setCompanyId(companyId);
            tripTO.setProductInfo(productInfo);
            tripTO.setCustomerId(customerId);
            tripTO.setOwnerShip(vehicleCategory);

            tripTO.setPreStartLocationStatus(preStartLocationStatus);
            tripTO.setActionName(actionName);
            tripTO.setActionRemarks(actionRemarks);
            tripTO.setPreStartLocationId(preStartLocationId);
            tripTO.setTotalPackages(totalPackages);

            if ("1".equals(tripTO.getPreStartLocationStatus())) {
                tripTO.setPreStartLocationPlanDate(tripScheduleDate);
                tripTO.setPreStartLocationPlanTimeHrs(tripScheduleTimeHrs);
                tripTO.setPreStartLocationPlanTimeMins(tripScheduleTimeMins);
            } else {
                if (preStartLocationRouteExpense == null || "".equals(preStartLocationRouteExpense)) {
                    preStartLocationRouteExpense = "0";
                }
                System.out.println("orderExpense:" + orderExpense + " preStartLocationRouteExpense:" + preStartLocationRouteExpense);
                orderExpense = "" + (Float.parseFloat(orderExpense) + Float.parseFloat(preStartLocationRouteExpense));
                tripTO.setPreStartLocationPlanDate(preStartLocationPlanDate);
                tripTO.setPreStartLocationPlanTimeHrs(preStartLocationPlanTimeHrs);
                tripTO.setPreStartLocationPlanTimeMins(preStartLocationPlanTimeMins);
            }

            tripTO.setPreStartLocationDistance(preStartLocationDistance);
            tripTO.setPreStartLocationDurationHrs(preStartLocationDurationHrs);
            tripTO.setPreStartLocationDurationMins(preStartLocationDurationMins);
            tripTO.setPreStartLocationVehicleMileage(preStartLocationVehicleMileage);
            tripTO.setPreStartLocationTollRate(preStartLocationTollRate);
            tripTO.setPreStartLocationRouteExpense(preStartLocationRouteExpense);

            //points related
            String[] orderId = request.getParameterValues("orderId");
            String[] pointId = request.getParameterValues("pointId");
            String[] pointType = request.getParameterValues("pointType");
            String[] pointOrder = request.getParameterValues("pointOrder");
            String[] pointAddresss = request.getParameterValues("pointAddresss");
            String[] pointPlanDate = request.getParameterValues("pointPlanDate");
            String[] pointPlanTimeHrs = request.getParameterValues("pointPlanTimeHrs");
            String[] pointPlanTimeMins = request.getParameterValues("pointPlanTimeMins");

            //load details
            String[] loadOrderId = request.getParameterValues("loadOrderId");
            String[] pendingPkgs = request.getParameterValues("pendingPkgs");
            String[] loadedPkgs = request.getParameterValues("loadedPkgs");

            String[] pendingWeight = request.getParameterValues("pendingWeight");
            String[] loadedWeight = request.getParameterValues("loadedWeight");
            String[] pendingGrossWeight = request.getParameterValues("pendingGrossWeight");
            String[] loadedGrossWeight = request.getParameterValues("loadedGrossWeight");

            tripTO.setLoadOrderId(loadOrderId);
            tripTO.setPendingPkgs(pendingPkgs);
            tripTO.setPendingWeight(pendingWeight);
            tripTO.setLoadedPkgs(loadedPkgs);
            tripTO.setLoadedWeight(loadedWeight);
            tripTO.setPendingGrossWeight(pendingGrossWeight);
            tripTO.setLoadedGrossWeight(loadedGrossWeight);

            tripTO.setVehicleNo(vehicleNoEmail);
            tripTO.setVehicleCapUtil(vehicleCapUtil);
            tripTO.setTripRemarks(tripRemarks);
            tripTO.setcNotes(cNotes);
            tripTO.setBillingType(billingType);
            tripTO.setCustomerName(customerName);
            tripTO.setCustomerType(customerType);
            tripTO.setRouteInfo(routeInfo);
            tripTO.setReeferRequired(reeferRequired);
            tripTO.setTotalWeight(totalWeight);
            tripTO.setHireVehicleNo(hireVehicleNo);
            tripTO.setHireVehicleDriver(hireVehicleDriver);
            tripTO.setPrimaryDriverName(ownVehicleDriver);

            tripTO.setConsignmentOrderId(consignmentOrderId);
            //tripTO.setTripDate(tripDate);
            tripTO.setVehicleId(vehicleId);
            tripTO.setTotalHrs(totalHours);
            tripTO.setTripTransitHours(totalHours);

            tripTO.setPrimaryDriverId(driver1Id);
            tripTO.setSecondaryDriver1Id(driver2Id);
            tripTO.setSecondaryDriver2Id(driver3Id);
            tripTO.setOrderExpense(orderExpense);
            tripTO.setOrderRevenue(orderRevenue);
            tripTO.setProfitMargin(profitMargin);
            tripTO.setTripScheduleDate(tripScheduleDate);
            tripTO.setTripScheduleTime(tripScheduleTime);
            //tripTO.setTripScheduleTimeHrs(tripScheduleTime);
            tripTO.setOrderIds(orderId);
            tripTO.setPointId(pointId);
            tripTO.setPointType(pointType);
            tripTO.setPointOrder(pointOrder);
            tripTO.setPointAddresss(pointAddresss);
            tripTO.setPointPlanDate(pointPlanDate);
            tripTO.setPointPlanTimeHrs(pointPlanTimeHrs);
            tripTO.setPointPlanTimeMins(pointPlanTimeMins);

            String vehicleVendorTemp = request.getParameter("vendor");
            String tempvendor[] = vehicleVendorTemp.split("~");
            String vehicleVendorId = tempvendor[0];
            tripTO.setVendorId(vehicleVendorId);

//            String lhcNo = request.getParameter("lhcNo");
//            String lhcId = request.getParameter("lhcId");
            String lhcNo = "";
            String lhcId = "";
            String additionalCost = "";
            String contractHireId = "";

            String vehicleTypes = request.getParameter("vehicleTypeIdSelected");
            String vehicleType = request.getParameter("vehicleType");

            String originId = request.getParameter("originId");
            String destinationId = request.getParameter("destinationId");
            tripTO.setVehicleTypeName(vehicleType);
            tripTO.setVehicleTypeId(vehicleTypes);
            tripTO.setUserId(userId);
            tripTO.setOriginId(originId);
            tripTO.setDestinationId(destinationId);

            tripTO.setLhcNo(lhcNo);
            tripTO.setLhcId(lhcId);

            String lhcDetails = "";
            lhcDetails = tripBP.getlhcContractDetails(tripTO);
            System.out.println("lhcDetails==" + lhcDetails);
            // System.out.println("lhcDetails3=="+lhcDetails.length());
//            request.setAttribute("additionalCost", lhcAmountDetails[0]);
//            request.setAttribute("contractHireId", lhcAmountDetails[1]);
            String agreedRate = request.getParameter("agreedRate");
            String movementTypeId = request.getParameter("movementTypeId");
            System.out.println("movementTypeId==="+movementTypeId);
            tripTO.setMovementTypeId(movementTypeId);
            if (lhcDetails != null && !lhcDetails.equals("")) {

                String[] temp = lhcDetails.split("~");
                System.out.println("additionalCost" + agreedRate);
                System.out.println("contractHireId" + temp[1]);
                tripTO.setAdditionalCost(agreedRate);
                tripTO.setContractHireId(temp[1]);

                int tripId = tripBP.saveTripSheet(tripTO);
                path = "BrattleFoods/viewTripSheet.jsp";
                System.out.println("status : " + tripId);

                if (tripId != 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Trip Planed Successfully. Trip Code No is " + tripTO.getTripCode());
                }

            } else {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Vendor Contract Details not Available. ");

            }

            mv = handleViewTripSheet(request, response, command);

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save trip sheet data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView saveAction(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ModelAndView mv = null;
        String menuPath = "Operation  >>  View TripSheet ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            String[] consignmentOrderId = request.getParameterValues("consignmentOrderId");
            String actionName = request.getParameter("actionName");
            String actionRemarks = request.getParameter("actionRemarks");

            tripTO.setConsignmentOrderId(consignmentOrderId);
            tripTO.setActionName(actionName);
            tripTO.setActionRemarks(actionRemarks);
            tripTO.setUserId(userId);
            if ("cancel".equalsIgnoreCase(actionName)) {
                tripTO.setStatusId("4");
            } else {
                tripTO.setStatusId("3");//hold
            }
            int status = tripBP.saveAction(tripTO);
            path = "BrattleFoods/viewTripSheet.jsp";
            mv = handleViewTripSheet(request, response, command);

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save trip sheet data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView saveTripUpdate(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ModelAndView mv = null;
        String menuPath = "Operation  >>  View TripSheet ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            String tripId = request.getParameter("tripId");

            String actionName = request.getParameter("actionName");
            String vehicleCapUtil = request.getParameter("vehicleCapUtil");
            String actionRemarks = request.getParameter("actionRemarks");

            String vehicleId = request.getParameter("vehicleId");
            String driver1Id = request.getParameter("driver1Id");
            String driver2Id = request.getParameter("driver2Id");
            String driver3Id = request.getParameter("driver3Id");

            tripTO.setVehicleId(vehicleId);

            tripTO.setPrimaryDriverId(driver1Id);
            tripTO.setVehicleCapUtil(vehicleCapUtil);
            tripTO.setSecondaryDriver1Id(driver2Id);
            tripTO.setSecondaryDriver2Id(driver3Id);
            tripTO.setTripId(tripId);
            tripTO.setActionName(actionName);
            tripTO.setActionRemarks(actionRemarks);
            tripTO.setUserId(userId);
            if ("1".equalsIgnoreCase(actionName)) {
                tripTO.setStatusId("8");//freeze
            } else if ("2".equalsIgnoreCase(actionName)) {
                tripTO.setStatusId("6");//unfreeze
            } else {
                tripTO.setStatusId("7");//vehicle Allocated
            }

            String preStartLocationId = request.getParameter("preStartLocationId");
            String preStartLocationPlanDate = request.getParameter("preStartLocationPlanDate");
            String preStartLocationPlanTimeHrs = request.getParameter("preStartLocationPlanTimeHrs");
            String preStartLocationPlanTimeMins = request.getParameter("preStartLocationPlanTimeMins");
            String preStartLocationDistance = request.getParameter("preStartLocationDistance");
            String preStartLocationDurationHrs = request.getParameter("preStartLocationDurationHrs");
            String preStartLocationDurationMins = request.getParameter("preStartLocationDurationMins");
            String preStartLocationVehicleMileage = request.getParameter("vehicleMileage");
            String preStartLocationTollRate = request.getParameter("tollRate");
            String preStartLocationRouteExpense = request.getParameter("preStartRouteExpense");
            String preStartLocationStatus = request.getParameter("preStartLocationCheckbox");
            String originId = request.getParameter("originId");
            String destinationId = request.getParameter("destinationId");

            if (preStartLocationStatus == null) {
                preStartLocationStatus = "0";
            }
            tripTO.setOriginId(originId);
            tripTO.setDestinationId(destinationId);
            tripTO.setPreStartLocationStatus(preStartLocationStatus);
            tripTO.setActionName(actionName);
            tripTO.setActionRemarks(actionRemarks);
            tripTO.setPreStartLocationId(preStartLocationId);
            if ("1".equalsIgnoreCase(actionName)) {
                if ("1".equals(tripTO.getPreStartLocationStatus())) {
                    tripTO.setPreStartLocationPlanDate("");
                    tripTO.setPreStartLocationPlanTimeHrs("");
                    tripTO.setPreStartLocationPlanTimeMins("");
                } else {
                    if (preStartLocationRouteExpense == null) {
                        preStartLocationRouteExpense = "0";
                    }
                    String orderExpense = request.getParameter("orderExpense");
                    orderExpense = "" + (Float.parseFloat(orderExpense) + Float.parseFloat(preStartLocationRouteExpense));
                    tripTO.setPreStartLocationPlanDate(preStartLocationPlanDate);
                    tripTO.setPreStartLocationPlanTimeHrs(preStartLocationPlanTimeHrs);
                    tripTO.setPreStartLocationPlanTimeMins(preStartLocationPlanTimeMins);
                    tripTO.setOrderExpense(orderExpense);
                }
            }

            tripTO.setPreStartLocationDistance(preStartLocationDistance);
            tripTO.setPreStartLocationDurationHrs(preStartLocationDurationHrs);
            tripTO.setPreStartLocationDurationMins(preStartLocationDurationMins);
            tripTO.setPreStartLocationVehicleMileage(preStartLocationVehicleMileage);
            tripTO.setPreStartLocationTollRate(preStartLocationTollRate);
            tripTO.setPreStartLocationRouteExpense(preStartLocationRouteExpense);

            int status = tripBP.saveTripUpdate(tripTO);
            String tripType = request.getParameter("tripType");
            String statusId = request.getParameter("statusId");
            request.setAttribute("tripType", tripType);
            request.setAttribute("statusId", statusId);
            path = "content/redirectPage.jsp";
            mv = handleViewTripSheet(request, response, command);

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save trip sheet data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
//        return new ModelAndView(path);
        return mv;
    }

    public ModelAndView saveTripSheetForVehicleChangeOld(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ModelAndView mv = null;
        String menuPath = "Operation  >>  View TripSheet ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            String tripId = request.getParameter("tripId");

            String actionName = request.getParameter("actionName");
            String vehicleCapUtil = request.getParameter("vehicleCapUtil");
            String actionRemarks = request.getParameter("actionRemarks");

            String vehicleId = request.getParameter("vehicleId");
            String driver1Id = request.getParameter("driver1Id");
            String driver2Id = request.getParameter("driver2Id");
            String driver3Id = request.getParameter("driver3Id");

            String lastVehicleEndKm = request.getParameter("lastVehicleEndKm");
            String vehicleStartKm = request.getParameter("vehicleStartKm");
            String lastVehicleEndOdometer = request.getParameter("lastVehicleEndOdometer");
            String vehicleStartOdometer = request.getParameter("vehicleStartOdometer");
            String cityId = request.getParameter("cityId");
            String vehicleChangeDate = request.getParameter("vehicleChangeDate");
            String vehicleChangeMinute = request.getParameter("vehicleChangeMinute");
            String vehicleChangeHour = request.getParameter("vehicleChangeHour");
            String vehicleRemarks1 = request.getParameter("vehicleRemarks");
            String vehicleNo = request.getParameter("vehicleNo");
            String vehicleNo1 = request.getParameter("vehicleNo1");
            String ownVehicleDriver = request.getParameter("driver1Name");
            String vehicleRemarks = vehicleRemarks1 + "Change Vehicle" + vehicleNo1 + " to " + vehicleNo;

            tripTO.setVehicleRemarks(vehicleRemarks);
            tripTO.setVehicleChangeDate(vehicleChangeDate);
            tripTO.setVehicleChangeHour(vehicleChangeHour);
            tripTO.setVehicleChangeMinute(vehicleChangeMinute);
            tripTO.setLastVehicleEndOdometer(lastVehicleEndOdometer);
            tripTO.setVehicleStartOdometer(vehicleStartOdometer);
            tripTO.setCityId(cityId);
            tripTO.setLastVehicleEndKm(lastVehicleEndKm);
            tripTO.setVehicleStartKm(vehicleStartKm);
            tripTO.setVehicleId(vehicleId);
            tripTO.setPrimaryDriverName(ownVehicleDriver);

            tripTO.setPrimaryDriverId(driver1Id);
            tripTO.setVehicleCapUtil(vehicleCapUtil);
            tripTO.setSecondaryDriver1Id(driver2Id);
            tripTO.setSecondaryDriver2Id(driver3Id);
            tripTO.setTripId(tripId);
            tripTO.setActionName(actionName);
            tripTO.setActionRemarks(actionRemarks);
            tripTO.setUserId(userId);

            if ("1".equalsIgnoreCase(actionName)) {
                tripTO.setStatusId("8");//freeze
            } else if ("2".equalsIgnoreCase(actionName)) {
                tripTO.setStatusId("6");//unfreeze
            } else {
                tripTO.setStatusId("7");//vehicle Allocated
            }

            String originId = request.getParameter("originId");
            String destinationId = request.getParameter("destinationId");

            tripTO.setOriginId(originId);
            tripTO.setDestinationId(destinationId);

            int status = tripBP.saveTripSheetForVehicleChange(tripTO, null);
            if (status != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Info Updated Successfully....");
            }
            String tripType = request.getParameter("tripType");
            String statusId = request.getParameter("statusId");
            request.setAttribute("tripType", tripType);
            request.setAttribute("statusId", statusId);

            path = "BrattleFoods/viewTripSheet.jsp";
            //path = "content/redirectPage.jsp";
            mv = handleViewTripSheet(request, response, command);

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save trip sheet data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
//        return new ModelAndView(path);
        return mv;
    }

    public void getVehicleRegNos(HttpServletRequest request, HttpServletResponse response, TripCommand command) throws IOException {
        HttpSession session = request.getSession();
        tripCommand = command;
        TripTO tripTO = new TripTO();

        ArrayList vehicleNos = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String responseValue = "";
            String tripType = "";
            response.setContentType("text/html");
            responseValue = request.getParameter("vehicleNo");
            tripType = request.getParameter("tripType");
            tripTO.setVehicleNo(responseValue);
            String roleId = "" + (Integer) session.getAttribute("RoleId");
            String companyId = (String) session.getAttribute("companyId");
            tripTO.setRoleId(roleId);
            tripTO.setCompanyId(companyId);
            tripTO.setTripType(tripType);
            vehicleNos = tripBP.getVehicleRegNos(tripTO);
            System.out.println("vehicleNos.size() = " + vehicleNos.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = vehicleNos.iterator();
            int cntr = 0;
            while (itr.hasNext() && cntr < 5) {
                JSONObject jsonObject = new JSONObject();
                tripTO = (TripTO) itr.next();
                jsonObject.put("Name", tripTO.getVehicleId() + "-" + tripTO.getVehicleNo() + "-" + tripTO.getVehicleTonnage()
                        + "-" + tripTO.getPrimaryDriverId() + "-" + tripTO.getPrimaryDriverName()
                        + "-" + tripTO.getSecondaryDriver1Id() + "-" + tripTO.getSecondaryDriver1Name()
                        + "-" + tripTO.getSecondaryDriver2Id() + "-" + tripTO.getSecondaryDriver2Name()
                        + "-" + tripTO.getFcExpiryDate() + "-" + tripTO.getInsuranceExpiryDate()
                        + "-" + tripTO.getPermitExpiryDate() + "-" + tripTO.getRoadTaxExpiryDate());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
                cntr++;
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getDrivers(HttpServletRequest request, HttpServletResponse response, TripCommand command) throws IOException {
        HttpSession session = request.getSession();
        tripCommand = command;
        TripTO tripTO = new TripTO();

        ArrayList drivers = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String responseValue = "";
            response.setContentType("text/html");
            responseValue = request.getParameter("driverName");
            tripTO.setDriverName(responseValue);
            drivers = tripBP.getDrivers(tripTO);
            System.out.println("drivers.size() = " + drivers.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = drivers.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                tripTO = (TripTO) itr.next();
                jsonObject.put("Name", tripTO.getDriverId() + "-" + tripTO.getDriverName());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView handleViewTripSheet(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View Trip Sheet ";
        String statusId1 = request.getParameter("statusId");
        System.out.println("getParameter value:" + request.getParameter("successMessage"));
        System.out.println("getAttribute value:" + request.getAttribute("successMessage"));
        request.setAttribute("errorMessage", request.getParameter("msg"));
        if (request.getAttribute("successMessage") == null) {
            request.setAttribute("successMessage", request.getParameter("successMessage"));
        } else {
            request.setAttribute("successMessage", request.getAttribute("successMessage"));

        }

        int podcheck = 0;
        if (statusId1 != null && statusId1 != "") {
            int statusId2 = Integer.parseInt(statusId1);
            if (statusId2 == 6) {
                menuPath = "Trip Vehicle Allotment ";
            }
            if (statusId2 == 7) {
                menuPath = "Trip For Freeze  ";
            }
            if (statusId2 == 8) {
                menuPath = "Start Trip Sheet ";
            }
            if (statusId2 == 9) {
            }
            if (statusId2 == 10) {
                menuPath = "End Trip Sheet ";
            }
            if (statusId2 == 100) {
                menuPath = "Trip POD ";                
            }
            if (statusId2 == 12) {
                podcheck = 10;
                menuPath = "TripClosure ";
            }
            if (statusId2 == 13) {
                menuPath = "Trip Settlement ";
            }
        }

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            String param = request.getParameter("param");
            System.out.println("param vale is:" + param);
            String documentRequired = request.getParameter("doucmentRequired");
            tripTO.setDocumentRequired(documentRequired);
            request.setAttribute("documentRequired", documentRequired);
            String tripType = request.getParameter("tripType");
            String vehicleId = request.getParameter("vehicleId");
            String fleetCenterId = request.getParameter("fleetCenterId");
            String tripStatusId = request.getParameter("tripStatusId");
            String vehicleTypeId = request.getParameter("vehicleTypeId");
            String zoneId = request.getParameter("zoneId");
            String customerId = request.getParameter("customerId");
            String cityFromId = request.getParameter("cityFromId");
            String cityFrom = request.getParameter("cityFrom");
            String admin = request.getParameter("admin");
            request.setAttribute("cityFromId", cityFromId);
            request.setAttribute("cityFrom", cityFrom);
            System.out.println("admin in the controller = " + admin);

            int userId = (Integer) session.getAttribute("userId");
            String userMovementType = (String) session.getAttribute("userMovementType");
            String roleId = "" + (Integer) session.getAttribute("RoleId");
            tripTO.setUserId(userId);
            tripTO.setUserMovementType(userMovementType);
            request.setAttribute("roleId", roleId);

            tripTO.setTripType(tripType);
            System.out.println("tripType = " + tripType);
            request.setAttribute("tripType", tripType);

            Date dNow = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(dNow);
            cal.add(Calendar.DATE, 0);
            dNow = cal.getTime();

            Date dNow1 = new Date();
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(dNow1);
            cal1.add(Calendar.DATE, -30);
            dNow1 = cal1.getTime();

            SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy");
            String fromday = ft.format(dNow1);
            String today = ft.format(dNow);
            System.out.println("the today date is " + fromday);

            if (tripCommand.getPodStatus() != null && !tripCommand.getPodStatus().equals("")) {
                tripTO.setPodStatus(tripCommand.getPodStatus());
                request.setAttribute("podStatus", tripCommand.getPodStatus());
            } else {
                tripTO.setPodStatus("");
                request.setAttribute("podStatus", "");
            }

            if (podcheck == 10) {
                tripTO.setPodStatus("0");
                request.setAttribute("podStatus", "0");
            }

            if (cityFromId != null && !cityFromId.equals("")) {
                tripTO.setCityFromId(cityFromId);
                request.setAttribute("cityFromId", cityFromId);
            } else {
                tripTO.setCityFromId("");
                request.setAttribute("cityFromId", "");
            }
            if (vehicleId != null && !vehicleId.equals("")) {
                tripTO.setVehicleId(vehicleId);
                request.setAttribute("vehicleId", vehicleId);
            } else {
                tripTO.setVehicleId("");
                request.setAttribute("vehicleId", "");
            }
            if (fleetCenterId != null && !fleetCenterId.equals("")) {
                tripTO.setFleetCenterId(fleetCenterId);
                request.setAttribute("fleetCenterId", fleetCenterId);
            } else {
                tripTO.setFleetCenterId("");
                request.setAttribute("fleetCenterId", "");
            }
            if (zoneId != null && !zoneId.equals("")) {
                tripTO.setZoneId(zoneId);
                request.setAttribute("zoneId", zoneId);
            } else {
                tripTO.setZoneId("");
                request.setAttribute("zoneId", "");
            }
            if (vehicleTypeId != null && !vehicleTypeId.equals("")) {
                tripTO.setVehicleTypeId(vehicleTypeId);
                request.setAttribute("vehicleTypeId", vehicleTypeId);
            } else {
                tripTO.setVehicleTypeId("");
                request.setAttribute("vehicleTypeId", "");
            }
            if (customerId != null && !customerId.equals("")) {
                tripTO.setCustomerId(customerId);
                request.setAttribute("customerId", customerId);
            } else {
                tripTO.setCustomerId("");
                request.setAttribute("customerId", "");
            }
            if (tripStatusId != null && !tripStatusId.equals("")) {
                tripTO.setTripStatusId(tripStatusId);
                request.setAttribute("tripStatusId", tripStatusId);
            } else {
                tripTO.setTripStatusId("");
                request.setAttribute("tripStatusId", "");
            }
            System.out.println("tripCommand.getTripSheetId()--" + tripCommand.getTripSheetId());
            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
                request.setAttribute("tripSheetId", tripTO.getTripSheetId());
                tripTO.setTripCode(tripTO.getTripSheetId());
            } else {
                String tripSheetId = request.getParameter("tripSheetId");
                System.out.println("tripSheetId--" + tripSheetId);
                tripTO.setTripSheetId(tripSheetId);
                tripTO.setTripCode(tripSheetId);
                request.setAttribute("tripSheetId", "");
            }
            String docketNo1 = request.getParameter("docketNo1");
            System.out.println("docketNo1--" + docketNo1);
            if (docketNo1 == null) {
                tripTO.setDocketNo("");
                request.setAttribute("docketNo", "");
            } else {
                tripTO.setDocketNo(docketNo1);
                request.setAttribute("docketNo", docketNo1);
            }

            if (tripCommand.getFromDate() != null && !tripCommand.getFromDate().equals("")) {
                tripTO.setFromDate(tripCommand.getFromDate());
                request.setAttribute("fromDate", tripTO.getFromDate());
            } else {
                tripTO.setFromDate(fromday);
                request.setAttribute("fromDate", fromday);
            }
            if (tripCommand.getToDate() != null && !tripCommand.getToDate().equals("")) {
                tripTO.setToDate(tripCommand.getToDate());
                request.setAttribute("toDate", tripTO.getToDate());
            } else {
                tripTO.setToDate(today);
                request.setAttribute("toDate", today);
            }
            String statusId = request.getParameter("statusId");
            if (statusId == null || "".equals(statusId)) {
                statusId = "0";
            }
            roleId = "" + (Integer) session.getAttribute("RoleId");
            String desigId = "" + (Integer) session.getAttribute("DesigId");

            String companyId = (String) session.getAttribute("companyId");
            tripTO.setRoleId(roleId);
            tripTO.setCompanyId(companyId);
            tripTO.setStatusId(statusId);
            request.setAttribute("statusId", statusId);
            admin = request.getParameter("admin");
            System.out.println("admin in the controller = " + admin);
            if (admin != null) {
                if (admin.equals("Yes")) {
                    tripTO.setExtraExpenseStatus("1");
                }
            }

            ArrayList tripDetails = new ArrayList();
            tripDetails = tripBP.getTripSheetDetails(tripTO);
            request.setAttribute("tripDetails", tripDetails);
            ArrayList statusDetails = new ArrayList();
            statusDetails = tripBP.getStatusDetails();

            ArrayList companyList = new ArrayList();
            companyList = companyBP.processGetCompanyList();
            request.setAttribute("companyList", companyList);

            ArrayList vehicleList = new ArrayList();
            vehicleList = tripBP.getVehicleList();
            request.setAttribute("vehicleList", vehicleList);

            ArrayList customerList = new ArrayList();
            customerList = tripBP.getCustomerList();
            request.setAttribute("customerList", customerList);

            ArrayList zoneList = new ArrayList();
            zoneList = tripBP.getZoneList();
            request.setAttribute("zoneList", zoneList);

            ArrayList vehicleTypeList = new ArrayList();
            vehicleTypeList = vehicleBP.getVehicleTypeList();
            request.setAttribute("vehicleTypeList", vehicleTypeList);
            request.setAttribute("vehicleTypeId", vehicleTypeId);

            request.setAttribute("statusDetails", statusDetails);
            String responsiveStatus = request.getParameter("respStatus");
            String startRequest = request.getParameter("startRequest");
            String endRequest = request.getParameter("endRequest");
            request.setAttribute("admin", admin);
            if (responsiveStatus == null) {
                if (request.getAttribute("respStatus") != null) {
                    responsiveStatus = (String) request.getAttribute("respStatus");
                }
                if (request.getAttribute("startRequest") != null) {
                    startRequest = (String) request.getAttribute("startRequest");
                }
                if (request.getAttribute("endRequest") != null) {
                    endRequest = (String) request.getAttribute("endRequest");
                }
            }

//            if (responsiveStatus != null && "Y".equals(responsiveStatus)) {
//                if ("Y".equals(startRequest)) {
//                    path = "content/trip/viewTripSheetResponsive.jsp";
//                } else {
//                    path = "content/trip/viewTripSheetEndResponsive.jsp";
//                }
//
//            } else 
            if (param != null && param.equals("exportExcel")) {
                path = "content/trip/viewTripSheetExcel.jsp";
            } else if (param != null && param.equals("podCopy")) {

                String[] tripIds = request.getParameterValues("podCopys");
                System.out.println("tripIds : " + tripIds);

                if (tripIds != null) {
                    for (int i = 0; i < tripIds.length; i++) {
                        String tripId = tripIds[i];
                        System.out.println("tripId :" + tripId);
                        String[] trpId = tripId.split(",");
                        tripTO.setTripIds(trpId);
                    }
                }

                ArrayList viewPODDetails = new ArrayList();
                viewPODDetails = tripBP.getPODUploadDetails(tripTO);
                if (viewPODDetails.size() > 0) {
                    request.setAttribute("viewPODDetails", viewPODDetails);
                }

                path = "content/trip/podDownload.jsp";

            } else {
                path = "content/trip/viewTripSheet.jsp";
            }
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

//Throttle Starts Here
    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView viewTripSettlement(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String tripSheetId = "";
        String menuPath = "Operation  >>  Trip Settlement ";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            path = "content/trip/tripSheetSettlement.jsp";
//            path = "content/trip/printSettlementVoucher.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Trip Settlement";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
                request.setAttribute("tripSheetId", tripCommand.getTripSheetId());
                tripSheetId = tripCommand.getTripSheetId();
            }
            if (tripCommand.getVehicleId() != null && !tripCommand.getVehicleId().equals("")) {
                tripTO.setVehicleId(tripCommand.getVehicleId());
                request.setAttribute("vehicleId", tripCommand.getVehicleId());
            }
            System.out.println("the tripSheetId is" + tripCommand.getTripSheetId());

            String tripDetails = tripBP.getTripDetails(tripSheetId);

            //Throttle Starts As On 12-12-2013
            tripTO.setTripId(tripTO.getTripSheetId());

            ArrayList statusDetails = new ArrayList();
            statusDetails = tripBP.getTripStausDetails(tripTO);
            if (statusDetails.size() > 0) {
                request.setAttribute("statusDetails", statusDetails);
            }

            TripTO tripTO1 = new TripTO();
            String vehicleTypeId = "";
            ArrayList totalExpenseDetails = new ArrayList();
            totalExpenseDetails = tripBP.getTotalExpenseDetails(tripTO);
            Iterator itr1 = totalExpenseDetails.iterator();
            while (itr1.hasNext()) {
                tripTO1 = (TripTO) itr1.next();
                request.setAttribute("tollAmount", tripTO1.getTollAmount());
                request.setAttribute("driverBatta", tripTO1.getDriverBatta());
                request.setAttribute("driverIncentive", tripTO1.getDriverIncentive());
                request.setAttribute("fuelPrice", tripTO1.getFuelPrice());
                request.setAttribute("milleage", tripTO1.getMilleage());
                request.setAttribute("reeferConsumption", tripTO1.getReeferConsumption());
                request.setAttribute("vehicleTypeId", tripTO1.getVehicleTypeId());
                vehicleTypeId = tripTO1.getVehicleTypeId();
            }

            TripTO tpTO = new TripTO();
            ArrayList tripSheetDetails = new ArrayList();
//            tripSheetDetails = tripBP.getEndTripSheetDetails(tripTO);
            tripSheetDetails = tripBP.getClosureEndTripSheetDetails(tripTO);
            Iterator itr = tripSheetDetails.iterator();
            while (itr.hasNext()) {
                tpTO = (TripTO) itr.next();
                request.setAttribute("tripSheetId", tpTO.getTripSheetId());
                request.setAttribute("cNotes", tpTO.getcNotes());
                request.setAttribute("vehicleNo", tpTO.getVehicleNo());
                //request.setAttribute("vehicleId", tpTO.getVehicleId());
                request.setAttribute("tripCode", tpTO.getTripCode());
                request.setAttribute("startDate", tpTO.getTripScheduleDate());
                request.setAttribute("startTime", tpTO.getTripScheduleTime());
                request.setAttribute("estimatedExpense", tpTO.getOrderExpense());
                request.setAttribute("rcmExpense", tpTO.getRcmExpense());
                request.setAttribute("origin", tpTO.getOrigin());
                request.setAttribute("destination", tpTO.getDestination());
                request.setAttribute("tripEndDate", tpTO.getTripEndDate());
                request.setAttribute("tripEndTime", tpTO.getTripEndTime());
                request.setAttribute("startKm", tpTO.getStartKm());
                request.setAttribute("startHm", tpTO.getStartHm());
                request.setAttribute("endKm", tpTO.getEndKm());
                request.setAttribute("endHm", tpTO.getEndHm());
                request.setAttribute("runKm", tpTO.getRunKm());
                request.setAttribute("runHm", tpTO.getRunHm());
                request.setAttribute("totaldays", tpTO.getTotalDays());
                request.setAttribute("totalHours", tpTO.getDurationHours());
                request.setAttribute("driverCount", tpTO.getDriverCount());
                request.setAttribute("extraExpenseValue", tpTO.getExtraExpenseValue());
                String[] temp = null;
                if (!"".equals(tpTO.getDriverName()) && tpTO.getDriverName().contains(",")) {
                    temp = tpTO.getDriverName().split(",");
                    request.setAttribute("driverName1", temp[0]);
                    request.setAttribute("driverName2", temp[1]);
                } else {
                    request.setAttribute("driverName1", tpTO.getDriverName());
                }
                request.setAttribute("driverName", tpTO.getDriverName());
            }
            request.setAttribute("tripSheetDetails", tripSheetDetails);
            String gpsKm = tripBP.getGpsKm(tripSheetId);

            if (gpsKm != null) {
                String[] temp = gpsKm.split("-");
                String gpsKmVal = "0";
                if (!"0".equals(temp[0])) {
                    gpsKmVal = temp[0];
                }
                request.setAttribute("gpsKm", "" + gpsKmVal);
                //use above calculations
                request.setAttribute("gpsKm", gpsKmVal);
                request.setAttribute("gpsHm", temp[1]);
            } else {
                request.setAttribute("gpsKm", 0);
                request.setAttribute("gpsHm", 0);

            }

            //Throttle Starts As On 12-12-2013
            String miscValue = tripBP.getMiscValue(vehicleTypeId);
            request.setAttribute("miscValue", miscValue);

            String[] temp1 = null;
            if (tripDetails != null && !"".equals(tripDetails)) {
                temp1 = tripDetails.split("~");
                request.setAttribute("customerName", temp1[0]);
                request.setAttribute("routeName", temp1[1]);
                request.setAttribute("tripStartDate", temp1[2]);
                request.setAttribute("tripStartTime", temp1[3]);
            }
            String tripInformation = tripBP.getTripDetails(tripSheetId);
            String driverCount = tripBP.getDriverCount(tripSheetId, tripTO);
            String tripAdvance = tripBP.getTripAdvance(tripSheetId, tripTO);
            String tripExpense = tripBP.getTripExpense(tripSheetId, tripTO);
            request.setAttribute("tripAdvance", tripAdvance);
            request.setAttribute("tripExpense", tripExpense);

            String[] temp = null;
            if (tripInformation != null && !"".equals(tripInformation)) {
                temp = tripInformation.split("~");
                request.setAttribute("totalRunKm", temp[0]);
                request.setAttribute("transitDays", temp[1]);
                request.setAttribute("tripRemarks", temp[2]);
            }
            float advance = 0.0f;
            float expense = 0.0f;
            advance = Float.parseFloat(tripAdvance);
            expense = Float.parseFloat(tripExpense);
            float amountDifference = 0.0f;
            if (advance > expense) {
                amountDifference = advance - expense;
            } else {
                amountDifference = expense - advance;
            }
            request.setAttribute("driverCount", driverCount);
            request.setAttribute("tripAdvance", tripAdvance);
            request.setAttribute("tripExpense", tripExpense);
            request.setAttribute("amountDifference", amountDifference);
            ArrayList tripAdvanceDetails = new ArrayList();
            tripAdvanceDetails = tripBP.getTripAdvanceDetails(tripSheetId);

            request.setAttribute("tripAdvanceDetails", tripAdvanceDetails);
            ArrayList tripFuelDetails = new ArrayList();
            tripFuelDetails = tripBP.getTripFuelDetails(tripSheetId);
            request.setAttribute("tripFuelDetails", tripFuelDetails);
            ArrayList tripExpenseDetails = new ArrayList();
            tripExpenseDetails = tripBP.getTripExpenseDetails(tripSheetId);
            request.setAttribute("tripExpenseDetails", tripExpenseDetails);
            ArrayList tripUnPackDetails = new ArrayList();
            tripUnPackDetails = tripBP.getTripUnPackDetails(tripTO);
            request.setAttribute("tripUnPackDetails", tripUnPackDetails);

            ArrayList tripDetailsNew = new ArrayList();
            tripDetailsNew = tripBP.getTripDetails(tripTO);
            ArrayList tripPointDetails = new ArrayList();
            ArrayList tripPreStartDetails = new ArrayList();
            ArrayList tripStartDetails = new ArrayList();
            ArrayList tripEndDetails = new ArrayList();
            tripPointDetails = tripBP.getTripPointDetails(tripTO);
            tripPreStartDetails = tripBP.getPreStartTripDetails(tripTO);
            tripStartDetails = tripBP.getStartedTripDetails(tripTO);
            tripEndDetails = tripBP.getEndTripDetails(tripTO);

            ArrayList expiryDateDetails = new ArrayList();
            expiryDateDetails = tripBP.getExpiryDateDetails(tripTO);
            request.setAttribute("expiryDateDetails", expiryDateDetails);

            // viewTripSettlement Function Add this arrayList......
            TripTO tTO = new TripTO();
            ArrayList bpclTransactionHistory = new ArrayList();
            double bpclAmount = 0.0f;
//            bpclTransactionHistory = tripBP.getBPCLTransactionHistory(tripTO);
//            if (bpclTransactionHistory.size() > 0) {
//                request.setAttribute("bpclTransactionHistory", bpclTransactionHistory);
//                Iterator itr3 = bpclTransactionHistory.iterator();
//                while (itr3.hasNext()) {
//                    tTO = (TripTO) itr3.next();
//                    bpclAmount += (Double) Double.parseDouble(tTO.getAmount());
//                }
//
//            } else {
//                bpclAmount = 0;
//            }
            request.setAttribute("bpclAmount", bpclAmount);

            ArrayList tripAdvanceDetailsStatus = new ArrayList();
            tripAdvanceDetailsStatus = tripBP.getTripAdvanceDetailsStatus(tripSheetId);

            request.setAttribute("tripAdvanceDetailsStatus", tripAdvanceDetailsStatus);
            request.setAttribute("tripDetails", tripDetails);
            request.setAttribute("tripPointDetails", tripPointDetails);
            request.setAttribute("tripPreStartDetails", tripPreStartDetails);
            request.setAttribute("tripStartDetails", tripStartDetails);
            request.setAttribute("tripEndDetails", tripEndDetails);
            request.setAttribute("tripDetails", tripDetailsNew);

            ArrayList vehicleChangeAdvanceDetails = new ArrayList();
            vehicleChangeAdvanceDetails = tripBP.getVehicleChangeTripAdvanceDetails(tripSheetId);
            request.setAttribute("vehicleChangeAdvanceDetails", vehicleChangeAdvanceDetails);
            request.setAttribute("vehicleChangeAdvanceDetailsSize", vehicleChangeAdvanceDetails.size());

            String tripUnclearedBalance = "";
            tripUnclearedBalance = tripBP.getTripUnclearedBalance(tripTO);
            request.setAttribute("tripUnclearedBalance", tripUnclearedBalance);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method is used to Save Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView saveDriverSettlement(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String tripSheetId = "";
        String vehicleId = "";
        String menuPath = "Operation  >>  Trip Settlement ";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            int userId = (Integer) session.getAttribute("userId");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Trip Settlement";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
                request.setAttribute("tripSheetId", tripCommand.getTripSheetId());
                tripSheetId = tripCommand.getTripSheetId();
            }
            if (tripCommand.getVehicleId() != null && !tripCommand.getVehicleId().equals("")) {
                tripTO.setVehicleId(tripCommand.getVehicleId());
                request.setAttribute("vehicleId", tripCommand.getVehicleId());
                vehicleId = tripCommand.getVehicleId();
            }
            System.out.println("the tripSheetId is" + tripCommand.getTripSheetId());

            String tripDetails = tripBP.getTripDetails(tripSheetId);

            //Throttle Starts As On 12-12-2013
            tripTO.setTripId(tripTO.getTripSheetId());
            String vehicleTypeId = "";
            TripTO tripTO1 = new TripTO();
            ArrayList totalExpenseDetails = new ArrayList();
            totalExpenseDetails = tripBP.getTotalExpenseDetails(tripTO);
            Iterator itr1 = totalExpenseDetails.iterator();
            while (itr1.hasNext()) {
                tripTO1 = (TripTO) itr1.next();
                request.setAttribute("tollAmount", tripTO1.getTollAmount());
                request.setAttribute("driverBatta", tripTO1.getDriverBatta());
                request.setAttribute("driverIncentive", tripTO1.getDriverIncentive());
                request.setAttribute("fuelPrice", tripTO1.getFuelPrice());
                request.setAttribute("milleage", tripTO1.getMilleage());
                request.setAttribute("reeferConsumption", tripTO1.getReeferConsumption());
                request.setAttribute("vehicleTypeId", tripTO1.getVehicleTypeId());
                vehicleTypeId = tripTO1.getVehicleTypeId();
            }

            TripTO tpTO = new TripTO();
            ArrayList tripSheetDetails = new ArrayList();
            tripSheetDetails = tripBP.getEndTripSheetDetails(tripTO);
            Iterator itr = tripSheetDetails.iterator();
            while (itr.hasNext()) {
                tpTO = (TripTO) itr.next();
                request.setAttribute("tripSheetId", tpTO.getTripSheetId());
                request.setAttribute("cNotes", tpTO.getcNotes());
                request.setAttribute("vehicleNo", tpTO.getVehicleNo());
                request.setAttribute("tripCode", tpTO.getTripCode());
                request.setAttribute("startDate", tpTO.getTripScheduleDate());
                request.setAttribute("startTime", tpTO.getTripScheduleTime());
                request.setAttribute("estimatedExpense", tpTO.getOrderExpense());
                request.setAttribute("rcmExpense", tpTO.getRcmExpense());
                request.setAttribute("origin", tpTO.getOrigin());
                request.setAttribute("destination", tpTO.getDestination());
                request.setAttribute("tripEndDate", tpTO.getTripEndDate());
                request.setAttribute("tripEndTime", tpTO.getTripEndTime());
                request.setAttribute("startKm", tpTO.getStartKm());
                request.setAttribute("startHm", tpTO.getStartHm());
                request.setAttribute("endKm", tpTO.getEndKm());
                request.setAttribute("endHm", tpTO.getEndHm());
                request.setAttribute("runKm", tpTO.getRunKm());
                request.setAttribute("runHm", tpTO.getRunHm());
                request.setAttribute("totaldays", tpTO.getTotalDays());
                request.setAttribute("totalHours", tpTO.getDurationHours());
                request.setAttribute("driverCount", tpTO.getDriverCount());
                request.setAttribute("extraExpenseValue", tpTO.getExtraExpenseValue());
                String[] temp = null;
                if (!"".equals(tpTO.getDriverName()) && tpTO.getDriverName().contains(",")) {
                    temp = tpTO.getDriverName().split(",");
                    request.setAttribute("driverName1", temp[0]);
                    request.setAttribute("driverName2", temp[1]);
                } else {
                    request.setAttribute("driverName1", tpTO.getDriverName());
                }
                request.setAttribute("driverName", tpTO.getDriverName());
            }
            request.setAttribute("tripSheetDetails", tripSheetDetails);
            String tripGpsKm = tripBP.getGpsKm(tripSheetId);

            if (tripGpsKm != null) {
                String[] temp = tripGpsKm.split("-");
                String gpsKmVal = "0";
                if (!"0".equals(temp[0])) {
                    gpsKmVal = temp[0];
                }
                request.setAttribute("gpsKm", "" + gpsKmVal);
                //use above calculations
                request.setAttribute("gpsKm", gpsKmVal);
                request.setAttribute("gpsHm", temp[1]);
            } else {
                request.setAttribute("gpsKm", 0);
                request.setAttribute("gpsHm", 0);

            }

            //Throttle Starts As On 12-12-2013
            String miscValue = tripBP.getMiscValue(vehicleTypeId);
            request.setAttribute("miscValue", miscValue);

            String[] temp1 = null;
            if (tripDetails != null && !"".equals(tripDetails)) {
                temp1 = tripDetails.split("~");
                request.setAttribute("customerName", temp1[0]);
                request.setAttribute("routeName", temp1[1]);
                request.setAttribute("tripStartDate", temp1[2]);
                request.setAttribute("tripStartTime", temp1[3]);
            }
            String tripInformation = tripBP.getTripDetails(tripSheetId);
            String driverCount = tripBP.getDriverCount(tripSheetId, tripTO);
            String tripAdvance = tripBP.getTripAdvance(tripSheetId, tripTO);
            String tripExpense = tripBP.getTripExpense(tripSheetId, tripTO);
            request.setAttribute("tripAdvance", tripAdvance);
            request.setAttribute("tripExpense", tripExpense);

            String[] temp = null;
            if (tripInformation != null && !"".equals(tripInformation)) {
                temp = tripInformation.split("~");
                request.setAttribute("totalRunKm", temp[0]);
                request.setAttribute("transitDays", temp[1]);
                request.setAttribute("tripRemarks", temp[2]);
            }
            float advance = 0.0f;
            float expense = 0.0f;
            advance = Float.parseFloat(tripAdvance);
            expense = Float.parseFloat(tripExpense);
            float amountDifference = 0.0f;
            if (advance > expense) {
                amountDifference = advance - expense;
            } else {
                amountDifference = expense - advance;
            }
            request.setAttribute("driverCount", driverCount);
            request.setAttribute("tripAdvance", tripAdvance);
            request.setAttribute("tripExpense", tripExpense);
            request.setAttribute("amountDifference", amountDifference);
            ArrayList tripAdvanceDetails = new ArrayList();
            tripAdvanceDetails = tripBP.getTripAdvanceDetails(tripSheetId);

            request.setAttribute("tripAdvanceDetails", tripAdvanceDetails);
            ArrayList tripFuelDetails = new ArrayList();
            tripFuelDetails = tripBP.getTripFuelDetails(tripSheetId);
            request.setAttribute("tripFuelDetails", tripFuelDetails);
            ArrayList tripExpenseDetails = new ArrayList();
            tripExpenseDetails = tripBP.getTripExpenseDetails(tripSheetId);
            request.setAttribute("tripExpenseDetails", tripExpenseDetails);

            String tripStartingBalance = request.getParameter("tripStartingBalance");
            String gpsKm = request.getParameter("gpsKm");
            String gpsHm = request.getParameter("gpsHm");
            String totalRunKm = request.getParameter("totalRunKm");
            String totalDays = request.getParameter("totalDays");
            String totalRefeerHours = request.getParameter("totalRefeerHours");
            String fuelPrice = request.getParameter("fuelPrice");
            String milleage = request.getParameter("milleage");
            String tripDieselConsume = request.getParameter("tripDieselConsume");
            String vehicleDieselConsume = request.getParameter("vehicleDieselConsume");
            String reeferDieselConsume = request.getParameter("reeferDieselConsume");
            request.setAttribute("tripDieselConsume", tripDieselConsume);
            request.setAttribute("vehicleDieselConsume", vehicleDieselConsume);
            request.setAttribute("reeferDieselConsume", reeferDieselConsume);
            String tripRcmAllocation = request.getParameter("tripRcmAllocation");
            String totalTripAdvance = request.getParameter("totalTripAdvance");
            String totalTripMisc = request.getParameter("totalTripMisc");
            String totalTripBhatta = request.getParameter("totalTripBhatta");
            String tripExtraExpense = request.getParameter("tripExtraExpense");
            String totalTripExpense = request.getParameter("totalTripExpense");
            String tripBalance = request.getParameter("tripBalance");
            String tripEndBalance = request.getParameter("tripEndBalance");
            String settlementRemarks = request.getParameter("settlementRemarks");
            String lrNumber = request.getParameter("lrNumber");
            String paymentMode = request.getParameter("paymentMode");
            String bpclTransactionAmount = request.getParameter("bpclAmount");
            String rcmExpense = request.getParameter("rcmExpense");
            String estimatedExpense = request.getParameter("estimatedExpense");
            request.setAttribute("settlementRemarks", settlementRemarks);
            request.setAttribute("lrNumber", lrNumber);
            request.setAttribute("amount", tripEndBalance);
            request.setAttribute("paymentMode", paymentMode);
            System.out.println("lrnumber" + lrNumber);

            TripTO tTO = new TripTO();
            ArrayList bpclTransactionHistory = new ArrayList();
            double bpclAmount = 0.0f;
//            bpclTransactionHistory = tripBP.getBPCLTransactionHistory(tripTO);
//            if (bpclTransactionHistory.size() > 0) {
//                request.setAttribute("bpclTransactionHistory", bpclTransactionHistory);
//                Iterator itr3 = bpclTransactionHistory.iterator();
//                while (itr3.hasNext()) {
//                    tTO = (TripTO) itr3.next();
//                    bpclAmount += (Double) Double.parseDouble(tTO.getAmount());
//                }
//
//            } else {
//                bpclAmount = 0;
//            }
            request.setAttribute("bpclAmount", bpclAmount);

            int insertTripSettlement = tripBP.insertSettlement(tripSheetId, estimatedExpense, bpclTransactionAmount, rcmExpense, vehicleDieselConsume, reeferDieselConsume, tripStartingBalance, gpsKm, gpsHm, totalRunKm, totalDays, totalRefeerHours, milleage, tripDieselConsume, tripRcmAllocation, totalTripAdvance, totalTripMisc, totalTripBhatta, totalTripExpense, tripBalance, tripEndBalance, settlementRemarks, lrNumber, paymentMode, fuelPrice, tripExtraExpense, userId, tripTO);
            int insertTripDriverSettlementDetails = 0;
            insertTripDriverSettlementDetails = tripBP.insertTripDriverSettlementDetails(tripSheetId, estimatedExpense, bpclTransactionAmount, rcmExpense, vehicleDieselConsume, reeferDieselConsume, tripStartingBalance, gpsKm, gpsHm, totalRunKm, totalDays, totalRefeerHours, milleage, tripDieselConsume, tripRcmAllocation, totalTripAdvance, totalTripMisc, totalTripBhatta, totalTripExpense, tripBalance, tripEndBalance, settlementRemarks, paymentMode, fuelPrice, tripExtraExpense, userId, tripTO);
            /////////////////////////////////////////////Email part///////////////////////
            String to = "";
            String cc = "";
            String smtp = "";
            int emailPort = 0;
            String frommailid = "";
            String password = "";
            String activitycode = "EMTRP1";

            ArrayList emaildetails = new ArrayList();
            emaildetails = tripBP.getEmailDetails(activitycode);

            String emailString = tripBP.getTripEmails(tripSheetId, "14");
            System.out.println("emailString:" + emailString);
            String[] emailTemp = emailString.split("~");
            int emailTempLenth = emailTemp.length;
            System.out.println("emailTempLenth: " + emailTempLenth);

            if (emailTempLenth > 0) {
                to = emailTemp[0];
            }
            if (emailTempLenth > 1) {
                cc = emailTemp[1];
            }

            itr1 = emaildetails.iterator();

            if (itr1.hasNext()) {
                tripTO = new TripTO();
                tripTO = (TripTO) itr1.next();
                smtp = tripTO.getSmtp();
                emailPort = Integer.parseInt(tripTO.getPort());
                frommailid = tripTO.getEmailId();
                password = tripTO.getPassword();
            }
            String cNotesEmail = request.getParameter("cNotesEmail");
            String vehicleNoEmail = request.getParameter("vehicleNoEmail");
            String tripCodeEmail = request.getParameter("tripCodeEmail");
            String customerNameEmail = request.getParameter("customerNameEmail");
            String routeInfoEmail = request.getParameter("routeInfoEmail");

            String emailFormat = "<html>"
                    + "<body>"
                    + "<p><b>Hi, <br><br>Trip Settled For " + tripCodeEmail + "</b></p>"
                    + "<br> Customer:" + customerNameEmail
                    + "<br> C Note No:" + cNotesEmail
                    + "<br> Route :" + routeInfoEmail
                    + "<br> Vehicle No :" + vehicleNoEmail
                    + "<br><br> Team BrattleFoods"
                    + "</body></html>";

            String subject = "Trip Settlement Done for Customer " + customerNameEmail + " Vehicle " + vehicleNoEmail + " Route " + routeInfoEmail;
            String content = emailFormat;

            if (!"".equals(to)) {
                int mailSendingId = 0;
                tripTO.setMailTypeId("2");
                tripTO.setMailSubjectTo(subject);
                tripTO.setMailSubjectCc(subject);
                tripTO.setMailSubjectBcc("");
                tripTO.setMailContentTo(content);
                tripTO.setMailContentCc(content);
                tripTO.setMailContentBcc("");
                tripTO.setMailIdTo(to);
                tripTO.setMailIdCc(cc);
                tripTO.setMailIdBcc("");
//                mailSendingId = tripBP.insertMailDetails(tripTO, tripTO.getUserId());
                new SendMail(smtp, emailPort, frommailid, password, subject, content, to, cc, userId).start();
            }

            /////////////////////////////////////////////Email part///////////////////////
            String print = request.getParameter("print");

            //insertSettlement = tripBP.insertSettlement(tripSheetId, totalTripAdvance, totalTripExpense, driverSettleAmount, driverReturnAmount, paymentMode, settlementRemarks, userId);
            if ("yes".equals(print)) {
                path = "content/trip/printSettlementVoucher.jsp";
            } else {
                path = "content/trip/viewTripSheet.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

//Nithya start 7 dec
    public ModelAndView handleViewPreTripSheet(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View Trip Sheet ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            tripTO.setUserId(userId);
            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
                request.setAttribute("tripSheetId", tripCommand.getTripSheetId());
            }

            String tripId = request.getParameter("tripId");
            tripTO.setTripId(tripTO.getTripSheetId());
            ArrayList expiryDateDetails = new ArrayList();
            expiryDateDetails = tripBP.getExpiryDateDetails(tripTO);
            request.setAttribute("expiryDateDetails", expiryDateDetails);

            ArrayList tripDetails = new ArrayList();
            ArrayList tripPointDetails = new ArrayList();
            tripDetails = tripBP.getTripDetails(tripTO);
            tripPointDetails = tripBP.getTripPointDetails(tripTO);

            request.setAttribute("tripDetails", tripDetails);
            request.setAttribute("tripPointDetails", tripPointDetails);
            ArrayList statusDetails = new ArrayList();
            statusDetails = tripBP.getTripStausDetails(tripTO);
            if (statusDetails.size() > 0) {
                request.setAttribute("statusDetails", statusDetails);
            }
            String tripSheetId = request.getParameter("tripId");
            ArrayList tripAdvanceDetails = new ArrayList();
            tripAdvanceDetails = tripBP.getTripAdvanceDetails(tripSheetId);
            request.setAttribute("tripAdvanceDetails", tripAdvanceDetails);

            path = "content/trip/preStartTripSheet.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView savePreTripSheet(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        ModelAndView mv = null;
        String menuPath = "Operation  >>  View Trip Sheet ";
        int insertStatus = 0;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            if (tripCommand.getPreStartDate() != null && !tripCommand.getPreStartDate().equals("")) {
                tripTO.setPreStartDate(tripCommand.getPreStartDate());
            }
            if (tripCommand.getPreOdometerReading() != null && !tripCommand.getPreOdometerReading().equals("")) {
                tripTO.setPreOdometerReading(tripCommand.getPreOdometerReading());
            }
            if (tripCommand.getPreTripRemarks() != null && !tripCommand.getPreTripRemarks().equals("")) {
                tripTO.setPreTripRemarks(tripCommand.getPreTripRemarks());
            }
            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
            }
            if (tripCommand.getPreStartHM() != null && !tripCommand.getPreStartHM().equals("")) {
                tripTO.setPreStartHM(tripCommand.getPreStartHM());
            }
            if (tripCommand.getTripPreStartHour() != null && !tripCommand.getTripPreStartHour().equals("")) {
                tripTO.setTripPreStartHour(tripCommand.getTripPreStartHour());
            }
            if (tripCommand.getTripPreStartMinute() != null && !tripCommand.getTripPreStartMinute().equals("")) {
                tripTO.setTripPreStartMinute(tripCommand.getTripPreStartMinute());
            }
//            if (tripCommand.getStatusId() != null && !tripCommand.getStatusId().equals("")) {
//                tripTO.setStatusId(tripCommand.getStatusId());
//            }
            tripTO.setStatusId("9"); //prestart stage

            insertStatus = tripBP.savePreTripSheetDetails(tripTO, userId);

            //enable advance payment
            String estimatedAdvancePerDay = request.getParameter("estimatedAdvancePerDay");
            Float tempAdv = 0.00F;
            int advValue = 0;
            if (estimatedAdvancePerDay != null && !"".equals(estimatedAdvancePerDay)) {
                tempAdv = Float.parseFloat(estimatedAdvancePerDay);
                Float remainValue = tempAdv % 100;
                int hundreds = (int) (tempAdv / 100);
                if (remainValue < 50) {
                    advValue = hundreds * 100;
                } else {
                    advValue = (hundreds * 100) + 100;
                }
            }
            tripTO.setEstimatedAdvancePerDay(advValue + "");
            tripTO.setUserId(userId);
            //adhoc finance advice turned off
            //insertStatus = tripBP.setAdvanceAdvice(tripTO);

            if (insertStatus != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Pre Trip Updated Successfully....");
            }
            path = "content/trip/preStartTripSheet.jsp";
            mv = handleViewTripSheet(request, response, command);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView handleViewStartTripSheetold(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View start Trip Sheet ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
                request.setAttribute("tripSheetId", tripCommand.getTripSheetId());
            }
            String previousTripEndTime = "";
            String previousTripEndTime1 = "";
            String[] temp1 = null;
            String[] temp2 = null;
            tripTO.setTripId(tripTO.getTripSheetId());

            ArrayList tripDetails = new ArrayList();
            ArrayList tripPointDetails = new ArrayList();
            ArrayList tripPackDetails = new ArrayList();

            tripPackDetails = tripBP.getTripPackDetails(tripTO);
            tripDetails = tripBP.getTripDetails(tripTO);
            tripPointDetails = tripBP.getTripPointDetails(tripTO);
            ArrayList tripPreStartDetails = new ArrayList();
            tripPreStartDetails = tripBP.getPreStartTripDetails(tripTO);
            ArrayList statusDetails = new ArrayList();
            statusDetails = tripBP.getTripStausDetails(tripTO);
            if (statusDetails.size() > 0) {
                request.setAttribute("statusDetails", statusDetails);
            }

            request.setAttribute("tripDetails", tripDetails);
            request.setAttribute("tripPointDetails", tripPointDetails);
            request.setAttribute("tripPreStartDetails", tripPreStartDetails);
            request.setAttribute("tripPackDetails", tripPackDetails);
            String tripSheetId = request.getParameter("tripId");
            ArrayList tripAdvanceDetails = new ArrayList();
            tripAdvanceDetails = tripBP.getTripAdvanceDetails(tripTO.getTripId());
            request.setAttribute("tripAdvanceDetails", tripAdvanceDetails);
            ArrayList tripAdvanceDetailsStatus = new ArrayList();
            tripAdvanceDetailsStatus = tripBP.getTripAdvanceDetailsStatus(tripTO.getTripId());

            ArrayList vehicleChangeAdvanceDetails = new ArrayList();
            vehicleChangeAdvanceDetails = tripBP.getVehicleChangeTripAdvanceDetails(tripTO.getTripId());
            request.setAttribute("vehicleChangeAdvanceDetails", vehicleChangeAdvanceDetails);
            request.setAttribute("vehicleChangeAdvanceDetailsSize", vehicleChangeAdvanceDetails.size());
            request.setAttribute("tripAdvanceDetailsStatus", tripAdvanceDetailsStatus);

            ArrayList expiryDateDetails = new ArrayList();
            expiryDateDetails = tripBP.getExpiryDateDetails(tripTO);
            request.setAttribute("expiryDateDetails", expiryDateDetails);
            String responsiveStatus = request.getParameter("respStatus");
            String tripType = request.getParameter("tripType");
            String statusId = request.getParameter("statusId");
            String previousTripOdoMeterReading = "";
            String[] temp = null;
            previousTripOdoMeterReading = tripBP.getPreviousTripsOdometerReading(tripTO.getTripId());
            System.out.println("previousTripOdoMeterReading = " + previousTripOdoMeterReading);

            if (previousTripOdoMeterReading != null && !previousTripOdoMeterReading.equals("")) {
                temp = previousTripOdoMeterReading.split("-");
                request.setAttribute("previousEndKm", temp[0]);
                request.setAttribute("previousEndHm", temp[1]);
            }
            request.setAttribute("tripType", tripType);
            request.setAttribute("statusId", statusId);

            previousTripEndTime = tripBP.getPreviousTripEndTime(tripTO.getTripId());
            System.out.println("previousTripDatetime = " + previousTripEndTime);
            if (previousTripEndTime != null && !previousTripEndTime.equals("")) {
                temp1 = previousTripEndTime.split("~");
                request.setAttribute("previousTripEndDate", temp1[0]);

                previousTripEndTime1 = temp1[1];
                request.setAttribute("previousDestinationId", temp1[2]);

            }
            if (previousTripEndTime1 != null && !previousTripEndTime.equals("")) {
                temp2 = previousTripEndTime1.split(":");
                request.setAttribute("previousTripEndHr", temp2[0]);
                request.setAttribute("previousTripEndMin", temp2[1]);

            }
            ArrayList loadDetails = new ArrayList();
            loadDetails = tripBP.getLoadDetails(tripTO);
            request.setAttribute("loadDetails", loadDetails);

            ArrayList lhcDetails = new ArrayList();
            lhcDetails = tripBP.getLhcDetails(tripTO);
            request.setAttribute("lhcDetails", lhcDetails);

            if (responsiveStatus != null && "Y".equals(responsiveStatus)) {
                path = "content/trip/startTripSheetResponsive.jsp";
            } else {
                path = "content/trip/startTripSheet.jsp";
            }

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewStartTripSheet(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View start Trip Sheet ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
                request.setAttribute("tripSheetId", tripCommand.getTripSheetId());
            }
            String previousTripEndTime = "";
            String previousTripEndTime1 = "";
            int UpdateSaveTripDetails = 0;
            String[] temp1 = null;
            String[] temp2 = null;
            String[] temp3 = null;
            tripTO.setTripId(tripTO.getTripSheetId());

            System.out.println("TripSheetId : " + tripTO.getTripSheetId());
            ArrayList tripDetails = new ArrayList();
            ArrayList tripPointDetails = new ArrayList();
            ArrayList tripPackDetails = new ArrayList();

            String roleId = "" + (Integer) session.getAttribute("RoleId");
            request.setAttribute("roleId", roleId);

            tripPackDetails = tripBP.getTripPackDetails(tripTO);
            tripDetails = tripBP.getTripDetails(tripTO);
            tripPointDetails = tripBP.getTripPointDetails(tripTO);
            ArrayList tripPreStartDetails = new ArrayList();
            tripPreStartDetails = tripBP.getPreStartTripDetails(tripTO);
            ArrayList statusDetails = new ArrayList();
            statusDetails = tripBP.getTripStausDetails(tripTO);
            if (statusDetails.size() > 0) {
                request.setAttribute("statusDetails", statusDetails);
            }

            request.setAttribute("UpdateSaveTripDetails", UpdateSaveTripDetails);
            System.out.println("UpdateSaveTripDetails" + request.getParameter("UpdateSaveTripDetails"));
            request.setAttribute("tripDetails", tripDetails);
            request.setAttribute("tripPointDetails", tripPointDetails);
            request.setAttribute("tripPreStartDetails", tripPreStartDetails);
            request.setAttribute("tripPackDetails", tripPackDetails);
            String tripSheetId = request.getParameter("tripId");
            ArrayList tripAdvanceDetails = new ArrayList();
            tripAdvanceDetails = tripBP.getTripAdvanceDetails(tripTO.getTripId());
            request.setAttribute("tripAdvanceDetails", tripAdvanceDetails);
            ArrayList tripAdvanceDetailsStatus = new ArrayList();
            tripAdvanceDetailsStatus = tripBP.getTripAdvanceDetailsStatus(tripTO.getTripId());

            ArrayList vehicleChangeAdvanceDetails = new ArrayList();
            vehicleChangeAdvanceDetails = tripBP.getVehicleChangeTripAdvanceDetails(tripTO.getTripId());
            request.setAttribute("vehicleChangeAdvanceDetails", vehicleChangeAdvanceDetails);
            request.setAttribute("vehicleChangeAdvanceDetailsSize", vehicleChangeAdvanceDetails.size());
            request.setAttribute("tripAdvanceDetailsStatus", tripAdvanceDetailsStatus);

            ArrayList expiryDateDetails = new ArrayList();
            expiryDateDetails = tripBP.getExpiryDateDetails(tripTO);
            request.setAttribute("expiryDateDetails", expiryDateDetails);
            String responsiveStatus = request.getParameter("respStatus");
            String tripType = request.getParameter("tripType");
            String statusId = request.getParameter("statusId");
            String previousTripOdoMeterReading = "";
            String[] temp = null;
            previousTripOdoMeterReading = tripBP.getPreviousTripsOdometerReading(tripTO.getTripId());
            System.out.println("previousTripOdoMeterReading = " + previousTripOdoMeterReading);

            if (previousTripOdoMeterReading != null && !previousTripOdoMeterReading.equals("")) {
                temp = previousTripOdoMeterReading.split("-");
                request.setAttribute("previousEndKm", temp[0]);
                request.setAttribute("previousEndHm", temp[1]);
            }
            request.setAttribute("tripType", tripType);
            request.setAttribute("statusId", statusId);

            previousTripEndTime = tripBP.getPreviousTripEndTime(tripTO.getTripId());
            System.out.println("previousTripDatetime = " + previousTripEndTime);
            if (previousTripEndTime != null && !previousTripEndTime.equals("")) {
                temp1 = previousTripEndTime.split("~");
                request.setAttribute("previousTripEndDate", temp1[0]);

                previousTripEndTime1 = temp1[1];
                request.setAttribute("previousDestinationId", temp1[2]);

            }
            if (previousTripEndTime1 != null && !previousTripEndTime.equals("")) {
                temp2 = previousTripEndTime1.split(":");
                request.setAttribute("previousTripEndHr", temp2[0]);
                request.setAttribute("previousTripEndMin", temp2[1]);

            }

            ArrayList loadDetails = new ArrayList();
            loadDetails = tripBP.getLoadDetails(tripTO);
            request.setAttribute("loadDetails", loadDetails);

            int Check1 = 0;
            int Check2 = 0;

            ArrayList lhcDetails = new ArrayList();
            lhcDetails = tripBP.getLhcDetails(tripTO);
            request.setAttribute("lhcDetails", lhcDetails);
            
            ArrayList driverDetails = new ArrayList();
            driverDetails = tripBP.getDriverDetails(tripTO);
            request.setAttribute("driverDetails", driverDetails);
            
            ArrayList movementDetails = new ArrayList();
            movementDetails = tripBP.getMovementDetails(tripTO);
            request.setAttribute("movementDetails", movementDetails);
            
            String[] appTime = null;
            TripTO trpTO1 = new TripTO();
            Iterator itr = movementDetails.iterator();
            while (itr.hasNext()) {
                trpTO1 = (TripTO) itr.next();
                appTime = trpTO1.getAppTime().split(":");
                request.setAttribute("appHour", appTime[0]);
                request.setAttribute("appMin", appTime[1]);

            }

            ArrayList triptouchpointDetails = new ArrayList();
            triptouchpointDetails = tripBP.getTriptouchpointDetails(tripTO);
            request.setAttribute("triptouchpointDetails", triptouchpointDetails);

            TripTO tripTO3 = new TripTO();
            Iterator itr3 = triptouchpointDetails.iterator();
            while (itr3.hasNext()) {
                tripTO3 = (TripTO) itr3.next();
                request.setAttribute("invoiceNo", tripTO3.getInvoiceNo());
                request.setAttribute("invoiceValue", tripTO3.getInvoiceValue());
                request.setAttribute("invoiceWeight", tripTO3.getPackageWeight());
                request.setAttribute("invoiceDate", tripTO3.getInvoiceDate());
                request.setAttribute("ewaybillNo", tripTO3.geteWayBillNo());

                Check1 = 1;
            }

            TripTO tripTO2 = new TripTO();
            Iterator itr2 = tripDetails.iterator();
            while (itr2.hasNext()) {
                tripTO2 = (TripTO) itr2.next();
                request.setAttribute("deviceId", tripTO2.getDeviceId());
                request.setAttribute("lrNumber", tripTO2.getLrNumber());
                request.setAttribute("sealNo", tripTO2.getSealNo());
                request.setAttribute("sealType", tripTO2.getSealNoType());

                if (!tripTO2.getDeviceId().equals("") && !tripTO2.getLrNumber().equals("") && !tripTO2.getSealNoType().equals("")) {
                    Check2 = 1;
                } else {
                    Check2 = 0;
                }
            }

            if (Check2 > 0 && Check1 > 0) {
                request.setAttribute("updateCheck", "0");
            } else {
                request.setAttribute("updateCheck", "1");
            }

            ArrayList tripDateDetails = new ArrayList();
            tripDateDetails = tripBP.getTripDateDetails(tripTO);
            request.setAttribute("tripDateDetails", tripDateDetails);

            Iterator itr1 = tripDateDetails.iterator();
            while (itr1.hasNext()) {
                tripTO = (TripTO) itr1.next();

                request.setAttribute("tripRemarks", tripTO.getRemarks());
                request.setAttribute("detention", tripTO.getOriginDetention());

                request.setAttribute("startDate", tripTO.getStartDate());
                temp = tripTO.getStartTime().split(":");
                request.setAttribute("startHours", temp[0]);
                request.setAttribute("startMin", temp[1]);

                request.setAttribute("vehicleactreportdate", tripTO.getVehicleactreportdate());
                temp1 = tripTO.getVehicleactreporthour().split(":");

                request.setAttribute("vehicleactreportHour", temp1[0]);
                request.setAttribute("vehicleactreportMin", temp1[1]);

                request.setAttribute("vehicleloadreportdate", tripTO.getVehicleloadreportdate());
                temp2 = tripTO.getVehicleloadreporthour().split(":");

                request.setAttribute("vehicleloadreportHour", temp2[0]);
                request.setAttribute("vehicleloadreportMin", temp2[1]);

                request.setAttribute("documentRecivedDate", tripTO.getDocumentRecivedDate());
                temp3 = tripTO.getDocumentRecivedHr().split(":");

                request.setAttribute("documentRecivedHour", temp3[0]);
                request.setAttribute("documentRecivedMin", temp3[1]);

            }

            if (responsiveStatus != null && "Y".equals(responsiveStatus)) {
                path = "content/trip/startTripSheetResponsive.jsp";
            } else {
                path = "content/trip/startTripSheet.jsp";
            }

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView updateEndTripSheetDuringClosure(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View start Trip Sheet ";
        int insertStatus = 0;
        int insertStatus1 = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
            }
            if (tripCommand.getVehicleId() != null && !tripCommand.getVehicleId().equals("")) {
                tripTO.setVehicleId(tripCommand.getVehicleId());
            }

            if (tripCommand.getEndHM() != null && !tripCommand.getEndHM().equals("")) {
                tripTO.setEndHM(tripCommand.getEndHM());
            }
            if (tripCommand.getEndOdometerReading() != null && !tripCommand.getEndOdometerReading().equals("")) {
                tripTO.setEndOdometerReading(tripCommand.getEndOdometerReading());
            }

            if (tripCommand.getTotalKM() != null && !tripCommand.getTotalKM().equals("")) {
                tripTO.setTotalKM(tripCommand.getTotalKM());
            }
            if (tripCommand.getTotalHrs() != null && !tripCommand.getTotalHrs().equals("")) {
                tripTO.setTotalHrs(tripCommand.getTotalHrs());
            }

            if (tripCommand.getEndDate() != null && !tripCommand.getEndDate().equals("")) {
                tripTO.setEndDate(tripCommand.getEndDate());
                System.out.println("tripCommand.getEndDate()" + tripCommand.getEndDate());
            }
            if (tripCommand.getTripEndHour() != null && !tripCommand.getTripEndHour().equals("")) {
                tripTO.setTripEndHour(tripCommand.getTripEndHour());
                System.out.println("tripCommand.getTripEndHour()" + tripCommand.getTripEndHour());
            }
            if (tripCommand.getTripEndMinute() != null && !tripCommand.getTripEndMinute().equals("")) {
                tripTO.setTripEndMinute(tripCommand.getTripEndMinute());
                System.out.println("tripCommand.getTripEndMinute()" + tripCommand.getTripEndMinute());
            }
            if (request.getParameter("totalDays1") != null && !request.getParameter("totalDays1").equals("")) {
                tripTO.setTotalDays(request.getParameter("totalDays1"));
                System.out.println("tripCommand.getTotalDays()" + request.getParameter("totalDays1"));
            }
            if (tripCommand.getTripTransitHour() != null && !tripCommand.getTripTransitHour().equals("")) {
                tripTO.setTripTransitHours(tripCommand.getTripTransitHour());
                System.out.println("tripCommand.getTripTransitHour()" + tripCommand.getTripTransitHour());
            }
            if (tripCommand.getSetteledHm() != null && !tripCommand.getSetteledHm().equals("")) {
                tripTO.setSetteledHm(tripCommand.getSetteledHm());
            }
            if (tripCommand.getSetteledKm() != null && !tripCommand.getSetteledKm().equals("")) {
                tripTO.setSetteledKm(tripCommand.getSetteledKm());
            }
            if (tripCommand.getSetteltotalKM() != null && !tripCommand.getSetteltotalKM().equals("")) {
                tripTO.setSetteltotalKM(tripCommand.getSetteltotalKM());
            }
            if (tripCommand.getSetteltotalHrs() != null && !tripCommand.getSetteltotalHrs().equals("")) {
                tripTO.setSetteltotalHrs(tripCommand.getSetteltotalHrs());
            }

            insertStatus = tripBP.updateEndTripSheetDuringClosure(tripTO, userId);

            if (insertStatus != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Trip End Details Updated Successfully....");
            }

            ArrayList tripsettelmentDetails = new ArrayList();
            tripsettelmentDetails = tripBP.tripsettelmentDetails(tripTO);
            if (tripsettelmentDetails.size() > 0) {
                request.setAttribute("tripsettelmentDetails", tripsettelmentDetails);
            }
            path = "content/trip/endTripSheet.jsp";
            mv = handleViewTripExpense(request, response, command);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView updateOverrideTripSheetDuringClosure(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View start Trip Sheet ";
        int insertStatus = 0;
        int insertStatus1 = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
            }
            if (tripCommand.getVehicleId() != null && !tripCommand.getVehicleId().equals("")) {
                tripTO.setVehicleId(tripCommand.getVehicleId());
            }

            if (tripCommand.getSetteledHm() != null && !tripCommand.getSetteledHm().equals("")) {
                tripTO.setSetteledHm(tripCommand.getSetteledHm());
            }
            if (tripCommand.getSetteledKm() != null && !tripCommand.getSetteledKm().equals("")) {
                tripTO.setSetteledKm(tripCommand.getSetteledKm());
            }
            if (tripCommand.getSetteltotalKM() != null && !tripCommand.getSetteltotalKM().equals("")) {
                tripTO.setSetteltotalKM(tripCommand.getSetteltotalKM());
            }
            if (tripCommand.getSetteltotalHrs() != null && !tripCommand.getSetteltotalHrs().equals("")) {
                tripTO.setSetteltotalHrs(tripCommand.getSetteltotalHrs());
            }
            if (tripCommand.getOverrideRemarks() != null && !tripCommand.getOverrideRemarks().equals("")) {
                tripTO.setOverrideTripRemarks(tripCommand.getOverrideRemarks());
            }

            if (tripCommand.getVehicleloadreportdate() != null && !"".equals(tripCommand.getVehicleloadreportdate())) {
                tripTO.setVehicleloadreportdate(tripCommand.getVehicleloadreportdate());
            }
            System.out.println("getVehicleloadreportdate" + tripCommand.getVehicleloadreportdate());
            if (tripCommand.getVehicleloadreporthour() != null && !"".equals(tripCommand.getVehicleloadreporthour())) {
                tripTO.setVehicleloadreporthour(tripCommand.getVehicleloadreporthour());
            }
            System.out.println("getVehicleloadreporthour" + tripCommand.getVehicleloadreporthour());
            if (tripCommand.getVehicleloadreportmin() != null && !"".equals(tripCommand.getVehicleloadreportmin())) {
                tripTO.setVehicleloadreportmin(tripCommand.getVehicleloadreportmin());
            }
            System.out.println("getVehicleloadreportmin" + tripCommand.getVehicleloadreportmin());
            if (tripCommand.getVehicleloadtemperature() != null && !"".equals(tripCommand.getVehicleloadtemperature())) {
                tripTO.setVehicleloadtemperature(tripCommand.getVehicleloadtemperature());
            }
            System.out.println("getVehicleloadtemperature" + tripCommand.getVehicleloadtemperature());

            insertStatus1 = tripBP.insertLoadingUnloadingDetailsClosure(tripTO, userId);
            insertStatus = tripBP.overrideEndTripSheetDuringClosure(tripTO, userId);

            if (insertStatus != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Trip End Details Updated Successfully....");
            }

            ArrayList getVehicleUnloadingDetails = new ArrayList();
            getVehicleUnloadingDetails = tripBP.getVehicleUnloadingDetails(tripTO);
            request.setAttribute("getVehicleUnloadingDetails", getVehicleUnloadingDetails);

            ArrayList tripsettelmentDetails = new ArrayList();
            tripsettelmentDetails = tripBP.tripsettelmentDetails(tripTO);
            if (tripsettelmentDetails.size() > 0) {
                request.setAttribute("tripsettelmentDetails", tripsettelmentDetails);
            }
            path = "content/trip/endTripSheet.jsp";
            mv = handleViewTripExpense(request, response, command);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView eetDuringClosure(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View start Trip Sheet ";
        int insertStatus = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());

            }
            if (tripCommand.getVehicleId() != null && !tripCommand.getVehicleId().equals("")) {
                tripTO.setVehicleId(tripCommand.getVehicleId());
            }

            String tripStartKm = request.getParameter("tripStartKm");
            String tripStartHm = request.getParameter("tripStartHm");
            String tripStartLoadTemp = request.getParameter("tripStartLoadTemp");
            String tripStartDate = request.getParameter("tripStartDate");
            String tripStartHour = request.getParameter("tripStartTimeHr");
            String tripStartMinutes = request.getParameter("tripStartTimeMin");

            String tripPlannedDate = request.getParameter("tripPlannedDate");
            String tripPlannedTimeHr = request.getParameter("tripPlannedTimeHr");
            String tripPlannedTimeMin = request.getParameter("tripPlannedTimeMin");
            String tripStartReportingDate = request.getParameter("tripStartReportingDate");

            tripTO.setStartKm(tripStartKm);
            tripTO.setStartHM(tripStartHm);
            tripTO.setLoadingTemperature(tripStartLoadTemp);
            tripTO.setStartDate(tripStartDate);
            tripTO.setStartTime(tripStartHour + ":" + tripStartMinutes + ":00");

            tripTO.setTripPlannedTime(tripPlannedTimeHr + ":" + tripPlannedTimeMin + ":00");
            tripTO.setTripPlannedDate(tripPlannedDate);
            tripTO.setTripStartReportingDate(tripStartReportingDate);

            insertStatus = tripBP.updateStartTripSheetDuringClosure(tripTO, userId);

            if (insertStatus != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Trip Start Details Updated Successfully....");
            }

            path = "content/trip/endTripSheet.jsp";
            mv = handleViewTripExpense(request, response, command);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView updateStartTripSheet(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        ModelAndView mv = null;
        tripCommand = command;
        String menuPath = "Operation  >>  Edit Start Trip Sheet ";
        String insertStatus = "";
        int insertStatus1 = 0;
        int insertStatus2 = 0;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            String tripSheetId = tripCommand.getTripSheetId();
            System.out.println("tripSheetId=" + tripSheetId);

            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
                tripTO.setTripId(tripCommand.getTripSheetId());
            }
            if (tripCommand.getStartDate() != null && !tripCommand.getStartDate().equals("")) {
                tripTO.setStartDate(tripCommand.getStartDate());
            }
            if (tripCommand.getStartOdometerReading() != null && !tripCommand.getStartOdometerReading().equals("")) {
                tripTO.setStartOdometerReading(tripCommand.getStartOdometerReading());
            }

            System.out.println("tripCommand.getStartOdometerReading() : " + tripCommand.getStartOdometerReading());

            if (tripCommand.getTripStartHour() != null && !tripCommand.getTripStartHour().equals("")) {
                tripTO.setTripStartHour(tripCommand.getTripStartHour());
            }
            if (tripCommand.getTripStartMinute() != null && !tripCommand.getTripStartMinute().equals("")) {
                tripTO.setTripStartMinute(tripCommand.getTripStartMinute());
            }
            if (tripCommand.getStartHM() != null && !tripCommand.getStartHM().equals("")) {
                tripTO.setStartHM(tripCommand.getStartHM());
            }
            if (tripCommand.getStartTripRemarks() != null && !tripCommand.getStartTripRemarks().equals("")) {
                tripTO.setStartTripRemarks(tripCommand.getStartTripRemarks());
            }
            if (tripCommand.getLrNumber() != null && !tripCommand.getLrNumber().equals("")) {
                tripTO.setLrNumber(tripCommand.getLrNumber());
            }
            if (tripCommand.getFuelLtr() != null && !tripCommand.getFuelLtr().equals("")) {
                tripTO.setFuelLtr(tripCommand.getFuelLtr());
            }
            //Muthu here starts...
            if (tripCommand.getSealNoType() != null && !tripCommand.getSealNoType().equals("")) {
                tripTO.setSealNoType(tripCommand.getSealNoType());
            }
            if (tripCommand.getSealNo() != null && !tripCommand.getSealNo().equals("")) {
                tripTO.setSealNo(tripCommand.getSealNo());
            }
            if (tripCommand.getVendorId() != null && !tripCommand.getVendorId().equals("")) {
                tripTO.setVendorId(tripCommand.getVendorId());
            }
            if (tripCommand.getVehicleTypeId() != null && !tripCommand.getVehicleTypeId().equals("")) {
                tripTO.setVehicleTypeId(tripCommand.getVehicleTypeId());
            }
            if (tripCommand.getVehicleId() != null && !tripCommand.getVehicleId().equals("")) {
                tripTO.setVehicleId(tripCommand.getVehicleId());
            }
            if (tripCommand.getVehicleNo() != null && !tripCommand.getVehicleNo().equals("")) {
                tripTO.setVehicleNo(tripCommand.getVehicleNo());
            }
//            if (tripCommand.getDriverName() != null && !tripCommand.getDriverName().equals("")) {
//                tripTO.setDriverName(tripCommand.getDriverName());
//            }
            if (tripCommand.getDriverMobile() != null && !tripCommand.getDriverMobile().equals("")) {
                tripTO.setDriverMobile(tripCommand.getDriverMobile());
            }
            if (tripCommand.getDlNumber() != null && !tripCommand.getDlNumber().equals("")) {
                tripTO.setDlNumber(tripCommand.getDlNumber());
            }
            if (tripCommand.geteWayBillNo() != null && !tripCommand.geteWayBillNo().equals("")) {
                tripTO.seteWayBillNo(tripCommand.geteWayBillNo());
            }
            if (tripCommand.getInvoiceNumber() != null && !tripCommand.getInvoiceNumber().equals("")) {
                tripTO.setInvoiceNumber(tripCommand.getInvoiceNumber());
            }
            if (tripCommand.getInvoiceValue() != null && !tripCommand.getInvoiceValue().equals("")) {
                tripTO.setInvoiceValue(tripCommand.getInvoiceValue());
            }
            if (tripCommand.getInvoiceDate() != null && !tripCommand.getInvoiceDate().equals("")) {
                tripTO.setInvoiceDate(tripCommand.getInvoiceDate());
            }
            if (tripCommand.getPackageWeight() != null && !tripCommand.getPackageWeight().equals("")) {
                tripTO.setPackageWeight(tripCommand.getPackageWeight());
            }

            //compute trip plan end date and time
            ///Arun  start//////////////
            if (tripCommand.getProductCodes() != null && !"".equals(tripCommand.getProductCodes())) {
                tripTO.setProductCodes(tripCommand.getProductCodes());
            }
            if (tripCommand.getProductNames() != null && !"".equals(tripCommand.getProductNames())) {
                tripTO.setProductNames(tripCommand.getProductNames());
            }
            if (tripCommand.getProductbatch() != null && !"".equals(tripCommand.getProductbatch())) {
                tripTO.setProductbatch(tripCommand.getProductbatch());
            }
            if (tripCommand.getPackagesNos() != null && !"".equals(tripCommand.getPackagesNos())) {
                tripTO.setPackagesNos(tripCommand.getPackagesNos());
            }
            if (tripCommand.getWeights() != null && !"".equals(tripCommand.getWeights())) {
                tripTO.setWeights(tripCommand.getWeights());
            }
            if (tripCommand.getProductuom() != null && !"".equals(tripCommand.getProductuom())) {
                tripTO.setProductuom(tripCommand.getProductuom());
            }

            if (tripCommand.getConsignmentId() != null && !"".equals(tripCommand.getConsignmentId())) {
                tripTO.setConsignmentId(tripCommand.getConsignmentId());
            }

            if (tripCommand.getVehicleactreportdate() != null && !"".equals(tripCommand.getVehicleactreportdate())) {
                tripTO.setVehicleactreportdate(tripCommand.getVehicleactreportdate());
            }
            if (tripCommand.getVehicleactreporthour() != null && !"".equals(tripCommand.getVehicleactreporthour())) {
                tripTO.setVehicleactreporthour(tripCommand.getVehicleactreporthour());
            }
            if (tripCommand.getVehicleactreportmin() != null && !"".equals(tripCommand.getVehicleactreportmin())) {
                tripTO.setVehicleactreportmin(tripCommand.getVehicleactreportmin());
            }
            if (tripCommand.getVehicleloadreportdate() != null && !"".equals(tripCommand.getVehicleloadreportdate())) {
                tripTO.setVehicleloadreportdate(tripCommand.getVehicleloadreportdate());
            }
            if (tripCommand.getVehicleloadreporthour() != null && !"".equals(tripCommand.getVehicleloadreporthour())) {
                tripTO.setVehicleloadreporthour(tripCommand.getVehicleloadreporthour());
            }
            if (tripCommand.getVehicleloadreportmin() != null && !"".equals(tripCommand.getVehicleloadreportmin())) {
                tripTO.setVehicleloadreportmin(tripCommand.getVehicleloadreportmin());
            }
            if (tripCommand.getVehicleloadtemperature() != null && !"".equals(tripCommand.getVehicleloadtemperature())) {
                tripTO.setVehicleloadtemperature(tripCommand.getVehicleloadtemperature());
            }
            if (tripCommand.getOriginDetention() != null && !"".equals(tripCommand.getOriginDetention())) {
                tripTO.setOriginDetention(tripCommand.getOriginDetention());
            }
            if (tripCommand.getCustomerId() != null && !"".equals(tripCommand.getCustomerId())) {
                tripTO.setCustomerId(tripCommand.getCustomerId());
            }

            if (tripCommand.getLrNumber() != null && !"".equals(tripCommand.getLrNumber())) {
                tripTO.setLrNumber(tripCommand.getLrNumber());
            }

            if (tripCommand.getDeviceId() != null && !tripCommand.getDeviceId().equals("")) {
                tripTO.setDeviceId(tripCommand.getDeviceId());
            }

            if (tripCommand.getLoadedpackages() != null && !"".equals(tripCommand.getLoadedpackages())) {
                tripTO.setLoadedpackages(tripCommand.getLoadedpackages());
            }

            if (tripCommand.getDocumentRecivedDate() != null && !tripCommand.getDocumentRecivedDate().equals("")) {
                tripTO.setDocumentRecivedDate(tripCommand.getDocumentRecivedDate());
            }

            if (tripCommand.getDocumentRecivedHr() != null && !tripCommand.getDocumentRecivedHr().equals("")) {
                tripTO.setDocumentRecivedHr(tripCommand.getDocumentRecivedHr());
            }
            if (tripCommand.getDocumentRecivedMi() != null && !tripCommand.getDocumentRecivedMi().equals("")) {
                tripTO.setDocumentRecivedMi(tripCommand.getDocumentRecivedMi());
            }

            String routeCourseId = request.getParameter("routeCourseId");
            tripTO.setRouteId(routeCourseId);
            ///Arun  start//////////////

            String estimatedTransitHours = request.getParameter("estimatedTransitHours");
            String estimatedTripEndDateTime = tripBP.getEstimatedTripEndDateTime(tripTO.getTripSheetId());
            String[] temp = estimatedTripEndDateTime.split("~");
            String tripPlanEndDate = "";
            String tripPlanEndTime = "";
            tripPlanEndDate = temp[0];
            tripPlanEndTime = temp[1];
            System.out.println("tripPlanEndDate:" + tripPlanEndDate);
            System.out.println("tripPlanEndTime:" + tripPlanEndTime);
            System.out.println("start date:" + tripTO.getStartDate());
            System.out.println("estimatedTransitHours:" + estimatedTransitHours);
            if ("00-00-0000".equals(tripPlanEndDate) || "".equals(tripPlanEndDate)) {
                if ("00:00:00".equals(tripPlanEndTime)) {
                    tripPlanEndTime = "";
                }
                DateFormat formatter;
                Date tripStartDate;
                formatter = new SimpleDateFormat("dd-MM-yyyy");
                tripStartDate = formatter.parse(tripTO.getStartDate());

                Calendar cal = Calendar.getInstance(); // creates calendar
                cal.setTime(tripStartDate); // sets calendar time/date
                int tripStartHours = Integer.parseInt(tripTO.getTripStartHour()) + (Integer.parseInt(tripTO.getTripStartMinute()) / 60);
                cal.add(Calendar.HOUR_OF_DAY, tripStartHours); // adds trip start hour
                cal.add(Calendar.HOUR_OF_DAY, (int) Float.parseFloat(estimatedTransitHours)); // adds transit hours
                Date tripPlanEndDateDt = cal.getTime(); // returns new date object, one hour in the future
                tripPlanEndDate = formatter.format(tripPlanEndDateDt);
                SimpleDateFormat sdf = new SimpleDateFormat("kk:mm:ss");
                tripPlanEndTime = sdf.format(tripPlanEndDateDt);
                if ("24:00:00".equals(tripPlanEndTime)) {
                    tripPlanEndTime = "00:00:00";
                }
            }

            //load details
            String[] loadOrderId = request.getParameterValues("loadOrderId");
            String[] pendingPkgs = request.getParameterValues("pendingPkgs");
            String[] pendingWeight = request.getParameterValues("pendingWeight");
            String[] loadedPkgs = request.getParameterValues("loadedPkgs");
            String[] loadedWeight = request.getParameterValues("loadedWeight");
            String totalPackages = request.getParameter("totalPackages");

            tripTO.setLoadOrderId(loadOrderId);
            tripTO.setPendingPkgs(pendingPkgs);
            tripTO.setPendingWeight(pendingWeight);
            tripTO.setLoadedPkgs(loadedPkgs);
            tripTO.setLoadedWeight(loadedWeight);
            tripTO.setTotalPackages(totalPackages);

            System.out.println("tripPlanEndDate:" + tripPlanEndDate);
            System.out.println("tripPlanEndTime:" + tripPlanEndTime);
            tripTO.setTripPlanEndDate(tripPlanEndDate);
            tripTO.setTripPlanEndTime(tripPlanEndTime);
            tripTO.setStatusId("8"); //trip started
            
            String driverName=request.getParameter("driverName");
            System.out.println("driverName==="+driverName);
            tripTO.setDriverName(driverName);
            String escortName=request.getParameter("escortName");
            System.out.println("escortName==="+escortName);
            tripTO.setEscortName(escortName);
            String escortMobile=request.getParameter("escortMobile");
            System.out.println("escortName==="+escortName);
            tripTO.setEscortMobile(escortMobile);
            
            String materialDesc=request.getParameter("materialDesc");
            System.out.println("materialDesc=="+materialDesc);
            tripTO.setMaterialDesc(materialDesc);
            String insuranceDetail=request.getParameter("insuranceDetail");
            System.out.println("insuranceDetail=="+insuranceDetail);
            tripTO.setInsuranceDetail(insuranceDetail);
            String appDate=request.getParameter("appDate");
            System.out.println("appDate=="+appDate);
            tripTO.setAppDate(appDate);
            String appHour=request.getParameter("appHour");
            System.out.println("appHour=="+appHour);
            tripTO.setAppHour(appHour);
            String appMin=request.getParameter("appMin");
            System.out.println("appMin=="+appMin);
            tripTO.setAppMin(appMin);
            String tatTime=request.getParameter("tatTime");
            System.out.println("tatTime=="+tatTime);
            tripTO.setTatTime(tatTime);

            insertStatus = tripBP.updateStartTripSheet(tripTO, userId);
            System.out.println("update trip sheet insert status===" + insertStatus);
            insertStatus1 = tripBP.insertLoadingUnloadingDetails(tripTO, userId);

            if (!insertStatus.equals("")) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Trip Updated Successfully....");
            }
//            
//            /////////////////////////////////////////////Email part///////////////////////
//            String to = "";
//            String cc = "";
//            String smtp = "";
//            int emailPort = 0;
//            String frommailid = "";
//            String password = "";
//            String activitycode = "EMTRP1";
//
//            ArrayList emaildetails = new ArrayList();
//            emaildetails = tripBP.getEmailDetails(activitycode);
//
//            String emailString = tripBP.getTripEmails(tripCommand.getTripSheetId(), tripTO.getStatusId());
//            System.out.println("emailString:" + emailString);
//            String[] emailTemp = emailString.split("~");
//            int emailTempLenth = emailTemp.length;
//            System.out.println("emailTempLenth: " + emailTempLenth);
//
//            if (emailTempLenth > 0) {
//                to = emailTemp[0];
//            }
//            if (emailTempLenth > 1) {
//                cc = emailTemp[1];
//            }
//
//            Iterator itr1 = emaildetails.iterator();
//
//            if (itr1.hasNext()) {
//                tripTO = new TripTO();
//                tripTO = (TripTO) itr1.next();
//                smtp = tripTO.getSmtp();
//                emailPort = Integer.parseInt(tripTO.getPort());
//                frommailid = tripTO.getEmailId();
//                password = tripTO.getPassword();
//            }
//            String vehicle = request.getParameter("vehicleno");
//
//            String cNotesEmail = request.getParameter("cNotesEmail");
//            String vehicleNoEmail = request.getParameter("vehicleNoEmail");
//            String tripCodeEmail = request.getParameter("tripCodeEmail");
//            String customerNameEmail = request.getParameter("customerNameEmail");
//            String routeInfoEmail = request.getParameter("routeInfoEmail");
//
////            tripTO.setcNotesEmail(cNotesEmail);
////            tripTO.setVehicleNoEmail(vehicleNoEmail);
////            tripTO.setTripCodeEmail(tripCodeEmail);
////            tripTO.setCustomerNameEmail(customerNameEmail);
////            tripTO.setRouteInfoEmail(routeInfoEmail);
//            String emailFormat = "<html>"
//                    + "<body>"
//                    + "<p><b>Hi, <br><br>Trip Started for the Vehicle " + vehicleNoEmail + " , CNote " + cNotesEmail + ". <br>"
//                    + "<br>Vehicle No:" + vehicleNoEmail
//                    + "<br>Trip Code:" + tripCodeEmail
//                    + "<br>Customer:" + customerNameEmail
//                    + "<br>Route:" + routeInfoEmail
//                    + "<br>Started Date:" + tripCommand.getStartDate() + " Time:" + tripCommand.getTripStartHour()
//                    + "<br><br>Team BrattleFoods"
//                    + "</body></html>";
//
//            String subject = "Trip Started for Customer " + customerNameEmail + " Vehicle " + vehicleNoEmail + " Route " + routeInfoEmail;
//            String content = emailFormat;
//            if (!"".equals(to)) {
//                int mailSendingId = 0;
//                tripTO.setMailTypeId("2");
//                tripTO.setMailSubjectTo(subject);
//                tripTO.setMailSubjectCc(subject);
//                tripTO.setMailSubjectBcc("");
//                tripTO.setMailContentTo(content);
//                tripTO.setMailContentCc(content);
//                tripTO.setMailContentBcc("");
//                tripTO.setMailIdTo(to);
//                tripTO.setMailIdCc(cc);
//                tripTO.setMailIdBcc("");
////                mailSendingId = tripBP.insertMailDetails(tripTO, userId);
//               // new SendMail(smtp, emailPort, frommailid, password, subject, content, to, cc, userId).start();
//            }

            /////////////////////////////////////////////Email part///////////////////////
            if (!insertStatus.equals("")) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Trip Started Successfully....");
            } else {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Trip Started Successfully....");
            }
            //path = "content/trip/startTripSheet.jsp";

            mv = handleViewStartTripSheet(request, response, command);

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
//        return new ModelAndView(path);
        return mv;
    }

    public ModelAndView viewTripPOD(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String tripSheetId = "";
        String menuPath = "Operation  >>  Edit Start Trip Sheet ";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            String roleId = "" + (Integer) session.getAttribute("RoleId");
            request.setAttribute("roleId", roleId);

            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
                request.setAttribute("tripSheetId", tripCommand.getTripSheetId());
            }
            if (tripCommand.getConsignmentOrderId() != null && !tripCommand.getConsignmentOrderId().equals("")) {
                tripTO.setOrderId(tripCommand.getConsignmentOrderId());
                request.setAttribute("consignmentOrderId", tripCommand.getConsignmentOrderId());
            }
            tripSheetId = tripCommand.getTripSheetId();
            System.out.println("the tripSheetId is" + tripCommand.getTripSheetId());

            ArrayList podDetails = new ArrayList();
            podDetails = tripBP.getTripPODDetails(tripSheetId);
            request.setAttribute("podDetails", podDetails);

            tripTO.setTripId(tripTO.getTripSheetId());

            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
                request.setAttribute("tripSheetId", tripCommand.getTripSheetId());
            }

            ArrayList startTripDetails = new ArrayList();
            startTripDetails = tripBP.getStartTripDetails(tripTO);
            request.setAttribute("startTripDetails", startTripDetails);
            ArrayList vehicleChangedTripDetails = new ArrayList();
            vehicleChangedTripDetails = tripBP.getVehicleChangeTripDetails(tripTO);
            if (vehicleChangedTripDetails.size() > 0) {
                request.setAttribute("vehicleChangedTripDetails", vehicleChangedTripDetails);
            }

            tripTO.setTripId(tripTO.getTripSheetId());

            ArrayList tripDetails = new ArrayList();
            ArrayList tripPointDetails = new ArrayList();
            ArrayList tripPreStartDetails = new ArrayList();
            ArrayList tripStartDetails = new ArrayList();
            ArrayList tripUnPackDetails = new ArrayList();
            ArrayList invoiceDetails = new ArrayList();
            ArrayList tripEndDetails = new ArrayList();

            tripDetails = tripBP.getTripDetails(tripTO);
            tripPointDetails = tripBP.getTripPointDetails(tripTO);
            tripPreStartDetails = tripBP.getPreStartTripDetails(tripTO);
            tripStartDetails = tripBP.getStartedTripDetails(tripTO);
            tripUnPackDetails = tripBP.getTripUnPackDetails(tripTO);
            invoiceDetails = tripBP.getTripInvoiceDetails(tripTO);
            tripEndDetails = tripBP.getEndTripDetails(tripTO);

            request.setAttribute("tripEndDetails", tripEndDetails);
            request.setAttribute("invoiceDetails", invoiceDetails);
            request.setAttribute("tripDetails", tripDetails);
            request.setAttribute("tripPointDetails", tripPointDetails);
            request.setAttribute("tripPreStartDetails", tripPreStartDetails);
            request.setAttribute("tripStartDetails", tripStartDetails);
            request.setAttribute("tripUnPackDetails", tripUnPackDetails);
            request.setAttribute("tripPointDetailsSize", tripPointDetails.size());

            ArrayList tripFuel = new ArrayList();
            tripFuel = tripBP.getTripFuel(tripTO);
            request.setAttribute("tripFuel", tripFuel);

            ArrayList wfuDetails = new ArrayList();
            wfuDetails = tripBP.getWfuDetails(tripTO);
            request.setAttribute("wfuDetails", wfuDetails);
            request.setAttribute("wfuDetailsSize", wfuDetails.size());

            ArrayList statusDetails = new ArrayList();
            statusDetails = tripBP.getTripStausDetails(tripTO);
            if (statusDetails.size() > 0) {
                request.setAttribute("statusDetails", statusDetails);
            }

            ArrayList tripAdvanceDetails = new ArrayList();
            tripAdvanceDetails = tripBP.getTripAdvanceDetails(tripTO.getTripSheetId());
            request.setAttribute("tripAdvanceDetails", tripAdvanceDetails);

            ArrayList viewPODDetails = new ArrayList();
            viewPODDetails = tripBP.getPODDetails(tripTO);
            if (viewPODDetails.size() > 0) {
                request.setAttribute("viewPODDetails", viewPODDetails);
            }
            ArrayList expiryDateDetails = new ArrayList();
            expiryDateDetails = tripBP.getExpiryDateDetails(tripTO);
            request.setAttribute("expiryDateDetails", expiryDateDetails);

            ArrayList tripAdvanceDetailsStatus = new ArrayList();
            tripAdvanceDetailsStatus = tripBP.getTripAdvanceDetailsStatus(tripTO.getTripSheetId());

            request.setAttribute("tripAdvanceDetailsStatus", tripAdvanceDetailsStatus);

            String responsiveStatus = request.getParameter("respStatus");
            String tripType = request.getParameter("tripType");
            String statusId = request.getParameter("statusId");
            request.setAttribute("tripType", tripType);
            request.setAttribute("statusId", statusId);

            ArrayList vehicleChangeAdvanceDetails = new ArrayList();
            vehicleChangeAdvanceDetails = tripBP.getVehicleChangeTripAdvanceDetails(tripTO.getTripSheetId());
            request.setAttribute("vehicleChangeAdvanceDetails", vehicleChangeAdvanceDetails);
            request.setAttribute("vehicleChangeAdvanceDetailsSize", vehicleChangeAdvanceDetails.size());

            ArrayList loadDetails = new ArrayList();
            loadDetails = tripBP.getLoadDetails(tripTO);
            request.setAttribute("loadDetails", loadDetails);

            ArrayList otherExpenseDetails = new ArrayList();
            otherExpenseDetails = tripBP.getOtherExpenseDetails(tripTO);
            if (otherExpenseDetails.size() > 0) {
                request.setAttribute("otherExpenseDetails", otherExpenseDetails);
                request.setAttribute("otherExpenseDetailsSize", otherExpenseDetails.size());
            }

            String otherExpenseDoneStatus = tripBP.getOtherExpenseDoneStatus(tripTO);
            request.setAttribute("otherExpenseDoneStatus", otherExpenseDoneStatus);

            String admin = request.getParameter("admin");
            request.setAttribute("admin", admin);

            ArrayList customerList = new ArrayList();
            customerList = tripBP.getTripCustomerList(tripTO);
            request.setAttribute("customerLists", customerList);

            ArrayList expenseDetails = new ArrayList();
            expenseDetails = tripBP.getExpenseDetails(tripTO);
            if (expenseDetails.size() > 0) {
                request.setAttribute("expenseDetails", expenseDetails);
            }
            ArrayList driverNameDetails = new ArrayList();
            driverNameDetails = tripBP.getDriverName(tripTO);
            if (driverNameDetails.size() > 0) {
                request.setAttribute("driverNameDetails", driverNameDetails);
            }

            path = "content/trip/tripPOD.jsp";

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveTripPodDetails(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  Edit Start Trip Sheet ";
        int insertStatus = 0;
        ModelAndView mv = null;
        String newFileName = "", actualFilePath = "";
        String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        int i = 0;
        int j = 0;
        int m = 0;
        int n = 0;
        int p = 0;
        int s = 0;
        String tripSheetId1 = "";
        String[] podRemarks1 = new String[10];
        String[] fileSaved = new String[10];
        String[] lrNumber1 = new String[10];
        String[] uploadedFileName = new String[10];
        String[] tempFilePath = new String[10];
        String[] city = new String[10];
        String consignorName1 = "";
        String consigneeName1 = "";
        String consignorAddress1 = "";
        String consigneeAddress1 = "";
        String consignorMobileNo1 = "";
        String consigneeMobileNo1 = "";
        String consignmentNote1 = "";
        String tripType = "";
        String statusId = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                System.out.println("this is tht");
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        Date now = new Date();
                        String date = now.getDate() + "" + (now.getMonth() + 1) + "" + (now.getYear() + 1900);
                        String time = now.getHours() + "" + now.getMinutes() + "" + now.getSeconds() + userId;
                        fPart = (FilePart) partObj;
                        uploadedFileName[j] = fPart.getFileName();

                        if (!"".equals(uploadedFileName[j]) && uploadedFileName[j] != null) {

                            // for ubuntu
//                            String[] splitFileName = uploadedFileName[j].split(".");
                            // for windows
                            String[] splitFileName = uploadedFileName[j].split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            fileSaved[j] = splitFileName[0] + date + time + "." + splitFileName[1];
                            fileName = fileSavedAs;

                            tempFilePath[j] = tempServerFilePath + "//" + fileSaved[j];
                            actualFilePath = actualServerFilePath + "//" + tempFilePath;
//                            for windows machines
//                            tempFilePath[j] = tempServerFilePath + "\\" + fileSaved[j];
//                            actualFilePath = actualServerFilePath + "\\" + tempFilePath;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(tempFilePath[j]));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath[j]));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
//                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
//                            String part1 = parts.replace("\\", "");
                        }

                        System.out.println("fileName..." + fileName);
                        j++;
                    } else if (partObj.isParam()) {
                        if (partObj.getName().equals("tripSheetId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            tripSheetId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("cityId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            city[s] = paramPart.getStringValue();
                            s++;
                        }
                        if (partObj.getName().equals("podRemarks")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            podRemarks1[n] = paramPart.getStringValue();
                            n++;
                        }

                        if (partObj.getName().equals("lrNumber")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            lrNumber1[p] = paramPart.getStringValue();
                            p++;
                        }
                        if (partObj.getName().equals("consignorName")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            consignorName1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("consigneeName")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            consigneeName1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("consignorAddress")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            consignorAddress1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("consigneeAddress")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            consigneeAddress1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("consignorMobileNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            consignorMobileNo1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("consigneeMobileNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            consigneeMobileNo1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("consignmentNote")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            consignmentNote1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("tripType")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            tripType = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("statusId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            statusId = paramPart.getStringValue();
                        }
                    }
                }
            }

            String file = "";
            String remarks = "";
            String lrNumber = "";
            String saveFile = "";
            String cityid = "";
            int status = 0;
            System.out.println("the pod filelength is" + lrNumber1.length);
            tripTO.setConsignorName(consignorName1);
            tripTO.setConsigneeName(consigneeName1);
            tripTO.setConsignorAddress(consignorAddress1);
            tripTO.setConsigneeAddress(consigneeAddress1);
            tripTO.setConsignorMobileNo(consignorMobileNo1);
            tripTO.setConsigneeMobileNo(consigneeMobileNo1);
            tripTO.setConsignmentNote(consignmentNote1);
            for (int x = 0; x < j; x++) {
                file = tempFilePath[x];
                remarks = podRemarks1[x];
                lrNumber = lrNumber1[x];
                saveFile = fileSaved[x];
                cityid = city[x];
                status = tripBP.saveTripPodDetails(file, tripSheetId1, cityid, remarks, lrNumber, saveFile, tripTO, userId);
            }
            if (status != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Trip POD Updated Successfully....");
            }
            ArrayList podDetails = new ArrayList();
            podDetails = tripBP.getTripPODDetails(tripSheetId1);
            request.setAttribute("podDetails", podDetails);
            request.setAttribute("tripType", tripType);
            request.setAttribute("statusId", statusId);

            path = "content/redirectPage.jsp";
            //mv = handleViewTripSheet(request, response, command);

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewEndTripSheet(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View start Trip Sheet ";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
                request.setAttribute("tripSheetId", tripCommand.getTripSheetId());
            }

            String roleId = "" + (Integer) session.getAttribute("RoleId");
            request.setAttribute("roleId", roleId);

            ArrayList startTripDetails = new ArrayList();
            startTripDetails = tripBP.getStartTripDetails(tripTO);
            request.setAttribute("startTripDetails", startTripDetails);
            ArrayList vehicleChangedTripDetails = new ArrayList();
            vehicleChangedTripDetails = tripBP.getVehicleChangeTripDetails(tripTO);
            if (vehicleChangedTripDetails.size() > 0) {
                request.setAttribute("vehicleChangedTripDetails", vehicleChangedTripDetails);
            }

            tripTO.setTripId(tripTO.getTripSheetId());

            ArrayList tripDetails = new ArrayList();
            ArrayList tripPointDetails = new ArrayList();
            ArrayList tripPreStartDetails = new ArrayList();
            ArrayList tripStartDetails = new ArrayList();
            ArrayList tripUnPackDetails = new ArrayList();
            ArrayList invoiceDetails = new ArrayList();

            tripDetails = tripBP.getTripDetails(tripTO);
            tripPointDetails = tripBP.getTripPointDetails(tripTO);
            tripPreStartDetails = tripBP.getPreStartTripDetails(tripTO);
            tripStartDetails = tripBP.getStartedTripDetails(tripTO);
            tripUnPackDetails = tripBP.getTripUnPackDetails(tripTO);
            invoiceDetails = tripBP.getTripInvoiceDetails(tripTO);

            request.setAttribute("invoiceDetails", invoiceDetails);
            request.setAttribute("tripDetails", tripDetails);
            request.setAttribute("tripPointDetails", tripPointDetails);
            request.setAttribute("tripPreStartDetails", tripPreStartDetails);
            request.setAttribute("tripStartDetails", tripStartDetails);
            request.setAttribute("tripUnPackDetails", tripUnPackDetails);
            request.setAttribute("tripPointDetailsSize", tripPointDetails.size());

            ArrayList tripFuel = new ArrayList();
            tripFuel = tripBP.getTripFuel(tripTO);
            request.setAttribute("tripFuel", tripFuel);

            ArrayList wfuDetails = new ArrayList();
            wfuDetails = tripBP.getWfuDetails(tripTO);
            request.setAttribute("wfuDetails", wfuDetails);
            request.setAttribute("wfuDetailsSize", wfuDetails.size());

            ArrayList statusDetails = new ArrayList();
            statusDetails = tripBP.getTripStausDetails(tripTO);
            if (statusDetails.size() > 0) {
                request.setAttribute("statusDetails", statusDetails);
            }
            String tripSheetId = request.getParameter("tripId");
            ArrayList tripAdvanceDetails = new ArrayList();
            tripAdvanceDetails = tripBP.getTripAdvanceDetails(tripTO.getTripSheetId());
            request.setAttribute("tripAdvanceDetails", tripAdvanceDetails);

            ArrayList viewPODDetails = new ArrayList();
            viewPODDetails = tripBP.getPODDetails(tripTO);
            if (viewPODDetails.size() > 0) {
                request.setAttribute("viewPODDetails", viewPODDetails);
            }
            ArrayList expiryDateDetails = new ArrayList();
            expiryDateDetails = tripBP.getExpiryDateDetails(tripTO);
            request.setAttribute("expiryDateDetails", expiryDateDetails);

            ArrayList tripAdvanceDetailsStatus = new ArrayList();
            tripAdvanceDetailsStatus = tripBP.getTripAdvanceDetailsStatus(tripTO.getTripSheetId());

            request.setAttribute("tripAdvanceDetailsStatus", tripAdvanceDetailsStatus);

            String responsiveStatus = request.getParameter("respStatus");
            String tripType = request.getParameter("tripType");
            String statusId = request.getParameter("statusId");
            request.setAttribute("tripType", tripType);
            request.setAttribute("statusId", statusId);

            ArrayList vehicleChangeAdvanceDetails = new ArrayList();
            vehicleChangeAdvanceDetails = tripBP.getVehicleChangeTripAdvanceDetails(tripTO.getTripSheetId());
            request.setAttribute("vehicleChangeAdvanceDetails", vehicleChangeAdvanceDetails);
            request.setAttribute("vehicleChangeAdvanceDetailsSize", vehicleChangeAdvanceDetails.size());

            ArrayList loadDetails = new ArrayList();
            loadDetails = tripBP.getLoadDetails(tripTO);
            request.setAttribute("loadDetails", loadDetails);

            ArrayList otherExpenseDetails = new ArrayList();
            otherExpenseDetails = tripBP.getOtherExpenseDetails(tripTO);
            if (otherExpenseDetails.size() > 0) {
                request.setAttribute("otherExpenseDetails", otherExpenseDetails);
                request.setAttribute("otherExpenseDetailsSize", otherExpenseDetails.size());
            }

            String otherExpenseDoneStatus = tripBP.getOtherExpenseDoneStatus(tripTO);
            request.setAttribute("otherExpenseDoneStatus", otherExpenseDoneStatus);

            String admin = request.getParameter("admin");
            request.setAttribute("admin", admin);

            ArrayList customerList = new ArrayList();
            customerList = tripBP.getTripCustomerList(tripTO);
            request.setAttribute("customerLists", customerList);

            ArrayList expenseDetails = new ArrayList();
            expenseDetails = tripBP.getExpenseDetails(tripTO);
            if (expenseDetails.size() > 0) {
                request.setAttribute("expenseDetails", expenseDetails);
            }
            ArrayList driverNameDetails = new ArrayList();
            driverNameDetails = tripBP.getDriverName(tripTO);
            if (driverNameDetails.size() > 0) {
                request.setAttribute("driverNameDetails", driverNameDetails);
            }

            if (responsiveStatus != null && "Y".equals(responsiveStatus)) {
                path = "content/trip/endTripSheetResponsive.jsp";
            } else {
                path = "content/trip/endTripSheet.jsp";
            }

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView updateEndTripSheet(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View start Trip Sheet ";
        int insertStatus = 0;
        int insertStatus1 = 0;
        int insertStatus2 = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            if (tripCommand.getNoticeNo() != null && !"".equals(tripCommand.getNoticeNo())) {
                tripTO.setNoticeNo(tripCommand.getNoticeNo());
            }
            if (tripCommand.getNatureOfLoss() != null && !"".equals(tripCommand.getNatureOfLoss())) {
                tripTO.setNatureOfLoss(tripCommand.getNatureOfLoss());
            }
            if (tripCommand.getActualWeight() != null && !"".equals(tripCommand.getActualWeight())) {
                tripTO.setActualWeight(tripCommand.getActualWeight());
            }
            if (tripCommand.getDeliveredWeight() != null && !"".equals(tripCommand.getDeliveredWeight())) {
                tripTO.setDeliveredWeight(tripCommand.getDeliveredWeight());
            }
            if (tripCommand.getOutwardCondition() != null && !"".equals(tripCommand.getOutwardCondition())) {
                tripTO.setOutwardCondition(tripCommand.getOutwardCondition());
            }
            if (tripCommand.getDetailsOfFacts() != null && !"".equals(tripCommand.getDetailsOfFacts())) {
                tripTO.setDetailsOfFacts(tripCommand.getDetailsOfFacts());
            }
            if (tripCommand.getClaimValue() != null && !"".equals(tripCommand.getClaimValue())) {
                tripTO.setClaimValue(tripCommand.getClaimValue());
            }
            if (tripCommand.getAmountLoss() != null && !"".equals(tripCommand.getAmountLoss())) {
                tripTO.setAmountLoss(tripCommand.getAmountLoss());
            }
            if (tripCommand.getAddressInfo() != null && !"".equals(tripCommand.getAddressInfo())) {
                tripTO.setAddressInfo(tripCommand.getAddressInfo());
            }

            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
            }
            if (tripCommand.getPlanEndDate() != null && !tripCommand.getPlanEndDate().equals("")) {
                tripTO.setPlanEndDate(tripCommand.getPlanEndDate());
            }
            if (tripCommand.getPlanEndHour() != null && !tripCommand.getPlanEndHour().equals("")) {
                tripTO.setPlanEndHour(tripCommand.getPlanEndHour());
            }
            if (tripCommand.getPlanEndMinute() != null && !tripCommand.getPlanEndMinute().equals("")) {
                tripTO.setPlanEndMinute(tripCommand.getPlanEndMinute());
            }
            if (tripCommand.getEndDate() != null && !tripCommand.getEndDate().equals("")) {
                tripTO.setEndDate(tripCommand.getEndDate());
            }
            if (tripCommand.getPlanStartDate() != null && !tripCommand.getPlanStartDate().equals("")) {
                tripTO.setPlanStartDate(tripCommand.getPlanStartDate());
            }

            if (tripCommand.getPlanStartHour() != null && !tripCommand.getPlanStartHour().equals("")) {
                tripTO.setPlanStartHour(tripCommand.getPlanStartHour());
            }
            if (tripCommand.getPlanStartMinute() != null && !tripCommand.getPlanStartMinute().equals("")) {
                tripTO.setPlanStartMinute(tripCommand.getPlanStartMinute());
            }
            if (tripCommand.getTripEndHour() != null && !tripCommand.getTripEndHour().equals("")) {
                tripTO.setTripEndHour(tripCommand.getTripEndHour());
                System.out.println(tripCommand.getTripEndHour());
            }
            if (tripCommand.getTripEndMinute() != null && !tripCommand.getTripEndMinute().equals("")) {
                tripTO.setTripEndMinute(tripCommand.getTripEndMinute());
            }
            if (tripCommand.getEndHM() != null && !tripCommand.getEndHM().equals("")) {
                tripTO.setEndHM(tripCommand.getEndHM());
            }
            if (tripCommand.getEndOdometerReading() != null && !tripCommand.getEndOdometerReading().equals("")) {
                tripTO.setEndOdometerReading(tripCommand.getEndOdometerReading());
            }

            String getStartOdometerReading = request.getParameter("startKM");

            if (getStartOdometerReading != null && !getStartOdometerReading.equals("")) {
                tripTO.setStartOdometerReading(getStartOdometerReading);
            }
            if (tripCommand.getEndTripRemarks() != null && !tripCommand.getEndTripRemarks().equals("")) {
                tripTO.setEndTripRemarks(tripCommand.getEndTripRemarks());
            }
            if (tripCommand.getTotalKM() != null && !tripCommand.getTotalKM().equals("")) {
                tripTO.setTotalKM(tripCommand.getTotalKM());
            }
            if (tripCommand.getTotalHrs() != null && !tripCommand.getTotalHrs().equals("")) {
                tripTO.setTotalHrs(tripCommand.getTotalHrs());
            }

            if (tripCommand.getDurationDay1() != null && !tripCommand.getDurationDay1().equals("")) {
                tripTO.setDurationDay1(tripCommand.getDurationDay1());
            }
            if (tripCommand.getDurationDay2() != null && !tripCommand.getDurationDay2().equals("")) {
                tripTO.setDurationDay2(tripCommand.getDurationDay2());
            }
            if (tripCommand.getTotalDays() != null && !tripCommand.getTotalDays().equals("")) {
                tripTO.setTotalDays(tripCommand.getTotalDays());
            }
            if (tripCommand.getLrNumber() != null && !tripCommand.getLrNumber().equals("")) {
                tripTO.setLrNumber(tripCommand.getLrNumber());
            }
            //if (tripCommand.getStatusId() != null && !tripCommand.getStatusId().equals("")) {
            tripTO.setStatusId("12"); // trip end
            //}

            //////////////////Arun Start//////////////////////////
            if (tripCommand.getProductCodes() != null && !"".equals(tripCommand.getProductCodes())) {
                tripTO.setProductCodes(tripCommand.getProductCodes());
            }

            if (tripCommand.getConsignmentId() != null && !"".equals(tripCommand.getConsignmentId())) {
                tripTO.setConsignmentId(tripCommand.getConsignmentId());
            }
            if (tripCommand.getTripArticleId() != null && !"".equals(tripCommand.getTripArticleId())) {
                tripTO.setTripArticleId(tripCommand.getTripArticleId());
            }
            if (tripCommand.getFuelLtr() != null && !tripCommand.getFuelLtr().equals("")) {
                tripTO.setFuelLtr(tripCommand.getFuelLtr());
            }
            if (tripCommand.getTripPlannedEndDate() != null && !tripCommand.getTripPlannedEndDate().equals("")) {
                tripTO.setTripPlannedEndDate(tripCommand.getTripPlannedEndDate());
            }

            if (tripCommand.getInvoiceDate() != null && !tripCommand.getInvoiceDate().equals("")) {
                tripTO.setInvoiceDate(tripCommand.getInvoiceDate());
            }

            if (tripCommand.getOriginDetention() != null && !"".equals(tripCommand.getOriginDetention())) {
                tripTO.setOriginDetention(tripCommand.getOriginDetention());
            }

            if (tripCommand.getCustomerId() != null && !"".equals(tripCommand.getCustomerId())) {
                tripTO.setCustomerId(tripCommand.getCustomerId());
            }

            String wfuUnloadingTime = request.getParameter("wfuUnloadingTime");
            String wfuUnloadingDate = request.getParameter("wfuUnloadingDate");
            if (wfuUnloadingDate != null) {
                tripTO.setVehicleactreportdate(wfuUnloadingDate);
                tripTO.setEndReportingTime(wfuUnloadingTime);
            }
            if (wfuUnloadingDate == null) {
                if (tripCommand.getVehicleactreportdate() != null && !"".equals(tripCommand.getVehicleactreportdate())) {
                    tripTO.setVehicleactreportdate(tripCommand.getVehicleactreportdate());
                }
                if (tripCommand.getVehicleactreporthour() != null && !"".equals(tripCommand.getVehicleactreporthour())) {
                    tripTO.setVehicleactreporthour(tripCommand.getVehicleactreporthour());
                }
                if (tripCommand.getVehicleactreportmin() != null && !"".equals(tripCommand.getVehicleactreportmin())) {
                    tripTO.setVehicleactreportmin(tripCommand.getVehicleactreportmin());
                }
            }

            if (tripCommand.getVehicleloadreportdate() != null && !"".equals(tripCommand.getVehicleloadreportdate())) {
                tripTO.setVehicleloadreportdate(tripCommand.getVehicleloadreportdate());
            }
            if (tripCommand.getVehicleloadreporthour() != null && !"".equals(tripCommand.getVehicleloadreporthour())) {
                tripTO.setVehicleloadreporthour(tripCommand.getVehicleloadreporthour());
            }
            if (tripCommand.getVehicleloadreportmin() != null && !"".equals(tripCommand.getVehicleloadreportmin())) {
                tripTO.setVehicleloadreportmin(tripCommand.getVehicleloadreportmin());
            }
            if (tripCommand.getVehicleloadtemperature() != null && !"".equals(tripCommand.getVehicleloadtemperature())) {
                tripTO.setVehicleloadtemperature(tripCommand.getVehicleloadtemperature());
            }
            if (tripCommand.getUnloadedpackages() != null && !"".equals(tripCommand.getUnloadedpackages())) {
                tripTO.setUnloadedpackages(tripCommand.getUnloadedpackages());
            }
            if (tripCommand.getShortage() != null && !"".equals(tripCommand.getShortage())) {
                tripTO.setShortage(tripCommand.getShortage());
            }
            if (tripCommand.getEmptyTripApprovalStatus() != null && !"".equals(tripCommand.getEmptyTripApprovalStatus())) {
                tripTO.setEmptyTripApprovalStatus(tripCommand.getEmptyTripApprovalStatus());
            }
            //New 
            if (tripCommand.getLoadedpackagesNew() != null && !"".equals(tripCommand.getLoadedpackagesNew())) {
                tripTO.setLoadedpackagesNew(tripCommand.getLoadedpackagesNew());
            }
            if (tripCommand.getUnloadedpackagesNew() != null && !"".equals(tripCommand.getUnloadedpackagesNew())) {
                tripTO.setUnloadedpackagesNew(tripCommand.getUnloadedpackagesNew());
            }
            if (tripCommand.getShortageNew() != null && !"".equals(tripCommand.getShortageNew())) {
                tripTO.setShortageNew(tripCommand.getShortageNew());
            }
            if (tripCommand.getDamageNew() != null && !"".equals(tripCommand.getDamageNew())) {
                tripTO.setDamageNew(tripCommand.getDamageNew());
            }

            //load details
            String[] loadOrderId = request.getParameterValues("loadOrderId");
            String[] pendingPkgs = request.getParameterValues("pendingPkgs");
            String[] pendingWeight = request.getParameterValues("pendingWeight");
//            String[] loadedPkgs = request.getParameterValues("loadedPkgs");
//            String[] loadedWeight = request.getParameterValues("loadedWeight");

            tripTO.setLoadOrderId(loadOrderId);
            tripTO.setPendingPkgs(pendingPkgs);
            tripTO.setPendingWeight(pendingWeight);
//            tripTO.setLoadedPkgs(loadedPkgs);
//            tripTO.setLoadedWeight(loadedWeight);

            String routeCourseId = request.getParameter("routeCourseId");
            tripTO.setRouteId(routeCourseId);

            insertStatus = tripBP.updateEndTripSheet(tripTO, userId);

            insertStatus1 = tripBP.insertLoadingUnloadingDetails(tripTO, userId);
            insertStatus2 = tripBP.updateTripFuel(tripTO, userId);
            //update ts_trip_start_end_log start
            int gpsEndLogStatus = tripBP.createGPSLogForTripEnd(tripTO.getTripSheetId());
            //update ts_trip_start_end_log end
            //////////////////Arun Start//////////////////////////

            if (insertStatus != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Trip End Updated Successfully....");
            }

            String customerId = request.getParameter("customerId");
            System.out.println("CustomerId in end" + customerId);
            String tripType = request.getParameter("tripType");
            System.out.println("tripType in end" + tripType);
            String vehicle = request.getParameter("vehicleno");

//            String to = "";
//            String cc = "";
//            String smtp = "";
//            int emailPort = 0;
//            String frommailid = "";
//            String password = "";
//            String activitycode = "EMTRP1";
//
//            ArrayList emaildetails = new ArrayList();
//            emaildetails = tripBP.getEmailDetails(activitycode);
//
//            String emailString = tripBP.getTripEmails(tripCommand.getTripSheetId(), tripTO.getStatusId());
//            System.out.println("emailString:" + emailString);
//            String[] emailTemp = emailString.split("~");
//            int emailTempLenth = emailTemp.length;
//            System.out.println("emailTempLenth: " + emailTempLenth);
//
//            if (emailTempLenth > 0) {
//                to = emailTemp[0];
//            }
//            if (emailTempLenth > 1) {
//                cc = emailTemp[1];
//            }
//
//            Iterator itr1 = emaildetails.iterator();
//
//            if (itr1.hasNext()) {
//                tripTO = new TripTO();
//                tripTO = (TripTO) itr1.next();
//                smtp = tripTO.getSmtp();
//                emailPort = Integer.parseInt(tripTO.getPort());
//                frommailid = tripTO.getEmailId();
//                password = tripTO.getPassword();
//            }
//            
//            if (Integer.parseInt(tripType) == 1 && Integer.parseInt(customerId) != 1040) {
//                cc = cc + "," + "gurmeet.singh9716@gmail.com";
//
//            }
//            /*
//             String emailFormat = "<html>"
//             + "<body>"
//             + "<p><b>Hi, <br><br>Trip Ended for the vehicle of " + vehicle + ". <br><br> End Date : " + tripCommand.getEndDate() + " and Hour : " + tripCommand.getTripEndHour() + "  and Time : " + tripCommand.getTripEndMinute() + ".</b></p>"
//             + "</body></html>";
//             */
//            String cNotesEmail = request.getParameter("cNotesEmail");
//            String vehicleNoEmail = request.getParameter("vehicleNoEmail");
//            String tripCodeEmail = request.getParameter("tripCodeEmail");
//            String customerNameEmail = request.getParameter("customerNameEmail");
//            String routeInfoEmail = request.getParameter("routeInfoEmail");
//            String paymentType = request.getParameter("paymentType");
//
////            tripTO.setcNotesEmail(cNotesEmail);
////            tripTO.setVehicleNoEmail(vehicleNoEmail);
////            tripTO.setTripCodeEmail(tripCodeEmail);
////            tripTO.setCustomerNameEmail(customerNameEmail);
////            tripTO.setRouteInfoEmail(routeInfoEmail);
//            String emailFormat = "<html>"
//                    + "<body>"
//                    + "<p><b>Hi, <br><br>Trip Ended for the Vehicle " + vehicleNoEmail + " , CNote " + cNotesEmail + ". <br>"
//                    + "<br>Vehicle No:" + vehicleNoEmail
//                    + "<br>Trip Code:" + tripCodeEmail
//                    + "<br>Customer:" + customerNameEmail
//                    + "<br>Payment Type:" + paymentType
//                    + "<br>Route:" + routeInfoEmail
//                    + "<br>End Date:" + tripCommand.getEndDate() + " Time:" + tripCommand.getTripEndHour() + ":" + tripCommand.getTripEndMinute()
//                    + "<br><br>Team BrattleFoods"
//                    + "</body></html>";
//
//            String subject = "Trip Ended for Customer " + customerNameEmail + " Vehicle " + vehicleNoEmail + " Route " + routeInfoEmail;
//            String content = emailFormat;
//            if (!"".equals(to)) {
//                int mailSendingId = 0;
//                tripTO.setMailTypeId("2");
//                tripTO.setMailSubjectTo(subject);
//                tripTO.setMailSubjectCc(subject);
//                tripTO.setMailSubjectBcc("");
//                tripTO.setMailContentTo(content);
//                tripTO.setMailContentCc(content);
//                tripTO.setMailContentBcc("");
//                tripTO.setMailIdTo(to);
//                tripTO.setMailIdCc(cc);
//                tripTO.setMailIdBcc("");
////                mailSendingId = tripBP.insertMailDetails(tripTO, userId);
//                new SendMail(smtp, emailPort, frommailid, password, subject, content, to, cc, userId).start();
//            }
            /////////////////////////////////////////////Email part///////////////////////
            String responsiveStatus = request.getParameter("respStatus");
            String statusId = request.getParameter("statusId");
            request.setAttribute("tripType", tripType);
            request.setAttribute("statusId", statusId);
            //path = "content/trip/endTripSheet.jsp";
            path = "content/redirectPage.jsp";

            if (responsiveStatus == null) {
                if (request.getAttribute("respStatus") != null) {
                    responsiveStatus = (String) request.getAttribute("respStatus");
                }
            }
            if (responsiveStatus != null && "Y".equals(responsiveStatus)) {
                request.setAttribute("respStatus", "Y");
                mv = handleViewTripSheet(request, response, command);
            } else {
                mv = handleViewTripSheet(request, response, command);
            }
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
//        return new ModelAndView(path);
        return mv;
    }

    public ModelAndView handleViewTripExpense(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  Trip Closure ";
        String tripSheetId = "";
        String orderType = "";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
                request.setAttribute("tripSheetId", tripCommand.getTripSheetId());
            }
            if (tripCommand.getOrderType() != null && !tripCommand.getOrderType().equals("")) {
                tripTO.setOrderType(tripCommand.getOrderType());
                request.setAttribute("orderType", tripCommand.getOrderType());
            }
            if (tripCommand.getVehicleId() != null && !tripCommand.getVehicleId().equals("")) {
                tripTO.setVehicleId(tripCommand.getVehicleId());
                request.setAttribute("vehicleId", tripCommand.getVehicleId());
            }
            tripTO.setTripId(tripTO.getTripSheetId());
            tripSheetId = tripTO.getTripSheetId();
            orderType = request.getParameter("orderType");

            request.setAttribute("orderTypeNew", orderType);
            System.out.println("orderType0000=" + orderType);
            ArrayList fuelDetails = new ArrayList();
            fuelDetails = tripBP.getFuelDetails(tripTO);
            if (fuelDetails.size() > 0) {
                request.setAttribute("fuelDetails", fuelDetails);
            }

            ArrayList otherExpenseDetails = new ArrayList();
            otherExpenseDetails = tripBP.getOtherExpenseDetails(tripTO);
            if (otherExpenseDetails.size() > 0) {
                request.setAttribute("otherExpenseDetails", otherExpenseDetails);
                request.setAttribute("otherExpenseDetailsSize", otherExpenseDetails.size());
            }

            ArrayList expenseDetails = new ArrayList();
            expenseDetails = tripBP.getExpenseDetails(tripTO);
            if (expenseDetails.size() > 0) {
                request.setAttribute("expenseDetails", expenseDetails);
            }
            ArrayList driverNameDetails = new ArrayList();
            driverNameDetails = tripBP.getDriverName(tripTO);
            if (driverNameDetails.size() > 0) {
                request.setAttribute("driverNameDetails", driverNameDetails);
            }

            ArrayList statusDetails = new ArrayList();
            statusDetails = tripBP.getTripStausDetails(tripTO);
            if (statusDetails.size() > 0) {
                request.setAttribute("statusDetails", statusDetails);
            }

            ArrayList customerList = new ArrayList();
            ArrayList tripDetails = new ArrayList();
            ArrayList tripPointDetails = new ArrayList();
            ArrayList tripPreStartDetails = new ArrayList();
            ArrayList tripStartDetails = new ArrayList();
            ArrayList tripEndDetails = new ArrayList();
            tripDetails = tripBP.getTripDetails(tripTO);
            customerList = tripBP.getTripCustomerList(tripTO);
            request.setAttribute("customerLists", customerList);
            //get trip type

            Iterator itr2 = tripDetails.iterator();
            String tripType = request.getParameter("tripType");
            String statusId = request.getParameter("statusId");
            request.setAttribute("tripType", tripType);
            request.setAttribute("statusId", statusId);

            while (itr2.hasNext()) {
                TripTO tripTO2 = (TripTO) itr2.next();
                tripType = tripTO2.getTripType();
            }
            tripTO.setTripType(tripType);

            System.out.println("tripType:" + tripType);
            tripPointDetails = tripBP.getTripPointDetails(tripTO);
            tripPreStartDetails = tripBP.getPreStartTripDetails(tripTO);
            tripStartDetails = tripBP.getStartedTripDetails(tripTO);
            tripEndDetails = tripBP.getEndTripDetails(tripTO);

            ArrayList tripUnPackDetails = new ArrayList();
            tripUnPackDetails = tripBP.getTripUnPackDetails(tripTO);
            request.setAttribute("tripUnPackDetails", tripUnPackDetails);

            request.setAttribute("tripDetails", tripDetails);
            request.setAttribute("tripPointDetails", tripPointDetails);
            request.setAttribute("tripPreStartDetails", tripPreStartDetails);
            request.setAttribute("tripStartDetails", tripStartDetails);
            request.setAttribute("tripEndDetails", tripEndDetails);
            ArrayList expiryDateDetails = new ArrayList();
            expiryDateDetails = tripBP.getExpiryDateDetails(tripTO);
            request.setAttribute("expiryDateDetails", expiryDateDetails);

            //system expense calculation start
            //Throttle Starts As On 12-12-2013
            tripTO.setTripId(tripTO.getTripSheetId());

            ArrayList viewPODDetails = new ArrayList();
            viewPODDetails = tripBP.getPODDetails(tripTO);
            if (viewPODDetails.size() > 0) {
                request.setAttribute("viewPODDetails", viewPODDetails);
            }
            String vehicleTypeId = "";
            TripTO tripTO1 = new TripTO();
            ArrayList totalExpenseDetails = new ArrayList();
            totalExpenseDetails = tripBP.getTotalExpenseDetails(tripTO);
            Iterator itr1 = totalExpenseDetails.iterator();
            /*
             tripTo.setSecondaryTollAmount(tempVar[0]);
             tripTo.setSecondaryAddlTollAmount(tempVar[1]);
             tripTo.setSecondaryMiscAmount(tempVar[2]);
             */
            while (itr1.hasNext()) {
                tripTO1 = (TripTO) itr1.next();
                request.setAttribute("tollAmount", tripTO1.getTollAmount());
                request.setAttribute("driverBatta", tripTO1.getDriverBatta());
                request.setAttribute("driverIncentive", tripTO1.getDriverIncentive());
                request.setAttribute("fuelPrice", tripTO1.getFuelPrice());
                request.setAttribute("milleage", tripTO1.getMilleage());
                request.setAttribute("reeferConsumption", tripTO1.getReeferConsumption());
                request.setAttribute("vehicleTypeId", tripTO1.getVehicleTypeId());
                vehicleTypeId = tripTO1.getVehicleTypeId();
                request.setAttribute("secTollAmount", tripTO1.getSecondaryTollAmount());
                request.setAttribute("secAddlTollAmount", tripTO1.getSecondaryAddlTollAmount());
                request.setAttribute("secMiscAmount", tripTO1.getSecondaryMiscAmount());
                request.setAttribute("secParkingAmount", tripTO1.getSecondaryParkingAmount());
                request.setAttribute("fuelTypeId", tripTO1.getFuelTypeId());

            }
            request.setAttribute("tripType", tripType);
            request.setAttribute("totalExpenseDetails", totalExpenseDetails);

            String startDate = "";
            String startTime = "";
            String endDate = "";
            String endTime = "";
            TripTO tpTO = new TripTO();
            ArrayList tripSheetDetails = new ArrayList();
            tripSheetDetails = tripBP.getClosureEndTripSheetDetails(tripTO);
            Iterator itr = tripSheetDetails.iterator();
            while (itr.hasNext()) {
                tpTO = (TripTO) itr.next();
                request.setAttribute("tripSheetId", tpTO.getTripSheetId());
                request.setAttribute("cNotes", tpTO.getcNotes());
                request.setAttribute("vehicleNo", tpTO.getVehicleNo());
                request.setAttribute("tripCode", tpTO.getTripCode());
                request.setAttribute("startDate", tpTO.getTripScheduleDate());
                startDate = tpTO.getTripScheduleDate();
                request.setAttribute("startTime", tpTO.getTripScheduleTime());
                startTime = tpTO.getTripScheduleTime();
                request.setAttribute("estimatedExpense", tpTO.getOrderExpense());
                request.setAttribute("rcmExpense", tpTO.getRcmExpense());
                request.setAttribute("origin", tpTO.getOrigin());
                request.setAttribute("destination", tpTO.getDestination());
                request.setAttribute("tripEndDate", tpTO.getTripEndDate());
                endDate = tpTO.getTripEndDate();
                request.setAttribute("tripEndTime", tpTO.getTripEndTime());
                endTime = tpTO.getTripEndTime();
                request.setAttribute("startKm", tpTO.getStartKm());
                request.setAttribute("startHm", tpTO.getStartHm());
                request.setAttribute("endKm", tpTO.getEndKm());
                request.setAttribute("endHm", tpTO.getEndHm());
                request.setAttribute("runKm", tpTO.getRunKm());
                request.setAttribute("runHm", tpTO.getRunHm());
                request.setAttribute("totaldays", tpTO.getTotalDays());
                request.setAttribute("totalHours", tpTO.getDurationHours());
                request.setAttribute("driverCount", tpTO.getDriverCount());
                request.setAttribute("extraExpenseValue", tpTO.getExtraExpenseValue());

                System.out.println("getOrderExpense = " + tpTO.getOrderExpense());
                System.out.println("getRcmExpense = " + tpTO.getRcmExpense());
                System.out.println("getStartKm = " + tpTO.getStartKm());
                System.out.println("getStartHm = " + tpTO.getStartHm());
                System.out.println("getEndKm = " + tpTO.getEndKm());
                System.out.println("getEndHm = " + tpTO.getEndHm());
                System.out.println("getRunKm = " + tpTO.getRunKm());
                System.out.println("getRunHm = " + tpTO.getRunHm());
                System.out.println("getTotalDays = " + tpTO.getTotalDays());
                System.out.println("getDurationHours = " + tpTO.getDurationHours());
                System.out.println("driverCount = " + tpTO.getDriverCount());
                System.out.println("tpTO.getExtraExpenseValue() " + tpTO.getExtraExpenseValue());
                String[] temp = null;
                if (!"".equals(tpTO.getDriverName()) && tpTO.getDriverName().contains(",")) {
                    temp = tpTO.getDriverName().split(",");
                    request.setAttribute("driverName1", temp[0]);
                    request.setAttribute("driverName2", temp[1]);
                } else {
                    request.setAttribute("driverName1", tpTO.getDriverName());
                }
                request.setAttribute("driverName", tpTO.getDriverName());
            }
            int tripCnt = 0;
            String tripCount = "";
            if (tripType.equals("2")) {
                tripCount = tripBP.getTripCount(tpTO);
                if (tripCount != null) {
                    tripCnt = Integer.parseInt(tripCount);
                }
            }

            request.setAttribute("tripCnt", tripCnt);
            request.setAttribute("tripSheetDetails", tripSheetDetails);
            String gpsKm = tripBP.getGpsKm(tripTO.getTripId());
            System.out.println("gpsKmVal = " + gpsKm);
            if (gpsKm != null) {
                String[] temp = gpsKm.split("-");
                String gpsKmVal = "0";
                if (!"0".equals(temp[0])) {
                    gpsKmVal = temp[0];
                }
                request.setAttribute("gpsKm", "" + gpsKmVal);
                //use above calculations
                request.setAttribute("gpsKm", gpsKmVal);
                request.setAttribute("gpsHm", temp[1]);
                System.out.println("gpsKmVal = " + gpsKmVal);
                System.out.println("gpsHm = " + temp[1]);
            } else {
                request.setAttribute("gpsKm", 0);
                request.setAttribute("gpsHm", 0);

            }
            //check if approval has been raised and its status for gps non usage
            String odometerApprovalStatus = tripBP.getClosureApprovalStatus(tripTO.getTripId(), "1");//1 indicates gps non usage approval request
            request.setAttribute("odometerApprovalStatus", odometerApprovalStatus);
            //ArrayList odometerApprovalHistory = tripBP;
            //approvalStatus =0 -> pending approval ; 1-> approved; 2-> rejected

            String expenseDeviationApprovalStatus = tripBP.getClosureApprovalStatus(tripTO.getTripId(), "2");//1 indicates expense Deviation approval request
            ArrayList approveProcessHistory = tripBP.getClosureApprovalProcessHistory(tripTO);
            request.setAttribute("approveProcessHistory", approveProcessHistory);
            request.setAttribute("expenseDeviationApprovalStatus", expenseDeviationApprovalStatus);
            //Throttle Starts As On 12-12-2013
            String miscValue = tripBP.getMiscValue(vehicleTypeId);
            request.setAttribute("miscValue", miscValue);

            //system expense calculation end
            //fetch total booked expenses
            String bookedExpense = tripBP.getBookedExpense(tripTO.getTripId(), tripTO);
            request.setAttribute("bookedExpense", bookedExpense);

            ArrayList startTripDetails = new ArrayList();
            startTripDetails = tripBP.getStartTripDetails(tripTO);
            request.setAttribute("startTripDetails", startTripDetails);
            //String tripSheetId = request.getParameter("tripId");
            ArrayList tripAdvanceDetails = new ArrayList();
            tripAdvanceDetails = tripBP.getTripAdvanceDetails(tripSheetId);
            request.setAttribute("tripAdvanceDetails", tripAdvanceDetails);

            ArrayList tripAdvanceDetailsStatus = new ArrayList();
            tripAdvanceDetailsStatus = tripBP.getTripAdvanceDetailsStatus(tripSheetId);

            request.setAttribute("tripAdvanceDetailsStatus", tripAdvanceDetailsStatus);
            ArrayList bpclTransactionHistory = new ArrayList();
//            bpclTransactionHistory = tripBP.getBPCLTransactionHistory(tripTO);
            request.setAttribute("bpclTransactionHistory", bpclTransactionHistory);

            ArrayList tripFuel = new ArrayList();
            tripFuel = tripBP.getTripFuel(tripTO);
            request.setAttribute("tripFuel", tripFuel);

            ArrayList vehicleChangeAdvanceDetails = new ArrayList();
            vehicleChangeAdvanceDetails = tripBP.getVehicleChangeTripAdvanceDetails(tripSheetId);
            request.setAttribute("vehicleChangeAdvanceDetails", vehicleChangeAdvanceDetails);
            request.setAttribute("vehicleChangeAdvanceDetailsSize", vehicleChangeAdvanceDetails.size());
            String otherExpenseDoneStatus = tripBP.getOtherExpenseDoneStatus(tripTO);
            request.setAttribute("otherExpenseDoneStatus", otherExpenseDoneStatus);
            ArrayList tripsettelmentDetails = new ArrayList();
            tripsettelmentDetails = tripBP.tripsettelmentDetails(tripTO);
            if (tripsettelmentDetails.size() > 0) {
                request.setAttribute("tripsettelmentDetails", tripsettelmentDetails);
                Iterator itrList = tripsettelmentDetails.iterator();
                TripTO tTO = new TripTO();
                while (itrList.hasNext()) {
                    tTO = (TripTO) itrList.next();
                    request.setAttribute("setteledKm", tTO.getSetteledKm());
                    request.setAttribute("setteledHm", tTO.getSetteledKm());
                    request.setAttribute("setteledTotalKm", tTO.getSetteltotalKM());
                    request.setAttribute("setteledTotalHm", tTO.getSetteltotalHrs());
                    request.setAttribute("overrideTripRemarks", tTO.getOverrideTripRemarks());
                    request.setAttribute("overrideBy", tTO.getOverRideBy());
                    request.setAttribute("overrideOn", tTO.getOverRideOn());
                }
            }
            ArrayList loadDetails = new ArrayList();
            loadDetails = tripBP.getLoadDetails(tripTO);
            request.setAttribute("loadDetails", loadDetails);

            String admin = request.getParameter("admin");
            request.setAttribute("admin", admin);
            String fclogin = request.getParameter("fclogin");
            System.out.println("fclogin:" + fclogin);
            request.setAttribute("fclogin", fclogin);

            ArrayList vehicleChangedTripDetails = new ArrayList();
            vehicleChangedTripDetails = tripBP.getVehicleChangeTripDetails(tripTO);
            if (vehicleChangedTripDetails.size() > 0) {
                request.setAttribute("vehicleChangedTripDetails", vehicleChangedTripDetails);
            }

            String roleId = "" + (Integer) session.getAttribute("RoleId");
            request.setAttribute("roleId", roleId);

            path = "content/trip/tripFinanceClosure.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView closeTrip(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View start Trip Sheet ";
        int insertStatus = 0;
        int insertStatus2 = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            String detentionHours = request.getParameter("detentionHours");
            String detentionCharge = request.getParameter("detentionCharge");
            String estimatedExpense = request.getParameter("estimatedExpense");
            String tollRate = request.getParameter("tollRate");
            String gpsKm = request.getParameter("gpsKm");
            String gpsHm = request.getParameter("gpsHm");
            String vehicleId = request.getParameter("vehicleId");

            String driverBattaPerDay = request.getParameter("driverBattaPerDay");
            String driverIncentivePerKm = request.getParameter("driverIncentivePerKm");
            String driverIncentive = request.getParameter("driverIncentive");
            String fuelPrice = request.getParameter("fuelPrice");
            String mileage = request.getParameter("mileage");
            String reeferMileage = request.getParameter("reeferMileage");
            String runKm = request.getParameter("runKm");
            String runHm = request.getParameter("runHm");
            String totalDays = request.getParameter("totalDays");

            String dieselUsed = request.getParameter("dieselUsed");
            String dieselCost = request.getParameter("dieselCost");

            String fuelUsed = request.getParameter("fuelUsed");
            String fuelCost = request.getParameter("fuelCost");

            String tollCost = request.getParameter("tollCost");
            String driverBatta = request.getParameter("driverBatta");
            String miscValue = request.getParameter("miscValue");
            String miscRate = request.getParameter("miscRate");

            String secTollCost = request.getParameter("secTollCost");
            String secAddlTollCost = request.getParameter("secAddlTollCost");
            String secMiscCost = request.getParameter("secMiscCost");
            String secParkingAmount = request.getParameter("secParkingAmount");
            String preColingAmount = request.getParameter("preColingAmount");
            String tripType = request.getParameter("tripType");
            String lrNumber = request.getParameter("lrNumber");
            String extraExpenseValue = request.getParameter("extraExpenseValue");

            if ("2".equals(tripType)) {//secondary
                miscValue = secMiscCost;
                tollCost = secTollCost;
                dieselUsed = fuelUsed;
                dieselCost = fuelCost;
            }

            String bookedExpense = request.getParameter("bookedExpense");
            String tripId = request.getParameter("tripSheetId");
            String tripSheetId = request.getParameter("tripSheetId");
            String systemExpense = request.getParameter("systemExpense");
            String nettExpense = request.getParameter("nettExpense");
            tripTO.setVehicleId(vehicleId);
            tripTO.setLrNumber(lrNumber);
            tripTO.setMiscValue(miscValue);
            tripTO.setMiscRate(miscRate);
            tripTO.setExtraExpenseValue(extraExpenseValue);
            tripTO.setMilleage(mileage);
            tripTO.setTotalExpenses(nettExpense);
            tripTO.setSystemExpense(systemExpense);
            tripTO.setDriverIncentive(driverIncentive);
            tripTO.setBookedExpense(bookedExpense);
            tripTO.setDriverBatta(driverBatta);
            tripTO.setTollCost(tollCost);
            tripTO.setDieselCost(dieselCost);
            tripTO.setDieselUsed(dieselUsed);
            tripTO.setTotalDays(totalDays);
            tripTO.setRunHm(runHm);
            tripTO.setRunKm(runKm);
            tripTO.setGpsKm(gpsKm);
            tripTO.setGpsHm(gpsHm);
            tripTO.setReeferMileage(reeferMileage);
            tripTO.setFuelPrice(fuelPrice);
            tripTO.setTripId(tripId);
            tripTO.setTripSheetId(tripId);
            tripTO.setEstimatedExpense(estimatedExpense);
            tripTO.setDetentionHours(detentionHours);
            tripTO.setDetentionCharge(detentionCharge);
            tripTO.setTollRate(tollRate);
            tripTO.setDriverBattaPerDay(driverBattaPerDay);
            tripTO.setDriverIncentivePerKm(driverIncentivePerKm);
            tripTO.setSecondaryParkingAmount(secParkingAmount);
            tripTO.setPreColingAmount(preColingAmount);
            tripTO.setSecAdditionalTollCost(secAddlTollCost);

            if (tripCommand.getFuelLtr() != null && !tripCommand.getFuelLtr().equals("")) {
                tripTO.setFuelLtr(tripCommand.getFuelLtr());
            }
            insertStatus2 = tripBP.updateTripFuel(tripTO, userId);
            insertStatus = tripBP.saveTripClosureDetails(tripTO, userId);
            if (insertStatus != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Closure Updated Successfully....");
            }
            //raju Trip clousure
            tripTO.setStatusId("13");//trip closed

            //load details
            String[] loadOrderId = request.getParameterValues("loadOrderId");
            String[] pendingPkgs = request.getParameterValues("pendingPkgs");
            String[] pendingWeight = request.getParameterValues("pendingWeight");
//            String[] loadedPkgs = request.getParameterValues("loadedPkgs");
//            String[] loadedWeight = request.getParameterValues("loadedWeight");

            tripTO.setLoadOrderId(loadOrderId);
            tripTO.setPendingPkgs(pendingPkgs);
            tripTO.setPendingWeight(pendingWeight);
//            tripTO.setLoadedPkgs(loadedPkgs);
//            tripTO.setLoadedWeight(loadedWeight);
            //update status
            insertStatus = tripBP.updateStatus(tripTO, userId);

            /////////////////////////////////////////////Email part///////////////////////
//            String to = "";
//            String cc = "";
//            String smtp = "";
//            int emailPort = 0;
//            String frommailid = "";
//            String password = "";
//            String activitycode = "EMTRP1";
//
//            ArrayList emaildetails = new ArrayList();
//            emaildetails = tripBP.getEmailDetails(activitycode);
//
//            String emailString = tripBP.getTripEmails(tripCommand.getTripSheetId(), tripTO.getStatusId());
//            System.out.println("emailString:" + emailString);
//            String[] emailTemp = emailString.split("~");
//            int emailTempLenth = emailTemp.length;
//            System.out.println("emailTempLenth: " + emailTempLenth);
//
//            if (emailTempLenth > 0) {
//                to = emailTemp[0];
//            }
//            if (emailTempLenth > 1) {
//                cc = emailTemp[1];
//            }
//
//            Iterator itr1 = emaildetails.iterator();
//
//            if (itr1.hasNext()) {
//                tripTO = new TripTO();
//                tripTO = (TripTO) itr1.next();
//                smtp = tripTO.getSmtp();
//                emailPort = Integer.parseInt(tripTO.getPort());
//                frommailid = tripTO.getEmailId();
//                password = tripTO.getPassword();
//            }
//            String cNotesEmail = request.getParameter("cNotesEmail");
//            String vehicleNoEmail = request.getParameter("vehicleNoEmail");
//            String tripCodeEmail = request.getParameter("tripCodeEmail");
//            String customerNameEmail = request.getParameter("customerNameEmail");
//            String routeInfoEmail = request.getParameter("routeInfoEmail");
//
//            String emailFormat = "<html>"
//                    + "<body>"
//                    + "<p>Hi, <br><br>Trip Closed For " + vehicleNoEmail + "</p>"
//                    + "<br> Customer:" + customerNameEmail
//                    + "<br> C Note No:" + cNotesEmail
//                    + "<br> Route :" + routeInfoEmail
//                    + "<br> Vehicle No :" + vehicleNoEmail
//                    + "<br><br> Team BrattleFoods"
//                    + "</body></html>";
//
//            String subject = "Trip Closure Completed for Customer " + customerNameEmail + " Vehicle " + vehicleNoEmail + " Route " + routeInfoEmail;
//            String content = emailFormat;
//            if (!"".equals(to)) {
//                try {
//                    // new SendMail(smtp, emailPort, frommailid, password, subject, content, to, cc).start();
//                    System.out.println("Now The Mail trigger for trip closure is commented");
//                } catch (Exception e) {
//                    System.out.println("mail error");
//                    e.printStackTrace();
//                }
//
//            }
            /////////////////////////////////////////////Email part///////////////////////
            path = "content/trip/endTripSheet.jsp";
            tripType = request.getParameter("tripType");
            String statusId = request.getParameter("statusId");
            request.setAttribute("tripType", tripType);
            request.setAttribute("statusId", statusId);

            mv = handleViewTripSheet(request, response, command);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView saveTotalExpense(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View start Trip Sheet ";
        int insertStatus = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
            }
            if (tripCommand.getDriverBatta() != null && !tripCommand.getDriverBatta().equals("")) {
                tripTO.setDriverBatta(tripCommand.getDriverBatta());
            }
            if (tripCommand.getDriverIncentive() != null && !tripCommand.getDriverIncentive().equals("")) {
                tripTO.setDriverIncentive(tripCommand.getDriverIncentive());
            }
            if (tripCommand.getTotalExpenses() != null && !tripCommand.getTotalExpenses().equals("")) {
                tripTO.setTotalExpenses(tripCommand.getTotalExpenses());
            }
            if (tripCommand.getFuelAmount() != null && !tripCommand.getFuelAmount().equals("")) {
                tripTO.setFuelAmount(tripCommand.getFuelAmount());
            }
            if (tripCommand.getFuelConsumed() != null && !tripCommand.getFuelConsumed().equals("")) {
                tripTO.setFuelConsumed(tripCommand.getFuelConsumed());
            }
            if (tripCommand.getFuelCost() != null && !tripCommand.getFuelCost().equals("")) {
                tripTO.setFuelCost(tripCommand.getFuelCost());
            }
            if (tripCommand.getTollCost() != null && !tripCommand.getTollCost().equals("")) {
                tripTO.setTollCost(tripCommand.getTollCost());
            }

            if (tripCommand.getTotalDays() != null && !tripCommand.getTotalDays().equals("")) {
                tripTO.setTotalDays(tripCommand.getTotalDays());
            }
            if (tripCommand.getTotalRumKM() != null && !tripCommand.getTotalRumKM().equals("")) {
                tripTO.setTotalRumKM(tripCommand.getTotalRumKM());
            }
            if (tripCommand.getTotalRefeerHours() != null && !tripCommand.getTotalRefeerHours().equals("")) {
                tripTO.setTotalRefeerHours(tripCommand.getTotalRefeerHours());
            }
            if (tripCommand.getTotalRefeerMinutes() != null && !tripCommand.getTotalRefeerMinutes().equals("")) {
                tripTO.setTotalRefeerMinutes(tripCommand.getTotalRefeerMinutes());
            }
            if (tripCommand.getMilleage() != null && !tripCommand.getMilleage().equals("")) {
                tripTO.setMilleage(tripCommand.getMilleage());
            }
            if (tripCommand.getTollAmount() != null && !tripCommand.getTollAmount().equals("")) {
                tripTO.setTollAmount(tripCommand.getTollAmount());
            }
            if (tripCommand.getBattaAmount() != null && !tripCommand.getBattaAmount().equals("")) {
                tripTO.setBattaAmount(tripCommand.getBattaAmount());
            }
            if (tripCommand.getIncentiveAmount() != null && !tripCommand.getIncentiveAmount().equals("")) {
                tripTO.setIncentiveAmount(tripCommand.getIncentiveAmount());
            }
            if (tripCommand.getVehicleTypeId() != null && !tripCommand.getVehicleTypeId().equals("")) {
                tripTO.setVehicleTypeId(tripCommand.getVehicleTypeId());
            }
            if (tripCommand.getRouteExpense() != null && !tripCommand.getRouteExpense().equals("")) {
                tripTO.setRouteExpense(tripCommand.getRouteExpense());
            }
            if (tripCommand.getEstimatedExpense() != null && !tripCommand.getEstimatedExpense().equals("")) {
                tripTO.setEstimatedExpense(tripCommand.getEstimatedExpense());
            }
            if (tripCommand.getReeferConsumption() != null && !tripCommand.getReeferConsumption().equals("")) {
                tripTO.setReeferConsumption(tripCommand.getReeferConsumption());
            }

            insertStatus = tripBP.saveTripClosure(tripTO, userId);
            if (insertStatus != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Closure Updated Successfully....");
            }

            tripTO.setStatusId("13");//trip closed
            //update status
            insertStatus = tripBP.updateStatus(tripTO, userId);
            path = "content/trip/endTripSheet.jsp";
            mv = handleViewTripSheet(request, response, command);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView saveTripFuelExpense(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View start Trip Sheet ";
        int insertStatus = 0;
        String tripSheetId = "";
        String orderType = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
            }
            if (tripCommand.getOrderType() != null && !tripCommand.getOrderType().equals("")) {
                tripTO.setOrderType(tripCommand.getOrderType());
            }

            tripSheetId = request.getParameter("tripSheetId");
            orderType = request.getParameter("orderType");
            System.out.println("this is tht" + tripSheetId);
            String fuelLocation[] = request.getParameterValues("location");
            String fillDate[] = request.getParameterValues("fillingDate");
            String fuelLitres[] = request.getParameterValues("litres");
            String fuelPricePerLitre[] = request.getParameterValues("amountPerLitre");
            String fuelAmount[] = request.getParameterValues("amount");
            String fuelRemarks[] = request.getParameterValues("fuelRemarks");

            if (fillDate != null) {
                for (int i = 0; i < fillDate.length; i++) {
                    if (fillDate[i] != null && fuelAmount[i] != null) {
                        insertStatus = tripBP.insertTripFuelExpense(tripSheetId, fuelLocation[i], fillDate[i], fuelLitres[i], fuelAmount[i],
                                fuelRemarks[i], fuelPricePerLitre[i], userId);
                    }
                }
            }
            ArrayList fuelDetails = new ArrayList();
            fuelDetails = tripBP.getFuelDetails(tripTO);
            if (fuelDetails.size() > 0) {
                request.setAttribute("fuelDetails", fuelDetails);
            }

            ArrayList otherExpenseDetails = new ArrayList();
            otherExpenseDetails = tripBP.getOtherExpenseDetails(tripTO);
            if (otherExpenseDetails.size() > 0) {
                request.setAttribute("otherExpenseDetails", otherExpenseDetails);
            }

            ArrayList expenseDetails = new ArrayList();
            expenseDetails = tripBP.getExpenseDetails(tripTO);
            if (expenseDetails.size() > 0) {
                request.setAttribute("expenseDetails", expenseDetails);
            }
            ArrayList driverNameDetails = new ArrayList();
            driverNameDetails = tripBP.getDriverName(tripTO);
            if (driverNameDetails.size() > 0) {
                request.setAttribute("driverNameDetails", driverNameDetails);
            }

            path = "content/trip/tripFinanceClosure.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveOtherTripExpense(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        ModelAndView mv = null;
        String menuPath = "Operation  >>  Trip Closure ";
        int insertStatus = 0;
        String tripSheetId = "";
        String orderType = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
            }
            orderType = request.getParameter("orderType");
            request.setAttribute("orderTypeNew", orderType);
            System.out.println("orderType0000=" + orderType);

            tripSheetId = request.getParameter("tripSheetId");
            String vehicleId = request.getParameter("vehicleId");
            System.out.println("this is tht" + tripSheetId);
            String expenseName[] = request.getParameterValues("expenseName");
            String employeeName[] = request.getParameterValues("employeeName");
            String expenseDate[] = request.getParameterValues("expenseDate");
            String expenseHour[] = request.getParameterValues("expenseHour");
            String expenseMinute[] = request.getParameterValues("expenseMinute");
            String expenseRemarks[] = request.getParameterValues("expenseRemarks");
            String expenseType[] = request.getParameterValues("expenseType");
            String taxPercentage[] = request.getParameterValues("taxPercentage");
            String expenses[] = request.getParameterValues("expenses");
            String netExpense[] = request.getParameterValues("netExpense");
            String billMode[] = request.getParameterValues("billMode");
            String marginValue[] = request.getParameterValues("marginValue");
            String expenseId[] = request.getParameterValues("editId");
            String customerId[] = request.getParameterValues("custId");
            String orderId[] = request.getParameterValues("orderId");
            String activeValue[] = request.getParameterValues("activeValue");
            String editId = request.getParameter("editId");
            String expenseLocation[] = request.getParameterValues("expenseLocation");
            System.out.println("expenseLocation[]==" + request.getParameterValues("expenseLocation"));

            tripTO.setLoadOrderId(orderId);

            if (editId != null && !"".equals(editId)) {
                for (int i = 0; i < expenseName.length; i++) {
                    insertStatus = tripBP.updateTripOtherExpense(tripSheetId, vehicleId, expenseName[i], employeeName[i], expenseType[i], expenseLocation[i], expenseDate[i], expenseHour[i], expenseMinute[i], taxPercentage[i],
                            expenseRemarks[i], expenses[i], netExpense[i], billMode[i], marginValue[i], expenseId[i], activeValue[i], customerId[i], orderId[i], userId);
                }
            } else {
                for (int i = 0; i < expenseName.length; i++) {
                    if (expenseDate[i] != null && !"".equals(expenseDate[i])) {
                        System.out.println("the expense is " + expenseName[i]);
                        System.out.println("orderId[i] : " + orderId[i]);

                        System.out.println(" mar " + marginValue[i]);
                        System.out.println(" cust " + customerId[i]);
                        System.out.println(" act " + activeValue[i]);

                        insertStatus = tripBP.insertTripOtherExpense(tripSheetId, vehicleId, expenseName[i], employeeName[i], expenseType[i], expenseLocation[i], expenseDate[i], expenseHour[i], expenseMinute[i], taxPercentage[i],
                                expenseRemarks[i], expenses[i], netExpense[i], billMode[i], marginValue[i], activeValue[i], customerId[i], orderId[i], userId);
                    }
                }
            }

            if (insertStatus != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Trip Expense Details Updated Successfully....");
            }

            ArrayList otherExpenseDetails = new ArrayList();
            otherExpenseDetails = tripBP.getOtherExpenseDetails(tripTO);
            if (otherExpenseDetails.size() > 0) {
                request.setAttribute("otherExpenseDetails", otherExpenseDetails);
            }

            ArrayList expenseDetails = new ArrayList();
            expenseDetails = tripBP.getExpenseDetails(tripTO);
            if (expenseDetails.size() > 0) {
                request.setAttribute("expenseDetails", expenseDetails);
            }
            ArrayList driverNameDetails = new ArrayList();
            driverNameDetails = tripBP.getDriverName(tripTO);
            if (driverNameDetails.size() > 0) {
                request.setAttribute("driverNameDetails", driverNameDetails);
            }

            String expenseStatus = request.getParameter("expenseStatus");
            System.out.println("expenseStatus----------" + expenseStatus);
            request.setAttribute("expenseStatus", expenseStatus);

            tripTO.setExpenseStatus(expenseStatus);
            if (expenseStatus == "1") {
                tripTO.setStatusId("13");
                System.out.println("abc");
                path = "content/trip/tripFinanceClosure.jsp";
            } else {
                System.out.println("cdb");
                tripTO.setStatusId("10");
//                path = "content/trip/tripExpense.jsp";
                path = "content/trip/endTripSheet.jsp";
            }
            if (request.getParameter("tripExpense").equals("1")) {
//                mv = handleViewTripSheet(request, response, command);
                mv = handleViewEndTripSheet(request, response, command);
            } else {
                mv = handleViewTripExpense(request, response, command);
            }
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView saveDoneOtherTripExpense(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        ModelAndView mv = null;
        String menuPath = "Operation  >>  Trip Closure ";
        int insertStatus = 0;
        String tripSheetId = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
            }
            tripSheetId = request.getParameter("tripSheetId");
            System.out.println("this is tht" + tripSheetId);
            String editId = request.getParameter("editId");
            insertStatus = tripBP.updateTripOtherExpenseDoneStatus(tripTO, userId);
            String otherExpenseDoneStatus = tripBP.getOtherExpenseDoneStatus(tripTO);
            request.setAttribute("otherExpenseDoneStatus", otherExpenseDoneStatus);
            path = "content/trip/tripFinanceClosure.jsp";
            mv = handleViewTripExpense(request, response, command);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView viewExpensePODDetailsOLD(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String tripSheetId = "";
        String menuPath = "Operation  >>  Edit Start Trip Sheet ";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            if (tripCommand.getTripExpenseId() != null && !tripCommand.getTripExpenseId().equals("")) {
                tripTO.setTripExpenseId(tripCommand.getTripExpenseId());
                request.setAttribute("tripExpenseId", tripCommand.getTripExpenseId());
            }
            System.out.println("this is the file upload563456875" + tripCommand.getTripExpenseId());
            path = "content/trip/tripExpensePod.jsp";

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewExpensePODDetails(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String tripSheetId = "";
        String menuPath = "Operation  >>  Edit Start Trip Sheet ";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            if (tripCommand.getTripExpenseId() != null && !tripCommand.getTripExpenseId().equals("")) {
                tripTO.setTripExpenseId(tripCommand.getTripExpenseId());
                request.setAttribute("tripExpenseId", tripCommand.getTripExpenseId());
            }
            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
                request.setAttribute("tripSheetId", tripCommand.getTripSheetId());
            }
            //String  documentRequired=request.getParameter("document_required");
            //tripTO.setDocumentRequired(documentRequired);
            System.out.println("tripid:" + tripTO.getTripSheetId());
            ArrayList tripExpenseDocumentRequiredDetails = new ArrayList();
            tripExpenseDocumentRequiredDetails = tripBP.getTripOtherExpenseDocumentRequired(tripTO);
            if (tripExpenseDocumentRequiredDetails.size() > 0) {
                request.setAttribute("tripExpenseDocumentRequiredDetails", tripExpenseDocumentRequiredDetails);
            }
            ArrayList tripExpenseDocumentFileDetails = new ArrayList();
            tripExpenseDocumentFileDetails = tripBP.getOtherExpenseFileDetails(tripTO);
            if (tripExpenseDocumentFileDetails.size() > 0) {
                request.setAttribute("tripExpenseDocumentFileDetails", tripExpenseDocumentFileDetails);
            }
            path = "content/trip/tripExpensePod.jsp";

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void saveClosureApprovalRequest(HttpServletRequest request, HttpServletResponse response, TripCommand command) throws IOException {
        int status = 0;
        String tripId = request.getParameter("tripId");
        String requestType = request.getParameter("requestType");
        String remarks = request.getParameter("remarks");
        String rcmExp = request.getParameter("rcmExp");
        String nettExp = request.getParameter("nettExp");

        HttpSession session = request.getSession();
        TripTO tripTO = null;
//         SendMail mail = new SendMail();
        try {
            int userId = (Integer) session.getAttribute("userId");
            status = tripBP.saveClosureApprovalRequest(tripId, requestType, remarks, userId, rcmExp, nettExp);
            if (status > 0) {

                //send approval mail request
                ArrayList viewApproveDetails = new ArrayList();

                //String to = "nipun.kohli@brattlefoods.com,srini@entitlesolutions.com";
                String to = "";
                String smtp = "";
                int emailPort = 0;
                String frommailid = "";
                String password = "";
                String activitycode = "EMTRP1";

                ArrayList emaildetails = new ArrayList();
                emaildetails = tripBP.getEmailDetails(activitycode);
                Iterator itr1 = emaildetails.iterator();

                if (itr1.hasNext()) {
                    tripTO = new TripTO();
                    tripTO = (TripTO) itr1.next();
                    smtp = tripTO.getSmtp();
                    emailPort = Integer.parseInt(tripTO.getPort());
                    frommailid = tripTO.getEmailId();
                    password = tripTO.getPassword();
                    to = tripTO.getTomailId();
                }

                viewApproveDetails = tripBP.viewApproveDetails(tripId, status);
                String tripid = "", cnotename = "", vehicletype = "", vehicleno = "", routename = "";
                String driver = "", planneddate = "", actualadvancepaid = "", requeston = "",
                        appremarks = "", requesttype = "", totalrunkm = "", totalrunhm = "", rcmexpense = "", systemexpense = "", tripCode = "";

                Iterator itr = viewApproveDetails.iterator();
                String customerName = "";
                String vehicleId = "";
                if (itr.hasNext()) {
                    tripTO = new TripTO();
                    tripTO = (TripTO) itr.next();

                    tripid = tripTO.getTripId();
                    customerName = tripTO.getCustomerName();
                    vehicleId = tripTO.getVehicleId();
                    cnotename = tripTO.getcNotes();
                    vehicletype = tripTO.getVehicleTypeName();
                    vehicleno = tripTO.getRegNo();
                    routename = tripTO.getRouteName();
                    driver = tripTO.getDriverName();
                    planneddate = tripTO.getTripPlannedDate();
                    actualadvancepaid = tripTO.getPaidAdvance();

                    requeston = tripTO.getRequeston();
                    appremarks = tripTO.getRequestremarks();
                    requesttype = tripTO.getRequestType();
                    totalrunkm = tripTO.getTotalRumKM();
                    totalrunhm = tripTO.getTotalRumHM();
                    rcmexpense = tripTO.getRcmExpense();
                    systemexpense = tripTO.getSystemExpense();
                    tripCode = tripTO.getTripCode();
                }

                ArrayList emailList = tripBP.getmailcontactDetails(tripid);
                TripTO tripTONew = null;
                itr = emailList.iterator();
                String fcHeadEmailId = "";
                if (itr.hasNext()) {
                    tripTONew = new TripTO();
                    tripTONew = (TripTO) itr.next();
                    fcHeadEmailId = tripTONew.getFcHeadEmail();
                }
                String cc = to;
                to = fcHeadEmailId;

                String subpart = "";

                if (requesttype.equals("1")) {
                    subpart = "<tr><td>&nbsp;&nbsp;Request Type</td><td>&nbsp;&nbsp;Odometer Usage</td></tr>"
                            + "<tr><td>&nbsp;&nbsp;Total Run Km</td><td>&nbsp;&nbsp;" + totalrunkm + "</td></tr>"
                            + "<tr><td>&nbsp;&nbsp;Total Run Hm</td><td>&nbsp;&nbsp;" + totalrunhm + "</td></tr>";
                } else {
                    subpart = "<tr><td>&nbsp;&nbsp;Request Type</td><td>&nbsp;&nbsp;Expense Deviation</td></tr>"
                            + "<tr><td>&nbsp;&nbsp;RCM Expense</td><td>&nbsp;&nbsp;" + rcmexpense + "</td></tr>"
                            + "<tr><td>&nbsp;&nbsp;System Expense</td><td>&nbsp;&nbsp;" + systemexpense + "</td></tr>";
                }

                String emailFormat = "<html>"
                        + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                        + "<tr>"
                        + "<th colspan='2'>Trip Closure Approval Confirmation</th>"
                        + "</tr>"
                        + "<tr><td>&nbsp;Trip Code</td><td>&nbsp;&nbsp;" + tripCode + "</td></tr>"
                        + "<tr><td>&nbsp;Cnote Name</td><td>&nbsp;&nbsp;" + cnotename + "</td></tr>"
                        + "<tr><td>&nbsp;Vehicle Type</td><td>&nbsp;&nbsp;" + vehicletype + "</td></tr>"
                        + "<tr><td>&nbsp;Vehicle No</td><td>&nbsp;&nbsp;" + vehicleno + "</td></tr>"
                        + "<tr><td>&nbsp;Route Name</td><td>&nbsp;&nbsp;" + routename + "</td></tr>"
                        + "<tr><td>&nbsp;Driver Name</td><td>&nbsp;&nbsp;" + driver + "</td></tr>"
                        + "<tr><td>&nbsp;Planned Date</td><td>&nbsp;&nbsp;" + planneddate + "</td></tr>"
                        + "<tr><td>&nbsp;Actual Advance Paid</td><td>&nbsp;&nbsp;" + actualadvancepaid + "</td></tr>"
                        + subpart
                        + "<tr><td>&nbsp;&nbsp;Request On</td><td>&nbsp;&nbsp;" + requeston + "</td></tr>"
                        + "<tr><td>&nbsp;&nbsp;Remarks</td><td>&nbsp;&nbsp;" + appremarks + "</td></tr>"
                        + "<tr height='25'><td></td><td></td></tr>"
                        + "<tr><td colspan='2' align='center'>"
                        //                        + "<a style='text-decoration: none' href='http://203.124.105.244:8089/KerryDemo/saveTripClosureapprove.do?userId=1081&tripid=" + tripid + "&cnote=" + cnotename + "&routename=" + routename + "&vehicleno=" + vehicleno + "&tripclosureid=" + status + "&approvestatus=1'>Approve</a>&nbsp;|&nbsp;"
                        //                        + "<a style='text-decoration: none' href='http://203.124.105.244:8089/KerryDemo/saveTripClosureapprove.do?userId=1081&tripid=" + tripid + "&cnote=" + cnotename + "&routename=" + routename + "&vehicleno=" + vehicleno + "&tripclosureid=" + status + "&approvestatus=2'>Reject</a>"
                        + "</td></tr>"
                        + "</table></body></html>";

                //String subject = "Trip Closure Approval for Consignment Note - " + cnotename;
                String subject = "Trip Closure Approval Request for Customer " + customerName + " Vehicle " + vehicleno + " Route " + routename;
                String content = emailFormat;

                // new SendMail(smtp, emailPort, frommailid, password, subject, content, to, cc).start();
                //fetch mail content
            }
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(status);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Model data in Ajax --> " + exception);
        }
    }

    public ModelAndView saveExpensePODDetailsOLD(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  Edit Start Trip Sheet ";
        int insertStatus = 0;
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        int i = 0;
        int j = 0;
        String tripExpenseId1 = "";
        String cityId1 = "";
        String podRemarks1 = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                System.out.println("this is tht");
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();

                        if (!"".equals(uploadedFileName) && uploadedFileName != null) {

                            String[] splitFileName = uploadedFileName.split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath = tempServerFilePath + "\\" + fileSavedAs;
                            actualFilePath = actualServerFilePath + "\\" + uploadedFileName;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
                            String part1 = parts.replace("\\", "");
                        }

                        System.out.println("fileName..." + fileName);
                        j++;
                    } else if (partObj.isParam()) {
                        if (partObj.getName().equals("tripExpenseId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            tripExpenseId1 = paramPart.getStringValue();
                        }

                    }
                }
            }

            int status = tripBP.saveTripExpensePodDetailsOLD(actualFilePath, tripExpenseId1, userId);

            path = "content/trip/tripPOD.jsp";

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveExpensePODDetails(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        ModelAndView mv = null;
        String menuPath = "Operation  >>  Edit Start Trip Sheet ";
        int insertStatus = 0;
        String newFileName = "", actualFilePath = "";
        String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", tripSheetId = "";
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        int i = 0;
        int j = 0;
        int r = 0;
        int s = 0;
        int t = 0;
        String[] tripExpenseId1 = new String[10];
        String[] fileSaved = new String[10];
        String[] uploadedFileName = new String[10];
        String[] tempFilePath = new String[10];
        String cityId1 = "";
        String podRemarks1 = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                System.out.println("this is tht");
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    Date now = new Date();
                    String date = now.getDate() + "" + (now.getMonth() + 1) + "" + (now.getYear() + 1900);
                    String time = now.getHours() + "" + now.getMinutes() + "" + now.getSeconds() + userId;
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName[j] = fPart.getFileName();

                        if (!"".equals(uploadedFileName[j]) && uploadedFileName[j] != null) {

                            String[] splitFileName = uploadedFileName[j].split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            fileSaved[j] = splitFileName[0] + date + time + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath[j] = tempServerFilePath + "\\" + fileSavedAs;
                            actualFilePath = actualServerFilePath + "\\" + uploadedFileName;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath[j]));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
                            String part1 = parts.replace("\\", "");
                        }

                        System.out.println("fileName..." + fileName);
                        j++;
                    } else if (partObj.isParam()) {
                        if (partObj.getName().equals("tripExpenseId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            tripExpenseId1[r] = paramPart.getStringValue();
                            r++;
                        }
                        if (partObj.getName().equals("tripSheetId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            tripSheetId = paramPart.getStringValue();
                            r++;
                        }

                    }
                }
            }
            String file = "";
            String saveFile = "";
            String tripExpenseId = "";
            System.out.println("tempFilePath.length = " + tempFilePath.length);
            for (int x = 0; x < j; x++) {
                file = tempFilePath[x];
                saveFile = fileSaved[x];
                tripExpenseId = tripExpenseId1[x];
                System.out.println("tempFilePath[x] = " + tempFilePath[x]);
                if (tempFilePath[x] != null) {
                    int status = tripBP.saveTripExpensePodDetails(file, saveFile, tripExpenseId, userId);
                }
            }

            // path = "content/trip/tripExpensePod.jsp";
            tripTO.setTripSheetId(tripSheetId);
            System.out.println("tripid:" + tripTO.getTripSheetId());
            ArrayList tripExpenseDocumentRequiredDetails = new ArrayList();
            tripExpenseDocumentRequiredDetails = tripBP.getTripOtherExpenseDocumentRequired(tripTO);
            if (tripExpenseDocumentRequiredDetails.size() > 0) {
                request.setAttribute("tripExpenseDocumentRequiredDetails", tripExpenseDocumentRequiredDetails);
            }
            ArrayList tripExpenseDocumentFileDetails = new ArrayList();
            tripExpenseDocumentFileDetails = tripBP.getOtherExpenseFileDetails(tripTO);
            if (tripExpenseDocumentFileDetails.size() > 0) {
                request.setAttribute("tripExpenseDocumentFileDetails", tripExpenseDocumentFileDetails);
            }
//            mv = viewExpensePODDetails(request, response, command);
            path = "content/trip/tripExpensePod.jsp";

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
//        return mv;
    }

    public ModelAndView deleteExpensePodFiles(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Expense Bill Upload >>  Delete ";
        int insertStatus = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Expense Bill Upload ";
            request.setAttribute("pageTitle", pageTitle);
            TripTO tripTO = new TripTO();

            String expenseId = request.getParameter("expenseId");
            tripTO.setExpenseId(expenseId);

            int deletestatus = 0;
            deletestatus = tripBP.deleteExpenaseBillCopy(tripTO);

            if (deletestatus > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Bill Copy Deleted Succesfully");
            }

            mv = viewExpensePODDetails(request, response, command);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        // return new ModelAndView(path);
        return mv;
    }

//Nithya end 7 dec
    public void checkPreStartRoute(HttpServletRequest request, HttpServletResponse response, TripCommand command) throws IOException {
        String suggestions = "";
        String preStartLocationId = request.getParameter("preStartLocationId");
        String originId = request.getParameter("originId");
        String vehicleTypeId = request.getParameter("vehicleTypeId");

        try {
            suggestions = tripBP.checkPreStartRoute(preStartLocationId, originId, vehicleTypeId);
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(suggestions);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Model data in Ajax --> " + exception);
        }
    }
//madhan  starts 24 12 2013

    public ModelAndView tripStakeHolders(HttpServletRequest request, HttpServletResponse reponse, TripCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();

        String path = "";
        String menuPath = "";
        menuPath = "User  >>  Role Function ";
        String pageTitle = "Role Function";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            ArrayList holdersList = new ArrayList();
            holdersList = tripBP.getTripStakeHoldersList();
            request.setAttribute("holdersList", holdersList);
            System.out.println("tripBP.getTripStakeHoldersList() Size" + holdersList.size());

            ArrayList statusList = new ArrayList();
            statusList = tripBP.getstatusList();
            request.setAttribute("statusList", statusList);
            System.out.println("tripBP.getstatusList() Size" + statusList.size());

            request.setAttribute("statusList", statusList);
            path = "/BrattleFoods/tripEmailSettings.jsp";

//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to view role details --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);

    }

    /**
     * This method used to Get All Functions.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView getEmailFunction(HttpServletRequest request, HttpServletResponse reponse, TripCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();

        String path = "";
        TripCommand TripCommand = command;
        // System.out.println("in getFunctions ");
        String menuPath = "";
        menuPath = "User  >>  Role Function ";
        String pageTitle = "Role Function";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String stackHolderId = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            stackHolderId = TripCommand.getStakeHolderId();
            request.setAttribute("stackHolderId", stackHolderId);

            ArrayList assignedFunc = new ArrayList();
            assignedFunc = tripBP.getAssignedFunc(stackHolderId);
            System.out.println("tripBP.getAssignedFunc Size" + assignedFunc.size());
            request.setAttribute("assignedFunc", assignedFunc);

            ArrayList statusList = new ArrayList();
            statusList = tripBP.getEditStatuslist(stackHolderId);
            request.setAttribute("statusList", statusList);
            System.out.println("tripBP.getEditStatusList() Size" + statusList.size());

            ArrayList holdersList = new ArrayList();
            holdersList = tripBP.getTripStakeHoldersList();
            request.setAttribute("holdersList", holdersList);
            System.out.println("tripBP.getTripStakeHoldersList() Size" + holdersList.size());

            path = "/BrattleFoods/tripEmailSettings.jsp";

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to view role details --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);
    }

    /**
     * This method used to Get All Functions.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView inserStakeHolderFunction(HttpServletRequest request, HttpServletResponse reponse, TripCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";
        TripCommand TripCommand = command;
        // System.out.println("in getFunctions ");
        String menuPath = "";
        menuPath = "User  >>  Role Function ";
        String pageTitle = "Role Function";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String stackHolderId = "";

        try {
            ArrayList holdersList = new ArrayList();
            holdersList = tripBP.getTripStakeHoldersList();
            request.setAttribute("holdersList", holdersList);
            System.out.println("tripBP.getTripStakeHoldersList() Size" + holdersList.size());

            ArrayList statusList = new ArrayList();
            statusList = tripBP.getEditStatuslist(stackHolderId);
            request.setAttribute("statusList", statusList);
            System.out.println("tripBP.getEditStatusList() Size" + statusList.size());

            //int stackHolderId = Integer.parseInt(TripCommand.getStakeHolderId());
            System.out.println("the tripSheetId is" + TripCommand.getStakeHolderId());
            String[] assignedFunc = request.getParameterValues("assignedfunc");
            if (assignedFunc != null) {
                stackHolderId = TripCommand.getStakeHolderId();
                System.out.println("insert Id ++++++" + assignedFunc);
                int userId = (Integer) session.getAttribute("userId");
                int status = tripBP.insertEmailSettings(assignedFunc, stackHolderId, userId);
                path = "/BrattleFoods/tripEmailSettings.jsp";
            } else {
                stackHolderId = TripCommand.getStakeHolderId();
                int userId = (Integer) session.getAttribute("userId");
                int status = tripBP.deleteStakeholder(assignedFunc, stackHolderId, userId);
                path = "/BrattleFoods/tripEmailSettings.jsp";
            }
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Email Function Assigned Successfully");
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to view role details --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);
    }

    public ModelAndView viewVehicleDriverAdvance(HttpServletRequest request, HttpServletResponse reponse, TripCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();

        String path = "";
        String menuPath = "";
        menuPath = "Operation  >>  Vehicle Advance ";
        String pageTitle = "Vehicle Advance";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        TripTO tripTO = new TripTO();
        try {
            String usageTypeId = request.getParameter("usageTypeId");
            System.out.println("usageTypeId = " + usageTypeId);
            tripTO.setUsageTypeId(usageTypeId);
            ArrayList statusList = new ArrayList();
            statusList = tripBP.getVehicleDriverAdvanceList(tripTO);
            request.setAttribute("statusList", statusList);
            request.setAttribute("usageTypeId", usageTypeId);
            path = "content/trip/vehicleDriverAdvance.jsp";

//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to view role details --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);

    }

    public ModelAndView saveVehicleDriverAdvance(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View Vehicle Advance ";
        int insertVehicleDriverAdvance = 0;
        int insertStatus = 0;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            if (tripCommand.getVehicleId() != null && !tripCommand.getVehicleId().equals("")) {
                tripTO.setVehicleId(tripCommand.getVehicleId());
            }
            if (tripCommand.getAdvanceAmount() != null && !tripCommand.getAdvanceAmount().equals("")) {
                tripTO.setAdvanceAmount(tripCommand.getAdvanceAmount());
            }
            if (tripCommand.getAdvanceDate() != null && !tripCommand.getAdvanceDate().equals("")) {
                tripTO.setAdvanceDate(tripCommand.getAdvanceDate());
            }
            if (tripCommand.getAdvanceRemarks() != null && !tripCommand.getAdvanceRemarks().equals("")) {
                tripTO.setAdvanceRemarks(tripCommand.getAdvanceRemarks());
            }
            if (tripCommand.getPrimaryDriverId() != null && !tripCommand.getPrimaryDriverId().equals("")) {
                tripTO.setPrimaryDriverId(tripCommand.getPrimaryDriverId());
            }
            if (tripCommand.getSecondaryDriverIdTwo() != null && !tripCommand.getSecondaryDriverIdTwo().equals("")) {
                tripTO.setSecondaryDriverIdTwo(tripCommand.getSecondaryDriverIdTwo());
            }
            if (tripCommand.getSecondaryDriverIdOne() != null && !tripCommand.getSecondaryDriverIdOne().equals("")) {
                tripTO.setSecondaryDriverIdOne(tripCommand.getSecondaryDriverIdOne());
            }
            if (tripCommand.getExpenseType() != null && !tripCommand.getExpenseType().equals("")) {
                tripTO.setExpenseType(tripCommand.getExpenseType());
            }
            String vehicleId = request.getParameter("vehicleId");
            String vehicleNo = request.getParameter("vehicleNo");
            String primaryDriver = request.getParameter("primaryDriver");
            String advanceAmount = tripCommand.getAdvanceAmount();
            String advanceDate = tripCommand.getAdvanceDate();
            String advanceRemarks = tripCommand.getAdvanceRemarks();
            String expenseType = tripCommand.getExpenseType();
            insertVehicleDriverAdvance = tripBP.insertVehicleDriverAdvance(tripTO, userId);
            tripTO.setApprovalStatus("1");
            String vehicleDriverAdvanceId = "";
            if (insertVehicleDriverAdvance > 0) {
                vehicleDriverAdvanceId = String.valueOf(insertVehicleDriverAdvance);
                tripTO.setVehicleDriverAdvanceId(String.valueOf(insertVehicleDriverAdvance));
                tripTO.setUserId(userId);
                insertStatus = tripBP.saveVehicleDriverAdvanceApproval(tripTO);
            }
            String usageTypeId = request.getParameter("usageTypeId");
            tripTO.setUsageTypeId(usageTypeId);
            ArrayList statusList = new ArrayList();
            statusList = tripBP.getVehicleDriverAdvanceList(tripTO);
            request.setAttribute("statusList", statusList);
            path = "content/trip/vehicleDriverAdvance.jsp";
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Advance Paid Sucessfully  ");

            //String to = "nipun.kohli@brattlefoods.com,srini@entitlesolutions.com";
            String emailTo = "";
            String emailCc = "";
            //String cc = "nithyap@entitlesolutions.com";

            String smtp = "";
            int emailPort = 0;
            String frommailid = "";
            String password = "";
            String activitycode = "";
            String toMailId = "";

            //Primary Operation Vehicles
            if (usageTypeId.equals("2")) {
                activitycode = "PRI-VEH-ADV";
                ArrayList emaildetails = new ArrayList();
                emaildetails = tripBP.getEmailDetails(activitycode);
                Iterator itr1 = emaildetails.iterator();

                if (itr1.hasNext()) {
                    tripTO = new TripTO();
                    tripTO = (TripTO) itr1.next();
                    smtp = tripTO.getSmtp();
                    emailPort = Integer.parseInt(tripTO.getPort());
                    frommailid = tripTO.getEmailId();
                    password = tripTO.getPassword();
                    toMailId = tripTO.getTomailId();
                    emailTo = "";
                    emailCc = "";
                }

                ArrayList getApprovalValueDetails = new ArrayList();
                TripTO tripTO1 = null;
                String email = "";
                String emailInfoTo = "";
                String emailInfoCc = "";
                int approvestatus = 0;
                if (expenseType.equals("Food")) {
                    getApprovalValueDetails = tripBP.getApprovalValueDetails(Double.parseDouble(advanceAmount));
                    Iterator itr = getApprovalValueDetails.iterator();
                    while (itr.hasNext()) {
                        tripTO1 = new TripTO();
                        tripTO1 = (TripTO) itr.next();
                        emailTo = tripTO1.getEmailId();
                        emailCc = tripTO1.getEmailCc();
                        emailInfoTo = tripTO1.getInfoEmailTo();
                        emailInfoCc = tripTO1.getInfoEmailCc();
                        approvestatus = Integer.parseInt(tripTO1.getApprovalstatus());
                    }
                } else if (expenseType.equals("Repair")) {
                    int configId = 13;
                    getApprovalValueDetails = tripBP.getRepairMaintenence(configId);
                    Iterator itr = getApprovalValueDetails.iterator();
                    while (itr.hasNext()) {
                        tripTO1 = new TripTO();
                        tripTO1 = (TripTO) itr.next();
                        emailTo = tripTO1.getEmailId();
                        emailCc = tripTO1.getEmailCc();
                        emailInfoTo = tripTO1.getInfoEmailTo();
                        emailInfoCc = tripTO1.getInfoEmailCc();
                        approvestatus = Integer.parseInt(tripTO1.getApprovalstatus());
                    }
                }

                String emailFormat = "<html>"
                        + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                        + "<tr>"
                        + "<th colspan='2'>Vehicle Driver Advance</th>"
                        + "</tr>"
                        + "<tr><td>&nbsp;Expense Type</td><td>&nbsp;&nbsp;" + expenseType + "</td></tr>"
                        + "<tr><td>&nbsp;Vehicle No</td><td>&nbsp;&nbsp;" + vehicleNo + "</td></tr>"
                        + "<tr><td>&nbsp;Driver Name</td><td>&nbsp;&nbsp;" + primaryDriver + "</td></tr>"
                        + "<tr><td>&nbsp;Advance Amount</td><td>&nbsp;&nbsp;" + advanceAmount + "</td></tr>"
                        + "<tr><td>&nbsp;Advance Date</td><td>&nbsp;&nbsp;" + advanceDate + "</td></tr>"
                        + "<tr><td>&nbsp;&nbsp;Remarks</td><td>&nbsp;&nbsp;" + advanceRemarks + "</td></tr>"
                        + "<tr><td colspan='2' align='center'>"
                        + "<a style='text-decoration: none' href='http://203.124.105.244:8089/KerryDemo/saveVehicleDriverAdvanceApproval.do?mailId=" + emailTo + "&userId=1081&vehicleDriverAdvanceId=" + vehicleDriverAdvanceId + "&vehicleId=" + vehicleId + "&expenseType=" + expenseType + "&advanceDate=" + advanceDate + "&advanceRemarks=" + advanceRemarks + "&vehicleNo=" + vehicleNo + "&primaryDriver=" + primaryDriver + "&usageTypeId=1" + "&advanceAmount=" + advanceAmount + "&approvestatus=2'>Approve</a>&nbsp;|&nbsp;"
                        + "<a style='text-decoration: none' href='http://203.124.105.244:8089/KerryDemo/saveVehicleDriverAdvanceApproval.do?mailId=" + emailTo + "&userId=1081&vehicleDriverAdvanceId=" + vehicleDriverAdvanceId + "&vehicleId=" + vehicleId + "&expenseType=" + expenseType + "&advanceDate=" + advanceDate + "&advanceRemarks=" + advanceRemarks + "&vehicleNo=" + vehicleNo + "&primaryDriver=" + primaryDriver + "&usageTypeId=1" + "&advanceAmount=" + advanceAmount + "&approvestatus=3'>Reject</a>"
                        //                    + "<a style='text-decoration: none' href='http://192.168.2.43:8084/KerryDemo/saveVehicleDriverAdvanceApproval.do?mailId=" + emailTo + "&userId=1081&vehicleDriverAdvanceId=" + vehicleDriverAdvanceId + "&vehicleId=" + vehicleId + "&expenseType=" + expenseType + "&advanceDate=" + advanceDate + "&advanceRemarks=" + advanceRemarks + "&vehicleNo=" + vehicleNo + "&primaryDriver=" + primaryDriver + "&advanceAmount=" + advanceAmount + "&approvestatus=2'>Approve</a>&nbsp;|&nbsp;"
                        //                    + "<a style='text-decoration: none' href='http://192.168.2.43:8084/KerryDemo/saveVehicleDriverAdvanceApproval.do?mailId=" + emailTo + "&userId=1081&vehicleDriverAdvanceId=" + vehicleDriverAdvanceId + "&vehicleId=" + vehicleId + "&expenseType=" + expenseType + "&advanceDate=" + advanceDate + "&advanceRemarks=" + advanceRemarks + "&vehicleNo=" + vehicleNo + "&primaryDriver=" + primaryDriver + "&advanceAmount=" + advanceAmount + "&approvestatus=3'>Reject</a>"
                        + "</td></tr>"
                        + "</table></body></html>";

                String subject = "Vehicle Driver Advance Approval Request for Vehicle -" + vehicleNo + " Driver " + primaryDriver;
                String content = emailFormat;
                int mailSendingId = 0;
                tripTO.setMailTypeId("2");
                tripTO.setMailSubjectTo(subject);
                tripTO.setMailSubjectCc(subject);
                tripTO.setMailSubjectBcc("");
                tripTO.setMailContentTo(content);
                tripTO.setMailContentCc(content);
                tripTO.setMailContentBcc("");
                tripTO.setMailIdTo(emailTo);
                tripTO.setMailIdCc(emailCc);
                tripTO.setMailIdBcc("");
//            mailSendingId = tripBP.insertMailDetails(tripTO, userId);
                new SendMail(smtp, emailPort, frommailid, password, subject, content, emailTo, emailCc, userId).start();

                String fcLeadMailId = "";
                if (usageTypeId.equals("2")) {
                    fcLeadMailId = tripBP.getFCLeadMailId(vehicleId);
                } else if (usageTypeId.equals("1")) {
                    fcLeadMailId = tripBP.getSecondaryFCLeadMailId(vehicleId);
                }

                emailFormat = "<html>"
                        + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                        + "<tr>"
                        + "<th colspan='2'>Vehicle Driver Advance</th>"
                        + "</tr>"
                        + "<tr><td>&nbsp;Expense Type</td><td>&nbsp;&nbsp;" + expenseType + "</td></tr>"
                        + "<tr><td>&nbsp;Vehicle No</td><td>&nbsp;&nbsp;" + vehicleNo + "</td></tr>"
                        + "<tr><td>&nbsp;Driver Name</td><td>&nbsp;&nbsp;" + primaryDriver + "</td></tr>"
                        + "<tr><td>&nbsp;Advance Amount</td><td>&nbsp;&nbsp;" + advanceAmount + "</td></tr>"
                        + "<tr><td>&nbsp;Advance Date</td><td>&nbsp;&nbsp;" + advanceDate + "</td></tr>"
                        + "<tr><td>&nbsp;&nbsp;Remarks</td><td>&nbsp;&nbsp;" + advanceRemarks + "</td></tr>"
                        + "<tr><td colspan='2' align='center'>"
                        + "</td></tr>"
                        + "</table></body></html>";

                subject = "Vehicle Driver Advance Approval Request for Vehicle -" + vehicleNo + " Driver " + primaryDriver;
                content = emailFormat;
                tripTO.setMailTypeId("2");
                tripTO.setMailSubjectTo(subject);
                tripTO.setMailSubjectCc(subject);
                tripTO.setMailSubjectBcc("");
                tripTO.setMailContentTo(content);
                tripTO.setMailContentCc(content);
                tripTO.setMailContentBcc("");
                tripTO.setMailIdTo(emailInfoTo + "," + fcLeadMailId);
                tripTO.setMailIdCc(emailCc);
                tripTO.setMailIdBcc("");
//            mailSendingId = tripBP.insertMailDetails(tripTO, userId);
                new SendMail(smtp, emailPort, frommailid, password, subject, content, emailInfoTo + "," + fcLeadMailId, emailCc, userId).start();

                //Secondary Operation Vehicles
            } else if (usageTypeId.equals("1")) {
                activitycode = "SEC-VEH-ADV";
                ArrayList emaildetails = new ArrayList();
                emaildetails = tripBP.getEmailDetails(activitycode);
                Iterator itr1 = emaildetails.iterator();

                if (itr1.hasNext()) {
                    tripTO = new TripTO();
                    tripTO = (TripTO) itr1.next();
                    smtp = tripTO.getSmtp();
                    emailPort = Integer.parseInt(tripTO.getPort());
                    frommailid = tripTO.getEmailId();
                    password = tripTO.getPassword();
                    toMailId = tripTO.getTomailId();
                    emailTo = "";
                    emailCc = "";
                }
                String secondaryApprovalPerson = "";
                secondaryApprovalPerson = tripBP.getSecondaryApprovalPerson(vehicleId);
                emailTo = secondaryApprovalPerson;
                String emailFormat = "<html>"
                        + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                        + "<tr>"
                        + "<th colspan='2'>Vehicle Driver Advance</th>"
                        + "</tr>"
                        + "<tr><td>&nbsp;Expense Type</td><td>&nbsp;&nbsp;" + expenseType + "</td></tr>"
                        + "<tr><td>&nbsp;Vehicle No</td><td>&nbsp;&nbsp;" + vehicleNo + "</td></tr>"
                        + "<tr><td>&nbsp;Driver Name</td><td>&nbsp;&nbsp;" + primaryDriver + "</td></tr>"
                        + "<tr><td>&nbsp;Advance Amount</td><td>&nbsp;&nbsp;" + advanceAmount + "</td></tr>"
                        + "<tr><td>&nbsp;Advance Date</td><td>&nbsp;&nbsp;" + advanceDate + "</td></tr>"
                        + "<tr><td>&nbsp;&nbsp;Remarks</td><td>&nbsp;&nbsp;" + advanceRemarks + "</td></tr>"
                        + "<tr><td colspan='2' align='center'>"
                        + "<a style='text-decoration: none' href='http://203.124.105.244:8089/KerryDemo/saveVehicleDriverAdvanceApproval.do?mailId=" + emailTo + "&userId=1081&vehicleDriverAdvanceId=" + vehicleDriverAdvanceId + "&vehicleId=" + vehicleId + "&expenseType=" + expenseType + "&advanceDate=" + advanceDate + "&advanceRemarks=" + advanceRemarks + "&vehicleNo=" + vehicleNo + "&primaryDriver=" + primaryDriver + "&usageTypeId=1" + "&advanceAmount=" + advanceAmount + "&approvestatus=2'>Approve</a>&nbsp;|&nbsp;"
                        + "<a style='text-decoration: none' href='http://203.124.105.244:8089/KerryDemo/saveVehicleDriverAdvanceApproval.do?mailId=" + emailTo + "&userId=1081&vehicleDriverAdvanceId=" + vehicleDriverAdvanceId + "&vehicleId=" + vehicleId + "&expenseType=" + expenseType + "&advanceDate=" + advanceDate + "&advanceRemarks=" + advanceRemarks + "&vehicleNo=" + vehicleNo + "&primaryDriver=" + primaryDriver + "&usageTypeId=1" + "&advanceAmount=" + advanceAmount + "&approvestatus=3'>Reject</a>"
                        //                    + "<a style='text-decoration: none' href='http://192.168.2.43:8084/KerryDemo/saveVehicleDriverAdvanceApproval.do?mailId=" + emailTo + "&userId=1081&vehicleDriverAdvanceId=" + vehicleDriverAdvanceId + "&vehicleId=" + vehicleId + "&expenseType=" + expenseType + "&advanceDate=" + advanceDate + "&advanceRemarks=" + advanceRemarks + "&vehicleNo=" + vehicleNo + "&primaryDriver=" + primaryDriver + "&advanceAmount=" + advanceAmount + "&approvestatus=2'>Approve</a>&nbsp;|&nbsp;"
                        //                    + "<a style='text-decoration: none' href='http://192.168.2.43:8084/KerryDemo/saveVehicleDriverAdvanceApproval.do?mailId=" + emailTo + "&userId=1081&vehicleDriverAdvanceId=" + vehicleDriverAdvanceId + "&vehicleId=" + vehicleId + "&expenseType=" + expenseType + "&advanceDate=" + advanceDate + "&advanceRemarks=" + advanceRemarks + "&vehicleNo=" + vehicleNo + "&primaryDriver=" + primaryDriver + "&advanceAmount=" + advanceAmount + "&approvestatus=3'>Reject</a>"
                        + "</td></tr>"
                        + "</table></body></html>";

                String subject = "Vehicle Driver Advance Approval Request for Vehicle -" + vehicleNo + " Driver " + primaryDriver;
                String content = emailFormat;
                int mailSendingId = 0;
                tripTO.setMailTypeId("2");
                tripTO.setMailSubjectTo(subject);
                tripTO.setMailSubjectCc(subject);
                tripTO.setMailSubjectBcc("");
                tripTO.setMailContentTo(content);
                tripTO.setMailContentCc(content);
                tripTO.setMailContentBcc("");
                tripTO.setMailIdTo(emailTo);
                tripTO.setMailIdCc(emailCc);
                tripTO.setMailIdBcc("");

//            mailSendingId = tripBP.insertMailDetails(tripTO, userId);
                new SendMail(smtp, emailPort, frommailid, password, subject, content, emailTo, emailCc, userId).start();

                String fcLeadMailId = "";
                if (usageTypeId.equals("2")) {
                    fcLeadMailId = tripBP.getFCLeadMailId(vehicleId);
                } else if (usageTypeId.equals("1")) {
                    fcLeadMailId = tripBP.getSecondaryFCLeadMailId(vehicleId);
                }

                emailFormat = "<html>"
                        + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                        + "<tr>"
                        + "<th colspan='2'>Vehicle Driver Advance</th>"
                        + "</tr>"
                        + "<tr><td>&nbsp;Expense Type</td><td>&nbsp;&nbsp;" + expenseType + "</td></tr>"
                        + "<tr><td>&nbsp;Vehicle No</td><td>&nbsp;&nbsp;" + vehicleNo + "</td></tr>"
                        + "<tr><td>&nbsp;Driver Name</td><td>&nbsp;&nbsp;" + primaryDriver + "</td></tr>"
                        + "<tr><td>&nbsp;Advance Amount</td><td>&nbsp;&nbsp;" + advanceAmount + "</td></tr>"
                        + "<tr><td>&nbsp;Advance Date</td><td>&nbsp;&nbsp;" + advanceDate + "</td></tr>"
                        + "<tr><td>&nbsp;&nbsp;Remarks</td><td>&nbsp;&nbsp;" + advanceRemarks + "</td></tr>"
                        + "<tr><td colspan='2' align='center'>"
                        + "</td></tr>"
                        + "</table></body></html>";

                subject = "Vehicle Driver Advance Approval Request for Vehicle -" + vehicleNo + " Driver " + primaryDriver;
                content = emailFormat;
                tripTO.setMailTypeId("2");
                tripTO.setMailSubjectTo(subject);
                tripTO.setMailSubjectCc(subject);
                tripTO.setMailSubjectBcc("");
                tripTO.setMailContentTo(content);
                tripTO.setMailContentCc(content);
                tripTO.setMailContentBcc("");
                String emailInfoTo = "";
                tripTO.setMailIdTo(emailInfoTo + "," + fcLeadMailId);
                tripTO.setMailIdCc(emailCc);
                tripTO.setMailIdBcc("");
//            mailSendingId = tripBP.insertMailDetails(tripTO, userId);
                new SendMail(smtp, emailPort, frommailid, password, subject, content, toMailId + "," + fcLeadMailId, emailCc, userId).start();
            }
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public void checkVehicleDriverAdvance(HttpServletRequest request, HttpServletResponse response, TripCommand command) throws IOException {
        System.out.println("i am in ajax ");
        HttpSession session = request.getSession();
        String vehicleId = request.getParameter("vehicleId");
        System.out.println("vehicleId" + vehicleId);
        String suggestions = tripBP.checkVehicleDriverAdvance(vehicleId);
        if (suggestions != null) {
            suggestions = suggestions;
        } else {
            suggestions = "";
        }
        System.out.println("suggestions" + suggestions);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.println(suggestions);
        writer.close();
    }
    //madhan  ends 24 12 2013

    public void getLocationName(HttpServletRequest request, HttpServletResponse response, TripCommand command) throws IOException {
        HttpSession session = request.getSession();
        tripCommand = command;
        TripTO tripTO = new TripTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList cityFromDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            response.setContentType("text/html");
            String zoneId = request.getParameter("zoneId");
            String cityId = request.getParameter("cityId");

            tripTO.setZoneId(zoneId);
            tripTO.setCityId(cityId);
            cityFromDetails = tripBP.getLocation(tripTO);
            System.out.println("userDetails.size() = " + cityFromDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = cityFromDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                tripTO = (TripTO) itr.next();
                jsonObject.put("Name", tripTO.getCityId() + "-" + tripTO.getCityName());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView bpclTransactionHistoryUpload(HttpServletRequest request, HttpServletResponse response, TripCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        int userId = 0;
        String pageTitle = "BPCL Transaction Details Uploading ";
        menuPath = "Operation >> BPCL Transaction Details Uploading";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        String transactionHistoryId = "", vehicleNo = "", accountId = "", dealerName = "", dealerCity = "", transactionDate = "", accountingDate = "", transactionType = "", currency = "", amount = "", volumeDocNo = "", amoutBalance = "", petromilesEarned = "", odometerReading = "";

        try {
            tripCommand = command;
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            path = "BrattleFoods/consignmentUploadPage.jsp";
            isMultipart = ServletFileUpload.isMultipartContent(request);
            ArrayList bpclTransactionList = new ArrayList();
            ArrayList tripWrongDataList = new ArrayList();
            int insertBPCLTransactionHistory = 0;
            int tripCount = 0;
            if (isMultipart) {
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();

                        if (!"".equals(uploadedFileName) && uploadedFileName != null) {

                            String[] splitFileName = uploadedFileName.split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath = tempServerFilePath + "\\" + fileSavedAs;
                            actualFilePath = actualServerFilePath + "\\" + uploadedFileName;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
                            String part1 = parts.replace("\\", "");

                            WorkbookSettings ws = new WorkbookSettings();
                            ws.setLocale(new Locale("en", "EN"));
                            Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                            Sheet s = workbook.getSheet(0);
                            System.out.println("rows" + userId + s.getRows());

                            ArrayList customerList = new ArrayList();
                            ArrayList productCategoryList = new ArrayList();
                            ArrayList vehicleList = new ArrayList();
                            boolean validationStatus = true;
                            boolean routeCheckReqd = true;
                            String dd = "";
                            String mm = "";
                            String yy = "";

                            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                            TripTO tripTO = new TripTO();
                            transactionHistoryId = s.getCell(1, 1).getContents();
                            request.setAttribute("transactionHistoryId", transactionHistoryId);
                            for (int i = 7; i <= s.getRows(); i++) {
                                tripTO = new TripTO();
                                vehicleNo = s.getCell(0, i).getContents();
                                tripTO.setVehicleNo(vehicleNo);

                                accountId = s.getCell(1, i).getContents();
                                tripTO.setAccountId(accountId);

                                dealerName = s.getCell(2, i).getContents();
                                tripTO.setDealerName(dealerName);

                                dealerCity = s.getCell(3, i).getContents();
                                tripTO.setDealerCity(dealerCity);

                                transactionDate = s.getCell(4, i).getContents();
                                tripTO.setTransactionDate(transactionDate);

                                accountingDate = s.getCell(5, i).getContents();
                                tripTO.setAccountingDate(accountingDate);

                                transactionType = s.getCell(6, i).getContents();
                                tripTO.setTransactionType(transactionType);

                                currency = s.getCell(7, i).getContents();
                                tripTO.setCurrency(currency);

                                amount = s.getCell(8, i).getContents();
                                tripTO.setAmount(amount);

                                volumeDocNo = s.getCell(9, i).getContents();
                                tripTO.setVolumeDocNo(volumeDocNo);

                                amoutBalance = s.getCell(10, i).getContents();
                                tripTO.setAmoutBalance(amoutBalance);

                                petromilesEarned = s.getCell(11, i).getContents();
                                tripTO.setPetromilesEarned(petromilesEarned);

                                odometerReading = s.getCell(12, i).getContents();

                                tripTO.setOdometerReading(odometerReading);
                                tripTO.setTransactionHistoryId(transactionHistoryId);
                                if ("".equals(vehicleNo) && "".equals(accountId)) {
                                    break;
                                }

                                String tripId = "";
                                tripCount = tripBP.getTripCodeCountForBpcl(tripTO);
                                System.out.println("tripCount in the controller = " + tripCount);
                                if (tripCount <= 1) {
                                    tripId = tripBP.getTripCodeForBpcl(tripTO);
                                    System.out.println("tripId in the controller = " + tripId);
                                    String[] temp = null;
                                    if (tripId != null && !"".equals(tripId)) {
                                        temp = tripId.split("~");
                                        tripTO.setTripId(temp[0]);
                                        tripTO.setTripCode(temp[1]);
                                        tripTO.setVehicleId(temp[2]);
                                    } else {
                                        tripTO.setTripId("0");
                                        tripTO.setTripCode("");
                                        tripTO.setVehicleId("0");
                                    }
                                    String checkTransactionHistoryId = tripBP.checkTransactionHistoryId(tripTO);
                                    if (checkTransactionHistoryId != null) {
                                        tripTO.setStatus("Y");
                                    } else if (checkTransactionHistoryId == null) {
                                        tripTO.setStatus("N");
                                    }
                                    bpclTransactionList.add(tripTO);
                                } else {
                                    tripWrongDataList = tripBP.getTripWrongDataList(tripTO);
                                    break;
                                }
                            }//////loop end

                        }
                    }
                }
            }
            if (tripWrongDataList.size() > 0 && tripCount > 1) {
                request.setAttribute("tripWrongDataList", tripWrongDataList);
                System.out.println("tripWrongDataList.size() we are testing = " + tripWrongDataList.size());
            } else {
                request.setAttribute("tripWrongDataList", tripWrongDataList);
                request.setAttribute("bpclTransactionList", bpclTransactionList);
            }
            path = "content/trip/bpclTransactionHistoryDetails.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    //New function
    public ModelAndView saveBpclTransactionHistory(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  BPCL Transaction Uploading";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Create Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            ArrayList bpclTransactionList = new ArrayList();
            TripTO tripTO = new TripTO();
            String[] status = null, tripId = null, vehicleId = null, vehicleNo = null, accountId = null, dealerName = null, dealerCity = null, transactionDate = null, accountingDate = null, transactionType = null, currency = null, amount = null, volumeDocNo = null, amoutBalance = null, petromilesEarned = null, odometerReading = null;
            String transactionHistoryId = "";
            transactionHistoryId = request.getParameter("transactionHistoryId");

            status = request.getParameterValues("status");
            tripId = request.getParameterValues("tripId");
            vehicleId = request.getParameterValues("vehicleId");
            vehicleNo = request.getParameterValues("vehicleNo");
            accountId = request.getParameterValues("accountId");
            dealerName = request.getParameterValues("dealerName");
            dealerCity = request.getParameterValues("dealerCity");
            transactionDate = request.getParameterValues("transactionDate");
            accountingDate = request.getParameterValues("accountingDate");
            transactionType = request.getParameterValues("transactionType");
            currency = request.getParameterValues("currency");
            amount = request.getParameterValues("amount");
            volumeDocNo = request.getParameterValues("volumeDocNo");
            amoutBalance = request.getParameterValues("amoutBalance");
            petromilesEarned = request.getParameterValues("petromilesEarned");
            odometerReading = request.getParameterValues("odometerReading");
            System.out.println("status.length = " + status.length);
            for (int i = 0; i < status.length; i++) {
                tripTO = new TripTO();
                if ("N".equals(status[i])) {
                    tripTO.setStatus(status[i]);
                    tripTO.setTripId(tripId[i]);
                    tripTO.setVehicleId(vehicleId[i]);
                    tripTO.setVehicleNo(vehicleNo[i]);
                    System.out.println("status[i] = " + status[i]);
                    System.out.println("vehicleNo[i] = " + vehicleNo[i]);
                    System.out.println("accountId[i] = " + accountId[i]);
                    tripTO.setAccountId(accountId[i]);
                    tripTO.setDealerName(dealerName[i]);
                    tripTO.setDealerCity(dealerCity[i]);
                    tripTO.setTransactionDate(transactionDate[i]);
                    tripTO.setAccountingDate(accountingDate[i]);
                    tripTO.setTransactionType(transactionType[i]);
                    tripTO.setCurrency(currency[i]);
                    tripTO.setAmount(amount[i]);
                    tripTO.setVolumeDocNo(volumeDocNo[i]);
                    tripTO.setAmoutBalance(amoutBalance[i]);
                    tripTO.setOdometerReading(odometerReading[i]);
                    bpclTransactionList.add(tripTO);
                    System.out.println("bpclTransactionlist.size() = " + bpclTransactionList.size());
                }
            }
            tripTO.setTransactionHistoryId(transactionHistoryId);
            int insertBPCLTransactionHistory = 0;
            insertBPCLTransactionHistory = tripBP.insertBPCLTransactionHistory(bpclTransactionList, tripTO, userId);
            if (insertBPCLTransactionHistory > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Excel Data Uploaded Sucessfully");
                request.setAttribute("bpclTransactionList", bpclTransactionList);
            } else {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Excel Data Not Uploaded");
            }
            path = "content/trip/bpclTransactionHistoryUpload.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView createEmptyTrip(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Operation  >>  Trip Planning ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Create Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            String vehicleTypeId = "";
            //calculate revenue if possible
            //check if vehicle type id is given in consignment order for revenue calculation
            TripTO tripTONew = null;
            TripTO trpTO = null;
            float totalRevenue = 0.00F;
            String revenue = "0";
            float totalExpense = 0.00F;
            String expense = "0";

            String vehicleId = request.getParameter("vehicleId");
            String vehicleNo = request.getParameter("vehicleNo");
            String tripType = request.getParameter("tripType");
            if (vehicleId == null) {
                vehicleId = "";
            }
            String roleId = "" + (Integer) session.getAttribute("RoleId");
            String companyId = (String) session.getAttribute("companyId");
            tripTO.setRoleId(roleId);
            tripTO.setTripType(tripType);
            tripTO.setCompanyId(companyId);
            if (!"".equals(vehicleNo)) {
                tripTO.setVehicleNo(vehicleNo);
                tripTO.setVehicleId(vehicleId);
                ArrayList vehicleDetails = new ArrayList();
                vehicleDetails = tripBP.getVehicleRegNos(tripTO);
                System.out.println("vehicleDetails.size() = " + vehicleDetails.size());
                trpTO = new TripTO();
                if (vehicleDetails.size() > 0) {
                    Iterator itr1 = vehicleDetails.iterator();
                    while (itr1.hasNext()) {
                        trpTO = (TripTO) itr1.next();
                        System.out.println("trpTO.getVehicleId() = " + trpTO.getVehicleId());
                        System.out.println("trpTO.getVehicleNo() = " + trpTO.getVehicleNo());
                        request.setAttribute("vehicleId", trpTO.getVehicleId());
                        request.setAttribute("vehicleNo", trpTO.getVehicleNo());
                        request.setAttribute("vehicleTonnage", trpTO.getVehicleTonnage());
                        request.setAttribute("primaryDriverId", trpTO.getPrimaryDriverId());
                        request.setAttribute("primaryDriverName", trpTO.getPrimaryDriverName());
                        request.setAttribute("secondaryDriver1Id", trpTO.getSecondaryDriver1Id());
                        request.setAttribute("secondaryDriver1Name", trpTO.getSecondaryDriver1Name());
                        request.setAttribute("secondaryDriver2Id", trpTO.getSecondaryDriver2Id());
                        request.setAttribute("secondaryDriver2Name", trpTO.getSecondaryDriver2Name());
                        request.setAttribute("vehicleTypeId", trpTO.getVehicleTypeId());
                        request.setAttribute("vehicleTypeName", trpTO.getVehicleTypeName());
                    }
                }

            }
            ArrayList customerDetails = new ArrayList();
            customerDetails = tripBP.getEmptyCustomerList(tripTO);
            System.out.println("Empty Customer" + customerDetails);
            request.setAttribute("customerDetails", customerDetails);
            request.setAttribute("tripType", tripType);

            String responsiveStatus = request.getParameter("respStatus");
            if (responsiveStatus != null && "Y".equals(responsiveStatus)) {
                path = "content/trip/createEmptyTripResonsive.jsp";
            } else {
                path = "content/trip/createEmptyTrip.jsp";
            }

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewWFUTripSheet(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View WFC Trip Sheet ";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
                request.setAttribute("tripSheetId", tripCommand.getTripSheetId());
            }

            ArrayList startTripDetails = new ArrayList();
            startTripDetails = tripBP.getStartTripDetails(tripTO);
            request.setAttribute("startTripDetails", startTripDetails);

            tripTO.setTripId(tripTO.getTripSheetId());

            ArrayList tripDetails = new ArrayList();
            ArrayList tripPointDetails = new ArrayList();
            ArrayList tripPreStartDetails = new ArrayList();
            ArrayList tripStartDetails = new ArrayList();
            ArrayList tripUnPackDetails = new ArrayList();
            ArrayList tripPackDetails = new ArrayList();
            tripPackDetails = tripBP.getTripPackDetails(tripTO);
            request.setAttribute("tripPackDetails", tripPackDetails);
            tripDetails = tripBP.getTripDetails(tripTO);
            tripPointDetails = tripBP.getTripPointDetails(tripTO);
            tripPreStartDetails = tripBP.getPreStartTripDetails(tripTO);
            tripStartDetails = tripBP.getStartedTripDetails(tripTO);

            tripUnPackDetails = tripBP.getTripUnPackDetails(tripTO);

            request.setAttribute("tripDetails", tripDetails);
            request.setAttribute("tripPointDetails", tripPointDetails);
            request.setAttribute("tripPreStartDetails", tripPreStartDetails);
            request.setAttribute("tripStartDetails", tripStartDetails);
            request.setAttribute("tripUnPackDetails", tripUnPackDetails);

            ArrayList statusDetails = new ArrayList();
            statusDetails = tripBP.getTripStausDetails(tripTO);
            if (statusDetails.size() > 0) {
                request.setAttribute("statusDetails", statusDetails);
            }
            String tripSheetId = request.getParameter("tripId");
            ArrayList tripAdvanceDetails = new ArrayList();
            tripAdvanceDetails = tripBP.getTripAdvanceDetails(tripTO.getTripSheetId());
            request.setAttribute("tripAdvanceDetails", tripAdvanceDetails);

            ArrayList viewPODDetails = new ArrayList();
            viewPODDetails = tripBP.getPODDetails(tripTO);
            if (viewPODDetails.size() > 0) {
                request.setAttribute("viewPODDetails", viewPODDetails);
            }
            ArrayList expiryDateDetails = new ArrayList();
            expiryDateDetails = tripBP.getExpiryDateDetails(tripTO);
            request.setAttribute("expiryDateDetails", expiryDateDetails);

            ArrayList tripAdvanceDetailsStatus = new ArrayList();
            tripAdvanceDetailsStatus = tripBP.getTripAdvanceDetailsStatus(tripTO.getTripSheetId());

            request.setAttribute("tripAdvanceDetailsStatus", tripAdvanceDetailsStatus);
            ArrayList loadDetails = new ArrayList();
            loadDetails = tripBP.getLoadDetails(tripTO);
            request.setAttribute("loadDetails", loadDetails);

            String tripType = request.getParameter("tripType");
            String statusId = request.getParameter("statusId");
            request.setAttribute("tripType", tripType);
            request.setAttribute("statusId", statusId);

            path = "content/trip/wfuTripSheet.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveWFUTripSheet(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View WFC Trip Sheet ";
        int insertStatus = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
            }

            if (tripCommand.getVehicleactreportdate() != null && !"".equals(tripCommand.getVehicleactreportdate())) {
                tripTO.setVehicleactreportdate(tripCommand.getVehicleactreportdate());
            }
            if (tripCommand.getVehicleactreporthour() != null && !"".equals(tripCommand.getVehicleactreporthour())) {
                tripTO.setVehicleactreporthour(tripCommand.getVehicleactreporthour());
            }
            if (tripCommand.getVehicleactreportmin() != null && !"".equals(tripCommand.getVehicleactreportmin())) {
                tripTO.setVehicleactreportmin(tripCommand.getVehicleactreportmin());
            }
            if (tripCommand.getWfuRemarks() != null && !"".equals(tripCommand.getWfuRemarks())) {
                tripTO.setWfuRemarks(tripCommand.getWfuRemarks());
            }

            //load details
            String[] loadOrderId = request.getParameterValues("loadOrderId");
            String[] pendingPkgs = request.getParameterValues("pendingPkgs");
            String[] pendingWeight = request.getParameterValues("pendingWeight");
            String[] loadedPkgs = request.getParameterValues("loadedPkgs");
            String[] loadedWeight = request.getParameterValues("loadedWeight");
            String[] pendingGrossWeight = request.getParameterValues("pendingGrossWeight");
            String[] loadedGrossWeight = request.getParameterValues("loadedGrossWeight");

            tripTO.setLoadOrderId(loadOrderId);
            tripTO.setPendingPkgs(pendingPkgs);
            tripTO.setPendingWeight(pendingWeight);
            tripTO.setLoadedPkgs(loadedPkgs);
            tripTO.setLoadedWeight(loadedWeight);
            tripTO.setPendingGrossWeight(pendingGrossWeight);
            tripTO.setLoadedGrossWeight(loadedGrossWeight);

            tripTO.setStatusId("18"); // trip WFC
            insertStatus = tripBP.updateStatus(tripTO, userId);

            insertStatus = tripBP.saveWFUTripSheet(tripTO, userId);
            String tripType = request.getParameter("tripType");
            String statusId = request.getParameter("statusId");
            request.setAttribute("tripType", tripType);
            request.setAttribute("statusId", statusId);

            if (insertStatus != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Trip WFU Details Updated Successfully....");
            }
            //path = "content/trip/wfuTripSheet.jsp";
            path = "content/redirectPage.jsp";
            mv = handleViewTripSheet(request, response, command);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
//        return new ModelAndView(path);
        return mv;
    }

    public ModelAndView emptyTripMerging(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  Empty Trip Merging ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Empty Trip Merging";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            if (tripCommand.getCustomerId() != null && !"".equals(tripCommand.getCustomerId())) {
                tripTO.setCustomerId(tripCommand.getCustomerId());
                request.setAttribute("customerId", tripCommand.getCustomerId());
            }
            if (request.getParameter("tripCode") != null && !"".equals(request.getParameter("tripCode"))) {
                tripTO.setTripCode(request.getParameter("tripCode"));
                request.setAttribute("tripCode", request.getParameter("tripCode"));
            }
            if (tripCommand.getFromDate() != null && !"".equals(tripCommand.getFromDate())) {
                tripTO.setFromDate(tripCommand.getFromDate());
                request.setAttribute("fromDate", tripCommand.getFromDate());
            }
            if (tripCommand.getToDate() != null && !"".equals(tripCommand.getToDate())) {
                tripTO.setToDate(tripCommand.getToDate());
                request.setAttribute("toDate", tripCommand.getToDate());
            }
            if (tripCommand.getVehicleId() != null && !"".equals(tripCommand.getVehicleId())) {
                tripTO.setVehicleId(tripCommand.getVehicleId());
                request.setAttribute("vehicleId", tripCommand.getVehicleId());
                System.out.println("vehicle id for merging:" + tripCommand.getVehicleId());
            }
            String tripIdValue = request.getParameter("tripIdValue");
            tripTO.setTripId(tripIdValue);
            ArrayList vehicleList = new ArrayList();
            vehicleList = tripBP.getVehicleList();
            request.setAttribute("vehicleList", vehicleList);
            ArrayList emptyTripList = new ArrayList();
            emptyTripList = tripBP.getEmptyTripMergingList(tripTO);
            if (emptyTripList.size() > 0) {
                request.setAttribute("emptyTripList", emptyTripList);
            }

            path = "content/trip/emptyTripMerging.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView emptyTripMergingDetails(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  Empty Trip Merging Details";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Empty Trip Merging";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            if (tripCommand.getCustomerId() != null && !"".equals(tripCommand.getCustomerId())) {
                tripTO.setCustomerId(tripCommand.getCustomerId());
                request.setAttribute("customerId", tripCommand.getCustomerId());
            }
            if (request.getParameter("tripCode") != null && !"".equals(request.getParameter("tripCode"))) {
                tripTO.setTripCode(request.getParameter("tripCode"));
                request.setAttribute("tripCode", request.getParameter("tripCode"));
            }
            if (tripCommand.getFromDate() != null && !"".equals(tripCommand.getFromDate())) {
                tripTO.setFromDate(tripCommand.getFromDate());
                request.setAttribute("fromDate", tripCommand.getFromDate());
            }
            if (tripCommand.getToDate() != null && !"".equals(tripCommand.getToDate())) {
                tripTO.setToDate(tripCommand.getToDate());
                request.setAttribute("toDate", tripCommand.getToDate());
            }
            if (tripCommand.getVehicleId() != null && !"".equals(tripCommand.getVehicleId())) {
                tripTO.setVehicleId(tripCommand.getVehicleId());
                request.setAttribute("vehicleId", tripCommand.getVehicleId());
            }
            String tripIdValue = request.getParameter("tripIdValue");
            String[] temp = null;
            temp = tripIdValue.split(",");
            String tripId = "";
            for (int i = 0; i < temp.length; i++) {
                if (!temp[i].equals("")) {
                    tripId = tripId + "," + temp[i];
                }
            }
            tripTO.setTripId(tripId);
            ArrayList vehicleList = new ArrayList();
            vehicleList = tripBP.getVehicleList();
            request.setAttribute("vehicleList", vehicleList);
            ArrayList emptyTripList = new ArrayList();
            emptyTripList = tripBP.getEmptyTripMergingList(tripTO);
            request.setAttribute("emptyTripList", emptyTripList);

            path = "content/trip/emptyTripMergingDetails.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveTripMergingDetails(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        tripCommand = command;
        String menuPath = "Operation  >>  Empty Trip Merging Details";
        int userId = (Integer) session.getAttribute("userId");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Empty Trip Merging";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            if (tripCommand.getCustomerId() != null && !"".equals(tripCommand.getCustomerId())) {
                tripTO.setCustomerId(tripCommand.getCustomerId());
                request.setAttribute("customerId", tripCommand.getCustomerId());
            }
            if (request.getParameter("tripCode") != null && !"".equals(request.getParameter("tripCode"))) {
                tripTO.setTripCode(request.getParameter("tripCode"));
                request.setAttribute("tripCode", request.getParameter("tripCode"));
            }
            if (tripCommand.getFromDate() != null && !"".equals(tripCommand.getFromDate())) {
                tripTO.setFromDate(tripCommand.getFromDate());
                request.setAttribute("fromDate", tripCommand.getFromDate());
            }
            if (tripCommand.getToDate() != null && !"".equals(tripCommand.getToDate())) {
                tripTO.setToDate(tripCommand.getToDate());
                request.setAttribute("toDate", tripCommand.getToDate());
            }
            if (tripCommand.getVehicleId() != null && !"".equals(tripCommand.getVehicleId())) {
                tripTO.setVehicleId(tripCommand.getVehicleId());
                request.setAttribute("vehicleId", tripCommand.getVehicleId());
            }
            String[] tripId = request.getParameterValues("tripId");
            String[] tripSequence = request.getParameterValues("tripSequence");
            String tripMergingRemarks = request.getParameter("tripMergingRemarks");

            int insertEmptyTripMerging = 0;
            insertEmptyTripMerging = tripBP.insertEmptyTripMerging(tripId, tripSequence, tripMergingRemarks, userId);
            String tripIdValue = request.getParameter("tripIdValue");
            tripTO.setTripId(tripIdValue);
            ArrayList vehicleList = new ArrayList();
            vehicleList = tripBP.getVehicleList();
            request.setAttribute("vehicleList", vehicleList);
            ArrayList emptyTripList = new ArrayList();
            tripTO = new TripTO();
            tripTO.setTripId(null);
            emptyTripList = tripBP.getEmptyTripMergingList(tripTO);
            if (emptyTripList.size() > 0) {
                request.setAttribute("emptyTripList", emptyTripList);
            }

            path = "content/trip/emptyTripMerging.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveEmptyTripApproval(HttpServletRequest request, HttpServletResponse response, TripCommand command) {

        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Operation  >>  Empty Trip Planning ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Approve Empty Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            String vehicleTypeId = "";
            //calculate revenue if possible
            //check if vehicle type id is given in consignment order for revenue calculation
            TripTO trpTO = null;
            float totalRevenue = 0.00F;
            String revenue = "0";
            float totalExpense = 0.00F;
            String expense = "0";
            int advanceappoval = 0;
            boolean checkflag = false;
            int saveEmptyTripApproval = 0;
            int userId = 0;
            if (session.getAttribute("userId") != null && !"".equals(session.getAttribute("userId"))) {
                userId = (Integer) session.getAttribute("userId");
            }
            System.out.println("userId = " + userId);
            String tripId = "";
            String approvalStatus = "";
            String approverId = "";
            String cnote = "";
            String routename = "";
            String vehicleno = "";
            String mailId = "";
            String customerName = "";
            String driverName = "";
            String vehicleType = "";
            String tripCode = "";
            String orderExpense = "";
            String totalKM = "";
            String tripScheduleDate = "";
            String tripScheduleTime = "";
            System.out.println("userId = " + userId);

            tripId = request.getParameter("tripid");
            approvalStatus = request.getParameter("approvestatus");
            cnote = request.getParameter("cnote");
            routename = request.getParameter("routename");
            vehicleno = request.getParameter("vehicleno");
            customerName = request.getParameter("customerName");
            driverName = request.getParameter("driverName");
            vehicleType = request.getParameter("vehicleType");
            tripCode = request.getParameter("tripCode");
            orderExpense = request.getParameter("orderExpense");
            totalKM = request.getParameter("totalKM");
            tripScheduleDate = request.getParameter("tripScheduleDate");
            tripScheduleTime = request.getParameter("tripScheduleTime");
            String tripType = request.getParameter("tripType");
            String vehicleId = request.getParameter("vehicleId");
            if (request.getParameter("userId") != null) {
                approverId = request.getParameter("userId");
                mailId = request.getParameter("mailId");
            } else if (userId != 0) {
                approverId = String.valueOf(userId);
            }
            System.out.println("tripType = " + tripType);
            int checkeEmptyTripApproval = 0;
            checkeEmptyTripApproval = tripBP.checkeEmptyTripApproval(tripId, approvalStatus, approverId);
            if (checkeEmptyTripApproval == 0) {
                checkflag = true;
                saveEmptyTripApproval = tripBP.saveEmptyTripApproval(tripId, approvalStatus, approverId, mailId);
            }
            String to = "";
            //String to = "nipun.kohli@brattlefoods.com,srini@entitlesolutions.com, vijay.bisht@brattlefoods.com";
            String smtp = "";
            int emailPort = 0;
            String frommailid = "";
            String password = "";
            String activitycode = "EMAPR1";

            ArrayList emaildetails = new ArrayList();
            emaildetails = tripBP.getEmailDetails(activitycode);
            Iterator itr = emaildetails.iterator();
            if (itr.hasNext()) {
                tripTO = new TripTO();
                tripTO = (TripTO) itr.next();
                smtp = tripTO.getSmtp();
                emailPort = Integer.parseInt(tripTO.getPort());
                frommailid = tripTO.getEmailId();
                password = tripTO.getPassword();
                to = tripTO.getTomailId();
            }

            ArrayList emailList = tripBP.getmailcontactDetails(tripId);
            TripTO tripTONew = null;
            String cc = "vijay.bisht@brattlefoods.com";
            itr = emailList.iterator();
            String fcHeadEmailId = "";
            if (itr.hasNext()) {
                tripTONew = new TripTO();
                tripTONew = (TripTO) itr.next();
                fcHeadEmailId = tripTONew.getFcHeadEmail();
            }
//            if (tripType.equals("1") && !tripType.equals(null)) {
//                to = fcHeadEmailId;
//            } else {
//                to = operationBP.getSecondaryFcPerson(vehicleId);
//                cc = "test@entitle.com";
//            }

            // to = "arun@entitlesolutions.com";
            String status = "";
            String emailFormat = "";
            if (request.getParameter("userId") != null) {
                if ("2".equals(approvalStatus)) {
                    status = "Approved";
                    emailFormat = "<html>"
                            + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                            + "<tr>"
                            + "<th colspan='2'>Empty Trip Details</th>"
                            + "</tr>"
                            + "<tr><td>&nbsp;Vehicle No</td><td>&nbsp;&nbsp;" + vehicleno + "</td></tr>"
                            + "<tr><td>&nbsp;Vehicle Type</td><td>&nbsp;&nbsp;" + vehicleType + "</td></tr>"
                            + "<tr><td>&nbsp;Customer Name</td><td>&nbsp;&nbsp;" + customerName + "</td></tr>"
                            + "<tr><td>&nbsp;Route Name</td><td>&nbsp;&nbsp;" + routename + "</td></tr>"
                            + "<tr><td>&nbsp;Driver Name</td><td>&nbsp;&nbsp;" + driverName + "</td></tr>"
                            + "<tr><td>&nbsp;Trip Code</td><td>&nbsp;&nbsp;" + tripCode + "</td></tr>"
                            + "<tr><td>&nbsp;RCM Expense</td><td>&nbsp;&nbsp;" + orderExpense + "</td></tr>"
                            + "<tr><td>&nbsp;Schedule Date</td><td>&nbsp;&nbsp;" + tripScheduleDate + "</td></tr>"
                            + "<tr><td>&nbsp;Schedule Time</td><td>&nbsp;&nbsp;" + tripScheduleTime + "</td></tr>"
                            + "<tr><td>&nbsp;Total KM</td><td>&nbsp;&nbsp;" + totalKM + "</td></tr>"
                            + "<tr><td>&nbsp;Status</td><td>&nbsp;&nbsp;" + status + "</td></tr>"
                            + "<tr><td>&nbsp;Approved By</td><td>&nbsp;&nbsp;" + mailId + "</td></tr>"
                            + "<tr><td>&nbsp;&nbsp;Remarks</td><td>&nbsp;&nbsp;" + "Empty Trip" + "</td></tr>"
                            + "<tr><td colspan='2' align='center'>"
                            + "</td></tr>"
                            + "</table></body></html>";
                } else {
                    status = "Rejected";
                    emailFormat = "<html>"
                            + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                            + "<tr>"
                            + "<th colspan='2'>Empty Trip Details</th>"
                            + "</tr>"
                            + "<tr><td>&nbsp;Vehicle No</td><td>&nbsp;&nbsp;" + vehicleno + "</td></tr>"
                            + "<tr><td>&nbsp;Vehicle Type</td><td>&nbsp;&nbsp;" + vehicleType + "</td></tr>"
                            + "<tr><td>&nbsp;Customer Name</td><td>&nbsp;&nbsp;" + customerName + "</td></tr>"
                            + "<tr><td>&nbsp;Route Name</td><td>&nbsp;&nbsp;" + routename + "</td></tr>"
                            + "<tr><td>&nbsp;Driver Name</td><td>&nbsp;&nbsp;" + driverName + "</td></tr>"
                            + "<tr><td>&nbsp;Trip Code</td><td>&nbsp;&nbsp;" + tripCode + "</td></tr>"
                            + "<tr><td>&nbsp;RCM Expense</td><td>&nbsp;&nbsp;" + orderExpense + "</td></tr>"
                            + "<tr><td>&nbsp;Schedule Date</td><td>&nbsp;&nbsp;" + tripScheduleDate + "</td></tr>"
                            + "<tr><td>&nbsp;Schedule Time</td><td>&nbsp;&nbsp;" + tripScheduleTime + "</td></tr>"
                            + "<tr><td>&nbsp;Total KM</td><td>&nbsp;&nbsp;" + totalKM + "</td></tr>"
                            + "<tr><td>&nbsp;Status</td><td>&nbsp;&nbsp;" + status + "</td></tr>"
                            + "<tr><td>&nbsp;Approved By</td><td>&nbsp;&nbsp;" + mailId + "</td></tr>"
                            + "<tr><td>&nbsp;&nbsp;Remarks</td><td>&nbsp;&nbsp;" + "Empty Trip" + "</td></tr>"
                            + "<tr><td colspan='2' align='center'>"
                            + "</td></tr>"
                            + "</table></body></html>";
                }
            } else if (userId != 0) {
                if ("2".equals(approvalStatus)) {
                    status = "Approved";
                    emailFormat = "<html>"
                            + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                            + "<tr>"
                            + "<th colspan='2'>Empty Trip Details</th>"
                            + "</tr>"
                            + "<tr><td>&nbsp;Vehicle No</td><td>&nbsp;&nbsp;" + vehicleno + "</td></tr>"
                            + "<tr><td>&nbsp;Vehicle Type</td><td>&nbsp;&nbsp;" + vehicleType + "</td></tr>"
                            + "<tr><td>&nbsp;Customer Name</td><td>&nbsp;&nbsp;" + customerName + "</td></tr>"
                            + "<tr><td>&nbsp;Route Name</td><td>&nbsp;&nbsp;" + routename + "</td></tr>"
                            + "<tr><td>&nbsp;Driver Name</td><td>&nbsp;&nbsp;" + driverName + "</td></tr>"
                            + "<tr><td>&nbsp;Trip Code</td><td>&nbsp;&nbsp;" + tripCode + "</td></tr>"
                            + "<tr><td>&nbsp;RCM Expense</td><td>&nbsp;&nbsp;" + orderExpense + "</td></tr>"
                            + "<tr><td>&nbsp;Schedule Date</td><td>&nbsp;&nbsp;" + tripScheduleDate + "</td></tr>"
                            + "<tr><td>&nbsp;Schedule Time</td><td>&nbsp;&nbsp;" + tripScheduleTime + "</td></tr>"
                            + "<tr><td>&nbsp;Total KM</td><td>&nbsp;&nbsp;" + totalKM + "</td></tr>"
                            + "<tr><td>&nbsp;Status</td><td>&nbsp;&nbsp;" + status + "</td></tr>"
                            + "<tr><td>&nbsp;Approved By</td><td>&nbsp;&nbsp;" + mailId + "</td></tr>"
                            + "<tr><td>&nbsp;&nbsp;Remarks</td><td>&nbsp;&nbsp;" + "Empty Trip" + "</td></tr>"
                            + "<tr><td colspan='2' align='center'>"
                            + "</td></tr>"
                            + "</table></body></html>";
                } else {
                    status = "Rejected";
                    emailFormat = "<html>"
                            + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                            + "<tr>"
                            + "<th colspan='2'>Empty Trip Details</th>"
                            + "</tr>"
                            + "<tr><td>&nbsp;Vehicle No</td><td>&nbsp;&nbsp;" + vehicleno + "</td></tr>"
                            + "<tr><td>&nbsp;Vehicle Type</td><td>&nbsp;&nbsp;" + vehicleType + "</td></tr>"
                            + "<tr><td>&nbsp;Customer Name</td><td>&nbsp;&nbsp;" + customerName + "</td></tr>"
                            + "<tr><td>&nbsp;Route Name</td><td>&nbsp;&nbsp;" + routename + "</td></tr>"
                            + "<tr><td>&nbsp;Driver Name</td><td>&nbsp;&nbsp;" + driverName + "</td></tr>"
                            + "<tr><td>&nbsp;Trip Code</td><td>&nbsp;&nbsp;" + tripCode + "</td></tr>"
                            + "<tr><td>&nbsp;RCM Expense</td><td>&nbsp;&nbsp;" + orderExpense + "</td></tr>"
                            + "<tr><td>&nbsp;Schedule Date</td><td>&nbsp;&nbsp;" + tripScheduleDate + "</td></tr>"
                            + "<tr><td>&nbsp;Schedule Time</td><td>&nbsp;&nbsp;" + tripScheduleTime + "</td></tr>"
                            + "<tr><td>&nbsp;Total KM</td><td>&nbsp;&nbsp;" + totalKM + "</td></tr>"
                            + "<tr><td>&nbsp;Status</td><td>&nbsp;&nbsp;" + status + "</td></tr>"
                            + "<tr><td>&nbsp;Approved By</td><td>&nbsp;&nbsp;" + mailId + "</td></tr>"
                            + "<tr><td>&nbsp;&nbsp;Remarks</td><td>&nbsp;&nbsp;" + "Empty Trip" + "</td></tr>"
                            + "<tr><td colspan='2' align='center'>"
                            + "</td></tr>"
                            + "</table></body></html>";
                }
            }

            String subject = "Confirmation of Empty Trip " + status + " of : " + vehicleno;
            String content = emailFormat;
            int mailSendingId = 0;
            tripTO.setMailTypeId("2");
            tripTO.setMailSubjectTo(subject);
            tripTO.setMailSubjectCc(subject);
            tripTO.setMailSubjectBcc("");
            tripTO.setMailContentTo(content);
            tripTO.setMailContentCc(content);
            tripTO.setMailContentBcc("");
            tripTO.setMailIdTo(to);
            tripTO.setMailIdCc(cc);
            tripTO.setMailIdBcc("");
//            mailSendingId = tripBP.insertMailDetails(tripTO, userId);
            new SendMail(smtp, emailPort, frommailid, password, subject, content, to, cc, userId).start();
            System.out.println("checkflag = " + checkflag);
            if (request.getParameter("userId") != null) {
                if (checkflag) {
                    path = "BrattleFoods/response.html";
                } else {
                    path = "BrattleFoods/responsed.html";
                }
            } else if (userId != 0) {
                ArrayList emptyTripDetails = new ArrayList();
                emptyTripDetails = tripBP.getEmptyTripDetails(tripTO);
                request.setAttribute("emptyTripDetails", emptyTripDetails);

                path = "content/trip/viewEmptyTripList.jsp";
            }

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView manualEmptyTripApproval(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  Empty Trip Merging Details";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Empty Trip Approval";
            request.setAttribute("pageTitle", pageTitle);
            TripTO tripTO = new TripTO();
            Date dNow = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(dNow);
            cal.add(Calendar.DATE, 0);
            dNow = cal.getTime();

            Date dNow1 = new Date();
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(dNow1);
            cal1.add(Calendar.DATE, -6);
            dNow1 = cal1.getTime();

            SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy");
            String today = ft.format(dNow);
            String fromday = ft.format(dNow1);
            System.out.println("the today date is " + fromday);

            if (tripCommand.getFromDate() != null && tripCommand.getFromDate() != "") {
                tripTO.setFromDate(tripCommand.getFromDate());
            } else {
                tripTO.setFromDate(fromday);
            }
            System.out.println("the today date is " + fromday);
            if (tripCommand.getToDate() != null && tripCommand.getToDate() != "") {
                tripTO.setToDate(tripCommand.getToDate());
            } else {
                tripTO.setToDate(today);
            }

            ArrayList emptyTripDetails = new ArrayList();
            emptyTripDetails = tripBP.getEmptyTripDetails(tripTO);
            System.out.println("emptyTripDetails.size()===" + emptyTripDetails.size());

            request.setAttribute("emptyTripDetails", emptyTripDetails);
            request.setAttribute("fromdate", tripTO.getFromDate());
            request.setAttribute("todate", tripTO.getToDate());

            path = "content/trip/viewEmptyTripList.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewEmptyTripApproval(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  Empty Trip Merging Details";
        TripTO tripTO = new TripTO();

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Empty Trip Approval";
            request.setAttribute("pageTitle", pageTitle);

            String tripId = request.getParameter("tripId");
            tripTO.setTripId(tripId);

            ArrayList emptyTripDetails = new ArrayList();
            emptyTripDetails = tripBP.getEmptyTripDetails(tripTO);

            request.setAttribute("emptyTripDetails", emptyTripDetails);

            path = "content/trip/viewEmptyTripApproval.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveManualEmptyTripApproval(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  Empty Trip Merging Details";
        TripTO tripTO = new TripTO();
        int status = 0;
        int userId = (Integer) session.getAttribute("userId");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Empty Trip Approval";
            request.setAttribute("pageTitle", pageTitle);

            String tripId = request.getParameter("tripId");
            String approvestatus = request.getParameter("approvestatus");
            tripTO.setTripId(tripId);
            tripTO.setApprovalStatus(approvestatus);
            tripTO.setUserId(userId);

            status = tripBP.saveManualEmptyTripApproval(tripTO);

            ArrayList emptyTripDetails = new ArrayList();
            emptyTripDetails = tripBP.getEmptyTripDetails(tripTO);
            request.setAttribute("emptyTripDetails", emptyTripDetails);

            path = "content/trip/viewEmptyTripList.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveTemperatureLogXlsUpload(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String menuPath = "Finance  >>  Upload Data Logger";
        int userId = (Integer) session.getAttribute("userId");
        int insertStatus = 0;
        ModelAndView mv = null;
        String newFileName = "", actualFilePath = "";
        String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", fileSaved = "";
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        int i = 0;
        String tripId = "";
        String uploadedFileName = "";
        String tempFilePath = "";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {

                    if (partObj.isParam()) {
                        if (partObj.getName().equals("tripId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            tripId = paramPart.getStringValue();
                        }
                    }
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/DataLogerXls/");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();

                        if (!"".equals(uploadedFileName) && uploadedFileName != null) {

                            String[] splitFileName = uploadedFileName.split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            fileSaved = splitFileName[0] + tripId + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath = tempServerFilePath + "\\" + fileSaved;
                            actualFilePath = actualServerFilePath + "\\" + tempFilePath;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(tempFilePath));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
                        }

                        System.out.println("fileName..." + fileName);
                    } else if (partObj.isParam()) {
                        if (partObj.getName().equals("tripId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            tripId = paramPart.getStringValue();
                        }

                    }
                }
            }

            int status = 0;
            tripTO.setTripId(tripId);
            status = tripBP.saveTempLogDetails(tempFilePath, fileSaved, tripTO, userId);

            System.out.println("tempFilePath new file path= " + tempFilePath);
            int graphInsertStatus = 0;
            graphInsertStatus = tripBP.insertBillingGraph(tripId, tempFilePath, userId);
            if (status != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Temp Log file Updated Successfully....");
            }
            path = "content/trip/temperatureLogUpload.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveTemperatureLogRequest(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View start Trip Sheet ";
        int insertStatus = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            String tripCode = request.getParameter("tripCode");
            String tripId = request.getParameter("tripId");
            String invoiceId = request.getParameter("invoiceId");
            String approvalStatus = request.getParameter("approvalStatus");
            tripTO.setTripId(tripId);
            tripTO.setApprovalStatus(approvalStatus);
            request.setAttribute("tripId", tripId);
            request.setAttribute("invoiceId", invoiceId);

            insertStatus = tripBP.saveTemperatureLogApproval(tripTO, userId);

            String emailTo = "";
            String emailCc = "";
//                    String cc = "nithyap@entitlesolutions.com";
            String smtp = "";
            int emailPort = 0;
            String frommailid = "";
            String password = "";
            String activitycode = "EMTRP0";

            ArrayList emaildetails = new ArrayList();
            emaildetails = tripBP.getEmailDetails(activitycode);
            Iterator itr1 = emaildetails.iterator();

            if (itr1.hasNext()) {
                tripTO = new TripTO();
                tripTO = (TripTO) itr1.next();
                smtp = tripTO.getSmtp();
                emailPort = Integer.parseInt(tripTO.getPort());
                frommailid = tripTO.getEmailId();
                password = tripTO.getPassword();
                emailTo = "abhishek.mishra@brattlefoods.com";
                emailCc = "subhrakanta.mishra@brattlefoods.com,transport@brattlefoods.com";

            }

            String emailFormat = "<html>"
                    + "<body>"
                    + "<p><b>Hi, <br><br>Temperature Log Details for the Trip   "
                    + "<br>Trip Code:" + tripCode
                    + "<br><a style='text-decoration: none' href='http://203.124.105.244:8089/KerryDemo/saveTemperatureLogApproval.do?userId=1081&tripid=" + tripId + "&approvestatus=2'>Approve</a>&nbsp;|&nbsp;"
                    + "<a style='text-decoration: none' href='http://203.124.105.244:8089/KerryDemo/saveTemperatureLogApproval.do?userId=1081&tripid=" + tripId + "&approvestatus=3'>Reject</a>"
                    + "<br><br>Team BrattleFoods"
                    + "</body></html>";

            String subject = "Temperature  Log Excel Approval For the Trip   " + tripCode;

            String content = emailFormat;
            int mailSendingId = 0;
            tripTO.setMailTypeId("2");
            tripTO.setMailSubjectTo(subject);
            tripTO.setMailSubjectCc(subject);
            tripTO.setMailSubjectBcc("");
            tripTO.setMailContentTo(content);
            tripTO.setMailContentCc(content);
            tripTO.setMailContentBcc("");
            tripTO.setMailIdTo(emailTo);
            tripTO.setMailIdCc(emailCc);
            tripTO.setMailIdBcc("");
//            mailSendingId = tripBP.insertMailDetails(tripTO, userId);
            new SendMail(smtp, emailPort, frommailid, password, subject, content, emailTo, emailCc, userId).start();

            /////////////////////////////////////////////Email part///////////////////////
            path = "content/trip/billingInvoice.jsp";
            mv = showinvoicedetail(request, response, command);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView saveTemperatureLogApproval(HttpServletRequest request, HttpServletResponse response, TripCommand command) {

        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Operation  >>  Empty Trip Planning ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Approve Empty Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            boolean checkflag = false;
            int insertStatus = 0;
            int userId = 0;
            if (session.getAttribute("userId") != null && !"".equals(session.getAttribute("userId"))) {
                userId = (Integer) session.getAttribute("userId");
            }
            System.out.println("userId = " + userId);
            String tripId = "";
            String approvalStatus = "";
            String approverId = "";
            System.out.println("userId = " + userId);

            tripId = request.getParameter("tripid");
            approvalStatus = request.getParameter("approvestatus");
            approverId = request.getParameter("userId");
            tripTO.setApprovalStatus(approvalStatus);
            tripTO.setTripId(tripId);

            int checkeTemperatureLogApproval = 0;
            checkeTemperatureLogApproval = tripBP.checkTemperatureLogApproval(tripTO, userId);
            if (checkeTemperatureLogApproval == 0) {
                checkflag = true;
                insertStatus = tripBP.saveTemperatureLogApproval(tripTO, userId);
            }
            String to = "";
            //String to = "nipun.kohli@brattlefoods.com,srini@entitlesolutions.com, vijay.bisht@brattlefoods.com";
            String smtp = "";
            int emailPort = 0;
            String frommailid = "";
            String password = "";
            String activitycode = "EMAPR1";

            ArrayList emaildetails = new ArrayList();
            emaildetails = tripBP.getEmailDetails(activitycode);
            Iterator itr = emaildetails.iterator();
            if (itr.hasNext()) {
                tripTO = new TripTO();
                tripTO = (TripTO) itr.next();
                smtp = tripTO.getSmtp();
                emailPort = Integer.parseInt(tripTO.getPort());
                frommailid = tripTO.getEmailId();
                password = tripTO.getPassword();
//                to = tripTO.getTomailId();
                to = "somashekharan.nair@brattlefoods.com";

            }

            ArrayList emailList = tripBP.getmailcontactDetails(tripId);
            TripTO tripTONew = null;
            String cc = "";
            itr = emailList.iterator();
            String fcHeadEmailId = "";
            if (itr.hasNext()) {
                tripTONew = new TripTO();
                tripTONew = (TripTO) itr.next();
//                fcHeadEmailId = tripTONew.getFcHeadEmail();
                fcHeadEmailId = "somashekharan.nair@brattlefoods.com";
            }
            to = fcHeadEmailId;
            // to = "arun@entitlesolutions.com";
            String status = "";
            String emailFormat = "";
            if ("2".equals(approvalStatus)) {
                emailFormat = "<html>"
                        + "<body><center><p><b>Confirmation of Temperature Log Approval</b></p></center>"
                        + "<p>Hi, <br><br>This is an confirmation mail for Temperature Log Approval. <br> tripId : " + tripId + ".</p>"
                        + "</body></html>";
                status = "Approved";

            } else {
                emailFormat = "<html>"
                        + "<body><center><p><b>Confirmation of Temperature Log Rejected</b></p></center>"
                        + "<p>Hi, <br><br>This is an confirmation mail for Temperature Log Rejected. <br> tripId : " + tripId + ".</p>"
                        + "</body></html>";
                status = "Rejected";
            }

            String subject = "Confirmation of Temperature Log  " + status + " of : " + tripId;
            String content = emailFormat;
            int mailSendingId = 0;
            tripTO.setMailTypeId("2");
            tripTO.setMailSubjectTo(subject);
            tripTO.setMailSubjectCc(subject);
            tripTO.setMailSubjectBcc("");
            tripTO.setMailContentTo(content);
            tripTO.setMailContentCc(content);
            tripTO.setMailContentBcc("");
            tripTO.setMailIdTo(to);
            tripTO.setMailIdCc(cc);
            tripTO.setMailIdBcc("");
//            mailSendingId = tripBP.insertMailDetails(tripTO, userId);
            new SendMail(smtp, emailPort, frommailid, password, subject, content, to, cc, userId).start();
            System.out.println("checkflag = " + checkflag);
            if (checkflag) {
                path = "BrattleFoods/response.html";
            } else {
                path = "BrattleFoods/responsed.html";
            }

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewTripPointDetails(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        tripCommand = command;
        String menuPath = "Operation  >>  Loading and Unloading Details";
        int userId = (Integer) session.getAttribute("userId");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Empty Trip Merging";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            String tripRouteCourseId = request.getParameter("tripRouteCourseId");
            tripTO.setRouteId(tripRouteCourseId);

            String tripSheetId = request.getParameter("tripId");
            String pointType = request.getParameter("pointType");
            String pointName = request.getParameter("pointName");
            request.setAttribute("tripRouteCourseId", tripRouteCourseId);
            request.setAttribute("pointType", pointType);
            request.setAttribute("tripSheetId", tripSheetId);
            request.setAttribute("pointName", pointName);

            if (pointType.equals("Start Point") || pointType.equals("Pick Up")) {
                path = "content/trip/loadingDetails.jsp";
            } else {
                path = "content/trip/unloadingDetails.jsp";

            }
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveLoadingUnloadingDetails(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        tripCommand = command;
        String menuPath = "Operation  >>  Loading and Unloading Details";
        int userId = (Integer) session.getAttribute("userId");
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Empty Trip Merging";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            if (tripCommand.getVehicleactreportdate() != null && !"".equals(tripCommand.getVehicleactreportdate())) {
                tripTO.setVehicleactreportdate(tripCommand.getVehicleactreportdate());
            }
            if (tripCommand.getVehicleactreporthour() != null && !"".equals(tripCommand.getVehicleactreporthour())) {
                tripTO.setVehicleactreporthour(tripCommand.getVehicleactreporthour());
            }
            if (tripCommand.getVehicleactreportmin() != null && !"".equals(tripCommand.getVehicleactreportmin())) {
                tripTO.setVehicleactreportmin(tripCommand.getVehicleactreportmin());
            }

            if (tripCommand.getVehicleloadreportdate() != null && !"".equals(tripCommand.getVehicleloadreportdate())) {
                tripTO.setVehicleloadreportdate(tripCommand.getVehicleloadreportdate());
            }
            if (tripCommand.getVehicleloadreporthour() != null && !"".equals(tripCommand.getVehicleloadreporthour())) {
                tripTO.setVehicleloadreporthour(tripCommand.getVehicleloadreporthour());
            }
            if (tripCommand.getVehicleloadreportmin() != null && !"".equals(tripCommand.getVehicleloadreportmin())) {
                tripTO.setVehicleloadreportmin(tripCommand.getVehicleloadreportmin());
            }
            if (tripCommand.getVehicleloadtemperature() != null && !"".equals(tripCommand.getVehicleloadtemperature())) {
                tripTO.setVehicleloadtemperature(tripCommand.getVehicleloadtemperature());
            }
            if (tripCommand.getTripSheetId() != null && !"".equals(tripCommand.getTripSheetId())) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
            }

            String routeCourseId = request.getParameter("routeCourseId");
            String pointType = request.getParameter("pointType");
            tripTO.setRouteId(routeCourseId);

            int insertStatus = 0;
            insertStatus = tripBP.insertLoadingUnloadingDetails(tripTO, userId);

            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
                request.setAttribute("tripSheetId", tripCommand.getTripSheetId());
            }
            tripTO.setTripId(tripTO.getTripSheetId());
            tripTO.setTripSheetId(tripTO.getTripSheetId());

            if (pointType.equals("Start Point") || pointType.equals("Pick Up")) {
                path = "content/trip/loadingDetails.jsp";
                mv = handleViewStartTripSheet(request, response, command);
            } else {
                path = "content/trip/unloadingDetails.jsp";
                mv = viewTripRouteDetails(request, response, command);
            }
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView viewTripRouteDetails(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        tripCommand = command;
        String menuPath = "Operation  >>  Loading and Unloading Details";
        int userId = (Integer) session.getAttribute("userId");
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Empty Trip Merging";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
                request.setAttribute("tripSheetId", tripCommand.getTripSheetId());
            }
            tripTO.setTripId(tripTO.getTripSheetId());
            tripTO.setTripSheetId(tripTO.getTripSheetId());
            ArrayList tripPointDetails = new ArrayList();
            tripPointDetails = tripBP.getTripPointDetails(tripTO);
            request.setAttribute("tripPointDetails", tripPointDetails);
            request.setAttribute("tripPointDetailsSize", tripPointDetails.size());
            String tripType = request.getParameter("tripType");
            String statusId = request.getParameter("statusId");
            request.setAttribute("tripType", tripType);
            request.setAttribute("statusId", statusId);
            path = "content/trip/tripPointDetails.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewTripDetailsMail(HttpServletRequest request, HttpServletResponse reponse, TripCommand command) {
        HttpSession session = request.getSession();

        String path = "";
        String menuPath = "";
        menuPath = "Operation  >>  View Trip Details For Email";
        String pageTitle = "Customer On-Hold";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = 0;
        TripTO tripTO = new TripTO();
        ModelAndView mv = null;
        boolean statusFlag = false;
        String emailFormat = "";
        try {

            String tripId = request.getParameter("tripId");
            ArrayList consignmentPaymentDetails = new ArrayList();
            tripTO.setTripId(tripId);
            tripTO.setPaymentType("");
            consignmentPaymentDetails = tripBP.getConsignmentPaymentDetails(tripTO);
            request.setAttribute("tripId", tripId);
            if (consignmentPaymentDetails.size() > 0) {
                request.setAttribute("consignmentPaymentDetails", consignmentPaymentDetails);
            }
            path = "content/trip/tripMail.jsp";

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to view role details --> " + exception);
            exception.printStackTrace();
        }
        return new ModelAndView(path);

    }

    public void getTOEmailList(HttpServletRequest request, HttpServletResponse response, TripCommand command) throws IOException {
        HttpSession session = request.getSession();
        tripCommand = command;
        TripTO tripTO = new TripTO();
        //        JsonTO jsonTO = new JsonTO();
        ArrayList emailList = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            response.setContentType("text/html");
            String mailTO = request.getParameter("queryParam");
            tripTO.setEmailId(mailTO);
            emailList = tripBP.getEmailList(tripTO);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = emailList.iterator();
            int i = 0;
            while (itr.hasNext()) {
                i++;
                JSONObject jsonObject = new JSONObject();
                tripTO = (TripTO) itr.next();
                jsonObject.put("name", tripTO.getEmailId());
                jsonObject.put("id", tripTO.getEmailId());
                //                jsonObject.put("id", i);
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getCCEmailList(HttpServletRequest request, HttpServletResponse response, TripCommand command) throws IOException {
        HttpSession session = request.getSession();
        tripCommand = command;
        TripTO tripTO = new TripTO();
        //        JsonTO jsonTO = new JsonTO();
        ArrayList emailList = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            response.setContentType("text/html");
            String mailCC = request.getParameter("mailCC");
            tripTO.setEmailId(mailCC);
            emailList = tripBP.getEmailList(tripTO);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = emailList.iterator();
            int i = 0;
            while (itr.hasNext()) {
                i++;
                JSONObject jsonObject = new JSONObject();
                tripTO = (TripTO) itr.next();
                jsonObject.put("name", tripTO.getEmailId());
                jsonObject.put("id", tripTO.getEmailId());
                //                jsonObject.put("id", i);
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView sendTripDetailsMail(HttpServletRequest request, HttpServletResponse reponse, TripCommand command) {
        HttpSession session = request.getSession();

        String path = "";
        String menuPath = "";
        menuPath = "Operation  >>  View Trip Details For Email";
        String pageTitle = "Customer On-Hold";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = 0;
        TripTO tripTO = new TripTO();
        ModelAndView mv = null;
        boolean statusFlag = false;
        String emailFormat = "";
        try {
            String to = "";
            String activitycode = "EMREQ1";
            //String to = "nipun.kohli@brattlefoods.com,srini@entitlesolutions.com";
            String smtp = "";
            int emailPort = 0;
            String frommailid = "";
            String password = "";
            String subject = "";
            String content = "";

            ArrayList emaildetails = new ArrayList();
            emaildetails = tripBP.getEmailDetails(activitycode);
            Iterator itr1 = emaildetails.iterator();
            TripTO tripTO1 = null;
            if (itr1.hasNext()) {
                tripTO1 = new TripTO();
                tripTO1 = (TripTO) itr1.next();
                smtp = tripTO1.getSmtp();
                emailPort = Integer.parseInt(tripTO1.getPort());
                frommailid = tripTO1.getEmailId();
                password = tripTO1.getPassword();
                to = tripTO1.getTomailId();
            }
            emailFormat = "<html>"
                    + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                    + "<tr>"
                    + "<th colspan='2' style='text-align: left'>Hi Team, <br><br> Trip Details For Consignment " + request.getParameter("consignmentOrderNos") + " </th>"
                    + "</tr>"
                    + "<tr><td>&nbsp;&nbsp;Customer Name</td><td>&nbsp;&nbsp;" + request.getParameter("customerName") + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Trip Code</td><td>&nbsp;&nbsp;" + request.getParameter("tripCode") + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Vehicle Type</td><td>&nbsp;&nbsp;" + request.getParameter("vehicleType") + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Vehicle No</td><td>&nbsp;&nbsp;" + request.getParameter("regNo") + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Driver Name</td><td>&nbsp;&nbsp;" + request.getParameter("driverName") + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Driver Mobile</td><td>&nbsp;&nbsp;" + request.getParameter("driverMobile") + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Current Location</td><td>&nbsp;&nbsp;" + request.getParameter("currentLocation") + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Vehicle Required Date</td><td>&nbsp;&nbsp;" + request.getParameter("vehicleRequiredDateTime") + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Route</td><td>&nbsp;&nbsp;" + request.getParameter("routeInfo") + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Product Category</td><td>&nbsp;&nbsp;" + request.getParameter("productInfo") + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Reefer</td><td>&nbsp;&nbsp;" + request.getParameter("reeferRequired") + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Total Packages (Nos)</td><td>&nbsp;&nbsp;" + request.getParameter("totalPackages") + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Total Distance (Km)</td><td>&nbsp;&nbsp;" + request.getParameter("totalKM") + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Transit Time (Hr):</td><td>&nbsp;&nbsp;" + request.getParameter("totalHrs") + " Min:" + request.getParameter("totalMins") + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Payment Type</td><td>&nbsp;&nbsp;" + request.getParameter("paymentType") + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Freight Amount</td><td>&nbsp;&nbsp;" + request.getParameter("freightAmount") + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Paid Amount</td><td>&nbsp;&nbsp;" + request.getParameter("paidAmount") + "</td></tr>"
                    + "</table></body></html>";

            subject = request.getParameter("subject");
            content = emailFormat;
            to = request.getParameter("mailTO");
            String cc = request.getParameter("mailCC");

            //mail.sendmail(smtp, emailPort, frommailid, password, subject, content, to);
            int mailSendingId = 0;
            tripTO.setMailTypeId("2");
            tripTO.setMailSubjectTo(subject);
            tripTO.setMailSubjectCc(subject);
            tripTO.setMailSubjectBcc("");
            tripTO.setMailContentTo(content);
            tripTO.setMailContentCc(content);
            tripTO.setMailContentBcc("");
            tripTO.setMailIdTo(to);
            tripTO.setMailIdCc(cc);
            tripTO.setMailIdBcc("");
//            mailSendingId = tripBP.insertMailDetails(tripTO, userId);
            new SendMail(smtp, emailPort, frommailid, password, subject, content, to, cc, userId).start();
            path = "content/trip/tripMail.jsp";

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to view role details --> " + exception);
            exception.printStackTrace();
        }
        return new ModelAndView(path);

    }

    public ModelAndView saveVehicleDriverAdvanceApproval(HttpServletRequest request, HttpServletResponse response, TripCommand command) {

        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Operation  >>  Vehicle Driver Advance Approval";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Approve Vehicle Driver Advance";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            boolean checkflag = false;
            int userId = 0;
            if (session.getAttribute("userId") != null && !"".equals(session.getAttribute("userId"))) {
                userId = (Integer) session.getAttribute("userId");
            }
            System.out.println("userId = " + userId);
            String vehicleDriverAdvanceId = "";
            String approvalStatus = "";
            String approverId = "";
            String primaryDriver = "";
            String vehicleNo = "";
            String advanceAmount = "";
            String expenseType = "";
            String advanceDate = "";
            String advanceRemarks = "";
            String vehicleId = "";
            String mailId = "";
            String userName = "";
            String usageTypeId = "";
            System.out.println("userId = " + userId);

            tripTO.setUserId(userId);
            int insertStatus = 0;

            if (request.getParameter("userId") != null) {
                mailId = request.getParameter("mailId");
                approverId = request.getParameter("userId");
                vehicleDriverAdvanceId = request.getParameter("vehicleDriverAdvanceId");
                vehicleId = request.getParameter("vehicleId");
                expenseType = request.getParameter("expenseType");
                advanceDate = request.getParameter("advanceDate");
                advanceRemarks = request.getParameter("advanceRemarks");
                vehicleNo = request.getParameter("vehicleNo");
                primaryDriver = request.getParameter("primaryDriver");
                advanceAmount = request.getParameter("advanceAmount");
                approvalStatus = request.getParameter("approvestatus");
                tripTO.setApprovalStatus(approvalStatus);
                tripTO.setVehicleDriverAdvanceId(vehicleDriverAdvanceId);
                tripTO.setMailId(mailId);
            } else if (userId != 0) {
                userName = (String) session.getAttribute("userName");
                approverId = String.valueOf(userId);
                vehicleDriverAdvanceId = request.getParameter("vehicleDriverAdvanceId");
                vehicleId = request.getParameter("vehicleId");
                expenseType = request.getParameter("expenseType");
                advanceDate = request.getParameter("advanceDate");
                advanceRemarks = request.getParameter("advanceRemarks");
                vehicleNo = request.getParameter("vehicleNo");
                primaryDriver = request.getParameter("primaryDriver");
                advanceAmount = request.getParameter("advanceAmount");
                approvalStatus = request.getParameter("approvestatus");
                tripTO.setApprovalStatus(approvalStatus);
                tripTO.setVehicleDriverAdvanceId(vehicleDriverAdvanceId);
                tripTO.setMailId("");
            }
            usageTypeId = tripBP.getVehicleUsageTypeId(vehicleId);
            int checkVehicleDriverAdvanceApproval = 0;
            checkVehicleDriverAdvanceApproval = tripBP.checkVehicleDriverAdvanceApproval(vehicleDriverAdvanceId, approvalStatus, approverId);
            if (checkVehicleDriverAdvanceApproval == 0) {
                checkflag = true;
                insertStatus = tripBP.saveVehicleDriverAdvanceApproval(tripTO);
            }
            String to = "";
            String cc = "";
            //String to = "nipun.kohli@brattlefoods.com,srini@entitlesolutions.com, vijay.bisht@brattlefoods.com";
            String smtp = "";
            int emailPort = 0;
            String frommailid = "";
            String password = "";

            String activitycode = "";
            String toMailId = "";

            if (usageTypeId.equals("2")) {
                //Primary Operation
                activitycode = "PRI-VEH-ADV";
            } else if (usageTypeId.equals("1")) {
                //Secondary Operation
                activitycode = "SEC-VEH-ADV";
            }

            ArrayList emaildetails = new ArrayList();
            emaildetails = tripBP.getEmailDetails(activitycode);
            Iterator itr = emaildetails.iterator();
            if (itr.hasNext()) {
                tripTO = new TripTO();
                tripTO = (TripTO) itr.next();
                smtp = tripTO.getSmtp();
                emailPort = Integer.parseInt(tripTO.getPort());
                frommailid = tripTO.getEmailId();
                password = tripTO.getPassword();
                cc = "";
                toMailId = tripTO.getTomailId();
            }
            String fcLeadMailId = "";
            if (usageTypeId.equals("2")) {
                fcLeadMailId = tripBP.getFCLeadMailId(vehicleId);
            } else if (usageTypeId.equals("1")) {
                fcLeadMailId = tripBP.getSecondaryFCLeadMailId(vehicleId);
            }
            String status = "";
            String emailFormat = "";
            if (request.getParameter("userId") != null) {
                if ("2".equals(approvalStatus)) {
                    emailFormat = "<html>"
                            + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                            + "<tr>"
                            + "<th colspan='2'>Confirmation of Vehicle Driver Advance Approved</th>"
                            + "</tr>"
                            + "<tr><td>&nbsp;Expense Type</td><td>&nbsp;&nbsp;" + expenseType + "</td></tr>"
                            + "<tr><td>&nbsp;Vehicle No</td><td>&nbsp;&nbsp;" + vehicleNo + "</td></tr>"
                            + "<tr><td>&nbsp;Driver Name</td><td>&nbsp;&nbsp;" + primaryDriver + "</td></tr>"
                            + "<tr><td>&nbsp;Advance Amount</td><td>&nbsp;&nbsp;" + advanceAmount + "</td></tr>"
                            + "<tr><td>&nbsp;Advance Date</td><td>&nbsp;&nbsp;" + advanceDate + "</td></tr>"
                            + "<tr><td>&nbsp;Approved By</td><td>&nbsp;&nbsp;" + mailId + "</td></tr>"
                            + "<tr><td>&nbsp;&nbsp;Remarks</td><td>&nbsp;&nbsp;" + advanceRemarks + "</td></tr>"
                            + "<tr><td colspan='2' align='center'>"
                            + "</td></tr>"
                            + "</table></body></html>";

                    String subject = "Vehicle Driver Advance Approval Request for Vehicle -" + vehicleNo + " Driver " + primaryDriver;
                    status = "Approved";

                } else {
                    emailFormat = "<html>"
                            + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                            + "<tr>"
                            + "<th colspan='2'>Confirmation of Vehicle Driver Advance Rejected</th>"
                            + "</tr>"
                            + "<tr><td>&nbsp;Expense Type</td><td>&nbsp;&nbsp;" + expenseType + "</td></tr>"
                            + "<tr><td>&nbsp;Vehicle No</td><td>&nbsp;&nbsp;" + vehicleNo + "</td></tr>"
                            + "<tr><td>&nbsp;Driver Name</td><td>&nbsp;&nbsp;" + primaryDriver + "</td></tr>"
                            + "<tr><td>&nbsp;Advance Amount</td><td>&nbsp;&nbsp;" + advanceAmount + "</td></tr>"
                            + "<tr><td>&nbsp;Advance Date</td><td>&nbsp;&nbsp;" + advanceDate + "</td></tr>"
                            + "<tr><td>&nbsp;Rejected By</td><td>&nbsp;&nbsp;" + mailId + "</td></tr>"
                            + "<tr><td>&nbsp;&nbsp;Remarks</td><td>&nbsp;&nbsp;" + advanceRemarks + "</td></tr>"
                            + "<tr><td colspan='2' align='center'>"
                            + "</td></tr>"
                            + "</table></body></html>";
                    status = "Rejected";
                }
            } else if (userId != 0) {
                if ("2".equals(approvalStatus)) {
                    emailFormat = "<html>"
                            + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                            + "<tr>"
                            + "<th colspan='2'>Confirmation of Vehicle Driver Advance Approved</th>"
                            + "</tr>"
                            + "<tr><td>&nbsp;Expense Type</td><td>&nbsp;&nbsp;" + expenseType + "</td></tr>"
                            + "<tr><td>&nbsp;Vehicle No</td><td>&nbsp;&nbsp;" + vehicleNo + "</td></tr>"
                            + "<tr><td>&nbsp;Driver Name</td><td>&nbsp;&nbsp;" + primaryDriver + "</td></tr>"
                            + "<tr><td>&nbsp;Advance Amount</td><td>&nbsp;&nbsp;" + advanceAmount + "</td></tr>"
                            + "<tr><td>&nbsp;Advance Date</td><td>&nbsp;&nbsp;" + advanceDate + "</td></tr>"
                            + "<tr><td>&nbsp;Approved By</td><td>&nbsp;&nbsp;" + userName + "</td></tr>"
                            + "<tr><td>&nbsp;&nbsp;Remarks</td><td>&nbsp;&nbsp;" + advanceRemarks + "</td></tr>"
                            + "<tr><td colspan='2' align='center'>"
                            + "</td></tr>"
                            + "</table></body></html>";

                    String subject = "Vehicle Driver Advance Approval Request for Vehicle -" + vehicleNo + " Driver " + primaryDriver;
                    status = "Approved";

                } else {
                    emailFormat = "<html>"
                            + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                            + "<tr>"
                            + "<th colspan='2'>Confirmation of Vehicle Driver Advance Rejected</th>"
                            + "</tr>"
                            + "<tr><td>&nbsp;Expense Type</td><td>&nbsp;&nbsp;" + expenseType + "</td></tr>"
                            + "<tr><td>&nbsp;Vehicle No</td><td>&nbsp;&nbsp;" + vehicleNo + "</td></tr>"
                            + "<tr><td>&nbsp;Driver Name</td><td>&nbsp;&nbsp;" + primaryDriver + "</td></tr>"
                            + "<tr><td>&nbsp;Advance Amount</td><td>&nbsp;&nbsp;" + advanceAmount + "</td></tr>"
                            + "<tr><td>&nbsp;Advance Date</td><td>&nbsp;&nbsp;" + advanceDate + "</td></tr>"
                            + "<tr><td>&nbsp;Rejected By</td><td>&nbsp;&nbsp;" + userName + "</td></tr>"
                            + "<tr><td>&nbsp;&nbsp;Remarks</td><td>&nbsp;&nbsp;" + advanceRemarks + "</td></tr>"
                            + "<tr><td colspan='2' align='center'>"
                            + "</td></tr>"
                            + "</table></body></html>";
                    status = "Rejected";
                }
            }

            String subject = "Confirmation of Vehicle Driver Advance " + status + " of : " + vehicleNo;
            String content = emailFormat;
            int mailSendingId = 0;
            tripTO.setMailTypeId("2");
            tripTO.setMailSubjectTo(subject);
            tripTO.setMailSubjectCc(subject);
            tripTO.setMailSubjectBcc("");
            tripTO.setMailContentTo(content);
            tripTO.setMailContentCc(content);
            tripTO.setMailContentBcc("");
            tripTO.setMailIdTo(fcLeadMailId + "," + toMailId);
            tripTO.setMailIdCc(cc);
            tripTO.setMailIdBcc("");
//            mailSendingId = tripBP.insertMailDetails(tripTO, userId);
            new SendMail(smtp, emailPort, frommailid, password, subject, content, fcLeadMailId + "," + toMailId, cc, userId).start();
            System.out.println("checkflag = " + checkflag);
            if (request.getParameter("userId") != null) {
                if (checkflag) {
                    path = "BrattleFoods/response.html";
                } else {
                    path = "BrattleFoods/responsed.html";
                }
            } else if (userId != 0) {
                OperationTO operationTO = new OperationTO();
                Date dNow = new Date();
                Calendar cal = Calendar.getInstance();
                cal.setTime(dNow);
                cal.add(Calendar.DATE, 0);
                dNow = cal.getTime();

                Date dNow1 = new Date();
                Calendar cal1 = Calendar.getInstance();
                cal1.setTime(dNow1);
                cal1.add(Calendar.DATE, -6);
                dNow1 = cal1.getTime();

                SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy");
                String today = ft.format(dNow);
                String fromday = ft.format(dNow1);

                operationTO.setFromDate(fromday);
                tripTO.setFromDate(fromday);

                operationTO.setToDate(today);
                tripTO.setToDate(today);

                ArrayList financeRequest = operationBP.getfinanceRequest(operationTO);
                ArrayList vehicleAdvanceRequest = new ArrayList();
                vehicleAdvanceRequest = tripBP.getVehicleAdvanceRequest(tripTO);

                request.setAttribute("vehicleAdvanceRequest", vehicleAdvanceRequest);
                request.setAttribute("financeRequest", financeRequest);
                request.setAttribute("fromdate", operationTO.getFromDate());
                request.setAttribute("todate", operationTO.getToDate());
                path = "BrattleFoods/financeAdviceRequest.jsp";
            }

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveManualVehicleDriverAdvanceApproval(HttpServletRequest request, HttpServletResponse response, TripCommand command) {

        HttpSession session = request.getSession();
        String path = "";
        ModelAndView mv = null;
        String menuPath = "Operation  >>  Vehicle Driver Advance Approval ";
        OperationTO operationTO = new OperationTO();
        TripTO tripTO = new TripTO();
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Approve Empty Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            int userId = (Integer) session.getAttribute("userId");
            System.out.println("userId = " + userId);
            String vehicleDriverAdvanceId = "";
            String approvalStatus = "";
            System.out.println("userId = " + userId);

            tripTO.setUserId(userId);
            int insertStatus = 0;

            vehicleDriverAdvanceId = request.getParameter("vehicleDriverAdvanceId");
            approvalStatus = request.getParameter("approvestatus");
            tripTO.setVehicleDriverAdvanceId(vehicleDriverAdvanceId);
            tripTO.setApprovalStatus(approvalStatus);
            tripTO.setUserId(userId);

            insertStatus = tripBP.saveVehicleDriverAdvanceApproval(tripTO);
            Date dNow = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(dNow);
            cal.add(Calendar.DATE, 0);
            dNow = cal.getTime();

            Date dNow1 = new Date();
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(dNow1);
            cal1.add(Calendar.DATE, -6);
            dNow1 = cal1.getTime();

            SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy");
            String today = ft.format(dNow);
            String fromday = ft.format(dNow1);

            operationTO.setFromDate(fromday);
            tripTO.setFromDate(fromday);

            operationTO.setToDate(today);
            tripTO.setToDate(today);

            ArrayList financeRequest = operationBP.getfinanceRequest(operationTO);
            ArrayList vehicleAdvanceRequest = new ArrayList();
            vehicleAdvanceRequest = tripBP.getVehicleAdvanceRequest(tripTO);

            request.setAttribute("vehicleAdvanceRequest", vehicleAdvanceRequest);
            request.setAttribute("financeRequest", financeRequest);
            request.setAttribute("fromdate", operationTO.getFromDate());
            request.setAttribute("todate", operationTO.getToDate());
            path = "BrattleFoods/financeAdviceRequest.jsp";

//            if ("2".equals(approvalStatus)) {
//                emailFormat = "<html>"
//                        + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
//                        + "<tr>"
//                        + "<th colspan='2'>Confirmation of Vehicle Driver Advance Approved</th>"
//                        + "</tr>"
//                        + "<tr><td>&nbsp;Expense Type</td><td>&nbsp;&nbsp;" + expenseType + "</td></tr>"
//                        + "<tr><td>&nbsp;Vehicle No</td><td>&nbsp;&nbsp;" + vehicleNo + "</td></tr>"
//                        + "<tr><td>&nbsp;Driver Name</td><td>&nbsp;&nbsp;" + primaryDriver + "</td></tr>"
//                        + "<tr><td>&nbsp;Advance Amount</td><td>&nbsp;&nbsp;" + advanceAmount + "</td></tr>"
//                        + "<tr><td>&nbsp;Advance Date</td><td>&nbsp;&nbsp;" + advanceDate + "</td></tr>"
//                        + "<tr><td>&nbsp;Approved By</td><td>&nbsp;&nbsp;" + mailId + "</td></tr>"
//                        + "<tr><td>&nbsp;&nbsp;Remarks</td><td>&nbsp;&nbsp;" + advanceRemarks + "</td></tr>"
//                        + "<tr><td colspan='2' align='center'>"
//                        + "</td></tr>"
//                        + "</table></body></html>";
//
//                String subject = "Vehicle Driver Advance Approval Request for Vehicle -" + vehicleNo + " Driver " + primaryDriver;
//                status = "Approved";
//
//            } else {
//                emailFormat = "<html>"
//                        + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
//                        + "<tr>"
//                        + "<th colspan='2'>Confirmation of Vehicle Driver Advance Rejected</th>"
//                        + "</tr>"
//                        + "<tr><td>&nbsp;Expense Type</td><td>&nbsp;&nbsp;" + expenseType + "</td></tr>"
//                        + "<tr><td>&nbsp;Vehicle No</td><td>&nbsp;&nbsp;" + vehicleNo + "</td></tr>"
//                        + "<tr><td>&nbsp;Driver Name</td><td>&nbsp;&nbsp;" + primaryDriver + "</td></tr>"
//                        + "<tr><td>&nbsp;Advance Amount</td><td>&nbsp;&nbsp;" + advanceAmount + "</td></tr>"
//                        + "<tr><td>&nbsp;Advance Date</td><td>&nbsp;&nbsp;" + advanceDate + "</td></tr>"
//                        + "<tr><td>&nbsp;Rejected By</td><td>&nbsp;&nbsp;" + mailId + "</td></tr>"
//                        + "<tr><td>&nbsp;&nbsp;Remarks</td><td>&nbsp;&nbsp;" + advanceRemarks + "</td></tr>"
//                        + "<tr><td colspan='2' align='center'>"
//                        + "</td></tr>"
//                        + "</table></body></html>";
//                status = "Rejected";
//            }
            path = "BrattleFoods/financeAdviceRequest.jsp";

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewVehicleDriverAdvanceApprove(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  Empty Trip Merging Details";
        TripTO tripTO = new TripTO();
        int status = 0;
        int userId = (Integer) session.getAttribute("userId");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Vehicle Driver Advance";
            request.setAttribute("pageTitle", pageTitle);

            String vehicleDriverAdvanceId = request.getParameter("vehicleDriverAdvanceId");
            String vehicleId = request.getParameter("vehicleId");
            String vehicleNo = request.getParameter("vehicleNo");
            String advanceDate = request.getParameter("advanceDate");
            String advanceRemarks = request.getParameter("advanceRemarks");
            String primaryDriver = request.getParameter("primaryDriver");
            String advanceAmount = request.getParameter("advanceAmount");
            String expenseType = request.getParameter("expenseType");
            request.setAttribute("vehicleDriverAdvanceId", vehicleDriverAdvanceId);
            request.setAttribute("vehicleId", vehicleId);
            request.setAttribute("vehicleNo", vehicleNo);
            request.setAttribute("advanceDate", advanceDate);
            request.setAttribute("advanceRemarks", advanceRemarks);
            request.setAttribute("primaryDriver", primaryDriver);
            request.setAttribute("advanceAmount", advanceAmount);
            request.setAttribute("expenseType", expenseType);
            tripTO.setVehicleId(vehicleId);
            tripTO.setUserId(userId);

            ArrayList viewVehicleDriverAdvanceList = new ArrayList();
            viewVehicleDriverAdvanceList = tripBP.getViewVehicleDriverAdvanceList(tripTO);
            request.setAttribute("viewVehicleDriverAdvanceList", viewVehicleDriverAdvanceList);

            path = "content/trip/viewVehicleDriverAdvance.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewVehicleDriverAdvanceDetails(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  Vehicle Driver Advance Details";
        TripTO tripTO = new TripTO();
        int status = 0;
        int userId = (Integer) session.getAttribute("userId");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Vehicle Driver Advance";
            request.setAttribute("pageTitle", pageTitle);

            String vehicleId = request.getParameter("vehicleId");
            String tripAdvanceId = request.getParameter("tripAdvanceId");
            tripTO.setVehicleId(vehicleId);
            tripTO.setVehicleDriverAdvanceId(tripAdvanceId);
            tripTO.setUserId(userId);

            ArrayList vehicleDriverAdvanceList = new ArrayList();
            vehicleDriverAdvanceList = tripBP.getVehicleAdvanceRequest(tripTO);
            request.setAttribute("vehicleDriverAdvanceList", vehicleDriverAdvanceList);

            ArrayList viewVehicleDriverAdvanceList = new ArrayList();
            viewVehicleDriverAdvanceList = tripBP.getViewVehicleDriverAdvanceList(tripTO);
            request.setAttribute("viewVehicleDriverAdvanceList", viewVehicleDriverAdvanceList);
            request.setAttribute("viewVehicleDriverAdvanceListSize", viewVehicleDriverAdvanceList.size());

            path = "content/trip/viewVehicleDriverAdvanceDetails.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView vehicleDriverAdvancePay(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View Vehicle Driver Advance";
        TripTO tripTO = new TripTO();
        int status = 0;
        int userId = (Integer) session.getAttribute("userId");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            String vehicleId = request.getParameter("vehicleId");
            String tripAdvanceId = request.getParameter("tripAdvanceId");
            tripTO.setVehicleId(vehicleId);
            tripTO.setVehicleDriverAdvanceId(tripAdvanceId);
            tripTO.setUserId(userId);

            ArrayList vehicleDriverAdvanceList = new ArrayList();
            vehicleDriverAdvanceList = tripBP.getVehicleAdvanceRequest(tripTO);
            request.setAttribute("vehicleDriverAdvanceList", vehicleDriverAdvanceList);

//            ArrayList viewVehicleDriverAdvanceList = new ArrayList();
//            viewVehicleDriverAdvanceList = tripBP.getViewVehicleDriverAdvanceList(tripTO);
//            request.setAttribute("viewVehicleDriverAdvanceList", viewVehicleDriverAdvanceList);
//            request.setAttribute("viewVehicleDriverAdvanceListSize", viewVehicleDriverAdvanceList.size());
            path = "content/trip/vehicleDriverAdvancePay.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveVehicleAdvancePay(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  Empty Trip Merging Details";
        TripTO tripTO = new TripTO();
        OperationTO operationTO = new OperationTO();
        int status = 0;
        int userId = (Integer) session.getAttribute("userId");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Vehicle Driver Advance";
            request.setAttribute("pageTitle", pageTitle);

            String vehicleDriverAdvanceId = request.getParameter("vehicleDriverAdvanceId");
            String advanceRemarks = request.getParameter("advanceRemarks");
            String usageTypeId = request.getParameter("usageTypeId");
            String vehicleId = request.getParameter("vehicleId");
            String paidamt = request.getParameter("paidamt");
            tripTO.setVehicleDriverAdvanceId(vehicleDriverAdvanceId);
            tripTO.setPaidAdvance(paidamt);
            tripTO.setPaidStatus("Y");
            tripTO.setUserId(userId);
            status = tripBP.saveVehicleDriverAdvancePay(tripTO);

            Date dNow = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(dNow);
            cal.add(Calendar.DATE, 0);
            dNow = cal.getTime();

            Date dNow1 = new Date();
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(dNow1);
            cal1.add(Calendar.DATE, -6);
            dNow1 = cal1.getTime();

            SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy");
            String today = ft.format(dNow);
            String fromday = ft.format(dNow1);

            operationTO.setFromDate(fromday);
            tripTO.setFromDate(fromday);

            operationTO.setToDate(today);
            tripTO.setToDate(today);

            ArrayList financeAdvice = operationBP.getfinanceAdvice(operationTO);
            ArrayList vehicleAdvanceRequest = new ArrayList();
            vehicleAdvanceRequest = tripBP.getVehicleAdvanceRequest(tripTO);

            request.setAttribute("vehicleAdvanceRequest", vehicleAdvanceRequest);

            request.setAttribute("financeAdvice", financeAdvice);
            request.setAttribute("fromdate", operationTO.getFromDate());
            request.setAttribute("todate", operationTO.getToDate());

            String emailInfoTo = "";
            String emailCc = "vijay.bisht@brattlefoods.com";
            String smtp = "";
            int emailPort = 0;
            String frommailid = "";
            String password = "";
            operationTO = new OperationTO();
            //smtp = operationTO.getSmtp();
            // emailPort = Integer.parseInt(operationTO.getPort());
            // frommailid = operationTO.getEmailId();
            // password = operationTO.getPassword();

            String activitycode = "EMAPR1";
            ArrayList emaildetails = new ArrayList();
            emaildetails = tripBP.getEmailDetails(activitycode);
            Iterator itr1 = emaildetails.iterator();

            if (itr1.hasNext()) {
                tripTO = new TripTO();
                tripTO = (TripTO) itr1.next();
                smtp = tripTO.getSmtp();
                emailPort = Integer.parseInt(tripTO.getPort());
                frommailid = tripTO.getEmailId();
                password = tripTO.getPassword();
                String toMailId = "";
                String emailTo = "";
                emailCc = "";
            }

            String fcLeadMailId = "";
            if (usageTypeId.equals("2")) {
                fcLeadMailId = tripBP.getFCLeadMailId(vehicleId);
            } else if (usageTypeId.equals("1")) {
                fcLeadMailId = tripBP.getSecondaryFCLeadMailId(vehicleId);
            }

            String vehicleNoEmail = request.getParameter("vehicleNoEmail");
            String vehicletypeEmail = request.getParameter("vehicletypeEmail");
            String driverEmail = request.getParameter("driverEmail");
            String approvedAmountEmail = request.getParameter("approvedAmountEmail");
            String expenseTypeEmail = request.getParameter("expenseTypeEmail");
            String emailFormat = "<html>"
                    + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                    + "<tr>"
                    + "<th colspan='2'>Vehicle Driver Advance</th>"
                    + "</tr>"
                    + "<tr><td>&nbsp;Expense Type</td><td>&nbsp;&nbsp;" + expenseTypeEmail + "</td></tr>"
                    + "<tr><td>&nbsp;Vehicle No</td><td>&nbsp;&nbsp;" + vehicleNoEmail + "</td></tr>"
                    + "<tr><td>&nbsp;Vehicle Type</td><td>&nbsp;&nbsp;" + vehicletypeEmail + "</td></tr>"
                    + "<tr><td>&nbsp;Driver Name</td><td>&nbsp;&nbsp;" + driverEmail + "</td></tr>"
                    + "<tr><td>&nbsp;Advance Amount</td><td>&nbsp;&nbsp;" + approvedAmountEmail + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Remarks</td><td>&nbsp;&nbsp;" + advanceRemarks + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Paid Amount</td><td>&nbsp;&nbsp;" + paidamt + "</td></tr>"
                    + "<tr><td colspan='2' align='center'>"
                    + "</td></tr>"
                    + "</table></body></html>";

            String subject = "Acknowledgement of Vehicle Driver Advance  for Vehicle -" + vehicleNoEmail + " Driver " + driverEmail;
            String content = emailFormat;
            tripTO.setMailTypeId("2");
            tripTO.setMailSubjectTo(subject);
            tripTO.setMailSubjectCc(subject);
            tripTO.setMailSubjectBcc("");
            tripTO.setMailContentTo(content);
            tripTO.setMailContentCc(content);
            tripTO.setMailContentBcc("");
            String mail = "";
            if (!emailInfoTo.equals("")) {
                tripTO.setMailIdTo(emailInfoTo + "," + fcLeadMailId);
            } else {
                tripTO.setMailIdTo(fcLeadMailId);
            }
            mail = tripTO.getMailIdTo();
            tripTO.setMailIdCc(emailCc);
            tripTO.setMailIdBcc("");
//            mailSendingId = tripBP.insertMailDetails(tripTO, userId);
            new SendMail(smtp, emailPort, frommailid, password, subject, content, mail, emailCc, userId).start();

            path = "BrattleFoods/financeAdvice.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView uploadBPCLData(HttpServletRequest request, HttpServletResponse reponse, TripCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();

        String path = "";
        String menuPath = "";
        menuPath = "Operation  >>  Upload BPCL(Tx.) Data";
        String pageTitle = "Upload BPCL Data";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            String[] temp = null;
            String lastBpclTxDate = "";
            lastBpclTxDate = tripBP.getLastBpclTransactionDate();
            temp = lastBpclTxDate.split("-");
            lastBpclTxDate = temp[2] + "-" + temp[1] + "-" + temp[0];
            request.setAttribute("lastBpclTxDate", lastBpclTxDate);
            path = "content/trip/bpclTransactionHistoryUpload.jsp";

//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to view role details --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);

    }

    public ModelAndView editInvoiceAmount(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Finance  >>  Generate Bill ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            int userId = (Integer) session.getAttribute("userId");
            String invoiceId = request.getParameter("invoiceId");
            String tripId = request.getParameter("tripId");
            tripTO.setInvoiceId(invoiceId);
            tripTO.setTripSheetId(tripId);
            tripTO.setTripId(tripId);
            request.setAttribute("tripId", tripId);
            request.setAttribute("invoiceId", invoiceId);
            ArrayList courierDetails = tripBP.getCourierDetails(tripTO);
            TripTO tripTONew = new TripTO();
            if (courierDetails.size() > 0) {
                request.setAttribute("courierDetails", courierDetails);
                Iterator itr2 = courierDetails.iterator();
                while (itr2.hasNext()) {
                    tripTONew = (TripTO) itr2.next();
                    request.setAttribute("courierNo", tripTONew.getCourierNo());
                    request.setAttribute("courierRemarks", tripTONew.getCourierRemarks());

                }
            }
            int billSubmitStatus = tripBP.checkBillSubmittedStatus(tripTO, userId);
            request.setAttribute("billSubmitStatus", billSubmitStatus);
            ArrayList invoiceHeader = tripBP.getInvoiceHeader(tripTO);
            Iterator itr1 = invoiceHeader.iterator();
            TripTO trpTO1 = new TripTO();
            while (itr1.hasNext()) {
                trpTO1 = (TripTO) itr1.next();
                request.setAttribute("billingType", trpTO1.getBillingType());
                request.setAttribute("totalWeight", trpTO1.getTotalWeight());
                request.setAttribute("ratePerKm", trpTO1.getRatePerKm());
            }
            ArrayList invoiceDetails = tripBP.getInvoiceDetails(tripTO);
            Iterator itr = invoiceDetails.iterator();
            TripTO trpTO = new TripTO();
            while (itr.hasNext()) {
                trpTO = (TripTO) itr.next();
                request.setAttribute("freightAmount", trpTO.getFreightAmount());
            }
            ArrayList invoiceDetailExpenses = tripBP.getInvoiceDetailExpenses(tripTO);

            ArrayList viewPODDetails = new ArrayList();
            viewPODDetails = tripBP.getPODDetails(tripTO);
            request.setAttribute("viewPODDetails", viewPODDetails);

            ArrayList viewTempLogDetails = new ArrayList();
            viewTempLogDetails = tripBP.getTempLogDetails(tripTO);
            request.setAttribute("viewTempLogDetails", viewTempLogDetails);

            ArrayList bpclTransactionHistory = new ArrayList();
            bpclTransactionHistory = tripBP.getBPCLTransactionHistory(tripTO);
            request.setAttribute("bpclTransactionHistory", bpclTransactionHistory);

            request.setAttribute("invoiceHeader", invoiceHeader);
            request.setAttribute("invoiceDetails", invoiceDetails);
            if (invoiceDetailExpenses.size() > 0) {
                request.setAttribute("invoiceDetailExpenses", invoiceDetailExpenses);
            }

            path = "content/trip/editInvoiceAmount.jsp";
            ArrayList tripDetails = new ArrayList();
            tripDetails = tripBP.getTripDetails(tripTO);

            request.setAttribute("tripDetails", tripDetails);
            /*
             ArrayList tripDetails = new ArrayList();
             ArrayList tripPointDetails = new ArrayList();
             tripDetails = tripBP.getTripDetails(tripTO);
             tripPointDetails = tripBP.getTripPointDetails(tripTO);

             request.setAttribute("tripDetails", tripDetails);
             request.setAttribute("tripPointDetails", tripPointDetails);
             path = "content/trip/viewTripSheetDetails.jsp";
             */
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView updateInvoiceAmount(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Finance  >>  Generate Bill ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);
            int userId = (Integer) session.getAttribute("userId");
            TripTO tripTO = new TripTO();

            String invoiceId = request.getParameter("invoiceId");
            String tripId = request.getParameter("tripId");
            tripTO.setInvoiceId(invoiceId);
            tripTO.setTripSheetId(tripId);
            tripTO.setTripId(tripId);
            request.setAttribute("tripId", tripId);
            String editMode = request.getParameter("editMode");
            String discountAmount = request.getParameter("discountAmount");
            String billedAmount = request.getParameter("billedAmount");
            String billedAmountPointToPoint = request.getParameter("billedAmountPointToPoint");
            String weight = request.getParameter("weight");
            String ratePerKg = request.getParameter("ratePerKg");
            String billedAmountPointToPointWeight = request.getParameter("billedAmountPointToPointWeight");
            String billingTypeId = request.getParameter("billingTypeId");
            tripTO.setBillingType(billingTypeId);
            tripTO.setEditMode(editMode);
            if (editMode.equals("1")) {
                tripTO.setDiscountAmount(discountAmount);
                tripTO.setBilledAmount(billedAmount);
            } else if (editMode.equals("2")) {
                if (billingTypeId.equals("1")) {
                    tripTO.setBilledAmount(billedAmountPointToPoint);
                } else if (billingTypeId.equals("2")) {
                    tripTO.setWeight(weight);
                    tripTO.setRatePerKg(ratePerKg);
                    tripTO.setBilledAmount(billedAmountPointToPointWeight);
                }

            }
            int billSubmitStatus = tripBP.checkBillSubmittedStatus(tripTO, userId);
            request.setAttribute("billSubmitStatus", billSubmitStatus);
            int updateInvoiceAmount = 0;
            updateInvoiceAmount = tripBP.updateInvoiceAmount(tripTO, userId);
            ArrayList courierDetails = tripBP.getCourierDetails(tripTO);
            TripTO tripTONew = new TripTO();
            if (courierDetails.size() > 0) {
                request.setAttribute("courierDetails", courierDetails);
                Iterator itr2 = courierDetails.iterator();
                while (itr2.hasNext()) {
                    tripTONew = (TripTO) itr2.next();
                    request.setAttribute("courierNo", tripTONew.getCourierNo());
                    request.setAttribute("courierRemarks", tripTONew.getCourierRemarks());

                }
            }
            ArrayList invoiceHeader = tripBP.getInvoiceHeader(tripTO);
            Iterator itr1 = invoiceHeader.iterator();
            TripTO trpTO1 = new TripTO();
            while (itr1.hasNext()) {
                trpTO1 = (TripTO) itr1.next();
                request.setAttribute("billingType", trpTO1.getBillingType());
                request.setAttribute("totalWeight", trpTO1.getTotalWeight());
                request.setAttribute("ratePerKm", trpTO1.getRatePerKm());
            }
            ArrayList invoiceDetails = tripBP.getInvoiceDetails(tripTO);
            Iterator itr = invoiceDetails.iterator();
            TripTO trpTO = new TripTO();
            while (itr.hasNext()) {
                trpTO = (TripTO) itr.next();
                request.setAttribute("freightAmount", trpTO.getFreightAmount());
            }
            ArrayList invoiceDetailExpenses = tripBP.getInvoiceDetailExpenses(tripTO);

            ArrayList viewPODDetails = new ArrayList();
            viewPODDetails = tripBP.getPODDetails(tripTO);
            request.setAttribute("viewPODDetails", viewPODDetails);

            ArrayList viewTempLogDetails = new ArrayList();
            viewTempLogDetails = tripBP.getTempLogDetails(tripTO);
            request.setAttribute("viewTempLogDetails", viewTempLogDetails);

            ArrayList bpclTransactionHistory = new ArrayList();
            bpclTransactionHistory = tripBP.getBPCLTransactionHistory(tripTO);
            request.setAttribute("bpclTransactionHistory", bpclTransactionHistory);

            request.setAttribute("invoiceHeader", invoiceHeader);
            request.setAttribute("invoiceDetails", invoiceDetails);
            if (invoiceDetailExpenses.size() > 0) {
                request.setAttribute("invoiceDetailExpenses", invoiceDetailExpenses);
            }

            path = "content/trip/billingInvoice.jsp";
            ArrayList tripDetails = new ArrayList();
            tripDetails = tripBP.getTripDetails(tripTO);

            request.setAttribute("tripDetails", tripDetails);
            /*
             ArrayList tripDetails = new ArrayList();
             ArrayList tripPointDetails = new ArrayList();
             tripDetails = tripBP.getTripDetails(tripTO);
             tripPointDetails = tripBP.getTripPointDetails(tripTO);

             request.setAttribute("tripDetails", tripDetails);
             request.setAttribute("tripPointDetails", tripPointDetails);
             path = "content/trip/viewTripSheetDetails.jsp";
             */
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView employeeInTrip(HttpServletRequest request, HttpServletResponse reponse, TripCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        tripCommand = command;
        String path = "";
        String menuPath = "";
        menuPath = "Operation  >>  Employee In Trip";
        String pageTitle = "Employee";
        TripTO tripTO = new TripTO();
        OperationTO operationTO = new OperationTO();
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            String staffId = request.getParameter("staffId");
            String activeInd = request.getParameter("activeInd");
            String remarks = request.getParameter("remarks");
            tripTO.setEmployeeId(staffId);
            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
            }
            ArrayList employeeTripDetails = new ArrayList();
            employeeTripDetails = tripBP.getEmployeeTripDetails(tripTO);
            request.setAttribute("employeeTripDetails", employeeTripDetails);
            request.setAttribute("activeInd", activeInd);
            request.setAttribute("remarks", remarks);
            ArrayList primaryDriverDetails = new ArrayList();
            primaryDriverDetails = operationBP.getPrimaryDriverName(operationTO);
            request.setAttribute("primaryDriverDetails", primaryDriverDetails);
            ArrayList cityList = new ArrayList();
            cityList = tripBP.getCityList();
            request.setAttribute("cityList", cityList);

            path = "content/trip/employeeInTrip.jsp";

//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to view role details --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);

    }

    public ModelAndView updateEmployeeTrip(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View start Trip Sheet ";
        int insertStatus = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
            }
            if (tripCommand.getStartDate() != null && !tripCommand.getStartDate().equals("")) {
                tripTO.setStartDate(tripCommand.getStartDate());
            }
            if (tripCommand.getEndDate() != null && !tripCommand.getEndDate().equals("")) {
                tripTO.setEndDate(tripCommand.getEndDate());
            }

            if (request.getParameter("startKM") != null && !request.getParameter("startKM").equals("")) {
                tripTO.setStartOdometerReading(request.getParameter("startKM"));
            }
            if (tripCommand.getEndOdometerReading() != null && !tripCommand.getEndOdometerReading().equals("")) {
                tripTO.setEndOdometerReading(tripCommand.getEndOdometerReading());
            }

            if (tripCommand.getTotalKM() != null && !tripCommand.getTotalKM().equals("")) {
                tripTO.setTotalKM(tripCommand.getTotalKM());
            }
            if (tripCommand.getTotalDays() != null && !tripCommand.getTotalDays().equals("")) {
                tripTO.setTotalDays(tripCommand.getTotalDays());
            }
            if (tripCommand.getVehicleId() != null && !tripCommand.getVehicleId().equals("")) {
                tripTO.setVehicleId(tripCommand.getVehicleId());
            }
            if (tripCommand.getCityId() != null && !tripCommand.getCityId().equals("")) {
                tripTO.setCityId(tripCommand.getCityId());
            }
            if (request.getParameter("startHM") != null && !request.getParameter("startHM").equals("")) {
                tripTO.setStartHM(request.getParameter("startHM"));
            }
            if (request.getParameter("endHM") != null && !request.getParameter("endHM").equals("")) {
                tripTO.setEndHM(request.getParameter("endHM"));
            }
            if (request.getParameter("totalHM") != null && !request.getParameter("totalHM").equals("")) {
                tripTO.setTotalHm(request.getParameter("totalHM"));
            }
            if (request.getParameter("driverRemarks") != null && !request.getParameter("driverRemarks").equals("")) {
                tripTO.setRemarks(request.getParameter("driverRemarks"));
            }
            String driverChangeMinute = request.getParameter("driverChangeHour");
            String driverChangeHour = request.getParameter("driverChangeMinute");
            String driverCount = request.getParameter("driverCount");
            String driverInTrip = request.getParameter("driverInTrip");

            tripTO.setDriverChangeHour(driverChangeHour);
            tripTO.setDriverChangeMinute(driverChangeMinute);
            String vehicleId = request.getParameter("vehicleId");
            tripTO.setVehicleId(vehicleId);
            String advancePaid = request.getParameter("advancePaid");
            String returnAmount = request.getParameter("returnAmount");
            String balanceAmount = request.getParameter("balanceAmount");
            String employeeId = request.getParameter("employeeId");
            String activeInd = request.getParameter("activeInd");
            String remarks = request.getParameter("remarks");
            String primaryDriverId = request.getParameter("primaryDriverId");
            tripTO.setActualAdvancePaid(advancePaid);
            tripTO.setReturnAmount(returnAmount);
            tripTO.setBalanceAmount(balanceAmount);
            tripTO.setEmployeeId(employeeId);
            tripTO.setStatus(activeInd);
            tripTO.setActionRemarks(remarks);
            tripTO.setPrimaryDriverId(primaryDriverId);
            tripTO.setDriverCount(driverCount);
            tripTO.setDriverInTrip(driverInTrip);

            insertStatus = tripBP.updateEmployeeInTrip(tripTO, userId);
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Driver Deactivated Successfully....");
            path = "BrattleFoods/primaryDriverRedirective.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void checkEmployeeInTrip(HttpServletRequest request, HttpServletResponse response, TripCommand command) throws IOException {
        System.out.println("i am in ajax ");
        HttpSession session = request.getSession();
        String driverId = request.getParameter("driverId");
        System.out.println("driverId" + driverId);
        String suggestions = tripBP.checkEmployeeInTrip(driverId);
        if (suggestions != null) {
            suggestions = suggestions;
        } else {
            suggestions = "";
        }
        System.out.println("suggestions" + suggestions);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.println(suggestions);
        writer.close();
    }

    public ModelAndView mailNotDelivered(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View start Trip Sheet ";
        int insertStatus = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            TripTO tripTO = new TripTO();
            tripTO.setMailDeliveredStatusId("0");
            ArrayList mailNotDeliveredList = new ArrayList();
            mailNotDeliveredList = tripBP.getMailNotDeliveredList(tripTO);
            if (mailNotDeliveredList.size() > 0) {
                request.setAttribute("mailNotDeliveredList", mailNotDeliveredList);
            }
            path = "BrattleFoods/mailNotDeliveredList.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView mailDelivered(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View start Trip Sheet ";
        int insertStatus = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            TripTO tripTO = new TripTO();
            tripTO.setMailDeliveredStatusId("1");
            ArrayList mailNotDeliveredList = new ArrayList();
            mailNotDeliveredList = tripBP.getMailNotDeliveredList(tripTO);
            if (mailNotDeliveredList.size() > 0) {
                request.setAttribute("mailNotDeliveredList", mailNotDeliveredList);
            }
            path = "BrattleFoods/mailNotDeliveredList.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewMailResendDetails(HttpServletRequest request, HttpServletResponse reponse, TripCommand command) {
        HttpSession session = request.getSession();

        String path = "";
        String menuPath = "";
        menuPath = "Operation  >>  View Email Resend Details";
        String pageTitle = "Email Resend Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = 0;
        TripTO tripTO = new TripTO();
        ModelAndView mv = null;
        boolean statusFlag = false;
        String emailFormat = "";
        try {

            String mailSendingId = request.getParameter("mailSendingId");
            ArrayList consignmentPaymentDetails = new ArrayList();
            tripTO.setMailSendingId(mailSendingId);
            ArrayList mailNotDeliveredList = new ArrayList();
            mailNotDeliveredList = tripBP.getMailNotDeliveredList(tripTO);
            if (mailNotDeliveredList.size() > 0) {
                request.setAttribute("mailNotDeliveredList", mailNotDeliveredList);
            }
            path = "content/trip/resendMail.jsp";

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to view role details --> " + exception);
            exception.printStackTrace();
        }
        return new ModelAndView(path);

    }

    public ModelAndView reSendMailDetails(HttpServletRequest request, HttpServletResponse reponse, TripCommand command) {
        HttpSession session = request.getSession();

        String path = "";
        String menuPath = "";
        menuPath = "Operation  >>  View Trip Details For Email";
        String pageTitle = "Customer On-Hold";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = 0;
        TripTO tripTO = new TripTO();
        ModelAndView mv = null;
        boolean statusFlag = false;
        String emailFormat = "";
        try {
            String to = "";
            String activitycode = "EMREQ1";
            //String to = "nipun.kohli@brattlefoods.com,srini@entitlesolutions.com";
            String smtp = "";
            int emailPort = 0;
            String frommailid = "";
            String password = "";
            String subject = "";
            String content = "";

            ArrayList emaildetails = new ArrayList();
            emaildetails = tripBP.getEmailDetails(activitycode);
            Iterator itr1 = emaildetails.iterator();
            TripTO tripTO1 = null;
            if (itr1.hasNext()) {
                tripTO1 = new TripTO();
                tripTO1 = (TripTO) itr1.next();
                smtp = tripTO1.getSmtp();
                emailPort = Integer.parseInt(tripTO1.getPort());
                frommailid = tripTO1.getEmailId();
                password = tripTO1.getPassword();
                to = tripTO1.getTomailId();
            }
            emailFormat = request.getParameter("mailContentTo");
            subject = request.getParameter("subject");
            content = emailFormat;
            to = request.getParameter("mailTO");
            String cc = request.getParameter("mailCC");

            //mail.sendmail(smtp, emailPort, frommailid, password, subject, content, to);
            int mailSendingId = 0;
            tripTO.setMailTypeId("2");
            tripTO.setMailSubjectTo(subject);
            tripTO.setMailSubjectCc(subject);
            tripTO.setMailSubjectBcc("");
            tripTO.setMailContentTo(content);
            tripTO.setMailContentCc(content);
            tripTO.setMailContentBcc("");
            tripTO.setMailIdTo(to);
            tripTO.setMailIdCc(cc);
            tripTO.setMailIdBcc("");
            String[] temp = null;
            if (cc.contains(",")) {
                temp = cc.split(",");
                for (int i = 0; i < temp.length; i++) {
                    if (temp[i].equals("naved.ahmad@brattlefoods.com")) {
                        cc = "";
                    } else {
                        cc = temp[i];
                    }
                }
            } else {
                cc = "";
            }

//            mailSendingId = tripBP.insertMailDetails(tripTO, userId);
            new SendMail(smtp, emailPort, frommailid, password, subject, content, to, cc, userId).start();
            path = "content/trip/resendMail.jsp";

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to view role details --> " + exception);
            exception.printStackTrace();
        }
        return new ModelAndView(path);

    }

    public ModelAndView uploadCustomerOutStanding(HttpServletRequest request, HttpServletResponse reponse, TripCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();

        String path = "";
        String menuPath = "";
        menuPath = "Operation  >>  Customer Outstanding Upload";
        String pageTitle = "Customer Outstanding Upload";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            String lastUploadCustomerOutStandingDate = "";
            lastUploadCustomerOutStandingDate = tripBP.getLastUploadCustomerOutstandingDate();
            request.setAttribute("lastUploadCustomerOutStandingDate", lastUploadCustomerOutStandingDate);
            path = "content/trip/customerOutstandingUpload.jsp";

//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to view role details --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);
    }

    public ModelAndView customerOutStandingHistoryUpload(HttpServletRequest request, HttpServletResponse response, TripCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        int userId = 0;
        String pageTitle = "Customer Outstanding Upload ";
        menuPath = "Operation >> Customer OutStanding Upload";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        String transactionHistoryId = "", customerCode = "", customerName = "", outStandingAmount = "", dealerCity = "", transactionDate = "", accountingDate = "", transactionType = "", currency = "", amount = "", volumeDocNo = "", amoutBalance = "", petromilesEarned = "", odometerReading = "";

        try {
            tripCommand = command;
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            isMultipart = ServletFileUpload.isMultipartContent(request);
            ArrayList customerOutStandingList = new ArrayList();
            ArrayList tripWrongDataList = new ArrayList();
            int insertBPCLTransactionHistory = 0;
            int tripCount = 0;
            if (isMultipart) {
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();

                        if (!"".equals(uploadedFileName) && uploadedFileName != null) {

                            String[] splitFileName = uploadedFileName.split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath = tempServerFilePath + "\\" + fileSavedAs;
                            actualFilePath = actualServerFilePath + "\\" + uploadedFileName;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
                            String part1 = parts.replace("\\", "");

                            WorkbookSettings ws = new WorkbookSettings();
                            ws.setLocale(new Locale("en", "EN"));
                            Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                            Sheet s = workbook.getSheet(0);
                            System.out.println("rows" + userId + s.getRows());

                            ArrayList customerList = new ArrayList();
                            ArrayList productCategoryList = new ArrayList();
                            ArrayList vehicleList = new ArrayList();
                            boolean validationStatus = true;
                            boolean routeCheckReqd = true;
                            String dd = "";
                            String mm = "";
                            String yy = "";
                            String nextCell = "";
                            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                            TripTO tripTO = new TripTO();
                            tripTO.setUserId(userId);
                            System.out.println("s.getRows() = " + s.getRows());
                            for (int i = 1; i < s.getRows(); i++) {
                                tripTO = new TripTO();
                                customerCode = s.getCell(0, i).getContents();
                                tripTO.setCustomerCode(customerCode);
                                customerName = s.getCell(1, i).getContents();
                                tripTO.setCustomerName(customerName);
                                outStandingAmount = s.getCell(2, i).getContents();
                                tripTO.setOutStandingAmount(outStandingAmount);

                                String temp[] = null;
                                String previouroutstandingAmount = tripBP.getPreviousOutStandingAmount(tripTO);
                                temp = previouroutstandingAmount.split("~");
                                tripTO.setAmount(temp[0]);
                                tripTO.setCustomerId(temp[1]);
                                customerOutStandingList.add(tripTO);

                            }//////loop end

                        }
                    }
                }
            }
            if (customerOutStandingList.size() > 0) {
                request.setAttribute("customerOutStandingList", customerOutStandingList);
            }
            path = "content/trip/customerOutstandingUpload.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView saveCustomerOutStandingHistory(HttpServletRequest request, HttpServletResponse reponse, TripCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();

        String path = "";
        String menuPath = "";
        menuPath = "Operation  >>  Customer Outstanding Upload";
        String pageTitle = "Customer Outstanding Upload";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        int updateCustomerOutStanding = 0;
        try {
            String[] customerId = null;
            String[] outStandingAmount = null;
            customerId = request.getParameterValues("customerId");
            outStandingAmount = request.getParameterValues("outStandingAmount");
            String lastUploadCustomerOutStandingDate = "";
            if (customerId.length > 0) {
                updateCustomerOutStanding = tripBP.updateCustomerOutStandingAmount(customerId, outStandingAmount, userId);
            }
            ArrayList customerOutStandingList = new ArrayList();
            customerOutStandingList = tripBP.getCustomerOutStandingList();
            if (customerOutStandingList.size() > 0) {
                request.setAttribute("customerOutStandingList", customerOutStandingList);
            }
            if (updateCustomerOutStanding > 0) {
                request.setAttribute("updateCustomerOutStanding", updateCustomerOutStanding);
                lastUploadCustomerOutStandingDate = tripBP.getLastUploadCustomerOutstandingDate();
                request.setAttribute("lastUploadCustomerOutStandingDate", lastUploadCustomerOutStandingDate);
            }
            path = "content/trip/customerOutstandingUpload.jsp";

//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to view role details --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);
    }

//    public ModelAndView updateConsignmentCreditLimitStatus(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
//
//        HttpSession session = request.getSession();
//        String path = "";
//
//        String menuPath = " Operation  >>  Approve/Reject Consignment Credit Limit Status ";
//
//        try {
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            //            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//            //                path = "content/common/NotAuthorized.jsp";
//            //            } else {
//            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            String pageTitle = " Approve/Reject Consignment Credit Limit Status ";
//            request.setAttribute("pageTitle", pageTitle);
//
//            TripTO tripTO = new TripTO();
//
//            boolean checkflag = false;
//            int updateStatus = 0;
//            int userId = 0;
//            if (session.getAttribute("userId") != null && !"".equals(session.getAttribute("userId"))) {
//                userId = (Integer) session.getAttribute("userId");
//            }
//            System.out.println("userId = " + userId);
//            String consignmentOrderId = "";
//            String approvalStatus = "";
//            String approverId = "";
//            String creditLimit = request.getParameter("creditLimit");
//            String outStanding = request.getParameter("outStanding");
//            String customerId = request.getParameter("customerId");
//            String step = request.getParameter("step");
//            System.out.println("userId = " + userId);
//
//            consignmentOrderId = request.getParameter("consignmentOrderId");
//            approvalStatus = request.getParameter("approvestatus");
//            approverId = request.getParameter("mailId");
//            tripTO.setApprovalStatus(approvalStatus);
//            tripTO.setConsignmentId(consignmentOrderId);
//            tripTO.setMailId(approverId);
//
//            if (step.equals("1")) {
//                String checkConsignmentCreditLimitStatus = "";
//                String[] temp = null;
//                int operationControllerCount = 0;
//                String approverMailId = "";
//                String cancelMailId = "";
//                String ceoMailId = "";
//                int approval = 0;
//                checkConsignmentCreditLimitStatus = tripBP.checkConsignmentCreditLimitStatus(tripTO, userId);
//                if (checkConsignmentCreditLimitStatus != null) {
//                    temp = checkConsignmentCreditLimitStatus.split("~");
//                    operationControllerCount = Integer.parseInt(temp[0]);
//                    approverMailId = temp[1];
//                    cancelMailId = temp[2];
//                    ceoMailId = temp[3];
//                    approval = Integer.parseInt(temp[4]);
//                    if (approvalStatus.equals("1")) {
//                        if (!"".equals(approverMailId)) {
//                            tripTO.setMailId(approverMailId + "," + tripTO.getMailId());
//                        }
//                    } else if (approvalStatus.equals("2")) {
//                        if (!"".equals(cancelMailId)) {
//                            tripTO.setMailId(cancelMailId + "," + tripTO.getMailId());
//
//                        }
//                    }
//
//                }
//                if (approval == 0 && operationControllerCount > 0) {
//                    checkflag = true;
//                    updateStatus = tripBP.updateConsignmentOrderCreditLimitStatus(tripTO, userId);
//                } else {
//                    checkflag = false;
//                }
//                if (updateStatus > 0) {
//                    checkConsignmentCreditLimitStatus = tripBP.checkConsignmentCreditLimitStatus(tripTO, userId);
//                    temp = checkConsignmentCreditLimitStatus.split("~");
//                    operationControllerCount = Integer.parseInt(temp[0]);
//                    approverMailId = temp[1];
//                    cancelMailId = temp[2];
//                    ceoMailId = temp[3];
//                    approval = Integer.parseInt(temp[4]);
//                    if (approval == 0 && operationControllerCount == 0) {
//                        if ("".equals(cancelMailId) && !"".equals(approverMailId)) {
//                            tripTO.setStatusId("5");
//                            checkflag = true;
//                            tripTO.setApprovalStatusId("1");
//                        } else if (!"".equals(cancelMailId) && "".equals(approverMailId)) {
//                            tripTO.setStatusId("4");
//                            tripTO.setApprovalStatusId("0");
//                            checkflag = true;
//                        } else if (!"".equals(cancelMailId) && !"".equals(approverMailId)) {
//                            tripTO.setStatusId("3");
//                            tripTO.setApprovalStatusId("");
//                        }
//                        if (!tripTO.getStatusId().equals("3")) {
//                            updateStatus = tripBP.updateConsignmentOrderApprovalStatus(tripTO, userId);
//                            ArrayList consignmentList = new ArrayList();
//                            OperationTO operationTO = new OperationTO();
//                            operationTO.setConsignmentOrderId(consignmentOrderId);
//                            consignmentList = operationBP.viewConsignmentList(operationTO);
//                            String to = "";
//                            String smtp = "";
//                            int emailPort = 0;
//                            String frommailid = "";
//                            String password = "";
//                            String activitycode = "";
//                            String statusText = "";
//                            String statusTextNew = "";
//                            if (tripTO.getStatusId().equals("5")) {
//                                activitycode = "CREDITLIMITAPP";
//                                statusText = "Order Approved";
//                                statusTextNew = "Approved";
//                            } else if (tripTO.getStatusId().equals("4")) {
//                                activitycode = "CREDITLIMITREJ";
//                                statusText = "Order Rejected";
//                                statusTextNew = "Rejected";
//                            }
//                            String contractId = "";
//                            ArrayList emaildetails = new ArrayList();
//                            emaildetails = tripBP.getEmailDetails(activitycode);
//                            Iterator itrMail = emaildetails.iterator();
//                            if (itrMail.hasNext()) {
//                                tripTO = new TripTO();
//                                tripTO = (TripTO) itrMail.next();
//                                smtp = tripTO.getSmtp();
//                                emailPort = Integer.parseInt(tripTO.getPort());
//                                frommailid = tripTO.getEmailId();
//                                password = tripTO.getPassword();
//                                to = tripTO.getTomailId();
//
//                            }
//                            String toEmailId = operationBP.getEmailToListForConsignment(customerId);
//
//                            String[] temp1 = toEmailId.split(",");
//                            to = "";
//                            int cntr = 0;
//                            for (int i = 0; i < temp1.length; i++) {
//                                if (!"-".equals(temp1[i]) && !"".equals(temp1[i])) {
//                                    if (cntr == 0) {
//                                        to = temp1[i];
//                                    } else {
//                                        to = to + "," + temp1[i];
//
//                                    }
//                                    cntr++;
//                                }
//                            }
//                            String confirmationCode = "CONFIRM0001";
//                            String confirmationMail = operationBP.getConfirmationMail(confirmationCode);
//                            if (confirmationMail != null) {
//                                to = to + "," + confirmationMail;
//                            }
//                            OperationTO oprTO = new OperationTO();
//                            if (consignmentList.size() > 0) {
//                                Iterator itr = consignmentList.iterator();
//                                if (itr.hasNext()) {
//                                    oprTO = (OperationTO) itr.next();
//
//                                    String subject = "Consignment Note " + statusTextNew + " For : " + operationTO.getCustomerName() + ". Route " + oprTO.getOrigin() + "-" + oprTO.getDestination() + " Vehicle Required On :" + operationTO.getVehicleRequiredDate() + " :" + operationTO.getVehicleRequiredHour();
//                                    String emailFormat1 = "<html>"
//                                            + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
//                                            + "<tr>"
//                                            + "<th colspan='2'>Consignment Order Created With Credit limit Approval has been " + statusTextNew + "</th>"
//                                            + "</tr>"
//                                            + "<tr><td>&nbsp; Consignment Order No:&nbsp;</td><td>&nbsp; " + oprTO.getConsignmentNoteNo() + "&nbsp;</td></tr>"
//                                            + "<tr><td>&nbsp; Customer:&nbsp;</td><td>&nbsp; " + oprTO.getCustomerName() + "&nbsp;</td></tr>"
//                                            + "<tr><td>&nbsp; Vehicle Type: &nbsp;</td><td>&nbsp;" + oprTO.getVehicleTypeName() + "&nbsp;</td></tr>"
//                                            + "<tr><td>&nbsp; Vehicle Required Date:&nbsp; </td><td>&nbsp;" + oprTO.getVehicleRequiredDate() + " Hour:" + oprTO.getVehicleRequiredHour() + "&nbsp;</td></tr>"
//                                            + "<tr><td> &nbsp;Route: &nbsp;</td><td>&nbsp;" + oprTO.getOrigin() + "-" + oprTO.getDestination() + "&nbsp;</td></tr>"
//                                            + "<tr><td>&nbsp; Product Category: &nbsp;</td><td>&nbsp;" + oprTO.getProductCategoryName() + "&nbsp;</td></tr>"
//                                            + "<tr><td>&nbsp; Reefer: &nbsp;</td><td>&nbsp;" + oprTO.getReeferRequired() + "&nbsp;</td></tr>"
//                                            + "<tr><td>&nbsp; Total Packages (Nos): &nbsp;</td><td>&nbsp;" + oprTO.getTotalPackage() + "&nbsp;</td></tr>"
//                                            + "<tr><td>&nbsp; Total Weight (Kg): &nbsp;</td><td>&nbsp;" + oprTO.getTotalWeightage() + "&nbsp;</td></tr>"
//                                            + "<tr><td>&nbsp; Total Distance (Km): &nbsp;</td><td>&nbsp;" + oprTO.getTotalDistance() + "&nbsp;</td></tr>"
//                                            + "<tr><td> &nbsp;Transit Hours (Hr): &nbsp;</td><td>&nbsp;" + oprTO.getTotalHours() + ":" + oprTO.getTotalMinutes() + "&nbsp;</td></tr>"
//                                            + "<tr><td> &nbsp;Revenue: &nbsp;</td><td>&nbsp;" + oprTO.getFreightCharges() + "&nbsp;</td></tr>"
//                                            + "<tr><td> &nbsp;Credit Limit: &nbsp;</td><td>&nbsp;" + creditLimit + "&nbsp;</td></tr>"
//                                            + "<tr><td> &nbsp;Out Standing Amount: &nbsp;</td><td>&nbsp;" + outStanding + "&nbsp;</td></tr>"
//                                            + "<tr><td> &nbsp;Order Sttaus: &nbsp;</td><td>&nbsp;" + statusText + "&nbsp;</td></tr>"
//                                            + "<tr height='25'><td></td><td></td></tr>"
//                                            + "</table></body></html>";
//                                    String content = emailFormat1;
//                                    new SendMail(smtp, emailPort, frommailid, password, subject, content, to, "", userId).start();
//                                }
//                            }
//                        } else if (tripTO.getStatusId().equals("3")) {
//                            ArrayList consignmentList = new ArrayList();
//                            OperationTO operationTO = new OperationTO();
//                            operationTO.setConsignmentOrderId(consignmentOrderId);
//                            consignmentList = operationBP.viewConsignmentList(operationTO);
//                            String to = "";
//                            String smtp = "";
//                            int emailPort = 0;
//                            String frommailid = "";
//                            String password = "";
//                            String activitycode = "CREDITLIMITCEO";
//                            String contractId = "";
//                            ArrayList emaildetails = new ArrayList();
//                            emaildetails = tripBP.getEmailDetails(activitycode);
//                            Iterator itrMail = emaildetails.iterator();
//                            if (itrMail.hasNext()) {
//                                tripTO = new TripTO();
//                                tripTO = (TripTO) itrMail.next();
//                                smtp = tripTO.getSmtp();
//                                emailPort = Integer.parseInt(tripTO.getPort());
//                                frommailid = tripTO.getEmailId();
//                                password = tripTO.getPassword();
//                                to = tripTO.getTomailId();
//
//                            }
//                            OperationTO oprTO = new OperationTO();
//                            if (consignmentList.size() > 0) {
//                                Iterator itr = consignmentList.iterator();
//                                if (itr.hasNext()) {
//                                    oprTO = (OperationTO) itr.next();
//
//                                    String subject = "Consignment Note Created With Credilt Limit Approval For : " + operationTO.getCustomerName() + ". Route " + oprTO.getOrigin() + "-" + oprTO.getDestination() + " Vehicle Required On :" + operationTO.getVehicleRequiredDate() + " :" + operationTO.getVehicleRequiredHour();
//                                    String emailFormat1 = "<html>"
//                                            + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
//                                            + "<tr>"
//                                            + "<th colspan='2'>Consignment Order Created With Credit limit Approval</th>"
//                                            + "</tr>"
//                                            + "<tr><td>&nbsp; Consignment Order No:&nbsp;</td><td>&nbsp; " + oprTO.getConsignmentNoteNo() + "&nbsp;</td></tr>"
//                                            + "<tr><td>&nbsp; Customer:&nbsp;</td><td>&nbsp; " + oprTO.getCustomerName() + "&nbsp;</td></tr>"
//                                            + "<tr><td>&nbsp; Vehicle Type: &nbsp;</td><td>&nbsp;" + oprTO.getVehicleTypeName() + "&nbsp;</td></tr>"
//                                            + "<tr><td>&nbsp; Vehicle Required Date:&nbsp; </td><td>&nbsp;" + oprTO.getVehicleRequiredDate() + " Hour:" + oprTO.getVehicleRequiredHour() + "&nbsp;</td></tr>"
//                                            + "<tr><td> &nbsp;Route: &nbsp;</td><td>&nbsp;" + oprTO.getOrigin() + "-" + oprTO.getDestination() + "&nbsp;</td></tr>"
//                                            + "<tr><td>&nbsp; Product Category: &nbsp;</td><td>&nbsp;" + oprTO.getProductCategoryName() + "&nbsp;</td></tr>"
//                                            + "<tr><td>&nbsp; Reefer: &nbsp;</td><td>&nbsp;" + oprTO.getReeferRequired() + "&nbsp;</td></tr>"
//                                            + "<tr><td>&nbsp; Total Packages (Nos): &nbsp;</td><td>&nbsp;" + oprTO.getTotalPackage() + "&nbsp;</td></tr>"
//                                            + "<tr><td>&nbsp; Total Weight (Kg): &nbsp;</td><td>&nbsp;" + oprTO.getTotalWeightage() + "&nbsp;</td></tr>"
//                                            + "<tr><td>&nbsp; Total Distance (Km): &nbsp;</td><td>&nbsp;" + oprTO.getTotalDistance() + "&nbsp;</td></tr>"
//                                            + "<tr><td> &nbsp;Transit Hours (Hr): &nbsp;</td><td>&nbsp;" + oprTO.getTotalHours() + ":" + oprTO.getTotalMinutes() + "&nbsp;</td></tr>"
//                                            + "<tr><td> &nbsp;Revenue: &nbsp;</td><td>&nbsp;" + oprTO.getFreightCharges() + "&nbsp;</td></tr>"
//                                            + "<tr><td> &nbsp;Credit Limit: &nbsp;</td><td>&nbsp;" + creditLimit + "&nbsp;</td></tr>"
//                                            + "<tr><td> &nbsp;Out Standing Amount: &nbsp;</td><td>&nbsp;" + outStanding + "&nbsp;</td></tr>"
//                                            + "<tr height='25'><td></td><td></td></tr>"
//                                            + "<tr><td colspan='2' align='center'>"
//                                            + "<a style='text-decoration: none' href='http://203.124.105.244:8089/KerryDemo/updateConsignmentCreditLimitStatus.do?consignmentOrderId=" + consignmentOrderId + "&mailId=" + to + "&creditLimit=" + creditLimit + "&outStanding=" + outStanding + "&step=" + 2 + "&customerId=" + customerId + "&approvestatus=1'>Approve</a>&nbsp;|&nbsp;"
//                                            + "<a style='text-decoration: none' href='http://203.124.105.244:8089/KerryDemo/updateConsignmentCreditLimitStatus.do?consignmentOrderId=" + consignmentOrderId + "&mailId=" + to + "&creditLimit=" + creditLimit + "&outStanding=" + outStanding + "&step=" + 2 + "&customerId=" + customerId + "&approvestatus=2'>Reject</a>"
//                                            + "</td></tr>"
//                                            + "</table></body></html>";
//                                    String content = emailFormat1;
//                                    new SendMail(smtp, emailPort, frommailid, password, subject, content, to, "", userId).start();
//                                }
//                            }
//                        }
//                    }
//                }
//            } else if (step.equals("2")) {
//                creditLimit = request.getParameter("creditLimit");
//                outStanding = request.getParameter("outStanding");
//                step = request.getParameter("step");
//                System.out.println("userId = " + userId);
//                consignmentOrderId = request.getParameter("consignmentOrderId");
//                approvalStatus = request.getParameter("approvestatus");
//                approverId = request.getParameter("mailId");
//                tripTO.setApprovalStatus(approvalStatus);
//                tripTO.setConsignmentId(consignmentOrderId);
//                tripTO.setMailId(approverId);
//                if (approvalStatus.equals("1")) {
//                    tripTO.setStatusId("5");
//                    tripTO.setApprovalStatusId("1");
//                } else if (approvalStatus.equals("2")) {
//                    tripTO.setStatusId("4");
//                    tripTO.setApprovalStatusId("2");
//                }
//                String checkConsignmentCreditLimitStatus = "";
//                String approverMailId = "";
//                String cancelMailId = "";
//                String ceoMailId = "";
//                int approval = 0;
//                int operationControllerCount = 1;
//                String[] temp = null;
//                checkConsignmentCreditLimitStatus = tripBP.checkConsignmentCreditLimitStatus(tripTO, userId);
//                if (checkConsignmentCreditLimitStatus != null) {
//                    temp = checkConsignmentCreditLimitStatus.split("~");
//                    operationControllerCount = Integer.parseInt(temp[0]);
//                    approverMailId = temp[1];
//                    cancelMailId = temp[2];
//                    ceoMailId = temp[3];
//                    approval = Integer.parseInt(temp[4]);
//                }
//                if (approval == 0 && operationControllerCount == 0) {
//                    int updateConsignmentCeoApprovalStatus = tripBP.updateConsignmentOrderApprovalStatus(tripTO, userId);
//
//                    ArrayList consignmentList = new ArrayList();
//                    OperationTO operationTO = new OperationTO();
//                    operationTO.setConsignmentOrderId(consignmentOrderId);
//                    consignmentList = operationBP.viewConsignmentList(operationTO);
//                    String to = "";
//                    String smtp = "";
//                    int emailPort = 0;
//                    String frommailid = "";
//                    String password = "";
//                    String activitycode = "";
//                    String statusText = "";
//                    String statusTextNew = "";
//                    if (tripTO.getStatusId().equals("5")) {
//                        activitycode = "CREDITLIMITCEO";
//                        statusText = "Order Approved";
//                        statusTextNew = "Approved";
//                    } else if (tripTO.getStatusId().equals("4")) {
//                        activitycode = "CREDITLIMITCEO";
//                        statusText = "Order Rejected";
//                        statusTextNew = "Rejected";
//                    }
//                    String contractId = "";
//                    ArrayList emaildetails = new ArrayList();
//                    emaildetails = tripBP.getEmailDetails(activitycode);
//                    Iterator itrMail = emaildetails.iterator();
//                    if (itrMail.hasNext()) {
//                        tripTO = new TripTO();
//                        tripTO = (TripTO) itrMail.next();
//                        smtp = tripTO.getSmtp();
//                        emailPort = Integer.parseInt(tripTO.getPort());
//                        frommailid = tripTO.getEmailId();
//                        password = tripTO.getPassword();
//                        to = tripTO.getTomailId();
//
//                    }
//                    String toEmailId = operationBP.getEmailToListForConsignment(customerId);
//
//
//                    String[] temp1 = toEmailId.split(",");
//                    to = "";
//                    int cntr = 0;
//                    for (int i = 0; i < temp1.length; i++) {
//                        if (!"-".equals(temp1[i]) && !"".equals(temp1[i])) {
//                            if (cntr == 0) {
//                                to = temp1[i];
//                            } else {
//                                to = to + "," + temp1[i];
//
//                            }
//                            cntr++;
//                        }
//                    }
//                    String confirmationCode = "CONFIRM0001";
//                    String confirmationMail = operationBP.getConfirmationMail(confirmationCode);
//                    if (confirmationMail != null) {
//                        to = to + "," + confirmationMail;
//                    }
//                    OperationTO oprTO = new OperationTO();
//                    if (consignmentList.size() > 0) {
//                        Iterator itr = consignmentList.iterator();
//                        if (itr.hasNext()) {
//                            oprTO = (OperationTO) itr.next();
//
//                            String subject = "Consignment Note " + statusTextNew + " For : " + operationTO.getCustomerName() + ". Route " + oprTO.getOrigin() + "-" + oprTO.getDestination() + " Vehicle Required On :" + operationTO.getVehicleRequiredDate() + " :" + operationTO.getVehicleRequiredHour();
//                            String emailFormat1 = "<html>"
//                                    + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
//                                    + "<tr>"
//                                    + "<th colspan='2'>Consignment Order Created With Credit limit Approval has been " + statusTextNew + "</th>"
//                                    + "</tr>"
//                                    + "<tr><td>&nbsp; Consignment Order No:&nbsp;</td><td>&nbsp; " + oprTO.getConsignmentNoteNo() + "&nbsp;</td></tr>"
//                                    + "<tr><td>&nbsp; Customer:&nbsp;</td><td>&nbsp; " + oprTO.getCustomerName() + "&nbsp;</td></tr>"
//                                    + "<tr><td>&nbsp; Vehicle Type: &nbsp;</td><td>&nbsp;" + oprTO.getVehicleTypeName() + "&nbsp;</td></tr>"
//                                    + "<tr><td>&nbsp; Vehicle Required Date:&nbsp; </td><td>&nbsp;" + oprTO.getVehicleRequiredDate() + " Hour:" + oprTO.getVehicleRequiredHour() + "&nbsp;</td></tr>"
//                                    + "<tr><td> &nbsp;Route: &nbsp;</td><td>&nbsp;" + oprTO.getOrigin() + "-" + oprTO.getDestination() + "&nbsp;</td></tr>"
//                                    + "<tr><td>&nbsp; Product Category: &nbsp;</td><td>&nbsp;" + oprTO.getProductCategoryName() + "&nbsp;</td></tr>"
//                                    + "<tr><td>&nbsp; Reefer: &nbsp;</td><td>&nbsp;" + oprTO.getReeferRequired() + "&nbsp;</td></tr>"
//                                    + "<tr><td>&nbsp; Total Packages (Nos): &nbsp;</td><td>&nbsp;" + oprTO.getTotalPackage() + "&nbsp;</td></tr>"
//                                    + "<tr><td>&nbsp; Total Weight (Kg): &nbsp;</td><td>&nbsp;" + oprTO.getTotalWeightage() + "&nbsp;</td></tr>"
//                                    + "<tr><td>&nbsp; Total Distance (Km): &nbsp;</td><td>&nbsp;" + oprTO.getTotalDistance() + "&nbsp;</td></tr>"
//                                    + "<tr><td> &nbsp;Transit Hours (Hr): &nbsp;</td><td>&nbsp;" + oprTO.getTotalHours() + ":" + oprTO.getTotalMinutes() + "&nbsp;</td></tr>"
//                                    + "<tr><td> &nbsp;Revenue: &nbsp;</td><td>&nbsp;" + oprTO.getFreightCharges() + "&nbsp;</td></tr>"
//                                    + "<tr><td> &nbsp;Credit Limit: &nbsp;</td><td>&nbsp;" + creditLimit + "&nbsp;</td></tr>"
//                                    + "<tr><td> &nbsp;Out Standing Amount: &nbsp;</td><td>&nbsp;" + outStanding + "&nbsp;</td></tr>"
//                                    + "<tr><td> &nbsp;Order Sttaus: &nbsp;</td><td>&nbsp;" + statusText + "&nbsp;</td></tr>"
//                                    + "<tr height='25'><td></td><td></td></tr>"
//                                    + "</table></body></html>";
//                            String content = emailFormat1;
//                            new SendMail(smtp, emailPort, frommailid, password, subject, content, to, "", userId).start();
//                        }
//                    }
//
//                    checkflag = true;
//                } else {
//                    checkflag = false;
//                }
//            }
//            System.out.println("checkflag = " + checkflag);
//            if (checkflag) {
//                path = "BrattleFoods/response.html";
//            } else {
//                path = "BrattleFoods/responsed.html";
//            }
//
//            //}
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }
    public ModelAndView updateConsignmentCreditLimitStatus(HttpServletRequest request, HttpServletResponse response, TripCommand command) {

        HttpSession session = request.getSession();
        String path = "";

        String menuPath = " Operation  >>  Approve/Reject Consignment Credit Limit Status ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = " Approve/Reject Consignment Credit Limit Status ";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            boolean checkflag = false;
            int updateStatus = 0;
            int userId = 0;
            if (session.getAttribute("userId") != null && !"".equals(session.getAttribute("userId"))) {
                userId = (Integer) session.getAttribute("userId");
            }
            System.out.println("userId = " + userId);
            String consignmentOrderId = "";
            String approvalStatus = "";
            String approverId = "";
            String creditLimit = request.getParameter("creditLimit");
            String outStanding = request.getParameter("outStanding");
            String customerId = request.getParameter("customerId");
            String step = request.getParameter("step");
            System.out.println("userId = " + userId);

            consignmentOrderId = request.getParameter("consignmentOrderId");
            approvalStatus = request.getParameter("approvestatus");
            approverId = request.getParameter("mailId");
            tripTO.setApprovalStatus(approvalStatus);
            tripTO.setConsignmentId(consignmentOrderId);
            tripTO.setMailId(approverId);

            if (step.equals("1")) {
                creditLimit = request.getParameter("creditLimit");
                outStanding = request.getParameter("outStanding");
                step = request.getParameter("step");
                System.out.println("userId = " + userId);
                consignmentOrderId = request.getParameter("consignmentOrderId");
                approvalStatus = request.getParameter("approvestatus");
                approverId = request.getParameter("mailId");
                tripTO.setApprovalStatus(approvalStatus);
                tripTO.setConsignmentId(consignmentOrderId);
                tripTO.setMailId(approverId);
                if (approvalStatus.equals("1")) {
                    tripTO.setStatusId("5");
                    tripTO.setApprovalStatusId("1");
                } else if (approvalStatus.equals("2")) {
                    tripTO.setStatusId("4");
                    tripTO.setApprovalStatusId("2");
                }
                String checkConsignmentCreditLimitStatus = "";
                String approverMailId = "";
                String cancelMailId = "";
                String ceoMailId = "";
                int approval = 0;
                int operationControllerCount = 1;
                String[] temp = null;
//                checkConsignmentCreditLimitStatus = tripBP.checkConsignmentCreditLimitStatus(tripTO, userId);
//                if (checkConsignmentCreditLimitStatus != null) {
//                    temp = checkConsignmentCreditLimitStatus.split("~");
//                    operationControllerCount = Integer.parseInt(temp[0]);
//                    approverMailId = temp[1];
//                    cancelMailId = temp[2];
//                    ceoMailId = temp[3];
//                    approval = Integer.parseInt(temp[4]);
//                }
                int updateConsignmentOrderCreditLimitStatus = tripBP.updateConsignmentOrderCreditLimitStatus(tripTO, userId);
                int updateConsignmentCeoApprovalStatus = tripBP.updateConsignmentOrderApprovalStatus(tripTO, userId);

                ArrayList consignmentList = new ArrayList();
                OperationTO operationTO = new OperationTO();
                operationTO.setConsignmentOrderId(consignmentOrderId);
                consignmentList = operationBP.viewConsignmentList(operationTO);
                String to = "";
                String smtp = "";
                int emailPort = 0;
                String frommailid = "";
                String password = "";
                String activitycode = "";
                String statusText = "";
                String statusTextNew = "";
                if (tripTO.getStatusId().equals("5")) {
                    activitycode = "CNOTEAPK1";
                    statusText = "Order Approved";
                    statusTextNew = "Approved";
                } else if (tripTO.getStatusId().equals("4")) {
                    activitycode = "CNOTEAPK1";
                    statusText = "Order Rejected";
                    statusTextNew = "Rejected";
                }
                String contractId = "";
                ArrayList emaildetails = new ArrayList();
                emaildetails = tripBP.getEmailDetails(activitycode);
                Iterator itrMail = emaildetails.iterator();
                if (itrMail.hasNext()) {
                    tripTO = new TripTO();
                    tripTO = (TripTO) itrMail.next();
                    smtp = tripTO.getSmtp();
                    emailPort = Integer.parseInt(tripTO.getPort());
                    frommailid = tripTO.getEmailId();
                    password = tripTO.getPassword();
                    to = "";

                }
                String toEmailId = operationBP.getEmailToListForConsignment(customerId);

                String[] temp1 = toEmailId.split(",");
                to = "";
                int cntr = 0;
                for (int i = 0; i < temp1.length; i++) {
                    if (!"-".equals(temp1[i]) && !"".equals(temp1[i])) {
                        if (cntr == 0) {
                            to = temp1[i];
                        } else {
                            to = to + "," + temp1[i];

                        }
                        cntr++;
                    }
                }
                String confirmationCode = "CONFIRM0001";
                String confirmationMail = operationBP.getConfirmationMail(confirmationCode);
                if (confirmationMail != null) {
                    to = to + "," + confirmationMail;
                }
                OperationTO oprTO = new OperationTO();
                if (consignmentList.size() > 0) {
                    Iterator itr = consignmentList.iterator();
                    if (itr.hasNext()) {
                        oprTO = (OperationTO) itr.next();

                        String subject = "Consignment Note " + statusTextNew + " For : " + operationTO.getCustomerName() + ". Route " + oprTO.getOrigin() + "-" + oprTO.getDestination() + " Vehicle Required On :" + operationTO.getVehicleRequiredDate() + " :" + operationTO.getVehicleRequiredHour();
                        String emailFormat1 = "<html>"
                                + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                                + "<tr>"
                                + "<th colspan='2'>Consignment Order Created With Credit limit Approval has been " + statusTextNew + "</th>"
                                + "</tr>"
                                + "<tr><td>&nbsp; Consignment Order No:&nbsp;</td><td>&nbsp; " + oprTO.getConsignmentNoteNo() + "&nbsp;</td></tr>"
                                + "<tr><td>&nbsp; Customer:&nbsp;</td><td>&nbsp; " + oprTO.getCustomerName() + "&nbsp;</td></tr>"
                                + "<tr><td>&nbsp; Vehicle Type: &nbsp;</td><td>&nbsp;" + oprTO.getVehicleTypeName() + "&nbsp;</td></tr>"
                                + "<tr><td>&nbsp; Vehicle Required Date:&nbsp; </td><td>&nbsp;" + oprTO.getVehicleRequiredDate() + " Hour:" + oprTO.getVehicleRequiredHour() + "&nbsp;</td></tr>"
                                + "<tr><td> &nbsp;Route: &nbsp;</td><td>&nbsp;" + oprTO.getOrigin() + "-" + oprTO.getDestination() + "&nbsp;</td></tr>"
                                + "<tr><td>&nbsp; Product Category: &nbsp;</td><td>&nbsp;" + oprTO.getProductCategoryName() + "&nbsp;</td></tr>"
                                + "<tr><td>&nbsp; Reefer: &nbsp;</td><td>&nbsp;" + oprTO.getReeferRequired() + "&nbsp;</td></tr>"
                                + "<tr><td>&nbsp; Total Packages (Nos): &nbsp;</td><td>&nbsp;" + oprTO.getTotalPackage() + "&nbsp;</td></tr>"
                                + "<tr><td>&nbsp; Total Weight (Kg): &nbsp;</td><td>&nbsp;" + oprTO.getTotalWeightage() + "&nbsp;</td></tr>"
                                + "<tr><td>&nbsp; Total Distance (Km): &nbsp;</td><td>&nbsp;" + oprTO.getTotalDistance() + "&nbsp;</td></tr>"
                                + "<tr><td> &nbsp;Transit Hours (Hr): &nbsp;</td><td>&nbsp;" + oprTO.getTotalHours() + ":" + oprTO.getTotalMinutes() + "&nbsp;</td></tr>"
                                + "<tr><td> &nbsp;Revenue: &nbsp;</td><td>&nbsp;" + oprTO.getFreightCharges() + "&nbsp;</td></tr>"
                                + "<tr><td> &nbsp;Credit Limit: &nbsp;</td><td>&nbsp;" + creditLimit + "&nbsp;</td></tr>"
                                + "<tr><td> &nbsp;Out Standing Amount: &nbsp;</td><td>&nbsp;" + outStanding + "&nbsp;</td></tr>"
                                + "<tr><td> &nbsp;Order Sttaus: &nbsp;</td><td>&nbsp;" + statusText + "&nbsp;</td></tr>"
                                + "<tr height='25'><td></td><td></td></tr>"
                                + "</table></body></html>";
                        String content = emailFormat1;
                        new SendMail(smtp, emailPort, frommailid, password, subject, content, to, "", userId).start();
                    }
                }

                checkflag = true;

            }
            System.out.println("checkflag = " + checkflag);
            if (checkflag) {
                path = "BrattleFoods/response.html";
            } else {
                path = "BrattleFoods/responsed.html";
            }

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView secondaryCustomerApprovalMailId(HttpServletRequest request, HttpServletResponse reponse, TripCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();

        String path = "";
        String menuPath = "";
        menuPath = "Operation  >>  Secondary Customer Approval Mail Id";
        String pageTitle = "Customer Approval Mail Id";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            ArrayList secondaryCustomerApprovalList = new ArrayList();
            secondaryCustomerApprovalList = tripBP.getSecondaryCustomerApprovalList();
            if (secondaryCustomerApprovalList.size() > 0) {
                request.setAttribute("secondaryCustomerApprovalList", secondaryCustomerApprovalList);
            }
            path = "content/trip/secondaryCustomerApprovalList.jsp";

//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to view role details --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);
    }

    public ModelAndView saveSecondaryCustomerApprovalMail(HttpServletRequest request, HttpServletResponse reponse, TripCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();

        String path = "";
        String menuPath = "";
        menuPath = "Operation  >>  Secondary Customer Approval Mail Id";
        String pageTitle = "Customer Approval Mail Id";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        TripTO tripTO = new TripTO();
        int update = 0;
        try {
            String[] customerId = request.getParameterValues("customerId");
            String[] approvalMailId = request.getParameterValues("approvalMailId");
            for (int i = 0; i < customerId.length; i++) {
                tripTO.setCustomerId(customerId[i]);
                tripTO.setEmailId(approvalMailId[i]);
                update += tripBP.updateSecondaryCustomerMail(tripTO);
            }

            ArrayList secondaryCustomerApprovalList = new ArrayList();
            secondaryCustomerApprovalList = tripBP.getSecondaryCustomerApprovalList();
            if (secondaryCustomerApprovalList.size() > 0) {
                request.setAttribute("secondaryCustomerApprovalList", secondaryCustomerApprovalList);
            }
            path = "content/trip/secondaryCustomerApprovalList.jsp";

//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to view role details --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);
    }

    public ModelAndView changeEmptyTripRouteDetails(HttpServletRequest request, HttpServletResponse reponse, TripCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();

        String path = "";
        String menuPath = "";
        tripCommand = command;
        menuPath = "Operation  >>  Change Empty Trip Route Details";
        String pageTitle = "Change Empty Trip Route Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        TripTO tripTO = new TripTO();
        int update = 0;
        try {

            if (tripCommand.getTripSheetId() != null && !"".equals(tripCommand.getTripSheetId())) {
                tripTO.setTripId(tripCommand.getTripSheetId());
                request.setAttribute("tripId", tripCommand.getTripSheetId());
            }
            ArrayList emptyTripDetails = new ArrayList();
            emptyTripDetails = tripBP.getEmptyTripDetails(tripTO);
            if (emptyTripDetails.size() > 0) {
                request.setAttribute("emptyTripDetails", emptyTripDetails);
            }
            Iterator itr = emptyTripDetails.iterator();
            TripTO tp = new TripTO();
            if (itr.hasNext()) {
                tp = new TripTO();
                tp = (TripTO) itr.next();
                request.setAttribute("tripCode", tp.getTripCode());
                request.setAttribute("tripStartDate", tp.getTripStartDate());
                request.setAttribute("tripStartTime", tp.getTripStartTime());
                request.setAttribute("vehicleType", tp.getVehicleType());
                request.setAttribute("totalKm", tp.getTotalKM());
                request.setAttribute("estimatedExpense", tp.getEstimatedExpense());
                request.setAttribute("routeInfo", tp.getRouteInfo());
                request.setAttribute("estimatedTransitDays", tp.getEstimatedTransitDays());
                request.setAttribute("estimatedTransitHours", tp.getEstimatedTransitHours());
                request.setAttribute("estimatedAdvancePerDay", tp.getEstimatedAdvancePerDay());
                request.setAttribute("vehicleTypeId", tp.getVehicleTypeId());
            }
            path = "content/trip/changeEmptyTripRoute.jsp";

//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to view role details --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);
    }

    public ModelAndView updateEmptyTripRouteDetails(HttpServletRequest request, HttpServletResponse reponse, TripCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();

        String path = "";
        String menuPath = "";
        tripCommand = command;
        menuPath = "Operation  >>  Change Empty Trip Route Details";
        String pageTitle = "Change Empty Trip Route Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        TripTO tripTO = new TripTO();
        OperationTO operationTO = new OperationTO();
        int update = 0;
        try {

            if (tripCommand.getTripSheetId() != null && !"".equals(tripCommand.getTripSheetId())) {
                tripTO.setTripId(tripCommand.getTripSheetId());
                request.setAttribute("tripId", tripCommand.getTripSheetId());
            }
            if (tripCommand.getOrigin() != null && !"".equals(tripCommand.getOrigin())) {
                tripTO.setOrigin(tripCommand.getOrigin());
                operationTO.setOrigin(tripCommand.getOrigin());
            }
            if (tripCommand.getDestination() != null && !"".equals(tripCommand.getDestination())) {
                tripTO.setDestination(tripCommand.getDestination());
                operationTO.setDestination(tripCommand.getDestination());
            }
            if (tripCommand.getCityFrom() != null && !"".equals(tripCommand.getCityFrom())) {
                tripTO.setCityFrom(tripCommand.getCityFrom());
            }
            if (tripCommand.getCityTo() != null && !"".equals(tripCommand.getCityTo())) {
                tripTO.setCityTo(tripCommand.getCityTo());
            }
            if (tripCommand.getTotalKm() != null && !"".equals(tripCommand.getTotalKm())) {
                tripTO.setTotalKm(tripCommand.getTotalKm());
            }
            if (tripCommand.getTotalHours() != null && !"".equals(tripCommand.getTotalHours())) {
                tripTO.setTotalHours(tripCommand.getTotalHours());
            }
            if (tripCommand.getTotalMinutes() != null && !"".equals(tripCommand.getTotalMinutes())) {
                tripTO.setTotalMinutes(tripCommand.getTotalMinutes());
            }
            if (tripCommand.getExpense() != null && !"".equals(tripCommand.getExpense())) {
                tripTO.setExpense(tripCommand.getExpense());
            }
            if (tripCommand.getEmptyTripRemarks() != null && !"".equals(tripCommand.getEmptyTripRemarks())) {
                tripTO.setEmptyTripRemarks(tripCommand.getEmptyTripRemarks());
            }
            String routeId = operationBP.getRouteId(operationTO);
            System.out.println("routeId = " + routeId);
            if (routeId != null) {
                tripTO.setRouteId(routeId);
            } else {
                tripTO.setRouteId("0");
            }
            int updateEmptyTripRoute = 0;
            updateEmptyTripRoute = tripBP.updateEmptyTripRoute(tripTO);
            ArrayList emptyTripDetails = new ArrayList();
            emptyTripDetails = tripBP.getEmptyTripDetails(tripTO);
            if (emptyTripDetails.size() > 0) {
                request.setAttribute("emptyTripDetails", emptyTripDetails);
            }
            Iterator itr = emptyTripDetails.iterator();
            TripTO tp = new TripTO();
            if (itr.hasNext()) {
                tp = new TripTO();
                tp = (TripTO) itr.next();
                request.setAttribute("tripCode", tp.getTripCode());
                request.setAttribute("tripStartDate", tp.getTripStartDate());
                request.setAttribute("tripStartTime", tp.getTripStartTime());
                request.setAttribute("vehicleType", tp.getVehicleType());
                request.setAttribute("totalKm", tp.getTotalKM());
                request.setAttribute("estimatedExpense", tp.getEstimatedExpense());
                request.setAttribute("routeInfo", tp.getRouteInfo());
                request.setAttribute("estimatedTransitDays", tp.getEstimatedTransitDays());
                request.setAttribute("estimatedTransitHours", tp.getEstimatedTransitHours());
                request.setAttribute("estimatedAdvancePerDay", tp.getEstimatedAdvancePerDay());
                request.setAttribute("vehicleTypeId", tp.getVehicleTypeId());
            }
            if (updateEmptyTripRoute > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Empty Trip Route Has Been Changed Sucessfully");
            }
            path = "content/trip/changeEmptyTripRoute.jsp";

//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to view role details --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);
    }

    public ModelAndView handleDeleteTripSheet(HttpServletRequest request, HttpServletResponse reponse, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Operation  >>  Update Trip Details";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            String tripId = request.getParameter("tripId");
            request.setAttribute("tripId", tripId);
            String tripStatusId = request.getParameter("tripStatusId");
            request.setAttribute("tripStatusId", tripStatusId);
            System.out.println("parameter1" + tripId);
            System.out.println("parameter2" + tripStatusId);
            tripTO.setTripId(tripId);
            tripTO.setTripSheetId(tripId);

            String tripSheetId = "";
            if (tripCommand != null) {
                tripSheetId = tripCommand.getTripSheetId();
            }

            if (tripSheetId != null && !tripSheetId.equals("")) {
                tripSheetId = request.getParameter("tripId");
            }

            String tripDetail = tripBP.getTripDetails(tripSheetId);
            ArrayList tripDetails = new ArrayList();
            tripDetails = tripBP.getTripDetails(tripTO);

            TripTO tripTO1 = new TripTO();
            String vehicleTypeId = "";
            ArrayList totalExpenseDetails = new ArrayList();
            totalExpenseDetails = tripBP.getTotalExpenseDetails(tripTO);
            Iterator itr1 = totalExpenseDetails.iterator();
            while (itr1.hasNext()) {
                tripTO1 = (TripTO) itr1.next();
                request.setAttribute("tollAmount", tripTO1.getTollAmount());
                request.setAttribute("driverBatta", tripTO1.getDriverBatta());
                request.setAttribute("driverIncentive", tripTO1.getDriverIncentive());
                request.setAttribute("fuelPrice", tripTO1.getFuelPrice());
                request.setAttribute("milleage", tripTO1.getMilleage());
                request.setAttribute("reeferConsumption", tripTO1.getReeferConsumption());
                request.setAttribute("vehicleTypeId", tripTO1.getVehicleTypeId());
                vehicleTypeId = tripTO1.getVehicleTypeId();
            }
            String customerName = "";
            TripTO tpTO = new TripTO();
            ArrayList tripSheetDetails = new ArrayList();
            tripSheetDetails = tripBP.getEndTripSheetDetails(tripTO);
            Iterator itr = tripSheetDetails.iterator();
            while (itr.hasNext()) {
                tpTO = (TripTO) itr.next();
                request.setAttribute("tripSheetId", tpTO.getTripSheetId());
                request.setAttribute("cNotes", tpTO.getcNotes());
                request.setAttribute("totalWeight", tpTO.getTotalWeight());
                request.setAttribute("vehicleNo", tpTO.getVehicleNo());
                request.setAttribute("tripCode", tpTO.getTripCode());
                request.setAttribute("startDate", tpTO.getTripScheduleDate());
                request.setAttribute("startTime", tpTO.getTripScheduleTime());
                String temp1[] = null;
                if (!tpTO.getTripScheduleTime().equals("") && tpTO.getTripScheduleTime() != null) {
                    temp1 = tpTO.getTripScheduleTime().split(":");
                    request.setAttribute("tripStartHour", temp1[0]);
                    request.setAttribute("tripStartMinute", temp1[1]);
                }
                request.setAttribute("estimatedExpense", tpTO.getOrderExpense());
                request.setAttribute("rcmExpense", tpTO.getRcmExpense());
                request.setAttribute("origin", tpTO.getOrigin());
                request.setAttribute("destination", tpTO.getDestination());
                request.setAttribute("tripEndDate", tpTO.getTripEndDate());
                request.setAttribute("tripEndTime", tpTO.getTripEndTime());
                System.out.println("tpTO.getTripEndTime() = " + tpTO.getTripEndTime());
                String temp2[] = null;
                if (tpTO.getTripEndTime() != null) {
                    temp2 = tpTO.getTripEndTime().split(":");
                    request.setAttribute("tripEndHour", temp2[0]);
                    request.setAttribute("tripEndMinute", temp2[1]);
                }
                String temp3[] = null;
                if (tpTO.getTripWfuTime() != null) {
                    temp3 = tpTO.getTripWfuTime().split(":");
                    request.setAttribute("tripWfuHour", temp3[0]);
                    request.setAttribute("tripWfuMinute", temp3[1]);
                }
                request.setAttribute("startKm", tpTO.getStartKm());
                request.setAttribute("startHm", tpTO.getStartHm());
                request.setAttribute("endKm", tpTO.getEndKm());
                request.setAttribute("endHm", tpTO.getEndHm());
                request.setAttribute("runKm", tpTO.getRunKm());
                request.setAttribute("runHm", tpTO.getRunHm());
                request.setAttribute("totaldays", tpTO.getTotalDays());
                request.setAttribute("totalHours", tpTO.getDurationHours());
                request.setAttribute("driverCount", tpTO.getDriverCount());
                request.setAttribute("extraExpenseValue", tpTO.getExtraExpenseValue());
                customerName = tpTO.getCustomerName();
                String[] temp = null;
                if (!"".equals(tpTO.getDriverName()) && tpTO.getDriverName().contains(",")) {
                    temp = tpTO.getDriverName().split(",");
                    request.setAttribute("driverName1", temp[0]);
                    request.setAttribute("driverName2", temp[1]);
                } else {
                    request.setAttribute("driverName1", tpTO.getDriverName());
                }
                request.setAttribute("driverName", tpTO.getDriverName());
            }
            request.setAttribute("tripSheetDetails", tripSheetDetails);
            String gpsKm = tripBP.getGpsKm(tripSheetId);
            if (gpsKm != null) {
                String[] temp = gpsKm.split("-");
                String gpsKmVal = "0";
                if (!"0".equals(temp[0])) {
                    gpsKmVal = temp[0];
                }
                request.setAttribute("gpsKm", "" + gpsKmVal);
                //use above calculations
                request.setAttribute("gpsKm", gpsKmVal);
                request.setAttribute("gpsHm", temp[1]);
            } else {
                request.setAttribute("gpsKm", 0);
                request.setAttribute("gpsHm", 0);

            }

            //Throttle Starts As On 12-12-2013
            String miscValue = tripBP.getMiscValue(vehicleTypeId);
            request.setAttribute("miscValue", miscValue);

            String[] temp1 = null;
            if (tripDetail != null && !"".equals(tripDetail)) {
                temp1 = tripDetail.split("~");
                request.setAttribute("customerName", temp1[0]);
                request.setAttribute("routeName", temp1[1]);
                request.setAttribute("tripStartDate", temp1[2]);
                request.setAttribute("tripStartTime", temp1[3]);
            }
            String tripInformation = tripBP.getTripDetails(tripSheetId);
            String driverCount = tripBP.getDriverCount(tripSheetId, tripTO);
            String tripAdvance = tripBP.getTripAdvance(tripSheetId, tripTO);
            String tripExpense = tripBP.getTripExpense(tripSheetId, tripTO);
            request.setAttribute("tripAdvance", tripAdvance);
            request.setAttribute("tripExpense", tripExpense);

            String[] temp = null;
            if (tripInformation != null && !"".equals(tripInformation)) {
                temp = tripInformation.split("~");
                request.setAttribute("totalRunKm", temp[0]);
                request.setAttribute("transitDays", temp[1]);
                request.setAttribute("tripRemarks", temp[2]);
            }
            float advance = 0.0f;
            float expense = 0.0f;
            advance = Float.parseFloat(tripAdvance);
            expense = Float.parseFloat(tripExpense);
            float amountDifference = 0.0f;
            if (advance > expense) {
                amountDifference = advance - expense;
            } else {
                amountDifference = expense - advance;
            }
            request.setAttribute("driverCount", driverCount);
            request.setAttribute("tripAdvance", tripAdvance);
            request.setAttribute("tripExpense", tripExpense);
            request.setAttribute("amountDifference", amountDifference);
            ArrayList tripAdvanceDetails = new ArrayList();
            tripAdvanceDetails = tripBP.getTripAdvanceDetails(request.getParameter("tripId"));

            request.setAttribute("tripAdvanceDetails", tripAdvanceDetails);
            ArrayList tripFuelDetails = new ArrayList();
            tripFuelDetails = tripBP.getTripFuelDetails(request.getParameter("tripId"));
            request.setAttribute("tripFuelDetails", tripFuelDetails);
            ArrayList tripExpenseDetails = new ArrayList();
            tripExpenseDetails = tripBP.getTripExpenseDetails(request.getParameter("tripId"));
            request.setAttribute("tripExpenseDetails", tripExpenseDetails);
            ArrayList tripUnPackDetails = new ArrayList();
            tripUnPackDetails = tripBP.getTripUnPackDetails(tripTO);
            request.setAttribute("tripUnPackDetails", tripUnPackDetails);
            ArrayList viewPODDetails = new ArrayList();
            viewPODDetails = tripBP.getPODDetails(tripTO);
            if (viewPODDetails.size() > 0) {
                request.setAttribute("viewPODDetails", viewPODDetails);
            }

            ArrayList expiryDateDetails = new ArrayList();
            expiryDateDetails = tripBP.getExpiryDateDetails(tripTO);
            request.setAttribute("expiryDateDetails", expiryDateDetails);
            ArrayList statusDetails = new ArrayList();
            statusDetails = tripBP.getTripStausDetails(tripTO);
            if (statusDetails.size() > 0) {
                request.setAttribute("statusDetails", statusDetails);
            }
            ArrayList settlementDetails = new ArrayList();
            settlementDetails = tripBP.getDriverSettlementDetails(tripTO);
            if (settlementDetails.size() > 0) {
                request.setAttribute("settlementDetails", settlementDetails);
            }

            ArrayList tripDetailsNew = new ArrayList();
            tripDetailsNew = tripBP.getTripDetails(tripTO);
            ArrayList tripPointDetails = new ArrayList();
            ArrayList tripPreStartDetails = new ArrayList();
            ArrayList tripStartDetails = new ArrayList();
            ArrayList tripEndDetails = new ArrayList();
            tripPointDetails = tripBP.getTripPointDetails(tripTO);
            tripPreStartDetails = tripBP.getPreStartTripDetails(tripTO);
            tripStartDetails = tripBP.getStartedTripDetails(tripTO);
            tripEndDetails = tripBP.getEndTripDetails(tripTO);

            TripTO tTO = new TripTO();
            ArrayList bpclTransactionHistory = new ArrayList();
            double bpclAmount = 0.0f;
            bpclTransactionHistory = tripBP.getBPCLTransactionHistory(tripTO);
            if (bpclTransactionHistory.size() > 0) {
                request.setAttribute("bpclTransactionHistory", bpclTransactionHistory);
                Iterator itr3 = bpclTransactionHistory.iterator();
                while (itr3.hasNext()) {
                    tTO = (TripTO) itr3.next();
                    bpclAmount += (Double) Double.parseDouble(tTO.getAmount());
                }

            } else {
                bpclAmount = 0;
            }
            request.setAttribute("bpclAmount", bpclAmount);

            ArrayList tripAdvanceDetailsStatus = new ArrayList();
            tripAdvanceDetailsStatus = tripBP.getTripAdvanceDetailsStatus(request.getParameter("tripId"));

            request.setAttribute("tripAdvanceDetailsStatus", tripAdvanceDetailsStatus);

            request.setAttribute("tripDetails", tripDetails);
            request.setAttribute("tripPointDetails", tripPointDetails);
            request.setAttribute("tripPreStartDetails", tripPreStartDetails);
            request.setAttribute("tripStartDetails", tripStartDetails);
            request.setAttribute("tripEndDetails", tripEndDetails);
            request.setAttribute("tripDetails", tripDetailsNew);

            ArrayList vehicleChangeAdvanceDetails = new ArrayList();
            vehicleChangeAdvanceDetails = tripBP.getVehicleChangeTripAdvanceDetails(request.getParameter("tripId"));
            request.setAttribute("vehicleChangeAdvanceDetails", vehicleChangeAdvanceDetails);
            request.setAttribute("vehicleChangeAdvanceDetailsSize", vehicleChangeAdvanceDetails.size());

            //getVehicleRegNos
            String roleId = "" + (Integer) session.getAttribute("RoleId");
            String companyId = (String) session.getAttribute("companyId");
            String tripType = request.getParameter("tripType");
            tripTO.setRoleId(roleId);
            tripTO.setCompanyId(companyId);
            tripTO.setCustomerName(customerName);
            tripTO.setVehicleNo("");
            tripTO.setVehicleId("");
            tripTO.setTripType(tripType);
            ArrayList vehicleNos = tripBP.getVehicleRegNos(tripTO);
            System.out.println("vehicleNos.size() = " + vehicleNos.size());
            request.setAttribute("vehicleNos", vehicleNos);
            String vehicleCurrentStatus = "";
            vehicleCurrentStatus = tripBP.getVehicleCurrentStatus(tripId);
            String temp4[] = null;
            if (vehicleCurrentStatus != null) {
                temp4 = vehicleCurrentStatus.split("~");
                request.setAttribute("vehicleCurrentStatus", temp4[0]);
                request.setAttribute("vehicleCurrentTripCode", temp4[1]);
            } else {
                request.setAttribute("vehicleCurrentStatus", "0");
                request.setAttribute("vehicleCurrentTripCode", "0");
            }

            path = "content/trip/deleteTripSheetDetails.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView updateDeleteTripSheet(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ModelAndView mv = null;
        TripTO tripTO = new TripTO();
        try {
            String tripId = request.getParameter("tripId");
            String updatedTripStatus = request.getParameter("updatedTripStatus");
            String tripStatusId = request.getParameter("tripStatusId");
            int userId = (Integer) session.getAttribute("userId");
            tripTO.setTripId(tripId);
            tripTO.setUserId(userId);
            System.out.println("trip status value is " + updatedTripStatus);
            int status = 0;
//        status =
            String startDate = "";
            if (tripStatusId.equals("10")) {
            } else if (tripStatusId.equals("18")) {
            } else if (tripStatusId.equals("12")) {
            }
            tripTO.setStatusId(updatedTripStatus);
            if (updatedTripStatus.equals("Delete")) {
                status = tripBP.updateDeleteTrip(tripTO);
                if (status != 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Trip Deleted  Successfully....");
                }
            } else if (updatedTripStatus.equals("8")) {
                status = tripBP.updateDeleteTrip(tripTO);
                if (status != 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Trip Freezed  Successfully....");
                }
            } else if (updatedTripStatus.equals("10")) {
                status = tripBP.updateDeleteTrip(tripTO);
                if (status != 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Trip Change Status as In-Progress Successfully....");
                }
            } else if (updatedTripStatus.equals("18")) {
                status = tripBP.updateDeleteTrip(tripTO);
                if (status != 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Trip Change Status as Wfu Successfully....");
                }

            }

            /*
             String tripStartDate = request.getParameter("startdate");
             String tripStartHour = request.getParameter("tripStartHour");
             String tripStartMinutes = request.getParameter("tripStartMinute");

             tripTO.setStartDate(tripStartDate);
             tripTO.setStartTime(tripStartHour + ":" + tripStartMinutes + ":00");


             if (tripCommand.getEndDate() != null && !tripCommand.getEndDate().equals("")) {
             tripTO.setEndDate(tripCommand.getEndDate());
             System.out.println("tripCommand.getEndDate()" + tripCommand.getEndDate());
             }
             if (tripCommand.getTripEndHour() != null && !tripCommand.getTripEndHour().equals("")) {
             tripTO.setTripEndHour(tripCommand.getTripEndHour());
             System.out.println("tripCommand.getTripEndHour()" + tripCommand.getTripEndHour());
             }
             if (tripCommand.getTripEndMinute() != null && !tripCommand.getTripEndMinute().equals("")) {
             tripTO.setTripEndMinute(tripCommand.getTripEndMinute());
             System.out.println("tripCommand.getTripEndMinute()" + tripCommand.getTripEndMinute());
             }
             if (request.getParameter("totalDays") != null && !request.getParameter("totalDays").equals("")) {
             tripTO.setTotalDays(request.getParameter("totalDays1"));
             System.out.println("tripCommand.getTotalDays()" + request.getParameter("totalDays1"));
             }
             if (tripCommand.getTripTransitHour() != null && !tripCommand.getTripTransitHour().equals("")) {
             tripTO.setTripTransitHours(tripCommand.getTripTransitHour());
             System.out.println("tripCommand.getTripTransitHour()" + tripCommand.getTripTransitHour());
             }
             int insertStatus = 0;
             if (tripStatusId.equals("10")) {
             int insertStart = tripBP.updateTripStartDetails(tripTO, userId);
             System.out.println("insert status" + insertStart);
             if (insertStart != 0) {
             request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Trip start details updated Successfully....");
             }
             } else {

             insertStatus = tripBP.updateTripEnd(tripTO, userId);
             System.out.println("insert status" + insertStatus);
             if (insertStatus != 0) {
             request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Trip End details updated Successfully....");
             }
             }
             *
             */
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        //path = "content/trip/viewTripSheet.jsp";
        mv = handleDeleteTripSheet(request, response, command);
        return mv;

    }

    public ModelAndView updateTripAdvance(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View start Trip Sheet ";
        int insertStatus = 0;
        ModelAndView mv = null;
        System.out.println("controler is working");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            // if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
            //     tripTO.setTripSheetId(tripCommand.getTripSheetId());
            // }
            String tripId = request.getParameter("tripId");
            tripTO.setTripId(tripId);
            String reqAmount[] = request.getParameterValues("reqamount");
            String changeAmount[] = request.getParameterValues("changeAmount");
            String tripAdvanceId[] = request.getParameterValues("tripAdvanceId");

            if (tripAdvanceId != null && tripAdvanceId.length > 0) {
                for (int i = 0; i < tripAdvanceId.length; i++) {
                    System.out.println("Float.parseFloat(reqAmount[i]) = " + Float.parseFloat(reqAmount[i]));
                    System.out.println("Float.parseFloat(changeAmount[i]) = " + Float.parseFloat(changeAmount[i]));
                    if (Float.parseFloat(reqAmount[i]) != Float.parseFloat(changeAmount[i])) {
                        tripTO.setAdvanceAmount(changeAmount[i]);
                        tripTO.setTripAdvanceId(tripAdvanceId[i]);
                        tripTO.setAdvanceAmount(reqAmount[i]);
                        insertStatus = tripBP.updateTripAdvance(tripTO, userId);

                    }
                }
            }

            System.out.println("insert staus is::" + insertStatus);

            if (insertStatus != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Trip Advance updated Successfully....");
            }

            //path = "content/RenderService/viewTripSheet.jsp";
            mv = handleDeleteTripSheet(request, response, command);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView viewVehicleTripExpense(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View Vehicle List For Trip Closure ";
        int insertStatus = 0;
        ModelAndView mv = null;
        System.out.println("controler is working");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            String tripId = request.getParameter("tripSheetId");
            String tripType = request.getParameter("tripType");
            String statusId = request.getParameter("statusId");
            String admin = request.getParameter("admin");
            request.setAttribute("tripType", tripType);
            request.setAttribute("statusId", statusId);
            request.setAttribute("admin", admin);
            request.setAttribute("tripId", tripId);
            tripTO.setTripId(tripId);
            ArrayList tripClosureVehicleList = new ArrayList();
            tripClosureVehicleList = tripBP.getTripClosureVehicleList(tripTO);
            request.setAttribute("tripClosureVehicleList", tripClosureVehicleList);
            path = "content/trip/viewTripClosureVehicleList.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewVehicleTripSettlement(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View Vehicle List For Trip Settlement ";
        int insertStatus = 0;
        ModelAndView mv = null;
        System.out.println("controler is working");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            String tripId = request.getParameter("tripSheetId");
            String tripType = request.getParameter("tripType");
            String statusId = request.getParameter("statusId");
            String admin = request.getParameter("admin");
            request.setAttribute("tripType", tripType);
            request.setAttribute("statusId", statusId);
            request.setAttribute("admin", admin);
            request.setAttribute("tripId", tripId);
            tripTO.setTripId(tripId);
            ArrayList tripSettlementVehicleList = new ArrayList();
            tripSettlementVehicleList = tripBP.getTripSettlementVehicleList(tripTO);
            request.setAttribute("tripSettlementVehicleList", tripSettlementVehicleList);
            path = "content/trip/viewTripSettlementVehicleList.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewTicketingDetails(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String roleId = "" + (Integer) session.getAttribute("RoleId");
        String path = "";
        tripCommand = command;
        String menuPath = "Ticketing  >>  View Ticketing ";
        int insertStatus = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Ticketing";
            request.setAttribute("pageTitle", pageTitle);
            TripTO tripTO = new TripTO();
            path = "content/trip/viewTicketingList.jsp";
            ArrayList ticketingStatusList = new ArrayList();
            ticketingStatusList = tripBP.getTicketingStatusList(tripTO);
            request.setAttribute("ticketingStatusList", ticketingStatusList);
            tripTO.setUserId(userId);
            tripTO.setRoleId(roleId);
            ArrayList ticketingList = new ArrayList();
            ticketingList = tripBP.getTicketingList(tripTO);
            request.setAttribute("ticketingList", ticketingList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView createTicket(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Ticketing  >>  Create Ticket ";
        int insertStatus = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Ticketing";
            request.setAttribute("pageTitle", pageTitle);
            TripTO tripTO = new TripTO();
            path = "content/trip/createTicket.jsp";
            ArrayList ticketingStatusList = new ArrayList();
            ticketingStatusList = tripBP.getTicketingStatusList(tripTO);
            request.setAttribute("ticketingStatusList", ticketingStatusList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveTicketFile(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String roleId = "" + (Integer) session.getAttribute("RoleId");
        String path = "";
        tripCommand = command;
        String menuPath = "";
        int insertStatus = 0;
        ModelAndView mv = null;
        String newFileName = "", actualFilePath = "";
        String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        int i = 0;
        int j = 0;
        int m = 0;
        int n = 0;
        int p = 0;
        int s = 0;
        String priority = "";
        String[] podRemarks1 = new String[10];
        String[] fileSaved = new String[10];
        String[] lrNumber1 = new String[10];
        String[] uploadedFileName = new String[10];
        String[] tempFilePath = new String[10];
        String[] fileRemarks = new String[10];
        String status = "";
        String type = "";
        String from = "";
        String to = "";
        String cc = "";
        String title = "";
        String message = "";
        String tripType = "";
        String statusId = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                System.out.println("this is tht");
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        Date now = new Date();
                        String date = now.getDate() + "" + (now.getMonth() + 1) + "" + (now.getYear() + 1900);
                        String time = now.getHours() + "" + now.getMinutes() + "" + now.getSeconds() + userId;
                        fPart = (FilePart) partObj;
                        uploadedFileName[j] = fPart.getFileName();

                        if (!"".equals(uploadedFileName[j]) && uploadedFileName[j] != null) {

                            String[] splitFileName = uploadedFileName[j].split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            fileSaved[j] = splitFileName[0] + date + time + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath[j] = tempServerFilePath + "\\" + fileSaved[j];
                            actualFilePath = actualServerFilePath + "\\" + tempFilePath;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(tempFilePath[j]));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath[j]));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
//                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
//                            String part1 = parts.replace("\\", "");
                        }

                        System.out.println("fileName..." + fileName);
                        j++;
                    } else if (partObj.isParam()) {
                        if (partObj.getName().equals("priority")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            priority = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("fileRemarks")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            fileRemarks[s] = paramPart.getStringValue();
                            s++;
                        }
//                        if (partObj.getName().equals("podRemarks")) {
//                            ParamPart paramPart = (ParamPart) partObj;
//                            podRemarks1[n] = paramPart.getStringValue();
//                            n++;
//                        }
//
//                        if (partObj.getName().equals("lrNumber")) {
//                            ParamPart paramPart = (ParamPart) partObj;
//                            lrNumber1[p] = paramPart.getStringValue();
//                            p++;
//                        }
                        if (partObj.getName().equals("status")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            status = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("type")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            type = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("from")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            from = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("to")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            to = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("cc")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            cc = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("title")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            title = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("message")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            message = paramPart.getStringValue();
                        }
                    }
                }
            }

            String file = "";
            String saveFile = "";
            String remarks = "";
            System.out.println("the pod filelength is" + lrNumber1.length);
            tripTO.setStatus(status);
            tripTO.setType(type);
            tripTO.setFrom(from);
            tripTO.setTo(to);
            tripTO.setCc(cc);
            tripTO.setTitle(title);
            tripTO.setMessage(message);
            tripTO.setPriority(priority);
            tripTO.setRoleId(roleId);
            insertStatus = tripBP.saveTicket(tripTO, userId);
            int insertStatusDetails = 0;
            tripTO.setTicketId(String.valueOf(insertStatus));
            for (int x = 0; x < j; x++) {
                remarks = fileRemarks[x];
                file = tempFilePath[x];
                saveFile = fileSaved[x];
                tripTO.setRequestremarks(remarks);
                insertStatusDetails = tripBP.saveTicketFile(file, saveFile, tripTO, userId);
            }
            if (insertStatus != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Ticket Created Sucessfully: TICKET NO:- " + "TN-" + insertStatus);
            }

            mv = viewTicketingDetails(request, response, command);

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView saveTicket(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "";
        int insertStatus = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Ticketing";
            request.setAttribute("pageTitle", pageTitle);
            TripTO tripTO = new TripTO();
            path = "content/trip/createTicket.jsp";
            String priority = request.getParameter("priority");
            String status = request.getParameter("status");
            String type = request.getParameter("type");
            String from = request.getParameter("from");
            String to = request.getParameter("to");
            String cc = request.getParameter("cc");
            String title = request.getParameter("title");
            String message = request.getParameter("message");
            tripTO.setStatus(status);
            tripTO.setType(type);
            tripTO.setFrom(from);
            tripTO.setTo(to);
            tripTO.setCc(cc);
            tripTO.setTitle(title);
            tripTO.setMessage(message);
            tripTO.setPriority(priority);
            insertStatus = tripBP.saveTicket(tripTO, userId);
            if (insertStatus != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Ticket Created Sucessfully: TICKET NO:- " + "TN-" + insertStatus);
            }

            mv = viewTicketingDetails(request, response, command);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView viewTicketDetails(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Ticketing  >>  Create Ticket ";
        int insertStatus = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Ticketing";
            request.setAttribute("pageTitle", pageTitle);
            TripTO tripTO = new TripTO();
            path = "content/trip/viewTicketingDetails.jsp";
            String ticketId = request.getParameter("ticketId");
            tripTO.setTicketId(ticketId);
            ArrayList ticketingStatusList = new ArrayList();
            ticketingStatusList = tripBP.getTicketingStatusList(tripTO);
            request.setAttribute("ticketingStatusList", ticketingStatusList);
            tripTO.setUserId(userId);
            ArrayList ticketingList = new ArrayList();
            ticketingList = tripBP.getTicketingList(tripTO);
            request.setAttribute("ticketingList", ticketingList);
            ArrayList ticketingDetailsList = new ArrayList();
            ticketingDetailsList = tripBP.getTicketingDetailsList(tripTO);
            if (ticketingDetailsList.size() > 0) {
                request.setAttribute("ticketingDetailsList", ticketingDetailsList);
            }
            ArrayList ticketingStatusDetailsList = new ArrayList();
            ticketingStatusDetailsList = tripBP.getTicketingStatusDetailsList(tripTO);
            request.setAttribute("ticketingStatusDetailsList", ticketingStatusDetailsList);
            request.setAttribute("ticketingStatusDetailsListSize", ticketingStatusDetailsList.size() + 1);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView updateTicketStatus(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Ticketing  >>  Create Ticket ";
        int insertStatus = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Ticketing";
            request.setAttribute("pageTitle", pageTitle);
            TripTO tripTO = new TripTO();
            path = "content/trip/createTicket.jsp";
            String statusId = request.getParameter("statusId");
            String ticketId = request.getParameter("ticketId");
            String from = request.getParameter("from");
            String to = request.getParameter("to");
            String cc = request.getParameter("cc");
            String ticketNo = request.getParameter("ticketNo");
            String message = request.getParameter("message");
            tripTO.setFrom(from);
            tripTO.setTo(to);
            tripTO.setCc(cc);
            tripTO.setMessage(message);
            tripTO.setStatusId(statusId);
            tripTO.setTicketId(ticketId);
            tripTO.setTicketNo(ticketNo);
            int updateStatus = 0;
            updateStatus = tripBP.updateTicketStatus(tripTO, userId);
            ArrayList ticketingStatusList = new ArrayList();
            ticketingStatusList = tripBP.getTicketingStatusList(tripTO);
            request.setAttribute("ticketingStatusList", ticketingStatusList);
            mv = viewTicketingDetails(request, response, command);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView confirmNextTrip(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Ticketing  >>  Create Ticket ";
        int insertStatus = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Ticketing";
            request.setAttribute("pageTitle", pageTitle);
            TripTO tripTO = new TripTO();
            if (tripCommand.getTripSheetId() != null && !"".equals(tripCommand.getTripSheetId())) {
                tripTO.setTripId(tripCommand.getTripSheetId());
            }
            int updateNextTrip = 0;
            tripTO.setNextTrip("1");
            updateNextTrip = tripBP.updateNextTrip(tripTO, userId);
            mv = handleViewTripSheet(request, response, command);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView handlePrimaryDriverSettlement(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Driver Settlement  >>  Primary Driver Settlement ";
        int insertStatus = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Driver Settlement";
            request.setAttribute("pageTitle", pageTitle);
            TripTO tripTO = new TripTO();
            path = "content/trip/primaryDriverSettlement.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewPrimaryDriverSettlement(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Driver Settlement  >>  Primary Driver Settlement ";
        int insertStatus = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Driver Settlement";
            request.setAttribute("pageTitle", pageTitle);
            TripTO tripTO = new TripTO();
            String primaryDriverId = request.getParameter("primaryDriverId");
            String primaryDriver = request.getParameter("primaryDriver");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            tripTO.setDriverId(primaryDriverId);
            tripTO.setFromDate(fromDate);
            tripTO.setToDate(toDate);
            ArrayList primaryDriverSettlementTrips = new ArrayList();
            primaryDriverSettlementTrips = tripBP.getPrimaryDriverSettlementTrip(tripTO);
            request.setAttribute("settlementTrips", primaryDriverSettlementTrips);
            request.setAttribute("settlementTripsSize", primaryDriverSettlementTrips.size());
            request.setAttribute("primaryDriverId", primaryDriverId);
            request.setAttribute("primaryDriver", primaryDriver);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            ArrayList vehicleAdvance = new ArrayList();
            vehicleAdvance = tripBP.getVehicleDriverAdvance(tripTO);
            request.setAttribute("vehicleAdvance", vehicleAdvance);
            request.setAttribute("vehicleAdvanceSize", vehicleAdvance.size());
            ArrayList driverIdleBhatta = new ArrayList();
            driverIdleBhatta = tripBP.getDriverIdleBhatta(tripTO);
            request.setAttribute("driverIdleBhatta", driverIdleBhatta);
            request.setAttribute("driverIdleBhattaSize", driverIdleBhatta.size());
            String driverLastBalanceAmount = "";
            driverLastBalanceAmount = tripBP.getDriverLastBalanceAmount(tripTO);
            request.setAttribute("driverLastBalanceAmount", driverLastBalanceAmount);
            request.setAttribute("driverLastBalanceDate", fromDate);

            path = "content/trip/primaryDriverSettlement.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView savePrimaryDriverSettlement(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        tripCommand = command;
        String menuPath = "Driver Settlement  >>  Primary Driver Settlement ";
        int insertStatus = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Driver Settlement";
            request.setAttribute("pageTitle", pageTitle);
            TripTO tripTO = new TripTO();
            String primaryDriverId = request.getParameter("primaryDriverId");
            String primaryDriver = request.getParameter("primaryDriver");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            tripTO.setDriverId(primaryDriverId);
            tripTO.setFromDate(fromDate);
            tripTO.setToDate(toDate);
            String driverLastBalance = request.getParameter("driverLastBalanceAmount");
            String tripAmount = request.getParameter("tripAmount");
            String settleAmount = request.getParameter("settleAmount");
            String payAmount = request.getParameter("payAmount");
            String paymentMode = request.getParameter("paymentMode");
            String balanceAmount = request.getParameter("balanceAmount");
            String settlementTripsSize = request.getParameter("settlementTripsSize");
            String bhattaAmount = request.getParameter("bhattaAmount");
            String settlementRemarks = request.getParameter("settlementRemarks");
            String idleDays = request.getParameter("idleDays");
            String advAmount = request.getParameter("advAmount");
            tripTO.setDriverLastBalance(driverLastBalance);
            tripTO.setTripAmount(tripAmount);
            tripTO.setSettleAmount(settleAmount);
            tripTO.setPayAmount(payAmount);
            tripTO.setPaymentMode(paymentMode);
            tripTO.setBalanceAmount(balanceAmount);
            tripTO.setSettlementTripsSize(settlementTripsSize);
            tripTO.setBhattaAmount(bhattaAmount);
            tripTO.setIdleDays(idleDays);
            tripTO.setAdvanceAmount(advAmount);
            tripTO.setRemarks(settlementRemarks);
            insertStatus = tripBP.insertPrimaryDriverSettlement(tripTO, userId);
            tripTO.setStatus(String.valueOf(insertStatus));
            int insertdetailsStatus = 0;
            String[] tripId = request.getParameterValues("tripId");
            String[] vehicleIds = request.getParameterValues("vehicleIds");
            String[] transactionAmount = request.getParameterValues("endingBalance");
            String[] amount = request.getParameterValues("amount");
            String[] driverCount = request.getParameterValues("driverCount");
            String[] vehicleAdvanceId = request.getParameterValues("vehicleAdvanceId");
            String[] advanceVehicleId = request.getParameterValues("advanceVehicleId");
            String[] advanceAmount = request.getParameterValues("advanceAmount");
            String[] advanceDriverCount = request.getParameterValues("advanceDriverCount");
            String[] paidAdvance = request.getParameterValues("paidAdvance");
            if (insertStatus > 0) {
                for (int i = 0; i < tripId.length; i++) {
                    tripTO.setTransactionType("1");
                    tripTO.setTransactionId(tripId[i]);
                    tripTO.setVehicleId(vehicleIds[i]);
                    tripTO.setTransactionAmount(transactionAmount[i]);
                    tripTO.setAmount(amount[i]);
                    tripTO.setDriverCount(driverCount[i]);
                    insertdetailsStatus = tripBP.insertPrimaryDriverSettlementDetails(tripTO, userId);
                }
                if (vehicleAdvanceId != null) {
                    for (int i = 0; i < vehicleAdvanceId.length; i++) {
                        tripTO.setTransactionType("2");
                        tripTO.setTransactionId(vehicleAdvanceId[i]);
                        tripTO.setVehicleId(advanceVehicleId[i]);
                        tripTO.setTransactionAmount(paidAdvance[i]);
                        tripTO.setAmount(advanceAmount[i]);
                        tripTO.setDriverCount(advanceDriverCount[i]);
                        insertdetailsStatus = tripBP.insertPrimaryDriverSettlementDetails(tripTO, userId);
                    }
                }
                String[] idleBhattaId = request.getParameterValues("idleBhattaId");
                if (idleBhattaId != null) {
                    for (int i = 0; i < idleBhattaId.length; i++) {
                        tripTO.setIdleBhattaId(idleBhattaId[i]);
                        insertdetailsStatus = tripBP.updateIdleBhattaStatus(tripTO, userId);
                    }
                }

            }

            ArrayList primaryDriverSettlementTrips = new ArrayList();
            primaryDriverSettlementTrips = tripBP.getPrimaryDriverSettlementTrip(tripTO);
            request.setAttribute("settlementTrips", primaryDriverSettlementTrips);
            request.setAttribute("settlementTripsSize", primaryDriverSettlementTrips.size());
            request.setAttribute("primaryDriverId", primaryDriverId);
            request.setAttribute("primaryDriver", primaryDriver);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            ArrayList vehicleAdvance = new ArrayList();
            vehicleAdvance = tripBP.getVehicleDriverAdvance(tripTO);
            request.setAttribute("vehicleAdvance", vehicleAdvance);
            request.setAttribute("vehicleAdvanceSize", vehicleAdvance.size());
            ArrayList driverIdleBhatta = new ArrayList();
            driverIdleBhatta = tripBP.getDriverIdleBhatta(tripTO);
            request.setAttribute("driverIdleBhatta", driverIdleBhatta);
            request.setAttribute("driverIdleBhattaSize", driverIdleBhatta.size());
            String driverLastBalanceAmount = "";
            driverLastBalanceAmount = tripBP.getDriverLastBalanceAmount(tripTO);
            request.setAttribute("driverLastBalanceAmount", driverLastBalanceAmount);
            request.setAttribute("driverLastBalanceDate", fromDate);

            path = "content/trip/primaryDriverSettlement.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewTripSheetForChallan(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >> View Upload Challan ";
        String statusId1 = request.getParameter("statusId");
        System.out.println("pass value:" + request.getParameter("successMessage"));
        request.setAttribute("errorMessage", request.getParameter("msg"));

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            String primaryDriver = request.getParameter("primaryDriver");
            request.setAttribute("primaryDriver", primaryDriver);
            String primaryDriverId = request.getParameter("primaryDriverId");
            request.setAttribute("primaryDriverId", primaryDriverId);
            String documentRequired = request.getParameter("documentRequired");
            request.setAttribute("documentRequired", documentRequired);
            String vehicleId = request.getParameter("vehicleId");
            if (primaryDriverId == null) {
                primaryDriverId = "";
            }
            tripTO.setDriverId(primaryDriverId);
            int userId = (Integer) session.getAttribute("userId");
            String roleId = "" + (Integer) session.getAttribute("RoleId");
            tripTO.setUserId(userId);
            request.setAttribute("roleId", roleId);

            if (vehicleId != null && !vehicleId.equals("")) {
                tripTO.setVehicleId(vehicleId);
                request.setAttribute("vehicleId", vehicleId);
            } else {
                tripTO.setVehicleId("");
                request.setAttribute("vehicleId", "");
            }

            if (tripCommand.getFromDate() != null && !tripCommand.getFromDate().equals("")) {
                tripTO.setFromDate(tripCommand.getFromDate());
                request.setAttribute("fromDate", tripTO.getFromDate());
            } else {
                tripTO.setFromDate("");
                request.setAttribute("fromDate", "");
            }
            if (tripCommand.getToDate() != null && !tripCommand.getToDate().equals("")) {
                tripTO.setToDate(tripCommand.getToDate());
                request.setAttribute("toDate", tripTO.getToDate());
            } else {
                tripTO.setToDate("");
                request.setAttribute("toDate", "");
            }
            tripTO.setDocumentRequiredStatus(documentRequired);
            ArrayList uploadChallanList = new ArrayList();
            uploadChallanList = tripBP.getTripSheetDetailsForChallan(tripTO);
            request.setAttribute("tripDetails", uploadChallanList);

            ArrayList vehicleList = new ArrayList();
            vehicleList = tripBP.getVehicleList();
            request.setAttribute("vehicleList", vehicleList);

            tripTO.setDocumentRequiredStatus("Y");
            String billExpense = "";
            String[] tempBillExpense = null;
            billExpense = tripBP.getTotalBillExpenseForDriver(tripTO);
            tempBillExpense = billExpense.split("~");
            request.setAttribute("uploadedChallan", tempBillExpense[0]);
            request.setAttribute("uploadedChallanAmount", tempBillExpense[1]);
            tripTO.setDocumentRequiredStatus("N");
            billExpense = tripBP.getTotalBillExpenseForDriver(tripTO);
            tempBillExpense = billExpense.split("~");
            request.setAttribute("pendingChallan", tempBillExpense[0]);
            request.setAttribute("pendingChallanAmount", tempBillExpense[1]);

            //}
            path = "content/trip/uploadChallan.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView displayInvoiceSummaryExcel(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >> View Upload Challan ";
        String statusId1 = request.getParameter("statusId");
        System.out.println("pass value:" + request.getParameter("successMessage"));
        request.setAttribute("errorMessage", request.getParameter("msg"));

        try {
            TripTO tripTO = new TripTO();
            String invoiceId = request.getParameter("invoiceId");
            System.out.println("request.getParameter. inv o" + request.getParameter("invoiceId"));
            tripTO.setInvoiceId(invoiceId);
            String invoiceType = request.getParameter("invoiceType");
            tripTO.setInvoiceType(invoiceType);
            System.out.println("invoiceType" + invoiceType);

            ArrayList invoiceHeader = tripBP.getTripsBilledDetails(tripTO);
            tripTO.setExpenseType("3");
            ArrayList tripsOtherExpenseDetails = tripBP.getTripsOtherExpenseDetails1(tripTO);

            ArrayList invoiceHeaderBill = tripBP.getOrderInvoiceHeaderBill(tripTO);
            tripTO.setExpenseType("1");
            ArrayList tripsOtherExpenseDetailsBill = tripBP.getTripsOtherExpenseDetails1(tripTO);

            ArrayList invoiceHeaderRemb = tripBP.getOrderInvoiceHeaderRemb(tripTO);
            tripTO.setExpenseType("4");
            ArrayList tripsOtherExpenseDetailsRemb = tripBP.getTripsOtherExpenseDetails1(tripTO);

            ArrayList invoiceTaxDetailsBill = tripBP.getInvoiceGSTTaxDetailsBill(tripTO);

            ArrayList invoiceTaxDetailsRemb = tripBP.getInvoiceGSTTaxDetailsRemb(tripTO);

            ArrayList invoiceHeaderCancellation = tripBP.getOrderInvoiceHeaderCancellation(tripTO);
            tripTO.setExpenseType("5");
            ArrayList invoiceTaxDetailsCancellation = tripBP.getInvoiceGSTTaxDetailsCancellation(tripTO);

            System.out.println("1111111111");

            request.setAttribute("invoiceHeader", invoiceHeader);
            request.setAttribute("invoiceHeaderRemb", invoiceHeaderRemb);
            request.setAttribute("invoiceHeaderBill", invoiceHeaderBill);
            request.setAttribute("tripsOtherExpenseDetails", tripsOtherExpenseDetails);
            request.setAttribute("tripsOtherExpenseDetailsRemb", tripsOtherExpenseDetailsRemb);
            request.setAttribute("tripsOtherExpenseDetailsBill", tripsOtherExpenseDetailsBill);
            request.setAttribute("invoiceTaxDetailsRemb", invoiceTaxDetailsRemb);
            request.setAttribute("invoiceTaxDetailsBill", invoiceTaxDetailsBill);
            request.setAttribute("invoiceHeaderCancellation", invoiceHeaderCancellation);
            request.setAttribute("invoiceTaxDetailsCancellation", invoiceTaxDetailsCancellation);

            if (request.getParameter("invoiceType").equals("reimbursement")) {
                path = "content/trip/displayInvoiceReimbursementSummaryExcel.jsp";
            } else if (request.getParameter("invoiceType").equals("billToCustomer")) {
                path = "content/trip/displayInvoiceExpenseSummaryExcel.jsp";
            } else if (request.getParameter("invoiceType").equals("cancellation")) {
                path = "content/trip/displayInvoiceCancellationSummaryExcel.jsp";
            } else {
                path = "content/trip/displayInvoiceFreightSummaryExcel.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView displayInvoicePrint(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "  ";
        tripCommand = command;
        Map<String, ArrayList> printData = new HashMap<String, ArrayList>();
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            TripTO tripTO = new TripTO();
            String invoiceId = request.getParameter("invoiceId");
            System.out.println("request.getParameter. inv id" + request.getParameter("invoiceId"));
            tripTO.setInvoiceId(invoiceId);
            String invoiceType = request.getParameter("invoiceType");
            String fuelInvoiceFlag = request.getParameter("fuelInvoiceFlag");
            request.setAttribute("fuelInvoiceFlag", fuelInvoiceFlag);
            tripTO.setInvoiceType(invoiceType);
            System.out.println("invoiceType" + invoiceType);

            ArrayList invoiceHeader = tripBP.getOrderInvoiceHeader(tripTO);
            tripTO.setExpenseType("3");
            ArrayList tripsOtherExpenseDetails = tripBP.getTripsOtherExpenseDetails1(tripTO);
            ArrayList tripsOtherExpense = tripBP.getTripsOtherExpense(tripTO);

            System.out.println("tripsinvoiceHeader : " + invoiceHeader.size());
            System.out.println("tripsOtherExpenseDetails : " + tripsOtherExpenseDetails.size());

            String cgstPer = "0", cgstAmt = "0.00";
            String sgstPer = "0", sgstAmt = "0.00";
            String igstPer = "0", igstAmt = "0.00";

            TripTO tripTONew = new TripTO();
            TripTO tripTONew1 = new TripTO();
            //TripTO tripTONew2 = new TripTO();
            Iterator itr = invoiceHeader.iterator();
            while (itr.hasNext()) {
                tripTONew = (TripTO) itr.next();
                request.setAttribute("custName", tripTONew.getCustomerName());
                request.setAttribute("invoiceNo", tripTONew.getInvoiceNo());
                request.setAttribute("customerAddress", tripTONew.getCustomerAddress());
                request.setAttribute("custStateCode", tripTONew.getCustStateCode());
                request.setAttribute("billingState", tripTONew.getBillingState());
                request.setAttribute("gstNo", tripTONew.getGstNo());
                request.setAttribute("panNo", tripTONew.getPanNo());
                request.setAttribute("billDate", tripTONew.getBillDate());
                request.setAttribute("placeofSupply", tripTONew.getPlaceofSupply());
                request.setAttribute("tripStartDate", tripTONew.getTripStartDate());
                request.setAttribute("tripEndDate", tripTONew.getTripEndDate());
                request.setAttribute("startPoint", tripTONew.getRouteInfo().split("-")[0]);
                request.setAttribute("endPoint", tripTONew.getRouteInfo().split("-")[1]);
                request.setAttribute("grandTotal", tripTONew.getGrandTotal());

                request.setAttribute("tripMonth", tripTONew.getTripMonth());
                request.setAttribute("tripYear", tripTONew.getTripYear());

                cgstAmt = tripTONew.getCgstValue();
                cgstPer = tripTONew.getCgstPercentage();

                sgstAmt = tripTONew.getSgstValue();
                sgstPer = tripTONew.getSgstPercentage();

                igstAmt = tripTONew.getIgstValue();
                igstPer = tripTONew.getIgstPercentage();

            }

            System.out.println("cgstAmt======" + cgstAmt + "=====cgstPer========" + cgstPer);
            System.out.println("sgstAmt======" + sgstAmt + "=====sgstPer========" + sgstPer);
            System.out.println("igstAmt======" + igstAmt + "=====igstPer========" + igstPer);

            request.setAttribute("cgstValue", cgstAmt);
            request.setAttribute("sgstValue", sgstAmt);
            request.setAttribute("igstValue", igstAmt);
            request.setAttribute("sgstPercentage", sgstPer);
            request.setAttribute("cgstPercentage", cgstPer);
            request.setAttribute("igstPercentage", igstPer);

            Iterator itr1 = tripsOtherExpense.iterator();
            while (itr1.hasNext()) {
                tripTONew1 = (TripTO) itr1.next();
                request.setAttribute("expenseAmount", tripTONew1.getExpenseValue());
            }

            printData.put("tripsOtherExpense", tripsOtherExpense);
            printData.put("invoiceHeader", invoiceHeader);
            printData.put("tripsOtherExpenseDetails", tripsOtherExpenseDetails);

            path = "content/trip/invoicePrint.jsp";

//            ArrayList invoiceHeaderBill = tripBP.getOrderInvoiceHeaderBill(tripTO);
//            tripTO.setExpenseType("1");
//            ArrayList tripsOtherExpenseDetailsBill = tripBP.getTripsOtherExpenseDetails1(tripTO);
//            
//            ArrayList invoiceTaxDetails = tripBP.getInvoiceGSTTaxDetails(tripTO);
//            System.out.println("invoiceTaxDetails=" + invoiceTaxDetails.size());
//            Iterator itr2 = invoiceTaxDetails.iterator();
//            while (itr2.hasNext()) {
//                tripTONew2 = (TripTO) itr2.next();
//
//                if ("CGST".equalsIgnoreCase(tripTONew2.getGstName())) {
//                    cgstAmt = tripTONew2.getTotalTax();
//                    cgstPer = tripTONew2.getGstPercentage();
//                    System.out.println("cgstAmt1======" + cgstAmt + "=====cgstPer2========" + cgstPer);
//                }
//                if ("SGST".equalsIgnoreCase(tripTONew2.getGstName())) {
//                    sgstAmt = tripTONew2.getTotalTax();
//                    sgstPer = tripTONew2.getGstPercentage();
//                    System.out.println("sgstAmt1======" + cgstAmt + "=====sgstPer2========" + sgstPer);
//                }
//                if ("IGST".equalsIgnoreCase(tripTONew2.getGstName())) {
//                    igstAmt = tripTONew2.getTotalTax();
//                    igstPer = tripTONew2.getGstPercentage();
//                    System.out.println("igstAmt=1=====" + igstAmt + "=====igstPer2========" + igstPer);
//                }
//            }
//            ArrayList invoiceHeaderRemb = tripBP.getOrderInvoiceHeaderRemb(tripTO);
//            tripTO.setExpenseType("4");
//            ArrayList tripsOtherExpenseDetailsRemb = tripBP.getTripsOtherExpenseDetails1(tripTO);
//            ArrayList invoiceTaxDetailsBill = tripBP.getInvoiceGSTTaxDetailsBill(tripTO);
//            ArrayList invoiceTaxDetailsRemb = tripBP.getInvoiceGSTTaxDetailsRemb(tripTO);
//            ArrayList invoiceHeaderCancellation = tripBP.getOrderInvoiceHeaderCancellation(tripTO);
//            tripTO.setExpenseType("5");
//            ArrayList invoiceTaxDetailsCancellation = tripBP.getInvoiceGSTTaxDetailsCancellation(tripTO);
//            ArrayList invoiceHeaderTsp = tripBP.getOrderInvoiceHeaderTsp(tripTO);
//            tripTO.setExpenseType("6");
//            ArrayList invoiceTaxDetailsTsp = tripBP.getInvoiceGSTTaxDetailsTsp(tripTO);
//            ArrayList invoiceHeaderRoyalty = tripBP.getOrderInvoiceHeaderRoyalty(tripTO);
//            tripTO.setExpenseType("7");
//            ArrayList invoiceTaxDetailsRoyalty = tripBP.getInvoiceGSTTaxDetailsRoyalty(tripTO);
//            System.out.println("1111111111");
//            printData.put("invoiceHeaderRemb", invoiceHeaderRemb);
//            printData.put("invoiceHeaderBill", invoiceHeaderBill);
//            printData.put("tripsOtherExpenseDetailsRemb", tripsOtherExpenseDetailsRemb);
//            printData.put("tripsOtherExpenseDetailsBill", tripsOtherExpenseDetailsBill);
//            printData.put("invoiceTaxDetailsRemb", invoiceTaxDetailsRemb);
//            printData.put("invoiceTaxDetailsBill", invoiceTaxDetailsBill);
//            printData.put("invoiceHeaderCancellation", invoiceHeaderCancellation);
//            printData.put("invoiceTaxDetailsCancellation", invoiceTaxDetailsCancellation);
//            printData.put("invoiceTaxDetails", invoiceTaxDetails);
////            printData.put("invoiceHeaderTsp", invoiceHeaderTsp);
//            printData.put("invoiceTaxDetailsTsp", invoiceTaxDetailsTsp);
//            printData.put("invoiceHeaderRoyalty", invoiceHeaderRoyalty);
//            printData.put("invoiceTaxDetailsRoyalty", invoiceTaxDetailsRoyalty);
//            printData.put("invoiceType", invoiceType);
//            printData.put("royalInvoiceDetails", royalInvoiceDetails);
//            printData.put("invoiceTaxDetailsBill", invoiceTaxDetailsBill);
//            printData.put("expenseInvoiceDetails", expenseInvoiceDetails);
//            if (request.getParameter("invoiceType").equals("reimbursement")) {
//                menuPath = "InvoicePdfViewRoyal";
//                System.out.println(" InvoicePdfViewRoyal" + menuPath);
//            } else if (request.getParameter("invoiceType").equals("billToCustomer")) {
//                menuPath = "InvoicePdfViewExpense";
//                System.out.println(" InvoicePdfViewExpense" + menuPath);
//            } else if (request.getParameter("invoiceType").equals("cancellation")) {
//                menuPath = "InvoicePdfViewCancellation";
//                System.out.println(" InvoicePdfViewCancellation" + menuPath);
//            } else if (request.getParameter("invoiceType").equals("tspInvoice")) {
//                menuPath = "InvoicePdfViewTSP";
//                System.out.println(" InvoicePdfViewCancellation" + menuPath);
//            } else if (request.getParameter("invoiceType").equals("royaltyInvoice")) {
//                menuPath = "InvoicePdfViewRoyalty";
//                System.out.println(" InvoicePdfViewCancellation" + menuPath);
//            } else if (request.getParameter("invoiceType").equals("tspDestinationInvoice")) {
//                menuPath = "InvoicePdfViewTSPdestination";
//                System.out.println(" InvoicePdfViewTSPdestination" + menuPath);
//            } else if (request.getParameter("invoiceType").equals("freightSeperate")) {
//                menuPath = "InvoicePdfViewSeperateFuel";
////                menuPath = "InvoicePdfViewWithoutFuel";
//                System.out.println(" hfhtfhfhf" + menuPath);
//            } else if (request.getParameter("invoiceType").equals("fuelInvoice")) {
//                menuPath = "InvoicePdfViewWithoutFuel";
//                System.out.println(" gggggggg" + menuPath);
//            } else {
//                menuPath = "InvoicePdfView";
//                System.out.println(" InvoicePdfView" + menuPath);
//            }
//            menuPath = "InvoicePdfView";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save trip sheet data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
//        return new ModelAndView(menuPath, "printData", printData);
        return new ModelAndView(path);
    }

    public ModelAndView handleViewTripExpenses(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  Trip Closure ";
        String tripSheetId = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
                request.setAttribute("tripSheetId", tripCommand.getTripSheetId());
            }
            if (tripCommand.getOrderType() != null && !tripCommand.getOrderType().equals("")) {
                tripTO.setOrderType(tripCommand.getOrderType());
                request.setAttribute("orderType", tripCommand.getOrderType());
            }

            if (tripCommand.getVehicleId() != null && !tripCommand.getVehicleId().equals("")) {
                tripTO.setVehicleId(tripCommand.getVehicleId());
                request.setAttribute("vehicleId", tripCommand.getVehicleId());
            }
            tripTO.setTripId(tripTO.getTripSheetId());
            tripSheetId = tripTO.getTripSheetId();
            String orderType = tripTO.getOrderType();
            ArrayList fuelDetails = new ArrayList();
            fuelDetails = tripBP.getFuelDetails(tripTO);
            if (fuelDetails.size() > 0) {
                request.setAttribute("fuelDetails", fuelDetails);
            }

            ArrayList otherExpenseDetails = new ArrayList();
            otherExpenseDetails = tripBP.getOtherExpenseDetails(tripTO);
            if (otherExpenseDetails.size() > 0) {
                request.setAttribute("otherExpenseDetails", otherExpenseDetails);
                request.setAttribute("otherExpenseDetailsSize", otherExpenseDetails.size());
            }

            ArrayList expenseDetails = new ArrayList();
            expenseDetails = tripBP.getExpenseDetails(tripTO);
            if (expenseDetails.size() > 0) {
                request.setAttribute("expenseDetails", expenseDetails);
            }
            ArrayList driverNameDetails = new ArrayList();
            driverNameDetails = tripBP.getDriverName(tripTO);
            if (driverNameDetails.size() > 0) {
                request.setAttribute("driverNameDetails", driverNameDetails);
            }

            ArrayList statusDetails = new ArrayList();
            statusDetails = tripBP.getTripStausDetails(tripTO);
            if (statusDetails.size() > 0) {
                request.setAttribute("statusDetails", statusDetails);
            }

            ArrayList tripDetails = new ArrayList();
            ArrayList tripPointDetails = new ArrayList();
            ArrayList tripPreStartDetails = new ArrayList();
            ArrayList tripStartDetails = new ArrayList();
            ArrayList tripEndDetails = new ArrayList();
            tripDetails = tripBP.getTripDetails(tripTO);
            //get trip type

            Iterator itr2 = tripDetails.iterator();
            String tripType = request.getParameter("tripType");
            String statusId = request.getParameter("statusId");
            request.setAttribute("tripType", tripType);
            request.setAttribute("statusId", statusId);

            while (itr2.hasNext()) {
                TripTO tripTO2 = (TripTO) itr2.next();
                tripType = tripTO2.getTripType();
            }
            tripTO.setTripType(tripType);

            System.out.println("tripType:" + tripType);
            tripPointDetails = tripBP.getTripPointDetails(tripTO);
            tripPreStartDetails = tripBP.getPreStartTripDetails(tripTO);
            tripStartDetails = tripBP.getStartedTripDetails(tripTO);
            tripEndDetails = tripBP.getEndTripDetails(tripTO);

            ArrayList tripUnPackDetails = new ArrayList();
            tripUnPackDetails = tripBP.getTripUnPackDetails(tripTO);
            request.setAttribute("tripUnPackDetails", tripUnPackDetails);

            request.setAttribute("tripDetails", tripDetails);
            request.setAttribute("tripPointDetails", tripPointDetails);
            request.setAttribute("tripPreStartDetails", tripPreStartDetails);
            request.setAttribute("tripStartDetails", tripStartDetails);
            request.setAttribute("tripEndDetails", tripEndDetails);
            ArrayList expiryDateDetails = new ArrayList();
            expiryDateDetails = tripBP.getExpiryDateDetails(tripTO);
            request.setAttribute("expiryDateDetails", expiryDateDetails);

            //system expense calculation start
            //Throttle Starts As On 12-12-2013
            tripTO.setTripId(tripTO.getTripSheetId());

            ArrayList viewPODDetails = new ArrayList();
            viewPODDetails = tripBP.getPODDetails(tripTO);
            if (viewPODDetails.size() > 0) {
                request.setAttribute("viewPODDetails", viewPODDetails);
            }
            String vehicleTypeId = "";
            TripTO tripTO1 = new TripTO();
            ArrayList totalExpenseDetails = new ArrayList();
            totalExpenseDetails = tripBP.getTotalExpenseDetails(tripTO);
            Iterator itr1 = totalExpenseDetails.iterator();
            /*
             tripTo.setSecondaryTollAmount(tempVar[0]);
             tripTo.setSecondaryAddlTollAmount(tempVar[1]);
             tripTo.setSecondaryMiscAmount(tempVar[2]);
             */
            while (itr1.hasNext()) {
                tripTO1 = (TripTO) itr1.next();
                request.setAttribute("tollAmount", tripTO1.getTollAmount());
                request.setAttribute("driverBatta", tripTO1.getDriverBatta());
                request.setAttribute("driverIncentive", tripTO1.getDriverIncentive());
                request.setAttribute("fuelPrice", tripTO1.getFuelPrice());
                request.setAttribute("milleage", tripTO1.getMilleage());
                request.setAttribute("reeferConsumption", tripTO1.getReeferConsumption());
                request.setAttribute("vehicleTypeId", tripTO1.getVehicleTypeId());
                vehicleTypeId = tripTO1.getVehicleTypeId();
                request.setAttribute("secTollAmount", tripTO1.getSecondaryTollAmount());
                request.setAttribute("secAddlTollAmount", tripTO1.getSecondaryAddlTollAmount());
                request.setAttribute("secMiscAmount", tripTO1.getSecondaryMiscAmount());
                request.setAttribute("secParkingAmount", tripTO1.getSecondaryParkingAmount());
                request.setAttribute("fuelTypeId", tripTO1.getFuelTypeId());

            }
            request.setAttribute("tripType", tripType);
            request.setAttribute("totalExpenseDetails", totalExpenseDetails);

            String startDate = "";
            String startTime = "";
            String endDate = "";
            String endTime = "";
            TripTO tpTO = new TripTO();
            ArrayList tripSheetDetails = new ArrayList();
            tripSheetDetails = tripBP.getClosureEndTripSheetDetails(tripTO);
            Iterator itr = tripSheetDetails.iterator();
            while (itr.hasNext()) {
                tpTO = (TripTO) itr.next();
                request.setAttribute("tripSheetId", tpTO.getTripSheetId());
                request.setAttribute("cNotes", tpTO.getcNotes());
                request.setAttribute("vehicleNo", tpTO.getVehicleNo());
                request.setAttribute("tripCode", tpTO.getTripCode());
                request.setAttribute("startDate", tpTO.getTripScheduleDate());
                startDate = tpTO.getTripScheduleDate();
                request.setAttribute("startTime", tpTO.getTripScheduleTime());
                startTime = tpTO.getTripScheduleTime();
                request.setAttribute("estimatedExpense", tpTO.getOrderExpense());
                request.setAttribute("rcmExpense", tpTO.getRcmExpense());
                request.setAttribute("origin", tpTO.getOrigin());
                request.setAttribute("destination", tpTO.getDestination());
                request.setAttribute("tripEndDate", tpTO.getTripEndDate());
                endDate = tpTO.getTripEndDate();
                request.setAttribute("tripEndTime", tpTO.getTripEndTime());
                endTime = tpTO.getTripEndTime();
                request.setAttribute("startKm", tpTO.getStartKm());
                request.setAttribute("startHm", tpTO.getStartHm());
                request.setAttribute("endKm", tpTO.getEndKm());
                request.setAttribute("endHm", tpTO.getEndHm());
                request.setAttribute("runKm", tpTO.getRunKm());
                request.setAttribute("runHm", tpTO.getRunHm());
                request.setAttribute("totaldays", tpTO.getTotalDays());
                request.setAttribute("totalHours", tpTO.getDurationHours());
                request.setAttribute("driverCount", tpTO.getDriverCount());
                request.setAttribute("extraExpenseValue", tpTO.getExtraExpenseValue());

                System.out.println("getOrderExpense = " + tpTO.getOrderExpense());
                System.out.println("getRcmExpense = " + tpTO.getRcmExpense());
                System.out.println("getStartKm = " + tpTO.getStartKm());
                System.out.println("getStartHm = " + tpTO.getStartHm());
                System.out.println("getEndKm = " + tpTO.getEndKm());
                System.out.println("getEndHm = " + tpTO.getEndHm());
                System.out.println("getRunKm = " + tpTO.getRunKm());
                System.out.println("getRunHm = " + tpTO.getRunHm());
                System.out.println("getTotalDays = " + tpTO.getTotalDays());
                System.out.println("getDurationHours = " + tpTO.getDurationHours());
                System.out.println("driverCount = " + tpTO.getDriverCount());
                System.out.println("tpTO.getExtraExpenseValue() " + tpTO.getExtraExpenseValue());
                String[] temp = null;
                if (!"".equals(tpTO.getDriverName()) && tpTO.getDriverName().contains(",")) {
                    temp = tpTO.getDriverName().split(",");
                    request.setAttribute("driverName1", temp[0]);
                    request.setAttribute("driverName2", temp[1]);
                } else {
                    request.setAttribute("driverName1", tpTO.getDriverName());
                }
                request.setAttribute("driverName", tpTO.getDriverName());
            }
            int tripCnt = 0;
            String tripCount = "";
            if (tripType.equals("2")) {
                tripCount = tripBP.getTripCount(tpTO);
                if (tripCount != null) {
                    tripCnt = Integer.parseInt(tripCount);
                }
            }

            request.setAttribute("tripCnt", tripCnt);
            request.setAttribute("tripSheetDetails", tripSheetDetails);
            String gpsKm = tripBP.getGpsKm(tripTO.getTripId());
            System.out.println("gpsKmVal = " + gpsKm);
            if (gpsKm != null) {
                String[] temp = gpsKm.split("-");
                String gpsKmVal = "0";
                if (!"0".equals(temp[0])) {
                    gpsKmVal = temp[0];
                }
                request.setAttribute("gpsKm", "" + gpsKmVal);
                //use above calculations
                request.setAttribute("gpsKm", gpsKmVal);
                request.setAttribute("gpsHm", temp[1]);
                System.out.println("gpsKmVal = " + gpsKmVal);
                System.out.println("gpsHm = " + temp[1]);
            } else {
                request.setAttribute("gpsKm", 0);
                request.setAttribute("gpsHm", 0);

            }
            //check if approval has been raised and its status for gps non usage
            String odometerApprovalStatus = tripBP.getClosureApprovalStatus(tripTO.getTripId(), "1");//1 indicates gps non usage approval request
            request.setAttribute("odometerApprovalStatus", odometerApprovalStatus);
            //ArrayList odometerApprovalHistory = tripBP;
            //approvalStatus =0 -> pending approval ; 1-> approved; 2-> rejected

            String expenseDeviationApprovalStatus = tripBP.getClosureApprovalStatus(tripTO.getTripId(), "2");//1 indicates expense Deviation approval request
            ArrayList approveProcessHistory = tripBP.getClosureApprovalProcessHistory(tripTO);
            request.setAttribute("approveProcessHistory", approveProcessHistory);
            request.setAttribute("expenseDeviationApprovalStatus", expenseDeviationApprovalStatus);
            //Throttle Starts As On 12-12-2013
            String miscValue = tripBP.getMiscValue(vehicleTypeId);
            request.setAttribute("miscValue", miscValue);

            //system expense calculation end
            //fetch total booked expenses
            String bookedExpense = tripBP.getBookedExpense(tripTO.getTripId(), tripTO);
            request.setAttribute("bookedExpense", bookedExpense);

            ArrayList startTripDetails = new ArrayList();
            startTripDetails = tripBP.getStartTripDetails(tripTO);
            request.setAttribute("startTripDetails", startTripDetails);
            //String tripSheetId = request.getParameter("tripId");
            ArrayList tripAdvanceDetails = new ArrayList();
            tripAdvanceDetails = tripBP.getTripAdvanceDetails(tripSheetId);
            request.setAttribute("tripAdvanceDetails", tripAdvanceDetails);

            ArrayList tripAdvanceDetailsStatus = new ArrayList();
            tripAdvanceDetailsStatus = tripBP.getTripAdvanceDetailsStatus(tripSheetId);

            request.setAttribute("tripAdvanceDetailsStatus", tripAdvanceDetailsStatus);

            ArrayList customerList = new ArrayList();
            customerList = tripBP.getTripCustomerList(tripTO);
            request.setAttribute("customerLists", customerList);

            ArrayList bpclTransactionHistory = new ArrayList();
            bpclTransactionHistory = tripBP.getBPCLTransactionHistory(tripTO);
            request.setAttribute("bpclTransactionHistory", bpclTransactionHistory);

            ArrayList tripFuel = new ArrayList();
            tripFuel = tripBP.getTripFuel(tripTO);
            request.setAttribute("tripFuel", tripFuel);

            ArrayList vehicleChangeAdvanceDetails = new ArrayList();
            vehicleChangeAdvanceDetails = tripBP.getVehicleChangeTripAdvanceDetails(tripSheetId);
            request.setAttribute("vehicleChangeAdvanceDetails", vehicleChangeAdvanceDetails);
            request.setAttribute("vehicleChangeAdvanceDetailsSize", vehicleChangeAdvanceDetails.size());
            String otherExpenseDoneStatus = tripBP.getOtherExpenseDoneStatus(tripTO);
            request.setAttribute("otherExpenseDoneStatus", otherExpenseDoneStatus);
            ArrayList tripsettelmentDetails = new ArrayList();
            tripsettelmentDetails = tripBP.tripsettelmentDetails(tripTO);
            if (tripsettelmentDetails.size() > 0) {
                request.setAttribute("tripsettelmentDetails", tripsettelmentDetails);
                Iterator itrList = tripsettelmentDetails.iterator();
                TripTO tTO = new TripTO();
                while (itrList.hasNext()) {
                    tTO = (TripTO) itrList.next();
                    request.setAttribute("setteledKm", tTO.getSetteledKm());
                    request.setAttribute("setteledHm", tTO.getSetteledKm());
                    request.setAttribute("setteledTotalKm", tTO.getSetteltotalKM());
                    request.setAttribute("setteledTotalHm", tTO.getSetteltotalHrs());
                    request.setAttribute("overrideTripRemarks", tTO.getOverrideTripRemarks());
                    request.setAttribute("overrideBy", tTO.getOverRideBy());
                    request.setAttribute("overrideOn", tTO.getOverRideOn());
                }
            }
            String admin = request.getParameter("admin");
            request.setAttribute("admin", admin);
            String fclogin = request.getParameter("fclogin");
            System.out.println("fclogin:" + fclogin);
            request.setAttribute("fclogin", fclogin);
            path = "content/trip/tripExpense.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

//    public ModelAndView updateTripStatus(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
//        if (request.getSession().isNew()) {
//            // return new ModelAndView("content/login.jsp");
//        }
//        HttpSession session = request.getSession();
//        int userId = (Integer) session.getAttribute("userId");
//        String path = "";
//        ModelAndView mv = null;
//        tripCommand = command;
//        String menuPath = "Operation  >>  Edit Start Trip Sheet ";
//        int insertStatus = 0;
//        try {
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            //            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//            //                path = "content/common/NotAuthorized.jsp";
//            //            } else {
//            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//            String pageTitle = "View Trip Sheet";
//            request.setAttribute("pageTitle", pageTitle);
//
//            TripTO tripTO = new TripTO();
//            String tripSheetId = tripCommand.getTripSheetId();
//
//            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
//                tripTO.setTripSheetId(tripCommand.getTripSheetId());
//            }
//            tripTO.setStatusId("23"); //trip started
//            insertStatus = tripBP.updateStatus(tripTO, userId);
//            path = "content/redirectPage.jsp";
//
//            String responsiveStatus = request.getParameter("respStatus");
//            String tripType = request.getParameter("tripType");
//            String statusId = request.getParameter("statusId");
//            request.setAttribute("tripType", tripType);
//            request.setAttribute("statusId", statusId);
//            if (responsiveStatus != null && "Y".equals(responsiveStatus)) {
//                request.setAttribute("respStatus", "Y");
//                mv = handleViewTripSheet(request, response, command);
//            } else {
//                mv = handleViewTripSheet(request, response, command);
//            }
//
//            //}
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        //        return new ModelAndView(path);
//        return mv;
//    }
    public ModelAndView updateTripStatus(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        ModelAndView mv = null;
        tripCommand = command;
        String menuPath = "Operation  >>  Edit Start Trip Sheet ";
        int insertStatus = 0;
        int insertStatu2 = 0;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            String tripSheetId = tripCommand.getTripSheetId();

            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
            }
            if (tripCommand.getLrNumber() != null && !tripCommand.getLrNumber().equals("")) {
                tripTO.setLrNumber(tripCommand.getLrNumber());
            }
            if (tripCommand.getFuelLtr() != null && !tripCommand.getFuelLtr().equals("")) {
                tripTO.setFuelLtr(tripCommand.getFuelLtr());
            }
            tripTO.setStatusId("23"); //trip started
            insertStatus = tripBP.updateStatus(tripTO, userId);
            insertStatu2 = tripBP.updateTripFuel(tripTO, userId);
            path = "content/redirectPage.jsp";

            String responsiveStatus = request.getParameter("respStatus");
            String tripType = request.getParameter("tripType");
            String statusId = request.getParameter("statusId");
            request.setAttribute("tripType", tripType);
            request.setAttribute("statusId", statusId);
            if (responsiveStatus != null && "Y".equals(responsiveStatus)) {
                request.setAttribute("respStatus", "Y");
                mv = handleViewTripSheet(request, response, command);
            } else {
                mv = handleViewTripSheet(request, response, command);
            }

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        //        return new ModelAndView(path);
        return mv;
    }

    public void getCustomerGSTPercentage(HttpServletRequest request, HttpServletResponse response, TripCommand command) throws IOException {
        HttpSession session = request.getSession();
        tripCommand = command;
        TripTO tripTO = new TripTO();

        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String responseValue = "";
            response.setContentType("text/html");
            responseValue = request.getParameter("billingState");
            tripTO.setBillingState(responseValue);
            ArrayList gstDetails = new ArrayList();
            gstDetails = tripBP.getGSTTaxPercentage(tripTO);
            TripTO gstTO = new TripTO();

            System.out.println("gstDetails.size() = " + gstDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = gstDetails.iterator();
            String gstPercentage = "";
            JSONObject jsonObject = new JSONObject();
            while (itr.hasNext()) {
                gstTO = (TripTO) itr.next();
                if ("IGST".equalsIgnoreCase(gstTO.getGstName())) {
                    gstPercentage = gstPercentage + "-" + gstTO.getGstPercentage();
                    System.out.println("IGST" + gstTO.getGstPercentage());
                    System.out.println("IGST" + gstPercentage);
                }
                if ("SGST".equalsIgnoreCase(gstTO.getGstName())) {
                    gstPercentage = gstPercentage + "-" + gstTO.getGstPercentage();
                    System.out.println("SGST" + gstTO.getGstPercentage());
                    System.out.println("IGST" + gstPercentage);
                }
                if ("CGST".equalsIgnoreCase(gstTO.getGstName())) {
                    gstPercentage = gstTO.getGstPercentage();
                    System.out.println("CGST" + gstTO.getGstPercentage());
                    System.out.println("IGST" + gstPercentage);
                }
            }
            jsonObject.put("Name", gstPercentage);
            System.out.println("jsonObject = " + jsonObject);
            jsonArray.put(jsonObject);
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getHireVendorTripExpense(HttpServletRequest request, HttpServletResponse response, TripCommand command) throws IOException {
        HttpSession session = request.getSession();
        tripCommand = command;
        TripTO tripTO = new TripTO();

        ArrayList vehicleNos = new ArrayList();
        String expesne = "";
        PrintWriter pw = response.getWriter();
        try {
            String vendorId = "", billingTypeId = "";
            String vehicleTypeId = "", orderId = "";
            response.setContentType("text/html");
            vendorId = request.getParameter("vendorId");
            vehicleTypeId = request.getParameter("vehicleTypeId");
            orderId = request.getParameter("orderId");
            billingTypeId = request.getParameter("billingTypeId");

            tripTO.setVendorId(vendorId);
            tripTO.setVehicleTypeId(vehicleTypeId);
            tripTO.setBillingType(billingTypeId);
            tripTO.setConsignmentId(orderId);
            System.out.println("vendorId" + vendorId + "vehicleTypeId" + vehicleTypeId + "orderId:" + orderId + "billingTypeId:" + billingTypeId);
            String roleId = "" + (Integer) session.getAttribute("RoleId");
            String companyId = (String) session.getAttribute("companyId");
            tripTO.setRoleId(roleId);
            tripTO.setCompanyId(companyId);

            expesne = tripBP.getHireVendorTripExpense(tripTO);
            System.out.println("expesne = " + expesne);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = vehicleNos.iterator();
            int cntr = 0;
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Id", expesne);
            System.out.println("jsonObject = " + jsonObject);
//            while (itr.hasNext()) {
//                tripTO = (TripTO) itr.next();
//
//
//                cntr++;
//            }
            jsonArray.put(jsonObject);
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getVendorVehicle(HttpServletRequest request, HttpServletResponse response, TripCommand command) throws IOException {
        HttpSession session = request.getSession();
        tripCommand = command;
        TripTO tripTO = new TripTO();

        ArrayList vehicleNos = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String vendorId = "";
            String vehicleTypeId = "", statusId = "";
            response.setContentType("text/html");
            vendorId = request.getParameter("vendorId");
            vehicleTypeId = request.getParameter("vehicleTypeId");
            statusId = request.getParameter("statusId");
            System.out.println("statusId====" + statusId);
            tripTO.setVendorId(vendorId);
            tripTO.setVehicleTypeId(vehicleTypeId);
            System.out.println("vendorId" + vendorId + "vehicleTypeId" + vehicleTypeId);
            String roleId = "" + (Integer) session.getAttribute("RoleId");
            String companyId = (String) session.getAttribute("companyId");
            tripTO.setRoleId(roleId);
            tripTO.setCompanyId(companyId);
            tripTO.setStatusId(statusId);
            vehicleNos = tripBP.getVehicleRegNos(tripTO);
            System.out.println("vehicleNos.size() = " + vehicleNos.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = vehicleNos.iterator();
            int cntr = 0;
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                tripTO = (TripTO) itr.next();
                jsonObject.put("Name", tripTO.getVehicleNo());
                jsonObject.put("Id", tripTO.getVehicleId() + "-" + tripTO.getVehicleNo() + "-" + tripTO.getVehicleTonnage() + "-" + tripTO.getPrimaryDriverId() + "-" + tripTO.getPrimaryDriverName() + "-" + tripTO.getDriverMobile() + "-" + "0" + "-" + tripTO.getVendorId() + "-" + tripTO.getOwnerShip() + "-" + tripTO.getPermitExpiryDate() + "-" + tripTO.getInsuranceExpiryDate() + "-" + tripTO.getRoadTaxExpiryDate() + "-" + tripTO.getFcExpiryDate());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
                cntr++;
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getDriverNames(HttpServletRequest request, HttpServletResponse response, TripCommand command) throws IOException {
        HttpSession session = request.getSession();
        tripCommand = command;
        TripTO tripTO = new TripTO();

        ArrayList drivers = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String responseValue = "";
            response.setContentType("text/html");
            responseValue = request.getParameter("driverName");
            System.out.println("responseValue=" + responseValue);
            tripTO.setDriverName(responseValue);
            drivers = tripBP.getDriverNames(tripTO);
            System.out.println("drivers.size() = " + drivers.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = drivers.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                tripTO = (TripTO) itr.next();
                jsonObject.put("Id", tripTO.getDriverId());
                jsonObject.put("Name", tripTO.getDriverName());
                jsonObject.put("Mobile", tripTO.getMobileNo());
                jsonObject.put("License", tripTO.getLicenseNo());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView updateEstimateFeright(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ModelAndView mv = null;
        String menuPath = "Operation  >>  View TripSheet ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            String InvoiceFlag = request.getParameter("InvoiceFlag");
            tripTO.setInvoiceFlag(InvoiceFlag);
            System.out.println("InvoiceFlag==" + InvoiceFlag);

//            String[] consignmentOrderId = request.getParameterValues("consignmentOrderId");
            String[] estimatedRevenue = request.getParameterValues("estimatedRevenue");
            String[] consignmentId = request.getParameterValues("consignmentId");

//            tripTO.setConsignmentOrderId(consignmentOrderId);
            tripTO.setUserId(userId);

            String[] tripIds = request.getParameterValues("tripId");
            String[] consignmentOrderIds = request.getParameterValues("selectedIndex");
            tripTO.setTripIds(tripIds);
            tripTO.setConsignmentOrderIds(consignmentId);

            int status = tripBP.updateEstimateFeright(tripTO, consignmentId, estimatedRevenue);

            System.out.println("status======" + status);

            String tripSheetId = request.getParameter("tripSheetId");
            String selectedCustomerId = request.getParameter("customerId");
            System.out.println("tripSheetId===" + tripSheetId);
            String reeferRequired = request.getParameter("reeferRequired");
            tripTO.setTripIds(tripIds);
            tripTO.setConsignmentOrderIds(consignmentId);
            tripTO.setCustomerId(selectedCustomerId);
            ArrayList tripsToBeBilledDetails = tripBP.getTripsToBeBilledDetails(tripTO);
            ArrayList tripsOtherExpenseDetails = tripBP.getTripsOtherExpenseDetails(tripTO);

            ArrayList gstDetails = new ArrayList();
            gstDetails = tripBP.getGSTTaxPercentage(tripTO);

            ArrayList companyList = new ArrayList();
            companyList = tripBP.getCompanyDetailsList(tripTO);
            request.setAttribute("companyList", companyList);

            ArrayList customerList = new ArrayList();
            customerList = tripBP.getCustomerDetailsList(tripTO);
            request.setAttribute("customerList", customerList);

            Iterator itrGst = gstDetails.iterator();
            TripTO gstTO = new TripTO();
            while (itrGst.hasNext()) {
                gstTO = (TripTO) itrGst.next();
                if ("IGST".equalsIgnoreCase(gstTO.getGstName())) {
                    request.setAttribute("igst", gstTO.getGstPercentage());
                    System.out.println("IGST" + gstTO.getGstPercentage());
                }
                if ("SGST".equalsIgnoreCase(gstTO.getGstName())) {
                    request.setAttribute("sgst", gstTO.getGstPercentage());
                    System.out.println("SGST" + gstTO.getGstPercentage());
                }
                if ("CGST".equalsIgnoreCase(gstTO.getGstName())) {
                    request.setAttribute("cgst", gstTO.getGstPercentage());
                    System.out.println("CGST" + gstTO.getGstPercentage());
                }
            }

            request.setAttribute("tripsToBeBilledDetails", tripsToBeBilledDetails);
            request.setAttribute("tripsOtherExpenseDetails", tripsOtherExpenseDetails);
            path = "content/trip/viewTripBillDetails.jsp";
            ArrayList tripDetails = new ArrayList();
            tripDetails = tripBP.getTripDetails(tripTO);

            request.setAttribute("reeferRequired", reeferRequired);
            request.setAttribute("tripSheetId", tripSheetId);
            request.setAttribute("tripDetails", tripDetails);
            System.out.println("tripIds.length" + tripIds.length);

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save trip sheet data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getOrderList(HttpServletRequest request, HttpServletResponse response, TripCommand command) throws IOException {
        String path = "";

        HttpSession session = request.getSession();
        PrintWriter pw = response.getWriter();

        TripTO tripTO = null;
        ArrayList OrderList = new ArrayList();
        try {
            int custId = 0;
            int tripId = 0;
            custId = Integer.parseInt(request.getParameter("custId"));
            tripId = Integer.parseInt(request.getParameter("tripId"));
            System.out.println("custId" + custId);
            System.out.println("tripId" + tripId);
            String returnValue = "";
            OrderList = tripBP.getOrderList(custId, tripId);
            Iterator itr = OrderList.iterator();
            while (itr.hasNext()) {
                tripTO = (TripTO) itr.next();
                returnValue = returnValue + "," + tripTO.getOrderDetails();
                System.out.println("returnValue" + returnValue);
            }
            pw.print(returnValue);

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to get problems-> " + exception);
            exception.printStackTrace();
        }

    }

    public void getVehicleLHCRegNos(HttpServletRequest request, HttpServletResponse response, TripCommand command) throws IOException {
        HttpSession session = request.getSession();
        tripCommand = command;
        TripTO tripTO = new TripTO();

        ArrayList vehicleNos = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String vendorId = "";
            String vehicleTypeId = "", statusId = "";
//            String lhcNo = "";
            String consignmentId = "";
            response.setContentType("text/html");
            vendorId = request.getParameter("vendorId");
            String vehicleCategory = request.getParameter("vehicleCategory");
            vehicleTypeId = request.getParameter("vehicleTypeId");
            consignmentId = request.getParameter("consignmentOrderId");
            System.out.println("consignmentId==" + consignmentId);
//            lhcNo = request.getParameter("lhcNo");
            statusId = request.getParameter("statusId");
//            System.out.println("lhcNo====" + lhcNo);
            tripTO.setVendorId(vendorId);
            tripTO.setVehicleTypeId(vehicleTypeId);
            tripTO.setConsignmentId(consignmentId);
            tripTO.setOwnerShip(vehicleCategory);
//            tripTO.setLhcNo(lhcNo);
            System.out.println("vendorId" + vendorId + "vehicleTypeId" + vehicleTypeId);
            String roleId = "" + (Integer) session.getAttribute("RoleId");
            String companyId = (String) session.getAttribute("companyId");
            tripTO.setRoleId(roleId);
            tripTO.setCompanyId(companyId);
            tripTO.setStatusId(statusId);
            vehicleNos = tripBP.getVehicleLHCRegNos(tripTO);
            System.out.println("vehicleNos.size() = " + vehicleNos.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = vehicleNos.iterator();
            int cntr = 0;
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                tripTO = (TripTO) itr.next();
//                jsonObject.put("Name", tripTO.getLhcNo() + "(" + tripTO.getRate() + ")");
//                jsonObject.put("Id", tripTO.getLhcId() + "~" + tripTO.getLhcNo() + "~" + tripTO.getVehicleNo() + "~" + tripTO.getRate() + "~" + tripTO.getlHCdate() + "~" + tripTO.getApprovalstatus());
                jsonObject.put("Name", tripTO.getVendorId());
                jsonObject.put("Id", tripTO.getTripAmount());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
                cntr++;
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getVendorVehicleType(HttpServletRequest request, HttpServletResponse response, TripCommand command) throws IOException {
        HttpSession session = request.getSession();
        tripCommand = command;
        TripTO tripTO = new TripTO();

        ArrayList vehicleType = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String vendorId = "";
            String originId = "";
            String destinationId = "";
            String ownerShip = "";

            response.setContentType("text/html");
            vendorId = request.getParameter("vendorId");
            originId = request.getParameter("originId");
            destinationId = request.getParameter("destinationId");
            ownerShip = request.getParameter("vehicleCategory");
            System.out.println("ownerShip " + ownerShip);
            String consignmentId = request.getParameter("consignmentId");

            tripTO.setConsignmentId(consignmentId);
            tripTO.setVendorId(vendorId);
            tripTO.setOriginId(originId);
            tripTO.setDestinationId(destinationId);
            tripTO.setOwnerShip(ownerShip);

            String vehicleTypeId = request.getParameter("vehicleTypeId");
            tripTO.setVehicleTypeId(vehicleTypeId);

            System.out.println("vendorId" + vendorId);
            String roleId = "" + (Integer) session.getAttribute("RoleId");
            String companyId = (String) session.getAttribute("companyId");
            tripTO.setRoleId(roleId);
            tripTO.setCompanyId(companyId);
            vehicleType = tripBP.getVendorVehicleType(tripTO);
            System.out.println("vehicleNos.size() = " + vehicleType.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = vehicleType.iterator();
            int cntr = 0;
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                tripTO = (TripTO) itr.next();
                jsonObject.put("Name", tripTO.getVehicleTypeName());
                jsonObject.put("Id", tripTO.getVehicleTypeId() + '-' + tripTO.getVehicleTonnage() + '~' + tripTO.getContractTypeId());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
                cntr++;
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getLhcVehicleRegNo(HttpServletRequest request, HttpServletResponse response, TripCommand command) throws IOException {
        HttpSession session = request.getSession();
        tripCommand = command;
        TripTO tripTO = new TripTO();

        ArrayList vehicleNoList = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();

        try {
            String vehicleNo = "";
            String vehicleTypeId = "", statusId = "";
            response.setContentType("text/html");

            vehicleTypeId = request.getParameter("vehicleTypeId");
            vehicleNo = request.getParameter("vehicleNo");
            String tripId = request.getParameter("tripId");
            String roleId = "" + (Integer) session.getAttribute("RoleId");
            String companyId = (String) session.getAttribute("companyId");

            tripTO.setVehicleTypeId(vehicleTypeId);
            tripTO.setVehicleNo(vehicleNo);
            tripTO.setRoleId(roleId);
            tripTO.setCompanyId(companyId);
            tripTO.setStatusId(statusId);
            tripTO.setTripId(tripId);

            System.out.println("vehicleTypeId" + vehicleTypeId);

            vehicleNoList = tripBP.getLhcVehicleRegNo(tripTO);

            JSONArray jsonArray = new JSONArray();
            Iterator itr = vehicleNoList.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                tripTO = (TripTO) itr.next();
                jsonObject.put("Name", tripTO.getVehicleNo());
                jsonObject.put("Id", tripTO.getId());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }

            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView handleTripSheetPrint(HttpServletRequest request, HttpServletResponse response, TripCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        ModelAndView mv = null;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList containerNoList = new ArrayList();
        TripTO tripTO = new TripTO();
        String menuPath = "";

        menuPath = "OPERATION >>PRINTTRIPGRN";
        String pageTitle = "PRINTTRIPGRN";
        String tripSheetId1 = "";
        String menuId = request.getParameter("menuId");

        if (menuId == null || "".equals(menuId)) {
            menuId = "0";
        }
        String printDocketNo = "";
        try {

            request.setAttribute("pageTitle", pageTitle);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            if ("null".equals(String.valueOf(request.getAttribute("tripSheetId"))) || String.valueOf(request.getAttribute("tripSheetId")) == null || "".equals(String.valueOf(request.getAttribute("tripSheetId")))) {
                tripSheetId1 = request.getParameter("tripSheetId");
                System.out.println("tripSheetId" + request.getParameter("tripSheetId"));
            } else {
                System.out.println("else" + request.getAttribute("tripSheetId"));
                tripSheetId1 = String.valueOf(request.getAttribute("tripSheetId"));
            }
            if ("null".equals(String.valueOf(request.getAttribute("printDocketNo"))) || String.valueOf(request.getAttribute("printDocketNo")) == null || "".equals(String.valueOf(request.getAttribute("printDocketNo")))) {
                printDocketNo = request.getParameter("printDocketNo");
                System.out.println("printDocketNo" + request.getParameter("printDocketNo"));
            } else {
                System.out.println("else" + request.getAttribute("printDocketNo"));
                printDocketNo = String.valueOf(request.getAttribute("printDocketNo"));
            }

            request.setAttribute("tripSheetId", tripSheetId1);
            System.out.println("tripSheetId for Gr :" + tripSheetId1);
            ArrayList tripDetailsList = tripBP.getPrintTripSheetDetails(tripSheetId1, printDocketNo);
            System.out.println("tripDetailsList=" + tripDetailsList.size());

            if (tripDetailsList.size() > 0) {
                TripTO tripTo = (TripTO) tripDetailsList.get(0);
                System.out.println("tripTo.getInvoiceDate() " + tripTo.getInvoiceDate());
                request.setAttribute("tripId", tripTo.getTripId());
                request.setAttribute("tripCode", tripTo.getTripCode());
                request.setAttribute("vehicleNo", tripTo.getRegNo());
                request.setAttribute("driverName", tripTo.getDriverName());
                request.setAttribute("driverMobile", tripTo.getDriverMobile());
                request.setAttribute("dlNo", tripTo.getDlNo());
                request.setAttribute("lrNumber", tripTo.getLrNumber());
                request.setAttribute("customerName", tripTo.getCustomerName());
                request.setAttribute("consignorGST", tripTo.getConsignorGST());
                request.setAttribute("consigneeGST", tripTo.getConsigneeGST());
                request.setAttribute("totalPackages", tripTo.getTotalPackages());
                request.setAttribute("consignmentNoteId", tripTo.getcNotes());
                request.setAttribute("invoiceDate", tripTo.getInvoiceDate());
                request.setAttribute("lrDate", tripTo.getFromDate());
                request.setAttribute("eWayBillNo", tripTo.geteWayBillNo());
                request.setAttribute("invoiceNumber", tripTo.getInvoiceNumber());
                request.setAttribute("invoiceValue", tripTo.getInvoiceValue());
                request.setAttribute("consignorName", tripTo.getConsignorName());
                request.setAttribute("consignorMobileNo", tripTo.getConsignorMobileNo());
                request.setAttribute("consignorAddress", tripTo.getConsignorAddress());
                request.setAttribute("consigneeName", tripTo.getConsigneeName());
                request.setAttribute("consigneeMobileNo", tripTo.getConsigneeMobileNo());
                request.setAttribute("consigneeAddress", tripTo.getConsigneeAddress());
                request.setAttribute("vehicleType", tripTo.getVehicleType());
                request.setAttribute("consignmentDate", tripTo.getConsignmentDate());
                request.setAttribute("packageWeight", tripTo.getPackageWeight());
                request.setAttribute("consignorContactPerson", tripTo.getConsignorContactPerson());
                request.setAttribute("consigneeContactPerson", tripTo.getConsigneeContactPerson());
                request.setAttribute("sealNo", tripTo.getSealNo());
                request.setAttribute("sealType", tripTo.getSealNoType());

            }

            String param = request.getParameter("value");
            System.out.println("param" + param);
            path = "content/trip/printTripsheetMoffusilFCLLCL.jsp";
            request.setAttribute("statusId", 10);
            mv = handleViewTripSheet(request, response, command);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleLHCPrint(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ModelAndView mv = null;
        String menuPath = "Operation  >>  Print LHC ";

        try {

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            String pageTitle = "Print LHC";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            String tripSheetId = request.getParameter("tripSheetId");
            tripTO.setTripSheetId(tripSheetId);
            System.out.println("tripSheetId : " + tripSheetId);

            ArrayList getLHCDetails = new ArrayList();
            getLHCDetails = tripBP.getLHCDetails(tripTO);
            request.setAttribute("LHCDetails", getLHCDetails);

            System.out.println("getLHCDetails : " + getLHCDetails.isEmpty());

            path = "content/trip/viewLHCDetails.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Print LHC Details data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveTripSheetForVehicleChange(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ModelAndView mv = null;
        String menuPath = "Operation  >>  View TripSheet ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            String actualFilePath = "";
            String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
            boolean isMultipart = false;
            Part partObj = null;
            FilePart fPart = null;
            int i = 0;
            int j = 0;
            int m = 0;
            int n = 0;
            int p = 0;
            int s = 0;

            String[] fileSaved = new String[10];
            String[] uploadedFileName = new String[10];
            String[] tempFilePath = new String[10];

            String tripId = "";
            String actionName = "";
            String vehicleCapUtil = "";
            String actionRemarks = "";

            String vehicleId = "";
            String driver1Id = "";
            String driver2Id = "";
            String driver3Id = "";

            String lastVehicleEndKm = "";
            String driverMobile = "";
            String vehicleStartKm = "";
            String lastVehicleEndOdometer = "";
            String vehicleStartOdometer = "";
            String cityId = "";
            String vehicleChangeDate = "";
            String vehicleChangeMinute = "";
            String vehicleChangeHour = "";
            String vehicleRemarks1 = "";
            String vehicleNo = "";
            String vehicleNo1 = "";
            String oldVehicleId = "";
            String ownVehicleDriver = "";
            String originId = "";
            String destinationId = "";
            String tripType = "";
            String statusId = "";
            String vehicleRemarks = "";
            String lhcId = "";

            String sealnotype = "";
            String sealno = "";
            String ewaybillDate = "";
            String ewaybillTime = "";
            String ewaybillHour = "";
            String ewaybillMinute = "";
            String insuranceSurvey = "";
            String consignmentId = "";

            isMultipart = org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        //actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                        actualServerFilePath = getServletContext().getRealPath("/LHCFiles/Files");
                        System.out.println("actualServerFilePath : " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        Date now = new Date();
                        String date = now.getDate() + "" + (now.getMonth() + 1) + "" + (now.getYear() + 1900);
                        String time = now.getHours() + "" + now.getMinutes() + "" + now.getSeconds() + userId;
                        fPart = (FilePart) partObj;
                        uploadedFileName[j] = fPart.getFileName();

                        if (!"".equals(uploadedFileName[j]) && uploadedFileName[j] != null) {
                            //System.out.println("partObj.getName() = " + partObj.getName());
                            String[] splitFileName = uploadedFileName[j].split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            //System.out.println("fileSavedAs = " + fileSavedAs);
                            fileSaved[j] = splitFileName[0] + date + time + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath[j] = tempServerFilePath + "/" + fileSaved[j];
                            System.out.println("tempServerFilePath..." + tempServerFilePath);
                            System.out.println("filename..." + fileSaved[j]);
                            System.out.println("tempPath..." + tempFilePath[j]);
                            actualFilePath = actualServerFilePath;
                            System.out.println("actPath..." + actualServerFilePath);
                            System.out.println("actualFilePath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(tempFilePath[j]));
                            File f1 = new File(actualFilePath);
                            f1.renameTo(new File(tempFilePath[j]));
                        }

                        j++;
                    } else if (partObj.isParam()) {

                        if (partObj.getName().equals("tripId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            tripId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("actionName")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            actionName = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vehicleCapUtil")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vehicleCapUtil = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("actionRemarks")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            actionRemarks = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("driver1Id")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            driver1Id = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("driver2Id")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            driver2Id = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("driver3Id")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            driver3Id = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("lastVehicleEndKm")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            lastVehicleEndKm = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vehicleStartKm")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vehicleStartKm = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("lastVehicleEndOdometer")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            lastVehicleEndOdometer = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vehicleStartOdometer")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vehicleStartOdometer = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("cityId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            cityId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vehicleChangeDate")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vehicleChangeDate = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vehicleChangeMinute")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vehicleChangeMinute = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vehicleChangeHour")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vehicleChangeHour = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vehicleRemarks")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vehicleRemarks = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vehicleNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vehicleNo = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vehicleNo1")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vehicleNo1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("driver1Name")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            ownVehicleDriver = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("originId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            originId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("destinationId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            destinationId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("tripType")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            tripType = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("statusId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            statusId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("ewayBillDate")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            ewaybillDate = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("ewaybillHour")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            ewaybillHour = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("ewaybillMinute")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            ewaybillMinute = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("isSurveyReq")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            insuranceSurvey = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("sealnotype")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            sealnotype = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("sealNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            sealno = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("consignmentId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            consignmentId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("oldVehicleId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            oldVehicleId = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("oldVehicleNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vehicleNo1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vehicleNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vehicleNo = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("vehicleId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vehicleId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("driverMobile")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            driverMobile = paramPart.getStringValue();
                        }

                    }
                }
            }

            vehicleRemarks = vehicleRemarks1 + "Change Vehicle" + vehicleNo1 + " to " + vehicleNo;

            tripTO.setVehicleRemarks(vehicleRemarks);
            tripTO.setVehicleChangeDate(vehicleChangeDate);
            tripTO.setVehicleChangeHour(vehicleChangeHour);
            tripTO.setVehicleChangeMinute(vehicleChangeMinute);
            tripTO.setLastVehicleEndOdometer(lastVehicleEndOdometer);
            tripTO.setVehicleStartOdometer(vehicleStartOdometer);
            tripTO.setCityId(cityId);
            tripTO.setLastVehicleEndKm(lastVehicleEndKm);
            tripTO.setVehicleStartKm(vehicleStartKm);
            tripTO.setPrimaryDriverName(ownVehicleDriver);

            tripTO.setVehicleId(vehicleId);
            tripTO.setOldVehicleId(oldVehicleId);
            tripTO.setVehicleNo(vehicleNo);

            System.out.println("veh id:" + vehicleId);
            System.out.println("old veh id:" + oldVehicleId);
            System.out.println("vehicleNo :" + vehicleNo);

            tripTO.setPrimaryDriverId(driver1Id);
            tripTO.setVehicleCapUtil(vehicleCapUtil);
            tripTO.setSecondaryDriver1Id(driver2Id);
            tripTO.setSecondaryDriver2Id(driver3Id);
            tripTO.setTripId(tripId);
            tripTO.setActionName(actionName);
            tripTO.setActionRemarks(actionRemarks);
            tripTO.setUserId(userId);
            tripTO.setDriverMobile(driverMobile);

            tripTO.setSealNoType(sealnotype);
            tripTO.setSealNo(sealno);

            if ("1".equalsIgnoreCase(actionName)) {
                tripTO.setStatusId("8");//freeze
            } else if ("2".equalsIgnoreCase(actionName)) {
                tripTO.setStatusId("6");//unfreeze
            } else {
                tripTO.setStatusId("7");//vehicle Allocated
            }

            tripTO.setOriginId(originId);
            tripTO.setDestinationId(destinationId);

            tripTO.setEwayBillDate(ewaybillDate);

            ewaybillTime = ewaybillHour + ":" + ewaybillMinute + ":00";

            tripTO.setEwayBillTime(ewaybillTime);
            tripTO.setInsuranceSurvey(insuranceSurvey);
            tripTO.setConsignmentId(consignmentId);
            tripTO.setLhcId(lhcId);

            int index = j;
            String[] file = new String[index];
            String[] saveFile = new String[index];

            for (int x = 0; x < j; x++) {
                //System.out.println("tempFilePath[x] = " + tempFilePath[x]);
                file[x] = tempFilePath[x];
                System.out.println("File[x] = " + file[x]);
                saveFile[x] = fileSaved[x];
                System.out.println("filename[x] = " + saveFile[x]);
            }

            int status = tripBP.saveTripSheetForVehicleChange(tripTO, saveFile);

            if (status != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Info Updated Successfully....");
            }

            request.setAttribute("tripType", tripType);
            request.setAttribute("statusId", statusId);

            path = "content/trip/viewTripSheet.jsp";
            // path = "content/redirectPage.jsp";
            mv = handleViewTripSheet(request, response, command);

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save trip sheet data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
//        return new ModelAndView(path);
        return mv;
    }

    public ModelAndView consignmentApproveRequest(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ModelAndView mv = null;
        String menuPath = "Operation  >>  View TripSheet ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            String[] consignmentOrderId = request.getParameterValues("consignmentOrderId");
            String lhcId = request.getParameter("lhcId");

            tripTO.setConsignmentOrderId(consignmentOrderId);
            tripTO.setUserId(userId);
            tripTO.setLhcId(lhcId);

            int status = tripBP.consignmentApproveRequest(tripTO);
            System.out.println("status :" + status);

            if (status != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Status Updated Successfully....");
            }

            path = "BrattleFoods/viewTripSheet.jsp";
            mv = handleViewTripSheet(request, response, command);

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save trip sheet data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView consignmentApproval(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  Consignment Details";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "consignment Approval";
            request.setAttribute("pageTitle", pageTitle);
            TripTO tripTO = new TripTO();
            Date dNow = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(dNow);
            cal.add(Calendar.DATE, 0);
            dNow = cal.getTime();

            Date dNow1 = new Date();
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(dNow1);
            cal1.add(Calendar.DATE, -6);
            dNow1 = cal1.getTime();

            SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy");
            String today = ft.format(dNow);
            String fromday = ft.format(dNow1);
            System.out.println("the from date is " + fromday);

            if (tripCommand.getFromDate() != null && tripCommand.getFromDate() != "") {
                tripTO.setFromDate(tripCommand.getFromDate());
            } else {
                tripTO.setFromDate(fromday);
            }
            if (tripCommand.getToDate() != null && tripCommand.getToDate() != "") {
                tripTO.setToDate(tripCommand.getToDate());
            } else {
                tripTO.setToDate(today);
            }
            System.out.println("the todate is " + today);

            ArrayList consignmentApproval = new ArrayList();
            consignmentApproval = tripBP.consignmentApproval(tripTO);
            System.out.println("consignmentApproval.size() : " + consignmentApproval.size());

            request.setAttribute("consignmentApproval", consignmentApproval);
            request.setAttribute("fromdate", tripTO.getFromDate());
            request.setAttribute("todate", tripTO.getToDate());

            path = "content/trip/viewConsignmentRequestList.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewConsignmentApproval(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  Consignment Details";
        TripTO tripTO = new TripTO();

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorizsaveEmptyTripApprovaled.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Consignment Approval";
            request.setAttribute("pageTitle", pageTitle);

            String consId = request.getParameter("consId");
            tripTO.setConsignmentId(consId);

            ArrayList viewConsignmentApproval = new ArrayList();
            viewConsignmentApproval = tripBP.viewConsignmentApprovalDetails(tripTO);

            String vendordetail = tripBP.getConsignVendorDetail(tripTO);
            String[] venVal = vendordetail.split("~");

            request.setAttribute("viewConsignmentApproval", viewConsignmentApproval);
            request.setAttribute("vendorName", venVal[0]);
            request.setAttribute("expense", venVal[1]);

            path = "content/trip/viewConsignmentApproval.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveConsignmentApproval(HttpServletRequest request, HttpServletResponse response, TripCommand command) {

        HttpSession session = request.getSession();
        String path = "";
        ModelAndView mv = null;

        String menuPath = "Operation  >>  Consignment Approval ";

        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Consignment Approve ";
            request.setAttribute("pageTitle", pageTitle);

            int userId = 0;
            if (session.getAttribute("userId") != null && !"".equals(session.getAttribute("userId"))) {
                userId = (Integer) session.getAttribute("userId");
            }

            System.out.println("userId = " + userId);

            String consignmentNote = "";
            String approvalStatus = "";

            consignmentNote = request.getParameter("consignmentNote");
            approvalStatus = request.getParameter("approvestatus");

            System.out.println("consignmentNote : " + consignmentNote + " approvalStatus : " + approvalStatus);

            int checkeEmptyTripApproval = tripBP.saveConsignmentApproval(consignmentNote, approvalStatus, userId + "");
            System.out.println("checkeEmptyTripApproval : " + checkeEmptyTripApproval);

            path = "BrattleFoods/viewTripSheet.jsp";

            if (checkeEmptyTripApproval != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Status Updated Successfully.");
            }

            mv = consignmentApproval(request, response, command);

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView handleTouchPoint(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ModelAndView mv = null;
        String menuPath = "Operation  >>  Print LHC ";

        try {

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            String pageTitle = "Print LHC";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            String tripSheetId = request.getParameter("tripSheetId");
            tripTO.setTripSheetId(tripSheetId);
            request.setAttribute("tripSheetId", tripSheetId);
            System.out.println("tripSheetId : " + tripSheetId);

            ArrayList getLHCDetails = new ArrayList();
            getLHCDetails = tripBP.getLHCDetails(tripTO);
            request.setAttribute("LHCDetails", getLHCDetails);

            ArrayList getLrNumber = new ArrayList();
            getLrNumber = tripBP.getLrNumber(tripSheetId);
            request.setAttribute("getLrNumber", getLrNumber);

            System.out.println("getLHCDetails : " + getLHCDetails.isEmpty());

            path = "content/trip/editTouchPoint.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Print LHC Details data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView updateTouctPoind(HttpServletRequest request, HttpServletResponse response, TripCommand command) throws IOException {
        HttpSession session = request.getSession();
        tripCommand = command;
        TripTO tripTO = new TripTO();
        int userId = (Integer) session.getAttribute("userId");
        ModelAndView mv = null;

        ArrayList vehicleNoList = new ArrayList();
        String path = "";
        String tripSheetId = "";
        PrintWriter pw = response.getWriter();
        try {
            if (tripCommand.getTripSheetId() != null && !"".equals(tripCommand.getTripSheetId())) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
                tripSheetId = tripCommand.getTripSheetId();
                System.out.println("tripCommand.getTripSheetId()=" + tripCommand.getTripSheetId());
            }
            if (tripCommand.getDocketNo() != null && !"".equals(tripCommand.getDocketNo())) {
                tripTO.setDocketNo(tripCommand.getDocketNo());
                System.out.println("tripCommand.getDocketNo()=" + tripCommand.getDocketNo());
            }
            if (tripCommand.getInvoiceNumber() != null && !"".equals(tripCommand.getInvoiceNumber())) {
                tripTO.setInvoiceNumber(tripCommand.getInvoiceNumber());
            }
            if (tripCommand.getInvoiceValue() != null && !"".equals(tripCommand.getInvoiceValue())) {
                tripTO.setInvoiceValue(tripCommand.getInvoiceValue());
                System.out.println("tripCommand.getInvoiceValue()=" + tripCommand.getInvoiceValue());
            }
            if (tripCommand.getInvoiceDate() != null && !"".equals(tripCommand.getInvoiceDate())) {
                tripTO.setInvoiceDate(tripCommand.getInvoiceDate());
            }

            ArrayList getLrNumber = new ArrayList();
            getLrNumber = tripBP.getLrNumber(tripSheetId);
            request.setAttribute("getLrNumber", getLrNumber);

            String insert = tripBP.insertTouchPoint(tripTO, userId);
            System.out.println("insert==" + insert);
            if (!insert.equals("")) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Invoice Details Updated Successfully");
            }
            request.setAttribute("printDocketNo", insert);
            mv = handleTripSheetPrint(request, response, command);

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
        return mv;
    }

    public ModelAndView viewVehicleLocation(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Finance  >>  view Vehicle Location ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Vehicle Location";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            String vehicleNo = request.getParameter("vehicleNo");
            tripTO.setVehicleNo(vehicleNo);
            request.setAttribute("vehicleNo", vehicleNo);
            System.out.println("vehicleNo :" + vehicleNo);
            ArrayList viewVehicleLocation = new ArrayList();
            viewVehicleLocation = tripBP.viewVehicleLocation(tripTO);
            request.setAttribute("viewVehicleLocation", viewVehicleLocation);

            path = "content/trip/viewVehicleLocation.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve vehicle location data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getConsignorDetails(HttpServletRequest request, HttpServletResponse response, TripCommand command) throws IOException {
        HttpSession session = request.getSession();
        tripCommand = command;
        TripTO tripTO = new TripTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList consignorDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String originId = "";
            String customerId = "";
            response.setContentType("text/html");
            originId = request.getParameter("originId");
            customerId = request.getParameter("customerId");
            tripTO.setOriginId(originId);
            tripTO.setCustomerId(customerId);
            consignorDetails = tripBP.getConsignorDetails(tripTO);
            System.out.println("consignorDetails.size() = " + consignorDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = consignorDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                tripTO = (TripTO) itr.next();
                jsonObject.put("Temp", tripTO.getTemp());
                jsonObject.put("Id", tripTO.getConsignorId());
                jsonObject.put("Name", tripTO.getConsignorName());
                jsonObject.put("Mobile", tripTO.getConsignorPhoneNo());
                jsonObject.put("Address", tripTO.getConsignorAddress());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView updateStartTripSheetDetails(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        ModelAndView mv = null;
        tripCommand = command;
        String menuPath = "Operation  >>  Edit Start Trip Sheet ";
        String insertStatus = "";
        int insertStatus1 = 0;
        int insertStatus2 = 0;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            String tripSheetId = tripCommand.getTripSheetId();
            System.out.println("tripSheetId=" + tripSheetId);

            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
                tripTO.setTripId(tripCommand.getTripSheetId());
            }
            if (tripCommand.getStartDate() != null && !tripCommand.getStartDate().equals("")) {
                tripTO.setStartDate(tripCommand.getStartDate());
            }
            if (tripCommand.getStartOdometerReading() != null && !tripCommand.getStartOdometerReading().equals("")) {
                tripTO.setStartOdometerReading(tripCommand.getStartOdometerReading());
            }
            if (tripCommand.getTripStartHour() != null && !tripCommand.getTripStartHour().equals("")) {
                tripTO.setTripStartHour(tripCommand.getTripStartHour());
            }
            if (tripCommand.getTripStartMinute() != null && !tripCommand.getTripStartMinute().equals("")) {
                tripTO.setTripStartMinute(tripCommand.getTripStartMinute());
            }
            if (tripCommand.getStartHM() != null && !tripCommand.getStartHM().equals("")) {
                tripTO.setStartHM(tripCommand.getStartHM());
            }
            if (tripCommand.getStartTripRemarks() != null && !tripCommand.getStartTripRemarks().equals("")) {
                tripTO.setStartTripRemarks(tripCommand.getStartTripRemarks());
            }
            if (tripCommand.getLrNumber() != null && !tripCommand.getLrNumber().equals("")) {
                tripTO.setLrNumber(tripCommand.getLrNumber());
            }
            if (tripCommand.getFuelLtr() != null && !tripCommand.getFuelLtr().equals("")) {
                tripTO.setFuelLtr(tripCommand.getFuelLtr());
            }
            //Muthu here starts...
            if (tripCommand.getSealNoType() != null && !tripCommand.getSealNoType().equals("")) {
                tripTO.setSealNoType(tripCommand.getSealNoType());
            }
            if (tripCommand.getSealNo() != null && !tripCommand.getSealNo().equals("")) {
                tripTO.setSealNo(tripCommand.getSealNo());
            }
            if (tripCommand.getVendorId() != null && !tripCommand.getVendorId().equals("")) {
                tripTO.setVendorId(tripCommand.getVendorId());
            }
            if (tripCommand.getVehicleTypeId() != null && !tripCommand.getVehicleTypeId().equals("")) {
                tripTO.setVehicleTypeId(tripCommand.getVehicleTypeId());
            }
            if (tripCommand.getVehicleId() != null && !tripCommand.getVehicleId().equals("")) {
                tripTO.setVehicleId(tripCommand.getVehicleId());
            }
            if (tripCommand.getVehicleNo() != null && !tripCommand.getVehicleNo().equals("")) {
                tripTO.setVehicleNo(tripCommand.getVehicleNo());
            }
            if (tripCommand.getDriverName() != null && !tripCommand.getDriverName().equals("")) {
                tripTO.setDriverName(tripCommand.getDriverName());
            }
            if (tripCommand.getDriverMobile() != null && !tripCommand.getDriverMobile().equals("")) {
                tripTO.setDriverMobile(tripCommand.getDriverMobile());
            }
            if (tripCommand.getDlNumber() != null && !tripCommand.getDlNumber().equals("")) {
                tripTO.setDlNumber(tripCommand.getDlNumber());
            }
            if (tripCommand.geteWayBillNo() != null && !tripCommand.geteWayBillNo().equals("")) {
                tripTO.seteWayBillNo(tripCommand.geteWayBillNo());
            }
            if (tripCommand.getInvoiceNumber() != null && !tripCommand.getInvoiceNumber().equals("")) {
                tripTO.setInvoiceNumber(tripCommand.getInvoiceNumber());
            }
            if (tripCommand.getInvoiceValue() != null && !tripCommand.getInvoiceValue().equals("")) {
                tripTO.setInvoiceValue(tripCommand.getInvoiceValue());
            }
            if (tripCommand.getInvoiceDate() != null && !tripCommand.getInvoiceDate().equals("")) {
                tripTO.setInvoiceDate(tripCommand.getInvoiceDate());
            }
            if (tripCommand.getPackageWeight() != null && !tripCommand.getPackageWeight().equals("")) {
                tripTO.setPackageWeight(tripCommand.getPackageWeight());
            }

            //compute trip plan end date and time
            ///Arun  start//////////////
            if (tripCommand.getProductCodes() != null && !"".equals(tripCommand.getProductCodes())) {
                tripTO.setProductCodes(tripCommand.getProductCodes());
            }
            if (tripCommand.getProductNames() != null && !"".equals(tripCommand.getProductNames())) {
                tripTO.setProductNames(tripCommand.getProductNames());
            }
            if (tripCommand.getProductbatch() != null && !"".equals(tripCommand.getProductbatch())) {
                tripTO.setProductbatch(tripCommand.getProductbatch());
            }
            if (tripCommand.getPackagesNos() != null && !"".equals(tripCommand.getPackagesNos())) {
                tripTO.setPackagesNos(tripCommand.getPackagesNos());
            }
            if (tripCommand.getWeights() != null && !"".equals(tripCommand.getWeights())) {
                tripTO.setWeights(tripCommand.getWeights());
            }
            if (tripCommand.getProductuom() != null && !"".equals(tripCommand.getProductuom())) {
                tripTO.setProductuom(tripCommand.getProductuom());
            }

            if (tripCommand.getConsignmentId() != null && !"".equals(tripCommand.getConsignmentId())) {
                tripTO.setConsignmentId(tripCommand.getConsignmentId());
            }

            if (tripCommand.getVehicleactreportdate() != null && !"".equals(tripCommand.getVehicleactreportdate())) {
                tripTO.setVehicleactreportdate(tripCommand.getVehicleactreportdate());
            }
            if (tripCommand.getVehicleactreporthour() != null && !"".equals(tripCommand.getVehicleactreporthour())) {
                tripTO.setVehicleactreporthour(tripCommand.getVehicleactreporthour());
            }
            if (tripCommand.getVehicleactreportmin() != null && !"".equals(tripCommand.getVehicleactreportmin())) {
                tripTO.setVehicleactreportmin(tripCommand.getVehicleactreportmin());
            }
            if (tripCommand.getVehicleloadreportdate() != null && !"".equals(tripCommand.getVehicleloadreportdate())) {
                tripTO.setVehicleloadreportdate(tripCommand.getVehicleloadreportdate());
            }
            if (tripCommand.getVehicleloadreporthour() != null && !"".equals(tripCommand.getVehicleloadreporthour())) {
                tripTO.setVehicleloadreporthour(tripCommand.getVehicleloadreporthour());
            }
            if (tripCommand.getVehicleloadreportmin() != null && !"".equals(tripCommand.getVehicleloadreportmin())) {
                tripTO.setVehicleloadreportmin(tripCommand.getVehicleloadreportmin());
            }
            if (tripCommand.getVehicleloadtemperature() != null && !"".equals(tripCommand.getVehicleloadtemperature())) {
                tripTO.setVehicleloadtemperature(tripCommand.getVehicleloadtemperature());
            }
            if (tripCommand.getOriginDetention() != null && !"".equals(tripCommand.getOriginDetention())) {
                tripTO.setOriginDetention(tripCommand.getOriginDetention());
            }
            if (tripCommand.getCustomerId() != null && !"".equals(tripCommand.getCustomerId())) {
                tripTO.setCustomerId(tripCommand.getCustomerId());
            }

            if (tripCommand.getLrNumber() != null && !"".equals(tripCommand.getLrNumber())) {
                tripTO.setLrNumber(tripCommand.getLrNumber());
            }

            if (tripCommand.getDeviceId() != null && !tripCommand.getDeviceId().equals("")) {
                tripTO.setDeviceId(tripCommand.getDeviceId());
            }

            if (tripCommand.getLoadedpackages() != null && !"".equals(tripCommand.getLoadedpackages())) {
                tripTO.setLoadedpackages(tripCommand.getLoadedpackages());
            }

            if (tripCommand.getDocumentRecivedDate() != null && !tripCommand.getDocumentRecivedDate().equals("")) {
                tripTO.setDocumentRecivedDate(tripCommand.getDocumentRecivedDate());
            }

            if (tripCommand.getDocumentRecivedHr() != null && !tripCommand.getDocumentRecivedHr().equals("")) {
                tripTO.setDocumentRecivedHr(tripCommand.getDocumentRecivedHr());
            }
            if (tripCommand.getDocumentRecivedMi() != null && !tripCommand.getDocumentRecivedMi().equals("")) {
                tripTO.setDocumentRecivedMi(tripCommand.getDocumentRecivedMi());
            }

            String routeCourseId = request.getParameter("routeCourseId");
            tripTO.setRouteId(routeCourseId);
            ///Arun  start//////////////

            String estimatedTransitHours = request.getParameter("estimatedTransitHours");
            String estimatedTripEndDateTime = tripBP.getEstimatedTripEndDateTime(tripTO.getTripSheetId());
            String[] temp = estimatedTripEndDateTime.split("~");
            String tripPlanEndDate = "";
            String tripPlanEndTime = "";
            tripPlanEndDate = temp[0];
            tripPlanEndTime = temp[1];
            System.out.println("tripPlanEndDate:" + tripPlanEndDate);
            System.out.println("tripPlanEndTime:" + tripPlanEndTime);
            System.out.println("start date:" + tripTO.getStartDate());
            System.out.println("estimatedTransitHours:" + estimatedTransitHours);
            if ("00-00-0000".equals(tripPlanEndDate) || "".equals(tripPlanEndDate)) {
                if ("00:00:00".equals(tripPlanEndTime)) {
                    tripPlanEndTime = "";
                }
                DateFormat formatter;
                Date tripStartDate;
                formatter = new SimpleDateFormat("dd-MM-yyyy");
                tripStartDate = formatter.parse(tripTO.getStartDate());

                Calendar cal = Calendar.getInstance(); // creates calendar
                cal.setTime(tripStartDate); // sets calendar time/date
                int tripStartHours = Integer.parseInt(tripTO.getTripStartHour()) + (Integer.parseInt(tripTO.getTripStartMinute()) / 60);
                cal.add(Calendar.HOUR_OF_DAY, tripStartHours); // adds trip start hour
                cal.add(Calendar.HOUR_OF_DAY, (int) Float.parseFloat(estimatedTransitHours)); // adds transit hours
                Date tripPlanEndDateDt = cal.getTime(); // returns new date object, one hour in the future
                tripPlanEndDate = formatter.format(tripPlanEndDateDt);
                SimpleDateFormat sdf = new SimpleDateFormat("kk:mm:ss");
                tripPlanEndTime = sdf.format(tripPlanEndDateDt);
                if ("24:00:00".equals(tripPlanEndTime)) {
                    tripPlanEndTime = "00:00:00";
                }
            }

            //load details
            String[] loadOrderId = request.getParameterValues("loadOrderId");
            String[] pendingPkgs = request.getParameterValues("pendingPkgs");
            String[] pendingWeight = request.getParameterValues("pendingWeight");
            String[] loadedPkgs = request.getParameterValues("loadedPkgs");
            String[] loadedWeight = request.getParameterValues("loadedWeight");
            String totalPackages = request.getParameter("totalPackages");

            tripTO.setLoadOrderId(loadOrderId);
            tripTO.setPendingPkgs(pendingPkgs);
            tripTO.setPendingWeight(pendingWeight);
            tripTO.setLoadedPkgs(loadedPkgs);
            tripTO.setLoadedWeight(loadedWeight);
            tripTO.setTotalPackages(totalPackages);

            System.out.println("tripPlanEndDate:" + tripPlanEndDate);
            System.out.println("tripPlanEndTime:" + tripPlanEndTime);
            tripTO.setTripPlanEndDate(tripPlanEndDate);
            tripTO.setTripPlanEndTime(tripPlanEndTime);
            tripTO.setStatusId("10"); //trip started
           
            
            String driverName=request.getParameter("driverName");
            System.out.println("driverName==="+driverName);
            tripTO.setDriverName(driverName);
            String escortName=request.getParameter("escortName");
            System.out.println("escortName==="+escortName);
            tripTO.setEscortName(escortName);
            String escortMobile=request.getParameter("escortMobile");
            System.out.println("escortName==="+escortName);
            tripTO.setEscortMobile(escortMobile);
            
            String materialDesc=request.getParameter("materialDesc");
            System.out.println("materialDesc=="+materialDesc);
            tripTO.setMaterialDesc(materialDesc);
            String insuranceDetail=request.getParameter("insuranceDetail");
            System.out.println("insuranceDetail=="+insuranceDetail);
            tripTO.setInsuranceDetail(insuranceDetail);
            String appDate=request.getParameter("appDate");
            System.out.println("appDate=="+appDate);
            tripTO.setAppDate(appDate);
            String appHour=request.getParameter("appHour");
            System.out.println("appHour=="+appHour);
            tripTO.setAppHour(appHour);
            String appMin=request.getParameter("appMin");
            System.out.println("appMin=="+appMin);
            tripTO.setAppMin(appMin);
            String tatTime=request.getParameter("tatTime");
            System.out.println("tatTime=="+tatTime);
            tripTO.setTatTime(tatTime);

            insertStatus = tripBP.updateStartTripSheet(tripTO, userId);
            System.out.println("update trip sheet insert status===" + insertStatus);
            insertStatus1 = tripBP.insertLoadingUnloadingDetails(tripTO, userId);
            insertStatus2 = tripBP.insertFuelLiter(tripTO, userId);

            // path = "content/trip/startTripSheet.jsp";
            //  mv = handleViewStartTripSheet(request, response, command);
            tripSheetId = request.getParameter("tripSheetId");
            tripTO.setTripSheetId(tripSheetId);
            String printDocketNo = request.getParameter("lrNumber");
            System.out.println("printDocketNo : " + printDocketNo);
            System.out.println("tripSheetId : " + tripSheetId);

            String UpdateSaveTripDetails = tripBP.UpdateSaveTripDetails(tripTO, userId);
            System.out.println("UpdateSaveTripDetails" + UpdateSaveTripDetails);

            request.setAttribute("tripType", "1");
            request.setAttribute("statusId", "10");
            request.setAttribute("tripSheetId", tripSheetId);
            request.setAttribute("UpdateSaveTripDetails", Integer.parseInt(UpdateSaveTripDetails));

            //mv = handleViewStartTripSheet(request, response, command);
            path = "content/redirectPage.jsp";
            request.setAttribute("successMessage", " Trip Updated Successfully....");

            String responsiveStatus = request.getParameter("respStatus");
            String tripType = request.getParameter("tripType");
            String statusId = request.getParameter("statusId");
            request.setAttribute("tripType", tripType);
            request.setAttribute("statusId", statusId);
            request.setAttribute("tripSheetId", tripSheetId);
            request.setAttribute("tripId", tripSheetId);

            if (responsiveStatus != null && "Y".equals(responsiveStatus)) {
                request.setAttribute("respStatus", "Y");
                request.setAttribute("printDocketNo", printDocketNo);
                mv = handleTripSheetPrint(request, response, command);
            } else {
                request.setAttribute("respStatus", "Y");
                request.setAttribute("printDocketNo", printDocketNo);
                mv = handleTripSheetPrint(request, response, command);
            }

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
//        return new ModelAndView(path);
        return mv;
    }

    public void checkDocketNoIsExist(HttpServletRequest request, HttpServletResponse response, TripCommand command) throws IOException {
        HttpSession session = request.getSession();
        tripCommand = command;
        TripTO tripTO = new TripTO();

        String path = "";
        PrintWriter pw = response.getWriter();
        String suggestions = "";

        try {
            String lrNumber = "";
            String statusId = "";
            response.setContentType("text/html");
            lrNumber = request.getParameter("lrNumber");
            tripTO.setLrNumber(lrNumber);
            System.out.println("lrNumber" + lrNumber);
            String roleId = "" + (Integer) session.getAttribute("RoleId");
            String companyId = (String) session.getAttribute("companyId");
            tripTO.setRoleId(roleId);
            tripTO.setCompanyId(companyId);
            tripTO.setStatusId(statusId);

            suggestions = tripBP.checkDocketNoIsExist(tripTO);
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(suggestions);
            writer.close();

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView podDownload(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ModelAndView mv = null;
        String menuPath = "Operation  >>  Download POD ";
        TripTO tripTO = new TripTO();
        try {

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            String pageTitle = "Download POD";
            request.setAttribute("pageTitle", pageTitle);

            String[] tripIds = request.getParameterValues("podCopy");
            System.out.println("tripIds : " + tripIds);

            if (tripIds != null) {
                for (int i = 0; i < tripIds.length; i++) {
                    String tripId = tripIds[i];
                    System.out.println("tripId :" + tripId);
                    String[] trpId = tripId.split(",");
                    tripTO.setTripIds(trpId);
                }
            }

            ArrayList viewPODDetails = new ArrayList();
            viewPODDetails = tripBP.getPODUploadDetails(tripTO);
            if (viewPODDetails.size() > 0) {
                request.setAttribute("viewPODDetails", viewPODDetails);
            }

            path = "content/trip/podDownload.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Print LHC Details data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    
        public ModelAndView cancelTrip(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        tripCommand = command;
        String menuPath = "";
        ModelAndView mv = null;
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  Route Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            String tripSheetId = request.getParameter("tripSheetId");
            String vehicleId = request.getParameter("vehicleId");
            String tripStatusId = request.getParameter("tripStatusId");
            String cancelRemarks = request.getParameter("cancelRemarks");
            int userId = (Integer) session.getAttribute("userId");
            System.out.println("userId = " + userId);
            int cancelTrip = tripBP.cancelTrip(tripSheetId, tripStatusId, cancelRemarks, userId);
            
            mv = new ModelAndView("viewTripSheetDetails.do?tripId=" + tripSheetId + "&vehicleId=" + vehicleId);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

        
        
        public ModelAndView handleEcomTrips(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        tripCommand = command;
        String menuPath = "Operation  >>  View Trip Sheet ";
        String statusId1 = request.getParameter("statusId");
        System.out.println("getParameter value:" + request.getParameter("successMessage"));
        System.out.println("getAttribute value:" + request.getAttribute("successMessage"));
        request.setAttribute("errorMessage", request.getParameter("msg"));
        if (request.getAttribute("successMessage") == null) {
            request.setAttribute("successMessage", request.getParameter("successMessage"));
        } else {
            request.setAttribute("successMessage", request.getAttribute("successMessage"));

        }

        int podcheck = 0;
        if (statusId1 != null && statusId1 != "") {
            int statusId2 = Integer.parseInt(statusId1);
            if (statusId2 == 6) {
                menuPath = "Trip Vehicle Allotment ";
            }
            if (statusId2 == 7) {
                menuPath = "Trip For Freeze  ";
            }
            if (statusId2 == 8) {
                menuPath = "Start Trip Sheet ";
            }
            if (statusId2 == 9) {
            }
            if (statusId2 == 10) {
                menuPath = "End Trip Sheet ";
            }
            if (statusId2 == 100) {
                menuPath = "Trip POD ";                
            }
            if (statusId2 == 12) {
                podcheck = 10;
                menuPath = "TripClosure ";
            }
            if (statusId2 == 13) {
                menuPath = "Trip Settlement ";
            }
        }

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();
            String param = request.getParameter("param");
            System.out.println("param vale is:" + param);
            String documentRequired = request.getParameter("doucmentRequired");
            tripTO.setDocumentRequired(documentRequired);
            request.setAttribute("documentRequired", documentRequired);
            String tripType = request.getParameter("tripType");
            String vehicleId = request.getParameter("vehicleId");
            String fleetCenterId = request.getParameter("fleetCenterId");
            String tripStatusId = request.getParameter("tripStatusId");
            String vehicleTypeId = request.getParameter("vehicleTypeId");
            String zoneId = request.getParameter("zoneId");
            String customerId = request.getParameter("customerId");
            String cityFromId = request.getParameter("cityFromId");
            String cityFrom = request.getParameter("cityFrom");
            String admin = request.getParameter("admin");
            request.setAttribute("cityFromId", cityFromId);
            request.setAttribute("cityFrom", cityFrom);
            System.out.println("admin in the controller = " + admin);

            int userId = (Integer) session.getAttribute("userId");
            String userMovementType = (String) session.getAttribute("userMovementType");
            String roleId = "" + (Integer) session.getAttribute("RoleId");
            tripTO.setUserId(userId);
            tripTO.setUserMovementType(userMovementType);
            request.setAttribute("roleId", roleId);

            tripTO.setTripType(tripType);
            System.out.println("tripType = " + tripType);
            request.setAttribute("tripType", tripType);

            Date dNow = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(dNow);
            cal.add(Calendar.DATE, 0);
            dNow = cal.getTime();

            Date dNow1 = new Date();
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(dNow1);
            cal1.add(Calendar.DATE, -30);
            dNow1 = cal1.getTime();

            SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy");
            String fromday = ft.format(dNow1);
            String today = ft.format(dNow);
            System.out.println("the today date is " + fromday);

            if (tripCommand.getPodStatus() != null && !tripCommand.getPodStatus().equals("")) {
                tripTO.setPodStatus(tripCommand.getPodStatus());
                request.setAttribute("podStatus", tripCommand.getPodStatus());
            } else {
                tripTO.setPodStatus("");
                request.setAttribute("podStatus", "");
            }

            if (podcheck == 10) {
                tripTO.setPodStatus("0");
                request.setAttribute("podStatus", "0");
            }

            if (cityFromId != null && !cityFromId.equals("")) {
                tripTO.setCityFromId(cityFromId);
                request.setAttribute("cityFromId", cityFromId);
            } else {
                tripTO.setCityFromId("");
                request.setAttribute("cityFromId", "");
            }
            if (vehicleId != null && !vehicleId.equals("")) {
                tripTO.setVehicleId(vehicleId);
                request.setAttribute("vehicleId", vehicleId);
            } else {
                tripTO.setVehicleId("");
                request.setAttribute("vehicleId", "");
            }
            if (fleetCenterId != null && !fleetCenterId.equals("")) {
                tripTO.setFleetCenterId(fleetCenterId);
                request.setAttribute("fleetCenterId", fleetCenterId);
            } else {
                tripTO.setFleetCenterId("");
                request.setAttribute("fleetCenterId", "");
            }
            if (zoneId != null && !zoneId.equals("")) {
                tripTO.setZoneId(zoneId);
                request.setAttribute("zoneId", zoneId);
            } else {
                tripTO.setZoneId("");
                request.setAttribute("zoneId", "");
            }
            if (vehicleTypeId != null && !vehicleTypeId.equals("")) {
                tripTO.setVehicleTypeId(vehicleTypeId);
                request.setAttribute("vehicleTypeId", vehicleTypeId);
            } else {
                tripTO.setVehicleTypeId("");
                request.setAttribute("vehicleTypeId", "");
            }
            if (customerId != null && !customerId.equals("")) {
                tripTO.setCustomerId(customerId);
                request.setAttribute("customerId", customerId);
            } else {
                tripTO.setCustomerId("");
                request.setAttribute("customerId", "");
            }
            if (tripStatusId != null && !tripStatusId.equals("")) {
                tripTO.setTripStatusId(tripStatusId);
                request.setAttribute("tripStatusId", tripStatusId);
            } else {
                tripTO.setTripStatusId("");
                request.setAttribute("tripStatusId", "");
            }
            System.out.println("tripCommand.getTripSheetId()--" + tripCommand.getTripSheetId());
            if (tripCommand.getTripSheetId() != null && !tripCommand.getTripSheetId().equals("")) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
                request.setAttribute("tripSheetId", tripTO.getTripSheetId());
                tripTO.setTripCode(tripTO.getTripSheetId());
            } else {
                String tripSheetId = request.getParameter("tripSheetId");
                System.out.println("tripSheetId--" + tripSheetId);
                tripTO.setTripSheetId(tripSheetId);
                tripTO.setTripCode(tripSheetId);
                request.setAttribute("tripSheetId", "");
            }
            String docketNo1 = request.getParameter("docketNo1");
            System.out.println("docketNo1--" + docketNo1);
            if (docketNo1 == null) {
                tripTO.setDocketNo("");
                request.setAttribute("docketNo", "");
            } else {
                tripTO.setDocketNo(docketNo1);
                request.setAttribute("docketNo", docketNo1);
            }

            if (tripCommand.getFromDate() != null && !tripCommand.getFromDate().equals("")) {
                tripTO.setFromDate(tripCommand.getFromDate());
                request.setAttribute("fromDate", tripTO.getFromDate());
            } else {
                tripTO.setFromDate(fromday);
                request.setAttribute("fromDate", fromday);
            }
            if (tripCommand.getToDate() != null && !tripCommand.getToDate().equals("")) {
                tripTO.setToDate(tripCommand.getToDate());
                request.setAttribute("toDate", tripTO.getToDate());
            } else {
                tripTO.setToDate(today);
                request.setAttribute("toDate", today);
            }
            String statusId = request.getParameter("statusId");
            if (statusId == null || "".equals(statusId)) {
                statusId = "0";
            }
            roleId = "" + (Integer) session.getAttribute("RoleId");
            String desigId = "" + (Integer) session.getAttribute("DesigId");

            String companyId = (String) session.getAttribute("companyId");
            tripTO.setRoleId(roleId);
            tripTO.setCompanyId(companyId);
            tripTO.setStatusId(statusId);
            request.setAttribute("statusId", statusId);
            admin = request.getParameter("admin");
            System.out.println("admin in the controller = " + admin);
            if (admin != null) {
                if (admin.equals("Yes")) {
                    tripTO.setExtraExpenseStatus("1");
                }
            }

            ArrayList tripDetails = new ArrayList();
            tripDetails = tripBP.getEcomDetails(tripTO);
            request.setAttribute("tripDetails", tripDetails);
            ArrayList statusDetails = new ArrayList();
            statusDetails = tripBP.getStatusDetails();

            ArrayList companyList = new ArrayList();
            companyList = companyBP.processGetCompanyList();
            request.setAttribute("companyList", companyList);

            ArrayList vehicleList = new ArrayList();
            vehicleList = tripBP.getVehicleList();
            request.setAttribute("vehicleList", vehicleList);

            ArrayList customerList = new ArrayList();
            customerList = tripBP.getCustomerList();
            request.setAttribute("customerList", customerList);

            ArrayList zoneList = new ArrayList();
            zoneList = tripBP.getZoneList();
            request.setAttribute("zoneList", zoneList);

            ArrayList vehicleTypeList = new ArrayList();
            vehicleTypeList = vehicleBP.getVehicleTypeList();
            request.setAttribute("vehicleTypeList", vehicleTypeList);
            request.setAttribute("vehicleTypeId", vehicleTypeId);

            request.setAttribute("statusDetails", statusDetails);
            String responsiveStatus = request.getParameter("respStatus");
            String startRequest = request.getParameter("startRequest");
            String endRequest = request.getParameter("endRequest");
            request.setAttribute("admin", admin);
            if (responsiveStatus == null) {
                if (request.getAttribute("respStatus") != null) {
                    responsiveStatus = (String) request.getAttribute("respStatus");
                }
                if (request.getAttribute("startRequest") != null) {
                    startRequest = (String) request.getAttribute("startRequest");
                }
                if (request.getAttribute("endRequest") != null) {
                    endRequest = (String) request.getAttribute("endRequest");
                }
            }

//            if (responsiveStatus != null && "Y".equals(responsiveStatus)) {
//                if ("Y".equals(startRequest)) {
//                    path = "content/trip/viewTripSheetResponsive.jsp";
//                } else {
//                    path = "content/trip/viewTripSheetEndResponsive.jsp";
//                }
//
//            } else 
            if (param != null && param.equals("exportExcel")) {
                path = "content/trip/viewTripSheetExcel.jsp";
            } else if (param != null && param.equals("podCopy")) {

                String[] tripIds = request.getParameterValues("podCopys");
                System.out.println("tripIds : " + tripIds);

                if (tripIds != null) {
                    for (int i = 0; i < tripIds.length; i++) {
                        String tripId = tripIds[i];
                        System.out.println("tripId :" + tripId);
                        String[] trpId = tripId.split(",");
                        tripTO.setTripIds(trpId);
                    }
                }

                ArrayList viewPODDetails = new ArrayList();
                viewPODDetails = tripBP.getPODUploadDetails(tripTO);
                if (viewPODDetails.size() > 0) {
                    request.setAttribute("viewPODDetails", viewPODDetails);
                }

                path = "content/trip/podDownload.jsp";

            } else {
                path = "content/trip/viewEcomTrips.jsp";
            } 
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
        
       public ModelAndView handleEcomDetails(HttpServletRequest request, HttpServletResponse response, TripCommand command) throws IOException {
        HttpSession session = request.getSession();
        tripCommand = command;
        TripTO tripTO = new TripTO();
        int userId = (Integer) session.getAttribute("userId");
        ModelAndView mv = null;

        ArrayList vehicleNoList = new ArrayList();
        String path = "";
        String tripSheetId = "";
        PrintWriter pw = response.getWriter();
        path = "content/trip/addEcomDetails.jsp";
         
        try {
            if (tripCommand.getTripSheetId() != null && !"".equals(tripCommand.getTripSheetId())) {
                tripTO.setTripSheetId(tripCommand.getTripSheetId());
                tripSheetId = tripCommand.getTripSheetId();
                System.out.println("tripCommand.getTripSheetId()=" + tripCommand.getTripSheetId());
            }
            if (tripCommand.getBillOverTime() != null && !"".equals(tripCommand.getBillOverTime())) {
                tripTO.setBillOverTime(tripCommand.getBillOverTime());
                System.out.println("tripCommand.getBillOverTime()=" + tripCommand.getBillOverTime());
            }
            if (tripCommand.getBillOverTimemin() != null && !"".equals(tripCommand.getBillOverTimemin())) {
                tripTO.setBillOverTimemin(tripCommand.getBillOverTimemin());
                System.out.println("tripCommand.getBillOverTimemin()=" + tripCommand.getBillOverTimemin());
            }
            if (tripCommand.getReceivedTime() != null && !"".equals(tripCommand.getReceivedTime())) {
                tripTO.setReceivedTime(tripCommand.getReceivedTime());
            }
            if (tripCommand.getReceivedTimemin() != null && !"".equals(tripCommand.getReceivedTimemin())) {
                tripTO.setReceivedTimemin(tripCommand.getReceivedTimemin());
            }
            if (tripCommand.getAppointTime() != null && !"".equals(tripCommand.getAppointTime())) {
                tripTO.setAppointTime(tripCommand.getAppointTime());
            }
            if (tripCommand.getAppointTimemin() != null && !"".equals(tripCommand.getAppointTimemin())) {
                tripTO.setAppointTimemin(tripCommand.getAppointTimemin());
            }
            if (tripCommand.getEwayBillNo() != null && !"".equals(tripCommand.getEwayBillNo())) {
                tripTO.setEwayBillNo(tripCommand.getEwayBillNo());
                System.out.println("tripCommand.getEwayBillNo()=" + tripCommand.getEwayBillNo());
            }
            if (tripCommand.getAppointDate() != null && !"".equals(tripCommand.getAppointDate())) {
                tripTO.setAppointDate(tripCommand.getAppointDate());
            }
            if (tripCommand.getExpDate() != null && !"".equals(tripCommand.getExpDate())) {
                tripTO.setExpDate(tripCommand.getExpDate());
            }
            if (tripCommand.getInvoiceNo() != null && !"".equals(tripCommand.getInvoiceNo())) {
                tripTO.setInvoiceNo(tripCommand.getInvoiceNo());
            }
           request.setAttribute("tripSheetId",tripSheetId );
            String lrNumber = "";
            lrNumber = tripBP.getLrNumberEcom(tripTO);
            request.setAttribute("lrNumber", lrNumber);
            System.out.println("lrNumbersss" + lrNumber);

            int insert = tripBP.insertEcomPoint(tripTO, userId,lrNumber);
            System.out.println("insert==" + insert);
//            if (!insert.equals("")) {
//                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Invoice Details Updated Successfully");
//            }
//            request.setAttribute("printDocketNo", insert);
//            mv = handleTripSheetPrint(request, response, command);
//            path = "content/trip/redirectPage.jsp";
          

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
        return new ModelAndView(path);
    }
        
        
        
}
