/*-------------------------------------------------------------------------
 * __NAME__.java
 * __DATE__
 *
 * Copyright (c) Entitle.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Entitle ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Entitle.
 -------------------------------------------------------------------------*/
package ets.domain.trip.data;  

import com.ibatis.sqlmap.client.SqlMapClient;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.util.sendInvoice;
import ets.domain.trip.business.TripTO;
import ets.domain.secondaryOperation.business.SecondaryOperationTO;

import ets.domain.util.FPLogUtils;
import ets.domain.util.ThrottleConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * ****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver Date Author Change
 * ----------------------------------------------------------------------------
 * 1.0 __DATE__ Your_Name ,Entitle Created
 *
 *****************************************************************************
 */
public class TripDAO extends SqlMapClientDaoSupport {

    /**
     * Creates a new instance of __NAME__
     */
    public TripDAO() {
    }
    private final static String CLASS = "TripDAO";

    /**
     * This method used to Get Desgintion Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getConsignmentList(TripTO tripTO) {
        Map map = new HashMap();
        String[] consignmentNos = tripTO.getConsignmentOrderNos().split(",");
        List consignmentOrderNos = new ArrayList(consignmentNos.length);
        for (int i = 0; i < consignmentNos.length; i++) {
            System.out.println("value iss:" + consignmentNos[i]);
            consignmentOrderNos.add(consignmentNos[i]);
        }
        map.put("consignmentOrderNos", consignmentOrderNos);

        ArrayList consignmentList = new ArrayList();
        try {
            System.out.println("map==" + map);
            consignmentList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentList", map);
            System.out.println("consignmentList size=" + consignmentList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return consignmentList;
    }

    public ArrayList getTripDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripDetails", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getTripCustomerList(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripCustomerList", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getTripsToBeBilledDetails(TripTO tripTO) {
        Map map = new HashMap();

        String[] tripIds = tripTO.getTripIds();
        String[] consignmentOrderIds = tripTO.getConsignmentOrderIds();
        List tripSheetIds = new ArrayList(tripIds.length);
        List consignmentIds = new ArrayList(consignmentOrderIds.length);
        for (int i = 0; i < tripIds.length; i++) {
            System.out.println("value:" + tripIds[i]);
            tripSheetIds.add(tripIds[i]);
        }
        for (int j = 0; j < consignmentOrderIds.length; j++) {
            System.out.println("cn value:" + consignmentOrderIds[j]);
            consignmentIds.add(consignmentOrderIds[j]);
        }
        String[] date = tripTO.getFromDate().split("-");
        String month = date[1];
        String year = date[2];
        map.put("month", month);
        map.put("Year", year);
        map.put("consignmentOrderIds", consignmentIds);
        map.put("tripSheetIds", tripSheetIds);
        map.put("customerId", tripTO.getCustomerId());
        map.put("InvoiceFlag", tripTO.getInvoiceFlag());
        ArrayList tripDetails = new ArrayList();
        System.out.println("map for trip det:" + map);

        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripsToBeBilledDetails", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getTripsOtherExpenseDetails(TripTO tripTO) {
        Map map = new HashMap();

        String[] tripIds = tripTO.getTripIds();
        String[] consignmentOrderIds = tripTO.getConsignmentOrderIds();
        List tripSheetIds = new ArrayList(tripIds.length);
        List consignmentIds = new ArrayList(consignmentOrderIds.length);
        for (int i = 0; i < tripIds.length; i++) {
            System.out.println("value:" + tripIds[i]);
            tripSheetIds.add(tripIds[i]);
        }
        for (int j = 0; j < consignmentOrderIds.length; j++) {
            System.out.println("cn value:" + consignmentOrderIds[j]);
            consignmentIds.add(consignmentOrderIds[j]);
        }
        map.put("consignmentOrderIds", consignmentIds);
        map.put("tripSheetIds", tripSheetIds);
        map.put("expenseType", tripTO.getExpenseType());
        map.put("customerId", tripTO.getCustomerId());
        ArrayList tripDetails = new ArrayList();
        System.out.println("mappp--" + map);
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripsOtherExpenseDetails", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getTripsOtherExpenseDetails1(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        String tripId = (String) getSqlMapClientTemplate().queryForObject("trip.getInvoiceTripIds", map);
        String consignmentOrderId = (String) getSqlMapClientTemplate().queryForObject("trip.getInvoiceConsignmentOrderIds", map);
        String customerId = (String) getSqlMapClientTemplate().queryForObject("trip.getInvoiceCustomerId", map);
        String[] tripIds = tripId.split(",");
        String[] consignmentIds = consignmentOrderId.split(",");
        List tripSheetIds = new ArrayList(tripIds.length);
        List consignmentOrderIds = new ArrayList(consignmentIds.length);
        for (int i = 0; i < tripIds.length; i++) {
            System.out.println("value:" + tripIds[i]);
            tripSheetIds.add(tripIds[i]);
        }
        for (int i = 0; i < consignmentIds.length; i++) {
            System.out.println("consignmentOrderId value:" + consignmentIds[i]);
            consignmentOrderIds.add(consignmentIds[i]);
        }
        map.put("consignmentOrderIds", consignmentOrderIds);
        map.put("tripSheetIds", tripSheetIds);
        map.put("expenseType", tripTO.getExpenseType());
        map.put("customerId", customerId);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripsOtherExpenseDetails", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getTripsOtherExpense(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        String tripId = (String) getSqlMapClientTemplate().queryForObject("trip.getInvoiceTripIds", map);
        String customerId = (String) getSqlMapClientTemplate().queryForObject("trip.getInvoiceCustomerId", map);
        map.put("customerId", customerId);
        String[] tripIds = tripId.split(",");
        List tripSheetIds = new ArrayList(tripIds.length);
        for (int i = 0; i < tripIds.length; i++) {
            System.out.println("value:" + tripIds[i]);
            tripSheetIds.add(tripIds[i]);
        }
        map.put("tripSheetIds", tripSheetIds);
        map.put("expenseType", tripTO.getExpenseType());
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripsOtherExpense", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getInvoiceHeader(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        ArrayList result = new ArrayList();
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInvoiceHeader", map);
            System.out.println("result size=" + result.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceHeader List", sqlException);
        }

        return result;
    }

    public ArrayList getInvoiceDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        ArrayList result = new ArrayList();
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInvoiceDetails", map);
            System.out.println("result size=" + result.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceDetails List", sqlException);
        }

        return result;
    }

    public ArrayList getInvoiceDetailExpenses(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        ArrayList result = new ArrayList();
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInvoiceDetailExpenses", map);
            System.out.println("result size=" + result.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceDetailExpenses Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceDetailExpenses List", sqlException);
        }

        return result;
    }

    public ArrayList getTripPointDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        ArrayList tripPointDetails = new ArrayList();
        try {
            tripPointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripPointDetails", map);
            System.out.println("getTripPointDetails size=" + tripPointDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripPointDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripPointDetails List", sqlException);
        }

        return tripPointDetails;
    }

    public ArrayList getOrderPointDetails(TripTO tripTO) {
        Map map = new HashMap();
        String[] consignmentNos = tripTO.getConsignmentOrderNos().split(",");
        List consignmentOrderNos = new ArrayList(consignmentNos.length);
        for (int i = 0; i < consignmentNos.length; i++) {
            System.out.println("value:" + consignmentNos[i]);
            consignmentOrderNos.add(consignmentNos[i]);
        }
        map.put("consignmentOrderNos", consignmentOrderNos);

        ArrayList tripPointDetails = new ArrayList();
        try {
            System.out.println("map value:" + map);
            tripPointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderPointDetails", map);
            System.out.println("getOrderPointDetails size=" + tripPointDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderPointDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderPointDetails List", sqlException);
        }

        return tripPointDetails;
    }

    public ArrayList getLoadDetails(TripTO tripTO) {
        Map map = new HashMap();
        String[] consignmentNos = tripTO.getConsignmentOrderNos().split(",");
        List consignmentOrderNos = new ArrayList(consignmentNos.length);
        for (int i = 0; i < consignmentNos.length; i++) {
            System.out.println("value:" + consignmentNos[i]);
            consignmentOrderNos.add(consignmentNos[i]);
        }
        map.put("consignmentOrderNos", consignmentOrderNos);
        map.put("tripId", tripTO.getTripId());
        ArrayList loadDetails = new ArrayList();
        try {
            System.out.println("map value:" + map);
            loadDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCBTPackageDetails", map);
            System.out.println("planningloadDetails size=" + loadDetails.size());

            if (!tripTO.getTripId().equals("") && tripTO.getTripId() != null) {
                loadDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripCBTPackageDetails", map);
                System.out.println("triploadDetails size=" + loadDetails.size());
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("loadDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderPointDetails List", sqlException);
        }

        return loadDetails;
    }

//    public ArrayList getVehicleRegNos(TripTO tripTO) {
//        Map map = new HashMap();
//        String vehicleNo = tripTO.getVehicleNo();
//        map.put("vehicleId", tripTO.getVehicleId());
//        System.out.println("vehicleNo = " + vehicleNo);
//        map.put("vehicleNo", vehicleNo + "%");
////        if(tripTO.getVehicleNo() != null && !"".equals(tripTO.getVehicleNo())){
////        }else{
////        map.put("vehicleNo", "");
////        }
//        if ("1".equals(tripTO.getTripType())) {
//            map.put("usageTypeId", 2);
//        } else if ("2".equals(tripTO.getTripType())) {
//            map.put("usageTypeId", 1);
//        }
//        if ("2".equals(tripTO.getTripType())) {
//            map.put("customerName", tripTO.getCustomerName());
//        }
//        map.put("roleId", tripTO.getRoleId());
//        map.put("companyId", tripTO.getCompanyId());
//
//        ArrayList vehicleNos = new ArrayList();
//        try {
//            System.out.println("map: in the dao" + map);
//            vehicleNos = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleRegNos", map);
//            System.out.println("vehicleNos size=" + vehicleNos.size());
//        } catch (Exception sqlException) {
//            sqlException.printStackTrace();
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
//        }
//
//        return vehicleNos;
//    }
    public ArrayList getVehicleRegNosForEmptyTrip(TripTO tripTO) {
        Map map = new HashMap();
        String vehicleNo = tripTO.getVehicleNo();
        map.put("vehicleId", tripTO.getVehicleId());
        System.out.println("vehicleNo = " + vehicleNo);
        map.put("vehicleNo", vehicleNo + "%");
//        if(tripTO.getVehicleNo() != null && !"".equals(tripTO.getVehicleNo())){
//        }else{
//        map.put("vehicleNo", "");
//        }
        if ("1".equals(tripTO.getTripType())) {
            map.put("usageTypeId", 2);
        } else if ("2".equals(tripTO.getTripType())) {
            map.put("usageTypeId", 1);
        }
        if ("2".equals(tripTO.getTripType())) {
            map.put("customerName", tripTO.getCustomerName());
        }
        map.put("roleId", tripTO.getRoleId());
        map.put("companyId", tripTO.getCompanyId());

        ArrayList vehicleNos = new ArrayList();
        try {
            System.out.println("map: in the dao" + map);
            vehicleNos = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleRegNosForEmptyTrip", map);
            System.out.println("vehicleNos size=" + vehicleNos.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return vehicleNos;
    }

    public ArrayList getVehicleRegNosForUpload(TripTO tripTO) {
        Map map = new HashMap();
        String vehicleNo = tripTO.getVehicleNo() + "%";
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("vehicleNo", vehicleNo);
        if ("1".equals(tripTO.getTripType())) {
            map.put("usageTypeId", 2);
        } else if ("2".equals(tripTO.getTripType())) {
            map.put("usageTypeId", 1);
        }
        map.put("roleId", tripTO.getRoleId());
        map.put("companyId", tripTO.getCompanyId());

        ArrayList vehicleNos = new ArrayList();
        try {
            System.out.println("map:" + map);
            String tripStatus = "";
            String jobcardStatus = "";

            tripStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleRegNosForUploadStatus", map);
            System.out.println("vehicleStatus = " + tripStatus);
            if (tripStatus == null) {
                map.put("tripStatus", 0);
                jobcardStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getJobcardForUploadStatus", map);
                if (jobcardStatus == null) {
                    map.put("tripStatus", 2);
                } else {
                    map.put("tripStatus", 0);
                }
            } else {
                map.put("tripStatus", 1);
            }

            System.out.println("map = " + map);
            vehicleNos = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleRegNosForUpload", map);
            System.out.println("vehicleNos size=" + vehicleNos.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleRegNosForUpload Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleRegNosForUpload List", sqlException);
        }

        return vehicleNos;
    }

    public ArrayList getDrivers(TripTO tripTO) {
        Map map = new HashMap();
        String driverName = tripTO.getDriverName() + "%";
        map.put("driverName", driverName);

        ArrayList drivers = new ArrayList();
        try {
            drivers = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDrivers", map);
            System.out.println("drivers size=" + drivers.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return drivers;
    }

    public String getConsignmentOrderRevenue(String orderId, String billingId, String vehicleTypeId, String reeferRequired, String customerType) {
        ArrayList resultList = new ArrayList();
        Map map = new HashMap();
        String revenue = "";
        try {
            map.put("customerType", customerType);
            map.put("orderId", orderId);

            System.out.println("map = " + map);
            if (customerType.equals("2")) {
                revenue = (String) getSqlMapClientTemplate().queryForObject("trip.getConsignmentOrderRevenueWalkin", map);
            } else {
                if (billingId.equals("2")) {
                    revenue = (String) getSqlMapClientTemplate().queryForObject("trip.getConsignmentOrderRevenue1", map);
                } else {
                    revenue = (String) getSqlMapClientTemplate().queryForObject("trip.getConsignmentOrderRevenue", map);
                }
            }

            System.out.println("revenue=" + revenue);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentOrderRevenue Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignmentOrderRevenue", sqlException);
        }
        return revenue;
    }

    public String getVehicleMileageAndTollRate(String vehicleTypeId) {
        ArrayList resultList = new ArrayList();
        Map map = new HashMap();
        String response = "";
        try {
            map.put("vehicleTypeId", vehicleTypeId);
            response = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleMileageAndTollRate", map);
            //resultList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentOrderRevenue", map);
            System.out.println("response=" + response);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleMileageAndTollRate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleMileageAndTollRate", sqlException);
        }
        return response;
    }

    public String getEstimatedTripEndDateTime(String tripId) {
        ArrayList resultList = new ArrayList();
        Map map = new HashMap();
        String response = "";
        try {
            map.put("tripId", tripId);
            response = (String) getSqlMapClientTemplate().queryForObject("trip.getEstimatedTripEndDateTime", map);
            //resultList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentOrderRevenue", map);
            System.out.println("response=" + response);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEstimatedTripEndDateTime Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEstimatedTripEndDateTime", sqlException);
        }
        return response;
    }

    public String checkPreStartRoute(String preStartLocationId, String originId, String vehicleTypeId) {
        ArrayList resultList = new ArrayList();
        Map map = new HashMap();
        String info = "";
        try {
            map.put("preStartLocationId", preStartLocationId);
            map.put("originId", originId);
            map.put("vehicleTypeId", vehicleTypeId);
            System.out.println("map:" + map);
            info = (String) getSqlMapClientTemplate().queryForObject("trip.checkPreStartRoute", map);
            //resultList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentOrderRevenue", map);
            System.out.println("info=" + info);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkPreStartRoute Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkPreStartRoute", sqlException);
        }
        return info;
    }

    public String getConsignmentOrderExpense(String orderId, String billingId, String vehicleTypeId, String reeferRequired, String customerTypeId) {
        Map map = new HashMap();
        String expense = "";
        try {
            map.put("orderId", orderId);
            map.put("billingId", billingId);
            map.put("vehicleTypeId", vehicleTypeId);
            map.put("reeferRequired", reeferRequired);
            map.put("customerTypeId", customerTypeId);
            System.out.println("map iiis" + map);
            if (customerTypeId.equals("2")) {
                expense = (String) getSqlMapClientTemplate().queryForObject("trip.getConsignmentOrderExpense1", map);
            } else {
                if (billingId.equals("2")) {
                    expense = (String) getSqlMapClientTemplate().queryForObject("trip.getConsignmentOrderExpense1", map);
                } else {
                    expense = (String) getSqlMapClientTemplate().queryForObject("trip.getConsignmentOrderExpense", map);
                }
            }
            System.out.println("expense=" + expense);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentOrderExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignmentOrderExpense", sqlException);
        }
        return expense;
    }

    public String getTripCodeSequence() {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        try {
            tripCodeSequence = (Integer) getSqlMapClientTemplate().insert("trip.getTripCodeSequence", map);
            System.out.println("tripCodeSequence=" + tripCodeSequence);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public String getCnoteCodeSequence() {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        try {
            tripCodeSequence = (Integer) getSqlMapClientTemplate().insert("trip.getCnoteCodeSequence", map);
            System.out.println("getCnoteCodeSequence=" + tripCodeSequence);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCnoteCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public String getInvoiceCodeSequence() {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        try {
            tripCodeSequence = (Integer) getSqlMapClientTemplate().insert("trip.getInvoiceCodeSequence", map);
            System.out.println("getInvoiceCodeSequence=" + tripCodeSequence);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public String getInvoiceCodeSequenceBill() {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        try {
            tripCodeSequence = (Integer) getSqlMapClientTemplate().insert("trip.getInvoiceCodeSequenceBill", map);
            System.out.println("getInvoiceCodeSequence=" + tripCodeSequence);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public String getInvoiceCodeSequenceRemb() {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        try {
            tripCodeSequence = (Integer) getSqlMapClientTemplate().insert("trip.getInvoiceCodeSequenceRemb", map);
            System.out.println("getInvoiceCodeSequence=" + tripCodeSequence);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public int saveTripSheet(TripTO tripTO) {
        Map map = new HashMap();
        int tripId = 0;
        int status = 0;
        try {
            map.put("tripCode", tripTO.getTripCode());
            map.put("companyId", tripTO.getCompanyId());
            map.put("productInfo", tripTO.getProductInfo());
            map.put("orderExpense", tripTO.getAdditionalCost());
            map.put("orderRevenue", tripTO.getOrderRevenue());
            map.put("profitMargin", tripTO.getProfitMargin());

            String[] pointPlanDate = tripTO.getPointPlanDate();
            map.put("tripScheduleDate", pointPlanDate[0]);
            map.put("tripScheduleTime", tripTO.getTripScheduleTime());
            map.put("statusId", tripTO.getStatusId());

            map.put("customerId", tripTO.getCustomerId());
            map.put("vehicleTypeId", tripTO.getVehicleTypeId());
            map.put("originId", tripTO.getOriginId());
            map.put("destinationId", tripTO.getDestinationId());
            map.put("movementTypeId", tripTO.getMovementTypeId());

            String tat = (String) getSqlMapClientTemplate().queryForObject("trip.getTATHours", map);

            if (tat == null) {
                map.put("transitHours", "0");
                map.put("transitDay", "0");
            } else {

                double tatDay = Double.parseDouble(tat);
                double tatTime = tatDay * 24;

                map.put("transitHours", tatTime);
                map.put("transitDay", tatDay);
            }

            map.put("advnaceToBePaidPerDay", tripTO.getAdvnaceToBePaidPerDay());
            map.put("emptyTripPurpose", tripTO.getEmptyTripPurpose());
            map.put("vehicleTypeName", tripTO.getVehicleTypeName());

            /////////////////////////// Sesha edit /////////////////////////////////////
            map.put("vendorId", tripTO.getVendorId());
            map.put("userId", tripTO.getUserId());

            Calendar calendar = Calendar.getInstance();
            int curyear = (int) calendar.get(Calendar.YEAR);
            String currentyear = curyear + "";
            currentyear = currentyear.substring(2);
            int nextyear = Integer.parseInt(currentyear) + 1;
            String accYear = currentyear + "" + nextyear;

            String lhcCodeSequence = "";
            lhcCodeSequence = (String) getSqlMapClientTemplate().queryForObject("vendor.getLHCNo", map);
            if (lhcCodeSequence == null) {
                lhcCodeSequence = "00001";
            } else if (lhcCodeSequence.length() == 1) {
                lhcCodeSequence = "0000" + lhcCodeSequence;
            } else if (lhcCodeSequence.length() == 2) {
                lhcCodeSequence = "000" + lhcCodeSequence;
            } else if (lhcCodeSequence.length() == 3) {
                lhcCodeSequence = "00" + lhcCodeSequence;
            } else if (lhcCodeSequence.length() == 4) {
                lhcCodeSequence = "0" + lhcCodeSequence;
            } else if (lhcCodeSequence.length() == 5) {
                lhcCodeSequence = "" + lhcCodeSequence;
            }

            String lhcCode = "LHC-" + accYear + "-" + lhcCodeSequence;

            map.put("lhcCode", lhcCode);
            map.put("userId", tripTO.getUserId());
            map.put("contractHireId", tripTO.getContractHireId());
            map.put("vehicleTypeId", tripTO.getVehicleTypeId());

            map.put("additionalCost", tripTO.getAdditionalCost());
            map.put("ownerShip", tripTO.getOwnerShip());

            System.out.println("map : " + map);

            int LHCId = (Integer) getSqlMapClientTemplate().insert("trip.insertLHC", map);
            System.out.println("LHCId : " + LHCId);

            /////////////////////////// Sesha edit /////////////////////////////////////
            map.put("lhcId", LHCId);

            map.put("cNotes", tripTO.getcNotes());
            map.put("billingType", tripTO.getBillingType());
            map.put("customerName", tripTO.getCustomerName());
            map.put("customerType", tripTO.getCustomerType());
            map.put("routeInfo", tripTO.getRouteInfo());
            map.put("reeferRequired", tripTO.getReeferRequired());
            map.put("totalWeight", tripTO.getTotalWeight());
            map.put("preStartLocationStatus", tripTO.getPreStartLocationStatus());
            map.put("originId", tripTO.getOriginId());
            map.put("destinationId", tripTO.getDestinationId());
            String tripType = tripTO.getTripType();
            if (tripType != null && !"".equals(tripType)) {
                if ("secondary".equalsIgnoreCase(tripType)) {
                    map.put("tripType", "2");
                } else {
                    map.put("tripType", "1");
                }
            } else {
                map.put("tripType", "1");
            }
            map.put("cbt", tripTO.getCbt());
            System.out.println("map value is:" + map);
            if ("0".equals(tripTO.getCustomerId())) {
                map.put("emptyTripStatusId", "1");
            } else {
                map.put("emptyTripStatusId", "0");
            }
            System.out.println("map trip sheet : " + map);
            tripId = (Integer) getSqlMapClientTemplate().insert("trip.saveTripSheet", map);
            map.put("tripId", tripId);
            System.out.println("tripId  : " + tripId);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripIdInLHC", map);
            System.out.println("LHC status : " + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripSheet", sqlException);
        }
        return tripId;
    }

    public int updatePreStartDetails(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("preStartLocationId", tripTO.getPreStartLocationId());
            map.put("preStartLocationPlanDate", tripTO.getPreStartLocationPlanDate());
            map.put("preStartLocationPlanTimeHrs", tripTO.getPreStartLocationPlanTimeHrs());
            map.put("preStartLocationPlanTimeMins", tripTO.getPreStartLocationPlanTimeMins());
            map.put("preStartLocationDistance", tripTO.getPreStartLocationDistance());
            map.put("preStartLocationDurationHrs", tripTO.getPreStartLocationDurationHrs());
            map.put("preStartLocationDurationMins", tripTO.getPreStartLocationDurationMins());
            map.put("preStartLocationVehicleMileage", tripTO.getPreStartLocationVehicleMileage());
            map.put("preStartLocationTollRate", tripTO.getPreStartLocationTollRate());
            map.put("preStartLocationRouteExpense", tripTO.getPreStartLocationRouteExpense());

            System.out.println("map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updatePreStartDetails", map);
            System.out.println("saveTripStatusDetails=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripStatusDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripStatusDetails", sqlException);
        }
        return status;
    }

    public int saveTripStatusDetails(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("statusId", tripTO.getStatusId());
            map.put("userId", tripTO.getUserId());
            map.put("remarks", "System Update");
            System.out.println("map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripStatusDetails", map);
            System.out.println("saveTripStatusDetails=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripStatusDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripStatusDetails", sqlException);
        }
        return status;
    }

    public int saveTripVehicle(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("userId", tripTO.getUserId());
            map.put("vehicleCapUtil", tripTO.getVehicleCapUtil());
            if ("0".equals(tripTO.getVehicleId())) {
                map.put("hireVehilceNo", tripTO.getHireVehicleNo());
            }
            System.out.println("map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripVehicle", map);
            System.out.println("saveTripVehicle=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripVehicle Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripVehicle", sqlException);
        }
        return status;
    }

    public int saveTripVehicleForVehicleChange(TripTO tripTO, String[] fileSaved) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("startKM", tripTO.getVehicleStartKm());
            map.put("endKM", tripTO.getLastVehicleEndKm());
            map.put("startHM", tripTO.getVehicleStartOdometer());
            map.put("endHM", tripTO.getLastVehicleEndOdometer());
            map.put("vehicleChangeDate", tripTO.getVehicleChangeDate());
            map.put("vehicleChangeTime", tripTO.getVehicleChangeHour() + ":" + tripTO.getVehicleChangeMinute() + ":00");
            map.put("userId", tripTO.getUserId());
            map.put("vehicleCapUtil", tripTO.getVehicleCapUtil());
            map.put("remarks", tripTO.getVehicleRemarks());
            map.put("cityId", tripTO.getCityId());

            map.put("tripSheetId", tripTO.getTripId());
            map.put("statusId", "22");

            map.put("ewayChangeDate", tripTO.getEwayBillDate());
            map.put("ewayChangeTime", tripTO.getEwayBillTime());
            map.put("inssurvey", tripTO.getInsuranceSurvey());

            map.put("sealNo", tripTO.getSealNo());
            map.put("LhcId", tripTO.getLhcId());
            map.put("sealNoType", tripTO.getSealNoType());
            map.put("consignmentId", tripTO.getConsignmentId());

            map.put("driverId", "0");
            map.put("driverName", "-");

            map.put("crossDock", null);
            map.put("fir", null);
            map.put("survey", null);
            map.put("incident", null);

            if (fileSaved.length > 0) {

                for (int x = 0; x < fileSaved.length; x++) {

                    if (fileSaved[x] != null) {

//                    File file = new File(actualFilePath[x]);                
//                    fis = new FileInputStream(file);                
//                    byte[] File = new byte[(int) file.length()];
//                    fis.read(File);
//                    fis.close();  
                        System.out.println("File[x]" + fileSaved[x]);

                        if (x == 0) {
                            map.put("crossDock", fileSaved[x]);
                        }

                        if (x == 1) {
                            map.put("fir", fileSaved[x]);
                        }

                        if (x == 2) {
                            map.put("survey", fileSaved[x]);
                        }

                        if (x == 3) {
                            map.put("incident", fileSaved[x]);
                        }
                    }
                }
            }

            map.put("oldvehicleId", tripTO.getOldVehicleId());
            map.put("vehicleNo", tripTO.getVehicleNo());
            map.put("mobile", tripTO.getDriverMobile());
            System.out.println("map value is 1111 :" + map);

            String regNos = tripTO.getVehicleNo();
            map.put("regNo", regNos + "%");
            String checkVehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.checkExistingVehicle", map);
            System.out.println("checkVehicleId: " + checkVehicleId);

            int vehicleInsertId = 0;
            if ("".equals(checkVehicleId) || checkVehicleId == null) {
                vehicleInsertId = (Integer) getSqlMapClientTemplate().insert("trip.insertVehicleMaster", map);
                System.out.println("vehicleInsertId : " + vehicleInsertId);
                map.put("vehicleId", vehicleInsertId);
                int vehilceRegInsert = (Integer) getSqlMapClientTemplate().insert("trip.insertVehicleRegNo", map);
                System.out.println("vehilceRegInsert : " + vehilceRegInsert);

            } else {
                map.put("vehicleId", tripTO.getVehicleId());
            }

            System.out.println("vehicleInsertId: " + vehicleInsertId);

            status = (Integer) getSqlMapClientTemplate().update("trip.updateLHCVehicleNo", map);

            // status = (Integer) getSqlMapClientTemplate().update("trip.removeTripLHC", map);
            // status = (Integer) getSqlMapClientTemplate().update("trip.updateLHCTrip", map);
            // status = (Integer) getSqlMapClientTemplate().update("trip.updateTripLHCDetails", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripSealNoDetails", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.insertTripVehicleChangeDetails", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.insertTripStatus", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriver", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripVehicleForVehicleChange", map);

            int maxVehicleSequence = (Integer) getSqlMapClientTemplate().queryForObject("trip.getMaximumVehicleSequence", map);
            map.put("vehicleSequence", maxVehicleSequence);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateVehicleSequence", map);
            System.out.println("saveTripVehicle=" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripVehicle Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripVehicle", sqlException);
        }
        return status;
    }

    public ArrayList getVehicleChangeTripAdvanceDetails(String tripSheetId) {
        Map map = new HashMap();
        ArrayList tripAdvanceDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripSheetId);
            System.out.println("closed Trip map is::" + map);
            tripAdvanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleChangeTripAdvanceDetails", map);
            System.out.println("getVehicleChangeTripAdvanceDetails.size() = " + tripAdvanceDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleChangeTripAdvanceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleChangeTripAdvanceDetails List", sqlException);
        }
        return tripAdvanceDetails;
    }

    public int clearVehicleAndDriverMapping(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            if ("".equals(tripTO.getLastVehicleEndKm()) || tripTO.getLastVehicleEndKm() == null) {
                map.put("endKM", 0);
            } else {
                map.put("endKM", tripTO.getLastVehicleEndKm());
            }
            if ("".equals(tripTO.getLastVehicleEndOdometer()) || tripTO.getLastVehicleEndOdometer() == null) {
                map.put("endHM", 0);
            } else {
                map.put("endHM", tripTO.getLastVehicleEndOdometer());
            }
            if (!"".equals(tripTO.getVehicleChangeDate())) {
                map.put("vehicleChangeDate", tripTO.getVehicleChangeDate());
            }
            if (!"".equals(tripTO.getVehicleChangeHour()) && !"".equals(tripTO.getVehicleChangeMinute())) {
                map.put("vehicleChangeTime", tripTO.getVehicleChangeHour() + ":" + tripTO.getVehicleChangeMinute() + ":00");
            }
            System.out.println("map value is:" + map);
            // status = (Integer) getSqlMapClientTemplate().update("trip.removeVehicleAvailability", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.removeTripVehicle", map);
            System.out.println("removeTripVehicle=" + status);
            status = (Integer) getSqlMapClientTemplate().update("trip.removeTripDriver", map);
            System.out.println("removeTripDriver=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripVehicle Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "clearVehicleAndDriverMapping", sqlException);
        }
        return status;
    }

    public int saveTripDriver(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("driverId", tripTO.getPrimaryDriverId());
            map.put("userId", tripTO.getUserId());
            map.put("vehicleId", tripTO.getVehicleId());
            if ("0".equals(tripTO.getPrimaryDriverId())) {
                System.out.println("hire" + tripTO.getHireVehicleDriver());
//                map.put("driverName", tripTO.getHireVehicleDriver());
                map.put("driverName", tripTO.getPrimaryDriverName());
            } else {
                System.out.println("own" + tripTO.getPrimaryDriverName());
                map.put("driverName", tripTO.getPrimaryDriverName());
            }
            map.put("type", "P");
            System.out.println("map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriver", map);
            if (!"0".equals(tripTO.getSecondaryDriver1Id()) && !"Driver Not Mapped".equals(tripTO.getSecondaryDriver1Id()) && !"".equals(tripTO.getSecondaryDriver1Id())) {
                map.put("driverId", tripTO.getSecondaryDriver1Id());
                map.put("type", "S");
                status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriver", map);
            }
            if (!"0".equals(tripTO.getSecondaryDriver2Id()) && !"Driver Not Mapped".equals(tripTO.getSecondaryDriver2Id()) && !"".equals(tripTO.getSecondaryDriver2Id())) {
                map.put("driverId", tripTO.getSecondaryDriver2Id());
                map.put("type", "S");
                status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriver", map);
            }

            System.out.println("saveTripDriver=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripDriver Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripDriver", sqlException);
        }
        return status;
    }

    public int saveTripConsignments(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            map.put("statusId", tripTO.getStatusId());
            if (!tripTO.getTotalPackages().equals("")) {
                map.put("totalPackages", tripTO.getTotalPackages());
            } else {
                map.put("totalPackages", "0");
            }

            String[] consignmentOrderIds = tripTO.getConsignmentOrderId();
            String[] loadOrderId = tripTO.getLoadOrderId();
            System.out.println("loadOrderId------" + loadOrderId.length);

            String[] pendingPkgs = tripTO.getPendingPkgs();
//            String[] pendingWeight = tripTO.getPendingWeight();
            String[] loadedPkgs = tripTO.getLoadedPkgs();
//            String[] loadedWeight = tripTO.getLoadedWeight();
//            String[] pendingGrossWeight = tripTO.getPendingGrossWeight();
//            String[] loadedGrossWeight = tripTO.getLoadedGrossWeight();
//            System.out.println("loadedGrossWeight" + loadedGrossWeight[0]);
            int j = 1;
            for (int i = 0; i < loadOrderId.length; i++) {
                map.put("consignmentOrderId", loadOrderId[i]);
                map.put("pendingPkgs", pendingPkgs[i]);
                map.put("pendingWeight", 0);
                map.put("loadedPkgs", loadedPkgs[i]);
                map.put("loadedWeight", 0);
                map.put("orderSequence", j);
                map.put("pendingGrossWeight", 0);
                map.put("loadedGrossWeight", 0);
                j++;

                if ("0".equals(pendingPkgs[i])) {

                    System.out.println("pendingPkgs=" + pendingPkgs[i]);
                    map.put("statusId", tripTO.getStatusId());
                } else {
                    System.out.println("else");
                    map.put("statusId", "5");
                }

                System.out.println("map value is:" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.saveTripConsignments", map);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentOrderStatus", map);

                System.out.println("saveTripConsignments=" + status);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripConsignments Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripConsignments", sqlException);
        }
        return status;
    }

    public int saveAction(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("remarks", tripTO.getActionName() + " : " + tripTO.getActionRemarks());
            map.put("userId", tripTO.getUserId());
            map.put("statusId", tripTO.getStatusId());
            String[] consignmentOrderIds = tripTO.getConsignmentOrderId();
            for (int i = 0; i < consignmentOrderIds.length; i++) {
                map.put("consignmentOrderId", consignmentOrderIds[i]);
                System.out.println("map value is:" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.saveAction", map);
                System.out.println("saveAction=" + status);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveAction Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveAction", sqlException);
        }
        return status;
    }

    public int saveClosureApprovalRequest(String tripId, String requestType, String remarks, int userId, String rcmExp, String nettExp) {
        Map map = new HashMap();
        int status = 0;
        try {
            if (nettExp == null || "".equals(nettExp)) {
                nettExp = "0";
            }
            if (rcmExp == null || "".equals(rcmExp)) {
                rcmExp = "0";
            }
            map.put("tripId", tripId);
            map.put("requestType", requestType);
            map.put("remarks", remarks);
            map.put("userId", userId);
            map.put("rcmExp", rcmExp);
            map.put("nettExp", nettExp);
            String kmHmInfo = (String) getSqlMapClientTemplate().queryForObject("trip.getTripKmHm", map);
            //String rcmSystemExpInfo = (String) getSqlMapClientTemplate().queryForObject("trip.getRcmSystemExpense", map);
            String[] temp = kmHmInfo.split("-");
            map.put("kmRun", temp[0]);
            map.put("hmRun", temp[1]);
            map.put("aprovedStatus", "1");
            System.out.println("map in the trip dao = " + map);
            status = (Integer) getSqlMapClientTemplate().insert("trip.saveClosureApprovalRequest", map);
            System.out.println("saveClosureApprovalRequest=" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveClosureApprovalRequest Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveClosureApprovalRequest", sqlException);
        }
        return status;
    }

    public int saveTripUpdate(TripTO tripTO) {
        Map map = new HashMap();
        Map map1 = new HashMap();
        int status = 0;
        int updateCBT = 0;
        ArrayList consignmentList = new ArrayList();
        try {
            map.put("remarks", tripTO.getActionName() + " : " + tripTO.getActionRemarks());
            map.put("userId", tripTO.getUserId());
            map.put("statusId", tripTO.getStatusId());

            map.put("tripId", tripTO.getTripId());
            status = (Integer) getSqlMapClientTemplate().update("trip.resetTripIdLhc", map);
            System.out.println("status1==" + status);
            status = (Integer) getSqlMapClientTemplate().update("trip.resetLhcIdTrip", map);
            System.out.println("status2==" + status);
            consignmentList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCbtTripLoadingDetails", map);
            System.out.println("consignmentList loading size=" + consignmentList.size());
            if (consignmentList.size() > 0) {
                Iterator itr4 = consignmentList.iterator();
                while (itr4.hasNext()) {
                    tripTO = (TripTO) itr4.next();
                    System.out.println("tripTO.getConsignmentOrderIds()=" + tripTO.getConsignmentId());
                    map.put("consignmentOrderId", tripTO.getConsignmentId());
                    map.put("pendingPkgs", Double.parseDouble(tripTO.getPendingPackages()) + Double.parseDouble(tripTO.getLoadPkgs()));
                    map.put("pendingChrgWgt", Double.parseDouble(tripTO.getPchargableWeight()) + Double.parseDouble(tripTO.getLoadChargeWgt()));
                    map.put("pendingGrossWgt", Double.parseDouble(tripTO.getPendingGrossWeights()) + Double.parseDouble(tripTO.getLoadGrossWgt()));

                    System.out.println("pkg=" + Double.parseDouble(tripTO.getPendingPackages()) + Double.parseDouble(tripTO.getLoadPkgs()));
                    System.out.println("char=" + Double.parseDouble(tripTO.getPchargableWeight()) + Double.parseDouble(tripTO.getLoadChargeWgt()));
                    System.out.println("grs=" + Double.parseDouble(tripTO.getPendingGrossWeights()) + Double.parseDouble(tripTO.getLoadGrossWgt()));

                    updateCBT = (Integer) getSqlMapClientTemplate().update("trip.updateCBTLoadDetails", map);
                    System.out.println("updateCBT=" + updateCBT);
                }

                consignmentList.add(tripTO);
            }
            System.out.println("map value:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripStatusDetails", map);

            //update trip master
            System.out.println("map value:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripStatus", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatus", map);
            System.out.println("saveAction=" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveAction Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveAction", sqlException);
        }
        return status;
    }

    public int saveTripRoutePlan(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            String[] orderId = tripTO.getOrderIds();
            String[] pointId = tripTO.getPointId();
            String[] pointType = tripTO.getPointType();
            String[] pointOrder = tripTO.getPointOrder();
            String[] pointAddresss = tripTO.getPointAddresss();
            String[] pointPlanDate = tripTO.getPointPlanDate();
            String[] pointPlanTimeHrs = tripTO.getPointPlanTimeHrs();
            String[] pointPlanTimeMins = tripTO.getPointPlanTimeMins();
            System.out.println("orderId.length:" + orderId.length);
            System.out.println("pointId.length:" + pointId.length);
            System.out.println("pointType.length:" + pointType.length);
            System.out.println("pointOrder.length:" + pointOrder.length);
            System.out.println("pointAddresss.length:" + pointAddresss.length);
            System.out.println("pointPlanDate.length:" + pointPlanDate.length);
            System.out.println("pointPlanTimeHrs.length:" + pointPlanTimeHrs.length);
            System.out.println("pointPlanTimeMins.length:" + pointPlanTimeMins.length);
            for (int i = 0; i < orderId.length; i++) {
                map.put("orderId", orderId[i]);
                map.put("pointId", pointId[i]);
                map.put("pointType", pointType[i]);
                map.put("pointOrder", pointOrder[i]);
                map.put("pointAddresss", pointAddresss[i]);
                map.put("pointPlanDate", pointPlanDate[i]);
                map.put("pointPlanTime", pointPlanTimeHrs[i] + ":" + pointPlanTimeMins[i]);
                System.out.println("map value is:" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.saveTripRoutePlan", map);
                System.out.println("saveTripRoutePlan=" + status);
            }
            System.out.println("");
            if (tripTO.getActionName() != null && "1".equals(tripTO.getActionName()) && !"".equals(tripTO.getPreStartLocationId())) {
                if ("0".equals(tripTO.getPreStartLocationStatus())) {
                    map.put("orderId", "0");
                    map.put("pointId", tripTO.getPreStartLocationId());
                    map.put("pointType", "Pre Start");
                    map.put("pointOrder", "0");
                    map.put("pointAddresss", "na");
                    map.put("pointPlanDate", tripTO.getPreStartLocationPlanDate());
                    map.put("pointPlanTime", tripTO.getPreStartLocationPlanTimeHrs() + ":" + tripTO.getPreStartLocationPlanTimeMins());
                    System.out.println("map value is:" + map);
                    status = (Integer) getSqlMapClientTemplate().update("trip.saveTripRoutePlan", map);
                    System.out.println("saveTripRoutePlan=" + status);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripRoutePlan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripRoutePlan", sqlException);
        }
        return status;
    }

    public int saveTripRoutePlanWhenTripUpdate(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            if (tripTO.getVehicleId() != null && !"".equals(tripTO.getVehicleId())) {
                if ("0".equals(tripTO.getPreStartLocationStatus())) {
                    map.put("orderId", "0");
                    map.put("pointId", tripTO.getPreStartLocationId());
                    map.put("pointType", "Pre Start");
                    map.put("pointOrder", "0");
                    map.put("pointAddresss", "na");
                    map.put("pointPlanDate", tripTO.getPreStartLocationPlanDate());
                    map.put("pointPlanTime", tripTO.getPreStartLocationPlanTimeHrs() + ":" + tripTO.getPreStartLocationPlanTimeMins());
                    System.out.println("map value is:" + map);
                    status = (Integer) getSqlMapClientTemplate().update("trip.saveTripRoutePlan", map);
                    System.out.println("saveTripRoutePlan=" + status);
                }
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripRoutePlan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripRoutePlan", sqlException);
        }
        return status;
    }

    public ArrayList getTripSheetDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("documentRequired", tripTO.getDocumentRequired());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("consignmentNo", tripTO.getConsignmentNo());
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        map.put("customerName", tripTO.getCustomerId());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("statusId", tripTO.getStatusId());
        map.put("tripStatusId", tripTO.getTripStatusId());
        map.put("roleId", tripTO.getRoleId());
        map.put("vehicleTypeId", tripTO.getVehicleTypeId());
        map.put("companyId", tripTO.getCompanyId());
        if (!"".equals(tripTO.getTripCode()) && tripTO.getTripCode() != null) {
            map.put("tripCode", "%" + tripTO.getTripCode() + "%");
        } else {
            map.put("tripCode", "");
        }
//        map.put("tripCode", "%"+tripTO.getTripCode() + "%");
        map.put("docketNo", tripTO.getDocketNo());
        map.put("zoneId", tripTO.getZoneId());
        map.put("fleetCenterId", tripTO.getFleetCenterId());
        map.put("cityFromId", tripTO.getCityFromId());
        map.put("podStatus", tripTO.getPodStatus());
        map.put("tripType", tripTO.getTripType());
        map.put("extraExpenseStatus", tripTO.getExtraExpenseStatus());

        map.put("userId", tripTO.getUserId());
        map.put("userMovementType", tripTO.getUserMovementType());
        String empId = (String) getSqlMapClientTemplate().queryForObject("customer.getEmployeeId", map);
        map.put("empId", empId);
        System.out.println("map = " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripSheetDetails", map);
            System.out.println("getTripSheetDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripSheetDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getStatusDetails() {
        Map map = new HashMap();
        System.out.println("map = " + map);
        ArrayList response = new ArrayList();
        try {
            response = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getStatusMaster", map);
            System.out.println("response size=" + response.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStatusDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getStatusDetails List", sqlException);
        }

        return response;
    }

    //       Throttle Starts Here
    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getTripDetails(String tripSheetId) {
        Map map = new HashMap();
        String getTripDetails = "";
        try {
            map.put("tripSheetId", tripSheetId);
            getTripDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getTripDetail", map);
            System.out.println("getTripDetails=" + getTripDetails);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails", sqlException);
        }
        return getTripDetails;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getTripInformation(String tripSheetId) {
        Map map = new HashMap();
        String getTripDetails = "";
        try {
            map.put("tripSheetId", tripSheetId);
            getTripDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getTripInformation", map);
            System.out.println("getTripDetails=" + getTripDetails);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails", sqlException);
        }
        return getTripDetails;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getDriverCount(String tripSheetId, TripTO tripTO) {
        Map map = new HashMap();
        String driverCount = "";
        try {
            map.put("tripSheetId", tripSheetId);
            map.put("vehicleId", tripTO.getVehicleId());
            driverCount = (String) getSqlMapClientTemplate().queryForObject("trip.getDriverCount", map);
            System.out.println("driverCount=" + driverCount);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverCount", sqlException);
        }
        return driverCount;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getTripAdvance(String tripSheetId, TripTO tripTO) {
        Map map = new HashMap();
        String tripAdvance = "";
        try {
            map.put("tripSheetId", tripSheetId);
            map.put("vehicleId", tripTO.getVehicleId());
            tripAdvance = (String) getSqlMapClientTemplate().queryForObject("trip.getTripAdvance", map);
            System.out.println("tripAdvance=" + tripAdvance);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripAdvance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripAdvance", sqlException);
        }
        return tripAdvance;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getTripExpense(String tripSheetId, TripTO tripTO) {
        Map map = new HashMap();
        String tripExpense = "";
        try {
            map.put("tripSheetId", tripSheetId);
            map.put("vehicleId", tripTO.getVehicleId());
            tripExpense = (String) getSqlMapClientTemplate().queryForObject("trip.getTripExpense", map);
            System.out.println("tripExpense=" + tripExpense);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripExpense", sqlException);
        }
        return tripExpense;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int insertSettlement(String tripSheetId, String estimatedExpense, String bpclTransactionAmount, String rcmExpense, String vehicleDieselConsume, String reeferDieselConsume, String tripStartingBalance, String gpsKm, String gpsHm, String totalRunKm, String totalDays, String totalRefeerHours, String milleage, String tripDieselConsume, String tripRcmAllocation, String totalTripAdvance, String totalTripMisc, String totalTripBhatta, String totalTripExpense, String tripBalance, String tripEndBalance, String settlementRemarks, String lrNumber, String paymentMode, String fuelPrice, String tripExtraExpense, int userId, TripTO tripTO) {
        Map map = new HashMap();
        Map map1 = new HashMap();
        int insertSettlement = 0;
        int updateSetttlementMaster = 0;
        int updateSettlementDetail = 0;
        map.put("tripSheetId", tripSheetId);
        map1.put("tripSheetId", tripSheetId);
        map.put("estimatedExpense", estimatedExpense);
        map.put("bpclTransactionAmount", bpclTransactionAmount);
        map.put("rcmExpense", rcmExpense);
        map.put("vehicleDieselConsume", vehicleDieselConsume);
        map.put("reeferDieselConsume", reeferDieselConsume);
        map.put("tripStartingBalance", tripStartingBalance);
        if (!"".equals(gpsHm) && gpsHm != null) {
            map.put("gpsHm", gpsHm);
        } else {
            map.put("gpsHm", "0");
        }
        if (!"".equals(gpsKm)) {
            map.put("gpsKm", gpsKm);
        } else {
            map.put("gpsKm", "0");
        }
        if (!"".equals(totalRunKm)) {
            map.put("totalRunKm", totalRunKm);
        } else {
            map.put("totalRunKm", "0");
        }
        if (!"".equals(totalDays)) {
            map.put("totalDays", totalDays);
        } else {
            map.put("totalDays", "0");
        }
        if (!"".equals(totalRefeerHours)) {
            map.put("totalRefeerHours", totalRefeerHours);
        } else {
            map.put("totalRefeerHours", "0");
        }
        if (!"".equals(milleage)) {
            map.put("milleage", milleage);
        } else {
            map.put("milleage", "0");
        }
        if (!"".equals(fuelPrice)) {
            map.put("fuelPrice", fuelPrice);
        } else {
            map.put("fuelPrice", "0");
        }
        if (!"".equals(tripDieselConsume)) {
            map.put("tripDieselConsume", tripDieselConsume);
        } else {
            map.put("tripDieselConsume", "0");
        }
        if (!"".equals(tripRcmAllocation)) {
            map.put("tripRcmAllocation", tripRcmAllocation);
        } else {
            map.put("tripRcmAllocation", "0");
        }
        if (!"".equals(totalTripAdvance)) {
            map.put("totalTripAdvance", totalTripAdvance);
        } else {
            map.put("totalTripAdvance", "0");

        }
        if (!"".equals(totalTripMisc)) {
            map.put("totalTripMisc", totalTripMisc);
        } else {
            map.put("totalTripMisc", "0");
        }
        if (!"".equals(totalTripBhatta)) {
            map.put("totalTripBhatta", totalTripBhatta);
        } else {
            map.put("totalTripBhatta", "0");
        }
        if (!"".equals(tripExtraExpense)) {
            map.put("tripExtraExpense", tripExtraExpense);
        } else {
            map.put("tripExtraExpense", "0");
        }
        if (!"".equals(totalTripExpense)) {
            map.put("totalTripExpense", totalTripExpense);
        } else {
            map.put("totalTripExpense", "0");
        }
        if (!"".equals(tripBalance)) {
            map.put("tripBalance", tripBalance);
        } else {
            map.put("tripBalance", "0");
        }
        if (!"".equals(tripBalance)) {
            map.put("tripBalance", tripBalance);
        } else {
            map.put("tripBalance", "0");
        }
        if (!"".equals(tripEndBalance)) {
            map.put("tripEndBalance", tripEndBalance);
        } else {
            map.put("tripEndBalance", "0");
        }

        map.put("settlementRemarks", settlementRemarks);
        map1.put("lrNumber", lrNumber);
        map.put("paymentMode", paymentMode);
        map.put("userId", userId);
        map.put("vehicleId", tripTO.getVehicleId());
        System.out.println("the insertSettlement" + map);
        int closureSize = 0;
        int settlementSize = 0;
        int lrNumbers = 0;
        try {
            insertSettlement = (Integer) getSqlMapClientTemplate().update("trip.insertSettlement", map);
            closureSize = (Integer) getSqlMapClientTemplate().queryForObject("trip.getClosureSize", map);
            settlementSize = (Integer) getSqlMapClientTemplate().queryForObject("trip.getSettlementSize", map);
            lrNumbers = (Integer) getSqlMapClientTemplate().update("trip.UpdatelrNumber", map1);
            if (insertSettlement > 0 && closureSize == settlementSize) {
                updateSetttlementMaster = (Integer) getSqlMapClientTemplate().update("trip.updateSetttlementMaster", map);
                updateSettlementDetail = (Integer) getSqlMapClientTemplate().update("trip.updateSettlementDetail", map);
                int status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatus", map);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertSettlement Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertSettlement List", sqlException);
        }

        return insertSettlement;
    }

    public int insertTripDriverSettlementDetails(String tripSheetId, String estimatedExpense, String bpclTransactionAmount, String rcmExpense, String vehicleDieselConsume, String reeferDieselConsume, String tripStartingBalance, String gpsKm, String gpsHm, String totalRunKm, String totalDays, String totalRefeerHours, String milleage, String tripDieselConsume, String tripRcmAllocation, String totalTripAdvance, String totalTripMisc, String totalTripBhatta, String totalTripExpense, String tripBalance, String tripEndBalance, String settlementRemarks, String paymentMode, String fuelPrice, String tripExtraExpense, int userId, TripTO tripTO) {
        Map map = new HashMap();
        int insertTripDriverSettlementDetails = 0;
        int updateSetttlementMaster = 0;
        int updateSettlementDetail = 0;
        map.put("tripSheetId", tripSheetId);
        map.put("estimatedExpense", estimatedExpense);
        map.put("bpclTransactionAmount", bpclTransactionAmount);
        map.put("rcmExpense", rcmExpense);
        map.put("vehicleDieselConsume", vehicleDieselConsume);
        map.put("reeferDieselConsume", reeferDieselConsume);
        map.put("tripStartingBalance", tripStartingBalance);
        if (!"".equals(gpsHm) && gpsHm != null) {
            map.put("gpsHm", gpsHm);
        } else {
            map.put("gpsHm", "0");
        }
        if (!"".equals(gpsKm)) {
            map.put("gpsKm", gpsKm);
        } else {
            map.put("gpsKm", "0");
        }
        if (!"".equals(totalRunKm)) {
            map.put("totalRunKm", totalRunKm);
        } else {
            map.put("totalRunKm", "0");
        }
        if (!"".equals(totalDays)) {
            map.put("totalDays", totalDays);
        } else {
            map.put("totalDays", "0");
        }
        if (!"".equals(totalRefeerHours)) {
            map.put("totalRefeerHours", totalRefeerHours);
        } else {
            map.put("totalRefeerHours", "0");
        }
        if (!"".equals(milleage)) {
            map.put("milleage", milleage);
        } else {
            map.put("milleage", "0");
        }
        if (!"".equals(fuelPrice)) {
            map.put("fuelPrice", fuelPrice);
        } else {
            map.put("fuelPrice", "0");
        }
        if (!"".equals(tripDieselConsume)) {
            map.put("tripDieselConsume", tripDieselConsume);
        } else {
            map.put("tripDieselConsume", "0");
        }
        if (!"".equals(tripRcmAllocation)) {
            map.put("tripRcmAllocation", tripRcmAllocation);
        } else {
            map.put("tripRcmAllocation", "0");
        }
        if (!"".equals(totalTripAdvance)) {
            map.put("totalTripAdvance", totalTripAdvance);
        } else {
            map.put("totalTripAdvance", "0");

        }
        if (!"".equals(totalTripMisc)) {
            map.put("totalTripMisc", totalTripMisc);
        } else {
            map.put("totalTripMisc", "0");
        }
        if (!"".equals(totalTripBhatta)) {
            map.put("totalTripBhatta", totalTripBhatta);
        } else {
            map.put("totalTripBhatta", "0");
        }
        if (!"".equals(tripExtraExpense)) {
            map.put("tripExtraExpense", tripExtraExpense);
        } else {
            map.put("tripExtraExpense", "0");
        }
        if (!"".equals(totalTripExpense)) {
            map.put("totalTripExpense", totalTripExpense);
        } else {
            map.put("totalTripExpense", "0");
        }
        if (!"".equals(tripBalance)) {
            map.put("tripBalance", tripBalance);
        } else {
            map.put("tripBalance", "0");
        }
        if (!"".equals(tripBalance)) {
            map.put("tripBalance", tripBalance);
        } else {
            map.put("tripBalance", "0");
        }
        if (!"".equals(tripEndBalance)) {
            map.put("tripEndBalance", tripEndBalance);
        } else {
            map.put("tripEndBalance", "0");
        }

        map.put("settlementRemarks", settlementRemarks);
        map.put("paymentMode", paymentMode);
        map.put("userId", userId);
        map.put("vehicleId", tripTO.getVehicleId());
        System.out.println("the insertSettlement" + map);
        int closureSize = 0;
        int settlementSize = 0;
        int driverChangeStatus = 0;
        ArrayList inActiveVehicleDriverlist = new ArrayList();
        ArrayList driverClosureList = new ArrayList();
        try {
            driverChangeStatus = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkDriverChangeStatus", map);
            System.out.println("driverChangeStatus = " + driverChangeStatus);
            Float totalKm = 0.0f;
            Float totalHm = 0.0f;
            Float advancePaid = 0.0f;
            int totalTripDays = 0;
            String driverInTrip = "";
            String[] tempDriverInTrip = null;
            int driverCount = 0;
            if (driverChangeStatus > 0) {
                // Driver Cnage Case
                inActiveVehicleDriverlist = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInactiveVehicleDriver", map);
                Iterator itr1 = inActiveVehicleDriverlist.iterator();
                TripTO tpTO = new TripTO();
                while (itr1.hasNext()) {
                    tpTO = new TripTO();
                    tpTO = (TripTO) itr1.next();
                    if (tpTO.getDriverInTrip().contains(",")) {
                        tempDriverInTrip = driverInTrip.split(",");
                        for (int i = 0; i < tempDriverInTrip.length; i++) {
                            map.put("driverId", tempDriverInTrip[i]);
                            driverClosureList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverClosureList", map);
                            Iterator itr = driverClosureList.iterator();
                            TripTO trpTO = new TripTO();
                            while (itr.hasNext()) {
                                trpTO = new TripTO();
                                trpTO = (TripTO) itr.next();
                                map.put("tripId", tripSheetId);
                                map.put("tripSheetId", tripSheetId);
                                map.put("driverId", trpTO.getDriverId());
                                map.put("estimatedExpense", trpTO.getEstimatedExpense());
                                map.put("bpclTransactionAmount", 0);
                                map.put("rcmExpense", trpTO.getEstimatedExpense());
                                map.put("vehicleDieselConsume", Float.parseFloat(trpTO.getRunKm()) / Float.parseFloat(trpTO.getVehicleMileage()));
                                map.put("reeferDieselConsume", Float.parseFloat(trpTO.getRunHm()) * Float.parseFloat(trpTO.getReeferMileage()));
                                map.put("tripStartingBalance", 0);
                                if (!"".equals(gpsHm) && gpsHm != null) {
                                    map.put("gpsHm", gpsHm);
                                } else {
                                    map.put("gpsHm", "0");
                                }
                                if (!"".equals(gpsKm)) {
                                    map.put("gpsKm", gpsKm);
                                } else {
                                    map.put("gpsKm", "0");
                                }
                                if (!"".equals(trpTO.getRunKm())) {
                                    map.put("totalRunKm", trpTO.getRunKm());
                                } else {
                                    map.put("totalRunKm", "0");
                                }
                                if (!"".equals(trpTO.getTripTransitDays())) {
                                    map.put("totalDays", trpTO.getTripTransitDays());
                                } else {
                                    map.put("totalDays", "0");
                                }
                                if (!"".equals(trpTO.getRunHm())) {
                                    map.put("totalRefeerHours", trpTO.getRunHm());
                                } else {
                                    map.put("totalRefeerHours", "0");
                                }
                                if (!"".equals(trpTO.getVehicleMileage())) {
                                    map.put("milleage", trpTO.getVehicleMileage());
                                } else {
                                    map.put("milleage", "0");
                                }
                                if (!"".equals(trpTO.getFuelCost())) {
                                    map.put("fuelPrice", trpTO.getFuelCost());
                                } else {
                                    map.put("fuelPrice", "0");
                                }
                                if (!"".equals(trpTO.getFuelConsumption())) {
                                    map.put("tripDieselConsume", trpTO.getFuelConsumption());
                                } else {
                                    map.put("tripDieselConsume", "0");
                                }
                                if (!"".equals(trpTO.getEstimatedExpense())) {
                                    map.put("tripRcmAllocation", trpTO.getEstimatedExpense());
                                } else {
                                    map.put("tripRcmAllocation", "0");
                                }
                                if (!"".equals(tpTO.getAdvancePaid())) {
                                    map.put("totalTripAdvance", Float.parseFloat(tpTO.getAdvancePaid()) / tempDriverInTrip.length);
                                } else {
                                    map.put("totalTripAdvance", "0");

                                }
                                if (!"".equals(trpTO.getMiscValue())) {
                                    map.put("totalTripMisc", trpTO.getMiscValue());
                                } else {
                                    map.put("totalTripMisc", "0");
                                }
                                if (!"".equals(trpTO.getDriverBhatta())) {
                                    map.put("totalTripBhatta", trpTO.getDriverBhatta());
                                } else {
                                    map.put("totalTripBhatta", "0");
                                }
                                String startDate = tpTO.getStartDate();
                                String endDate = tpTO.getEndDate();
                                map.put("tripStartDate", startDate);
                                map.put("tripEndDate", endDate);
                                String expenseValue = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleExtraExpense", map);
                                if (!"".equals(expenseValue)) {
                                    map.put("tripExtraExpense", Float.parseFloat(expenseValue) / tempDriverInTrip.length);
                                } else {
                                    map.put("tripExtraExpense", "0");
                                }
                                if (!"".equals(trpTO.getEstimatedExpense())) {
                                    map.put("totalTripExpense", Float.parseFloat(trpTO.getEstimatedExpense()));
                                } else {
                                    map.put("totalTripExpense", "0");
                                }
                                if (!"".equals(tripBalance)) {
                                    map.put("tripBalance", (Float.parseFloat(trpTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid()) / tempDriverInTrip.length));
                                } else {
                                    map.put("tripBalance", "0");
                                }
                                if (!"".equals(tripBalance)) {
                                    map.put("tripBalance", (Float.parseFloat(trpTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid()) / tempDriverInTrip.length));
                                } else {
                                    map.put("tripBalance", "0");
                                }
                                if (!"".equals(tripEndBalance)) {
                                    map.put("tripEndBalance", (Float.parseFloat(trpTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid()) / tempDriverInTrip.length));
                                } else {
                                    map.put("tripEndBalance", "0");
                                }

                                map.put("settlementRemarks", settlementRemarks);
                                map.put("paymentMode", paymentMode);
                                map.put("userId", userId);
                                map.put("vehicleId", tripTO.getVehicleId());
                                System.out.println("the insertSettlement" + map);
                                insertTripDriverSettlementDetails = (Integer) getSqlMapClientTemplate().update("trip.insertTripDriverSettlementDetails", map);
                                System.out.println("insert status = " + insertTripDriverSettlementDetails);
                            }
                        }
                    } else {
                        driverInTrip = tpTO.getDriverInTrip();
                        map.put("driverId", driverInTrip);
                        driverClosureList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverClosureList", map);
                        Iterator itr = driverClosureList.iterator();
                        TripTO trpTO = new TripTO();
                        while (itr.hasNext()) {
                            trpTO = new TripTO();
                            trpTO = (TripTO) itr.next();
                            map.put("tripId", tripSheetId);
                            map.put("tripSheetId", tripSheetId);
                            map.put("driverId", trpTO.getDriverId());
                            map.put("estimatedExpense", trpTO.getEstimatedExpense());
                            map.put("bpclTransactionAmount", 0);
                            map.put("rcmExpense", trpTO.getEstimatedExpense());
                            map.put("vehicleDieselConsume", Float.parseFloat(trpTO.getRunKm()) / Float.parseFloat(trpTO.getVehicleMileage()));
                            map.put("reeferDieselConsume", Float.parseFloat(trpTO.getRunHm()) * Float.parseFloat(trpTO.getReeferMileage()));
                            map.put("tripStartingBalance", 0);
                            if (!"".equals(gpsHm) && gpsHm != null) {
                                map.put("gpsHm", gpsHm);
                            } else {
                                map.put("gpsHm", "0");
                            }
                            if (!"".equals(gpsKm)) {
                                map.put("gpsKm", gpsKm);
                            } else {
                                map.put("gpsKm", "0");
                            }
                            if (!"".equals(trpTO.getRunKm())) {
                                map.put("totalRunKm", trpTO.getRunKm());
                            } else {
                                map.put("totalRunKm", "0");
                            }
                            if (!"".equals(trpTO.getTripTransitDays())) {
                                map.put("totalDays", trpTO.getTripTransitDays());
                            } else {
                                map.put("totalDays", "0");
                            }
                            if (!"".equals(trpTO.getRunHm())) {
                                map.put("totalRefeerHours", trpTO.getRunHm());
                            } else {
                                map.put("totalRefeerHours", "0");
                            }
                            if (!"".equals(trpTO.getVehicleMileage())) {
                                map.put("milleage", trpTO.getVehicleMileage());
                            } else {
                                map.put("milleage", "0");
                            }
                            if (!"".equals(trpTO.getFuelCost())) {
                                map.put("fuelPrice", trpTO.getFuelCost());
                            } else {
                                map.put("fuelPrice", "0");
                            }
                            if (!"".equals(trpTO.getFuelConsumption())) {
                                map.put("tripDieselConsume", trpTO.getFuelConsumption());
                            } else {
                                map.put("tripDieselConsume", "0");
                            }
                            if (!"".equals(trpTO.getEstimatedExpense())) {
                                map.put("tripRcmAllocation", trpTO.getEstimatedExpense());
                            } else {
                                map.put("tripRcmAllocation", "0");
                            }
                            if (!"".equals(tpTO.getAdvancePaid())) {
                                map.put("totalTripAdvance", Float.parseFloat(tpTO.getAdvancePaid()));
                            } else {
                                map.put("totalTripAdvance", "0");

                            }
                            if (!"".equals(trpTO.getMiscValue())) {
                                map.put("totalTripMisc", trpTO.getMiscValue());
                            } else {
                                map.put("totalTripMisc", "0");
                            }
                            if (!"".equals(trpTO.getDriverBhatta())) {
                                map.put("totalTripBhatta", trpTO.getDriverBhatta());
                            } else {
                                map.put("totalTripBhatta", "0");
                            }
                            String startDate = tpTO.getStartDate();
                            String endDate = tpTO.getEndDate();
                            map.put("tripStartDate", startDate);
                            map.put("tripEndDate", endDate);
                            String expenseValue = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleExtraExpense", map);
                            if (!"".equals(expenseValue)) {
                                map.put("tripExtraExpense", Float.parseFloat(expenseValue));
                            } else {
                                map.put("tripExtraExpense", "0");
                            }
                            if (!"".equals(trpTO.getEstimatedExpense())) {
                                map.put("totalTripExpense", Float.parseFloat(trpTO.getEstimatedExpense()));
                            } else {
                                map.put("totalTripExpense", "0");
                            }
                            if (!"".equals(tripBalance)) {
                                map.put("tripBalance", (Float.parseFloat(trpTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid())));
                            } else {
                                map.put("tripBalance", "0");
                            }
                            if (!"".equals(tripBalance)) {
                                map.put("tripBalance", (Float.parseFloat(trpTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid())));
                            } else {
                                map.put("tripBalance", "0");
                            }
                            if (!"".equals(tripEndBalance)) {
                                map.put("tripEndBalance", (Float.parseFloat(trpTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid())));
                            } else {
                                map.put("tripEndBalance", "0");
                            }

                            map.put("settlementRemarks", settlementRemarks);
                            map.put("paymentMode", paymentMode);
                            map.put("userId", userId);
                            map.put("vehicleId", tripTO.getVehicleId());
                            System.out.println("the insertSettlement" + map);
                            insertTripDriverSettlementDetails = (Integer) getSqlMapClientTemplate().update("trip.insertTripDriverSettlementDetails", map);
                            System.out.println("insert status = " + insertTripDriverSettlementDetails);
                        }
                    }

                }

                int vehicleDriverCount = 0;
                String driverId = "";
                vehicleDriverCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getVehicleDriverCount", map);
                System.out.println("vehicleDriverCount = " + vehicleDriverCount);
                if (vehicleDriverCount > 0) {
                    map.put("driverId", "");
                    driverClosureList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverClosureList", map);
                    System.out.println("driverClosureList.size() = " + driverClosureList.size());
                    Iterator itr = driverClosureList.iterator();
                    TripTO tTO = new TripTO();
                    while (itr.hasNext()) {
                        tTO = new TripTO();
                        tTO = (TripTO) itr.next();
                        map.put("tripSheetId", tripSheetId);
                        map.put("driverId", tTO.getDriverId());
                        map.put("estimatedExpense", tTO.getEstimatedExpense());
                        map.put("bpclTransactionAmount", 0);
                        map.put("rcmExpense", tTO.getEstimatedExpense());
                        map.put("vehicleDieselConsume", Float.parseFloat(tTO.getRunKm()) / Float.parseFloat(tTO.getVehicleMileage()));
                        map.put("reeferDieselConsume", Float.parseFloat(tTO.getRunHm()) * Float.parseFloat(tTO.getReeferMileage()));
                        map.put("tripStartingBalance", 0);
                        if (!"".equals(gpsHm) && gpsHm != null) {
                            map.put("gpsHm", gpsHm);
                        } else {
                            map.put("gpsHm", "0");
                        }
                        if (!"".equals(gpsKm)) {
                            map.put("gpsKm", gpsKm);
                        } else {
                            map.put("gpsKm", "0");
                        }
                        if (!"".equals(tTO.getRunKm())) {
                            map.put("totalRunKm", tTO.getRunKm());
                        } else {
                            map.put("totalRunKm", "0");
                        }
                        if (!"".equals(tTO.getTripTransitDays())) {
                            map.put("totalDays", tTO.getTripTransitDays());
                        } else {
                            map.put("totalDays", "0");
                        }
                        if (!"".equals(tTO.getRunHm())) {
                            map.put("totalRefeerHours", tTO.getRunHm());
                        } else {
                            map.put("totalRefeerHours", "0");
                        }
                        if (!"".equals(tTO.getVehicleMileage())) {
                            map.put("milleage", tTO.getVehicleMileage());
                        } else {
                            map.put("milleage", "0");
                        }
                        if (!"".equals(tTO.getFuelCost())) {
                            map.put("fuelPrice", tTO.getFuelCost());
                        } else {
                            map.put("fuelPrice", "0");
                        }
                        if (!"".equals(tTO.getFuelConsumption())) {
                            map.put("tripDieselConsume", tTO.getFuelConsumption());
                        } else {
                            map.put("tripDieselConsume", "0");
                        }
                        if (!"".equals(tTO.getEstimatedExpense())) {
                            map.put("tripRcmAllocation", tTO.getEstimatedExpense());
                        } else {
                            map.put("tripRcmAllocation", "0");
                        }
                        if (!"".equals(totalTripAdvance)) {
                            map.put("totalTripAdvance", Float.parseFloat(totalTripAdvance) - Float.parseFloat(tpTO.getAdvancePaid()) / vehicleDriverCount);
                        } else {
                            map.put("totalTripAdvance", "0");

                        }
                        if (!"".equals(tTO.getMiscValue())) {
                            map.put("totalTripMisc", tTO.getMiscValue());
                        } else {
                            map.put("totalTripMisc", "0");
                        }
                        if (!"".equals(tTO.getDriverBhatta())) {
                            map.put("totalTripBhatta", tTO.getDriverBhatta());
                        } else {
                            map.put("totalTripBhatta", "0");
                        }
                        String startDate = tpTO.getStartDate();
                        String endDate = tpTO.getEndDate();
                        map.put("tripStartDate", startDate);
                        map.put("tripEndDate", endDate);
                        String expenseValue = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleExtraExpenseEnd", map);
                        if (!"".equals(expenseValue)) {
                            map.put("tripExtraExpense", Float.parseFloat(expenseValue) / vehicleDriverCount);
                        } else {
                            map.put("tripExtraExpense", "0");
                        }
                        if (!"".equals(totalTripExpense)) {
                            map.put("totalTripExpense", Float.parseFloat(tTO.getEstimatedExpense()) / vehicleDriverCount);
                        } else {
                            map.put("totalTripExpense", "0");
                        }

                        if (!"".equals(tripBalance)) {
                            map.put("tripBalance", (Float.parseFloat(tTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid())) / vehicleDriverCount);
                        } else {
                            map.put("tripBalance", "0");
                        }
                        if (!"".equals(tripBalance)) {
                            map.put("tripBalance", (Float.parseFloat(tTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid())) / vehicleDriverCount);
                        } else {
                            map.put("tripBalance", "0");
                        }
                        if (!"".equals(tripEndBalance)) {
                            map.put("tripEndBalance", (Float.parseFloat(tTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid())) / vehicleDriverCount);
                        } else {
                            map.put("tripEndBalance", "0");
                        }

                        map.put("settlementRemarks", settlementRemarks);
                        map.put("paymentMode", paymentMode);
                        map.put("userId", userId);
                        map.put("vehicleId", tripTO.getVehicleId());
                        System.out.println("the insertSettlement" + map);
                        insertTripDriverSettlementDetails = (Integer) getSqlMapClientTemplate().update("trip.insertTripDriverSettlementDetails", map);
                        System.out.println("insert status = " + insertTripDriverSettlementDetails);
                    }

                }

            } else {
                //Driver Not Change Case
                int vehicleDriverCount = 0;
                String driverId = "";
                vehicleDriverCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getVehicleDriverCount", map);
                System.out.println("vehicleDriverCount = " + vehicleDriverCount);
                if (vehicleDriverCount > 0) {
                    driverClosureList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverClosureList", map);
                    System.out.println("driverClosureList.size() = " + driverClosureList.size());
                    Iterator itr = driverClosureList.iterator();
                    TripTO tpTO = new TripTO();
                    while (itr.hasNext()) {
                        tpTO = new TripTO();
                        tpTO = (TripTO) itr.next();
                        map.put("tripSheetId", tripSheetId);
                        map.put("driverId", tpTO.getDriverId());
                        map.put("estimatedExpense", tpTO.getEstimatedExpense());
                        map.put("bpclTransactionAmount", Float.parseFloat(bpclTransactionAmount) / vehicleDriverCount);
                        map.put("rcmExpense", tpTO.getEstimatedExpense());
                        map.put("vehicleDieselConsume", Float.parseFloat(vehicleDieselConsume) / vehicleDriverCount);
                        map.put("reeferDieselConsume", Float.parseFloat(reeferDieselConsume) / vehicleDriverCount);
                        map.put("tripStartingBalance", tripStartingBalance);
                        if (!"".equals(gpsHm) && gpsHm != null) {
                            map.put("gpsHm", gpsHm);
                        } else {
                            map.put("gpsHm", "0");
                        }
                        if (!"".equals(gpsKm)) {
                            map.put("gpsKm", gpsKm);
                        } else {
                            map.put("gpsKm", "0");
                        }
                        if (!"".equals(tpTO.getRunKm())) {
                            map.put("totalRunKm", tpTO.getRunKm());
                        } else {
                            map.put("totalRunKm", "0");
                        }
                        if (!"".equals(totalDays)) {
                            map.put("totalDays", totalDays);
                        } else {
                            map.put("totalDays", "0");
                        }
                        if (!"".equals(tpTO.getRunHm())) {
                            map.put("totalRefeerHours", tpTO.getRunHm());
                        } else {
                            map.put("totalRefeerHours", "0");
                        }
                        if (!"".equals(tpTO.getVehicleMileage())) {
                            map.put("milleage", tpTO.getVehicleMileage());
                        } else {
                            map.put("milleage", "0");
                        }
                        if (!"".equals(fuelPrice)) {
                            map.put("fuelPrice", fuelPrice);
                        } else {
                            map.put("fuelPrice", "0");
                        }
                        if (!"".equals(tpTO.getFuelConsumption())) {
                            map.put("tripDieselConsume", tpTO.getFuelConsumption());
                        } else {
                            map.put("tripDieselConsume", "0");
                        }
                        if (!"".equals(tpTO.getEstimatedExpense())) {
                            map.put("tripRcmAllocation", tpTO.getEstimatedExpense());
                        } else {
                            map.put("tripRcmAllocation", "0");
                        }
                        if (!"".equals(totalTripAdvance)) {
                            map.put("totalTripAdvance", Float.parseFloat(totalTripAdvance) / vehicleDriverCount);
                        } else {
                            map.put("totalTripAdvance", "0");

                        }
                        if (!"".equals(tpTO.getMiscValue())) {
                            map.put("totalTripMisc", tpTO.getMiscValue());
                        } else {
                            map.put("totalTripMisc", "0");
                        }
                        if (!"".equals(tpTO.getDriverBhatta())) {
                            map.put("totalTripBhatta", tpTO.getDriverBhatta());
                        } else {
                            map.put("totalTripBhatta", "0");
                        }
                        if (!"".equals(tripExtraExpense)) {
                            map.put("tripExtraExpense", Float.parseFloat(tripExtraExpense) / vehicleDriverCount);
                        } else {
                            map.put("tripExtraExpense", "0");
                        }
                        if (!"".equals(totalTripExpense)) {
                            map.put("totalTripExpense", Float.parseFloat(totalTripExpense) / vehicleDriverCount);
                        } else {
                            map.put("totalTripExpense", "0");
                        }
                        if (!"".equals(tripBalance)) {
                            map.put("tripBalance", Float.parseFloat(tripBalance) / vehicleDriverCount);
                        } else {
                            map.put("tripBalance", "0");
                        }
                        if (!"".equals(tripBalance)) {
                            map.put("tripBalance", Float.parseFloat(tripBalance) / vehicleDriverCount);
                        } else {
                            map.put("tripBalance", "0");
                        }
                        if (!"".equals(tripEndBalance)) {
                            map.put("tripEndBalance", Float.parseFloat(tripEndBalance) / vehicleDriverCount);
                        } else {
                            map.put("tripEndBalance", "0");
                        }

                        map.put("settlementRemarks", settlementRemarks);
                        map.put("paymentMode", paymentMode);
                        map.put("userId", userId);
                        map.put("vehicleId", tripTO.getVehicleId());
                        System.out.println("the insertSettlement" + map);
                        insertTripDriverSettlementDetails = (Integer) getSqlMapClientTemplate().update("trip.insertTripDriverSettlementDetails", map);
                        System.out.println("insert status = " + insertTripDriverSettlementDetails);
                    }

                }

            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertSettlement Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertSettlement List", sqlException);
        }

        return insertTripDriverSettlementDetails;
    }

    // Throttle Starts 12-12-2013
    public ArrayList getEndTripSheetDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("consignmentNo", tripTO.getConsignmentNo());
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        map.put("customerName", tripTO.getCustomerName());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("podStatus", tripTO.getPodStatus());

        System.out.println("map in the end trip sheet details= " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEndTripSheetDetails", map);
            System.out.println("getTripSheetDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripSheetDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getClosureEndTripSheetDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("consignmentNo", tripTO.getConsignmentNo());
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        map.put("customerName", tripTO.getCustomerName());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("podStatus", tripTO.getPodStatus());
        int tripVehicleCount = 0;
        System.out.println("map in the end trip sheet details= " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripVehicleCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleCount", map);
            System.out.println("tripVehicleCount = " + tripVehicleCount);
            if (tripVehicleCount == 1) {
                tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getClosureEndTripSheetDetails", map);
            } else {
                tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getClosureEndTripSheetDetailsForVehicleChange", map);
            }
            System.out.println("getTripSheetDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripSheetDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getClosureApprovalProcessHistory(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        System.out.println("map = " + map);
        ArrayList result = new ArrayList();
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getClosureApprovalProcessHistory", map);
            System.out.println("getClosureApprovalProcessHistory result size=" + result.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosureApprovalProcessHistory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getClosureApprovalProcessHistory List", sqlException);
        }

        return result;
    }

    public String getGpsKm(String tripSheetId) {
        ArrayList resultList = new ArrayList();
        Map map = new HashMap();
        String gpsKm = "";
        try {
            map.put("tripSheetId", tripSheetId);
            gpsKm = (String) getSqlMapClientTemplate().queryForObject("trip.getGpsKm", map);
            System.out.println("gpsKm in the dao test by Throttle = " + gpsKm);
            if (gpsKm == null) {
                gpsKm = "0-0";
            }
            //resultList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentOrderRevenue", map);
            System.out.println("gpsKm in dao =" + gpsKm);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getGpsKm Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getGpsKm", sqlException);
        }
        return gpsKm;
    }

    public String getClosureApprovalStatus(String tripSheetId, String requestType) {
        Map map = new HashMap();
        String result = "";
        try {
            map.put("tripSheetId", tripSheetId);
            map.put("requestType", requestType);
            result = (String) getSqlMapClientTemplate().queryForObject("trip.getClosureApprovalStatus", map);
            System.out.println("result=" + result);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosureApprovalStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getClosureApprovalStatus", sqlException);
        }
        return result;
    }

    public String getMiscValue(String vehicleTypeId) {
        ArrayList resultList = new ArrayList();
        Map map = new HashMap();
        String getMiscValue = "";
        try {
            map.put("vehicleTypeId", vehicleTypeId);
            System.out.println("map = " + map);
            if (vehicleTypeId.equals("1021") || vehicleTypeId.equals("1022")) {
                getMiscValue = (String) getSqlMapClientTemplate().queryForObject("trip.getMiscValue", map);
            } else if (vehicleTypeId.equals("1023") || vehicleTypeId.equals("1024")) {
                getMiscValue = (String) getSqlMapClientTemplate().queryForObject("trip.getMiscValue3118", map);
            } else {
                getMiscValue = (String) getSqlMapClientTemplate().queryForObject("trip.getMiscValue", map);
            }
            //resultList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentOrderRevenue", map);
            System.out.println("revenue=" + getMiscValue);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMiscValue Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getMiscValue", sqlException);
        }
        return getMiscValue;
    }

    public String getBookedExpense(String tripSheetId, TripTO tripTO) {
        ArrayList resultList = new ArrayList();
        Map map = new HashMap();
        String result = "";
        try {
            map.put("tripId", tripSheetId);
            map.put("vehicleId", tripTO.getVehicleId());
            result = (String) getSqlMapClientTemplate().queryForObject("trip.getBookedExpense", map);
            System.out.println("result=" + result);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBookedExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBookedExpense", sqlException);
        }
        return result;
    }
//Nithya Start 7 Dec 

    public int savePreTripSheetDetails(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("preStartDate", tripTO.getPreStartDate());
        map.put("preStarttime", tripTO.getTripPreStartHour() + ":" + tripTO.getTripPreStartMinute() + ":00");
        map.put("preOdometerReading", tripTO.getPreOdometerReading());
        map.put("preTripRemarks", tripTO.getPreTripRemarks());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("tripId", tripTO.getTripSheetId());
        map.put("preStartHM", tripTO.getPreStartHM());
        map.put("statusId", tripTO.getStatusId());
        map.put("userId", userId);
        System.out.println("the pretripsheetdetails" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.insertPreTripSheetDetails", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updatePreTripSheetDetails", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatus", map);
            System.out.println("savePreTripSheetDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("savePreTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "savePreTripSheetDetails List", sqlException);
        }

        return status;
    }

    public int setAdvanceAdvice(TripTO tripTO) {
        Map map = new HashMap();
        map.put("estimatedAdvancePerDay", tripTO.getEstimatedAdvancePerDay());
        map.put("userId", tripTO.getUserId());
        map.put("tripId", tripTO.getTripSheetId());
        System.out.println("the setAdvanceAdvice" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.insertTripAdvance", map);
            System.out.println("setAdvanceAdvice size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int saveBillHeader(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceDate", tripTO.getInvoiceDate());
        map.put("invoiceType", tripTO.getInvoiceType());
        map.put("gstLevy", tripTO.getGstLevy());
        map.put("placeofSupply", tripTO.getPlaceofSupply());
        map.put("billingId", tripTO.getBillingId());
        map.put("companyId", tripTO.getCompanyId());
        map.put("userId", tripTO.getUserId());
        map.put("invoiceCode", tripTO.getInvoiceCode());
        map.put("invoiceNo", tripTO.getInvoiceNo());
        map.put("invoiceRefNo", tripTO.getInvRefNo());
        map.put("goodsMovement", tripTO.getGoodsMovement());

        map.put("fuelEscalation", tripTO.getFuelEscalation());
        map.put("fuelEstCharge", tripTO.getFuelEstCharge());
        map.put("fuelType", tripTO.getFuelType());
        map.put("fuelGstLevy", tripTO.getFuelGstLevy());
        map.put("fuelPlaceofSupply", tripTO.getFuelPlaceofSupply());
        map.put("fuelInvoiceNo", tripTO.getFuelInvoiceNo());
        map.put("fuelInvoiceDate", tripTO.getFuelInvoiceDate());
        map.put("remarks", tripTO.getRemarks());
        map.put("fuelEstInvoiceFlag", tripTO.getFuelEstInvoiceFlag());

        map.put("billing", tripTO.getBilling());
        map.put("tripId", tripTO.getTripId());
        map.put("customerId", tripTO.getCustomerId());
        map.put("billType", "1");
        String cancellationFlag = tripTO.getCancelInvoiceFlag();

        System.out.println("cancellationFlag=" + cancellationFlag);

        if ("4".equals(cancellationFlag)) {
            map.put("cancellationFlag", "1");
            map.put("noOfTrips", "0");
            map.put("totalRevenue", tripTO.getTotalCancellationInvoiceAmt());
            map.put("grandTotal", tripTO.getTotalCancellationInvoiceAmt());

        } else {
            map.put("cancellationFlag", "0");
            map.put("noOfTrips", tripTO.getNoOfTrips());
            map.put("totalRevenue", tripTO.getTotalRevenue());
            double grandTotal = Double.parseDouble(tripTO.getGrandTotal()) + Double.parseDouble(tripTO.getFuelEstCharge());
            System.out.println("grandTotal====" + grandTotal);
            map.put("grandTotal", grandTotal);
        }

        map.put("totalExpToBeBilled", tripTO.getTotalExpToBeBilled());
        System.out.println("the saveBillHeader" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().insert("trip.saveBillHeader", map);
            System.out.println("saveBillHeader size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int saveBillHeaderBill(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceDate", tripTO.getInvoiceDateBill());
        map.put("invoiceType", tripTO.getInvoiceTypeBill());
        map.put("gstLevy", tripTO.getGstLevyBill());
        map.put("placeofSupply", tripTO.getPlaceofSupplyBill());
        map.put("invoiceId", tripTO.getInvoiceId());
        map.put("billingId", tripTO.getBillingId());
        map.put("companyId", tripTO.getCompanyId());
        map.put("userId", tripTO.getUserId());
        map.put("invoiceCode", tripTO.getInvoiceCode());
        map.put("invoiceNo", tripTO.getInvoiceNo());
//        map.put("goodsMovement", tripTO.getGoodsMovement());
//        raju
        map.put("billing", tripTO.getBilling());
        map.put("tripId", tripTO.getTripId());
        map.put("customerId", tripTO.getCustomerId());
        map.put("billType", "1");
        map.put("noOfTrips", tripTO.getNoOfTrips());
        map.put("grandTotal", tripTO.getGrandTotal());
        map.put("totalRevenue", tripTO.getTotalRevenue());
        map.put("totalExpToBeBilled", tripTO.getTotalExpToBeBilled());
        map.put("taxAmount", tripTO.getTaxValue());
        System.out.println("the saveBillHeader" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveBillHeaderBill", map);
            System.out.println("saveBillHeaderBillsaveBillHeaderBill size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int saveBillHeaderRemb(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceDate", tripTO.getInvoiceDateRemb());
        map.put("invoiceType", tripTO.getInvoiceTypeRemb());
        map.put("gstLevy", tripTO.getGstLevyRemb());
        map.put("placeofSupply", tripTO.getPlaceofSupplyRemb());
        map.put("invoiceId", tripTO.getInvoiceId());
        map.put("billingId", tripTO.getBillingId());
        map.put("companyId", tripTO.getCompanyId());
        map.put("userId", tripTO.getUserId());
        map.put("invoiceCode", tripTO.getInvoiceCode());
        map.put("invoiceNo", tripTO.getInvoiceNo());
//        raju
        map.put("billing", tripTO.getBilling());
        map.put("tripId", tripTO.getTripId());
        map.put("customerId", tripTO.getCustomerId());
        map.put("billType", "1");
        map.put("noOfTrips", tripTO.getNoOfTrips());
        map.put("grandTotal", tripTO.getGrandTotal());
        map.put("taxAmount", tripTO.getTaxValue());
        map.put("totalRevenue", tripTO.getTotalRevenue());
        map.put("totalExpToBeBilled", tripTO.getTotalExpToBeBilled());
        System.out.println("the saveBillHeader" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveBillHeaderRemb", map);
            System.out.println("saveBillHeaderRembsaveBillHeaderRembsaveBillHeaderRemb size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int saveBillDetails(TripTO tripTO) {
        Map map = new HashMap();

        map.put("userId", tripTO.getUserId());
        map.put("invoiceNo", tripTO.getInvoiceNo());
        map.put("invoiceCode", tripTO.getInvoiceCode());
        map.put("invoiceId", tripTO.getInvoiceId());
        map.put("tripId", tripTO.getTripId());
        map.put("consignmentId", tripTO.getConsignmentId());
        map.put("customerId", tripTO.getCustomerId());
        map.put("totalRevenue", tripTO.getEstimatedRevenue());
        String totalExpToBeBilled = tripTO.getExpenseToBeBilledToCustomer();
        System.out.println("totalExpToBeBilled " + totalExpToBeBilled);
        if (!"".equals(totalExpToBeBilled) && totalExpToBeBilled != null) {
            totalExpToBeBilled = totalExpToBeBilled;
        } else {
            totalExpToBeBilled = "0.00";
        }
        Float grandTotal = Float.parseFloat(tripTO.getEstimatedRevenue())
                + Float.parseFloat(totalExpToBeBilled);
        map.put("totalExpToBeBilled", totalExpToBeBilled);

//        map.put("totalExpToBeBilled", tripTO.getExpenseToBeBilledToCustomer());
//        Float grandTotal = Float.parseFloat(tripTO.getEstimatedRevenue()) + Float.parseFloat(tripTO.getExpenseToBeBilledToCustomer());
        map.put("grandTotal", grandTotal);
        System.out.println("the saveBillDetails:" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().insert("trip.saveBillDetails", map);
            System.out.println("saveBillDetails size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int saveBillDetailsBill(TripTO tripTO) {
        Map map = new HashMap();

        map.put("userId", tripTO.getUserId());
        map.put("invoiceNo", tripTO.getInvoiceNo());
        map.put("invoiceCode", tripTO.getInvoiceCode());
        map.put("invoiceId", tripTO.getInvoiceId());
        map.put("tripId", tripTO.getTripId());
        map.put("consignmentId", tripTO.getConsignmentId());
        map.put("customerId", tripTO.getCustomerId());
        map.put("totalRevenue", tripTO.getEstimatedRevenue());
        map.put("totalExpToBeBilled", tripTO.getExpenseToBeBilledToCustomer());
        Float grandTotal = Float.parseFloat(tripTO.getEstimatedRevenue()) + Float.parseFloat(tripTO.getExpenseToBeBilledToCustomer());
        map.put("grandTotal", grandTotal);
        System.out.println("the saveBillDetails:" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().insert("trip.saveBillDetailsBill", map);
            System.out.println("saveBillDetails size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int saveBillDetailsRemb(TripTO tripTO) {
        Map map = new HashMap();

        map.put("userId", tripTO.getUserId());
        map.put("invoiceNo", tripTO.getInvoiceNo());
        map.put("invoiceCode", tripTO.getInvoiceCode());
        map.put("invoiceId", tripTO.getInvoiceId());
        map.put("tripId", tripTO.getTripId());
        map.put("consignmentId", tripTO.getConsignmentId());
        map.put("customerId", tripTO.getCustomerId());
        map.put("totalRevenue", tripTO.getEstimatedRevenue());
        map.put("totalExpToBeBilled", tripTO.getExpenseToBeBilledToCustomer());
        Float grandTotal = Float.parseFloat(tripTO.getEstimatedRevenue()) + Float.parseFloat(tripTO.getExpenseToBeBilledToCustomer());
        map.put("grandTotal", grandTotal);
        System.out.println("the saveBillDetails:" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().insert("trip.saveBillDetailsRemb", map);
            System.out.println("saveBillDetails size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int saveBillDetailExpense(TripTO tripTO) {
        Map map = new HashMap();

        map.put("userId", tripTO.getUserId());
        map.put("invoiceCode", tripTO.getInvoiceCode());
        map.put("invoiceDetailId", tripTO.getInvoiceDetailId());
        map.put("tripId", tripTO.getTripId());
        map.put("expenseId", tripTO.getExpenseId());
        map.put("expenseName", tripTO.getExpenseName() + "; " + tripTO.getExpenseRemarks());
        map.put("marginValue", tripTO.getMarginValue());
        map.put("taxPercentage", tripTO.getTaxPercentage());
        map.put("expenseValue", tripTO.getExpenseValue());
        Float totalValue = 0.00F;
        Float taxValue = 0.00F;
        Float nettValue = 0.00F;
        String marginValue = "0";
        String expenseValue = "0";
        String taxPercentage = "0";
        if (tripTO.getMarginValue() == null || "".equals(tripTO.getMarginValue())) {
            marginValue = "0";
        } else {
            marginValue = tripTO.getMarginValue();
        }
        if (tripTO.getTaxPercentage() == null || "".equals(tripTO.getTaxPercentage())) {
            taxPercentage = "0";
        } else {
            taxPercentage = tripTO.getTaxPercentage();
        }
        if (tripTO.getExpenseValue() == null || "".equals(tripTO.getExpenseValue())) {
            expenseValue = "0";
        } else {
            expenseValue = tripTO.getExpenseValue();
        }
        totalValue = (Float.parseFloat(expenseValue) + Float.parseFloat(marginValue));
        taxValue = ((Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                * Float.parseFloat(taxPercentage) / 100);
        nettValue = (Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                + ((Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                * Float.parseFloat(taxPercentage) / 100);
        map.put("taxValue", taxValue);
        map.put("totalValue", totalValue);
        map.put("nettValue", nettValue);
        System.out.println("the saveBillDetailExpense:" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveBillDetailExpense", map);
            System.out.println("saveBillDetailExpense size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int saveBillDetailExpenseBill(TripTO tripTO) {
        Map map = new HashMap();

        map.put("userId", tripTO.getUserId());
        map.put("invoiceCode", tripTO.getInvoiceCode());
        map.put("invoiceDetailId", tripTO.getInvoiceDetailId());
        map.put("tripId", tripTO.getTripId());
        map.put("expenseId", tripTO.getExpenseId());
        map.put("expenseName", tripTO.getExpenseName() + "; " + tripTO.getExpenseRemarks());
        map.put("marginValue", tripTO.getMarginValue());
        map.put("taxPercentage", tripTO.getTaxPercentage());
        map.put("expenseValue", tripTO.getExpenseValue());
        Float totalValue = 0.00F;
        Float taxValue = 0.00F;
        Float nettValue = 0.00F;
        String marginValue = "0";
        String expenseValue = "0";
        String taxPercentage = "0";
        if (tripTO.getMarginValue() == null || "".equals(tripTO.getMarginValue())) {
            marginValue = "0";
        } else {
            marginValue = tripTO.getMarginValue();
        }
        if (tripTO.getTaxPercentage() == null || "".equals(tripTO.getTaxPercentage())) {
            taxPercentage = "0";
        } else {
            taxPercentage = tripTO.getTaxPercentage();
        }
        if (tripTO.getExpenseValue() == null || "".equals(tripTO.getExpenseValue())) {
            expenseValue = "0";
        } else {
            expenseValue = tripTO.getExpenseValue();
        }
        totalValue = (Float.parseFloat(expenseValue) + Float.parseFloat(marginValue));
        taxValue = ((Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                * Float.parseFloat(taxPercentage) / 100);
        nettValue = (Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                + ((Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                * Float.parseFloat(taxPercentage) / 100);
        map.put("taxValue", taxValue);
        map.put("totalValue", totalValue);
        map.put("nettValue", nettValue);
        System.out.println("the saveBillDetailExpense:" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveBillDetailExpenseBill", map);
            System.out.println("saveBillDetailExpense size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int saveBillDetailExpenseRemb(TripTO tripTO) {
        Map map = new HashMap();

        map.put("userId", tripTO.getUserId());
        map.put("invoiceCode", tripTO.getInvoiceCode());
        map.put("invoiceDetailId", tripTO.getInvoiceDetailId());
        map.put("tripId", tripTO.getTripId());
        map.put("expenseId", tripTO.getExpenseId());
        map.put("expenseName", tripTO.getExpenseName() + "; " + tripTO.getExpenseRemarks());
        map.put("marginValue", tripTO.getMarginValue());
        map.put("taxPercentage", tripTO.getTaxPercentage());
        map.put("expenseValue", tripTO.getExpenseValue());
        Float totalValue = 0.00F;
        Float taxValue = 0.00F;
        Float nettValue = 0.00F;
        String marginValue = "0";
        String expenseValue = "0";
        String taxPercentage = "0";
        if (tripTO.getMarginValue() == null || "".equals(tripTO.getMarginValue())) {
            marginValue = "0";
        } else {
            marginValue = tripTO.getMarginValue();
        }
        if (tripTO.getTaxPercentage() == null || "".equals(tripTO.getTaxPercentage())) {
            taxPercentage = "0";
        } else {
            taxPercentage = tripTO.getTaxPercentage();
        }
        if (tripTO.getExpenseValue() == null || "".equals(tripTO.getExpenseValue())) {
            expenseValue = "0";
        } else {
            expenseValue = tripTO.getExpenseValue();
        }
        totalValue = (Float.parseFloat(expenseValue) + Float.parseFloat(marginValue));
        taxValue = ((Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                * Float.parseFloat(taxPercentage) / 100);
        nettValue = (Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                + ((Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                * Float.parseFloat(taxPercentage) / 100);
        map.put("taxValue", taxValue);
        map.put("totalValue", totalValue);
        map.put("nettValue", nettValue);
        System.out.println("the saveBillDetailExpense:" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveBillDetailExpenseRemb", map);
            System.out.println("saveBillDetailExpense size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public String updateStartTripSheet(TripTO tripTO, int userId, SqlMapClient session) {
        synchronized (this) {
            Map map = new HashMap();
            System.out.println("EnteringupdateStartTripSheet=== ");

            map.put("userId", userId);
            map.put("planStartTime", tripTO.getPlanStartHour() + ":" + tripTO.getPlanStartMinute() + ":00");
            map.put("planStartDate", tripTO.getPlanStartDate());
            map.put("startDate", tripTO.getStartDate());
            map.put("startTime", tripTO.getTripStartHour() + ":" + tripTO.getTripStartMinute() + ":00");
            map.put("startOdometerReading", tripTO.getStartOdometerReading());
            map.put("startHM", "0");
            map.put("tripSheetId", tripTO.getTripSheetId());
            map.put("tripId", tripTO.getTripSheetId());
            map.put("startTripRemarks", tripTO.getStartTripRemarks());
            map.put("statusId", tripTO.getStatusId());
            map.put("tripPlanEndDate", tripTO.getTripPlanEndDate());
            map.put("tripPlanEndTime", tripTO.getTripPlanEndTime());
            map.put("lrNumber", tripTO.getLrNumber());
            map.put("vehicleactreportdate", tripTO.getVehicleactreportdate());
            map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");
            map.put("vehicleloadreportdate", tripTO.getVehicleloadreportdate());
            map.put("vehicleloadreporttime", tripTO.getVehicleloadreporthour() + ":" + tripTO.getVehicleloadreportmin() + ":00");
            map.put("vehicleloadtemperature", tripTO.getVehicleloadtemperature());

            map.put("documentRecivedDate", tripTO.getDocumentRecivedDate());
            map.put("documentRecivedTime", tripTO.getDocumentRecivedHr() + ":" + tripTO.getDocumentRecivedMi() + ":00");
            map.put("documentRecivedHr", tripTO.getDocumentRecivedHr());
            map.put("documentRecivedMi", tripTO.getDocumentRecivedMi());

            map.put("vehicleTypeId", tripTO.getVehicleTypeId());
            map.put("vehicleNo", tripTO.getVehicleNo());

            map.put("vendorId", tripTO.getVendorId());
            map.put("sealNoType", tripTO.getSealNoType());
            map.put("sealNo", tripTO.getSealNo());
            map.put("remarks", tripTO.getStartTripRemarks().trim());

            map.put("driverName", tripTO.getDriverName());
            map.put("driverMobile", tripTO.getDriverMobile());
            map.put("escortName", tripTO.getEscortName());
            map.put("escortMobile", tripTO.getEscortMobile());
            map.put("dlNumber", tripTO.getDlNumber());
            map.put("eWayBillNo", tripTO.geteWayBillNo());
            map.put("deviceid", tripTO.getDeviceId());
            
            map.put("materialDesc", tripTO.getMaterialDesc());
            map.put("insuranceDetail", tripTO.getInsuranceDetail());
            map.put("appDate", tripTO.getAppDate());
//            map.put("appTime", tripTO.getAppTime());
            map.put("tatTime", tripTO.getTatTime());
            
            map.put("appTime", tripTO.getAppHour()+":"+tripTO.getAppMin()+":00");

            String[] loadedPkg = tripTO.getLoadedPkgs();

            if (loadedPkg == null) {
                map.put("loadedpkgs", "0");
            } else {
                map.put("loadedpkgs", loadedPkg[0]);
            }

            int status = 0;
            int status1 = 0;
            String checkVehicleId = "";
            ArrayList tripDetails = new ArrayList();
            String printDocketNo = "";

            try {
                System.out.println("mapping=="+map);
                 int status2 = (Integer) session.update("trip.updateTripMaster", map);
                        System.out.println("updateTripMaster -" + status2);

                status = (Integer) session.update("trip.updateStartTripSheet", map);
                String consignmentId = "";
                consignmentId = (String) session.queryForObject("trip.getConsignmentId", map);
                System.out.println("consignmentId : " + consignmentId);
                map.put("consignmentId", consignmentId);
                map.put("LRNumber", tripTO.getLrNumber());

                String regNos = tripTO.getVehicleNo();

                if (!regNos.equals("")) {
                    map.put("regNo", regNos + "");
                    checkVehicleId = (String) session.queryForObject("trip.checkExistingVehicle", map);

                    if ("".equals(checkVehicleId) || checkVehicleId == null) {
                        String ownership = (String) session.queryForObject("trip.checkVehicleOwnership", map);
                        map.put("ownership", ownership);
                        int vehicleInsertId = (Integer) session.insert("trip.insertVehicleMaster", map);
                        System.out.println("vehicleInsertId== if" + vehicleInsertId);
                        map.put("vehicleId", vehicleInsertId);
                        int vehilceRegInsert = (Integer) session.insert("trip.insertVehicleRegNo", map);
                        System.out.println("vehilceRegInsert- if" + vehilceRegInsert);

                        if (ownership.equals("2")) {
                            String contractDedicateId = (String) session.queryForObject("trip.checkDedicateContractId", map);
                            map.put("contractDedicateId", contractDedicateId);
                            map.put("vehicleRegNo", regNos);
                            System.out.println("the updatetripsheetdetails : " + map);

                            int vehicleConfig = (Integer) session.insert("trip.insertVehicleVendor", map);
                            System.out.println("vehicleConfig : " + vehicleConfig);

                            int checkUnits = (Integer) session.queryForObject("trip.checkDedicateUnits", map);

                            map.put("checkUnits", checkUnits);
                            int updateTripVehicle = (Integer) session.update("trip.updateContractUnits", map);
                            System.out.println("updateTripVehicle : " + updateTripVehicle);

                        }

                        int updateTripVehicle = (Integer) session.update("trip.updateTripVehicle", map);
                        System.out.println("updateTripVehicle- if" + updateTripVehicle);

                    } else {
                        map.put("vehicleId", checkVehicleId);
                        int updateTripVehicle = (Integer) session.update("trip.updateTripVehicle", map);
                        System.out.println("updateTripVehicle else -" + updateTripVehicle);
                    }

                    System.out.println("the updatetripsheetdetails : " + map);

                    status1 = (Integer) session.update("trip.updateLHCVehicleNo", map);

                    status1 = (Integer) session.update("trip.updateTripDriverDetails", map);
                }

                map.put("expenseName", "1047");
                map.put("detentionAmt", tripTO.getOriginDetention());
                map.put("customerId", tripTO.getCustomerId());

                System.out.println("detentionAmt : " + tripTO.getOriginDetention());

                if (!"".equals(tripTO.getOriginDetention()) && !"0".equals(tripTO.getOriginDetention()) && tripTO.getStatusId().equals("10")) {
                    status1 = (Integer) session.update("trip.updateTripDetention", map);
                    System.out.println("status-deded----" + status1);
                }
                // Lr Number Generation starts here...

                String accYear = ThrottleConstants.accountYear;
                int a = 1;

                ////////////////////////////////////// Temp LR part 24-10-2018 /////////////////////////////////
                map.put("LRNumber", tripTO.getLrNumber());

                if (!tripTO.getLrNumber().equals("")) {
                    map.put("accYear", accYear);
                    map.put("invoiceNumber", tripTO.getInvoiceNumber());
                    map.put("invoiceValue", tripTO.getInvoiceValue());
                    map.put("invoiceDate", tripTO.getInvoiceDate());
                    map.put("docketNo", tripTO.getLrNumber());
                    map.put("lrCode", tripTO.getLrNumber());
                    map.put("invoiceWeight", tripTO.getPackageWeight());
                    System.out.println("LRNumber map : " + map);

                    int lrcheck = 0;
                    int lrId = 0;
                    lrcheck = (Integer) session.queryForObject("trip.checkLrExists", map);

                    if (lrcheck == 0) {
                        lrId = (Integer) session.insert("trip.insertLR", map);
                    } else {
                        status1 = (Integer) session.update("trip.updateLrNumberMaster", map);
                    }

                    System.out.println("lrId : " + lrId);

                    ArrayList GetOrderIdFirstPoint = new ArrayList();
                    GetOrderIdFirstPoint = (ArrayList) session.queryForList("trip.GetOrderIdFirstPoint", map);
                    Iterator itr1 = GetOrderIdFirstPoint.iterator();
                    TripTO tpTO = new TripTO();
                    int cnt = 1;
                    while (itr1.hasNext()) {
                        tpTO = new TripTO();
                        tpTO = (TripTO) itr1.next();
                        if (cnt == 1) {
                            String orderSequenceOne = tpTO.getOrderId();
                            map.put("orderSequenceOne", orderSequenceOne);
                        } else {
                            String orderSequenceTwo = tpTO.getOrderId();
                            map.put("orderSequenceTwo", orderSequenceTwo);
                        }
                        cnt++;
                    }

                    map.put("pointOrder", a);

                    int checkpoint = (Integer) session.queryForObject("trip.getTripTouchPointDetails", map);

                    if (checkpoint > 0) {
                        status = (Integer) session.update("trip.updateEndTouchPointDetails", map);
                    } else {
                        status = (Integer) session.update("trip.insertEndTouchPointDetails", map);
                    }
                    a++;

                }

                map.put("statusId", tripTO.getStatusId());
                if (!tripTO.getTotalPackages().equals("")) {
                    map.put("totalPackages", tripTO.getTotalPackages());
                } else {
                    map.put("totalPackages", "0");
                }

                String[] consignmentOrderIds = tripTO.getConsignmentOrderId();
                String[] loadOrderId = tripTO.getLoadOrderId();
                System.out.println("loadOrderId------" + loadOrderId.length);

                String[] pendingPkgs = tripTO.getPendingPkgs();
                String[] loadedPkgs = tripTO.getLoadedPkgs();

                //          String[] pendingWeight = tripTO.getPendingWeight();
                //          String[] loadedWeight = tripTO.getLoadedWeight();
                //          String[] pendingGrossWeight = tripTO.getPendingGrossWeight();
                //          String[] loadedGrossWeight = tripTO.getLoadedGrossWeight();
                //          System.out.println("loadedGrossWeight" + loadedGrossWeight[0]);
                int j = 1;
                for (int i = 0; i < 1; i++) {
                    map.put("consignmentOrderId", loadOrderId[i]);
                    map.put("pendingWeight", 0);
                    map.put("loadedWeight", 0);
                    map.put("orderSequence", j);
                    map.put("pendingGrossWeight", 0);
                    map.put("loadedGrossWeight", 0);
                    j++;

                    map.put("loadedPkgs", loadedPkgs[i]);

                    System.out.println("loadedPkgs : " + loadedPkgs[i]);

                    status1 = (Integer) session.update("trip.updateTripConsignmentStatus", map);

                    map.put("pendingTripPkgs", pendingPkgs[i]);
                    System.out.println("pendingPkgs : " + pendingPkgs[i]);
                    System.out.println("TotalPackages : " + tripTO.getTotalPackages());

                    map.put("pendingPkgs", pendingPkgs[i]);

                    if (Double.parseDouble(pendingPkgs[i]) == 0) {
                        map.put("statusId", tripTO.getStatusId());
                    } else {
                        System.out.println("else");
                        map.put("statusId", "5");
                    }

                    System.out.println("consignemnt map value is:" + map);
                    status1 = (Integer) session.update("trip.updateConsignOrderStatus", map);

                    System.out.println("saveTripConsignments=" + status1);
                }

                //////////////////// API Auto Entry ////////////////////////////
                map.put("tripid", tripTO.getTripSheetId());
                map.put("driverMobile", tripTO.getDriverMobile());
                map.put("regNo", regNos + "");
                map.put("vendorId", tripTO.getVendorId());
                map.put("customerId", tripTO.getCustomerId());
                map.put("LRNumber", tripTO.getLrNumber());

                if (loadedPkg == null) {
                    map.put("loadedpkgs", "0");
                } else {
                    map.put("loadedpkgs", loadedPkg[0] + " Pallets");
                }

                map.put("vehicleTypeId", tripTO.getVehicleTypeId());
                map.put("dispatchDate", tripTO.getStartDate());
                map.put("userId", userId);

                String tripGSPDetails = (String) session.queryForObject("trip.getTripLocationDetails", map);

                int tripGPS = (Integer) session.queryForObject("trip.IsTripGPSExist", map);

                if (tripGPS == 0) {

                    String[] trips = tripGSPDetails.split("~");

                    if (trips != null) {

                        map.put("srcname", trips[0]);
                        map.put("toname", trips[1]);
                        map.put("destname", trips[1]);
                        map.put("eta_in_hours", trips[2]);

                        status = (Integer) session.update("trip.updateTripAPIDetails", map);

                    }
                }
                //////////////////// Advance Auto Entry ////////////////////////////

                String tripadvanceDetails = (String) session.queryForObject("trip.getActualAdvanceAmount", map);

                if (tripadvanceDetails == null) {
                    tripadvanceDetails = "0";
                }

                double tripadvance = Double.parseDouble(tripadvanceDetails);

                if (tripadvance > 0) {

                    map.put("advancerequestamt", tripadvance);
                    map.put("userId", userId);
                    System.out.println("map val" + map);

                    int manualAdvanceRequest = (Integer) session.insert("trip.autoAdvanceRequest", map);

                    System.out.println("manualAdvanceRequest : " + manualAdvanceRequest);
                }

                //                   else {
//
//                    String manualLrNumberCount = (String) session.queryForObject("trip.getManualLrCount", map);
//
//                    System.out.println("manualLrNumberCount : " + manualLrNumberCount);
//
//                    if (manualLrNumberCount != null) {
//
//                        String[] manualLrNumber = manualLrNumberCount.split("~");
//                        System.out.println("ManualLrLength.length :: " + manualLrNumber.length);
//                        String manualLrCode = "";
//
//                        for (int i = 0; i < manualLrNumber.length; i++) {
//                            manualLrCode = manualLrNumber[i];
//                            System.out.println("manual lr no : " + manualLrNumber[i]);
//                            map.put("lrCode", manualLrCode);
//                            map.put("docketNo", manualLrCode);
//                            map.put("accYear", accYear);
//                            map.put("invoiceWeight", tripTO.getPackageWeight());
//
//                            if (!manualLrCode.equalsIgnoreCase("NA") && !manualLrCode.equalsIgnoreCase("0")) {
//
//                                int lrcheck = 0;
//                                int lrId = 0;
//                                lrcheck = (Integer) session.queryForObject("trip.checkLrExists", map);
//
//                                if (lrcheck == 0) {
//                                    lrId = (Integer) session.insert("trip.insertLR", map);
//                                } else {
//                                    status1 = (Integer) session.update("trip.updateLrNumberMaster", map);
//                                }
//
//                                if (i == 0) {
//                                    printDocketNo = manualLrCode;
//                                    System.out.println("ifffff");
//                                    map.put("invoiceNumber", tripTO.getInvoiceNumber());
//                                    map.put("invoiceValue", tripTO.getInvoiceValue());
//                                    map.put("invoiceDate", tripTO.getInvoiceDate());
//
//                                    ArrayList GetOrderIdFirstPoint = new ArrayList();
//                                    GetOrderIdFirstPoint = (ArrayList) session.queryForList("trip.GetOrderIdFirstPoint", map);
//                                    System.out.println("GetOrderIdFirstPoint=" + GetOrderIdFirstPoint.size());
//                                    Iterator itr1 = GetOrderIdFirstPoint.iterator();
//                                    TripTO tpTO = new TripTO();
//                                    int cnt = 1;
//                                    while (itr1.hasNext()) {
//                                        tpTO = new TripTO();
//                                        tpTO = (TripTO) itr1.next();
//                                        if (cnt == 1) {
//                                            String orderSequenceOne = tpTO.getOrderId();
//                                            map.put("orderSequenceOne", orderSequenceOne);
//                                        } else {
//                                            String orderSequenceTwo = tpTO.getOrderId();
//                                            map.put("orderSequenceTwo", orderSequenceTwo);
//                                        }
//                                        cnt++;
//                                    }
//                                } else {
//                                    System.out.println("else.....");
//                                    map.put("invoiceNumber", "");
//                                    map.put("invoiceValue", "");
//                                    map.put("invoiceDate", "");
//                                    ArrayList GetOrderIdSecondPoint = new ArrayList();
//                                    GetOrderIdSecondPoint = (ArrayList) session.queryForList("trip.GetOrderIdSecondPoint", map);
//                                    System.out.println("GetOrderIdSecondPoint---" + GetOrderIdSecondPoint.size());
//                                    Iterator itr1 = GetOrderIdSecondPoint.iterator();
//                                    TripTO tpTO = new TripTO();
//                                    int cnt = 1;
//                                    while (itr1.hasNext()) {
//                                        tpTO = new TripTO();
//                                        tpTO = (TripTO) itr1.next();
//                                        if (cnt == 1) {
//                                            String orderSequenceOne = tpTO.getOrderId();
//                                            System.out.println("tpTO.getOrderId()-" + tpTO.getOrderId());
//                                            map.put("orderSequenceOne", orderSequenceOne);
//                                        } else {
//                                            String orderSequenceTwo = tpTO.getOrderId();
//                                            System.out.println("tpTO.getOrderId()-" + tpTO.getOrderId());
//                                            map.put("orderSequenceTwo", orderSequenceTwo);
//                                        }
//                                        cnt++;
//                                    }
//
//                                }
//
//                                map.put("pointOrder", a);
//
//                                int checkpoint = (Integer) session.queryForObject("trip.getTripTouchPointDetails", map);
//
//                                if (checkpoint > 0) {
//                                    status = (Integer) session.update("trip.updateEndTouchPointDetails", map);
//                                } else {
//                                    status = (Integer) session.update("trip.insertEndTouchPointDetails", map);
//                                }
//                                a++;
//
//                            }
//                        }
//
//                    } else {
//
//                        /////////////////////////////////// Temp LR part 24-10-2018 /////////////////////////////////
//                        String lrNumberCount = (String) session.queryForObject("trip.getLrCount", map);
//                        String[] lrNum = lrNumberCount.split("-");
//                        System.out.println("lrLength.length :: " + lrNum.length);
//
//                        int j = 1;
//                        for (int i = 0; i < lrNum.length - 1; i++) {
//                            map.put("accYear", accYear);
//                            String lrNumberSequence = "";
//                            lrNumberSequence = (String) session.queryForObject("trip.getLrNo", map);
//                            System.out.println("lrNumberSequence: " + lrNumberSequence);
//
//                            if (lrNumberSequence == null) {
//                                lrNumberSequence = "1";
//                            }
//
//                            String lrCode = "";
//                            String lrCity = (String) session.queryForObject("trip.getLRCity", map);
//                            String[] lrCty = lrCity.split("-");
//
//                            if (i == 0) {
//                                lrCode = "3PL-" + lrCty[0] + " " + lrNumberSequence;
//                                printDocketNo = lrCode;
//                            } else {
//                                lrCode = "3PL-" + lrCty[0] + " " + lrNumberSequence;
//                            }
//
//                            map.put("docketNo", lrCode);
//                            map.put("lrCode", lrCode);
//                            map.put("invoiceWeight", tripTO.getPackageWeight());
//                            System.out.println("invoiceWeight : " + tripTO.getPackageWeight());
//
//                            int lrcheck = 0;
//                            int lrId = 0;
//
////                    lrcheck = (Integer) session.queryForObject("trip.checkLrExists", map);
////
////                    if (lrcheck == 0) {
////                        lrId = (Integer) session.insert("trip.insertLR", map);
////                    }else{
////                        status1 = (Integer) session.update("trip.updateLrNumberMaster", map);
////                    }
//                            if (i == 0) {
//                                map.put("invoiceNumber", tripTO.getInvoiceNumber());
//                                map.put("invoiceValue", tripTO.getInvoiceValue());
//                                map.put("invoiceDate", tripTO.getInvoiceDate());
//
//                                ArrayList GetOrderIdFirstPoint = new ArrayList();
//                                GetOrderIdFirstPoint = (ArrayList) session.queryForList("trip.GetOrderIdFirstPoint", map);
//                                Iterator itr1 = GetOrderIdFirstPoint.iterator();
//                                TripTO tpTO = new TripTO();
//                                int cnt = 1;
//                                while (itr1.hasNext()) {
//                                    tpTO = new TripTO();
//                                    tpTO = (TripTO) itr1.next();
//                                    if (cnt == 1) {
//                                        String orderSequenceOne = tpTO.getOrderId();
//                                        map.put("orderSequenceOne", orderSequenceOne);
//                                    } else {
//                                        String orderSequenceTwo = tpTO.getOrderId();
//                                        map.put("orderSequenceTwo", orderSequenceTwo);
//                                    }
//                                    cnt++;
//                                }
//                            } else {
//                                map.put("invoiceNumber", "");
//                                map.put("invoiceValue", "");
//                                map.put("invoiceDate", "");
//                                ArrayList GetOrderIdSecondPoint = new ArrayList();
//                                GetOrderIdSecondPoint = (ArrayList) session.queryForList("trip.GetOrderIdSecondPoint", map);
//                                Iterator itr1 = GetOrderIdSecondPoint.iterator();
//                                TripTO tpTO = new TripTO();
//                                int cnt = 1;
//                                while (itr1.hasNext()) {
//                                    tpTO = new TripTO();
//                                    tpTO = (TripTO) itr1.next();
//                                    if (cnt == 1) {
//                                        String orderSequenceOne = tpTO.getOrderId();
//                                        System.out.println("tpTO.getOrderId()-" + tpTO.getOrderId());
//                                        map.put("orderSequenceOne", orderSequenceOne);
//                                    } else {
//                                        String orderSequenceTwo = tpTO.getOrderId();
//                                        System.out.println("tpTO.getOrderId()-" + tpTO.getOrderId());
//                                        map.put("orderSequenceTwo", orderSequenceTwo);
//                                    }
//                                    cnt++;
//                                }
//
//                            }
//
//                            map.put("pointOrder", j);
//
//                            int checkpoint = (Integer) session.queryForObject("trip.getTripTouchPointDetails", map);
//
//                            if (checkpoint > 0) {
//                                status = (Integer) session.update("trip.updateEndTouchPointDetails", map);
//                            } else {
//                                status = (Integer) session.update("trip.insertEndTouchPointDetails", map);
//                            }
//                            System.out.println("insertEndTouchPointDetails--" + status);
//                            j++;
//
//                        }
//
//                    }
//                }
                //////////////////// Advance Auto Entry ////////////////////////////
                //                String tripRevenueDetails = "";
                //                String tripTemp[] = null;
                //                String customerId = "";
                //                String tripType = "";
                //                String estimatedRevenue = "";
                //                String emptyTrip = "";
                //                tripRevenueDetails = (String) session.queryForObject("trip.getTripRevenueDetails", map);
                //                System.out.println("tripRevenueDetails = " + tripRevenueDetails);
                //                if (!"".equals(tripRevenueDetails)) {
                //                    tripTemp = tripRevenueDetails.split("~");
                //                    customerId = tripTemp[0];
                //                    tripType = tripTemp[1];
                //                    estimatedRevenue = tripTemp[2];
                //                    emptyTrip = tripTemp[3];
                //                }
                    /*    if ("1".equals(tripType) && "0".equals(emptyTrip)) {
                 String code2 = "";
                 String[] temp = null;
                 int insertStatus = 0;
                 map.put("userId", userId);
                 map.put("DetailCode", "1");
                 map.put("voucherType", "%SALES%");
                 code2 = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVoucherCode", map);
                 temp = code2.split("-");
                 int codeval2 = Integer.parseInt(temp[1]);
                 int codev2 = codeval2 + 1;
                 String voucherCode = "SALES-" + codev2;
                 System.out.println("voucherCode = " + voucherCode);
                 map.put("voucherCode", voucherCode);
                 map.put("mainEntryType", "VOUCHER");
                 map.put("entryType", "SALES");
    
                 //get ledger info
                 map.put("customer", customerId);
                 String ledgerInfo = (String) getSqlMapClientTemplate().queryForObject("trip.getCustomerLedgerInfo", map);
                 System.out.println("ledgerInfo:" + ledgerInfo);
                 temp = ledgerInfo.split("~");
                 String ledgerId = temp[0];
                 String particularsId = temp[1];
                 map.put("ledgerId", ledgerId);
                 map.put("particularsId", particularsId);
    
                 map.put("amount", estimatedRevenue);//hidde for crossing amount edit option by madhavan
                 //  map.put("amount", crossingAmount);
                 map.put("Accounts_Type", "DEBIT");
                 map.put("Remark", "Freight Charges");
                 map.put("Reference", "Trip");
                 //                System.out.println("tripRevenueNew " + map.get("particularsId") + " = " + tripRevenue);
                 //                getTripNo = (String) getSqlMapClientTemplate().queryForObject("operation.getTripNo", map);
                 System.out.println("tripId = " + tripTO.getTripSheetId());
                 map.put("SearchCode", tripTO.getTripSheetId());
                 System.out.println("map1 =---------------------> " + map);
                 insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                 System.out.println("status1 = " + insertStatus);
                 //--------------------------------- acc 2nd row start --------------------------
                 if (insertStatus > 0) {
                 map.put("DetailCode", "2");
                 map.put("ledgerId", "51");
                 map.put("particularsId", "LEDGER-39");
                 map.put("Accounts_Type", "CREDIT");
                 System.out.println("tripRevenueNew " + map.get("particularsId") + " = " + estimatedRevenue);
                 System.out.println("map2 =---------------------> " + map);
                 insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                 System.out.println("status2 = " + insertStatus);
                 }
    
                 }
                 */
                printDocketNo = status + "";

            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("updateStartTripSheet Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheet List", sqlException);
            }
            return printDocketNo;
        }
    }

    public String updateStartTripSheetold(TripTO tripTO, int userId, SqlMapClient session) {
        synchronized (this) {
            Map map = new HashMap();

            map.put("userId", userId);
            map.put("planStartTime", tripTO.getPlanStartHour() + ":" + tripTO.getPlanStartMinute() + ":00");
            map.put("planStartDate", tripTO.getPlanStartDate());
            map.put("startDate", tripTO.getStartDate());
            map.put("startTime", tripTO.getTripStartHour() + ":" + tripTO.getTripStartMinute() + ":00");
            map.put("startOdometerReading", tripTO.getStartOdometerReading());
            map.put("startHM", tripTO.getStartHM());
            map.put("tripSheetId", tripTO.getTripSheetId());
            map.put("tripId", tripTO.getTripSheetId());
            map.put("startTripRemarks", tripTO.getStartTripRemarks());
            map.put("statusId", tripTO.getStatusId());
            map.put("tripPlanEndDate", tripTO.getTripPlanEndDate());
            map.put("tripPlanEndTime", tripTO.getTripPlanEndTime());
            map.put("lrNumber", tripTO.getLrNumber());
            map.put("vehicleactreportdate", tripTO.getVehicleactreportdate());
            map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");
            map.put("vehicleloadreportdate", tripTO.getVehicleloadreportdate());
            map.put("vehicleloadreporttime", tripTO.getVehicleloadreporthour() + ":" + tripTO.getVehicleloadreportmin() + ":00");
            map.put("vehicleloadtemperature", tripTO.getVehicleloadtemperature());

            map.put("vehicleTypeId", tripTO.getVehicleTypeId());
            map.put("vehicleNo", tripTO.getVehicleNo());

            map.put("vendorId", tripTO.getVendorId());
            map.put("sealNoType", tripTO.getSealNoType());
            map.put("sealNo", tripTO.getSealNo());

            map.put("driverName", tripTO.getDriverName());
            map.put("driverMobile", tripTO.getDriverMobile());
            map.put("dlNumber", tripTO.getDlNumber());
            map.put("eWayBillNo", tripTO.geteWayBillNo());

            System.out.println("the updatetripsheetdetails" + map);
            int status = 0;
            String checkVehicleId = "";
            ArrayList tripDetails = new ArrayList();
            String printDocketNo = "";

            try {
                status = (Integer) session.update("trip.insertStartTripSheetDetails", map);
                status = (Integer) session.update("trip.updateStartTripSheet", map);
                String consignmentId = "";
                consignmentId = (String) session.queryForObject("trip.getConsignmentId", map);
                System.out.println("consignmentId : " + consignmentId);
                map.put("consignmentId", consignmentId);
                status = (Integer) session.update("trip.updateOrderInvoiceDetails", map);

                String regNos = tripTO.getVehicleNo();
                map.put("regNo", regNos + "%");
                checkVehicleId = (String) session.queryForObject("trip.checkExistingVehicle", map);

                if ("".equals(checkVehicleId) || checkVehicleId == null) {
                    int vehicleInsertId = (Integer) session.insert("trip.insertVehicleMaster", map);
                    System.out.println("vehicleInsertId== if" + vehicleInsertId);
                    map.put("vehicleId", vehicleInsertId);
                    int vehilceRegInsert = (Integer) session.insert("trip.insertVehicleRegNo", map);
                    System.out.println("vehilceRegInsert- if" + vehilceRegInsert);
                    int updateTripVehicle = (Integer) session.update("trip.updateTripVehicle", map);
                    System.out.println("updateTripVehicle- if" + updateTripVehicle);

                } else {
                    map.put("vehicleId", checkVehicleId);
                    int updateTripVehicle = (Integer) session.update("trip.updateTripVehicle", map);
                    System.out.println("updateTripVehicle else -" + updateTripVehicle);
                }

                status = (Integer) session.update("trip.updateLHCVehicleNo", map);

                status = (Integer) session.update("trip.updateTripDriverDetails", map);

                status = (Integer) session.update("trip.updateTripVehicleDetails", map);

                map.put("expenseName", "1047");
                map.put("detentionAmt", tripTO.getOriginDetention());
                map.put("customerId", tripTO.getCustomerId());
                if (!"".equals(tripTO.getOriginDetention()) && !"0".equals(tripTO.getOriginDetention())) {
                    status = (Integer) session.update("trip.updateTripDetention", map);
                    System.out.println("status-deded----" + status);
                }
                // Lr Number Generation starts here...

                String accYear = ThrottleConstants.accountYear;

                /////////////////////////////////// Temp LR part 24-10-2018 /////////////////////////////////
                /////////////////////////////////// Temp LR part 24-10-2018 /////////////////////////////////
                String lrNumberCount = (String) session.queryForObject("trip.getLrCount", map);
                String[] lrNum = lrNumberCount.split("-");
                System.out.println("lrLength.length :: " + lrNum.length);

                int j = 1;
                for (int i = 0; i < lrNum.length - 1; i++) {
                    map.put("accYear", accYear);
                    String lrNumberSequence = "";
                    lrNumberSequence = (String) session.queryForObject("trip.getLrNo", map);

                    if (lrNumberSequence == null) {
                        lrNumberSequence = "1";
                    }

                    String lrCode = "";
                    String lrCity = (String) session.queryForObject("trip.getLRCity", map);
                    String[] lrCty = lrCity.split("-");

                    if (i == 0) {
                        lrCode = "3PL-" + lrCty[0] + " " + lrNumberSequence;
                        printDocketNo = lrCode;
                    } else {
                        lrCode = "3PL-" + lrCty[0] + " " + lrNumberSequence;
                    }
                    map.put("docketNo", lrCode);
                    map.put("lrCode", lrCode);
                    map.put("invoiceWeight", tripTO.getPackageWeight());

                    System.out.println("invoiceWeight : " + tripTO.getPackageWeight());

                    int lrId = (Integer) session.insert("trip.insertLR", map);
                    System.out.println("lrId : " + lrId);

                    if (i == 0) {
                        map.put("invoiceNumber", tripTO.getInvoiceNumber());
                        map.put("invoiceValue", tripTO.getInvoiceValue());
                        map.put("invoiceDate", tripTO.getInvoiceDate());

                        ArrayList GetOrderIdFirstPoint = new ArrayList();
                        GetOrderIdFirstPoint = (ArrayList) session.queryForList("trip.GetOrderIdFirstPoint", map);
                        Iterator itr1 = GetOrderIdFirstPoint.iterator();
                        TripTO tpTO = new TripTO();
                        int cnt = 1;
                        while (itr1.hasNext()) {
                            tpTO = new TripTO();
                            tpTO = (TripTO) itr1.next();
                            if (cnt == 1) {
                                String orderSequenceOne = tpTO.getOrderId();
                                map.put("orderSequenceOne", orderSequenceOne);
                            } else {
                                String orderSequenceTwo = tpTO.getOrderId();
                                map.put("orderSequenceTwo", orderSequenceTwo);
                            }
                            cnt++;
                        }
                    } else {
                        map.put("invoiceNumber", "");
                        map.put("invoiceValue", "");
                        map.put("invoiceDate", "");
                        ArrayList GetOrderIdSecondPoint = new ArrayList();
                        GetOrderIdSecondPoint = (ArrayList) session.queryForList("trip.GetOrderIdSecondPoint", map);
                        Iterator itr1 = GetOrderIdSecondPoint.iterator();
                        TripTO tpTO = new TripTO();
                        int cnt = 1;
                        while (itr1.hasNext()) {
                            tpTO = new TripTO();
                            tpTO = (TripTO) itr1.next();
                            if (cnt == 1) {
                                String orderSequenceOne = tpTO.getOrderId();
                                System.out.println("tpTO.getOrderId()-" + tpTO.getOrderId());
                                map.put("orderSequenceOne", orderSequenceOne);
                            } else {
                                String orderSequenceTwo = tpTO.getOrderId();
                                System.out.println("tpTO.getOrderId()-" + tpTO.getOrderId());
                                map.put("orderSequenceTwo", orderSequenceTwo);
                            }
                            cnt++;
                        }

                    }

                    map.put("pointOrder", j);
                    status = (Integer) session.update("trip.insertEndTouchPointDetails", map);
                    System.out.println("insertEndTouchPointDetails--" + status);
                    j++;

                }
                String[] loadOrderId = tripTO.getLoadOrderId();
                System.out.println("loadOrderId------" + loadOrderId);
                String[] pendingPkgs = tripTO.getPendingPkgs();
                String[] pendingWeight = tripTO.getPendingWeight();

//            int j = 1;
//            System.out.println("loadOrderId.length=============" + loadOrderId.length);
//            for (int i = 0; i < loadOrderId.length; i++) {
//                map.put("consignmentOrderId", loadOrderId[i]);
//                map.put("pendingPkgs", pendingPkgs[i]);
//                map.put("pendingWeight", pendingWeight[i]);
//                map.put("orderSequence", j);
//                j++;
//                if ("0.00".equals(pendingPkgs[i]) && "0.00".equals(pendingWeight[i])) {
//                    System.out.println("Gooooooo=" + pendingWeight[i]);
//                    System.out.println("Gooooooommmm=" + pendingPkgs[i]);
//                    map.put("statusId", tripTO.getStatusId());
//                } else {
//                    System.out.println("elseeeeeeeeeeeeeeeee");
//                    map.put("statusId", "5");
//                }
//                System.out.println("map value is:" + map);
//
//                status = (Integer) session.update("trip.updateConsignmentStatus", map);
//                System.out.println("status=====" + status);
//            }
                int insertConsignmentArticle = 0;
                map.put("consignmentId", tripTO.getConsignmentId());
                System.out.println("the inner" + map);
                String[] productCodes = null;
                String[] productNames = null;
                String[] packageNos = null;
                String[] weights = null;
                String[] productbatch = null;
                String[] productuom = null;
                String[] loadedpackages = null;
                productCodes = tripTO.getProductCodes();
                productNames = tripTO.getProductNames();
                packageNos = tripTO.getPackagesNos();
                weights = tripTO.getWeights();
                productbatch = tripTO.getProductbatch();
                productuom = tripTO.getProductuom();
                loadedpackages = tripTO.getLoadedpackages();
                for (int i = 0; i < productCodes.length; i++) {
                    map.put("productCode", productCodes[i]);
                    map.put("producName", productNames[i]);
                    map.put("packageNos", packageNos[i]);
                    map.put("weights", weights[i]);
                    map.put("productbatch", productbatch[i]);
                    map.put("productuom", productuom[i]);
                    map.put("loadedpackages", loadedpackages[i]);
                    System.out.println("the full" + map);
                    if (productCodes[i] != null && !"".equals(productCodes[i]) && !"0".equals(productuom[i])) {
                        insertConsignmentArticle = (Integer) session.update("trip.insertConsignmentArticle", map);
                    }

                }
//                String tripRevenueDetails = "";
//                String tripTemp[] = null;
//                String customerId = "";
//                String tripType = "";
//                String estimatedRevenue = "";
//                String emptyTrip = "";
//                tripRevenueDetails = (String) session.queryForObject("trip.getTripRevenueDetails", map);
//                System.out.println("tripRevenueDetails = " + tripRevenueDetails);
//                if (!"".equals(tripRevenueDetails)) {
//                    tripTemp = tripRevenueDetails.split("~");
//                    customerId = tripTemp[0];
//                    tripType = tripTemp[1];
//                    estimatedRevenue = tripTemp[2];
//                    emptyTrip = tripTemp[3];
//                }
                /*    if ("1".equals(tripType) && "0".equals(emptyTrip)) {
                 String code2 = "";
                 String[] temp = null;
                 int insertStatus = 0;
                 map.put("userId", userId);
                 map.put("DetailCode", "1");
                 map.put("voucherType", "%SALES%");
                 code2 = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVoucherCode", map);
                 temp = code2.split("-");
                 int codeval2 = Integer.parseInt(temp[1]);
                 int codev2 = codeval2 + 1;
                 String voucherCode = "SALES-" + codev2;
                 System.out.println("voucherCode = " + voucherCode);
                 map.put("voucherCode", voucherCode);
                 map.put("mainEntryType", "VOUCHER");
                 map.put("entryType", "SALES");

                 //get ledger info
                 map.put("customer", customerId);
                 String ledgerInfo = (String) getSqlMapClientTemplate().queryForObject("trip.getCustomerLedgerInfo", map);
                 System.out.println("ledgerInfo:" + ledgerInfo);
                 temp = ledgerInfo.split("~");
                 String ledgerId = temp[0];
                 String particularsId = temp[1];
                 map.put("ledgerId", ledgerId);
                 map.put("particularsId", particularsId);

                 map.put("amount", estimatedRevenue);//hidde for crossing amount edit option by madhavan
                 //  map.put("amount", crossingAmount);
                 map.put("Accounts_Type", "DEBIT");
                 map.put("Remark", "Freight Charges");
                 map.put("Reference", "Trip");
                 //                System.out.println("tripRevenueNew " + map.get("particularsId") + " = " + tripRevenue);
                 //                getTripNo = (String) getSqlMapClientTemplate().queryForObject("operation.getTripNo", map);
                 System.out.println("tripId = " + tripTO.getTripSheetId());
                 map.put("SearchCode", tripTO.getTripSheetId());
                 System.out.println("map1 =---------------------> " + map);
                 insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                 System.out.println("status1 = " + insertStatus);
                 //--------------------------------- acc 2nd row start --------------------------
                 if (insertStatus > 0) {
                 map.put("DetailCode", "2");
                 map.put("ledgerId", "51");
                 map.put("particularsId", "LEDGER-39");
                 map.put("Accounts_Type", "CREDIT");
                 System.out.println("tripRevenueNew " + map.get("particularsId") + " = " + estimatedRevenue);
                 System.out.println("map2 =---------------------> " + map);
                 insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                 System.out.println("status2 = " + insertStatus);
                 }

                 }
                 */

                System.out.println("updateStartTripSheet size=" + tripDetails.size());
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("updateStartTripSheet Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheet List", sqlException);
            }
            return printDocketNo;
        }
    }

    public ArrayList getTripPODDetails(String tripSheetId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripSheetId);

        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripPODDetails", map);
            System.out.println("getTripPODDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripPODDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripPODDetails List", sqlException);
        }

        return tripDetails;
    }

    public int saveTripPodDetails(String actualFilePath, String cityId1, String tripSheetId1, String podRemarks1, String lrNumber1, String fileSaved, TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        FileInputStream fis = null;
        int tripId = 0;
        int cityId = 0;
        try {
            if (tripSheetId1 != null && tripSheetId1 != "") {
                tripId = Integer.parseInt(tripSheetId1);
                map.put("tripSheetId", tripId);
            }

            if (cityId1 != null && cityId1 != "") {
                cityId = Integer.parseInt(cityId1);
                map.put("cityId", cityId);
            }
            map.put("podRemarks", podRemarks1);
            map.put("fileName", fileSaved);
            map.put("lrNumber", lrNumber1);
            map.put("userId", userId);
            System.out.println("actualFilePath = " + actualFilePath);
            File file = new File(actualFilePath);
            System.out.println("file = " + file);
            fis = new FileInputStream(file);
            System.out.println("fis = " + fis);
            byte[] podFile = new byte[(int) file.length()];
            System.out.println("podFile = " + podFile);
            fis.read(podFile);
            fis.close();
            map.put("podFile", podFile);
            System.out.println("the saveTripPodDetails123455" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripPodDetails", map);
            map.put("consignorName", tripTO.getConsignorName());
            map.put("consigneeName", tripTO.getConsigneeName());
            map.put("consignorMobileNo", tripTO.getConsignorMobileNo());
            map.put("consigneeMobileNo", tripTO.getConsigneeMobileNo());
            map.put("consignorAddress", tripTO.getConsignorAddress());
            map.put("consigneeAddress", tripTO.getConsigneeAddress());
            map.put("consignmentNote", tripTO.getConsignmentNote());
            System.out.println("the consignorDetails" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignorDetails", map);
            System.out.println("saveTripPodDetails size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripPodDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripPodDetails List", sqlException);
        }

        return status;
    }

    public ArrayList getVehicleList() throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = null;
        Map map = new HashMap();

        try {
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return vehicleList;
    }

    public ArrayList getDriverSettlementDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList settlementDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            settlementDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverSettlementDetails", map);
            System.out.println("getDriverSettlementDetails.size() = " + settlementDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverSettlementDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverSettlementDetails List", sqlException);
        }
        return settlementDetails;
    }

    public int createGPSLogForTripStart(String tripId) {
        Map map = new HashMap();
        int status = 0;
        try {

            map.put("tripId", tripId);
            System.out.println("the createGPSLogForTripStart" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.createGPSLogForTripStart", map);
            System.out.println("createGPSLogForTripStart size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("createGPSLogForTripStart Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "createGPSLogForTripStart List", sqlException);
        }

        return status;
    }

    public int createGPSLogForTripEnd(String tripId) {
        Map map = new HashMap();
        int status = 0;
        try {

            map.put("tripId", tripId);
            System.out.println("the createGPSLogForTripEnd" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.createGPSLogForTripEnd", map);
            System.out.println("createGPSLogForTripEnd =" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("createGPSLogForTripEnd Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "createGPSLogForTripEnd List", sqlException);
        }

        return status;
    }

    public ArrayList getStartTripDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        ArrayList tripDetails = new ArrayList();
        int tripVehicleCount = 0;
        try {
//            tripVehicleCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleCount", map);
//            if(tripVehicleCount == 1){
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getStartTripDetails", map);
//            }else{
//            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getStartTripDetailsForVehicleChange", map);
//            }
            System.out.println("getStartTripDetails size=" + tripDetails.size());
            System.out.println("getStartTripDetails =" + map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStartTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getStartTripDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getPreStartTripDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        System.out.println("getPreStartTripDetails" + tripTO.getTripId());
        ArrayList tripPreStartDetails = new ArrayList();
        try {
            tripPreStartDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getPreStartTripDetails", map);
            System.out.println("getPreStartTripDetails size=" + tripPreStartDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPreStartTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPreStartTripDetails List", sqlException);
        }

        return tripPreStartDetails;
    }

    public ArrayList getStartedTripDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        map.put("vehicleId", tripTO.getVehicleId());
        String vehicleId = "";
        ArrayList tripStartDetails = new ArrayList();
        int tripVehicleCount = 0;
        System.out.println("map in trip start details = " + map);
        try {
            tripStartDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getStartedTripDetails", map);
            System.out.println("tripVehicleCount in DAO if size = " + tripStartDetails.size());
            System.out.println("getStartedTripDetails size=" + tripStartDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStartedTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getStartedTripDetails List", sqlException);
        }

        return tripStartDetails;
    }

    public ArrayList getVehicleChangeTripDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        String vehicleId = "";
        ArrayList tripStartDetails = new ArrayList();
        int tripVehicleCount = 0;
        System.out.println("map in trip start details = " + map);
        try {
            tripVehicleCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleCount", map);
            System.out.println("tripVehicleCount in DAO = " + tripVehicleCount);
            if (tripVehicleCount == 1) {
//                System.out.println("tripVehicleCount in DAO if = " + tripVehicleCount);
//                tripStartDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getStartedTripDetails", map);
//                System.out.println("tripVehicleCount in DAO if size = " + tripStartDetails.size());
            } else {
                System.out.println("tripVehicleCount in DAO else = " + tripVehicleCount);
//                vehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleId", map);
//                map.put("vehicleId", vehicleId);
                tripStartDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getStartedTripDetailsForVehicleChange", map);
                System.out.println("tripVehicleCount in DAO else size = " + tripStartDetails.size());
            }
            System.out.println("getStartedTripDetails size=" + tripStartDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStartedTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getStartedTripDetails List", sqlException);
        }

        return tripStartDetails;
    }

    public ArrayList getEndTripDetails(TripTO tripTO) {
        Map map = new HashMap();
//        map.put("vehicleId", tripTO.getVehicleId());
        ArrayList tripEndDetails = new ArrayList();
        ArrayList tripDetails = new ArrayList();
        map.put("tripSheetId", tripTO.getTripId());
        map.put("tripId", tripTO.getTripId());
        int tripVehicleCount = 0;
        String vehicleId = "";
        TripTO trpTO1 = new TripTO();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripDetails", map);
            System.out.println("tripDetails:" + tripDetails.size());
            Iterator itr2 = tripDetails.iterator();
            while (itr2.hasNext()) {
                trpTO1 = (TripTO) itr2.next();
                vehicleId = trpTO1.getVehicleId();
            }
            map.put("vehicleId", vehicleId);
            System.out.println("map : " + map);
            tripVehicleCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleCount", map);
            if (tripVehicleCount == 1) {
                tripEndDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEndTripDetails", map);
            } else {
                tripEndDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEndTripDetailsForVehicleChange", map);
            }
            System.out.println("getEndTripDetails size=" + tripEndDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEndTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEndTripDetails List", sqlException);
        }

        return tripEndDetails;
    }

    public ArrayList getGPSDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        ArrayList gpsDetails = new ArrayList();
        try {
            gpsDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getGPSDetails", map);
            System.out.println("getGPSDetails size=" + gpsDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getGPSDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getGPSDetails List", sqlException);
        }

        return gpsDetails;
    }

    public ArrayList getTotalExpenseDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        ArrayList expenseDetails = new ArrayList();
        String vehicleId = "";
        String modelId = "";
        String vehicleTypeId = "";
        vehicleId = tripTO.getVehicleId();
        try {

            if ("".equals(vehicleId)) {
//            if (vehicleId.equals("")) {
                vehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleId", map);
            }
            if (!"0".equalsIgnoreCase(vehicleId)) {
                map.put("vehicleId", vehicleId);
                modelId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleModelId", map);
                map.put("modelId", modelId);
                vehicleTypeId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleTypeId", map);
                if ("".equals(vehicleTypeId)) {
//                if (vehicleTypeId.equals("0")) {
                    vehicleTypeId = "1";
                }
                map.put("vehicleTypeId", vehicleTypeId);
            } else {
                vehicleTypeId = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleTypeId", map);
                map.put("vehicleTypeId", vehicleTypeId);
            }
            System.out.println("555555555555555555555555555555555 " + map);
            expenseDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTotalExpenseDetails", map);
            System.out.println("getTotalExpenseDetails size=" + expenseDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTotalExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTotalExpenseDetails List", sqlException);
        }

        return expenseDetails;
    }

    public String getTollAmount(String vehicleTypeId) {
        Map map = new HashMap();
        String tollAmount = "";
        map.put("vehicleTypeId", vehicleTypeId);
        System.out.println("map = " + map);
        try {
            if (vehicleTypeId.equals("1021") || vehicleTypeId.equals("1022")) {
                tollAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getTollAmount", map);
            } else if (vehicleTypeId.equals("1023") || vehicleTypeId.equals("1024")) {
                tollAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getTollAmount3118", map);
            } else {
                tollAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getTollAmount", map);
            }
            System.out.println("getTollAmount size=" + tollAmount);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tollAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "tollAmount List", sqlException);
        }

        return tollAmount;
    }

    public String getSecondaryRouteExpense(TripTO tripTO) {
        Map map = new HashMap();
        String result = "";
        try {
            map.put("tripSheetId", tripTO.getTripId());
            System.out.println("map = " + map);
            result = (String) getSqlMapClientTemplate().queryForObject("trip.getSecondaryRouteExpense", map);
            System.out.println("getSecondaryRouteExpense size=" + result);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryRouteExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryRouteExpense List", sqlException);
        }

        return result;
    }

    public String getDriverIncentiveAmount(String vehicleTypeId) {
        Map map = new HashMap();
        String tollAmount = "";
        map.put("vehicleTypeId", vehicleTypeId);
        try {
            if (vehicleTypeId.equals("1021") || vehicleTypeId.equals("1022")) {
                tollAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getDriverIncentive", map);
            } else if (vehicleTypeId.equals("1023") || vehicleTypeId.equals("1024")) {
                tollAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getDriverIncentive", map);
            } else {
                tollAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getDriverIncentive", map);
            }
            System.out.println("getDriverIncentive size=" + tollAmount);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverIncentive Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverIncentive List", sqlException);
        }

        return tollAmount;
    }

    public String getDriverBataAmount(String vehicleTypeId) {
        Map map = new HashMap();
        String tollAmount = "";
        try {
            tollAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getDriverBatta", map);
            System.out.println("getDriverBataAmount size=" + tollAmount);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverBataAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverBataAmount List", sqlException);
        }

        return tollAmount;
    }

    public String getFuelPrice(String vehicleTypeId) {
        Map map = new HashMap();
        String fuelPrice = "";
        try {
            map.put("vehicleTypeId", vehicleTypeId);
            System.out.println("map:" + map);
            fuelPrice = (String) getSqlMapClientTemplate().queryForObject("trip.getFuelPrice", map);
            System.out.println("getFuelPrice size=" + fuelPrice);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelPrice List", sqlException);
        }

        return fuelPrice;
    }

    public String getFuelType(String vehicleTypeId) {
        Map map = new HashMap();
        String fuelType = "";
        try {
            map.put("vehicleTypeId", vehicleTypeId);
            System.out.println("map:" + map);
            fuelType = (String) getSqlMapClientTemplate().queryForObject("trip.getFuelType", map);
            System.out.println("getFuelType size=" + fuelType);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelType Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelType List", sqlException);
        }

        return fuelType;
    }

    public String getSecondaryFuelPrice(String vehicleTypeId, TripTO tripTo) {
        Map map = new HashMap();
        String fuelPrice = "";
        try {
            map.put("vehicleTypeId", vehicleTypeId);
            map.put("tripId", tripTo.getTripId());
            System.out.println("map:" + map);
            fuelPrice = (String) getSqlMapClientTemplate().queryForObject("trip.getSecondaryFuelPrice", map);
            System.out.println("getFuelPrice size=" + fuelPrice);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelPrice List", sqlException);
        }

        return fuelPrice;
    }

    public String getSecondaryFuelType(String vehicleTypeId, TripTO tripTo) {
        Map map = new HashMap();
        String secondaryFuelType = "";
        try {
            map.put("vehicleTypeId", vehicleTypeId);
            map.put("tripId", tripTo.getTripId());
            System.out.println("map:" + map);
            secondaryFuelType = (String) getSqlMapClientTemplate().queryForObject("trip.getSecondaryFuelType", map);
            System.out.println("getSecondaryFuelType size=" + secondaryFuelType);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryFuelType Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryFuelType List", sqlException);
        }

        return secondaryFuelType;
    }

    public String getTripEmails(String tripId, String statusId) {
        Map map = new HashMap();
        map.put("tripId", tripId);
        map.put("statusId", statusId);
        String result = "";
        try {
            System.out.println("map:" + map);
            result = (String) getSqlMapClientTemplate().queryForObject("trip.getTripEmails", map);
            System.out.println("getTripEmails value=" + result);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripEmails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripEmails List", sqlException);
        }

        return result;
    }

    public int saveTripClosure(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleTypeId", tripTO.getVehicleTypeId());
        map.put("estimatedExpense", tripTO.getEstimatedExpense());
        map.put("totalDays", tripTO.getTotalDays());
        map.put("runKM", tripTO.getTotalRumKM());
        map.put("reeferHours", tripTO.getTotalRefeerHours());
        map.put("reeferMinutes", tripTO.getTotalRefeerMinutes());
        map.put("mileage", tripTO.getMilleage());
        map.put("reeferConsumption", tripTO.getReeferConsumption());
        map.put("fuelConsumed", tripTO.getFuelConsumed());
        map.put("fuelAmount", tripTO.getFuelAmount());
        map.put("fuelCost", tripTO.getFuelCost());
        map.put("tollAmount", tripTO.getTollAmount());
        map.put("tollCost", tripTO.getTollCost());
        map.put("incentiveAmount", tripTO.getIncentiveAmount());
        map.put("driverIncentive", tripTO.getDriverIncentive());
        map.put("battaAmount", tripTO.getBattaAmount());
        map.put("driverBatta", tripTO.getDriverBatta());
        map.put("routeExpense", tripTO.getRouteExpense());
        map.put("totalExpense", tripTO.getTotalExpenses());
        map.put("userId", userId);
        System.out.println("the saveTripClosure" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripClosure", map);
            System.out.println("saveTripClosure size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripClosure List", sqlException);
        }

        return status;
    }

    public int saveTripClosureDetails(TripTO tripTO, int userId) {
        Map map = new HashMap();
        Map map1 = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        map.put("lrNumber", tripTO.getLrNumber());
        map.put("tripSheetId", tripTO.getTripId());
        map.put("lrNumber", tripTO.getLrNumber());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("estimatedExpense", tripTO.getEstimatedExpense());
        map.put("totalDays", tripTO.getTotalDays());
        map.put("runKM", tripTO.getRunKm());
        map.put("reeferHours", tripTO.getRunHm());
        map.put("gpsKm", tripTO.getGpsKm());
        map.put("gpsHm", tripTO.getGpsHm());
        map.put("reeferMinutes", "0");
        map.put("mileage", tripTO.getMilleage());
        map.put("reeferConsumption", tripTO.getReeferMileage());
        map.put("fuelConsumed", tripTO.getDieselUsed());
        map.put("fuelAmount", tripTO.getFuelPrice());
        map.put("fuelCost", tripTO.getDieselCost());
        map.put("tollAmount", tripTO.getTollRate());
        map.put("tollCost", tripTO.getTollCost());
        map.put("incentiveAmount", tripTO.getDriverIncentivePerKm());
        map.put("driverIncentive", tripTO.getDriverIncentive());
        map.put("miscValue", tripTO.getMiscValue());
        map.put("battaAmount", tripTO.getDriverBattaPerDay());
        map.put("driverBatta", tripTO.getDriverBatta());
        map.put("systemExpenses", 0);
        map.put("routeExpense", 0);
        map.put("totalExpense", tripTO.getEstimatedExpense());
        map.put("userId", userId);
        map.put("detentionHours", tripTO.getDetentionHours());
        map.put("detentionCharge", tripTO.getDetentionCharge());

        if (tripTO.getSecondaryParkingAmount().equals("")) {
            map.put("secParkingAmount", "0");
        } else {
            map.put("secParkingAmount", tripTO.getSecondaryParkingAmount());
        }
        if (tripTO.getPreColingAmount().equals("")) {
            map.put("preColingAmount", "0");
        } else {
            map.put("preColingAmount", tripTO.getPreColingAmount());
        }
        if (tripTO.getSecAdditionalTollCost().equals("")) {
            map.put("secAddlTollCost", "0");
        } else {
            map.put("secAddlTollCost", tripTO.getSecAdditionalTollCost());
        }
        System.out.println("the saveTripClosure" + map);
        int status = 0;
        int status1 = 0;
        int driverChangeStatus = 0;
        ArrayList tripDetails = new ArrayList();
        ArrayList vehicleDriverlist = new ArrayList();
        ArrayList inActiveVehicleDriverlist = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().insert("trip.saveTripClosure", map);
            System.out.println("status--" + status);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDetentionCharge", map);
            System.out.println("status-rerer-" + status);
            status = (Integer) getSqlMapClientTemplate().update("trip.UpdateTripExpense", map);
            System.out.println("status tripClosureId =" + status);
            if (status > 0) {
                map.put("tripClosureId", status);
                driverChangeStatus = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkDriverChangeStatus", map);
                System.out.println("driverChangeStatus = " + driverChangeStatus);
                if (driverChangeStatus > 0) {
                    // Driver Cnage Case
                    inActiveVehicleDriverlist = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInactiveVehicleDriver", map);
                    Iterator itr1 = inActiveVehicleDriverlist.iterator();
                    TripTO tpTO = new TripTO();
                    Float runKm = 0.0f;
                    Float totalRunKm = 0.0f;
                    Float runHm = 0.0f;
                    Float totalRunHm = 0.0f;
                    Float milleage = 0.0f;
                    Float reeferMileage = 0.0f;
                    Float dieselUsed = 0.0f;
                    Float driverBatta = 0.0f;
                    Float miscRate = 0.0f;
                    Float extraExpenseValue = 0.0f;
                    Float fuelPrice = 0.0f;
                    Float tollRate = 0.0f;
                    Float driverIncentivePerKm = 0.0f;
                    Float nettExpense = 0.0f;
                    Float bookedExpense = 0.0f;
                    Float tempEstimatedExpense = 0.0f;
                    Float estimatedExpense = 0.0f;
                    Float driverBattaPerDay = 100.0f;
                    String startDate = "";
                    String endDate = "";
                    String expenseValue = "";
                    int totalDays = 0;
                    int totalDaysSum = 0;
                    int driverCount = 0;
                    int driverCountSum = 0;
                    while (itr1.hasNext()) {
                        tpTO = new TripTO();
                        tpTO = (TripTO) itr1.next();
                        String[] tempDriverId = null;
                        driverCount = Integer.parseInt(tpTO.getDriverCount());
                        driverCountSum += Integer.parseInt(tpTO.getDriverCount());
                        totalDays = Integer.parseInt(tpTO.getTotalDays());
                        totalDaysSum += Integer.parseInt(tpTO.getTotalDays());
                        tempDriverId = tpTO.getDriverInTrip().split(",");
                        runKm = Float.parseFloat((String) tpTO.getTotalKm());
                        totalRunKm += Float.parseFloat((String) tpTO.getTotalKm());
                        runHm = Float.parseFloat((String) tpTO.getTotalHm());
                        totalRunHm += Float.parseFloat((String) tpTO.getTotalHm());
                        milleage = Float.parseFloat((String) tripTO.getMilleage());
                        reeferMileage = Float.parseFloat((String) tripTO.getReeferMileage());
                        dieselUsed = (runKm / milleage) + (runHm * reeferMileage);
                        driverBatta = totalDays * driverBattaPerDay * driverCount;
                        miscRate = Float.parseFloat((String) tripTO.getMiscRate());
                        startDate = tpTO.getStartDate();
                        endDate = tpTO.getEndDate();
                        map.put("tripStartDate", startDate);
                        map.put("tripEndDate", endDate);
                        expenseValue = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleExtraExpense", map);
                        extraExpenseValue = Float.parseFloat((String) expenseValue);
                        fuelPrice = Float.parseFloat((String) tripTO.getFuelPrice());
                        tollRate = Float.parseFloat((String) tripTO.getTollRate());
                        driverIncentivePerKm = Float.parseFloat((String) tripTO.getDriverIncentivePerKm());
                        bookedExpense = Float.parseFloat((String) tripTO.getBookedExpense());
                        Float miscValue = ((runKm * miscRate) + extraExpenseValue + driverBatta) * 5 / 100;
                        Float dieselCost = dieselUsed * fuelPrice;
                        Float tollCost = runKm * tollRate;
                        Float driverIncentive = runKm * driverIncentivePerKm;
                        Float systemExpense = dieselCost + tollCost + driverIncentive + driverBatta + miscValue;
                        nettExpense = systemExpense + bookedExpense;
                        tempEstimatedExpense = Float.parseFloat(tripTO.getEstimatedExpense()) / runKm;
                        estimatedExpense = tempEstimatedExpense * runKm;

                        if (tempDriverId.length > 0) {
                            for (int i = 0; i < tempDriverId.length; i++) {
                                map.put("driverId", tempDriverId[i]);
                                map.put("estimatedExpense", estimatedExpense / driverCount);
                                map.put("totalDays", totalDays);
                                map.put("runKM", runKm / driverCount);
                                map.put("reeferHours", runHm / driverCount);
                                map.put("gpsKm", 0);
                                map.put("gpsHm", 0);
                                map.put("reeferMinutes", "0");
                                map.put("mileage", milleage);
                                map.put("reeferConsumption", reeferMileage);
                                map.put("fuelConsumed", dieselUsed / driverCount);
                                map.put("fuelAmount", tripTO.getFuelPrice());
                                map.put("fuelCost", dieselCost / driverCount);
                                map.put("tollAmount", tripTO.getTollRate());
                                map.put("tollCost", tollCost / driverCount);
                                map.put("incentiveAmount", tripTO.getDriverIncentivePerKm());
                                map.put("driverIncentive", driverIncentive / driverCount);
                                map.put("miscValue", miscValue);
                                map.put("battaAmount", tripTO.getDriverBattaPerDay());
                                map.put("driverBatta", driverBatta / driverCount);
                                map.put("systemExpenses", systemExpense / driverCount);
                                map.put("routeExpense", bookedExpense / driverCount);
                                map.put("totalExpense", nettExpense / driverCount);
                                System.out.println("final Map = " + map);
                                status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriverClosure", map);
                            }
                        }
                    }

                    int vehicleDriverCount = 0;
                    String driverId = "";
                    vehicleDriverCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getVehicleDriverCount", map);
                    System.out.println("vehicleDriverCount = " + vehicleDriverCount);
                    if (vehicleDriverCount > 0) {
                        vehicleDriverlist = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripVehicleDriver", map);
                        System.out.println("vehicleDriverlist.size() = " + vehicleDriverlist.size());
                        Iterator itr2 = vehicleDriverlist.iterator();
                        TripTO trpTO = new TripTO();
                        vehicleDriverCount = vehicleDriverCount - driverCountSum;
                        totalDays = totalDays - totalDaysSum;
                        while (itr2.hasNext()) {
                            trpTO = new TripTO();
                            trpTO = (TripTO) itr2.next();
                            driverId = trpTO.getDriverId();
                            System.out.println("driverId = " + driverId);
                            map.put("driverId", driverId);
                            runKm = (Float.parseFloat((String) tripTO.getRunKm()) - totalRunKm) / vehicleDriverCount;
                            runHm = (Float.parseFloat((String) tripTO.getRunHm()) - totalRunHm) / vehicleDriverCount;
                            milleage = Float.parseFloat((String) tripTO.getMilleage());
                            reeferMileage = Float.parseFloat((String) tripTO.getReeferMileage());
                            dieselUsed = (runKm / milleage) + (runHm * reeferMileage);
                            driverBatta = totalDays * driverBattaPerDay * vehicleDriverCount;
                            miscRate = Float.parseFloat((String) tripTO.getMiscRate());
                            startDate = tpTO.getStartDate();
                            endDate = tpTO.getEndDate();
                            map.put("tripStartDate", startDate);
                            map.put("tripEndDate", endDate);
                            expenseValue = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleExtraExpenseEnd", map);
                            extraExpenseValue = Float.parseFloat((String) expenseValue);
                            fuelPrice = Float.parseFloat((String) tripTO.getFuelPrice());
                            tollRate = Float.parseFloat((String) tripTO.getTollRate());
                            driverIncentivePerKm = Float.parseFloat((String) tripTO.getDriverIncentivePerKm());
                            bookedExpense = Float.parseFloat((String) tripTO.getBookedExpense());
                            Float miscValue = ((runKm * miscRate) + extraExpenseValue + driverBatta) * 5 / 100;
                            Float dieselCost = dieselUsed * fuelPrice;
                            Float tollCost = runKm * tollRate;
                            Float driverIncentive = runKm * driverIncentivePerKm;
                            Float systemExpense = dieselCost + tollCost + driverIncentive + driverBatta + miscValue;
                            nettExpense = systemExpense + bookedExpense;
                            tempEstimatedExpense = Float.parseFloat(tripTO.getEstimatedExpense()) / runKm;
                            estimatedExpense = tempEstimatedExpense * runKm;
                            map.put("estimatedExpense", Float.parseFloat(tripTO.getEstimatedExpense()) / vehicleDriverCount);
                            map.put("totalDays", tripTO.getTotalDays());
                            map.put("runKM", Float.parseFloat(tripTO.getRunKm()) / vehicleDriverCount);
                            map.put("reeferHours", Float.parseFloat(tripTO.getRunHm()) / vehicleDriverCount);
                            map.put("gpsKm", Float.parseFloat(tripTO.getGpsKm()) / vehicleDriverCount);
                            map.put("gpsHm", Float.parseFloat(tripTO.getGpsHm()) / vehicleDriverCount);
                            map.put("reeferMinutes", "0");
                            map.put("mileage", tripTO.getMilleage());
                            map.put("reeferConsumption", tripTO.getReeferMileage());
                            map.put("fuelConsumed", Float.parseFloat(tripTO.getDieselUsed()) / vehicleDriverCount);
                            map.put("fuelAmount", tripTO.getFuelPrice());
                            map.put("fuelCost", Float.parseFloat(tripTO.getDieselCost()) / vehicleDriverCount);
                            map.put("tollAmount", tripTO.getTollRate());
                            map.put("tollCost", Float.parseFloat(tripTO.getTollCost()) / vehicleDriverCount);
                            map.put("incentiveAmount", tripTO.getDriverIncentivePerKm());
                            map.put("driverIncentive", Float.parseFloat(tripTO.getDriverIncentive()) / vehicleDriverCount);
                            map.put("miscValue", tripTO.getMiscValue());
                            map.put("battaAmount", tripTO.getDriverBattaPerDay());
                            map.put("driverBatta", Float.parseFloat(tripTO.getDriverBatta()) / vehicleDriverCount);
                            map.put("systemExpenses", Float.parseFloat(tripTO.getSystemExpense()) / vehicleDriverCount);
                            map.put("routeExpense", Float.parseFloat(tripTO.getBookedExpense()) / vehicleDriverCount);
                            map.put("totalExpense", Float.parseFloat(tripTO.getTotalExpenses()) / vehicleDriverCount);
                            System.out.println("final Map = " + map);
                            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriverClosure", map);
                        }
                    }

                } else {
                    // Driver not Change Case
                    int vehicleDriverCount = 0;
                    String driverId = "";
                    vehicleDriverCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getVehicleDriverCount", map);
                    System.out.println("vehicleDriverCount = " + vehicleDriverCount);
                    if (vehicleDriverCount > 0) {
                        vehicleDriverlist = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripVehicleDriver", map);
                        System.out.println("vehicleDriverlist.size() = " + vehicleDriverlist.size());
                        Iterator itr1 = vehicleDriverlist.iterator();
                        TripTO tpTO = new TripTO();
                        while (itr1.hasNext()) {
                            tpTO = new TripTO();
                            tpTO = (TripTO) itr1.next();
                            driverId = tpTO.getDriverId();
                            System.out.println("driverId = " + driverId);
                            map.put("driverId", driverId);
                            map.put("estimatedExpense", Float.parseFloat(tripTO.getEstimatedExpense()) / vehicleDriverCount);
                            map.put("totalDays", tripTO.getTotalDays());
                            map.put("runKM", Float.parseFloat(tripTO.getRunKm()) / vehicleDriverCount);
                            map.put("reeferHours", Float.parseFloat(tripTO.getRunHm()) / vehicleDriverCount);
                            map.put("gpsKm", Float.parseFloat(tripTO.getGpsKm()) / vehicleDriverCount);
                            map.put("gpsHm", Float.parseFloat(tripTO.getGpsHm()) / vehicleDriverCount);
                            map.put("reeferMinutes", "0");
                            map.put("mileage", tripTO.getMilleage());
                            map.put("reeferConsumption", tripTO.getReeferMileage());
                            map.put("fuelConsumed", Float.parseFloat(tripTO.getDieselUsed()) / vehicleDriverCount);
                            map.put("fuelAmount", tripTO.getFuelPrice());
                            map.put("fuelCost", Float.parseFloat(tripTO.getDieselCost()) / vehicleDriverCount);
                            map.put("tollAmount", tripTO.getTollRate());
                            map.put("tollCost", Float.parseFloat(tripTO.getTollCost()) / vehicleDriverCount);
                            map.put("incentiveAmount", tripTO.getDriverIncentivePerKm());
                            map.put("driverIncentive", Float.parseFloat(tripTO.getDriverIncentive()) / vehicleDriverCount);
                            map.put("miscValue", tripTO.getMiscValue());
                            map.put("battaAmount", tripTO.getDriverBattaPerDay());
                            map.put("driverBatta", Float.parseFloat(tripTO.getDriverBatta()) / vehicleDriverCount);
                            map.put("systemExpenses", 0);
//                            map.put("systemExpenses", Float.parseFloat(tripTO.getSystemExpense()) / vehicleDriverCount);
                            map.put("routeExpense", Float.parseFloat(tripTO.getBookedExpense()) / vehicleDriverCount);
                            map.put("totalExpense", Float.parseFloat(tripTO.getTotalExpenses()) / vehicleDriverCount);
                            System.out.println("final Map = " + map);
                            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriverClosure", map);
                        }
                    }

                }

            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripClosure List", sqlException);
        }

        return status;
    }

    public int updateEndTripSheet(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("endDate", tripTO.getEndDate());
        map.put("emptyTripApprovalStatus", tripTO.getEmptyTripApprovalStatus());
        map.put("planEndDate", tripTO.getPlanEndDate());
        map.put("planEndTime", tripTO.getPlanEndHour() + ":" + tripTO.getPlanEndMinute() + ":00");
        map.put("endTime", tripTO.getTripEndHour() + ":" + tripTO.getTripEndMinute() + ":00");
        map.put("endOdometerReading", tripTO.getEndOdometerReading());
        map.put("startOdometerReading", tripTO.getStartOdometerReading());
        map.put("endHM", tripTO.getEndHM());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("tripId", tripTO.getTripSheetId());
        map.put("endTripRemarks", tripTO.getEndTripRemarks());
        map.put("totalKM", tripTO.getTotalKM());
        map.put("totalHrs", tripTO.getTotalHrs());
        map.put("lrNumber", tripTO.getLrNumber());
        map.put("userId", userId);
        map.put("statusId", tripTO.getStatusId());
        map.put("totalDays", tripTO.getTotalDays());
        map.put("loaded", tripTO.getLoadedpackagesNew());
        map.put("unloaded", tripTO.getUnloadedpackagesNew());

        map.put("docketNo", tripTO.getDocketNo());
        map.put("invoiceNumber", tripTO.getInvoiceNumber());
        map.put("invoiceValue", tripTO.getInvoiceValue());
        map.put("invoiceDate", tripTO.getInvoiceDate());
        map.put("eWayBillNo", "NA");
        System.out.println("the updateEndTripSheet : " + map);

        map.put("packageDelivered", tripTO.getPackageDelivered());
        map.put("noticeNo", tripTO.getNoticeNo());
        map.put("natureOfLoss", tripTO.getNatureOfLoss());
        map.put("actualWeight", tripTO.getActualWeight());
        map.put("deliveredWeight", tripTO.getDeliveredWeight());
        map.put("outwardCondition", tripTO.getOutwardCondition());
        map.put("detailsOfFacts", tripTO.getDetailsOfFacts());
        map.put("claimValue", tripTO.getClaimValue());
        map.put("amountLoss", tripTO.getAmountLoss());
        map.put("addressInfo", tripTO.getAddressInfo());
        map.put("damage", tripTO.getDamageNew());

        if ("Shortage".equalsIgnoreCase(tripTO.getNatureOfLoss())) {
            map.put("shortage", tripTO.getDamageNew());
        } else {
            map.put("damageNew", tripTO.getDamageNew());

        }

        double claimValue = Double.parseDouble(tripTO.getClaimValue());
        System.out.println("claimValue : " + claimValue);
        if (claimValue <= 10000) {
            map.put("status", 1);
        } else {
            map.put("status", 2);
        }
        String cofSequence = "";
        cofSequence = (String) getSqlMapClientTemplate().queryForObject("trip.getcofNo", map);
        System.out.println("cofSequence :" + cofSequence);

        if (cofSequence == null || "0".equals(cofSequence)) {
            cofSequence = "1";
        }

        String cofNo = "";
        cofNo = "KL" + "/" + "3PL" + "/" + cofSequence;
        map.put("cofNo", cofNo);
        System.out.println("map : " + map);

        if (!"".equals(tripTO.getNoticeNo())) {
            int status = (Integer) getSqlMapClientTemplate().update("trip.insertCofDetails", map);
        }

        map.put("vehicleactreportdate", tripTO.getVehicleactreportdate());
        if ("2".equals(tripTO.getEmptyTripApprovalStatus())) {
            map.put("vehicleloadreportdate", tripTO.getEndDate());
        } else {
            map.put("vehicleloadreportdate", tripTO.getVehicleloadreportdate());
        }
        //map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");

        map.put("vehicleloadreporttime", tripTO.getVehicleloadreporthour() + ":" + tripTO.getVehicleloadreportmin() + ":00");
        map.put("vehicleloadtemperature", tripTO.getVehicleloadtemperature());
        map.put("tripPlannedEndDate", tripTO.getTripPlannedEndDate());

        if (tripTO.getEndReportingTime() != null && !"".equals(tripTO.getEndReportingTime())) {
            map.put("vehicleactreporttime", tripTO.getEndReportingTime());
        }
        if (tripTO.getEndReportingTime() == null && !"".equals(tripTO.getEndReportingTime())) {
            map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");
        }

        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            int consignmentId = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripConsignmentId", map);
            System.out.println("consignmentId==" + consignmentId);
            map.put("consignmentId", consignmentId);
            status = (Integer) getSqlMapClientTemplate().update("trip.insertEndTripSheetDetails", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateEndTripSheet", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripVehicleKM", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateShortageDetails", map);

            String vehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleId", map);
            System.out.println("vehicleId--" + vehicleId);
            map.put("vehicleId", vehicleId);
            map.put("expenseName", "1048");
            map.put("detentionAmt", tripTO.getOriginDetention());
            map.put("customerId", tripTO.getCustomerId());
            if (!"".equals(tripTO.getOriginDetention()) && !"0".equals(tripTO.getOriginDetention())) {
                status = (Integer) getSqlMapClientTemplate().update("trip.updateTripDetention", map);
                System.out.println("status-enddddddd----" + status);
            }
            String[] loadOrderId = tripTO.getLoadOrderId();
            System.out.println("loadOrderId------" + loadOrderId);
            String[] pendingPkgs = tripTO.getPendingPkgs();
            String[] pendingWeight = tripTO.getPendingWeight();
//            String[] loadedPkgs = tripTO.getLoadedPkgs();
//            String[] loadedWeight = tripTO.getLoadedWeight();

            int j = 1;
            for (int i = 0; i < loadOrderId.length; i++) {
                map.put("consignmentOrderId", loadOrderId[i]);
                map.put("pendingPkgs", pendingPkgs[i]);
                map.put("pendingWeight", pendingWeight[i]);
//                map.put("loadedPkgs", loadedPkgs[i]);
//                map.put("loadedWeight", loadedWeight[i]);
                map.put("orderSequence", j);
                j++;

                System.out.println("pendingPkgs[i]===" + pendingPkgs[i]);
                System.out.println("pendingWeight[i]===" + pendingWeight[i]);

                if ("0.00".equals(pendingPkgs[i]) && "0.00".equals(pendingWeight[i])) {
                    System.out.println("Gooooooo=" + pendingWeight[i]);
                    System.out.println("Gooooooommmm=" + pendingPkgs[i]);
                    map.put("statusId", tripTO.getStatusId());
                } else {
                    System.out.println("elseeeeeeeeeeeeeeeee");
                    map.put("statusId", "5");
                }
                System.out.println("map value is:" + map);

                status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatus", map);

            }
            System.out.println("tripTO.getEmptyTripApprovalStatus()=" + tripTO.getEmptyTripApprovalStatus());
            System.out.println("tripTO.getEmptyTripApprovalStatus()=" + tripTO.getStatusId());
            if ("2".equals(tripTO.getEmptyTripApprovalStatus()) && "10".equals(tripTO.getStatusId())) {
                status = (Integer) getSqlMapClientTemplate().update("trip.updatetstsVehicleAvailability", map);
            }

            int updateConsignmentArticle = 0;
            map.put("consignmentId", tripTO.getConsignmentId());
            String[] productCodes = null;
            String[] unloadedpackages = null;
            String[] shortage = null;
            String[] articleId = null;
            productCodes = tripTO.getProductCodes();
            unloadedpackages = tripTO.getUnloadedpackages();
            articleId = tripTO.getTripArticleId();
            shortage = tripTO.getShortage();
            if (tripTO.getProductCodes() != null && !"".equals(tripTO.getProductCodes()) && !"".equals(tripTO.getConsignmentId()) && tripTO.getConsignmentId() != null) {
                for (int i = 0; i < productCodes.length; i++) {
                    map.put("productCode", productCodes[i]);
                    map.put("loadedpackages", unloadedpackages[i]);
                    map.put("shortage", shortage[i]);
                    map.put("tripArticleId", articleId[i]);
                    System.out.println("the inner" + map);
                    updateConsignmentArticle = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentArticle", map);
                }
            }

            System.out.println("updateEndTripSheet size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateEndTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateEndTripSheet List", sqlException);
        }

        return status;
    }

    public ArrayList getEmptyTripDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList emptyTripList = new ArrayList();
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        map.put("tripId", tripTO.getTripId());
        try {
            emptyTripList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmptyTripDetails", map);
            System.out.println("getEmptyTripDetails.size() = " + emptyTripList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmptyTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEmptyTripDetails List", sqlException);
        }
        return emptyTripList;
    }

    public int saveManualEmptyTripApproval(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        map.put("tripSheetId", tripTO.getTripId());
        map.put("approvalStatus", tripTO.getApprovalStatus());
        map.put("userId", tripTO.getUserId());
        System.out.println("the saveEmptyTripApproval" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveEmptyTripApproval", map);
            System.out.println("saveEmptyTripApproval size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveEmptyTripApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveEmptyTripApproval List", sqlException);
        }

        return status;
    }

    public int updateStartTripSheetDuringClosure(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("startKm", tripTO.getStartKm());
        map.put("startHm", tripTO.getStartHM());
        map.put("tripStartLoadTemp", tripTO.getLoadingTemperature());
        map.put("tripStartDate", tripTO.getStartDate());
        map.put("tripStartTime", tripTO.getStartTime());
        map.put("tripPlannedTime", tripTO.getTripPlannedTime());
        map.put("tripPlannedDate", tripTO.getTripPlannedDate());
        map.put("tripStartReportingDate", tripTO.getTripStartReportingDate());
        map.put("userId", userId);
        System.out.println("the updateStartTripSheetDuringClosure" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateStartTripSheetDuringClosure", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateStartTripVehicleDuringClosure", map);
            System.out.println("updateStartTripSheetDuringClosure =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int updateEndTripSheetDuringClosure(TripTO tripTO, int userId) {
        Map map = new HashMap();

        map.put("endTime", tripTO.getTripEndHour() + ":" + tripTO.getTripEndMinute() + ":00");
        map.put("endDate", tripTO.getEndDate());
        map.put("totalDays", tripTO.getTotalDays());
        map.put("tripTransitHours", tripTO.getTripTransitHours());
        map.put("endOdometerReading", tripTO.getEndOdometerReading());
        map.put("endHM", tripTO.getEndHM());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        //map.put("endTripRemarks", tripTO.getEndTripRemarks());
        map.put("totalKM", tripTO.getTotalKM());
        map.put("totalHrs", tripTO.getTotalHrs());
        map.put("userId", userId);
        if (!"".equals(tripTO.getSetteledKm()) && (tripTO.getSetteledKm()) != null) {
            map.put("setteledKm", tripTO.getSetteledKm());
        }
        if (!"".equals(tripTO.getSetteledHm()) && (tripTO.getSetteledHm()) != null) {
            map.put("setteledHm", tripTO.getSetteledHm());
        }
        if (!"".equals(tripTO.getSetteltotalKM()) && (tripTO.getSetteltotalKM()) != null) {
            map.put("setteltotalKM", tripTO.getSetteltotalKM());
        }
        if (!"".equals(tripTO.getSetteltotalHrs()) && (tripTO.getSetteltotalHrs()) != null) {
            map.put("setteltotalHrs", tripTO.getSetteltotalHrs());
        }
        System.out.println("the updateEndTripSheetDuringClosure" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateEndTripSheetDuringClosure", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateEndTripVehicleDuringClosure", map);
            System.out.println("updateEndTripSheetDuringClosure size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateEndTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateEndTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int overrideEndTripSheetDuringClosure(TripTO tripTO, int userId) {
        Map map = new HashMap();

        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("overrideTripRemarks", tripTO.getOverrideTripRemarks());
        map.put("userId", userId);
        if (!"".equals(tripTO.getSetteledKm()) && (tripTO.getSetteledKm()) != null) {
            map.put("setteledKm", tripTO.getSetteledKm());
        }
        if (!"".equals(tripTO.getSetteledHm()) && (tripTO.getSetteledHm()) != null) {
            map.put("setteledHm", tripTO.getSetteledHm());
        }
        if (!"".equals(tripTO.getSetteltotalKM()) && (tripTO.getSetteltotalKM()) != null) {
            map.put("setteltotalKM", tripTO.getSetteltotalKM());
        }
        if (!"".equals(tripTO.getSetteltotalHrs()) && (tripTO.getSetteltotalHrs()) != null) {
            map.put("setteltotalHrs", tripTO.getSetteltotalHrs());
        }
        System.out.println("the overrideEndTripSheetDuringClosure" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.overrideEndTripSheetDuringClosure", map);
            System.out.println("overrideEndTripSheetDuringClosure size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("overrideEndTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "overrideEndTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

//    public int updateStatus(TripTO tripTO, int userId) {
//        Map map = new HashMap();
//        map.put("tripSheetId", tripTO.getTripSheetId());
//        map.put("tripId", tripTO.getTripSheetId());
//        map.put("userId", userId);
//        map.put("statusId", tripTO.getStatusId());
//        if("16".equals(tripTO.getStatusId())){
//            String[] consignmentOrderIds = tripTO.getConsignmentOrderIds();
//            List consignmentIds = new ArrayList(consignmentOrderIds.length);
//            for (int j = 0; j < consignmentOrderIds.length; j++) {
//                System.out.println("cn value:" + consignmentOrderIds[j]);
//                consignmentIds.add(consignmentOrderIds[j]);
//            }
//            map.put("consignmentOrderIds", consignmentIds);
//        }
//        
//        
//        
//        System.out.println("the updateEndTripSheet" + map);
//        int status = 0;
//        ArrayList tripDetails = new ArrayList();
//        int tripVehicleCount = 0;
//        int tripClosureCount = 0;
//        int tripCustomerCount = 0;
//        int tripInvoiceCount = 0;
//        try {
//            //values(#tripSheetId#, #statusId#, #KM#, #HM#, #remarks#, #userId#, now())
//            tripCustomerCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripCustomerCount", map);
//            tripInvoiceCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripInvoiceCount", map);
//            tripVehicleCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleCount", map);
//            tripClosureCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripClosureCount", map);
//            System.out.println("tripInvoiceCount:"+tripInvoiceCount+"tripCustomerCount:"+tripCustomerCount);
//            
//            if(Integer.parseInt(tripTO.getStatusId()) > 13){
//            if (tripClosureCount > 0) {
//                if (tripInvoiceCount == tripCustomerCount) {
//                    map.put("KM", 0);
//                    map.put("HM", 0);
//                    map.put("remarks", "system update");
//                    status = (Integer) getSqlMapClientTemplate().update("trip.insertTripStatus", map);
//                    status = (Integer) getSqlMapClientTemplate().update("trip.updateTripStatus", map);
//                    if("16".equals(tripTO.getStatusId())){
//                    status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatusBilling", map);
//                    }else{
//                    status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatus", map);
//                        
//                    }
//                    System.out.println("updateStatus 11111111111111111111111111111111111111111=" + tripDetails.size());
//                }else{
//                    if("16".equals(tripTO.getStatusId())){
//                    status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatusBilling", map);
//                    }else{
//                    status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatus", map);
//                        
//                    }
//                }
//                }
//                
//            } else {
//                map.put("KM", 0);
//                map.put("HM", 0);
//                map.put("remarks", "system update");
//                status = (Integer) getSqlMapClientTemplate().update("trip.insertTripStatus", map);
//                status = (Integer) getSqlMapClientTemplate().update("trip.updateTripStatus", map);
//                if("16".equals(tripTO.getStatusId())){
//                    status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatusBilling", map);
//                    }else{
//                    status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatus", map);
//                        
//                    }
//                System.out.println("updateStatus 2222222222222222222size=" + tripDetails.size());
//            }
//        } catch (Exception sqlException) {
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("updateStatus Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStatus List", sqlException);
//        }
//
//        return status;
//    }
    public int updateStatus(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("tripId", tripTO.getTripSheetId());
        map.put("userId", userId);
        map.put("statusId", tripTO.getStatusId());
        map.put("lrNumber", tripTO.getLrNumber());

        System.out.println("updateEndTripSheet arun check : " + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        int tripVehicleCount = 0;
        int tripClosureCount = 0;
        int lrNumbers = 0;

        try {

            tripVehicleCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleCount", map);
            tripClosureCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripClosureCount", map);
            System.out.println("tripVehicleCount:" + tripVehicleCount);
            System.out.println("tripClosureCount:" + tripClosureCount);

            if (tripClosureCount > 0) {
//                if (tripVehicleCount == tripClosureCount) {
                map.put("KM", 0);
                map.put("HM", 0);
                map.put("remarks", "system update");
                status = (Integer) getSqlMapClientTemplate().update("trip.insertTripStatus", map);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateTripStatus", map);
                System.out.println("updateStatus size=" + tripDetails.size());
//                }
            } else {
                map.put("KM", 0);
                map.put("HM", 0);
                map.put("remarks", "system update");
                System.out.println("else updateTripStatus arun part :" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.insertTripStatus", map);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateTripStatus", map);
                System.out.println("updateStatus size : " + tripDetails.size());
            }

            String[] loadOrderId = tripTO.getLoadOrderId();
            System.out.println("loadOrderId------" + loadOrderId);
            String[] pendingPkgs = tripTO.getPendingPkgs();
            String[] pendingWeight = tripTO.getPendingWeight();
            String[] loadedPkgs = tripTO.getLoadedPkgs();
            String[] loadedWeight = tripTO.getLoadedWeight();

            int j = 1;
            for (int i = 0; i < loadOrderId.length; i++) {
                map.put("consignmentOrderId", loadOrderId[i]);
                map.put("pendingPkgs", pendingPkgs[i]);
                map.put("pendingWeight", "0");
                map.put("orderSequence", j);
                j++;

                System.out.println("arun pendingPkgs[i] check : " + pendingPkgs[i]);

                if ("0.00".equals(pendingPkgs[i])) {

                    System.out.println("pendingPkgs=" + pendingPkgs[i]);

                    int tripStatus = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkTripStatus", map);
                    if (tripStatus == 1) {
                        map.put("statusId", tripTO.getStatusId());
                    } else {
                        map.put("statusId", "5");
                    }

                } else {
                    System.out.println("else arun part ");
                    map.put("statusId", "5");
                }
                System.out.println("map value is:" + map);

                status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatus", map);
            }

            lrNumbers = (Integer) getSqlMapClientTemplate().update("trip.UpdatelrNumber", map);
            System.out.println("updateStatus size=" + tripDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStatus List", sqlException);
        }

        return status;
    }

    public int updateStatusForInvoice(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("tripId", tripTO.getTripSheetId());
        map.put("userId", userId);
        map.put("statusId", tripTO.getStatusId());
        map.put("lrNumber", tripTO.getLrNumber());

        System.out.println("the updateEndTripSheet invoiceee" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        int tripVehicleCount = 0;
        int tripClosureCount = 0;
        int lrNumbers = 0;
        try {
            //values(#tripSheetId#, #statusId#, #KM#, #HM#, #remarks#, #userId#, now())

            map.put("KM", 0);
            map.put("HM", 0);
            map.put("remarks", "system update");
            status = (Integer) getSqlMapClientTemplate().update("trip.insertTripStatus", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripStatus", map);
            for (int i = 0; i < tripTO.getConsignmentOrderIds().length; i++) {
                System.out.println("tripTO.getConsignmentOrderIds()[i]=" + tripTO.getConsignmentOrderIds()[i]);
                map.put("consignmentOrderIds", tripTO.getConsignmentOrderIds()[i]);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatusForInvoice", map);
            }
            lrNumbers = (Integer) getSqlMapClientTemplate().update("trip.UpdatelrNumber", map);
            System.out.println("updateStatus size=" + tripDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStatus List", sqlException);
        }

        return status;
    }

    public ArrayList getFuelDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());

        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getFuelDetails", map);
            System.out.println("getFuelDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getOtherExpenseDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());

        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOtherExpenseDetails", map);
            System.out.println("getOtherExpenseDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOtherExpenseDetails List", sqlException);
        }

        return tripDetails;
    }

    public int insertTripFuelExpense(String tripSheetId, String location, String fillDate, String fuelLitres, String fuelAmount, String fuelRemarks, String fuelPricePerLitre, int userId) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        if (tripSheetId != null && tripSheetId != "") {
            tripId = Integer.parseInt(tripSheetId);
            map.put("tripSheetId", tripId);
        }
        map.put("fillDate", fillDate);
        map.put("location", location);
        map.put("fuelPricePerLitre", fuelPricePerLitre);
        map.put("fuelLitres", fuelLitres);
        map.put("fuelAmount", fuelAmount);
        map.put("fuelRemarks", fuelRemarks);
        map.put("userId", userId);
        System.out.println("the insertTripFuel" + map);
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.insertTripFuel", map);
            System.out.println("insertTripFuel size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertTripFuel Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertTripFuel List", sqlException);
        }

        return status;
    }

    public ArrayList getExpenseDetails(TripTO tripTO) {
        Map map = new HashMap();
        String orderType = tripTO.getOrderType();
        System.out.println("orderType==" + orderType);
        map.put("orderType", orderType);
        ArrayList expenseDetails = new ArrayList();
        try {
            expenseDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getExpenseDetails", map);
            System.out.println("getExpenseDetails size=" + expenseDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getExpenseDetails List", sqlException);
        }

        return expenseDetails;
    }

    public ArrayList getDriverName(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        ArrayList driverNameDetails = new ArrayList();
        System.out.println("map getDriverName = " + map);
        try {
            driverNameDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverName", map);
            System.out.println("getDriverName size=" + driverNameDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverName List", sqlException);
        }

        return driverNameDetails;
    }

    public int insertTripOtherExpense(String tripSheetId, String vehicleId, String expenseName, String employeeName, String expenseType, String expenseLocation, String expenseDate, String expenseHour, String expenseMinute, String taxPercentage, String expenseRemarks, String expenses, String netExpense, String billMode, String marginValue, String activeValue, String customerId, String orderId, int userId) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        if (tripSheetId != null && tripSheetId != "") {
            tripId = Integer.parseInt(tripSheetId);
            map.put("tripSheetId", tripId);
        }

        map.put("vehicleId", vehicleId);
        map.put("expenseType", expenseType);
        map.put("billMode", billMode);
        map.put("expenseName", expenseName);
        map.put("employeeName", employeeName);
        map.put("activeValue", activeValue);
        map.put("activeValue", activeValue);
        if ("".equals(customerId)) {
            map.put("customerId", "0");
        } else {
            map.put("customerId", customerId);
        }
        if ("".equals(orderId)) {
            map.put("consignmentOrderId", "0");
        } else {
            map.put("consignmentOrderId", orderId);
        }
        String[] temp = null;
        temp = expenseDate.split("-");
        map.put("expenseDate", temp[2] + "-" + temp[1] + "-" + temp[0] + " " + expenseHour + ":" + expenseMinute + ":" + "00");
        if (!"".equals(taxPercentage)) {
            map.put("taxPercentage", taxPercentage);
        } else {
            map.put("taxPercentage", "0");
        }
        map.put("expenseRemarks", expenseRemarks);
        if (!"".equals(expenses)) {
            map.put("expenses", expenses);
        } else {
            map.put("expenses", "0");
        }
        if (!"".equals(netExpense)) {
            map.put("netExpense", netExpense);
        } else {
            map.put("netExpense", "0");
        }
        if (!"".equals(marginValue)) {
            map.put("marginValue", marginValue);
        } else {
            map.put("marginValue", "0");
        }
        map.put("userId", userId);
        map.put("expenseLocation", expenseLocation);

        System.out.println("the insertTripOtherExpense" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.insertTripOtherExpense", map);
            System.out.println("insertTripOtherExpense size=");
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertTripFuel Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertTripFuel List", sqlException);
        }

        return status;
    }

    public ArrayList getExpiryDateDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList expiryDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            expiryDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getExpiryDateDetails", map);
            System.out.println("getExpiryDateDetails.size() = " + expiryDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getExpiryDateDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getExpiryDateDetails List", sqlException);
        }
        return expiryDetails;
    }

    public int updateTripOtherExpense(String tripSheetId, String vehicleId, String expenseName, String employeeName, String expenseType, String expenseLocation, String expenseDate, String expenseHour, String expenseMinute, String taxPercentage, String expenseRemarks, String expenses, String netExpense, String billMode, String marginValue, String expenseId, String activeValue, String customerId, String orderId, int userId) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        int expenseType1 = 0;
        int expenseLocation1 = 0;
        int billMode1 = 0;
        if (tripSheetId != null && tripSheetId != "") {
            tripId = Integer.parseInt(tripSheetId);
            map.put("tripSheetId", tripId);
        }
        map.put("vehicleId", vehicleId);
        if (billMode != null && billMode != "") {
            billMode1 = Integer.parseInt(billMode);
            map.put("billMode", billMode1);
        }
        if (expenseType != null && expenseType != "") {
            expenseType1 = Integer.parseInt(expenseType);
            map.put("expenseType", expenseType1);
        }
        if (!"".equals(taxPercentage)) {
            map.put("taxPercentage", taxPercentage);
        } else {
            map.put("taxPercentage", "0");
        }
        map.put("expenseRemarks", expenseRemarks);
        if (!"".equals(expenses)) {
            map.put("expenses", expenses);
        } else {
            map.put("expenses", "0");
        }
        if (!"".equals(netExpense)) {
            map.put("netExpense", netExpense);
        } else {
            map.put("netExpense", "0");
        }
        if (!"".equals(marginValue)) {
            map.put("marginValue", marginValue);
        } else {
            map.put("marginValue", "0");
        }
        map.put("expenseType", expenseType);
        map.put("billMode", billMode);
        map.put("expenseName", expenseName);
        map.put("expenseId", expenseId);
        map.put("employeeName", employeeName);
        map.put("customerId", customerId);
        map.put("orderId", orderId);
        map.put("expenseLocation", expenseLocation);
//        map.put("consignmentOrderId", customerId);
        String[] temp = null;
        temp = expenseDate.split("-");
        map.put("expenseDate", temp[2] + "-" + temp[1] + "-" + temp[0] + " " + expenseHour + ":" + expenseMinute + ":00");
        map.put("expenseRemarks", expenseRemarks);
        map.put("activeValue", activeValue);
        map.put("userId", userId);
        if (expenseLocation != null && expenseLocation != "") {
            expenseLocation1 = Integer.parseInt(expenseLocation);
            map.put("expenseLocation", expenseLocation1);
        }
        System.out.println("the updateTripOtherExpense" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripOtherExpense", map);
            System.out.println("updateTripOtherExpense size=");
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTripOtherExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTripOtherExpense List", sqlException);
        }

        return status;
    }

    public int saveTripExpensePodDetailsOLD(String actualFilePath, String tripExpenseId1, int userId) {
        Map map = new HashMap();
        int status = 0;
        FileInputStream fis = null;
        int tripExpenseId = 0;
        try {
            if (tripExpenseId1 != null && tripExpenseId1 != "") {
                tripExpenseId = Integer.parseInt(tripExpenseId1);
                map.put("tripExpenseId", tripExpenseId);
            }
            map.put("userId", userId);
            File file = new File(actualFilePath);
            fis = new FileInputStream(file);
            byte[] podFile = new byte[(int) file.length()];
            fis.read(podFile);
            fis.close();
            map.put("podFile", podFile);
            System.out.println("the saveTripExpensePodDetails" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripExpensePodDetailsOLD", map);
            System.out.println("saveTripExpensePodDetails size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripExpensePodDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripExpensePodDetails List", sqlException);
        }

        return status;
    }

    public int saveTripExpensePodDetails(String actualFilePath, String fileSave, String tripExpenseId1, int userId) {
        Map map = new HashMap();
        int status = 0;
        int status1 = 0;
        FileInputStream fis = null;
        int tripExpenseId = 0;
        try {
            if (tripExpenseId1 != null && tripExpenseId1 != "") {
                tripExpenseId = Integer.parseInt(tripExpenseId1);
                map.put("tripExpenseId", tripExpenseId);
            }
            map.put("userId", userId);
            File file = new File(actualFilePath);
            fis = new FileInputStream(file);
            byte[] podFile = new byte[(int) file.length()];
            fis.read(podFile);
            fis.close();
            map.put("podFile", podFile);
            map.put("fileSave", fileSave);
            System.out.println("the saveTripExpensePodDetails" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripExpensePodDetails", map);
            status1 = (Integer) getSqlMapClientTemplate().update("trip.saveTripExpenseDoumentUploadedDetails", map);
            System.out.println("saveTripExpensePodDetails size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripExpensePodDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripExpensePodDetails List", sqlException);
        }

        return status;
    }
//Nithya End 7 Dec
//Throttle Start Here 07-12-2013

    /**
     * This method used to get Trip Advance Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getTripAdvanceDetails(String tripSheetId) {
        Map map = new HashMap();
        ArrayList tripAdvanceDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripSheetId);
            System.out.println("closed Trip map is::" + map);
            tripAdvanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripAdvanceDetails", map);
            if (tripAdvanceDetails.size() == 0) {
                tripAdvanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripAdvanceDetailsOld", map);
            }
            System.out.println("tripAdvanceDetails.size() = " + tripAdvanceDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripAdvanceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripAdvanceDetails List", sqlException);
        }
        return tripAdvanceDetails;
    }

    /**
     * This method used to get Trip Fuel Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getTripFuelDetails(String tripSheetId) {
        Map map = new HashMap();
        ArrayList tripFuelDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripSheetId);
            System.out.println("closed Trip map is::" + map);
            tripFuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripFuelDetails", map);
            System.out.println("tripFuelDetails.size() = " + tripFuelDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripFuelDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripFuelDetails List", sqlException);
        }
        return tripFuelDetails;
    }

    /**
     * This method used to get Trip Fuel Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getTripExpenseDetails(String tripSheetId) {
        Map map = new HashMap();
        ArrayList tripExpenseDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripSheetId);
            System.out.println("closed Trip map is::" + map);
            tripExpenseDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripExpenseDetails", map);
            System.out.println("tripExpenseDetails.size() = " + tripExpenseDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripExpenseDetails List", sqlException);
        }
        return tripExpenseDetails;
    }

    public ArrayList getTripStausDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList statusDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println("closed Trip map is::" + map);
            statusDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripStatusDetails", map);
            System.out.println("getTripStausDetails.size() = " + statusDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripStausDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripStausDetails List", sqlException);
        }
        return statusDetails;
    }

    public ArrayList getPODDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList statusDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            map.put("orderId", tripTO.getOrderId());
            System.out.println(" Trip map is::" + map);
            statusDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getPODDetails", map);
            System.out.println("getPODDetails.size() = " + statusDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPODDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPODDetails List", sqlException);
        }
        return statusDetails;
    }

    public ArrayList getPODUploadDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList statusDetails = new ArrayList();
        try {
            map.put("tripIds", tripTO.getTripIds());
            System.out.println(" Trip map is::" + map);
            statusDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getPODUploadDetails", map);
            System.out.println("getPODDetails.size() = " + statusDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPODDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPODDetails List", sqlException);
        }
        return statusDetails;
    }

    public ArrayList getTripPackDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList tripPackDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            tripPackDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripPackDetails", map);
            System.out.println("getTripPackDetails.size() = " + tripPackDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripPackDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripPackDetails List", sqlException);
        }
        return tripPackDetails;
    }

    public ArrayList getTripUnPackDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList tripUnPackDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            tripUnPackDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripUnPackDetails", map);
            System.out.println("tripUnPackDetails.size() = " + tripUnPackDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tripUnPackDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "tripUnPackDetails List", sqlException);
        }
        return tripUnPackDetails;
    }

    public ArrayList viewApproveDetails(String tripid, int tripclosureid) {
        Map map = new HashMap();
        ArrayList viewApproveDetails = new ArrayList();
        map.put("tripid", tripid);
        map.put("tripclosureid", tripclosureid);

        try {
            viewApproveDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.viewApproveDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("viewApproveDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "viewApproveDetails", sqlException);
        }
        return viewApproveDetails;
    }

    public ArrayList getEmailDetails(String activitycode) {
        ArrayList EmailDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("actcode", activitycode);
            EmailDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmailDetails", map);
            System.out.println("EmailDetails size=" + EmailDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("EmailDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "EmailDetails List", sqlException);
        }

        return EmailDetails;
    }

    public ArrayList getCityList() {
        ArrayList cityList = new ArrayList();
        Map map = new HashMap();
        try {

            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCityList", map);
            System.out.println("getCityList size=" + cityList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCityList List", sqlException);
        }

        return cityList;
    }

    public ArrayList getmailcontactDetails(String tripid) {
        ArrayList getmailcontactDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("tripid", tripid);
            getmailcontactDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getmailContactDetails", map);
            System.out.println("getmailcontactDetails size=" + getmailcontactDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getmailcontactDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getmailcontactDetails List", sqlException);
        }

        return getmailcontactDetails;
    }

    public ArrayList getTripStakeHoldersList() throws FPRuntimeException, FPBusinessException {
        ArrayList holdersList = new ArrayList();
        Map map = new HashMap();
        try {

            holdersList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripStakeHolders", map);
            System.out.println("Size in holdersList " + holdersList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return holdersList;
    }

    public ArrayList getstatusList() throws FPRuntimeException, FPBusinessException {
        ArrayList statusList = null;
        Map map = new HashMap();
        try {

            statusList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getstatusList", map);
            System.out.println("Size in statusList " + statusList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return statusList;
    }

    /**
     * This method used to Get Available Functions .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getEmailFunction(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList setStatusList = new ArrayList();
        Map map = new HashMap();
        //        int statusId;
        try {
            map.put("stakeHolderId", tripTO.getStakeHolderId());
            map.put("holderName", tripTO.getHolderName());
            map.put("statusId", tripTO.getStatusId());
            map.put("statusName", tripTO.getStatusName());
            System.out.print("map----" + map);
            setStatusList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getSetStatusList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return setStatusList;
    }

    /**
     * This method used to Get Available Functions .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getAssignedFunc(String stackHolderId) throws FPRuntimeException, FPBusinessException {
        ArrayList assignedFunc = null;
        Map map = new HashMap();
        //        int statusId;
        try {
            map.put("statusId", stackHolderId);

            assignedFunc = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getSetStatusList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return assignedFunc;
    }

    /**
     * This method used to Get Assigned Functions.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getAssignedFucntions(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList assignedFunctions = new ArrayList();
        Map map = new HashMap();
        //String sqlQuery = "select Function_Id from Role_function where Role_Id="+roleId+"";

        try {
            map.put("statusId", tripTO.getStatusId());

            assignedFunctions = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getAssignedFunctions", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return assignedFunctions;
    }

    /**
     * This method used to Get Assigned Functions.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertEmailSettings(String[] assignedFunc, String stackHolderId, int userId) throws FPRuntimeException, FPBusinessException {

        Map map = new HashMap();
        int updateStatus = 0;
        int insertStatus = 0;
        try {
            map.put("statusId", stackHolderId);
            System.out.println("stake holder List" + map + "length assigned func" + assignedFunc.length);
            if ((Integer) getSqlMapClientTemplate().update("trip.deleteStakeholder", map) != null) {

                updateStatus = (Integer) getSqlMapClientTemplate().update("trip.deleteStakeholder", map);
            }
            if (assignedFunc != null) {
                for (int i = 0; i < assignedFunc.length; i++) {
                    map.put("statusId", stackHolderId);
                    map.put("userId", userId);
                    map.put("assignedFuncId", assignedFunc[i]);
                    System.out.println("insert map" + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().insert("trip.insertEmailFunction", map);
                }
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return insertStatus;
    }

    /**
     * This method used to Modify Functions.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int deleteStakeholder(String[] assignedFunc, String stakeHolderId, int userId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int updateStatus = 0;
        try {

            map.put("statusId", stakeHolderId);
            if ((Integer) getSqlMapClientTemplate().update("trip.deleteStakeholder", map) != null) {

                updateStatus = (Integer) getSqlMapClientTemplate().update("trip.deleteStakeholder", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("deletefunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "deletefunctions", sqlException);
        }
        return updateStatus;
    }

    public ArrayList getEditStatuslist(String stackHolderId) throws FPRuntimeException, FPBusinessException {
        ArrayList statusList = null;
        Map map = new HashMap();
        //        int statusId;
        try {
            map.put("statusId", stackHolderId);

            statusList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEditStatusList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return statusList;
    }

    public int insertVehicleDriverAdvance(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int insertVehicleDriverAdvance = 0;
        int index = 0;
//        map.put("userId", userId);
        try {

            map.put("vehicleId", tripTO.getVehicleId());
            map.put("advanceAmount", tripTO.getAdvanceAmount());
            map.put("advanceDate", tripTO.getAdvanceDate());
            map.put("advanceRemarks", tripTO.getAdvanceRemarks());
            map.put("primaryDriverId", tripTO.getPrimaryDriverId());
            map.put("secondaryDriverIdOne", tripTO.getSecondaryDriverIdOne());
            map.put("secondaryDriverIdTwo", tripTO.getSecondaryDriverIdTwo());
            map.put("expenseType", tripTO.getExpenseType());
            map.put("userId", userId);
            System.out.println("map value is:" + map);
            insertVehicleDriverAdvance = (Integer) getSqlMapClientTemplate().insert("trip.insertVehicleDriverAdvance", map);
            System.out.println("insertVehicleDriverAdvance=" + insertVehicleDriverAdvance);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVehicleDriverAdvance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertVehicleDriverAdvance", sqlException);
        }
        return insertVehicleDriverAdvance;
    }

    public String checkVehicleDriverAdvance(String vehicleId) {
        Map map = new HashMap();
        TripTO tripTO = null;
        map.put("vehicleId", vehicleId);
        String checkVehicleDriverAdvance = "";
        System.out.println("map = " + map);
        try {
            checkVehicleDriverAdvance = (String) getSqlMapClientTemplate().queryForObject("trip.checkVehicleDriverAdvance", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkVehicleDriverAdvance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkVehicleDriverAdvance", sqlException);
        }
        return checkVehicleDriverAdvance;

    }

    public ArrayList getVehicleDriverAdvanceList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList statusList = null;
        Map map = new HashMap();
        map.put("usageTypeId", tripTO.getUsageTypeId());
        System.out.println("map = " + map);
        try {

            statusList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleDriverAdvanceList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDriverAdvanceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleDriverAdvanceList", sqlException);
        }
        return statusList;
    }

    public ArrayList getCustomerList() throws FPRuntimeException, FPBusinessException {
        ArrayList customerList = null;
        Map map = new HashMap();
        try {
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCustomerList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCustomerList", sqlException);
        }
        return customerList;
    }

    public ArrayList getZoneList() throws FPRuntimeException, FPBusinessException {
        ArrayList zoneList = null;
        Map map = new HashMap();
        try {
            zoneList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getZoneList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCustomerList", sqlException);
        }
        return zoneList;
    }

    public ArrayList getLocation(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList cityList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("city", tripTO.getCityId() + "%");
        map.put("zoneId", tripTO.getZoneId());
        System.out.println("map = " + map);

        try {
            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getLocation", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCity", sqlException);
        }
        return cityList;
    }

    public int insertBPCLTransactionHistory(ArrayList bpclTransactionList, TripTO tripTO1, int userId) {
        Map map = new HashMap();
        int insertVehicleDriverAdvance = 0;
        int index = 0;
        TripTO tripTO = new TripTO();
        try {
            map.put("userId", userId);
            map.put("transactionHistoryId", tripTO1.getTransactionHistoryId());
            Iterator itr = bpclTransactionList.iterator();
            while (itr.hasNext()) {
                tripTO = (TripTO) itr.next();
                map.put("tripId", tripTO.getTripId());
                map.put("vehicleId", tripTO.getVehicleId());
                map.put("vehicleNo", tripTO.getVehicleNo());
                map.put("accountId", tripTO.getAccountId());
                map.put("dealerName", tripTO.getDealerName());
                map.put("dealerCity", tripTO.getDealerCity());
                String[] temp = null;
                String date = "";
                String time = "";
                temp = tripTO.getTransactionDate().split(" ");
                String[] temp1 = temp[0].split("-");
                date = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
                time = temp[1];
                String transactionDateTime = date + " " + time;
                map.put("transactionDate", transactionDateTime);

                String[] accTemp = null;
                String accDate = "";
                String accTime = "";
                accTemp = tripTO.getAccountingDate().split(" ");
                String[] accDateTemp = accTemp[0].split("-");
                accDate = accDateTemp[2] + "-" + accDateTemp[1] + "-" + accDateTemp[0];
                accTime = accTemp[1];
                String accDateTime = accDate + " " + accTime;
                map.put("accountingDate", accDateTime);

                map.put("transactionType", tripTO.getTransactionType());
                map.put("currency", tripTO.getCurrency());
                map.put("amount", tripTO.getAmount());
                map.put("volumeDocNo", tripTO.getVolumeDocNo());
                map.put("amountBalance", tripTO.getAmoutBalance());
                map.put("petromilesEarned", tripTO.getPetromilesEarned());
                map.put("odometerReading", tripTO.getOdometerReading());
                System.out.println("map value is:" + map);
                insertVehicleDriverAdvance += (Integer) getSqlMapClientTemplate().update("trip.insertBPCLTransactionHistory", map);
            }
            System.out.println("insertVehicleDriverAdvance=" + insertVehicleDriverAdvance);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertBPCLTransactionHistory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertBPCLTransactionHistory", sqlException);
        }
        return insertVehicleDriverAdvance;
    }

    public String checkTransactionHistoryId(TripTO tripTO) {
        Map map = new HashMap();
        map.put("transactionHistoryId", tripTO.getTransactionHistoryId());
        map.put("vehicleNo", tripTO.getVehicleNo());
        map.put("accountId", tripTO.getAccountId());
        map.put("dealerName", tripTO.getDealerName());
        map.put("dealerCity", tripTO.getDealerCity());
        String[] temp = null;
        String date = "";
        String time = "";
        temp = tripTO.getTransactionDate().split(" ");
        String[] temp1 = temp[0].split("-");
        date = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
        time = temp[1];
        String transactionDateTime = date + " " + time;
        map.put("transactionDate", transactionDateTime);

        String[] accTemp = null;
        String accDate = "";
        String accTime = "";
        accTemp = tripTO.getAccountingDate().split(" ");
        String[] accDateTemp = accTemp[0].split("-");
        accDate = accDateTemp[2] + "-" + accDateTemp[1] + "-" + accDateTemp[0];
        accTime = accTemp[1];
        String accDateTime = accDate + " " + accTime;
        map.put("accountingDate", accDateTime);
        String status = "";
        System.out.println("map = " + map);
        try {
            status = (String) getSqlMapClientTemplate().queryForObject("trip.checkTransactionHistoryId", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkTransactionHistoryId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkTransactionHistoryId", sqlException);
        }
        return status;

    }

    public String getTripCodeForBpcl(TripTO tripTO) {
        Map map = new HashMap();
        map.put("vehicleNo", tripTO.getVehicleNo());
        String[] temp = null;
        String date = "";
        String time = "";
        temp = tripTO.getTransactionDate().split(" ");
        String[] temp1 = temp[0].split("-");
        date = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
        time = temp[1];
        String transactionDateTime = date + " " + time;
        map.put("transactionDate", transactionDateTime);
        String vehicleId = "";
        String tripId = "";
        System.out.println("map = " + map);
        try {
            vehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleIdForBpcl", map);
            if (vehicleId != null && !"".equals(vehicleId)) {
                map.put("vehicleId", vehicleId);
                System.out.println("map vehicleId = " + map);
                tripId = (String) getSqlMapClientTemplate().queryForObject("trip.getTripIdForBpcl", map);
                if (tripId != null) {
                    tripId = tripId + "~" + vehicleId;
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripIdForBpcl Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripIdForBpcl", sqlException);
        }
        return tripId;

    }

    public int getTripCodeCountForBpcl(TripTO tripTO) {
        Map map = new HashMap();
        map.put("vehicleNo", tripTO.getVehicleNo());
        String[] temp = null;
        String date = "";
        String time = "";
        temp = tripTO.getTransactionDate().split(" ");
        String[] temp1 = temp[0].split("-");
        date = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
        time = temp[1];
        String transactionDateTime = date + " " + time;
        map.put("transactionDate", transactionDateTime);
        String vehicleId = "";
        int tripCount = 0;
        System.out.println("map = " + map);
        try {
            vehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleIdForBpcl", map);
            if (vehicleId != null && !"".equals(vehicleId)) {
                map.put("vehicleId", vehicleId);
                System.out.println("map vehicleId = " + map);
                tripCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripCodeCountForBpcl", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCodeCountForBpcl Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripCodeCountForBpcl", sqlException);
        }
        return tripCount;

    }

    public ArrayList getTripWrongDataList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList tripWrongDataList = new ArrayList();
        map.put("vehicleNo", tripTO.getVehicleNo());
        String[] temp = null;
        String date = "";
        String time = "";
        temp = tripTO.getTransactionDate().split(" ");
        String[] temp1 = temp[0].split("-");
        date = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
        time = temp[1];
        String transactionDateTime = date + " " + time;
        map.put("transactionDate", transactionDateTime);
        String vehicleId = "";
        int tripCount = 0;
        System.out.println("map = " + map);

        try {
            vehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleIdForBpcl", map);
            if (vehicleId != null && !"".equals(vehicleId)) {
                map.put("vehicleId", vehicleId);
                System.out.println("map vehicleId = " + map);
                tripWrongDataList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripWrongDataList", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripWrongDataList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripWrongDataList", sqlException);
        }
        return tripWrongDataList;
    }

    public ArrayList getBPCLTransactionHistory(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList bpclTransactionHistory = new ArrayList();
        ArrayList bpclTransactionHistoryStartDate = new ArrayList();
        ArrayList bpclTransactionHistoryEndDate = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("tripId", tripTO.getTripId());
        map.put("vehicleId", tripTO.getVehicleId());
        System.out.println("map = " + map);
        TripTO tripTO1 = new TripTO();
        TripTO tripTO2 = new TripTO();
        String startDate = "";
        String currentTripStartDate = "";
        String endDate = "";
        try {
            bpclTransactionHistoryStartDate = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getBPCLTransactionHistoryDate", map);
            Iterator itr = bpclTransactionHistoryStartDate.iterator();
            if (itr.hasNext()) {
                tripTO1 = new TripTO();
                tripTO1 = (TripTO) itr.next();
                startDate = tripTO1.getStartDate();
                currentTripStartDate = tripTO1.getCurrentTripStartDate();
                endDate = tripTO1.getEndDate();
            }
//            bpclTransactionHistoryStartDate = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getBPCLTransactionHistoryStartDate", map);
//            Iterator itr = bpclTransactionHistoryStartDate.iterator();
//
//            if(itr.hasNext()){
//                tripTO1 = new TripTO();
//                tripTO1 = (TripTO) itr.next();
//                startDate = tripTO1.getStartDate();
//            }
//            bpclTransactionHistoryEndDate = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getBPCLTransactionHistoryEndDate", map);
//            Iterator itr1 = bpclTransactionHistoryEndDate.iterator();
//            if(itr1.hasNext()){
//                tripTO2 = new TripTO();
//                tripTO2 = (TripTO) itr1.next();
//                endDate = tripTO2.getEndDate();
//            }
            map.put("startDate", startDate);
            map.put("currentTripStartDate", currentTripStartDate);
            map.put("endDate", endDate);
            System.out.println("map = " + map);
            bpclTransactionHistory = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getBPCLTransactionHistory", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBPCLTransactionHistory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getBPCLTransactionHistory", sqlException);
        }
        return bpclTransactionHistory;
    }

    public ArrayList getConsignmentListForUpdate(String consignmentOrderId) {
        Map map = new HashMap();
        map.put("consignmentOrderId", consignmentOrderId);

        ArrayList consignmentList = new ArrayList();
        try {
            consignmentList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentListForUpdate", map);
            System.out.println("consignmentList size=" + consignmentList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return consignmentList;
    }

    public int saveTripRoutePlanForEmptyTrip(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            String consignmentId = tripTO.getConsignmentId();
            String originId = tripTO.getOriginId();
            String destinationId = tripTO.getDestinationId();
            String origin = tripTO.getOrigin();
            String destination = tripTO.getDestination();
            String plannedDate = tripTO.getPreStartLocationPlanDate();
            String plannedHour = tripTO.getPreStartLocationPlanTimeHrs();
            String plannedMinutes = tripTO.getPreStartLocationPlanTimeMins();
            String routeId = "0";
            map.put("orderId", consignmentId);
            map.put("pointId", originId);
            map.put("pointType", "Start Point");
            map.put("pointOrder", "1");
            map.put("pointAddresss", origin);
            map.put("pointPlanDate", plannedDate);
            map.put("pointPlanTime", plannedHour + ":" + plannedMinutes);
            map.put("routeId", routeId);
            System.out.println("map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripRoutePlan", map);
            System.out.println("saveTripRoutePlan=" + status);
            routeId = tripTO.getRouteId();
            map.put("orderId", consignmentId);
            map.put("pointId", destinationId);
            map.put("pointType", "End Point");
            map.put("pointOrder", "2");
            map.put("pointAddresss", destination);
            map.put("pointPlanDate", "00-00-0000");
            map.put("pointPlanTime", "00" + ":" + "00");
            map.put("routeId", routeId);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripRoutePlan", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripRoutePlan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripRoutePlan", sqlException);
        }
        return status;
    }

    public ArrayList getTripAdvanceDetailsStatus(String tripSheetId) {
        Map map = new HashMap();
        ArrayList tripAdvanceDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripSheetId);
            System.out.println("closed Trip map is::" + map);
            tripAdvanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripAdvanceDetailsStatus", map);
            if (tripAdvanceDetails.size() > 0) {
                tripAdvanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripAdvanceDetailsStatusOld", map);
            }
            System.out.println("getTripAdvanceDetailsStatus.size() = " + tripAdvanceDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripAdvanceDetailsStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripAdvanceDetailsStatus List", sqlException);
        }
        return tripAdvanceDetails;
    }

    public int saveWFUTripSheet(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleactreportdate", tripTO.getVehicleactreportdate());
        map.put("wfuRemarks", tripTO.getWfuRemarks());
        map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");

        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveWFUTripSheet", map);
            System.out.println("updateEndTripSheet size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateEndTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateEndTripSheet List", sqlException);
        }

        return status;
    }

    public ArrayList getWfuDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList wfuDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            wfuDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getWfuDetails", map);
            System.out.println("getWfuDetails.size() = " + wfuDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWfuDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWfuDetails List", sqlException);
        }
        return wfuDetails;
    }

    public ArrayList getEmptyTripMergingList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList emptyTripList = new ArrayList();
        map.put("customerId", tripTO.getCustomerId());
        map.put("tripCode", tripTO.getTripCode());
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        if (tripTO.getVehicleId().equals("")) {
            map.put("vehicleId", "0");
        } else {
            map.put("vehicleId", tripTO.getVehicleId());
        }
        map.put("tripId", tripTO.getTripId());
        System.out.println("map for merging=" + map);
        System.out.println("tripId=" + tripTO.getTripId() + "");
        try {
            if (tripTO.getTripId() != null && !"".equals(tripTO.getTripId())) {
                String[] tripIdNos = tripTO.getTripId().split(",");
                List tripIds = new ArrayList(tripIdNos.length);
                for (int i = 0; i < tripIdNos.length; i++) {
                    System.out.println("value:" + tripIdNos[i]);
                    tripIds.add(tripIdNos[i]);
                }
                map.put("tripIds", tripIds);
                emptyTripList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmptyTripMergingList", map);
            } else if (tripTO.getTripId() == null) {
                System.out.println("map = " + map);
                emptyTripList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmptyTripMergingLists", map);
            } else if ("".equals(tripTO.getTripId())) {
                System.out.println("map = " + map);
                emptyTripList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmptyTripMergingLists", map);
            }
            System.out.println("getEmptyTripMergingList.size() = " + emptyTripList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmptyTripMergingList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEmptyTripMergingList List", sqlException);
        }
        return emptyTripList;
    }

    public int insertEmptyTripMerging(String[] tripId, String[] tripSequence, String tripMergingRemarks, int userId) {
        Map map = new HashMap();
        int mergeId = 0;
        int mergeCount = 0;
        try {
            map.put("userId", userId);
            map.put("tripMergingRemarks", tripMergingRemarks);
            mergeId = (Integer) getSqlMapClientTemplate().insert("trip.saveTripMergeMaster", map);
            if (mergeId > 0) {
                map.put("mergeId", mergeId);
                for (int i = 0; i < tripId.length; i++) {
                    map.put("tripId", tripId[i]);
                    if ("".equals(tripSequence[i])) {
                        map.put("tripSequence", 0);
                    } else {
                        map.put("tripSequence", tripSequence[i]);
                    }
                    mergeCount += (Integer) getSqlMapClientTemplate().update("trip.saveTripMergeDetails", map);
                }
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripMergeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripMergeDetails", sqlException);
        }
        return mergeId;
    }

    public String getTripCode(int tripId) {
        Map map = new HashMap();
        TripTO tripTO = null;
        map.put("tripId", tripId);
        String tripCode = "";
        System.out.println("map = " + map);
        try {
            tripCode = (String) getSqlMapClientTemplate().queryForObject("trip.getTripCode", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCode Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripCode", sqlException);
        }
        return tripCode;

    }

    public int saveEmptyTripApproval(String tripId, String approvalStatus, String userId, String mailId) {
        Map map = new HashMap();
        int status = 0;
        map.put("tripSheetId", tripId);
        map.put("approvalStatus", approvalStatus);
        map.put("userId", userId);
        map.put("mailId", mailId);
        System.out.println("the saveEmptyTripApproval" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveEmptyTripApproval", map);
            System.out.println("saveEmptyTripApproval size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveEmptyTripApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveEmptyTripApproval List", sqlException);
        }

        return status;
    }

    public int saveTripRoutePlanForSecondaryTrip(TripTO tripTO, ArrayList orderPointDetails) {
        Map map = new HashMap();
        int status = 0;
        try {
            SecondaryOperationTO operationTO = null;
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            String consignmentId = tripTO.getConsignmentId();
            String originId = tripTO.getOriginId();
            String destinationId = tripTO.getDestinationId();
            String origin = tripTO.getOrigin();
            String destination = tripTO.getDestination();
            String plannedDate = tripTO.getPreStartLocationPlanDate();
            String plannedHour = tripTO.getPreStartLocationPlanTimeHrs();
            String plannedMinutes = tripTO.getPreStartLocationPlanTimeMins();
            String routeId = "0";
            map.put("orderId", consignmentId);
            Iterator itr = orderPointDetails.iterator();
            while (itr.hasNext()) {
                operationTO = new SecondaryOperationTO();
                operationTO = (SecondaryOperationTO) itr.next();
                map.put("pointId", operationTO.getPointId());
                map.put("pointType", operationTO.getPointType());
                map.put("pointOrder", operationTO.getPointSequence());
                map.put("pointAddresss", operationTO.getPointAddresss());
                map.put("pointPlanDate", tripTO.getPreStartLocationPlanDate());
                map.put("pointPlanTime", "00:00");
                map.put("routeId", 0);
                System.out.println("map value is dsdssdsdsdsdsdsdsdsdsdsds:" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.saveTripRoutePlan", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripRoutePlan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripRoutePlan", sqlException);
        }
        return status;
    }

    public int checkeEmptyTripApproval(String tripId, String approvalStatus, String userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("tripSheetId", tripId);
        map.put("approvalStatus", approvalStatus);
        map.put("userId", userId);
        System.out.println("the checkeEmptyTripApproval" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkeEmptyTripApproval", map);
            System.out.println("saveEmptyTripApproval size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkeEmptyTripApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkeEmptyTripApproval List", sqlException);
        }

        return status;
    }

    public ArrayList getTempLogDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList viewTempLogDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            viewTempLogDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTempLogDetails", map);
            System.out.println("viewTempLogDetails.size() = " + viewTempLogDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmptyTripMergingList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEmptyTripMergingList List", sqlException);
        }
        return viewTempLogDetails;
    }

    public int saveTempLogDetails(String actualFilePath, String fileSaved, TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        FileInputStream fis = null;
        int tripId = 0;
        int cityId = 0;
        try {
            if (tripTO.getTripId() != null && tripTO.getTripId() != "") {
                tripId = Integer.parseInt(tripTO.getTripId());
                map.put("tripSheetId", tripId);
            }

            map.put("fileName", fileSaved);
            map.put("userId", userId);

            File file = new File(actualFilePath);
            fis = new FileInputStream(file);
            byte[] tempFile = new byte[(int) file.length()];
            fis.read(tempFile);
            fis.close();
            map.put("tempFile", tempFile);
            System.out.println("the saveTripTempLogDetails123455" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTempLogDetails", map);
            System.out.println("saveTripTempLogDetails123455 size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripPodDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripPodDetails List", sqlException);
        }

        return status;
    }

    public int saveTemperatureLogApproval(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("tripSheetId", tripTO.getTripId());
        map.put("approvalStatus", tripTO.getApprovalStatus());
        map.put("userId", userId);
        System.out.println("the checkeEmptyTripApproval" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTemperatureLogApproval", map);
            System.out.println("saveTemperatureLogApproval size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTemperatureLogApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkeEmptyTripApproval List", sqlException);
        }

        return status;
    }

    public int checkTemperatureLogApproval(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("tripSheetId", tripTO.getTripId());
        map.put("approvalStatus", tripTO.getApprovalStatus());
        map.put("userId", userId);
        System.out.println("the checkeTemperatureLogApproval" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkTemperatureLogApproval", map);
            System.out.println("checkeTemperatureLogApproval size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkeTemperatureLogApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkeEmptyTripApproval List", sqlException);
        }

        return status;
    }

    public String checkConsignmentCreditLimitStatus(TripTO tripTO, int userId) {
        Map map = new HashMap();
        String status = "";
        map.put("consignmentId", tripTO.getConsignmentId());
        map.put("approvalStatus", tripTO.getApprovalStatus());
        map.put("mailId", tripTO.getMailId());
        System.out.println("the checkeTemperatureLogApproval" + map);
        try {
            status = (String) getSqlMapClientTemplate().queryForObject("trip.checkConsignmentCreditLimitStatus", map);
            System.out.println("ConsignmentCreditLimitStatus size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ConsignmentCreditLimitStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ConsignmentCreditLimitStatus List", sqlException);
        }

        return status;
    }

    public int updateConsignmentOrderCreditLimitStatus(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("consignmentOrderId", tripTO.getConsignmentId());
        map.put("approvalStatus", tripTO.getApprovalStatus());
        map.put("mailId", tripTO.getMailId());
        System.out.println("the updateConsignmentOrderCreditLimitStatus" + map);
        try {
            if (tripTO.getApprovalStatus().equals("1")) {
                status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentOrderCreditLimitApprovalStatus", map);
            } else if (tripTO.getApprovalStatus().equals("2")) {
                status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentOrderCreditLimitCancelStatus", map);
            }
            System.out.println("updateConsignmentOrderCreditLimitStatus size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateConsignmentOrderCreditLimitStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateConsignmentOrderCreditLimitStatus", sqlException);
        }

        return status;
    }

    public int updateConsignmentOrderApprovalStatus(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("consignmentOrderId", tripTO.getConsignmentId());
        map.put("approvalStatus", tripTO.getApprovalStatus());
        map.put("mailId", tripTO.getMailId());
        map.put("statusId", tripTO.getStatusId());
        map.put("approvalStatusId", tripTO.getApprovalStatusId());
        map.put("userId", userId);
        System.out.println("the updateConsignmentOrderApprovalStatus" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentOrderApprovalStatus", map);
            map.put("updateType", "System Update");
            map.put("remarks", "Credit Limit Update Status");
            status = (Integer) getSqlMapClientTemplate().update("trip.insertConsignmentStatus", map);
            System.out.println("updateConsignmentOrderApprovalStatus size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateConsignmentOrderApprovalStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateConsignmentOrderApprovalStatus List", sqlException);
        }

        return status;
    }

    public int insertLoadingUnloadingDetails(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            System.out.println("DAO insertLoadingUnloadingDetails ");
            SecondaryOperationTO operationTO = null;
            map.put("userId", userId);
            map.put("vehicleactreportdate", tripTO.getVehicleactreportdate());
            map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");
            map.put("vehicleloadreportdate", tripTO.getVehicleloadreportdate());
            map.put("vehicleloadreporttime", tripTO.getVehicleloadreporthour() + ":" + tripTO.getVehicleloadreportmin() + ":00");
            map.put("vehicleloadtemperature", tripTO.getVehicleloadtemperature());
            map.put("tripRouteCourseId", tripTO.getRouteId());
            System.out.println("trip Route Course :  " + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveLoadingUnloadingDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripRoutePlan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripRoutePlan", sqlException);
        }
        return status;
    }

    public int insertLoadingUnloadingDetailsClosure(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            System.out.println("DAO12345");
            SecondaryOperationTO operationTO = null;
            map.put("userId", userId);
            map.put("vehicleactreportdate", tripTO.getVehicleactreportdate());
//            map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + "00");
            map.put("vehicleloadreportdate", tripTO.getVehicleloadreportdate());
            map.put("vehicleloadreporttime", tripTO.getVehicleloadreporthour() + ":" + tripTO.getVehicleloadreportmin() + ":00");
            map.put("vehicleloadtemperature", tripTO.getVehicleloadtemperature());
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println("map value is00 " + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveLoadingUnloadingDetailsClosure", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripRoutePlan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripRoutePlan", sqlException);
        }
        return status;
    }

    public ArrayList getStatusList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList statuList = new ArrayList();
        try {
            statuList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getStatusList", map);
            System.out.println("getStatusList.size() = " + statuList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStatusList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getStatusList List", sqlException);
        }
        return statuList;
    }

    public ArrayList getTripVehicleNo(TripTO tripTO) {
        Map map = new HashMap();
        map.put("statusId", tripTO.getStatusId());
        map.put("roleId", tripTO.getRoleId());
        map.put("companyId", tripTO.getCompanyId());
        if ("1".equals(tripTO.getTripType())) {
            map.put("usageTypeId", 2);
        } else {
            map.put("usageTypeId", 1);
        }
        if ("2".equals(tripTO.getTripType())) {
            map.put("customerName", tripTO.getCustomerName());
        }
        ArrayList vehicleNos = new ArrayList();
        try {
            System.out.println("map:" + map);
            vehicleNos = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripVehicleNo", map);
            System.out.println("vehicleNos size=" + vehicleNos.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return vehicleNos;
    }

    public ArrayList getConsignmentPaymentDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList consignmentPaymentList = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripId());
            map.put("paymentType", tripTO.getPaymentType());
            System.out.println(" Trip map is::" + map);
            consignmentPaymentList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentPaymentDetails", map);
            System.out.println("consignmentPaymentList.size() = " + consignmentPaymentList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentPaymentDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignmentPaymentDetails List", sqlException);
        }
        return consignmentPaymentList;
    }

    public ArrayList getEmailList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList getEmailList = new ArrayList();
        try {
            map.put("mailId", tripTO.getEmailId());
            System.out.println(" Trip map is::" + map);
            getEmailList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmailList", map);
            System.out.println("getEmailList.size() = " + getEmailList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmailList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEmailList List", sqlException);
        }
        return getEmailList;
    }

    public int saveVehicleDriverAdvanceApproval(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        map.put("vehicleDriverAdvanceId", tripTO.getVehicleDriverAdvanceId());
        map.put("approvalStatus", tripTO.getApprovalStatus());
        map.put("userId", tripTO.getUserId());
        map.put("mailId", tripTO.getMailId());
        System.out.println("the saveVehicleDriverAdvanceApproval" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveVehicleDriverAdvanceApproval", map);
            System.out.println("saveVehicleDriverAdvanceApproval size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveVehicleDriverAdvanceApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveVehicleDriverAdvanceApproval List", sqlException);
        }

        return status;
    }

    public int checkVehicleDriverAdvanceApproval(String vehicleDriverAdvanceId, String approvalStatus, String userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("vehicleDriverAdvanceId", vehicleDriverAdvanceId);
        map.put("approvalStatus", approvalStatus);
        map.put("userId", userId);
        System.out.println("the checkeEmptyTripApproval" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkVehicleDriverAdvanceApproval", map);
            System.out.println("saveEmptyTripApproval size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate ttro the calling class
             */
            FPLogUtils.fpDebugLog("checkeEmptyTripApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkeEmptyTripApproval List", sqlException);
        }

        return status;
    }

    public ArrayList getVehicleAdvanceRequest(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList wfuDetails = new ArrayList();
        try {
            map.put("fromdate", tripTO.getFromDate());
            map.put("todate", tripTO.getToDate());
            map.put("vehicleAdvanceId", tripTO.getVehicleDriverAdvanceId());
            System.out.println(" Trip map is::" + map);
            wfuDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleAdvanceRequest", map);
            System.out.println("getVehicleAdvanceRequest.size() = " + wfuDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleAdvanceRequest Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleAdvanceRequest List", sqlException);
        }
        return wfuDetails;
    }

    public ArrayList getViewVehicleDriverAdvanceList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList advanceDetails = new ArrayList();
        try {
            map.put("vehicleId", tripTO.getVehicleId());
            System.out.println(" Trip map is::" + map);
            String tripId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleTripId", map);
            if (tripId != null) {
                map.put("tripId", tripId);
                System.out.println(" Trip map is::" + map);
                advanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleAdvanceDetailsList", map);
                System.out.println("getVehicleAdvanceDetailsList.size() = " + advanceDetails.size());
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getViewVehicleDriverAdvanceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getViewVehicleDriverAdvanceList List", sqlException);
        }
        return advanceDetails;
    }

    public int saveVehicleDriverAdvancePay(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        map.put("vehicleDriverAdvanceId", tripTO.getVehicleDriverAdvanceId());
        map.put("paidAdvance", tripTO.getPaidAdvance());
        map.put("paidStatus", tripTO.getPaidStatus());
        map.put("userId", tripTO.getUserId());
        System.out.println("the saveVehicleDriverAdvancePay" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveVehicleDriverAdvancePay", map);
            System.out.println("saveVehicleDriverAdvancePay size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveVehicleDriverAdvancePay Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveVehicleDriverAdvancePay List", sqlException);
        }

        return status;
    }

    public String getLastBpclTransactionDate() {
        Map map = new HashMap();
        String lastBpclTxDate = "";
        System.out.println("map = " + map);
        try {
            lastBpclTxDate = (String) getSqlMapClientTemplate().queryForObject("trip.getLastBpclTransactionDate", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLastBpclTransactionDate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getLastBpclTransactionDate", sqlException);
        }
        return lastBpclTxDate;

    }

    public ArrayList getApprovalValueDetails(double actualval) {
        ArrayList getapprovalValueDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("actualval", actualval);
            System.out.println("map:" + map);
            getapprovalValueDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getApprovalValueDetails", map);
            System.out.println("getapprovalValueDetails size=" + getapprovalValueDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getApprovalValueDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getApprovalValueDetails List", sqlException);
        }

        return getapprovalValueDetails;
    }

    public ArrayList getRepairMaintenence(int configId) {
        ArrayList getapprovalValueDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("configId", configId);
            System.out.println("map:" + map);
            getapprovalValueDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getRepairMaintenence", map);
            System.out.println("getapprovalValueDetails size=" + getapprovalValueDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRepairMaintenence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRepairMaintenence List", sqlException);
        }

        return getapprovalValueDetails;
    }

    public String getFCLeadMailId(String vehicleId) {
        Map map = new HashMap();
        String fcLeadMailId = "";
        map.put("vehicleId", vehicleId);
        System.out.println("map = " + map);
        try {
            fcLeadMailId = (String) getSqlMapClientTemplate().queryForObject("trip.getFCLeadMailId", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFCLeadMailId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getFCLeadMailId", sqlException);
        }
        return fcLeadMailId;

    }

    public String getSecondaryFCLeadMailId(String vehicleId) {
        Map map = new HashMap();
        String fcLeadMailId = "";
        map.put("vehicleId", vehicleId);
        System.out.println("map = " + map);
        try {
            fcLeadMailId = (String) getSqlMapClientTemplate().queryForObject("trip.getSecondaryFCLeadMailId", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryFCLeadMailId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getSecondaryFCLeadMailId", sqlException);
        }
        return fcLeadMailId;

    }

    public String getSecondaryApprovalPerson(String vehicleId) {
        Map map = new HashMap();
        String approvalPersonMailId = "";
        map.put("vehicleId", vehicleId);
        System.out.println("map = " + map);
        try {
            approvalPersonMailId = (String) getSqlMapClientTemplate().queryForObject("trip.getSecondaryApprovalPerson", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFCLeadMailId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getFCLeadMailId", sqlException);
        }
        return approvalPersonMailId;

    }

    public String getPreviousTripsOdometerReading(String tripSheetId) {
        Map map = new HashMap();
        String previousTripsOdometerReading = "";
        String tripVehicleId = "";
        String previousTripDetails = "";
        String previousTripEndKm = null;
        String previousTripEndHm = null;
        String previousTripType = "";
        String reeferRequired = "";
        String[] temp = null;
        map.put("tripSheetId", tripSheetId);
        System.out.println("map = " + map);
        String tripEndDetails = "";
        try {
            tripVehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleId", map);
            System.out.println("tripVehicleId = " + tripVehicleId);
            if (tripVehicleId != null) {
                map.put("tripVehicleId", tripVehicleId);
                previousTripDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getPreviousTripEndKm", map);
                System.out.println("previousTripDetails = " + previousTripDetails);
                if (previousTripDetails != null) {
                    temp = previousTripDetails.split("~");
                    previousTripEndKm = temp[0];
                    previousTripType = temp[2];
                    reeferRequired = temp[3];
                    if (previousTripType.equals("0") && reeferRequired.equals("Yes")) {
                        previousTripEndHm = temp[1];
                    } else {
                        previousTripEndHm = (String) getSqlMapClientTemplate().queryForObject("trip.getPreviousTripEndHm", map);
                        System.out.println("previousTripEndHm = " + previousTripEndHm);
                    }
                }
                if (previousTripEndKm != null && previousTripEndHm != null) {
                    tripEndDetails = previousTripEndKm + "-" + previousTripEndHm;
                } else {
                    tripEndDetails = 0 + "-" + 0;
                }
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPreviousTripsOdometerReading Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getPreviousTripsOdometerReading", sqlException);
        }
        return tripEndDetails;

    }

    public int insertBillingGraph(String tripSheetId, String graphPath, int userId) throws FileNotFoundException, IOException {
        Map map = new HashMap();
        String[] temp = null;
        map.put("tripSheetId", tripSheetId);
        System.out.println("map = " + map);
        int insertStatus = 0;
        int updateStatus = 0;
        FileInputStream fis = null;
        File file = new File(graphPath);
        System.out.println("file = " + file);
        fis = new FileInputStream(file);
        System.out.println("fis = " + fis);
        byte[] graphFile = new byte[(int) file.length()];
        System.out.println("podFile = " + graphFile);
        fis.read(graphFile);
        fis.close();
        map.put("graphFile", graphFile);
        map.put("fileName", "dataLogExcel" + tripSheetId + ".xls");
        map.put("userId", userId);
        try {
            String status = (String) getSqlMapClientTemplate().queryForObject("trip.checkGraphStatus", map);
            if (status == null) {
                insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertBillingGraph", map);
            } else {
                updateStatus = (Integer) getSqlMapClientTemplate().update("trip.updateStatusBillingGraph", map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertBillingGraph", map);

            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertBillingGraph Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertBillingGraph", sqlException);
        }
        return insertStatus;

    }

    public String getTripCount(TripTO tripTO) {
        Map map = new HashMap();
        String tripVehicleId = "";
        String tripCount = "";
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("tripStartDate", tripTO.getTripScheduleDate());
        map.put("tripEndDate", tripTO.getTripEndDate());
        System.out.println("map = " + map);
        try {
            tripVehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getSecondaryTripVehicleId", map);
            if (tripVehicleId != null) {
                map.put("tripVehicleId", tripVehicleId);
                tripCount = (String) getSqlMapClientTemplate().queryForObject("trip.getSecondaryTripCount", map);

            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPreviousTripsOdometerReading Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getPreviousTripsOdometerReading", sqlException);
        }
        return tripCount;

    }

    public int updateInvoiceAmount(TripTO tripTO, int userId) throws FileNotFoundException, IOException {
        Map map = new HashMap();
        int updateStatus = 0;
        map.put("editMode", tripTO.getEditMode());
        map.put("invoiceId", tripTO.getInvoiceId());
        map.put("userId", userId);
        try {

            map.put("billedAmount", tripTO.getBilledAmount());
            if (tripTO.getWeight() != null && !"".equals(tripTO.getWeight())) {
                map.put("weight", tripTO.getWeight());
            } else {
                map.put("weight", 0);
            }
            if (tripTO.getRatePerKg() != null && !"".equals(tripTO.getRatePerKg())) {
                map.put("ratePerKg", tripTO.getRatePerKg());
            } else {
                map.put("ratePerKg", 0);
            }
            if (tripTO.getDiscountAmount() != null && !"".equals(tripTO.getDiscountAmount())) {
                map.put("discountAmount", tripTO.getDiscountAmount());
            } else {
                map.put("discountAmount", 0);
            }
            updateStatus = (Integer) getSqlMapClientTemplate().update("trip.updateInvoiceAmountHeader", map);
            updateStatus = (Integer) getSqlMapClientTemplate().update("trip.updateInvoiceAmountDetail", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertBillingGraph Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertBillingGraph", sqlException);
        }
        return updateStatus;

    }

    public int insertCourierDetails(TripTO tripTO, int userId) throws FileNotFoundException, IOException {
        Map map = new HashMap();
        int insertCourierDetails = 0;
        map.put("invoiceId", tripTO.getInvoiceId());
        map.put("courierNo", tripTO.getCourierNo());
        map.put("courierRemarks", tripTO.getCourierRemarks());
        map.put("userId", userId);
        try {
            insertCourierDetails = (Integer) getSqlMapClientTemplate().update("trip.insertCourierDetails", map);
            if (insertCourierDetails > 0) {
                insertCourierDetails = (Integer) getSqlMapClientTemplate().update("trip.updateCourierDetails", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertCourierDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertCourierDetails", sqlException);
        }
        return insertCourierDetails;

    }

    public ArrayList getCourierDetails(TripTO tripTO) {
        ArrayList invoiceDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("invoiceId", tripTO.getInvoiceId());
            System.out.println("map:" + map);
            invoiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCourierDetails", map);
            System.out.println("getCourierDetails size=" + invoiceDetails.size());
        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCourierDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCourierDetails List", sqlException);
        }

        return invoiceDetails;
    }

    public int checkTempGraphApprovedStatus(TripTO tripTO, int userId) throws FileNotFoundException, IOException {
        Map map = new HashMap();
        int checkTempGraphApprovedStatus = 0;
        map.put("tripId", tripTO.getTripId());
        map.put("userId", userId);
        try {
            checkTempGraphApprovedStatus = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkTempGraphApprovedStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkTempGraphApprovedStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkTempGraphApprovedStatus", sqlException);
        }
        return checkTempGraphApprovedStatus;

    }

    public int checkBillSubmittedStatus(TripTO tripTO, int userId) throws FileNotFoundException, IOException {
        Map map = new HashMap();
        int checkBillSubmittedStatus = 0;
        map.put("tripId", tripTO.getTripId());
        map.put("invoiceId", tripTO.getInvoiceId());
        map.put("userId", userId);
        try {
            checkBillSubmittedStatus = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkBillSubmittedStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkBillSubmittedStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkBillSubmittedStatus", sqlException);
        }
        return checkBillSubmittedStatus;

    }

    public ArrayList getVehicleLogDetails(TripTO tripTO) {
        ArrayList vehicleLogDetails = new ArrayList();
        ArrayList vehicleLogDetailss = new ArrayList();
        Map map = new HashMap();
        TripTO tripTO1 = new TripTO();
        TripTO tripTO2 = new TripTO();
        try {
            String tripId = tripTO.getTripId();
            System.out.println("map:" + map);
            map.put("tripId", tripId);
            String startDate = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleLogDate", map);
            System.out.println("tripIdtripIdtripId:" + tripId);
            System.out.println("startDatestartDate:" + startDate);
            int transitHours = Integer.parseInt(tripTO.getTripTransitHours());
            System.out.println("transitHourstransitHours:" + transitHours);
            int totalHrs = Integer.parseInt(tripTO.getTotalHrs());
            System.out.println("totalHrstotalHrs:" + totalHrs);
            int j = 0;
            int k = transitHours / totalHrs;
            for (int i = 0; i < k; i++) {
                tripTO2 = new TripTO();
                j = j + k;
                map.put("time", j);
                map.put("startDate", startDate);
                map.put("tripId", tripId);
                System.out.println("map:" + map);

                vehicleLogDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleLogDetails", map);
                System.out.println("tripTO1.vehicleLogDetails()" + vehicleLogDetails.size());
                if (vehicleLogDetails.size() > 0) {
                    Iterator itr4 = vehicleLogDetails.iterator();
                    while (itr4.hasNext()) {
                        tripTO1 = (TripTO) itr4.next();
                        tripTO2.setLogDateTime(tripTO1.getLogDateTime());
                        System.out.println("tripTO1.getLogDateTime()" + tripTO.getLogDateTime());
                        tripTO2.setLogDate(tripTO1.getLogDate());
                        tripTO2.setLogTime(tripTO1.getLogTime());
                        System.out.println("tripTO1.getLogTime()" + tripTO.getLogTime());
                        tripTO2.setLocation(tripTO1.getLocation());
                        tripTO2.setTemperature(tripTO1.getTemperature());
                        tripTO2.setRegNo(tripTO1.getRegNo());
                        tripTO2.setDistance(tripTO1.getDistance());
                        System.out.println("tripTO1.getDistance()" + tripTO.getDistance());
                    }

                    vehicleLogDetailss.add(tripTO2);
                }
            }

            System.out.println("vehicleLogDetailss size=" + vehicleLogDetailss.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleLogDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleLogDetails List", sqlException);
        }

        return vehicleLogDetailss;
    }

    public ArrayList getEmployeeTripDetails(TripTO tripTO) {
        ArrayList invoiceDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("tripId", tripTO.getTripSheetId());
            map.put("employeeId", tripTO.getEmployeeId());
            System.out.println("map:" + map);
            invoiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmployeeTripDetails", map);
            System.out.println("getEmployeeTripDetails size=" + invoiceDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmployeeTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEmployeeTripDetails List", sqlException);
        }

        return invoiceDetails;
    }

    public int updateEmployeeInTrip(TripTO tripTO, int userId) throws FileNotFoundException, IOException {
        Map map = new HashMap();
        int status = 0;
        map.put("startDate", tripTO.getStartDate());
        map.put("endDate", tripTO.getEndDate());
        String driverChangeHour = "00";
        String driverChangeMinute = "00";
        if (tripTO.getDriverChangeHour() != null && !"".equals(tripTO.getDriverChangeHour())) {
            driverChangeHour = "00";
        } else {
            driverChangeHour = tripTO.getDriverChangeHour();
        }
        if (tripTO.getDriverChangeMinute() != null && !"".equals(tripTO.getDriverChangeMinute())) {
            driverChangeMinute = "00";
        } else {
            driverChangeMinute = tripTO.getDriverChangeMinute();
        }
        map.put("endTime", driverChangeHour + ":" + driverChangeMinute + ":00");
        map.put("startOdometerReading", tripTO.getStartOdometerReading());
        map.put("endOdometerReading", tripTO.getEndOdometerReading());
        map.put("totalKm", tripTO.getTotalKM());
        map.put("totalDays", tripTO.getTotalDays());
        map.put("balanceAmount", tripTO.getBalanceAmount());
        map.put("returnAmount", tripTO.getReturnAmount());
        map.put("advancePaid", tripTO.getActualAdvancePaid());
        map.put("staffId", tripTO.getEmployeeId());
        map.put("remarks", tripTO.getActionRemarks());
        map.put("activeInd", tripTO.getStatus());
        map.put("tripId", tripTO.getTripSheetId());
        map.put("driverId", tripTO.getPrimaryDriverId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("startHm", tripTO.getStartHM());
        map.put("endHm", tripTO.getEndHM());
        map.put("totalHm", tripTO.getTotalHm());
        map.put("remarks", tripTO.getRemarks());
        map.put("cityId", tripTO.getCityId());
        map.put("driverCount", tripTO.getDriverCount());
        map.put("driverInTrip", tripTO.getDriverInTrip());
        map.put("userId", userId);
        String driverType = "";
        System.out.println("map update driver on a trip = " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateEmployeeInTrip", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.inactiveTripDriver", map);
            driverType = (String) getSqlMapClientTemplate().queryForObject("trip.getDriverType", map);
            if (driverType.equals("P")) {
                map.put("type", "P");
            } else if (driverType.equals("S")) {
                map.put("type", "S");
            } else if (driverType.equals("T")) {
                map.put("type", "S");
            }
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriver", map);
            status = (Integer) getSqlMapClientTemplate().update("employee.saveEmployeeStatus", map);
            System.out.println("updateNewDriverVehicleDriverMapping" + map);
            String vehicleid = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleIdForNewDriver", map);
            if (vehicleid != null) {
                map.put("oldVehicleId", vehicleid);
                map.put("oldDriverId", "0");
                if (driverType.equals("P")) {
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateNewDriverVehicleDriverMappingPrimary", map);
                } else if (driverType.equals("S")) {
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateNewDriverVehicleDriverMappingSecondary", map);
                } else if (driverType.equals("T")) {
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateNewDriverVehicleDriverMappingTeritory", map);
                }
                System.out.println("updateOldDriverVehicleDriverMapping" + map);
//                  status = (Integer) getSqlMapClientTemplate().update("trip.updateOldDriverVehicleDriverMapping", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertCourierDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertCourierDetails", sqlException);
        }
        return status;

    }

    public String checkEmployeeInTrip(String driverId) {
        Map map = new HashMap();
        TripTO tripTO = null;
        map.put("staffId", driverId);
        String checkEmployeeInTrip = "";
        System.out.println("map = " + map);
        try {
            checkEmployeeInTrip = (String) getSqlMapClientTemplate().queryForObject("employee.checkDriverStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkEmployeeInTrip Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkEmployeeInTrip", sqlException);
        }
        return checkEmployeeInTrip;

    }

    public int insertMailDetails(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int insertMailDetails = 0;
        map.put("mailTypeId", tripTO.getMailTypeId());
        map.put("mailSubjectTo", tripTO.getMailSubjectTo());
        map.put("mailSubjectCc", tripTO.getMailSubjectCc());
        map.put("mailSubjectBcc", tripTO.getMailSubjectBcc());
        map.put("mailContentTo", tripTO.getMailContentTo());
        map.put("mailContentCc", tripTO.getMailContentCc());
        map.put("mailContentBcc", tripTO.getMailContentBcc());
        map.put("mailTo", tripTO.getMailIdTo());
        map.put("mailCc", tripTO.getMailIdCc());
        map.put("mailBcc", tripTO.getMailIdBcc());
        map.put("userId", userId);
        System.out.println("map = " + map);
        try {
            insertMailDetails = (Integer) getSqlMapClientTemplate().insert("trip.insertMailDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertMailDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertMailDetails", sqlException);
        }
        return insertMailDetails;

    }

    public ArrayList getMailNotDeliveredList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList mailNotDeliveredList = new ArrayList();
        map.put("mailSendingId", tripTO.getMailSendingId());
        map.put("mailDeliveredStatusId", tripTO.getMailDeliveredStatusId());
        try {
            System.out.println("closed Trip map is::" + map);
            mailNotDeliveredList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getMailNotDeliveredList", map);
            System.out.println("getMailNotDeliveredList.size() = " + mailNotDeliveredList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMailNotDeliveredList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getMailNotDeliveredList List", sqlException);
        }
        return mailNotDeliveredList;
    }

    public int updateTripOtherExpenseDoneStatus(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int updateOtherExpenseDoneStatus = 0;
        map.put("tripId", tripTO.getTripSheetId());
        map.put("userId", userId);
        System.out.println("map = " + map);
        try {
            updateOtherExpenseDoneStatus = (Integer) getSqlMapClientTemplate().update("trip.updateOtherExpenseDoneStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateOtherExpenseDoneStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateOtherExpenseDoneStatus", sqlException);
        }
        return updateOtherExpenseDoneStatus;

    }

    public String getOtherExpenseDoneStatus(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripSheetId());
        String otherExpenseDoneStatus = "";
        System.out.println("map = " + map);
        try {
            otherExpenseDoneStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getOtherExpenseDoneStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOtherExpenseDoneStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getOtherExpenseDoneStatus", sqlException);
        }
        return otherExpenseDoneStatus;

    }

    public String getLastUploadCustomerOutstandingDate() {
        Map map = new HashMap();
        String lastCustomerOutstandingDate = "";
        System.out.println("map = " + map);
        try {
            lastCustomerOutstandingDate = (String) getSqlMapClientTemplate().queryForObject("trip.getLastUploadCustomerOutstandingDate", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLastUploadCustomerOutstandingDate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getLastUploadCustomerOutstandingDate", sqlException);
        }
        return lastCustomerOutstandingDate;

    }

    public String getPreviousOutStandingAmount(TripTO tripTO) {
        Map map = new HashMap();
        map.put("customerCode", tripTO.getCustomerCode());
        map.put("customerName", tripTO.getCustomerName());
        map.put("userId", tripTO.getUserId());
        System.out.println("map = " + map);
        String previousOutStandingAmount = "";
        int updateCustomerOutStanding = 0;
        try {
            previousOutStandingAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getPreviousOutStandingAmount", map);
            System.out.println("previousOutStandingAmount = " + previousOutStandingAmount);
            if (previousOutStandingAmount == null) {
                updateCustomerOutStanding = (Integer) getSqlMapClientTemplate().update("trip.insertCustomerOutStandingAmount", map);
                System.out.println("updateCustomerOutStanding = " + updateCustomerOutStanding);
                previousOutStandingAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getPreviousOutStandingAmount", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPreviousOutStandingAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getPreviousOutStandingAmount", sqlException);
        }
        return previousOutStandingAmount;

    }

    public int updateCustomerOutStandingAmount(String[] customerId, String[] outStandingAmount, int userId) {
        Map map = new HashMap();
        int updateCustomerOutStandingAmount = 0;
        double creditLimit = 0.0;
        map.put("userId", userId);
        try {
            for (int i = 0; i < customerId.length; i++) {
                map.put("customerId", customerId[i]);
                map.put("amount", outStandingAmount[i]);
                creditLimit = (Double) getSqlMapClientTemplate().queryForObject("trip.getCustomerCreditLimit", map);
                int retVal = Double.compare(creditLimit, Double.parseDouble(outStandingAmount[i]));
                if (retVal >= 0) {
                    map.put("cnoteCount", "0");
                } else if (retVal < 0) {
                    map.put("cnoteCount", "1");
                }
                System.out.println("map = " + map);
                updateCustomerOutStandingAmount += (Integer) getSqlMapClientTemplate().update("trip.updateCustomerOutStandingAmount", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateCustomerOutStandingAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateCustomerOutStandingAmount", sqlException);
        }
        return updateCustomerOutStandingAmount;

    }

    public ArrayList getCustomerOutStandingList() {
        Map map = new HashMap();
        ArrayList customerOutStandingList = new ArrayList();
        try {
            System.out.println("closed Trip map is::" + map);
            customerOutStandingList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCustomerOutStandingList", map);
            System.out.println("customerOutStandingList.size() = " + customerOutStandingList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerOutStandingList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerOutStandingList List", sqlException);
        }
        return customerOutStandingList;
    }

    public ArrayList getSecondaryCustomerApprovalList() {
        Map map = new HashMap();
        ArrayList secondaryCustomerApprovalList = new ArrayList();
        try {
            System.out.println("closed Trip map is::" + map);
            secondaryCustomerApprovalList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getSecondaryCustomerApprovalList", map);
            System.out.println("getSecondaryCustomerApprovalList.size() = " + secondaryCustomerApprovalList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryCustomerApprovalList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryCustomerApprovalList List", sqlException);
        }
        return secondaryCustomerApprovalList;
    }

    public int updateSecondaryCustomerMail(TripTO tripTO) {
        Map map = new HashMap();
        int updateSecondaryCustomerMail = 0;
        try {
            map.put("customerId", tripTO.getCustomerId());
            map.put("emailId", tripTO.getEmailId());
            System.out.println("map = " + map);
            updateSecondaryCustomerMail = (Integer) getSqlMapClientTemplate().update("trip.updateSecondaryCustomerMail", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateSecondaryCustomerMail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateSecondaryCustomerMail", sqlException);
        }
        return updateSecondaryCustomerMail;

    }

    public int updateEmptyTripRoute(TripTO tripTO) {
        Map map = new HashMap();
        int updateEmptyTripRoute = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("originId", tripTO.getOrigin());
            map.put("destinationId", tripTO.getDestination());
            map.put("cityFrom", tripTO.getCityFrom());
            map.put("cityTo", tripTO.getCityTo());
            map.put("routeInfo", tripTO.getCityFrom() + "-" + tripTO.getCityTo());
            map.put("expense", tripTO.getExpense());
            map.put("totalKm", tripTO.getTotalKm());
            map.put("totalHrs", tripTO.getTotalHours());
            map.put("totalMinutes", tripTO.getTotalMinutes());
            map.put("emptyTripRemarks", tripTO.getEmptyTripRemarks());
            map.put("routeId", tripTO.getRouteId());
            //set transit days
            float transitHours = 0.00F;
            if (tripTO.getTotalHours() != null && !"".equals(tripTO.getTotalHours())) {
                transitHours = Float.parseFloat(tripTO.getTotalHours());
            }
            double transitDay = (double) transitHours / 24;
            tripTO.setTripTransitDays(transitDay + "");
            //set advance to be paid per day
            float totalExpense = 0.00f;
            if (tripTO.getExpense() != null && !"".equals(tripTO.getExpense())) {
                totalExpense = Float.parseFloat(tripTO.getExpense());
            }

            int transitDayValue = (int) transitDay;
            if (transitDay > transitDayValue) {
                transitDay = transitDayValue + 1;
            }
            double advnaceToBePaidPerDay = 0;

            if (totalExpense > 0 && transitHours > 0) {
                advnaceToBePaidPerDay = totalExpense / transitDay;
            }
            map.put("transitDay", transitDay);
            map.put("transitHours", transitHours);
            map.put("advnaceToBePaidPerDay", advnaceToBePaidPerDay);

            System.out.println("map = " + map);
            updateEmptyTripRoute = (Integer) getSqlMapClientTemplate().update("trip.updateEmptyTripMaster", map);
            System.out.println("updateEmptyTripRoute = " + updateEmptyTripRoute);
            int updateEmptyTripRouteCourse = 0;
            int updateEmptyTripConsignment = 0;
            if (updateEmptyTripRoute > 0) {
                updateEmptyTripRouteCourse = (Integer) getSqlMapClientTemplate().update("trip.updateEmptyTripRouteCourseOrigin", map);
                System.out.println("updateEmptyTripRouteCourse one= " + updateEmptyTripRouteCourse);
                updateEmptyTripRouteCourse = (Integer) getSqlMapClientTemplate().update("trip.updateEmptyTripRouteCourseDestination", map);
                System.out.println("updateEmptyTripRouteCourse two= " + updateEmptyTripRouteCourse);
                updateEmptyTripConsignment = (Integer) getSqlMapClientTemplate().update("trip.updateEmptyTripConsignment", map);
                System.out.println("updateEmptyTripConsignment two= " + updateEmptyTripConsignment);

            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateEmptyTripRoute Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateEmptyTripRoute", sqlException);
        }
        return updateEmptyTripRoute;

    }

    public String getVehicleUsageTypeId(String vehicleId) {
        Map map = new HashMap();
        map.put("vehicleId", vehicleId);
        System.out.println("map = " + map);
        String usageTypeId = "";
        try {
            usageTypeId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleUsageTypeId", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleUsageTypeId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleUsageTypeId", sqlException);
        }
        return usageTypeId;

    }

    public String getVehicleCurrentStatus(String tripId) {
        Map map = new HashMap();
        map.put("tripId", tripId);
        System.out.println("map = " + map);
        String tripStatusId = "";
        try {
            tripStatusId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleCurrentStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleCurrentStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleCurrentStatus", sqlException);
        }
        return tripStatusId;

    }

    public int updateDeleteTrip(TripTO tripTO) {
        Map map = new HashMap();
        int updatedeletetrip = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            map.put("tripStatusId", tripTO.getStatusId());
            String vehicleCurrentStatus = "";
            System.out.println("map = " + map);
            if (tripTO.getStatusId().equals("Delete")) {
                updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateDeleteTrip", map);
                updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateDeleteTripVehicle", map);
                updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateDeleteTripStatusDetails", map);
                updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateDeleteTripAdvance", map);
            } else if (tripTO.getStatusId().equals("8")) {
                updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateFreezeTrip", map);
                updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateFreezeTripVehicle", map);
                updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateFreezeTripStatusDetails", map);
            } else if (tripTO.getStatusId().equals("10")) {
                vehicleCurrentStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleCurrentStatus", map);
                if (vehicleCurrentStatus == null) {
                    updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateInprogressTripEnd", map);
                    updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateInprogressTripWfu", map);
                    updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateInprogressTripVehicle", map);
                    updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateInprogressTripStatusDetails", map);
                }
            } else if (tripTO.getStatusId().equals("18")) {
                vehicleCurrentStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleCurrentStatus", map);
                if (vehicleCurrentStatus == null) {
                    updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateWfuTrip", map);
                    updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateWfuTripVehicle", map);
                    updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.deleteWfuTripStatusDetailsEnd", map);
                    String tripStatusId = (String) getSqlMapClientTemplate().queryForObject("trip.getWfuStatusId", map);
                    if (tripStatusId == null) {
                        updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateWfuTripStatusDetails", map);
                    }
                }
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updatedeletetrip Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updatedeletetrip", sqlException);
        }
        return updatedeletetrip;

    }

    public int updateTripAdvance(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        map.put("tripAdvanceId", tripTO.getTripAdvanceId());
        map.put("reqAmount", tripTO.getAdvanceAmount());

        map.put("userId", userId);
        System.out.println("the updateTripAdance" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripAdvance", map);
            System.out.println("updateTripAdvance=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTripAdvance" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTripAdvance", sqlException);
        }

        return status;
    }

    public int updateTripEnd(TripTO tripTO, int userId) {
        Map map = new HashMap();
        // map.put("startTime", tripTO.getTripStartHour() + ":" + tripTO.getTripStartMinute() + ":00");
        //  map.put("startDate", tripTO.getStartDate());
        map.put("endTime", tripTO.getTripEndHour() + ":" + tripTO.getTripEndMinute() + ":00");
        map.put("endDate", tripTO.getEndDate());
        map.put("totalDays", tripTO.getTotalDays());
        map.put("tripTransitHours", tripTO.getTripTransitHours());
        //map.put("endOdometerReading", tripTO.getEndOdometerReading());
        //map.put("endHM", tripTO.getEndHM());
        map.put("tripId", tripTO.getTripId());
        // map.put("tripSheetId", tripTO.getTripSheetId());
        //map.put("endTripRemarks", tripTO.getEndTripRemarks());
        // map.put("totalKM", tripTO.getTotalKM());
        // map.put("totalHrs", tripTO.getTotalHrs());
        // map.put("userId", userId);
        map.put("tripStartDate", tripTO.getStartDate());
        map.put("tripStartTime", tripTO.getStartTime());
        System.out.println("the updateEndTrip Details in the DAO " + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateEndTrip", map);
            System.out.println("updateEndTrip size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateEndTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateEndTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int updateTripStartDetails(TripTO tripTO, int userId) {
        Map map = new HashMap();

        map.put("tripId", tripTO.getTripId());

        map.put("tripStartDate", tripTO.getStartDate());
        map.put("tripStartTime", tripTO.getStartTime());
        System.out.println("the updateStartTrip Details in the DAO" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateStartTripDetails", map);
            System.out.println("updateEndTrip size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateEndTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateEndTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public ArrayList getTripClosureVehicleList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList tripClosureVehicle = new ArrayList();
        ArrayList tripClosureVehicleList = new ArrayList();
        TripTO tripTO1 = new TripTO();
        TripTO tripTO2 = new TripTO();
        int serialNo = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            System.out.println("closed Trip map is::" + map);
            tripClosureVehicle = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripClosureVehicleList", map);
            if (tripClosureVehicle.size() > 0) {
                Iterator itr = tripClosureVehicle.iterator();
                while (itr.hasNext()) {
                    tripTO2 = new TripTO();
                    tripTO1 = (TripTO) itr.next();
                    serialNo = Integer.parseInt(tripTO1.getSerialNumber());
                    tripTO2.setSerialNumber(tripTO1.getSerialNumber());
                    if (serialNo == 1) {
                        tripTO2.setTripCode(tripTO1.getTripCode());
                        tripTO2.setRegNo(tripTO1.getRegNo());
                        tripTO2.setRouteInfo(tripTO1.getRouteInfo());
                        tripTO2.setOriginCityName(tripTO1.getOriginCityName());
                        tripTO2.setStartDate(tripTO1.getStartDate());
                        tripTO2.setEndDate(tripTO1.getVehicleChangeDate());
                        tripTO2.setStartKm(tripTO1.getStartKm());
                        tripTO2.setEndKm(tripTO1.getEndKm());
                        tripTO2.setTotalKm(tripTO1.getTotalKm());
                        tripTO2.setStartHm(tripTO1.getStartHm());
                        tripTO2.setEndHm(tripTO1.getEndHm());
                        tripTO2.setTotalHm(tripTO1.getTotalHm());
                        tripTO2.setTransitDays1(tripTO1.getTransitDays1());
                        tripTO2.setVehicleId(tripTO1.getVehicleId());
                    } else if (serialNo > 1 && serialNo != tripClosureVehicle.size()) {
                        tripTO2.setTripCode(tripTO1.getTripCode());
                        tripTO2.setRegNo(tripTO1.getRegNo());
                        tripTO2.setRouteInfo(tripTO1.getRouteInfo());
                        tripTO2.setOriginCityName(tripTO1.getVehicleChangeCityName());
                        tripTO2.setStartDate(tripTO1.getVehicleChangeDate());
                        tripTO2.setEndDate(tripTO1.getVehicleChangeDate());
                        tripTO2.setStartKm(tripTO1.getStartKm());
                        tripTO2.setEndKm(tripTO1.getEndKm());
                        tripTO2.setTotalKm(tripTO1.getTotalKm());
                        tripTO2.setStartHm(tripTO1.getStartHm());
                        tripTO2.setEndHm(tripTO1.getEndHm());
                        tripTO2.setTotalHm(tripTO1.getTotalHm());
                        tripTO2.setTransitDays1(tripTO1.getTransitDays2());
                        tripTO2.setVehicleId(tripTO1.getVehicleId());
                    } else if (serialNo > 1 && serialNo == tripClosureVehicle.size()) {
                        tripTO2.setTripCode(tripTO1.getTripCode());
                        tripTO2.setRegNo(tripTO1.getRegNo());
                        tripTO2.setRouteInfo(tripTO1.getRouteInfo());
                        tripTO2.setOriginCityName(tripTO1.getVehicleChangeCityName());
                        tripTO2.setStartDate(tripTO1.getVehicleChangeDate());
                        tripTO2.setEndDate(tripTO1.getEndDate());
                        tripTO2.setStartKm(tripTO1.getStartKm());
                        tripTO2.setEndKm(tripTO1.getEndKm());
                        tripTO2.setTotalKm(tripTO1.getTotalKm());
                        tripTO2.setStartHm(tripTO1.getStartHm());
                        tripTO2.setEndHm(tripTO1.getEndHm());
                        tripTO2.setTotalHm(tripTO1.getTotalHm());
                        tripTO2.setTransitDays1(tripTO1.getTransitDays2());
                        tripTO2.setVehicleId(tripTO1.getVehicleId());
                    }
                    tripClosureVehicleList.add(tripTO2);
                }
            }
            System.out.println("getTripClosureVehicleList.size() = " + tripClosureVehicleList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripClosureVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripClosureVehicleList List", sqlException);
        }
        return tripClosureVehicleList;
    }

    public ArrayList getTripSettlementVehicleList(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        System.out.println("map = " + map);
        ArrayList customer = new ArrayList();

        try {
            customer = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripSettlementVehicleList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripSettlementVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripSettlementVehicleList", sqlException);
        }
        return customer;

    }

    public ArrayList getEmptyCustomerList(TripTO tripTO) {
        Map map = new HashMap();

        System.out.println("map = " + map);
        ArrayList customer = new ArrayList();

        try {
            customer = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmptyCustomerList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmptyCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getEmptyCustomerList", sqlException);
        }
        return customer;

    }

    public ArrayList getTicketingStatusList(TripTO tripTO) {
        Map map = new HashMap();

        System.out.println("map = " + map);
        ArrayList ticketingStatusList = new ArrayList();

        try {
            ticketingStatusList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTicketingStatusList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTicketingStatusList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTicketingStatusList", sqlException);
        }
        return ticketingStatusList;

    }

    public ArrayList getTicketingList(TripTO tripTO) {
        Map map = new HashMap();
        map.put("userId", tripTO.getUserId());
        map.put("ticketId", tripTO.getTicketId());
        map.put("roleId", tripTO.getRoleId());
        System.out.println("map = " + map);
        ArrayList ticketingList = new ArrayList();
        try {
            ticketingList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTicketingList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTicketingList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTicketingList", sqlException);
        }
        return ticketingList;
    }

    public ArrayList getTicketList(TripTO tripTO) {
        Map map = new HashMap();
        map.put("userId", tripTO.getUserId());
        map.put("ticketId", tripTO.getTicketId());
        System.out.println("map = " + map);
        ArrayList ticketingList = new ArrayList();
        try {
            ticketingList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTicketList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTicketList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTicketList", sqlException);
        }
        return ticketingList;
    }

    public ArrayList getTicketingDetailsList(TripTO tripTO) {
        Map map = new HashMap();
        map.put("userId", tripTO.getUserId());
        map.put("ticketId", tripTO.getTicketId());
        System.out.println("map = " + map);
        ArrayList ticketingList = new ArrayList();
        try {
            ticketingList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTicketingDetailsList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTicketingDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTicketingDetailsList", sqlException);
        }
        return ticketingList;
    }

    public ArrayList getTicketingStatusDetailsList(TripTO tripTO) {
        Map map = new HashMap();
        map.put("userId", tripTO.getUserId());
        map.put("ticketId", tripTO.getTicketId());
        System.out.println("map = " + map);
        ArrayList ticketingList = new ArrayList();
        try {
            ticketingList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTicketingStatusDetailsList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTicketingStatusDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTicketingStatusDetailsList", sqlException);
        }
        return ticketingList;
    }

    public int saveTicketFile(String actualFilePath, String fileSaved, TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int ticketId = 0;
        FileInputStream fis = null;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("fileName", fileSaved);
            map.put("userId", userId);
            System.out.println("actualFilePath = " + actualFilePath);
            File file = new File(actualFilePath);
            System.out.println("file = " + file);
            fis = new FileInputStream(file);
            System.out.println("fis = " + fis);
            byte[] ticketFile = new byte[(int) file.length()];
            System.out.println("ticketFile = " + ticketFile);
            fis.read(ticketFile);
            fis.close();
            map.put("ticketFile", ticketFile);
            map.put("ticketId", tripTO.getTicketId());
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTicketDetails", map);
            System.out.println("saveTripPodDetails size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTicket Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTicket List", sqlException);
        }

        return ticketId;
    }

    public int saveTicket(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int ticketId = 0;
        int status = 0;
        FileInputStream fis = null;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("userId", userId);
            map.put("status", tripTO.getStatus());
            map.put("type", tripTO.getType());
            map.put("from", tripTO.getFrom());
            map.put("to", tripTO.getTo());
            map.put("cc", tripTO.getCc());
            map.put("title", tripTO.getTitle());
            map.put("message", tripTO.getMessage());
            map.put("priority", tripTO.getPriority());
            System.out.println("the consignorDetails" + map);
            ticketId = (Integer) getSqlMapClientTemplate().insert("trip.saveTicket", map);
            map.put("ticketId", ticketId);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTicketStatusDetails", map);
            System.out.println("saveTripPodDetails size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTicket Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTicket List", sqlException);
        }

        return ticketId;
    }

    public int updateTicketStatus(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int ticketId = 0;
        int status = 0;
        FileInputStream fis = null;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("userId", userId);
            map.put("ticketId", tripTO.getTicketId());
            map.put("statusId", tripTO.getStatusId());
            map.put("message", tripTO.getMessage());
            System.out.println("the consignorDetails" + map);
            ticketId = (Integer) getSqlMapClientTemplate().update("trip.updateTicketMaster", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTicketStatusDetails", map);
            System.out.println("saveTripPodDetails size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTicketMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTicketMaster List", sqlException);
        }

        return ticketId;
    }

    public ArrayList gettripsettelmentDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        map.put("vehicleId", tripTO.getVehicleId());
        ArrayList tripsettelmentDetails = new ArrayList();
        System.out.println(" gettripsettelmentDetails map:" + map);
        try {

            tripsettelmentDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.gettripsettelmentDetails", map);

            System.out.println(" gettripsettelmentDetails size=" + tripsettelmentDetails.size());
            System.out.println(" gettripsettelmentDetails elements=" + tripsettelmentDetails);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog(" gettripsettelmentDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "tripsettelmentDetails List", sqlException);
        }

        return tripsettelmentDetails;
    }

    public ArrayList getVehicleUnloadingDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        map.put("vehicleId", tripTO.getVehicleId());
        ArrayList getVehicleUnloadingDetails = new ArrayList();
        System.out.println(" getVehicleUnloadingDetails map:" + map);
        try {

            getVehicleUnloadingDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleUnloadingDetails", map);

            System.out.println(" getVehicleUnloadingDetails size=" + getVehicleUnloadingDetails.size());
            System.out.println(" getVehicleUnloadingDetails elements=" + getVehicleUnloadingDetails);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog(" getVehicleUnloadingDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleUnloadingDetails List", sqlException);
        }

        return getVehicleUnloadingDetails;
    }

    public String getPreviousTripsEndTime(String tripSheetId) {
        Map map = new HashMap();

        String tripVehicleId = "";
        String tripVehicleId1 = "";
        String previousTripEndDetails = "";
        String previousTripType = "";
        String[] temp = null;
        map.put("tripSheetId", tripSheetId);
        System.out.println("map = " + map);
        String tripEndDetails = "";
        String previousTripEndDate = null;
        String previousTripEndTime = null;
        ArrayList tripClosureVehicle = new ArrayList();
        TripTO tripTO1 = new TripTO();
        TripTO tripTO2 = new TripTO();
        int serialNo = 0;
        String PreviousTripId = "";
        try {

            tripVehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleId", map);

            System.out.println("tripVehicleId = " + tripVehicleId);
            if (tripVehicleId != null && tripVehicleId != "") {
                map.put("tripVehicleId", tripVehicleId);

                PreviousTripId = (String) getSqlMapClientTemplate().queryForObject("trip.getPreviousTripId", map);
                System.out.println("tripVehicleId = " + PreviousTripId);
                if (PreviousTripId != null && PreviousTripId != "") {
                    map.put("tripId", PreviousTripId);
                    previousTripEndDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getPreviousTripEndTime", map);
                    System.out.println("previousTripEndTimeDetails = " + previousTripEndDetails);
                }
            }
//            if (previousTripEndDetails != null && previousTripEndDetails != "") {
//                System.out.println("previousTripEndDetails = " + previousTripEndDetails);
//                temp = previousTripEndDetails.split("~");
//                previousTripEndDate = temp[0];
//                previousTripEndTime = temp[1];
//            }
//            System.out.println("previousTripEndDate = " + previousTripEndDate);
//            System.out.println("previousTripEndTime = " + previousTripEndTime);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPreviousTripsOdometerReading Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getPreviousTripsOdometerReading", sqlException);
        }
        return previousTripEndDetails;

    }

    public String getTripVehicleStatus(String tripId) {
        Map map = new HashMap();
        map.put("tripId", tripId);
        String tripVehicleStatus = "";
        try {

            tripVehicleStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripVehicleStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripVehicleStatus List", sqlException);
        }

        return tripVehicleStatus;
    }

    public ArrayList getSecFCmailcontactDetails(String customerName) {
        ArrayList getSecFCmailcontactDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("customerName", customerName);
            getSecFCmailcontactDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getSecFCmailContactDetails", map);
            System.out.println("getSecFCmailcontactDetails size=" + getSecFCmailcontactDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getmailcontactDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getmailcontactDetails List", sqlException);
        }

        return getSecFCmailcontactDetails;
    }

    public int updateNextTrip(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int ticketId = 0;
        FileInputStream fis = null;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("nextTrip", tripTO.getNextTrip());
            map.put("userId", userId);
            System.out.println("update Next Trip Status = " + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateNextTrip", map);
            System.out.println("updateNextTrip =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateNextTrip Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateNextTrip List", sqlException);
        }

        return ticketId;
    }

    public ArrayList getPrimaryDriverSettlementTrip(TripTO tripTO) {
        ArrayList primaryDriverSettlementTrip = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("driverId", tripTO.getDriverId());
            map.put("fromDate", tripTO.getFromDate());
            map.put("toDate", tripTO.getToDate());
            System.out.println("map getPrimaryDriverSettlementTrip = " + map);
            primaryDriverSettlementTrip = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getPrimaryDriverSettlementTrip", map);
            System.out.println("getPrimaryDriverSettlementTrip size=" + primaryDriverSettlementTrip.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPrimaryDriverSettlementTrip Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPrimaryDriverSettlementTrip List", sqlException);
        }

        return primaryDriverSettlementTrip;
    }

    public ArrayList getVehicleDriverAdvance(TripTO tripTO) {
        ArrayList vehicleDriverAdvance = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("driverId", tripTO.getDriverId());
            map.put("fromDate", tripTO.getFromDate());
            map.put("toDate", tripTO.getToDate());
            vehicleDriverAdvance = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleDriverAdvance", map);
            System.out.println("getSecFCmailcontactDetails size=" + vehicleDriverAdvance.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDriverAdvance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleDriverAdvance List", sqlException);
        }

        return vehicleDriverAdvance;
    }

    public ArrayList getDriverIdleBhatta(TripTO tripTO) {
        ArrayList driverIdleBhatta = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("driverId", tripTO.getDriverId());
            map.put("fromDate", tripTO.getFromDate());
            map.put("toDate", tripTO.getToDate());
            driverIdleBhatta = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverIdleBhatta", map);
            System.out.println("getDriverIdleBhatta size=" + driverIdleBhatta.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverIdleBhatta Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverIdleBhatta List", sqlException);
        }

        return driverIdleBhatta;
    }

    public String getDriverLastBalanceAmount(TripTO tripTO) {
        String driverLastBalanceAmount = "";
        Map map = new HashMap();
        try {
            map.put("driverId", tripTO.getDriverId());
            map.put("fromDate", tripTO.getFromDate());
            map.put("toDate", tripTO.getToDate());
            driverLastBalanceAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getDriverLastBalanceAmount", map);
            if (driverLastBalanceAmount == null) {
                driverLastBalanceAmount = "0";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverLastBalanceAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverLastBalanceAmount List", sqlException);
        }

        return driverLastBalanceAmount;
    }

    public int insertPrimaryDriverSettlement(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("driverId", tripTO.getDriverId());
            map.put("fromDate", tripTO.getFromDate());
            map.put("toDate", tripTO.getToDate());
            map.put("driverLastBalance", tripTO.getDriverLastBalance());
            map.put("tripAmount", tripTO.getTripAmount());
            map.put("advanceAmount", tripTO.getAdvanceAmount());
            map.put("settleAmount", tripTO.getSettleAmount());
            map.put("payAmount", tripTO.getPayAmount());
            map.put("paymentMode", tripTO.getPaymentMode());
            map.put("balanceAmount", tripTO.getBalanceAmount());
            map.put("settlementTripSize", tripTO.getSettlementTripsSize());
            map.put("bhattaAmount", tripTO.getBhattaAmount());
            map.put("idleDays", tripTO.getIdleDays());
            map.put("settlementRemarks", tripTO.getRemarks());
            map.put("userId", userId);
            System.out.println("update Next Trip Status = " + map);
            status = (Integer) getSqlMapClientTemplate().insert("trip.insertPrimaryDriverSettlement", map);
            System.out.println("insertPrimaryDriverSettlement =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertPrimaryDriverSettlement Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertPrimaryDriverSettlement List", sqlException);
        }

        return status;
    }

    public int insertPrimaryDriverSettlementDetails(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("driverSettlementId", tripTO.getStatus());
            map.put("transactionType", tripTO.getTransactionType());
            map.put("transactionId", tripTO.getTransactionId());
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("driverId", tripTO.getDriverId());
            map.put("transactionAmount", tripTO.getTransactionAmount());
            map.put("driverCount", tripTO.getDriverCount());
            map.put("settlementAmount", tripTO.getAmount());
            map.put("userId", userId);
            System.out.println("update Next Trip Status = " + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.insertPrimaryDriverSettlementDetails", map);
            System.out.println("insertPrimaryDriverSettlement =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertPrimaryDriverSettlement Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertPrimaryDriverSettlement List", sqlException);
        }

        return status;
    }

    public int updateIdleBhattaStatus(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("idleBhattaId", tripTO.getIdleBhattaId());
            map.put("userId", userId);
            System.out.println("update Next Trip Status = " + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateIdleBhattaStatus", map);
            System.out.println("insertPrimaryDriverSettlement =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateIdleBhattaStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateIdleBhattaStatus List", sqlException);
        }

        return status;
    }

    public ArrayList getTripOtherExpenseDocumentRequired(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripSheetId());
        System.out.println("map in other expense document:=" + map);
        ArrayList getDocumentRequiredDetails = new ArrayList();
        try {

            getDocumentRequiredDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripOtherExpenseDocumentRequired", map);
            System.out.println("getDocumentRequiredDetails.size() = " + getDocumentRequiredDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDocumentRequiredDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDocumentRequiredDetails", sqlException);
        }

        return getDocumentRequiredDetails;
    }

    public ArrayList getOtherExpenseFileDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList statusDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            statusDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOtherExpenseFile", map);
            System.out.println("getPODDetails.size() = " + statusDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPODDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPODDetails List", sqlException);
        }
        return statusDetails;
    }

    public int deleteExpenaseBillCopy(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int updateStatus = 0;
        int updateStatus1 = 0;
        try {

            map.put("expenseId", tripTO.getExpenseId());
            System.out.println("map for expense delete:" + map);

            updateStatus = (Integer) getSqlMapClientTemplate().update("trip.deleteExpenseBillCopy", map);
            updateStatus1 = (Integer) getSqlMapClientTemplate().update("trip.updateTripExpenseForBillDelete", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("deletefunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "deletefunctions", sqlException);
        }
        return updateStatus;
    }

    public String getTripUnclearedBalance(TripTO tripTO) {
        String driverLastBalanceAmount = "";
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        try {
            driverLastBalanceAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getTripUnclearedBalance", map);
            if (driverLastBalanceAmount == null) {
                driverLastBalanceAmount = "0";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripUnclearedBalance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripUnclearedBalance List", sqlException);
        }

        return driverLastBalanceAmount;
    }

    public ArrayList getTripSheetDetailsForChallan(TripTO tripTO) {
        Map map = new HashMap();
        map.put("driverId", tripTO.getDriverId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        map.put("documentRequiredStatus", tripTO.getDocumentRequiredStatus());
        System.out.println("map = " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripSheetDetailsForChallan", map);
            System.out.println("getTripSheetDetails for challan size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripSheetDetailsforchallan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripSheetDetailsforchallan List", sqlException);
        }

        return tripDetails;
    }

    public String getTotalBillExpenseForDriver(TripTO tripTO) {
        Map map = new HashMap();
        map.put("driverId", tripTO.getDriverId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        map.put("documentRequiredStatus", tripTO.getDocumentRequiredStatus());
        String billExpense = "";
        try {

            billExpense = (String) getSqlMapClientTemplate().queryForObject("trip.getTotalBillExpenseForDriver", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTotalBillExpenseForDriver Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTotalBillExpenseForDriver List", sqlException);
        }

        return billExpense;
    }

    public ArrayList getOrderInvoiceHeader(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        ArrayList result = new ArrayList();
        System.out.println("map = " + map);
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderInvoiceHeader", map);
            System.out.println("result size=" + result.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderInvoiceHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderInvoiceHeader List", sqlException);
        }

        return result;
    }

    public ArrayList getTripsBilledDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList result = new ArrayList();
        try {
            map.put("invoiceId", tripTO.getInvoiceId());
            String tripId = (String) getSqlMapClientTemplate().queryForObject("trip.getInvoiceTripIds", map);
            String[] tripIds = tripId.split(",");
            List tripSheetIds = new ArrayList(tripIds.length);
            for (int i = 0; i < tripIds.length; i++) {
                System.out.println("value:" + tripIds[i]);
                tripSheetIds.add(tripIds[i]);
            }
            map.put("tripSheetIds", tripSheetIds);

            String consignmentOrderId = (String) getSqlMapClientTemplate().queryForObject("trip.getInvoiceConsignmentOrderIds", map);
            String[] consignmentIds = consignmentOrderId.split(",");
            List consignmentOrderIds = new ArrayList(consignmentIds.length);

            for (int i = 0; i < consignmentIds.length; i++) {
                System.out.println("consignmentOrderId value:" + consignmentIds[i]);
                consignmentOrderIds.add(consignmentIds[i]);
            }
            map.put("consignmentOrderIds", consignmentOrderIds);

            System.out.println("map = " + map);
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripsBilledDetails", map);
            System.out.println("result size=" + result.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderInvoiceHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderInvoiceHeader List", sqlException);
        }

        return result;
    }

    public ArrayList getOrderInvoiceHeaderBill(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        ArrayList result = new ArrayList();
        System.out.println("map = " + map);
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderInvoiceHeaderBill", map);
            System.out.println("result size=" + result.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderInvoiceHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderInvoiceHeader List", sqlException);
        }

        return result;
    }

    public ArrayList getOrderInvoiceHeaderRemb(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        ArrayList result = new ArrayList();
        System.out.println("map = " + map);
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderInvoiceHeaderRemb", map);
            System.out.println("result size=" + result.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderInvoiceHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderInvoiceHeader List", sqlException);
        }

        return result;
    }

    public ArrayList getGSTTaxPercentage(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList taxList = new ArrayList();
        try {
            map.put("billingState", tripTO.getBillingState());
            System.out.println("map:" + map);
            taxList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getGSTTaxPercentage", map);
            System.out.println("taxList size...." + taxList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getInvoicedOrderList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoicedOrderList List", sqlException);
        }
        return taxList;
    }

    public ArrayList getCompanyDetailsList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList companyList = new ArrayList();
        try {
            map.put("stateId", tripTO.getCityId());
            companyList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCompanyDetailsList", map);
            System.out.println("taxList size...." + companyList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getInvoicedOrderList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoicedOrderList List", sqlException);
        }
        return companyList;
    }

    public ArrayList getCustomerDetailsList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList customerList = new ArrayList();
        try {
            map.put("customerId", tripTO.getCustomerId());
            System.out.println(" mappppp iiss" + map);
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCustomerDetailsList", map);
            System.out.println("taxList size...." + customerList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getCustomerDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerDetailsList List", sqlException);
        }
        return customerList;
    }

    public int updateInvoiceBillTax(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("invoiceId", tripTO.getInvoiceId());
            if (tripTO.getIgstValue() != null && !"".equalsIgnoreCase(tripTO.getIgstValue()) && !"0".equalsIgnoreCase(tripTO.getIgstValue()) && !"0.00".equalsIgnoreCase(tripTO.getIgstValue())) {
                map.put("taxAmount", tripTO.getIgstValue());
                map.put("gstType", "IGST");
                map.put("taxPercentage", tripTO.getIgstPercentage());
                System.out.println("update Next Trip Status = " + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.insertInvoiceBillTax", map);
            }
            if (tripTO.getCgstValue() != null && !"".equalsIgnoreCase(tripTO.getCgstValue()) && !"0".equalsIgnoreCase(tripTO.getCgstValue()) && !"0.00".equalsIgnoreCase(tripTO.getCgstValue())) {
                map.put("taxAmount", tripTO.getCgstValue());
                map.put("gstType", "CGST");
                map.put("taxPercentage", tripTO.getCgstPercentage());
                System.out.println("update Next Trip Status = " + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.insertInvoiceBillTax", map);
            }
            if (tripTO.getSgstValue() != null && !"".equalsIgnoreCase(tripTO.getSgstValue()) && !"0".equalsIgnoreCase(tripTO.getSgstValue()) && !"0.00".equalsIgnoreCase(tripTO.getSgstValue())) {
                map.put("taxAmount", tripTO.getSgstValue());
                map.put("gstType", "SGST");
                map.put("taxPercentage", tripTO.getSgstPercentage());
                System.out.println("update Next Trip Status = " + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.insertInvoiceBillTax", map);
            }

            System.out.println("insertPrimaryDriverSettlement =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateIdleBhattaStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateIdleBhattaStatus List", sqlException);
        }

        return status;
    }

    public int updateInvoiceReimbusTax(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("invoiceId", tripTO.getInvoiceId());
            if (tripTO.getIgstValue() != null && !"".equalsIgnoreCase(tripTO.getIgstValue()) && !"0".equalsIgnoreCase(tripTO.getIgstValue()) && !"0.00".equalsIgnoreCase(tripTO.getIgstValue())) {
                map.put("taxAmount", tripTO.getIgstValue());
                map.put("gstType", "IGST");
                map.put("taxPercentage", tripTO.getIgstPercentage());
                System.out.println("update Next Trip Status = " + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.insertInvoiceReimbusTax", map);
            }
            if (tripTO.getCgstValue() != null && !"".equalsIgnoreCase(tripTO.getCgstValue()) && !"0".equalsIgnoreCase(tripTO.getCgstValue()) && !"0.00".equalsIgnoreCase(tripTO.getCgstValue())) {
                map.put("taxAmount", tripTO.getCgstValue());
                map.put("gstType", "CGST");
                map.put("taxPercentage", tripTO.getCgstPercentage());
                System.out.println("update Next Trip Status = " + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.insertInvoiceReimbusTax", map);
            }
            if (tripTO.getSgstValue() != null && !"".equalsIgnoreCase(tripTO.getSgstValue()) && !"0".equalsIgnoreCase(tripTO.getSgstValue()) && !"0.00".equalsIgnoreCase(tripTO.getSgstValue())) {
                map.put("taxAmount", tripTO.getSgstValue());
                map.put("gstType", "SGST");
                map.put("taxPercentage", tripTO.getSgstPercentage());
                System.out.println("update Next Trip Status = " + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.insertInvoiceReimbusTax", map);
            }

            System.out.println("insertInvoiceReimbusTax =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateIdleBhattaStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateIdleBhattaStatus List", sqlException);
        }

        return status;
    }

    public ArrayList getInvoiceGSTTaxDetailsBill(TripTO tripTO) {
        Map map = new HashMap();

        map.put("invoiceId", tripTO.getInvoiceId());

        ArrayList gstDetails = new ArrayList();
        try {
            gstDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInvoiceGSTTaxDetailsBill", map);
            System.out.println("gstDetails size=" + gstDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return gstDetails;
    }

    public ArrayList getInvoiceGSTTaxDetailsRemb(TripTO tripTO) {
        Map map = new HashMap();

        map.put("invoiceId", tripTO.getInvoiceId());

        ArrayList gstDetails = new ArrayList();
        try {
            gstDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInvoiceGSTTaxDetailsRemb", map);
            System.out.println("gstDetails size=" + gstDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return gstDetails;
    }

    public String getshowGSTNo(TripTO tripTO) {
        Map map = new HashMap();

        map.put("invoiceId", tripTO.getInvoiceId());

        String getshowGSTNo = new String();
        try {
            getshowGSTNo = (String) getSqlMapClientTemplate().queryForObject("trip.getshowGSTNo", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getshowGSTNo List", sqlException);
        }

        return getshowGSTNo;
    }

    public String getHireVendorTripExpense(TripTO tripTO) {
        Map map = new HashMap();
        String originId = "", point1Id = "", point2Id = "", destinationId = "";
        map.put("vendorId", tripTO.getVendorId());
        map.put("vehicleTypeId", tripTO.getVehicleTypeId());
        map.put("orderId", tripTO.getConsignmentId());
        map.put("billingTypeId", tripTO.getBillingType());

        System.out.println("map 1111= " + map);

        String pointIds = (String) getSqlMapClientTemplate().queryForObject("trip.getOrderPoints", map);
        System.out.println("pointIds = " + pointIds);
        String[] tempPoint = pointIds.split(",");
        if (tempPoint.length == 2) {
            originId = tempPoint[0];
            destinationId = tempPoint[1];
        } else if (tempPoint.length == 3) {
            originId = tempPoint[0];
            point1Id = tempPoint[1];
            destinationId = tempPoint[2];
        } else if (tempPoint.length == 4) {
            originId = tempPoint[0];
            point1Id = tempPoint[1];
            point2Id = tempPoint[2];
            destinationId = tempPoint[3];
        }
        map.put("originId", originId);
        map.put("point1Id", point1Id);
        map.put("point2Id", point2Id);
        map.put("destinationId", destinationId);
        System.out.println("map 2222222= " + map);
        String expense = new String();
        try {
            expense = (String) getSqlMapClientTemplate().queryForObject("trip.getHireVendorTripExpense", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getshowGSTNo List", sqlException);
        }

        return expense;
    }

    public ArrayList getVendorList() {
        Map map = new HashMap();
        ArrayList vendorList = new ArrayList();
        try {
            vendorList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVendorList", map);
            System.out.println("vendorList.size() = " + vendorList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStatusList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getStatusList List", sqlException);
        }
        return vendorList;
    }
//       public ArrayList getVehicleRegNos(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
//        ArrayList vehicleNos = new ArrayList();
//        vehicleNos = tripDAO.getVehicleRegNos(tripTO);
//        return vehicleNos;
//    }

    public ArrayList getVehicleRegNos(TripTO tripTO) {
        Map map = new HashMap();
        map.put("destination", tripTO.getDestinationId());
        map.put("origin", tripTO.getOriginId());
        String vehicleNo = tripTO.getVehicleNo();
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("statusId", tripTO.getStatusId());
        System.out.println("vehicleNo = " + vehicleNo);
//        map.put("vehicleNo", vehicleNo);
        map.put("vehicleNo", vehicleNo + "%");
//        if(tripTO.getVehicleNo() != null && !"".equals(tripTO.getVehicleNo())){
//        }else{
//        map.put("vehicleNo", "");
//        }
        if ("1".equals(tripTO.getTripType())) {
            map.put("usageTypeId", 2);
        } else if ("2".equals(tripTO.getTripType())) {
            map.put("usageTypeId", 1);
        }
        if ("2".equals(tripTO.getTripType())) {
            map.put("customerName", tripTO.getCustomerName());
        }
        map.put("roleId", tripTO.getRoleId());
        map.put("companyId", tripTO.getCompanyId());
        if ("1".equals(tripTO.getVendorId())) {
            map.put("ownerShip", "1");
            map.put("vendorId", "");
        } else {
            map.put("vendorId", tripTO.getVendorId());
            map.put("ownerShip", "");
        }
        if (tripTO.getVehicleTypeId() != null && !"".equals(tripTO.getVehicleTypeId())) {
            map.put("vehicleTypeId", tripTO.getVehicleTypeId().split("-")[0]);
        } else {
            map.put("vehicleTypeId", tripTO.getVehicleTypeId());
        }

        ArrayList vehicleNos = new ArrayList();
        try {
            System.out.println("map: in the dao" + map);
            vehicleNos = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleRegNos", map);
            System.out.println("vehicleNos size=" + vehicleNos.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return vehicleNos;
    }

    public ArrayList getVendorVehicleType(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList vehicleTypeList = new ArrayList();
        ArrayList pointList = new ArrayList();

        map.put("roleId", tripTO.getRoleId());
        map.put("companyId", tripTO.getCompanyId());
        map.put("vendorId", tripTO.getVendorId());
        map.put("originId", tripTO.getOriginId());
        map.put("destinationId", tripTO.getDestinationId());
        map.put("ownerShip", tripTO.getOwnerShip());

        if (tripTO.getOwnerShip().equals("")) {
            map.put("ownerShip", "0");
        }

        map.put("consignmentId", tripTO.getConsignmentId());

        String firstPointId = "", point1Id = "", point2Id = "", point3Id = "", point4Id = "", finalPointId = "";

        pointList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripPointList", map);

        String[] temp = new String[pointList.size()];
        int x = 0;
        TripTO tripTO1 = new TripTO();
        Iterator itr = pointList.iterator();
        while (itr.hasNext()) {
            tripTO1 = (TripTO) itr.next();
            temp[x] = tripTO1.getPoint1Id();
            x++;
        }

        for (int i = 0; i < temp.length; i++) {
            if (i == 0) {
                firstPointId = temp[i];
            }
            if (i == 1) {
                if ((i + 1) == temp.length) {
                    finalPointId = temp[i];
                } else {
                    point1Id = temp[i];
                }
            }
            if (i == 2) {
                if ((i + 1) == temp.length) {
                    finalPointId = temp[i];
                } else {
                    point2Id = temp[i];
                }
            }
            if (i == 3) {
                if ((i + 1) == temp.length) {
                    finalPointId = temp[i];
                } else {
                    point3Id = temp[i];
                }
            }
            if (i == 4) {
                if ((i + 1) == temp.length) {
                    finalPointId = temp[i];
                } else {
                    point4Id = temp[i];
                }
            }
            if (i == 5) {
                finalPointId = temp[i];
            }

        }

        map.put("firstPointId", firstPointId);
        map.put("point1Id", point1Id);
        map.put("point2Id", point2Id);
        map.put("point3Id", point3Id);
        map.put("point4Id", point4Id);
        map.put("finalPointId", finalPointId);

        map.put("vehicleTypeId", tripTO.getVehicleTypeId());

        System.out.println("map = " + map);

        try {
            vehicleTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVendorVehicleType", map);
            System.out.println("vehicleTypeList size=" + vehicleTypeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleTypeList List", sqlException);
        }

        return vehicleTypeList;
    }

    public ArrayList getFinanceStateList(TripTO tripTO) {
        Map map = new HashMap();
        System.out.println("map = " + map);
        ArrayList financeStateList = new ArrayList();
        try {
            financeStateList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getFinanceStateList", map);
            System.out.println("financeStateList size=" + financeStateList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("financeStateList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleTypeList List", sqlException);
        }

        return financeStateList;
    }

    public ArrayList getDriverNames(TripTO tripTO) {
        Map map = new HashMap();
        String driverName = tripTO.getDriverName() + "%";
        map.put("driverName", driverName);
        System.out.println("map==" + map);

        ArrayList drivers = new ArrayList();
        try {
            drivers = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverNames", map);
            System.out.println("drivers size=" + drivers.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return drivers;
    }

    public int updateEstimateFeright(TripTO tripTO, String[] consignmentId, String[] estimatedRevenue) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            for (int i = 0; i < consignmentId.length; i++) {
                System.out.println("value:" + consignmentId[i]);
                System.out.println("estimatedRevenue:" + estimatedRevenue[i]);
                map.put("estimatedRevenue", estimatedRevenue[i]);
                map.put("consignmentId", consignmentId[i]);
                System.out.println("map============" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateEstimateFeright", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveAction Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveAction", sqlException);
        }
        return status;
    }

    public int insertFuelLiter(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            System.out.println("DAO12345lll;");
//            SecondaryOperationTO operationTO = null;
            map.put("tripSheetId", tripTO.getTripSheetId());
            map.put("fuelLtr", tripTO.getFuelLtr());
            map.put("userId", userId);
            System.out.println("map value is00123 " + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.insertFuelLiters", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripRoutePlan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripRoutePlan", sqlException);
        }
        return status;
    }

    public ArrayList getTripFuel(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList getTripFuel = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            getTripFuel = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripFuel", map);
            System.out.println("getTripFuel.size() = " + getTripFuel.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripFuel Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripFuel List", sqlException);
        }
        return getTripFuel;
    }

    public int updateTripFuel(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            if ("".equals(tripTO.getFuelLtr())) {
                map.put("fuelLtr", 0);
            } else {
                map.put("fuelLtr", tripTO.getFuelLtr());
            }
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripFuel", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTripFuel Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTripFuel", sqlException);
        }
        return status;
    }

    public int saveBillHeaderCancellation(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceDate", tripTO.getCancelInvoiceDate());
        map.put("invoiceType", tripTO.getCancelInvoiceType());
        map.put("gstLevy", tripTO.getCancelGstLevy());
        map.put("placeofSupply", tripTO.getCancelPlaceofSupply());
        map.put("invoiceId", tripTO.getInvoiceId());
        map.put("billingId", tripTO.getBillingId());
        map.put("companyId", tripTO.getCompanyId());
        map.put("userId", tripTO.getUserId());
        map.put("invoiceCode", tripTO.getInvoiceCode());
        map.put("invoiceNo", tripTO.getCancelInvoiceNo());
//        raju
        map.put("billing", tripTO.getBilling());
        map.put("tripId", "0");
        map.put("customerId", tripTO.getCustomerId());
        map.put("billType", "1");
        map.put("noOfTrips", "0");
        map.put("grandTotal", tripTO.getGrandTotal());
        map.put("taxAmount", tripTO.getTaxValue());
        map.put("totalRevenue", tripTO.getTotalRevenue());
        map.put("totalExpToBeBilled", "0");
        System.out.println("the saveBillHeader cancelll" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveBillHeaderCancellation", map);
            System.out.println("saveBillHeaderCancellationsaveBillHeaderCancellation size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int saveBillDetailsCancellation(TripTO tripTO) {
        Map map = new HashMap();

        map.put("userId", tripTO.getUserId());
        map.put("invoiceNo", tripTO.getInvoiceNo());
        map.put("invoiceCode", tripTO.getInvoiceCode());
        map.put("invoiceId", tripTO.getInvoiceId());
        map.put("tripId", "0");
        map.put("consignmentId", tripTO.getConsignmentId());
        map.put("customerId", tripTO.getCustomerId());
        map.put("totalRevenue", tripTO.getEstimatedRevenue());
        map.put("totalExpToBeBilled", "0");
        Float grandTotal = Float.parseFloat(tripTO.getEstimatedRevenue());
        map.put("grandTotal", grandTotal);
        System.out.println("the saveBillDetails:" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().insert("trip.saveBillDetailsCancellation", map);
            System.out.println("saveBillDetails size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int updateInvoiceCancellationTax(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("invoiceId", tripTO.getInvoiceId());
            if (tripTO.getIgstValue() != null && !"".equalsIgnoreCase(tripTO.getIgstValue()) && !"0".equalsIgnoreCase(tripTO.getIgstValue()) && !"0.00".equalsIgnoreCase(tripTO.getIgstValue())) {
                map.put("taxAmount", tripTO.getIgstValue());
                map.put("gstType", "IGST");
                map.put("taxPercentage", tripTO.getIgstPercentage());
                System.out.println("update Next Trip Status = " + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.insertInvoiceCancellationTax", map);
            }
            if (tripTO.getCgstValue() != null && !"".equalsIgnoreCase(tripTO.getCgstValue()) && !"0".equalsIgnoreCase(tripTO.getCgstValue()) && !"0.00".equalsIgnoreCase(tripTO.getCgstValue())) {
                map.put("taxAmount", tripTO.getCgstValue());
                map.put("gstType", "CGST");
                map.put("taxPercentage", tripTO.getCgstPercentage());
                System.out.println("update Next Trip Status = " + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.insertInvoiceCancellationTax", map);
            }
            if (tripTO.getSgstValue() != null && !"".equalsIgnoreCase(tripTO.getSgstValue()) && !"0".equalsIgnoreCase(tripTO.getSgstValue()) && !"0.00".equalsIgnoreCase(tripTO.getSgstValue())) {
                map.put("taxAmount", tripTO.getSgstValue());
                map.put("gstType", "SGST");
                map.put("taxPercentage", tripTO.getSgstPercentage());
                System.out.println("update Next Trip Status = " + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.insertInvoiceCancellationTax", map);
            }

            System.out.println("insertInvoiceCancellationTax =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateIdleBhattaStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateIdleBhattaStatus List", sqlException);
        }

        return status;
    }

    public int updateCancellationStatus(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("userId", userId);
        map.put("statusId", tripTO.getStatusId());
        System.out.println("the updateCancellationStatus" + map);
        int status = 0;
        int flag = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            //values(#tripSheetId#, #statusId#, #KM#, #HM#, #remarks#, #userId#, now())
            for (int i = 0; i < tripTO.getConsignmentOrderIds().length; i++) {
                System.out.println("value:" + tripTO.getConsignmentOrderIds()[i]);
                map.put("consignmentId", tripTO.getConsignmentOrderIds()[i]);
                System.out.println("updateCancellationStatus map============" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatusForCancellationBill", map);
                System.out.println("status size=" + status);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStatus List", sqlException);
        }

        return status;
    }

    public ArrayList getOrderInvoiceHeaderCancellation(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        ArrayList result = new ArrayList();
        System.out.println("map = " + map);
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderInvoiceHeaderCancellation", map);
            System.out.println("result size=" + result.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderInvoiceHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderInvoiceHeader List", sqlException);
        }

        return result;
    }

    public ArrayList getInvoiceGSTTaxDetailsCancellation(TripTO tripTO) {
        Map map = new HashMap();

        map.put("invoiceId", tripTO.getInvoiceId());

        ArrayList gstDetails = new ArrayList();
        try {
            gstDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInvoiceGSTTaxDetailsCancellation", map);
            System.out.println("gstDetails size=dea" + gstDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return gstDetails;
    }
// TSP     

    public int saveBillHeaderTSP(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceDate", tripTO.getTspInvoiceDate());
        map.put("invoiceType", tripTO.getTspInvoiceType());
        map.put("gstLevy", tripTO.getTspGstLevy());
        map.put("placeofSupply", tripTO.getTspPlaceofSupply());
        map.put("billingId", tripTO.getBillingId());
        map.put("companyId", tripTO.getCompanyId());
        map.put("userId", tripTO.getUserId());
        map.put("invoiceCode", tripTO.getInvoiceCode());
        map.put("invoiceNo", tripTO.getTspInvoiceNo());
        map.put("goodsMovement", tripTO.getGoodsMovement());
//        sesha
        map.put("fuelEscalation", "0");
        map.put("fuelEstCharge", "0");
//        raju
        map.put("billing", tripTO.getBilling());
        map.put("tripId", tripTO.getTripId());
        map.put("customerId", tripTO.getCustomerId());
        map.put("billType", "1");

        String invoiceFlag = tripTO.getInvoiceFlag();
        System.out.println("invoiceFlag=" + invoiceFlag);
        if ("3".equals(invoiceFlag)) {
            map.put("originTspFlag", "1");
            map.put("destinationTspFlag", "0");
        } else if ("5".equals(invoiceFlag)) {
            map.put("originTspFlag", "0");
            map.put("destinationTspFlag", "1");
        } else {
            map.put("originTspFlag", "0");
            map.put("destinationTspFlag", "0");
        }

        String cancellationFlag = tripTO.getCancelInvoiceFlag();
        System.out.println("cancellationFlag=" + cancellationFlag);
        if ("4".equals(cancellationFlag)) {
            map.put("cancellationFlag", "1");
            map.put("noOfTrips", "0");
            map.put("totalRevenue", tripTO.getTotalCancellationInvoiceAmt());
            map.put("grandTotal", tripTO.getTotalCancellationInvoiceAmt());

        } else {
            map.put("cancellationFlag", "0");
            map.put("noOfTrips", tripTO.getNoOfTrips());
            map.put("totalRevenue", tripTO.getTspRoyaltyAmount());
            System.out.println("grandTotal====" + tripTO.getTotalTspRoyaltyInvoiceAmt());
            map.put("grandTotal", tripTO.getTotalTspRoyaltyInvoiceAmt());
        }

        map.put("totalExpToBeBilled", "0");
//        map.put("totalExpToBeBilled", tripTO.getTotalTspRoyaltyInvoiceAmt());
        System.out.println("the saveBillHeaderTSP" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().insert("trip.saveBillHeaderTSP", map);
            System.out.println("saveBillHeader size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int saveBillDetailsTSP(TripTO tripTO) {
        Map map = new HashMap();

        map.put("userId", tripTO.getUserId());
        map.put("invoiceNo", tripTO.getTspInvoiceNo());
        map.put("invoiceCode", tripTO.getInvoiceCode());
        map.put("invoiceId", tripTO.getInvoiceId());
        map.put("tripId", tripTO.getTripId());
        map.put("consignmentId", tripTO.getConsignmentId());
        map.put("customerId", tripTO.getCustomerId());
        map.put("totalRevenue", tripTO.getExpenseToBeBilledToCustomer());
        String totalExpToBeBilled = tripTO.getTotalExpToBeBilled();
        System.out.println("totalExpToBeBilled " + totalExpToBeBilled);
        if (!"".equals(totalExpToBeBilled) && totalExpToBeBilled != null) {
            totalExpToBeBilled = totalExpToBeBilled;
        } else {
            totalExpToBeBilled = "0.00";
        }
        Float grandTotal = Float.parseFloat(tripTO.getEstimatedRevenue())
                + Float.parseFloat(totalExpToBeBilled);
        map.put("totalExpToBeBilled", totalExpToBeBilled);

//        map.put("totalExpToBeBilled", tripTO.getExpenseToBeBilledToCustomer());
//        Float grandTotal = Float.parseFloat(tripTO.getEstimatedRevenue()) + Float.parseFloat(tripTO.getExpenseToBeBilledToCustomer());
        map.put("grandTotal", grandTotal);
        System.out.println("the saveBillDetails:" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().insert("trip.saveBillDetailsTSP", map);
            System.out.println("saveBillDetails size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int updateTSPinvoiceStatus(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("userId", userId);
//        map.put("statusId", tripTO.getStatusId());
        String tspInvoiceFlagOrgDes = tripTO.getInvoiceFlag();
        System.out.println("tspInvoiceFlagOrgDes==" + tspInvoiceFlagOrgDes);
        if ("3".equals(tspInvoiceFlagOrgDes)) { // update tsp status in consignment note
            map.put("tspOriging", "1");
            map.put("tspDestination", "0");
        } else if ("5".equals(tspInvoiceFlagOrgDes)) {
            map.put("tspOriging", "0");
            map.put("tspDestination", "1");
        } else {
            map.put("tspOriging", "0");
            map.put("tspDestination", "0");
        }

        System.out.println("the updateTSPinvoiceStatus" + map);
        int status = 0;
        int flag = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            //values(#tripSheetId#, #statusId#, #KM#, #HM#, #remarks#, #userId#, now())
            for (int i = 0; i < tripTO.getConsignmentOrderIds().length; i++) {
                System.out.println("value:" + tripTO.getConsignmentOrderIds()[i]);
                map.put("consignmentId", tripTO.getConsignmentOrderIds()[i]);
                System.out.println("updateTSPinvoiceStatus map============" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatusForTSPBill", map);
                System.out.println("status size=" + status);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStatus List", sqlException);
        }

        return status;
    }

    public int updateInvoiceTspTax(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("invoiceId", tripTO.getInvoiceId());
            if (tripTO.getIgstValue() != null && !"".equalsIgnoreCase(tripTO.getIgstValue()) && !"0".equalsIgnoreCase(tripTO.getIgstValue()) && !"0.00".equalsIgnoreCase(tripTO.getIgstValue())) {
                map.put("taxAmount", tripTO.getIgstValue());
                map.put("gstType", "IGST");
                map.put("taxPercentage", tripTO.getIgstPercentage());
                System.out.println("update Next Trip Status = " + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.insertInvoiceTspBillTax", map);
            }
            if (tripTO.getCgstValue() != null && !"".equalsIgnoreCase(tripTO.getCgstValue()) && !"0".equalsIgnoreCase(tripTO.getCgstValue()) && !"0.00".equalsIgnoreCase(tripTO.getCgstValue())) {
                map.put("taxAmount", tripTO.getCgstValue());
                map.put("gstType", "CGST");
                map.put("taxPercentage", tripTO.getCgstPercentage());
                System.out.println("update Next Trip Status = " + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.insertInvoiceTspBillTax", map);
            }
            if (tripTO.getSgstValue() != null && !"".equalsIgnoreCase(tripTO.getSgstValue()) && !"0".equalsIgnoreCase(tripTO.getSgstValue()) && !"0.00".equalsIgnoreCase(tripTO.getSgstValue())) {
                map.put("taxAmount", tripTO.getSgstValue());
                map.put("gstType", "SGST");
                map.put("taxPercentage", tripTO.getSgstPercentage());
                System.out.println("update Next Trip Status = " + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.insertInvoiceTspBillTax", map);
            }

            System.out.println("insertInvoiceTspBillTax =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateIdleBhattaStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateIdleBhattaStatus List", sqlException);
        }

        return status;
    }
// Royalty    

    public int saveBillHeaderRoyalty(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceDate", tripTO.getRoyaltyInvoiceDate());
        map.put("invoiceType", tripTO.getRoyaltyInvoiceType());
        map.put("gstLevy", tripTO.getRoyaltyGstLevy());
        map.put("placeofSupply", tripTO.getRoyaltyPlaceofSupply());
        map.put("billingId", tripTO.getBillingId());
        map.put("companyId", tripTO.getCompanyId());
        map.put("userId", tripTO.getUserId());
        map.put("invoiceCode", tripTO.getInvoiceCode());
        map.put("invoiceNo", tripTO.getRoyaltyInvoiceNo());
        map.put("goodsMovement", tripTO.getGoodsMovement());

        map.put("fuelEscalation", "0");
        map.put("fuelEstCharge", "0");
//        raju
        map.put("billing", tripTO.getBilling());
        map.put("tripId", tripTO.getTripId());
        map.put("customerId", tripTO.getCustomerId());
        map.put("billType", "1");
        String cancellationFlag = tripTO.getCancelInvoiceFlag();
        System.out.println("cancellationFlag=" + cancellationFlag);
        if ("4".equals(cancellationFlag)) {
            map.put("cancellationFlag", "1");
            map.put("noOfTrips", "0");
            map.put("totalRevenue", tripTO.getTotalCancellationInvoiceAmt());
            map.put("grandTotal", tripTO.getTotalCancellationInvoiceAmt());

        } else {
            map.put("cancellationFlag", "0");
            map.put("noOfTrips", tripTO.getNoOfTrips());
            map.put("totalRevenue", tripTO.getTspRoyaltyAmount());
            System.out.println("grandTotal====" + tripTO.getTotalTspRoyaltyInvoiceAmt());
            map.put("grandTotal", tripTO.getTotalTspRoyaltyInvoiceAmt());
        }
        map.put("totalExpToBeBilled", "0");
//        map.put("totalExpToBeBilled", tripTO.getTotalTspRoyaltyInvoiceAmt());
        System.out.println("the saveBillHeaderTSP" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().insert("trip.saveBillHeaderRoyalty", map);
            System.out.println("saveBillHeader size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int saveBillDetailsRoyalty(TripTO tripTO) {
        Map map = new HashMap();

        map.put("userId", tripTO.getUserId());
        map.put("invoiceNo", tripTO.getTspInvoiceNo());
        map.put("invoiceCode", tripTO.getInvoiceCode());
        map.put("invoiceId", tripTO.getInvoiceId());
        map.put("tripId", tripTO.getTripId());
        map.put("consignmentId", tripTO.getConsignmentId());
        map.put("customerId", tripTO.getCustomerId());
        map.put("totalRevenue", tripTO.getExpenseToBeBilledToCustomer());
        String totalExpToBeBilled = tripTO.getTotalTspRoyaltyInvoiceAmt();
        System.out.println("totalExpToBeBilled " + totalExpToBeBilled);
        if (!"".equals(totalExpToBeBilled) && totalExpToBeBilled != null) {
            totalExpToBeBilled = totalExpToBeBilled;
        } else {
            totalExpToBeBilled = "0.00";
        }
        Float grandTotal = Float.parseFloat(tripTO.getEstimatedRevenue())
                + Float.parseFloat(totalExpToBeBilled);
        map.put("totalExpToBeBilled", totalExpToBeBilled);

//        map.put("totalExpToBeBilled", tripTO.getExpenseToBeBilledToCustomer());
//        Float grandTotal = Float.parseFloat(tripTO.getEstimatedRevenue()) + Float.parseFloat(tripTO.getExpenseToBeBilledToCustomer());
        map.put("grandTotal", grandTotal);
        System.out.println("the saveBillDetails:" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().insert("trip.saveBillDetailsRoyalty", map);
            System.out.println("saveBillDetails size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int updateRoyaltyStatus(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("userId", userId);
        map.put("statusId", tripTO.getStatusId());
        System.out.println("the updateTSPinvoiceStatus" + map);
        int status = 0;
        int flag = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            //values(#tripSheetId#, #statusId#, #KM#, #HM#, #remarks#, #userId#, now())
            for (int i = 0; i < tripTO.getConsignmentOrderIds().length; i++) {
                System.out.println("value:" + tripTO.getConsignmentOrderIds()[i]);
                map.put("consignmentId", tripTO.getConsignmentOrderIds()[i]);
                System.out.println("updateRoyaltyinvoiceStatus map============" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatusForRoyaltyBill", map);
                System.out.println("updateConsignmentStatusForRoyaltyBill size=" + status);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStatus List", sqlException);
        }

        return status;
    }

    public int updateInvoiceRoyaltyTax(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("invoiceId", tripTO.getInvoiceId());
            if (tripTO.getIgstValue() != null && !"".equalsIgnoreCase(tripTO.getIgstValue()) && !"0".equalsIgnoreCase(tripTO.getIgstValue()) && !"0.00".equalsIgnoreCase(tripTO.getIgstValue())) {
                map.put("taxAmount", tripTO.getIgstValue());
                map.put("gstType", "IGST");
                map.put("taxPercentage", tripTO.getIgstPercentage());
                System.out.println("update Next Trip Status = " + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.insertInvoiceRoyaltyBillTax", map);
            }
            if (tripTO.getCgstValue() != null && !"".equalsIgnoreCase(tripTO.getCgstValue()) && !"0".equalsIgnoreCase(tripTO.getCgstValue()) && !"0.00".equalsIgnoreCase(tripTO.getCgstValue())) {
                map.put("taxAmount", tripTO.getCgstValue());
                map.put("gstType", "CGST");
                map.put("taxPercentage", tripTO.getCgstPercentage());
                System.out.println("update Next Trip Status = " + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.insertInvoiceRoyaltyBillTax", map);
            }
            if (tripTO.getSgstValue() != null && !"".equalsIgnoreCase(tripTO.getSgstValue()) && !"0".equalsIgnoreCase(tripTO.getSgstValue()) && !"0.00".equalsIgnoreCase(tripTO.getSgstValue())) {
                map.put("taxAmount", tripTO.getSgstValue());
                map.put("gstType", "SGST");
                map.put("taxPercentage", tripTO.getSgstPercentage());
                System.out.println("update Next Trip Status = " + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.insertInvoiceRoyaltyBillTax", map);
            }

            System.out.println("insertInvoiceRoyaltyBillTax =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateIdleBhattaStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateIdleBhattaStatus List", sqlException);
        }

        return status;
    }

    public ArrayList getOrderList(int custId, int tripId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList tempList = new ArrayList();
        map.put("custId", custId);
        map.put("tripId", tripId);

        try {
            tempList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderList", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getOrderList", sqlException);
        }
        return tempList;
    }

    //  from here pdf list
    public ArrayList getOrderInvoiceHeaderTsp(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        if ("tspInvoice".equals(tripTO.getInvoiceType())) {
            map.put("invoiceType", "1");
        } else if ("tspDestinationInvoice".equals(tripTO.getInvoiceType())) {
            map.put("invoiceType", "2");
        } else {
            map.put("invoiceType", "");
        }

        ArrayList result = new ArrayList();
        System.out.println("map = " + map);
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderInvoiceHeaderTSP", map);
            System.out.println("result size=" + result.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderInvoiceHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderInvoiceHeader List", sqlException);
        }

        return result;
    }

    public ArrayList getInvoiceGSTTaxDetailsTsp(TripTO tripTO) {
        Map map = new HashMap();

        map.put("invoiceId", tripTO.getInvoiceId());

        ArrayList gstDetails = new ArrayList();
        try {
            gstDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInvoiceGSTTaxDetailsTSP", map);
            System.out.println("gstDetails size=dea" + gstDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return gstDetails;
    }

    public ArrayList getOrderInvoiceHeaderRoyalty(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        ArrayList result = new ArrayList();
        System.out.println("map = " + map);
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderInvoiceHeaderRoyalty", map);
            System.out.println("result size=" + result.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderInvoiceHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderInvoiceHeader List", sqlException);
        }

        return result;
    }

    public ArrayList getInvoiceGSTTaxDetailsRoyalty(TripTO tripTO) {
        Map map = new HashMap();

        map.put("invoiceId", tripTO.getInvoiceId());

        ArrayList gstDetails = new ArrayList();
        try {
            gstDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInvoiceGSTTaxDetailsRoyalty", map);
            System.out.println("gstDetails size=dea" + gstDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return gstDetails;
    }

    public ArrayList getVehicleLHCRegNos(TripTO tripTO) {
        Map map = new HashMap();
        String vehicleNo = tripTO.getVehicleNo();
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("statusId", tripTO.getStatusId());
        System.out.println("vehicleNo = " + vehicleNo);
        map.put("vehicleNo", vehicleNo + "%");

        if ("2".equals(tripTO.getTripType())) {
            map.put("customerName", tripTO.getCustomerName());
        }
        map.put("roleId", tripTO.getRoleId());
        map.put("companyId", tripTO.getCompanyId());

        if ("1".equals(tripTO.getVendorId())) {
            map.put("ownerShip", "1");
            map.put("vendorId", "");
        } else {
            map.put("vendorId", tripTO.getVendorId());
            map.put("ownerShip", tripTO.getOwnerShip());
        }

        if (tripTO.getVehicleTypeId() != null && !"".equals(tripTO.getVehicleTypeId())) {
            map.put("vehicleTypeId", tripTO.getVehicleTypeId().split("-")[0]);
        } else {
            map.put("vehicleTypeId", tripTO.getVehicleTypeId());
        }

        map.put("consignmentId", tripTO.getConsignmentId());

        ArrayList vehicleNos = new ArrayList();
        String consignmentId = "";
        try {

            consignmentId = (String) getSqlMapClientTemplate().queryForObject("trip.getConsignmentOriginDestination", map);
            if (!"".equals(consignmentId)) {
                System.out.println("origin=" + consignmentId.split("-")[0]);
                System.out.println("destination=" + consignmentId.split("-")[1]);
                map.put("origin", consignmentId.split("-")[0]);
                map.put("destination", consignmentId.split("-")[1]);
            }
            System.out.println("map: in the dao for LHC " + map);
//            vehicleNos = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleLHCRegNos", map);
            vehicleNos = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleLHCRateCost", map);
            System.out.println("vehicleNos size LHC=" + vehicleNos.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return vehicleNos;
    }

    public String getOrderVehicleType(TripTO tripTO) {
        Map map = new HashMap();
        map.put("statusId", tripTO.getStatusId());

        map.put("roleId", tripTO.getRoleId());
        map.put("companyId", tripTO.getCompanyId());

        if ("1".equals(tripTO.getVendorId())) {
            map.put("ownerShip", "1");
            map.put("vendorId", "");
        } else {
            map.put("vendorId", tripTO.getVendorId());
            map.put("ownerShip", "");
        }
        if (tripTO.getVehicleTypeId() != null && !"".equals(tripTO.getVehicleTypeId())) {
            map.put("vehicleTypeId", tripTO.getVehicleTypeId().split("-")[0]);
        } else {
            map.put("vehicleTypeId", tripTO.getVehicleTypeId());
        }
        map.put("consignmentId", tripTO.getConsignmentId());

        String vehicleType = "";
        try {
            System.out.println("map: in the dao for LHC" + map);
            vehicleType = (String) getSqlMapClientTemplate().queryForObject("trip.getConsignmentVehicleType", map);
            System.out.println("vehicleTypevehicleTypevehicleTypevehicleType==" + vehicleType);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return vehicleType;
    }

    public ArrayList getLhcDetails(TripTO tripTO) {
        Map map = new HashMap();
        String[] consignmentNos = tripTO.getConsignmentOrderNos().split(",");
        List consignmentOrderNos = new ArrayList(consignmentNos.length);
        for (int i = 0; i < consignmentNos.length; i++) {
            System.out.println("value:" + consignmentNos[i]);
            consignmentOrderNos.add(consignmentNos[i]);
        }
        map.put("consignmentOrderNos", consignmentOrderNos);
        map.put("tripId", tripTO.getTripId());
        ArrayList lhcDetails = new ArrayList();
        try {

            System.out.println("map lhcDetails:" + map);
            lhcDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getLhcDetails", map);
            System.out.println("lhcDetails size=" + lhcDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("lhcDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "lhcDetails List", sqlException);
        }

        return lhcDetails;
    }
    

    public ArrayList getLhcVehicleRegNo(TripTO tripTO) {
        Map map = new HashMap();
        map.put("statusId", tripTO.getStatusId());
        String vehicleNos = tripTO.getVehicleNo();
        map.put("vehicleNo", vehicleNos + "%");
        map.put("roleId", tripTO.getRoleId());
        map.put("tripId", tripTO.getTripId());
        map.put("companyId", tripTO.getCompanyId());
        System.out.println("tripTO.getVehicleTypeId()-" + tripTO.getVehicleTypeId());
        System.out.println("vehicleNo = " + vehicleNos);
        map.put("vehicleTypeId", tripTO.getVehicleTypeId());

        ArrayList vehicleNo = new ArrayList();
        try {
            System.out.println("map: in the dao for LHC Veh No" + map);
            vehicleNo = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getLhcVehNo", map);
            System.out.println("vehicleNo==" + vehicleNo.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return vehicleNo;
    }

    public ArrayList getPrintTripSheetDetails(String tripSheetId, String printDocketNo) {
        Map map = new HashMap();
        ArrayList tripDetailsList = new ArrayList();
        try {

            map.put("tripSheetId", tripSheetId);
            map.put("lrNumber", printDocketNo);
            System.out.println("map for size:" + map);
            tripDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getPrintTripSheetDetails", map);
            System.out.println("tripDetailsList size=" + tripDetailsList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPrintTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPrintTripSheetDetails List", sqlException);
        }

        return tripDetailsList;
    }

    public ArrayList getLHCDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList getLHCDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            getLHCDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.viewLHCDetails", map);
            System.out.println("LHCDetails size...." + getLHCDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getLHCDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getLHCDetails List", sqlException);
        }
        return getLHCDetails;
    }

    public int updateGeneralTax(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("invoiceId", tripTO.getInvoiceId());
            if (tripTO.getIgstValue() != null && !"".equalsIgnoreCase(tripTO.getIgstValue()) && !"0".equalsIgnoreCase(tripTO.getIgstValue()) && !"0.00".equalsIgnoreCase(tripTO.getIgstValue())) {
                map.put("taxAmount", tripTO.getIgstValue());
                map.put("gstType", "IGST");
                map.put("taxPercentage", tripTO.getIgstPercentage());
                System.out.println("update hghcgg Trip Status = " + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.insertGeneralInvoiceTax", map);
            }
            if (tripTO.getCgstValue() != null && !"".equalsIgnoreCase(tripTO.getCgstValue()) && !"0".equalsIgnoreCase(tripTO.getCgstValue()) && !"0.00".equalsIgnoreCase(tripTO.getCgstValue())) {
                map.put("taxAmount", tripTO.getCgstValue());
                map.put("gstType", "CGST");
                map.put("taxPercentage", tripTO.getCgstPercentage());
                System.out.println("update Next hgfhf Status = " + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.insertGeneralInvoiceTax", map);
            }
            if (tripTO.getSgstValue() != null && !"".equalsIgnoreCase(tripTO.getSgstValue()) && !"0".equalsIgnoreCase(tripTO.getSgstValue()) && !"0.00".equalsIgnoreCase(tripTO.getSgstValue())) {
                map.put("taxAmount", tripTO.getSgstValue());
                map.put("gstType", "SGST");
                map.put("taxPercentage", tripTO.getSgstPercentage());
                System.out.println("update wqqf Trip Status = " + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.insertGeneralInvoiceTax", map);
            }

            System.out.println("insertGeneralInvoiceTax =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateIdleBhattaStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateIdleBhattaStatus List", sqlException);
        }

        return status;
    }

    public ArrayList getInvoiceGSTTaxDetails(TripTO tripTO) {
        Map map = new HashMap();

        map.put("invoiceId", tripTO.getInvoiceId());

        ArrayList gstDetails = new ArrayList();
        try {
            gstDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInvoiceGSTTax", map);
            System.out.println("gstDetails size=dea" + gstDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return gstDetails;
    }

    public ArrayList getConsDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        ArrayList consDetails = new ArrayList();
        try {
            consDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsDetails", map);
            System.out.println("consDetails size=" + consDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consDetails List", sqlException);
        }

        return consDetails;
    }

    public int consignmentApproveRequest(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("userId", tripTO.getUserId());
            map.put("lhcid", tripTO.getLhcId());
            String[] consignmentOrderIds = tripTO.getConsignmentOrderId();
            for (int i = 0; i < consignmentOrderIds.length; i++) {
                map.put("consignmentOrderId", consignmentOrderIds[i]);
                System.out.println("map value is:" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.consignmentApproveRequest", map);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateLHCStatus", map);
                System.out.println("requset : " + status);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("consignmentApproveRequest Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentApproveRequest", sqlException);
        }
        return status;
    }

    public ArrayList consignmentApproval(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList consignmentApproval = new ArrayList();
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        try {
            consignmentApproval = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getconsignApprovalDetails", map);
            System.out.println("consignmentApproval.size() = " + consignmentApproval.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentApproval List", sqlException);
        }
        return consignmentApproval;
    }

    public ArrayList viewConsignmentApprovalDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList consignmentApproval = new ArrayList();
        map.put("consId", tripTO.getConsignmentId());

        try {
            consignmentApproval = (ArrayList) getSqlMapClientTemplate().queryForList("trip.viewConsignmentApprovalDetails", map);
            System.out.println("consignmentApproval.size() = " + consignmentApproval.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentApproval List", sqlException);
        }
        return consignmentApproval;
    }

    public String getConsignVendorDetail(TripTO tripTO) {
        Map map = new HashMap();
        String vendordetail = "";

        map.put("consId", tripTO.getConsignmentId());

        try {
            System.out.println("map:" + map);
            vendordetail = (String) getSqlMapClientTemplate().queryForObject("trip.getConsignVendorDetail", map);

            if (vendordetail == null) {
                vendordetail = "NA~0";
            }

            System.out.println(" getConsignVendorDetail " + vendordetail);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignVendorDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignVendorDetail List", sqlException);
        }

        return vendordetail;
    }

    public int saveConsignmentApproval(String consid, String approvalStatus, String userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("consignmentOrderId", consid);
        map.put("approvalStatus", approvalStatus);
        map.put("userId", userId);
        try {
            if (approvalStatus.equalsIgnoreCase("2")) {
                status = (Integer) getSqlMapClientTemplate().update("trip.saveConsignmentApprove", map);
            } else {
                System.out.println("the saveConsignmentApproval" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.saveConsignmentReject", map);
                status = (Integer) getSqlMapClientTemplate().update("trip.releaseLHCStatus", map);
            }
            System.out.println("saveConsignmentApproval size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveConsignmentApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveConsignmentApproval List", sqlException);
        }

        return status;
    }

    public String getCustomereFSId(String custid) {
        Map map = new HashMap();
        String response = "";
        try {
            map.put("custid", custid);
            response = (String) getSqlMapClientTemplate().queryForObject("trip.getCustomereFSId", map);
            System.out.println("response=" + response);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomereFSId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomereFSId", sqlException);
        }
        return response;
    }

    public ArrayList viewVehicleLocation(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList statusDetails = new ArrayList();
        try {
            map.put("vehNo", tripTO.getVehicleNo());
            System.out.println(" Trip map is::" + map);
            statusDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleLocation", map);
            System.out.println("viewVehicleLocation.size() = " + statusDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("viewVehicleLocation Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "viewVehicleLocation List", sqlException);
        }
        return statusDetails;
    }

    public ArrayList getRequestedLHC(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList statusDetails = new ArrayList();
        try {
            String[] consignmentNos = tripTO.getConsignmentOrderNos().split(",");
            List consignmentOrderNos = new ArrayList(consignmentNos.length);
            for (int i = 0; i < consignmentNos.length; i++) {
                System.out.println("value iss: " + consignmentNos[i]);
                consignmentOrderNos.add(consignmentNos[i]);
            }
            map.put("consignmentOrderNos", consignmentOrderNos);
            statusDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getRequestedLHC", map);
            System.out.println("response=" + statusDetails);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRequestedLHC Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRequestedLHC", sqlException);
        }
        return statusDetails;
    }

    public String insertTouchPoint(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        String docketNo = "";

        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("lrNumber", tripTO.getLrNumber());
        map.put("docketNo", tripTO.getDocketNo());
        map.put("invoiceNumber", tripTO.getInvoiceNumber());
        map.put("invoiceValue", tripTO.getInvoiceValue());
        map.put("userId", tripTO.getInvoiceValue());
        map.put("invoiceDate", tripTO.getInvoiceDate());
        docketNo = tripTO.getDocketNo();
//        map.put("eWayBillNo", "NA" );
        map.put("userId", userId);
        System.out.println("insertTouchPoint" + map);
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.UpdateEndTouchPointDetails", map);
            System.out.println("insertTripFuel size=" + tripDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertTripFuel Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertTripFuel List", sqlException);
        }

        return docketNo;
    }

    public ArrayList getLrNumber(String tripSheetId) {
        Map map = new HashMap();
        ArrayList getLrNumber = new ArrayList();
        try {

            map.put("tripSheetId", tripSheetId);
            System.out.println("map for size:" + map);
            getLrNumber = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getLrNumber", map);
            System.out.println("getLrNumber size=" + getLrNumber.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLrNumber Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getLrNumber List", sqlException);
        }

        return getLrNumber;
    }

    public ArrayList getTripInvoiceDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        ArrayList tripInvoiceDetails = new ArrayList();
        System.out.println("map in trip start details = " + map);

        try {
            tripInvoiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripInvoiceDetails", map);
            System.out.println("getTripInvoiceDetails in DAO if size = " + tripInvoiceDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripInvoiceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripInvoiceDetails List", sqlException);
        }

        return tripInvoiceDetails;
    }

    public String getlhcContractDetails(TripTO tripTO) {
        Map map = new HashMap();
        String getlhcDetails = "";
        try {
            map.put("vehicleType", tripTO.getVehicleTypeId());
            map.put("originId", tripTO.getOriginId());
            map.put("destinationId", tripTO.getDestinationId());
            map.put("vehicleVendorId", tripTO.getVendorId());
            map.put("ownerShip", tripTO.getOwnerShip());
            System.out.println("lhcDetails=" + map);
            getlhcDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getlhcContractDetails", map);
            System.out.println("getlhcDetails=" + getlhcDetails);

            if (getlhcDetails == null) {
                getlhcDetails = "";
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getlhcDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getlhcDetails", sqlException);
        }
        return getlhcDetails;
    }

    public ArrayList getConsignorDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("originId", tripTO.getOriginId());
        map.put("customerId", tripTO.getCustomerId());

        ArrayList getConsignorDetails = new ArrayList();
        try {
            System.out.println("map======" + map);
            getConsignorDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignorDetails", map);
            System.out.println("getConsignorDetails==" + getConsignorDetails);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignorDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignorDetails List", sqlException);
        }

        return getConsignorDetails;
    }

    public String UpdateSaveTripDetails(TripTO tripTO, int userId, SqlMapClient session) {
        synchronized (this) {
            Map map = new HashMap();

            int status = 0;

            try {
                map.put("tripId", tripTO.getTripSheetId());
                System.out.println(" update strt trip : " + map);
                status = (Integer) session.update("trip.updateTripDetails", map);
                System.out.println("status size=" + status);
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("updateStartTripSheet Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheet List", sqlException);
            }
            return status + "";
        }
    }

    public ArrayList getTripDateDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripSheetId());

        ArrayList getTripDateDetails = new ArrayList();
        try {
            System.out.println("map======" + map);

            getTripDateDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripDateDetails", map);
            System.out.println("getTripDateDetails==" + getTripDateDetails);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDateDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDateDetails List", sqlException);
        }

        return getTripDateDetails;
    }

    public ArrayList getTriptouchpointDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripSheetId());

        ArrayList getTriptouchpointDetails = new ArrayList();
        try {
            System.out.println("map : " + map);

            getTriptouchpointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTriptouchpointDetails", map);
            System.out.println("getTriptouchpointDetails : " + getTriptouchpointDetails);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTriptouchpointDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTriptouchpointDetails List", sqlException);
        }

        return getTriptouchpointDetails;
    }

    public String checkDocketNoIsExist(TripTO tripTO) {
        Map map = new HashMap();
        map.put("statusId", tripTO.getStatusId());
        String lrnumber = tripTO.getLrNumber();
        map.put("lrnumber", lrnumber + "");
        map.put("roleId", tripTO.getRoleId());
        map.put("companyId", tripTO.getCompanyId());
        System.out.println("lrnumber = " + lrnumber);

        String Lrnumber = "";
        try {
            System.out.println("map: in the dao for LHC Veh No" + map);
            Lrnumber = (String) getSqlMapClientTemplate().queryForObject("trip.checkDocketNoIsExist", map);
            System.out.println("Lrnumber :: " + Lrnumber);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkDocketNoIsExist Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkDocketNoIsExist List", sqlException);
        }

        return Lrnumber;
    }

    public int saveCustomerInvoice(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int invoiceid = 0;
        String invoiceNo = "";
        synchronized (this) {

            DateFormat dateFormat = new SimpleDateFormat("yy");
            DateFormat dateFormat1 = new SimpleDateFormat("MM");
            Date date = new Date();
            System.out.println("dateFormat1.format(date)-yy--" + dateFormat.format(date));
            System.out.println("dateFormat.format(date)-mm--" + dateFormat1.format(date));
            int nextYear = Integer.parseInt(dateFormat.format(date)) + 1;

            SimpleDateFormat ft1 = new SimpleDateFormat("dd-MMM-yy");
            SimpleDateFormat ft2 = new SimpleDateFormat("dd-MM-yyyy");

            try {

                map.put("custid", tripTO.getCustomerId());
                String invoiceDate = "";
                int postId = 0;
                double invoice_amount = 0;
                double cgstAmt = 0;
                double sgstAmt = 0;
                double igstAmt = 0;
                double net_amount = 0;
                double totalTaxAmt = 0;

                String igstPer = "0";
                String sgstPer = "0";
                String cgstPer = "0";
                String consignorCode = "0";
                String consigneeCode = "0";

                String compGSTNo = tripTO.getGstNo();

                String eFSId = (String) session.queryForObject("trip.getCustomereFSId", map);

                if (eFSId != null && (!"".equals(eFSId))) {

                    int tripCodeSequence = (Integer) session.insert("trip.getInvoiceCodeSequence", map);
                    System.out.println("tripCodeSequence : " + tripCodeSequence);

                    String invoiceCode = tripCodeSequence + "";

                    if (invoiceCode == null) {
                        invoiceCode = "000001";
                    } else if (invoiceCode.length() == 1) {
                        invoiceCode = "00000" + invoiceCode;
                    } else if (invoiceCode.length() == 2) {
                        invoiceCode = "0000" + invoiceCode;
                    } else if (invoiceCode.length() == 3) {
                        invoiceCode = "000" + invoiceCode;
                    } else if (invoiceCode.length() == 4) {
                        invoiceCode = "00" + invoiceCode;
                    } else if (invoiceCode.length() == 5) {
                        invoiceCode = "0" + invoiceCode;
                    } else if (invoiceCode.length() == 6) {
                        invoiceCode = "" + invoiceCode;
                    }

                    invoiceNo = "" + dateFormat.format(date) + dateFormat1.format(date) + invoiceCode;
                    System.out.println("invoiceNo : " + invoiceNo);

                    String invref = "INV/" + dateFormat.format(date) + "-" + nextYear + "/" + invoiceCode;
                    System.out.println("invref : " + invref);

                    tripTO.setInvoiceCode(invoiceCode);
                    tripTO.setInvoiceNo(invoiceNo);
                    tripTO.setInvRefNo(invref);
                    invoiceDate = tripTO.getInvoiceDate();

                    ////////////// invoice header part /////////////////////////
                    double grandTotal = Double.parseDouble(tripTO.getGrandTotal()) + Double.parseDouble(tripTO.getCgstAmt()) + Double.parseDouble(tripTO.getSgstAmt()) + Double.parseDouble(tripTO.getIgstAmt());
                    double grandRoundTotal = Double.parseDouble(tripTO.getGrandTotalRound()) + Double.parseDouble(tripTO.getCgstAmtRound()) + Double.parseDouble(tripTO.getSgstAmtRound()) + Double.parseDouble(tripTO.getIgstAmtRound());

                    double invoiceAmount = Double.parseDouble(tripTO.getTotalRevenue()) + Double.parseDouble(tripTO.getTotalExpToBeBilled());
                    double invoiceAmountRound = Double.parseDouble(tripTO.getFreightAmountRound()) + Double.parseDouble(tripTO.getTotalExpToBeBilled());

                    map.put("invoiceDate", tripTO.getInvoiceDate());
                    map.put("invoiceType", "CREDIT");
                    map.put("gstLevy", "REGULAR");
                    map.put("placeofSupply", "NA");
                    map.put("billingId", tripTO.getBillingId());
                    map.put("companyId", tripTO.getCompanyId());
                    map.put("userId", tripTO.getUserId());
                    map.put("invoiceCode", tripTO.getInvoiceCode());
                    map.put("invoiceNo", tripTO.getInvoiceNo());
                    map.put("invoiceRefNo", tripTO.getInvRefNo());
                    map.put("billing", "NA");
                    map.put("customerId", tripTO.getCustomerId());
                    map.put("billType", "1");
                    map.put("cancellationFlag", "0");
                    map.put("noOfTrips", tripTO.getNoOfTrips());

                    map.put("totalRevenue", tripTO.getTotalRevenue());
                    map.put("totalExpToBeBilled", tripTO.getTotalExpToBeBilled());
                    map.put("invoiceAmount", invoiceAmount);
                    map.put("grandTotal", grandTotal);

                    map.put("freightAmountRound", tripTO.getFreightAmountRound());
                    map.put("grandTotalRound", grandRoundTotal);
                    map.put("invoiceAmountRound", invoiceAmountRound);

                    map.put("cgstPercentage", tripTO.getCgst());
                    map.put("sgstPercentage", tripTO.getSgst());
                    map.put("igstPercentage", tripTO.getIgst());

                    map.put("cgstAmount", tripTO.getCgstAmt());
                    map.put("sgstAmount", tripTO.getSgstAmt());
                    map.put("igstAmount", tripTO.getIgstAmt());

                    map.put("cgstAmountRound", tripTO.getCgstAmtRound());
                    map.put("sgstAmountRound", tripTO.getSgstAmtRound());
                    map.put("igstAmountRound", tripTO.getIgstAmtRound());

                    System.out.println("the saveBillHeader" + map);

                    invoiceid = (Integer) session.insert("trip.saveBillHeader", map);

                    ////////////// invoice header part ////////////////////////
                    ////////////// invoice detail part ////////////////////////
                    ArrayList tripsToBeBilledDetails = new ArrayList();

                    String[] tripIds = tripTO.getTripIds();
                    String[] consignmentOrderIds = tripTO.getConsignmentOrderIds();
                    List tripSheetIds = new ArrayList(tripIds.length);
                    List consignmentIds = new ArrayList(consignmentOrderIds.length);

                    for (int i = 0; i < tripIds.length; i++) {
                        System.out.println("value:" + tripIds[i]);
                        tripSheetIds.add(tripIds[i]);
                    }

                    for (int j = 0; j < consignmentOrderIds.length; j++) {
                        System.out.println("cn value:" + consignmentOrderIds[j]);
                        consignmentIds.add(consignmentOrderIds[j]);
                    }

                    String[] date1 = tripTO.getFromDate().split("-");
                    String month = date1[1];
                    String year = date1[2];
                    map.put("month", month);
                    map.put("Year", year);
                    map.put("consignmentOrderIds", consignmentIds);
                    map.put("tripSheetIds", tripSheetIds);
                    map.put("customerId", tripTO.getCustomerId());
                    map.put("InvoiceFlag", tripTO.getInvoiceFlag());

                    String shipperAndConsignee = (String) session.queryForObject("trip.getShipperAndConsigneeCode", map);

                    String[] shipperConsignee = shipperAndConsignee.split("-");

                    consignorCode = shipperConsignee[0];
                    System.out.println("consignorCode : " + consignorCode);

                    consigneeCode = shipperConsignee[1];
                    System.out.println("consigneeCode : " + consigneeCode);

                    System.out.println("map for trip det:" + map);
                    tripsToBeBilledDetails = (ArrayList) session.queryForList("trip.getTripsToBeBilledDetails", map);
                    System.out.println("tripsToBeBilledDetails size=" + tripsToBeBilledDetails.size());

                    ////////////// invoice detail part ////////////////////////
                    int invoiceDetailId = 0;
                    int status = 0;

                    if (invoiceid > 0) {
                        Iterator itr = tripsToBeBilledDetails.iterator();
                        TripTO tripTONew = null;

                        while (itr.hasNext()) {
                            tripTONew = new TripTO();
                            tripTONew = (TripTO) itr.next();
                            tripTONew.setInvoiceId("" + invoiceid);
                            tripTONew.setInvoiceCode(invoiceCode);

                            tripTONew.setIgstPercentage(tripTO.getIgst());
                            tripTONew.setSgstPercentage(tripTO.getSgst());
                            tripTONew.setCgstPercentage(tripTO.getCgst());

                            tripTONew.setTripSheetId(tripTONew.getTripId());
                            tripTONew.setStatusId("16");

                            map.put("invoiceId", tripTONew.getInvoiceId());
                            map.put("userId", tripTO.getUserId());
                            map.put("customerId", tripTO.getCustomerId());
                            map.put("invoiceNo", tripTO.getInvoiceNo());
                            map.put("invoiceCode", tripTO.getInvoiceCode());
                            map.put("invoiceId", invoiceid);
                            map.put("tripId", tripTONew.getTripId());
                            map.put("consignmentId", tripTONew.getConsignmentId());

                            String totalExpToBeBilled = tripTONew.getExpenseToBeBilledToCustomer();

                            System.out.println("trip id  " + tripTONew.getTripId());
                            System.out.println("totalExpToBeBilled " + totalExpToBeBilled);
                            System.out.println("EstimatedRevenue() " + tripTONew.getEstimatedRevenue());

                            if (!"".equals(totalExpToBeBilled) && totalExpToBeBilled != null) {
                                totalExpToBeBilled = totalExpToBeBilled;
                            } else {
                                totalExpToBeBilled = "0.00";
                            }

                            double grandTotals = Double.parseDouble(tripTONew.getEstimatedRevenue()) + Double.parseDouble(totalExpToBeBilled);

                            double igst = Double.parseDouble(tripTO.getIgst());
                            double cgst = Double.parseDouble(tripTO.getCgst());
                            double sgst = Double.parseDouble(tripTO.getSgst());

                            double taxCGST = 0;
                            double taxSGST = 0;
                            double taxIGST = 0;

                            if (igst > 0) {
                                taxIGST = ((grandTotals) * igst) / 100;
                                taxCGST = Double.parseDouble("0");
                                taxCGST = Double.parseDouble("0");
                            } else {
                                taxCGST = ((grandTotals) * cgst) / 100;
                                taxSGST = ((grandTotals) * sgst) / 100;
                                taxIGST = Double.parseDouble("0");
                            }

                            grandTotals = grandTotals + taxCGST + taxSGST + taxIGST;

                            tripTONew.setCgstValue(taxCGST + "");
                            tripTONew.setSgstValue(taxSGST + "");
                            tripTONew.setIgstValue(taxIGST + "");

                            map.put("totalRevenue", tripTONew.getEstimatedRevenue());
                            map.put("totalExpToBeBilled", totalExpToBeBilled);
                            map.put("grandTotal", grandTotals);
                            map.put("cgstAmount", taxCGST);
                            map.put("sgstAmount", taxSGST);
                            map.put("igstAmount", taxIGST);

                            System.out.println("the saveBillDetails:" + map);

                            invoiceDetailId = (Integer) session.insert("trip.saveBillDetails", map);
                            System.out.println("saveBillDetails size=" + invoiceDetailId);

                            map.put("statusId", "16");
                            map.put("remarks", "Billed");
                            status = (Integer) session.update("trip.updateBillTripSheet", map);
                            status = (Integer) session.update("trip.updateBillConsignment", map);
                            status = (Integer) session.update("trip.saveTripStatusDetails", map);

                            for (int i = 0; i < tripIds.length; i++) {
                                System.out.println("value:" + tripIds[i]);
                                tripSheetIds.add(tripIds[i]);
                            }
                            for (int j = 0; j < consignmentOrderIds.length; j++) {
                                System.out.println("cn value:" + consignmentOrderIds[j]);
                                consignmentIds.add(consignmentOrderIds[j]);
                            }

                            System.out.println("mappp--" + map);

                        }

                        //////////////////// Expense part ///////////////////////////
                        tripTO.setExpenseType("3");
                        map.put("consignmentOrderIds", consignmentIds);
                        map.put("tripSheetIds", tripSheetIds);
                        map.put("expenseType", tripTO.getExpenseType());
                        map.put("customerId", tripTO.getCustomerId());

//                        ArrayList tripsOtherExpenseDetails = new ArrayList();
//                        tripsOtherExpenseDetails = (ArrayList) session.queryForList("trip.getTripsOtherExpenseDetails", map);
//                        System.out.println("tripsOtherExpenseDetails size=" + tripsOtherExpenseDetails.size());
//                        Iterator itr1 = tripsOtherExpenseDetails.iterator();
//
//                        TripTO tripTONew1 = null;
//
//                        while (itr1.hasNext()) {
//                            tripTONew1 = new TripTO();
//                            tripTONew1 = (TripTO) itr1.next();
//                            tripTONew1.setTripId(tripTONew.getTripId());
//                            tripTONew1.setInvoiceId("" + invoiceid);
//                            tripTONew1.setInvoiceDetailId("" + invoiceDetailId);
//                            tripTONew1.setInvoiceCode(invoiceCode);
//
//                            map.put("userId", tripTO.getUserId());
//                            map.put("invoiceCode", tripTO.getInvoiceCode());
//                            map.put("invoiceDetailId", tripTONew1.getInvoiceDetailId());
//                            map.put("tripId", tripTONew.getTripId());
//                            map.put("expenseId", tripTONew1.getExpenseId());
//                            map.put("expenseName", tripTONew1.getExpenseName() + "; " + tripTONew1.getExpenseRemarks());
//                            map.put("marginValue", tripTONew1.getMarginValue());
//                            map.put("taxPercentage", tripTONew1.getTaxPercentage());
//                            map.put("expenseValue", tripTONew1.getExpenseValue());
//
//                            Float totalValue = 0.00F;
//                            Float taxValue = 0.00F;
//                            Float nettValue = 0.00F;
//                            String marginValue = "0";
//                            String expenseValue = "0";
//                            
//                            String taxPercentage = "0";
//
//                            if (tripTONew1.getMarginValue() == null || "".equals(tripTONew1.getMarginValue())) {
//                                marginValue = "0";
//                            } else {
//                                marginValue = tripTONew1.getMarginValue();
//                            }
//                            if (tripTONew1.getTaxPercentage() == null || "".equals(tripTONew1.getTaxPercentage())) {
//                                taxPercentage = "0";
//                            } else {
//                                taxPercentage = tripTONew1.getTaxPercentage();
//                            }
//                            if (tripTONew1.getExpenseValue() == null || "".equals(tripTONew1.getExpenseValue())) {
//                                expenseValue = "0";
//                            } else {
//                                expenseValue = tripTONew1.getExpenseValue();
//                            }
//                            totalValue = (Float.parseFloat(expenseValue) + Float.parseFloat(marginValue));
//                            taxValue = ((Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
//                                    * Float.parseFloat(taxPercentage) / 100);
//                            nettValue = (Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
//                                    + ((Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
//                                    * Float.parseFloat(taxPercentage) / 100);
//                            map.put("taxValue", taxValue);
//                            map.put("totalValue", totalValue);
//                            map.put("nettValue", nettValue);
//
//                            System.out.println("the saveBillDetailExpense:" + map);
//
//                            status = (Integer) session.update("trip.saveBillDetailExpense", map);
//                            System.out.println("saveBillDetailExpense size=" + status);
//
//                            //////////////////// Expense part ///////////////////////////
//                        }
                        ////////////////////// Invoice Web service Part ////////////////////////////////////////////////////// 
                        invoice_amount = Double.parseDouble(tripTO.getFreightAmountRound()) + Double.parseDouble(tripTO.getTotalExpToBeBilled());
                        totalTaxAmt = Math.round(invoice_amount);

                        cgstAmt = Double.parseDouble(tripTO.getCgstAmtRound());
                        sgstAmt = Double.parseDouble(tripTO.getSgstAmtRound());
                        igstAmt = Double.parseDouble(tripTO.getIgstAmtRound());

                        net_amount = invoice_amount + cgstAmt + sgstAmt + igstAmt;
                        net_amount = Math.round(net_amount);

                        igstPer = tripTO.getIgst();
                        sgstPer = tripTO.getSgst();
                        cgstPer = tripTO.getCgst();
                        compGSTNo = tripTO.getGstNo();

                        System.out.println("cgstAmt : " + cgstAmt + " per:" + cgstPer);
                        System.out.println("sgstAmt : " + sgstAmt + " per:" + sgstPer);
                        System.out.println("igstAmt : " + igstAmt + " per:" + igstPer);

                        /////////////////////// posting details ///////////////////////  
                        Date dDate = ft2.parse(invoiceDate);
                        String today = ft1.format(dDate);

                        map.put("customerEFSId", eFSId);
                        map.put("customerGST", compGSTNo);
                        map.put("branchEFSId", ThrottleConstants.branchId);
                        map.put("invoiceNo", invoiceNo);
                        map.put("invoiceDate", today);
                        map.put("totalFreightCharge", invoice_amount + "");
                        map.put("taxableAmount", totalTaxAmt);
                        map.put("taxableAmountRound", totalTaxAmt);
                        map.put("CGST", cgstPer);
                        map.put("CGSTAmt", cgstAmt);
                        map.put("CGSTAmtRound", cgstAmt);
                        map.put("SGST", sgstPer);
                        map.put("SGSTAmt", sgstAmt);
                        map.put("SGSTAmtRound", sgstAmt);
                        map.put("IGST", igstPer);
                        map.put("IGSTAmt", igstAmt);
                        map.put("IGSTAmtRound", igstAmt);
                        map.put("grandTotal", net_amount);
                        map.put("grandTotalRound", net_amount);
                        map.put("postingStatus", "0");
                        map.put("inv_type", "Customer");
                        map.put("invoiceId", invoiceid);
                        map.put("consigneeCode", consigneeCode);
                        map.put("consignorCode", consignorCode);

                        System.out.println("customer invoice map : " + map);
                        postId = (Integer) session.insert("trip.insertInvoicePostStatusDetails", map);
                        System.out.println("postStatus--" + postId);

                        /////////////////////// posting details ///////////////////////
                        if (postId > 0) {
                           // new sendInvoice(eFSId, invoiceNo, today, invoice_amount + "", totalTaxAmt + "", cgstPer, cgstAmt + "", sgstPer, sgstAmt + "", igstPer, igstAmt + "", net_amount + "", compGSTNo, postId, consignorCode, consigneeCode).start();
                        }
                    }

                    tripTO.setInvoiceId(invoiceid + "");

                }

            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("saveCustomerInvoice Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-MRS-01", CLASS, "saveCustomerInvoice List", sqlException);
            }
        }

        return invoiceid;
    }
    
        public int cancelTrip(String tripSheetId, String tripStatusId, String cancelRemarks, int userId) {
        Map map = new HashMap();
        int cancelTrip = 0;
        map.put("userId", userId);
        try {
            try {
                map.put("tripSheetId", tripSheetId);
                map.put("tripStatusId", tripStatusId);
                map.put("remarks", cancelRemarks);
                map.put("updateType", "System Update");
                map.put("userId", userId);
                System.out.println("mpa = " + map);
                cancelTrip = (Integer) getSqlMapClientTemplate().update("trip.cancelTrip", map);
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("saveCustomerInvoice Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-MRS-01", CLASS, "saveCustomerInvoice List", sqlException);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("cancelTrip Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "cancelTrip", sqlException);
        }

        return cancelTrip;
    }
        
        
        public ArrayList getEcomDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("documentRequired", tripTO.getDocumentRequired());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("consignmentNo", tripTO.getConsignmentNo());
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        map.put("customerName", tripTO.getCustomerId());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("statusId", tripTO.getStatusId());
        map.put("tripStatusId", tripTO.getTripStatusId());
        map.put("roleId", tripTO.getRoleId());
        map.put("vehicleTypeId", tripTO.getVehicleTypeId());
        map.put("companyId", tripTO.getCompanyId());
        if (!"".equals(tripTO.getTripCode()) && tripTO.getTripCode() != null) {
            map.put("tripCode", "%" + tripTO.getTripCode() + "%");
        } else {
            map.put("tripCode", "");
        }
//        map.put("tripCode", "%"+tripTO.getTripCode() + "%");
        map.put("docketNo", tripTO.getDocketNo());
        map.put("zoneId", tripTO.getZoneId());
        map.put("fleetCenterId", tripTO.getFleetCenterId());
        map.put("cityFromId", tripTO.getCityFromId());
        map.put("podStatus", tripTO.getPodStatus());
        map.put("tripType", tripTO.getTripType());
        map.put("extraExpenseStatus", tripTO.getExtraExpenseStatus());

        map.put("userId", tripTO.getUserId());
        map.put("userMovementType", tripTO.getUserMovementType());
        String empId = (String) getSqlMapClientTemplate().queryForObject("customer.getEmployeeId", map);
        map.put("empId", empId);
        System.out.println("map = " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEcomDetails", map);
            System.out.println("getTripSheetDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripSheetDetails List", sqlException);
        }

        return tripDetails;
    }
        
        
        public ArrayList getDriverDetails(TripTO tripTO) {
        Map map = new HashMap();
        String[] consignmentNos = tripTO.getConsignmentOrderNos().split(",");
        List consignmentOrderNos = new ArrayList(consignmentNos.length);
        for (int i = 0; i < consignmentNos.length; i++) {
            System.out.println("value:" + consignmentNos[i]);
            consignmentOrderNos.add(consignmentNos[i]);
        }
        map.put("consignmentOrderNos", consignmentOrderNos);
        map.put("tripId", tripTO.getTripId());
        ArrayList driverDetails = new ArrayList();
        try {

            System.out.println("map driverDetails:" + map);
            driverDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverDetails", map);
            System.out.println("driverDetails size=" + driverDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("driverDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "driverDetails List", sqlException);
        }

        return driverDetails;
    }
        public ArrayList getMovementDetails(TripTO tripTO) {
        Map map = new HashMap();
        String[] consignmentNos = tripTO.getConsignmentOrderNos().split(",");
        List consignmentOrderNos = new ArrayList(consignmentNos.length);
        for (int i = 0; i < consignmentNos.length; i++) {
            System.out.println("value:" + consignmentNos[i]);
            consignmentOrderNos.add(consignmentNos[i]);
        }
        map.put("consignmentOrderNos", consignmentOrderNos);
        map.put("tripId", tripTO.getTripId());
        ArrayList movementDetails = new ArrayList();
        try {

            System.out.println("map movementDetails:" + map);
            movementDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getMovementDetails", map);
            System.out.println("movementDetails size=" + movementDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("movementDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "movementDetails List", sqlException);
        }

        return movementDetails;
    }
        
         public int insertEcomDetails(TripTO tripTO, int userId,String lrNumber) {
        Map map = new HashMap();
        int status = 0;
        int Update = 0;
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("lrNumber", lrNumber);
        map.put("lrNumberEcom", lrNumber+"/Ecom");
        map.put("billOverTime", tripTO.getBillOverTime() + ":" + tripTO.getBillOverTimemin() + ":00");
        map.put("receivedTime", tripTO.getReceivedTime() + ":" + tripTO.getReceivedTimemin()+ ":00");
        map.put("appointmentTime", tripTO.getAppointTime() + ":" + tripTO.getAppointTimemin()+ ":00");
        map.put("ewayBillNo", tripTO.getEwayBillNo());
        map.put("appointDate", tripTO.getAppointDate());
        map.put("expDate", tripTO.getExpDate());
        map.put("userId", userId);
        map.put("invoiceNo", tripTO.getInvoiceNo());
            System.out.println("insertEcomDetails:" + map);
            status = (Integer) getSqlMapClientTemplate().insert("trip.insertEcomDetails", map);
            System.out.println("insertEcomDetails=" + status);
            
            if (status>0){
                
               Update = (Integer) getSqlMapClientTemplate().update("trip.UpdateEcomDetail", map);  
                
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripStatusDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripStatusDetails", sqlException);
        }
        return status;
    }
    
    
     public String getLrNumberEcom(TripTO tripTO) {
        Map map = new HashMap();
        String getLrNumber = "";
        try {

            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println("map for size:" + map);
            getLrNumber = (String) getSqlMapClientTemplate().queryForObject("trip.getLrNumberEcom", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLrNumber Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getLrNumber List", sqlException);
        }

        return getLrNumber;
    }
}
