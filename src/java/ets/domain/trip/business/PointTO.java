/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ets.domain.trip.business;

/**
 *
 * @author srini
 */
public class PointTO {
    private String movementTypeId = "";
    private String id = "";
    private String lhcId = "";
    private String vendorId = "";
    private String lhcNo = "";
    private String vehicleNo = "";
    private String vehicleTypeId = "";
    private String driverName = "";
    private String driverMobile = "";
    private String dlNo = "";
    private String awbNo = "";
    private String loadedpackagesNew = "";
    private String totalChargeableWeight = "";
    private String pendingGrossWeight = "";
    private String startReportingDate = "";
    private String startReportingTime = "";
    private String loadingTime = "";
    private String loadingDate = "";
    private String loadingTemperature = "";
    private String consignmentOrderId = "";
    private String consignmentOrderNo = "";
    private String tripId = "";
    private String tripRouteCourseId = "";
    private String consignmentRouteCourseId = "";
    private String pointId = "";
    private String pointName = "";
    private String pointType = "";
    private String pointSequence = "";
    private String pointAddress = "";
    private String pointPlanDate = "";
    private String pointPlanTime = "";
    private String pointPlanTimeHrs = "";
    private String pointPlanTimeMins = "";
    private String pointDistance = "";
    private String totalPackages = "";
    private String totalWeight = "";
    private String pendingPackages = "";
    private String pendingWeight = "";

    public String getConsignmentOrderId() {
        return consignmentOrderId;
    }

    public void setConsignmentOrderId(String consignmentOrderId) {
        this.consignmentOrderId = consignmentOrderId;
    }

    public String getPointAddress() {
        return pointAddress;
    }

    public void setPointAddress(String pointAddress) {
        this.pointAddress = pointAddress;
    }

    public String getPointId() {
        return pointId;
    }

    public void setPointId(String pointId) {
        this.pointId = pointId;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public String getPointPlanDate() {
        return pointPlanDate;
    }

    public void setPointPlanDate(String pointPlanDate) {
        this.pointPlanDate = pointPlanDate;
    }

    public String getPointPlanTime() {
        return pointPlanTime;
    }

    public void setPointPlanTime(String pointPlanTime) {
        this.pointPlanTime = pointPlanTime;
    }

    public String getPointSequence() {
        return pointSequence;
    }

    public void setPointSequence(String pointSequence) {
        this.pointSequence = pointSequence;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getPointPlanTimeHrs() {
        return pointPlanTimeHrs;
    }

    public void setPointPlanTimeHrs(String pointPlanTimeHrs) {
        this.pointPlanTimeHrs = pointPlanTimeHrs;
    }

    public String getPointPlanTimeMins() {
        return pointPlanTimeMins;
    }

    public void setPointPlanTimeMins(String pointPlanTimeMins) {
        this.pointPlanTimeMins = pointPlanTimeMins;
    }

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public String getTripRouteCourseId() {
        return tripRouteCourseId;
    }

    public void setTripRouteCourseId(String tripRouteCourseId) {
        this.tripRouteCourseId = tripRouteCourseId;
    }

    public String getConsignmentRouteCourseId() {
        return consignmentRouteCourseId;
    }

    public void setConsignmentRouteCourseId(String consignmentRouteCourseId) {
        this.consignmentRouteCourseId = consignmentRouteCourseId;
    }

    public String getConsignmentOrderNo() {
        return consignmentOrderNo;
    }

    public void setConsignmentOrderNo(String consignmentOrderNo) {
        this.consignmentOrderNo = consignmentOrderNo;
    }

    public String getLoadingDate() {
        return loadingDate;
    }

    public void setLoadingDate(String loadingDate) {
        this.loadingDate = loadingDate;
    }

    public String getLoadingTemperature() {
        return loadingTemperature;
    }

    public void setLoadingTemperature(String loadingTemperature) {
        this.loadingTemperature = loadingTemperature;
    }

    public String getLoadingTime() {
        return loadingTime;
    }

    public void setLoadingTime(String loadingTime) {
        this.loadingTime = loadingTime;
    }

    public String getStartReportingDate() {
        return startReportingDate;
    }

    public void setStartReportingDate(String startReportingDate) {
        this.startReportingDate = startReportingDate;
    }

    public String getStartReportingTime() {
        return startReportingTime;
    }

    public void setStartReportingTime(String startReportingTime) {
        this.startReportingTime = startReportingTime;
    }

    public String getPointDistance() {
        return pointDistance;
    }

    public void setPointDistance(String pointDistance) {
        this.pointDistance = pointDistance;
    }

    public String getTotalPackages() {
        return totalPackages;
    }

    public void setTotalPackages(String totalPackages) {
        this.totalPackages = totalPackages;
    }

    public String getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(String totalWeight) {
        this.totalWeight = totalWeight;
    }

    public String getPendingPackages() {
        return pendingPackages;
    }

    public void setPendingPackages(String pendingPackages) {
        this.pendingPackages = pendingPackages;
    }

    public String getPendingWeight() {
        return pendingWeight;
    }

    public void setPendingWeight(String pendingWeight) {
        this.pendingWeight = pendingWeight;
    }

    public String getAwbNo() {
        return awbNo;
    }

    public void setAwbNo(String awbNo) {
        this.awbNo = awbNo;
    }

    public String getTotalChargeableWeight() {
        return totalChargeableWeight;
    }

    public void setTotalChargeableWeight(String totalChargeableWeight) {
        this.totalChargeableWeight = totalChargeableWeight;
    }

    public String getPendingGrossWeight() {
        return pendingGrossWeight;
    }

    public void setPendingGrossWeight(String pendingGrossWeight) {
        this.pendingGrossWeight = pendingGrossWeight;
    }

    public String getLhcId() {
        return lhcId;
    }

    public void setLhcId(String lhcId) {
        this.lhcId = lhcId;
    }

    public String getLhcNo() {
        return lhcNo;
    }

    public void setLhcNo(String lhcNo) {
        this.lhcNo = lhcNo;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverMobile() {
        return driverMobile;
    }

    public void setDriverMobile(String driverMobile) {
        this.driverMobile = driverMobile;
    }

    public String getDlNo() {
        return dlNo;
    }

    public void setDlNo(String dlNo) {
        this.dlNo = dlNo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getLoadedpackagesNew() {
        return loadedpackagesNew;
    }

    public void setLoadedpackagesNew(String loadedpackagesNew) {
        this.loadedpackagesNew = loadedpackagesNew;
    }

    public String getMovementTypeId() {
        return movementTypeId;
    }

    public void setMovementTypeId(String movementTypeId) {
        this.movementTypeId = movementTypeId;
    }

    

}
