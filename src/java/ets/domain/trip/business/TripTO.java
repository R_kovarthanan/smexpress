/*-------------------------------------------------------------------------
 * __NAME__.java
 * __DATE__
 *
 * Copyright (c) Entitle.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Entitle ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Entitle.
 -------------------------------------------------------------------------*/
package ets.domain.trip.business;

import java.util.ArrayList;

/**
 * ****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver Date Author Change
 * ----------------------------------------------------------------------------
 * 1.0 __DATE__ Your_Name ,Entitle Created
 *
 *****************************************************************************
 */
public class TripTO {

    /**
     * Creates a new instance of __NAME__
     */
    public TripTO() {
    }
     private String appointTime = "";
    private String appointTimemin = "";
    private String billOverTime = "";
    private String billOverTimemin = "";
    private String receivedTime = "";
    private String receivedTimemin = "";
    private String ewayBillNo = "";
    private String appointDate = "";
    private String expDate = "";
     private String appTime = "";
    private String movementtypeId = "";
    private String movementtype = "";
    private String materialDesc = "";
    private String insuranceDetail = "";
    private String appDate = "";
    private String appHour = "";
    private String appMin = "";
    
    private String escortName = "";
    private String escortMobile = "";
    private String tatTime = "";
    private String movementTypeId = "";
    
    private String documentRecivedDate = "";
    private String documentRecivedHr = "";
    private String documentRecivedMi = ""; 
    private String[] origPendingPkgs = null; 

    
    private String temp = "";
    private String tripMonth = "";
    private String tripYear = "";
    private String invRefNo = "";
    private String consignorId = "";
    private String deviceId = "0";
    private String consignorPhoneNo = "";
    
    private String additionalCost = "";
    private String contractHireId = "";
    
    private String originDetention = "";
    private String damageNew = "";
    private String docketNo = "";
    private String ewayBillDate = "";
    private String ewayBillTime = "";
    private String insuranceSurvey = "";
    private String gstTaxExpAmountIGST = "";
    private String gstTaxExpAmountCGST = "";
    private String gstTaxExpAmountSGST = "";
    private String lHCdate = "";
    private String lrdate = "";
    private String detentionCharge = "";
    private String detentionHours = "";
    private String lhcId = "";
    private String dlNo = "";
    private String sealNoType = "";
    private String sealNo = "";
    private String dlNumber = "";
    private String loadedpackagesNew = "";
    private String unloadedpackagesNew = "";
    private String shortageNew = "";
    private String eWayBillNo = "";
    private String invoiceNumber = "0";
    private String invoiceValue = "0";
    private String id = "";
    private String lhcNo = "";
    private String pendingPackages = "";
    private String userMovementType = "";
    private String pendingGrossWeights = "";
    private String pchargableWeight = "";
    private String loadPkgs = "";
    private String loadChargeWgt = "";
    private String loadGrossWgt = "";
    private String tripPlannedEndDate = "";
    private String goodsMovement = "";
    private String expenseLocation = "";
    private String awbNo = "";
    private String fuelEstInvoiceFlag = "";
    private String fuelType = "";
    private String fuelGstLevy = "";
    private String fuelPlaceofSupply = "";
    private String fuelInvoiceNo = "";
    private String fuelInvoiceDate = "";
    private String[] pendingGrossWeight = null;
    private String[] loadedGrossWeight = null;

//
    private String[] loadOrderId = null;
    private String[] pendingPkgs = null;
    private String[] pendingWeight = null;
    private String[] loadedPkgs = null;
    private String[] loadedWeight = null;
    // TSP & Royalty
    private String orderDetails = "";
    private String tspInvoiceType = "";
    private String tspGstLevy = "";
    private String tspPlaceofSupply = "";
    private String tspInvoiceNo = "";
    private String tspInvoiceDate = "";
    private String royaltyInvoiceType = "";
    private String royaltyGstLevy = "";
    private String royaltyPlaceofSupply = "";
    private String royaltyInvoiceNo = "";
    private String royaltyInvoiceDate = "";
    private String tspRoyaltyAmount = "";
    private String totalTspRoyaltyInvoiceAmt = "";
    private String orderType = "";

    // cancellation Invoice
    private String cancelInvoiceType = "";
    private String cancelGstLevy = "";
    private String cancelPlaceofSupply = "";
    private String cancelInvoiceNo = "";
    private String cancelInvoiceDate = "";
    private String cancellationCharge = "";
    private String totalCancellationInvoiceAmt = "";
    private String cancelInvoiceFlag = "";
    private String InvoiceFlag = "";

    private String fuelEstCharge = "";
    private String fuelEscalation = "";

    private String LicenseNo = "";
    private String cbt = "";
    private String expenseStatus = "";
    private String total = "";
    private String mailStatus = "";
    private String totalOtherAmount = "";
    private String invoiceNoBill = "";
    private String invoiceNoRemb = "";
    private String invoiceDateRemb = "";
    private String invoiceDate = "";
    private String invoiceDateBill = "";
    private String invoiceTypeBill = "";
    private String invoiceTypeRemb = "";
    private String gstLevy = "";
    private String gstLevyBill = "";
    private String gstLevyRemb = "";
    private String placeofSupply = "";
    private String placeofSupplyBill = "";
    private String placeofSupplyRemb = "";
    private String stateId = "";
    private String stateName = "";
    private String customerTypeId = "";
    private String invoiceCustName = "";
    private String compPanNo = "";
    private String custTinno = "";
    private String custStateCode = "";
    private String companyState = "";
    private String companyTinNo = "";
    private String companyStateCode = "";

    private String hireVehicleNo = "";
    private String hireVehicleDriver = "";
    private String contractTypeId = "";
    private String ownerShip = "";
    private String vendorTypeId = "";
    private String vendorId = "";
    private String vendorName = "";
    private String compGSTNo = "";
    private String state = "";
    private String compAddress = "";
    private String billingId = "";
    private String billingAddress = "";
    private String companyName = "";
    private String totalTax = "";
    private String otherExpenseAmount = "";
    private String igstPercentage = "";
    private String cgstPercentage = "";
    private String sgstPercentage = "";
    private String igstValue = "";
    private String cgstValue = "";
    private String sgstValue = "";
    private String billingParty = "";
    private String billingState = "";
    private String gstNo = "";
    private String panNo = "";
    private String gstCode = "";
    private String gstName = "";
    private String gstPercentage = "";
    private String invoiceType = "";
    private String totalDocumentAmount = "";
    private String uploadedChallan = "";
    private String uploadedChallanAmount = "";
    private String pendingChallan = "";
    private String pendingChallanAmount = "";
    private String preCoolingCost = "";
    private String tollCostPerKm = "";
    private String advancePaid = "";
    private String expenseHour = "";
    private String expenseMinute = "";
    private String miscRate = "";
    private String advanceReturn = "";
    private String driverChangeMinute = "";
    private String driverChangeHour = "";
    private String driverInTrip = "";
    private String documentRequiredStatus = "";
    private String unclearedAmount = "";
    private String billCopyFile = "";
    private String billCopyName = "";
    private String documentRequired = "";
    private String transactionAmount = "";
    private String transactionId = "";
    private String idleDays = "";
    private String idleBhattaId = "";
    private String bhattaAmount = "";
    private String driverLastBalance = "";
    private String settleAmount = "";
    private String payAmount = "";
    private String settlementTripsSize = "";
    private String tripAmount = "";
    private String vehicleAdvanceId = "";
    private String bhattaDate = "";
    private String advanceCode = "";
    private String endingBalance = "";
    private String vehicleMileage = "";
    private String fuelConsumption = "";
    private String fuelExpense = "";
    private String tollExpense = "";
    private String driverBhatta = "";
    private String parkingCost = "";
    private String systemExpenses = "";
    private String nextTripCountStatus = "";
    private String currentTripStartDate = "";
    private String nextTrip = "";
    private String overrideTripRemarks = "";
    private String overRideBy = "";
    private String createdOn = "";
    private String overRideOn = "";
    private String setteledKm = "";
    private String setteledHm = "";
    private String setteltotalKM;
    private String setteltotalHrs;
    private String remarks = "";
    private String followedBy = "";
    private String followedOn = "";
    private String ticketingDetailId = "";
    private String fileName = "";
    private String ticketingFile = "";
    private String ticketNo = "";
    private String raisedBy = "";
    private String raisedOn = "";
    private String ticketId = "";
    private String priority = "";
    private String from = "";
    private String to = "";
    private String cc = "";
    private String title = "";
    private String message = "";
    private String type = "";
    private String emptyTripPurpose = "";
    private String transitDays1 = "";
    private String transitDays2 = "";
    private String serialNumber = "";
    private String originCityName = "";
    private String destinationCityName = "";
    private String vehicleChangeCityName = "";
    private String totalHm = "";
    private String tripAdvanceId = "";
    private String tripWfuDate = "";
    private String tripWfuTime = "";
    private String usageTypeId = "";
    private String secAdditionalTollCost = "";
    private String preColingAmount = "";
    private String secondaryParkingAmount = "";
    private String totalMinutes = "";
    private String totalKm = "";
    private String totalHours = "";
    private String expense = "";
    private String emptyTripRemarks = "";
    private String cityFrom = "";
    private String cityTo = "";
    private String mailDeliveredStatusId = "";
    private String mailSendingId = "";
    private String approvalStatusId = "";
    private String outStandingAmount = "";
    private String customerCode = "";
    private String extraExpenseStatus = "";
    private String startDate = "";
    private String endDate = "";
    private String startedBy = "";
    private String endedBy = "";
    private String mailDate = "";
    private String mailIdBcc = "";
    private String mailIdCc = "";
    private String mailIdTo = "";
    private String mailContentBcc = "";
    private String mailContentCc = "";
    private String mailContentTo = "";
    private String mailSubjectBcc = "";
    private String mailSubjectCc = "";
    private String mailSubjectTo = "";
    private String mailTypeId = "";
    private String mailId = "";
    private String jobcardId = "";
    private String businessType = "";
    private String emptyTrip = "";
    private String oldVehicleNo = "";
    private String oldVehicleId = "";
    private String oldDriverName = "";
    private String vehicleRemarks = "";
    private String vehicleChangeDate = "";
    private String vehicleChangeMinute = "";
    private String vehicleChangeHour = "";
    private String lastVehicleEndOdometer = "";
    private String vehicleStartOdometer = "";
    private String balanceAmount = "";
    private String lastVehicleEndKm = "";
    private String vehicleStartKm = "";
    private String returnAmount = "";
    private String licenceNo = "";
    private String licenceDate = "";
    private String employeeCode = "";
    private String joiningDate = "";
    private String fuelTypeId = "";
    private String logDateTime = "";
    private String logDate = "";
    private String logTime = "";
    private String temperature = "";
    private String location = "";
    private String distance = "";
    private String courierRemarks = "";
    private String courierNo = "";
    private String weight = "";
    private String ratePerKg = "";
    private String editMode = "";
    private String discountAmount = "";
    private String billedAmount = "";
    private String billingNameAddress = "";
    private String closureTotalExpense = "";
    private String vehicleDieselUsed = "";
    private String reeferDieselUsed = "";
    private String tempApprovalStatus = "";
    private String tripGraphId = "";
    private String ratePerKm = "";
    private String emailCc = "";
    private String approvalstatus = "";
    private String infoEmailTo = "";
    private String infoEmailCc = "";
    private String extraExpenseValue = "0";
    private String driverCount = "0";
    private String tripCountStatus = "";
    private String vehicleDriverAdvanceId = "";
    private String paymentTypeId = "";
    private String paidAmount = "";
    private String driverMobile = "";
    private String currentLocation = "";
    private String totalPackages = "";
    private String vehicleRequiredDateTime = "";
    private String consignmentDate = "";
    private String customerOrderReferenceNo = "";
    private String orderReferenceNo = "";
    private String tempLogFile = "";
    private String routeNameStatus = "";
    private int routeContractId = 0;
    private String bpclTransactionId = "";
    private String secondaryTollAmount = "";
    private String secondaryAddlTollAmount = "";
    private String secondaryMiscAmount = "";
    private String transactionHistoryId = "", accountId = "", dealerName = "", dealerCity = "", transactionDate = "", accountingDate = "", transactionType = "", currency = "", amount = "", volumeDocNo = "", amoutBalance = "", petromilesEarned = "", odometerReading = "";
    private String consignorName = "";
    private String consigneeName = "";
    private String consignorAddress = "";
    private String consigneeAddress = "";
    private String consignorMobileNo = "";
    private String consigneeMobileNo = "";
    private String estimatedKM = "";
    private String emptyTripApprovalStatus = "";
    private String wfuRemarks = "";
    private String tripStartDate = "";
    private String tripStartTime = "";
    private String tripType = "";
    private String gpsKm = "";
    private String gpsHm = "";
    private String actualAdvancePaid = "";
    private String wfuUnloadingDate = "";
    private String wfuUnloadingTime = "";
    private String podStatus = "";
    private String podCount = "";
    private String paymentType = "";
    private String advanceToPayStatus = "";
    private String toPayStatus = "";
    private String currentTemperature = "";
    private String cityFromId = "";
    private String expectedArrivalDate = "";
    private String expectedArrivalTime = "";
    private String fleetCenterNo = "";
    private String fleetCenterName = "";
    private String mobileNo = "";
    private String gpsLocation = "";
    private String tripEndBalance = "";
    private String paymentMode = "";
    private String tripStartingBalance = "";
    private String tripBalance = "";
    private String zoneId = "";
    private String ZoneName = "";
    private String startReportingDate = "";
    private String startReportingTime = "";
    private String loadingTime = "";
    private String loadingDate = "";
    private String loadingTemperature = "";
    private String endReportingDate = "";
    private String endReportingTime = "";
    private String unLoadingTime = "";
    private String unLoadingDate = "";
    private String unLoadingTemperature = "";
    private String lrNumber = "";
    private String fuelLtr = "";
    private String secondaryDriverIdOne = "";
    private String secondaryDriverIdTwo = "";
    private String secondaryDriverNameOne = "";
    private String secondaryDriverNameTwo = "";
    private String advanceAmount = "";
    private String advanceRemarks = "";
    private String cNotesEmail = "";
    private String vehicleNoEmail = "";
    private String tripCodeEmail = "";
    private String customerNameEmail = "";
    private String routeInfoEmail = "";
    private String stakeHolderId = "";
    private String holderName = "";
    private String invoiceNo = "";
    private String fcHeadEmail = "";
    private String customerEmail = "";
    private String smtp = "";
    private String port = "";
    private String emailId = "";
    private String password = "";
    private String tomailId = "";
    private String uomId = "";
    private String routeName = "";
    private String miscValue = "";
    private String requeston = "";
    private String requestremarks = "";
    private String totalRumHM = "";
    private String consignmentId = "";
    private String articleCode = "";
    private String articleName = "";
    private String batch = "";
    private String packageNos = "";
    private String packageWeight = "0";
    private String uom = "";
    private String tripArticleid = "";
    private String loadpackageNos = "";
    private String[] tripArticleId = null;
    private String[] productCodes = null;
    private String[] productNames = null;
    private String[] packagesNos = null;
    private String[] weights = null;
    private String[] productbatch = null;
    private String[] productuom = null;
    private String[] loadedpackages = null;
    private String[] unloadedpackages = null;
    private String[] shortage = null;
    private String vehicleactreportdate = "";
    private String vehicleactreporthour = "";
    private String vehicleactreportmin = "";
    private String vehicleloadreportdate = "";
    private String vehicleloadreporthour = "";
    private String vehicleloadreportmin = "";
    private String vehicleloadtemperature = "";
    private String approvalRequestBy = "";
    private String approvalRequestOn = "";
    private String approvalRequestRemarks = "";
    private String approvalRemarks = "";
    private String approvedBy = "";
    private String approvedOn = "";
    private String approvalStatus = "";
    private String requestType = "";
    private String systemExpense = "";
    private String rcmExpense = "";
    private String otherExpense = "";
    private String nettExpense = "";
    private String bookedExpense = "";
    private String driverBatta = "";
    private String tollCost = "";
    private String dieselCost = "";
    private String dieselUsed = "";
    private String totalDays = "";
    private String runHm = "";
    private String runKm = "";
    private String reeferMileage = "";
    private String milleage = "";
    private String fuelPrice = "";
    private String driverIncentivePerKm = "";
    private String driverBattaPerDay = "";
    private String tollRate = "";
    private String estimatedExpense = "";
    private String permitExpiryDate = "";

    private String roleId = "";
    private String fleetCenterId = "";
    private String companyId = "";
    private String roadTaxExpiryDate = "";
    private String fcExpiryDate = "";
    private String insuranceExpiryDate = "";
    private String userName = "";
    private String productInfo = "";
    private String tripEndDate = "";
    private String expenseToBeBilledToCustomer = "";
    private String tripEndTime = "";
    private String startKm = "";
    private String startHm = "";
    private String endKm = "";
    private String endHm = "";
    private String freightAmount = "";
    private String tripPlanEndDate = "";
    private String tripPlanEndTime = "";
    private String tripclosureid = "";
    private String totalMins = "";
    private String tollAmount = "";
    private String driverIncentive = "";
    private String reeferConsumption = "";
    private String fuelConsumed = "";
    private String fuelCost = "";
    private String routeExpense = "";
    private String totalExpenses = "";
    private String totalRumKM = "";
    private String totalRefeerHours = "";
    private String totalRefeerMinutes = "";
    private String battaAmount = "";
    private String incentiveAmount = "";
    private String routeExpenses = "";
    private String planStartDate = "";
    private String planStartHour = "";
    private String planStartMinute = "";
    private String planEndDate = "";
    private String planEndHour = "";
    private String planEndMinute = "";
    private String planStartTime = "";
    private String planEndTime = "";
    private String actionName = "";
    private String actionRemarks = "";
    private String preStartLocationId = "";
    private String preStartLocation = "";
    private String preStartLocationPlanDate = "";
    private String preStartLocationPlanTime = "";
    private String preStartLocationPlanTimeHrs = "";
    private String preStartLocationPlanTimeMins = "";
    private String preStartLocationDistance = "";
    private String preStartLocationDurationHrs = "";
    private String preStartLocationDurationMins = "";
    private String preStartLocationVehicleMileage = "";
    private String preStartLocationTollRate = "";
    private String preStartLocationRouteExpense = "";
    private String preStartLocationStatus = "";
    private String tripEndMinute = "";
    private String tripEndHour = "";
    private String tripStartMinute = "";
    private String tripStartHour = "";
    private String tripPreStartMinute = "";
    private String tripPreStartHour = "";
    private String advanceDate = "";
    private String tripDay = "";
    private String estimatedAdance = "";
    private String requestedAdvance = "";
    private String paidAdvance = "";
    private String totalFuelValue = "";
    private String fuelRemarks = "";
    private String empName = "";
    private String applicableTaxPercentage = "";
    private String totalExpenseValue = "";
    private String fuelDate = "";
    private String podPointId = "";
    private String startTime = "";
    private String startOdometerReading = "";
    private String startHM = "";
    //private String pointId = "";
    private String cityId = "";
    private String cityName = "";
    private String podRemarks = "";
    private String podFile = "";
    private String startTripRemarks = "";
    private String endTime = "";
    private String endHM = "";
    private String endOdometerReading = "";
    private String endTripRemarks = "";
    private String preStartHM = "";
    private String fuelLocation = "";
    private String fillDate = "";
    private String fuelLitres = "";
    private String fuelPricePerLitre = "";
    private String fuelAmount = "";
    private String fuelremarks = "";
    private String expenseId = "";
    private String expenseDate = "";
    private String expenseType = "";
    private String totalExpenseAmount = "";
    private String expenseRemarks = "";
    private String expenseName = "";
    private String employeeId = "";
    private String employeeName = "";
    private String taxPercentage = "";
    private double taxPercentageD = 0;
    private String totalValue = "";
    private String nettValue = "";
    private String taxValue = "";
    private String expenseValue = "";
    private String statusId = "";
    private String tripStatusId = "";
    private String tripExpenseId = "";
    private String tripPodId = "";
    private String durationHours = "";
    private String durationDay1 = "";
    private String durationDay2 = "";
    //Throttle Start Here
    private String tripPlannedDate = "";
    private String tripPlannedTime = "";
    private String tripPlannedTimeHr = "";
    private String tripPlannedTimeMin = "";
    private String tripStartReportingDate = "";

    private String tripPlannedStartTime = "";
    private String tripSheetDate = "";
    private String consignmentNote = "";
    private String routeInfo = "";
    private String vehicleTonnage = "";
    private String estimatedTonnage = "";
    private String vehicleUtilisation = "";
    private String tripRemarks = "";
    private String vehicleType = "";
    private String estimatedRevenue = "";
    private String totalRevenue = "";
    private String grandTotal = "";
    private String totalExpToBeBilled = "";
    private String estimatedTransitDays = "";
    private String estimatedTransitHours = "";
    private String estimatedAdvancePerDay = "";
    //Throttle End Here
    private String primaryDriverId = "";
    private String secondaryDriver1Id = "";
    private String secondaryDriver2Id = "";
    private String primaryDriverName = "";
    private String secondaryDriver1Name = "";
    private String secondaryDriver2Name = "";
    private String tripDate = "";
    private String vehicleCapUtil = "";
    private String vehicleCategory = "";
    private String tripId = "";
    private String[] tripIds = null;
    private String[] consignmentOrderIds = null;
    private int userId = 0;
    private String driver1Id = "";
    private String statusName = "";
    private String profitMargin = "";
    private String passThroughStatus = "";
    private String marginValue = "";
    private String tripCode = "";
    private String invoiceCode = "";
    private String invoiceId = "";
    private String invoiceDetailId = "";
    private String tripTransitDays = "";
    private String tripTransitHours = "";
    private String advnaceToBePaidPerDay = "";
    private String[] consignmentOrderId = null;
    private String[] orderIds = null;
    private String[] pointId = null;
    private String[] pointType = null;
    private String[] pointOrder = null;
    private String[] pointAddresss = null;
    private String[] pointPlanDate = null;
    private String[] pointPlanTime = null;
    private String[] pointPlanTimeHrs = null;
    private String[] pointPlanTimeMins = null;
    private String cNotes = "";
    private String customerName = "";
    private String customerAddress = "";
    private String billDate = "";
    private String totalWeight = "";
    private String reeferRequired = "";
    private String consginmentRemarks = "";
    private String customerType = "";
    private String billingType = "";
    private String vehicleTypeId = "";
    private String paidStatus = "";
    private String vehicleId = "";
    private String vehicleNo = "";
    private String driverId = "";
    private String driverName = "";
    private String cleanerId = "";
    private String cleanerName = "";
    private String vehicleTypeName = "";
    private String orderExpense = "";
    private String orderRevenue = "";
    private String tripScheduleDate = "";
    private String tripScheduleTime = "";
    private String tripScheduleTimeHrs = "";
    private String tripScheduleTimeMins = "";
    private String tripScheduleTimeDB = "";
    private String point1PlannedDate = "";
    private String point1PlannedTimeHrs = "";
    private String point1PlannedTimeMins = "";
    private String point2PlannedDate = "";
    private String point2PlannedTimeHrs = "";
    private String point2PlannedTimeMins = "";
    private String point3PlannedDate = "";
    private String point3PlannedTimeHrs = "";
    private String point3PlannedTimeMins = "";
    private String point4PlannedDate = "";
    private String point4PlannedTimeHrs = "";
    private String point4PlannedTimeMins = "";
    private String finalPlannedDate = "";
    private String finalPlannedTimeMins = "";
    private String finalPlannedTimeHrs = "";
    private String orderId = "";
    private String orderNo = "";
    private String routeId = "";
    private String origin = "";
    private String originId = "";
    private String destination = "";
    private String destinationId = "";
    private String firstPointId = "";
    private String finalPointId = "";
    private String point1Id = "";
    private String point1Name = "";
    private String point2Id = "";
    private String point2Name = "";
    private String point3Id = "";
    private String point3Name = "";
    private String point4Id = "";
    private String point4Name = "";
    private String totalKM = "";
    private String totalHrs = "";
    private String totalPoints = "";
    private String interimExists = "";
    private String consignmentOrderNos = "";
    private String vehicleExpense = "";
    private String reeferExpense = "";
    private boolean doesConsignmentRouteCourseExists = false;
    private ArrayList consignmentRoutePoints = null;
//       Nithiya starts here
    private String preStartDate = "";
    private String preStartTime = "";
    private String preOdometerReading = "";
    private String preTripRemarks = "";
    private String regNo = "";
    private String tripSheetId = "";
    private String consignmentNo = "";
    private String customerId = "";
    private String noOfTrips = "";
    private String fromDate = "";
    private String toDate = "";
    private String status = "";
    //raju
    private String editFreightAmount = "";
    private String billing = "";
    private String latitude = "";
    private String longitude = "";
    private String rate = "";
    private String touchPointStatus = "";
    private String touchPointDocketno = "";
    private String consignorGST = "";
    private String consigneeGST = "";
    private String consignorContactPerson = "";
    private String consigneeContactPerson = "";

    private String packagePickedUp = "";
    private String packageDelivered = "";
    private String noticeNo = "";
    private String natureOfLoss = "";
    private String actualWeight = "";
    private String deliveredWeight = "";
    private String outwardCondition = "";
    private String detailsOfFacts = "";
    private String claimValue = "";
    private String amountLoss = "";
    private String addressInfo = "";
    
    private String cgst = "";
    private String sgst = "";
    private String igst = "";
    private String cgstAmt = "";
    private String sgstAmt = "";
    private String igstAmt = "";
    private String igstAmtRound = "";
    private String cgstAmtRound = "";
    private String sgstAmtRound = "";
    private String grandTotalRound = "";
    private String freightAmountRound = "";

    public String getPackagePickedUp() {
        return packagePickedUp;
    }

    public void setPackagePickedUp(String packagePickedUp) {
        this.packagePickedUp = packagePickedUp;
    }

    public String getPackageDelivered() {
        return packageDelivered;
    }

    public void setPackageDelivered(String packageDelivered) {
        this.packageDelivered = packageDelivered;
    }

    public String getNoticeNo() {
        return noticeNo;
    }

    public void setNoticeNo(String noticeNo) {
        this.noticeNo = noticeNo;
    }

    public String getNatureOfLoss() {
        return natureOfLoss;
    }

    public void setNatureOfLoss(String natureOfLoss) {
        this.natureOfLoss = natureOfLoss;
    }

    public String getActualWeight() {
        return actualWeight;
    }

    public void setActualWeight(String actualWeight) {
        this.actualWeight = actualWeight;
    }

    public String getDeliveredWeight() {
        return deliveredWeight;
    }

    public void setDeliveredWeight(String deliveredWeight) {
        this.deliveredWeight = deliveredWeight;
    }

    public String getOutwardCondition() {
        return outwardCondition;
    }

    public void setOutwardCondition(String outwardCondition) {
        this.outwardCondition = outwardCondition;
    }

    public String getDetailsOfFacts() {
        return detailsOfFacts;
    }

    public void setDetailsOfFacts(String detailsOfFacts) {
        this.detailsOfFacts = detailsOfFacts;
    }

    public String getClaimValue() {
        return claimValue;
    }

    public void setClaimValue(String claimValue) {
        this.claimValue = claimValue;
    }

    public String getAmountLoss() {
        return amountLoss;
    }

    public void setAmountLoss(String amountLoss) {
        this.amountLoss = amountLoss;
    }

    public String getAddressInfo() {
        return addressInfo;
    }

    public void setAddressInfo(String addressInfo) {
        this.addressInfo = addressInfo;
    }
    
    

    public String getConsignorContactPerson() {
        return consignorContactPerson;
    }

    public void setConsignorContactPerson(String consignorContactPerson) {
        this.consignorContactPerson = consignorContactPerson;
    }

    public String getConsigneeContactPerson() {
        return consigneeContactPerson;
    }

    public void setConsigneeContactPerson(String consigneeContactPerson) {
        this.consigneeContactPerson = consigneeContactPerson;
    }

    public String getOldVehicleId() {
        return oldVehicleId;
    }

    public void setOldVehicleId(String oldVehicleId) {
        this.oldVehicleId = oldVehicleId;
    }

    public String getConsignorGST() {
        return consignorGST;
    }

    public void setConsignorGST(String consignorGST) {
        this.consignorGST = consignorGST;
    }

    public String getConsigneeGST() {
        return consigneeGST;
    }

    public void setConsigneeGST(String consigneeGST) {
        this.consigneeGST = consigneeGST;
    }

    public String getTouchPointDocketno() {
        return touchPointDocketno;
    }

    public void setTouchPointDocketno(String touchPointDocketno) {
        this.touchPointDocketno = touchPointDocketno;
    }

    public String getTouchPointStatus() {
        return touchPointStatus;
    }

    public void setTouchPointStatus(String touchPointStatus) {
        this.touchPointStatus = touchPointStatus;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getConsignmentOrderNos() {
        return consignmentOrderNos;
    }

    public void setConsignmentOrderNos(String consignmentOrderNos) {
        this.consignmentOrderNos = consignmentOrderNos;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getFinalPointId() {
        return finalPointId;
    }

    public void setFinalPointId(String finalPointId) {
        this.finalPointId = finalPointId;
    }

    public String getFirstPointId() {
        return firstPointId;
    }

    public void setFirstPointId(String firstPointId) {
        this.firstPointId = firstPointId;
    }

    public String getInterimExists() {
        return interimExists;
    }

    public void setInterimExists(String interimExists) {
        this.interimExists = interimExists;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getPoint1Id() {
        return point1Id;
    }

    public void setPoint1Id(String point1Id) {
        this.point1Id = point1Id;
    }

    public String getPoint1Name() {
        return point1Name;
    }

    public void setPoint1Name(String point1Name) {
        this.point1Name = point1Name;
    }

    public String getPoint2Id() {
        return point2Id;
    }

    public void setPoint2Id(String point2Id) {
        this.point2Id = point2Id;
    }

    public String getPoint2Name() {
        return point2Name;
    }

    public void setPoint2Name(String point2Name) {
        this.point2Name = point2Name;
    }

    public String getPoint3Id() {
        return point3Id;
    }

    public void setPoint3Id(String point3Id) {
        this.point3Id = point3Id;
    }

    public String getPoint3Name() {
        return point3Name;
    }

    public void setPoint3Name(String point3Name) {
        this.point3Name = point3Name;
    }

    public String getPoint4Id() {
        return point4Id;
    }

    public void setPoint4Id(String point4Id) {
        this.point4Id = point4Id;
    }

    public String getPoint4Name() {
        return point4Name;
    }

    public void setPoint4Name(String point4Name) {
        this.point4Name = point4Name;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getTotalHrs() {
        return totalHrs;
    }

    public void setTotalHrs(String totalHrs) {
        this.totalHrs = totalHrs;
    }

    public String getTotalKM() {
        return totalKM;
    }

    public void setTotalKM(String totalKM) {
        this.totalKM = totalKM;
    }

    public String getFinalPlannedDate() {
        return finalPlannedDate;
    }

    public void setFinalPlannedDate(String finalPlannedDate) {
        this.finalPlannedDate = finalPlannedDate;
    }

    public String getPoint1PlannedDate() {
        return point1PlannedDate;
    }

    public void setPoint1PlannedDate(String point1PlannedDate) {
        this.point1PlannedDate = point1PlannedDate;
    }

    public String getPoint2PlannedDate() {
        return point2PlannedDate;
    }

    public void setPoint2PlannedDate(String point2PlannedDate) {
        this.point2PlannedDate = point2PlannedDate;
    }

    public String getPoint3PlannedDate() {
        return point3PlannedDate;
    }

    public void setPoint3PlannedDate(String point3PlannedDate) {
        this.point3PlannedDate = point3PlannedDate;
    }

    public String getPoint4PlannedDate() {
        return point4PlannedDate;
    }

    public void setPoint4PlannedDate(String point4PlannedDate) {
        this.point4PlannedDate = point4PlannedDate;
    }

    public String getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(String totalPoints) {
        this.totalPoints = totalPoints;
    }

    public String getTripScheduleDate() {
        return tripScheduleDate;
    }

    public void setTripScheduleDate(String tripScheduleDate) {
        this.tripScheduleDate = tripScheduleDate;
    }

    public String getTripScheduleTime() {
        return tripScheduleTime;
    }

    public void setTripScheduleTime(String tripScheduleTime) {
        this.tripScheduleTime = tripScheduleTime;
    }

    public String getOrderRevenue() {
        return orderRevenue;
    }

    public void setOrderRevenue(String orderRevenue) {
        this.orderRevenue = orderRevenue;
    }

    public String getReeferExpense() {
        return reeferExpense;
    }

    public void setReeferExpense(String reeferExpense) {
        this.reeferExpense = reeferExpense;
    }

    public String getVehicleExpense() {
        return vehicleExpense;
    }

    public void setVehicleExpense(String vehicleExpense) {
        this.vehicleExpense = vehicleExpense;
    }

    public String getOrderExpense() {
        return orderExpense;
    }

    public void setOrderExpense(String orderExpense) {
        this.orderExpense = orderExpense;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getBillingType() {
        return billingType;
    }

    public void setBillingType(String billingType) {
        this.billingType = billingType;
    }

    public String getConsginmentRemarks() {
        return consginmentRemarks;
    }

    public void setConsginmentRemarks(String consginmentRemarks) {
        this.consginmentRemarks = consginmentRemarks;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getReeferRequired() {
        return reeferRequired;
    }

    public void setReeferRequired(String reeferRequired) {
        this.reeferRequired = reeferRequired;
    }

    public String getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(String totalWeight) {
        this.totalWeight = totalWeight;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public boolean isDoesConsignmentRouteCourseExists() {
        return doesConsignmentRouteCourseExists;
    }

    public void setDoesConsignmentRouteCourseExists(boolean doesConsignmentRouteCourseExists) {
        this.doesConsignmentRouteCourseExists = doesConsignmentRouteCourseExists;
    }

    public ArrayList getConsignmentRoutePoints() {
        return consignmentRoutePoints;
    }

    public void setConsignmentRoutePoints(ArrayList consignmentRoutePoints) {
        this.consignmentRoutePoints = consignmentRoutePoints;
    }

    public String getCleanerId() {
        return cleanerId;
    }

    public void setCleanerId(String cleanerId) {
        this.cleanerId = cleanerId;
    }

    public String getCleanerName() {
        return cleanerName;
    }

    public void setCleanerName(String cleanerName) {
        this.cleanerName = cleanerName;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String[] getConsignmentOrderId() {
        return consignmentOrderId;
    }

    public void setConsignmentOrderId(String[] consignmentOrderId) {
        this.consignmentOrderId = consignmentOrderId;
    }

    public String getDriver1Id() {
        return driver1Id;
    }

    public void setDriver1Id(String driver1Id) {
        this.driver1Id = driver1Id;
    }

    public String[] getOrderIds() {
        return orderIds;
    }

    public void setOrderIds(String[] orderIds) {
        this.orderIds = orderIds;
    }

    public String[] getPointAddresss() {
        return pointAddresss;
    }

    public void setPointAddresss(String[] pointAddresss) {
        this.pointAddresss = pointAddresss;
    }

    public String[] getPointId() {
        return pointId;
    }

    public void setPointId(String[] pointId) {
        this.pointId = pointId;
    }

    public String[] getPointOrder() {
        return pointOrder;
    }

    public void setPointOrder(String[] pointOrder) {
        this.pointOrder = pointOrder;
    }

    public String[] getPointPlanDate() {
        return pointPlanDate;
    }

    public void setPointPlanDate(String[] pointPlanDate) {
        this.pointPlanDate = pointPlanDate;
    }

    public String[] getPointPlanTime() {
        return pointPlanTime;
    }

    public void setPointPlanTime(String[] pointPlanTime) {
        this.pointPlanTime = pointPlanTime;
    }

    public String[] getPointType() {
        return pointType;
    }

    public void setPointType(String[] pointType) {
        this.pointType = pointType;
    }

    public String getProfitMargin() {
        return profitMargin;
    }

    public void setProfitMargin(String profitMargin) {
        this.profitMargin = profitMargin;
    }

    public String getTripDate() {
        return tripDate;
    }

    public void setTripDate(String tripDate) {
        this.tripDate = tripDate;
    }

    public String getTripCode() {
        return tripCode;
    }

    public void setTripCode(String tripCode) {
        this.tripCode = tripCode;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getTripTransitDays() {
        return tripTransitDays;
    }

    public void setTripTransitDays(String tripTransitDays) {
        this.tripTransitDays = tripTransitDays;
    }

    public String getTripTransitHours() {
        return tripTransitHours;
    }

    public void setTripTransitHours(String tripTransitHours) {
        this.tripTransitHours = tripTransitHours;
    }

    public String getAdvnaceToBePaidPerDay() {
        return advnaceToBePaidPerDay;
    }

    public void setAdvnaceToBePaidPerDay(String advnaceToBePaidPerDay) {
        this.advnaceToBePaidPerDay = advnaceToBePaidPerDay;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getTripScheduleTimeDB() {
        return tripScheduleTimeDB;
    }

    public void setTripScheduleTimeDB(String tripScheduleTimeDB) {
        this.tripScheduleTimeDB = tripScheduleTimeDB;
    }

    public String getFinalPlannedTimeHrs() {
        return finalPlannedTimeHrs;
    }

    public void setFinalPlannedTimeHrs(String finalPlannedTimeHrs) {
        this.finalPlannedTimeHrs = finalPlannedTimeHrs;
    }

    public String getFinalPlannedTimeMins() {
        return finalPlannedTimeMins;
    }

    public void setFinalPlannedTimeMins(String finalPlannedTimeMins) {
        this.finalPlannedTimeMins = finalPlannedTimeMins;
    }

    public String getPoint1PlannedTimeHrs() {
        return point1PlannedTimeHrs;
    }

    public void setPoint1PlannedTimeHrs(String point1PlannedTimeHrs) {
        this.point1PlannedTimeHrs = point1PlannedTimeHrs;
    }

    public String getPoint1PlannedTimeMins() {
        return point1PlannedTimeMins;
    }

    public void setPoint1PlannedTimeMins(String point1PlannedTimeMins) {
        this.point1PlannedTimeMins = point1PlannedTimeMins;
    }

    public String getPoint2PlannedTimeHrs() {
        return point2PlannedTimeHrs;
    }

    public void setPoint2PlannedTimeHrs(String point2PlannedTimeHrs) {
        this.point2PlannedTimeHrs = point2PlannedTimeHrs;
    }

    public String getPoint2PlannedTimeMins() {
        return point2PlannedTimeMins;
    }

    public void setPoint2PlannedTimeMins(String point2PlannedTimeMins) {
        this.point2PlannedTimeMins = point2PlannedTimeMins;
    }

    public String getPoint3PlannedTimeHrs() {
        return point3PlannedTimeHrs;
    }

    public void setPoint3PlannedTimeHrs(String point3PlannedTimeHrs) {
        this.point3PlannedTimeHrs = point3PlannedTimeHrs;
    }

    public String getPoint3PlannedTimeMins() {
        return point3PlannedTimeMins;
    }

    public void setPoint3PlannedTimeMins(String point3PlannedTimeMins) {
        this.point3PlannedTimeMins = point3PlannedTimeMins;
    }

    public String getPoint4PlannedTimeHrs() {
        return point4PlannedTimeHrs;
    }

    public void setPoint4PlannedTimeHrs(String point4PlannedTimeHrs) {
        this.point4PlannedTimeHrs = point4PlannedTimeHrs;
    }

    public String getPoint4PlannedTimeMins() {
        return point4PlannedTimeMins;
    }

    public void setPoint4PlannedTimeMins(String point4PlannedTimeMins) {
        this.point4PlannedTimeMins = point4PlannedTimeMins;
    }

    public String getTripScheduleTimeHrs() {
        return tripScheduleTimeHrs;
    }

    public void setTripScheduleTimeHrs(String tripScheduleTimeHrs) {
        this.tripScheduleTimeHrs = tripScheduleTimeHrs;
    }

    public String getTripScheduleTimeMins() {
        return tripScheduleTimeMins;
    }

    public void setTripScheduleTimeMins(String tripScheduleTimeMins) {
        this.tripScheduleTimeMins = tripScheduleTimeMins;
    }

    public String[] getPointPlanTimeHrs() {
        return pointPlanTimeHrs;
    }

    public void setPointPlanTimeHrs(String[] pointPlanTimeHrs) {
        this.pointPlanTimeHrs = pointPlanTimeHrs;
    }

    public String[] getPointPlanTimeMins() {
        return pointPlanTimeMins;
    }

    public void setPointPlanTimeMins(String[] pointPlanTimeMins) {
        this.pointPlanTimeMins = pointPlanTimeMins;
    }

    public String getVehicleTonnage() {
        return vehicleTonnage;
    }

    public void setVehicleTonnage(String vehicleTonnage) {
        this.vehicleTonnage = vehicleTonnage;
    }

    public String getcNotes() {
        return cNotes;
    }

    public void setcNotes(String cNotes) {
        this.cNotes = cNotes;
    }

    public String getRouteInfo() {
        return routeInfo;
    }

    public void setRouteInfo(String routeInfo) {
        this.routeInfo = routeInfo;
    }

    public String getTripRemarks() {
        return tripRemarks;
    }

    public void setTripRemarks(String tripRemarks) {
        this.tripRemarks = tripRemarks;
    }

    public String getVehicleCapUtil() {
        return vehicleCapUtil;
    }

    public void setVehicleCapUtil(String vehicleCapUtil) {
        this.vehicleCapUtil = vehicleCapUtil;
    }

    public String getConsignmentNo() {
        return consignmentNo;
    }

    public void setConsignmentNo(String consignmentNo) {
        this.consignmentNo = consignmentNo;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getPreOdometerReading() {
        return preOdometerReading;
    }

    public void setPreOdometerReading(String preOdometerReading) {
        this.preOdometerReading = preOdometerReading;
    }

    public String getPreStartDate() {
        return preStartDate;
    }

    public void setPreStartDate(String preStartDate) {
        this.preStartDate = preStartDate;
    }

    public String getPreStartLocation() {
        return preStartLocation;
    }

    public void setPreStartLocation(String preStartLocation) {
        this.preStartLocation = preStartLocation;
    }

    public String getPreStartTime() {
        return preStartTime;
    }

    public void setPreStartTime(String preStartTime) {
        this.preStartTime = preStartTime;
    }

    public String getPreTripRemarks() {
        return preTripRemarks;
    }

    public void setPreTripRemarks(String preTripRemarks) {
        this.preTripRemarks = preTripRemarks;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getTripSheetId() {
        return tripSheetId;
    }

    public void setTripSheetId(String tripSheetId) {
        this.tripSheetId = tripSheetId;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getActionRemarks() {
        return actionRemarks;
    }

    public void setActionRemarks(String actionRemarks) {
        this.actionRemarks = actionRemarks;
    }

    public String getPrimaryDriverId() {
        return primaryDriverId;
    }

    public void setPrimaryDriverId(String primaryDriverId) {
        this.primaryDriverId = primaryDriverId;
    }

    public String getPrimaryDriverName() {
        return primaryDriverName;
    }

    public void setPrimaryDriverName(String primaryDriverName) {
        this.primaryDriverName = primaryDriverName;
    }

    public String getSecondaryDriver1Id() {
        return secondaryDriver1Id;
    }

    public void setSecondaryDriver1Id(String secondaryDriver1Id) {
        this.secondaryDriver1Id = secondaryDriver1Id;
    }

    public String getSecondaryDriver1Name() {
        return secondaryDriver1Name;
    }

    public void setSecondaryDriver1Name(String secondaryDriver1Name) {
        this.secondaryDriver1Name = secondaryDriver1Name;
    }

    public String getSecondaryDriver2Id() {
        return secondaryDriver2Id;
    }

    public void setSecondaryDriver2Id(String secondaryDriver2Id) {
        this.secondaryDriver2Id = secondaryDriver2Id;
    }

    public String getSecondaryDriver2Name() {
        return secondaryDriver2Name;
    }

    public void setSecondaryDriver2Name(String secondaryDriver2Name) {
        this.secondaryDriver2Name = secondaryDriver2Name;
    }

    public String getConsignmentNote() {
        return consignmentNote;
    }

    public void setConsignmentNote(String consignmentNote) {
        this.consignmentNote = consignmentNote;
    }

    public String getEstimatedAdvancePerDay() {
        return estimatedAdvancePerDay;
    }

    public void setEstimatedAdvancePerDay(String estimatedAdvancePerDay) {
        this.estimatedAdvancePerDay = estimatedAdvancePerDay;
    }

    public String getEstimatedRevenue() {
        return estimatedRevenue;
    }

    public void setEstimatedRevenue(String estimatedRevenue) {
        this.estimatedRevenue = estimatedRevenue;
    }

    public String getEstimatedTonnage() {
        return estimatedTonnage;
    }

    public void setEstimatedTonnage(String estimatedTonnage) {
        this.estimatedTonnage = estimatedTonnage;
    }

    public String getEstimatedTransitDays() {
        return estimatedTransitDays;
    }

    public void setEstimatedTransitDays(String estimatedTransitDays) {
        this.estimatedTransitDays = estimatedTransitDays;
    }

    public String getEstimatedTransitHours() {
        return estimatedTransitHours;
    }

    public void setEstimatedTransitHours(String estimatedTransitHours) {
        this.estimatedTransitHours = estimatedTransitHours;
    }

    public String getTripPlannedDate() {
        return tripPlannedDate;
    }

    public void setTripPlannedDate(String tripPlannedDate) {
        this.tripPlannedDate = tripPlannedDate;
    }

    public String getTripPlannedStartTime() {
        return tripPlannedStartTime;
    }

    public void setTripPlannedStartTime(String tripPlannedStartTime) {
        this.tripPlannedStartTime = tripPlannedStartTime;
    }

    public String getTripSheetDate() {
        return tripSheetDate;
    }

    public void setTripSheetDate(String tripSheetDate) {
        this.tripSheetDate = tripSheetDate;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleUtilisation() {
        return vehicleUtilisation;
    }

    public void setVehicleUtilisation(String vehicleUtilisation) {
        this.vehicleUtilisation = vehicleUtilisation;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDurationDay1() {
        return durationDay1;
    }

    public void setDurationDay1(String durationDay1) {
        this.durationDay1 = durationDay1;
    }

    public String getDurationDay2() {
        return durationDay2;
    }

    public void setDurationDay2(String durationDay2) {
        this.durationDay2 = durationDay2;
    }

    public String getDurationHours() {
        return durationHours;
    }

    public void setDurationHours(String durationHours) {
        this.durationHours = durationHours;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndHM() {
        return endHM;
    }

    public void setEndHM(String endHM) {
        this.endHM = endHM;
    }

    public String getEndOdometerReading() {
        return endOdometerReading;
    }

    public void setEndOdometerReading(String endOdometerReading) {
        this.endOdometerReading = endOdometerReading;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getEndTripRemarks() {
        return endTripRemarks;
    }

    public void setEndTripRemarks(String endTripRemarks) {
        this.endTripRemarks = endTripRemarks;
    }

    public String getExpenseDate() {
        return expenseDate;
    }

    public void setExpenseDate(String expenseDate) {
        this.expenseDate = expenseDate;
    }

    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }

    public String getExpenseName() {
        return expenseName;
    }

    public void setExpenseName(String expenseName) {
        this.expenseName = expenseName;
    }

    public String getExpenseRemarks() {
        return expenseRemarks;
    }

    public void setExpenseRemarks(String expenseRemarks) {
        this.expenseRemarks = expenseRemarks;
    }

    public String getExpenseType() {
        return expenseType;
    }

    public void setExpenseType(String expenseType) {
        this.expenseType = expenseType;
    }

    public String getExpenseValue() {
        return expenseValue;
    }

    public void setExpenseValue(String expenseValue) {
        this.expenseValue = expenseValue;
    }

    public String getFillDate() {
        return fillDate;
    }

    public void setFillDate(String fillDate) {
        this.fillDate = fillDate;
    }

    public String getFuelAmount() {
        return fuelAmount;
    }

    public void setFuelAmount(String fuelAmount) {
        this.fuelAmount = fuelAmount;
    }

    public String getFuelLitres() {
        return fuelLitres;
    }

    public void setFuelLitres(String fuelLitres) {
        this.fuelLitres = fuelLitres;
    }

    public String getFuelLocation() {
        return fuelLocation;
    }

    public void setFuelLocation(String fuelLocation) {
        this.fuelLocation = fuelLocation;
    }

    public String getFuelPricePerLitre() {
        return fuelPricePerLitre;
    }

    public void setFuelPricePerLitre(String fuelPricePerLitre) {
        this.fuelPricePerLitre = fuelPricePerLitre;
    }

    public String getFuelremarks() {
        return fuelremarks;
    }

    public void setFuelremarks(String fuelremarks) {
        this.fuelremarks = fuelremarks;
    }

    public String getPodFile() {
        return podFile;
    }

    public void setPodFile(String podFile) {
        this.podFile = podFile;
    }

    public String getPodRemarks() {
        return podRemarks;
    }

    public void setPodRemarks(String podRemarks) {
        this.podRemarks = podRemarks;
    }

    public String getPreStartHM() {
        return preStartHM;
    }

    public void setPreStartHM(String preStartHM) {
        this.preStartHM = preStartHM;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartHM() {
        return startHM;
    }

    public void setStartHM(String startHM) {
        this.startHM = startHM;
    }

    public String getStartOdometerReading() {
        return startOdometerReading;
    }

    public void setStartOdometerReading(String startOdometerReading) {
        this.startOdometerReading = startOdometerReading;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getStartTripRemarks() {
        return startTripRemarks;
    }

    public void setStartTripRemarks(String startTripRemarks) {
        this.startTripRemarks = startTripRemarks;
    }

    public String getTaxPercentage() {
        return taxPercentage;
    }

    public void setTaxPercentage(String taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

    public String getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(String totalDays) {
        this.totalDays = totalDays;
    }

    public String getTotalExpenseAmount() {
        return totalExpenseAmount;
    }

    public void setTotalExpenseAmount(String totalExpenseAmount) {
        this.totalExpenseAmount = totalExpenseAmount;
    }

    public String getTripExpenseId() {
        return tripExpenseId;
    }

    public void setTripExpenseId(String tripExpenseId) {
        this.tripExpenseId = tripExpenseId;
    }

    public String getTripPodId() {
        return tripPodId;
    }

    public void setTripPodId(String tripPodId) {
        this.tripPodId = tripPodId;
    }

    public String getPodPointId() {
        return podPointId;
    }

    public void setPodPointId(String podPointId) {
        this.podPointId = podPointId;
    }

    public String getAdvanceDate() {
        return advanceDate;
    }

    public void setAdvanceDate(String advanceDate) {
        this.advanceDate = advanceDate;
    }

    public String getApplicableTaxPercentage() {
        return applicableTaxPercentage;
    }

    public void setApplicableTaxPercentage(String applicableTaxPercentage) {
        this.applicableTaxPercentage = applicableTaxPercentage;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getEstimatedAdance() {
        return estimatedAdance;
    }

    public void setEstimatedAdance(String estimatedAdance) {
        this.estimatedAdance = estimatedAdance;
    }

    public String getFuelDate() {
        return fuelDate;
    }

    public void setFuelDate(String fuelDate) {
        this.fuelDate = fuelDate;
    }

    public String getFuelRemarks() {
        return fuelRemarks;
    }

    public void setFuelRemarks(String fuelRemarks) {
        this.fuelRemarks = fuelRemarks;
    }

    public String getPaidAdvance() {
        return paidAdvance;
    }

    public void setPaidAdvance(String paidAdvance) {
        this.paidAdvance = paidAdvance;
    }

    public String getRequestedAdvance() {
        return requestedAdvance;
    }

    public void setRequestedAdvance(String requestedAdvance) {
        this.requestedAdvance = requestedAdvance;
    }

    public String getTotalExpenseValue() {
        return totalExpenseValue;
    }

    public void setTotalExpenseValue(String totalExpenseValue) {
        this.totalExpenseValue = totalExpenseValue;
    }

    public String getTotalFuelValue() {
        return totalFuelValue;
    }

    public void setTotalFuelValue(String totalFuelValue) {
        this.totalFuelValue = totalFuelValue;
    }

    public String getTripDay() {
        return tripDay;
    }

    public void setTripDay(String tripDay) {
        this.tripDay = tripDay;
    }

    public String getTripEndHour() {
        return tripEndHour;
    }

    public void setTripEndHour(String tripEndHour) {
        this.tripEndHour = tripEndHour;
    }

    public String getTripEndMinute() {
        return tripEndMinute;
    }

    public void setTripEndMinute(String tripEndMinute) {
        this.tripEndMinute = tripEndMinute;
    }

    public String getTripPreStartHour() {
        return tripPreStartHour;
    }

    public void setTripPreStartHour(String tripPreStartHour) {
        this.tripPreStartHour = tripPreStartHour;
    }

    public String getTripPreStartMinute() {
        return tripPreStartMinute;
    }

    public void setTripPreStartMinute(String tripPreStartMinute) {
        this.tripPreStartMinute = tripPreStartMinute;
    }

    public String getTripStartHour() {
        return tripStartHour;
    }

    public void setTripStartHour(String tripStartHour) {
        this.tripStartHour = tripStartHour;
    }

    public String getTripStartMinute() {
        return tripStartMinute;
    }

    public void setTripStartMinute(String tripStartMinute) {
        this.tripStartMinute = tripStartMinute;
    }

    public String getPreStartLocationDistance() {
        return preStartLocationDistance;
    }

    public void setPreStartLocationDistance(String preStartLocationDistance) {
        this.preStartLocationDistance = preStartLocationDistance;
    }

    public String getPreStartLocationDurationHrs() {
        return preStartLocationDurationHrs;
    }

    public void setPreStartLocationDurationHrs(String preStartLocationDurationHrs) {
        this.preStartLocationDurationHrs = preStartLocationDurationHrs;
    }

    public String getPreStartLocationDurationMins() {
        return preStartLocationDurationMins;
    }

    public void setPreStartLocationDurationMins(String preStartLocationDurationMins) {
        this.preStartLocationDurationMins = preStartLocationDurationMins;
    }

    public String getPreStartLocationId() {
        return preStartLocationId;
    }

    public void setPreStartLocationId(String preStartLocationId) {
        this.preStartLocationId = preStartLocationId;
    }

    public String getPreStartLocationPlanDate() {
        return preStartLocationPlanDate;
    }

    public void setPreStartLocationPlanDate(String preStartLocationPlanDate) {
        this.preStartLocationPlanDate = preStartLocationPlanDate;
    }

    public String getPreStartLocationPlanTimeHrs() {
        return preStartLocationPlanTimeHrs;
    }

    public void setPreStartLocationPlanTimeHrs(String preStartLocationPlanTimeHrs) {
        this.preStartLocationPlanTimeHrs = preStartLocationPlanTimeHrs;
    }

    public String getPreStartLocationPlanTimeMins() {
        return preStartLocationPlanTimeMins;
    }

    public void setPreStartLocationPlanTimeMins(String preStartLocationPlanTimeMins) {
        this.preStartLocationPlanTimeMins = preStartLocationPlanTimeMins;
    }

    public String getPreStartLocationRouteExpense() {
        return preStartLocationRouteExpense;
    }

    public void setPreStartLocationRouteExpense(String preStartLocationRouteExpense) {
        this.preStartLocationRouteExpense = preStartLocationRouteExpense;
    }

    public String getPreStartLocationTollRate() {
        return preStartLocationTollRate;
    }

    public void setPreStartLocationTollRate(String preStartLocationTollRate) {
        this.preStartLocationTollRate = preStartLocationTollRate;
    }

    public String getPreStartLocationVehicleMileage() {
        return preStartLocationVehicleMileage;
    }

    public void setPreStartLocationVehicleMileage(String preStartLocationVehicleMileage) {
        this.preStartLocationVehicleMileage = preStartLocationVehicleMileage;
    }

    public String getPreStartLocationStatus() {
        return preStartLocationStatus;
    }

    public void setPreStartLocationStatus(String preStartLocationStatus) {
        this.preStartLocationStatus = preStartLocationStatus;
    }

    public String getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }

    public String getOriginId() {
        return originId;
    }

    public void setOriginId(String originId) {
        this.originId = originId;
    }

    public String getPlanEndDate() {
        return planEndDate;
    }

    public void setPlanEndDate(String planEndDate) {
        this.planEndDate = planEndDate;
    }

    public String getPlanEndHour() {
        return planEndHour;
    }

    public void setPlanEndHour(String planEndHour) {
        this.planEndHour = planEndHour;
    }

    public String getPlanEndMinute() {
        return planEndMinute;
    }

    public void setPlanEndMinute(String planEndMinute) {
        this.planEndMinute = planEndMinute;
    }

    public String getPlanEndTime() {
        return planEndTime;
    }

    public void setPlanEndTime(String planEndTime) {
        this.planEndTime = planEndTime;
    }

    public String getPlanStartDate() {
        return planStartDate;
    }

    public void setPlanStartDate(String planStartDate) {
        this.planStartDate = planStartDate;
    }

    public String getPlanStartHour() {
        return planStartHour;
    }

    public void setPlanStartHour(String planStartHour) {
        this.planStartHour = planStartHour;
    }

    public String getPlanStartMinute() {
        return planStartMinute;
    }

    public void setPlanStartMinute(String planStartMinute) {
        this.planStartMinute = planStartMinute;
    }

    public String getPlanStartTime() {
        return planStartTime;
    }

    public void setPlanStartTime(String planStartTime) {
        this.planStartTime = planStartTime;
    }

    public String getBattaAmount() {
        return battaAmount;
    }

    public void setBattaAmount(String battaAmount) {
        this.battaAmount = battaAmount;
    }

    public String getDriverBatta() {
        return driverBatta;
    }

    public void setDriverBatta(String driverBatta) {
        this.driverBatta = driverBatta;
    }

    public String getDriverIncentive() {
        return driverIncentive;
    }

    public void setDriverIncentive(String driverIncentive) {
        this.driverIncentive = driverIncentive;
    }

    public String getEstimatedExpense() {
        return estimatedExpense;
    }

    public void setEstimatedExpense(String estimatedExpense) {
        this.estimatedExpense = estimatedExpense;
    }

    public String getFuelConsumed() {
        return fuelConsumed;
    }

    public void setFuelConsumed(String fuelConsumed) {
        this.fuelConsumed = fuelConsumed;
    }

    public String getFuelCost() {
        return fuelCost;
    }

    public void setFuelCost(String fuelCost) {
        this.fuelCost = fuelCost;
    }

    public String getFuelPrice() {
        return fuelPrice;
    }

    public void setFuelPrice(String fuelPrice) {
        this.fuelPrice = fuelPrice;
    }

    public String getIncentiveAmount() {
        return incentiveAmount;
    }

    public void setIncentiveAmount(String incentiveAmount) {
        this.incentiveAmount = incentiveAmount;
    }

    public String getMilleage() {
        return milleage;
    }

    public void setMilleage(String milleage) {
        this.milleage = milleage;
    }

    public String getReeferConsumption() {
        return reeferConsumption;
    }

    public void setReeferConsumption(String reeferConsumption) {
        this.reeferConsumption = reeferConsumption;
    }

    public String getRouteExpense() {
        return routeExpense;
    }

    public void setRouteExpense(String routeExpense) {
        this.routeExpense = routeExpense;
    }

    public String getRouteExpenses() {
        return routeExpenses;
    }

    public void setRouteExpenses(String routeExpenses) {
        this.routeExpenses = routeExpenses;
    }

    public String getTollAmount() {
        return tollAmount;
    }

    public void setTollAmount(String tollAmount) {
        this.tollAmount = tollAmount;
    }

    public String getTollCost() {
        return tollCost;
    }

    public void setTollCost(String tollCost) {
        this.tollCost = tollCost;
    }

    public String getTotalExpenses() {
        return totalExpenses;
    }

    public void setTotalExpenses(String totalExpenses) {
        this.totalExpenses = totalExpenses;
    }

    public String getTotalMins() {
        return totalMins;
    }

    public void setTotalMins(String totalMins) {
        this.totalMins = totalMins;
    }

    public String getTotalRefeerHours() {
        return totalRefeerHours;
    }

    public void setTotalRefeerHours(String totalRefeerHours) {
        this.totalRefeerHours = totalRefeerHours;
    }

    public String getTotalRefeerMinutes() {
        return totalRefeerMinutes;
    }

    public void setTotalRefeerMinutes(String totalRefeerMinutes) {
        this.totalRefeerMinutes = totalRefeerMinutes;
    }

    public String getTotalRumKM() {
        return totalRumKM;
    }

    public void setTotalRumKM(String totalRumKM) {
        this.totalRumKM = totalRumKM;
    }

    public String getTripclosureid() {
        return tripclosureid;
    }

    public void setTripclosureid(String tripclosureid) {
        this.tripclosureid = tripclosureid;
    }

    public String getPreStartLocationPlanTime() {
        return preStartLocationPlanTime;
    }

    public void setPreStartLocationPlanTime(String preStartLocationPlanTime) {
        this.preStartLocationPlanTime = preStartLocationPlanTime;
    }

    public String getTripPlanEndDate() {
        return tripPlanEndDate;
    }

    public void setTripPlanEndDate(String tripPlanEndDate) {
        this.tripPlanEndDate = tripPlanEndDate;
    }

    public String getTripPlanEndTime() {
        return tripPlanEndTime;
    }

    public void setTripPlanEndTime(String tripPlanEndTime) {
        this.tripPlanEndTime = tripPlanEndTime;
    }

    public String getEndHm() {
        return endHm;
    }

    public void setEndHm(String endHm) {
        this.endHm = endHm;
    }

    public String getEndKm() {
        return endKm;
    }

    public void setEndKm(String endKm) {
        this.endKm = endKm;
    }

    public String getRunHm() {
        return runHm;
    }

    public void setRunHm(String runHm) {
        this.runHm = runHm;
    }

    public String getRunKm() {
        return runKm;
    }

    public void setRunKm(String runKm) {
        this.runKm = runKm;
    }

    public String getStartHm() {
        return startHm;
    }

    public void setStartHm(String startHm) {
        this.startHm = startHm;
    }

    public String getStartKm() {
        return startKm;
    }

    public void setStartKm(String startKm) {
        this.startKm = startKm;
    }

    public String getTripEndDate() {
        return tripEndDate;
    }

    public void setTripEndDate(String tripEndDate) {
        this.tripEndDate = tripEndDate;
    }

    public String getTripEndTime() {
        return tripEndTime;
    }

    public void setTripEndTime(String tripEndTime) {
        this.tripEndTime = tripEndTime;
    }

    public String[] getTripIds() {
        return tripIds;
    }

    public void setTripIds(String[] tripIds) {
        this.tripIds = tripIds;
    }

    public String getExpenseToBeBilledToCustomer() {
        return expenseToBeBilledToCustomer;
    }

    public void setExpenseToBeBilledToCustomer(String expenseToBeBilledToCustomer) {
        this.expenseToBeBilledToCustomer = expenseToBeBilledToCustomer;
    }

    public String getMarginValue() {
        return marginValue;
    }

    public void setMarginValue(String marginValue) {
        this.marginValue = marginValue;
    }

    public String getPassThroughStatus() {
        return passThroughStatus;
    }

    public void setPassThroughStatus(String passThroughStatus) {
        this.passThroughStatus = passThroughStatus;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getTotalRevenue() {
        return totalRevenue;
    }

    public void setTotalRevenue(String totalRevenue) {
        this.totalRevenue = totalRevenue;
    }

    public String getTotalExpToBeBilled() {
        return totalExpToBeBilled;
    }

    public void setTotalExpToBeBilled(String totalExpToBeBilled) {
        this.totalExpToBeBilled = totalExpToBeBilled;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getNoOfTrips() {
        return noOfTrips;
    }

    public void setNoOfTrips(String noOfTrips) {
        this.noOfTrips = noOfTrips;
    }

    public String getInvoiceDetailId() {
        return invoiceDetailId;
    }

    public void setInvoiceDetailId(String invoiceDetailId) {
        this.invoiceDetailId = invoiceDetailId;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getFreightAmount() {
        return freightAmount;
    }

    public void setFreightAmount(String freightAmount) {
        this.freightAmount = freightAmount;
    }

    public String getNettValue() {
        return nettValue;
    }

    public void setNettValue(String nettValue) {
        this.nettValue = nettValue;
    }

    public String getTaxValue() {
        return taxValue;
    }

    public void setTaxValue(String taxValue) {
        this.taxValue = taxValue;
    }

    public String getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(String totalValue) {
        this.totalValue = totalValue;
    }

    public double getTaxPercentageD() {
        return taxPercentageD;
    }

    public void setTaxPercentageD(double taxPercentageD) {
        this.taxPercentageD = taxPercentageD;
    }

    public String getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(String productInfo) {
        this.productInfo = productInfo;
    }

    public String getFcExpiryDate() {
        return fcExpiryDate;
    }

    public void setFcExpiryDate(String fcExpiryDate) {
        this.fcExpiryDate = fcExpiryDate;
    }

    public String getInsuranceExpiryDate() {
        return insuranceExpiryDate;
    }

    public void setInsuranceExpiryDate(String insuranceExpiryDate) {
        this.insuranceExpiryDate = insuranceExpiryDate;
    }

    public String getPermitExpiryDate() {
        return permitExpiryDate;
    }

    public void setPermitExpiryDate(String permitExpiryDate) {
        this.permitExpiryDate = permitExpiryDate;
    }

    public String getRoadTaxExpiryDate() {
        return roadTaxExpiryDate;
    }

    public void setRoadTaxExpiryDate(String roadTaxExpiryDate) {
        this.roadTaxExpiryDate = roadTaxExpiryDate;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getTripStatusId() {
        return tripStatusId;
    }

    public void setTripStatusId(String tripStatusId) {
        this.tripStatusId = tripStatusId;
    }

    public String getFleetCenterId() {
        return fleetCenterId;
    }

    public void setFleetCenterId(String fleetCenterId) {
        this.fleetCenterId = fleetCenterId;
    }

    public String getBookedExpense() {
        return bookedExpense;
    }

    public void setBookedExpense(String bookedExpense) {
        this.bookedExpense = bookedExpense;
    }

    public String getDieselCost() {
        return dieselCost;
    }

    public void setDieselCost(String dieselCost) {
        this.dieselCost = dieselCost;
    }

    public String getDieselUsed() {
        return dieselUsed;
    }

    public void setDieselUsed(String dieselUsed) {
        this.dieselUsed = dieselUsed;
    }

    public String getDriverBattaPerDay() {
        return driverBattaPerDay;
    }

    public void setDriverBattaPerDay(String driverBattaPerDay) {
        this.driverBattaPerDay = driverBattaPerDay;
    }

    public String getDriverIncentivePerKm() {
        return driverIncentivePerKm;
    }

    public void setDriverIncentivePerKm(String driverIncentivePerKm) {
        this.driverIncentivePerKm = driverIncentivePerKm;
    }

    public String getReeferMileage() {
        return reeferMileage;
    }

    public void setReeferMileage(String reeferMileage) {
        this.reeferMileage = reeferMileage;
    }

    public String getTollRate() {
        return tollRate;
    }

    public void setTollRate(String tollRate) {
        this.tollRate = tollRate;
    }

    public String getSystemExpense() {
        return systemExpense;
    }

    public void setSystemExpense(String systemExpense) {
        this.systemExpense = systemExpense;
    }

    public String getApprovalRemarks() {
        return approvalRemarks;
    }

    public void setApprovalRemarks(String approvalRemarks) {
        this.approvalRemarks = approvalRemarks;
    }

    public String getApprovalRequestBy() {
        return approvalRequestBy;
    }

    public void setApprovalRequestBy(String approvalRequestBy) {
        this.approvalRequestBy = approvalRequestBy;
    }

    public String getApprovalRequestOn() {
        return approvalRequestOn;
    }

    public void setApprovalRequestOn(String approvalRequestOn) {
        this.approvalRequestOn = approvalRequestOn;
    }

    public String getApprovalRequestRemarks() {
        return approvalRequestRemarks;
    }

    public void setApprovalRequestRemarks(String approvalRequestRemarks) {
        this.approvalRequestRemarks = approvalRequestRemarks;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getApprovedOn() {
        return approvedOn;
    }

    public void setApprovedOn(String approvedOn) {
        this.approvedOn = approvedOn;
    }

    public String getNettExpense() {
        return nettExpense;
    }

    public void setNettExpense(String nettExpense) {
        this.nettExpense = nettExpense;
    }

    public String getOtherExpense() {
        return otherExpense;
    }

    public void setOtherExpense(String otherExpense) {
        this.otherExpense = otherExpense;
    }

    public String getRcmExpense() {
        return rcmExpense;
    }

    public void setRcmExpense(String rcmExpense) {
        this.rcmExpense = rcmExpense;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getArticleCode() {
        return articleCode;
    }

    public void setArticleCode(String articleCode) {
        this.articleCode = articleCode;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getConsignmentId() {
        return consignmentId;
    }

    public void setConsignmentId(String consignmentId) {
        this.consignmentId = consignmentId;
    }

    public String[] getLoadedpackages() {
        return loadedpackages;
    }

    public void setLoadedpackages(String[] loadedpackages) {
        this.loadedpackages = loadedpackages;
    }

    public String getLoadpackageNos() {
        return loadpackageNos;
    }

    public void setLoadpackageNos(String loadpackageNos) {
        this.loadpackageNos = loadpackageNos;
    }

    public String getPackageNos() {
        return packageNos;
    }

    public void setPackageNos(String packageNos) {
        this.packageNos = packageNos;
    }

    public String getPackageWeight() {
        return packageWeight;
    }

    public void setPackageWeight(String packageWeight) {
        this.packageWeight = packageWeight;
    }

    public String[] getPackagesNos() {
        return packagesNos;
    }

    public void setPackagesNos(String[] packagesNos) {
        this.packagesNos = packagesNos;
    }

    public String[] getProductCodes() {
        return productCodes;
    }

    public void setProductCodes(String[] productCodes) {
        this.productCodes = productCodes;
    }

    public String[] getProductNames() {
        return productNames;
    }

    public void setProductNames(String[] productNames) {
        this.productNames = productNames;
    }

    public String[] getProductbatch() {
        return productbatch;
    }

    public void setProductbatch(String[] productbatch) {
        this.productbatch = productbatch;
    }

    public String[] getProductuom() {
        return productuom;
    }

    public void setProductuom(String[] productuom) {
        this.productuom = productuom;
    }

    public String[] getShortage() {
        return shortage;
    }

    public void setShortage(String[] shortage) {
        this.shortage = shortage;
    }

    public String[] getTripArticleId() {
        return tripArticleId;
    }

    public void setTripArticleId(String[] tripArticleId) {
        this.tripArticleId = tripArticleId;
    }

    public String getTripArticleid() {
        return tripArticleid;
    }

    public void setTripArticleid(String tripArticleid) {
        this.tripArticleid = tripArticleid;
    }

    public String[] getUnloadedpackages() {
        return unloadedpackages;
    }

    public void setUnloadedpackages(String[] unloadedpackages) {
        this.unloadedpackages = unloadedpackages;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getVehicleactreportdate() {
        return vehicleactreportdate;
    }

    public void setVehicleactreportdate(String vehicleactreportdate) {
        this.vehicleactreportdate = vehicleactreportdate;
    }

    public String getVehicleactreporthour() {
        return vehicleactreporthour;
    }

    public void setVehicleactreporthour(String vehicleactreporthour) {
        this.vehicleactreporthour = vehicleactreporthour;
    }

    public String getVehicleactreportmin() {
        return vehicleactreportmin;
    }

    public void setVehicleactreportmin(String vehicleactreportmin) {
        this.vehicleactreportmin = vehicleactreportmin;
    }

    public String getVehicleloadreportdate() {
        return vehicleloadreportdate;
    }

    public void setVehicleloadreportdate(String vehicleloadreportdate) {
        this.vehicleloadreportdate = vehicleloadreportdate;
    }

    public String getVehicleloadreporthour() {
        return vehicleloadreporthour;
    }

    public void setVehicleloadreporthour(String vehicleloadreporthour) {
        this.vehicleloadreporthour = vehicleloadreporthour;
    }

    public String getVehicleloadreportmin() {
        return vehicleloadreportmin;
    }

    public void setVehicleloadreportmin(String vehicleloadreportmin) {
        this.vehicleloadreportmin = vehicleloadreportmin;
    }

    public String getVehicleloadtemperature() {
        return vehicleloadtemperature;
    }

    public void setVehicleloadtemperature(String vehicleloadtemperature) {
        this.vehicleloadtemperature = vehicleloadtemperature;
    }

    public String[] getWeights() {
        return weights;
    }

    public void setWeights(String[] weights) {
        this.weights = weights;
    }

    public String getRequeston() {
        return requeston;
    }

    public void setRequeston(String requeston) {
        this.requeston = requeston;
    }

    public String getRequestremarks() {
        return requestremarks;
    }

    public void setRequestremarks(String requestremarks) {
        this.requestremarks = requestremarks;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getTotalRumHM() {
        return totalRumHM;
    }

    public void setTotalRumHM(String totalRumHM) {
        this.totalRumHM = totalRumHM;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getSmtp() {
        return smtp;
    }

    public void setSmtp(String smtp) {
        this.smtp = smtp;
    }

    public String getTomailId() {
        return tomailId;
    }

    public void setTomailId(String tomailId) {
        this.tomailId = tomailId;
    }

    public String getMiscValue() {
        return miscValue;
    }

    public void setMiscValue(String miscValue) {
        this.miscValue = miscValue;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getFcHeadEmail() {
        return fcHeadEmail;
    }

    public void setFcHeadEmail(String fcHeadEmail) {
        this.fcHeadEmail = fcHeadEmail;
    }

    public String getUomId() {
        return uomId;
    }

    public void setUomId(String uomId) {
        this.uomId = uomId;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public String getStakeHolderId() {
        return stakeHolderId;
    }

    public void setStakeHolderId(String stakeHolderId) {
        this.stakeHolderId = stakeHolderId;
    }

    public String getcNotesEmail() {
        return cNotesEmail;
    }

    public void setcNotesEmail(String cNotesEmail) {
        this.cNotesEmail = cNotesEmail;
    }

    public String getCustomerNameEmail() {
        return customerNameEmail;
    }

    public void setCustomerNameEmail(String customerNameEmail) {
        this.customerNameEmail = customerNameEmail;
    }

    public String getRouteInfoEmail() {
        return routeInfoEmail;
    }

    public void setRouteInfoEmail(String routeInfoEmail) {
        this.routeInfoEmail = routeInfoEmail;
    }

    public String getTripCodeEmail() {
        return tripCodeEmail;
    }

    public void setTripCodeEmail(String tripCodeEmail) {
        this.tripCodeEmail = tripCodeEmail;
    }

    public String getVehicleNoEmail() {
        return vehicleNoEmail;
    }

    public void setVehicleNoEmail(String vehicleNoEmail) {
        this.vehicleNoEmail = vehicleNoEmail;
    }

    public String getAdvanceAmount() {
        return advanceAmount;
    }

    public void setAdvanceAmount(String advanceAmount) {
        this.advanceAmount = advanceAmount;
    }

    public String getAdvanceRemarks() {
        return advanceRemarks;
    }

    public void setAdvanceRemarks(String advanceRemarks) {
        this.advanceRemarks = advanceRemarks;
    }

    public String getSecondaryDriverIdOne() {
        return secondaryDriverIdOne;
    }

    public void setSecondaryDriverIdOne(String secondaryDriverIdOne) {
        this.secondaryDriverIdOne = secondaryDriverIdOne;
    }

    public String getSecondaryDriverIdTwo() {
        return secondaryDriverIdTwo;
    }

    public void setSecondaryDriverIdTwo(String secondaryDriverIdTwo) {
        this.secondaryDriverIdTwo = secondaryDriverIdTwo;
    }

    public String getSecondaryDriverNameOne() {
        return secondaryDriverNameOne;
    }

    public void setSecondaryDriverNameOne(String secondaryDriverNameOne) {
        this.secondaryDriverNameOne = secondaryDriverNameOne;
    }

    public String getSecondaryDriverNameTwo() {
        return secondaryDriverNameTwo;
    }

    public void setSecondaryDriverNameTwo(String secondaryDriverNameTwo) {
        this.secondaryDriverNameTwo = secondaryDriverNameTwo;
    }

    public String getZoneName() {
        return ZoneName;
    }

    public void setZoneName(String ZoneName) {
        this.ZoneName = ZoneName;
    }

    public String getEndReportingDate() {
        return endReportingDate;
    }

    public void setEndReportingDate(String endReportingDate) {
        this.endReportingDate = endReportingDate;
    }

    public String getEndReportingTime() {
        return endReportingTime;
    }

    public void setEndReportingTime(String endReportingTime) {
        this.endReportingTime = endReportingTime;
    }

    public String getLoadingDate() {
        return loadingDate;
    }

    public void setLoadingDate(String loadingDate) {
        this.loadingDate = loadingDate;
    }

    public String getLoadingTemperature() {
        return loadingTemperature;
    }

    public void setLoadingTemperature(String loadingTemperature) {
        this.loadingTemperature = loadingTemperature;
    }

    public String getLoadingTime() {
        return loadingTime;
    }

    public void setLoadingTime(String loadingTime) {
        this.loadingTime = loadingTime;
    }

    public String getLrNumber() {
        return lrNumber;
    }

    public void setLrNumber(String lrNumber) {
        this.lrNumber = lrNumber;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getStartReportingDate() {
        return startReportingDate;
    }

    public void setStartReportingDate(String startReportingDate) {
        this.startReportingDate = startReportingDate;
    }

    public String getStartReportingTime() {
        return startReportingTime;
    }

    public void setStartReportingTime(String startReportingTime) {
        this.startReportingTime = startReportingTime;
    }

    public String getTripBalance() {
        return tripBalance;
    }

    public void setTripBalance(String tripBalance) {
        this.tripBalance = tripBalance;
    }

    public String getTripEndBalance() {
        return tripEndBalance;
    }

    public void setTripEndBalance(String tripEndBalance) {
        this.tripEndBalance = tripEndBalance;
    }

    public String getTripStartingBalance() {
        return tripStartingBalance;
    }

    public void setTripStartingBalance(String tripStartingBalance) {
        this.tripStartingBalance = tripStartingBalance;
    }

    public String getUnLoadingDate() {
        return unLoadingDate;
    }

    public void setUnLoadingDate(String unLoadingDate) {
        this.unLoadingDate = unLoadingDate;
    }

    public String getUnLoadingTemperature() {
        return unLoadingTemperature;
    }

    public void setUnLoadingTemperature(String unLoadingTemperature) {
        this.unLoadingTemperature = unLoadingTemperature;
    }

    public String getUnLoadingTime() {
        return unLoadingTime;
    }

    public void setUnLoadingTime(String unLoadingTime) {
        this.unLoadingTime = unLoadingTime;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getCityFromId() {
        return cityFromId;
    }

    public void setCityFromId(String cityFromId) {
        this.cityFromId = cityFromId;
    }

    public String getCurrentTemperature() {
        return currentTemperature;
    }

    public void setCurrentTemperature(String currentTemperature) {
        this.currentTemperature = currentTemperature;
    }

    public String getExpectedArrivalDate() {
        return expectedArrivalDate;
    }

    public void setExpectedArrivalDate(String expectedArrivalDate) {
        this.expectedArrivalDate = expectedArrivalDate;
    }

    public String getExpectedArrivalTime() {
        return expectedArrivalTime;
    }

    public void setExpectedArrivalTime(String expectedArrivalTime) {
        this.expectedArrivalTime = expectedArrivalTime;
    }

    public String getFleetCenterName() {
        return fleetCenterName;
    }

    public void setFleetCenterName(String fleetCenterName) {
        this.fleetCenterName = fleetCenterName;
    }

    public String getFleetCenterNo() {
        return fleetCenterNo;
    }

    public void setFleetCenterNo(String fleetCenterNo) {
        this.fleetCenterNo = fleetCenterNo;
    }

    public String getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(String gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountingDate() {
        return accountingDate;
    }

    public void setAccountingDate(String accountingDate) {
        this.accountingDate = accountingDate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAmoutBalance() {
        return amoutBalance;
    }

    public void setAmoutBalance(String amoutBalance) {
        this.amoutBalance = amoutBalance;
    }

    public String getBpclTransactionId() {
        return bpclTransactionId;
    }

    public void setBpclTransactionId(String bpclTransactionId) {
        this.bpclTransactionId = bpclTransactionId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDealerCity() {
        return dealerCity;
    }

    public void setDealerCity(String dealerCity) {
        this.dealerCity = dealerCity;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getOdometerReading() {
        return odometerReading;
    }

    public void setOdometerReading(String odometerReading) {
        this.odometerReading = odometerReading;
    }

    public String getPetromilesEarned() {
        return petromilesEarned;
    }

    public void setPetromilesEarned(String petromilesEarned) {
        this.petromilesEarned = petromilesEarned;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionHistoryId() {
        return transactionHistoryId;
    }

    public void setTransactionHistoryId(String transactionHistoryId) {
        this.transactionHistoryId = transactionHistoryId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getVolumeDocNo() {
        return volumeDocNo;
    }

    public void setVolumeDocNo(String volumeDocNo) {
        this.volumeDocNo = volumeDocNo;
    }

    public String getConsigneeAddress() {
        return consigneeAddress;
    }

    public void setConsigneeAddress(String consigneeAddress) {
        this.consigneeAddress = consigneeAddress;
    }

    public String getConsigneeMobileNo() {
        return consigneeMobileNo;
    }

    public void setConsigneeMobileNo(String consigneeMobileNo) {
        this.consigneeMobileNo = consigneeMobileNo;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsignorAddress() {
        return consignorAddress;
    }

    public void setConsignorAddress(String consignorAddress) {
        this.consignorAddress = consignorAddress;
    }

    public String getConsignorMobileNo() {
        return consignorMobileNo;
    }

    public void setConsignorMobileNo(String consignorMobileNo) {
        this.consignorMobileNo = consignorMobileNo;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getPodCount() {
        return podCount;
    }

    public void setPodCount(String podCount) {
        this.podCount = podCount;
    }

    public String getPaidStatus() {
        return paidStatus;
    }

    public void setPaidStatus(String paidStatus) {
        this.paidStatus = paidStatus;
    }

    public String getEstimatedKM() {
        return estimatedKM;
    }

    public void setEstimatedKM(String estimatedKM) {
        this.estimatedKM = estimatedKM;
    }

    public String getCustomerOrderReferenceNo() {
        return customerOrderReferenceNo;
    }

    public void setCustomerOrderReferenceNo(String customerOrderReferenceNo) {
        this.customerOrderReferenceNo = customerOrderReferenceNo;
    }

    public String getAdvanceToPayStatus() {
        return advanceToPayStatus;
    }

    public void setAdvanceToPayStatus(String advanceToPayStatus) {
        this.advanceToPayStatus = advanceToPayStatus;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getToPayStatus() {
        return toPayStatus;
    }

    public void setToPayStatus(String toPayStatus) {
        this.toPayStatus = toPayStatus;
    }

    public String getPodStatus() {
        return podStatus;
    }

    public void setPodStatus(String podStatus) {
        this.podStatus = podStatus;
    }

    public String getWfuRemarks() {
        return wfuRemarks;
    }

    public void setWfuRemarks(String wfuRemarks) {
        this.wfuRemarks = wfuRemarks;
    }

    public String getWfuUnloadingDate() {
        return wfuUnloadingDate;
    }

    public void setWfuUnloadingDate(String wfuUnloadingDate) {
        this.wfuUnloadingDate = wfuUnloadingDate;
    }

    public String getWfuUnloadingTime() {
        return wfuUnloadingTime;
    }

    public void setWfuUnloadingTime(String wfuUnloadingTime) {
        this.wfuUnloadingTime = wfuUnloadingTime;
    }

    public String getTripStartDate() {
        return tripStartDate;
    }

    public void setTripStartDate(String tripStartDate) {
        this.tripStartDate = tripStartDate;
    }

    public String getTripStartTime() {
        return tripStartTime;
    }

    public void setTripStartTime(String tripStartTime) {
        this.tripStartTime = tripStartTime;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getEmptyTripApprovalStatus() {
        return emptyTripApprovalStatus;
    }

    public void setEmptyTripApprovalStatus(String emptyTripApprovalStatus) {
        this.emptyTripApprovalStatus = emptyTripApprovalStatus;
    }

    public int getRouteContractId() {
        return routeContractId;
    }

    public void setRouteContractId(int routeContractId) {
        this.routeContractId = routeContractId;
    }

    public String getActualAdvancePaid() {
        return actualAdvancePaid;
    }

    public void setActualAdvancePaid(String actualAdvancePaid) {
        this.actualAdvancePaid = actualAdvancePaid;
    }

    public String getSecondaryAddlTollAmount() {
        return secondaryAddlTollAmount;
    }

    public void setSecondaryAddlTollAmount(String secondaryAddlTollAmount) {
        this.secondaryAddlTollAmount = secondaryAddlTollAmount;
    }

    public String getSecondaryMiscAmount() {
        return secondaryMiscAmount;
    }

    public void setSecondaryMiscAmount(String secondaryMiscAmount) {
        this.secondaryMiscAmount = secondaryMiscAmount;
    }

    public String getSecondaryTollAmount() {
        return secondaryTollAmount;
    }

    public void setSecondaryTollAmount(String secondaryTollAmount) {
        this.secondaryTollAmount = secondaryTollAmount;
    }

    public String getRouteNameStatus() {
        return routeNameStatus;
    }

    public void setRouteNameStatus(String routeNameStatus) {
        this.routeNameStatus = routeNameStatus;
    }

    public String getGpsKm() {
        return gpsKm;
    }

    public void setGpsKm(String gpsKm) {
        this.gpsKm = gpsKm;
    }

    public String getGpsHm() {
        return gpsHm;
    }

    public void setGpsHm(String gpsHm) {
        this.gpsHm = gpsHm;
    }

    public String getTempLogFile() {
        return tempLogFile;
    }

    public void setTempLogFile(String tempLogFile) {
        this.tempLogFile = tempLogFile;
    }

    public String getConsignmentDate() {
        return consignmentDate;
    }

    public void setConsignmentDate(String consignmentDate) {
        this.consignmentDate = consignmentDate;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public String getDriverMobile() {
        return driverMobile;
    }

    public void setDriverMobile(String driverMobile) {
        this.driverMobile = driverMobile;
    }

    public String getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(String paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(String paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public String getTotalPackages() {
        return totalPackages;
    }

    public void setTotalPackages(String totalPackages) {
        this.totalPackages = totalPackages;
    }

    public String getVehicleRequiredDateTime() {
        return vehicleRequiredDateTime;
    }

    public void setVehicleRequiredDateTime(String vehicleRequiredDateTime) {
        this.vehicleRequiredDateTime = vehicleRequiredDateTime;
    }

    public String getVehicleDriverAdvanceId() {
        return vehicleDriverAdvanceId;
    }

    public void setVehicleDriverAdvanceId(String vehicleDriverAdvanceId) {
        this.vehicleDriverAdvanceId = vehicleDriverAdvanceId;
    }

    public String getTripCountStatus() {
        return tripCountStatus;
    }

    public void setTripCountStatus(String tripCountStatus) {
        this.tripCountStatus = tripCountStatus;
    }

    public String getDriverCount() {
        return driverCount;
    }

    public void setDriverCount(String driverCount) {
        this.driverCount = driverCount;
    }

    public String getExtraExpenseValue() {
        return extraExpenseValue;
    }

    public void setExtraExpenseValue(String extraExpenseValue) {
        this.extraExpenseValue = extraExpenseValue;
    }

    public String getEmailCc() {
        return emailCc;
    }

    public void setEmailCc(String emailCc) {
        this.emailCc = emailCc;
    }

    public String getInfoEmailTo() {
        return infoEmailTo;
    }

    public void setInfoEmailTo(String infoEmailTo) {
        this.infoEmailTo = infoEmailTo;
    }

    public String getInfoEmailCc() {
        return infoEmailCc;
    }

    public void setInfoEmailCc(String infoEmailCc) {
        this.infoEmailCc = infoEmailCc;
    }

    public String getApprovalstatus() {
        return approvalstatus;
    }

    public void setApprovalstatus(String approvalstatus) {
        this.approvalstatus = approvalstatus;
    }

    public String getRatePerKm() {
        return ratePerKm;
    }

    public void setRatePerKm(String ratePerKm) {
        this.ratePerKm = ratePerKm;
    }

    public String getTripGraphId() {
        return tripGraphId;
    }

    public void setTripGraphId(String tripGraphId) {
        this.tripGraphId = tripGraphId;
    }

    public String getTempApprovalStatus() {
        return tempApprovalStatus;
    }

    public void setTempApprovalStatus(String tempApprovalStatus) {
        this.tempApprovalStatus = tempApprovalStatus;
    }

    public String getVehicleDieselUsed() {
        return vehicleDieselUsed;
    }

    public void setVehicleDieselUsed(String vehicleDieselUsed) {
        this.vehicleDieselUsed = vehicleDieselUsed;
    }

    public String getReeferDieselUsed() {
        return reeferDieselUsed;
    }

    public void setReeferDieselUsed(String reeferDieselUsed) {
        this.reeferDieselUsed = reeferDieselUsed;
    }

    public String getClosureTotalExpense() {
        return closureTotalExpense;
    }

    public void setClosureTotalExpense(String closureTotalExpense) {
        this.closureTotalExpense = closureTotalExpense;
    }

    public String getBillingNameAddress() {
        return billingNameAddress;
    }

    public void setBillingNameAddress(String billingNameAddress) {
        this.billingNameAddress = billingNameAddress;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getBilledAmount() {
        return billedAmount;
    }

    public void setBilledAmount(String billedAmount) {
        this.billedAmount = billedAmount;
    }

    public String getEditMode() {
        return editMode;
    }

    public void setEditMode(String editMode) {
        this.editMode = editMode;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getRatePerKg() {
        return ratePerKg;
    }

    public void setRatePerKg(String ratePerKg) {
        this.ratePerKg = ratePerKg;
    }

    public String getCourierRemarks() {
        return courierRemarks;
    }

    public void setCourierRemarks(String courierRemarks) {
        this.courierRemarks = courierRemarks;
    }

    public String getCourierNo() {
        return courierNo;
    }

    public void setCourierNo(String courierNo) {
        this.courierNo = courierNo;
    }

    public String getLogDateTime() {
        return logDateTime;
    }

    public void setLogDateTime(String logDateTime) {
        this.logDateTime = logDateTime;
    }

    public String getLogDate() {
        return logDate;
    }

    public void setLogDate(String logDate) {
        this.logDate = logDate;
    }

    public String getLogTime() {
        return logTime;
    }

    public void setLogTime(String logTime) {
        this.logTime = logTime;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getFuelTypeId() {
        return fuelTypeId;
    }

    public void setFuelTypeId(String fuelTypeId) {
        this.fuelTypeId = fuelTypeId;
    }

    public String getReturnAmount() {
        return returnAmount;
    }

    public void setReturnAmount(String returnAmount) {
        this.returnAmount = returnAmount;
    }

    public String getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getLicenceNo() {
        return licenceNo;
    }

    public void setLicenceNo(String licenceNo) {
        this.licenceNo = licenceNo;
    }

    public String getLicenceDate() {
        return licenceDate;
    }

    public void setLicenceDate(String licenceDate) {
        this.licenceDate = licenceDate;
    }

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(String joiningDate) {
        this.joiningDate = joiningDate;
    }

    public String getLastVehicleEndKm() {
        return lastVehicleEndKm;
    }

    public void setLastVehicleEndKm(String lastVehicleEndKm) {
        this.lastVehicleEndKm = lastVehicleEndKm;
    }

    public String getVehicleStartKm() {
        return vehicleStartKm;
    }

    public void setVehicleStartKm(String vehicleStartKm) {
        this.vehicleStartKm = vehicleStartKm;
    }

    public String getOldVehicleNo() {
        return oldVehicleNo;
    }

    public void setOldVehicleNo(String oldVehicleNo) {
        this.oldVehicleNo = oldVehicleNo;
    }

    public String getOldDriverName() {
        return oldDriverName;
    }

    public void setOldDriverName(String oldDriverName) {
        this.oldDriverName = oldDriverName;
    }

    public String getVehicleRemarks() {
        return vehicleRemarks;
    }

    public void setVehicleRemarks(String vehicleRemarks) {
        this.vehicleRemarks = vehicleRemarks;
    }

    public String getVehicleChangeDate() {
        return vehicleChangeDate;
    }

    public void setVehicleChangeDate(String vehicleChangeDate) {
        this.vehicleChangeDate = vehicleChangeDate;
    }

    public String getVehicleChangeMinute() {
        return vehicleChangeMinute;
    }

    public void setVehicleChangeMinute(String vehicleChangeMinute) {
        this.vehicleChangeMinute = vehicleChangeMinute;
    }

    public String getVehicleChangeHour() {
        return vehicleChangeHour;
    }

    public void setVehicleChangeHour(String vehicleChangeHour) {
        this.vehicleChangeHour = vehicleChangeHour;
    }

    public String getLastVehicleEndOdometer() {
        return lastVehicleEndOdometer;
    }

    public void setLastVehicleEndOdometer(String lastVehicleEndOdometer) {
        this.lastVehicleEndOdometer = lastVehicleEndOdometer;
    }

    public String getVehicleStartOdometer() {
        return vehicleStartOdometer;
    }

    public void setVehicleStartOdometer(String vehicleStartOdometer) {
        this.vehicleStartOdometer = vehicleStartOdometer;
    }

    public String getEmptyTrip() {
        return emptyTrip;
    }

    public void setEmptyTrip(String emptyTrip) {
        this.emptyTrip = emptyTrip;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getMailId() {
        return mailId;
    }

    public void setMailId(String mailId) {
        this.mailId = mailId;
    }

    public String getMailIdBcc() {
        return mailIdBcc;
    }

    public void setMailIdBcc(String mailIdBcc) {
        this.mailIdBcc = mailIdBcc;
    }

    public String getMailIdCc() {
        return mailIdCc;
    }

    public void setMailIdCc(String mailIdCc) {
        this.mailIdCc = mailIdCc;
    }

    public String getMailIdTo() {
        return mailIdTo;
    }

    public void setMailIdTo(String mailIdTo) {
        this.mailIdTo = mailIdTo;
    }

    public String getMailContentBcc() {
        return mailContentBcc;
    }

    public void setMailContentBcc(String mailContentBcc) {
        this.mailContentBcc = mailContentBcc;
    }

    public String getMailContentCc() {
        return mailContentCc;
    }

    public void setMailContentCc(String mailContentCc) {
        this.mailContentCc = mailContentCc;
    }

    public String getMailContentTo() {
        return mailContentTo;
    }

    public void setMailContentTo(String mailContentTo) {
        this.mailContentTo = mailContentTo;
    }

    public String getMailSubjectBcc() {
        return mailSubjectBcc;
    }

    public void setMailSubjectBcc(String mailSubjectBcc) {
        this.mailSubjectBcc = mailSubjectBcc;
    }

    public String getMailSubjectCc() {
        return mailSubjectCc;
    }

    public void setMailSubjectCc(String mailSubjectCc) {
        this.mailSubjectCc = mailSubjectCc;
    }

    public String getMailSubjectTo() {
        return mailSubjectTo;
    }

    public void setMailSubjectTo(String mailSubjectTo) {
        this.mailSubjectTo = mailSubjectTo;
    }

    public String getMailTypeId() {
        return mailTypeId;
    }

    public void setMailTypeId(String mailTypeId) {
        this.mailTypeId = mailTypeId;
    }

    public String getJobcardId() {
        return jobcardId;
    }

    public void setJobcardId(String jobcardId) {
        this.jobcardId = jobcardId;
    }

    public String getMailDate() {
        return mailDate;
    }

    public void setMailDate(String mailDate) {
        this.mailDate = mailDate;
    }

    public String getStartedBy() {
        return startedBy;
    }

    public void setStartedBy(String startedBy) {
        this.startedBy = startedBy;
    }

    public String getEndedBy() {
        return endedBy;
    }

    public void setEndedBy(String endedBy) {
        this.endedBy = endedBy;
    }

    public String getExtraExpenseStatus() {
        return extraExpenseStatus;
    }

    public void setExtraExpenseStatus(String extraExpenseStatus) {
        this.extraExpenseStatus = extraExpenseStatus;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getOutStandingAmount() {
        return outStandingAmount;
    }

    public void setOutStandingAmount(String outStandingAmount) {
        this.outStandingAmount = outStandingAmount;
    }

    public String getApprovalStatusId() {
        return approvalStatusId;
    }

    public void setApprovalStatusId(String approvalStatusId) {
        this.approvalStatusId = approvalStatusId;
    }

    public String getMailSendingId() {
        return mailSendingId;
    }

    public void setMailSendingId(String mailSendingId) {
        this.mailSendingId = mailSendingId;
    }

    public String getMailDeliveredStatusId() {
        return mailDeliveredStatusId;
    }

    public void setMailDeliveredStatusId(String mailDeliveredStatusId) {
        this.mailDeliveredStatusId = mailDeliveredStatusId;
    }

    public String getCityFrom() {
        return cityFrom;
    }

    public void setCityFrom(String cityFrom) {
        this.cityFrom = cityFrom;
    }

    public String getCityTo() {
        return cityTo;
    }

    public void setCityTo(String cityTo) {
        this.cityTo = cityTo;
    }

    public String getEmptyTripRemarks() {
        return emptyTripRemarks;
    }

    public void setEmptyTripRemarks(String emptyTripRemarks) {
        this.emptyTripRemarks = emptyTripRemarks;
    }

    public String getExpense() {
        return expense;
    }

    public void setExpense(String expense) {
        this.expense = expense;
    }

    public String getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(String totalHours) {
        this.totalHours = totalHours;
    }

    public String getTotalKm() {
        return totalKm;
    }

    public void setTotalKm(String totalKm) {
        this.totalKm = totalKm;
    }

    public String getTotalMinutes() {
        return totalMinutes;
    }

    public void setTotalMinutes(String totalMinutes) {
        this.totalMinutes = totalMinutes;
    }

    public String getSecondaryParkingAmount() {
        return secondaryParkingAmount;
    }

    public void setSecondaryParkingAmount(String secondaryParkingAmount) {
        this.secondaryParkingAmount = secondaryParkingAmount;
    }

    public String getPreColingAmount() {
        return preColingAmount;
    }

    public void setPreColingAmount(String preColingAmount) {
        this.preColingAmount = preColingAmount;
    }

    public String getSecAdditionalTollCost() {
        return secAdditionalTollCost;
    }

    public void setSecAdditionalTollCost(String secAdditionalTollCost) {
        this.secAdditionalTollCost = secAdditionalTollCost;
    }

    public String getUsageTypeId() {
        return usageTypeId;
    }

    public void setUsageTypeId(String usageTypeId) {
        this.usageTypeId = usageTypeId;
    }

    public String getTripWfuDate() {
        return tripWfuDate;
    }

    public void setTripWfuDate(String tripWfuDate) {
        this.tripWfuDate = tripWfuDate;
    }

    public String getTripWfuTime() {
        return tripWfuTime;
    }

    public void setTripWfuTime(String tripWfuTime) {
        this.tripWfuTime = tripWfuTime;
    }

    public String getTripAdvanceId() {
        return tripAdvanceId;
    }

    public void setTripAdvanceId(String tripAdvanceId) {
        this.tripAdvanceId = tripAdvanceId;
    }

    public String getDestinationCityName() {
        return destinationCityName;
    }

    public void setDestinationCityName(String destinationCityName) {
        this.destinationCityName = destinationCityName;
    }

    public String getOriginCityName() {
        return originCityName;
    }

    public void setOriginCityName(String originCityName) {
        this.originCityName = originCityName;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getTotalHm() {
        return totalHm;
    }

    public void setTotalHm(String totalHm) {
        this.totalHm = totalHm;
    }

    public String getVehicleChangeCityName() {
        return vehicleChangeCityName;
    }

    public void setVehicleChangeCityName(String vehicleChangeCityName) {
        this.vehicleChangeCityName = vehicleChangeCityName;
    }

    public String getTransitDays1() {
        return transitDays1;
    }

    public void setTransitDays1(String transitDays1) {
        this.transitDays1 = transitDays1;
    }

    public String getTransitDays2() {
        return transitDays2;
    }

    public void setTransitDays2(String transitDays2) {
        this.transitDays2 = transitDays2;
    }

    public String getEmptyTripPurpose() {
        return emptyTripPurpose;
    }

    public void setEmptyTripPurpose(String emptyTripPurpose) {
        this.emptyTripPurpose = emptyTripPurpose;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getRaisedBy() {
        return raisedBy;
    }

    public void setRaisedBy(String raisedBy) {
        this.raisedBy = raisedBy;
    }

    public String getRaisedOn() {
        return raisedOn;
    }

    public void setRaisedOn(String raisedOn) {
        this.raisedOn = raisedOn;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTicketingDetailId() {
        return ticketingDetailId;
    }

    public void setTicketingDetailId(String ticketingDetailId) {
        this.ticketingDetailId = ticketingDetailId;
    }

    public String getTicketingFile() {
        return ticketingFile;
    }

    public void setTicketingFile(String ticketingFile) {
        this.ticketingFile = ticketingFile;
    }

    public String getFollowedBy() {
        return followedBy;
    }

    public void setFollowedBy(String followedBy) {
        this.followedBy = followedBy;
    }

    public String getFollowedOn() {
        return followedOn;
    }

    public void setFollowedOn(String followedOn) {
        this.followedOn = followedOn;
    }

    public String getSetteledHm() {
        return setteledHm;
    }

    public void setSetteledHm(String setteledHm) {
        this.setteledHm = setteledHm;
    }

    public String getSetteledKm() {
        return setteledKm;
    }

    public void setSetteledKm(String setteledKm) {
        this.setteledKm = setteledKm;
    }

    public String getSetteltotalHrs() {
        return setteltotalHrs;
    }

    public void setSetteltotalHrs(String setteltotalHrs) {
        this.setteltotalHrs = setteltotalHrs;
    }

    public String getSetteltotalKM() {
        return setteltotalKM;
    }

    public void setSetteltotalKM(String setteltotalKM) {
        this.setteltotalKM = setteltotalKM;
    }

    public String getOverRideBy() {
        return overRideBy;
    }

    public void setOverRideBy(String overRideBy) {
        this.overRideBy = overRideBy;
    }

    public String getOverRideOn() {
        return overRideOn;
    }

    public void setOverRideOn(String overRideOn) {
        this.overRideOn = overRideOn;
    }

    public String getOverrideTripRemarks() {
        return overrideTripRemarks;
    }

    public void setOverrideTripRemarks(String overrideTripRemarks) {
        this.overrideTripRemarks = overrideTripRemarks;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getNextTrip() {
        return nextTrip;
    }

    public void setNextTrip(String nextTrip) {
        this.nextTrip = nextTrip;
    }

    public String getCurrentTripStartDate() {
        return currentTripStartDate;
    }

    public void setCurrentTripStartDate(String currentTripStartDate) {
        this.currentTripStartDate = currentTripStartDate;
    }

    public String getNextTripCountStatus() {
        return nextTripCountStatus;
    }

    public void setNextTripCountStatus(String nextTripCountStatus) {
        this.nextTripCountStatus = nextTripCountStatus;
    }

    public String getDriverBhatta() {
        return driverBhatta;
    }

    public void setDriverBhatta(String driverBhatta) {
        this.driverBhatta = driverBhatta;
    }

    public String getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(String fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public String getFuelExpense() {
        return fuelExpense;
    }

    public void setFuelExpense(String fuelExpense) {
        this.fuelExpense = fuelExpense;
    }

    public String getParkingCost() {
        return parkingCost;
    }

    public void setParkingCost(String parkingCost) {
        this.parkingCost = parkingCost;
    }

    public String getSystemExpenses() {
        return systemExpenses;
    }

    public void setSystemExpenses(String systemExpenses) {
        this.systemExpenses = systemExpenses;
    }

    public String getTollExpense() {
        return tollExpense;
    }

    public void setTollExpense(String tollExpense) {
        this.tollExpense = tollExpense;
    }

    public String getVehicleMileage() {
        return vehicleMileage;
    }

    public void setVehicleMileage(String vehicleMileage) {
        this.vehicleMileage = vehicleMileage;
    }

    public String getEndingBalance() {
        return endingBalance;
    }

    public void setEndingBalance(String endingBalance) {
        this.endingBalance = endingBalance;
    }

    public String getAdvanceCode() {
        return advanceCode;
    }

    public void setAdvanceCode(String advanceCode) {
        this.advanceCode = advanceCode;
    }

    public String getBhattaDate() {
        return bhattaDate;
    }

    public void setBhattaDate(String bhattaDate) {
        this.bhattaDate = bhattaDate;
    }

    public String getIdleBhattaId() {
        return idleBhattaId;
    }

    public void setIdleBhattaId(String idleBhattaId) {
        this.idleBhattaId = idleBhattaId;
    }

    public String getVehicleAdvanceId() {
        return vehicleAdvanceId;
    }

    public void setVehicleAdvanceId(String vehicleAdvanceId) {
        this.vehicleAdvanceId = vehicleAdvanceId;
    }

    public String getDriverLastBalance() {
        return driverLastBalance;
    }

    public void setDriverLastBalance(String driverLastBalance) {
        this.driverLastBalance = driverLastBalance;
    }

    public String getTripAmount() {
        return tripAmount;
    }

    public void setTripAmount(String tripAmount) {
        this.tripAmount = tripAmount;
    }

    public String getSettleAmount() {
        return settleAmount;
    }

    public void setSettleAmount(String settleAmount) {
        this.settleAmount = settleAmount;
    }

    public String getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(String payAmount) {
        this.payAmount = payAmount;
    }

    public String getSettlementTripsSize() {
        return settlementTripsSize;
    }

    public void setSettlementTripsSize(String settlementTripsSize) {
        this.settlementTripsSize = settlementTripsSize;
    }

    public String getBhattaAmount() {
        return bhattaAmount;
    }

    public void setBhattaAmount(String bhattaAmount) {
        this.bhattaAmount = bhattaAmount;
    }

    public String getIdleDays() {
        return idleDays;
    }

    public void setIdleDays(String idleDays) {
        this.idleDays = idleDays;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getBillCopyName() {
        return billCopyName;
    }

    public void setBillCopyName(String billCopyName) {
        this.billCopyName = billCopyName;
    }

    public String getDocumentRequired() {
        return documentRequired;
    }

    public void setDocumentRequired(String documentRequired) {
        this.documentRequired = documentRequired;
    }

    public String getDocumentRequiredStatus() {
        return documentRequiredStatus;
    }

    public void setDocumentRequiredStatus(String documentRequiredStatus) {
        this.documentRequiredStatus = documentRequiredStatus;
    }

    public String getBillCopyFile() {
        return billCopyFile;
    }

    public void setBillCopyFile(String billCopyFile) {
        this.billCopyFile = billCopyFile;
    }

    public String getUnclearedAmount() {
        return unclearedAmount;
    }

    public void setUnclearedAmount(String unclearedAmount) {
        this.unclearedAmount = unclearedAmount;
    }

    public String getDriverChangeHour() {
        return driverChangeHour;
    }

    public void setDriverChangeHour(String driverChangeHour) {
        this.driverChangeHour = driverChangeHour;
    }

    public String getDriverChangeMinute() {
        return driverChangeMinute;
    }

    public void setDriverChangeMinute(String driverChangeMinute) {
        this.driverChangeMinute = driverChangeMinute;
    }

    public String getDriverInTrip() {
        return driverInTrip;
    }

    public void setDriverInTrip(String driverInTrip) {
        this.driverInTrip = driverInTrip;
    }

    public String getAdvancePaid() {
        return advancePaid;
    }

    public void setAdvancePaid(String advancePaid) {
        this.advancePaid = advancePaid;
    }

    public String getAdvanceReturn() {
        return advanceReturn;
    }

    public void setAdvanceReturn(String advanceReturn) {
        this.advanceReturn = advanceReturn;
    }

    public String getMiscRate() {
        return miscRate;
    }

    public void setMiscRate(String miscRate) {
        this.miscRate = miscRate;
    }

    public String getExpenseHour() {
        return expenseHour;
    }

    public void setExpenseHour(String expenseHour) {
        this.expenseHour = expenseHour;
    }

    public String getExpenseMinute() {
        return expenseMinute;
    }

    public void setExpenseMinute(String expenseMinute) {
        this.expenseMinute = expenseMinute;
    }

    public String getPreCoolingCost() {
        return preCoolingCost;
    }

    public void setPreCoolingCost(String preCoolingCost) {
        this.preCoolingCost = preCoolingCost;
    }

    public String getTollCostPerKm() {
        return tollCostPerKm;
    }

    public void setTollCostPerKm(String tollCostPerKm) {
        this.tollCostPerKm = tollCostPerKm;
    }

    public String getPendingChallan() {
        return pendingChallan;
    }

    public void setPendingChallan(String pendingChallan) {
        this.pendingChallan = pendingChallan;
    }

    public String getPendingChallanAmount() {
        return pendingChallanAmount;
    }

    public void setPendingChallanAmount(String pendingChallanAmount) {
        this.pendingChallanAmount = pendingChallanAmount;
    }

    public String getTotalDocumentAmount() {
        return totalDocumentAmount;
    }

    public void setTotalDocumentAmount(String totalDocumentAmount) {
        this.totalDocumentAmount = totalDocumentAmount;
    }

    public String getUploadedChallan() {
        return uploadedChallan;
    }

    public void setUploadedChallan(String uploadedChallan) {
        this.uploadedChallan = uploadedChallan;
    }

    public String getUploadedChallanAmount() {
        return uploadedChallanAmount;
    }

    public void setUploadedChallanAmount(String uploadedChallanAmount) {
        this.uploadedChallanAmount = uploadedChallanAmount;
    }

    public String getEditFreightAmount() {
        return editFreightAmount;
    }

    public void setEditFreightAmount(String editFreightAmount) {
        this.editFreightAmount = editFreightAmount;
    }

    public String getBilling() {
        return billing;
    }

    public void setBilling(String billing) {
        this.billing = billing;
    }

    public String getOrderReferenceNo() {
        return orderReferenceNo;
    }

    public void setOrderReferenceNo(String orderReferenceNo) {
        this.orderReferenceNo = orderReferenceNo;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getBillingState() {
        return billingState;
    }

    public void setBillingState(String billingState) {
        this.billingState = billingState;
    }

    public String getGstCode() {
        return gstCode;
    }

    public void setGstCode(String gstCode) {
        this.gstCode = gstCode;
    }

    public String getGstName() {
        return gstName;
    }

    public void setGstName(String gstName) {
        this.gstName = gstName;
    }

    public String getGstPercentage() {
        return gstPercentage;
    }

    public void setGstPercentage(String gstPercentage) {
        this.gstPercentage = gstPercentage;
    }

    public String getBillingParty() {
        return billingParty;
    }

    public void setBillingParty(String billingParty) {
        this.billingParty = billingParty;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public String getIgstValue() {
        return igstValue;
    }

    public void setIgstValue(String igstValue) {
        this.igstValue = igstValue;
    }

    public String getCgstValue() {
        return cgstValue;
    }

    public void setCgstValue(String cgstValue) {
        this.cgstValue = cgstValue;
    }

    public String getSgstValue() {
        return sgstValue;
    }

    public void setSgstValue(String sgstValue) {
        this.sgstValue = sgstValue;
    }

    public String getIgstPercentage() {
        return igstPercentage;
    }

    public void setIgstPercentage(String igstPercentage) {
        this.igstPercentage = igstPercentage;
    }

    public String getCgstPercentage() {
        return cgstPercentage;
    }

    public void setCgstPercentage(String cgstPercentage) {
        this.cgstPercentage = cgstPercentage;
    }

    public String getSgstPercentage() {
        return sgstPercentage;
    }

    public void setSgstPercentage(String sgstPercentage) {
        this.sgstPercentage = sgstPercentage;
    }

    public String getOtherExpenseAmount() {
        return otherExpenseAmount;
    }

    public void setOtherExpenseAmount(String otherExpenseAmount) {
        this.otherExpenseAmount = otherExpenseAmount;
    }

    public String getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(String totalTax) {
        this.totalTax = totalTax;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getBillingId() {
        return billingId;
    }

    public void setBillingId(String billingId) {
        this.billingId = billingId;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getCompGSTNo() {
        return compGSTNo;
    }

    public void setCompGSTNo(String compGSTNo) {
        this.compGSTNo = compGSTNo;
    }

    public String getCompAddress() {
        return compAddress;
    }

    public void setCompAddress(String compAddress) {
        this.compAddress = compAddress;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getContractTypeId() {
        return contractTypeId;
    }

    public void setContractTypeId(String contractTypeId) {
        this.contractTypeId = contractTypeId;
    }

    public String getVendorTypeId() {
        return vendorTypeId;
    }

    public void setVendorTypeId(String vendorTypeId) {
        this.vendorTypeId = vendorTypeId;
    }

    public String getOwnerShip() {
        return ownerShip;
    }

    public void setOwnerShip(String ownerShip) {
        this.ownerShip = ownerShip;
    }

    public String getHireVehicleNo() {
        return hireVehicleNo;
    }

    public void setHireVehicleNo(String hireVehicleNo) {
        this.hireVehicleNo = hireVehicleNo;
    }

    public String getInvoiceCustName() {
        return invoiceCustName;
    }

    public void setInvoiceCustName(String invoiceCustName) {
        this.invoiceCustName = invoiceCustName;
    }

    public String getCompPanNo() {
        return compPanNo;
    }

    public void setCompPanNo(String compPanNo) {
        this.compPanNo = compPanNo;
    }

    public String getCustTinno() {
        return custTinno;
    }

    public void setCustTinno(String custTinno) {
        this.custTinno = custTinno;
    }

    public String getCustStateCode() {
        return custStateCode;
    }

    public void setCustStateCode(String custStateCode) {
        this.custStateCode = custStateCode;
    }

    public String getCompanyState() {
        return companyState;
    }

    public void setCompanyState(String companyState) {
        this.companyState = companyState;
    }

    public String getCompanyTinNo() {
        return companyTinNo;
    }

    public void setCompanyTinNo(String companyTinNo) {
        this.companyTinNo = companyTinNo;
    }

    public String getCompanyStateCode() {
        return companyStateCode;
    }

    public void setCompanyStateCode(String companyStateCode) {
        this.companyStateCode = companyStateCode;
    }

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public String getGstLevy() {
        return gstLevy;
    }

    public void setGstLevy(String gstLevy) {
        this.gstLevy = gstLevy;
    }

    public String getGstLevyBill() {
        return gstLevyBill;
    }

    public void setGstLevyBill(String gstLevyBill) {
        this.gstLevyBill = gstLevyBill;
    }

    public String getGstLevyRemb() {
        return gstLevyRemb;
    }

    public void setGstLevyRemb(String gstLevyRemb) {
        this.gstLevyRemb = gstLevyRemb;
    }

    public String getInvoiceTypeBill() {
        return invoiceTypeBill;
    }

    public void setInvoiceTypeBill(String invoiceTypeBill) {
        this.invoiceTypeBill = invoiceTypeBill;
    }

    public String getInvoiceTypeRemb() {
        return invoiceTypeRemb;
    }

    public void setInvoiceTypeRemb(String invoiceTypeRemb) {
        this.invoiceTypeRemb = invoiceTypeRemb;
    }

    public String getPlaceofSupply() {
        return placeofSupply;
    }

    public void setPlaceofSupply(String placeofSupply) {
        this.placeofSupply = placeofSupply;
    }

    public String getPlaceofSupplyBill() {
        return placeofSupplyBill;
    }

    public void setPlaceofSupplyBill(String placeofSupplyBill) {
        this.placeofSupplyBill = placeofSupplyBill;
    }

    public String getPlaceofSupplyRemb() {
        return placeofSupplyRemb;
    }

    public void setPlaceofSupplyRemb(String placeofSupplyRemb) {
        this.placeofSupplyRemb = placeofSupplyRemb;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getInvoiceNoBill() {
        return invoiceNoBill;
    }

    public void setInvoiceNoBill(String invoiceNoBill) {
        this.invoiceNoBill = invoiceNoBill;
    }

    public String getInvoiceNoRemb() {
        return invoiceNoRemb;
    }

    public void setInvoiceNoRemb(String invoiceNoRemb) {
        this.invoiceNoRemb = invoiceNoRemb;
    }

    public String getInvoiceDateRemb() {
        return invoiceDateRemb;
    }

    public void setInvoiceDateRemb(String invoiceDateRemb) {
        this.invoiceDateRemb = invoiceDateRemb;
    }

    public String getInvoiceDateBill() {
        return invoiceDateBill;
    }

    public void setInvoiceDateBill(String invoiceDateBill) {
        this.invoiceDateBill = invoiceDateBill;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String[] getConsignmentOrderIds() {
        return consignmentOrderIds;
    }

    public void setConsignmentOrderIds(String[] consignmentOrderIds) {
        this.consignmentOrderIds = consignmentOrderIds;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotalOtherAmount() {
        return totalOtherAmount;
    }

    public void setTotalOtherAmount(String totalOtherAmount) {
        this.totalOtherAmount = totalOtherAmount;
    }

    public String getHireVehicleDriver() {
        return hireVehicleDriver;
    }

    public void setHireVehicleDriver(String hireVehicleDriver) {
        this.hireVehicleDriver = hireVehicleDriver;
    }

    public String getMailStatus() {
        return mailStatus;
    }

    public void setMailStatus(String mailStatus) {
        this.mailStatus = mailStatus;
    }

    public String getExpenseStatus() {
        return expenseStatus;
    }

    public void setExpenseStatus(String expenseStatus) {
        this.expenseStatus = expenseStatus;
    }

    public String getLicenseNo() {
        return LicenseNo;
    }

    public void setLicenseNo(String LicenseNo) {
        this.LicenseNo = LicenseNo;
    }

    public String getTripPlannedTimeHr() {
        return tripPlannedTimeHr;
    }

    public void setTripPlannedTimeHr(String tripPlannedTimeHr) {
        this.tripPlannedTimeHr = tripPlannedTimeHr;
    }

    public String getTripPlannedTimeMin() {
        return tripPlannedTimeMin;
    }

    public void setTripPlannedTimeMin(String tripPlannedTimeMin) {
        this.tripPlannedTimeMin = tripPlannedTimeMin;
    }

    public String getTripStartReportingDate() {
        return tripStartReportingDate;
    }

    public void setTripStartReportingDate(String tripStartReportingDate) {
        this.tripStartReportingDate = tripStartReportingDate;
    }

    public String getTripPlannedTime() {
        return tripPlannedTime;
    }

    public void setTripPlannedTime(String tripPlannedTime) {
        this.tripPlannedTime = tripPlannedTime;
    }

    public String getFuelLtr() {
        return fuelLtr;
    }

    public void setFuelLtr(String fuelLtr) {
        this.fuelLtr = fuelLtr;
    }

    public String[] getLoadOrderId() {
        return loadOrderId;
    }

    public void setLoadOrderId(String[] loadOrderId) {
        this.loadOrderId = loadOrderId;
    }

    public String[] getPendingPkgs() {
        return pendingPkgs;
    }

    public void setPendingPkgs(String[] pendingPkgs) {
        this.pendingPkgs = pendingPkgs;
    }

    public String[] getPendingWeight() {
        return pendingWeight;
    }

    public void setPendingWeight(String[] pendingWeight) {
        this.pendingWeight = pendingWeight;
    }

    public String[] getLoadedPkgs() {
        return loadedPkgs;
    }

    public void setLoadedPkgs(String[] loadedPkgs) {
        this.loadedPkgs = loadedPkgs;
    }

    public String[] getLoadedWeight() {
        return loadedWeight;
    }

    public void setLoadedWeight(String[] loadedWeight) {
        this.loadedWeight = loadedWeight;
    }

    public String getCbt() {
        return cbt;
    }

    public void setCbt(String cbt) {
        this.cbt = cbt;
    }

    public String getGoodsMovement() {
        return goodsMovement;
    }

    public void setGoodsMovement(String goodsMovement) {
        this.goodsMovement = goodsMovement;
    }

    public String getFuelEstCharge() {
        return fuelEstCharge;
    }

    public void setFuelEstCharge(String fuelEstCharge) {
        this.fuelEstCharge = fuelEstCharge;
    }

    public String getFuelEscalation() {
        return fuelEscalation;
    }

    public void setFuelEscalation(String fuelEscalation) {
        this.fuelEscalation = fuelEscalation;
    }

    public String getCancelInvoiceType() {
        return cancelInvoiceType;
    }

    public void setCancelInvoiceType(String cancelInvoiceType) {
        this.cancelInvoiceType = cancelInvoiceType;
    }

    public String getCancelGstLevy() {
        return cancelGstLevy;
    }

    public void setCancelGstLevy(String cancelGstLevy) {
        this.cancelGstLevy = cancelGstLevy;
    }

    public String getCancelPlaceofSupply() {
        return cancelPlaceofSupply;
    }

    public void setCancelPlaceofSupply(String cancelPlaceofSupply) {
        this.cancelPlaceofSupply = cancelPlaceofSupply;
    }

    public String getCancelInvoiceNo() {
        return cancelInvoiceNo;
    }

    public void setCancelInvoiceNo(String cancelInvoiceNo) {
        this.cancelInvoiceNo = cancelInvoiceNo;
    }

    public String getCancelInvoiceDate() {
        return cancelInvoiceDate;
    }

    public void setCancelInvoiceDate(String cancelInvoiceDate) {
        this.cancelInvoiceDate = cancelInvoiceDate;
    }

    public String getCancellationCharge() {
        return cancellationCharge;
    }

    public void setCancellationCharge(String cancellationCharge) {
        this.cancellationCharge = cancellationCharge;
    }

    public String getTotalCancellationInvoiceAmt() {
        return totalCancellationInvoiceAmt;
    }

    public void setTotalCancellationInvoiceAmt(String totalCancellationInvoiceAmt) {
        this.totalCancellationInvoiceAmt = totalCancellationInvoiceAmt;
    }

    public String getCancelInvoiceFlag() {
        return cancelInvoiceFlag;
    }

    public void setCancelInvoiceFlag(String cancelInvoiceFlag) {
        this.cancelInvoiceFlag = cancelInvoiceFlag;
    }

    public String getInvoiceFlag() {
        return InvoiceFlag;
    }

    public void setInvoiceFlag(String InvoiceFlag) {
        this.InvoiceFlag = InvoiceFlag;
    }

    public String getTspInvoiceType() {
        return tspInvoiceType;
    }

    public void setTspInvoiceType(String tspInvoiceType) {
        this.tspInvoiceType = tspInvoiceType;
    }

    public String getTspGstLevy() {
        return tspGstLevy;
    }

    public void setTspGstLevy(String tspGstLevy) {
        this.tspGstLevy = tspGstLevy;
    }

    public String getTspPlaceofSupply() {
        return tspPlaceofSupply;
    }

    public void setTspPlaceofSupply(String tspPlaceofSupply) {
        this.tspPlaceofSupply = tspPlaceofSupply;
    }

    public String getTspInvoiceNo() {
        return tspInvoiceNo;
    }

    public void setTspInvoiceNo(String tspInvoiceNo) {
        this.tspInvoiceNo = tspInvoiceNo;
    }

    public String getTspInvoiceDate() {
        return tspInvoiceDate;
    }

    public void setTspInvoiceDate(String tspInvoiceDate) {
        this.tspInvoiceDate = tspInvoiceDate;
    }

    public String getRoyaltyInvoiceType() {
        return royaltyInvoiceType;
    }

    public void setRoyaltyInvoiceType(String royaltyInvoiceType) {
        this.royaltyInvoiceType = royaltyInvoiceType;
    }

    public String getRoyaltyGstLevy() {
        return royaltyGstLevy;
    }

    public void setRoyaltyGstLevy(String royaltyGstLevy) {
        this.royaltyGstLevy = royaltyGstLevy;
    }

    public String getRoyaltyPlaceofSupply() {
        return royaltyPlaceofSupply;
    }

    public void setRoyaltyPlaceofSupply(String royaltyPlaceofSupply) {
        this.royaltyPlaceofSupply = royaltyPlaceofSupply;
    }

    public String getRoyaltyInvoiceNo() {
        return royaltyInvoiceNo;
    }

    public void setRoyaltyInvoiceNo(String royaltyInvoiceNo) {
        this.royaltyInvoiceNo = royaltyInvoiceNo;
    }

    public String getRoyaltyInvoiceDate() {
        return royaltyInvoiceDate;
    }

    public void setRoyaltyInvoiceDate(String royaltyInvoiceDate) {
        this.royaltyInvoiceDate = royaltyInvoiceDate;
    }

    public String getTspRoyaltyAmount() {
        return tspRoyaltyAmount;
    }

    public void setTspRoyaltyAmount(String tspRoyaltyAmount) {
        this.tspRoyaltyAmount = tspRoyaltyAmount;
    }

    public String getTotalTspRoyaltyInvoiceAmt() {
        return totalTspRoyaltyInvoiceAmt;
    }

    public void setTotalTspRoyaltyInvoiceAmt(String totalTspRoyaltyInvoiceAmt) {
        this.totalTspRoyaltyInvoiceAmt = totalTspRoyaltyInvoiceAmt;
    }

    public String getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(String orderDetails) {
        this.orderDetails = orderDetails;
    }

    public String getExpenseLocation() {
        return expenseLocation;
    }

    public void setExpenseLocation(String expenseLocation) {
        this.expenseLocation = expenseLocation;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getAwbNo() {
        return awbNo;
    }

    public void setAwbNo(String awbNo) {
        this.awbNo = awbNo;
    }

    public String getFuelEstInvoiceFlag() {
        return fuelEstInvoiceFlag;
    }

    public void setFuelEstInvoiceFlag(String fuelEstInvoiceFlag) {
        this.fuelEstInvoiceFlag = fuelEstInvoiceFlag;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getFuelGstLevy() {
        return fuelGstLevy;
    }

    public void setFuelGstLevy(String fuelGstLevy) {
        this.fuelGstLevy = fuelGstLevy;
    }

    public String getFuelPlaceofSupply() {
        return fuelPlaceofSupply;
    }

    public void setFuelPlaceofSupply(String fuelPlaceofSupply) {
        this.fuelPlaceofSupply = fuelPlaceofSupply;
    }

    public String getFuelInvoiceNo() {
        return fuelInvoiceNo;
    }

    public void setFuelInvoiceNo(String fuelInvoiceNo) {
        this.fuelInvoiceNo = fuelInvoiceNo;
    }

    public String getFuelInvoiceDate() {
        return fuelInvoiceDate;
    }

    public void setFuelInvoiceDate(String fuelInvoiceDate) {
        this.fuelInvoiceDate = fuelInvoiceDate;
    }

    public String getTripPlannedEndDate() {
        return tripPlannedEndDate;
    }

    public void setTripPlannedEndDate(String tripPlannedEndDate) {
        this.tripPlannedEndDate = tripPlannedEndDate;
    }

    public String[] getPendingGrossWeight() {
        return pendingGrossWeight;
    }

    public void setPendingGrossWeight(String[] pendingGrossWeight) {
        this.pendingGrossWeight = pendingGrossWeight;
    }

    public String[] getLoadedGrossWeight() {
        return loadedGrossWeight;
    }

    public void setLoadedGrossWeight(String[] loadedGrossWeight) {
        this.loadedGrossWeight = loadedGrossWeight;
    }

    public String getPendingPackages() {
        return pendingPackages;
    }

    public void setPendingPackages(String pendingPackages) {
        this.pendingPackages = pendingPackages;
    }

    public String getPendingGrossWeights() {
        return pendingGrossWeights;
    }

    public void setPendingGrossWeights(String pendingGrossWeights) {
        this.pendingGrossWeights = pendingGrossWeights;
    }

    public String getPchargableWeight() {
        return pchargableWeight;
    }

    public void setPchargableWeight(String pchargableWeight) {
        this.pchargableWeight = pchargableWeight;
    }

    public String getLoadPkgs() {
        return loadPkgs;
    }

    public void setLoadPkgs(String loadPkgs) {
        this.loadPkgs = loadPkgs;
    }

    public String getLoadChargeWgt() {
        return loadChargeWgt;
    }

    public void setLoadChargeWgt(String loadChargeWgt) {
        this.loadChargeWgt = loadChargeWgt;
    }

    public String getLoadGrossWgt() {
        return loadGrossWgt;
    }

    public void setLoadGrossWgt(String loadGrossWgt) {
        this.loadGrossWgt = loadGrossWgt;
    }

    public String getUserMovementType() {
        return userMovementType;
    }

    public void setUserMovementType(String userMovementType) {
        this.userMovementType = userMovementType;
    }

    public String getLhcId() {
        return lhcId;
    }

    public void setLhcId(String lhcId) {
        this.lhcId = lhcId;
    }

    public String getLhcNo() {
        return lhcNo;
    }

    public void setLhcNo(String lhcNo) {
        this.lhcNo = lhcNo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDlNumber() {
        return dlNumber;
    }

    public void setDlNumber(String dlNumber) {
        this.dlNumber = dlNumber;
    }

    public String geteWayBillNo() {
        return eWayBillNo;
    }

    public void seteWayBillNo(String eWayBillNo) {
        this.eWayBillNo = eWayBillNo;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceValue() {
        return invoiceValue;
    }

    public void setInvoiceValue(String invoiceValue) {
        this.invoiceValue = invoiceValue;
    }

    public String getSealNoType() {
        return sealNoType;
    }

    public void setSealNoType(String sealNoType) {
        this.sealNoType = sealNoType;
    }

    public String getSealNo() {
        return sealNo;
    }

    public void setSealNo(String sealNo) {
        this.sealNo = sealNo;
    }

    public String getDlNo() {
        return dlNo;
    }

    public void setDlNo(String dlNo) {
        this.dlNo = dlNo;
    }

    public String getLoadedpackagesNew() {
        return loadedpackagesNew;
    }

    public void setLoadedpackagesNew(String loadedpackagesNew) {
        this.loadedpackagesNew = loadedpackagesNew;
    }

    public String getUnloadedpackagesNew() {
        return unloadedpackagesNew;
    }

    public void setUnloadedpackagesNew(String unloadedpackagesNew) {
        this.unloadedpackagesNew = unloadedpackagesNew;
    }

    public String getShortageNew() {
        return shortageNew;
    }

    public void setShortageNew(String shortageNew) {
        this.shortageNew = shortageNew;
    }

    public String getDetentionCharge() {
        return detentionCharge;
    }

    public void setDetentionCharge(String detentionCharge) {
        this.detentionCharge = detentionCharge;
    }

    public String getDetentionHours() {
        return detentionHours;
    }

    public void setDetentionHours(String detentionHours) {
        this.detentionHours = detentionHours;
    }

    public String getlHCdate() {
        return lHCdate;
    }

    public void setlHCdate(String lHCdate) {
        this.lHCdate = lHCdate;
    }

    public String getLrdate() {
        return lrdate;
    }

    public void setLrdate(String lrdate) {
        this.lrdate = lrdate;
    }

    public String getGstTaxExpAmountIGST() {
        return gstTaxExpAmountIGST;
    }

    public void setGstTaxExpAmountIGST(String gstTaxExpAmountIGST) {
        this.gstTaxExpAmountIGST = gstTaxExpAmountIGST;
    }

    public String getGstTaxExpAmountCGST() {
        return gstTaxExpAmountCGST;
    }

    public void setGstTaxExpAmountCGST(String gstTaxExpAmountCGST) {
        this.gstTaxExpAmountCGST = gstTaxExpAmountCGST;
    }

    public String getGstTaxExpAmountSGST() {
        return gstTaxExpAmountSGST;
    }

    public void setGstTaxExpAmountSGST(String gstTaxExpAmountSGST) {
        this.gstTaxExpAmountSGST = gstTaxExpAmountSGST;
    }

    public String getEwayBillDate() {
        return ewayBillDate;
    }

    public void setEwayBillDate(String ewayBillDate) {
        this.ewayBillDate = ewayBillDate;
    }

    public String getEwayBillTime() {
        return ewayBillTime;
    }

    public void setEwayBillTime(String ewayBillTime) {
        this.ewayBillTime = ewayBillTime;
    }

    public String getInsuranceSurvey() {
        return insuranceSurvey;
    }

    public void setInsuranceSurvey(String insuranceSurvey) {
        this.insuranceSurvey = insuranceSurvey;
    }

    public String getDocketNo() {
        return docketNo;
    }

    public void setDocketNo(String docketNo) {
        this.docketNo = docketNo;
    }

    public String getDamageNew() {
        return damageNew;
    }

    public void setDamageNew(String damageNew) {
        this.damageNew = damageNew;
    }

    public String getOriginDetention() {
        return originDetention;
    }

    public void setOriginDetention(String originDetention) {
        this.originDetention = originDetention;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAdditionalCost() {
        return additionalCost;
    }

    public void setAdditionalCost(String additionalCost) {
        this.additionalCost = additionalCost;
    }

    public String getContractHireId() {
        return contractHireId;
    }

    public void setContractHireId(String contractHireId) {
        this.contractHireId = contractHireId;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getConsignorId() {
        return consignorId;
    }

    public void setConsignorId(String consignorId) {
        this.consignorId = consignorId;
    }

    public String getConsignorPhoneNo() {
        return consignorPhoneNo;
    }

    public void setConsignorPhoneNo(String consignorPhoneNo) {
        this.consignorPhoneNo = consignorPhoneNo;
    }

    public String getDocumentRecivedDate() {
        return documentRecivedDate;
    }

    public void setDocumentRecivedDate(String documentRecivedDate) {
        this.documentRecivedDate = documentRecivedDate;
    }

    public String getDocumentRecivedHr() {
        return documentRecivedHr;
    }

    public void setDocumentRecivedHr(String documentRecivedHr) {
        this.documentRecivedHr = documentRecivedHr;
    }

    public String getDocumentRecivedMi() {
        return documentRecivedMi;
    }

    public void setDocumentRecivedMi(String documentRecivedMi) {
        this.documentRecivedMi = documentRecivedMi;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String[] getOrigPendingPkgs() {
        return origPendingPkgs;
    }

    public void setOrigPendingPkgs(String[] origPendingPkgs) {
        this.origPendingPkgs = origPendingPkgs;
    }

    public String getInvRefNo() {
        return invRefNo;
    }

    public void setInvRefNo(String invRefNo) {
        this.invRefNo = invRefNo;
    }

    public String getCgst() {
        return cgst;
    }

    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    public String getSgst() {
        return sgst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public String getIgst() {
        return igst;
    }

    public void setIgst(String igst) {
        this.igst = igst;
    }

    public String getCgstAmt() {
        return cgstAmt;
    }

    public void setCgstAmt(String cgstAmt) {
        this.cgstAmt = cgstAmt;
    }

    public String getSgstAmt() {
        return sgstAmt;
    }

    public void setSgstAmt(String sgstAmt) {
        this.sgstAmt = sgstAmt;
    }

    public String getIgstAmt() {
        return igstAmt;
    }

    public void setIgstAmt(String igstAmt) {
        this.igstAmt = igstAmt;
    }

    public String getIgstAmtRound() {
        return igstAmtRound;
    }

    public void setIgstAmtRound(String igstAmtRound) {
        this.igstAmtRound = igstAmtRound;
    }

    public String getCgstAmtRound() {
        return cgstAmtRound;
    }

    public void setCgstAmtRound(String cgstAmtRound) {
        this.cgstAmtRound = cgstAmtRound;
    }

    public String getSgstAmtRound() {
        return sgstAmtRound;
    }

    public void setSgstAmtRound(String sgstAmtRound) {
        this.sgstAmtRound = sgstAmtRound;
    }

    public String getGrandTotalRound() {
        return grandTotalRound;
    }

    public void setGrandTotalRound(String grandTotalRound) {
        this.grandTotalRound = grandTotalRound;
    }

    public String getFreightAmountRound() {
        return freightAmountRound;
    }

    public void setFreightAmountRound(String freightAmountRound) {
        this.freightAmountRound = freightAmountRound;
    }

    public String getTripMonth() {
        return tripMonth;
    }

    public void setTripMonth(String tripMonth) {
        this.tripMonth = tripMonth;
    }

    public String getTripYear() {
        return tripYear;
    }

    public void setTripYear(String tripYear) {
        this.tripYear = tripYear;
    }

    public String getVehicleCategory() {
        return vehicleCategory;
    }

    public void setVehicleCategory(String vehicleCategory) {
        this.vehicleCategory = vehicleCategory;
    }

    public String getMovementTypeId() {
        return movementTypeId;
    }

    public void setMovementTypeId(String movementTypeId) {
        this.movementTypeId = movementTypeId;
    }

    public String getTatTime() {
        return tatTime;
    }

    public void setTatTime(String tatTime) {
        this.tatTime = tatTime;
    }

    public String getEscortName() {
        return escortName;
    }

    public void setEscortName(String escortName) {
        this.escortName = escortName;
    }

    public String getEscortMobile() {
        return escortMobile;
    }

    public void setEscortMobile(String escortMobile) {
        this.escortMobile = escortMobile;
    }

    public String getMaterialDesc() {
        return materialDesc;
    }

    public void setMaterialDesc(String materialDesc) {
        this.materialDesc = materialDesc;
    }

    public String getInsuranceDetail() {
        return insuranceDetail;
    }

    public void setInsuranceDetail(String insuranceDetail) {
        this.insuranceDetail = insuranceDetail;
    }

    public String getAppDate() {
        return appDate;
    }

    public void setAppDate(String appDate) {
        this.appDate = appDate;
    }

    public String getAppHour() {
        return appHour;
    }

    public void setAppHour(String appHour) {
        this.appHour = appHour;
    }

    public String getAppMin() {
        return appMin;
    }

    public void setAppMin(String appMin) {
        this.appMin = appMin;
    }

    public String getMovementtypeId() {
        return movementtypeId;
    }

    public void setMovementtypeId(String movementtypeId) {
        this.movementtypeId = movementtypeId;
    }

    public String getMovementtype() {
        return movementtype;
    }

    public void setMovementtype(String movementtype) {
        this.movementtype = movementtype;
    }

    public String getAppTime() {
        return appTime;
    }

    public void setAppTime(String appTime) {
        this.appTime = appTime;
    }

    public String getAppointTime() {
        return appointTime;
    }

    public void setAppointTime(String appointTime) {
        this.appointTime = appointTime;
    }

    public String getAppointTimemin() {
        return appointTimemin;
    }

    public void setAppointTimemin(String appointTimemin) {
        this.appointTimemin = appointTimemin;
    }

    public String getBillOverTime() {
        return billOverTime;
    }

    public void setBillOverTime(String billOverTime) {
        this.billOverTime = billOverTime;
    }

    public String getBillOverTimemin() {
        return billOverTimemin;
    }

    public void setBillOverTimemin(String billOverTimemin) {
        this.billOverTimemin = billOverTimemin;
    }

    public String getReceivedTime() {
        return receivedTime;
    }

    public void setReceivedTime(String receivedTime) {
        this.receivedTime = receivedTime;
    }

    public String getReceivedTimemin() {
        return receivedTimemin;
    }

    public void setReceivedTimemin(String receivedTimemin) {
        this.receivedTimemin = receivedTimemin;
    }

    public String getEwayBillNo() {
        return ewayBillNo;
    }

    public void setEwayBillNo(String ewayBillNo) {
        this.ewayBillNo = ewayBillNo;
    }

    public String getAppointDate() {
        return appointDate;
    }

    public void setAppointDate(String appointDate) {
        this.appointDate = appointDate;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    
     

}
