/*-------------------------------------------------------------------------
 * __NAME__.java
 * __DATE__
 *
 * Copyright (c) Entitle.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Entitle ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Entitle.
 -------------------------------------------------------------------------*/
package ets.domain.trip.business;

import com.ibatis.sqlmap.client.SqlMapClient;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.util.SendMail;
import ets.domain.trip.business.TripTO;
import ets.domain.trip.data.TripDAO;
import ets.domain.util.ThrottleConstants;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import javax.mail.Message;
import javax.mail.MessagingException;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver Date Author Change
 * ----------------------------------------------------------------------------
 * 1.0 __DATE__ Your_Name ,Entitle Created
 *
 *****************************************************************************
 */
public class TripBP {

    /**
     * Creates a new instance of __NAME__
     */
    public TripBP() {
    }
    TripDAO tripDAO;

    public TripDAO getTripDAO() {
        return tripDAO;
    }

    public void setTripDAO(TripDAO tripDAO) {
        this.tripDAO = tripDAO;
    }

    public ArrayList getConsignmentList(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList consignmentList = new ArrayList();
        consignmentList = tripDAO.getConsignmentList(tripTO);
        return consignmentList;
    }

    public ArrayList getExpiryDateDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList expiryDetails = new ArrayList();
        expiryDetails = tripDAO.getExpiryDateDetails(tripTO);
        return expiryDetails;
    }

    public int updateTripOtherExpense(String tripSheetId, String vehicleId, String expenseName, String employeeName, String expenseType, String expenseLocation, String expenseDate, String expenseHour, String expenseMinute, String taxPercentage, String expenseRemarks, String expenses, String netExpense, String billMode, String marginValue, String expenseId, String activeValue, String customerId, String orderId, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateTripOtherExpense(tripSheetId, vehicleId, expenseName, employeeName, expenseType, expenseLocation, expenseDate, expenseHour, expenseMinute, taxPercentage,
                expenseRemarks, expenses, netExpense, billMode, marginValue, expenseId, activeValue, customerId, orderId, userId);
        return status;
    }

    public ArrayList getTripDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getTripDetails(tripTO);
        return tripDetails;
    }

    public ArrayList getTripCustomerList(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getTripCustomerList(tripTO);
        return tripDetails;
    }

    public ArrayList getTripsToBeBilledDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getTripsToBeBilledDetails(tripTO);
        return tripDetails;
    }

    public ArrayList getTripsOtherExpenseDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getTripsOtherExpenseDetails(tripTO);
        return tripDetails;
    }

    public ArrayList getTripsOtherExpenseDetails1(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getTripsOtherExpenseDetails1(tripTO);
        return tripDetails;
    }

    public ArrayList getTripsOtherExpense(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getTripsOtherExpense(tripTO);
        return tripDetails;
    }

    public ArrayList getInvoiceHeader(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList result = new ArrayList();
        result = tripDAO.getInvoiceHeader(tripTO);
        return result;
    }

    public ArrayList getInvoiceDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList result = new ArrayList();
        result = tripDAO.getInvoiceDetails(tripTO);
        return result;
    }

    public ArrayList getInvoiceDetailExpenses(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList result = new ArrayList();
        result = tripDAO.getInvoiceDetailExpenses(tripTO);
        return result;
    }

    public ArrayList getTripPointDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripPointDetails = new ArrayList();
        tripPointDetails = tripDAO.getTripPointDetails(tripTO);
        return tripPointDetails;
    }

    public ArrayList getOrderPointDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripPointDetails = new ArrayList();
        tripPointDetails = tripDAO.getOrderPointDetails(tripTO);
        return tripPointDetails;
    }

    public ArrayList getLoadDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList loadDetails = new ArrayList();
        loadDetails = tripDAO.getLoadDetails(tripTO);
        return loadDetails;
    }

    public String getConsignmentOrderRevenue(String orderId, String billingId, String vehicleTypeId, String reeferRequired, String customerType) throws FPBusinessException, FPRuntimeException {
        String revenue = "";
        revenue = tripDAO.getConsignmentOrderRevenue(orderId, billingId, vehicleTypeId, reeferRequired, customerType);
        return revenue;
    }

    public String getVehicleMileageAndTollRate(String vehicleTypeId) throws FPBusinessException, FPRuntimeException {
        String response = "";
        response = tripDAO.getVehicleMileageAndTollRate(vehicleTypeId);
        return response;
    }

    public String getEstimatedTripEndDateTime(String tripId) throws FPBusinessException, FPRuntimeException {
        String response = "";
        response = tripDAO.getEstimatedTripEndDateTime(tripId);
        return response;
    }

    public String checkPreStartRoute(String preStartLocationId, String originId, String vehicleTypeId) throws FPBusinessException, FPRuntimeException {
        String info = "";
        info = tripDAO.checkPreStartRoute(preStartLocationId, originId, vehicleTypeId);
        return info;
    }

//    public ArrayList getVehicleRegNos(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
//        ArrayList vehicleNos = new ArrayList();
//        vehicleNos = tripDAO.getVehicleRegNos(tripTO);
//        return vehicleNos;
//    }
    public ArrayList getVehicleRegNosForEmptyTrip(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleNos = new ArrayList();
        vehicleNos = tripDAO.getVehicleRegNosForEmptyTrip(tripTO);
        return vehicleNos;
    }

    public ArrayList getVehicleRegNosForUpload(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleNos = new ArrayList();
        vehicleNos = tripDAO.getVehicleRegNosForUpload(tripTO);
        return vehicleNos;
    }

    public ArrayList getDrivers(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList drivers = new ArrayList();
        drivers = tripDAO.getDrivers(tripTO);
        return drivers;
    }

    public String getConsignmentOrderExpense(String orderId, String billingId, String vehicleTypeId, String reeferRequired, String customerTypeId) throws FPBusinessException, FPRuntimeException {
        String expense = "";
        expense = tripDAO.getConsignmentOrderExpense(orderId, billingId, vehicleTypeId, reeferRequired, customerTypeId);
        return expense;
    }

    public int saveAction(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = tripDAO.saveAction(tripTO);
        return status;
    }

    public int saveClosureApprovalRequest(String tripId, String requestType, String remarks, int userId, String rcmExp, String nettExp) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = tripDAO.saveClosureApprovalRequest(tripId, requestType, remarks, userId, rcmExp, nettExp);
        return status;
    }

    public int saveTripUpdate(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = tripDAO.saveTripUpdate(tripTO);

        //if action is freeeze update prestart plan info
        if ("1".equals(tripTO.getActionName())
                && !"1".equals(tripTO.getPreStartLocationStatus())) { //freeze status
            status = tripDAO.updatePreStartDetails(tripTO);
        } else if ("2".equals(tripTO.getActionName())) {//unfreeze
            //remove vehicle and driver mapping
            status = tripDAO.clearVehicleAndDriverMapping(tripTO);
        }

        //update vehicle info
        if (tripTO.getVehicleId() != null && !"".equals(tripTO.getVehicleId())) {
            status = tripDAO.saveTripVehicle(tripTO);
        }
        //update driver info
        if (tripTO.getPrimaryDriverId() != null && !"0".equals(tripTO.getPrimaryDriverId()) && !"".equals(tripTO.getPrimaryDriverId())) {
            status = tripDAO.saveTripDriver(tripTO);
        }

        //if pre start route is given add one point location
        status = tripDAO.saveTripRoutePlanWhenTripUpdate(tripTO);

        return status;
    }

    public int saveTripSheetForVehicleChange(TripTO tripTO, String[] saveFile) throws FPBusinessException, FPRuntimeException {
        int status = 0;

        //reset vehicle and driver mapping
        status = tripDAO.clearVehicleAndDriverMapping(tripTO);
        //update vehicle info
        status = tripDAO.saveTripVehicleForVehicleChange(tripTO, saveFile);

//        if (tripTO.getVehicleId() != null && !"".equals(tripTO.getVehicleId())) {
//        }
//        //update driver info
//          //  status = tripDAO.saveTripDriver(tripTO);
//        if (tripTO.getPrimaryDriverId() != null && !"0".equals(tripTO.getPrimaryDriverId()) && !"".equals(tripTO.getPrimaryDriverId())) {
//        }
        return status;
    }

    public String getInvoiceCodeSequence() {
        String codeSequence = "";
        codeSequence = tripDAO.getInvoiceCodeSequence();
        return codeSequence;
    }

    public String getInvoiceCodeSequenceBill() {
        String codeSequence = "";
        codeSequence = tripDAO.getInvoiceCodeSequenceBill();
        return codeSequence;
    }

    public String getInvoiceCodeSequenceRemb() {
        String codeSequence = "";
        codeSequence = tripDAO.getInvoiceCodeSequenceRemb();
        return codeSequence;
    }

    public String getCnoteCodeSequence() {
        String codeSequence = "";
        codeSequence = tripDAO.getCnoteCodeSequence();
        return codeSequence;
    }

    public int saveTripSheet(TripTO tripTO) throws FPBusinessException, FPRuntimeException, IOException {
        int status = 0;
        String tripStatus = "";
        //generate tripcode

        String accountYear = ThrottleConstants.accountYear;
        String tripCode = "TS/" + accountYear + "/";

        String tripCodeSequence = tripDAO.getTripCodeSequence();
        System.out.println("tripCodetripCode" + tripCode);
        tripCode = tripCode + tripCodeSequence;
        System.out.println("kjhdchkw" + tripCode);
//        int tripCodeNo =Integer.parseInt(tripCode) + Integer.parseInt(tripCodeSequence);
        String tripCodeNo = tripCode + tripCodeSequence;
        tripTO.setTripCode(tripCode);
        System.out.println("tripCodeNo-----------" + tripCodeNo);
        System.out.println("tripCode-----------" + tripCode);

      
        double transitDay = 0;
        
      

        double advnaceToBePaidPerDay = 0;
        tripTO.setAdvnaceToBePaidPerDay(advnaceToBePaidPerDay + "");

        //set trip status
        if ("1".equals(tripTO.getActionName())) { //freeze status
            tripTO.setStatusId("8");
            tripStatus = "Freezed";

        } else {
            if (tripTO.getVehicleId() != null && !"".equals(tripTO.getVehicleId())) {
                tripTO.setStatusId("7");
                tripStatus = "Trip Vehicle Alloted";
            } else {
                tripTO.setStatusId("6");
                tripStatus = "Trip Created";

            }
        }
        //create trip master
        int tripId = tripDAO.saveTripSheet(tripTO);

        if (tripId > 0) {
            tripTO.setTripId(tripId + "");

            //if action is freeeze update prestart plan info
            if ("1".equals(tripTO.getActionName())
                    && !"1".equals(tripTO.getPreStartLocationStatus())) { //freeze status
                status = tripDAO.updatePreStartDetails(tripTO);
            }
            //create trip status details
            status = tripDAO.saveTripStatusDetails(tripTO);
            //create trip consignment order
            status = tripDAO.saveTripConsignments(tripTO);
            //create trip vehicles
            status = tripDAO.saveTripVehicle(tripTO);
            if (tripTO.getVehicleId() != null && !"".equals(tripTO.getVehicleId())) {
            }
            //create trip driver
            status = tripDAO.saveTripDriver(tripTO);
            if (tripTO.getPrimaryDriverId() != null && !"0".equals(tripTO.getPrimaryDriverId()) && !"".equals(tripTO.getPrimaryDriverId())) {
            }
            //crate trip points
            status = tripDAO.saveTripRoutePlan(tripTO);
            System.out.println("statusBP------------" + status);
            //////////////////Arun Start//////////////////////////
//            if ("8".equals(tripTO.getStatusId())) { // freezed
//                String to = "";
//                String cc = "";
//                String smtp = "";
//                int emailPort = 0;
//                String frommailid = "";
//                String password = "";
//                String activitycode = "EMTRP1";
//
//                ArrayList emaildetails = new ArrayList();
//                emaildetails = getEmailDetails(activitycode);
//
//                String emailString = getTripEmails(tripTO.getTripId(), tripTO.getStatusId());
//                System.out.println("emailString:" + emailString);
//                String[] emailTemp = emailString.split("~");
//                int emailTempLenth = emailTemp.length;
//                System.out.println("emailTempLenth: " + emailTempLenth);
//
//                if (emailTempLenth > 0) {
//                    to = emailTemp[0];
//                }
//                if (emailTempLenth > 1) {
//                    cc = emailTemp[1];
//                }
//
//                Iterator itr1 = emaildetails.iterator();
//                TripTO tripTO1 = null;
//                if (itr1.hasNext()) {
//                    tripTO1 = new TripTO();
//                    tripTO1 = (TripTO) itr1.next();
//                    smtp = tripTO1.getSmtp();
//                    emailPort = Integer.parseInt(tripTO1.getPort());
//                    frommailid = tripTO1.getEmailId();
//                    password = tripTO1.getPassword();
//                }
//                String vehicle = tripTO.getVehicleNo();
//
//                String emailFormat = "<html>"
//                        + "<body>"
//                        + "<p>Hi, <br><br>Trip Freezed For " + tripTO.getTripCode() + "</p>"
//                        + "<br> Customer:" + tripTO.getCustomerName()
//                        + "<br> C Note No:" + tripTO.getcNotes()
//                        + "<br> Route :" + tripTO.getRouteInfo()
//                        + "<br> Vehicle No :" + vehicle
//                        + "<br><br> Team BrattleFoods"
//                        + "</body></html>";
//
//                String subject = "Trip Freezed for Customer " + tripTO.getCustomerName() + " Vehicle " + vehicle + " Route " + tripTO.getRouteInfo();
//                int tripMail = 2;
//                int consignmentId = 0;
//                int routeId = 0;
//                String mailType = "WOA";
//                String content = emailFormat;
//                if (!"".equals(to)) {
//                    int mailSendingId = 0;
//                    tripTO.setMailTypeId("2");
//                    tripTO.setMailSubjectTo(subject);
//                    tripTO.setMailSubjectCc(subject);
//                    tripTO.setMailSubjectBcc("");
//                    tripTO.setMailContentTo(content);
//                    tripTO.setMailContentCc(content);
//                    tripTO.setMailContentBcc("");
//                    tripTO.setMailIdTo(to);
//                    tripTO.setMailIdCc(cc);
//                    tripTO.setMailIdBcc("");
////                    mailSendingId = tripDAO.insertMailDetails(tripTO, tripTO.getUserId());
//                    new SendMail(smtp, emailPort, frommailid, password, subject, content, to, cc, tripTO.getUserId()).start();
//                }
//            }

            /////////////////////////////////////////////Email part///////////////////////
        }
        //send mail on trip creation to customer and account manager for contract customers
        return tripId;
    }

    public ArrayList getTripSheetDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getTripSheetDetails(tripTO);
        return tripDetails;
    }

    public ArrayList getStatusDetails() throws FPRuntimeException, FPBusinessException {
        ArrayList response = new ArrayList();
        response = tripDAO.getStatusDetails();
        return response;
    }

    //       Throttle Starts Here
    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getTripDetails(String tripSheetId) {
        String tripDetails = "";
        tripDetails = tripDAO.getTripDetails(tripSheetId);
        return tripDetails;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getTripInformation(String tripSheetId) {
        String tripDetails = "";
        tripDetails = tripDAO.getTripInformation(tripSheetId);
        return tripDetails;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getDriverCount(String tripSheetId, TripTO tripTO) {
        String driverCount = "";
        driverCount = tripDAO.getDriverCount(tripSheetId, tripTO);
        return driverCount;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getTripAdvance(String tripSheetId, TripTO tripTO) {
        String tripAdvance = "";
        tripAdvance = tripDAO.getTripAdvance(tripSheetId, tripTO);
        return tripAdvance;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getTripExpense(String tripSheetId, TripTO tripTO) {
        String tripExpense = "";
        tripExpense = tripDAO.getTripExpense(tripSheetId, tripTO);
        return tripExpense;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int insertSettlement(String tripSheetId, String estimatedExpense, String bpclTransactionAmount, String rcmExpense, String vehicleDieselConsume, String reeferDieselConsume, String tripStartingBalance, String gpsKm, String gpsHm, String totalRunKm, String totalDays, String totalRefeerHours, String milleage, String tripDieselConsume, String tripRcmAllocation, String totalTripAdvance, String totalTripMisc, String totalTripBhatta, String totalTripExpense, String tripBalance, String tripEndBalance, String settlementRemarks, String lrNumber, String paymentMode, String fuelPrice, String tripExtraExpense, int userId, TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int insertSettlement = 0;
        insertSettlement = tripDAO.insertSettlement(tripSheetId, estimatedExpense, bpclTransactionAmount, rcmExpense, vehicleDieselConsume, reeferDieselConsume, tripStartingBalance, gpsKm, gpsHm, totalRunKm, totalDays, totalRefeerHours, milleage, tripDieselConsume, tripRcmAllocation, totalTripAdvance, totalTripMisc, totalTripBhatta, totalTripExpense, tripBalance, tripEndBalance, settlementRemarks, lrNumber, paymentMode, fuelPrice, tripExtraExpense, userId, tripTO);
        return insertSettlement;
    }

    public int insertTripDriverSettlementDetails(String tripSheetId, String estimatedExpense, String bpclTransactionAmount, String rcmExpense, String vehicleDieselConsume, String reeferDieselConsume, String tripStartingBalance, String gpsKm, String gpsHm, String totalRunKm, String totalDays, String totalRefeerHours, String milleage, String tripDieselConsume, String tripRcmAllocation, String totalTripAdvance, String totalTripMisc, String totalTripBhatta, String totalTripExpense, String tripBalance, String tripEndBalance, String settlementRemarks, String paymentMode, String fuelPrice, String tripExtraExpense, int userId, TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int insertSettlement = 0;
        insertSettlement = tripDAO.insertTripDriverSettlementDetails(tripSheetId, estimatedExpense, bpclTransactionAmount, rcmExpense, vehicleDieselConsume, reeferDieselConsume, tripStartingBalance, gpsKm, gpsHm, totalRunKm, totalDays, totalRefeerHours, milleage, tripDieselConsume, tripRcmAllocation, totalTripAdvance, totalTripMisc, totalTripBhatta, totalTripExpense, tripBalance, tripEndBalance, settlementRemarks, paymentMode, fuelPrice, tripExtraExpense, userId, tripTO);
        return insertSettlement;
    }
    // Throttle Starts 12-12-2013

    public ArrayList getEndTripSheetDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getEndTripSheetDetails(tripTO);
        return tripDetails;
    }

    public ArrayList getClosureEndTripSheetDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getClosureEndTripSheetDetails(tripTO);
        return tripDetails;
    }

    public ArrayList getClosureApprovalProcessHistory(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList result = new ArrayList();
        result = tripDAO.getClosureApprovalProcessHistory(tripTO);
        return result;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getGpsKm(String tripSheetId) {
        String tripDetails = "";
        tripDetails = tripDAO.getGpsKm(tripSheetId);
        return tripDetails;
    }

    public String getClosureApprovalStatus(String tripSheetId, String requestType) {
        String result = "";
        result = tripDAO.getClosureApprovalStatus(tripSheetId, requestType);
        return result;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getMiscValue(String vehicleTypeId) {
        String getMiscValue = "";
        getMiscValue = tripDAO.getMiscValue(vehicleTypeId);
        return getMiscValue;
    }

    public String getBookedExpense(String tripSheetId, TripTO tripTO) {
        String result = "";
        result = tripDAO.getBookedExpense(tripSheetId, tripTO);
        return result;
    }

    public String getTripEmails(String tripId, String statusId) {
        String result = "";
        result = tripDAO.getTripEmails(tripId, statusId);
        String[] temp = result.split("~");
        String emails = "";
        String stakeHolders = "";
        if (temp.length > 0) {
            emails = temp[0];
        }
        if (temp.length > 1) {
            stakeHolders = temp[1];
        }
        String to = "";
        String cc = "";
        int toCntr = 0;
        int ccCntr = 0;
        String[] emailTemp = emails.split(",");
        System.out.println("emailTemp.length:" + emailTemp.length);
        System.out.println("stakeHolders:" + stakeHolders);
        System.out.println("1:" + stakeHolders.indexOf("1"));
        for (int i = 0; i < emailTemp.length; i++) {
            if (i == 0) {
                if (stakeHolders.indexOf("1") >= 0) {//does customer email exists
                    System.out.println("emailTemp[i] step 1:" + emailTemp[i]);
                    if (!"".equals(emailTemp[i]) && !"-".equals(emailTemp[i]) && (emailTemp[i].indexOf("@") >= 0)) {
                        if (toCntr == 0) {
                            to = emailTemp[i];
                            toCntr++;
                        } else {
                            to = to + "," + emailTemp[i];
                        }

                    }
                }
            }
            if (i == 1) {
                if (stakeHolders.indexOf("2") >= 0) {//does act mgr email exists
                    System.out.println("emailTemp[i] step 2:" + emailTemp[i]);
                    if (!"".equals(emailTemp[i]) && !"-".equals(emailTemp[i]) && (emailTemp[i].indexOf("@") >= 0)) {
                        if (toCntr == 0) {
                            to = emailTemp[i];
                            toCntr++;
                        } else {
                            to = to + "," + emailTemp[i];
                        }
                    }
                }
            }
            if (i == 2) {
                if (stakeHolders.indexOf("3") >= 0) {//does act mgr email exists
                    System.out.println("emailTemp[i] step 3:" + emailTemp[i]);
                    if (!"".equals(emailTemp[i]) && !"-".equals(emailTemp[i]) && (emailTemp[i].indexOf("@") >= 0)) {
                        if (toCntr == 0) {
                            to = emailTemp[i];
                            toCntr++;
                        } else {
                            to = to + "," + emailTemp[i];
                        }

                    }
                }
            }
            if (i > 2) {
                if (!"".equals(emailTemp[i]) && !"-".equals(emailTemp[i]) && (emailTemp[i].indexOf("@") >= 0)) {
                    System.out.println("emailTemp[i] step 4:" + emailTemp[i]);
                    if (ccCntr == 0) {
                        cc = emailTemp[i];
                        ccCntr++;
                    } else {
                        cc = cc + "," + emailTemp[i];
                    }

                }
            }
        }
        result = to + "~" + cc;

        return result;
    }
//NIthya start 7 dec

    public int savePreTripSheetDetails(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.savePreTripSheetDetails(tripTO, userId);
        return status;
    }

    public int setAdvanceAdvice(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.setAdvanceAdvice(tripTO);
        return status;
    }

    public int saveBillHeader(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveBillHeader(tripTO);
        return status;
    }

    public int saveBillHeaderBill(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveBillHeaderBill(tripTO);
        return status;
    }

    public int saveBillHeaderRemb(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveBillHeaderRemb(tripTO);
        return status;
    }

    public int saveBillDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveBillDetails(tripTO);
        return status;
    }

    public int saveBillDetailExpense(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveBillDetailExpense(tripTO);
        return status;
    }

    public int saveBillDetailsBill(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveBillDetailsBill(tripTO);
        return status;
    }

    public int saveBillDetailExpenseBill(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveBillDetailExpenseBill(tripTO);
        return status;
    }

    public int saveBillDetailsRemb(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveBillDetailsRemb(tripTO);
        return status;
    }

    public int saveBillDetailExpenseRemb(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveBillDetailExpenseRemb(tripTO);
        return status;
    }

    public String updateStartTripSheet(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        String status = "";
        SqlMapClient session = tripDAO.getSqlMapClient();
        try {
            session.startTransaction();
            status = tripDAO.updateStartTripSheet(tripTO, userId, session);
            if (!status.equals("")) {
                session.commitTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("am here 7 : " + status);
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }

    public int createGPSLogForTripStart(String tripId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.createGPSLogForTripStart(tripId);
        return status;
    }

    public int createGPSLogForTripEnd(String tripId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.createGPSLogForTripEnd(tripId);
        return status;
    }

    public ArrayList getTripPODDetails(String tripSheetId) throws FPRuntimeException, FPBusinessException {
        ArrayList podDetails = new ArrayList();
        podDetails = tripDAO.getTripPODDetails(tripSheetId);
        return podDetails;
    }

    public int saveTripPodDetails(String actualFilePath, String cityId1, String tripSheetId1, String podRemarks1, String lrNumber, String fileSaved, TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveTripPodDetails(actualFilePath, tripSheetId1, cityId1, podRemarks1, lrNumber, fileSaved, tripTO, userId);
        return status;
    }

    public ArrayList getVehicleList() throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = null;
        vehicleList = tripDAO.getVehicleList();
        return vehicleList;
    }

    public ArrayList getDriverSettlementDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList settlementDetails = new ArrayList();
        settlementDetails = tripDAO.getDriverSettlementDetails(tripTO);
        return settlementDetails;
    }

    public ArrayList getStartTripDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getStartTripDetails(tripTO);
        return tripDetails;
    }

    public ArrayList getVehicleChangeTripDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getVehicleChangeTripDetails(tripTO);
        return tripDetails;
    }

    public int updateEndTripSheet(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateEndTripSheet(tripTO, userId);
        return status;
    }

    public int updateEndTripSheetDuringClosure(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateEndTripSheetDuringClosure(tripTO, userId);
        return status;
    }

    public int overrideEndTripSheetDuringClosure(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.overrideEndTripSheetDuringClosure(tripTO, userId);
        return status;
    }

    public int updateStartTripSheetDuringClosure(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateStartTripSheetDuringClosure(tripTO, userId);
        return status;
    }

    public int updateStatus(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateStatus(tripTO, userId);
        return status;
    }

    public int updateStatusForInvoice(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateStatusForInvoice(tripTO, userId);
        return status;
    }

    public ArrayList getFuelDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList fuelDetails = new ArrayList();
        fuelDetails = tripDAO.getFuelDetails(tripTO);
        return fuelDetails;
    }

    public ArrayList getOtherExpenseDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList otherExpenses = new ArrayList();
        otherExpenses = tripDAO.getOtherExpenseDetails(tripTO);
        return otherExpenses;
    }

    public int insertTripFuelExpense(String tripSheetId, String location, String fillDate, String fuelLitres, String fuelAmount, String fuelRemarks, String fuelPricePerLitre, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.insertTripFuelExpense(tripSheetId, location, fillDate, fuelLitres, fuelAmount,
                fuelRemarks, fuelPricePerLitre, userId);
        return status;
    }

    public ArrayList getExpenseDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList expenseDetails = new ArrayList();
        expenseDetails = tripDAO.getExpenseDetails(tripTO);
        return expenseDetails;
    }

    public ArrayList getDriverName(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList driverNameDetails = new ArrayList();
        driverNameDetails = tripDAO.getDriverName(tripTO);
        return driverNameDetails;
    }

    public int insertTripOtherExpense(String tripSheetId, String vehicleId, String expenseName, String employeeName, String expenseType, String expenseLocation, String expenseDate, String expenseHour, String expenseMinute, String taxPercentage, String expenseRemarks, String expenses, String netExpense, String billMode, String marginValue, String activeValue, String customerId, String orderId, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.insertTripOtherExpense(tripSheetId, vehicleId, expenseName, employeeName, expenseType, expenseLocation, expenseDate, expenseHour, expenseMinute, taxPercentage,
                expenseRemarks, expenses, netExpense, billMode, marginValue, activeValue, customerId, orderId, userId);
        return status;
    }

    public int saveTripExpensePodDetailsOLD(String actualFilePath, String tripExpenseId1, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveTripExpensePodDetailsOLD(actualFilePath, tripExpenseId1, userId);
        return status;
    }

    public int saveTripExpensePodDetails(String fileSave, String actualFilePath, String tripExpenseId1, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveTripExpensePodDetails(fileSave, actualFilePath, tripExpenseId1, userId);
        return status;
    }
//NIthya END 7 dec
//Throttle Start Here 07-12-2013

    /**
     * This method used to get Trip Advance Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getTripAdvanceDetails(String tripSheetId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripAdvanceDetails = new ArrayList();
        tripAdvanceDetails = tripDAO.getTripAdvanceDetails(tripSheetId);
        return tripAdvanceDetails;
    }

    /**
     * This method used to get Trip Advance Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getTripFuelDetails(String tripSheetId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripFuelDetails = new ArrayList();
        tripFuelDetails = tripDAO.getTripFuelDetails(tripSheetId);
        return tripFuelDetails;
    }

    /**
     * This method used to get Trip Expense Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getTripExpenseDetails(String tripSheetId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripExpenseDetails = new ArrayList();
        tripExpenseDetails = tripDAO.getTripExpenseDetails(tripSheetId);
        return tripExpenseDetails;
    }

    public ArrayList getPreStartTripDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList preStartDetails = new ArrayList();
        preStartDetails = tripDAO.getPreStartTripDetails(tripTO);
        return preStartDetails;
    }

    public ArrayList getStartedTripDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList startDetails = new ArrayList();
        startDetails = tripDAO.getStartedTripDetails(tripTO);
        return startDetails;
    }

    public ArrayList getEndTripDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList endDetails = new ArrayList();
        endDetails = tripDAO.getEndTripDetails(tripTO);
        return endDetails;
    }

    public ArrayList getGPSDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList gpsDetails = new ArrayList();
        gpsDetails = tripDAO.getGPSDetails(tripTO);
        return gpsDetails;
    }

    public ArrayList getTotalExpenseDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList totalExpense = new ArrayList();
        ArrayList totalExpenseNew = new ArrayList();
        TripTO tripTo = new TripTO();
        totalExpense = tripDAO.getTotalExpenseDetails(tripTO);
        String driverIncentive = "0";
        String tollAmount = "0";
        String driverBatta = "0";
        String fuelPrice = "0";
        String fuelType = "0";
        String vehicleTypeId = "0";
        Iterator it = totalExpense.iterator();
        while (it.hasNext()) {
            tripTo = new TripTO();
            tripTo = (TripTO) it.next();
//            if (!"2".equals(tripTO.getTripType())) {//primary
            vehicleTypeId = tripTo.getVehicleTypeId();
            driverIncentive = tripDAO.getDriverIncentiveAmount(vehicleTypeId);
            tollAmount = tripDAO.getTollAmount(vehicleTypeId);
            driverBatta = tripDAO.getDriverBataAmount(vehicleTypeId);
            fuelPrice = tripDAO.getFuelPrice(tripTo.getVehicleTypeId());
            fuelType = tripDAO.getFuelType(tripTo.getVehicleTypeId());
//            } else {//secondary
//                //get toll, addltoll, misc for the route 
//                String secondaryRouteExpense = tripDAO.getSecondaryRouteExpense(tripTO);
//                System.out.println("secondaryRouteExpense:" + secondaryRouteExpense);
//                String[] tempVar = secondaryRouteExpense.split("~");
//
//                tripTo.setSecondaryTollAmount(tempVar[0]);
//                tripTo.setSecondaryAddlTollAmount(tempVar[1]);
//                tripTo.setSecondaryMiscAmount(tempVar[2]);
//                tripTo.setSecondaryParkingAmount(tempVar[3]);
//                fuelPrice = tripDAO.getSecondaryFuelPrice(tripTo.getVehicleTypeId(), tripTO);
//                fuelType = tripDAO.getSecondaryFuelType(tripTo.getVehicleTypeId(), tripTO);
//            }
            tripTo.setTollAmount(tollAmount);
            tripTo.setDriverBatta(driverBatta);
            tripTo.setDriverIncentive(driverIncentive);
            tripTo.setFuelPrice(fuelPrice);
            tripTo.setFuelTypeId(fuelType);
            totalExpenseNew.add(tripTo);
        }
        return totalExpenseNew;
    }

    public int saveTripClosure(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveTripClosure(tripTO, userId);
        return status;
    }

    public int saveTripClosureDetails(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveTripClosureDetails(tripTO, userId);
        return status;
    }

    public ArrayList getTripStausDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList statusDetails = new ArrayList();
        statusDetails = tripDAO.getTripStausDetails(tripTO);
        return statusDetails;
    }

    public ArrayList getPODDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList podDetails = new ArrayList();
        podDetails = tripDAO.getPODDetails(tripTO);
        return podDetails;
    }
    public ArrayList getPODUploadDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList podDetails = new ArrayList();
        podDetails = tripDAO.getPODUploadDetails(tripTO);
        return podDetails;
    }

    public ArrayList getTripPackDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripPackDetails = new ArrayList();
        tripPackDetails = tripDAO.getTripPackDetails(tripTO);
        return tripPackDetails;
    }

    public ArrayList getTripUnPackDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripUnPackDetails = new ArrayList();
        tripUnPackDetails = tripDAO.getTripUnPackDetails(tripTO);
        return tripUnPackDetails;
    }

    public ArrayList viewApproveDetails(String tripid, int tripclosureid) throws FPBusinessException, FPRuntimeException {
        ArrayList viewApproveDetails = new ArrayList();
        viewApproveDetails = tripDAO.viewApproveDetails(tripid, tripclosureid);
        return viewApproveDetails;
    }

    public ArrayList getEmailDetails(String activitycode) throws FPRuntimeException, FPBusinessException {
        ArrayList EmailDetails = new ArrayList();
        EmailDetails = tripDAO.getEmailDetails(activitycode);
        return EmailDetails;
    }

    public ArrayList getmailcontactDetails(String tripid) throws FPRuntimeException, FPBusinessException {
        ArrayList EmailcontactDetails = new ArrayList();
        EmailcontactDetails = tripDAO.getmailcontactDetails(tripid);
        return EmailcontactDetails;
    }

    public ArrayList getCityList() throws FPRuntimeException, FPBusinessException {
        ArrayList cityList = new ArrayList();
        cityList = tripDAO.getCityList();
        return cityList;
    }

    /**
     * This method used to getTripStakeHolders .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getTripStakeHoldersList() throws FPRuntimeException, FPBusinessException {
        ArrayList holdersList = null;
        holdersList = tripDAO.getTripStakeHoldersList();
        return holdersList;
    }

    /**
     * This method used to getTripStakeHolders .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getstatusList() throws FPRuntimeException, FPBusinessException {
        ArrayList statusList = null;
        statusList = tripDAO.getstatusList();
        return statusList;
    }

    /**
     * This method used to Get Available Functions .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getEmailFunction(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList setStatusList = new ArrayList();
        setStatusList = tripDAO.getEmailFunction(tripTO);
        return setStatusList;
    }

    /**
     * This method used to Get Available Functions .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getAssignedFunc(String stackHolderId) throws FPRuntimeException, FPBusinessException {
        ArrayList assignedFunc = null;
        assignedFunc = tripDAO.getAssignedFunc(stackHolderId);
        return assignedFunc;
    }

    /**
     * This method used to Get Available Functions .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertEmailSettings(String[] assignedFunc, String stackHolderId, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.insertEmailSettings(assignedFunc, stackHolderId, userId);
        return status;
    }

    /**
     * This method used to Get Available Functions .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int deleteStakeholder(String[] assignedFunc, String stackHolderId, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.deleteStakeholder(assignedFunc, stackHolderId, userId);
        return status;
    }

    //    /**
    //     * This method used to Get  Assigned Functions.
    //     *
    //     * @param request - Http request object.
    //     *
    //     * @throws FPBusinessException - Throws when a business Exception araises
    //     *
    //     * @throws FPRuntimeException - Throws when a Runtime Exception araises
    //     */
    public ArrayList getEditStatuslist(String stackHolderId) throws FPRuntimeException, FPBusinessException {
        ArrayList statusList = null;
        statusList = tripDAO.getEditStatuslist(stackHolderId);
        return statusList;
    }

    /**
     * This method is used to insertvehicleDriverAdvance.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int insertVehicleDriverAdvance(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertVehicleDriverAdvance = 0;
        insertVehicleDriverAdvance = tripDAO.insertVehicleDriverAdvance(tripTO, userId);
        return insertVehicleDriverAdvance;
    }

    public String checkVehicleDriverAdvance(String vehicleId) {
        String checkVehicleDriverAdvance = "";
        checkVehicleDriverAdvance = tripDAO.checkVehicleDriverAdvance(vehicleId);
        return checkVehicleDriverAdvance;
    }

    public ArrayList getVehicleDriverAdvanceList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList statusList = null;
        statusList = tripDAO.getVehicleDriverAdvanceList(tripTO);
        return statusList;
    }

    public ArrayList getCustomerList() throws FPRuntimeException, FPBusinessException {
        ArrayList customerList = null;
        customerList = tripDAO.getCustomerList();
        return customerList;
    }

    public ArrayList getZoneList() throws FPRuntimeException, FPBusinessException {
        ArrayList zoneList = null;
        zoneList = tripDAO.getZoneList();
        return zoneList;
    }

    public ArrayList getLocation(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList city = new ArrayList();
        city = tripDAO.getLocation(tripTO);
        return city;
    }

    public int insertBPCLTransactionHistory(ArrayList bpclTransactionList, TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertBPCLTransactionHistory = 0;
        insertBPCLTransactionHistory = tripDAO.insertBPCLTransactionHistory(bpclTransactionList, tripTO, userId);
        return insertBPCLTransactionHistory;
    }

    public String checkTransactionHistoryId(TripTO tripTO) {
        String status = "";
        status = tripDAO.checkTransactionHistoryId(tripTO);
        return status;
    }

    public String getTripCodeForBpcl(TripTO tripTO) {
        String status = "";
        status = tripDAO.getTripCodeForBpcl(tripTO);
        return status;
    }

    public int getTripCodeCountForBpcl(TripTO tripTO) {
        int status = 0;
        status = tripDAO.getTripCodeCountForBpcl(tripTO);
        return status;
    }

    public ArrayList getTripWrongDataList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripWrongDataList = new ArrayList();
        tripWrongDataList = tripDAO.getTripWrongDataList(tripTO);
        return tripWrongDataList;
    }

    public ArrayList getBPCLTransactionHistory(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList bpclTransactionHistory = new ArrayList();
        bpclTransactionHistory = tripDAO.getBPCLTransactionHistory(tripTO);
        return bpclTransactionHistory;
    }

    public ArrayList getConsignmentListForUpdate(String consignmentOrderId) throws FPBusinessException, FPRuntimeException {
        ArrayList consignmentList = new ArrayList();
        consignmentList = tripDAO.getConsignmentListForUpdate(consignmentOrderId);
        return consignmentList;
    }

    public int saveEmptyTripSheet(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        String tripStatus = "";
        //generate tripcode
        String tripCode = "TS/17-18/";
        String tripCodeSequence = tripDAO.getTripCodeSequence();
        tripCode = tripCode + tripCodeSequence;
        tripTO.setTripCode(tripCode);

        //set transit days
        float transitHours = 0.00F;
        if (tripTO.getTripTransitHours() != null && !"".equals(tripTO.getTripTransitHours())) {
            transitHours = Float.parseFloat(tripTO.getTripTransitHours());
        }
        double transitDay = (double) transitHours / 24;
        tripTO.setTripTransitDays(transitDay + "");
        //set advance to be paid per day
        float totalExpense = 0.00f;
        if (tripTO.getOrderExpense() != null && !"".equals(tripTO.getOrderExpense())) {
            totalExpense = Float.parseFloat(tripTO.getOrderExpense());
        }

        int transitDayValue = (int) transitDay;
        if (transitDay > transitDayValue) {
            transitDay = transitDayValue + 1;
        }
        double advnaceToBePaidPerDay = 0;

        if (totalExpense > 0 && transitHours > 0) {
            advnaceToBePaidPerDay = totalExpense / transitDay;
        }
        tripTO.setAdvnaceToBePaidPerDay(advnaceToBePaidPerDay + "");

        tripTO.setStatusId("8");
        tripStatus = "Trip Created";

        //create trip master
        int tripId = tripDAO.saveTripSheet(tripTO);

        if (tripId > 0) {
            tripTO.setTripId(tripId + "");

            //if action is freeeze update prestart plan info
            /*
             if ("1".equals(tripTO.getActionName())
             && !"1".equals(tripTO.getPreStartLocationStatus())) { //freeze status
             status = tripDAO.updatePreStartDetails(tripTO);
             }
             */
            //create trip status details
            status = tripDAO.saveTripStatusDetails(tripTO);
            //create trip consignment order
            status = tripDAO.saveTripConsignments(tripTO);
            //create trip vehicles
            if (tripTO.getVehicleId() != null && !"".equals(tripTO.getVehicleId())) {
                status = tripDAO.saveTripVehicle(tripTO);
            }
            //create trip driver
//            if (tripTO.getPrimaryDriverId() != null && !"0".equals(tripTO.getPrimaryDriverId()) && !"".equals(tripTO.getPrimaryDriverId())) {
            status = tripDAO.saveTripDriver(tripTO);
//            }
            //crate trip points
            status = tripDAO.saveTripRoutePlanForEmptyTrip(tripTO);

            //////////////////Arun Start//////////////////////////
            String to = "";
            String cc = "";
            String smtp = "";
            int emailPort = 0;
            String frommailid = "";
            String password = "";
            String activitycode = "EMTRP1";

            ArrayList emaildetails = new ArrayList();
            emaildetails = getEmailDetails(activitycode);

            String emailString = getTripEmails(tripTO.getTripId(), tripTO.getStatusId());
            System.out.println("emailString:" + emailString);
            String[] emailTemp = emailString.split("~");
            int emailTempLenth = emailTemp.length;
            System.out.println("emailTempLenth: " + emailTempLenth);

            if (emailTempLenth > 0) {
                to = emailTemp[0];
            }
            if (emailTempLenth > 1) {
                cc = emailTemp[1];
            }

            Iterator itr1 = emaildetails.iterator();
            TripTO tripTO1 = null;
            if (itr1.hasNext()) {
                tripTO1 = new TripTO();
                tripTO1 = (TripTO) itr1.next();
                smtp = tripTO1.getSmtp();
                emailPort = Integer.parseInt(tripTO1.getPort());
                frommailid = tripTO1.getEmailId();
                password = tripTO1.getPassword();
            }
            String vehicle = tripTO.getVehicleNo();

            String emailFormat = "<html>"
                    + "<body>"
                    + "<p>Hi, <br><br>Empty Trip Created For " + tripTO.getTripCode() + "</p>"
                    + "<br> Customer:" + tripTO.getCustomerName()
                    + "<br> C Note No:" + tripTO.getcNotes()
                    + "<br> Route :" + tripTO.getRouteInfo()
                    + "<br> Vehicle No :" + vehicle
                    + "<br><br> Team BrattleFoods"
                    + "</body></html>";

            String subject = "Empty Trip Created for Vehicle " + vehicle + " Customer " + tripTO.getCustomerName() + " Route " + tripTO.getRouteInfo();

            String content = emailFormat;
            if (!"".equals(to)) {
                //new SendMail(smtp, emailPort, frommailid, password, subject, content, to, cc).start();
            }

            /////////////////////////////////////////////Email part///////////////////////
        }
        //send mail on trip creation to customer and account manager for contract customers
        return tripId;
    }

    public ArrayList getTripAdvanceDetailsStatus(String tripSheetId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripAdvanceDetailsStatus = new ArrayList();
        tripAdvanceDetailsStatus = tripDAO.getTripAdvanceDetailsStatus(tripSheetId);
        return tripAdvanceDetailsStatus;
    }

    public int saveWFUTripSheet(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveWFUTripSheet(tripTO, userId);
        return status;
    }

    public ArrayList getWfuDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList wfuDetails = new ArrayList();
        wfuDetails = tripDAO.getWfuDetails(tripTO);
        return wfuDetails;
    }

    public ArrayList getEmptyTripMergingList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList emptyTripList = new ArrayList();
        emptyTripList = tripDAO.getEmptyTripMergingList(tripTO);
        return emptyTripList;
    }

    public int insertEmptyTripMerging(String[] tripId, String[] tripSequence, String tripMergingRemarks, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.insertEmptyTripMerging(tripId, tripSequence, tripMergingRemarks, userId);
        return status;
    }

    public String getTripCode(int tripId) {
        String tripCode = "";
        tripCode = tripDAO.getTripCode(tripId);
        return tripCode;
    }

    public int saveEmptyTripApproval(String tripId, String approvalStatus, String userId, String mailId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveEmptyTripApproval(tripId, approvalStatus, userId, mailId);
        return status;
    }

    public int saveSecondaryTripSheet(TripTO tripTO, ArrayList orderPointDetails) throws FPBusinessException, FPRuntimeException, IOException {
        int status = 0;
        String tripStatus = "";
        //generate tripcode
        String tripCode = "TS/13-14/";
        String tripCodeSequence = tripDAO.getTripCodeSequence();
        tripCode = tripCode + tripCodeSequence;
        tripTO.setTripCode(tripCode);

        //set transit days
        float transitHours = 0.00F;
        if (tripTO.getTripTransitHours() != null && !"".equals(tripTO.getTripTransitHours())) {
            transitHours = Float.parseFloat(tripTO.getTripTransitHours());
        }
        double transitDay = (double) transitHours / 24;
        tripTO.setTripTransitDays(transitDay + "");
        //set advance to be paid per day
        float totalExpense = 0.00f;
        if (tripTO.getOrderExpense() != null && !"".equals(tripTO.getOrderExpense())) {
            totalExpense = Float.parseFloat(tripTO.getOrderExpense());
        }

        int transitDayValue = (int) transitDay;
        if (transitDay > transitDayValue) {
            transitDay = transitDayValue + 1;
        }
        double advnaceToBePaidPerDay = 0;

        if (totalExpense > 0 && transitHours > 0) {
            advnaceToBePaidPerDay = totalExpense / transitDay;
        }
        tripTO.setAdvnaceToBePaidPerDay(advnaceToBePaidPerDay + "");

        tripTO.setStatusId("8");
        tripStatus = "Trip Created";

        //create trip master
        tripTO.setTripType("secondary");
        int tripId = tripDAO.saveTripSheet(tripTO);

        if (tripId > 0) {
            tripTO.setTripId(tripId + "");

            //if action is freeeze update prestart plan info
            /*
             if ("1".equals(tripTO.getActionName())
             && !"1".equals(tripTO.getPreStartLocationStatus())) { //freeze status
             status = tripDAO.updatePreStartDetails(tripTO);
             }
             */
            //create trip status details
            status = tripDAO.saveTripStatusDetails(tripTO);
            //create trip consignment order
            status = tripDAO.saveTripConsignments(tripTO);
            //create trip vehicles
            if (tripTO.getVehicleId() != null && !"".equals(tripTO.getVehicleId())) {
                status = tripDAO.saveTripVehicle(tripTO);
            }
            //create trip driver
            System.out.println("tripTO.getPrimaryDriverId()srini:" + tripTO.getPrimaryDriverId());
            if (tripTO.getPrimaryDriverId() != null && !"0".equals(tripTO.getPrimaryDriverId()) && !"".equals(tripTO.getPrimaryDriverId())) {
                status = tripDAO.saveTripDriver(tripTO);
            }
            //crate trip points
            status = tripDAO.saveTripRoutePlanForSecondaryTrip(tripTO, orderPointDetails);

            //////////////////Arun Start//////////////////////////
            String to = "";
            String cc = "";
            String smtp = "";
            int emailPort = 0;
            String frommailid = "";
            String password = "";
            String activitycode = "EMTRP1";

            ArrayList emaildetails = new ArrayList();
            emaildetails = getEmailDetails(activitycode);

            String emailString = getTripEmails(tripTO.getTripId(), tripTO.getStatusId());
            System.out.println("emailString:" + emailString);
            String[] emailTemp = emailString.split("~");
            int emailTempLenth = emailTemp.length;
            System.out.println("emailTempLenth: " + emailTempLenth);

            if (emailTempLenth > 0) {
                to = emailTemp[0];
            }
            if (emailTempLenth > 1) {
                cc = emailTemp[1];
            }

            Iterator itr1 = emaildetails.iterator();
            TripTO tripTO1 = null;
            if (itr1.hasNext()) {
                tripTO1 = new TripTO();
                tripTO1 = (TripTO) itr1.next();
                smtp = tripTO1.getSmtp();
                emailPort = Integer.parseInt(tripTO1.getPort());
                frommailid = tripTO1.getEmailId();
                password = tripTO1.getPassword();
            }
            String vehicle = tripTO.getVehicleNo();

            String emailFormat = "<html>"
                    + "<body>"
                    + "<p>Hi, <br><br>Secondary Trip Created For " + tripTO.getTripCode() + "</p>"
                    + "<br> Customer:" + tripTO.getCustomerName()
                    + "<br> C Note No:" + tripTO.getcNotes()
                    + "<br> Route :" + tripTO.getRouteInfo()
                    + "<br> Vehicle No :" + vehicle
                    + "<br><br> Team BrattleFoods"
                    + "</body></html>";

            String subject = "Secondary Trip Created for Vehicle " + vehicle + " Customer " + tripTO.getCustomerName() + " Route " + tripTO.getRouteInfo();
            String content = emailFormat;
            if (!"".equals(to)) {
                int mailSendingId = 0;
                tripTO.setMailTypeId("2");
                tripTO.setMailSubjectTo(subject);
                tripTO.setMailSubjectCc(subject);
                tripTO.setMailSubjectBcc("");
                tripTO.setMailContentTo(content);
                tripTO.setMailContentCc(content);
                tripTO.setMailContentBcc("");
                tripTO.setMailIdTo(to);
                tripTO.setMailIdCc(cc);
                tripTO.setMailIdBcc("");
//                mailSendingId = tripDAO.insertMailDetails(tripTO, tripTO.getUserId());
                new SendMail(smtp, emailPort, frommailid, password, subject, content, to, cc, tripTO.getUserId()).start();
            }

            /////////////////////////////////////////////Email part///////////////////////
        }
        //send mail on trip creation to customer and account manager for contract customers
        return tripId;
    }

    public ArrayList getEmptyTripDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList emptyTripList = new ArrayList();
        emptyTripList = tripDAO.getEmptyTripDetails(tripTO);
        return emptyTripList;
    }

    public int saveManualEmptyTripApproval(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveManualEmptyTripApproval(tripTO);
        return status;
    }

    public int checkeEmptyTripApproval(String tripId, String approvalStatus, String userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.checkeEmptyTripApproval(tripId, approvalStatus, userId);
        return status;
    }

    public ArrayList getTempLogDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList viewTempLogDetails = new ArrayList();
        viewTempLogDetails = tripDAO.getTempLogDetails(tripTO);
        return viewTempLogDetails;
    }

    public int saveTempLogDetails(String actualFilePath, String fileSaved, TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveTempLogDetails(actualFilePath, fileSaved, tripTO, userId);
        return status;
    }

    public int saveTemperatureLogApproval(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveTemperatureLogApproval(tripTO, userId);
        return status;
    }

    public int checkTemperatureLogApproval(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.checkTemperatureLogApproval(tripTO, userId);
        return status;
    }

    public String checkConsignmentCreditLimitStatus(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = tripDAO.checkConsignmentCreditLimitStatus(tripTO, userId);
        return status;
    }

    public int updateConsignmentOrderCreditLimitStatus(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateConsignmentOrderCreditLimitStatus(tripTO, userId);
        return status;
    }

    public int updateConsignmentOrderApprovalStatus(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateConsignmentOrderApprovalStatus(tripTO, userId);
        return status;
    }

    public int insertLoadingUnloadingDetails(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        System.out.println("TEST");
        status = tripDAO.insertLoadingUnloadingDetails(tripTO, userId);
        return status;
    }

    public int insertLoadingUnloadingDetailsClosure(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        System.out.println("TEST");
        status = tripDAO.insertLoadingUnloadingDetailsClosure(tripTO, userId);
        return status;
    }

    public ArrayList getStatusList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList statusList = new ArrayList();
        statusList = tripDAO.getStatusList(tripTO);
        return statusList;
    }

    public ArrayList getTripVehicleNo(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleNos = new ArrayList();
        vehicleNos = tripDAO.getTripVehicleNo(tripTO);
        return vehicleNos;
    }

    public ArrayList getConsignmentPaymentDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consignmentPaymentList = new ArrayList();
        consignmentPaymentList = tripDAO.getConsignmentPaymentDetails(tripTO);
        return consignmentPaymentList;
    }

    public ArrayList getEmailList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getEmailList = new ArrayList();
        getEmailList = tripDAO.getEmailList(tripTO);
        return getEmailList;
    }

    public int saveVehicleDriverAdvanceApproval(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveVehicleDriverAdvanceApproval(tripTO);
        return status;
    }

    public int checkVehicleDriverAdvanceApproval(String vehicleDriverAdvanceId, String approvalStatus, String userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.checkVehicleDriverAdvanceApproval(vehicleDriverAdvanceId, approvalStatus, userId);
        return status;
    }

    public ArrayList getVehicleAdvanceRequest(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList advanceRequest = new ArrayList();
        advanceRequest = tripDAO.getVehicleAdvanceRequest(tripTO);
        return advanceRequest;
    }

    public ArrayList getViewVehicleDriverAdvanceList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList viewVehicleDriverAdvanceList = new ArrayList();
        viewVehicleDriverAdvanceList = tripDAO.getViewVehicleDriverAdvanceList(tripTO);
        return viewVehicleDriverAdvanceList;
    }

    public int saveVehicleDriverAdvancePay(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveVehicleDriverAdvancePay(tripTO);
        return status;
    }

    public String getLastBpclTransactionDate() {
        String lastBpclTxDate = "";
        lastBpclTxDate = tripDAO.getLastBpclTransactionDate();
        return lastBpclTxDate;
    }

    public ArrayList getApprovalValueDetails(double actualval) throws FPRuntimeException, FPBusinessException {
        ArrayList getApprovalValueDetails = new ArrayList();
        getApprovalValueDetails = tripDAO.getApprovalValueDetails(actualval);
        return getApprovalValueDetails;
    }

    public ArrayList getRepairMaintenence(int configId) throws FPRuntimeException, FPBusinessException {
        ArrayList getRepairMaintenence = new ArrayList();
        getRepairMaintenence = tripDAO.getRepairMaintenence(configId);
        return getRepairMaintenence;
    }

    public String getFCLeadMailId(String vehicleId) {
        String fcLeadMailId = "";
        fcLeadMailId = tripDAO.getFCLeadMailId(vehicleId);
        return fcLeadMailId;
    }

    public String getSecondaryFCLeadMailId(String vehicleId) {
        String fcLeadMailId = "";
        fcLeadMailId = tripDAO.getSecondaryFCLeadMailId(vehicleId);
        return fcLeadMailId;
    }

    public String getSecondaryApprovalPerson(String vehicleId) {
        String approvalPersonMailId = "";
        approvalPersonMailId = tripDAO.getSecondaryApprovalPerson(vehicleId);
        return approvalPersonMailId;
    }

    public String getPreviousTripsOdometerReading(String tripSheetId) {
        String previousTripsOdometerReading = "";
        previousTripsOdometerReading = tripDAO.getPreviousTripsOdometerReading(tripSheetId);
        return previousTripsOdometerReading;
    }

    public int insertBillingGraph(String tripSheetId, String graphPath, int userId) throws IOException {
        int insertBillingGraph = 0;
        insertBillingGraph = tripDAO.insertBillingGraph(tripSheetId, graphPath, userId);
        return insertBillingGraph;
    }

    public String getTripCount(TripTO tripTO) {
        String tripCount = "";
        tripCount = tripDAO.getTripCount(tripTO);
        return tripCount;
    }

    public int updateInvoiceAmount(TripTO tripTO, int userId) throws IOException {
        int updateInvoiceAmount = 0;
        updateInvoiceAmount = tripDAO.updateInvoiceAmount(tripTO, userId);
        return updateInvoiceAmount;
    }

    public int insertCourierDetails(TripTO tripTO, int userId) throws IOException {
        int insertCourierDetails = 0;
        insertCourierDetails = tripDAO.insertCourierDetails(tripTO, userId);
        return insertCourierDetails;
    }

    public ArrayList getCourierDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList courierDetails = new ArrayList();
        courierDetails = tripDAO.getCourierDetails(tripTO);
        return courierDetails;
    }

    public int checkTempGraphApprovedStatus(TripTO tripTO, int userId) throws IOException {
        int checkTempGraphApprovedStatus = 0;
        checkTempGraphApprovedStatus = tripDAO.checkTempGraphApprovedStatus(tripTO, userId);
        return checkTempGraphApprovedStatus;
    }

    public int checkBillSubmittedStatus(TripTO tripTO, int userId) throws IOException {
        int checkBillSubmittedStatus = 0;
        checkBillSubmittedStatus = tripDAO.checkBillSubmittedStatus(tripTO, userId);
        return checkBillSubmittedStatus;
    }

    public ArrayList getVehicleLogDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleLogDetails = new ArrayList();
        vehicleLogDetails = tripDAO.getVehicleLogDetails(tripTO);
        return vehicleLogDetails;
    }

    public ArrayList getEmployeeTripDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList employeeTripDetails = new ArrayList();
        employeeTripDetails = tripDAO.getEmployeeTripDetails(tripTO);
        return employeeTripDetails;
    }

    public int updateEmployeeInTrip(TripTO tripTO, int userId) throws IOException {
        int checkBillSubmittedStatus = 0;
        checkBillSubmittedStatus = tripDAO.updateEmployeeInTrip(tripTO, userId);
        return checkBillSubmittedStatus;
    }

    public String checkEmployeeInTrip(String driverId) {
        String checkEmployeeInTrip = "";
        checkEmployeeInTrip = tripDAO.checkEmployeeInTrip(driverId);
        return checkEmployeeInTrip;
    }

    public ArrayList getVehicleChangeTripAdvanceDetails(String tripSheetId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripAdvanceDetails = new ArrayList();
        tripAdvanceDetails = tripDAO.getVehicleChangeTripAdvanceDetails(tripSheetId);
        return tripAdvanceDetails;
    }

    public int insertMailDetails(TripTO tripTO, int userId) throws IOException {
        int insertMailDetails = 0;
        insertMailDetails = tripDAO.insertMailDetails(tripTO, userId);
        return insertMailDetails;
    }

    public ArrayList getMailNotDeliveredList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList mailNotDeliveredList = new ArrayList();
        mailNotDeliveredList = tripDAO.getMailNotDeliveredList(tripTO);
        return mailNotDeliveredList;
    }

    public int updateTripOtherExpenseDoneStatus(TripTO tripTO, int userId) {
        int updateOtherExpenseDoneStatus = 0;
        updateOtherExpenseDoneStatus = tripDAO.updateTripOtherExpenseDoneStatus(tripTO, userId);
        return updateOtherExpenseDoneStatus;
    }

    public String getOtherExpenseDoneStatus(TripTO tripTO) {
        String otherExpenseDoneStatus = "";
        otherExpenseDoneStatus = tripDAO.getOtherExpenseDoneStatus(tripTO);
        return otherExpenseDoneStatus;
    }

    public String getLastUploadCustomerOutstandingDate() {
        String lastCustomerOutstandingDate = "";
        lastCustomerOutstandingDate = tripDAO.getLastUploadCustomerOutstandingDate();
        return lastCustomerOutstandingDate;
    }

    public String getPreviousOutStandingAmount(TripTO tripTO) {
        String outStandingAmount = "";
        outStandingAmount = tripDAO.getPreviousOutStandingAmount(tripTO);
        return outStandingAmount;
    }

    public int updateCustomerOutStandingAmount(String[] customerId, String[] outStandingAmount, int userId) {
        int updateCustomerOutStandingAmount = 0;
        updateCustomerOutStandingAmount = tripDAO.updateCustomerOutStandingAmount(customerId, outStandingAmount, userId);
        return updateCustomerOutStandingAmount;
    }

    public ArrayList getCustomerOutStandingList() throws FPRuntimeException, FPBusinessException {
        ArrayList customerOutStandingList = new ArrayList();
        customerOutStandingList = tripDAO.getCustomerOutStandingList();
        return customerOutStandingList;
    }

    public ArrayList getSecondaryCustomerApprovalList() throws FPRuntimeException, FPBusinessException {
        ArrayList secondaryCustomerApprovalList = new ArrayList();
        secondaryCustomerApprovalList = tripDAO.getSecondaryCustomerApprovalList();
        return secondaryCustomerApprovalList;
    }

    public int updateSecondaryCustomerMail(TripTO tripTO) {
        int updateSecondaryCustomerMail = 0;
        updateSecondaryCustomerMail = tripDAO.updateSecondaryCustomerMail(tripTO);
        return updateSecondaryCustomerMail;
    }

    public int updateEmptyTripRoute(TripTO tripTO) {
        int updateEmptyTripRoute = 0;
        updateEmptyTripRoute = tripDAO.updateEmptyTripRoute(tripTO);
        return updateEmptyTripRoute;
    }

    public String getVehicleUsageTypeId(String vehicleId) {
        String usageTypeId = "";
        usageTypeId = tripDAO.getVehicleUsageTypeId(vehicleId);
        return usageTypeId;
    }

    public String getVehicleCurrentStatus(String tripId) {
        String vehicleCurrentStatus = "";
        vehicleCurrentStatus = tripDAO.getVehicleCurrentStatus(tripId);
        return vehicleCurrentStatus;
    }

    public int updateDeleteTrip(TripTO tripTO) {
        int updatedeletetrip = 0;
        updatedeletetrip = tripDAO.updateDeleteTrip(tripTO);
        return updatedeletetrip;

    }

    public int updateTripAdvance(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateTripAdvance(tripTO, userId);
        return status;
    }

    public int updateTripEnd(TripTO tripTO, int userId) {
        int updatetripend = 0;
        updatetripend = tripDAO.updateTripEnd(tripTO, userId);
        return updatetripend;

    }

    public int updateTripStartDetails(TripTO tripTO, int userId) {
        int updateTripStartDetails = 0;
        updateTripStartDetails = tripDAO.updateTripStartDetails(tripTO, userId);
        return updateTripStartDetails;
    }

    public ArrayList getTripClosureVehicleList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripClosureVehicleList = new ArrayList();
        tripClosureVehicleList = tripDAO.getTripClosureVehicleList(tripTO);
        return tripClosureVehicleList;
    }

    public ArrayList getTripSettlementVehicleList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripClosureVehicleList = new ArrayList();
        tripClosureVehicleList = tripDAO.getTripSettlementVehicleList(tripTO);
        return tripClosureVehicleList;
    }

    public ArrayList getEmptyCustomerList(TripTO tripTO) {
        ArrayList getEmptyCustomerList = new ArrayList();
        getEmptyCustomerList = tripDAO.getEmptyCustomerList(tripTO);
        return getEmptyCustomerList;
    }

    public ArrayList getTicketingStatusList(TripTO tripTO) {
        ArrayList ticketingStatusList = new ArrayList();
        ticketingStatusList = tripDAO.getTicketingStatusList(tripTO);
        return ticketingStatusList;
    }

    public ArrayList getTicketingList(TripTO tripTO) {
        ArrayList ticketingStatusList = new ArrayList();
        ticketingStatusList = tripDAO.getTicketingList(tripTO);
        return ticketingStatusList;
    }

    public ArrayList getTicketingDetailsList(TripTO tripTO) {
        ArrayList ticketingDetailsList = new ArrayList();
        ticketingDetailsList = tripDAO.getTicketingDetailsList(tripTO);
        return ticketingDetailsList;
    }

    public ArrayList getTicketingStatusDetailsList(TripTO tripTO) {
        ArrayList ticketingStatusDetailsList = new ArrayList();
        ticketingStatusDetailsList = tripDAO.getTicketingStatusDetailsList(tripTO);
        return ticketingStatusDetailsList;
    }

    public int saveTicketFile(String actualFilePath, String fileSaved, TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveTicketFile(actualFilePath, fileSaved, tripTO, userId);
        return status;
    }

    public int saveTicket(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int ticketId = 0;
        ticketId = tripDAO.saveTicket(tripTO, userId);
        System.out.println("ticketId in the tripBP = " + ticketId);
        if (ticketId > 0) {
            String to = "";
            String cc = "";
            String smtp = "";
            int emailPort = 0;
            String priority = "";
            String type = "";
            String statusName = "";
            String raisedBy = "";
            String raisedOn = "";
            String frommailid = "";
            String password = "";
            String activitycode = "EMTRP1";

            ArrayList emaildetails = new ArrayList();
            emaildetails = getEmailDetails(activitycode);
            Iterator itr1 = emaildetails.iterator();
            TripTO tripTO1 = null;
            if (itr1.hasNext()) {
                tripTO1 = new TripTO();
                tripTO1 = (TripTO) itr1.next();
                smtp = tripTO1.getSmtp();
                emailPort = Integer.parseInt(tripTO1.getPort());
                frommailid = "ticketing.brattle@gmail.com";
                password = "brattle@123";
            }
            tripTO.setTicketId(String.valueOf(ticketId));
            tripTO.setUserId(userId);
            ArrayList ticketingStatusList = new ArrayList();
            ticketingStatusList = tripDAO.getTicketingList(tripTO);
            Iterator itr2 = ticketingStatusList.iterator();
            TripTO tripTO2 = null;
            if (itr2.hasNext()) {
                tripTO2 = new TripTO();
                tripTO2 = (TripTO) itr2.next();
                priority = tripTO2.getPriority();
                type = tripTO2.getType();
                statusName = tripTO2.getStatus();
                raisedBy = tripTO2.getRaisedBy();
                raisedOn = tripTO2.getRaisedOn();
            }
            String emailFormat = "<html>"
                    + "<body>"
                    + "<p>Hi, <br><br>Ticket Created :</p>"
                    + "<br> Ticket No:" + "TN-" + ticketId
                    + "<br> Priority:" + priority
                    + "<br> Type :" + type
                    + "<br> Status :" + statusName
                    + "<br> Raised By :" + raisedBy
                    + "<br> Raised On :" + raisedOn
                    + "<br> Message :" + tripTO.getMessage()
                    + "<br><br> Team BrattleFoods"
                    + "</body></html>";

            String subject = tripTO.getTitle();
            String content = emailFormat;
            new SendMail(smtp, emailPort, frommailid, password, subject, content, tripTO.getTo(), tripTO.getCc(), tripTO.getUserId()).start();
            String mobileNo = "8122725178,9940204724,08826350111,8826437820,9790963689";
            String sMsg = "Hi Throttle";
            try {
                //send sms
                sMsg = "Ticket No : " + "TN-" + ticketId + ", Type : " + type + ", Status : " + statusName + ", Priority : " + priority + ", Raised By : " + raisedBy + ", Raised On : " + raisedOn;
                System.out.println("smsg is :" + sMsg);
                sMsg = sMsg.replaceAll(" ", "%20");
                System.out.println("After sMsg: " + sMsg);
                //http://www.nexmoo.com/hebron/smssent.php?mno=9940117411&msg=text%20msg
                URL url = new URL("http://www.nexmoo.com/entitle/sms.php?phoneno=" + mobileNo + "&msg=" + sMsg);
                System.out.println("url:" + url.toString());
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
                OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
                out.write("target=" + "getAllStopDetails");
                out.close();
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String decodedString = in.readLine();
                System.out.println(decodedString); //msg sent successfully
                if (decodedString.equals("msg sent successfully")) {
                    System.out.println("msg sent successfully");
                }
                in.close();
            } catch (Exception e) {
                System.out.println("Unable to send may be sms... pls check....");
                e.printStackTrace();
            }

        }
        return ticketId;
    }

    public int updateTicketStatus(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateTicketStatus(tripTO, userId);
        if (status > 0) {
            String to = "";
            String cc = "";
            String smtp = "";
            int emailPort = 0;
            String priority = "";
            String type = "";
            String statusName = "";
            String raisedBy = "";
            String raisedOn = "";
            String frommailid = "";
            String password = "";
            String activitycode = "EMTRP1";

            ArrayList emaildetails = new ArrayList();
            emaildetails = getEmailDetails(activitycode);
            Iterator itr1 = emaildetails.iterator();
            TripTO tripTO1 = null;
            if (itr1.hasNext()) {
                tripTO1 = new TripTO();
                tripTO1 = (TripTO) itr1.next();
                smtp = tripTO1.getSmtp();
                emailPort = Integer.parseInt(tripTO1.getPort());
                frommailid = "ticketing.brattle@gmail.com";
                password = "brattle@123";
            }
            ArrayList ticketingStatusList = new ArrayList();
            ticketingStatusList = tripDAO.getTicketList(tripTO);
            Iterator itr2 = ticketingStatusList.iterator();
            TripTO tripTO2 = null;
            if (itr2.hasNext()) {
                tripTO2 = new TripTO();
                tripTO2 = (TripTO) itr2.next();
                priority = tripTO2.getPriority();
                type = tripTO2.getType();
                statusName = tripTO2.getStatus();
                raisedBy = tripTO2.getRaisedBy();
                raisedOn = tripTO2.getRaisedOn();
            }
            String emailFormat = "<html>"
                    + "<body>"
                    + "<p>Hi, <br><br>Ticket Updated :</p>"
                    + "<br> Ticket No:" + tripTO.getTicketNo()
                    + "<br> Priority:" + priority
                    + "<br> Type :" + type
                    + "<br> Status :" + statusName
                    + "<br> Followed By :" + raisedBy
                    + "<br> Followed On :" + raisedOn
                    + "<br> Message :" + tripTO.getMessage()
                    + "<br><br> Team BrattleFoods"
                    + "</body></html>";

            String subject = tripTO.getTitle();
            String content = emailFormat;
            new SendMail(smtp, emailPort, frommailid, password, subject, content, tripTO.getTo(), tripTO.getCc(), tripTO.getUserId()).start();
            String mobileNo = "8122725178,9940204724,08826350111,8826437820,9790963689";
            String sMsg = "Hi Throttle";
            try {
                //send sms
                sMsg = "Ticket No : " + tripTO.getTicketNo() + ", Type : " + type + ", Status : " + statusName + ", Priority : " + priority + ", Followed By : " + raisedBy + ", Followed On : " + raisedOn;
                System.out.println("smsg is :" + sMsg);
                sMsg = sMsg.replaceAll(" ", "%20");
                System.out.println("After sMsg: " + sMsg);
                //http://www.nexmoo.com/hebron/smssent.php?mno=9940117411&msg=text%20msg
                URL url = new URL("http://www.nexmoo.com/entitle/sms.php?phoneno=" + mobileNo + "&msg=" + sMsg);
                System.out.println("url:" + url.toString());
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
                OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
                out.write("target=" + "getAllStopDetails");
                out.close();
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String decodedString = in.readLine();
                System.out.println(decodedString); //msg sent successfully
                if (decodedString.equals("msg sent successfully")) {
                    System.out.println("msg sent successfully");
                }
                in.close();
            } catch (Exception e) {
                System.out.println("Unable to send may be sms... pls check....");
                e.printStackTrace();
            }

        }
        return status;
    }

    public ArrayList tripsettelmentDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripsettelmentDetails = new ArrayList();
        tripsettelmentDetails = tripDAO.gettripsettelmentDetails(tripTO);
        return tripsettelmentDetails;
    }

    public ArrayList getVehicleUnloadingDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList getVehicleUnloadingDetails = new ArrayList();
        getVehicleUnloadingDetails = tripDAO.getVehicleUnloadingDetails(tripTO);
        return getVehicleUnloadingDetails;
    }

    public String getPreviousTripEndTime(String tripSheetId) {
        String getPreviousTripEndTime = "";
        getPreviousTripEndTime = tripDAO.getPreviousTripsEndTime(tripSheetId);
        return getPreviousTripEndTime;
    }

    public ArrayList getSecFCmailcontactDetails(String customerName) throws FPRuntimeException, FPBusinessException {
        ArrayList EmailcontactDetails = new ArrayList();
        EmailcontactDetails = tripDAO.getSecFCmailcontactDetails(customerName);
        return EmailcontactDetails;
    }

    public int updateNextTrip(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateNextTrip(tripTO, userId);
        return status;
    }

    public ArrayList getPrimaryDriverSettlementTrip(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList primaryDriverSettlementTrip = new ArrayList();
        primaryDriverSettlementTrip = tripDAO.getPrimaryDriverSettlementTrip(tripTO);
        return primaryDriverSettlementTrip;
    }

    public ArrayList getVehicleDriverAdvance(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleDriverAdvance = new ArrayList();
        vehicleDriverAdvance = tripDAO.getVehicleDriverAdvance(tripTO);
        return vehicleDriverAdvance;
    }

    public ArrayList getDriverIdleBhatta(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList driverIdleBhatta = new ArrayList();
        driverIdleBhatta = tripDAO.getDriverIdleBhatta(tripTO);
        return driverIdleBhatta;
    }

    public String getDriverLastBalanceAmount(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        String driverLastBalanceAmount = "";
        driverLastBalanceAmount = tripDAO.getDriverLastBalanceAmount(tripTO);
        return driverLastBalanceAmount;
    }

    public int insertPrimaryDriverSettlement(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.insertPrimaryDriverSettlement(tripTO, userId);
        return status;
    }

    public int insertPrimaryDriverSettlementDetails(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.insertPrimaryDriverSettlementDetails(tripTO, userId);
        return status;
    }

    public int updateIdleBhattaStatus(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateIdleBhattaStatus(tripTO, userId);
        return status;
    }

    public ArrayList getTripOtherExpenseDocumentRequired(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDocumentRequiredDetails = new ArrayList();
        tripDocumentRequiredDetails = tripDAO.getTripOtherExpenseDocumentRequired(tripTO);
        return tripDocumentRequiredDetails;
    }

    public ArrayList getOtherExpenseFileDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList podDetails = new ArrayList();
        podDetails = tripDAO.getOtherExpenseFileDetails(tripTO);
        return podDetails;
    }

    public int deleteExpenaseBillCopy(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.deleteExpenaseBillCopy(tripTO);
        return status;
    }

    public String getTripUnclearedBalance(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        String tripUnclearedBalance = "";
        tripUnclearedBalance = tripDAO.getTripUnclearedBalance(tripTO);
        return tripUnclearedBalance;
    }

    public ArrayList getTripSheetDetailsForChallan(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getTripSheetDetailsForChallan(tripTO);
        return tripDetails;
    }

    public String getTotalBillExpenseForDriver(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = tripDAO.getTotalBillExpenseForDriver(tripTO);
        if (status == null) {
            status = "0~0";
        }
        return status;
    }

    public ArrayList getTripsBilledDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList result = new ArrayList();
        result = tripDAO.getTripsBilledDetails(tripTO);
        return result;
    }

    public ArrayList getOrderInvoiceHeader(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList result = new ArrayList();
        result = tripDAO.getOrderInvoiceHeader(tripTO);
        return result;
    }

    public ArrayList getOrderInvoiceHeaderBill(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList result = new ArrayList();
        result = tripDAO.getOrderInvoiceHeaderBill(tripTO);
        return result;
    }

    public ArrayList getOrderInvoiceHeaderRemb(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList result = new ArrayList();
        result = tripDAO.getOrderInvoiceHeaderRemb(tripTO);
        return result;
    }

    public ArrayList getGSTTaxPercentage(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList taxList = new ArrayList();
        taxList = tripDAO.getGSTTaxPercentage(tripTO);
        return taxList;
    }

    public ArrayList getCompanyDetailsList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList companyList = new ArrayList();
        companyList = tripDAO.getCompanyDetailsList(tripTO);
        return companyList;
    }

    public ArrayList getCustomerDetailsList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList customerList = new ArrayList();
        customerList = tripDAO.getCustomerDetailsList(tripTO);
        return customerList;
    }

    public int updateInvoiceBillTax(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateInvoiceBillTax(tripTO);
        return status;
    }

    public int updateInvoiceReimbusTax(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateInvoiceReimbusTax(tripTO);
        return status;
    }

    public ArrayList getInvoiceGSTTaxDetailsBill(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList gstDetails = new ArrayList();
        gstDetails = tripDAO.getInvoiceGSTTaxDetailsBill(tripTO);
        return gstDetails;
    }

    public ArrayList getInvoiceGSTTaxDetailsRemb(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList gstDetails = new ArrayList();
        gstDetails = tripDAO.getInvoiceGSTTaxDetailsRemb(tripTO);
        return gstDetails;
    }

    public String showGSTNo(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        String showGSTNo = new String();
        showGSTNo = tripDAO.getshowGSTNo(tripTO);
        return showGSTNo;
    }

    public String getHireVendorTripExpense(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        String info = "";
        info = tripDAO.getHireVendorTripExpense(tripTO);
        return info;
    }

    public ArrayList getVendorList() throws FPRuntimeException, FPBusinessException {
        ArrayList statusList = new ArrayList();
        statusList = tripDAO.getVendorList();
        return statusList;
    }

    public ArrayList getVehicleRegNos(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleNos = new ArrayList();
        vehicleNos = tripDAO.getVehicleRegNos(tripTO);
        return vehicleNos;
    }

    public ArrayList getVendorVehicleType(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleNos = new ArrayList();
        vehicleNos = tripDAO.getVendorVehicleType(tripTO);
        return vehicleNos;
    }

    public ArrayList getFinanceStateList(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList financeStateList = new ArrayList();
        financeStateList = tripDAO.getFinanceStateList(tripTO);
        return financeStateList;
    }

    public ArrayList getDriverNames(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList drivers = new ArrayList();
        drivers = tripDAO.getDriverNames(tripTO);
        return drivers;
    }

    public int updateEstimateFeright(TripTO tripTO, String[] consignmentId, String[] estimatedRevenue) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = tripDAO.updateEstimateFeright(tripTO, consignmentId, estimatedRevenue);
        return status;

    }

    public int insertFuelLiter(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        System.out.println("TEST BP");
        status = tripDAO.insertFuelLiter(tripTO, userId);
        return status;
    }

    public ArrayList getTripFuel(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getTripFuel = new ArrayList();
        getTripFuel = tripDAO.getTripFuel(tripTO);
        return getTripFuel;
    }

    public int updateTripFuel(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        System.out.println("TEST");
        status = tripDAO.updateTripFuel(tripTO, userId);
        return status;
    }

    public int saveBillHeaderCancellation(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveBillHeaderCancellation(tripTO);
        return status;
    }

    public int saveBillDetailsCancellation(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveBillDetailsCancellation(tripTO);
        return status;
    }

    public int updateInvoiceCancellationTax(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateInvoiceCancellationTax(tripTO);
        return status;
    }

    public int updateCancellationStatus(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateCancellationStatus(tripTO, userId);
        return status;
    }

    public ArrayList getOrderInvoiceHeaderCancellation(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList result = new ArrayList();
        result = tripDAO.getOrderInvoiceHeaderCancellation(tripTO);
        return result;
    }

    public ArrayList getInvoiceGSTTaxDetailsCancellation(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList gstDetails = new ArrayList();
        gstDetails = tripDAO.getInvoiceGSTTaxDetailsCancellation(tripTO);
        return gstDetails;
    }
//  no need

    public int saveBillHeaderTSP(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveBillHeaderTSP(tripTO);
        return status;
    }

    public int saveBillDetailsTSP(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveBillDetailsTSP(tripTO);
        return status;
    }

    public int updateTSPinvoiceStatus(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateTSPinvoiceStatus(tripTO, userId);
        return status;
    }

    public int updateInvoiceTspTax(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateInvoiceTspTax(tripTO);
        return status;
    }

    public int saveBillHeaderRoyalty(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveBillHeaderRoyalty(tripTO);
        return status;
    }

    public int saveBillDetailsRoyalty(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveBillDetailsRoyalty(tripTO);
        return status;
    }

    public int updateRoyaltyStatus(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateRoyaltyStatus(tripTO, userId);
        return status;
    }

    public int updateInvoiceRoyaltyTax(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateInvoiceRoyaltyTax(tripTO);
        return status;
    }

    // no need
    public ArrayList getOrderList(int custId, int tripId) throws FPRuntimeException, FPBusinessException {
        ArrayList tempList = new ArrayList();
        tempList = tripDAO.getOrderList(custId, tripId);
        if (tempList.size() == 0) {
            // throw new FPBusinessException("EM-SER-10");
        }
        return tempList;
    }

    public ArrayList getOrderInvoiceHeaderTsp(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList result = new ArrayList();
        result = tripDAO.getOrderInvoiceHeaderTsp(tripTO);
        return result;
    }

    public ArrayList getInvoiceGSTTaxDetailsTsp(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList gstDetails = new ArrayList();
        gstDetails = tripDAO.getInvoiceGSTTaxDetailsTsp(tripTO);
        return gstDetails;
    }

    public ArrayList getOrderInvoiceHeaderRoyalty(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList result = new ArrayList();
        result = tripDAO.getOrderInvoiceHeaderRoyalty(tripTO);
        return result;
    }

    public ArrayList getInvoiceGSTTaxDetailsRoyalty(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList gstDetails = new ArrayList();
        gstDetails = tripDAO.getInvoiceGSTTaxDetailsRoyalty(tripTO);
        return gstDetails;
    }

    public ArrayList getVehicleLHCRegNos(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleNos = new ArrayList();
        vehicleNos = tripDAO.getVehicleLHCRegNos(tripTO);
        return vehicleNos;
    }

    public String getOrderVehicleType(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        String vehicleType = "";
        vehicleType = tripDAO.getOrderVehicleType(tripTO);
        return vehicleType;
    }

    public ArrayList getLhcDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList lhcDetails = new ArrayList();
        lhcDetails = tripDAO.getLhcDetails(tripTO);
        return lhcDetails;
    }

    public ArrayList getLhcVehicleRegNo(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleNo = new ArrayList();
        vehicleNo = tripDAO.getLhcVehicleRegNo(tripTO);
        return vehicleNo;
    }

    public ArrayList getPrintTripSheetDetails(String tripSheetId, String printDocketNo) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetailsList = new ArrayList();
        tripDetailsList = tripDAO.getPrintTripSheetDetails(tripSheetId, printDocketNo);
        return tripDetailsList;
    }

    public ArrayList getLHCDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getLHCDetails = new ArrayList();
        getLHCDetails = tripDAO.getLHCDetails(tripTO);
        return getLHCDetails;
    }

    public int updateGeneralTax(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateGeneralTax(tripTO);
        return status;
    }

    public ArrayList getInvoiceGSTTaxDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList gstDetails = new ArrayList();
        gstDetails = tripDAO.getInvoiceGSTTaxDetails(tripTO);
        return gstDetails;
    }

    public ArrayList getConsDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList consDetails = new ArrayList();
        consDetails = tripDAO.getConsDetails(tripTO);
        return consDetails;
    }

    public int consignmentApproveRequest(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = tripDAO.consignmentApproveRequest(tripTO);
        return status;
    }

    public ArrayList consignmentApproval(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consignmentApproval = new ArrayList();
        consignmentApproval = tripDAO.consignmentApproval(tripTO);
        return consignmentApproval;
    }

    public ArrayList viewConsignmentApprovalDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList viewConsignmentApprovalDetails = new ArrayList();
        viewConsignmentApprovalDetails = tripDAO.viewConsignmentApprovalDetails(tripTO);
        return viewConsignmentApprovalDetails;
    }

    public String getConsignVendorDetail(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        String vehicleType = "";
        vehicleType = tripDAO.getConsignVendorDetail(tripTO);
        return vehicleType;
    }

    public int saveConsignmentApproval(String consid, String approvalStatus, String userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveConsignmentApproval(consid, approvalStatus, userId);
        return status;
    }

    public String getCustomereFSId(String custid) throws FPBusinessException, FPRuntimeException {
        String response = "";
        response = tripDAO.getCustomereFSId(custid);
        return response;
    }

    public ArrayList viewVehicleLocation(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList viewVehicleLocation = new ArrayList();
        viewVehicleLocation = tripDAO.viewVehicleLocation(tripTO);
        return viewVehicleLocation;
    }

    public ArrayList getRequestedLHC(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList response = new ArrayList();
        response = tripDAO.getRequestedLHC(tripTO);
        return response;
    }

    public String insertTouchPoint(TripTO tripTO, int userId) throws FPBusinessException, FPRuntimeException {
        String status = "";
        status = tripDAO.insertTouchPoint(tripTO, userId);
        return status;
    }

    public ArrayList getLrNumber(String tripSheetId) throws FPBusinessException, FPRuntimeException {
        ArrayList getLrNumber = new ArrayList();
        getLrNumber = tripDAO.getLrNumber(tripSheetId);
        return getLrNumber;
    }

    public ArrayList getTripInvoiceDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList startDetails = new ArrayList();
        startDetails = tripDAO.getTripInvoiceDetails(tripTO);
        return startDetails;
    }

    public String getlhcContractDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        String getlhcDetails = "";
        getlhcDetails = tripDAO.getlhcContractDetails(tripTO);
        return getlhcDetails;
    }

    public ArrayList getConsignorDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getConsignorDetails = new ArrayList();
        getConsignorDetails = tripDAO.getConsignorDetails(tripTO);
        return getConsignorDetails;
    }

    public String UpdateSaveTripDetails(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        String status = "";
        SqlMapClient session = tripDAO.getSqlMapClient();
        try {
            session.startTransaction();
            status = tripDAO.UpdateSaveTripDetails(tripTO, userId, session);

            if (!status.equals("")) {
                session.commitTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }

    public ArrayList getTripDateDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getTripDateDetails = new ArrayList();
        getTripDateDetails = tripDAO.getTripDateDetails(tripTO);
        return getTripDateDetails;
    }

    public ArrayList getTriptouchpointDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getTripDateDetails = new ArrayList();
        getTripDateDetails = tripDAO.getTriptouchpointDetails(tripTO);
        return getTripDateDetails;
    }

    public String checkDocketNoIsExist(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        String lrno = "";
        lrno = tripDAO.checkDocketNoIsExist(tripTO);
        return lrno;
    }

    public int saveCustomerInvoice(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        SqlMapClient session = tripDAO.getSqlMapClient();
        try {
            session.startTransaction();
            status = tripDAO.saveCustomerInvoice(tripTO, session);
            if (status>0) {
                session.commitTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("am here 7 : " + status);
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }
    
        public int cancelTrip(String tripSheetId, String tripStatusId, String cancelRemarks, int userId) throws FPRuntimeException, FPBusinessException {
        int cancelConsignmentNote = 0;
        cancelConsignmentNote = tripDAO.cancelTrip(tripSheetId, tripStatusId, cancelRemarks, userId);
        return cancelConsignmentNote;
    }
        
        public ArrayList getEcomDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getEcomDetails(tripTO);
        return tripDetails;
    }
        
        public ArrayList getDriverDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList driverDetails = new ArrayList();
        driverDetails = tripDAO.getDriverDetails(tripTO);
        return driverDetails;
    }
        public ArrayList getMovementDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList movementDetails = new ArrayList();
        movementDetails = tripDAO.getMovementDetails(tripTO);
        return movementDetails;
    }
        
          public String getLrNumberEcom(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        String getLrNumber = "";
        getLrNumber = tripDAO.getLrNumberEcom(tripTO);
        return getLrNumber;
    }
    
    
     public int insertEcomPoint(TripTO tripTO, int userId, String lrNumber) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = tripDAO.insertEcomDetails(tripTO, userId, lrNumber);
        return status;
    }
}
