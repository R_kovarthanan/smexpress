/*---------------------------------------------------------------------------
 * ServiceCommand.java
 * Mar 5, 2009
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
-------------------------------------------------------------------------*/
package ets.domain.service.web;

/****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver              Date                      Author                    Change
 * ---------------------------------------------------------------------------
 * 1.0           Mar 3, 2009              vijay			       Created
 *
 ******************************************************************************/
public class ServiceCommand {
           private String[] km=null;
           private String[] month=null;
           private String mfrId="";
           private String modId="";
            private String[] desc=null;
            private String[] kms=null;
            private String[] months=null;
            private String[] descs=null;
            private String[] activeInds=null;
            private String [] selectedindex=null;
            
            private String [] fserviceIds=null;
            private String [] ser=null;
            private String  serviceName="";
            private String  description="";
            private String[]  serviceNames=null;
            private String[] serviceIds=null;
            private String vtypeId="";
            private String secId="";
           private String probId="";
            private String[]  activityName=null;
            private String[]  causeName=null;
            private String[] activityId=null;

    public String[] getActivityId() {
        return activityId;
    }

    public void setActivityId(String[] activityId) {
        this.activityId = activityId;
    }

    public String[] getActivityName() {
        return activityName;
    }

    public void setActivityName(String[] activityName) {
        this.activityName = activityName;
    }
            

    public String getProbId() {
        return probId;
    }

    public void setProbId(String probId) {
        this.probId = probId;
    }

    public String getSecId() {
        return secId;
    }

    public void setSecId(String secId) {
        this.secId = secId;
    }

    public String[] getSer() {
        return ser;
    }

    public void setSer(String[] ser) {
        this.ser = ser;
    }
           

    public String getVtypeId() {
        return vtypeId;
    }

    public void setVtypeId(String vtypeId) {
        this.vtypeId = vtypeId;
    }
            

    public String[] getSelectedindex() {
        return selectedindex;
    }

    public void setSelectedindex(String[] selectedindex) {
        this.selectedindex = selectedindex;
    }

    public String[] getServiceIds() {
        return serviceIds;
    }

    public void setServiceIds(String[] serviceIds) {
        this.serviceIds = serviceIds;
    }

    public String[] getServiceNames() {
        return serviceNames;
    }

    public void setServiceNames(String[] serviceNames) {
        this.serviceNames = serviceNames;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
            

    public String[] getFserviceIds() {
        return fserviceIds;
    }

    public void setFserviceIds(String[] fserviceIds) {
        this.fserviceIds = fserviceIds;
    }
            

    public String[] getActiveInds() {
        return activeInds;
    }

    public void setActiveInds(String[] activeInds) {
        this.activeInds = activeInds;
    }


    public String[] getDescs() {
        return descs;
    }

    public void setDescs(String[] descs) {
        this.descs = descs;
    }
    public String[] getKms() {
        return kms;
    }

    public void setKms(String[] kms) {
        this.kms = kms;
    }

    public String getMfrId() {
        return mfrId;
    }

    public void setMfrId(String mfrId) {
        this.mfrId = mfrId;
    }

    public String getModId() {
        return modId;
    }

    public void setModId(String modId) {
        this.modId = modId;
    }


    public String[] getMonths() {
        return months;
    }

    public void setMonths(String[] months) {
        this.months = months;
    }

    

    public String[] getDesc() {
        return desc;
    }

    public void setDesc(String[] desc) {
        this.desc = desc;
    }

    public String[] getKm() {
        return km;
    }

    public void setKm(String[] km) {
        this.km = km;
    }

    public String[] getMonth() {
        return month;
    }

    public void setMonth(String[] month) {
        this.month = month;
    }

    public String[] getCauseName() {
        return causeName;
    }

    public void setCauseName(String[] causeName) {
        this.causeName = causeName;
    }
            
            

}
