/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.operation.web;

/**
 *
 * @author vijay
 */
public class OperationCommand {
    
    private String newRouteFlag = "";
    private String onwardReturn = "";
    private String onwardTrip = "";
    private String billType = "";
    private String walkinPanNo = "";
    private String walkinGSTNo = "";
    private String walkinConsoignor = "";
    private String walkinConsignee = "";
    
    private String noOfTrucks = "";

    private String[] tatTime = null;
    private String routeConsContractId = "";
    private String consignmentOrderId = "";
    private String contractVehicleTypeId = "";
    private String deliveryService = "";
    private String[] gcnNos = null;
    private String[] consigDate = null;
    private String[] custRefDate = null;
    private String gstNo = "";
    private String panNo = "";
    private String walkinCustomerId = "";
    private String[] consigneeIds = null;
    private String[] consignorIds = null;
    private String[] phoneNos = null;
    //    CBT
     private String airWayBillNo = "";
    private String awbNo = "";
    private String awbType = "";
    private String allowedTrucks = "";
    private String truckNos = "";
    private String onwardReturnType = "";
    
//    private String tripStatusId = "";
    private String totalVolumeActual = "";
    private String allBranchAccess = "";
    private String totalBooking = "";
    private String totalGrossWeight = "";
    private String totalChargeableWeight = "";
    private String awbTotalGrossWeight = "";
    private String awbTotalPackages = "";
    private String awbReceivedPackages = "";
    private String awbPendingPackages = "";
    
        private String wareHouseName = "";
    private String wareHouseLocation = "";
    private String shipmentAcceptanceDate = "";
    private String shipmentAccpHour = "";
    private String shipmentAccpMinute = "";
    private String currencyType = "";
    private String rateMode = "";
    private String[] bookingReferenceRemarks = null;
    private String orderDeliveryHour = "";
    private String orderDeliveryMinute = "";
    private String perKgRate = "";
    private String bookingRemarks = "";

    private String vehicleInfo = "";
    private String vehicleCapacity = "";
    
    private String truckCode = "";
    private String fleetCode = "";
    private String fleetCapacity = "";
    private String fleetVolume = "";
    private String fleetName = "";
    private String fleetId = "";    
    private String truckName = "";

    private String awbOriginId = "";
    private String awbOriginName = "";
    private String awbDestinationName = "";
    private String awbDestinationId = "";
    private String length = "";
    private String breadth = "";
    private String height = "";
    private String chargeAbleWeigth = "";
    private String volume = "";
    
    private String []truckOriginId ;    
    private String []availableCap ;
    private String []assignedCap ;    
    
    
    private String rateMode1="";
    private String rateValue1="";    
    private String departOnTime="";    
    private String truckAvailable="";
    private String truckAssigned="";
    private String consignmentRouteCourseId="";
    private String customsStatus="";
    private String []consArticleId;
    private String []consRouteCourseId;

    private String barCode = "";
      private String airWiseBillNo = "";
      private String loadStatus = "";
      private String loadTime = "";
      private String customerDoc = "";
      private String unloadStatus = "";
      private String unloadTime = "";
      private String podRecieved = "";
      private String awbOriginRegion = "";
      private String orderReferenceEnd = "";
      
      private int parcelNo = 0;

    private String originCityId = "";
    private String consignmentOrder = "";
    private String branchId = "";
    private String []usedVol ;
        private String []usedCapacity ;


        private String reasonId = "";
      private String reasonCode = "";
      private String reasonDescription = "";
      private String truckId = "";
      private String truckCodeId = "";
      private String[] contractFreightTable1 = null;
      
//      sesha
//      private String categoryCode = "";
//    private String categoryName = "";
//    private String sacCode = "";
//    private String sacDescription = "";
//    private String gstCategoryId = "";
//    private String description = "";
//    private String gstCategoryName = "";
//    private String categoryId = "";
//    private String gstCodeMasterId = "";
//    private String gstName = "";
//    private String state = "";
//    private String gstCode = "";
//    private String gstPercentage = "";
//    private String validFrom = "";
//    private String validTo = "";
//    private String gstRateDetailId = "";
//    private String gstCategoryCode = "";
//    private String gstRateId = "";
//    private String hsnName = "";
//    private String hsnCode = "";
//    private String hsnDescription = "";
//    private String gstProductId = "";
//    private String gstType = "";
//    private String gstProductCategoryCode = "";
//    private String stateName = "";
//    private String activeInd = "";
    //new changes on 26-02-2015
    
    private String orderReferenceAwb = "";
    private String[] truckTravelKm = null;
    private String[] truckTravelHour = null;
    private String[] truckTravelMinute = null;
    private String[] fleetTypeId = null;
    private String[] truckDestinationId = null;
    private String[] truckRouteId = null;
    private String rateValue = "";
    private String contract = "";
    private String freightReceipt = "";
    private String[] netUnits = null;
    private String[] netQuantity = null;
    private String awbOrigin = "";
    private String awbDestination = "";
    private String awbOrderDeliveryDate = "";
    private String awbDestinationRegion = "";
    private String awbMovementType = "";
    private String[] noOfPieces = null;
    private String[] grossWeight = null;
    private String[] chargeableWeight = null;
    private String[] chargeableWeightId = null;
    private String[] lengths = null;
    private String[] widths = null;
    private String[] heights = null;
    private String[] dgHandlingCode = null;
    private String[] unIdNo = null; 
    private String totalChargeableWeights = "";
    private String totalVolume = "";
    private String[] availableWeight = null;
    private String[] availableVolume = null;
    private String airlineName = "";
    private String shipmentType = "";
    private String[] truckDepDate = null;
    private String orderReferenceAwbNo = "";
    private String[] vehicleIds = null;
    private String[] vehicleRegNo ;
    private String productRate = "";
    private String[] volumes = null;
    private String commodity = "";
    private String[] classCode = null;
    private String[] pkgInstruction = null;
    private String[] pkgGroup = null;
    private String []receivedPackages ;
//    CBT
//    <GST>
    private String[] consignorNames = null;
    private String[] consignorPhoneNos = null;
    private String[] consignorAddresss = null;
    private String[] consigneeNames = null;
    private String[] consigneePhoneNos = null;
    private String[] consigneeAddresss = null;
    private String categoryCode = "";
    private String categoryName = "";
    private String sacCode = "";
    private String sacDescription = "";
    private String gstCategoryId = "";
    private String description = "";
    private String gstCategoryName = "";
    private String categoryId = "";
    private String gstCodeMasterId = "";
    private String gstName = "";
    private String state = "";
    private String gstCode = "";
    private String gstPercentage = "";
    private String validFrom = "";
    private String validTo = "";
    private String gstRateDetailId = "";
    private String gstCategoryCode = "";
    private String gstRateId = "";
    private String hsnName = "";
    private String hsnCode = "";
    private String hsnDescription = "";
    private String gstProductId = "";
    private String gstType = "";
    private String gstProductCategoryCode = "";
    private String stateName = "";
    private String activeInd = "";
    private String googleCityName = "";
    private String latitude = "";
    private String longitude = "";
//Senthil 10-12-2013
    private String[] routeCostIds = null;
    private String tripType = "";
    private String paidAmount = "";
    private String fuelTypeId = "";
    private String fuelUnite = "";
    private String fuelTypeName = "";
    private String fuelType = "";
    private String paymentType = "";
    private String paymentModeId = "";
    private String paymentModeName = "";
    private String rtgsNo = "";
    private String rtgsRemarks = "";
    private String chequeNo = "";
    private String chequeRemarks = "";
    private String draftNo = "";
    private String draftRemarks = "";
    private String custId = "";
    private String custCode = "";
    private String custName = "";
    private String custType = "";
    private String custContactPerson = "";
    private String custAddress = "";
    private String custCity = "";
    private String custState = "";
    private String custPhone = "";
    private String custMobile = "";
    private String custEmail = "";
    private String accountManagerId = "";
    private String accountManager = "";
    private String creditLimit = "";
    private String creditDays = "";
    private String[] consignmentArticleId = null;
    private String tripclosureid = "";
    private String[] invoiceTripId = null;
    private String fleetCenterId = "";
    //Senthil 10-12-2013
    //Mathan 10-12-2013
    private String productCategoryName = "";
    private String mappingId = "";
    private String batchType = "";
    private String id = "";
    private String reeferMinimumTemperature = "";
    private String reeferMaximumTemperature = "";
    private String parameterName = "";
    private String parameterValue = "";
    private String parameterUnit = "";
    private String parameterDescription = "";
    private String frequencyId = "";
    private String frequencyName = "";
    private String alertName = "";
    private String alertDes = "";
    private String alertBased = "";
    private String alertRaised = "";
    private String alerttoEmail = "";
    private String alertccEmail = "";
    private String alertsemailSub = "";
    private String alertsfrequently = "";
    private String alertRaised1 = "";
    private String escalationtoEmail1 = "";
    private String escalationccEmail1 = "";
    private String escalationemailSub1 = "";
    private String repeat = "";
    private String alertid = "";
    private String alertstatus = "";
    private String checkListName = "";
    private String checkListStage = "";
    private String checkListDate = "";
    private String checkListId = "";
    private String checkListStatus = "";
    private String stage = "";
    private String stageName = "";
    private String StageId = "";
    private String cityName = "";
    private String cityState = "";
    private String zoneid = "";
    private String zoneName = "";
    private String zoneId = "";
    //Mathan 10-12-2013
    private String advicedate = "";
    private String approvestatus = "";
    private String approveremarks = "";
    private String fromDate = "";
    private String toDate = "";
    private String[] selectedValue = null;
    private String expectedArrivalDate = "";
    private String expectedArrivalTime = "";
    private String vehicleid = "";
    private String vehicleId = "";
    private String primaryDriverId = "";
    private String secondaryDriverIdOne = "";
    private String secondaryDriverIdTwo = "";
//    CLPL Trip sheet by ashok start
    private String stageId = "";
    private String tonnage = "";
    private String bags = "";
    private String orderNo = "";
    private String pinkSlip = "";
//    CLPL Trip sheet by ashok end
    // CLPL  trip management
    private String lpsID = "";
    private String lpsNumber = "";
    private String partyName = "";
    private String lpsDate = "";
    private String billStatus = "";
    private String orderNumber = "";
    private String contractor = "";
    private String packerNumber = "";
    private String quantity = "";
    private String route = "";
    private String destination = "";
    private String productName = "";
    private String packing = "";
    private String clplPriority = "";
    private String active_ind = "";
    private String productId = "";
    private String productCode = "";
    private String lorryNo = "";
    private String gatePassNo = "";
    private String watchandward = "";
    private String remarks = "";
    private String date = "";
    private String lpsNo = "";
    private String tripdateP = "";
//    Pink Slip
    private String registerNo = "";
    private String regno = "";
    private String driName = "";
    private String routeId = "";
    private String selectedRouteId = "";
    private String selectedLpsIds = "";
    private String pinkSlipID = "";
    private String routeID = "";
    private String vehicleID = "";
    private String driverID = "";
    private String vehicleTonnage = "";
    private String ownership = "";
    private String selectedBillStatus = "";
    //CLPL Invoice
    private String noOfTrip = "";
    private String taxAmount = "";
    private String invoiceCode = "";
    private String invRefCode = "";
    private String invoiceDate = "";
    private String grandTotal = "";
    private String invoiceStatus = "";
    private String numberOfTri = "";
    private String invoiceFor = "";
    private String invoiceDetailId = "";
    private String tripNo = "";
    private String freightAmount = "";
    private String ledgerName = "";
    private String vehicleNo = "";
    private String vendorName = "";
    private String invoiceId = "";
    //clpl trip edit
    private String GPSKm = "";
    private String returnTripProduct = "";
    private String returnFromLocation = "";
    private String returnToLocation = "";
    private String returnDate = "";
    private String returnTonnage = "";
    private String returnLoadedTonnage = "";
    private String returnLoadedDate = "";
    private String returnDeliveredTonnage = "";
    private String returnDeliveredDate = "";
    private String shortageTonnage = "";
    private String returnAmount = "";
    private String returnExpenses = "";
    //        clpl close trip
    private String tripReceivedAmount = "";
    private String fromLocation = "";
    private String toLocation = "";
    private String returnTripType = "";
    private String returnProductName = "";
    private String outKM = "";
    private String inKM = "";
    private String returnKM = "";
    private String returnTon = "";
    private String fromlocationid = "";
    private String fromlocation = "";
    private String deliveredDANo = "";
    private String loadedSlipNo = "";
    private String deliveredTonnage = "";
    private String voucherNo = "";
    private String vDate = "";
    private String accountDetail = "";
    private String accountEntryID = "";
    private String accountEntryDate = "";
    private String accountsAmount = "";
    private String accountsType = "";
    private String narration = "";
    private String ledgerID = "";
    private String tonnageRateMarket = "";
    private String twoLpsStatus = "";
//    Brattle Foods
    private String routeCode = "";
    private String travelTime = "";
    private String distance = "";
    private String reeferRunning = "";
    private String roadType = "";
    private String tollAmount = "";
    private String fuelCost = "";
    private String[] mfrIds = null;
    private String[] modelIds = null;
    private String[] vehMileage = null;
    private String[] vehExpense = null;
    private String[] reeferConsump = null;
    private String[] reeferExpense = null;
    private String[] varRate = null;
    private String[] varCost = null;
    private String[] totExpense = null;
    private String routeIdFrom = "";
    private String routeIdTo = "";
    private String editRouteId = "";
    private String status = "";
    private String routeNameFrom = "";
    private String routeNameTo = "";
    private String cityFromId = "";
    private String cityToId = "";
    private String travelHour = "";
    private String travelMinute = "";
    private String reeferHour = "";
    private String reeferMinute = "";
    private String tollAmountType = "";
    private String avgTollAmount = "";
    private String avgMisCost = "";
    private String avgDriverIncentive = "";
    private String avgFactor = "";
    private String[] vehTypeId = null;
    private String reeferMileage = "";
    private String[] fuelCostPerKm = null;
    private String[] fuelCostPerHr = null;
    private String[] tollAmounts = null;
    private String[] miscCostKm = null;
    private String[] driverIncenKm = null;
    private String[] factor = null;
    private String[] varExpense = null;
    private String stChargeName = "";
    private String stChargeDesc = "";
    private String stChargeUnit = "";
    private String stChargeId = "";
    private String CustomerId = "";
    private String contractNo = "";
    private String customerCode = "";
    private String customerName = "";
    private String contractFrom = "";
    private String contractTo = "";
    private String billingTypeId = "";
    private String[] stChargeIds = null;
    private String[] amount = null;
    private String[] standardChargeDate = null;
    private String[] standardChargeSelect = null;
    private String[] ptpRouteContractCode = null;
    private String[] ptpVehicleTypeId = null;
    private String[] ptpPickupPointId = null;
    private String[] ptpPickupPoint = null;
    private String[] interimPoint1 = null;
    private String[] interimPoint2 = null;
    private String[] interimPoint3 = null;
    private String[] interimPoint4 = null;
    private String[] ptpDropPoint = null;
    private String[] interimPointId1 = null;
    private String[] interimPointId2 = null;
    private String[] interimPointId3 = null;
    private String[] interimPointId4 = null;
    private String[] ptpDropPointId = null;
    private String[] interimPoint1Km = null;
    private String[] interimPoint2Km = null;
    private String[] interimPoint3Km = null;
    private String[] interimPoint4Km = null;
    private String[] ptpDropPointKm = null;
    private String[] interimPoint1Hrs = null;
    private String[] interimPoint2Hrs = null;
    private String[] interimPoint3Hrs = null;
    private String[] interimPoint4Hrs = null;
    private String[] ptpDropPointHrs = null;
    private String[] interimPoint1Minutes = null;
    private String[] interimPoint2Minutes = null;
    private String[] interimPoint3Minutes = null;
    private String[] interimPoint4Minutes = null;
    private String[] ptpDropPointMinutes = null;
    private String[] interimPoint1RouteId = null;
    private String[] interimPoint2RouteId = null;
    private String[] interimPoint3RouteId = null;
    private String[] interimPoint4RouteId = null;
    private String[] ptpDropPointRouteId = null;
    private String[] pointRouteId = null;
    private String[] ptpTotalKm = null;
    private String[] ptpTotalHours = null;
    private String[] ptpTotalMinutes = null;
    private String[] ptpRateWithReefer = null;
    private String[] ptpRateWithoutReefer = null;
    private String[] ptpwRouteContractCode = null;
    private String[] ptpwVehicleTypeId = null;
    private String[] ptpwPickupPointId = null;
    private String[] ptpwPickupPoint = null;
    private String[] ptpwDropPoint = null;
    private String[] ptpwDropPointId = null;
    private String[] ptpwPointRouteId = null;
    private String[] ptpwTotalKm = null;
    private String[] ptpwTotalHrs = null;
    private String[] ptpwTotalMinutes = null;
    private String[] vehicleTypeId = null;
    private String[] ptpwRateWithReefer = null;
    private String[] ptpwRateWithoutReefer = null;
    private String[] actualKmVehicleTypeId = null;
    private String[] vehicleRsPerKm = null;
    private String[] reeferRsPerHour = null;
    private String[] actualKmSelect = null;
    private String contractId = "";
    private String[] fuelCostPerKms = null;
    private String[] fuelCostPerHrs = null;
    private String[] reefMileage = null;
    private String fuelCostKm = "";
    private String fuelCostHr = "";
    private String tollAmountperkm = "";
    private String miscCostperkm = "";
    private String driverIncentperkm = "";
    private String variExpense = "";
    private String vehiExpense = "";
    private String reefeExpense = "";
    private String totaExpense = "";
    private String effectivDate = "";
    private String fuelPrice = "";
    private String cityId = "";
    private String effectiveDate = "";
    private String endDate = "";
    private String lastPrice = "";
    private String priceDiff = "";
    /* Consignment Note */
    private String customerTypeId = "0";
    private String entryType = "";
    private String consignmentNoteNo = "";
    private String consignmentDate = "";
    private String orderReferenceNo = "";
    private String orderReferenceRemarks = "";
    private String productCategoryId = "0";
    private String customerAddress = "";
    private String pincode = "";
    private String customerMobileNo = "";
    private String mailId = "";
    private String customerPhoneNo = "";
    private String origin = "";
    private String businessType = "0";
    private String multiPickup = "";
    private String multiDelivery = "";
    private String consignmentOrderInstruction = "";
    private String serviceType = "0";
    private String reeferRequired = "";
    private String contractRateId = "0";
    private String vehicleRequiredDate = "";
    private String vehicleRequiredHour = "";
    private String vehicleRequiredMinute = "";
    private String vehicleInstruction = "";
    private String consignorName = "";
    private String consignorPhoneNo = "";
    private String consignorAddress = "";
    private String consigneeName = "";
    private String consigneePhoneNo = "";
    private String consigneeAddress = "";
    private String rateWithReefer = "";
    private String rateWithoutReefer = "";
    private String totalPackage = "";
    private String totalWeightage = "";
    private String totalHours = "";
    private String totalMinutes = "";
    private String totFreightAmount = "";
    private String subTotal = "";
    private String totalCharges = "";
    private String routeContractId = "0";
    private String totalKm = "";
    private String docCharges = "";
    private String odaCharges = "";
    private String multiPickupCharge = "";
    private String multiDeliveryCharge = "";
    private String handleCharges = "";
    private String otherCharges = "";
    private String unloadingCharges = "";
    private String loadingCharges = "";
    private String standardChargeRemarks = "";
    private String[] productCodes = null;
    private String[] productNames = null;
    private String[] packagesNos = null;
    private String[] weights = null;
    private String[] batchCode = null;
    private String[] uom = null;
    private String walkinCustomerName = "";
    private String walkinCustomerCode = "";
    private String walkinCustomerAddress = "";
    private String walkinPincode = "";
    private String walkinCustomerMobileNo = "";
    private String walkinMailId = "";
    private String walkinCustomerPhoneNo = "";
    private String walkInBillingTypeId = "";
    private String walkinFreightWithReefer = "";
    private String walkinFreightWithoutReefer = "";
    private String walkinRateWithReeferPerKg = "";
    private String walkinRateWithoutReeferPerKg = "";
    private String walkinRateWithReeferPerKm = "";
    private String walkinRateWithoutReeferPerKm = "";
    private String destinationId = "";
    private String[] pointId = null;
    private String[] pointName = null;
    private String[] pointType = null;
    private String[] order = null;
    private String[] pointOrder = null;
    private String[] pointAddresss = null;
    private String[] deliveryAddress = null;
    private String[] weightKG = null;
    private String[] grossWG = null;
    private String[] invoicedNo = null;
    private String[] multiplePointRouteId = null;
    private String[] pointPlanDate = null;
    private String[] pointPlanHour = null;
    private String[] pointPlanMinute = null;
    private String endPointId = "";
    private String finalRouteId = "";
    private String endPointName = "";
    private String endPointType = "";
    private String endOrder = "";
    private String endPointAddresss = "";
    private String endPointPlanDate = "";
    private String endPointPlanHour = "";
    private String endPointPlanMinute = "";
    private String advancerequestamt = "";
    private String requeststatus = "";
    private String requeston = "";
    private String requestby = "";
    private String requestremarks = "";
    private String tripcode = "";
    private String tripid = "";
    private String estimatedtransitday = "";
    private String estimatedrevenue = "";
    private String estimatedadvanceperday = "";
    private String tripday = "";
    private String paidamt = "";
    private String paidstatus = "";
    private String tobepaidtoday = "";
    private String estimatedexpense = "";
    //raju
    private String orderType = "";
    private String gcnNo = "";
    private String customerReference = "";
    private String editFreightAmount = " ";
//    17-11-2017
    private String[] minWgtBase = null;
    private String minimumBaseWgt = "";    
    private String vendoreFSId = "";
    private String cityCode = "";
    
    private String customerPincode = "";
    private String firstPickupId = "";
    private String firstPickupName = "";
    private String point1Id = "";
    private String point1Name = "";
    private String finalPointId = "";
    private String finalPointName = "";
    private String vehicleTypeName = "";
    private String escalationPercent = "";
    private String escalationPrice = "";
    private String vehicleTypeIds = "";
    private String pallets = "";
    private String consignorId = "";
    private String consigneeId = "";
    private String[] movementTypeId =null;

    public String[] getMovementTypeId() {
        return movementTypeId;
    }

    public void setMovementTypeId(String[] movementTypeId) {
        this.movementTypeId = movementTypeId;
    }
    
    

    
    
    public String getCustomerPincode() {
        return customerPincode;
    }

    public void setCustomerPincode(String customerPincode) {
        this.customerPincode = customerPincode;
    }

    public String getFirstPickupId() {
        return firstPickupId;
    }

    public void setFirstPickupId(String firstPickupId) {
        this.firstPickupId = firstPickupId;
    }

    public String getFirstPickupName() {
        return firstPickupName;
    }

    public void setFirstPickupName(String firstPickupName) {
        this.firstPickupName = firstPickupName;
    }

    public String getPoint1Id() {
        return point1Id;
    }

    public void setPoint1Id(String point1Id) {
        this.point1Id = point1Id;
    }

    public String getPoint1Name() {
        return point1Name;
    }

    public void setPoint1Name(String point1Name) {
        this.point1Name = point1Name;
    }

    public String getFinalPointId() {
        return finalPointId;
    }

    public void setFinalPointId(String finalPointId) {
        this.finalPointId = finalPointId;
    }

    public String getFinalPointName() {
        return finalPointName;
    }

    public void setFinalPointName(String finalPointName) {
        this.finalPointName = finalPointName;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public String getEscalationPercent() {
        return escalationPercent;
    }

    public void setEscalationPercent(String escalationPercent) {
        this.escalationPercent = escalationPercent;
    }

    public String getEscalationPrice() {
        return escalationPrice;
    }

    public void setEscalationPrice(String escalationPrice) {
        this.escalationPrice = escalationPrice;
    }

    public String getVehicleTypeIds() {
        return vehicleTypeIds;
    }

    public void setVehicleTypeIds(String vehicleTypeIds) {
        this.vehicleTypeIds = vehicleTypeIds;
    }

    public String getPallets() {
        return pallets;
    }

    public void setPallets(String pallets) {
        this.pallets = pallets;
    }

    public String getConsignorId() {
        return consignorId;
    }

    public void setConsignorId(String consignorId) {
        this.consignorId = consignorId;
    }

    public String getConsigneeId() {
        return consigneeId;
    }

    public void setConsigneeId(String consigneeId) {
        this.consigneeId = consigneeId;
    }
    
    

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }    
    

    public String getVendoreFSId() {
        return vendoreFSId;
    }

    public void setVendoreFSId(String vendoreFSId) {
        this.vendoreFSId = vendoreFSId;
    }
    

    public String getAdvancerequestamt() {
        return advancerequestamt;
    }

    public void setAdvancerequestamt(String advancerequestamt) {
        this.advancerequestamt = advancerequestamt;
    }

    public String getRequestby() {
        return requestby;
    }

    public void setRequestby(String requestby) {
        this.requestby = requestby;
    }

    public String getRequeston() {
        return requeston;
    }

    public void setRequeston(String requeston) {
        this.requeston = requeston;
    }

    public String getRequestremarks() {
        return requestremarks;
    }

    public void setRequestremarks(String requestremarks) {
        this.requestremarks = requestremarks;
    }

    public String getRequeststatus() {
        return requeststatus;
    }

    public void setRequeststatus(String requeststatus) {
        this.requeststatus = requeststatus;
    }


    /* Consignment Note */
    //  Brattle Foods
    public String getEstimatedexpense() {
        return estimatedexpense;
    }

    public void setEstimatedexpense(String estimatedexpense) {
        this.estimatedexpense = estimatedexpense;
    }

    public String getTobepaidtoday() {
        return tobepaidtoday;
    }

    public void setTobepaidtoday(String tobepaidtoday) {
        this.tobepaidtoday = tobepaidtoday;
    }

    public String getPaidamt() {
        return paidamt;
    }

    public void setPaidamt(String paidamt) {
        this.paidamt = paidamt;
    }

    public String getPaidstatus() {
        return paidstatus;
    }

    public void setPaidstatus(String paidstatus) {
        this.paidstatus = paidstatus;
    }

    public String getTripday() {
        return tripday;
    }

    public void setTripday(String tripday) {
        this.tripday = tripday;
    }

    public String getTripid() {
        return tripid;
    }

    public void setTripid(String tripid) {
        this.tripid = tripid;
    }

    public String getEstimatedadvanceperday() {
        return estimatedadvanceperday;
    }

    public void setEstimatedadvanceperday(String estimatedadvanceperday) {
        this.estimatedadvanceperday = estimatedadvanceperday;
    }

    public String getEstimatedrevenue() {
        return estimatedrevenue;
    }

    public void setEstimatedrevenue(String estimatedrevenue) {
        this.estimatedrevenue = estimatedrevenue;
    }

    public String getEstimatedtransitday() {
        return estimatedtransitday;
    }

    public void setEstimatedtransitday(String estimatedtransitday) {
        this.estimatedtransitday = estimatedtransitday;
    }

    public String getTripcode() {
        return tripcode;
    }

    public void setTripcode(String tripcode) {
        this.tripcode = tripcode;
    }

    /* Consignment Note */
//    Brattle Foods
    public String getBags() {
        return bags;
    }

    public void setBags(String bags) {
        this.bags = bags;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getPinkSlip() {
        return pinkSlip;
    }

    public void setPinkSlip(String pinkSlip) {
        this.pinkSlip = pinkSlip;
    }

    public String getStageId() {
        return stageId;
    }

    public void setStageId(String stageId) {
        this.stageId = stageId;
    }

    public String getTonnage() {
        return tonnage;
    }

    public void setTonnage(String tonnage) {
        this.tonnage = tonnage;
    }

    public String getActive_ind() {
        return active_ind;
    }

    public void setActive_ind(String active_ind) {
        this.active_ind = active_ind;
    }

    public String getClplPriority() {
        return clplPriority;
    }

    public void setClplPriority(String clplPriority) {
        this.clplPriority = clplPriority;
    }

    public String getContractor() {
        return contractor;
    }

    public void setContractor(String contractor) {
        this.contractor = contractor;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getGatePassNo() {
        return gatePassNo;
    }

    public void setGatePassNo(String gatePassNo) {
        this.gatePassNo = gatePassNo;
    }

    public String getLorryNo() {
        return lorryNo;
    }

    public void setLorryNo(String lorryNo) {
        this.lorryNo = lorryNo;
    }

    public String getLpsDate() {
        return lpsDate;
    }

    public void setLpsDate(String lpsDate) {
        this.lpsDate = lpsDate;
    }

    public String getLpsID() {
        return lpsID;
    }

    public void setLpsID(String lpsID) {
        this.lpsID = lpsID;
    }

    public String getLpsNo() {
        return lpsNo;
    }

    public void setLpsNo(String lpsNo) {
        this.lpsNo = lpsNo;
    }

    public String getLpsNumber() {
        return lpsNumber;
    }

    public void setLpsNumber(String lpsNumber) {
        this.lpsNumber = lpsNumber;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getPackerNumber() {
        return packerNumber;
    }

    public void setPackerNumber(String packerNumber) {
        this.packerNumber = packerNumber;
    }

    public String getPacking() {
        return packing;
    }

    public void setPacking(String packing) {
        this.packing = packing;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getWatchandward() {
        return watchandward;
    }

    public void setWatchandward(String watchandward) {
        this.watchandward = watchandward;
    }

    public String getDriName() {
        return driName;
    }

    public void setDriName(String driName) {
        this.driName = driName;
    }

    public String getDriverID() {
        return driverID;
    }

    public void setDriverID(String driverID) {
        this.driverID = driverID;
    }

    public String getOwnership() {
        return ownership;
    }

    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    public String getPinkSlipID() {
        return pinkSlipID;
    }

    public void setPinkSlipID(String pinkSlipID) {
        this.pinkSlipID = pinkSlipID;
    }

    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getRouteID() {
        return routeID;
    }

    public void setRouteID(String routeID) {
        this.routeID = routeID;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getSelectedLpsIds() {
        return selectedLpsIds;
    }

    public void setSelectedLpsIds(String selectedLpsIds) {
        this.selectedLpsIds = selectedLpsIds;
    }

    public String getVehicleID() {
        return vehicleID;
    }

    public void setVehicleID(String vehicleID) {
        this.vehicleID = vehicleID;
    }

    public String getVehicleTonnage() {
        return vehicleTonnage;
    }

    public void setVehicleTonnage(String vehicleTonnage) {
        this.vehicleTonnage = vehicleTonnage;
    }

    public String getNoOfTrip() {
        return noOfTrip;
    }

    public void setNoOfTrip(String noOfTrip) {
        this.noOfTrip = noOfTrip;
    }

    public String getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getInvRefCode() {
        return invRefCode;
    }

    public void setInvRefCode(String invRefCode) {
        this.invRefCode = invRefCode;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInvoiceFor() {
        return invoiceFor;
    }

    public void setInvoiceFor(String invoiceFor) {
        this.invoiceFor = invoiceFor;
    }

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public String getNumberOfTri() {
        return numberOfTri;
    }

    public void setNumberOfTri(String numberOfTri) {
        this.numberOfTri = numberOfTri;
    }

    public String getFreightAmount() {
        return freightAmount;
    }

    public void setFreightAmount(String freightAmount) {
        this.freightAmount = freightAmount;
    }

    public String getInvoiceDetailId() {
        return invoiceDetailId;
    }

    public void setInvoiceDetailId(String invoiceDetailId) {
        this.invoiceDetailId = invoiceDetailId;
    }

    public String getLedgerName() {
        return ledgerName;
    }

    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public String getTripNo() {
        return tripNo;
    }

    public void setTripNo(String tripNo) {
        this.tripNo = tripNo;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getGPSKm() {
        return GPSKm;
    }

    public void setGPSKm(String GPSKm) {
        this.GPSKm = GPSKm;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getReturnFromLocation() {
        return returnFromLocation;
    }

    public void setReturnFromLocation(String returnFromLocation) {
        this.returnFromLocation = returnFromLocation;
    }

    public String getReturnToLocation() {
        return returnToLocation;
    }

    public void setReturnToLocation(String returnToLocation) {
        this.returnToLocation = returnToLocation;
    }

    public String getReturnTonnage() {
        return returnTonnage;
    }

    public void setReturnTonnage(String returnTonnage) {
        this.returnTonnage = returnTonnage;
    }

    public String getReturnTripProduct() {
        return returnTripProduct;
    }

    public void setReturnTripProduct(String returnTripProduct) {
        this.returnTripProduct = returnTripProduct;
    }

    public String getReturnAmount() {
        return returnAmount;
    }

    public void setReturnAmount(String returnAmount) {
        this.returnAmount = returnAmount;
    }

    public String getReturnExpenses() {
        return returnExpenses;
    }

    public void setReturnExpenses(String returnExpenses) {
        this.returnExpenses = returnExpenses;
    }

    public String getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }

    public String getSelectedBillStatus() {
        return selectedBillStatus;
    }

    public void setSelectedBillStatus(String selectedBillStatus) {
        this.selectedBillStatus = selectedBillStatus;
    }

    public String getSelectedRouteId() {
        return selectedRouteId;
    }

    public void setSelectedRouteId(String selectedRouteId) {
        this.selectedRouteId = selectedRouteId;
    }

    public String getTripReceivedAmount() {
        return tripReceivedAmount;
    }

    public void setTripReceivedAmount(String tripReceivedAmount) {
        this.tripReceivedAmount = tripReceivedAmount;
    }

    public String getReturnDeliveredDate() {
        return returnDeliveredDate;
    }

    public void setReturnDeliveredDate(String returnDeliveredDate) {
        this.returnDeliveredDate = returnDeliveredDate;
    }

    public String getReturnDeliveredTonnage() {
        return returnDeliveredTonnage;
    }

    public void setReturnDeliveredTonnage(String returnDeliveredTonnage) {
        this.returnDeliveredTonnage = returnDeliveredTonnage;
    }

    public String getReturnLoadedDate() {
        return returnLoadedDate;
    }

    public void setReturnLoadedDate(String returnLoadedDate) {
        this.returnLoadedDate = returnLoadedDate;
    }

    public String getReturnLoadedTonnage() {
        return returnLoadedTonnage;
    }

    public void setReturnLoadedTonnage(String returnLoadedTonnage) {
        this.returnLoadedTonnage = returnLoadedTonnage;
    }

    public String getShortageTonnage() {
        return shortageTonnage;
    }

    public void setShortageTonnage(String shortageTonnage) {
        this.shortageTonnage = shortageTonnage;
    }

    public String getRegisterNo() {
        return registerNo;
    }

    public void setRegisterNo(String registerNo) {
        this.registerNo = registerNo;
    }

    public String getTripdateP() {
        return tripdateP;
    }

    public void setTripdateP(String tripdateP) {
        this.tripdateP = tripdateP;
    }

    public String getFromLocation() {
        return fromLocation;
    }

    public void setFromLocation(String fromLocation) {
        this.fromLocation = fromLocation;
    }

    public String getInKM() {
        return inKM;
    }

    public void setInKM(String inKM) {
        this.inKM = inKM;
    }

    public String getOutKM() {
        return outKM;
    }

    public void setOutKM(String outKM) {
        this.outKM = outKM;
    }

    public String getReturnKM() {
        return returnKM;
    }

    public void setReturnKM(String returnKM) {
        this.returnKM = returnKM;
    }

    public String getReturnProductName() {
        return returnProductName;
    }

    public void setReturnProductName(String returnProductName) {
        this.returnProductName = returnProductName;
    }

    public String getReturnTon() {
        return returnTon;
    }

    public void setReturnTon(String returnTon) {
        this.returnTon = returnTon;
    }

    public String getReturnTripType() {
        return returnTripType;
    }

    public void setReturnTripType(String returnTripType) {
        this.returnTripType = returnTripType;
    }

    public String getToLocation() {
        return toLocation;
    }

    public void setToLocation(String toLocation) {
        this.toLocation = toLocation;
    }

    public String getFromlocation() {
        return fromlocation;
    }

    public void setFromlocation(String fromlocation) {
        this.fromlocation = fromlocation;
    }

    public String getFromlocationid() {
        return fromlocationid;
    }

    public void setFromlocationid(String fromlocationid) {
        this.fromlocationid = fromlocationid;
    }

    public String getDeliveredDANo() {
        return deliveredDANo;
    }

    public void setDeliveredDANo(String deliveredDANo) {
        this.deliveredDANo = deliveredDANo;
    }

    public String getDeliveredTonnage() {
        return deliveredTonnage;
    }

    public void setDeliveredTonnage(String deliveredTonnage) {
        this.deliveredTonnage = deliveredTonnage;
    }

    public String getLoadedSlipNo() {
        return loadedSlipNo;
    }

    public void setLoadedSlipNo(String loadedSlipNo) {
        this.loadedSlipNo = loadedSlipNo;
    }

    public String getAccountDetail() {
        return accountDetail;
    }

    public void setAccountDetail(String accountDetail) {
        this.accountDetail = accountDetail;
    }

    public String getAccountEntryDate() {
        return accountEntryDate;
    }

    public void setAccountEntryDate(String accountEntryDate) {
        this.accountEntryDate = accountEntryDate;
    }

    public String getAccountEntryID() {
        return accountEntryID;
    }

    public void setAccountEntryID(String accountEntryID) {
        this.accountEntryID = accountEntryID;
    }

    public String getLedgerID() {
        return ledgerID;
    }

    public void setLedgerID(String ledgerID) {
        this.ledgerID = ledgerID;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public String getvDate() {
        return vDate;
    }

    public void setvDate(String vDate) {
        this.vDate = vDate;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    public String getAccountsAmount() {
        return accountsAmount;
    }

    public void setAccountsAmount(String accountsAmount) {
        this.accountsAmount = accountsAmount;
    }

    public String getAccountsType() {
        return accountsType;
    }

    public void setAccountsType(String accountsType) {
        this.accountsType = accountsType;
    }

    public String getTonnageRateMarket() {
        return tonnageRateMarket;
    }

    public void setTonnageRateMarket(String tonnageRateMarket) {
        this.tonnageRateMarket = tonnageRateMarket;
    }

    public String getTwoLpsStatus() {
        return twoLpsStatus;
    }

    public void setTwoLpsStatus(String twoLpsStatus) {
        this.twoLpsStatus = twoLpsStatus;
    }

    public String getRouteCode() {
        return routeCode;
    }

    public void setRouteCode(String routeCode) {
        this.routeCode = routeCode;
    }

    public String getTravelTime() {
        return travelTime;
    }

    public void setTravelTime(String travelTime) {
        this.travelTime = travelTime;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getReeferRunning() {
        return reeferRunning;
    }

    public void setReeferRunning(String reeferRunning) {
        this.reeferRunning = reeferRunning;
    }

    public String getTollAmount() {
        return tollAmount;
    }

    public void setTollAmount(String tollAmount) {
        this.tollAmount = tollAmount;
    }

    public String getFuelCost() {
        return fuelCost;
    }

    public void setFuelCost(String fuelCost) {
        this.fuelCost = fuelCost;
    }

    public String[] getMfrIds() {
        return mfrIds;
    }

    public void setMfrIds(String[] mfrIds) {
        this.mfrIds = mfrIds;
    }

    public String[] getModelIds() {
        return modelIds;
    }

    public void setModelIds(String[] modelIds) {
        this.modelIds = modelIds;
    }

    public String[] getVehMileage() {
        return vehMileage;
    }

    public void setVehMileage(String[] vehMileage) {
        this.vehMileage = vehMileage;
    }

    public String[] getVehExpense() {
        return vehExpense;
    }

    public void setVehExpense(String[] vehExpense) {
        this.vehExpense = vehExpense;
    }

    public String[] getReeferConsump() {
        return reeferConsump;
    }

    public void setReeferConsump(String[] reeferConsump) {
        this.reeferConsump = reeferConsump;
    }

    public String[] getReeferExpense() {
        return reeferExpense;
    }

    public void setReeferExpense(String[] reeferExpense) {
        this.reeferExpense = reeferExpense;
    }

    public String[] getVarRate() {
        return varRate;
    }

    public void setVarRate(String[] varRate) {
        this.varRate = varRate;
    }

    public String[] getVarCost() {
        return varCost;
    }

    public void setVarCost(String[] varCost) {
        this.varCost = varCost;
    }

    public String[] getTotExpense() {
        return totExpense;
    }

    public void setTotExpense(String[] totExpense) {
        this.totExpense = totExpense;
    }

    public String getRoadType() {
        return roadType;
    }

    public void setRoadType(String roadType) {
        this.roadType = roadType;
    }

    public String getRouteIdFrom() {
        return routeIdFrom;
    }

    public void setRouteIdFrom(String routeIdFrom) {
        this.routeIdFrom = routeIdFrom;
    }

    public String getRouteIdTo() {
        return routeIdTo;
    }

    public void setRouteIdTo(String routeIdTo) {
        this.routeIdTo = routeIdTo;
    }

    public String getEditRouteId() {
        return editRouteId;
    }

    public void setEditRouteId(String editRouteId) {
        this.editRouteId = editRouteId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRouteNameFrom() {
        return routeNameFrom;
    }

    public void setRouteNameFrom(String routeNameFrom) {
        this.routeNameFrom = routeNameFrom;
    }

    public String getRouteNameTo() {
        return routeNameTo;
    }

    public void setRouteNameTo(String routeNameTo) {
        this.routeNameTo = routeNameTo;
    }

    public String getStChargeName() {
        return stChargeName;
    }

    public void setStChargeName(String stChargeName) {
        this.stChargeName = stChargeName;
    }

    public String getStChargeDesc() {
        return stChargeDesc;
    }

    public void setStChargeDesc(String stChargeDesc) {
        this.stChargeDesc = stChargeDesc;
    }

    public String getStChargeUnit() {
        return stChargeUnit;
    }

    public void setStChargeUnit(String stChargeUnit) {
        this.stChargeUnit = stChargeUnit;
    }

    public String getStChargeId() {
        return stChargeId;
    }

    public void setStChargeId(String stChargeId) {
        this.stChargeId = stChargeId;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getContractFrom() {
        return contractFrom;
    }

    public void setContractFrom(String contractFrom) {
        this.contractFrom = contractFrom;
    }

    public String getContractTo() {
        return contractTo;
    }

    public void setContractTo(String contractTo) {
        this.contractTo = contractTo;
    }

    public String getBillingTypeId() {
        return billingTypeId;
    }

    public void setBillingTypeId(String billingTypeId) {
        this.billingTypeId = billingTypeId;
    }

    public String[] getStChargeIds() {
        return stChargeIds;
    }

    public void setStChargeIds(String[] stChargeIds) {
        this.stChargeIds = stChargeIds;
    }

    public String[] getAmount() {
        return amount;
    }

    public void setAmount(String[] amount) {
        this.amount = amount;
    }

    public String[] getStandardChargeDate() {
        return standardChargeDate;
    }

    public void setStandardChargeDate(String[] standardChargeDate) {
        this.standardChargeDate = standardChargeDate;
    }

    public String[] getStandardChargeSelect() {
        return standardChargeSelect;
    }

    public void setStandardChargeSelect(String[] standardChargeSelect) {
        this.standardChargeSelect = standardChargeSelect;
    }

    public String[] getPtpRouteContractCode() {
        return ptpRouteContractCode;
    }

    public void setPtpRouteContractCode(String[] ptpRouteContractCode) {
        this.ptpRouteContractCode = ptpRouteContractCode;
    }

    public String[] getPtpPickupPoint() {
        return ptpPickupPoint;
    }

    public void setPtpPickupPoint(String[] ptpPickupPoint) {
        this.ptpPickupPoint = ptpPickupPoint;
    }

    public String[] getInterimPoint1() {
        return interimPoint1;
    }

    public void setInterimPoint1(String[] interimPoint1) {
        this.interimPoint1 = interimPoint1;
    }

    public String[] getInterimPointId1() {
        return interimPointId1;
    }

    public void setInterimPointId1(String[] interimPointId1) {
        this.interimPointId1 = interimPointId1;
    }

    public String[] getInterimPoint1Km() {
        return interimPoint1Km;
    }

    public void setInterimPoint1Km(String[] interimPoint1Km) {
        this.interimPoint1Km = interimPoint1Km;
    }

    public String[] getInterimPoint2() {
        return interimPoint2;
    }

    public void setInterimPoint2(String[] interimPoint2) {
        this.interimPoint2 = interimPoint2;
    }

    public String[] getInterimPointId2() {
        return interimPointId2;
    }

    public void setInterimPointId2(String[] interimPointId2) {
        this.interimPointId2 = interimPointId2;
    }

    public String[] getInterimPoint2Km() {
        return interimPoint2Km;
    }

    public void setInterimPoint2Km(String[] interimPoint2Km) {
        this.interimPoint2Km = interimPoint2Km;
    }

    public String[] getInterimPoint3() {
        return interimPoint3;
    }

    public void setInterimPoint3(String[] interimPoint3) {
        this.interimPoint3 = interimPoint3;
    }

    public String[] getInterimPointId3() {
        return interimPointId3;
    }

    public void setInterimPointId3(String[] interimPointId3) {
        this.interimPointId3 = interimPointId3;
    }

    public String[] getInterimPoint3Km() {
        return interimPoint3Km;
    }

    public void setInterimPoint3Km(String[] interimPoint3Km) {
        this.interimPoint3Km = interimPoint3Km;
    }

    public String[] getInterimPoint4() {
        return interimPoint4;
    }

    public void setInterimPoint4(String[] interimPoint4) {
        this.interimPoint4 = interimPoint4;
    }

    public String[] getInterimPointId4() {
        return interimPointId4;
    }

    public void setInterimPointId4(String[] interimPointId4) {
        this.interimPointId4 = interimPointId4;
    }

    public String[] getInterimPoint4Km() {
        return interimPoint4Km;
    }

    public void setInterimPoint4Km(String[] interimPoint4Km) {
        this.interimPoint4Km = interimPoint4Km;
    }

    public String[] getPtpDropPoint() {
        return ptpDropPoint;
    }

    public void setPtpDropPoint(String[] ptpDropPoint) {
        this.ptpDropPoint = ptpDropPoint;
    }

    public String[] getPtpDropPointId() {
        return ptpDropPointId;
    }

    public void setPtpDropPointId(String[] ptpDropPointId) {
        this.ptpDropPointId = ptpDropPointId;
    }

    public String[] getPtpDropPointKm() {
        return ptpDropPointKm;
    }

    public void setPtpDropPointKm(String[] ptpDropPointKm) {
        this.ptpDropPointKm = ptpDropPointKm;
    }

    public String[] getPtpTotalKm() {
        return ptpTotalKm;
    }

    public void setPtpTotalKm(String[] ptpTotalKm) {
        this.ptpTotalKm = ptpTotalKm;
    }

    public String[] getPtpwRouteContractCode() {
        return ptpwRouteContractCode;
    }

    public void setPtpwRouteContractCode(String[] ptpwRouteContractCode) {
        this.ptpwRouteContractCode = ptpwRouteContractCode;
    }

    public String[] getPtpwPickupPoint() {
        return ptpwPickupPoint;
    }

    public void setPtpwPickupPoint(String[] ptpwPickupPoint) {
        this.ptpwPickupPoint = ptpwPickupPoint;
    }

    public String[] getPtpwDropPoint() {
        return ptpwDropPoint;
    }

    public void setPtpwDropPoint(String[] ptpwDropPoint) {
        this.ptpwDropPoint = ptpwDropPoint;
    }

    public String[] getPtpwTotalKm() {
        return ptpwTotalKm;
    }

    public void setPtpwTotalKm(String[] ptpwTotalKm) {
        this.ptpwTotalKm = ptpwTotalKm;
    }

    public String[] getActualKmVehicleTypeId() {
        return actualKmVehicleTypeId;
    }

    public void setActualKmVehicleTypeId(String[] actualKmVehicleTypeId) {
        this.actualKmVehicleTypeId = actualKmVehicleTypeId;
    }

    public String[] getVehicleRsPerKm() {
        return vehicleRsPerKm;
    }

    public void setVehicleRsPerKm(String[] vehicleRsPerKm) {
        this.vehicleRsPerKm = vehicleRsPerKm;
    }

    public String[] getReeferRsPerHour() {
        return reeferRsPerHour;
    }

    public void setReeferRsPerHour(String[] reeferRsPerHour) {
        this.reeferRsPerHour = reeferRsPerHour;
    }

    public String[] getActualKmSelect() {
        return actualKmSelect;
    }

    public void setActualKmSelect(String[] actualKmSelect) {
        this.actualKmSelect = actualKmSelect;
    }

    public String getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(String CustomerId) {
        this.CustomerId = CustomerId;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getFuelPrice() {
        return fuelPrice;
    }

    public void setFuelPrice(String fuelPrice) {
        this.fuelPrice = fuelPrice;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getLastPrice() {
        return lastPrice;
    }

    public void setLastPrice(String lastPrice) {
        this.lastPrice = lastPrice;
    }

    public String getPriceDiff() {
        return priceDiff;
    }

    public void setPriceDiff(String priceDiff) {
        this.priceDiff = priceDiff;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String[] getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String[] vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String[] getInterimPoint1Hrs() {
        return interimPoint1Hrs;
    }

    public void setInterimPoint1Hrs(String[] interimPoint1Hrs) {
        this.interimPoint1Hrs = interimPoint1Hrs;
    }

    public String[] getInterimPoint2Hrs() {
        return interimPoint2Hrs;
    }

    public void setInterimPoint2Hrs(String[] interimPoint2Hrs) {
        this.interimPoint2Hrs = interimPoint2Hrs;
    }

    public String[] getInterimPoint3Hrs() {
        return interimPoint3Hrs;
    }

    public void setInterimPoint3Hrs(String[] interimPoint3Hrs) {
        this.interimPoint3Hrs = interimPoint3Hrs;
    }

    public String[] getInterimPoint4Hrs() {
        return interimPoint4Hrs;
    }

    public void setInterimPoint4Hrs(String[] interimPoint4Hrs) {
        this.interimPoint4Hrs = interimPoint4Hrs;
    }

    public String[] getPtpDropPointHrs() {
        return ptpDropPointHrs;
    }

    public void setPtpDropPointHrs(String[] ptpDropPointHrs) {
        this.ptpDropPointHrs = ptpDropPointHrs;
    }

    public String[] getInterimPoint1RouteId() {
        return interimPoint1RouteId;
    }

    public void setInterimPoint1RouteId(String[] interimPoint1RouteId) {
        this.interimPoint1RouteId = interimPoint1RouteId;
    }

    public String[] getInterimPoint2RouteId() {
        return interimPoint2RouteId;
    }

    public void setInterimPoint2RouteId(String[] interimPoint2RouteId) {
        this.interimPoint2RouteId = interimPoint2RouteId;
    }

    public String[] getInterimPoint3RouteId() {
        return interimPoint3RouteId;
    }

    public void setInterimPoint3RouteId(String[] interimPoint3RouteId) {
        this.interimPoint3RouteId = interimPoint3RouteId;
    }

    public String[] getInterimPoint4RouteId() {
        return interimPoint4RouteId;
    }

    public void setInterimPoint4RouteId(String[] interimPoint4RouteId) {
        this.interimPoint4RouteId = interimPoint4RouteId;
    }

    public String[] getPtpDropPointRouteId() {
        return ptpDropPointRouteId;
    }

    public void setPtpDropPointRouteId(String[] ptpDropPointRouteId) {
        this.ptpDropPointRouteId = ptpDropPointRouteId;
    }

    public String[] getPtpPickupPointId() {
        return ptpPickupPointId;
    }

    public void setPtpPickupPointId(String[] ptpPickupPointId) {
        this.ptpPickupPointId = ptpPickupPointId;
    }

    public String[] getPtpwPickupPointId() {
        return ptpwPickupPointId;
    }

    public void setPtpwPickupPointId(String[] ptpwPickupPointId) {
        this.ptpwPickupPointId = ptpwPickupPointId;
    }

    public String[] getPtpwDropPointId() {
        return ptpwDropPointId;
    }

    public void setPtpwDropPointId(String[] ptpwDropPointId) {
        this.ptpwDropPointId = ptpwDropPointId;
    }

    public String[] getPtpwPointRouteId() {
        return ptpwPointRouteId;
    }

    public void setPtpwPointRouteId(String[] ptpwPointRouteId) {
        this.ptpwPointRouteId = ptpwPointRouteId;
    }

    public String[] getPtpwTotalHrs() {
        return ptpwTotalHrs;
    }

    public void setPtpwTotalHrs(String[] ptpwTotalHrs) {
        this.ptpwTotalHrs = ptpwTotalHrs;
    }

    public String[] getInterimPoint1Minutes() {
        return interimPoint1Minutes;
    }

    public void setInterimPoint1Minutes(String[] interimPoint1Minutes) {
        this.interimPoint1Minutes = interimPoint1Minutes;
    }

    public String[] getInterimPoint2Minutes() {
        return interimPoint2Minutes;
    }

    public void setInterimPoint2Minutes(String[] interimPoint2Minutes) {
        this.interimPoint2Minutes = interimPoint2Minutes;
    }

    public String[] getInterimPoint3Minutes() {
        return interimPoint3Minutes;
    }

    public void setInterimPoint3Minutes(String[] interimPoint3Minutes) {
        this.interimPoint3Minutes = interimPoint3Minutes;
    }

    public String[] getInterimPoint4Minutes() {
        return interimPoint4Minutes;
    }

    public void setInterimPoint4Minutes(String[] interimPoint4Minutes) {
        this.interimPoint4Minutes = interimPoint4Minutes;
    }

    public String[] getPtpDropPointMinutes() {
        return ptpDropPointMinutes;
    }

    public void setPtpDropPointMinutes(String[] ptpDropPointMinutes) {
        this.ptpDropPointMinutes = ptpDropPointMinutes;
    }

    public String[] getPtpTotalHours() {
        return ptpTotalHours;
    }

    public void setPtpTotalHours(String[] ptpTotalHours) {
        this.ptpTotalHours = ptpTotalHours;
    }

    public String[] getPtpTotalMinutes() {
        return ptpTotalMinutes;
    }

    public void setPtpTotalMinutes(String[] ptpTotalMinutes) {
        this.ptpTotalMinutes = ptpTotalMinutes;
    }

    public String[] getPtpVehicleTypeId() {
        return ptpVehicleTypeId;
    }

    public void setPtpVehicleTypeId(String[] ptpVehicleTypeId) {
        this.ptpVehicleTypeId = ptpVehicleTypeId;
    }

    public String[] getPtpRateWithReefer() {
        return ptpRateWithReefer;
    }

    public void setPtpRateWithReefer(String[] ptpRateWithReefer) {
        this.ptpRateWithReefer = ptpRateWithReefer;
    }

    public String[] getPtpRateWithoutReefer() {
        return ptpRateWithoutReefer;
    }

    public void setPtpRateWithoutReefer(String[] ptpRateWithoutReefer) {
        this.ptpRateWithoutReefer = ptpRateWithoutReefer;
    }

    public String[] getPtpwVehicleTypeId() {
        return ptpwVehicleTypeId;
    }

    public void setPtpwVehicleTypeId(String[] ptpwVehicleTypeId) {
        this.ptpwVehicleTypeId = ptpwVehicleTypeId;
    }

    public String[] getPtpwRateWithReefer() {
        return ptpwRateWithReefer;
    }

    public void setPtpwRateWithReefer(String[] ptpwRateWithReefer) {
        this.ptpwRateWithReefer = ptpwRateWithReefer;
    }

    public String[] getPtpwRateWithoutReefer() {
        return ptpwRateWithoutReefer;
    }

    public void setPtpwRateWithoutReefer(String[] ptpwRateWithoutReefer) {
        this.ptpwRateWithoutReefer = ptpwRateWithoutReefer;
    }

    public String[] getPtpwTotalMinutes() {
        return ptpwTotalMinutes;
    }

    public void setPtpwTotalMinutes(String[] ptpwTotalMinutes) {
        this.ptpwTotalMinutes = ptpwTotalMinutes;
    }

    public String getCityFromId() {
        return cityFromId;
    }

    public void setCityFromId(String cityFromId) {
        this.cityFromId = cityFromId;
    }

    public String getCityToId() {
        return cityToId;
    }

    public void setCityToId(String cityToId) {
        this.cityToId = cityToId;
    }

    public String getTravelHour() {
        return travelHour;
    }

    public void setTravelHour(String travelHour) {
        this.travelHour = travelHour;
    }

    public String getTravelMinute() {
        return travelMinute;
    }

    public void setTravelMinute(String travelMinute) {
        this.travelMinute = travelMinute;
    }

    public String getReeferHour() {
        return reeferHour;
    }

    public void setReeferHour(String reeferHour) {
        this.reeferHour = reeferHour;
    }

    public String getReeferMinute() {
        return reeferMinute;
    }

    public void setReeferMinute(String reeferMinute) {
        this.reeferMinute = reeferMinute;
    }

    public String getTollAmountType() {
        return tollAmountType;
    }

    public void setTollAmountType(String tollAmountType) {
        this.tollAmountType = tollAmountType;
    }

    public String getAvgTollAmount() {
        return avgTollAmount;
    }

    public void setAvgTollAmount(String avgTollAmount) {
        this.avgTollAmount = avgTollAmount;
    }

    public String getAvgMisCost() {
        return avgMisCost;
    }

    public void setAvgMisCost(String avgMisCost) {
        this.avgMisCost = avgMisCost;
    }

    public String getAvgDriverIncentive() {
        return avgDriverIncentive;
    }

    public void setAvgDriverIncentive(String avgDriverIncentive) {
        this.avgDriverIncentive = avgDriverIncentive;
    }

    public String getAvgFactor() {
        return avgFactor;
    }

    public void setAvgFactor(String avgFactor) {
        this.avgFactor = avgFactor;
    }

    public String[] getVehTypeId() {
        return vehTypeId;
    }

    public void setVehTypeId(String[] vehTypeId) {
        this.vehTypeId = vehTypeId;
    }

    public String getReeferMileage() {
        return reeferMileage;
    }

    public void setReeferMileage(String reeferMileage) {
        this.reeferMileage = reeferMileage;
    }

    public String[] getFuelCostPerKm() {
        return fuelCostPerKm;
    }

    public void setFuelCostPerKm(String[] fuelCostPerKm) {
        this.fuelCostPerKm = fuelCostPerKm;
    }

    public String[] getFuelCostPerHr() {
        return fuelCostPerHr;
    }

    public void setFuelCostPerHr(String[] fuelCostPerHr) {
        this.fuelCostPerHr = fuelCostPerHr;
    }

    public String[] getTollAmounts() {
        return tollAmounts;
    }

    public void setTollAmounts(String[] tollAmounts) {
        this.tollAmounts = tollAmounts;
    }

    public String[] getMiscCostKm() {
        return miscCostKm;
    }

    public void setMiscCostKm(String[] miscCostKm) {
        this.miscCostKm = miscCostKm;
    }

    public String[] getDriverIncenKm() {
        return driverIncenKm;
    }

    public void setDriverIncenKm(String[] driverIncenKm) {
        this.driverIncenKm = driverIncenKm;
    }

    public String[] getFactor() {
        return factor;
    }

    public void setFactor(String[] factor) {
        this.factor = factor;
    }

    public String[] getVarExpense() {
        return varExpense;
    }

    public void setVarExpense(String[] varExpense) {
        this.varExpense = varExpense;
    }

    public String getEntryType() {
        return entryType;
    }

    public void setEntryType(String entryType) {
        this.entryType = entryType;
    }

    public String getConsignmentNoteNo() {
        return consignmentNoteNo;
    }

    public void setConsignmentNoteNo(String consignmentNoteNo) {
        this.consignmentNoteNo = consignmentNoteNo;
    }

    public String getConsignmentDate() {
        return consignmentDate;
    }

    public void setConsignmentDate(String consignmentDate) {
        this.consignmentDate = consignmentDate;
    }

    public String getOrderReferenceNo() {
        return orderReferenceNo;
    }

    public void setOrderReferenceNo(String orderReferenceNo) {
        this.orderReferenceNo = orderReferenceNo;
    }

    public String getOrderReferenceRemarks() {
        return orderReferenceRemarks;
    }

    public void setOrderReferenceRemarks(String orderReferenceRemarks) {
        this.orderReferenceRemarks = orderReferenceRemarks;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCustomerMobileNo() {
        return customerMobileNo;
    }

    public void setCustomerMobileNo(String customerMobileNo) {
        this.customerMobileNo = customerMobileNo;
    }

    public String getMailId() {
        return mailId;
    }

    public void setMailId(String mailId) {
        this.mailId = mailId;
    }

    public String getCustomerPhoneNo() {
        return customerPhoneNo;
    }

    public void setCustomerPhoneNo(String customerPhoneNo) {
        this.customerPhoneNo = customerPhoneNo;
    }

    public String getWalkinCustomerName() {
        return walkinCustomerName;
    }

    public void setWalkinCustomerName(String walkinCustomerName) {
        this.walkinCustomerName = walkinCustomerName;
    }

    public String getWalkinCustomerCode() {
        return walkinCustomerCode;
    }

    public void setWalkinCustomerCode(String walkinCustomerCode) {
        this.walkinCustomerCode = walkinCustomerCode;
    }

    public String getWalkinCustomerAddress() {
        return walkinCustomerAddress;
    }

    public void setWalkinCustomerAddress(String walkinCustomerAddress) {
        this.walkinCustomerAddress = walkinCustomerAddress;
    }

    public String getWalkinPincode() {
        return walkinPincode;
    }

    public void setWalkinPincode(String walkinPincode) {
        this.walkinPincode = walkinPincode;
    }

    public String getWalkinCustomerMobileNo() {
        return walkinCustomerMobileNo;
    }

    public void setWalkinCustomerMobileNo(String walkinCustomerMobileNo) {
        this.walkinCustomerMobileNo = walkinCustomerMobileNo;
    }

    public String getWalkinMailId() {
        return walkinMailId;
    }

    public void setWalkinMailId(String walkinMailId) {
        this.walkinMailId = walkinMailId;
    }

    public String getWalkinCustomerPhoneNo() {
        return walkinCustomerPhoneNo;
    }

    public void setWalkinCustomerPhoneNo(String walkinCustomerPhoneNo) {
        this.walkinCustomerPhoneNo = walkinCustomerPhoneNo;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getMultiPickup() {
        return multiPickup;
    }

    public void setMultiPickup(String multiPickup) {
        this.multiPickup = multiPickup;
    }

    public String getMultiDelivery() {
        return multiDelivery;
    }

    public void setMultiDelivery(String multiDelivery) {
        this.multiDelivery = multiDelivery;
    }

    public String getConsignmentOrderInstruction() {
        return consignmentOrderInstruction;
    }

    public void setConsignmentOrderInstruction(String consignmentOrderInstruction) {
        this.consignmentOrderInstruction = consignmentOrderInstruction;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getReeferRequired() {
        return reeferRequired;
    }

    public void setReeferRequired(String reeferRequired) {
        this.reeferRequired = reeferRequired;
    }

    public String getContractRateId() {
        return contractRateId;
    }

    public void setContractRateId(String contractRateId) {
        this.contractRateId = contractRateId;
    }

    public String getVehicleRequiredDate() {
        return vehicleRequiredDate;
    }

    public void setVehicleRequiredDate(String vehicleRequiredDate) {
        this.vehicleRequiredDate = vehicleRequiredDate;
    }

    public String getVehicleRequiredHour() {
        return vehicleRequiredHour;
    }

    public void setVehicleRequiredHour(String vehicleRequiredHour) {
        this.vehicleRequiredHour = vehicleRequiredHour;
    }

    public String getVehicleRequiredMinute() {
        return vehicleRequiredMinute;
    }

    public void setVehicleRequiredMinute(String vehicleRequiredMinute) {
        this.vehicleRequiredMinute = vehicleRequiredMinute;
    }

    public String getVehicleInstruction() {
        return vehicleInstruction;
    }

    public void setVehicleInstruction(String vehicleInstruction) {
        this.vehicleInstruction = vehicleInstruction;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getConsignorPhoneNo() {
        return consignorPhoneNo;
    }

    public void setConsignorPhoneNo(String consignorPhoneNo) {
        this.consignorPhoneNo = consignorPhoneNo;
    }

    public String getConsignorAddress() {
        return consignorAddress;
    }

    public void setConsignorAddress(String consignorAddress) {
        this.consignorAddress = consignorAddress;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsigneePhoneNo() {
        return consigneePhoneNo;
    }

    public void setConsigneePhoneNo(String consigneePhoneNo) {
        this.consigneePhoneNo = consigneePhoneNo;
    }

    public String getConsigneeAddress() {
        return consigneeAddress;
    }

    public void setConsigneeAddress(String consigneeAddress) {
        this.consigneeAddress = consigneeAddress;
    }

    public String getRateWithReefer() {
        return rateWithReefer;
    }

    public void setRateWithReefer(String rateWithReefer) {
        this.rateWithReefer = rateWithReefer;
    }

    public String getRateWithoutReefer() {
        return rateWithoutReefer;
    }

    public void setRateWithoutReefer(String rateWithoutReefer) {
        this.rateWithoutReefer = rateWithoutReefer;
    }

    public String getTotalPackage() {
        return totalPackage;
    }

    public void setTotalPackage(String totalPackage) {
        this.totalPackage = totalPackage;
    }

    public String getTotalWeightage() {
        return totalWeightage;
    }

    public void setTotalWeightage(String totalWeightage) {
        this.totalWeightage = totalWeightage;
    }

    public String getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(String totalHours) {
        this.totalHours = totalHours;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getTotalCharges() {
        return totalCharges;
    }

    public void setTotalCharges(String totalCharges) {
        this.totalCharges = totalCharges;
    }

    public String getDocCharges() {
        return docCharges;
    }

    public void setDocCharges(String docCharges) {
        this.docCharges = docCharges;
    }

    public String getOdaCharges() {
        return odaCharges;
    }

    public void setOdaCharges(String odaCharges) {
        this.odaCharges = odaCharges;
    }

    public String getMultiPickupCharge() {
        return multiPickupCharge;
    }

    public void setMultiPickupCharge(String multiPickupCharge) {
        this.multiPickupCharge = multiPickupCharge;
    }

    public String getMultiDeliveryCharge() {
        return multiDeliveryCharge;
    }

    public void setMultiDeliveryCharge(String multiDeliveryCharge) {
        this.multiDeliveryCharge = multiDeliveryCharge;
    }

    public String getHandleCharges() {
        return handleCharges;
    }

    public void setHandleCharges(String handleCharges) {
        this.handleCharges = handleCharges;
    }

    public String getOtherCharges() {
        return otherCharges;
    }

    public void setOtherCharges(String otherCharges) {
        this.otherCharges = otherCharges;
    }

    public String getUnloadingCharges() {
        return unloadingCharges;
    }

    public void setUnloadingCharges(String unloadingCharges) {
        this.unloadingCharges = unloadingCharges;
    }

    public String getLoadingCharges() {
        return loadingCharges;
    }

    public void setLoadingCharges(String loadingCharges) {
        this.loadingCharges = loadingCharges;
    }

    public String getWalkInBillingTypeId() {
        return walkInBillingTypeId;
    }

    public void setWalkInBillingTypeId(String walkInBillingTypeId) {
        this.walkInBillingTypeId = walkInBillingTypeId;
    }

    public String getWalkinFreightWithReefer() {
        return walkinFreightWithReefer;
    }

    public void setWalkinFreightWithReefer(String walkinFreightWithReefer) {
        this.walkinFreightWithReefer = walkinFreightWithReefer;
    }

    public String getWalkinFreightWithoutReefer() {
        return walkinFreightWithoutReefer;
    }

    public void setWalkinFreightWithoutReefer(String walkinFreightWithoutReefer) {
        this.walkinFreightWithoutReefer = walkinFreightWithoutReefer;
    }

    public String getWalkinRateWithReeferPerKg() {
        return walkinRateWithReeferPerKg;
    }

    public void setWalkinRateWithReeferPerKg(String walkinRateWithReeferPerKg) {
        this.walkinRateWithReeferPerKg = walkinRateWithReeferPerKg;
    }

    public String getWalkinRateWithoutReeferPerKg() {
        return walkinRateWithoutReeferPerKg;
    }

    public void setWalkinRateWithoutReeferPerKg(String walkinRateWithoutReeferPerKg) {
        this.walkinRateWithoutReeferPerKg = walkinRateWithoutReeferPerKg;
    }

    public String getWalkinRateWithReeferPerKm() {
        return walkinRateWithReeferPerKm;
    }

    public void setWalkinRateWithReeferPerKm(String walkinRateWithReeferPerKm) {
        this.walkinRateWithReeferPerKm = walkinRateWithReeferPerKm;
    }

    public String getWalkinRateWithoutReeferPerKm() {
        return walkinRateWithoutReeferPerKm;
    }

    public void setWalkinRateWithoutReeferPerKm(String walkinRateWithoutReeferPerKm) {
        this.walkinRateWithoutReeferPerKm = walkinRateWithoutReeferPerKm;
    }

    public String[] getProductCodes() {
        return productCodes;
    }

    public void setProductCodes(String[] productCodes) {
        this.productCodes = productCodes;
    }

    public String[] getProductNames() {
        return productNames;
    }

    public void setProductNames(String[] productNames) {
        this.productNames = productNames;
    }

    public String[] getPackagesNos() {
        return packagesNos;
    }

    public void setPackagesNos(String[] packagesNos) {
        this.packagesNos = packagesNos;
    }

    public String[] getWeights() {
        return weights;
    }

    public void setWeights(String[] weights) {
        this.weights = weights;
    }

    public String getRouteContractId() {
        return routeContractId;
    }

    public void setRouteContractId(String routeContractId) {
        this.routeContractId = routeContractId;
    }

    public String getTotalKm() {
        return totalKm;
    }

    public void setTotalKm(String totalKm) {
        this.totalKm = totalKm;
    }

    public String getTotFreightAmount() {
        return totFreightAmount;
    }

    public void setTotFreightAmount(String totFreightAmount) {
        this.totFreightAmount = totFreightAmount;
    }

    public String getStandardChargeRemarks() {
        return standardChargeRemarks;
    }

    public void setStandardChargeRemarks(String standardChargeRemarks) {
        this.standardChargeRemarks = standardChargeRemarks;
    }

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public String getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public String getTotalMinutes() {
        return totalMinutes;
    }

    public void setTotalMinutes(String totalMinutes) {
        this.totalMinutes = totalMinutes;
    }

    public String getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }

    public String[] getFuelCostPerKms() {
        return fuelCostPerKms;
    }

    public void setFuelCostPerKms(String[] fuelCostPerKms) {
        this.fuelCostPerKms = fuelCostPerKms;
    }

    public String[] getFuelCostPerHrs() {
        return fuelCostPerHrs;
    }

    public void setFuelCostPerHrs(String[] fuelCostPerHrs) {
        this.fuelCostPerHrs = fuelCostPerHrs;
    }

    public String[] getReefMileage() {
        return reefMileage;
    }

    public void setReefMileage(String[] reefMileage) {
        this.reefMileage = reefMileage;
    }

    public String getFuelCostKm() {
        return fuelCostKm;
    }

    public void setFuelCostKm(String fuelCostKm) {
        this.fuelCostKm = fuelCostKm;
    }

    public String getFuelCostHr() {
        return fuelCostHr;
    }

    public void setFuelCostHr(String fuelCostHr) {
        this.fuelCostHr = fuelCostHr;
    }

    public String getTollAmountperkm() {
        return tollAmountperkm;
    }

    public void setTollAmountperkm(String tollAmountperkm) {
        this.tollAmountperkm = tollAmountperkm;
    }

    public String getMiscCostperkm() {
        return miscCostperkm;
    }

    public void setMiscCostperkm(String miscCostperkm) {
        this.miscCostperkm = miscCostperkm;
    }

    public String getDriverIncentperkm() {
        return driverIncentperkm;
    }

    public void setDriverIncentperkm(String driverIncentperkm) {
        this.driverIncentperkm = driverIncentperkm;
    }

    public String getVariExpense() {
        return variExpense;
    }

    public void setVariExpense(String variExpense) {
        this.variExpense = variExpense;
    }

    public String getVehiExpense() {
        return vehiExpense;
    }

    public void setVehiExpense(String vehiExpense) {
        this.vehiExpense = vehiExpense;
    }

    public String getReefeExpense() {
        return reefeExpense;
    }

    public void setReefeExpense(String reefeExpense) {
        this.reefeExpense = reefeExpense;
    }

    public String getTotaExpense() {
        return totaExpense;
    }

    public void setTotaExpense(String totaExpense) {
        this.totaExpense = totaExpense;
    }

    public String getEffectivDate() {
        return effectivDate;
    }

    public void setEffectivDate(String effectivDate) {
        this.effectivDate = effectivDate;
    }

    public void setPointId(String[] pointId) {
        this.pointId = pointId;
    }

    public void setPointName(String[] pointName) {
        this.pointName = pointName;
    }

    public void setPointType(String[] pointType) {
        this.pointType = pointType;
    }

    public void setOrder(String[] order) {
        this.order = order;
    }

    public void setPointAddresss(String[] pointAddresss) {
        this.pointAddresss = pointAddresss;
    }

    public void setPointPlanDate(String[] pointPlanDate) {
        this.pointPlanDate = pointPlanDate;
    }

    public void setPointPlanHour(String[] pointPlanHour) {
        this.pointPlanHour = pointPlanHour;
    }

    public void setPointPlanMinute(String[] pointPlanMinute) {
        this.pointPlanMinute = pointPlanMinute;
    }

    public void setEndPointId(String endPointId) {
        this.endPointId = endPointId;
    }

    public void setFinalRouteId(String finalRouteId) {
        this.finalRouteId = finalRouteId;
    }

    public void setEndPointName(String endPointName) {
        this.endPointName = endPointName;
    }

    public void setEndPointType(String endPointType) {
        this.endPointType = endPointType;
    }

    public void setEndOrder(String endOrder) {
        this.endOrder = endOrder;
    }

    public void setEndPointAddresss(String endPointAddresss) {
        this.endPointAddresss = endPointAddresss;
    }

    public void setEndPointPlanDate(String endPointPlanDate) {
        this.endPointPlanDate = endPointPlanDate;
    }

    public void setEndPointPlanHour(String endPointPlanHour) {
        this.endPointPlanHour = endPointPlanHour;
    }

    public void setEndPointPlanMinute(String endPointPlanMinute) {
        this.endPointPlanMinute = endPointPlanMinute;
    }

    public String[] getPointId() {
        return pointId;
    }

    public String[] getPointName() {
        return pointName;
    }

    public String[] getPointType() {
        return pointType;
    }

    public String[] getOrder() {
        return order;
    }

    public String[] getPointAddresss() {
        return pointAddresss;
    }

    public String[] getPointPlanDate() {
        return pointPlanDate;
    }

    public String[] getPointPlanHour() {
        return pointPlanHour;
    }

    public String[] getPointPlanMinute() {
        return pointPlanMinute;
    }

    public String getEndPointId() {
        return endPointId;
    }

    public String getFinalRouteId() {
        return finalRouteId;
    }

    public String getEndPointName() {
        return endPointName;
    }

    public String getEndPointType() {
        return endPointType;
    }

    public String getEndOrder() {
        return endOrder;
    }

    public String getEndPointAddresss() {
        return endPointAddresss;
    }

    public String getEndPointPlanDate() {
        return endPointPlanDate;
    }

    public String getEndPointPlanHour() {
        return endPointPlanHour;
    }

    public String getEndPointPlanMinute() {
        return endPointPlanMinute;
    }

    public String[] getMultiplePointRouteId() {
        return multiplePointRouteId;
    }

    public void setMultiplePointRouteId(String[] multiplePointRouteId) {
        this.multiplePointRouteId = multiplePointRouteId;
    }

    public String[] getPointOrder() {
        return pointOrder;
    }

    public void setPointOrder(String[] pointOrder) {
        this.pointOrder = pointOrder;
    }

    public String[] getPointRouteId() {
        return pointRouteId;
    }

    public void setPointRouteId(String[] pointRouteId) {
        this.pointRouteId = pointRouteId;
    }

    public String getPrimaryDriverId() {
        return primaryDriverId;
    }

    public void setPrimaryDriverId(String primaryDriverId) {
        this.primaryDriverId = primaryDriverId;
    }

    public String getSecondaryDriverIdOne() {
        return secondaryDriverIdOne;
    }

    public void setSecondaryDriverIdOne(String secondaryDriverIdOne) {
        this.secondaryDriverIdOne = secondaryDriverIdOne;
    }

    public String getSecondaryDriverIdTwo() {
        return secondaryDriverIdTwo;
    }

    public void setSecondaryDriverIdTwo(String secondaryDriverIdTwo) {
        this.secondaryDriverIdTwo = secondaryDriverIdTwo;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getExpectedArrivalDate() {
        return expectedArrivalDate;
    }

    public void setExpectedArrivalDate(String expectedArrivalDate) {
        this.expectedArrivalDate = expectedArrivalDate;
    }

    public String getExpectedArrivalTime() {
        return expectedArrivalTime;
    }

    public void setExpectedArrivalTime(String expectedArrivalTime) {
        this.expectedArrivalTime = expectedArrivalTime;
    }

    public String getVehicleid() {
        return vehicleid;
    }

    public void setVehicleid(String vehicleid) {
        this.vehicleid = vehicleid;
    }

    public String[] getSelectedValue() {
        return selectedValue;
    }

    public void setSelectedValue(String[] selectedValue) {
        this.selectedValue = selectedValue;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getApproveremarks() {
        return approveremarks;
    }

    public void setApproveremarks(String approveremarks) {
        this.approveremarks = approveremarks;
    }

    public String getApprovestatus() {
        return approvestatus;
    }

    public void setApprovestatus(String approvestatus) {
        this.approvestatus = approvestatus;
    }

    public String getAdvicedate() {
        return advicedate;
    }

    public void setAdvicedate(String advicedate) {
        this.advicedate = advicedate;
    }

    public String getAlertBased() {
        return alertBased;
    }

    public void setAlertBased(String alertBased) {
        this.alertBased = alertBased;
    }

    public String getAlertDes() {
        return alertDes;
    }

    public void setAlertDes(String alertDes) {
        this.alertDes = alertDes;
    }

    public String getAlertName() {
        return alertName;
    }

    public void setAlertName(String alertName) {
        this.alertName = alertName;
    }

    public String getAlertRaised() {
        return alertRaised;
    }

    public void setAlertRaised(String alertRaised) {
        this.alertRaised = alertRaised;
    }

    public String getAlertRaised1() {
        return alertRaised1;
    }

    public void setAlertRaised1(String alertRaised1) {
        this.alertRaised1 = alertRaised1;
    }

    public String getAlertccEmail() {
        return alertccEmail;
    }

    public void setAlertccEmail(String alertccEmail) {
        this.alertccEmail = alertccEmail;
    }

    public String getAlertid() {
        return alertid;
    }

    public void setAlertid(String alertid) {
        this.alertid = alertid;
    }

    public String getAlertsemailSub() {
        return alertsemailSub;
    }

    public void setAlertsemailSub(String alertsemailSub) {
        this.alertsemailSub = alertsemailSub;
    }

    public String getAlertsfrequently() {
        return alertsfrequently;
    }

    public void setAlertsfrequently(String alertsfrequently) {
        this.alertsfrequently = alertsfrequently;
    }

    public String getAlertstatus() {
        return alertstatus;
    }

    public void setAlertstatus(String alertstatus) {
        this.alertstatus = alertstatus;
    }

    public String getAlerttoEmail() {
        return alerttoEmail;
    }

    public void setAlerttoEmail(String alerttoEmail) {
        this.alerttoEmail = alerttoEmail;
    }

    public String getCheckListDate() {
        return checkListDate;
    }

    public void setCheckListDate(String checkListDate) {
        this.checkListDate = checkListDate;
    }

    public String getCheckListId() {
        return checkListId;
    }

    public void setCheckListId(String checkListId) {
        this.checkListId = checkListId;
    }

    public String getCheckListName() {
        return checkListName;
    }

    public void setCheckListName(String checkListName) {
        this.checkListName = checkListName;
    }

    public String getCheckListStage() {
        return checkListStage;
    }

    public void setCheckListStage(String checkListStage) {
        this.checkListStage = checkListStage;
    }

    public String getCheckListStatus() {
        return checkListStatus;
    }

    public void setCheckListStatus(String checkListStatus) {
        this.checkListStatus = checkListStatus;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityState() {
        return cityState;
    }

    public void setCityState(String cityState) {
        this.cityState = cityState;
    }

    public String getEscalationccEmail1() {
        return escalationccEmail1;
    }

    public void setEscalationccEmail1(String escalationccEmail1) {
        this.escalationccEmail1 = escalationccEmail1;
    }

    public String getEscalationemailSub1() {
        return escalationemailSub1;
    }

    public void setEscalationemailSub1(String escalationemailSub1) {
        this.escalationemailSub1 = escalationemailSub1;
    }

    public String getEscalationtoEmail1() {
        return escalationtoEmail1;
    }

    public void setEscalationtoEmail1(String escalationtoEmail1) {
        this.escalationtoEmail1 = escalationtoEmail1;
    }

    public String getFrequencyId() {
        return frequencyId;
    }

    public void setFrequencyId(String frequencyId) {
        this.frequencyId = frequencyId;
    }

    public String getFrequencyName() {
        return frequencyName;
    }

    public void setFrequencyName(String frequencyName) {
        this.frequencyName = frequencyName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String[] getInvoiceTripId() {
        return invoiceTripId;
    }

    public void setInvoiceTripId(String[] invoiceTripId) {
        this.invoiceTripId = invoiceTripId;
    }

    public String getParameterDescription() {
        return parameterDescription;
    }

    public void setParameterDescription(String parameterDescription) {
        this.parameterDescription = parameterDescription;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getParameterUnit() {
        return parameterUnit;
    }

    public void setParameterUnit(String parameterUnit) {
        this.parameterUnit = parameterUnit;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    public String getProductCategoryName() {
        return productCategoryName;
    }

    public void setProductCategoryName(String productCategoryName) {
        this.productCategoryName = productCategoryName;
    }

    public String getReeferMaximumTemperature() {
        return reeferMaximumTemperature;
    }

    public void setReeferMaximumTemperature(String reeferMaximumTemperature) {
        this.reeferMaximumTemperature = reeferMaximumTemperature;
    }

    public String getReeferMinimumTemperature() {
        return reeferMinimumTemperature;
    }

    public void setReeferMinimumTemperature(String reeferMinimumTemperature) {
        this.reeferMinimumTemperature = reeferMinimumTemperature;
    }

    public String getRepeat() {
        return repeat;
    }

    public void setRepeat(String repeat) {
        this.repeat = repeat;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getZoneid() {
        return zoneid;
    }

    public void setZoneid(String zoneid) {
        this.zoneid = zoneid;
    }

    public String getTripclosureid() {
        return tripclosureid;
    }

    public void setTripclosureid(String tripclosureid) {
        this.tripclosureid = tripclosureid;
    }

    public String getMappingId() {
        return mappingId;
    }

    public void setMappingId(String mappingId) {
        this.mappingId = mappingId;
    }

    public String getBatchType() {
        return batchType;
    }

    public void setBatchType(String batchType) {
        this.batchType = batchType;
    }

    public String getFleetCenterId() {
        return fleetCenterId;
    }

    public void setFleetCenterId(String fleetCenterId) {
        this.fleetCenterId = fleetCenterId;
    }

    public String[] getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String[] batchCode) {
        this.batchCode = batchCode;
    }

    public String[] getUom() {
        return uom;
    }

    public void setUom(String[] uom) {
        this.uom = uom;
    }

    public String[] getConsignmentArticleId() {
        return consignmentArticleId;
    }

    public void setConsignmentArticleId(String[] consignmentArticleId) {
        this.consignmentArticleId = consignmentArticleId;
    }

    public String getAccountManager() {
        return accountManager;
    }

    public void setAccountManager(String accountManager) {
        this.accountManager = accountManager;
    }

    public String getAccountManagerId() {
        return accountManagerId;
    }

    public void setAccountManagerId(String accountManagerId) {
        this.accountManagerId = accountManagerId;
    }

    public String getCreditDays() {
        return creditDays;
    }

    public void setCreditDays(String creditDays) {
        this.creditDays = creditDays;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getCustAddress() {
        return custAddress;
    }

    public void setCustAddress(String custAddress) {
        this.custAddress = custAddress;
    }

    public String getCustCity() {
        return custCity;
    }

    public void setCustCity(String custCity) {
        this.custCity = custCity;
    }

    public String getCustCode() {
        return custCode;
    }

    public void setCustCode(String custCode) {
        this.custCode = custCode;
    }

    public String getCustContactPerson() {
        return custContactPerson;
    }

    public void setCustContactPerson(String custContactPerson) {
        this.custContactPerson = custContactPerson;
    }

    public String getCustEmail() {
        return custEmail;
    }

    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getCustMobile() {
        return custMobile;
    }

    public void setCustMobile(String custMobile) {
        this.custMobile = custMobile;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustPhone() {
        return custPhone;
    }

    public void setCustPhone(String custPhone) {
        this.custPhone = custPhone;
    }

    public String getCustState() {
        return custState;
    }

    public void setCustState(String custState) {
        this.custState = custState;
    }

    public String getCustType() {
        return custType;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getChequeRemarks() {
        return chequeRemarks;
    }

    public void setChequeRemarks(String chequeRemarks) {
        this.chequeRemarks = chequeRemarks;
    }

    public String getDraftNo() {
        return draftNo;
    }

    public void setDraftNo(String draftNo) {
        this.draftNo = draftNo;
    }

    public String getDraftRemarks() {
        return draftRemarks;
    }

    public void setDraftRemarks(String draftRemarks) {
        this.draftRemarks = draftRemarks;
    }

    public String getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(String paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getPaymentModeId() {
        return paymentModeId;
    }

    public void setPaymentModeId(String paymentModeId) {
        this.paymentModeId = paymentModeId;
    }

    public String getPaymentModeName() {
        return paymentModeName;
    }

    public void setPaymentModeName(String paymentModeName) {
        this.paymentModeName = paymentModeName;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getRtgsNo() {
        return rtgsNo;
    }

    public void setRtgsNo(String rtgsNo) {
        this.rtgsNo = rtgsNo;
    }

    public String getRtgsRemarks() {
        return rtgsRemarks;
    }

    public void setRtgsRemarks(String rtgsRemarks) {
        this.rtgsRemarks = rtgsRemarks;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getFuelUnite() {
        return fuelUnite;
    }

    public void setFuelUnite(String fuelUnite) {
        this.fuelUnite = fuelUnite;
    }

    public String getFuelTypeId() {
        return fuelTypeId;
    }

    public void setFuelTypeId(String fuelTypeId) {
        this.fuelTypeId = fuelTypeId;
    }

    public String getFuelTypeName() {
        return fuelTypeName;
    }

    public void setFuelTypeName(String fuelTypeName) {
        this.fuelTypeName = fuelTypeName;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String[] getRouteCostIds() {
        return routeCostIds;
    }

    public void setRouteCostIds(String[] routeCostIds) {
        this.routeCostIds = routeCostIds;
    }

    public String getGoogleCityName() {
        return googleCityName;
    }

    public void setGoogleCityName(String googleCityName) {
        this.googleCityName = googleCityName;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getGcnNo() {
        return gcnNo;
    }

    public void setGcnNo(String gcnNo) {
        this.gcnNo = gcnNo;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public String getEditFreightAmount() {
        return editFreightAmount;
    }

    public void setEditFreightAmount(String editFreightAmount) {
        this.editFreightAmount = editFreightAmount;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getSacCode() {
        return sacCode;
    }

    public void setSacCode(String sacCode) {
        this.sacCode = sacCode;
    }

    public String getSacDescription() {
        return sacDescription;
    }

    public void setSacDescription(String sacDescription) {
        this.sacDescription = sacDescription;
    }

    public String getGstCategoryId() {
        return gstCategoryId;
    }

    public void setGstCategoryId(String gstCategoryId) {
        this.gstCategoryId = gstCategoryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGstCategoryName() {
        return gstCategoryName;
    }

    public void setGstCategoryName(String gstCategoryName) {
        this.gstCategoryName = gstCategoryName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getGstCodeMasterId() {
        return gstCodeMasterId;
    }

    public void setGstCodeMasterId(String gstCodeMasterId) {
        this.gstCodeMasterId = gstCodeMasterId;
    }

    public String getGstName() {
        return gstName;
    }

    public void setGstName(String gstName) {
        this.gstName = gstName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getGstCode() {
        return gstCode;
    }

    public void setGstCode(String gstCode) {
        this.gstCode = gstCode;
    }

    public String getGstPercentage() {
        return gstPercentage;
    }

    public void setGstPercentage(String gstPercentage) {
        this.gstPercentage = gstPercentage;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public String getGstRateDetailId() {
        return gstRateDetailId;
    }

    public void setGstRateDetailId(String gstRateDetailId) {
        this.gstRateDetailId = gstRateDetailId;
    }

    public String getGstCategoryCode() {
        return gstCategoryCode;
    }

    public void setGstCategoryCode(String gstCategoryCode) {
        this.gstCategoryCode = gstCategoryCode;
    }

    public String getGstRateId() {
        return gstRateId;
    }

    public void setGstRateId(String gstRateId) {
        this.gstRateId = gstRateId;
    }

    public String getHsnName() {
        return hsnName;
    }

    public void setHsnName(String hsnName) {
        this.hsnName = hsnName;
    }

    public String getHsnCode() {
        return hsnCode;
    }

    public void setHsnCode(String hsnCode) {
        this.hsnCode = hsnCode;
    }

    public String getHsnDescription() {
        return hsnDescription;
    }

    public void setHsnDescription(String hsnDescription) {
        this.hsnDescription = hsnDescription;
    }

    public String getGstProductId() {
        return gstProductId;
    }

    public void setGstProductId(String gstProductId) {
        this.gstProductId = gstProductId;
    }

    public String getGstType() {
        return gstType;
    }

    public void setGstType(String gstType) {
        this.gstType = gstType;
    }

    public String getGstProductCategoryCode() {
        return gstProductCategoryCode;
    }

    public void setGstProductCategoryCode(String gstProductCategoryCode) {
        this.gstProductCategoryCode = gstProductCategoryCode;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String[] getConsigneeAddresss() {
        return consigneeAddresss;
    }

    public void setConsigneeAddresss(String[] consigneeAddresss) {
        this.consigneeAddresss = consigneeAddresss;
    }

    public String[] getConsigneeNames() {
        return consigneeNames;
    }

    public void setConsigneeNames(String[] consigneeNames) {
        this.consigneeNames = consigneeNames;
    }

    public String[] getConsigneePhoneNos() {
        return consigneePhoneNos;
    }

    public void setConsigneePhoneNos(String[] consigneePhoneNos) {
        this.consigneePhoneNos = consigneePhoneNos;
    }

    public String[] getConsignorAddresss() {
        return consignorAddresss;
    }

    public void setConsignorAddresss(String[] consignorAddresss) {
        this.consignorAddresss = consignorAddresss;
    }

    public String[] getConsignorNames() {
        return consignorNames;
    }

    public void setConsignorNames(String[] consignorNames) {
        this.consignorNames = consignorNames;
    }

    public String[] getConsignorPhoneNos() {
        return consignorPhoneNos;
    }

    public void setConsignorPhoneNos(String[] consignorPhoneNos) {
        this.consignorPhoneNos = consignorPhoneNos;
    }

    public String[] getConsigneeIds() {
        return consigneeIds;
    }

    public void setConsigneeIds(String[] consigneeIds) {
        this.consigneeIds = consigneeIds;
    }

    public String[] getConsignorIds() {
        return consignorIds;
    }

    public void setConsignorIds(String[] consignorIds) {
        this.consignorIds = consignorIds;
    }

    public String[] getPhoneNos() {
        return phoneNos;
    }

    public void setPhoneNos(String[] phoneNos) {
        this.phoneNos = phoneNos;
    }

    public String getWalkinCustomerId() {
        return walkinCustomerId;
    }

    public void setWalkinCustomerId(String walkinCustomerId) {
        this.walkinCustomerId = walkinCustomerId;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public String[] getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String[] deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String[] getInvoicedNo() {
        return invoicedNo;
    }

    public void setInvoicedNo(String[] invoicedNo) {
        this.invoicedNo = invoicedNo;
    }

    public String[] getWeightKG() {
        return weightKG;
    }

    public void setWeightKG(String[] weightKG) {
        this.weightKG = weightKG;
    }

    public String[] getMinWgtBase() {
        return minWgtBase;
    }

    public void setMinWgtBase(String[] minWgtBase) {
        this.minWgtBase = minWgtBase;
    }

    public String getMinimumBaseWgt() {
        return minimumBaseWgt;
    }

    public void setMinimumBaseWgt(String minimumBaseWgt) {
        this.minimumBaseWgt = minimumBaseWgt;
    }

    public String[] getGcnNos() {
        return gcnNos;
    }

    public void setGcnNos(String[] gcnNos) {
        this.gcnNos = gcnNos;
    }

    public String[] getConsigDate() {
        return consigDate;
    }

    public void setConsigDate(String[] consigDate) {
        this.consigDate = consigDate;
    }

    public String[] getCustRefDate() {
        return custRefDate;
    }

    public void setCustRefDate(String[] custRefDate) {
        this.custRefDate = custRefDate;
    }

//    CBT---
    public String getAirWayBillNo() {
        return airWayBillNo;
    }

    public void setAirWayBillNo(String airWayBillNo) {
        this.airWayBillNo = airWayBillNo;
    }

    public String getAwbNo() {
        return awbNo;
    }

    public void setAwbNo(String awbNo) {
        this.awbNo = awbNo;
    }

    public String getAwbType() {
        return awbType;
    }

    public void setAwbType(String awbType) {
        this.awbType = awbType;
    }

    public String getAllowedTrucks() {
        return allowedTrucks;
    }

    public void setAllowedTrucks(String allowedTrucks) {
        this.allowedTrucks = allowedTrucks;
    }

    public String getTruckNos() {
        return truckNos;
    }

    public void setTruckNos(String truckNos) {
        this.truckNos = truckNos;
    }

    public String getTotalVolumeActual() {
        return totalVolumeActual;
    }

    public void setTotalVolumeActual(String totalVolumeActual) {
        this.totalVolumeActual = totalVolumeActual;
    }

    public String getAllBranchAccess() {
        return allBranchAccess;
    }

    public void setAllBranchAccess(String allBranchAccess) {
        this.allBranchAccess = allBranchAccess;
    }

    public String getTotalBooking() {
        return totalBooking;
    }

    public void setTotalBooking(String totalBooking) {
        this.totalBooking = totalBooking;
    }

    public String getTotalGrossWeight() {
        return totalGrossWeight;
    }

    public void setTotalGrossWeight(String totalGrossWeight) {
        this.totalGrossWeight = totalGrossWeight;
    }

    public String getTotalChargeableWeight() {
        return totalChargeableWeight;
    }

    public void setTotalChargeableWeight(String totalChargeableWeight) {
        this.totalChargeableWeight = totalChargeableWeight;
    }

    public String getAwbTotalGrossWeight() {
        return awbTotalGrossWeight;
    }

    public void setAwbTotalGrossWeight(String awbTotalGrossWeight) {
        this.awbTotalGrossWeight = awbTotalGrossWeight;
    }

    public String getAwbTotalPackages() {
        return awbTotalPackages;
    }

    public void setAwbTotalPackages(String awbTotalPackages) {
        this.awbTotalPackages = awbTotalPackages;
    }

    public String getAwbReceivedPackages() {
        return awbReceivedPackages;
    }

    public void setAwbReceivedPackages(String awbReceivedPackages) {
        this.awbReceivedPackages = awbReceivedPackages;
    }

    public String getAwbPendingPackages() {
        return awbPendingPackages;
    }

    public void setAwbPendingPackages(String awbPendingPackages) {
        this.awbPendingPackages = awbPendingPackages;
    }

    public String getWareHouseName() {
        return wareHouseName;
    }

    public void setWareHouseName(String wareHouseName) {
        this.wareHouseName = wareHouseName;
    }

    public String getWareHouseLocation() {
        return wareHouseLocation;
    }

    public void setWareHouseLocation(String wareHouseLocation) {
        this.wareHouseLocation = wareHouseLocation;
    }

    public String getShipmentAcceptanceDate() {
        return shipmentAcceptanceDate;
    }

    public void setShipmentAcceptanceDate(String shipmentAcceptanceDate) {
        this.shipmentAcceptanceDate = shipmentAcceptanceDate;
    }

    public String getShipmentAccpHour() {
        return shipmentAccpHour;
    }

    public void setShipmentAccpHour(String shipmentAccpHour) {
        this.shipmentAccpHour = shipmentAccpHour;
    }

    public String getShipmentAccpMinute() {
        return shipmentAccpMinute;
    }

    public void setShipmentAccpMinute(String shipmentAccpMinute) {
        this.shipmentAccpMinute = shipmentAccpMinute;
    }

    public String getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(String currencyType) {
        this.currencyType = currencyType;
    }

    public String getRateMode() {
        return rateMode;
    }

    public void setRateMode(String rateMode) {
        this.rateMode = rateMode;
    }

    public String[] getBookingReferenceRemarks() {
        return bookingReferenceRemarks;
    }

    public void setBookingReferenceRemarks(String[] bookingReferenceRemarks) {
        this.bookingReferenceRemarks = bookingReferenceRemarks;
    }

    public String getOrderDeliveryHour() {
        return orderDeliveryHour;
    }

    public void setOrderDeliveryHour(String orderDeliveryHour) {
        this.orderDeliveryHour = orderDeliveryHour;
    }

    public String getOrderDeliveryMinute() {
        return orderDeliveryMinute;
    }

    public void setOrderDeliveryMinute(String orderDeliveryMinute) {
        this.orderDeliveryMinute = orderDeliveryMinute;
    }

    public String getPerKgRate() {
        return perKgRate;
    }

    public void setPerKgRate(String perKgRate) {
        this.perKgRate = perKgRate;
    }

    public String getBookingRemarks() {
        return bookingRemarks;
    }

    public void setBookingRemarks(String bookingRemarks) {
        this.bookingRemarks = bookingRemarks;
    }

    public String getVehicleInfo() {
        return vehicleInfo;
    }

    public void setVehicleInfo(String vehicleInfo) {
        this.vehicleInfo = vehicleInfo;
    }

    public String getVehicleCapacity() {
        return vehicleCapacity;
    }

    public void setVehicleCapacity(String vehicleCapacity) {
        this.vehicleCapacity = vehicleCapacity;
    }

    public String getTruckCode() {
        return truckCode;
    }

    public void setTruckCode(String truckCode) {
        this.truckCode = truckCode;
    }

    public String getFleetCode() {
        return fleetCode;
    }

    public void setFleetCode(String fleetCode) {
        this.fleetCode = fleetCode;
    }

    public String getFleetCapacity() {
        return fleetCapacity;
    }

    public void setFleetCapacity(String fleetCapacity) {
        this.fleetCapacity = fleetCapacity;
    }

    public String getFleetVolume() {
        return fleetVolume;
    }

    public void setFleetVolume(String fleetVolume) {
        this.fleetVolume = fleetVolume;
    }

    public String getFleetName() {
        return fleetName;
    }

    public void setFleetName(String fleetName) {
        this.fleetName = fleetName;
    }

    public String getFleetId() {
        return fleetId;
    }

    public void setFleetId(String fleetId) {
        this.fleetId = fleetId;
    }

    public String getTruckName() {
        return truckName;
    }

    public void setTruckName(String truckName) {
        this.truckName = truckName;
    }

    public String getAwbOriginId() {
        return awbOriginId;
    }

    public void setAwbOriginId(String awbOriginId) {
        this.awbOriginId = awbOriginId;
    }

    public String getAwbOriginName() {
        return awbOriginName;
    }

    public void setAwbOriginName(String awbOriginName) {
        this.awbOriginName = awbOriginName;
    }

    public String getAwbDestinationName() {
        return awbDestinationName;
    }

    public void setAwbDestinationName(String awbDestinationName) {
        this.awbDestinationName = awbDestinationName;
    }

    public String getAwbDestinationId() {
        return awbDestinationId;
    }

    public void setAwbDestinationId(String awbDestinationId) {
        this.awbDestinationId = awbDestinationId;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getBreadth() {
        return breadth;
    }

    public void setBreadth(String breadth) {
        this.breadth = breadth;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getChargeAbleWeigth() {
        return chargeAbleWeigth;
    }

    public void setChargeAbleWeigth(String chargeAbleWeigth) {
        this.chargeAbleWeigth = chargeAbleWeigth;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String[] getTruckOriginId() {
        return truckOriginId;
    }

    public void setTruckOriginId(String[] truckOriginId) {
        this.truckOriginId = truckOriginId;
    }

    public String[] getAvailableCap() {
        return availableCap;
    }

    public void setAvailableCap(String[] availableCap) {
        this.availableCap = availableCap;
    }

    public String[] getAssignedCap() {
        return assignedCap;
    }

    public void setAssignedCap(String[] assignedCap) {
        this.assignedCap = assignedCap;
    }

    public String getRateMode1() {
        return rateMode1;
    }

    public void setRateMode1(String rateMode1) {
        this.rateMode1 = rateMode1;
    }

    public String getRateValue1() {
        return rateValue1;
    }

    public void setRateValue1(String rateValue1) {
        this.rateValue1 = rateValue1;
    }

    public String getDepartOnTime() {
        return departOnTime;
    }

    public void setDepartOnTime(String departOnTime) {
        this.departOnTime = departOnTime;
    }

    public String getTruckAvailable() {
        return truckAvailable;
    }

    public void setTruckAvailable(String truckAvailable) {
        this.truckAvailable = truckAvailable;
    }

    public String getTruckAssigned() {
        return truckAssigned;
    }

    public void setTruckAssigned(String truckAssigned) {
        this.truckAssigned = truckAssigned;
    }

    public String getConsignmentRouteCourseId() {
        return consignmentRouteCourseId;
    }

    public void setConsignmentRouteCourseId(String consignmentRouteCourseId) {
        this.consignmentRouteCourseId = consignmentRouteCourseId;
    }

    public String getCustomsStatus() {
        return customsStatus;
    }

    public void setCustomsStatus(String customsStatus) {
        this.customsStatus = customsStatus;
    }

    public String[] getConsArticleId() {
        return consArticleId;
    }

    public void setConsArticleId(String[] consArticleId) {
        this.consArticleId = consArticleId;
    }

    public String[] getConsRouteCourseId() {
        return consRouteCourseId;
    }

    public void setConsRouteCourseId(String[] consRouteCourseId) {
        this.consRouteCourseId = consRouteCourseId;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getAirWiseBillNo() {
        return airWiseBillNo;
    }

    public void setAirWiseBillNo(String airWiseBillNo) {
        this.airWiseBillNo = airWiseBillNo;
    }

    public String getLoadStatus() {
        return loadStatus;
    }

    public void setLoadStatus(String loadStatus) {
        this.loadStatus = loadStatus;
    }

    public String getLoadTime() {
        return loadTime;
    }

    public void setLoadTime(String loadTime) {
        this.loadTime = loadTime;
    }

    public String getCustomerDoc() {
        return customerDoc;
    }

    public void setCustomerDoc(String customerDoc) {
        this.customerDoc = customerDoc;
    }

    public String getUnloadStatus() {
        return unloadStatus;
    }

    public void setUnloadStatus(String unloadStatus) {
        this.unloadStatus = unloadStatus;
    }

    public String getUnloadTime() {
        return unloadTime;
    }

    public void setUnloadTime(String unloadTime) {
        this.unloadTime = unloadTime;
    }

    public String getPodRecieved() {
        return podRecieved;
    }

    public void setPodRecieved(String podRecieved) {
        this.podRecieved = podRecieved;
    }

    public String getAwbOriginRegion() {
        return awbOriginRegion;
    }

    public void setAwbOriginRegion(String awbOriginRegion) {
        this.awbOriginRegion = awbOriginRegion;
    }

    public String getOrderReferenceEnd() {
        return orderReferenceEnd;
    }

    public void setOrderReferenceEnd(String orderReferenceEnd) {
        this.orderReferenceEnd = orderReferenceEnd;
    }

    public int getParcelNo() {
        return parcelNo;
    }

    public void setParcelNo(int parcelNo) {
        this.parcelNo = parcelNo;
    }

    public String getOriginCityId() {
        return originCityId;
    }

    public void setOriginCityId(String originCityId) {
        this.originCityId = originCityId;
    }

    public String getConsignmentOrder() {
        return consignmentOrder;
    }

    public void setConsignmentOrder(String consignmentOrder) {
        this.consignmentOrder = consignmentOrder;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String[] getUsedVol() {
        return usedVol;
    }

    public void setUsedVol(String[] usedVol) {
        this.usedVol = usedVol;
    }

    public String[] getUsedCapacity() {
        return usedCapacity;
    }

    public void setUsedCapacity(String[] usedCapacity) {
        this.usedCapacity = usedCapacity;
    }

    public String getReasonId() {
        return reasonId;
    }

    public void setReasonId(String reasonId) {
        this.reasonId = reasonId;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    public String getTruckId() {
        return truckId;
    }

    public void setTruckId(String truckId) {
        this.truckId = truckId;
    }

    public String getTruckCodeId() {
        return truckCodeId;
    }

    public void setTruckCodeId(String truckCodeId) {
        this.truckCodeId = truckCodeId;
    }

    public String[] getContractFreightTable1() {
        return contractFreightTable1;
    }

    public void setContractFreightTable1(String[] contractFreightTable1) {
        this.contractFreightTable1 = contractFreightTable1;
    }

    public String getOrderReferenceAwb() {
        return orderReferenceAwb;
    }

    public void setOrderReferenceAwb(String orderReferenceAwb) {
        this.orderReferenceAwb = orderReferenceAwb;
    }

    public String[] getTruckTravelKm() {
        return truckTravelKm;
    }

    public void setTruckTravelKm(String[] truckTravelKm) {
        this.truckTravelKm = truckTravelKm;
    }

    public String[] getTruckTravelHour() {
        return truckTravelHour;
    }

    public void setTruckTravelHour(String[] truckTravelHour) {
        this.truckTravelHour = truckTravelHour;
    }

    public String[] getTruckTravelMinute() {
        return truckTravelMinute;
    }

    public void setTruckTravelMinute(String[] truckTravelMinute) {
        this.truckTravelMinute = truckTravelMinute;
    }

    public String[] getFleetTypeId() {
        return fleetTypeId;
    }

    public void setFleetTypeId(String[] fleetTypeId) {
        this.fleetTypeId = fleetTypeId;
    }

    public String[] getTruckDestinationId() {
        return truckDestinationId;
    }

    public void setTruckDestinationId(String[] truckDestinationId) {
        this.truckDestinationId = truckDestinationId;
    }

    public String[] getTruckRouteId() {
        return truckRouteId;
    }

    public void setTruckRouteId(String[] truckRouteId) {
        this.truckRouteId = truckRouteId;
    }

    public String getRateValue() {
        return rateValue;
    }

    public void setRateValue(String rateValue) {
        this.rateValue = rateValue;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getFreightReceipt() {
        return freightReceipt;
    }

    public void setFreightReceipt(String freightReceipt) {
        this.freightReceipt = freightReceipt;
    }

    public String[] getNetUnits() {
        return netUnits;
    }

    public void setNetUnits(String[] netUnits) {
        this.netUnits = netUnits;
    }

    public String[] getNetQuantity() {
        return netQuantity;
    }

    public void setNetQuantity(String[] netQuantity) {
        this.netQuantity = netQuantity;
    }

    public String getAwbOrigin() {
        return awbOrigin;
    }

    public void setAwbOrigin(String awbOrigin) {
        this.awbOrigin = awbOrigin;
    }

    public String getAwbDestination() {
        return awbDestination;
    }

    public void setAwbDestination(String awbDestination) {
        this.awbDestination = awbDestination;
    }

    public String getAwbOrderDeliveryDate() {
        return awbOrderDeliveryDate;
    }

    public void setAwbOrderDeliveryDate(String awbOrderDeliveryDate) {
        this.awbOrderDeliveryDate = awbOrderDeliveryDate;
    }

    public String getAwbDestinationRegion() {
        return awbDestinationRegion;
    }

    public void setAwbDestinationRegion(String awbDestinationRegion) {
        this.awbDestinationRegion = awbDestinationRegion;
    }

    public String getAwbMovementType() {
        return awbMovementType;
    }

    public void setAwbMovementType(String awbMovementType) {
        this.awbMovementType = awbMovementType;
    }

    public String[] getNoOfPieces() {
        return noOfPieces;
    }

    public void setNoOfPieces(String[] noOfPieces) {
        this.noOfPieces = noOfPieces;
    }

    public String[] getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(String[] grossWeight) {
        this.grossWeight = grossWeight;
    }

    public String[] getChargeableWeight() {
        return chargeableWeight;
    }

    public void setChargeableWeight(String[] chargeableWeight) {
        this.chargeableWeight = chargeableWeight;
    }

    public String[] getChargeableWeightId() {
        return chargeableWeightId;
    }

    public void setChargeableWeightId(String[] chargeableWeightId) {
        this.chargeableWeightId = chargeableWeightId;
    }

    public String[] getLengths() {
        return lengths;
    }

    public void setLengths(String[] lengths) {
        this.lengths = lengths;
    }

    public String[] getWidths() {
        return widths;
    }

    public void setWidths(String[] widths) {
        this.widths = widths;
    }

    public String[] getHeights() {
        return heights;
    }

    public void setHeights(String[] heights) {
        this.heights = heights;
    }

    public String[] getDgHandlingCode() {
        return dgHandlingCode;
    }

    public void setDgHandlingCode(String[] dgHandlingCode) {
        this.dgHandlingCode = dgHandlingCode;
    }

    public String[] getUnIdNo() {
        return unIdNo;
    }

    public void setUnIdNo(String[] unIdNo) {
        this.unIdNo = unIdNo;
    }

    public String getTotalChargeableWeights() {
        return totalChargeableWeights;
    }

    public void setTotalChargeableWeights(String totalChargeableWeights) {
        this.totalChargeableWeights = totalChargeableWeights;
    }

    public String getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(String totalVolume) {
        this.totalVolume = totalVolume;
    }

    public String[] getAvailableWeight() {
        return availableWeight;
    }

    public void setAvailableWeight(String[] availableWeight) {
        this.availableWeight = availableWeight;
    }

    public String[] getAvailableVolume() {
        return availableVolume;
    }

    public void setAvailableVolume(String[] availableVolume) {
        this.availableVolume = availableVolume;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    public String getShipmentType() {
        return shipmentType;
    }

    public void setShipmentType(String shipmentType) {
        this.shipmentType = shipmentType;
    }

    public String[] getTruckDepDate() {
        return truckDepDate;
    }

    public void setTruckDepDate(String[] truckDepDate) {
        this.truckDepDate = truckDepDate;
    }

    public String getOrderReferenceAwbNo() {
        return orderReferenceAwbNo;
    }

    public void setOrderReferenceAwbNo(String orderReferenceAwbNo) {
        this.orderReferenceAwbNo = orderReferenceAwbNo;
    }

    public String[] getVehicleIds() {
        return vehicleIds;
    }

    public void setVehicleIds(String[] vehicleIds) {
        this.vehicleIds = vehicleIds;
    }

    public String[] getVehicleRegNo() {
        return vehicleRegNo;
    }

    public void setVehicleRegNo(String[] vehicleRegNo) {
        this.vehicleRegNo = vehicleRegNo;
    }

    public String getProductRate() {
        return productRate;
    }

    public void setProductRate(String productRate) {
        this.productRate = productRate;
    }

    public String[] getVolumes() {
        return volumes;
    }

    public void setVolumes(String[] volumes) {
        this.volumes = volumes;
    }

    public String getCommodity() {
        return commodity;
    }

    public void setCommodity(String commodity) {
        this.commodity = commodity;
    }

    public String[] getClassCode() {
        return classCode;
    }

    public void setClassCode(String[] classCode) {
        this.classCode = classCode;
    }

    public String[] getPkgInstruction() {
        return pkgInstruction;
    }

    public void setPkgInstruction(String[] pkgInstruction) {
        this.pkgInstruction = pkgInstruction;
    }

    public String[] getPkgGroup() {
        return pkgGroup;
    }

    public void setPkgGroup(String[] pkgGroup) {
        this.pkgGroup = pkgGroup;
    }

    public String[] getReceivedPackages() {
        return receivedPackages;
    }

    public void setReceivedPackages(String[] receivedPackages) {
        this.receivedPackages = receivedPackages;
    }

//    CBT
    public String[] getGrossWG() {
        return grossWG;
    }

    public void setGrossWG(String[] grossWG) {
        this.grossWG = grossWG;
    }

    public String getOnwardReturnType() {
        return onwardReturnType;
    }

    public void setOnwardReturnType(String onwardReturnType) {
        this.onwardReturnType = onwardReturnType;
    }

    public String getContractVehicleTypeId() {
        return contractVehicleTypeId;
    }
    
    public void setContractVehicleTypeId(String contractVehicleTypeId) {
        this.contractVehicleTypeId = contractVehicleTypeId;
}

    public String getConsignmentOrderId() {
        return consignmentOrderId;
    }

    public void setConsignmentOrderId(String consignmentOrderId) {
        this.consignmentOrderId = consignmentOrderId;
    }

    public String getRouteConsContractId() {
        return routeConsContractId;
    }

    public void setRouteConsContractId(String routeConsContractId) {
        this.routeConsContractId = routeConsContractId;
    }

    public String[] getTatTime() {
        return tatTime;
    }

    public void setTatTime(String[] tatTime) {
        this.tatTime = tatTime;
    }

    public String getDeliveryService() {
        return deliveryService;
    }

    public void setDeliveryService(String deliveryService) {
        this.deliveryService = deliveryService;
    }

    public String getNewRouteFlag() {
        return newRouteFlag;
    }

    public void setNewRouteFlag(String newRouteFlag) {
        this.newRouteFlag = newRouteFlag;
    }

    public String getOnwardReturn() {
        return onwardReturn;
    }

    public void setOnwardReturn(String onwardReturn) {
        this.onwardReturn = onwardReturn;
    }

    public String getOnwardTrip() {
        return onwardTrip;
    }

    public void setOnwardTrip(String onwardTrip) {
        this.onwardTrip = onwardTrip;
    }

    public String getNoOfTrucks() {
        return noOfTrucks;
    }

    public void setNoOfTrucks(String noOfTrucks) {
        this.noOfTrucks = noOfTrucks;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public String getWalkinPanNo() {
        return walkinPanNo;
    }

    public void setWalkinPanNo(String walkinPanNo) {
        this.walkinPanNo = walkinPanNo;
    }

    public String getWalkinGSTNo() {
        return walkinGSTNo;
    }

    public void setWalkinGSTNo(String walkinGSTNo) {
        this.walkinGSTNo = walkinGSTNo;
    }

    public String getWalkinConsoignor() {
        return walkinConsoignor;
    }

    public void setWalkinConsoignor(String walkinConsoignor) {
        this.walkinConsoignor = walkinConsoignor;
    }

    public String getWalkinConsignee() {
        return walkinConsignee;
    }

    public void setWalkinConsignee(String walkinConsignee) {
        this.walkinConsignee = walkinConsignee;
    }

    
}
