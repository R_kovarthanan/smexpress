/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ets.domain.operation.business;

/**
 *
 * @author Administrator
 */
public class TripFuelDetailsTO {


private String tripBunkName = "";
private String tripBunkPlace = "";
private String tripBunkPlaceEntry = "";
private String tripFuelDate = "";
private String tripFuelAmounts = "";
private String tripFuelLtrs = "";
private String tripFuelPerson = "";
private String tripFuelRemarks = "";
private String fuelLocatId = "";


private String category = "";
private String ledgerId = "";
private String expenseId = "";
private String tripExpenseId = "";
private String ledgerCode = "";
private String tripSheetId = "";
private String returnTripId = "";
private String amount = "";
private String tdate = "";
private String liter = "";
private String remark = "";
private String mode = "";
private String bunkId = "";
private String bunkName = "";

    public String getBunkId() {
        return bunkId;
    }

    public void setBunkId(String bunkId) {
        this.bunkId = bunkId;
    }

    public String getBunkName() {
        return bunkName;
    }

    public void setBunkName(String bunkName) {
        this.bunkName = bunkName;
    }

    
    public String getTripBunkName() {
        return tripBunkName;
    }

    public void setTripBunkName(String tripBunkName) {
        this.tripBunkName = tripBunkName;
    }

    public String getTripBunkPlace() {
        return tripBunkPlace;
    }

    public void setTripBunkPlace(String tripBunkPlace) {
        this.tripBunkPlace = tripBunkPlace;
    }

    public String getTripFuelAmounts() {
        return tripFuelAmounts;
    }

    public void setTripFuelAmounts(String tripFuelAmounts) {
        this.tripFuelAmounts = tripFuelAmounts;
    }

    public String getTripFuelDate() {
        return tripFuelDate;
    }

    public void setTripFuelDate(String tripFuelDate) {
        this.tripFuelDate = tripFuelDate;
    }

    public String getTripFuelLtrs() {
        return tripFuelLtrs;
    }

    public void setTripFuelLtrs(String tripFuelLtrs) {
        this.tripFuelLtrs = tripFuelLtrs;
    }

    public String getTripFuelPerson() {
        return tripFuelPerson;
    }

    public void setTripFuelPerson(String tripFuelPerson) {
        this.tripFuelPerson = tripFuelPerson;
    }

    public String getTripFuelRemarks() {
        return tripFuelRemarks;
    }

    public void setTripFuelRemarks(String tripFuelRemarks) {
        this.tripFuelRemarks = tripFuelRemarks;
    }

    public String getFuelLocatId() {
        return fuelLocatId;
    }

    public void setFuelLocatId(String fuelLocatId) {
        this.fuelLocatId = fuelLocatId;
    }
    
    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLiter() {
        return liter;
    }

    public void setLiter(String liter) {
        this.liter = liter;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTdate() {
        return tdate;
    }

    public void setTdate(String tdate) {
        this.tdate = tdate;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getTripBunkPlaceEntry() {
        return tripBunkPlaceEntry;
    }

    public void setTripBunkPlaceEntry(String tripBunkPlaceEntry) {
        this.tripBunkPlaceEntry = tripBunkPlaceEntry;
    }

    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }

    public String getLedgerCode() {
        return ledgerCode;
    }

    public void setLedgerCode(String ledgerCode) {
        this.ledgerCode = ledgerCode;
    }

    public String getLedgerId() {
        return ledgerId;
    }

    public void setLedgerId(String ledgerId) {
        this.ledgerId = ledgerId;
    }

    public String getReturnTripId() {
        return returnTripId;
    }

    public void setReturnTripId(String returnTripId) {
        this.returnTripId = returnTripId;
    }

    public String getTripSheetId() {
        return tripSheetId;
    }

    public void setTripSheetId(String tripSheetId) {
        this.tripSheetId = tripSheetId;
    }

    public String getTripExpenseId() {
        return tripExpenseId;
    }

    public void setTripExpenseId(String tripExpenseId) {
        this.tripExpenseId = tripExpenseId;
    }

    
    
    

}
