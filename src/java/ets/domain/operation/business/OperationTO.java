
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.operation.business;

import java.util.ArrayList;
import java.io.*;

public class OperationTO implements Serializable {
    
    private String vehicleNus = "";
    private String newRouteFlag = "";
    private String onwardReturn = "";
    private String onwardTrip = "";
    private String originlatitude = "";
    private String originlongitude = "";
    private String destinationlatitude = "";
    private String destinationlongitude = "";
    private String billType = "";
    private String walkinPanNo = "";
    private String walkinGSTNo = "";
    private String walkinConsoignor = "";
    private String walkinConsignee = "";
    private String vehId = "";
    private String noOfTrucks = "";
    
    
    
    private String routeTAT = "";
    private String packageDescription = "";
    private String movementTypeId = "";
    private String movementTypeName = "";
    
    private String tripEndDate = "";
    private String deliveryService = "";
    private String tatTime = "";
    private String penality = "";
    private String timeSlotTo = "";
    private String timeSlot = "";
    private String chargeamount = "";
    private String pcmunit = "";
    private String pcmremarks = "";
    private String detention = "";
    private String chargeamt = "";
    private String dcmunit = "";
    private String dcmunitTo = "";
    private String dcmremarks = "";
    private String companyID1 = "";
    private String routeConsContractId = "";
    private String fuelInvoiceNo = "";
    private String pointNames = "";
    private String companyId = "";
    private String userMovementType = "";
    private String employeeId = "";
    
    //
    private String delAdress = "";
    private String grossWeights = "";
    private String cusRefDate = "";
    private String KG = "";
    private String pendingPackages = "";
    private String pendingWeight = "";
    private String[] gcnNos = null;
    private String[] consigDate = null;
    private String[] custRefDate = null;
    private String gstNo = "";
    private String panNo = "";
    private String walkinCustomerId = "";
    private String consignorId = "";
    private String consigneeId = "";
    private String temp = "";
    private String[] phoneNos = null;
    private String[] consigneeIds = null;
    private String[] consignorIds = null;
    
    // cancellationInvoice
     private String cancellationInvoice = "";
     private String onwardReturnType = "";
    //    CBT
     private String airWayBillNo = "";
    private String awbNo = "";
    private String awbType = "";
    private String allowedTrucks = "";
    private String truckNos = "";
    private String fuelBillFlag = "";
    
//    private String tripStatusId = "";
    private String totalVolumeActual = "";
    private String allBranchAccess = "";
    private String totalBooking = "";
    private String totalGrossWeight = "";
    private String totalChargeableWeight = "";
    private String awbTotalGrossWeight = "";
    private String awbTotalPackages = "";
    private String awbReceivedPackages = "";
    private String awbPendingPackages = "";
    
        private String wareHouseName = "";
    private String wareHouseLocation = "";
    private String shipmentAcceptanceDate = "";
    private String shipmentAccpHour = "";
    private String shipmentAccpMinute = "";
    private String currencyType = "";
    private String rateMode = "";
    private String[] bookingReferenceRemarks = null;
    private String orderDeliveryHour = "";
    private String orderDeliveryMinute = "";
    private String perKgRate = "";
    private String bookingRemarks = "";

    private String vehicleInfo = "";
    private String vehicleCapacity = "";
    
    private String truckCode = "";
    private String fleetCode = "";
    private String fleetCapacity = "";
    private String fleetVolume = "";
    private String fleetName = "";
    private String fleetId = "";    
    private String truckName = "";

    private String awbOriginId = "";
    private String awbOriginName = "";
    private String awbDestinationName = "";
    private String awbDestinationId = "";
    private String length = "";
    private String breadth = "";
    private String height = "";
    private String chargeAbleWeigth = "";
    private String volume = "";
    private String originId = "";
    private String destinationId = "";
    private String originName = "";
    private String destinationName = "";
    
    private String []truckOriginId ;    
    private String []availableCap ;
    private String []assignedCap ;    
    
    
    private String rateMode1="";
    private String rateValue1="";    
    private String departOnTime="";    
    private String truckAvailable="";
    private String truckAssigned="";
    private String consignmentRouteCourseId="";
    private String customsStatus="";
    private String []consArticleId;
    private String []consRouteCourseId;

    private String barCode = "";
      private String airWiseBillNo = "";
      private String loadStatus = "";
      private String loadTime = "";
      private String customerDoc = "";
      private String unloadStatus = "";
      private String unloadTime = "";
      private String podRecieved = "";
      private String awbOriginRegion = "";
      private String orderReferenceEnd = "";
      
      private int parcelNo = 0;

    private String originCityId = "";
    private String consignmentOrder = "";
    private String branchId = "";
    private String []usedVol ;
        private String []usedCapacity ;


        private String reasonId = "";
      private String reasonCode = "";
      private String reasonDescription = "";
      private String truckId = "";
      private String truckCodeId = "";
      private String[] contractFreightTable1 = null;
      
//      sesha
      private String categoryCode = "";
    private String categoryName = "";
    private String sacCode = "";
    private String sacDescription = "";
    private String gstCategoryId = "";
    private String description = "";
    private String gstCategoryName = "";
    private String categoryId = "";
    private String gstCodeMasterId = "";
    private String gstName = "";
    private String state = "";
    private String gstCode = "";
    private String gstPercentage = "";
    private String validFrom = "";
    private String validTo = "";
    private String gstRateDetailId = "";
    private String gstCategoryCode = "";
    private String gstRateId = "";
    private String hsnName = "";
    private String hsnCode = "";
    private String hsnDescription = "";
    private String gstProductId = "";
    private String gstType = "";
    private String gstProductCategoryCode = "";
    private String stateName = "";
//    private String activeInd = "";
    //new changes on 26-02-2015
    
    private String orderReferenceAwb = "";
    private String cityRegion = "";
    private String[] truckTravelKm = null;
    private String[] truckTravelHour = null;
    private String[] truckTravelMinute = null;
    private String[] fleetTypeId = null;
    private String[] truckDestinationId = null;
    private String[] truckRouteId = null;
    private String rateValue = "";
    private String contract = "";
    private String freightReceipt = "";
    private String[] netUnits = null;
    private String[] netQuantity = null;
    private String awbOrigin = "";
    private String awbDestination = "";
    private String awbOrderDeliveryDate = "";
    private String awbDestinationRegion = "";
    private String awbMovementType = "";
    private String[] noOfPieces = null;
    private String[] grossWeight = null;
    private String[] chargeableWeight = null;
    private String[] chargeableWeightId = null;
    private String[] lengths = null;
    private String[] widths = null;
    private String[] heights = null;
    private String[] dgHandlingCode = null;
    private String[] unIdNo = null; 
    private String totalChargeableWeights = "";
    private String totalVolume = "";
    private String[] availableWeight = null;
    private String[] availableVolume = null;
    private String airlineName = "";
    private String shipmentType = "";
    private String[] truckDepDate = null;
    private String orderReferenceAwbNo = "";
    private String[] vehicleIds = null;
    private String[] vehicleRegNo ;
    private String productRate = "";
    private String[] volumes = null;
    private String commodity = "";
    private String[] classCode = null;
    private String[] pkgInstruction = null;
    private String[] pkgGroup = null;
    private String []receivedPackages ;
//    CBT
//    <GST>
    private String[] consignorNames = null;
    private String[] consignorPhoneNos = null;
    private String[] consignorAddresss = null;
    private String[] consigneeNames = null;
    private String[] consigneePhoneNos = null;
    private String[] consigneeAddresss = null;

    private String billToCustomer = "";
    private String reimbursement = "";
//    private String activeind = "";

    private String googleCityName = "";
    private String latitude = "";
    private String longitude = "";
    private String jobCardTypeNew = "";
    private String jobCardCode = "";
    private String jobCardRemarks = "";
    private String jobCardIssueDate = "";
    private String[] routeCostIds = null;
    private String[] fixedKmStatus = null;
    private String[] editId = null;
    private String[] fixedKmvehicleTypeId = null;
    private String[] vehicleNos = null;
    private String[] fixedTotalKm = null;
    private String[] fixedTotalHm = null;
    private String[] fixedRateWithReefer = null;
    private String[] fixedExtraKmRateWithReefer = null;
    private String[] fixedExtraHmRateWithReefer = null;
    private String[] fixedRateWithoutReefer = null;
    private String[] fixedExtraKmRateWithoutReefer = null;
    private String outStandingDate = "";
    private String routeCostId = "";
    private String emptyTripPurpose = "";
    private String contractFixedRateId = "";
    private String contractVehicleNos = "";
    private String extraKmRatePerKmRateWithReefer = "";
    private String extraHmRatePerHmRateWithReefer = "";
    private String extraKmRatePerKmRateWithoutReefer = "";
    private String totalHm = "";
    private String extraKmRateWithReefer = "";
    private String extraHmRateWithReefer = "";
    private String extraKmRateWithoutReefer = "";
    private String tripEndHm = "";
    private String serviceVendorId = "";
    private String priorityId = "";
    private String reqHour = "";
    private String reqMinute = "";
    private String reqSeconds = "";
    private String km = "";
    private String jobCardType = "";
    private String sectionId = "";
    private String sectionName = "";
    private String serviceTypeName = "";
    private String serviceTypeId = "";
    private String usageTypeId = "";
    private String closedBy = "";
    private String actualCompletionDate = "";
    private String plannedCompleteHours = "";
    private String plannedCompleteDays = "";
    private String jobcardHours = "";
    private String jobcardDays = "";
    private double creditLimitAmount = 0;
    private double outStanding = 0;
    private double deviateAmount = 0;
    private double tripRevenue = 0;
    private double paidAdvance = 0;
    private String wfuDay = "";
    private String tripStatusId = "";
    private String approvedBy = "";
    private String amountExceed = "";
    private String estimatedExpense = "";
    private String totalRequestAmount = "";
    private String tripPod = "";
    private String nextStatusId = "";
    private String userName = "";
    private String timeApprovalStatus = "";
    private String emailTo = "";
    private String emailCc = "";
    private String infoEmptyTo = "";
    private String infoEmailCc = "";
    private String paymentTypeId = "";
    private String productCategory = "", productCategoryUnchecked = "", tempMinimumTemp = "", tempMaximumTemperature = "", tempReeferRequired = "",
            temp1MinimumTemp = "", temp1MaximumTemperature = "", temp1ReeferRequired = "";
    private double factors = 0;
    private String bpclTransactionId = "";
    private String fuelTypeId = "";
    private String fuelTypeName = "";
    private String fuelUnite = "";

    private String infoEmailTo = "";

    private String fuelType = "";
    private String advanceStatus = "";
    private String advanceToPayStatus = "";
    private String paidAmount = "";
    private String paymentModeId = "";
    private String paymentModeName = "";
    private String paymentType = "";
    private String rtgsNo = "";
    private String rtgsRemarks = "";
    private String chequeNo = "";
    private String chequeRemarks = "";
    private String draftNo = "";
    private String draftRemarks = "";
    private float totalRevenue = 0;
    private float totalExp = 0;
    private String custCode = "";
    private String custType = "";
    private String custContactPerson = "";
    private String accountManagerId = "";
    private String accountManager = "";
    private String creditLimit = "";
    private String creditDays = "";
    private String fleetCenterContactNo = "";
    private String mobileNo = "";
    private String[] articleId = null;
    private String consignmentArticleId = "";
    private String batchName = "";
    private String unitOfMeasurement = "";
    private String contractVehicleTypeId = "";
    private String distanceTravelled = "";
    private String tripAdvaceId = "";
    private String currentTemperature = "";
    private String actualKmVehicleTypeId = "";
    private String smtp = "";
    private String port = "";
    private String emailId = "";
    private String password = "";
    private String tomailId = "";
    //Senthil 10-12-2013
    private String consignmentRefNo = "";
    private String pickupdate = "";
    private String pickupTime = "";
    private String consignmentStatus = "";
    private String consignmentVehicleId = "";
    private String consignmentVehicleStatus = "";
    private String configId = "";
    private String requesttype = "";
    private String[] batchCode = null;
    private String[] uom = null;
    private String totalrunkm = "";
    private String totalrunhm = "";
    private String rcmexpense = "";
    private String systemexpense = "";
    private String fleetCenterId = "";
    private String totalWeights = "";
    private String tripPlanningRemark = "";
    private String consigmentId = "";
    private String invoicecustomer = "";
    private String totalDrivers = null;
    private String totFreightCharges = null;
    private String totalkmrun = null;
    private double totalTripFreightAmt = 0.0;
    private double totalExpenseAmt = 0.0;
    private int totaldrivers = 0;
    private double totalTripKmRun = 0;
    private double totalFreightAmt = 0;
    //Senthil 10-12-2013
    //Mathan 10-12-2013
    private String productCategoryName = "";
    private String id = "";
    private String reeferMinimumTemperature = "";
    private String reeferMaximumTemperature = "";
    private String parameterName = "";
    private String parameterValue = "";
    private String parameterUnit = "";
    private String parameterDescription = "";
    private String frequencyId = "";
    private String frequencyName = "";
    private String alertName = "";
    private String alertDes = "";
    private String alertBased = "";
    private String alertRaised = "";
    private String alerttoEmail = "";
    private String alertccEmail = "";
    private String alertsemailSub = "";
    private String alertsfrequently = "";
    private String alertRaised1 = "";
    private String escalationtoEmail1 = "";
    private String escalationccEmail1 = "";
    private String escalationemailSub1 = "";
    private String repeat = "";
    private String alertid = "";
    private String alertstatus = "";
    private String checkListName = "";
    private String checkListStage = "";
    private String checkListDate = "";
    private String checkListId = "";
    private String checkListStatus = "";
    private String stage = "";
    private String stageName = "";
    private String StageId = "";
    private String cityName = "";
    private String cityState = "";
    private String zoneid = "";
    private String zoneName = "";
    private String zoneId = "";
    //Mathan 10-12-2013
    private String approvestatus = "";
    private String statusName = "";
    private String approveremarks = "";
    private String requestedadvance = "";
    private String batchType = "";
    private String advicedate = "";
    private String isactive = "";
    private String pointOrder = null;
    private String contractRouteOrigin = null;
    private String expectedArrivalTime = null;
    private String expectedArrivalDate = null;
    private String tripStartDate = null;
    private String tripstartkm = null;
    private String tripendkm = null;
    private String freightcharges = null;
    private String customerContractId = "";
    private String consignmentOrderRefRemarks = "";
    private String customerPincode = "";
    private String customerMobile = "";
    private String customerPhone = "";
    private String customerEmail = "";
    private String customerBillingType = "";
    private String budinessType = "";
    private String consigmentInstruction = "";
    private String vehicleRequiredTime = "";
    private String consignorMobile = "";
    private String consigneeMobile = "";
    private String invoiceType = "";
    private String kmRate = "";
    private String kgRate = "";
    private String fixedRate = "";
    private String totalPackages = "";
    private String totalWeight = "";
    private String totalDistance = "";
    private String freightCharges = "";
    private String totalStatndardCharges = "";
    private String consigmentOrderStatus = "";
    private String consignmentPointId = "";
    private String consignmentPointType = "";
    private String consignmentPointAddress = "";
    private String pointSequence = "";
    private String pointPlanTime = "";
    private String pointDate = "";
    private String pointAddress = "";
    private String consignmentOrderNo = "";
    private String articleCode = "";
    private String articleName = "";
    private String packageNos = "";
    private String packageWeight = "";
    //Vehicle Driver Mapping
    private String mappingId = "";
    private String regNo = "";
    private String primaryDriverId = "";
    private String primaryDriverName = "";
    private String secondaryDriverIdOne = "";
    private String secondaryDriverNameOne = "";
    private String secondaryDriverIdTwo = "";
    private String secondaryDriverNameTwo = "";
    private String approvalstatus = "";
    private String estimatedadvance = "";
    private String cnoteName = "";
    private String actualadvancepaid = "";
    private String priority1 = "";
    private String mfrId = "";
    private int servicetypeId = 0;
    private int compId = 0;
    private int cnoteCount = 0;
    private String servicetypeName = "";
    private String driverVendorId = "";
    private String vehicleVendorId = "";
    private String compName = "";
    private String bayNo = "";
    private String technicianId = "";
    private String technicianName = "";
    private String estimatedHrs = "";
    private String actualHrs = "";
    private String roleId = "";
    private int userId = 0;
    private int workOrderId = 0;
    private String regno = "";
    private ArrayList activity = new ArrayList();
    private ArrayList periodicServiceList = new ArrayList();
    private int priority = 0;
    private String activityName = "";
    private String[] pointRouteId = null;
    private int elapsedDays = 0;
    private String reqDate = "";
    private String hour = "";
    private String minute = "";
    private String sec = "";
    private String remarks = "";
    private String cRemarks = "";
    private int kmReading = 0;
    private int vehicleId = 0;
    private int serviceLocationId = 0;
    private String driverName = "";
    private String serviceLocation = "";
    private String dateOfIssue = "";
    private String vehicleTypeName = "";
    private String chassNo = "";
    private String usageName = "";
    private String engineNo = "";
    private String mfrName = "";
    private String modelName = "";
    private String secName = "";
    private String probName = "";
    private String desc = "";
    private String saleDate = "";
    int severity = 0;
    private String symptoms = "";
    private String scheduledDeleivery = "";
    private String scheduledDate = "";
    private String status = "";
    private String serviceName = "";
    private String cust = "";
    private int jobCardId = 0;
    private String jcMYFormatNo = "";
    private int serviceId = 0;
    private int balanceKm = 0;
    private int balanceHm = 0;
    private int probId = 0;
    private int empId = 0;
    private int secId = 0;
    private int vendorId = 0;
    private int model = 0;
    private float totalAmount = 0;
    private String vendorName = "";
    private String identifiedby = "";
    private String servicedDate = "";
    private String closedDate = "";
    private String lastServicedKm = "";
    private String custName = "";
    private String custAddress = "";
    private String custCity = "";
    private String custState = "";
    private String custPhone = "";
    private String custMobile = "";
    private String custEmail = "";
    private int check = 0;
    private int currentKM = 0;
    private int hourMeter = 0;
    private int currentHM = 0;
    private int flag = 0;
    private int mrsId = 0;
    private int itemId = 0;
    private int reqQty = 0;
    private int issQty = 0;
    private int woId = 0;
    private String vendorAddress = "";
    private String woComp = "";
    private String problemName = "";
    private String phone = "";
    private String createdDate = "";
    private String pcd = "";
    private String intime = "";
    private String totalKm = "0";
    private String[] address = null;
    private String[] problemNameSplit = null;
    private String[] symptomsSplit = null;
    //Hari
    private int billNo = 0;
//   bala
    private int customertypeId = 0;
    private String customertypeName = "";
    private String settlementType = "";
    //---------- Trip details ------
    private String tripSheetId = "";
    private String tripCode = "";
    private String tripRouteId = "";
    private String tripVehicleId = "";
    private String tripScheduleId = "";
    private String tripScheduleDateTime = "";
    private String tripScheduleDate = "";
    private String tripScheduleTime = "";
    private String timeLeft = "";
    private String timeLeftValue = "";
    private String tripDriverId = "";
    private String tripDate = "";
    private String tripDepartureDate = "";
    private String tripArrivalDate = "";
    private String tripKmsOut = "";
    private String tripKmsIn = "";
    private String tripTotalLitres = "";
    private String tripFuelAmount = "";
    private String tripTotalAllowances = "";
    private String tripTotalExpenses = "";
    private String expenseName = "";
    private String expenseId = "";
    private String tripTotalKms = "";
    private String tripBalanceAmount = "";
    private String tripStatus = "";
    //tripId,identityNo,deviceId,routeName,outKM,driverName,outDateTime,inKM,location,inDateTime,status
    private String tripId = "";
    private String returnTripId = "";
    private String identityNo = "";
    private String deviceId = "";
    private String routeName = "";
    private String outKM = "";
    private String outDateTime = "";
    private String inKM = "";
    private String location = "";
    private String inDateTime = "";
    private String settlementFlag = "";
    private String issuerName = "";
    private String designation = "";
    private String amount = "";
    private String advDatetime = "";
    private String fuelName = "";
    private String bunkName = "";
    private String liters = "";
    private String fuelDatetime = "";
    private String inOutIndication = "";
    private String inOutDateTime = "";
    private String locationName = "";
    private String tonnageRate = "0";
    private String totalTonnage = "";
    private String deliveredTonnage = "";
    private String shortage = "";
    private String empName = "";
    private String vehicleNo = "";
    private String embarkDate = "";
    private String alightDate = "";
    private String alightStatus = "";
    private String createdOn = "";
    private String expensesDesc = "";
    private String serviceVendor = "";
    private String locationId = "";
    private String custId = "";
    private String tripType = "";
    private String totalTonAmount = "";
    private String issuerId = "";
    private String bunkId = "";
    private String driverId = "";
    private String cleanerStatus = "";
    private String depHours = "";
    private String depMints = "";
    private String arrHours = "";
    private String arrMints = "";
    private String vehicleTypeId = "";
    private String flagString = "";
    private String totalFuel = "";
    private String totalFuelAmount = "";
    private String mileage = "";
//    CLPL Trip sheet start
    private String stageId = "";
    private String tonnage = "";
    private String bags = "";
    private String orderNo = "";
    private String pinkSlip = "";
    private String tripdateP = "";
    private String tolocation = "";
//    CLPL Trip sheet end
    // CLPL  trip management
    private String lpsID = "";
    private String lpsNumber = "";
    private String partyName = "";
    private String lpsDate = "";
    private String billStatus = "";
    private String orderNumber = "";
    private String contractor = "";
    private String packerNumber = "";
    private String route = "";
    private String quantity = "";
    private String destination = "";
    private String productName = "";
    private String packing = "";
    private String clplPriority = "";
    private String active_ind = "";
    private String productId = "";
    private String productCode = "";
    private String lorryNo = "";
    private String gatePassNo = "";
    private String watchandward = "";
    private String date = "";
    private String lpsNo = "";
    private String returnAmt = "";
    private String returnExp = "";
    private String totalBalAmtRt = "";
    private String totalExpensesAmtRt = "";
    //Pink slip
    private String registerNo = "";
    private String driName = "";
    private String routeId = "";
    private String selectedRouteId = "";
    private String selectedLpsIds = "";
    private String pinkSlipID = "";
    private String routeID = "";
    private String vehicleID = "";
    private String driverID = "";
    private String vehicleTonnage = "";
    private String ownership = "";
    private String selectedBillStatus = "";
    //CLPL Invoice
    private String openDateTime = "";
    private String customerId = "";
    private String customerName = "";
    private String closeDateTime = "";
    private String totalallowance = "";
    private String totalamount = "";
    private String totalexpenses = "";
    private String balanceamount = "";
    private String totalkms = "";
    private String revenue = "";
    private String act_Ind = "";
    private String cust_Name = "";
    private String emp_Name = "";
    private String fromDate = "";
    private String toDate = "";
    private String remark = "";
    private String totalInvAmount = "";
    private String noOfTrip = "";
    private String taxAmount = "";
    private String vehicleid = "";
    private String invoiceCode = "";
    private String invRefCode = "";
    private String invoiceDate = "";
    private String grandTotal = "";
    private String invoiceStatus = "";
    private String numberOfTri = "";
    private String invoiceFor = "";
    private String invoiceDetailId = "";
    private String tripNo = "";
    private String freightAmount = "";
    private String ledgerName = "";
    private String ledgerId = "";
    private String invoiceId = "";
    private String summary = "";
    //Market Vehicle Settlement
    private String totalDeliveredTonnage = "";
    private String commissionPercentage = "";
    private String totalTannage = "";
    private String commissionAmount = "";
    private String settlementAmount = "";
    private String totalshortage = "";
    private String totalregno = "";
    private String LedgerCode = "";
    private String settlementid = "";
    private String numberOfTrip = "";
    private String commisssionAmount = "";
    private String totalAmountL = "";
    private String createddate = "";
    private String voucherCode = "";
    private String totalamaount = "";
    private String productname = "";
    //clpl trip edit
    private String GPSKm = "";
    private String returnTripProduct = "";
    private String returnFromLocation = "";
    private String returnToLocation = "";
    private String returnDate = "";
    private String returnTonnage = "";
    private String returnLoadedTonnage = "";
    private String returnLoadedDate = "";
    private String returnDeliveredTonnage = "";
    private String returnDeliveredDate = "";
    private String shortageTonnage = "";
    private String returnAmount = "";
    private String returnExpenses = "";
    private String totalBalAmtRetuen = "";
    private String totalExpensesAmtReturn = "";
//        clpl close trip
    private String tripReceivedAmount = "";
    private String fromLocationid = "";
    private String toLocationid = "";
    private String fromLocation = "";
    private String toLocation = "";
    private String returnTripType = "";
    private String returnProductName = "";
    private String returnKM = "";
    private String returnTon = "";
    private String deliveredDANo = "";
    private String loadedSlipNo = "";
    private String loadedTonnage = "";
    private String fLocationName = "";
    private String fLocationId = "";
    private String tLocationName = "";
    private String tLocationId = "";
    private String voucherNo = "";
    private String vDate = "";
    private String accountDetail = "";
    private String accountEntryID = "";
    private String accountEntryDate = "";
    private String accountsAmount = "";
    private String accountsType = "";
    private String narration = "";
    private String ledgerID = "";
    private String detailCode = "";
    private String invoiceNo = "";
    private String gpno = "";
    private String branchLedgerName = "";
    private String driverLedgerName = "";
    private int sno = 0;
    private String selectedRow = "";
    private String crossingType = "";
    private String crossingAmount = "";
    private String tonnageRateMarket = "";
    private String twoLpsStatus = "";
    private String tripRevenueOne = "";
    private String tripRevenueTwo = "";
    private String selectedRouteName = "";
    private String selectedProductName = "";
    private String selectedPacking = "";
    //Print page
    private String totalBags = "";
    private String billStatusNew = "";
//    Brattle Foods
    private String modelId = "";
    private String vehicleMileage = "";
    private String reeferMileage = "";
    private String routeCode = "";
    private String travelTime = "";
    private String distance = "";
    private String reeferRunning = "";
    private String roadType = "";
    private String tollAmount = "";
    private String fuelCost = "";
    private String[] mfrIds = null;
    private String[] modelIds = null;
    private String[] vehTypeId = null;
    private String[] vehMileage = null;
    private String[] vehExpense = null;
    private String[] reeferConsump = null;
    private String[] reeferExpense = null;
    private String[] varRate = null;
    private String[] varCost = null;
    private String[] totExpense = null;
    private String routeIdFrom = "";
    private String routeIdTo = "";
    private String editRouteId = "";
    private String fuelPrice = "";
    private String routeFrom = "";
    private String routeTo = "";
    private String vehicleExpense = "";
    private String reeferExpenses = "";
    private String variableCostPerKm = "";
    private String variableExpense = "";
    private String totalExpense = "";
    private String effectiveDate = "";
    private String cityFromId = "";
    private String cityFromName = "";
    private String cityToId = "";
    private String cityToName = "";
    private String travelHour = "";
    private String travelMinute = "";
    private String reeferHour = "";
    private String reeferMinute = "";
    private String tollAmountType = "";
    private String avgTollAmount = "";
    private String avgMisCost = "";
    private String avgDriverIncentive = "";
    private String avgFactor = "";
    private String[] fuelCostPerKm = null;
    private String[] fuelCostPerHr = null;
    private String[] tollAmounts = null;
    private String[] miscCostKm = null;
    private String[] driverIncenKm = null;
    private String[] factor = null;
    private String[] varExpense = null;
    private String city = "";
    private String[] fuelCostPerKms = null;
    private String[] fuelCostPerHrs = null;
    private String[] reefMileage = null;
    private String fuelCostKm = "";
    private String fuelCostHr = "";
    private String tollAmountperkm = "";
    private String miscCostperkm = "";
    private String driverIncentperkm = "";
    private String variExpense = "";
    private String vehiExpense = "";
    private String reefeExpense = "";
    private String totaExpense = "";
    private String effectivDate = "";
    private String stChargeName = "";
    private String stChargeDesc = "";
    private String stChargeUnit = "";
    private String stChargeId = "";
    private String unitName = "";
    private String billingTypeId = "";
    private String billingTypeName = "";
    private String elementName = "";
    private String elementValue = "";
    private String rowCount = "";
    private String ptpPickupPoint = "";
    private String interimPoint = "";
    private String contractNo = "";
    private String customerCode = "";
    private String contractFrom = "";
    private String contractTo = "";
    private String[] ptpRouteContractCode = null;
    private String[] ptpMfrIds = null;
    private String[] ptpPickupPoints = null;
    private String[] interimPoint1 = null;
    private String[] interimPointId1 = null;
    private String[] interimPoint1Km = null;
    private String[] interimPoint2 = null;
    private String[] interimPointId2 = null;
    private String[] interimPoint2Km = null;
    private String[] interimPoint3 = null;
    private String[] interimPointId3 = null;
    private String[] interimPoint3Km = null;
    private String[] interimPoint4 = null;
    private String[] interimPointId4 = null;
    private String[] interimPoint4Km = null;
    private String[] ptpDropPoint = null;
    private String[] ptpDropPointId = null;
    private String[] ptpDropPointKm = null;
    private String[] ptpTotalKm = null;
    private String[] ptpRateWithReefer = null;
    private String[] ptpRateWithoutReefer = null;
    private String[] ptpwRouteContractCode = null;
    private String[] ptpwMfrIds = null;
    private String[] ptpwPickupPoint = null;
    private String[] ptpwDropPoint = null;
    private String[] ptpwPointId = null;
    private String[] ptpwTotalKm = null;
    private String[] ptpwRateWithReefer = null;
    private String[] ptpwRateWithoutReefer = null;
    private int contractId = 0;
    private String customerDetails = "";
    private String firstPickup = "";
    private String interimPickup1 = "";
    private String point1Id = "";
    private String point1Type = "";
    private String point2Type = "";
    private String point3Type = "";
    private String point4Type = "";
    private String point1Km = "";
    private String interimPickup2 = "";
    private String point2Id = "";
    private String point2Km = "";
    private String interimPickup3 = "";
    private String point3Id = "";
    private String point3Km = "";
    private String interimPickup4 = "";
    private String point4Id = "";
    private String point4Km = "";
    private String finalDrop = "";
    private String finalPointId = "";
    private String finalPointKm = "";
    private String rateWithReeferPerKm = "";
    private String rateWithoutReeferPerKm = "";
    private String rateWithReeferPerKg = "";
    private String rateWithoutReeferPerKg = "";
    private String reeferRupeesPerHour = "";
    private String vehicleRupeesPerKm = "";
    private String routeBillingId = "";
    private int routeContractId = 0;
    private String customerTypeId = "";
    private String customerTypeName = "";
    private String routeContractCode = "";
    private String interimPointName = "";
    private String cityId = "";
    private String endDate = "";
    private String lastPrice = "";
    private String priceDiff = "";
    private String fuelPriceId = "";
    private String accountManagerName = "";
    private String routeFromId = "";
    private String routeToId = "";

    /* Consignment Note */
    private String entryType = "";
    private String consignmentNoteNo = "";
    private String consignmentDate = "";
    private String orderReferenceNo = "";
    private String orderReferenceRemarks = "";
    private String productCategoryId = "";
    private String tripclosureid = "";
    private String customerAddress = "";
    private String pincode = "";
    private String customerMobileNo = "";
    private String mailId = "";
    private String customerPhoneNo = "";
    private String walkinCustomerName = "";
    private String walkinCustomerCode = "";
    private String walkinCustomerAddress = "";
    private String walkinPincode = "";
    private String walkinCustomerMobileNo = "";
    private String walkinMailId = "";
    private String walkinCustomerPhoneNo = "";
    private String origin = "";
    private String businessType = "";
    private String multiPickup = "";
    private String multiDelivery = "";
    private String consignmentOrderInstruction = "";
    private String[] productCodes = null;
    private String[] productNames = null;
    private String[] packagesNos = null;
    private String[] weights = null;
    private String serviceType = "";
    private String reeferRequired = "";
    private String contractRateId = "";
    private String vehicleRequiredDate = "";
    private String vehicleRequiredHour = "";
    private String vehicleRequiredMinute = "";
    private String vehicleInstruction = "";
    private String consignorName = "";
    private String consignorPhoneNo = "";
    private String consignorAddress = "";
    private String consigneeName = "";
    private String consigneePhoneNo = "";
    private String consigneeAddress = "";
    private String rateWithReefer = "";
    private String rateWithoutReefer = "";
    private String totalPackage = "";
    private String totalWeightage = "";
    private String totalHours = "";
    private String totalMinutes = "";
    private String totalPoints = "";
    private String totFreightAmount = "";
    private String subTotal = "";
    private String totalCharges = "";
    private String docCharges = "";
    private String odaCharges = "";
    private String multiPickupCharge = "";
    private String multiDeliveryCharge = "";
    private String handleCharges = "";
    private String otherCharges = "";
    private String unloadingCharges = "";
    private String loadingCharges = "";
    private String standardChargeRemarks = "";
    private String walkInBillingTypeId = "";
    private String walkinFreightWithReefer = "";
    private String walkinFreightWithoutReefer = "";
    private String walkinRateWithReeferPerKg = "";
    private String walkinRateWithoutReeferPerKg = "";
    private String walkinRateWithReeferPerKm = "";
    private String walkinRateWithoutReeferPerKm = "";

    /* Consignment Note */
    /*Contract Edit */
    private String firstPickupId = "";
    private String firstPickupName = "";
    private String point1Name = "";
    private String point1Hrs = "";
    private String point1Minutes = "";
    private String point1RouteId = "";
    private String point2Name = "";
    private String point2Hrs = "";
    private String point2Minutes = "";
    private String point2RouteId = "";
    private String point3Name = "";
    private String point3Hrs = "";
    private String point3Minutes = "";
    private String point3RouteId = "";
    private String point4Name = "";
    private String point4Hrs = "";
    private String point4Minutes = "";
    private String point4RouteId = "";
    private String finalPointName = "";
    private String prevPointId = "";
    private String finalPointHrs = "";
    private String finalPointMinutes = "";
    private String finalPointRouteId = "";
    private String vehicleRatePerKm = "";
    private String reeferRatePerHour = "";
    private String activeInd = "";
    /*Contract Edit */
    private String consignmentOrderId = "";
    private String consignmentOrderDate = "";
    private String customerType = "";
    private String consigmentOrigin = "";
    private String consigmentDestination = "";
    private String[] pointId = null;
    private String[] pointName = null;
    private String[] pointType = null;
    private String[] order = null;
    private String[] pointAddresss = null;
    private String[] deliveryAddress = null;
    private String[] weightKG = null;
    private String[] grossWG = null;
    private String[] invoicedNo = null;
    private String[] pointPlanDate = null;
    private String[] pointPlanHour = null;
    private String[] pointPlanMinute = null;
    private String[] multiplePointRouteId = null;
    private String endPointId = "";
    private String finalRouteId = "";
    private String endPointName = "";
    private String endPointType = "";
    private String endOrder = "";
    private String endPointAddresss = "";
    private String endPointPlanDate = "";
    private String endPointPlanHour = "";
    private String endPointPlanMinute = "";
    private String tripcode = "";
    private String planneddate = "";
    private String estimatedrevenue = "";
    private String estimatedtransitday = "";
    private String estimatedadvanceperday = "";
    private String tobepaidtoday = "";
    private String advancerequestamt = "";
    private String requeststatus = "";
    private String requeston = "";
    private String requestby = "";
    private String requestremarks = "";
    private String tripid = "";
    private String tripday = "";
    private String paidamt = "";
    private String paidstatus = "";
    private String estimatedexpense = "";
    private String TripActualAmount = "";

    //raju
    private String orderType = "";
    private String gcnNo = "";
    private String customerReference = "";
    private String editFreightAmount = " ";

//    17-11-2017
    private String[] minWgtBase = null;
    private String minimumBaseWgt = "";
    
    private String vendoreFSId = "";
    
    private String delhiPrice = "";
    private String mumbaiPrice = "";
    private String kolkataPrice = "";
    private String chennaiPrice = "";
    private String averagePrice = "";
    private String increasePrice = "";
    private String escalationPrice = "";
    private String escalationPercent = "";
    private String cityCode = "";
    
    private String fuelMonth = "";
    private String fuelYear = "";    
    
    private String vehicleTypeIds = "";
    private String touchPoint1 = "";
    private String touchPoint2 = "";
    private String pallets = "";
    private String vehicleType = "";
    private String flage = "";
    private String TSP = "";
    private String consignor = "";
    private String consignee = "";
    private String consignmentExcelUploadSize = "";
    private int counts = 0;
    private String first_docket_no = "";
    private String second_docket_no = "";
     private String uploadId = "";
     private String fileName = "";
      private byte profileImg[];
      
      
      

    public String getUploadId() {
        return uploadId;
    }

    public void setUploadId(String uploadId) {
        this.uploadId = uploadId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(byte[] profileImg) {
        this.profileImg = profileImg;
    }
    
  

    public String getFirst_docket_no() {
        return first_docket_no;
    }

    public void setFirst_docket_no(String first_docket_no) {
        this.first_docket_no = first_docket_no;
    }

    public String getSecond_docket_no() {
        return second_docket_no;
    }

    public void setSecond_docket_no(String second_docket_no) {
        this.second_docket_no = second_docket_no;
    }

    public String getVehicleTypeIds() {
        return vehicleTypeIds;
    }

    public void setVehicleTypeIds(String vehicleTypeIds) {
        this.vehicleTypeIds = vehicleTypeIds;
    }

    public String getTouchPoint1() {
        return touchPoint1;
    }

    public void setTouchPoint1(String touchPoint1) {
        this.touchPoint1 = touchPoint1;
    }

    public String getTouchPoint2() {
        return touchPoint2;
    }

    public void setTouchPoint2(String touchPoint2) {
        this.touchPoint2 = touchPoint2;
    }

    public String getPallets() {
        return pallets;
    }

    public void setPallets(String pallets) {
        this.pallets = pallets;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getFlage() {
        return flage;
    }

    public void setFlage(String flage) {
        this.flage = flage;
    }

    public String getTSP() {
        return TSP;
    }

    public void setTSP(String TSP) {
        this.TSP = TSP;
    }

    public String getConsignor() {
        return consignor;
    }

    public void setConsignor(String consignor) {
        this.consignor = consignor;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getConsignmentExcelUploadSize() {
        return consignmentExcelUploadSize;
    }

    public void setConsignmentExcelUploadSize(String consignmentExcelUploadSize) {
        this.consignmentExcelUploadSize = consignmentExcelUploadSize;
    }

    public int getCounts() {
        return counts;
    }

    public void setCounts(int counts) {
        this.counts = counts;
    }
    
    

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }    
    

    public String getDelhiPrice() {
        return delhiPrice;
    }

    public void setDelhiPrice(String delhiPrice) {
        this.delhiPrice = delhiPrice;
    }

    public String getMumbaiPrice() {
        return mumbaiPrice;
    }

    public void setMumbaiPrice(String mumbaiPrice) {
        this.mumbaiPrice = mumbaiPrice;
    }

    public String getKolkataPrice() {
        return kolkataPrice;
    }

    public void setKolkataPrice(String kolkataPrice) {
        this.kolkataPrice = kolkataPrice;
    }

    public String getChennaiPrice() {
        return chennaiPrice;
    }

    public void setChennaiPrice(String chennaiPrice) {
        this.chennaiPrice = chennaiPrice;
    }

    public String getAveragePrice() {
        return averagePrice;
    }

    public void setAveragePrice(String averagePrice) {
        this.averagePrice = averagePrice;
    }

    public String getIncreasePrice() {
        return increasePrice;
    }

    public void setIncreasePrice(String increasePrice) {
        this.increasePrice = increasePrice;
    }

    public String getEscalationPrice() {
        return escalationPrice;
    }

    public void setEscalationPrice(String escalationPrice) {
        this.escalationPrice = escalationPrice;
    }

    public String getEscalationPercent() {
        return escalationPercent;
    }

    public void setEscalationPercent(String escalationPercent) {
        this.escalationPercent = escalationPercent;
    }    
    

    public String getVendoreFSId() {
        return vendoreFSId;
    }

    public void setVendoreFSId(String vendoreFSId) {
        this.vendoreFSId = vendoreFSId;
    }

    

    public String getTripActualAmount() {
        return TripActualAmount;
    }

    public void setTripActualAmount(String TripActualAmount) {
        this.TripActualAmount = TripActualAmount;
    }

    public String getEstimatedexpense() {
        return estimatedexpense;
    }

    public void setEstimatedexpense(String estimatedexpense) {
        this.estimatedexpense = estimatedexpense;
    }

    public String getPaidamt() {
        return paidamt;
    }

    public void setPaidamt(String paidamt) {
        this.paidamt = paidamt;
    }

    public String getPaidstatus() {
        return paidstatus;
    }

    public void setPaidstatus(String paidstatus) {
        this.paidstatus = paidstatus;
    }

    public String getTripday() {
        return tripday;
    }

    public void setTripday(String tripday) {
        this.tripday = tripday;
    }

    public String getTripid() {
        return tripid;
    }

    public void setTripid(String tripid) {
        this.tripid = tripid;
    }

    public String getAdvancerequestamt() {
        return advancerequestamt;
    }

    public void setAdvancerequestamt(String advancerequestamt) {
        this.advancerequestamt = advancerequestamt;
    }

    public String getRequestby() {
        return requestby;
    }

    public void setRequestby(String requestby) {
        this.requestby = requestby;
    }

    public String getRequeston() {
        return requeston;
    }

    public void setRequeston(String requeston) {
        this.requeston = requeston;
    }

    public String getRequestremarks() {
        return requestremarks;
    }

    public void setRequestremarks(String requestremarks) {
        this.requestremarks = requestremarks;
    }

    public String getRequeststatus() {
        return requeststatus;
    }

    public void setRequeststatus(String requeststatus) {
        this.requeststatus = requeststatus;
    }

    public String getTobepaidtoday() {
        return tobepaidtoday;
    }

    public void setTobepaidtoday(String tobepaidtoday) {
        this.tobepaidtoday = tobepaidtoday;
    }

    public String getEstimatedadvanceperday() {
        return estimatedadvanceperday;
    }

    public void setEstimatedadvanceperday(String estimatedadvanceperday) {
        this.estimatedadvanceperday = estimatedadvanceperday;
    }

    public String getEstimatedtransitday() {
        return estimatedtransitday;
    }

    public void setEstimatedtransitday(String estimatedtransitday) {
        this.estimatedtransitday = estimatedtransitday;
    }

    public String getEstimatedrevenue() {
        return estimatedrevenue;
    }

    public void setEstimatedrevenue(String estimatedrevenue) {
        this.estimatedrevenue = estimatedrevenue;
    }

    public String getPlanneddate() {
        return planneddate;
    }

    public void setPlanneddate(String planneddate) {
        this.planneddate = planneddate;
    }

    public String getTripcode() {
        return tripcode;
    }

    public void setTripcode(String tripcode) {
        this.tripcode = tripcode;
    }

//    Brattle Foods
    public String getServiceVendor() {
        return serviceVendor;
    }

    public void setServiceVendor(String serviceVendor) {
        this.serviceVendor = serviceVendor;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    public String getInDateTime() {
        return inDateTime;
    }

    public void setInDateTime(String inDateTime) {
        this.inDateTime = inDateTime;
    }

    public String getInKM() {
        return inKM;
    }

    public void setInKM(String inKM) {
        this.inKM = inKM;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOutDateTime() {
        return outDateTime;
    }

    public void setOutDateTime(String outDateTime) {
        this.outDateTime = outDateTime;
    }

    public String getOutKM() {
        return outKM;
    }

    public void setOutKM(String outKM) {
        this.outKM = outKM;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getCustAddress() {
        return custAddress;
    }

    public void setCustAddress(String custAddress) {
        this.custAddress = custAddress;
    }

    public String getCustCity() {
        return custCity;
    }

    public void setCustCity(String custCity) {
        this.custCity = custCity;
    }

    public String getCustEmail() {
        return custEmail;
    }

    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }

    public String getCustMobile() {
        return custMobile;
    }

    public void setCustMobile(String custMobile) {
        this.custMobile = custMobile;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustPhone() {
        return custPhone;
    }

    public void setCustPhone(String custPhone) {
        this.custPhone = custPhone;
    }

    public String getCustState() {
        return custState;
    }

    public void setCustState(String custState) {
        this.custState = custState;
    }

    public String getTotalKm() {
        return totalKm;
    }

    public void setTotalKm(String totalKm) {
        this.totalKm = totalKm;
    }

    public String getIntime() {
        return intime;
    }

    public void setIntime(String intime) {
        this.intime = intime;
    }

    public String getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(String saleDate) {
        this.saleDate = saleDate;
    }

    public int getCustomertypeId() {
        return customertypeId;
    }

    public void setCustomertypeId(int customertypeId) {
        this.customertypeId = customertypeId;
    }

    public String getCustomertypeName() {
        return customertypeName;
    }

    public void setCustomertypeName(String customertypeName) {
        this.customertypeName = customertypeName;
    }
//   bala ends

    public int getBillNo() {
        return billNo;
    }

    public void setBillNo(int billNo) {
        this.billNo = billNo;
    }

    public int getModel() {
        return model;
    }

    public void setModel(int model) {
        this.model = model;
    }

    public int getIssQty() {
        return issQty;
    }

    public void setIssQty(int issQty) {
        this.issQty = issQty;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getMrsId() {
        return mrsId;
    }

    public void setMrsId(int mrsId) {
        this.mrsId = mrsId;
    }

    public int getReqQty() {
        return reqQty;
    }

    public void setReqQty(int reqQty) {
        this.reqQty = reqQty;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getBalanceHm() {
        return balanceHm;
    }

    public void setBalanceHm(int balanceHm) {
        this.balanceHm = balanceHm;
    }

    public String getClosedDate() {
        return closedDate;
    }

    public void setClosedDate(String closedDate) {
        this.closedDate = closedDate;
    }

    public String getServicedDate() {
        return servicedDate;
    }

    public void setServicedDate(String servicedDate) {
        this.servicedDate = servicedDate;
    }

    public int getCurrentHM() {
        return currentHM;
    }

    public void setCurrentHM(int currentHM) {
        this.currentHM = currentHM;
    }

    public int getHourMeter() {
        return hourMeter;
    }

    public void setHourMeter(int hourMeter) {
        this.hourMeter = hourMeter;
    }

    public int getCurrentKM() {
        return currentKM;
    }

    public void setCurrentKM(int currentKM) {
        this.currentKM = currentKM;
    }

    public String getIdentifiedby() {
        return identifiedby;
    }

    public void setIdentifiedby(String identifiedby) {
        this.identifiedby = identifiedby;
    }

    public String getCust() {
        return cust;
    }

    public void setCust(String cust) {
        this.cust = cust;
    }

    public int getCheck() {
        return check;
    }

    public void setCheck(int check) {
        this.check = check;
    }

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public int getSecId() {
        return secId;
    }

    public void setSecId(int secId) {
        this.secId = secId;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public int getProbId() {
        return probId;
    }

    public void setProbId(int probId) {
        this.probId = probId;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public float getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(float totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getBalanceKm() {
        return balanceKm;
    }

    public void setBalanceKm(int balanceKm) {
        this.balanceKm = balanceKm;
    }

    public int getJobCardId() {
        return jobCardId;
    }

    public void setJobCardId(int jobCardId) {
        this.jobCardId = jobCardId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getScheduledDate() {
        return scheduledDate;
    }

    public void setScheduledDate(String scheduledDate) {
        this.scheduledDate = scheduledDate;
    }

    public String getScheduledDeleivery() {
        return scheduledDeleivery;
    }

    public void setScheduledDeleivery(String scheduledDeleivery) {
        this.scheduledDeleivery = scheduledDeleivery;
    }

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getProbName() {
        return probName;
    }

    public void setProbName(String probName) {
        this.probName = probName;
    }

    public String getSecName() {
        return secName;
    }

    public void setSecName(String secName) {
        this.secName = secName;
    }

    public int getSeverity() {
        return severity;
    }

    public void setSeverity(int severity) {
        this.severity = severity;
    }

    public String getMfrName() {
        return mfrName;
    }

    public void setMfrName(String mfrName) {
        this.mfrName = mfrName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getChassNo() {
        return chassNo;
    }

    public void setChassNo(String chassNo) {
        this.chassNo = chassNo;
    }

    public String getEngineNo() {
        return engineNo;
    }

    public void setEngineNo(String engineNo) {
        this.engineNo = engineNo;
    }

    public String getUsageName() {
        return usageName;
    }

    public void setUsageName(String usageName) {
        this.usageName = usageName;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public int getElapsedDays() {
        return elapsedDays;
    }

    public void setElapsedDays(int elapsedDays) {
        this.elapsedDays = elapsedDays;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public int getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(int workOrderId) {
        this.workOrderId = workOrderId;
    }

    public String getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDateOfIssue(String dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public int getCompId() {
        return compId;
    }

    public void setCompId(int compId) {
        this.compId = compId;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public int getServicetypeId() {
        return servicetypeId;
    }

    public void setServicetypeId(int servicetypeId) {
        this.servicetypeId = servicetypeId;
    }

    public String getServicetypeName() {
        return servicetypeName;
    }

    public void setServicetypeName(String servicetypeName) {
        this.servicetypeName = servicetypeName;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public int getKmReading() {
        return kmReading;
    }

    public void setKmReading(int kmReading) {
        this.kmReading = kmReading;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getReqDate() {
        return reqDate;
    }

    public void setReqDate(String reqDate) {
        this.reqDate = reqDate;
    }

    public String getSec() {
        return sec;
    }

    public void setSec(String sec) {
        this.sec = sec;
    }

    public int getServiceLocationId() {
        return serviceLocationId;
    }

    public void setServiceLocationId(int serviceLocationId) {
        this.serviceLocationId = serviceLocationId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getPriority1() {
        return priority1;
    }

    public void setPriority1(String priority1) {
        this.priority1 = priority1;
    }

    public String getJcMYFormatNo() {
        return jcMYFormatNo;
    }

    public void setJcMYFormatNo(String jcMYFormatNo) {
        this.jcMYFormatNo = jcMYFormatNo;
    }

    public String getLastServicedKm() {
        return lastServicedKm;
    }

    public void setLastServicedKm(String lastServicedKm) {
        this.lastServicedKm = lastServicedKm;
    }

    public int getWoId() {
        return woId;
    }

    public void setWoId(int woId) {
        this.woId = woId;
    }

    public String[] getAddress() {
        return address;
    }

    public void setAddress(String[] address) {
        this.address = address;
    }

    public String getVendorAddress() {
        return vendorAddress;
    }

    public void setVendorAddress(String vendorAddress) {
        this.vendorAddress = vendorAddress;
    }

    public String getProblemName() {
        return problemName;
    }

    public void setProblemName(String problemName) {
        this.problemName = problemName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getWoComp() {
        return woComp;
    }

    public void setWoComp(String woComp) {
        this.woComp = woComp;
    }

    public ArrayList getActivity() {
        return activity;
    }

    public void setActivity(ArrayList activity) {
        this.activity = activity;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String[] getProblemNameSplit() {
        return problemNameSplit;
    }

    public void setProblemNameSplit(String[] problemNameSplit) {
        this.problemNameSplit = problemNameSplit;
    }

    public String[] getSymptomsSplit() {
        return symptomsSplit;
    }

    public void setSymptomsSplit(String[] symptomsSplit) {
        this.symptomsSplit = symptomsSplit;
    }

    public String getPcd() {
        return pcd;
    }

    public void setPcd(String pcd) {
        this.pcd = pcd;
    }

    public String getServiceLocation() {
        return serviceLocation;
    }

    public void setServiceLocation(String serviceLocation) {
        this.serviceLocation = serviceLocation;
    }

    public String getcRemarks() {
        return cRemarks;
    }

    public void setcRemarks(String cRemarks) {
        this.cRemarks = cRemarks;
    }

    public String getBayNo() {
        return bayNo;
    }

    public void setBayNo(String bayNo) {
        this.bayNo = bayNo;
    }

    public String getMfrId() {
        return mfrId;
    }

    public void setMfrId(String mfrId) {
        this.mfrId = mfrId;
    }

    public String getActualHrs() {
        return actualHrs;
    }

    public void setActualHrs(String actualHrs) {
        this.actualHrs = actualHrs;
    }

    public String getEstimatedHrs() {
        return estimatedHrs;
    }

    public void setEstimatedHrs(String estimatedHrs) {
        this.estimatedHrs = estimatedHrs;
    }

    public String getTechnicianId() {
        return technicianId;
    }

    public void setTechnicianId(String technicianId) {
        this.technicianId = technicianId;
    }

    public String getTechnicianName() {
        return technicianName;
    }

    public void setTechnicianName(String technicianName) {
        this.technicianName = technicianName;
    }

    public String getTripArrivalDate() {
        return tripArrivalDate;
    }

    public void setTripArrivalDate(String tripArrivalDate) {
        this.tripArrivalDate = tripArrivalDate;
    }

    public String getTripBalanceAmount() {
        return tripBalanceAmount;
    }

    public void setTripBalanceAmount(String tripBalanceAmount) {
        this.tripBalanceAmount = tripBalanceAmount;
    }

    public String getTripCode() {
        return tripCode;
    }

    public void setTripCode(String tripCode) {
        this.tripCode = tripCode;
    }

    public String getTripDate() {
        return tripDate;
    }

    public void setTripDate(String tripDate) {
        this.tripDate = tripDate;
    }

    public String getTripDepartureDate() {
        return tripDepartureDate;
    }

    public void setTripDepartureDate(String tripDepartureDate) {
        this.tripDepartureDate = tripDepartureDate;
    }

    public String getTripDriverId() {
        return tripDriverId;
    }

    public void setTripDriverId(String tripDriverId) {
        this.tripDriverId = tripDriverId;
    }

    public String getTripFuelAmount() {
        return tripFuelAmount;
    }

    public void setTripFuelAmount(String tripFuelAmount) {
        this.tripFuelAmount = tripFuelAmount;
    }

    public String getTripKmsIn() {
        return tripKmsIn;
    }

    public void setTripKmsIn(String tripKmsIn) {
        this.tripKmsIn = tripKmsIn;
    }

    public String getTripKmsOut() {
        return tripKmsOut;
    }

    public void setTripKmsOut(String tripKmsOut) {
        this.tripKmsOut = tripKmsOut;
    }

    public String getTripRouteId() {
        return tripRouteId;
    }

    public void setTripRouteId(String tripRouteId) {
        this.tripRouteId = tripRouteId;
    }

    public String getTripScheduleId() {
        return tripScheduleId;
    }

    public void setTripScheduleId(String tripScheduleId) {
        this.tripScheduleId = tripScheduleId;
    }

    public String getTripSheetId() {
        return tripSheetId;
    }

    public void setTripSheetId(String tripSheetId) {
        this.tripSheetId = tripSheetId;
    }

    public String getTripStatus() {
        return tripStatus;
    }

    public void setTripStatus(String tripStatus) {
        this.tripStatus = tripStatus;
    }

    public String getTripTotalExpenses() {
        return tripTotalExpenses;
    }

    public void setTripTotalExpenses(String tripTotalExpenses) {
        this.tripTotalExpenses = tripTotalExpenses;
    }

    public String getTripTotalKms() {
        return tripTotalKms;
    }

    public void setTripTotalKms(String tripTotalKms) {
        this.tripTotalKms = tripTotalKms;
    }

    public String getTripTotalLitres() {
        return tripTotalLitres;
    }

    public void setTripTotalLitres(String tripTotalLitres) {
        this.tripTotalLitres = tripTotalLitres;
    }

    public String getTripVehicleId() {
        return tripVehicleId;
    }

    public void setTripVehicleId(String tripVehicleId) {
        this.tripVehicleId = tripVehicleId;
    }

    public String getTripTotalAllowances() {
        return tripTotalAllowances;
    }

    public void setTripTotalAllowances(String tripTotalAllowances) {
        this.tripTotalAllowances = tripTotalAllowances;
    }

    public String getAdvDatetime() {
        return advDatetime;
    }

    public void setAdvDatetime(String advDatetime) {
        this.advDatetime = advDatetime;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBunkName() {
        return bunkName;
    }

    public void setBunkName(String bunkName) {
        this.bunkName = bunkName;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getFuelDatetime() {
        return fuelDatetime;
    }

    public void setFuelDatetime(String fuelDatetime) {
        this.fuelDatetime = fuelDatetime;
    }

    public String getFuelName() {
        return fuelName;
    }

    public void setFuelName(String fuelName) {
        this.fuelName = fuelName;
    }

    public String getInOutDateTime() {
        return inOutDateTime;
    }

    public void setInOutDateTime(String inOutDateTime) {
        this.inOutDateTime = inOutDateTime;
    }

    public String getInOutIndication() {
        return inOutIndication;
    }

    public void setInOutIndication(String inOutIndication) {
        this.inOutIndication = inOutIndication;
    }

    public String getIssuerName() {
        return issuerName;
    }

    public void setIssuerName(String issuerName) {
        this.issuerName = issuerName;
    }

    public String getLiters() {
        return liters;
    }

    public void setLiters(String liters) {
        this.liters = liters;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getSettlementFlag() {
        return settlementFlag;
    }

    public void setSettlementFlag(String settlementFlag) {
        this.settlementFlag = settlementFlag;
    }

    public String getDeliveredTonnage() {
        return deliveredTonnage;
    }

    public void setDeliveredTonnage(String deliveredTonnage) {
        this.deliveredTonnage = deliveredTonnage;
    }

    public String getTotalTonnage() {
        return totalTonnage;
    }

    public void setTotalTonnage(String totalTonnage) {
        this.totalTonnage = totalTonnage;
    }

    public String getShortage() {
        return shortage;
    }

    public void setShortage(String shortage) {
        this.shortage = shortage;
    }

    public String getAlightDate() {
        return alightDate;
    }

    public void setAlightDate(String alightDate) {
        this.alightDate = alightDate;
    }

    public String getAlightStatus() {
        return alightStatus;
    }

    public void setAlightStatus(String alightStatus) {
        this.alightStatus = alightStatus;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getExpensesDesc() {
        return expensesDesc;
    }

    public void setExpensesDesc(String expensesDesc) {
        this.expensesDesc = expensesDesc;
    }

    public String getEmbarkDate() {
        return embarkDate;
    }

    public void setEmbarkDate(String embarkDate) {
        this.embarkDate = embarkDate;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getTotalTonAmount() {
        return totalTonAmount;
    }

    public void setTotalTonAmount(String totalTonAmount) {
        this.totalTonAmount = totalTonAmount;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getIssuerId() {
        return issuerId;
    }

    public void setIssuerId(String issuerId) {
        this.issuerId = issuerId;
    }

    public String getBunkId() {
        return bunkId;
    }

    public void setBunkId(String bunkId) {
        this.bunkId = bunkId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getCleanerStatus() {
        return cleanerStatus;
    }

    public void setCleanerStatus(String cleanerStatus) {
        this.cleanerStatus = cleanerStatus;
    }

    public String getArrHours() {
        return arrHours;
    }

    public void setArrHours(String arrHours) {
        this.arrHours = arrHours;
    }

    public String getArrMints() {
        return arrMints;
    }

    public void setArrMints(String arrMints) {
        this.arrMints = arrMints;
    }

    public String getDepHours() {
        return depHours;
    }

    public void setDepHours(String depHours) {
        this.depHours = depHours;
    }

    public String getDepMints() {
        return depMints;
    }

    public void setDepMints(String depMints) {
        this.depMints = depMints;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public ArrayList getPeriodicServiceList() {
        return periodicServiceList;
    }

    public void setPeriodicServiceList(ArrayList periodicServiceList) {
        this.periodicServiceList = periodicServiceList;
    }

    public String getFlagString() {
        return flagString;
    }

    public void setFlagString(String flagString) {
        this.flagString = flagString;
    }

    public String getTotalFuel() {
        return totalFuel;
    }

    public void setTotalFuel(String totalFuel) {
        this.totalFuel = totalFuel;
    }

    public String getTotalFuelAmount() {
        return totalFuelAmount;
    }

    public void setTotalFuelAmount(String totalFuelAmount) {
        this.totalFuelAmount = totalFuelAmount;
    }

    public String getBags() {
        return bags;
    }

    public void setBags(String bags) {
        this.bags = bags;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getPinkSlip() {
        return pinkSlip;
    }

    public void setPinkSlip(String pinkSlip) {
        this.pinkSlip = pinkSlip;
    }

    public String getStageId() {
        return stageId;
    }

    public void setStageId(String stageId) {
        this.stageId = stageId;
    }

    public String getTonnage() {
        return tonnage;
    }

    public void setTonnage(String tonnage) {
        this.tonnage = tonnage;
    }

    public String getActive_ind() {
        return active_ind;
    }

    public void setActive_ind(String active_ind) {
        this.active_ind = active_ind;
    }

    public String getClplPriority() {
        return clplPriority;
    }

    public void setClplPriority(String clplPriority) {
        this.clplPriority = clplPriority;
    }

    public String getContractor() {
        return contractor;
    }

    public void setContractor(String contractor) {
        this.contractor = contractor;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getGatePassNo() {
        return gatePassNo;
    }

    public void setGatePassNo(String gatePassNo) {
        this.gatePassNo = gatePassNo;
    }

    public String getLorryNo() {
        return lorryNo;
    }

    public void setLorryNo(String lorryNo) {
        this.lorryNo = lorryNo;
    }

    public String getLpsDate() {
        return lpsDate;
    }

    public void setLpsDate(String lpsDate) {
        this.lpsDate = lpsDate;
    }

    public String getLpsID() {
        return lpsID;
    }

    public void setLpsID(String lpsID) {
        this.lpsID = lpsID;
    }

    public String getLpsNo() {
        return lpsNo;
    }

    public void setLpsNo(String lpsNo) {
        this.lpsNo = lpsNo;
    }

    public String getLpsNumber() {
        return lpsNumber;
    }

    public void setLpsNumber(String lpsNumber) {
        this.lpsNumber = lpsNumber;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getPackerNumber() {
        return packerNumber;
    }

    public void setPackerNumber(String packerNumber) {
        this.packerNumber = packerNumber;
    }

    public String getPacking() {
        return packing;
    }

    public void setPacking(String packing) {
        this.packing = packing;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getWatchandward() {
        return watchandward;
    }

    public void setWatchandward(String watchandward) {
        this.watchandward = watchandward;
    }

    public String getDriName() {
        return driName;
    }

    public void setDriName(String driName) {
        this.driName = driName;
    }

    public String getDriverID() {
        return driverID;
    }

    public void setDriverID(String driverID) {
        this.driverID = driverID;
    }

    public String getOwnership() {
        return ownership;
    }

    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    public String getPinkSlipID() {
        return pinkSlipID;
    }

    public void setPinkSlipID(String pinkSlipID) {
        this.pinkSlipID = pinkSlipID;
    }

    public String getRouteID() {
        return routeID;
    }

    public void setRouteID(String routeID) {
        this.routeID = routeID;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getSelectedLpsIds() {
        return selectedLpsIds;
    }

    public void setSelectedLpsIds(String selectedLpsIds) {
        this.selectedLpsIds = selectedLpsIds;
    }

    public String getVehicleID() {
        return vehicleID;
    }

    public void setVehicleID(String vehicleID) {
        this.vehicleID = vehicleID;
    }

    public String getVehicleTonnage() {
        return vehicleTonnage;
    }

    public void setVehicleTonnage(String vehicleTonnage) {
        this.vehicleTonnage = vehicleTonnage;
    }

    public String getAct_Ind() {
        return act_Ind;
    }

    public void setAct_Ind(String act_Ind) {
        this.act_Ind = act_Ind;
    }

    public String getBalanceamount() {
        return balanceamount;
    }

    public void setBalanceamount(String balanceamount) {
        this.balanceamount = balanceamount;
    }

    public String getCloseDateTime() {
        return closeDateTime;
    }

    public void setCloseDateTime(String closeDateTime) {
        this.closeDateTime = closeDateTime;
    }

    public String getCust_Name() {
        return cust_Name;
    }

    public void setCust_Name(String cust_Name) {
        this.cust_Name = cust_Name;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getEmp_Name() {
        return emp_Name;
    }

    public void setEmp_Name(String emp_Name) {
        this.emp_Name = emp_Name;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getNoOfTrip() {
        return noOfTrip;
    }

    public void setNoOfTrip(String noOfTrip) {
        this.noOfTrip = noOfTrip;
    }

    public String getOpenDateTime() {
        return openDateTime;
    }

    public void setOpenDateTime(String openDateTime) {
        this.openDateTime = openDateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRevenue() {
        return revenue;
    }

    public void setRevenue(String revenue) {
        this.revenue = revenue;
    }

    public String getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getTotalInvAmount() {
        return totalInvAmount;
    }

    public void setTotalInvAmount(String totalInvAmount) {
        this.totalInvAmount = totalInvAmount;
    }

    public String getTotalallowance() {
        return totalallowance;
    }

    public void setTotalallowance(String totalallowance) {
        this.totalallowance = totalallowance;
    }

    public String getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(String totalamount) {
        this.totalamount = totalamount;
    }

    public String getTotalexpenses() {
        return totalexpenses;
    }

    public void setTotalexpenses(String totalexpenses) {
        this.totalexpenses = totalexpenses;
    }

    public String getTotalkms() {
        return totalkms;
    }

    public void setTotalkms(String totalkms) {
        this.totalkms = totalkms;
    }

    public String getVehicleid() {
        return vehicleid;
    }

    public void setVehicleid(String vehicleid) {
        this.vehicleid = vehicleid;
    }

    public String getLedgerCode() {
        return LedgerCode;
    }

    public void setLedgerCode(String LedgerCode) {
        this.LedgerCode = LedgerCode;
    }

    public String getCommissionAmount() {
        return commissionAmount;
    }

    public void setCommissionAmount(String commissionAmount) {
        this.commissionAmount = commissionAmount;
    }

    public String getCommissionPercentage() {
        return commissionPercentage;
    }

    public void setCommissionPercentage(String commissionPercentage) {
        this.commissionPercentage = commissionPercentage;
    }

    public String getSettlementAmount() {
        return settlementAmount;
    }

    public void setSettlementAmount(String settlementAmount) {
        this.settlementAmount = settlementAmount;
    }

    public String getTotalDeliveredTonnage() {
        return totalDeliveredTonnage;
    }

    public void setTotalDeliveredTonnage(String totalDeliveredTonnage) {
        this.totalDeliveredTonnage = totalDeliveredTonnage;
    }

    public String getTotalTannage() {
        return totalTannage;
    }

    public void setTotalTannage(String totalTannage) {
        this.totalTannage = totalTannage;
    }

    public String getTotalregno() {
        return totalregno;
    }

    public void setTotalregno(String totalregno) {
        this.totalregno = totalregno;
    }

    public String getTotalshortage() {
        return totalshortage;
    }

    public void setTotalshortage(String totalshortage) {
        this.totalshortage = totalshortage;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getInvRefCode() {
        return invRefCode;
    }

    public void setInvRefCode(String invRefCode) {
        this.invRefCode = invRefCode;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInvoiceFor() {
        return invoiceFor;
    }

    public void setInvoiceFor(String invoiceFor) {
        this.invoiceFor = invoiceFor;
    }

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public String getNumberOfTri() {
        return numberOfTri;
    }

    public void setNumberOfTri(String numberOfTri) {
        this.numberOfTri = numberOfTri;
    }

    public String getFreightAmount() {
        return freightAmount;
    }

    public void setFreightAmount(String freightAmount) {
        this.freightAmount = freightAmount;
    }

    public String getInvoiceDetailId() {
        return invoiceDetailId;
    }

    public void setInvoiceDetailId(String invoiceDetailId) {
        this.invoiceDetailId = invoiceDetailId;
    }

    public String getLedgerName() {
        return ledgerName;
    }

    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public String getTripNo() {
        return tripNo;
    }

    public void setTripNo(String tripNo) {
        this.tripNo = tripNo;
    }

    public String getCommisssionAmount() {
        return commisssionAmount;
    }

    public void setCommisssionAmount(String commisssionAmount) {
        this.commisssionAmount = commisssionAmount;
    }

    public String getNumberOfTrip() {
        return numberOfTrip;
    }

    public void setNumberOfTrip(String numberOfTrip) {
        this.numberOfTrip = numberOfTrip;
    }

    public String getSettlementid() {
        return settlementid;
    }

    public void setSettlementid(String settlementid) {
        this.settlementid = settlementid;
    }

    public String getTotalAmountL() {
        return totalAmountL;
    }

    public void setTotalAmountL(String totalAmountL) {
        this.totalAmountL = totalAmountL;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getTotalamaount() {
        return totalamaount;
    }

    public void setTotalamaount(String totalamaount) {
        this.totalamaount = totalamaount;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getGPSKm() {
        return GPSKm;
    }

    public void setGPSKm(String GPSKm) {
        this.GPSKm = GPSKm;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getReturnFromLocation() {
        return returnFromLocation;
    }

    public void setReturnFromLocation(String returnFromLocation) {
        this.returnFromLocation = returnFromLocation;
    }

    public String getReturnToLocation() {
        return returnToLocation;
    }

    public void setReturnToLocation(String returnToLocation) {
        this.returnToLocation = returnToLocation;
    }

    public String getReturnTonnage() {
        return returnTonnage;
    }

    public void setReturnTonnage(String returnTonnage) {
        this.returnTonnage = returnTonnage;
    }

    public String getReturnTripProduct() {
        return returnTripProduct;
    }

    public void setReturnTripProduct(String returnTripProduct) {
        this.returnTripProduct = returnTripProduct;
    }

    public String getReturnAmount() {
        return returnAmount;
    }

    public void setReturnAmount(String returnAmount) {
        this.returnAmount = returnAmount;
    }

    public String getReturnExpenses() {
        return returnExpenses;
    }

    public void setReturnExpenses(String returnExpenses) {
        this.returnExpenses = returnExpenses;
    }

    public String getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }

    public String getSelectedBillStatus() {
        return selectedBillStatus;
    }

    public void setSelectedBillStatus(String selectedBillStatus) {
        this.selectedBillStatus = selectedBillStatus;
    }

    public String getSelectedRouteId() {
        return selectedRouteId;
    }

    public void setSelectedRouteId(String selectedRouteId) {
        this.selectedRouteId = selectedRouteId;
    }

    public String getTripReceivedAmount() {
        return tripReceivedAmount;
    }

    public void setTripReceivedAmount(String tripReceivedAmount) {
        this.tripReceivedAmount = tripReceivedAmount;
    }

    public String getReturnDeliveredDate() {
        return returnDeliveredDate;
    }

    public void setReturnDeliveredDate(String returnDeliveredDate) {
        this.returnDeliveredDate = returnDeliveredDate;
    }

    public String getReturnDeliveredTonnage() {
        return returnDeliveredTonnage;
    }

    public void setReturnDeliveredTonnage(String returnDeliveredTonnage) {
        this.returnDeliveredTonnage = returnDeliveredTonnage;
    }

    public String getReturnLoadedDate() {
        return returnLoadedDate;
    }

    public void setReturnLoadedDate(String returnLoadedDate) {
        this.returnLoadedDate = returnLoadedDate;
    }

    public String getReturnLoadedTonnage() {
        return returnLoadedTonnage;
    }

    public void setReturnLoadedTonnage(String returnLoadedTonnage) {
        this.returnLoadedTonnage = returnLoadedTonnage;
    }

    public String getShortageTonnage() {
        return shortageTonnage;
    }

    public void setShortageTonnage(String shortageTonnage) {
        this.shortageTonnage = shortageTonnage;
    }

    public String getRegisterNo() {
        return registerNo;
    }

    public void setRegisterNo(String registerNo) {
        this.registerNo = registerNo;
    }

    public String getTripdateP() {
        return tripdateP;
    }

    public void setTripdateP(String tripdateP) {
        this.tripdateP = tripdateP;
    }

    public String getTolocation() {
        return tolocation;
    }

    public void setTolocation(String tolocation) {
        this.tolocation = tolocation;
    }

    public String getTotalBalAmtRetuen() {
        return totalBalAmtRetuen;
    }

    public void setTotalBalAmtRetuen(String totalBalAmtRetuen) {
        this.totalBalAmtRetuen = totalBalAmtRetuen;
    }

    public String getTotalExpensesAmtReturn() {
        return totalExpensesAmtReturn;
    }

    public void setTotalExpensesAmtReturn(String totalExpensesAmtReturn) {
        this.totalExpensesAmtReturn = totalExpensesAmtReturn;
    }

    public String getReturnAmt() {
        return returnAmt;
    }

    public void setReturnAmt(String returnAmt) {
        this.returnAmt = returnAmt;
    }

    public String getReturnExp() {
        return returnExp;
    }

    public void setReturnExp(String returnExp) {
        this.returnExp = returnExp;
    }

    public String getTotalBalAmtRt() {
        return totalBalAmtRt;
    }

    public void setTotalBalAmtRt(String totalBalAmtRt) {
        this.totalBalAmtRt = totalBalAmtRt;
    }

    public String getTotalExpensesAmtRt() {
        return totalExpensesAmtRt;
    }

    public void setTotalExpensesAmtRt(String totalExpensesAmtRt) {
        this.totalExpensesAmtRt = totalExpensesAmtRt;
    }

    public String getTonnageRate() {
        return tonnageRate;
    }

    public void setTonnageRate(String tonnageRate) {
        this.tonnageRate = tonnageRate;
    }

    public String getFromLocation() {
        return fromLocation;
    }

    public void setFromLocation(String fromLocation) {
        this.fromLocation = fromLocation;
    }

    public String getReturnKM() {
        return returnKM;
    }

    public void setReturnKM(String returnKM) {
        this.returnKM = returnKM;
    }

    public String getReturnProductName() {
        return returnProductName;
    }

    public void setReturnProductName(String returnProductName) {
        this.returnProductName = returnProductName;
    }

    public String getReturnTon() {
        return returnTon;
    }

    public void setReturnTon(String returnTon) {
        this.returnTon = returnTon;
    }

    public String getReturnTripType() {
        return returnTripType;
    }

    public void setReturnTripType(String returnTripType) {
        this.returnTripType = returnTripType;
    }

    public String getToLocation() {
        return toLocation;
    }

    public void setToLocation(String toLocation) {
        this.toLocation = toLocation;
    }

    public String getReturnTripId() {
        return returnTripId;
    }

    public void setReturnTripId(String returnTripId) {
        this.returnTripId = returnTripId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getFromLocationid() {
        return fromLocationid;
    }

    public void setFromLocationid(String fromLocationid) {
        this.fromLocationid = fromLocationid;
    }

    public String getToLocationid() {
        return toLocationid;
    }

    public void setToLocationid(String toLocationid) {
        this.toLocationid = toLocationid;
    }

    public String getDeliveredDANo() {
        return deliveredDANo;
    }

    public void setDeliveredDANo(String deliveredDANo) {
        this.deliveredDANo = deliveredDANo;
    }

    public String getLoadedSlipNo() {
        return loadedSlipNo;
    }

    public void setLoadedSlipNo(String loadedSlipNo) {
        this.loadedSlipNo = loadedSlipNo;
    }

    public String getLoadedTonnage() {
        return loadedTonnage;
    }

    public void setLoadedTonnage(String loadedTonnage) {
        this.loadedTonnage = loadedTonnage;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getDriverVendorId() {
        return driverVendorId;
    }

    public void setDriverVendorId(String driverVendorId) {
        this.driverVendorId = driverVendorId;
    }

    public String getVehicleVendorId() {
        return vehicleVendorId;
    }

    public void setVehicleVendorId(String vehicleVendorId) {
        this.vehicleVendorId = vehicleVendorId;
    }

    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }

    public String getExpenseName() {
        return expenseName;
    }

    public void setExpenseName(String expenseName) {
        this.expenseName = expenseName;
    }

    public String getLedgerId() {
        return ledgerId;
    }

    public void setLedgerId(String ledgerId) {
        this.ledgerId = ledgerId;
    }

    public String getfLocationId() {
        return fLocationId;
    }

    public void setfLocationId(String fLocationId) {
        this.fLocationId = fLocationId;
    }

    public String getfLocationName() {
        return fLocationName;
    }

    public void setfLocationName(String fLocationName) {
        this.fLocationName = fLocationName;
    }

    public String gettLocationId() {
        return tLocationId;
    }

    public void settLocationId(String tLocationId) {
        this.tLocationId = tLocationId;
    }

    public String gettLocationName() {
        return tLocationName;
    }

    public void settLocationName(String tLocationName) {
        this.tLocationName = tLocationName;
    }

    public String getAccountDetail() {
        return accountDetail;
    }

    public void setAccountDetail(String accountDetail) {
        this.accountDetail = accountDetail;
    }

    public String getAccountEntryDate() {
        return accountEntryDate;
    }

    public void setAccountEntryDate(String accountEntryDate) {
        this.accountEntryDate = accountEntryDate;
    }

    public String getAccountEntryID() {
        return accountEntryID;
    }

    public void setAccountEntryID(String accountEntryID) {
        this.accountEntryID = accountEntryID;
    }

    public String getAccountsAmount() {
        return accountsAmount;
    }

    public void setAccountsAmount(String accountsAmount) {
        this.accountsAmount = accountsAmount;
    }

    public String getAccountsType() {
        return accountsType;
    }

    public void setAccountsType(String accountsType) {
        this.accountsType = accountsType;
    }

    public String getLedgerID() {
        return ledgerID;
    }

    public void setLedgerID(String ledgerID) {
        this.ledgerID = ledgerID;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public String getvDate() {
        return vDate;
    }

    public void setvDate(String vDate) {
        this.vDate = vDate;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    public String getDetailCode() {
        return detailCode;
    }

    public void setDetailCode(String detailCode) {
        this.detailCode = detailCode;
    }

    public String getGpno() {
        return gpno;
    }

    public void setGpno(String gpno) {
        this.gpno = gpno;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getBranchLedgerName() {
        return branchLedgerName;
    }

    public void setBranchLedgerName(String branchLedgerName) {
        this.branchLedgerName = branchLedgerName;
    }

    public String getDriverLedgerName() {
        return driverLedgerName;
    }

    public void setDriverLedgerName(String driverLedgerName) {
        this.driverLedgerName = driverLedgerName;
    }

    public String getTotalBags() {
        return totalBags;
    }

    public void setTotalBags(String totalBags) {
        this.totalBags = totalBags;
    }

    public String getSettlementType() {
        return settlementType;
    }

    public void setSettlementType(String settlementType) {
        this.settlementType = settlementType;
    }

    public String getSelectedRow() {
        return selectedRow;
    }

    public void setSelectedRow(String selectedRow) {
        this.selectedRow = selectedRow;
    }

    public int getSno() {
        return sno;
    }

    public void setSno(int sno) {
        this.sno = sno;
    }

    public String getCrossingAmount() {
        return crossingAmount;
    }

    public void setCrossingAmount(String crossingAmount) {
        this.crossingAmount = crossingAmount;
    }

    public String getCrossingType() {
        return crossingType;
    }

    public void setCrossingType(String crossingType) {
        this.crossingType = crossingType;
    }

    public String getTonnageRateMarket() {
        return tonnageRateMarket;
    }

    public void setTonnageRateMarket(String tonnageRateMarket) {
        this.tonnageRateMarket = tonnageRateMarket;
    }

    public String getTwoLpsStatus() {
        return twoLpsStatus;
    }

    public void setTwoLpsStatus(String twoLpsStatus) {
        this.twoLpsStatus = twoLpsStatus;
    }

    public String getTripRevenueOne() {
        return tripRevenueOne;
    }

    public void setTripRevenueOne(String tripRevenueOne) {
        this.tripRevenueOne = tripRevenueOne;
    }

    public String getTripRevenueTwo() {
        return tripRevenueTwo;
    }

    public void setTripRevenueTwo(String tripRevenueTwo) {
        this.tripRevenueTwo = tripRevenueTwo;
    }

    public String getSelectedRouteName() {
        return selectedRouteName;
    }

    public void setSelectedRouteName(String selectedRouteName) {
        this.selectedRouteName = selectedRouteName;
    }

    public String getSelectedPacking() {
        return selectedPacking;
    }

    public void setSelectedPacking(String selectedPacking) {
        this.selectedPacking = selectedPacking;
    }

    public String getSelectedProductName() {
        return selectedProductName;
    }

    public void setSelectedProductName(String selectedProductName) {
        this.selectedProductName = selectedProductName;
    }

    public String getBillStatusNew() {
        return billStatusNew;
    }

    public void setBillStatusNew(String billStatusNew) {
        this.billStatusNew = billStatusNew;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getVehicleMileage() {
        return vehicleMileage;
    }

    public void setVehicleMileage(String vehicleMileage) {
        this.vehicleMileage = vehicleMileage;
    }

    public String getReeferMileage() {
        return reeferMileage;
    }

    public void setReeferMileage(String reeferMileage) {
        this.reeferMileage = reeferMileage;
    }

    public String getRouteCode() {
        return routeCode;
    }

    public void setRouteCode(String routeCode) {
        this.routeCode = routeCode;
    }

    public String getTravelTime() {
        return travelTime;
    }

    public void setTravelTime(String travelTime) {
        this.travelTime = travelTime;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getReeferRunning() {
        return reeferRunning;
    }

    public void setReeferRunning(String reeferRunning) {
        this.reeferRunning = reeferRunning;
    }

    public String getTollAmount() {
        return tollAmount;
    }

    public void setTollAmount(String tollAmount) {
        this.tollAmount = tollAmount;
    }

    public String getFuelCost() {
        return fuelCost;
    }

    public void setFuelCost(String fuelCost) {
        this.fuelCost = fuelCost;
    }

    public String[] getMfrIds() {
        return mfrIds;
    }

    public void setMfrIds(String[] mfrIds) {
        this.mfrIds = mfrIds;
    }

    public String[] getModelIds() {
        return modelIds;
    }

    public void setModelIds(String[] modelIds) {
        this.modelIds = modelIds;
    }

    public String[] getVehMileage() {
        return vehMileage;
    }

    public void setVehMileage(String[] vehMileage) {
        this.vehMileage = vehMileage;
    }

    public String[] getVehExpense() {
        return vehExpense;
    }

    public void setVehExpense(String[] vehExpense) {
        this.vehExpense = vehExpense;
    }

    public String[] getReeferConsump() {
        return reeferConsump;
    }

    public void setReeferConsump(String[] reeferConsump) {
        this.reeferConsump = reeferConsump;
    }

    public String[] getReeferExpense() {
        return reeferExpense;
    }

    public void setReeferExpense(String[] reeferExpense) {
        this.reeferExpense = reeferExpense;
    }

    public String[] getVarRate() {
        return varRate;
    }

    public void setVarRate(String[] varRate) {
        this.varRate = varRate;
    }

    public String[] getVarCost() {
        return varCost;
    }

    public void setVarCost(String[] varCost) {
        this.varCost = varCost;
    }

    public String[] getTotExpense() {
        return totExpense;
    }

    public void setTotExpense(String[] totExpense) {
        this.totExpense = totExpense;
    }

    public String getRoadType() {
        return roadType;
    }

    public void setRoadType(String roadType) {
        this.roadType = roadType;
    }

    public String getRouteIdFrom() {
        return routeIdFrom;
    }

    public void setRouteIdFrom(String routeIdFrom) {
        this.routeIdFrom = routeIdFrom;
    }

    public String getRouteIdTo() {
        return routeIdTo;
    }

    public void setRouteIdTo(String routeIdTo) {
        this.routeIdTo = routeIdTo;
    }

    public String getEditRouteId() {
        return editRouteId;
    }

    public void setEditRouteId(String editRouteId) {
        this.editRouteId = editRouteId;
    }

    public String getFuelPrice() {
        return fuelPrice;
    }

    public void setFuelPrice(String fuelPrice) {
        this.fuelPrice = fuelPrice;
    }

    public String getRouteFrom() {
        return routeFrom;
    }

    public void setRouteFrom(String routeFrom) {
        this.routeFrom = routeFrom;
    }

    public String getRouteTo() {
        return routeTo;
    }

    public void setRouteTo(String routeTo) {
        this.routeTo = routeTo;
    }

    public String getVehicleExpense() {
        return vehicleExpense;
    }

    public void setVehicleExpense(String vehicleExpense) {
        this.vehicleExpense = vehicleExpense;
    }

    public String getReeferExpenses() {
        return reeferExpenses;
    }

    public void setReeferExpenses(String reeferExpenses) {
        this.reeferExpenses = reeferExpenses;
    }

    public String getVariableCostPerKm() {
        return variableCostPerKm;
    }

    public void setVariableCostPerKm(String variableCostPerKm) {
        this.variableCostPerKm = variableCostPerKm;
    }

    public String getVariableExpense() {
        return variableExpense;
    }

    public void setVariableExpense(String variableExpense) {
        this.variableExpense = variableExpense;
    }

    public String getTotalExpense() {
        return totalExpense;
    }

    public void setTotalExpense(String totalExpense) {
        this.totalExpense = totalExpense;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getStChargeName() {
        return stChargeName;
    }

    public void setStChargeName(String stChargeName) {
        this.stChargeName = stChargeName;
    }

    public String getStChargeDesc() {
        return stChargeDesc;
    }

    public void setStChargeDesc(String stChargeDesc) {
        this.stChargeDesc = stChargeDesc;
    }

    public String getStChargeUnit() {
        return stChargeUnit;
    }

    public void setStChargeUnit(String stChargeUnit) {
        this.stChargeUnit = stChargeUnit;
    }

    public String getStChargeId() {
        return stChargeId;
    }

    public void setStChargeId(String stChargeId) {
        this.stChargeId = stChargeId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getBillingTypeId() {
        return billingTypeId;
    }

    public void setBillingTypeId(String billingTypeId) {
        this.billingTypeId = billingTypeId;
    }

    public String getBillingTypeName() {
        return billingTypeName;
    }

    public void setBillingTypeName(String billingTypeName) {
        this.billingTypeName = billingTypeName;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getElementValue() {
        return elementValue;
    }

    public void setElementValue(String elementValue) {
        this.elementValue = elementValue;
    }

    public String getRowCount() {
        return rowCount;
    }

    public void setRowCount(String rowCount) {
        this.rowCount = rowCount;
    }

    public String getInterimPoint() {
        return interimPoint;
    }

    public void setInterimPoint(String interimPoint) {
        this.interimPoint = interimPoint;
    }

    public String getPtpPickupPoint() {
        return ptpPickupPoint;
    }

    public void setPtpPickupPoint(String ptpPickupPoint) {
        this.ptpPickupPoint = ptpPickupPoint;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getContractFrom() {
        return contractFrom;
    }

    public void setContractFrom(String contractFrom) {
        this.contractFrom = contractFrom;
    }

    public String getContractTo() {
        return contractTo;
    }

    public void setContractTo(String contractTo) {
        this.contractTo = contractTo;
    }

    public String[] getPtpRouteContractCode() {
        return ptpRouteContractCode;
    }

    public void setPtpRouteContractCode(String[] ptpRouteContractCode) {
        this.ptpRouteContractCode = ptpRouteContractCode;
    }

    public String[] getPtpMfrIds() {
        return ptpMfrIds;
    }

    public void setPtpMfrIds(String[] ptpMfrIds) {
        this.ptpMfrIds = ptpMfrIds;
    }

    public String[] getInterimPoint1() {
        return interimPoint1;
    }

    public void setInterimPoint1(String[] interimPoint1) {
        this.interimPoint1 = interimPoint1;
    }

    public String[] getInterimPointId1() {
        return interimPointId1;
    }

    public void setInterimPointId1(String[] interimPointId1) {
        this.interimPointId1 = interimPointId1;
    }

    public String[] getInterimPoint1Km() {
        return interimPoint1Km;
    }

    public void setInterimPoint1Km(String[] interimPoint1Km) {
        this.interimPoint1Km = interimPoint1Km;
    }

    public String[] getInterimPoint2() {
        return interimPoint2;
    }

    public void setInterimPoint2(String[] interimPoint2) {
        this.interimPoint2 = interimPoint2;
    }

    public String[] getInterimPointId2() {
        return interimPointId2;
    }

    public void setInterimPointId2(String[] interimPointId2) {
        this.interimPointId2 = interimPointId2;
    }

    public String[] getInterimPoint2Km() {
        return interimPoint2Km;
    }

    public void setInterimPoint2Km(String[] interimPoint2Km) {
        this.interimPoint2Km = interimPoint2Km;
    }

    public String[] getInterimPoint3() {
        return interimPoint3;
    }

    public void setInterimPoint3(String[] interimPoint3) {
        this.interimPoint3 = interimPoint3;
    }

    public String[] getInterimPointId3() {
        return interimPointId3;
    }

    public void setInterimPointId3(String[] interimPointId3) {
        this.interimPointId3 = interimPointId3;
    }

    public String[] getInterimPoint3Km() {
        return interimPoint3Km;
    }

    public void setInterimPoint3Km(String[] interimPoint3Km) {
        this.interimPoint3Km = interimPoint3Km;
    }

    public String[] getInterimPoint4() {
        return interimPoint4;
    }

    public void setInterimPoint4(String[] interimPoint4) {
        this.interimPoint4 = interimPoint4;
    }

    public String[] getInterimPointId4() {
        return interimPointId4;
    }

    public void setInterimPointId4(String[] interimPointId4) {
        this.interimPointId4 = interimPointId4;
    }

    public String[] getInterimPoint4Km() {
        return interimPoint4Km;
    }

    public void setInterimPoint4Km(String[] interimPoint4Km) {
        this.interimPoint4Km = interimPoint4Km;
    }

    public String[] getPtpDropPoint() {
        return ptpDropPoint;
    }

    public void setPtpDropPoint(String[] ptpDropPoint) {
        this.ptpDropPoint = ptpDropPoint;
    }

    public String[] getPtpDropPointId() {
        return ptpDropPointId;
    }

    public void setPtpDropPointId(String[] ptpDropPointId) {
        this.ptpDropPointId = ptpDropPointId;
    }

    public String[] getPtpDropPointKm() {
        return ptpDropPointKm;
    }

    public void setPtpDropPointKm(String[] ptpDropPointKm) {
        this.ptpDropPointKm = ptpDropPointKm;
    }

    public String[] getPtpTotalKm() {
        return ptpTotalKm;
    }

    public void setPtpTotalKm(String[] ptpTotalKm) {
        this.ptpTotalKm = ptpTotalKm;
    }

    public String[] getPtpRateWithReefer() {
        return ptpRateWithReefer;
    }

    public void setPtpRateWithReefer(String[] ptpRateWithReefer) {
        this.ptpRateWithReefer = ptpRateWithReefer;
    }

    public String[] getPtpRateWithoutReefer() {
        return ptpRateWithoutReefer;
    }

    public void setPtpRateWithoutReefer(String[] ptpRateWithoutReefer) {
        this.ptpRateWithoutReefer = ptpRateWithoutReefer;
    }

    public String[] getPtpwRouteContractCode() {
        return ptpwRouteContractCode;
    }

    public void setPtpwRouteContractCode(String[] ptpwRouteContractCode) {
        this.ptpwRouteContractCode = ptpwRouteContractCode;
    }

    public String[] getPtpwMfrIds() {
        return ptpwMfrIds;
    }

    public void setPtpwMfrIds(String[] ptpwMfrIds) {
        this.ptpwMfrIds = ptpwMfrIds;
    }

    public String[] getPtpwPickupPoint() {
        return ptpwPickupPoint;
    }

    public void setPtpwPickupPoint(String[] ptpwPickupPoint) {
        this.ptpwPickupPoint = ptpwPickupPoint;
    }

    public String[] getPtpwDropPoint() {
        return ptpwDropPoint;
    }

    public void setPtpwDropPoint(String[] ptpwDropPoint) {
        this.ptpwDropPoint = ptpwDropPoint;
    }

    public String[] getPtpwTotalKm() {
        return ptpwTotalKm;
    }

    public void setPtpwTotalKm(String[] ptpwTotalKm) {
        this.ptpwTotalKm = ptpwTotalKm;
    }

    public String[] getPtpwRateWithReefer() {
        return ptpwRateWithReefer;
    }

    public void setPtpwRateWithReefer(String[] ptpwRateWithReefer) {
        this.ptpwRateWithReefer = ptpwRateWithReefer;
    }

    public String[] getPtpwRateWithoutReefer() {
        return ptpwRateWithoutReefer;
    }

    public void setPtpwRateWithoutReefer(String[] ptpwRateWithoutReefer) {
        this.ptpwRateWithoutReefer = ptpwRateWithoutReefer;
    }

    public String[] getPtpPickupPoints() {
        return ptpPickupPoints;
    }

    public void setPtpPickupPoints(String[] ptpPickupPoints) {
        this.ptpPickupPoints = ptpPickupPoints;
    }

    public int getContractId() {
        return contractId;
    }

    public void setContractId(int contractId) {
        this.contractId = contractId;
    }

    public String getCustomerDetails() {
        return customerDetails;
    }

    public void setCustomerDetails(String customerDetails) {
        this.customerDetails = customerDetails;
    }

    public String[] getPtpwPointId() {
        return ptpwPointId;
    }

    public void setPtpwPointId(String[] ptpwPointId) {
        this.ptpwPointId = ptpwPointId;
    }

    public String getFirstPickup() {
        return firstPickup;
    }

    public void setFirstPickup(String firstPickup) {
        this.firstPickup = firstPickup;
    }

    public String getInterimPickup1() {
        return interimPickup1;
    }

    public void setInterimPickup1(String interimPickup1) {
        this.interimPickup1 = interimPickup1;
    }

    public String getPoint1Id() {
        return point1Id;
    }

    public void setPoint1Id(String point1Id) {
        this.point1Id = point1Id;
    }

    public String getPoint1Km() {
        return point1Km;
    }

    public void setPoint1Km(String point1Km) {
        this.point1Km = point1Km;
    }

    public String getInterimPickup2() {
        return interimPickup2;
    }

    public void setInterimPickup2(String interimPickup2) {
        this.interimPickup2 = interimPickup2;
    }

    public String getPoint2Id() {
        return point2Id;
    }

    public void setPoint2Id(String point2Id) {
        this.point2Id = point2Id;
    }

    public String getPoint2Km() {
        return point2Km;
    }

    public void setPoint2Km(String point2Km) {
        this.point2Km = point2Km;
    }

    public String getInterimPickup3() {
        return interimPickup3;
    }

    public void setInterimPickup3(String interimPickup3) {
        this.interimPickup3 = interimPickup3;
    }

    public String getPoint3Id() {
        return point3Id;
    }

    public void setPoint3Id(String point3Id) {
        this.point3Id = point3Id;
    }

    public String getPoint3Km() {
        return point3Km;
    }

    public void setPoint3Km(String point3Km) {
        this.point3Km = point3Km;
    }

    public String getInterimPickup4() {
        return interimPickup4;
    }

    public void setInterimPickup4(String interimPickup4) {
        this.interimPickup4 = interimPickup4;
    }

    public String getPoint4Id() {
        return point4Id;
    }

    public void setPoint4Id(String point4Id) {
        this.point4Id = point4Id;
    }

    public String getPoint4Km() {
        return point4Km;
    }

    public void setPoint4Km(String point4Km) {
        this.point4Km = point4Km;
    }

    public String getFinalDrop() {
        return finalDrop;
    }

    public void setFinalDrop(String finalDrop) {
        this.finalDrop = finalDrop;
    }

    public String getFinalPointId() {
        return finalPointId;
    }

    public void setFinalPointId(String finalPointId) {
        this.finalPointId = finalPointId;
    }

    public String getFinalPointKm() {
        return finalPointKm;
    }

    public void setFinalPointKm(String finalPointKm) {
        this.finalPointKm = finalPointKm;
    }

    public String getRateWithReeferPerKm() {
        return rateWithReeferPerKm;
    }

    public void setRateWithReeferPerKm(String rateWithReeferPerKm) {
        this.rateWithReeferPerKm = rateWithReeferPerKm;
    }

    public String getRateWithoutReeferPerKm() {
        return rateWithoutReeferPerKm;
    }

    public void setRateWithoutReeferPerKm(String rateWithoutReeferPerKm) {
        this.rateWithoutReeferPerKm = rateWithoutReeferPerKm;
    }

    public String getRateWithReeferPerKg() {
        return rateWithReeferPerKg;
    }

    public void setRateWithReeferPerKg(String rateWithReeferPerKg) {
        this.rateWithReeferPerKg = rateWithReeferPerKg;
    }

    public String getRateWithoutReeferPerKg() {
        return rateWithoutReeferPerKg;
    }

    public void setRateWithoutReeferPerKg(String rateWithoutReeferPerKg) {
        this.rateWithoutReeferPerKg = rateWithoutReeferPerKg;
    }

    public String getReeferRupeesPerHour() {
        return reeferRupeesPerHour;
    }

    public void setReeferRupeesPerHour(String reeferRupeesPerHour) {
        this.reeferRupeesPerHour = reeferRupeesPerHour;
    }

    public String getVehicleRupeesPerKm() {
        return vehicleRupeesPerKm;
    }

    public void setVehicleRupeesPerKm(String vehicleRupeesPerKm) {
        this.vehicleRupeesPerKm = vehicleRupeesPerKm;
    }

    public String getRouteBillingId() {
        return routeBillingId;
    }

    public void setRouteBillingId(String routeBillingId) {
        this.routeBillingId = routeBillingId;
    }

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public String getCustomerTypeName() {
        return customerTypeName;
    }

    public void setCustomerTypeName(String customerTypeName) {
        this.customerTypeName = customerTypeName;
    }

    public String getRouteContractCode() {
        return routeContractCode;
    }

    public void setRouteContractCode(String routeContractCode) {
        this.routeContractCode = routeContractCode;
    }

    public String getInterimPointName() {
        return interimPointName;
    }

    public void setInterimPointName(String interimPointName) {
        this.interimPointName = interimPointName;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getLastPrice() {
        return lastPrice;
    }

    public void setLastPrice(String lastPrice) {
        this.lastPrice = lastPrice;
    }

    public String getPriceDiff() {
        return priceDiff;
    }

    public void setPriceDiff(String priceDiff) {
        this.priceDiff = priceDiff;
    }

    public String getFuelPriceId() {
        return fuelPriceId;
    }

    public void setFuelPriceId(String fuelPriceId) {
        this.fuelPriceId = fuelPriceId;
    }

    public int getRouteContractId() {
        return routeContractId;
    }

    public void setRouteContractId(int routeContractId) {
        this.routeContractId = routeContractId;
    }

    public String getAccountManagerName() {
        return accountManagerName;
    }

    public void setAccountManagerName(String accountManagerName) {
        this.accountManagerName = accountManagerName;
    }

    public String getRouteFromId() {
        return routeFromId;
    }

    public void setRouteFromId(String routeFromId) {
        this.routeFromId = routeFromId;
    }

    public String getRouteToId() {
        return routeToId;
    }

    public void setRouteToId(String routeToId) {
        this.routeToId = routeToId;
    }

    public String getCityFromId() {
        return cityFromId;
    }

    public void setCityFromId(String cityFromId) {
        this.cityFromId = cityFromId;
    }

    public String getCityToId() {
        return cityToId;
    }

    public void setCityToId(String cityToId) {
        this.cityToId = cityToId;
    }

    public String getTravelHour() {
        return travelHour;
    }

    public void setTravelHour(String travelHour) {
        this.travelHour = travelHour;
    }

    public String getTravelMinute() {
        return travelMinute;
    }

    public void setTravelMinute(String travelMinute) {
        this.travelMinute = travelMinute;
    }

    public String getReeferHour() {
        return reeferHour;
    }

    public void setReeferHour(String reeferHour) {
        this.reeferHour = reeferHour;
    }

    public String getReeferMinute() {
        return reeferMinute;
    }

    public void setReeferMinute(String reeferMinute) {
        this.reeferMinute = reeferMinute;
    }

    public String getTollAmountType() {
        return tollAmountType;
    }

    public void setTollAmountType(String tollAmountType) {
        this.tollAmountType = tollAmountType;
    }

    public String getAvgTollAmount() {
        return avgTollAmount;
    }

    public void setAvgTollAmount(String avgTollAmount) {
        this.avgTollAmount = avgTollAmount;
    }

    public String getAvgMisCost() {
        return avgMisCost;
    }

    public void setAvgMisCost(String avgMisCost) {
        this.avgMisCost = avgMisCost;
    }

    public String getAvgDriverIncentive() {
        return avgDriverIncentive;
    }

    public void setAvgDriverIncentive(String avgDriverIncentive) {
        this.avgDriverIncentive = avgDriverIncentive;
    }

    public String getAvgFactor() {
        return avgFactor;
    }

    public void setAvgFactor(String avgFactor) {
        this.avgFactor = avgFactor;
    }

    public String[] getVehTypeId() {
        return vehTypeId;
    }

    public void setVehTypeId(String[] vehTypeId) {
        this.vehTypeId = vehTypeId;
    }

    public String[] getFuelCostPerKm() {
        return fuelCostPerKm;
    }

    public void setFuelCostPerKm(String[] fuelCostPerKm) {
        this.fuelCostPerKm = fuelCostPerKm;
    }

    public String[] getFuelCostPerHr() {
        return fuelCostPerHr;
    }

    public void setFuelCostPerHr(String[] fuelCostPerHr) {
        this.fuelCostPerHr = fuelCostPerHr;
    }

    public String[] getTollAmounts() {
        return tollAmounts;
    }

    public void setTollAmounts(String[] tollAmounts) {
        this.tollAmounts = tollAmounts;
    }

    public String[] getMiscCostKm() {
        return miscCostKm;
    }

    public void setMiscCostKm(String[] miscCostKm) {
        this.miscCostKm = miscCostKm;
    }

    public String[] getDriverIncenKm() {
        return driverIncenKm;
    }

    public void setDriverIncenKm(String[] driverIncenKm) {
        this.driverIncenKm = driverIncenKm;
    }

    public String[] getFactor() {
        return factor;
    }

    public void setFactor(String[] factor) {
        this.factor = factor;
    }

    public String[] getVarExpense() {
        return varExpense;
    }

    public void setVarExpense(String[] varExpense) {
        this.varExpense = varExpense;
    }

    public String getCityFromName() {
        return cityFromName;
    }

    public void setCityFromName(String cityFromName) {
        this.cityFromName = cityFromName;
    }

    public String getCityToName() {
        return cityToName;
    }

    public void setCityToName(String cityToName) {
        this.cityToName = cityToName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEntryType() {
        return entryType;
    }

    public void setEntryType(String entryType) {
        this.entryType = entryType;
    }

    public String getConsignmentNoteNo() {
        return consignmentNoteNo;
    }

    public void setConsignmentNoteNo(String consignmentNoteNo) {
        this.consignmentNoteNo = consignmentNoteNo;
    }

    public String getConsignmentDate() {
        return consignmentDate;
    }

    public void setConsignmentDate(String consignmentDate) {
        this.consignmentDate = consignmentDate;
    }

    public String getOrderReferenceNo() {
        return orderReferenceNo;
    }

    public void setOrderReferenceNo(String orderReferenceNo) {
        this.orderReferenceNo = orderReferenceNo;
    }

    public String getOrderReferenceRemarks() {
        return orderReferenceRemarks;
    }

    public void setOrderReferenceRemarks(String orderReferenceRemarks) {
        this.orderReferenceRemarks = orderReferenceRemarks;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCustomerMobileNo() {
        return customerMobileNo;
    }

    public void setCustomerMobileNo(String customerMobileNo) {
        this.customerMobileNo = customerMobileNo;
    }

    public String getMailId() {
        return mailId;
    }

    public void setMailId(String mailId) {
        this.mailId = mailId;
    }

    public String getCustomerPhoneNo() {
        return customerPhoneNo;
    }

    public void setCustomerPhoneNo(String customerPhoneNo) {
        this.customerPhoneNo = customerPhoneNo;
    }

    public String getWalkinCustomerName() {
        return walkinCustomerName;
    }

    public void setWalkinCustomerName(String walkinCustomerName) {
        this.walkinCustomerName = walkinCustomerName;
    }

    public String getWalkinCustomerCode() {
        return walkinCustomerCode;
    }

    public void setWalkinCustomerCode(String walkinCustomerCode) {
        this.walkinCustomerCode = walkinCustomerCode;
    }

    public String getWalkinCustomerAddress() {
        return walkinCustomerAddress;
    }

    public void setWalkinCustomerAddress(String walkinCustomerAddress) {
        this.walkinCustomerAddress = walkinCustomerAddress;
    }

    public String getWalkinPincode() {
        return walkinPincode;
    }

    public void setWalkinPincode(String walkinPincode) {
        this.walkinPincode = walkinPincode;
    }

    public String getWalkinCustomerMobileNo() {
        return walkinCustomerMobileNo;
    }

    public void setWalkinCustomerMobileNo(String walkinCustomerMobileNo) {
        this.walkinCustomerMobileNo = walkinCustomerMobileNo;
    }

    public String getWalkinMailId() {
        return walkinMailId;
    }

    public void setWalkinMailId(String walkinMailId) {
        this.walkinMailId = walkinMailId;
    }

    public String getWalkinCustomerPhoneNo() {
        return walkinCustomerPhoneNo;
    }

    public void setWalkinCustomerPhoneNo(String walkinCustomerPhoneNo) {
        this.walkinCustomerPhoneNo = walkinCustomerPhoneNo;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getMultiPickup() {
        return multiPickup;
    }

    public void setMultiPickup(String multiPickup) {
        this.multiPickup = multiPickup;
    }

    public String getMultiDelivery() {
        return multiDelivery;
    }

    public void setMultiDelivery(String multiDelivery) {
        this.multiDelivery = multiDelivery;
    }

    public String getConsignmentOrderInstruction() {
        return consignmentOrderInstruction;
    }

    public void setConsignmentOrderInstruction(String consignmentOrderInstruction) {
        this.consignmentOrderInstruction = consignmentOrderInstruction;
    }

    public String[] getPackagesNos() {
        return packagesNos;
    }

    public void setPackagesNos(String[] packagesNos) {
        this.packagesNos = packagesNos;
    }

    public String[] getWeights() {
        return weights;
    }

    public void setWeights(String[] weights) {
        this.weights = weights;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getReeferRequired() {
        return reeferRequired;
    }

    public void setReeferRequired(String reeferRequired) {
        this.reeferRequired = reeferRequired;
    }

    public String getContractRateId() {
        return contractRateId;
    }

    public void setContractRateId(String contractRateId) {
        this.contractRateId = contractRateId;
    }

    public String getVehicleRequiredDate() {
        return vehicleRequiredDate;
    }

    public void setVehicleRequiredDate(String vehicleRequiredDate) {
        this.vehicleRequiredDate = vehicleRequiredDate;
    }

    public String getVehicleRequiredHour() {
        return vehicleRequiredHour;
    }

    public void setVehicleRequiredHour(String vehicleRequiredHour) {
        this.vehicleRequiredHour = vehicleRequiredHour;
    }

    public String getVehicleRequiredMinute() {
        return vehicleRequiredMinute;
    }

    public void setVehicleRequiredMinute(String vehicleRequiredMinute) {
        this.vehicleRequiredMinute = vehicleRequiredMinute;
    }

    public String getVehicleInstruction() {
        return vehicleInstruction;
    }

    public void setVehicleInstruction(String vehicleInstruction) {
        this.vehicleInstruction = vehicleInstruction;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getConsignorPhoneNo() {
        return consignorPhoneNo;
    }

    public void setConsignorPhoneNo(String consignorPhoneNo) {
        this.consignorPhoneNo = consignorPhoneNo;
    }

    public String getConsignorAddress() {
        return consignorAddress;
    }

    public void setConsignorAddress(String consignorAddress) {
        this.consignorAddress = consignorAddress;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsigneePhoneNo() {
        return consigneePhoneNo;
    }

    public void setConsigneePhoneNo(String consigneePhoneNo) {
        this.consigneePhoneNo = consigneePhoneNo;
    }

    public String getConsigneeAddress() {
        return consigneeAddress;
    }

    public void setConsigneeAddress(String consigneeAddress) {
        this.consigneeAddress = consigneeAddress;
    }

    public String getRateWithReefer() {
        return rateWithReefer;
    }

    public void setRateWithReefer(String rateWithReefer) {
        this.rateWithReefer = rateWithReefer;
    }

    public String getRateWithoutReefer() {
        return rateWithoutReefer;
    }

    public void setRateWithoutReefer(String rateWithoutReefer) {
        this.rateWithoutReefer = rateWithoutReefer;
    }

    public String getTotalPackage() {
        return totalPackage;
    }

    public void setTotalPackage(String totalPackage) {
        this.totalPackage = totalPackage;
    }

    public String getTotalWeightage() {
        return totalWeightage;
    }

    public void setTotalWeightage(String totalWeightage) {
        this.totalWeightage = totalWeightage;
    }

    public String getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(String totalHours) {
        this.totalHours = totalHours;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getTotalCharges() {
        return totalCharges;
    }

    public void setTotalCharges(String totalCharges) {
        this.totalCharges = totalCharges;
    }

    public String getDocCharges() {
        return docCharges;
    }

    public void setDocCharges(String docCharges) {
        this.docCharges = docCharges;
    }

    public String getOdaCharges() {
        return odaCharges;
    }

    public void setOdaCharges(String odaCharges) {
        this.odaCharges = odaCharges;
    }

    public String getMultiPickupCharge() {
        return multiPickupCharge;
    }

    public void setMultiPickupCharge(String multiPickupCharge) {
        this.multiPickupCharge = multiPickupCharge;
    }

    public String getMultiDeliveryCharge() {
        return multiDeliveryCharge;
    }

    public void setMultiDeliveryCharge(String multiDeliveryCharge) {
        this.multiDeliveryCharge = multiDeliveryCharge;
    }

    public String getHandleCharges() {
        return handleCharges;
    }

    public void setHandleCharges(String handleCharges) {
        this.handleCharges = handleCharges;
    }

    public String getOtherCharges() {
        return otherCharges;
    }

    public void setOtherCharges(String otherCharges) {
        this.otherCharges = otherCharges;
    }

    public String getUnloadingCharges() {
        return unloadingCharges;
    }

    public void setUnloadingCharges(String unloadingCharges) {
        this.unloadingCharges = unloadingCharges;
    }

    public String getLoadingCharges() {
        return loadingCharges;
    }

    public void setLoadingCharges(String loadingCharges) {
        this.loadingCharges = loadingCharges;
    }

    public String getWalkInBillingTypeId() {
        return walkInBillingTypeId;
    }

    public void setWalkInBillingTypeId(String walkInBillingTypeId) {
        this.walkInBillingTypeId = walkInBillingTypeId;
    }

    public String getWalkinFreightWithReefer() {
        return walkinFreightWithReefer;
    }

    public void setWalkinFreightWithReefer(String walkinFreightWithReefer) {
        this.walkinFreightWithReefer = walkinFreightWithReefer;
    }

    public String getWalkinFreightWithoutReefer() {
        return walkinFreightWithoutReefer;
    }

    public void setWalkinFreightWithoutReefer(String walkinFreightWithoutReefer) {
        this.walkinFreightWithoutReefer = walkinFreightWithoutReefer;
    }

    public String getWalkinRateWithReeferPerKg() {
        return walkinRateWithReeferPerKg;
    }

    public void setWalkinRateWithReeferPerKg(String walkinRateWithReeferPerKg) {
        this.walkinRateWithReeferPerKg = walkinRateWithReeferPerKg;
    }

    public String getWalkinRateWithoutReeferPerKg() {
        return walkinRateWithoutReeferPerKg;
    }

    public void setWalkinRateWithoutReeferPerKg(String walkinRateWithoutReeferPerKg) {
        this.walkinRateWithoutReeferPerKg = walkinRateWithoutReeferPerKg;
    }

    public String getWalkinRateWithReeferPerKm() {
        return walkinRateWithReeferPerKm;
    }

    public void setWalkinRateWithReeferPerKm(String walkinRateWithReeferPerKm) {
        this.walkinRateWithReeferPerKm = walkinRateWithReeferPerKm;
    }

    public String getWalkinRateWithoutReeferPerKm() {
        return walkinRateWithoutReeferPerKm;
    }

    public void setWalkinRateWithoutReeferPerKm(String walkinRateWithoutReeferPerKm) {
        this.walkinRateWithoutReeferPerKm = walkinRateWithoutReeferPerKm;
    }

    public String[] getProductCodes() {
        return productCodes;
    }

    public void setProductCodes(String[] productCodes) {
        this.productCodes = productCodes;
    }

    public String[] getProductNames() {
        return productNames;
    }

    public void setProductNames(String[] productNames) {
        this.productNames = productNames;
    }

    public String getTotFreightAmount() {
        return totFreightAmount;
    }

    public void setTotFreightAmount(String totFreightAmount) {
        this.totFreightAmount = totFreightAmount;
    }

    public String getStandardChargeRemarks() {
        return standardChargeRemarks;
    }

    public void setStandardChargeRemarks(String standardChargeRemarks) {
        this.standardChargeRemarks = standardChargeRemarks;
    }

    public String getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public String getProductCategoryName() {
        return productCategoryName;
    }

    public void setProductCategoryName(String productCategoryName) {
        this.productCategoryName = productCategoryName;
    }

    public String getFirstPickupId() {
        return firstPickupId;
    }

    public void setFirstPickupId(String firstPickupId) {
        this.firstPickupId = firstPickupId;
    }

    public String getFirstPickupName() {
        return firstPickupName;
    }

    public void setFirstPickupName(String firstPickupName) {
        this.firstPickupName = firstPickupName;
    }

    public String getPoint1Name() {
        return point1Name;
    }

    public void setPoint1Name(String point1Name) {
        this.point1Name = point1Name;
    }

    public String getPoint1Hrs() {
        return point1Hrs;
    }

    public void setPoint1Hrs(String point1Hrs) {
        this.point1Hrs = point1Hrs;
    }

    public String getPoint1Minutes() {
        return point1Minutes;
    }

    public void setPoint1Minutes(String point1Minutes) {
        this.point1Minutes = point1Minutes;
    }

    public String getPoint1RouteId() {
        return point1RouteId;
    }

    public void setPoint1RouteId(String point1RouteId) {
        this.point1RouteId = point1RouteId;
    }

    public String getPoint2Name() {
        return point2Name;
    }

    public void setPoint2Name(String point2Name) {
        this.point2Name = point2Name;
    }

    public String getPoint2Hrs() {
        return point2Hrs;
    }

    public void setPoint2Hrs(String point2Hrs) {
        this.point2Hrs = point2Hrs;
    }

    public String getPoint2Minutes() {
        return point2Minutes;
    }

    public void setPoint2Minutes(String point2Minutes) {
        this.point2Minutes = point2Minutes;
    }

    public String getPoint2RouteId() {
        return point2RouteId;
    }

    public void setPoint2RouteId(String point2RouteId) {
        this.point2RouteId = point2RouteId;
    }

    public String getPoint3Name() {
        return point3Name;
    }

    public void setPoint3Name(String point3Name) {
        this.point3Name = point3Name;
    }

    public String getPoint3Minutes() {
        return point3Minutes;
    }

    public void setPoint3Minutes(String point3Minutes) {
        this.point3Minutes = point3Minutes;
    }

    public String getPoint3RouteId() {
        return point3RouteId;
    }

    public void setPoint3RouteId(String point3RouteId) {
        this.point3RouteId = point3RouteId;
    }

    public String getPoint4Name() {
        return point4Name;
    }

    public void setPoint4Name(String point4Name) {
        this.point4Name = point4Name;
    }

    public String getPoint4Hrs() {
        return point4Hrs;
    }

    public void setPoint4Hrs(String point4Hrs) {
        this.point4Hrs = point4Hrs;
    }

    public String getPoint4Minutes() {
        return point4Minutes;
    }

    public void setPoint4Minutes(String point4Minutes) {
        this.point4Minutes = point4Minutes;
    }

    public String getPoint4RouteId() {
        return point4RouteId;
    }

    public void setPoint4RouteId(String point4RouteId) {
        this.point4RouteId = point4RouteId;
    }

    public String getFinalPointName() {
        return finalPointName;
    }

    public void setFinalPointName(String finalPointName) {
        this.finalPointName = finalPointName;
    }

    public String getFinalPointHrs() {
        return finalPointHrs;
    }

    public void setFinalPointHrs(String finalPointHrs) {
        this.finalPointHrs = finalPointHrs;
    }

    public String getFinalPointMinutes() {
        return finalPointMinutes;
    }

    public void setFinalPointMinutes(String finalPointMinutes) {
        this.finalPointMinutes = finalPointMinutes;
    }

    public String getFinalPointRouteId() {
        return finalPointRouteId;
    }

    public void setFinalPointRouteId(String finalPointRouteId) {
        this.finalPointRouteId = finalPointRouteId;
    }

    public String getVehicleRatePerKm() {
        return vehicleRatePerKm;
    }

    public void setVehicleRatePerKm(String vehicleRatePerKm) {
        this.vehicleRatePerKm = vehicleRatePerKm;
    }

    public String getReeferRatePerHour() {
        return reeferRatePerHour;
    }

    public void setReeferRatePerHour(String reeferRatePerHour) {
        this.reeferRatePerHour = reeferRatePerHour;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getTotalMinutes() {
        return totalMinutes;
    }

    public void setTotalMinutes(String totalMinutes) {
        this.totalMinutes = totalMinutes;
    }

    public String getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(String totalPoints) {
        this.totalPoints = totalPoints;
    }

    public String[] getFuelCostPerKms() {
        return fuelCostPerKms;
    }

    public void setFuelCostPerKms(String[] fuelCostPerKms) {
        this.fuelCostPerKms = fuelCostPerKms;
    }

    public String[] getFuelCostPerHrs() {
        return fuelCostPerHrs;
    }

    public void setFuelCostPerHrs(String[] fuelCostPerHrs) {
        this.fuelCostPerHrs = fuelCostPerHrs;
    }

    public String[] getReefMileage() {
        return reefMileage;
    }

    public void setReefMileage(String[] reefMileage) {
        this.reefMileage = reefMileage;
    }

    public String getFuelCostKm() {
        return fuelCostKm;
    }

    public void setFuelCostKm(String fuelCostKm) {
        this.fuelCostKm = fuelCostKm;
    }

    public String getFuelCostHr() {
        return fuelCostHr;
    }

    public void setFuelCostHr(String fuelCostHr) {
        this.fuelCostHr = fuelCostHr;
    }

    public String getTollAmountperkm() {
        return tollAmountperkm;
    }

    public void setTollAmountperkm(String tollAmountperkm) {
        this.tollAmountperkm = tollAmountperkm;
    }

    public String getMiscCostperkm() {
        return miscCostperkm;
    }

    public void setMiscCostperkm(String miscCostperkm) {
        this.miscCostperkm = miscCostperkm;
    }

    public String getDriverIncentperkm() {
        return driverIncentperkm;
    }

    public void setDriverIncentperkm(String driverIncentperkm) {
        this.driverIncentperkm = driverIncentperkm;
    }

    public String getVariExpense() {
        return variExpense;
    }

    public void setVariExpense(String variExpense) {
        this.variExpense = variExpense;
    }

    public String getVehiExpense() {
        return vehiExpense;
    }

    public void setVehiExpense(String vehiExpense) {
        this.vehiExpense = vehiExpense;
    }

    public String getReefeExpense() {
        return reefeExpense;
    }

    public void setReefeExpense(String reefeExpense) {
        this.reefeExpense = reefeExpense;
    }

    public String getTotaExpense() {
        return totaExpense;
    }

    public void setTotaExpense(String totaExpense) {
        this.totaExpense = totaExpense;
    }

    public String getEffectivDate() {
        return effectivDate;
    }

    public void setEffectivDate(String effectivDate) {
        this.effectivDate = effectivDate;
    }

    public String getPoint3Hrs() {
        return point3Hrs;
    }

    public void setPoint3Hrs(String point3Hrs) {
        this.point3Hrs = point3Hrs;
    }

    public String getConsignmentOrderId() {
        return consignmentOrderId;
    }

    public void setConsignmentOrderId(String consignmentOrderId) {
        this.consignmentOrderId = consignmentOrderId;
    }

    public String getConsignmentOrderDate() {
        return consignmentOrderDate;
    }

    public void setConsignmentOrderDate(String consignmentOrderDate) {
        this.consignmentOrderDate = consignmentOrderDate;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getConsigmentOrigin() {
        return consigmentOrigin;
    }

    public void setConsigmentOrigin(String consigmentOrigin) {
        this.consigmentOrigin = consigmentOrigin;
    }

    public String getConsigmentDestination() {
        return consigmentDestination;
    }

    public void setConsigmentDestination(String consigmentDestination) {
        this.consigmentDestination = consigmentDestination;
    }

    public String[] getPointId() {
        return pointId;
    }

    public void setPointId(String[] pointId) {
        this.pointId = pointId;
    }

    public String[] getPointName() {
        return pointName;
    }

    public void setPointName(String[] pointName) {
        this.pointName = pointName;
    }

    public String[] getPointType() {
        return pointType;
    }

    public void setPointType(String[] pointType) {
        this.pointType = pointType;
    }

    public String[] getOrder() {
        return order;
    }

    public void setOrder(String[] order) {
        this.order = order;
    }

    public String[] getPointAddresss() {
        return pointAddresss;
    }

    public void setPointAddresss(String[] pointAddresss) {
        this.pointAddresss = pointAddresss;
    }

    public String[] getPointPlanDate() {
        return pointPlanDate;
    }

    public void setPointPlanDate(String[] pointPlanDate) {
        this.pointPlanDate = pointPlanDate;
    }

    public String[] getPointPlanHour() {
        return pointPlanHour;
    }

    public void setPointPlanHour(String[] pointPlanHour) {
        this.pointPlanHour = pointPlanHour;
    }

    public String[] getPointPlanMinute() {
        return pointPlanMinute;
    }

    public void setPointPlanMinute(String[] pointPlanMinute) {
        this.pointPlanMinute = pointPlanMinute;
    }

    public String getEndPointId() {
        return endPointId;
    }

    public void setEndPointId(String endPointId) {
        this.endPointId = endPointId;
    }

    public String getFinalRouteId() {
        return finalRouteId;
    }

    public void setFinalRouteId(String finalRouteId) {
        this.finalRouteId = finalRouteId;
    }

    public String getEndPointName() {
        return endPointName;
    }

    public void setEndPointName(String endPointName) {
        this.endPointName = endPointName;
    }

    public String getEndPointType() {
        return endPointType;
    }

    public void setEndPointType(String endPointType) {
        this.endPointType = endPointType;
    }

    public String getEndOrder() {
        return endOrder;
    }

    public void setEndOrder(String endOrder) {
        this.endOrder = endOrder;
    }

    public String getEndPointAddresss() {
        return endPointAddresss;
    }

    public void setEndPointAddresss(String endPointAddresss) {
        this.endPointAddresss = endPointAddresss;
    }

    public String getEndPointPlanDate() {
        return endPointPlanDate;
    }

    public void setEndPointPlanDate(String endPointPlanDate) {
        this.endPointPlanDate = endPointPlanDate;
    }

    public String getEndPointPlanHour() {
        return endPointPlanHour;
    }

    public void setEndPointPlanHour(String endPointPlanHour) {
        this.endPointPlanHour = endPointPlanHour;
    }

    public String getEndPointPlanMinute() {
        return endPointPlanMinute;
    }

    public void setEndPointPlanMinute(String endPointPlanMinute) {
        this.endPointPlanMinute = endPointPlanMinute;
    }

    public String[] getMultiplePointRouteId() {
        return multiplePointRouteId;
    }

    public void setMultiplePointRouteId(String[] multiplePointRouteId) {
        this.multiplePointRouteId = multiplePointRouteId;
    }

    public String[] getPointRouteId() {
        return pointRouteId;
    }

    public void setPointRouteId(String[] pointRouteId) {
        this.pointRouteId = pointRouteId;
    }

    public String getApprovalstatus() {
        return approvalstatus;
    }

    public void setApprovalstatus(String approvalstatus) {
        this.approvalstatus = approvalstatus;
    }

    public String getTimeLeft() {
        return timeLeft;
    }

    public void setTimeLeft(String timeLeft) {
        this.timeLeft = timeLeft;
    }

    public String getTimeLeftValue() {
        return timeLeftValue;
    }

    public void setTimeLeftValue(String timeLeftValue) {
        this.timeLeftValue = timeLeftValue;
    }

    public String getTripScheduleDateTime() {
        return tripScheduleDateTime;
    }

    public void setTripScheduleDateTime(String tripScheduleDateTime) {
        this.tripScheduleDateTime = tripScheduleDateTime;
    }

    public String getTripScheduleDate() {
        return tripScheduleDate;
    }

    public void setTripScheduleDate(String tripScheduleDate) {
        this.tripScheduleDate = tripScheduleDate;
    }

    public String getTripScheduleTime() {
        return tripScheduleTime;
    }

    public void setTripScheduleTime(String tripScheduleTime) {
        this.tripScheduleTime = tripScheduleTime;
    }

    public String getActualadvancepaid() {
        return actualadvancepaid;
    }

    public void setActualadvancepaid(String actualadvancepaid) {
        this.actualadvancepaid = actualadvancepaid;
    }

    public String getCnoteName() {
        return cnoteName;
    }

    public void setCnoteName(String cnoteName) {
        this.cnoteName = cnoteName;
    }

    public String getEstimatedadvance() {
        return estimatedadvance;
    }

    public void setEstimatedadvance(String estimatedadvance) {
        this.estimatedadvance = estimatedadvance;
    }

    public String getArticleCode() {
        return articleCode;
    }

    public void setArticleCode(String articleCode) {
        this.articleCode = articleCode;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public String getBudinessType() {
        return budinessType;
    }

    public void setBudinessType(String budinessType) {
        this.budinessType = budinessType;
    }

    public String getConsigmentInstruction() {
        return consigmentInstruction;
    }

    public void setConsigmentInstruction(String consigmentInstruction) {
        this.consigmentInstruction = consigmentInstruction;
    }

    public String getConsigmentOrderStatus() {
        return consigmentOrderStatus;
    }

    public void setConsigmentOrderStatus(String consigmentOrderStatus) {
        this.consigmentOrderStatus = consigmentOrderStatus;
    }

    public String getConsigneeMobile() {
        return consigneeMobile;
    }

    public void setConsigneeMobile(String consigneeMobile) {
        this.consigneeMobile = consigneeMobile;
    }

    public String getConsignmentOrderNo() {
        return consignmentOrderNo;
    }

    public void setConsignmentOrderNo(String consignmentOrderNo) {
        this.consignmentOrderNo = consignmentOrderNo;
    }

    public String getConsignmentOrderRefRemarks() {
        return consignmentOrderRefRemarks;
    }

    public void setConsignmentOrderRefRemarks(String consignmentOrderRefRemarks) {
        this.consignmentOrderRefRemarks = consignmentOrderRefRemarks;
    }

    public String getConsignmentPointAddress() {
        return consignmentPointAddress;
    }

    public void setConsignmentPointAddress(String consignmentPointAddress) {
        this.consignmentPointAddress = consignmentPointAddress;
    }

    public String getConsignmentPointId() {
        return consignmentPointId;
    }

    public void setConsignmentPointId(String consignmentPointId) {
        this.consignmentPointId = consignmentPointId;
    }

    public String getConsignmentPointType() {
        return consignmentPointType;
    }

    public void setConsignmentPointType(String consignmentPointType) {
        this.consignmentPointType = consignmentPointType;
    }

    public String getConsignorMobile() {
        return consignorMobile;
    }

    public void setConsignorMobile(String consignorMobile) {
        this.consignorMobile = consignorMobile;
    }

    public String getCustomerBillingType() {
        return customerBillingType;
    }

    public void setCustomerBillingType(String customerBillingType) {
        this.customerBillingType = customerBillingType;
    }

    public String getCustomerContractId() {
        return customerContractId;
    }

    public void setCustomerContractId(String customerContractId) {
        this.customerContractId = customerContractId;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerPincode() {
        return customerPincode;
    }

    public void setCustomerPincode(String customerPincode) {
        this.customerPincode = customerPincode;
    }

    public String getFixedRate() {
        return fixedRate;
    }

    public void setFixedRate(String fixedRate) {
        this.fixedRate = fixedRate;
    }

    public String getFreightCharges() {
        return freightCharges;
    }

    public void setFreightCharges(String freightCharges) {
        this.freightCharges = freightCharges;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getKgRate() {
        return kgRate;
    }

    public void setKgRate(String kgRate) {
        this.kgRate = kgRate;
    }

    public String getKmRate() {
        return kmRate;
    }

    public void setKmRate(String kmRate) {
        this.kmRate = kmRate;
    }

    public String getMappingId() {
        return mappingId;
    }

    public void setMappingId(String mappingId) {
        this.mappingId = mappingId;
    }

    public String getPackageNos() {
        return packageNos;
    }

    public void setPackageNos(String packageNos) {
        this.packageNos = packageNos;
    }

    public String getPackageWeight() {
        return packageWeight;
    }

    public void setPackageWeight(String packageWeight) {
        this.packageWeight = packageWeight;
    }

    public String getPointAddress() {
        return pointAddress;
    }

    public void setPointAddress(String pointAddress) {
        this.pointAddress = pointAddress;
    }

    public String getPointDate() {
        return pointDate;
    }

    public void setPointDate(String pointDate) {
        this.pointDate = pointDate;
    }

    public String getPointPlanTime() {
        return pointPlanTime;
    }

    public void setPointPlanTime(String pointPlanTime) {
        this.pointPlanTime = pointPlanTime;
    }

    public String getPointSequence() {
        return pointSequence;
    }

    public void setPointSequence(String pointSequence) {
        this.pointSequence = pointSequence;
    }

    public String getPrimaryDriverId() {
        return primaryDriverId;
    }

    public void setPrimaryDriverId(String primaryDriverId) {
        this.primaryDriverId = primaryDriverId;
    }

    public String getPrimaryDriverName() {
        return primaryDriverName;
    }

    public void setPrimaryDriverName(String primaryDriverName) {
        this.primaryDriverName = primaryDriverName;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getSecondaryDriverIdOne() {
        return secondaryDriverIdOne;
    }

    public void setSecondaryDriverIdOne(String secondaryDriverIdOne) {
        this.secondaryDriverIdOne = secondaryDriverIdOne;
    }

    public String getSecondaryDriverIdTwo() {
        return secondaryDriverIdTwo;
    }

    public void setSecondaryDriverIdTwo(String secondaryDriverIdTwo) {
        this.secondaryDriverIdTwo = secondaryDriverIdTwo;
    }

    public String getSecondaryDriverNameOne() {
        return secondaryDriverNameOne;
    }

    public void setSecondaryDriverNameOne(String secondaryDriverNameOne) {
        this.secondaryDriverNameOne = secondaryDriverNameOne;
    }

    public String getSecondaryDriverNameTwo() {
        return secondaryDriverNameTwo;
    }

    public void setSecondaryDriverNameTwo(String secondaryDriverNameTwo) {
        this.secondaryDriverNameTwo = secondaryDriverNameTwo;
    }

    public String getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(String totalDistance) {
        this.totalDistance = totalDistance;
    }

    public String getTotalPackages() {
        return totalPackages;
    }

    public void setTotalPackages(String totalPackages) {
        this.totalPackages = totalPackages;
    }

    public String getTotalStatndardCharges() {
        return totalStatndardCharges;
    }

    public void setTotalStatndardCharges(String totalStatndardCharges) {
        this.totalStatndardCharges = totalStatndardCharges;
    }

    public String getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(String totalWeight) {
        this.totalWeight = totalWeight;
    }

    public String getVehicleRequiredTime() {
        return vehicleRequiredTime;
    }

    public void setVehicleRequiredTime(String vehicleRequiredTime) {
        this.vehicleRequiredTime = vehicleRequiredTime;
    }

    public String getFreightcharges() {
        return freightcharges;
    }

    public void setFreightcharges(String freightcharges) {
        this.freightcharges = freightcharges;
    }

    public String getTripStartDate() {
        return tripStartDate;
    }

    public void setTripStartDate(String tripStartDate) {
        this.tripStartDate = tripStartDate;
    }

    public String getTripendkm() {
        return tripendkm;
    }

    public void setTripendkm(String tripendkm) {
        this.tripendkm = tripendkm;
    }

    public String getTripstartkm() {
        return tripstartkm;
    }

    public void setTripstartkm(String tripstartkm) {
        this.tripstartkm = tripstartkm;
    }

    public String getExpectedArrivalDate() {
        return expectedArrivalDate;
    }

    public void setExpectedArrivalDate(String expectedArrivalDate) {
        this.expectedArrivalDate = expectedArrivalDate;
    }

    public String getExpectedArrivalTime() {
        return expectedArrivalTime;
    }

    public void setExpectedArrivalTime(String expectedArrivalTime) {
        this.expectedArrivalTime = expectedArrivalTime;
    }

    public String getContractRouteOrigin() {
        return contractRouteOrigin;
    }

    public void setContractRouteOrigin(String contractRouteOrigin) {
        this.contractRouteOrigin = contractRouteOrigin;
    }

    public String getPrevPointId() {
        return prevPointId;
    }

    public void setPrevPointId(String prevPointId) {
        this.prevPointId = prevPointId;
    }

    public String getPointOrder() {
        return pointOrder;
    }

    public void setPointOrder(String pointOrder) {
        this.pointOrder = pointOrder;
    }

    public String getAdvicedate() {
        return advicedate;
    }

    public void setAdvicedate(String advicedate) {
        this.advicedate = advicedate;
    }

    public String getBatchType() {
        return batchType;
    }

    public void setBatchType(String batchType) {
        this.batchType = batchType;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    public String getRequestedadvance() {
        return requestedadvance;
    }

    public void setRequestedadvance(String requestedadvance) {
        this.requestedadvance = requestedadvance;
    }

    public String getApproveremarks() {
        return approveremarks;
    }

    public void setApproveremarks(String approveremarks) {
        this.approveremarks = approveremarks;
    }

    public String getApprovestatus() {
        return approvestatus;
    }

    public void setApprovestatus(String approvestatus) {
        this.approvestatus = approvestatus;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getAlertBased() {
        return alertBased;
    }

    public void setAlertBased(String alertBased) {
        this.alertBased = alertBased;
    }

    public String getAlertDes() {
        return alertDes;
    }

    public void setAlertDes(String alertDes) {
        this.alertDes = alertDes;
    }

    public String getAlertName() {
        return alertName;
    }

    public void setAlertName(String alertName) {
        this.alertName = alertName;
    }

    public String getAlertRaised() {
        return alertRaised;
    }

    public void setAlertRaised(String alertRaised) {
        this.alertRaised = alertRaised;
    }

    public String getAlertRaised1() {
        return alertRaised1;
    }

    public void setAlertRaised1(String alertRaised1) {
        this.alertRaised1 = alertRaised1;
    }

    public String getAlertccEmail() {
        return alertccEmail;
    }

    public void setAlertccEmail(String alertccEmail) {
        this.alertccEmail = alertccEmail;
    }

    public String getAlertid() {
        return alertid;
    }

    public void setAlertid(String alertid) {
        this.alertid = alertid;
    }

    public String getAlertsemailSub() {
        return alertsemailSub;
    }

    public void setAlertsemailSub(String alertsemailSub) {
        this.alertsemailSub = alertsemailSub;
    }

    public String getAlertsfrequently() {
        return alertsfrequently;
    }

    public void setAlertsfrequently(String alertsfrequently) {
        this.alertsfrequently = alertsfrequently;
    }

    public String getAlertstatus() {
        return alertstatus;
    }

    public void setAlertstatus(String alertstatus) {
        this.alertstatus = alertstatus;
    }

    public String getAlerttoEmail() {
        return alerttoEmail;
    }

    public void setAlerttoEmail(String alerttoEmail) {
        this.alerttoEmail = alerttoEmail;
    }

    public String getCheckListDate() {
        return checkListDate;
    }

    public void setCheckListDate(String checkListDate) {
        this.checkListDate = checkListDate;
    }

    public String getCheckListId() {
        return checkListId;
    }

    public void setCheckListId(String checkListId) {
        this.checkListId = checkListId;
    }

    public String getCheckListName() {
        return checkListName;
    }

    public void setCheckListName(String checkListName) {
        this.checkListName = checkListName;
    }

    public String getCheckListStage() {
        return checkListStage;
    }

    public void setCheckListStage(String checkListStage) {
        this.checkListStage = checkListStage;
    }

    public String getCheckListStatus() {
        return checkListStatus;
    }

    public void setCheckListStatus(String checkListStatus) {
        this.checkListStatus = checkListStatus;
    }

    public String getCityState() {
        return cityState;
    }

    public void setCityState(String cityState) {
        this.cityState = cityState;
    }

    public String getConsigmentId() {
        return consigmentId;
    }

    public void setConsigmentId(String consigmentId) {
        this.consigmentId = consigmentId;
    }

    public String getEscalationccEmail1() {
        return escalationccEmail1;
    }

    public void setEscalationccEmail1(String escalationccEmail1) {
        this.escalationccEmail1 = escalationccEmail1;
    }

    public String getEscalationemailSub1() {
        return escalationemailSub1;
    }

    public void setEscalationemailSub1(String escalationemailSub1) {
        this.escalationemailSub1 = escalationemailSub1;
    }

    public String getEscalationtoEmail1() {
        return escalationtoEmail1;
    }

    public void setEscalationtoEmail1(String escalationtoEmail1) {
        this.escalationtoEmail1 = escalationtoEmail1;
    }

    public String getFrequencyId() {
        return frequencyId;
    }

    public void setFrequencyId(String frequencyId) {
        this.frequencyId = frequencyId;
    }

    public String getFrequencyName() {
        return frequencyName;
    }

    public void setFrequencyName(String frequencyName) {
        this.frequencyName = frequencyName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInvoicecustomer() {
        return invoicecustomer;
    }

    public void setInvoicecustomer(String invoicecustomer) {
        this.invoicecustomer = invoicecustomer;
    }

    public String getParameterDescription() {
        return parameterDescription;
    }

    public void setParameterDescription(String parameterDescription) {
        this.parameterDescription = parameterDescription;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getParameterUnit() {
        return parameterUnit;
    }

    public void setParameterUnit(String parameterUnit) {
        this.parameterUnit = parameterUnit;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    public String getReeferMaximumTemperature() {
        return reeferMaximumTemperature;
    }

    public void setReeferMaximumTemperature(String reeferMaximumTemperature) {
        this.reeferMaximumTemperature = reeferMaximumTemperature;
    }

    public String getReeferMinimumTemperature() {
        return reeferMinimumTemperature;
    }

    public void setReeferMinimumTemperature(String reeferMinimumTemperature) {
        this.reeferMinimumTemperature = reeferMinimumTemperature;
    }

    public String getRepeat() {
        return repeat;
    }

    public void setRepeat(String repeat) {
        this.repeat = repeat;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }

    public String getTotFreightCharges() {
        return totFreightCharges;
    }

    public void setTotFreightCharges(String totFreightCharges) {
        this.totFreightCharges = totFreightCharges;
    }

    public String getTotalDrivers() {
        return totalDrivers;
    }

    public void setTotalDrivers(String totalDrivers) {
        this.totalDrivers = totalDrivers;
    }

    public double getTotalExpenseAmt() {
        return totalExpenseAmt;
    }

    public void setTotalExpenseAmt(double totalExpenseAmt) {
        this.totalExpenseAmt = totalExpenseAmt;
    }

    public double getTotalFreightAmt() {
        return totalFreightAmt;
    }

    public void setTotalFreightAmt(double totalFreightAmt) {
        this.totalFreightAmt = totalFreightAmt;
    }

    public double getTotalTripFreightAmt() {
        return totalTripFreightAmt;
    }

    public void setTotalTripFreightAmt(double totalTripFreightAmt) {
        this.totalTripFreightAmt = totalTripFreightAmt;
    }

    public double getTotalTripKmRun() {
        return totalTripKmRun;
    }

    public void setTotalTripKmRun(double totalTripKmRun) {
        this.totalTripKmRun = totalTripKmRun;
    }

    public String getTotalWeights() {
        return totalWeights;
    }

    public void setTotalWeights(String totalWeights) {
        this.totalWeights = totalWeights;
    }

    public int getTotaldrivers() {
        return totaldrivers;
    }

    public void setTotaldrivers(int totaldrivers) {
        this.totaldrivers = totaldrivers;
    }

    public String getTotalkmrun() {
        return totalkmrun;
    }

    public void setTotalkmrun(String totalkmrun) {
        this.totalkmrun = totalkmrun;
    }

    public String getTripPlanningRemark() {
        return tripPlanningRemark;
    }

    public void setTripPlanningRemark(String tripPlanningRemark) {
        this.tripPlanningRemark = tripPlanningRemark;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getZoneid() {
        return zoneid;
    }

    public void setZoneid(String zoneid) {
        this.zoneid = zoneid;
    }

    public String getTripclosureid() {
        return tripclosureid;
    }

    public void setTripclosureid(String tripclosureid) {
        this.tripclosureid = tripclosureid;
    }

    public String getFleetCenterId() {
        return fleetCenterId;
    }

    public void setFleetCenterId(String fleetCenterId) {
        this.fleetCenterId = fleetCenterId;
    }

    public String getRcmexpense() {
        return rcmexpense;
    }

    public void setRcmexpense(String rcmexpense) {
        this.rcmexpense = rcmexpense;
    }

    public String getRequesttype() {
        return requesttype;
    }

    public void setRequesttype(String requesttype) {
        this.requesttype = requesttype;
    }

    public String getSystemexpense() {
        return systemexpense;
    }

    public void setSystemexpense(String systemexpense) {
        this.systemexpense = systemexpense;
    }

    public String getTotalrunhm() {
        return totalrunhm;
    }

    public void setTotalrunhm(String totalrunhm) {
        this.totalrunhm = totalrunhm;
    }

    public String getTotalrunkm() {
        return totalrunkm;
    }

    public void setTotalrunkm(String totalrunkm) {
        this.totalrunkm = totalrunkm;
    }

    public String[] getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String[] batchCode) {
        this.batchCode = batchCode;
    }

    public String[] getUom() {
        return uom;
    }

    public void setUom(String[] uom) {
        this.uom = uom;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public String getConsignmentStatus() {
        return consignmentStatus;
    }

    public void setConsignmentStatus(String consignmentStatus) {
        this.consignmentStatus = consignmentStatus;
    }

    public String getConsignmentVehicleId() {
        return consignmentVehicleId;
    }

    public void setConsignmentVehicleId(String consignmentVehicleId) {
        this.consignmentVehicleId = consignmentVehicleId;
    }

    public String getConsignmentVehicleStatus() {
        return consignmentVehicleStatus;
    }

    public void setConsignmentVehicleStatus(String consignmentVehicleStatus) {
        this.consignmentVehicleStatus = consignmentVehicleStatus;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getConsignmentRefNo() {
        return consignmentRefNo;
    }

    public void setConsignmentRefNo(String consignmentRefNo) {
        this.consignmentRefNo = consignmentRefNo;
    }

    public String getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(String pickupTime) {
        this.pickupTime = pickupTime;
    }

    public String getPickupdate() {
        return pickupdate;
    }

    public void setPickupdate(String pickupdate) {
        this.pickupdate = pickupdate;
    }

    public String getPoint1Type() {
        return point1Type;
    }

    public void setPoint1Type(String point1Type) {
        this.point1Type = point1Type;
    }

    public String getPoint2Type() {
        return point2Type;
    }

    public void setPoint2Type(String point2Type) {
        this.point2Type = point2Type;
    }

    public String getPoint3Type() {
        return point3Type;
    }

    public void setPoint3Type(String point3Type) {
        this.point3Type = point3Type;
    }

    public String getPoint4Type() {
        return point4Type;
    }

    public void setPoint4Type(String point4Type) {
        this.point4Type = point4Type;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getSmtp() {
        return smtp;
    }

    public void setSmtp(String smtp) {
        this.smtp = smtp;
    }

    public String getTomailId() {
        return tomailId;
    }

    public void setTomailId(String tomailId) {
        this.tomailId = tomailId;
    }

    public String getActualKmVehicleTypeId() {
        return actualKmVehicleTypeId;
    }

    public void setActualKmVehicleTypeId(String actualKmVehicleTypeId) {
        this.actualKmVehicleTypeId = actualKmVehicleTypeId;
    }

    public String[] getArticleId() {
        return articleId;
    }

    public void setArticleId(String[] articleId) {
        this.articleId = articleId;
    }

    public String getBatchName() {
        return batchName;
    }

    public void setBatchName(String batchName) {
        this.batchName = batchName;
    }

    public String getConsignmentArticleId() {
        return consignmentArticleId;
    }

    public void setConsignmentArticleId(String consignmentArticleId) {
        this.consignmentArticleId = consignmentArticleId;
    }

    public String getContractVehicleTypeId() {
        return contractVehicleTypeId;
    }

    public void setContractVehicleTypeId(String contractVehicleTypeId) {
        this.contractVehicleTypeId = contractVehicleTypeId;
    }

    public String getUnitOfMeasurement() {
        return unitOfMeasurement;
    }

    public void setUnitOfMeasurement(String unitOfMeasurement) {
        this.unitOfMeasurement = unitOfMeasurement;
    }

    public String getCurrentTemperature() {
        return currentTemperature;
    }

    public void setCurrentTemperature(String currentTemperature) {
        this.currentTemperature = currentTemperature;
    }

    public String getDistanceTravelled() {
        return distanceTravelled;
    }

    public void setDistanceTravelled(String distanceTravelled) {
        this.distanceTravelled = distanceTravelled;
    }

    public String getTripAdvaceId() {
        return tripAdvaceId;
    }

    public void setTripAdvaceId(String tripAdvaceId) {
        this.tripAdvaceId = tripAdvaceId;
    }

    public String getFleetCenterContactNo() {
        return fleetCenterContactNo;
    }

    public void setFleetCenterContactNo(String fleetCenterContactNo) {
        this.fleetCenterContactNo = fleetCenterContactNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public double getFactors() {
        return factors;
    }

    public void setFactors(double factors) {
        this.factors = factors;
    }

    public String getAccountManager() {
        return accountManager;
    }

    public void setAccountManager(String accountManager) {
        this.accountManager = accountManager;
    }

    public String getAccountManagerId() {
        return accountManagerId;
    }

    public void setAccountManagerId(String accountManagerId) {
        this.accountManagerId = accountManagerId;
    }

    public String getCreditDays() {
        return creditDays;
    }

    public void setCreditDays(String creditDays) {
        this.creditDays = creditDays;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getCustCode() {
        return custCode;
    }

    public void setCustCode(String custCode) {
        this.custCode = custCode;
    }

    public String getCustContactPerson() {
        return custContactPerson;
    }

    public void setCustContactPerson(String custContactPerson) {
        this.custContactPerson = custContactPerson;
    }

    public String getCustType() {
        return custType;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }

    public float getTotalExp() {
        return totalExp;
    }

    public void setTotalExp(float totalExp) {
        this.totalExp = totalExp;
    }

    public float getTotalRevenue() {
        return totalRevenue;
    }

    public void setTotalRevenue(float totalRevenue) {
        this.totalRevenue = totalRevenue;
    }

    public String getProductCategoryUnchecked() {
        return productCategoryUnchecked;
    }

    public void setProductCategoryUnchecked(String productCategoryUnchecked) {
        this.productCategoryUnchecked = productCategoryUnchecked;
    }

    public String getTemp1MaximumTemperature() {
        return temp1MaximumTemperature;
    }

    public void setTemp1MaximumTemperature(String temp1MaximumTemperature) {
        this.temp1MaximumTemperature = temp1MaximumTemperature;
    }

    public String getTemp1MinimumTemp() {
        return temp1MinimumTemp;
    }

    public void setTemp1MinimumTemp(String temp1MinimumTemp) {
        this.temp1MinimumTemp = temp1MinimumTemp;
    }

    public String getTemp1ReeferRequired() {
        return temp1ReeferRequired;
    }

    public void setTemp1ReeferRequired(String temp1ReeferRequired) {
        this.temp1ReeferRequired = temp1ReeferRequired;
    }

    public String getTempMaximumTemperature() {
        return tempMaximumTemperature;
    }

    public void setTempMaximumTemperature(String tempMaximumTemperature) {
        this.tempMaximumTemperature = tempMaximumTemperature;
    }

    public String getTempMinimumTemp() {
        return tempMinimumTemp;
    }

    public void setTempMinimumTemp(String tempMinimumTemp) {
        this.tempMinimumTemp = tempMinimumTemp;
    }

    public String getTempReeferRequired() {
        return tempReeferRequired;
    }

    public void setTempReeferRequired(String tempReeferRequired) {
        this.tempReeferRequired = tempReeferRequired;
    }

    public String getBpclTransactionId() {
        return bpclTransactionId;
    }

    public void setBpclTransactionId(String bpclTransactionId) {
        this.bpclTransactionId = bpclTransactionId;
    }

    public String getAdvanceStatus() {
        return advanceStatus;
    }

    public void setAdvanceStatus(String advanceStatus) {
        this.advanceStatus = advanceStatus;
    }

    public String getAdvanceToPayStatus() {
        return advanceToPayStatus;
    }

    public void setAdvanceToPayStatus(String advanceToPayStatus) {
        this.advanceToPayStatus = advanceToPayStatus;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getChequeRemarks() {
        return chequeRemarks;
    }

    public void setChequeRemarks(String chequeRemarks) {
        this.chequeRemarks = chequeRemarks;
    }

    public String getDraftNo() {
        return draftNo;
    }

    public void setDraftNo(String draftNo) {
        this.draftNo = draftNo;
    }

    public String getDraftRemarks() {
        return draftRemarks;
    }

    public void setDraftRemarks(String draftRemarks) {
        this.draftRemarks = draftRemarks;
    }

    public String getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(String paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getPaymentModeId() {
        return paymentModeId;
    }

    public void setPaymentModeId(String paymentModeId) {
        this.paymentModeId = paymentModeId;
    }

    public String getPaymentModeName() {
        return paymentModeName;
    }

    public void setPaymentModeName(String paymentModeName) {
        this.paymentModeName = paymentModeName;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getRtgsNo() {
        return rtgsNo;
    }

    public void setRtgsNo(String rtgsNo) {
        this.rtgsNo = rtgsNo;
    }

    public String getRtgsRemarks() {
        return rtgsRemarks;
    }

    public void setRtgsRemarks(String rtgsRemarks) {
        this.rtgsRemarks = rtgsRemarks;
    }

    public String getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(String paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getFuelUnite() {
        return fuelUnite;
    }

    public void setFuelUnite(String fuelUnite) {
        this.fuelUnite = fuelUnite;
    }

    public String getFuelTypeId() {
        return fuelTypeId;
    }

    public void setFuelTypeId(String fuelTypeId) {
        this.fuelTypeId = fuelTypeId;
    }

    public String getFuelTypeName() {
        return fuelTypeName;
    }

    public void setFuelTypeName(String fuelTypeName) {
        this.fuelTypeName = fuelTypeName;
    }

    public String getEmailCc() {
        return emailCc;
    }

    public void setEmailCc(String emailCc) {
        this.emailCc = emailCc;
    }

    public String getInfoEmailCc() {
        return infoEmailCc;
    }

    public void setInfoEmailCc(String infoEmailCc) {
        this.infoEmailCc = infoEmailCc;
    }

    public String getInfoEmailTo() {
        return infoEmailTo;
    }

    public void setInfoEmailTo(String infoEmailTo) {
        this.infoEmailTo = infoEmailTo;
    }

    public String getEmailTo() {
        return emailTo;
    }

    public void setEmailTo(String emailTo) {
        this.emailTo = emailTo;
    }

    public String getInfoEmptyTo() {
        return infoEmptyTo;
    }

    public void setInfoEmptyTo(String infoEmptyTo) {
        this.infoEmptyTo = infoEmptyTo;
    }

    public String getTimeApprovalStatus() {
        return timeApprovalStatus;
    }

    public void setTimeApprovalStatus(String timeApprovalStatus) {
        this.timeApprovalStatus = timeApprovalStatus;
    }

    public String getNextStatusId() {
        return nextStatusId;
    }

    public void setNextStatusId(String nextStatusId) {
        this.nextStatusId = nextStatusId;
    }

    public String getTripPod() {
        return tripPod;
    }

    public void setTripPod(String tripPod) {
        this.tripPod = tripPod;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getAmountExceed() {
        return amountExceed;
    }

    public void setAmountExceed(String amountExceed) {
        this.amountExceed = amountExceed;
    }

    public String getEstimatedExpense() {
        return estimatedExpense;
    }

    public void setEstimatedExpense(String estimatedExpense) {
        this.estimatedExpense = estimatedExpense;
    }

    public String getTotalRequestAmount() {
        return totalRequestAmount;
    }

    public void setTotalRequestAmount(String totalRequestAmount) {
        this.totalRequestAmount = totalRequestAmount;
    }

    public double getPaidAdvance() {
        return paidAdvance;
    }

    public void setPaidAdvance(double paidAdvance) {
        this.paidAdvance = paidAdvance;
    }

    public double getTripRevenue() {
        return tripRevenue;
    }

    public void setTripRevenue(double tripRevenue) {
        this.tripRevenue = tripRevenue;
    }

    public String getWfuDay() {
        return wfuDay;
    }

    public void setWfuDay(String wfuDay) {
        this.wfuDay = wfuDay;
    }

    public String getTripStatusId() {
        return tripStatusId;
    }

    public void setTripStatusId(String tripStatusId) {
        this.tripStatusId = tripStatusId;
    }

    public double getDeviateAmount() {
        return deviateAmount;
    }

    public void setDeviateAmount(double deviateAmount) {
        this.deviateAmount = deviateAmount;
    }

    public double getOutStanding() {
        return outStanding;
    }

    public void setOutStanding(double outStanding) {
        this.outStanding = outStanding;
    }

    public double getCreditLimitAmount() {
        return creditLimitAmount;
    }

    public void setCreditLimitAmount(double creditLimitAmount) {
        this.creditLimitAmount = creditLimitAmount;
    }

    public String getJobcardDays() {
        return jobcardDays;
    }

    public void setJobcardDays(String jobcardDays) {
        this.jobcardDays = jobcardDays;
    }

    public String getJobcardHours() {
        return jobcardHours;
    }

    public void setJobcardHours(String jobcardHours) {
        this.jobcardHours = jobcardHours;
    }

    public String getPlannedCompleteDays() {
        return plannedCompleteDays;
    }

    public void setPlannedCompleteDays(String plannedCompleteDays) {
        this.plannedCompleteDays = plannedCompleteDays;
    }

    public String getPlannedCompleteHours() {
        return plannedCompleteHours;
    }

    public void setPlannedCompleteHours(String plannedCompleteHours) {
        this.plannedCompleteHours = plannedCompleteHours;
    }

    public String getActualCompletionDate() {
        return actualCompletionDate;
    }

    public void setActualCompletionDate(String actualCompletionDate) {
        this.actualCompletionDate = actualCompletionDate;
    }

    public String getClosedBy() {
        return closedBy;
    }

    public void setClosedBy(String closedBy) {
        this.closedBy = closedBy;
    }

    public String getUsageTypeId() {
        return usageTypeId;
    }

    public void setUsageTypeId(String usageTypeId) {
        this.usageTypeId = usageTypeId;
    }

    public String getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(String serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getServiceTypeName() {
        return serviceTypeName;
    }

    public void setServiceTypeName(String serviceTypeName) {
        this.serviceTypeName = serviceTypeName;
    }

    public String getJobCardType() {
        return jobCardType;
    }

    public void setJobCardType(String jobCardType) {
        this.jobCardType = jobCardType;
    }

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        this.km = km;
    }

    public String getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(String priorityId) {
        this.priorityId = priorityId;
    }

    public String getReqHour() {
        return reqHour;
    }

    public void setReqHour(String reqHour) {
        this.reqHour = reqHour;
    }

    public String getReqMinute() {
        return reqMinute;
    }

    public void setReqMinute(String reqMinute) {
        this.reqMinute = reqMinute;
    }

    public String getReqSeconds() {
        return reqSeconds;
    }

    public void setReqSeconds(String reqSeconds) {
        this.reqSeconds = reqSeconds;
    }

    public String getServiceVendorId() {
        return serviceVendorId;
    }

    public void setServiceVendorId(String serviceVendorId) {
        this.serviceVendorId = serviceVendorId;
    }

    public String getTripEndHm() {
        return tripEndHm;
    }

    public void setTripEndHm(String tripEndHm) {
        this.tripEndHm = tripEndHm;
    }

    public String getExtraHmRateWithReefer() {
        return extraHmRateWithReefer;
    }

    public void setExtraHmRateWithReefer(String extraHmRateWithReefer) {
        this.extraHmRateWithReefer = extraHmRateWithReefer;
    }

    public String getExtraKmRateWithReefer() {
        return extraKmRateWithReefer;
    }

    public void setExtraKmRateWithReefer(String extraKmRateWithReefer) {
        this.extraKmRateWithReefer = extraKmRateWithReefer;
    }

    public String getExtraKmRateWithoutReefer() {
        return extraKmRateWithoutReefer;
    }

    public void setExtraKmRateWithoutReefer(String extraKmRateWithoutReefer) {
        this.extraKmRateWithoutReefer = extraKmRateWithoutReefer;
    }

    public String getTotalHm() {
        return totalHm;
    }

    public void setTotalHm(String totalHm) {
        this.totalHm = totalHm;
    }

    public String[] getFixedKmvehicleTypeId() {
        return fixedKmvehicleTypeId;
    }

    public void setFixedKmvehicleTypeId(String[] fixedKmvehicleTypeId) {
        this.fixedKmvehicleTypeId = fixedKmvehicleTypeId;
    }

    public String[] getFixedRateWithReefer() {
        return fixedRateWithReefer;
    }

    public void setFixedRateWithReefer(String[] fixedRateWithReefer) {
        this.fixedRateWithReefer = fixedRateWithReefer;
    }

    public String[] getFixedRateWithoutReefer() {
        return fixedRateWithoutReefer;
    }

    public void setFixedRateWithoutReefer(String[] fixedRateWithoutReefer) {
        this.fixedRateWithoutReefer = fixedRateWithoutReefer;
    }

    public String[] getFixedTotalHm() {
        return fixedTotalHm;
    }

    public void setFixedTotalHm(String[] fixedTotalHm) {
        this.fixedTotalHm = fixedTotalHm;
    }

    public String[] getFixedTotalKm() {
        return fixedTotalKm;
    }

    public void setFixedTotalKm(String[] fixedTotalKm) {
        this.fixedTotalKm = fixedTotalKm;
    }

    public String[] getVehicleNos() {
        return vehicleNos;
    }

    public void setVehicleNos(String[] vehicleNos) {
        this.vehicleNos = vehicleNos;
    }

    public String[] getFixedExtraHmRateWithReefer() {
        return fixedExtraHmRateWithReefer;
    }

    public void setFixedExtraHmRateWithReefer(String[] fixedExtraHmRateWithReefer) {
        this.fixedExtraHmRateWithReefer = fixedExtraHmRateWithReefer;
    }

    public String[] getFixedExtraKmRateWithReefer() {
        return fixedExtraKmRateWithReefer;
    }

    public void setFixedExtraKmRateWithReefer(String[] fixedExtraKmRateWithReefer) {
        this.fixedExtraKmRateWithReefer = fixedExtraKmRateWithReefer;
    }

    public String[] getFixedExtraKmRateWithoutReefer() {
        return fixedExtraKmRateWithoutReefer;
    }

    public void setFixedExtraKmRateWithoutReefer(String[] fixedExtraKmRateWithoutReefer) {
        this.fixedExtraKmRateWithoutReefer = fixedExtraKmRateWithoutReefer;
    }

    public String getContractFixedRateId() {
        return contractFixedRateId;
    }

    public void setContractFixedRateId(String contractFixedRateId) {
        this.contractFixedRateId = contractFixedRateId;
    }

    public String getContractVehicleNos() {
        return contractVehicleNos;
    }

    public void setContractVehicleNos(String contractVehicleNos) {
        this.contractVehicleNos = contractVehicleNos;
    }

    public String getExtraHmRatePerHmRateWithReefer() {
        return extraHmRatePerHmRateWithReefer;
    }

    public void setExtraHmRatePerHmRateWithReefer(String extraHmRatePerHmRateWithReefer) {
        this.extraHmRatePerHmRateWithReefer = extraHmRatePerHmRateWithReefer;
    }

    public String getExtraKmRatePerKmRateWithReefer() {
        return extraKmRatePerKmRateWithReefer;
    }

    public void setExtraKmRatePerKmRateWithReefer(String extraKmRatePerKmRateWithReefer) {
        this.extraKmRatePerKmRateWithReefer = extraKmRatePerKmRateWithReefer;
    }

    public String getExtraKmRatePerKmRateWithoutReefer() {
        return extraKmRatePerKmRateWithoutReefer;
    }

    public void setExtraKmRatePerKmRateWithoutReefer(String extraKmRatePerKmRateWithoutReefer) {
        this.extraKmRatePerKmRateWithoutReefer = extraKmRatePerKmRateWithoutReefer;
    }

    public String[] getEditId() {
        return editId;
    }

    public void setEditId(String[] editId) {
        this.editId = editId;
    }

    public String[] getFixedKmStatus() {
        return fixedKmStatus;
    }

    public void setFixedKmStatus(String[] fixedKmStatus) {
        this.fixedKmStatus = fixedKmStatus;
    }

    public String getEmptyTripPurpose() {
        return emptyTripPurpose;
    }

    public void setEmptyTripPurpose(String emptyTripPurpose) {
        this.emptyTripPurpose = emptyTripPurpose;
    }

    public String getRouteCostId() {
        return routeCostId;
    }

    public void setRouteCostId(String routeCostId) {
        this.routeCostId = routeCostId;
    }

    public String[] getRouteCostIds() {
        return routeCostIds;
    }

    public void setRouteCostIds(String[] routeCostIds) {
        this.routeCostIds = routeCostIds;
    }

    public String getOutStandingDate() {
        return outStandingDate;
    }

    public void setOutStandingDate(String outStandingDate) {
        this.outStandingDate = outStandingDate;
    }

    public int getCnoteCount() {
        return cnoteCount;
    }

    public void setCnoteCount(int cnoteCount) {
        this.cnoteCount = cnoteCount;
    }

    public String getJobCardCode() {
        return jobCardCode;
    }

    public void setJobCardCode(String jobCardCode) {
        this.jobCardCode = jobCardCode;
    }

    public String getJobCardIssueDate() {
        return jobCardIssueDate;
    }

    public void setJobCardIssueDate(String jobCardIssueDate) {
        this.jobCardIssueDate = jobCardIssueDate;
    }

    public String getJobCardRemarks() {
        return jobCardRemarks;
    }

    public void setJobCardRemarks(String jobCardRemarks) {
        this.jobCardRemarks = jobCardRemarks;
    }

    public String getJobCardTypeNew() {
        return jobCardTypeNew;
    }

    public void setJobCardTypeNew(String jobCardTypeNew) {
        this.jobCardTypeNew = jobCardTypeNew;
    }

    public String getGoogleCityName() {
        return googleCityName;
    }

    public void setGoogleCityName(String googleCityName) {
        this.googleCityName = googleCityName;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getOriginCityId() {
        return originCityId;
    }

    public void setOriginCityId(String originCityId) {
        this.originCityId = originCityId;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getGcnNo() {
        return gcnNo;
    }

    public void setGcnNo(String gcnNo) {
        this.gcnNo = gcnNo;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public String getEditFreightAmount() {
        return editFreightAmount;
    }

    public void setEditFreightAmount(String editFreightAmount) {
        this.editFreightAmount = editFreightAmount;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getSacCode() {
        return sacCode;
    }

    public void setSacCode(String sacCode) {
        this.sacCode = sacCode;
    }

    public String getSacDescription() {
        return sacDescription;
    }

    public void setSacDescription(String sacDescription) {
        this.sacDescription = sacDescription;
    }

    public String getGstCategoryId() {
        return gstCategoryId;
    }

    public void setGstCategoryId(String gstCategoryId) {
        this.gstCategoryId = gstCategoryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGstCategoryName() {
        return gstCategoryName;
    }

    public void setGstCategoryName(String gstCategoryName) {
        this.gstCategoryName = gstCategoryName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getGstCodeMasterId() {
        return gstCodeMasterId;
    }

    public void setGstCodeMasterId(String gstCodeMasterId) {
        this.gstCodeMasterId = gstCodeMasterId;
    }

    public String getGstName() {
        return gstName;
    }

    public void setGstName(String gstName) {
        this.gstName = gstName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getGstCode() {
        return gstCode;
    }

    public void setGstCode(String gstCode) {
        this.gstCode = gstCode;
    }

    public String getGstPercentage() {
        return gstPercentage;
    }

    public void setGstPercentage(String gstPercentage) {
        this.gstPercentage = gstPercentage;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public String getGstRateDetailId() {
        return gstRateDetailId;
    }

    public void setGstRateDetailId(String gstRateDetailId) {
        this.gstRateDetailId = gstRateDetailId;
    }

    public String getGstCategoryCode() {
        return gstCategoryCode;
    }

    public void setGstCategoryCode(String gstCategoryCode) {
        this.gstCategoryCode = gstCategoryCode;
    }

    public String getGstRateId() {
        return gstRateId;
    }

    public void setGstRateId(String gstRateId) {
        this.gstRateId = gstRateId;
    }

    public String getHsnName() {
        return hsnName;
    }

    public void setHsnName(String hsnName) {
        this.hsnName = hsnName;
    }

    public String getHsnCode() {
        return hsnCode;
    }

    public void setHsnCode(String hsnCode) {
        this.hsnCode = hsnCode;
    }

    public String getHsnDescription() {
        return hsnDescription;
    }

    public void setHsnDescription(String hsnDescription) {
        this.hsnDescription = hsnDescription;
    }

    public String getGstProductId() {
        return gstProductId;
    }

    public void setGstProductId(String gstProductId) {
        this.gstProductId = gstProductId;
    }

    public String getGstType() {
        return gstType;
    }

    public void setGstType(String gstType) {
        this.gstType = gstType;
    }

    public String getGstProductCategoryCode() {
        return gstProductCategoryCode;
    }

    public void setGstProductCategoryCode(String gstProductCategoryCode) {
        this.gstProductCategoryCode = gstProductCategoryCode;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getBillToCustomer() {
        return billToCustomer;
    }

    public void setBillToCustomer(String billToCustomer) {
        this.billToCustomer = billToCustomer;
    }

    public String getReimbursement() {
        return reimbursement;
    }

    public void setReimbursement(String reimbursement) {
        this.reimbursement = reimbursement;
    }

    public String[] getConsigneeAddresss() {
        return consigneeAddresss;
    }

    public void setConsigneeAddresss(String[] consigneeAddresss) {
        this.consigneeAddresss = consigneeAddresss;
    }

    public String[] getConsigneeNames() {
        return consigneeNames;
    }

    public void setConsigneeNames(String[] consigneeNames) {
        this.consigneeNames = consigneeNames;
    }

    public String[] getConsigneePhoneNos() {
        return consigneePhoneNos;
    }

    public void setConsigneePhoneNos(String[] consigneePhoneNos) {
        this.consigneePhoneNos = consigneePhoneNos;
    }

    public String[] getConsignorAddresss() {
        return consignorAddresss;
    }

    public void setConsignorAddresss(String[] consignorAddresss) {
        this.consignorAddresss = consignorAddresss;
    }

    public String[] getConsignorNames() {
        return consignorNames;
    }

    public void setConsignorNames(String[] consignorNames) {
        this.consignorNames = consignorNames;
    }

    public String[] getConsignorPhoneNos() {
        return consignorPhoneNos;
    }

    public void setConsignorPhoneNos(String[] consignorPhoneNos) {
        this.consignorPhoneNos = consignorPhoneNos;
    }

    public String getConsignorId() {
        return consignorId;
    }

    public void setConsignorId(String consignorId) {
        this.consignorId = consignorId;
    }

    public String getConsigneeId() {
        return consigneeId;
    }

    public void setConsigneeId(String consigneeId) {
        this.consigneeId = consigneeId;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String[] getPhoneNos() {
        return phoneNos;
    }

    public void setPhoneNos(String[] phoneNos) {
        this.phoneNos = phoneNos;
    }

    public String[] getConsigneeIds() {
        return consigneeIds;
    }

    public void setConsigneeIds(String[] consigneeIds) {
        this.consigneeIds = consigneeIds;
    }

    public String[] getConsignorIds() {
        return consignorIds;
    }

    public void setConsignorIds(String[] consignorIds) {
        this.consignorIds = consignorIds;
    }

    public String getWalkinCustomerId() {
        return walkinCustomerId;
    }

    public void setWalkinCustomerId(String walkinCustomerId) {
        this.walkinCustomerId = walkinCustomerId;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public String[] getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String[] deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String[] getInvoicedNo() {
        return invoicedNo;
    }

    public void setInvoicedNo(String[] invoicedNo) {
        this.invoicedNo = invoicedNo;
    }

    public String[] getWeightKG() {
        return weightKG;
    }

    public void setWeightKG(String[] weightKG) {
        this.weightKG = weightKG;
    }

    public String[] getMinWgtBase() {
        return minWgtBase;
    }

    public void setMinWgtBase(String[] minWgtBase) {
        this.minWgtBase = minWgtBase;
    }

    public String getMinimumBaseWgt() {
        return minimumBaseWgt;
    }

    public void setMinimumBaseWgt(String minimumBaseWgt) {
        this.minimumBaseWgt = minimumBaseWgt;
    }

    public String[] getGcnNos() {
        return gcnNos;
    }

    public void setGcnNos(String[] gcnNos) {
        this.gcnNos = gcnNos;
    }

    public String[] getConsigDate() {
        return consigDate;
    }

    public void setConsigDate(String[] consigDate) {
        this.consigDate = consigDate;
    }

    public String[] getCustRefDate() {
        return custRefDate;
    }

    public void setCustRefDate(String[] custRefDate) {
        this.custRefDate = custRefDate;
    }

    
//    CBT


    public String getAirWayBillNo() {
        return airWayBillNo;
    }

    public void setAirWayBillNo(String airWayBillNo) {
        this.airWayBillNo = airWayBillNo;
    }

    public String getAwbNo() {
        return awbNo;
    }

    public void setAwbNo(String awbNo) {
        this.awbNo = awbNo;
    }

    public String getAwbType() {
        return awbType;
    }

    public void setAwbType(String awbType) {
        this.awbType = awbType;
    }

    public String getAllowedTrucks() {
        return allowedTrucks;
    }

    public void setAllowedTrucks(String allowedTrucks) {
        this.allowedTrucks = allowedTrucks;
    }

    public String getTruckNos() {
        return truckNos;
    }

    public void setTruckNos(String truckNos) {
        this.truckNos = truckNos;
    }

    public String getTotalVolumeActual() {
        return totalVolumeActual;
    }

    public void setTotalVolumeActual(String totalVolumeActual) {
        this.totalVolumeActual = totalVolumeActual;
    }

    public String getAllBranchAccess() {
        return allBranchAccess;
    }

    public void setAllBranchAccess(String allBranchAccess) {
        this.allBranchAccess = allBranchAccess;
    }

    public String getTotalBooking() {
        return totalBooking;
    }

    public void setTotalBooking(String totalBooking) {
        this.totalBooking = totalBooking;
    }

    public String getTotalGrossWeight() {
        return totalGrossWeight;
    }

    public void setTotalGrossWeight(String totalGrossWeight) {
        this.totalGrossWeight = totalGrossWeight;
    }

    public String getTotalChargeableWeight() {
        return totalChargeableWeight;
    }

    public void setTotalChargeableWeight(String totalChargeableWeight) {
        this.totalChargeableWeight = totalChargeableWeight;
    }

    public String getAwbTotalGrossWeight() {
        return awbTotalGrossWeight;
    }

    public void setAwbTotalGrossWeight(String awbTotalGrossWeight) {
        this.awbTotalGrossWeight = awbTotalGrossWeight;
    }

    public String getAwbTotalPackages() {
        return awbTotalPackages;
    }

    public void setAwbTotalPackages(String awbTotalPackages) {
        this.awbTotalPackages = awbTotalPackages;
    }

    public String getAwbReceivedPackages() {
        return awbReceivedPackages;
    }

    public void setAwbReceivedPackages(String awbReceivedPackages) {
        this.awbReceivedPackages = awbReceivedPackages;
    }

    public String getAwbPendingPackages() {
        return awbPendingPackages;
    }

    public void setAwbPendingPackages(String awbPendingPackages) {
        this.awbPendingPackages = awbPendingPackages;
    }

    public String getWareHouseName() {
        return wareHouseName;
    }

    public void setWareHouseName(String wareHouseName) {
        this.wareHouseName = wareHouseName;
    }

    public String getWareHouseLocation() {
        return wareHouseLocation;
    }

    public void setWareHouseLocation(String wareHouseLocation) {
        this.wareHouseLocation = wareHouseLocation;
    }

    public String getShipmentAcceptanceDate() {
        return shipmentAcceptanceDate;
    }

    public void setShipmentAcceptanceDate(String shipmentAcceptanceDate) {
        this.shipmentAcceptanceDate = shipmentAcceptanceDate;
    }

    public String getShipmentAccpHour() {
        return shipmentAccpHour;
    }

    public void setShipmentAccpHour(String shipmentAccpHour) {
        this.shipmentAccpHour = shipmentAccpHour;
    }

    public String getShipmentAccpMinute() {
        return shipmentAccpMinute;
    }

    public void setShipmentAccpMinute(String shipmentAccpMinute) {
        this.shipmentAccpMinute = shipmentAccpMinute;
    }

    public String getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(String currencyType) {
        this.currencyType = currencyType;
    }

    public String getRateMode() {
        return rateMode;
    }

    public void setRateMode(String rateMode) {
        this.rateMode = rateMode;
    }

    public String[] getBookingReferenceRemarks() {
        return bookingReferenceRemarks;
    }

    public void setBookingReferenceRemarks(String[] bookingReferenceRemarks) {
        this.bookingReferenceRemarks = bookingReferenceRemarks;
    }

    public String getOrderDeliveryHour() {
        return orderDeliveryHour;
    }

    public void setOrderDeliveryHour(String orderDeliveryHour) {
        this.orderDeliveryHour = orderDeliveryHour;
    }

    public String getOrderDeliveryMinute() {
        return orderDeliveryMinute;
    }

    public void setOrderDeliveryMinute(String orderDeliveryMinute) {
        this.orderDeliveryMinute = orderDeliveryMinute;
    }

    public String getPerKgRate() {
        return perKgRate;
    }

    public void setPerKgRate(String perKgRate) {
        this.perKgRate = perKgRate;
    }

    public String getBookingRemarks() {
        return bookingRemarks;
    }

    public void setBookingRemarks(String bookingRemarks) {
        this.bookingRemarks = bookingRemarks;
    }

    public String getVehicleInfo() {
        return vehicleInfo;
    }

    public void setVehicleInfo(String vehicleInfo) {
        this.vehicleInfo = vehicleInfo;
    }

    public String getVehicleCapacity() {
        return vehicleCapacity;
    }

    public void setVehicleCapacity(String vehicleCapacity) {
        this.vehicleCapacity = vehicleCapacity;
    }

    public String getTruckCode() {
        return truckCode;
    }

    public void setTruckCode(String truckCode) {
        this.truckCode = truckCode;
    }

    public String getFleetCode() {
        return fleetCode;
    }

    public void setFleetCode(String fleetCode) {
        this.fleetCode = fleetCode;
    }

    public String getFleetCapacity() {
        return fleetCapacity;
    }

    public void setFleetCapacity(String fleetCapacity) {
        this.fleetCapacity = fleetCapacity;
    }

    public String getFleetVolume() {
        return fleetVolume;
    }

    public void setFleetVolume(String fleetVolume) {
        this.fleetVolume = fleetVolume;
    }

    public String getFleetName() {
        return fleetName;
    }

    public void setFleetName(String fleetName) {
        this.fleetName = fleetName;
    }

    public String getFleetId() {
        return fleetId;
    }

    public void setFleetId(String fleetId) {
        this.fleetId = fleetId;
    }

    public String getTruckName() {
        return truckName;
    }

    public void setTruckName(String truckName) {
        this.truckName = truckName;
    }

    public String getAwbOriginId() {
        return awbOriginId;
    }

    public void setAwbOriginId(String awbOriginId) {
        this.awbOriginId = awbOriginId;
    }

    public String getAwbOriginName() {
        return awbOriginName;
    }

    public void setAwbOriginName(String awbOriginName) {
        this.awbOriginName = awbOriginName;
    }

    public String getAwbDestinationName() {
        return awbDestinationName;
    }

    public void setAwbDestinationName(String awbDestinationName) {
        this.awbDestinationName = awbDestinationName;
    }

    public String getAwbDestinationId() {
        return awbDestinationId;
    }

    public void setAwbDestinationId(String awbDestinationId) {
        this.awbDestinationId = awbDestinationId;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getBreadth() {
        return breadth;
    }

    public void setBreadth(String breadth) {
        this.breadth = breadth;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getChargeAbleWeigth() {
        return chargeAbleWeigth;
    }

    public void setChargeAbleWeigth(String chargeAbleWeigth) {
        this.chargeAbleWeigth = chargeAbleWeigth;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String[] getTruckOriginId() {
        return truckOriginId;
    }

    public void setTruckOriginId(String[] truckOriginId) {
        this.truckOriginId = truckOriginId;
    }

    public String[] getAvailableCap() {
        return availableCap;
    }

    public void setAvailableCap(String[] availableCap) {
        this.availableCap = availableCap;
    }

    public String[] getAssignedCap() {
        return assignedCap;
    }

    public void setAssignedCap(String[] assignedCap) {
        this.assignedCap = assignedCap;
    }

    public String getRateMode1() {
        return rateMode1;
    }

    public void setRateMode1(String rateMode1) {
        this.rateMode1 = rateMode1;
    }

    public String getRateValue1() {
        return rateValue1;
    }

    public void setRateValue1(String rateValue1) {
        this.rateValue1 = rateValue1;
    }

    public String getDepartOnTime() {
        return departOnTime;
    }

    public void setDepartOnTime(String departOnTime) {
        this.departOnTime = departOnTime;
    }

    public String getTruckAvailable() {
        return truckAvailable;
    }

    public void setTruckAvailable(String truckAvailable) {
        this.truckAvailable = truckAvailable;
    }

    public String getTruckAssigned() {
        return truckAssigned;
    }

    public void setTruckAssigned(String truckAssigned) {
        this.truckAssigned = truckAssigned;
    }

    public String getConsignmentRouteCourseId() {
        return consignmentRouteCourseId;
    }

    public void setConsignmentRouteCourseId(String consignmentRouteCourseId) {
        this.consignmentRouteCourseId = consignmentRouteCourseId;
    }

    public String getCustomsStatus() {
        return customsStatus;
    }

    public void setCustomsStatus(String customsStatus) {
        this.customsStatus = customsStatus;
    }

    public String[] getConsArticleId() {
        return consArticleId;
    }

    public void setConsArticleId(String[] consArticleId) {
        this.consArticleId = consArticleId;
    }

    public String[] getConsRouteCourseId() {
        return consRouteCourseId;
    }

    public void setConsRouteCourseId(String[] consRouteCourseId) {
        this.consRouteCourseId = consRouteCourseId;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getAirWiseBillNo() {
        return airWiseBillNo;
    }

    public void setAirWiseBillNo(String airWiseBillNo) {
        this.airWiseBillNo = airWiseBillNo;
    }

    public String getLoadStatus() {
        return loadStatus;
    }

    public void setLoadStatus(String loadStatus) {
        this.loadStatus = loadStatus;
    }

    public String getLoadTime() {
        return loadTime;
    }

    public void setLoadTime(String loadTime) {
        this.loadTime = loadTime;
    }

    public String getCustomerDoc() {
        return customerDoc;
    }

    public void setCustomerDoc(String customerDoc) {
        this.customerDoc = customerDoc;
    }

    public String getUnloadStatus() {
        return unloadStatus;
    }

    public void setUnloadStatus(String unloadStatus) {
        this.unloadStatus = unloadStatus;
    }

    public String getUnloadTime() {
        return unloadTime;
    }

    public void setUnloadTime(String unloadTime) {
        this.unloadTime = unloadTime;
    }

    public String getPodRecieved() {
        return podRecieved;
    }

    public void setPodRecieved(String podRecieved) {
        this.podRecieved = podRecieved;
    }

    public String getAwbOriginRegion() {
        return awbOriginRegion;
    }

    public void setAwbOriginRegion(String awbOriginRegion) {
        this.awbOriginRegion = awbOriginRegion;
    }

    public String getOrderReferenceEnd() {
        return orderReferenceEnd;
    }

    public void setOrderReferenceEnd(String orderReferenceEnd) {
        this.orderReferenceEnd = orderReferenceEnd;
    }

    public int getParcelNo() {
        return parcelNo;
    }

    public void setParcelNo(int parcelNo) {
        this.parcelNo = parcelNo;
    }

    public String getConsignmentOrder() {
        return consignmentOrder;
    }

    public void setConsignmentOrder(String consignmentOrder) {
        this.consignmentOrder = consignmentOrder;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String[] getUsedVol() {
        return usedVol;
    }

    public void setUsedVol(String[] usedVol) {
        this.usedVol = usedVol;
    }

    public String[] getUsedCapacity() {
        return usedCapacity;
    }

    public void setUsedCapacity(String[] usedCapacity) {
        this.usedCapacity = usedCapacity;
    }

    public String getReasonId() {
        return reasonId;
    }

    public void setReasonId(String reasonId) {
        this.reasonId = reasonId;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    public String getTruckId() {
        return truckId;
    }

    public void setTruckId(String truckId) {
        this.truckId = truckId;
    }

    public String getTruckCodeId() {
        return truckCodeId;
    }

    public void setTruckCodeId(String truckCodeId) {
        this.truckCodeId = truckCodeId;
    }

    public String[] getContractFreightTable1() {
        return contractFreightTable1;
    }

    public void setContractFreightTable1(String[] contractFreightTable1) {
        this.contractFreightTable1 = contractFreightTable1;
    }
      
    
    public String getOrderReferenceAwb() {
        return orderReferenceAwb;
    }

    public void setOrderReferenceAwb(String orderReferenceAwb) {
        this.orderReferenceAwb = orderReferenceAwb;
    }

    public String[] getTruckTravelKm() {
        return truckTravelKm;
    }

    public void setTruckTravelKm(String[] truckTravelKm) {
        this.truckTravelKm = truckTravelKm;
    }

    public String[] getTruckTravelHour() {
        return truckTravelHour;
    }

    public void setTruckTravelHour(String[] truckTravelHour) {
        this.truckTravelHour = truckTravelHour;
    }

    public String[] getTruckTravelMinute() {
        return truckTravelMinute;
    }

    public void setTruckTravelMinute(String[] truckTravelMinute) {
        this.truckTravelMinute = truckTravelMinute;
    }

    public String[] getFleetTypeId() {
        return fleetTypeId;
    }

    public void setFleetTypeId(String[] fleetTypeId) {
        this.fleetTypeId = fleetTypeId;
    }

    public String[] getTruckDestinationId() {
        return truckDestinationId;
    }

    public void setTruckDestinationId(String[] truckDestinationId) {
        this.truckDestinationId = truckDestinationId;
    }

    public String[] getTruckRouteId() {
        return truckRouteId;
    }

    public void setTruckRouteId(String[] truckRouteId) {
        this.truckRouteId = truckRouteId;
    }

    public String getRateValue() {
        return rateValue;
    }

    public void setRateValue(String rateValue) {
        this.rateValue = rateValue;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getFreightReceipt() {
        return freightReceipt;
    }

    public void setFreightReceipt(String freightReceipt) {
        this.freightReceipt = freightReceipt;
    }

    public String[] getNetUnits() {
        return netUnits;
    }

    public void setNetUnits(String[] netUnits) {
        this.netUnits = netUnits;
    }

    public String[] getNetQuantity() {
        return netQuantity;
    }

    public void setNetQuantity(String[] netQuantity) {
        this.netQuantity = netQuantity;
    }

    public String getAwbOrigin() {
        return awbOrigin;
    }

    public void setAwbOrigin(String awbOrigin) {
        this.awbOrigin = awbOrigin;
    }

    public String getAwbDestination() {
        return awbDestination;
    }

    public void setAwbDestination(String awbDestination) {
        this.awbDestination = awbDestination;
    }

    public String getAwbOrderDeliveryDate() {
        return awbOrderDeliveryDate;
    }

    public void setAwbOrderDeliveryDate(String awbOrderDeliveryDate) {
        this.awbOrderDeliveryDate = awbOrderDeliveryDate;
    }

    public String getAwbDestinationRegion() {
        return awbDestinationRegion;
    }

    public void setAwbDestinationRegion(String awbDestinationRegion) {
        this.awbDestinationRegion = awbDestinationRegion;
    }

    public String getAwbMovementType() {
        return awbMovementType;
    }

    public void setAwbMovementType(String awbMovementType) {
        this.awbMovementType = awbMovementType;
    }

    public String[] getNoOfPieces() {
        return noOfPieces;
    }

    public void setNoOfPieces(String[] noOfPieces) {
        this.noOfPieces = noOfPieces;
    }

    public String[] getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(String[] grossWeight) {
        this.grossWeight = grossWeight;
    }

    public String[] getChargeableWeight() {
        return chargeableWeight;
    }

    public void setChargeableWeight(String[] chargeableWeight) {
        this.chargeableWeight = chargeableWeight;
    }

    public String[] getChargeableWeightId() {
        return chargeableWeightId;
    }

    public void setChargeableWeightId(String[] chargeableWeightId) {
        this.chargeableWeightId = chargeableWeightId;
    }

    public String[] getLengths() {
        return lengths;
    }

    public void setLengths(String[] lengths) {
        this.lengths = lengths;
    }

    public String[] getWidths() {
        return widths;
    }

    public void setWidths(String[] widths) {
        this.widths = widths;
    }

    public String[] getHeights() {
        return heights;
    }

    public void setHeights(String[] heights) {
        this.heights = heights;
    }

    public String[] getDgHandlingCode() {
        return dgHandlingCode;
    }

    public void setDgHandlingCode(String[] dgHandlingCode) {
        this.dgHandlingCode = dgHandlingCode;
    }

    public String[] getUnIdNo() {
        return unIdNo;
    }

    public void setUnIdNo(String[] unIdNo) {
        this.unIdNo = unIdNo;
    }

    public String getTotalChargeableWeights() {
        return totalChargeableWeights;
    }

    public void setTotalChargeableWeights(String totalChargeableWeights) {
        this.totalChargeableWeights = totalChargeableWeights;
    }

    public String getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(String totalVolume) {
        this.totalVolume = totalVolume;
    }

    public String[] getAvailableWeight() {
        return availableWeight;
    }

    public void setAvailableWeight(String[] availableWeight) {
        this.availableWeight = availableWeight;
    }

    public String[] getAvailableVolume() {
        return availableVolume;
    }

    public void setAvailableVolume(String[] availableVolume) {
        this.availableVolume = availableVolume;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    public String getShipmentType() {
        return shipmentType;
    }

    public void setShipmentType(String shipmentType) {
        this.shipmentType = shipmentType;
    }

    public String[] getTruckDepDate() {
        return truckDepDate;
    }

    public void setTruckDepDate(String[] truckDepDate) {
        this.truckDepDate = truckDepDate;
    }
    
    

    public String getOrderReferenceAwbNo() {
        return orderReferenceAwbNo;
    }

    public void setOrderReferenceAwbNo(String orderReferenceAwbNo) {
        this.orderReferenceAwbNo = orderReferenceAwbNo;
    }

    public String[] getVehicleIds() {
        return vehicleIds;
    }

    public void setVehicleIds(String[] vehicleIds) {
        this.vehicleIds = vehicleIds;
    }

    public String[] getVehicleRegNo() {
        return vehicleRegNo;
    }

    public void setVehicleRegNo(String[] vehicleRegNo) {
        this.vehicleRegNo = vehicleRegNo;
    }

    public String getProductRate() {
        return productRate;
    }

    public void setProductRate(String productRate) {
        this.productRate = productRate;
    }

    public String[] getVolumes() {
        return volumes;
    }

    public void setVolumes(String[] volumes) {
        this.volumes = volumes;
    }

    public String getCommodity() {
        return commodity;
    }

    public void setCommodity(String commodity) {
        this.commodity = commodity;
    }

    public String[] getClassCode() {
        return classCode;
    }

    public void setClassCode(String[] classCode) {
        this.classCode = classCode;
    }

    public String[] getPkgInstruction() {
        return pkgInstruction;
    }

    public void setPkgInstruction(String[] pkgInstruction) {
        this.pkgInstruction = pkgInstruction;
    }

    public String[] getPkgGroup() {
        return pkgGroup;
    }

    public void setPkgGroup(String[] pkgGroup) {
        this.pkgGroup = pkgGroup;
    }

    public String[] getReceivedPackages() {
        return receivedPackages;
    }

    public void setReceivedPackages(String[] receivedPackages) {
        this.receivedPackages = receivedPackages;
    }
    //    CBT

    public String getOriginId() {
        return originId;
    }

    public void setOriginId(String originId) {
        this.originId = originId;
    }

    public String getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }

    public String getOriginName() {
        return originName;
    }

    public void setOriginName(String originName) {
        this.originName = originName;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public String getPendingPackages() {
        return pendingPackages;
    }

    public void setPendingPackages(String pendingPackages) {
        this.pendingPackages = pendingPackages;
    }

    public String getPendingWeight() {
        return pendingWeight;
    }

    public void setPendingWeight(String pendingWeight) {
        this.pendingWeight = pendingWeight;
    }

    public String[] getGrossWG() {
        return grossWG;
    }

    public void setGrossWG(String[] grossWG) {
        this.grossWG = grossWG;
    }

    public String getCancellationInvoice() {
        return cancellationInvoice;
    }

    public void setCancellationInvoice(String cancellationInvoice) {
        this.cancellationInvoice = cancellationInvoice;
    }

    public String getFuelBillFlag() {
        return fuelBillFlag;
    }

    public void setFuelBillFlag(String fuelBillFlag) {
        this.fuelBillFlag = fuelBillFlag;
    }

    public String getOnwardReturnType() {
        return onwardReturnType;
    }

    public void setOnwardReturnType(String onwardReturnType) {
        this.onwardReturnType = onwardReturnType;
    }

    public String getFuelInvoiceNo() {
        return fuelInvoiceNo;
    }

    public void setFuelInvoiceNo(String fuelInvoiceNo) {
        this.fuelInvoiceNo = fuelInvoiceNo;
    }

    public String getDelAdress() {
        return delAdress;
    }

    public void setDelAdress(String delAdress) {
        this.delAdress = delAdress;
    }

    public String getKG() {
        return KG;
    }

    public void setKG(String KG) {
        this.KG = KG;
    }

    public String getCusRefDate() {
        return cusRefDate;
    }

    public void setCusRefDate(String cusRefDate) {
        this.cusRefDate = cusRefDate;
    }

    public String getGrossWeights() {
        return grossWeights;
    }

    public void setGrossWeights(String grossWeights) {
        this.grossWeights = grossWeights;
    }
    
    public String getPointNames() {
        return pointNames;
    }
    
    public void setPointNames(String pointNames) {
        this.pointNames = pointNames;
}

    public String getRouteConsContractId() {
        return routeConsContractId;
    }

    public void setRouteConsContractId(String routeConsContractId) {
        this.routeConsContractId = routeConsContractId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getUserMovementType() {
        return userMovementType;
    }

    public void setUserMovementType(String userMovementType) {
        this.userMovementType = userMovementType;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getCompanyID1() {
        return companyID1;
    }

    public void setCompanyID1(String companyID1) {
        this.companyID1 = companyID1;
    }

    public String getPenality() {
        return penality;
    }

    public void setPenality(String penality) {
        this.penality = penality;
    }

    public String getChargeamount() {
        return chargeamount;
    }

    public void setChargeamount(String chargeamount) {
        this.chargeamount = chargeamount;
    }

    public String getPcmunit() {
        return pcmunit;
    }

    public void setPcmunit(String pcmunit) {
        this.pcmunit = pcmunit;
    }

    public String getPcmremarks() {
        return pcmremarks;
    }

    public void setPcmremarks(String pcmremarks) {
        this.pcmremarks = pcmremarks;
    }

    public String getDetention() {
        return detention;
    }

    public void setDetention(String detention) {
        this.detention = detention;
    }

    public String getChargeamt() {
        return chargeamt;
    }

    public void setChargeamt(String chargeamt) {
        this.chargeamt = chargeamt;
    }

    public String getDcmunit() {
        return dcmunit;
    }

    public void setDcmunit(String dcmunit) {
        this.dcmunit = dcmunit;
    }

    public String getDcmunitTo() {
        return dcmunitTo;
    }

    public void setDcmunitTo(String dcmunitTo) {
        this.dcmunitTo = dcmunitTo;
    }

    public String getDcmremarks() {
        return dcmremarks;
    }

    public void setDcmremarks(String dcmremarks) {
        this.dcmremarks = dcmremarks;
    }

    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    public String getTimeSlotTo() {
        return timeSlotTo;
    }

    public void setTimeSlotTo(String timeSlotTo) {
        this.timeSlotTo = timeSlotTo;
    }

    public String getTatTime() {
        return tatTime;
    }

    public void setTatTime(String tatTime) {
        this.tatTime = tatTime;
    }

    public String getFuelMonth() {
        return fuelMonth;
    }

    public void setFuelMonth(String fuelMonth) {
        this.fuelMonth = fuelMonth;
    }

    public String getFuelYear() {
        return fuelYear;
    }

    public void setFuelYear(String fuelYear) {
        this.fuelYear = fuelYear;
    }

    public String getCityRegion() {
        return cityRegion;
    }

    public void setCityRegion(String cityRegion) {
        this.cityRegion = cityRegion;
    }  

    public String getTripEndDate() {
        return tripEndDate;
    }

    public void setTripEndDate(String tripEndDate) {
        this.tripEndDate = tripEndDate;
    }

    public String getDeliveryService() {
        return deliveryService;
    }

    public void setDeliveryService(String deliveryService) {
        this.deliveryService = deliveryService;
    }

    public String getMovementTypeId() {
        return movementTypeId;
    }

    public void setMovementTypeId(String movementTypeId) {
        this.movementTypeId = movementTypeId;
    }

    public String getMovementTypeName() {
        return movementTypeName;
    }

    public void setMovementTypeName(String movementTypeName) {
        this.movementTypeName = movementTypeName;
    }

    public String getPackageDescription() {
        return packageDescription;
    }

    public void setPackageDescription(String packageDescription) {
        this.packageDescription = packageDescription;
    }

    public String getRouteTAT() {
        return routeTAT;
    }

    public void setRouteTAT(String routeTAT) {
        this.routeTAT = routeTAT;
    }

    public String getNewRouteFlag() {
        return newRouteFlag;
    }

    public void setNewRouteFlag(String newRouteFlag) {
        this.newRouteFlag = newRouteFlag;
    }

    public String getOnwardReturn() {
        return onwardReturn;
    }

    public void setOnwardReturn(String onwardReturn) {
        this.onwardReturn = onwardReturn;
    }

    public String getOnwardTrip() {
        return onwardTrip;
    }

    public void setOnwardTrip(String onwardTrip) {
        this.onwardTrip = onwardTrip;
    }

    public String getOriginlatitude() {
        return originlatitude;
    }

    public void setOriginlatitude(String originlatitude) {
        this.originlatitude = originlatitude;
    }

    public String getOriginlongitude() {
        return originlongitude;
    }

    public void setOriginlongitude(String originlongitude) {
        this.originlongitude = originlongitude;
    }

    public String getDestinationlatitude() {
        return destinationlatitude;
    }

    public void setDestinationlatitude(String destinationlatitude) {
        this.destinationlatitude = destinationlatitude;
    }

    public String getDestinationlongitude() {
        return destinationlongitude;
    }

    public void setDestinationlongitude(String destinationlongitude) {
        this.destinationlongitude = destinationlongitude;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public String getWalkinPanNo() {
        return walkinPanNo;
    }

    public void setWalkinPanNo(String walkinPanNo) {
        this.walkinPanNo = walkinPanNo;
    }

    public String getWalkinGSTNo() {
        return walkinGSTNo;
    }

    public void setWalkinGSTNo(String walkinGSTNo) {
        this.walkinGSTNo = walkinGSTNo;
    }

    public String getWalkinConsoignor() {
        return walkinConsoignor;
    }

    public void setWalkinConsoignor(String walkinConsoignor) {
        this.walkinConsoignor = walkinConsoignor;
    }

    public String getWalkinConsignee() {
        return walkinConsignee;
    }

    public void setWalkinConsignee(String walkinConsignee) {
        this.walkinConsignee = walkinConsignee;
    }

    

    public String getNoOfTrucks() {
        return noOfTrucks;
    }

    public void setNoOfTrucks(String noOfTrucks) {
        this.noOfTrucks = noOfTrucks;
    }

    public String getVehId() {
        return vehId;
    }

    public void setVehId(String vehId) {
        this.vehId = vehId;
    }

    public String getVehicleNus() {
        return vehicleNus;
    }

    public void setVehicleNus(String vehicleNus) {
        this.vehicleNus = vehicleNus;
    }
 
    
}