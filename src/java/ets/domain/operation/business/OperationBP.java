/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.operation.business;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.operation.data.OperationDAO;
import java.util.ArrayList;
import java.util.Iterator;
import ets.arch.util.FPUtil;
import java.io.IOException;
import java.util.Map;
import java.util.List;
import java.io.File;
import java.io.*;
import jxl.*;
import java.util.*;
import jxl.Workbook;
import jxl.read.biff.*;
import com.ibatis.sqlmap.client.*;

/**
 *
 * @author vijay
 */
public class OperationBP {

    static FPUtil fpUtil = FPUtil.getInstance();
    static final int poTextSplitSize = Integer.parseInt(fpUtil.getInstance().getProperty("PO_ADDRESS_SPLIT"));
    OperationDAO operationDAO;

    public OperationDAO getOperationDAO() {
        return operationDAO;
    }

    public void setOperationDAO(OperationDAO operationDAO) {
        this.operationDAO = operationDAO;
    }

    public ArrayList processGetMfrList() throws FPRuntimeException, FPBusinessException {

        ArrayList MfrList = new ArrayList();
        MfrList = operationDAO.getMfrList();
        if (MfrList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return MfrList;
    }

    public ArrayList getExistingTechnicians(String jobCardId, String probId) throws FPRuntimeException, FPBusinessException {

        ArrayList existingTechnicians = new ArrayList();
        existingTechnicians = operationDAO.getExistingTechnicians(jobCardId, probId);
        return existingTechnicians;
    }

    public ArrayList getJobCardTechnicians(String jobCardId) throws FPRuntimeException, FPBusinessException {

        ArrayList existingTechnicians = new ArrayList();
        existingTechnicians = operationDAO.getJobCardTechnicians(jobCardId);
        return existingTechnicians;
    }

    public int saveJobCardTechnicians(String jobCardId, String probId,
            String[] technicianId, String[] estimatedHrs,
            String[] actualHrs, int userId) {
        int status = 0;
        for (int i = 0; i < technicianId.length; i++) {
            if (!"0".equals(technicianId[i])) {
                status = operationDAO.saveJobCardTechnicians(jobCardId, probId,
                        technicianId[i], estimatedHrs[i], actualHrs[i], userId);
            }
        }
        return status;
    }

    public ArrayList getServiceTypes() throws FPBusinessException, FPRuntimeException {
        ArrayList serviceTypes = new ArrayList();
        serviceTypes = operationDAO.getServiceTypes();
        if (serviceTypes.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return serviceTypes;
    }

    public ArrayList getServiceTypesForWo() throws FPBusinessException, FPRuntimeException {
        ArrayList serviceTypes = new ArrayList();
        serviceTypes = operationDAO.getServiceTypesForWo();
        if (serviceTypes.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return serviceTypes;
    }
//    bala

    public ArrayList getCustomerTypes() throws FPBusinessException, FPRuntimeException {
        ArrayList customerTypes = new ArrayList();
        customerTypes = operationDAO.getCustomerTypes();
        return customerTypes;
    }
//bala ends

    public ArrayList getServicePoints() throws FPBusinessException, FPRuntimeException {
        ArrayList servicePoints = new ArrayList();
        servicePoints = operationDAO.getServicePoints();
        if (servicePoints.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return servicePoints;
    }

    public ArrayList getServiceVendors() throws FPBusinessException, FPRuntimeException {
        ArrayList serviceVendors = new ArrayList();
        serviceVendors = operationDAO.getServiceVendors();
        return serviceVendors;
    }

    public int insertWorkOrder(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        String temp = operationTO.getReqDate();
        String[] date = temp.split("-");
        int hour = 12;
        if ((operationTO.getSec()).equals("1")) {
            System.out.println("hour" + operationTO.getHour());
            hour = hour + Integer.parseInt(operationTO.getHour());
            operationTO.setHour(String.valueOf(hour));
        }
        String reqdate = date[2] + "-" + date[1] + "-" + date[0] + " " + operationTO.getHour() + ":" + operationTO.getMinute() + ":00";
        operationTO.setReqDate(reqdate);
        temp = operationTO.getDateOfIssue();
        date = temp.split("-");
        String dateofIssue = date[2] + "-" + date[1] + "-" + date[0];
        System.out.println("regno New= " + operationTO.getRegno());
        operationTO.setDateOfIssue(dateofIssue);
        status = operationDAO.insertWorkOrder(operationTO);
        return status;
    }

    public int insertWorkOrderProblems(int secId, int probId, String symptoms, int Severity, int workorderId) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = operationDAO.insertWorkOrderProblems(secId, probId, symptoms, Severity, workorderId);
        return status;
    }

    public String getVehicleNos(String regNo, int compId, int compTypeId) {
        String regNos = "";
        regNo = regNo + "%";
        regNos = operationDAO.getVehicleNos(regNo, compId, compTypeId);
        return regNos;
    }

    public ArrayList scheduleWODetails(int compId) throws FPBusinessException, FPRuntimeException {
        ArrayList scheduleWODetails = new ArrayList();
        scheduleWODetails = operationDAO.scheduleWODetails(compId);
//        if (scheduleWODetails.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
//        }
        return scheduleWODetails;
    }

    public ArrayList getVehicleDetails(int vehicleId, int jobcardId) throws FPBusinessException, FPRuntimeException {
        ArrayList scheduleWODetails = new ArrayList();
        scheduleWODetails = operationDAO.getVehicleDetails(vehicleId, jobcardId);
        if (scheduleWODetails.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return scheduleWODetails;
    }

    public ArrayList getWorkOrderDetails(int workOrderId) throws FPBusinessException, FPRuntimeException {
        ArrayList woDetails = new ArrayList();
        woDetails = operationDAO.getWorkOrderDetails(workOrderId);

        if (woDetails.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        } else {
            OperationTO operationTO = null;
            String reqDate = "";
            Iterator itr = woDetails.iterator();
            if (itr.hasNext()) {
                operationTO = new OperationTO();
                operationTO = (OperationTO) itr.next();
                reqDate = operationTO.getReqDate();

                String[] time = reqDate.split(" ");

                String date = time[0];
                String[] redate = date.split("-");
                String temp = time[1];
                time = null;
                time = temp.split(":");
                String tempDate = "";
                int hour = Integer.parseInt(time[0]);
                if (hour > 12) {
                    hour = hour - 12;
                    tempDate = redate[2] + "-" + redate[1] + "-" + redate[0] + " " + String.valueOf(hour) + ":" + time[1] + "PM";

                    operationTO.setReqDate(tempDate);
                } else {

                    tempDate = redate[2] + "-" + redate[1] + "-" + redate[0] + " " + time[0] + ":" + time[1] + "AM";
                    System.out.println("d& t" + tempDate);
                    operationTO.setReqDate(tempDate);
                }

            }
        }

        return woDetails;
    }

    public ArrayList getProblemDetails(int workOrderId, int jobcardId) throws FPBusinessException, FPRuntimeException {
        ArrayList scheduleWODetails = new ArrayList();
        scheduleWODetails = operationDAO.getProblemDetails(workOrderId, jobcardId);
        //

        if (scheduleWODetails.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return scheduleWODetails;
    }

    public ArrayList getProblemMaster() throws FPBusinessException, FPRuntimeException {
        ArrayList problemMaster = new ArrayList();
        problemMaster = operationDAO.getProblemMaster();
        //

        if (problemMaster.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return problemMaster;
    }

    public ArrayList getWoProblemDetails(int workOrderId, int jobcardId) throws FPBusinessException, FPRuntimeException {
        ArrayList scheduleWODetails = new ArrayList();
        scheduleWODetails = operationDAO.getWoProblemDetails(workOrderId, jobcardId);
        //

        if (scheduleWODetails.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return scheduleWODetails;
    }

    public ArrayList scheduledWODetails(String date) throws FPBusinessException, FPRuntimeException {
        ArrayList scheduleWODetails = new ArrayList();
        String[] temp = date.split("-");
        String dateofIssue = temp[2] + "-" + temp[1] + "-" + temp[0];

        scheduleWODetails = operationDAO.scheduledWODetails(dateofIssue);
        if (scheduleWODetails.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        }
        return scheduleWODetails;

    }

    public ArrayList vehicleEligibleForJobCrad() throws FPBusinessException, FPRuntimeException {
        ArrayList vehicleEligibleForJobCrad = new ArrayList();

        vehicleEligibleForJobCrad = operationDAO.vehicleEligibleForJobCrad();

        return vehicleEligibleForJobCrad;

    }

    public int insertWorkOrderSchdule(int workOrderId, String scheduleDate, int userId, String deliDate, String serviceVendor) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        String[] temp = scheduleDate.split("-");
        String dateofIssue = temp[2] + "-" + temp[1] + "-" + temp[0];
        temp = deliDate.split("-");
        String date1 = temp[2] + "-" + temp[1] + "-" + temp[0];
        status = operationDAO.insertWorkOrderSchdule(workOrderId, dateofIssue, userId, date1, serviceVendor);
        return status = 0;
    }

    public int getWorkOrderOperationPointId(int workOrderId) throws FPBusinessException, FPRuntimeException {
        int compId = 0;
        compId = operationDAO.getWorkOrderOperationPointId(workOrderId);
        return compId;
    }

    public ArrayList searchWorkOrderStatus(String fromDate, String toDate, String regNo, int compId, String status, String workorderId, int roleId) throws FPBusinessException, FPRuntimeException {
        ArrayList scheduleWODetails = new ArrayList();

        String[] temp = null;
        if (fromDate != "" && toDate != "") {
            temp = fromDate.split("-");
            String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
            fromDate = sDate;
            temp = toDate.split("-");

            String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
            toDate = eDate;
        }

        scheduleWODetails = operationDAO.searchWorkOrderStatus(fromDate, toDate, regNo, compId, status, workorderId, roleId);
        if (scheduleWODetails.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return scheduleWODetails;

    }

    public ArrayList searchJobCardStatus(String jcno, String regno, String status, int operId, String fromDate, String toDate, int compId, int startIndex, int endIndex, String custType) throws FPBusinessException, FPRuntimeException {
        ArrayList scheduleWODetails = new ArrayList();
        String[] temp1 = null;
        if (fromDate != "" && toDate != "") {
            temp1 = fromDate.split("-");
            String sDate = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
            fromDate = sDate;
            temp1 = toDate.split("-");
            String eDate = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
            toDate = eDate;
        }
        scheduleWODetails = operationDAO.searchJobCardStatus(jcno, regno, status, operId, fromDate, toDate, compId, startIndex, endIndex, custType);

        if (scheduleWODetails.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        } else {
            OperationTO operationTO = null;
            String reqDate = "";
            Iterator itr = scheduleWODetails.iterator();
            while (itr.hasNext()) {
                operationTO = new OperationTO();
                operationTO = (OperationTO) itr.next();
                reqDate = operationTO.getReqDate();

                if (!reqDate.equalsIgnoreCase("Not Scheduled")) {
                    String[] time = reqDate.split(" ");
                    String date = time[0];
                    String[] redate = date.split("-");
                    String temp = time[1];
                    String pcd = "";
                    time = null;
                    time = temp.split(":");
                    String tempDate = "";
                    int hour = Integer.parseInt(time[0]);
                    if (hour > 12) {
                        hour = hour - 12;
                        pcd = redate[2] + "-" + redate[1] + "-" + redate[0];
                        tempDate = redate[2] + "-" + redate[1] + "-" + redate[0] + " " + String.valueOf(hour) + ":" + time[1] + "PM";

                        operationTO.setReqDate(tempDate);
                        operationTO.setPcd(pcd);
                    } else {
                        pcd = redate[2] + "-" + redate[1] + "-" + redate[0];
                        tempDate = redate[2] + "-" + redate[1] + "-" + redate[0] + " " + time[0] + ":" + time[1] + "AM";
                        System.out.println("d& t" + tempDate);
                        operationTO.setReqDate(tempDate);
                        operationTO.setPcd(pcd);
                    }

                }

            }
        }
        return scheduleWODetails;

    }

    public ArrayList getJobcardList(int vehicleId, int jobcardid) {
        ArrayList scheduleWODetails = new ArrayList();
        scheduleWODetails = operationDAO.getJobcardList(vehicleId, jobcardid);
        if (scheduleWODetails.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return scheduleWODetails;
    }

    public ArrayList getProblemList(int vehicleId, int workOrderId) {
        ArrayList scheduleWODetails = new ArrayList();
        scheduleWODetails = operationDAO.getProblemList(vehicleId, workOrderId);
        if (scheduleWODetails.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return scheduleWODetails;
    }

    public ArrayList getServiceList(int vehicleId, int km, int jobcardId) {
        ArrayList serviceList = new ArrayList();
        serviceList = operationDAO.getServiceList(vehicleId, km, jobcardId);
        if (serviceList.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return serviceList;
    }

    public int insertJobCardScheduleDetails(String[] sec, int userId, int jobcardId, String[] probId, String[] technicianId, String[] scheduledDate, String[] status, String[] cremarks, String[] cause, String[] remark, String[] index, String[] symptoms, String[] severity, String[] validate) {

        System.out.println("technicianId l=" + technicianId.length);
        System.out.println("index l=" + index.length);
        int pId = 0;
        int techId = 0;
        String[] temp1 = null;
        String stat = "";
        String remarks = "";
        int insertStatus = 0;
        String cau = "";
        String rema = "";
        String[] prob = null;
        String sym = "";
        int sev = 0;
        int value = 0;
        int temp = 1;
        int secId = 0;
        int vali = 0;
        if (status != null) {

            for (int i = 0; i < index.length; i++) {
                value = Integer.parseInt(index[i]);
                stat = status[value];
                remarks = cremarks[value];
                vali = Integer.parseInt(validate[value]);
                System.out.println("vali" + vali);
                if (vali == 0) {
                    System.out.println("vali" + vali);
                    prob = probId[value].split("-");
                    pId = Integer.parseInt(prob[0]);
                    System.out.println("vali" + vali);
                } else {
                    pId = Integer.parseInt(probId[value]);
                }

                String pcDate = "";

                if (vali == 0) {
                    sev = Integer.parseInt(severity[value]);
                    sym = symptoms[value];
                    insertStatus = operationDAO.insertJobCardProblems(secId, pId, sym, sev, jobcardId, temp);
                }
                secId = Integer.parseInt(sec[value]);
                if (scheduledDate[value] != "" && scheduledDate[value] != null) {
                    System.out.println("date=" + scheduledDate[value]);
                    if (secId != 1016) {
                        System.out.println("techId=" + technicianId[value]);
                        techId = Integer.parseInt(technicianId[value]);
                    } else {
                        techId = 1;
                    }
                    temp1 = scheduledDate[value].split("-");
                    pcDate = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
                    System.out.println("stat" + stat);
                    if (stat.equals("P")) {
                        //insert                                          
                        System.out.println("in insert");
                        System.out.println("pId" + pId);
                        System.out.println("remarks:" + remarks);

                        insertStatus = operationDAO.insertJobCardScheduleDetails(jobcardId, pId, pcDate, techId, stat, remarks, userId);

                    } else if (stat.equals("C")) {
                        //update                        
                        insertStatus = operationDAO.updateJobCardScheduleDetails(pcDate, jobcardId, pId, techId, stat, remarks, cau, rema);

                    }
                }

            }

        }
        return insertStatus;

    }

    public int insertJobCardProblems(int secId, int probIds, String symptoms, int Severity, int jobcardId, int temp) {
        int status = 0;
        status = operationDAO.insertJobCardProblems(secId, probIds, symptoms, Severity, jobcardId, temp);
        return status;
    }

    public ArrayList jobCardProblemDetails(int jobcardId) {
        ArrayList scheduleWODetails = new ArrayList();
        scheduleWODetails = operationDAO.jobCardProblemDetails(jobcardId);
        if (scheduleWODetails.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return scheduleWODetails;
    }

    public ArrayList processServiceDetails(int jobcardId) {
        ArrayList Details = new ArrayList();
        Details = operationDAO.getServiceDetails(jobcardId);
        return Details;
    }

    public String getJobCardOwner(int jobcardId) {
        String jobCardOwner = operationDAO.getJobCardOwner(jobcardId);
        return jobCardOwner;
    }

    public ArrayList processActivity(int probId, int jobcardId) {
        ArrayList Details = new ArrayList();
        Details = operationDAO.getActivity(probId, jobcardId);
        return Details;
    }

    public int getTotalJobcards(String jcno, String regno, String status, int operId, String fromDate, String toDate, int compId, String custType) {
        int totalRecords = 0;
        String[] temp1 = null;
        if (fromDate != "" && toDate != "") {
            temp1 = fromDate.split("-");
            String sDate = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
            fromDate = sDate;
            temp1 = toDate.split("-");
            String eDate = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
            toDate = eDate;
        }

        totalRecords = operationDAO.getTotalJobcards(jcno, regno, status, operId, fromDate, toDate, compId, custType);
        return totalRecords;

    }

    public ArrayList getVendorDetails() {
        ArrayList scheduleWODetails = new ArrayList();
        scheduleWODetails = operationDAO.getVendorDetails();
        if (scheduleWODetails.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return scheduleWODetails;
    }

    public int insertBodyPartsWO(int jobcardId, String remarks, int userId, String[] probId, String[] index, String[] vendorId) {
        int value = 0;
        int pId = 0;
        int workOrderId = 0;
        int vendor = 0;
        int temp = 0;
        int status = 0;
        for (int i = 0; i < index.length; i++) {
            value = Integer.parseInt(index[i]);
            pId = Integer.parseInt(probId[value]);
            vendor = Integer.parseInt(vendorId[value]);
            if (vendor != temp) {
                workOrderId = operationDAO.insertBodyPartsWO(jobcardId, remarks, userId, vendor);
                System.out.println("workOrderId" + workOrderId);
                temp = vendor;
            }
            if (workOrderId != 0) {
                System.out.println("workOrderId" + workOrderId);
                status = operationDAO.insertWOProblems(workOrderId, pId);
            }

        }
        return status;
    }

    public int insertExternalTechnician(int jobcardId, int probId, String techName, String compName, String contactNo, String labourCharge) {
        int status = 0;
        status = operationDAO.insertExternalTechnician(jobcardId, probId, techName, compName, contactNo, labourCharge);
        return status;
    }

    public int getVehicleId(String regNo) {
        int status = 0;
        status = operationDAO.getVehicleId(regNo);
        return status;
    }

    public String checkVehicleRegNo(String regNo) {
        String mess = "";
        mess = operationDAO.checkVehicleRegNo(regNo);
        return mess;
    }

    public String getVehicleMMU(int vehicleId) throws FPBusinessException {
        String vehicleDetails = "";
        OperationTO operationTO = null;
        ArrayList tempList = new ArrayList();
        tempList = getVehicleDetails(vehicleId, 0);
        if (tempList.size() != 0) {
            Iterator itr = tempList.iterator();
            if (itr.hasNext()) {
                operationTO = new OperationTO();
                operationTO = (OperationTO) itr.next();
                vehicleDetails = operationTO.getMfrName() + "-" + operationTO.getModelName() + "-" + operationTO.getUsageName() + "-" + vehicleId;
            }
        }
        return vehicleDetails;
    }

    public int insertOtherJobCardWorkOrder(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        String temp = operationTO.getReqDate();
        String[] date = temp.split("-");
        int hour = 12;
        if ((operationTO.getSec()).equals("1")) {
            System.out.println("hour" + operationTO.getHour());
            hour = hour + Integer.parseInt(operationTO.getHour());
            operationTO.setHour(String.valueOf(hour));
        }
        String reqdate = date[2] + "-" + date[1] + "-" + date[0] + " " + operationTO.getHour() + ":" + operationTO.getMinute() + ":00";
        operationTO.setReqDate(reqdate);
        temp = operationTO.getDateOfIssue();
        date = temp.split("-");
        String dateofIssue = date[2] + "-" + date[1] + "-" + date[0];
        operationTO.setDateOfIssue(dateofIssue);
        status = operationDAO.insertOtherJobCardWorkOrder(operationTO);
        return status;

    }

    public int insertJobCardWorkOrder(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        String temp = operationTO.getReqDate();
        String[] date = temp.split("-");
        int hour = 12;
        if ((operationTO.getSec()).equals("1")) {
            System.out.println("hour" + operationTO.getHour());
            hour = hour + Integer.parseInt(operationTO.getHour());
            operationTO.setHour(String.valueOf(hour));
        }
        String reqdate = date[2] + "-" + date[1] + "-" + date[0] + " " + operationTO.getHour() + ":" + operationTO.getMinute() + ":00";
        operationTO.setReqDate(reqdate);
        temp = operationTO.getDateOfIssue();
        date = temp.split("-");
        String dateofIssue = date[2] + "-" + date[1] + "-" + date[0];
        operationTO.setDateOfIssue(dateofIssue);
        status = operationDAO.insertJobCardWorkOrder(operationTO);
        return status;

    }

    public int insertJobCardServices(int currentkm, int currenthm, int userId, int jobcardId, String[] serviceindex, String[] serviceId, String[] servicedDate, String[] serviceTech, String[] serviceStatus, int vehicleId) {

        int temp = 0;
        String stat = "";
        int insertStatus = 0;
        int sId = 0;
        int techId = 0;
        String[] date = null;
        String dateofIssue = "";
        System.out.println("serviceStatus.len=" + serviceStatus.length);
        System.out.println("serviceindex.len=" + serviceindex.length);
        for (int i = 0; i < serviceindex.length; i++) {
            temp = Integer.parseInt(serviceindex[i]);
            System.out.println("inde value=" + temp);
            sId = Integer.parseInt(serviceId[temp]);
            stat = serviceStatus[temp];
            if (stat.equals("P") || stat.equals("C")) {
                techId = Integer.parseInt(serviceTech[temp]);
                date = servicedDate[temp].split("-");
                dateofIssue = date[2] + "-" + date[1] + "-" + date[0];
                insertStatus = operationDAO.insertJobCardServiceDetails(jobcardId, sId, dateofIssue, techId, stat, userId);
                if (stat.equals("C")) {
                    insertStatus = operationDAO.insertJobCardServices(userId, jobcardId, sId, currentkm, currenthm, vehicleId);
                }
            }
        }
        return insertStatus;

    }

    public ArrayList getScheduledServices(int vehicleId, int jobcardId) {
        ArrayList scheduledServices = new ArrayList();
        scheduledServices = operationDAO.getScheduledServices(vehicleId, jobcardId);

        return scheduledServices;
    }

    public ArrayList getBodyWorksList(int jobcardId) {
        ArrayList scheduledServices = new ArrayList();
        scheduledServices = operationDAO.getBodyWorksList(jobcardId);
        return scheduledServices;
    }

    public int getTotalFSList(int compId, String temp) throws FPBusinessException, FPRuntimeException {
        int totalRecords = 0;
        String[] date = temp.split("-");
        String dateofIssue = date[2] + "-" + date[1] + "-" + date[0];
        totalRecords = operationDAO.getTotalFSList(dateofIssue, compId);
        if (totalRecords == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return totalRecords;

    }

    public ArrayList getFSList(String temp, int compId, int startIndex, int endIndex) {
        ArrayList getFSList = new ArrayList();
        String[] date = temp.split("-");
        String dateofIssue = date[2] + "-" + date[1] + "-" + date[0];
        getFSList = operationDAO.getFSList(dateofIssue, compId, startIndex, endIndex);
        if (getFSList.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return getFSList;
    }

    public int insertFreeSevicedVehicles(String[] vehicles, String[] serviceId, int userId, String[] index, String[] kms, String[] hms, String[] servicedDate) throws FPBusinessException, FPRuntimeException {
        int temp = 0;
        String stat = "";
        int insertStatus = 0;
        int km = 0;
        int hm = 0;
        int vehicleId = 0;
        int freeServiceId = 0;

        String[] date = null;
        String dateofIssue = "";
        for (int i = 0; i < index.length; i++) {
            temp = Integer.parseInt(index[i]);
            freeServiceId = Integer.parseInt(serviceId[temp]);
            vehicleId = Integer.parseInt(vehicles[temp]);
            km = Integer.parseInt(kms[temp]);
            hm = Integer.parseInt(hms[temp]);
            date = servicedDate[temp].split("-");
            dateofIssue = date[2] + "-" + date[1] + "-" + date[0];
            insertStatus = operationDAO.insertFreeSevicedVehicles(vehicleId, freeServiceId, dateofIssue, km, hm, userId);

        }
        return insertStatus;
    }

    public ArrayList periodicServiceList(int compId, String temp) {
        ArrayList periodicServiceList = new ArrayList();
        ArrayList statusList = new ArrayList();
        int vehicleId = 0;
        String[] date = temp.split("-");
        String dateofIssue = date[2] + "-" + date[1] + "-" + date[0];
        String psDate = "";
        periodicServiceList = operationDAO.periodicServiceList(compId, dateofIssue);
        OperationTO operationTO = null;
        Iterator itr = periodicServiceList.iterator();
        while (itr.hasNext()) {
            operationTO = new OperationTO();
            operationTO = (OperationTO) itr.next();
            if (operationDAO.periodicServiceStatus(operationTO.getVehicleId(), operationTO.getServicedDate()) > 0) {
                operationTO.setStatus("1");
            } else {
                operationTO.setStatus("0");
            }

        }

        if (periodicServiceList.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return periodicServiceList;
    }

    public ArrayList getDirectJCList(int compId) throws FPBusinessException, FPRuntimeException {
        ArrayList tempList = new ArrayList();
        tempList = operationDAO.getDirectJCList(compId);
        System.out.println("tempList.size()" + tempList.size());
        if (tempList.size() == 0) {

            throw new FPBusinessException("EM-GEN-01");

        } else {
            OperationTO operationTO = null;
            String reqDate = "";
            Iterator itr = tempList.iterator();
            if (itr.hasNext()) {
                operationTO = new OperationTO();
                operationTO = (OperationTO) itr.next();
                reqDate = operationTO.getReqDate();

                String[] time = reqDate.split(" ");

                String date = time[0];
                String[] redate = date.split("-");
                String temp = time[1];
                time = null;
                time = temp.split(":");
                String tempDate = "";
                int hour = Integer.parseInt(time[0]);
                if (hour > 12) {
                    hour = hour - 12;
                    tempDate = redate[2] + "-" + redate[1] + "-" + redate[0] + " " + String.valueOf(hour) + ":" + time[1] + "PM";

                    operationTO.setReqDate(tempDate);
                } else {

                    tempDate = redate[2] + "-" + redate[1] + "-" + redate[0] + " " + time[0] + ":" + time[1] + "AM";
                    System.out.println("d& t" + tempDate);
                    operationTO.setReqDate(tempDate);
                }

            }
        }

        return tempList;
    }

    public int insertDirectJCStatus(int status, int jobCardId, int userId) {

        int temp = 0;
        temp = operationDAO.insertDirectJCStatus(status, jobCardId, userId);
        return temp;
    }

    public ArrayList getJobCardList() throws FPBusinessException, FPRuntimeException {
        ArrayList getJobCardList = new ArrayList();
        getJobCardList = operationDAO.getJobCardList();
        if (getJobCardList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return getJobCardList;
    }

    public ArrayList getBodyBillList(int jobcardId) {
        ArrayList getJobCardList = new ArrayList();
        getJobCardList = operationDAO.getBodyBillList(jobcardId);
        if (getJobCardList.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return getJobCardList;
    }

    public int saveBodyWorksBill(String woId, String invoiceNo,
            String spareAmount, String vatAmount, String serviceAmount,
            String serviceTaxAmount, String remarks,
            String totalAmount, int userId) {

        int status = 0;

        status = operationDAO.saveBodyWorksBill(woId, spareAmount, vatAmount, serviceAmount,
                serviceTaxAmount, remarks, invoiceNo, totalAmount, userId);

        return status;
    }

    public int insertServicedDetails(int userId, int jobcardId, int serviceId, int km, int hms, int vehicleId, String servicedDate) {
        int status = 0;

        String[] date = servicedDate.split("-");

        String dateofIssue = date[2] + "-" + date[1] + "-" + date[0];
        status = operationDAO.insertServicedDetails(userId, jobcardId, serviceId, km, hms, vehicleId, dateofIssue);

        return status;

    }

    public int updateCompletionDate(int jobcardId, String CompletionDate, String jremarks) {
        int status = 0;

        String[] date = CompletionDate.split("-");

        String dateofIssue = date[2] + "-" + date[1] + "-" + date[0];

        System.out.println("processed Final jremarks:" + jremarks);
        status = operationDAO.updateCompletionDate(jobcardId, dateofIssue, jremarks);

        return status;
    }

    public String getcomDate(int jobcardId) {
        String status = "";

        status = operationDAO.getcompDate(jobcardId);

        return status;
    }

    public ArrayList processJcVehDetail(int jobcardId) {
        ArrayList list = new ArrayList();

        list = operationDAO.getJcVehDetail(jobcardId);

        return list;
    }

    public int isThisProblemExist(int jobcardId, int probId) {
        int status = 0;
        status = operationDAO.isThisProblemExist(jobcardId, probId);
        return status;

    }

    public float jobCardMrsDetails(int jobcardId) {
        ArrayList jobCardMrsDetails = new ArrayList();
        OperationTO operationTO = null;
        jobCardMrsDetails = operationDAO.jobCardMrsDetails(jobcardId);

        int totReqQty = 0;
        int totIssQty = 0;
        float serviedRate = 0;

        if (jobCardMrsDetails.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        } else {

            Iterator itr = jobCardMrsDetails.iterator();
            while (itr.hasNext()) {
                operationTO = new OperationTO();
                operationTO = (OperationTO) itr.next();
                totReqQty += operationTO.getReqQty();
                totIssQty += operationTO.getIssQty();
            }
            System.out.println("totReqQty" + totReqQty);
            System.out.println("totIssQty" + totIssQty);
            serviedRate = (float) totIssQty / totReqQty;
            System.out.println("serviedRate" + serviedRate);

        }
        return (serviedRate * 100);
    }

    public ArrayList handleNonGracePeriodServices(int vehicleId, ArrayList serviceList) throws FPBusinessException, FPRuntimeException {
        ArrayList NonGraceServices = new ArrayList();
        ArrayList newServiceList = new ArrayList();
        OperationTO serviceTO = new OperationTO();
        OperationTO nonGraceServTO = new OperationTO();
        Iterator serviceItr, nonGraceServiceItr;
        int count = 0;

        NonGraceServices = operationDAO.getNonGracePeriodServices(vehicleId);
        /*
         nonGraceServiceItr = NonGraceServices.iterator();
        
         while (nonGraceServiceItr.hasNext()) {
         nonGraceServTO = new OperationTO();
         nonGraceServTO = (OperationTO)nonGraceServiceItr.next();
         serviceItr = serviceList.iterator();
         while(serviceItr.hasNext()){
         serviceTO = new OperationTO();
         serviceTO = (OperationTO)serviceItr.next();
         if(serviceTO.getServiceId()== nonGraceServTO.getServiceId()){
         count++;
         }
         System.out.print("included service="+serviceTO.getServiceId()+"-");
         System.out.println("nonGraceServTO service="+nonGraceServTO.getServiceId());
         }
         if(count==0){
         newServiceList.add(nonGraceServTO);
         }
         count=0;
         }*/
        System.out.println("new ArrayList=" + newServiceList.size());
        return NonGraceServices;
    }

    public ArrayList getNonGraceSchedServices(ArrayList serviceList, int vehicleId, int jobCardId, int km, int hm) {
        ArrayList nonGraceScheduled = new ArrayList();
        ArrayList newServiceList = new ArrayList();
        OperationTO serviceTO = new OperationTO();
        OperationTO nonGraceServTO = new OperationTO();
        Iterator serviceItr, nonGraceServiceItr;
        int count = 0;

        nonGraceScheduled = operationDAO.getNonGraceSchedServices(vehicleId, jobCardId, km, hm);
        nonGraceServiceItr = nonGraceScheduled.iterator();

        while (nonGraceServiceItr.hasNext()) {
            nonGraceServTO = new OperationTO();
            nonGraceServTO = (OperationTO) nonGraceServiceItr.next();
            serviceItr = serviceList.iterator();
            while (serviceItr.hasNext()) {
                serviceTO = new OperationTO();
                serviceTO = (OperationTO) serviceItr.next();
                if (serviceTO.getServiceId() == nonGraceServTO.getServiceId()) {
                    count++;
                }
                System.out.print("included service=" + serviceTO.getServiceId() + "-");
                System.out.println("nonGraceServTO service=" + nonGraceServTO.getServiceId());
            }
            if (count == 0) {
                newServiceList.add(nonGraceServTO);
            }
            count = 0;
        }

        if (serviceList.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        System.out.println("newServiceLisBP=" + newServiceList.size());
        return newServiceList;
    }

    public String getDueHmKm(OperationTO operationTO) {
        String mess = "";
        mess = operationDAO.getDueHmKm(operationTO);
        return mess;
    }

    public String getActualKm(String vehicleNo) {
        String mess = "";
        mess = operationDAO.getActualKm(vehicleNo);
        return mess;
    }

    public ArrayList processWoDetail(String woId) {

        ArrayList poDetail = new ArrayList();
        ArrayList newPoDetail = new ArrayList();
        OperationTO purch = new OperationTO();
        Iterator itr;
        poDetail = operationDAO.getWoDetail(woId);
        itr = poDetail.iterator();
        while (itr.hasNext()) {
            purch = new OperationTO();
            purch = (OperationTO) itr.next();
            purch.setAddress(addressSplit(purch.getVendorAddress()));
            newPoDetail.add(purch);
        }
        return newPoDetail;
    }

    public String[] addressSplit(String longText) {
        String[] splitText = null;
        int lower = 0, higher;
        int splitSize = poTextSplitSize;
        int size = longText.length();
        splitText = new String[(size / splitSize) + 1];
        for (int i = 0; i < (size / splitSize) + 1; i++) {
            if ((size - lower) < splitSize) {
                higher = (size);
            } else {
                higher = lower + splitSize;
            }
            splitText[i] = longText.substring(lower, higher);
            System.out.println(splitText[i]);
            lower = lower + splitSize;
        }
        return splitText;
    }

    public String[] longTextSplit(String longText, int spltSize) {
        String[] splitText = null;
        int lower = 0, higher;
        int splitSize = spltSize;
        int size = longText.length();
        splitText = new String[(size / splitSize) + 1];
        for (int i = 0; i < (size / splitSize) + 1; i++) {
            if ((size - lower) < splitSize) {
                higher = (size);
            } else {
                higher = lower + splitSize;
            }
            splitText[i] = longText.substring(lower, higher);
            System.out.println(splitText[i]);
            lower = lower + splitSize;
        }
        return splitText;
    }

    public ArrayList sortPrintJobcardData(ArrayList problemList) {
        Iterator itr = problemList.iterator();
        ArrayList newProblemList = new ArrayList();
        String[] problemName, symptoms;
        OperationTO oper;
        while (itr.hasNext()) {
            oper = new OperationTO();
            oper = (OperationTO) itr.next();
            problemName = longTextSplit(oper.getProbName(), 28);
            symptoms = longTextSplit(oper.getSymptoms(), 28);
            oper.setProblemNameSplit(problemName);
            oper.setSymptomsSplit(symptoms);
            newProblemList.add(oper);
        }
        return newProblemList;
    }

    public ArrayList getContractJobcards() {
        ArrayList jcList = new ArrayList();
        jcList = operationDAO.getContractJobcards();
        return jcList;
    }

    public String getContractWos(int jobCardId) {
        ArrayList woList = new ArrayList();
        OperationTO woNos;
        Iterator itr;
        String sugestions = "";
        woList = operationDAO.getContractWos(jobCardId);
        itr = woList.iterator();
        while (itr.hasNext()) {
            woNos = new OperationTO();
            woNos = (OperationTO) itr.next();
            if (sugestions.equals("")) {
                sugestions = "" + woNos.getWoId() + "-" + woNos.getWoId();
            } else {
                sugestions = sugestions + "~" + woNos.getWoId() + "-" + woNos.getWoId();
            }
        }
        return sugestions;
    }

//---------Trip Details Starts
    public int saveTripDetails(String routeId, String vehicleId, String tripCode, String tripDate, String departureDate, String arrivalDate,
            String driverNameId, String kmsOut, String kmsIn, String totalallowance, String totalliters, String totalfuelamount, String totalexpenses, String balanceamount, String totalkms, String status,
            String tonnage, String companyId, String tripRevenue, String cleanerStatus, String tripType,
            String pinkSlip, String orderNo, String bags, String gpsKm, String billStatus, String vehicleType, String productId, String gpno, String invoiceNo,
            int userId, String customertype, String twoLpsStatus, String tripRevenueOne, String tripRevenueTwo, String companyName, String selectedBillStatus, String extraKm) {
        int lastInsertId = 0;
        lastInsertId = operationDAO.saveTripDetails(routeId, vehicleId, tripCode, tripDate, departureDate, arrivalDate,
                driverNameId, kmsOut, kmsIn, totalallowance, totalliters, totalfuelamount, totalexpenses, balanceamount, totalkms, status,
                tonnage, companyId, tripRevenue, cleanerStatus, tripType,
                pinkSlip, orderNo, bags, gpsKm, billStatus, vehicleType, productId, gpno, invoiceNo, userId,
                customertype, twoLpsStatus, tripRevenueOne, tripRevenueTwo, companyName, selectedBillStatus, extraKm);
        return lastInsertId;
    }
//  public int updateTripDetails(String tripSheetId,String routeId, String vehicleId, String tripCode, String tripDate, String departureDate, String arrivalDate,
//                         String driverNameId, String kmsOut, String  kmsIn, String totalallowance, String totalliters, String totalfuelamount, String totalexpenses, String balanceamount, String totalkms,String status,
//                         String loadedTonnage, String deliveredTonnage, String cleanerStatus, String tripType, String fromLocation, String toLocation, String productName, String returnAmount, String returnExpenses,
//                         String tripReceivedAmount, String returnLoadedTonnage, String returnLoadedDate, String returnDeliveredTonnage, String returnDeliveredDate, String shortageTonnage,
//                         String totalExpensesReturn, String balanceAmountRetuen, int userId) {
//        int lastInsertId = 0;
//        lastInsertId = operationDAO.updateTripDetails(tripSheetId,routeId, vehicleId,tripCode,tripDate,departureDate,arrivalDate,
//                        driverNameId,kmsOut, kmsIn,totalallowance,totalliters,totalfuelamount,totalexpenses,balanceamount,totalkms,status,loadedTonnage,
//                        deliveredTonnage,cleanerStatus,tripType, fromLocation, toLocation, productName, returnAmount, returnExpenses,
//                        tripReceivedAmount, returnLoadedTonnage, returnLoadedDate, returnDeliveredTonnage, returnDeliveredDate, shortageTonnage,
//                        totalExpensesReturn, balanceAmountRetuen, userId);
//        return lastInsertId;
//    }

    public int updateTripDetails(String tripSheetId, String routeId, String vehicleId, String tripCode, String tripDate, String departureDate, String arrivalDate,
            String driverNameId, String kmsOut, String kmsIn, String totalallowance, String totalliters, String totalfuelamount, String totalexpenses, String balanceamount, String totalkms, String status,
            String loadedTonnage, String deliveredTonnage, String cleanerStatus, String tripType,
            String totalExpensesReturn, String balanceAmountRetuen, int userId, String mileage, String settlementFlag) {
        int lastInsertId = 0;
        lastInsertId = operationDAO.updateTripDetails(tripSheetId, routeId, vehicleId, tripCode, tripDate, departureDate, arrivalDate,
                driverNameId, kmsOut, kmsIn, totalallowance, totalliters, totalfuelamount, totalexpenses, balanceamount, totalkms, status, loadedTonnage,
                deliveredTonnage, cleanerStatus, tripType,
                totalExpensesReturn, balanceAmountRetuen, userId, mileage, settlementFlag);
        return lastInsertId;
    }

    public int insertTripAllowence(int userId, String returnTripId, int tripId, String stageId,
            String tripAllowanceDate, String tripAllowanceAmount,
            String tripAllowancePaidBy, String tripAllowanceRemarks, String driverId, String driverType, String driverVendorId,
            String vehicleType, String vehicleVendorId) {
        int status = 0;
        status = operationDAO.insertTripAllowence(userId, returnTripId, tripId, stageId, tripAllowanceDate, tripAllowanceAmount,
                tripAllowancePaidBy, tripAllowanceRemarks, driverId, driverType, driverVendorId, vehicleType, vehicleVendorId);
        return status;
    }

    public int resetTripAllowence(String returnTripId, int tripId) {
        int status = 0;
        status = operationDAO.resetTripAllowence(returnTripId, tripId);
        return status;
    }

    public int resetTripFuel(String returnTripId, int tripId) {
        int status = 0;
        status = operationDAO.resetTripFuel(returnTripId, tripId);
        return status;
    }

    public int insertTripExpenses(int userId, String returnTripId, int tripId, String stageIdExp, String tripExpensesDate, String tripExpensesAmount,
            String tripExpensesPaidBy, String tripExpensesRemarks, String tripFuelLitre) {
        int status = 0;
        status = operationDAO.insertTripExpenses(userId, returnTripId, tripId, stageIdExp, tripExpensesDate, tripExpensesAmount,
                tripExpensesPaidBy, tripExpensesRemarks, tripFuelLitre);
        return status;
    }

    public int updateFixedTripExpenses(int userId, String tripSheetIdParam, String expExpId, String expExpAmt) {
        int status = 0;
        status = operationDAO.updateFixedTripExpenses(userId, tripSheetIdParam, expExpId, expExpAmt);
        return status;
    }

    public int reverseToPayRevenue(String tripSheetIdParam, String tripRevenue, String customerId, int userId) {
        int status = 0;
        status = operationDAO.reverseToPayRevenue(tripSheetIdParam, tripRevenue, customerId, userId);
        return status;
    }

    public int insertReturnTrip(int tripId, String fromLocation, String returnLoadedDate, String toLocation, String returnDeliveredDate,
            String returnProductName, String returnKM, String returnFuel, String returnTon,
            String returnExpenses, String returnAmount) {
        int status = 0;
        status = operationDAO.insertRreturnTrip(tripId, fromLocation, returnLoadedDate, toLocation, returnDeliveredDate,
                returnProductName, returnKM, returnFuel, returnTon,
                returnExpenses, returnAmount);
        return status;
    }

    public int insertTripFuel(int userId, String returnTripId, int tripId, String bunkName, String bunkPlace, String fuelDate, String fuelAmount,
            String fuelLtrs, String fuelRemarks, String driverId) {
        int status = 0;
        status = operationDAO.insertTripFuel(userId, returnTripId, tripId, bunkName, bunkPlace, fuelDate, fuelAmount, fuelLtrs, fuelRemarks, driverId);
        return status;
    }

    public String getTripIDPrint() {
        String TripID = "";
        TripID = operationDAO.getTripIDPrint();
        return TripID;
    }

    public ArrayList getTripDetails(String searchTripDate) {
        ArrayList tripDetails = new ArrayList();
        tripDetails = operationDAO.getTripDetails(searchTripDate);
        return tripDetails;
    }

    public ArrayList getTripDetailsEdit(String tripSheetId) {
        ArrayList tripDetails = new ArrayList();
        tripDetails = operationDAO.getTripDetailsEdit(tripSheetId);
        return tripDetails;
    }

    public ArrayList getTwoLpsStutas(String tripSheetId) {
        ArrayList twoLpsStutas = new ArrayList();
        twoLpsStutas = operationDAO.getTwoLpsStutas(tripSheetId);
        return twoLpsStutas;
    }

    public ArrayList gettripDetailsView(String tripSheetId) {
        ArrayList tripDetailsView = new ArrayList();
        tripDetailsView = operationDAO.gettripDetailsView(tripSheetId);
        return tripDetailsView;
    }

    public ArrayList getgpsList(String tripSheetId) {
        System.out.println("21");
        ArrayList gpsList = new ArrayList();
        System.out.println("22");
        gpsList = operationDAO.getgpsList(tripSheetId);
        System.out.println("23");
        System.out.println("24");
        return gpsList;
    }

    public ArrayList getAllowanceDetails(String tripSheetId) {
        ArrayList allowanceDetails = new ArrayList();
        allowanceDetails = operationDAO.getAllowanceDetails(tripSheetId, "0");
        return allowanceDetails;
    }

    public ArrayList getAllowanceDetails(String tripSheetId, String returnTripId) {
        ArrayList allowanceDetails = new ArrayList();
        allowanceDetails = operationDAO.getAllowanceDetails(tripSheetId, returnTripId);
        return allowanceDetails;
    }

    public ArrayList getFuelDetails(String tripSheetId, String returnTripId) {
        ArrayList allowanceDetails = new ArrayList();
        allowanceDetails = operationDAO.getFuelDetails(tripSheetId, returnTripId);
        return allowanceDetails;
    }

    public ArrayList getFuelDetails(String tripSheetId) {
        ArrayList allowanceDetails = new ArrayList();
        allowanceDetails = operationDAO.getFuelDetails(tripSheetId, "0");
        return allowanceDetails;
    }

    public ArrayList getEtmExpenses(String tripId) {
        ArrayList etmExpDetails = new ArrayList();
        etmExpDetails = operationDAO.getEtmExpenses(tripId, "0");
        return etmExpDetails;
    }

    public ArrayList getEtmExpenses(String tripId, String returnTripId) {
        ArrayList etmExpDetails = new ArrayList();
        etmExpDetails = operationDAO.getEtmExpenses(tripId, returnTripId);
        return etmExpDetails;
    }

    public ArrayList getPaymentVoucherDetails(String tripSheetId) {
        ArrayList paymentVoucherDetails = new ArrayList();
        paymentVoucherDetails = operationDAO.getPaymentVoucherDetails(tripSheetId);
        return paymentVoucherDetails;
    }

    //Rathimeena Code Start
    public ArrayList searchProDriverSettlement(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList proDriverSettlement = new ArrayList();
        String[] temp = null;
        /*if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        proDriverSettlement = operationDAO.searchProDriverSettlement(fromDate, toDate, regNo, driId);
        if (proDriverSettlement.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return proDriverSettlement;
    }

    public ArrayList cleanerTripDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList cleanerTrip = new ArrayList();
        String[] temp = null;
        /*if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        cleanerTrip = operationDAO.cleanerTripDetails(fromDate, toDate, regNo, driId);
        if (cleanerTrip.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return cleanerTrip;
    }

    public ArrayList getFixedExpDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList fixedExpDetails = new ArrayList();
        String[] temp = null;
        /*if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        fixedExpDetails = operationDAO.getFixedExpDetails(fromDate, toDate, regNo, driId);
        System.out.println("fixedExpDetails.size().. BP : " + fixedExpDetails.size());
        if (fixedExpDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return fixedExpDetails;
    }

    public ArrayList getDriverExpDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList driverExpDetails = new ArrayList();
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        driverExpDetails = operationDAO.getDriverExpDetails(fromDate, toDate, regNo, driId);
        if (driverExpDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return driverExpDetails;
    }

    public ArrayList getAdvDetails(String fromDate, String toDate, String regNo, int driId, String tripIds) throws FPBusinessException, FPRuntimeException {
        ArrayList AdvDetails = new ArrayList();
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        AdvDetails = operationDAO.getAdvDetails(fromDate, toDate, regNo, driId, tripIds);
        if (AdvDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return AdvDetails;
    }

    public ArrayList getFuelDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList fuelDetails = new ArrayList();
        /**
         * String[] temp = null; if (fromDate != "" && toDate != "") { temp =
         * fromDate.split("-"); String sDate = temp[2] + "-" + temp[1] + "-" +
         * temp[0]; fromDate = sDate; temp = toDate.split("-");
         *
         * String eDate = temp[2] + "-" + temp[1] + "-" + temp[0]; toDate =
         * eDate; }
         */
        fuelDetails = operationDAO.getFuelDetails(fromDate, toDate, regNo, driId);
        if (fuelDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return fuelDetails;
    }

    public ArrayList getHaltDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList haltDetails = new ArrayList();
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        haltDetails = operationDAO.getHaltDetails(fromDate, toDate, regNo, driId);
        if (haltDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return haltDetails;
    }

    public ArrayList getRemarkDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList remarkDetails = new ArrayList();
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        remarkDetails = operationDAO.getRemarkDetails(fromDate, toDate, regNo, driId);
        if (remarkDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return remarkDetails;
    }

    public int insertEposTripExpenses(String tripId, String expenseDesc, String expenseAmt, String expenseDate, String expenseRemarks, int userId) {
        int status = 0;
        status = operationDAO.insertEposTripExpenses(tripId, expenseDesc, expenseAmt, expenseDate, expenseRemarks, userId);
        return status;
    }

    public int saveTripClosureAccountDetails(ArrayList etmExpDetails, String vehicleType, String driverType, String vehicleVendorId, String driverVendorId,
            String driverNameId, int userId, String settlementType) {
        int status = 0;
        status = operationDAO.saveTripClosureAccountDetails(etmExpDetails, vehicleType, driverType, vehicleVendorId, driverVendorId,
                driverNameId, userId, settlementType);
        return status;
    }

    public int saveTripClosureAdvanceAccountDetails(ArrayList tripAllowanceDetails, String vehicleType, String driverType, String vehicleVendorId, String driverVendorId,
            String driverNameId, int userId) {
        int status = 0;
        status = operationDAO.saveTripClosureAdvanceAccountDetails(tripAllowanceDetails, vehicleType, driverType, vehicleVendorId, driverVendorId,
                driverNameId, userId);
        return status;
    }

    public int getTripCountDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int tripCount = 0;
        tripCount = operationDAO.getTripCountDetails(fromDate, toDate, regNo, driId);
        return tripCount;
    }

    public int getTotalTonnageDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int totalTonnage = 0;
        totalTonnage = operationDAO.getTotalTonnageDetails(fromDate, toDate, regNo, driId);
        return totalTonnage;
    }

    public int getOutKmDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int outKm = 0;
        outKm = operationDAO.getOutKmDetails(fromDate, toDate, regNo, driId);
        return outKm;
    }

    public int getInKmDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int inKm = 0;
        inKm = operationDAO.getInKmDetails(fromDate, toDate, regNo, driId);
        return inKm;
    }

    public ArrayList getTotFuelDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        ArrayList totalFuelDetails = new ArrayList();
        totalFuelDetails = operationDAO.getTotFuelDetails(fromDate, toDate, regNo, driId);
        return totalFuelDetails;
    }

    public int getDriverExpenseDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int driExpense = 0;
        driExpense = operationDAO.getDriverExpenseDetails(fromDate, toDate, regNo, driId);
        return driExpense;
    }

    public int getDriverSalaryDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int driExpense = 0;
        driExpense = operationDAO.getDriverSalaryDetails(fromDate, toDate, regNo, driId);
        return driExpense;
    }

    public int getGeneralExpenseDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int getExpense = 0;
        getExpense = operationDAO.getGeneralExpenseDetails(fromDate, toDate, regNo, driId);
        return getExpense;
    }

    public int getDriverAdvanceDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int advDetails = 0;
        advDetails = operationDAO.getDriverAdvanceDetails(fromDate, toDate, regNo, driId);
        return advDetails;
    }

    public int getDriverBataDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int advDetails = 0;
        advDetails = operationDAO.getDriverBataDetails(fromDate, toDate, regNo, driId);
        return advDetails;
    }

    public int insertTripDriverSettlement(Map map) {
        int status = 0;
        status = operationDAO.insertTripDriverSettlement(map);
        return status;
    }

    public int updateDriverSettlement(String fromDate, String toDate, String regNo, int driId) {
        int status = 0;
        status = operationDAO.updateDriverSettlement(fromDate, toDate, regNo, driId);
        return status;
    }

    public ArrayList getOperationLocation() throws FPBusinessException, FPRuntimeException {
        ArrayList opLocation = new ArrayList();
        opLocation = operationDAO.getOperationLocation();
        return opLocation;
    }

    public ArrayList getExpensesList() throws FPBusinessException, FPRuntimeException {
        ArrayList expensesList = new ArrayList();
        expensesList = operationDAO.getExpensesList();
        return expensesList;
    }

    public ArrayList getBunkNameLocation() throws FPBusinessException, FPRuntimeException {
        ArrayList bunkLocation = new ArrayList();
        bunkLocation = operationDAO.getBunkNameLocation();
        return bunkLocation;
    }

    public ArrayList getAdvancePaidBy() throws FPBusinessException, FPRuntimeException {
        ArrayList paidBy = new ArrayList();
        paidBy = operationDAO.getAdvancePaidBy();
        return paidBy;
    }

    public ArrayList getBunkList() throws FPBusinessException, FPRuntimeException {
        ArrayList bunkList = new ArrayList();
        bunkList = operationDAO.getBunkList();
        return bunkList;
    }

    public ArrayList getReturnTripList(String tripId, String returnTripId) throws FPBusinessException, FPRuntimeException {
        ArrayList returnTripList = new ArrayList();
        returnTripList = operationDAO.getReturnTripList(tripId, returnTripId);
        return returnTripList;
    }

    public ArrayList getDriverName() throws FPBusinessException, FPRuntimeException {
        ArrayList driverName = new ArrayList();
        driverName = operationDAO.getDriverName();
        return driverName;
    }

    public ArrayList getCardNos() throws FPBusinessException, FPRuntimeException {
        ArrayList cardNos = new ArrayList();
        cardNos = operationDAO.getCardNos();
        return cardNos;
    }

    public ArrayList getVehicleNos() throws FPBusinessException, FPRuntimeException {
        ArrayList vehicleNos = new ArrayList();
        vehicleNos = operationDAO.getVehicleNos();
        return vehicleNos;
    }

    public ArrayList getVehicleNosWithDriver() throws FPBusinessException, FPRuntimeException {
        ArrayList vehicleNos = new ArrayList();
        vehicleNos = operationDAO.getVehicleNosWithDriver();
        return vehicleNos;
    }

    public ArrayList getCustName() throws FPBusinessException, FPRuntimeException {
        ArrayList custName = new ArrayList();
        custName = operationDAO.getCustName();
        return custName;
    }

    public ArrayList searchOverAllTripDetails(String tripId, String regno, String driId, String status, String locId, String fromDate, String toDate, String ownership) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetails = new ArrayList();
        System.out.println("searchOverAllTripDetails...BP");
        tripDetails = operationDAO.searchOverAllTripDetails(tripId, regno, driId, status, locId, fromDate, toDate, ownership);
        return tripDetails;
    }

    public ArrayList searchDriverDetails(String driName) throws FPBusinessException, FPRuntimeException {
        ArrayList cardDetails = new ArrayList();
        System.out.println("searchDriverDetails...BP");
        cardDetails = operationDAO.searchDriverDetails(driName);
        return cardDetails;
    }

    public ArrayList alterCardMapping(String driId) throws FPBusinessException, FPRuntimeException {
        ArrayList cardDetails = new ArrayList();
        System.out.println("alterCardMapping...BP");
        cardDetails = operationDAO.alterCardMapping(driId);
        return cardDetails;
    }

    public int saveCardVehicleDriver(String cardId, String vehicleId, String driverId, String remark) {
        int status = 0;
        status = operationDAO.saveCardVehicleDriver(cardId, vehicleId, driverId, remark);
        return status;
    }
    //Rathimeena Code End

//   CLPL code start
    //-----------PinkSlip starts here---------------
    public String getPinkSlipDetail(String vehicleNo) {
        System.out.println("pink ajax BP.............");
        String pinkSlipDetails = "";
        pinkSlipDetails = operationDAO.getPinkSlipDetail(vehicleNo);
        return pinkSlipDetails;
    }

    public String pinkSlipVechicelStatus(String vehicleNo) {
        System.out.println("pink ajax BP ashok.............");
        String status = "";
        status = operationDAO.pinkSlipVechicelStatus(vehicleNo);
        return status;
    }

    public String getReturnTripDetails(String tripId) {
        String returnTripDetails = "";
        returnTripDetails = operationDAO.getReturnTripDetails(tripId);
        return returnTripDetails;
    }

    /**
     * This method used to get lps List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getLpsList(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        ArrayList lpsList = new ArrayList();
        lpsList = operationDAO.getLpsList(operationTO);
        //         if (groupList.size() == 0) {
        //            throw new FPBusinessException("EM-GEN-01");
        //        }
        return lpsList;
    }

    public String getLpsSummary(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        String lpsSummary = "";
        lpsSummary = operationDAO.getLpsSummary(operationTO);
        return lpsSummary;
    }

    public int saveLPS(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = operationDAO.saveLPS(operationTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public ArrayList processLPSalterList(String lpsId) throws FPBusinessException, FPRuntimeException {
        ArrayList lpsList = new ArrayList();
        lpsList = operationDAO.processLPSalterList(lpsId);
        if (lpsList.size() == 0) {
            throw new FPBusinessException("EM-CUST-01");
        }
        return lpsList;
    }

    public String getCustomerSuggestion(String customer) {
        customer = customer + "%";
        customer = operationDAO.getCustomerSuggestion(customer);
        return customer;
    }

    public String getFromLocation(String formLocation) {
        formLocation = formLocation + "%%";
        formLocation = operationDAO.getFromLocation(formLocation);
        System.out.println("formLocation = " + formLocation);
        return formLocation;
    }

    public String getToLocation(String toLocation, String customer, String fromLocation) {
        toLocation = toLocation + "%%";
        toLocation = operationDAO.getToLocation(toLocation, customer, fromLocation);
        return toLocation;
    }

    public String getProductSuggestion(String product) {
        product = product + "%";
        product = operationDAO.getProductSuggestion(product);
        return product;
    }

    public ArrayList getLPSProductList() throws FPBusinessException, FPRuntimeException {
        System.out.println("..................iii");
        ArrayList productList = new ArrayList();
        productList = operationDAO.getLPSProductList();
        return productList;
    }

    public int saveAlterLPS(OperationTO operationTO, String lpsId, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = operationDAO.saveAlterLPS(operationTO, lpsId, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    /**
     * This method used to get PinkSlip List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String pinkSlipVehicleNo(String regno) {
        System.out.println("i am in pinkSlip ajax  in bp");
        String vehileRegNo = "";
        System.out.println("regno =////====> " + regno);
        vehileRegNo = operationDAO.pinkSlipVehicleNo(regno);
        return vehileRegNo;
    }

    public String pinkSlipDriver(String driName) {
        String driverName = "";
        driName = driName + "%";
        driverName = operationDAO.pinkSlipDriver(driName);
        return driverName;
    }

    public String getVehicleDetailsPinkSlip(String regNo) {
        String vehicleDetailsPinkSlip = "";
        vehicleDetailsPinkSlip = operationDAO.getVehicleDetailsPinkSlip(regNo);
        System.out.println("vehicleDetailsPinkSlip in BP= " + vehicleDetailsPinkSlip);
        return vehicleDetailsPinkSlip;
    }

    public ArrayList getLPSOrderList() throws FPBusinessException, FPRuntimeException {
        ArrayList LPSOrderList = new ArrayList();
        LPSOrderList = operationDAO.getLPSOrderList();
        return LPSOrderList;
    }

    public ArrayList getlorryDetailList(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        ArrayList lorryDetailList = new ArrayList();
        lorryDetailList = operationDAO.getlorryDetailList(operationTO);
        return lorryDetailList;
    }

    public ArrayList getPinkSlipList() throws FPBusinessException, FPRuntimeException {
        ArrayList pinkSlipList = new ArrayList();
        pinkSlipList = operationDAO.getPinkSlipList();
        return pinkSlipList;
    }

    public ArrayList getpinkSlipSummaryDetail(OperationTO operationTo) throws FPBusinessException, FPRuntimeException {
        ArrayList pinkSlipSummaryList = new ArrayList();
        pinkSlipSummaryList = operationDAO.getpinkSlipSummaryDetail(operationTo);
        return pinkSlipSummaryList;
    }

    public String getpinkSlipSummary(OperationTO operationTo) throws FPBusinessException, FPRuntimeException {
        String pinkSlipSummary = "";
        pinkSlipSummary = operationDAO.getpinkSlipSummary(operationTo);
        return pinkSlipSummary;
    }

    public ArrayList getViewPinkSlip(String pinkSlipID, String lpsID) throws FPBusinessException, FPRuntimeException {
        ArrayList pinkSlipList = new ArrayList();
        pinkSlipList = operationDAO.getViewPinkSlip(pinkSlipID, lpsID);
        return pinkSlipList;

    }

    public ArrayList getSuggestedLPSList(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        ArrayList SuggestedLPSList = new ArrayList();
        SuggestedLPSList = operationDAO.getSuggestedLPSList(operationTO);
        return SuggestedLPSList;
    }

    public ArrayList getSuggestedPinkSlip(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        ArrayList SuggestedPinkSlip = new ArrayList();
        SuggestedPinkSlip = operationDAO.getSuggestedPinkSlip(operationTO);
        System.out.println("SuggestedPinkSlip in BP = " + SuggestedPinkSlip.size());
        return SuggestedPinkSlip;
    }

    public int savePinkSlip(OperationTO operationTO, int userId, String selectedLpsIds, String selecteddestination, String selectedBags, String selectedTon, String twoLPSStatus, String selectedTonRate, String selectedTonRateMarket, String ownership, String selecteddestinationName, String selectedbillStatus, String selectedPacking, String selectedProductName) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = operationDAO.savePinkSlip(operationTO, userId, selectedLpsIds, selecteddestination, selectedBags, selectedTon, twoLPSStatus, selectedTonRate, selectedTonRateMarket, ownership, selecteddestinationName, selectedbillStatus, selectedPacking, selectedProductName);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public ArrayList getpinkSliplist() throws FPBusinessException, FPRuntimeException {
        ArrayList SuggestedPinkSlip = new ArrayList();
        SuggestedPinkSlip = operationDAO.getpinkSliplist();
        System.out.println("SuggestedPinkSlip in BP = " + SuggestedPinkSlip.size());
        return SuggestedPinkSlip;
    }

    public ArrayList getpinkSlipSummarylist(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        ArrayList pinkSlipSummarylist = new ArrayList();
        pinkSlipSummarylist = operationDAO.getpinkSlipSummarylist(operationTO);
        System.out.println("SuggestedPinkSlip in BP = " + pinkSlipSummarylist.size());
        return pinkSlipSummarylist;
    }

    public int getpinkSlipDelete(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        int pinkSlipDelete = 0;
        pinkSlipDelete = operationDAO.getpinkSlipDelete(operationTO);
        System.out.println("SuggestedPinkSlip in BP = " + pinkSlipDelete);
        return pinkSlipDelete;
    }

    public List getPinkSlipLPSList() throws FPBusinessException, FPRuntimeException {
        List SuggestedPinkSlip = new ArrayList();
        SuggestedPinkSlip = operationDAO.getPinkSlipLPSList();
        System.out.println("SuggestedPinkSlip in BP = " + SuggestedPinkSlip.size());
        return SuggestedPinkSlip;
    }

    public List getpinkSlipSummaryLPSList(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        List pinkSlipSummaryLPSList = new ArrayList();
        pinkSlipSummaryLPSList = operationDAO.getpinkSlipSummaryLPSList(operationTO);
        System.out.println("SuggestedPinkSlip in BP = " + pinkSlipSummaryLPSList.size());
        return pinkSlipSummaryLPSList;
    }

    public ArrayList getViewPinkSlipLPSList(String lpsID) throws FPBusinessException, FPRuntimeException {
        ArrayList SuggestedPinkSlip = new ArrayList();
        SuggestedPinkSlip = operationDAO.getViewPinkSlipLPSList(lpsID);
        System.out.println("SuggestedPinkSlip in BP = " + SuggestedPinkSlip.size());
        return SuggestedPinkSlip;
    }

    /**
     * This method used to get ProductMaster.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getProductList() throws FPBusinessException, FPRuntimeException {
        ArrayList productList = new ArrayList();
        productList = operationDAO.getProductList();
//         if (groupList.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
//        }
        return productList;
    }

    public int saveProductMaster(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = operationDAO.saveProductMaster(operationTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public ArrayList processProductMasterList(String productId) throws FPBusinessException, FPRuntimeException {
        ArrayList productList = new ArrayList();
        productList = operationDAO.processAlterProductMasterList(productId);
        if (productList.size() == 0) {
            throw new FPBusinessException("EM-CUST-01");
        }
        return productList;
    }

    public int saveAlterProductMaster(OperationTO operationTO, String productId, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = operationDAO.saveAlterProductMaster(operationTO, productId, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    /**
     * This method used to Invoice .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getTripList(String fromDate, String toDate, String customer, String ownership, String billType) throws FPBusinessException, FPRuntimeException {
        ArrayList tripList = new ArrayList();
        tripList = operationDAO.getTripList(fromDate, toDate, customer, ownership, billType);
        return tripList;
    }

    public ArrayList getLastInvoiceDate(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        ArrayList LastInvoiceDate = new ArrayList();
        LastInvoiceDate = operationDAO.getLastInvoiceDate(operationTO);
        return LastInvoiceDate;
    }

    public ArrayList getCustomerList() throws FPBusinessException, FPRuntimeException {
        ArrayList customerList = new ArrayList();
        customerList = operationDAO.getCustomerList();
        return customerList;
    }

    public ArrayList gettotalList(String fromDate, String toDate, String customer) throws FPBusinessException, FPRuntimeException {
        ArrayList totalList = new ArrayList();
        totalList = operationDAO.gettotalList(fromDate, toDate, customer);
        return totalList;
    }

    public int saveInvoiceDetail(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int lastInsertId = 0;
        lastInsertId = operationDAO.saveInvoiceDetail(operationTO, userId);
        return lastInsertId;
    }

    public int saveInvoice(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = operationDAO.saveInvoice(operationTO, userId);
        return status;
    }

    public int alterTripStatus(String tripId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = operationDAO.alterTripStatus(tripId);
        return status;
    }

    public int saveaccountEntry(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int lastInsertId = 0;
        lastInsertId = operationDAO.saveaccountEntry(operationTO, userId);
        return lastInsertId;
    }

    public String getInvoiceCode() throws FPRuntimeException, FPBusinessException {
        String invoiceCode = "";
        invoiceCode = operationDAO.getInvoiceCode();
        return invoiceCode;
    }

    public ArrayList getInvoiceDetailList(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        ArrayList invoiceDetailList = new ArrayList();
        invoiceDetailList = operationDAO.getInvoiceDetailList(operationTO);
        return invoiceDetailList;
    }

    public ArrayList getinvoiceTripDetailList(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        ArrayList invoiceTripDetailList = new ArrayList();
        invoiceTripDetailList = operationDAO.getinvoiceTripDetailList(operationTO);
        return invoiceTripDetailList;
    }

    public ArrayList getinvoiceHeaderList() throws FPBusinessException, FPRuntimeException {
        ArrayList invoiceHeaderList = new ArrayList();
        invoiceHeaderList = operationDAO.getinvoiceHeaderList();
        return invoiceHeaderList;
    }

    public ArrayList getDisplyInvoiceSummary(String fromDate, String toDate, String customer) throws FPBusinessException, FPRuntimeException {
        ArrayList displyInvoiceSummary = new ArrayList();
        displyInvoiceSummary = operationDAO.getDisplyInvoiceSummary(fromDate, toDate, customer);
        return displyInvoiceSummary;
    }

    /**
     * This method used to get Market Vehicle Settlement.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
// VendorList:
    public ArrayList processVendorList()
            throws FPBusinessException, FPRuntimeException {
        ArrayList vendorList = new ArrayList();
        vendorList = operationDAO.getVendorList();
        if (vendorList.size() == 0) {
            throw new FPBusinessException("EM-MPR-02");
        } else {
            return vendorList;
        }
    }

    public ArrayList getMarketVehicleList(String fromDate, String toDate, String regno, String vendorId) throws FPBusinessException, FPRuntimeException {
        ArrayList marketVehicleList = new ArrayList();
        String[] temp = null;
        if (fromDate != "" && toDate != "") {
            temp = fromDate.split("-");
            String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
            fromDate = sDate;
            temp = toDate.split("-");

            String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
            toDate = eDate;
        }

        marketVehicleList = operationDAO.getMarketVehicleList(fromDate, toDate, regno, vendorId);
        if (marketVehicleList.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return marketVehicleList;
    }

    public ArrayList getTotalMarketVehicleList(String fromDate, String toDate, String regno, String vendorId) throws FPBusinessException, FPRuntimeException {
        ArrayList totalMarketVehicleList = new ArrayList();
        String[] temp = null;
        if (fromDate != "" && toDate != "") {
            temp = fromDate.split("-");
            String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
            fromDate = sDate;
            temp = toDate.split("-");

            String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
            toDate = eDate;
        }

        totalMarketVehicleList = operationDAO.getTotalMarketVehicleList(fromDate, toDate, regno, vendorId);
        if (totalMarketVehicleList.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return totalMarketVehicleList;
    }

    public ArrayList getCommissionVal() throws FPBusinessException, FPRuntimeException {
        ArrayList commissionVal = new ArrayList();
        commissionVal = operationDAO.getCommissionVal();

        return commissionVal;
    }

    public int saveVendorSettlementHeader(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int lastInsertId = 0;
        lastInsertId = operationDAO.saveVendorSettlementHeader(operationTO, userId);
        return lastInsertId;
    }

    public int alterTripSettlement(String tripId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = operationDAO.alterTripSettlement(tripId);

        return status;
    }

    public int saveVendorSettlementAccEntry(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int lastInsertId = 0;
        lastInsertId = operationDAO.saveVendorSettlementAccEntry(operationTO, userId);
        return lastInsertId;
    }

    public int saveVendorSettlementDetail(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = operationDAO.saveVendorSettlementDetail(operationTO, userId);

        return status;
    }

    public ArrayList getsettlementHeader() throws FPBusinessException, FPRuntimeException {
        ArrayList settlementHeader = new ArrayList();
        settlementHeader = operationDAO.getsettlementHeader();

        return settlementHeader;
    }

    public ArrayList getsettlementDetail() throws FPBusinessException, FPRuntimeException {
        ArrayList settlementDetail = new ArrayList();
        settlementDetail = operationDAO.getsettlementDetail();

        return settlementDetail;
    }

    public String saveReturnMovement(int userId, String tripCode, String tripDate, String customer, String fromLocation, String fromLocationId, String toLocation, String toLocationId,
            String returnTripType, String returnProductName, String returnLoadedDate, String returnDeliveredDate, String loadedTon, String deliveredTon, String shortageTon, String loadedSlipNo, String deliveredDANo, String returnKM, String inKM, String outKM, String returnAmount) {
        System.out.println("in BP......");
        String returnTripId = "";
        returnTripId = operationDAO.saveReturnMovement(userId, tripCode, tripDate, customer, fromLocation, fromLocationId, toLocation, toLocationId, returnTripType, returnProductName, returnLoadedDate, returnDeliveredDate, loadedTon, deliveredTon, shortageTon, loadedSlipNo, deliveredDANo, returnKM, inKM, outKM, returnAmount);

        return returnTripId;
    }

    public String getFLocation(String fromLocation) {
        //String destination = "";
        fromLocation = fromLocation + "%";
        fromLocation = operationDAO.getFLocation(fromLocation);
        return fromLocation;
    }

    public String getTLocation(String toLocation) {
        //String destination = "";
        toLocation = toLocation + "%";
        toLocation = operationDAO.getTLocation(toLocation);
        return toLocation;
    }

    public ArrayList consignmentPrint(String tripSheetId) {
        ArrayList tripDetails = new ArrayList();
        tripDetails = operationDAO.consignmentPrint(tripSheetId);
        return tripDetails;
    }

    //CLPL code start
//    Brattle Foods Starts Here
    public String getCurrentFuelPrice() {
        String currentFuelPrice = "";
        currentFuelPrice = operationDAO.getCurrentFuelPrice();
        return currentFuelPrice;
    }

    public String getCurrentCNGPrice() {
        String currentCNGPrice = "";
        currentCNGPrice = operationDAO.getCurrentCNGPrice();
        return currentCNGPrice;
    }

    public ArrayList getMileageConfigList(String customerId) {
        ArrayList mileageConfigList = new ArrayList();
        mileageConfigList = operationDAO.getMileageConfigList(customerId);
        return mileageConfigList;
    }

    /**
     * This method used to Get Customer Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getConfigDetails(OperationTO operationTO) {
        String configDetails = "";
        configDetails = operationDAO.getConfigDetails(operationTO);
        return configDetails;
    }

    public String checkRouteCode(OperationTO operationTO) {
        String checkRouteCode = "";
        checkRouteCode = operationDAO.checkRouteCode(operationTO);
        return checkRouteCode;
    }

    public String getNextRouteCode() {
        String nextRouteCode = "";
        nextRouteCode = operationDAO.getNextRouteCode();
        return nextRouteCode;
    }

    public int saveRoute(OperationTO operationTO, int userId) {
        int insertStatus = 0;
        insertStatus = operationDAO.saveRoute(operationTO, userId);
        return insertStatus;
    }

    public ArrayList getRouteList(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        ArrayList routeList = new ArrayList();
        routeList = operationDAO.getRouteList(operationTO);
        return routeList;
    }

    public ArrayList getRouteNameFrom(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList routeNameFrom = new ArrayList();
        routeNameFrom = operationDAO.getRouteNameFrom(operationTO);
        return routeNameFrom;
    }

    public ArrayList getRouteNameTo(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList routeNameTo = new ArrayList();
        routeNameTo = operationDAO.getRouteNameTo(operationTO);
        return routeNameTo;
    }

    public ArrayList getRouteDetailsList(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        ArrayList routeDetailsList = new ArrayList();
        routeDetailsList = operationDAO.getRouteDetailsList(operationTO);
        return routeDetailsList;
    }

    public int updateRoute(OperationTO operationTO, int userId) {
        int updateStatus = 0;
        updateStatus = operationDAO.updateRoute(operationTO, userId);
        return updateStatus;
    }

    public ArrayList getRouteCode(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList routeCode = new ArrayList();
        routeCode = operationDAO.getRouteCode(operationTO);
        return routeCode;
    }

    public ArrayList getCity(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList city = new ArrayList();
        city = operationDAO.getCity(operationTO);
        return city;
    }

    /**
     * This method used to Check Route.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String checkRoute(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        String checkRoute = "";
        checkRoute = operationDAO.checkRoute(operationTO);
        return checkRoute;
    }

    /**
     * This method used to Save Standard Charges .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int saveStandardCharge(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int saveStandardCharge = 0;
        saveStandardCharge = operationDAO.saveStandardCharge(operationTO, userId);

        return saveStandardCharge;
    }

    /**
     * This method used to Get Unit List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getUnitList() throws FPBusinessException, FPRuntimeException {
        ArrayList unitList = new ArrayList();
        unitList = operationDAO.getUnitList();
        return unitList;
    }

    /**
     * This method used to Get Standard Charge List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getStandardChargeList() throws FPBusinessException, FPRuntimeException {
        ArrayList standardChargeList = new ArrayList();
        standardChargeList = operationDAO.getStandardChargeList();

        return standardChargeList;
    }

    /**
     * This method used to Update Standard Charge .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int updateStandardCharge(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateStandardCharge = 0;
        updateStandardCharge = operationDAO.updateStandardCharge(operationTO, userId);
        return updateStandardCharge;
    }

    /**
     * This method used to Get Vehicle Mfr Model List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleMfrModelList() throws FPBusinessException, FPRuntimeException {
        ArrayList vehicleMfrModelList = new ArrayList();
        vehicleMfrModelList = operationDAO.getVehicleMfrModelList();
        return vehicleMfrModelList;
    }

    /**
     * This method used to Get Vehicle Type List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleTypeList() throws FPBusinessException, FPRuntimeException {
        ArrayList vehicleTypeList = new ArrayList();
        vehicleTypeList = operationDAO.getVehicleTypeList();

        return vehicleTypeList;
    }

    public ArrayList getVehicleTypeForActualKM(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        ArrayList vehicleTypeList = new ArrayList();
        vehicleTypeList = operationDAO.getVehicleTypeForActualKM(operationTO);

        return vehicleTypeList;
    }

    /**
     * This method used to Get Billing Type List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getBillingTypeList() throws FPBusinessException, FPRuntimeException {
        ArrayList billingTypeList = new ArrayList();
        billingTypeList = operationDAO.getBillingTypeList();

        return billingTypeList;
    }

    public ArrayList getPickupPoint(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList pickupPoint = new ArrayList();
        pickupPoint = operationDAO.getPickupPoint(operationTO);
        return pickupPoint;
    }

    public ArrayList getInterimPoint(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList interimPoint = new ArrayList();
        interimPoint = operationDAO.getInterimPoint(operationTO);
        return interimPoint;
    }

    /**
     * This method used to Get Customer Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getCustomerDetails(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList customerDetails = new ArrayList();
        customerDetails = operationDAO.getCustomerDetails(operationTO);
        return customerDetails;
    }

    public ArrayList getWalkinCustomerDetails(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList customerDetails = new ArrayList();
        customerDetails = operationDAO.getWalkinCustomerDetails(operationTO);
        return customerDetails;
    }

    /**
     * This method used to insert Customer Contract Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int insertCustomerContractDetails(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = operationDAO.insertCustomerContractDetails(operationTO, userId);
        return insertStatus;
    }

    /**
     * This method used to insert Contract Standard Charge Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int insertContractStandardChargeDetails(OperationTO operationTO, String[] stChargeId, String[] stChargeAmount, String[] stChargeDate, String[] stChargeSelect, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = operationDAO.insertContractStandardChargeDetails(operationTO, stChargeId, stChargeAmount, stChargeDate, stChargeSelect, userId);
        return insertStatus;
    }

    /**
     * This method used to insert Contract PTP Billing Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int insertContractPTPBillingDetails(OperationTO operationTO, String[] ptpRouteCode, String[] ptpVehicleTypeId, String[] ptpPickupPoint, String[] ptpInterimPoint1, String[] ptpInterimPoint2, String[] ptpInterimPoint3, String[] ptpInterimPoint4, String[] ptpDropPoint, String[] ptpPickupPointId, String[] ptpInterimPointId1, String[] ptpInterimPointId2, String[] ptpInterimPointId3, String[] ptpInterimPointId4, String[] ptpDropPointId, String[] ptpInterimPoint1Km, String[] ptpInterimPoint2Km, String[] ptpInterimPoint3Km, String[] ptpInterimPoint4Km, String[] ptpDropPointKm, String[] interimPoint1Hrs, String[] interimPoint2Hrs, String[] interimPoint3Hrs, String[] interimPoint4Hrs, String[] ptpDropPointHrs, String[] interimPoint1Minutes, String[] interimPoint2Minutes, String[] interimPoint3Minutes, String[] interimPoint4Minutes, String[] ptpDropPointMinutes, String[] interimPoint1RouteId, String[] interimPoint2RouteId, String[] interimPoint3RouteId, String[] interimPoint4RouteId, String[] ptpDropPointRouteId, String[] ptpTotalKm, String[] ptpTotalHours, String[] ptpTotalMinutes, String[] ptpRateWithReefer, String[] ptpRateWithoutReefer,String[] movementTypeId, int userId, String[] tatTime) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = operationDAO.insertContractPTPBillingDetails(operationTO, ptpRouteCode, ptpVehicleTypeId, ptpPickupPoint, ptpInterimPoint1, ptpInterimPoint2, ptpInterimPoint3, ptpInterimPoint4, ptpDropPoint, ptpPickupPointId, ptpInterimPointId1, ptpInterimPointId2, ptpInterimPointId3, ptpInterimPointId4, ptpDropPointId, ptpInterimPoint1Km, ptpInterimPoint2Km, ptpInterimPoint3Km, ptpInterimPoint4Km, ptpDropPointKm, interimPoint1Hrs, interimPoint2Hrs, interimPoint3Hrs, interimPoint4Hrs, ptpDropPointHrs, interimPoint1Minutes, interimPoint2Minutes, interimPoint3Minutes, interimPoint4Minutes, ptpDropPointMinutes, interimPoint1RouteId, interimPoint2RouteId, interimPoint3RouteId, interimPoint4RouteId, ptpDropPointRouteId, ptpTotalKm, ptpTotalHours, ptpTotalMinutes, ptpRateWithReefer, ptpRateWithoutReefer,movementTypeId, userId, tatTime);
        System.out.println("arun called operationTO.getBillingTypeId() : "+operationTO.getBillingTypeId());
        if (operationTO.getBillingTypeId().equals("4")) {
            insertStatus = operationDAO.insertFixedContractRates(operationTO, userId);
        }
        return insertStatus;
    }

    /**
     * This method used to insert Contract PTPW Billing Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int insertContractPTPWBillingDetails(OperationTO operationTO, String[] ptpwRouteContractCode, String[] minWgtBase, String[] ptpwVehicleTypeId, String[] ptpwPickupPointId, String[] ptpwPickupPoint, String[] ptpwDropPoint, String[] ptpwDropPointId, String[] ptpwPointRouteId, String[] ptpwTotalKm, String[] ptpwTotalHrs, String[] ptpwTotalMinutes, String[] ptpwRateWithReefer, String[] ptpwRateWithoutReefer, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = operationDAO.insertContractPTPWBillingDetails(operationTO, ptpwRouteContractCode, minWgtBase, ptpwVehicleTypeId, ptpwPickupPointId, ptpwPickupPoint, ptpwDropPoint, ptpwDropPointId, ptpwPointRouteId, ptpwTotalKm, ptpwTotalHrs, ptpwTotalMinutes, ptpwRateWithReefer, ptpwRateWithoutReefer, userId);
        return insertStatus;
    }

    /**
     * This method used to insert Actual Km Billing Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int insertActualKmBillingDetails(OperationTO operationTO, String[] actualKmVehicleTypeId, String[] actualVehicleRsPerKm, String[] actualReeferRsPerHour, String[] actualKmSelect, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = operationDAO.insertActualKmBillingDetails(operationTO, actualKmVehicleTypeId, actualVehicleRsPerKm, actualReeferRsPerHour, actualKmSelect, userId);
        return insertStatus;
    }

    /**
     * This method used to Get Customer Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getCustomer(OperationTO operationTO) {
        String customerDetails = "";
        customerDetails = operationDAO.getCustomer(operationTO);
        return customerDetails;
    }

    /**
     * This method used to Get Contract Standard Charge List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getContractStandardChargeList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList cnotractStandardChargeList = new ArrayList();
        cnotractStandardChargeList = operationDAO.getContractStandardChargeList(operationTO);
        return cnotractStandardChargeList;
    }

    /**
     * This method used to Get Customer Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getCustomerContractDetails(OperationTO operationTO) {
        String customerContractDetails = "";
        customerContractDetails = operationDAO.getCustomerContractDetails(operationTO);
        return customerContractDetails;
    }

    /**
     * This method used to Get Contract Billing List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getContractBillingList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList contractBillingList = new ArrayList();
        contractBillingList = operationDAO.getContractBillingList(operationTO);
        return contractBillingList;
    }

    public ArrayList getContractFixedRateList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList contractFixedRateList = new ArrayList();
        contractFixedRateList = operationDAO.getContractFixedRateList(operationTO);
        return contractFixedRateList;
    }

    /**
     * This method used to Edit Contract Standard Charge List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList editContractStandardChargeList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList editContractStandardChargeList = new ArrayList();
        editContractStandardChargeList = operationDAO.editContractStandardChargeList(operationTO);
        return editContractStandardChargeList;
    }

    /**
     * This method used to Update Customer Contract Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int updateCustomerContractDetails(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 0;
        updateStatus = operationDAO.updateCustomerContractDetails(operationTO, userId);
        return updateStatus;
    }

    /**
     * This method used to Update Contract Standard Charge Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int updateContractStandardChargeDetails(OperationTO operationTO, String[] stChargeId, String[] stChargeAmount, String[] stChargeDate, String[] stChargeSelect, int userId) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 0;
        updateStatus = operationDAO.updateContractStandardChargeDetails(operationTO, stChargeId, stChargeAmount, stChargeDate, stChargeSelect, userId);
        return updateStatus;
    }

    /**
     * This method used to Get Consignment Order No.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getConsignmentOrderNo(OperationTO operationTO) {
        String consignmentOrderNo = "";
        consignmentOrderNo = operationDAO.getConsignmentOrderNo(operationTO);
        return consignmentOrderNo;
    }

    /**
     * This method used to Edit Contract Standard Charge List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getProductCategoryList() throws FPRuntimeException, FPBusinessException {
        ArrayList productCategoryList = new ArrayList();
        productCategoryList = operationDAO.getProductCategoryList();
        return productCategoryList;
    }

    /**
     * This method used to Insert Consignment Note No.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int insertConsignmentNote(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = operationDAO.insertConsignmentNote(operationTO, userId);
        return insertStatus;
    }

    /**
     * This method used to Insert Multi Pickup And Multi Origin Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int insertMultiplePoints(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = operationDAO.insertMultiplePoints(operationTO, userId);
        return insertStatus;
    }
    //Throttle Starts 11-12-2013

    public String checkVehicleDriverMapping(String vehicleId) {
        String checkVehicleDriverMapping = "";
        checkVehicleDriverMapping = operationDAO.checkVehicleDriverMapping(vehicleId);
        return checkVehicleDriverMapping;
    }
    //Throttle Ends 11-12-2013

    /**
     * This method used to Get Customer Type List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getCustomerTypeList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList customerTypeList = new ArrayList();
        customerTypeList = operationDAO.getCustomerTypeList(operationTO);
        return customerTypeList;
    }

    /**
     * This method used to Get Contract Route List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getContractRouteList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList contractRouteList = new ArrayList();
        contractRouteList = operationDAO.getContractRouteList(operationTO);
        return contractRouteList;
    }

    public ArrayList fetchCustomerProducts(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList result = new ArrayList();
        result = operationDAO.fetchCustomerProducts(operationTO);
        return result;
    }

    public ArrayList getContractRoutes(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList contractRouteList = new ArrayList();
        contractRouteList = operationDAO.getContractRoutes(operationTO);
        return contractRouteList;
    }

    public ArrayList getContractRoutesOrigin(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList contractRouteList = new ArrayList();
        contractRouteList = operationDAO.getContractRoutesOrigin(operationTO);
        return contractRouteList;
    }

    public ArrayList getContractRoutesOriginDestination(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList contractRouteList = new ArrayList();
        contractRouteList = operationDAO.getContractRoutesOriginDestination(operationTO);
        return contractRouteList;
    }

    public ArrayList getContractRouteCourse(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList contractRouteList = new ArrayList();
        contractRouteList = operationDAO.getContractRouteCourse(operationTO);
        return contractRouteList;
    }

    /**
     * This method used to Get Contract Route Destination List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getRouteDestinationList(int routeId, String customerTypeId) throws FPRuntimeException, FPBusinessException {
        ArrayList routeDestinationList = new ArrayList();
        int counter = 0;
        OperationTO operationTO = null;
        routeDestinationList = operationDAO.getRouteDestinationList(routeId, customerTypeId);
        Iterator itr;
        String routeDestination = "";
        if (routeDestinationList.size() == 0) {
            routeDestination = "";
        } else {
            itr = routeDestinationList.iterator();
            while (itr.hasNext()) {
                operationTO = new OperationTO();
                operationTO = (OperationTO) itr.next();
                if (counter == 0) {
                    routeDestination = operationTO.getTotalKm() + "#" + operationTO.getFinalDrop();
                    counter++;
                } else {
                    routeDestination = routeDestination + "~" + operationTO.getTotalKm() + "#" + operationTO.getFinalDrop();
                }
            }
        }
        return routeDestination;
    }

    /**
     * This method used to Get Contract Route Destination List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getContractRouteDestinationList(int originId, String billingTypeId, String contractId) throws FPRuntimeException, FPBusinessException {
        ArrayList routeDestinationList = new ArrayList();
        int counter = 0;
        OperationTO operationTO = null;
        routeDestinationList = operationDAO.getContractRouteDestinationList(originId, billingTypeId, contractId);
        Iterator itr;
        String routeDestination = "";
        if (routeDestinationList.size() == 0) {
            routeDestination = "";
        } else {
            itr = routeDestinationList.iterator();
            while (itr.hasNext()) {
                operationTO = new OperationTO();
                operationTO = (OperationTO) itr.next();
                if (counter == 0) {
                    routeDestination = operationTO.getFinalPointId() + "#" + operationTO.getFinalPointName();
                    counter++;
                } else {
                    routeDestination = routeDestination + "~" + operationTO.getFinalPointId() + "#" + operationTO.getFinalPointName();
                }
            }
        }
        return routeDestination;
    }

    /**
     * This method used to Get Contract Vehicle Type List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getContractVehicleTypeList(int contractId, String billingTypeId, String customerTypeId, String origin, String destination) throws FPRuntimeException, FPBusinessException {
        ArrayList routeVehicleTypeList = new ArrayList();
        int counter = 0;
        OperationTO operationTO = null;
        routeVehicleTypeList = operationDAO.getContractVehicleTypeList(contractId, billingTypeId, customerTypeId, origin, destination);
        Iterator itr;
        String routeDestination = "";
        if (routeVehicleTypeList.size() == 0) {
            routeDestination = "";
        } else {
            itr = routeVehicleTypeList.iterator();
            while (itr.hasNext()) {
                operationTO = new OperationTO();
                operationTO = (OperationTO) itr.next();
                if (counter == 0) {
                    routeDestination = operationTO.getVehicleTypeId() + "#" + operationTO.getVehicleTypeName();
                    counter++;
                } else {
                    routeDestination = routeDestination + "~" + operationTO.getVehicleTypeId() + "#" + operationTO.getVehicleTypeName();
                }
            }
        }
        return routeDestination;
    }

    public ArrayList getContractVehicleTypeForPointsList(int contractId,
            String firstPointId, String point1Id, String point2Id, String point3Id, String point4Id, String finalPointId, String billingTypeId) throws FPRuntimeException, FPBusinessException {
        ArrayList routeVehicleTypeList = new ArrayList();
        int counter = 0;
        OperationTO operationTO = null;
        routeVehicleTypeList = operationDAO.getContractVehicleTypeForPointsList(contractId, firstPointId, point1Id, point2Id, point3Id, point4Id, finalPointId, billingTypeId);
        /*
         Iterator itr;
         String routeDestination = "";
         if (routeVehicleTypeList.size() == 0) {
         routeDestination = "";
         } else {
         itr = routeVehicleTypeList.iterator();
         while (itr.hasNext()) {
         operationTO = new OperationTO();
         operationTO = (OperationTO) itr.next();
         if (counter == 0) {
         routeDestination = operationTO.getVehicleTypeId() + "#" + operationTO.getVehicleTypeName();
         counter++;
         } else {
         routeDestination = routeDestination + "~" + operationTO.getVehicleTypeId() + "#" + operationTO.getVehicleTypeName();
         }
         }
         }
         */
        return routeVehicleTypeList;
    }

    /**
     * This method is used to View Fuel Price.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getfuelPriceMaster() throws FPBusinessException, FPRuntimeException {
        ArrayList viewfuelPriceMaster = new ArrayList();
        viewfuelPriceMaster = operationDAO.getfuelPriceMaster();

        return viewfuelPriceMaster;
    }

    /**
     * This method is used to Get Save Fuel Price.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public double saveFuelPriceMaster(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        double fuelPriceMaster = 0.0;
        fuelPriceMaster = operationDAO.saveFuelPriceMaster(operationTO, userId);

        return fuelPriceMaster;
    }

    /**
     * This method used to Get Average Current Fuel Cost.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getAverageCurrentFuelCost(OperationTO operationTO) {
        String avgCurrentFuelCost = "";
        avgCurrentFuelCost = operationDAO.getAverageCurrentFuelCost(operationTO);
        return avgCurrentFuelCost;
    }

    /**
     * This method used to Get Account Manager Name.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getAccountManager(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList accountManagerList = new ArrayList();
        accountManagerList = operationDAO.getAccountManager(operationTO);
        return accountManagerList;
    }

    /**
     * This method used to Get City Name.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getCityList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList cityList = new ArrayList();
        cityList = operationDAO.getCityList(operationTO);
        return cityList;
    }

    public ArrayList getPTPCityList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList cityList = new ArrayList();
        String pointOrder = operationTO.getPointOrder();
        if ("2".equals(pointOrder)) {//looking for interim point 1
            cityList = operationDAO.getPoint1CityList(operationTO);
        } else if ("3".equals(pointOrder)) {//looking for interim point 2
            cityList = operationDAO.getPoint2CityList(operationTO);
        } else if ("4".equals(pointOrder)) {//looking for interim point 3
            cityList = operationDAO.getPoint3CityList(operationTO);
        } else if ("5".equals(pointOrder)) {//looking for interim point 4
            cityList = operationDAO.getPoint4CityList(operationTO);
        }

        return cityList;
    }

    /**
     * This method used to Get Consignment List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getConsignmentList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consignmentList = new ArrayList();
        consignmentList = operationDAO.getConsignmentList(operationTO);
        return consignmentList;
    }

    public ArrayList getConsignmentViewList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consignmentList = new ArrayList();
        consignmentList = operationDAO.getConsignmentViewList(operationTO);
        return consignmentList;
    }

    public ArrayList getConsignmentDetails(String id) throws FPRuntimeException, FPBusinessException {
        ArrayList consignmentList = new ArrayList();
        consignmentList = operationDAO.getConsignmentDetails(id);
        return consignmentList;
    }

    /**
     * This method used to Get Route Course.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getRouteCourse(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList routeCourse = new ArrayList();
        routeCourse = operationDAO.getRouteCourse(operationTO);
        return routeCourse;
    }

    /**
     * This method used to Check Route Exists For CNote Route Course.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String checkIfRouteExists(String pointIdValue, String pointIdPrev, String pointIdNext) {
        String checkIfRouteExists = "";
        checkIfRouteExists = operationDAO.checkIfRouteExists(pointIdValue, pointIdPrev, pointIdNext);
        return checkIfRouteExists;
    }

//    Brattle Foods Ends Here
//    Brattle Foods Ends starts Arun 7 dec  Here
    /**
     * This method used to Get Consignment List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList viewConsignmentList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consignmentList = new ArrayList();
        consignmentList = operationDAO.viewConsignmentList(operationTO);
        return consignmentList;
    }

    /**
     * This method used to Get Consignment Articles.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getConsignmentArticles(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consignmentArticles = new ArrayList();
        consignmentArticles = operationDAO.getConsignmentArticles(operationTO);
        return consignmentArticles;
    }

    /**
     * This method used to Get Consignment Points.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getConsignmentPoints(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consignmentPoint = new ArrayList();
        consignmentPoint = operationDAO.getConsignmentPoints(operationTO);
        return consignmentPoint;
    }

    public ArrayList getConsignmentPointsSequenceFist(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consignmentPointSequenceFist = new ArrayList();
        consignmentPointSequenceFist = operationDAO.getConsignmentPointsSequenceFist(operationTO);
        return consignmentPointSequenceFist;
    }

    public ArrayList getConsignmentPointsSequenceMiddle(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consignmentPointSequenceMiddle = new ArrayList();
        consignmentPointSequenceMiddle = operationDAO.getConsignmentPointsSequenceMiddle(operationTO);
        return consignmentPointSequenceMiddle;
    }

    public ArrayList getConsignmentPointsSequenceLast(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consignmentPointSequenceLast = new ArrayList();
        consignmentPointSequenceLast = operationDAO.getConsignmentPointsSequenceLast(operationTO);
        return consignmentPointSequenceLast;
    }

    /**
     * This method used to Cancel Consignment Note.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int cancelConsignmentNote(String consignmentOrderId, String consignmentStatusId, String cancelRemarks, int userId) throws FPRuntimeException, FPBusinessException {
        int cancelConsignmentNote = 0;
        cancelConsignmentNote = operationDAO.cancelConsignmentNote(consignmentOrderId, consignmentStatusId, cancelRemarks, userId);
        return cancelConsignmentNote;
    }

    /**
     * This method is used to Vehicle And Driver Planning.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleDriverMapping(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleDriverMapping = new ArrayList();
        vehicleDriverMapping = operationDAO.getVehicleDriverMapping(operationTO);
        return vehicleDriverMapping;
    }

    /**
     * This method used to Get Vehicle Reg No List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleNo(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleNo = new ArrayList();
        vehicleNo = operationDAO.getVehicleNo(operationTO);
        return vehicleNo;
    }

    public ArrayList getVehicleAdvanceVehicleNo(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleNo = new ArrayList();
        vehicleNo = operationDAO.getVehicleAdvanceVehicleNo(operationTO);
        return vehicleNo;
    }

    /**
     * This method used to Get Driver Name List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getDriverName(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList driverName = new ArrayList();
        driverName = operationDAO.getDriverName(operationTO);
        return driverName;
    }

    /**
     * This method used to Insert Vehicle And Driver Mapping.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int insertVehicleDriverMapping(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertVehicleDriverMapping = 0;
        insertVehicleDriverMapping = operationDAO.insertVehicleDriverMapping(operationTO, userId);
        return insertVehicleDriverMapping;
    }

//    Brattle Foods Ends end Arun 7 dec Here
    /**
     * This method used to get zone .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = new ArrayList();
        vehicleList = operationDAO.getVehicleList(operationTO);
        return vehicleList;
    }

    /**
     * This method used to get zone .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleAvailabilityList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleAvailabilityList = new ArrayList();
        vehicleAvailabilityList = operationDAO.getVehicleAvailabilityList(operationTO);
        return vehicleAvailabilityList;
    }
//Senthil start 7 Dec

    /**
     * This method used get closed Trips.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getClosedTrips(String fromDate, String toDate, String custName, int userId, String cancelInvoiceId) throws FPRuntimeException, FPBusinessException {
        ArrayList clisedTrips = null;
        clisedTrips = operationDAO.getClosedTrips(fromDate, toDate, custName, userId, cancelInvoiceId);
        return clisedTrips;
    }

    /**
     * This method used get customer List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    /**
     * This method used get last Invoice Id.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int getLastInvoceId() throws FPRuntimeException, FPBusinessException {
        int lastInvoiceId = 0;
        lastInvoiceId = operationDAO.getLastInvoceId();
        return lastInvoiceId;
    }

    /**
     * This method used get trip deatails.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getCustomerTripDetails(String tripId, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripList = null;
        tripList = operationDAO.getCustomerTripDetails(tripId, userId);
        return tripList;
    }

    /**
     * This method used insert invoice Header
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int insertInvoiceHeader(String customerName, String customerId, String billingType, String fromdate, String todate, int userId) throws FPRuntimeException, FPBusinessException {
        int lastInvoiceId = 0;
        lastInvoiceId = operationDAO.insertInvoiceHeader(customerName, customerId, billingType, fromdate, todate, userId);
        return lastInvoiceId;
    }

    /**
     * This method used insert invoice headerDetails
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int insertInvoiceDetails(String invoiceCode, String tripId, String cnoteNo, String totalweights, String totalweight, String customerName, String custId, String tripDate, String destination, int userId, String invoiceRefCode, int lastinsertId, String freightamt, String cnoteId) throws FPRuntimeException, FPBusinessException {
        int lastInvoiceId = 0;
        lastInvoiceId = operationDAO.insertInvoiceDetails(invoiceCode, tripId, cnoteNo, totalweight, totalweights, customerName, custId, tripDate, destination, userId, invoiceRefCode, lastinsertId, freightamt, cnoteId);
        return lastInvoiceId;
    }

    /**
     * This method used insert invoice headerDetails
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int updateInvoiceHeader(int lastinsertId, String invoiceCode, String invoiceRefCode, double totalamt) throws FPRuntimeException, FPBusinessException {
        int lastInvoiceId = 0;
        lastInvoiceId = operationDAO.updateInvoiceHeader(lastinsertId, invoiceCode, invoiceRefCode, totalamt);
        return lastInvoiceId;
    }

    /**
     * This method used get customerDetails
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public OperationTO handleCustomerDetails(String customerId) throws FPRuntimeException, FPBusinessException {
        OperationTO opto = null;
        opto = operationDAO.handleCustomerDetails(customerId);
        return opto;
    }

    public String getCnoteCodeSequence() {
        String codeSequence = "";
        codeSequence = operationDAO.getCnoteCodeSequence();
        return codeSequence;
    }

    public String getVehicleRegNo(String vehicleId) {
        String result = "";
        result = operationDAO.getVehicleRegNo(vehicleId);
        return result;
    }
//Senthil end 7 Dec
//Arun 11 Dec Start

    public ArrayList getfinanceAdviceDetails(String dateval, String type, String tripType) throws FPBusinessException, FPRuntimeException {
        ArrayList financeAdviceDetails = new ArrayList();
        String IsExistapprove = "";
        financeAdviceDetails = operationDAO.getfinanceAdviceDetails(dateval, type, tripType);

        return financeAdviceDetails;
    }

    public ArrayList viewAdviceDetails(String tripAdvaceId) throws FPBusinessException, FPRuntimeException {
        ArrayList viewAdviceDetails = new ArrayList();
        viewAdviceDetails = operationDAO.viewAdviceDetails(tripAdvaceId);
        return viewAdviceDetails;
    }

    public ArrayList getfinanceAdvice(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        ArrayList financeAdvice = new ArrayList();
        financeAdvice = operationDAO.getfinanceAdvice(operationTO);
        return financeAdvice;
    }

    public ArrayList getfinanceRequest(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        ArrayList financeRequest = new ArrayList();
        financeRequest = operationDAO.getfinanceRequest(operationTO);
        return financeRequest;
    }

    public int addAdvanceRequest(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int fuelPriceMaster = 0;
        fuelPriceMaster = operationDAO.addAdvanceRequest(operationTO, userId);

        return fuelPriceMaster;
    }

    public int saveAdvanceApproval(OperationTO operationTO, int userId, String batchType) throws FPRuntimeException, FPBusinessException {
        int fuelPriceMaster = 0;
        fuelPriceMaster = operationDAO.saveAdvanceApproval(operationTO, userId, batchType);

        return fuelPriceMaster;
    }

    public int saveTripAmount(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int fuelPriceMaster = 0;
        fuelPriceMaster = operationDAO.saveTripAmount(operationTO, userId);

        return fuelPriceMaster;
    }
//Arun 11 Dec End

//Mathan  Starts 19-12-2013
    /**
     * This method used to Save City .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int saveCityMaster(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int saveCityMaster = 0;
        saveCityMaster = operationDAO.saveCityMaster(operationTO, userId);

        return saveCityMaster;
    }

    /**
     * This method used to get zone .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getZoneList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList zoneList = new ArrayList();
        zoneList = operationDAO.getZoneList(operationTO);
        return zoneList;
    }

    /**
     * This method used to get City .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getCityMaster(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList cityMasterList = new ArrayList();
        cityMasterList = operationDAO.getCityMaster(operationTO);
        return cityMasterList;
    }

    /**
     * This method used to update City .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int updateCityMaster(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateCityMaster = 0;
        updateCityMaster = operationDAO.updateCityMaster(operationTO, userId);

        return updateCityMaster;
    }

    /**
     * This method used to Save Check list .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int saveCheckList(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int saveCheckList = 0;
        saveCheckList = operationDAO.saveCheckList(operationTO, userId);

        return saveCheckList;
    }

    /**
     * This method used to Get Check List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getCheckList() throws FPBusinessException, FPRuntimeException {
        ArrayList checkList = new ArrayList();
        checkList = operationDAO.getCheckList();

        return checkList;
    }

    /**
     * This method used to Update Check List .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int updateCheckList(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateCheckList = 0;
        updateCheckList = operationDAO.updateCheckList(operationTO, userId);
        return updateCheckList;
    }

    /**
     * This method used to Get Stage List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getalertfrequency() throws FPBusinessException, FPRuntimeException {
        ArrayList frequencyList = new ArrayList();
        frequencyList = operationDAO.getalertfrequency();

        return frequencyList;
    }

    /**
     * This method used to Save Alert List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int saveAlterList(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int saveAlterList = 0;
        saveAlterList = operationDAO.saveAlterList(operationTO, userId);

        return saveAlterList;
    }

    /**
     * This method used to get AlertList.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getAlertList() throws FPBusinessException, FPRuntimeException {
        ArrayList alertList = new ArrayList();
        alertList = operationDAO.getAlertList();

        return alertList;
    }

    /**
     * This method used to updateAlertList .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int updateAlertList(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateAlertList = 0;
        updateAlertList = operationDAO.updateAlertList(operationTO, userId);
        return updateAlertList;
    }

    /**
     * This method used to Get Stage List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getStageMasterList() throws FPBusinessException, FPRuntimeException {
        ArrayList stageList = new ArrayList();
        stageList = operationDAO.getStageMasterList();

        return stageList;
    }
    //    configdetails

    /**
     * This method used to saveConfigDetails .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int saveConfigDetails(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int saveConfigDetails = 0;
        saveConfigDetails = operationDAO.saveConfigDetails(operationTO, userId);

        return saveConfigDetails;
    }

    /**
     * This method used to Get ProductCategory.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getConfigDetailsLists() throws FPBusinessException, FPRuntimeException {
        ArrayList productCategoryList = new ArrayList();
        productCategoryList = operationDAO.getConfigDetailsLists();

        return productCategoryList;
    }

    /**
     * This method used to updateConfigDetails .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int updateConfigDetails(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateConfigDetails = 0;
        updateConfigDetails = operationDAO.updateConfigDetails(operationTO, userId);
        return updateConfigDetails;
    }

    /**
     * This method used to Save ProductCategory .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int saveProductCategory(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int saveProductCategory = 0;
        saveProductCategory = operationDAO.saveProductCategory(operationTO, userId);

        return saveProductCategory;
    }

    /**
     * This method used to Get ProductCategory.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getProductCategory() throws FPBusinessException, FPRuntimeException {
        ArrayList productCategoryList = new ArrayList();
        productCategoryList = operationDAO.getProductCategory();

        return productCategoryList;
    }

    /**
     * This method used to updateProductCategory .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int updateProductCategory(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateProductCategory = 0;
        updateProductCategory = operationDAO.updateProductCategory(operationTO, userId);
        return updateProductCategory;
    }
    //Mathan  Ends 19-12-2013

    //Senthil Starts 10-12-2013
    /**
     * This method used fetch closed trips
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getClosedTripList(int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList closedList = null;
        closedList = operationDAO.getClosedTripList(userId);
        return closedList;
    }

    /**
     * This method used get closed Trips.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getClosedTrips(String fromDate, String toDate, int userId, String custId, String cancelInvoiceId) throws FPRuntimeException, FPBusinessException {
        ArrayList clisedTrips = null;
        clisedTrips = operationDAO.getClosedTrips(fromDate, toDate, custId, userId, cancelInvoiceId);
        return clisedTrips;
    }

    /**
     * This method used fetch closed trips
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public double getTripExpense(String tripId) throws FPRuntimeException, FPBusinessException {
        double totalExpense = 0.0;
        totalExpense = operationDAO.getTripExpense(tripId);
        return totalExpense;
    }

    /**
     * This method used fetch closed trips
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public double getTotalFreaghtAmt(String cnoteNo) throws FPRuntimeException, FPBusinessException {
        double totalFreight = 0.0;
        totalFreight = operationDAO.getTotalFreaghtAmt(cnoteNo);
        return totalFreight;
    }

    /**
     * This method used fetch closed trips
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int getTotalDrivers(String tripId) throws FPRuntimeException, FPBusinessException {
        int totaldrivers = 0;
        totaldrivers = operationDAO.getTotalDrivers(tripId);
        return totaldrivers;
    }

    /**
     * This method used fetch closed trips
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public OperationTO getTripInvoiceCustomerDetails(String headerTripId) throws FPRuntimeException, FPBusinessException {
        OperationTO opto = null;
        opto = operationDAO.getTripInvoiceCustomerDetails(headerTripId);
        return opto;
    }

    /**
     * This method used fetch closed trips
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getInvoiceCustomerDetails(String headerTripId) throws FPRuntimeException, FPBusinessException {
        ArrayList cnoteList = null;
        cnoteList = operationDAO.getInvoiceCustomerDetails(headerTripId);
        return cnoteList;
    }

    /**
     * This method used change trip status
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int updateTripStatus(String tripId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = operationDAO.updateTripStatus(tripId);
        return status;
    }

    /**
     * This method used get closed bill
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getClosedBilledList(String customerId, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {
        ArrayList closedBill = null;
        closedBill = operationDAO.getClosedBilledList(customerId, fromDate, toDate);
        return closedBill;
    }

    public ArrayList viewBillForSubmission(String customerId, String fromDate, String toDate, String cancelInvoiceId) throws FPRuntimeException, FPBusinessException {
        ArrayList closedBill = null;
        closedBill = operationDAO.viewBillForSubmission(customerId, fromDate, toDate, cancelInvoiceId);
        return closedBill;
    }

    /**
     * This method used invoice header details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public OperationTO getInvoiceHeaderDetails(String invoicecode) throws FPRuntimeException, FPBusinessException {
        OperationTO opto = null;
        opto = operationDAO.getInvoiceHeaderDetails(invoicecode);
        return opto;
    }

    /**
     * This method used invoice headersub details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getInvoiceHeaderSubDeatil(String invoicecode) throws FPRuntimeException, FPBusinessException {
        ArrayList invoicedetailList = null;
        invoicedetailList = operationDAO.getInvoiceHeaderSubDeatil(invoicecode);
        return invoicedetailList;
    }

    /**
     * This method used insert invoice Header
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int insertInvoiceHeader(String customerName, String customerId, String billingType, int userId, String remark, String grandtotal) throws FPRuntimeException, FPBusinessException {
        int lastInvoiceId = 0;
        lastInvoiceId = operationDAO.insertInvoiceHeader(customerName, customerId, billingType, userId, remark, grandtotal);
        return lastInvoiceId;
    }

    public int submitBill(String invoiceId, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = operationDAO.submitBill(invoiceId, userId);
        return status;
    }

    /**
     * This method used insert invoice headerDetails
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int insertInvoiceDetails(String invoiceCode, String tripId, String orderId, String customerName, String custonerId, String tripStartDate, String destination, String invoiceRefCode, String freightamt, String totalweights, int userId) throws FPRuntimeException, FPBusinessException {
        int lastInvoiceId = 0;
        lastInvoiceId = operationDAO.insertInvoiceDetails(invoiceCode, tripId, orderId, customerName, custonerId, tripStartDate, destination, invoiceRefCode, freightamt, totalweights, userId);
        return lastInvoiceId;
    }

    /**
     * This method used insert invoice headerDetails
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int updateInvoiceHeader(int lastinsertId, String invoiceCode, String invoiceRefCode) throws FPRuntimeException, FPBusinessException {
        int lastInvoiceId = 0;
        lastInvoiceId = operationDAO.updateInvoiceHeader(lastinsertId, invoiceCode, invoiceRefCode);
        return lastInvoiceId;
    }

    /**
     * This method used get customer List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getTripCustomerList() throws FPRuntimeException, FPBusinessException {
        ArrayList customerList = null;
        customerList = operationDAO.getTripCustomerList();
        return customerList;
    }

    public int insertTripPlanning(OperationTO opto) {
        int status = 0;
        status = operationDAO.insertTripPlanning(opto);
        return status;
    }

    /**
     * This method used to get vehicle Id
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int handleVehicleId(String vehicleRegNo) {
        int vehicleNo = 0;
        vehicleNo = operationDAO.handleVehicleId(vehicleRegNo);
        return vehicleNo;
    }

    /**
     * // * This method used to get cnote xl details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList handleCnotexlDetail(int userId, String fromDate, String toDate) {
        ArrayList consignmentList = null;
        consignmentList = operationDAO.handleCnotexlDetail(userId, fromDate, toDate);
        return consignmentList;
    }

////senthil 10-12-2013
    public ArrayList gettripClosureRequest(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        ArrayList financeRequest = new ArrayList();
        financeRequest = operationDAO.gettripClosureRequest(operationTO);
        return financeRequest;
    }

    public ArrayList viewTripClosureRequest(String tripid, String tripclosureid) throws FPBusinessException, FPRuntimeException {
        ArrayList viewTripClosureRequest = new ArrayList();
        viewTripClosureRequest = operationDAO.viewTripClosureRequest(tripid, tripclosureid);
        return viewTripClosureRequest;
    }

    public int saveTripClosureapprove(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int saveTripClosureapprove = 0;
        saveTripClosureapprove = operationDAO.saveTripClosureapprove(operationTO, userId);

        return saveTripClosureapprove;
    }

    public String checkCheckListName(OperationTO operationTO) {
        String checkCheckListName = "";
        checkCheckListName = operationDAO.checkCheckListName(operationTO);
        return checkCheckListName;
    }

    public String checkCityName(OperationTO operationTO) {
        String checkCityName = "";
        checkCityName = operationDAO.checkCityName(operationTO);
        return checkCityName;
    }

    public String checkParameterName(OperationTO operationTO) {
        String checkParameterName = "";
        checkParameterName = operationDAO.checkParameterName(operationTO);
        return checkParameterName;
    }

    public String checkproductCategoryName(OperationTO operationTO) {
        String checkproductCategoryName = "";
        checkproductCategoryName = operationDAO.checkproductCategoryName(operationTO);
        return checkproductCategoryName;
    }

    public String checkStandardChargeName(OperationTO operationTO) {
        String checkStandardChargeName = "";
        checkStandardChargeName = operationDAO.checkStandardChargeName(operationTO);
        return checkStandardChargeName;
    }

    public ArrayList getTripStatus() throws FPBusinessException, FPRuntimeException {
        ArrayList statusDetails = new ArrayList();
        statusDetails = operationDAO.getTripStatus();

        return statusDetails;
    }

    public ArrayList getConsignorName(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consignorName = new ArrayList();
        consignorName = operationDAO.getConsignorName(operationTO);
        return consignorName;
    }

    public ArrayList getConsigneeName(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consigneeName = new ArrayList();
        consigneeName = operationDAO.getConsigneeName(operationTO);
        return consigneeName;
    }
//operationBP

    /**
     * This method used to Get User Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getUserDetails(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList userDetails = new ArrayList();
        userDetails = operationDAO.getUserDetails(operationTO);
        return userDetails;
    }

    /**
     * This method used to Get User Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getCustomerList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList customerList = new ArrayList();
        customerList = operationDAO.getCustomerList(operationTO);
        return customerList;
    }

    /**
     * This method used to Get User Customer.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getUserCustomer(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList userCustomer = new ArrayList();
        userCustomer = operationDAO.getUserCustomer(operationTO);
        return userCustomer;
    }

    /**
     * This method used to Delete User Customer.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int deleteUserCustomer(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int delete = 0;
        delete = operationDAO.deleteUserCustomer(operationTO, userId);
        return delete;
    }

    /**
     * This method used to Save User Customer.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int saveUserCustomer(String[] customerId, OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int delete = 0;
        delete = operationDAO.saveUserCustomer(customerId, operationTO, userId);
        return delete;
    }

    public ArrayList uploadTripPlanning(String filePath, int userId) throws IOException, FPRuntimeException, FPBusinessException, BiffException {
        String markDetails[] = new String[10];
        File fl = null;
        ArrayList tripPlanningList = new ArrayList();
        OperationTO operationTO = new OperationTO();
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        DataInputStream dis = null;
        ArrayList tripPlanList = new ArrayList();
        String fileName = "";
        boolean fileExists = true;
        String[] fileType = null;
        String[] vehId = new String[200];
        String[] cNoteId = new String[200];
        int t = 0;
        fileName = filePath;
        System.out.println(fileName);
        try {
            fl = new File(fileName);
            fis = new FileInputStream(fl);
            bis = new BufferedInputStream(fis);
            dis = new DataInputStream(bis);
            fileExists = true;
        } catch (FileNotFoundException fnfexcp) {
            //fnfexcp.printStackTrace();
            System.out.println("FNF");
            fileExists = false;
        }
        if (fileExists) {
            //first record need not be processed
            if (fileName.contains(".xls")) {
                System.out.println("i am in excel sheet upload = ");
                WorkbookSettings ws = new WorkbookSettings();
                try {
                    ws.setLocale(new Locale("en", "EN"));
                    System.out.println("ws = " + ws);
                    Workbook workbook = Workbook.getWorkbook(new File(fileName), ws);
                    Sheet s = workbook.getSheet(0);

                    System.out.println("rows" + s.getRows());
                    //gets the value of cell at specified column and row
                    String[] consignmentNo = new String[200];
                    String[] vehicleNo = new String[200];
                    int r = 0;
                    int m = 0;
                    int a = 0;
                    int k = 0;
                    for (int j = 2; j < s.getRows(); j++) {
                        for (int i = 0; i < s.getColumns(); i++) {
                            System.out.println("i = " + i + "j = " + j);
                            Cell val = s.getCell(i, j);
                            System.out.println(val.getContents());
                            if (i == 1) {
                                consignmentNo[r++] = val.getContents();
                            } else if (i == 12) {
                                vehicleNo[m++] = val.getContents();
                            }

                        }
                    }
                    int p = 0;

                    System.out.println("consignmentNo.length = " + consignmentNo.length);
                    int x = 0, y = 0;
                    while (t < consignmentNo.length) {
                        if (consignmentNo[t] != null) {
                            operationTO = new OperationTO();
                            String consignmentId = operationDAO.getConsignmentOrderId(consignmentNo[t]);
                            if (consignmentId != null) {
                                String consignmentStatus = operationDAO.getConsignmentStatus(consignmentId);
                                if (consignmentStatus != null) {
                                    operationTO.setConsignmentOrderNo(consignmentNo[t]);
                                    operationTO.setConsignmentOrderId("0");
                                    operationTO.setConsignmentStatus("Trip Generated");
                                } else {
                                    operationTO.setConsignmentOrderNo(consignmentNo[t]);
                                    operationTO.setConsignmentOrderId(consignmentId);
                                    operationTO.setConsignmentStatus("OK");
                                    String consignmentTripPlanStatus = operationDAO.getConsignmentTripPlanStatus(consignmentId);
                                    if (consignmentTripPlanStatus != null) {
                                        operationTO.setConsignmentOrderNo(consignmentNo[t]);
                                        operationTO.setConsignmentOrderId(consignmentId);
                                        operationTO.setConsignmentStatus("Trip Already Planned");
                                    } else {
                                        operationTO.setConsignmentOrderNo(consignmentNo[t]);
                                        operationTO.setConsignmentOrderId(consignmentId);
                                        operationTO.setConsignmentStatus("OK");
                                    }
                                }

                            } else {
                                operationTO.setConsignmentOrderNo(consignmentNo[t]);
                                operationTO.setConsignmentOrderId("0");
                                operationTO.setConsignmentStatus("Invalid Consignment No");
                            }
                            System.out.println("consignmentId = " + consignmentId);
                            cNoteId[t] = consignmentId;
                            if (vehicleNo[t] != null) {
                                String vehicleId = operationDAO.getTripPlanVehicleId(vehicleNo[t]);
                                if (vehicleId != null) {
                                    operationTO.setVehicleNo(vehicleNo[t]);
                                    operationTO.setConsignmentVehicleId(vehicleId);
                                    operationTO.setConsignmentVehicleStatus("OK");
                                    String vehicleTripPlanStatus = operationDAO.getVehicleTripPlanStatus(consignmentId, vehicleId);
                                    if (vehicleTripPlanStatus != null) {
                                        operationTO.setVehicleNo(vehicleNo[t]);
                                        operationTO.setConsignmentVehicleId(vehicleId);
                                        operationTO.setConsignmentVehicleStatus("Trip Already Planned");
                                    }
                                } else {
                                    operationTO.setVehicleNo(vehicleNo[t]);
                                    operationTO.setConsignmentVehicleId("0");
                                    operationTO.setConsignmentVehicleStatus("Invalid Vehicle No");
                                }
                                System.out.println("vehicleId = " + vehicleId);
                                vehId[t] = vehicleId;
                            } else {
                                vehId[t] = "0";
                            }
                            t++;
                            tripPlanList.add(operationTO);
                        } else {
                            break;
                        }
                    }
                    workbook.close();
                } catch (Exception BiffException) {
                    System.out.println("BiffException = " + BiffException.getMessage());
                }
            } else {
                //
            }
        }
        //status = saveUploadTripPlanning(cNoteId,vehId,userId,t);
        return tripPlanList;
    }

    public int saveUploadTripPlanning(String[] cNoteId, String[] vehId, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = operationDAO.insertUploadTripPlanning(cNoteId, vehId, userId);
        return insertStatus;
    }

    /**
     * This method used to Get Consignment List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getTripPlannedVehicleList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripPlannedVehicleList = new ArrayList();
        tripPlannedVehicleList = operationDAO.getTripPlannedVehicleList(operationTO);
        return tripPlannedVehicleList;
    }

//////////////////////senthil starts at 17-12-2013///////////////
    public ArrayList checkCustomerAvailability(String customerName, String customerCode) throws FPRuntimeException, FPBusinessException {
        ArrayList customerList = null;
        customerList = operationDAO.checkCustomerAvailability(customerName, customerCode);

        return customerList;
    }

    public ArrayList checkConsignmentVehicleType(String vehicleType) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = null;
        vehicleList = operationDAO.checkConsignmentVehicleType(vehicleType);
        return vehicleList;
    }

    public String getConsignmentCity(String cityName) throws FPRuntimeException, FPBusinessException {
        String cityId = "";
        cityId = operationDAO.getConsignmentCity(cityName);
        return cityId;
    }

    public String getEmailToList(String custId) throws FPRuntimeException, FPBusinessException {
        String result = "";
        result = operationDAO.getEmailToList(custId);
        return result;
    }

    public String getEmailToListForConsignment(String custId) throws FPRuntimeException, FPBusinessException {
        String result = "";
        result = operationDAO.getEmailToListForConsignment(custId);
        return result;
    }

    public String getConfirmationMail(String confirmationCode) throws FPRuntimeException, FPBusinessException {
        String result = "";
        result = operationDAO.getConfirmationMail(confirmationCode);
        return result;
    }

    public String handleConsignRoute(String fromCityId, String toCityId) throws FPRuntimeException, FPBusinessException {
        String routeId = "";
        routeId = operationDAO.handleConsignRoute(fromCityId, toCityId);
        return routeId;
    }

    public ArrayList checkProductCategory(String prodCategory) throws FPRuntimeException, FPBusinessException {
        ArrayList opto = null;
        opto = operationDAO.checkProductCategory(prodCategory);
        return opto;
    }

    public OperationTO handleRouteContractDetails(String firstPickupId, String point1RouteId, String point2RouteId, String point3RouteId, String finalpointId) throws FPRuntimeException, FPBusinessException {
        OperationTO opto = null;
        opto = operationDAO.handleRouteContractDetails(firstPickupId, point1RouteId, point2RouteId, point3RouteId, finalpointId);
        return opto;
    }

    public OperationTO handleRouteDetailsNew(String firstPickupId, String point1RouteId, String point2RouteId, String point3RouteId, String finalpointId) throws FPRuntimeException, FPBusinessException {
        OperationTO opto = null;
        opto = operationDAO.handleRouteDetailsNew(firstPickupId, point1RouteId, point2RouteId, point3RouteId, finalpointId);
        return opto;
    }

    public OperationTO handleRouteContractDetailsNew(String originId, String point1Id, String point2Id, String point3Id, String point4Id,
            String finalpointId, String contractId) throws FPRuntimeException, FPBusinessException {
        OperationTO opto = null;
        opto = operationDAO.handleRouteContractDetailsNew(originId, point1Id, point2Id, point3Id, point4Id, finalpointId, contractId);
        return opto;
    }

    public OperationTO handleContractRateDetails(String vehicleTypeId, int contractId, String routeContractId) throws FPRuntimeException, FPBusinessException {
        OperationTO opto = null;
        opto = operationDAO.handleContractRateDetails(vehicleTypeId, contractId, routeContractId);
        return opto;
    }
    //////////////////////senthil starts at 17-12-2013///////////////
    //////////////////////senthil starts at 20-12-2013///////////////

    public int saveImportConsignmentNote(OperationTO opto, int userId) throws FPRuntimeException, FPBusinessException {
        int cnoteId = 0;
        cnoteId = operationDAO.saveImportConsignmentNote(opto, userId);
        return cnoteId;
    }

    public int checkCustomerOrdRefNo(String orderNo) {
        int status = 0;
        status = operationDAO.checkCustomerOrdRefNo(orderNo);
        return status;
    }
 
    public int updateCustomerContract(String routeContractId, String contractRateId, String ptpRateWithReefer, String ptpRateWithoutReefer, String validStatus, String tatTime) {
        int status = 0;
        status = operationDAO.updateCustomerContract(routeContractId, contractRateId, ptpRateWithReefer, ptpRateWithoutReefer, validStatus, tatTime);
        return status;
    }

    public int updateCustomerContractFixedKm(String routeContractId, String contractRateId, String ptpRateWithReefer, String ptpRateWithoutReefer, String validStatus) {
        int status = 0;
        status = operationDAO.updateCustomerContract(routeContractId, contractRateId, ptpRateWithReefer, ptpRateWithoutReefer, validStatus, "0");
        return status;
    }

    public int updateCustomerContract1(String routeContractId, String contractRateId, String ptpRateWithReefer, String ptpRateWithoutReefer, String validStatus, String minWgtBase) {
        int status = 0;
        status = operationDAO.updateCustomerContract1(routeContractId, contractRateId, ptpRateWithReefer, ptpRateWithoutReefer, validStatus, minWgtBase);
        return status;
    }

    public int updateCustomerContract2(String contractRateId, String vehicleRatePerKm, String reeferRatePerHour, String validStatus) {
        int status = 0;
        status = operationDAO.updateCustomerContract2(contractRateId, vehicleRatePerKm, reeferRatePerHour, validStatus);
        return status;
    }

    public int createConsignmentRoute(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        int sequence = 1;
        String pointId = "";
        String pointType = "";
        String pointPlanDate = "";
        String pointPlanTime = "";
        String pointSequence = "";
        String consigmentId = operationTO.getConsigmentId();

        //origin
        pointId = operationTO.getFromLocation();
        pointType = "Pick Up";
        pointPlanDate = operationTO.getPickupdate();
        pointPlanTime = operationTO.getPickupTime();
        pointSequence = sequence + "";
        insertStatus = operationDAO.insertConsignmentRoutePlan(consigmentId, pointId, pointType, pointSequence, pointPlanDate, pointPlanTime, userId);

        //interim point 1
        if (!"".equals(operationTO.getPoint1Id())) {
            sequence++;
            pointId = operationTO.getPoint1Id();
            pointType = operationTO.getPoint1Type();
            pointPlanDate = "";
            pointPlanTime = "00:00:00";
            pointSequence = sequence + "";
            insertStatus = operationDAO.insertConsignmentRoutePlan(consigmentId, pointId, pointType, pointSequence, pointPlanDate, pointPlanTime, userId);
        }

        //interim point 2
        if (!"".equals(operationTO.getPoint2Id())) {
            sequence++;
            pointId = operationTO.getPoint2Id();
            pointType = operationTO.getPoint2Type();
            pointPlanDate = "";
            pointPlanTime = "00:00:00";
            pointSequence = sequence + "";
            insertStatus = operationDAO.insertConsignmentRoutePlan(consigmentId, pointId, pointType, pointSequence, pointPlanDate, pointPlanTime, userId);
        }
        //interim point 3
        if (!"".equals(operationTO.getPoint3Id())) {
            sequence++;
            pointId = operationTO.getPoint3Id();
            pointType = operationTO.getPoint3Type();
            pointPlanDate = "";
            pointPlanTime = "00:00:00";
            pointSequence = sequence + "";
            insertStatus = operationDAO.insertConsignmentRoutePlan(consigmentId, pointId, pointType, pointSequence, pointPlanDate, pointPlanTime, userId);
        }
        //interim point 4
        if (!"".equals(operationTO.getPoint4Id())) {
            sequence++;
            pointId = operationTO.getPoint4Id();
            pointType = operationTO.getPoint4Type();
            pointPlanDate = "";
            pointPlanTime = "00:00:00";
            pointSequence = sequence + "";
            insertStatus = operationDAO.insertConsignmentRoutePlan(consigmentId, pointId, pointType, pointSequence, pointPlanDate, pointPlanTime, userId);
        }
        //destination
        sequence++;
        pointId = operationTO.getToLocation();
        pointType = "Drop";
        pointPlanDate = "";
        pointPlanTime = "00:00:00";
        pointSequence = sequence + "";
        insertStatus = operationDAO.insertConsignmentRoutePlan(consigmentId, pointId, pointType, pointSequence, pointPlanDate, pointPlanTime, userId);

        return insertStatus;
    }
    //////////////////////senthil ends at 20-12-2013///////////////

    public ArrayList getEmailDetails(String activitycode) throws FPRuntimeException, FPBusinessException {
        ArrayList EmailDetails = new ArrayList();
        EmailDetails = operationDAO.getEmailDetails(activitycode);
        return EmailDetails;
    }

    public int manualAdvanceRequest(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int manualAdvanceRequest = 0;
        manualAdvanceRequest = operationDAO.manualAdvanceRequest(operationTO, userId);
        return manualAdvanceRequest;
    }

    public int payManualAdvanceRequest(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int manualAdvanceRequest = 0;
        manualAdvanceRequest = operationDAO.payManualAdvanceRequest(operationTO, userId);
        return manualAdvanceRequest;
    }

    public ArrayList getmanualfinanceAdvDetails(String tripid) throws FPRuntimeException, FPBusinessException {
        ArrayList manualfinanceAdvDetails = new ArrayList();
        manualfinanceAdvDetails = operationDAO.getmanualfinanceAdvDetails(tripid);
        return manualfinanceAdvDetails;
    }

    public double getPreStartExpense(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        double preStartExpense = 0;
        preStartExpense = operationDAO.getPreStartExpense(operationTO, userId);
        return preStartExpense;
    }

    public double getfcRequestamount(int reqid) throws FPRuntimeException, FPBusinessException {
        double fcRequestamount = 0;
        fcRequestamount = operationDAO.getfcRequestamount(reqid);
        return fcRequestamount;
    }

    public double gettotalReqamount(String tripid, String advdate) throws FPRuntimeException, FPBusinessException {
        double totalReqamount = 0;
        totalReqamount = operationDAO.gettotalReqamount(tripid, advdate);
        return totalReqamount;
    }

    public String getTripStatusAndWFUDay(String tripid) throws FPRuntimeException, FPBusinessException {
        String result = "";
        result = operationDAO.getTripStatusAndWFUDay(tripid);
        return result;
    }

    public ArrayList getapprovalValueDetails(double actualval, String tripId) throws FPRuntimeException, FPBusinessException {
        ArrayList getapprovalValueDetails = new ArrayList();
        getapprovalValueDetails = operationDAO.getapprovalValueDetails(actualval, tripId);
        return getapprovalValueDetails;
    }

    public ArrayList getTripDeviationDetails(String tripId, int manualAdvanceRequest) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDeviationDetails = new ArrayList();
        tripDeviationDetails = operationDAO.getTripDeviationDetails(tripId, manualAdvanceRequest);
        return tripDeviationDetails;
    }

    public int manualAdvanceApprove(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int manualAdvanceApprove = 0;
        manualAdvanceApprove = operationDAO.manualAdvanceApprove(operationTO, userId);
        return manualAdvanceApprove;
    }

    public ArrayList getapprovalFCValueDetails() throws FPRuntimeException, FPBusinessException {
        ArrayList getapprovalFCValueDetails = new ArrayList();
        getapprovalFCValueDetails = operationDAO.getapprovalFCValueDetails();
        return getapprovalFCValueDetails;
    }

    public int saveManualAdvanceApproval(OperationTO operationTO, int userId, String advid) throws FPRuntimeException, FPBusinessException {
        int saveManualAdvanceApproval = 0;
        saveManualAdvanceApproval = operationDAO.saveManualAdvanceApproval(operationTO, userId, advid);

        return saveManualAdvanceApproval;
    }

    public int getManualAdvanceApprovalStatus(String advid) throws FPRuntimeException, FPBusinessException {
        int approvalValue = 0;
        approvalValue = operationDAO.getManualAdvanceApprovalStatus(advid);

        return approvalValue;
    }

    public int updateConsignmentDetails(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 0;
        updateStatus = operationDAO.updateConsignmentDetails(operationTO, userId);
        return updateStatus;
    }

    public int updateConsignmentArticles(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 0;
        updateStatus = operationDAO.updateConsignmentArticles(operationTO, userId);
        return updateStatus;
    }

    public int deleteMultiplePoints(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = operationDAO.deleteMultiplePoints(operationTO, userId);
        return insertStatus;
    }

    public double gettotalPaidamount(String tripid, String advdate) throws FPRuntimeException, FPBusinessException {
        double totalPaidamount = 0;
        totalPaidamount = operationDAO.gettotalPaidamount(tripid, advdate);
        return totalPaidamount;
    }

    public double getovertotalPaidamount(String tripid) throws FPRuntimeException, FPBusinessException {
        double getovertotalPaidamount = 0;
        getovertotalPaidamount = operationDAO.getovertotalPaidamount(tripid);
        return getovertotalPaidamount;
    }

    public double getEstimatedamount(String tripid, String advdate) throws FPRuntimeException, FPBusinessException {
        double getEstimatedamount = 0;
        getEstimatedamount = operationDAO.getEstimatedamount(tripid, advdate);
        return getEstimatedamount;
    }

    public ArrayList getInvoiceDetails(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceDetails = new ArrayList();
        invoiceDetails = operationDAO.getInvoiceDetails(operationTO);
        return invoiceDetails;
    }

    public ArrayList processGetCompanyList() throws FPBusinessException, FPRuntimeException {
        ArrayList companyList = new ArrayList();
        companyList = operationDAO.getCompanyList();
        return companyList;
    }

    public ArrayList getCityNameList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList cityList = new ArrayList();
        cityList = operationDAO.getCityNameList(operationTO);
        return cityList;
    }

    public int updateCustomerDetails(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 0;
        updateStatus = operationDAO.updateCustomerDetails(operationTO, userId);
        return updateStatus;
    }

    public String getTripId(String consignmentOrderId) {
        String tripId = "";
        tripId = operationDAO.getTripId(consignmentOrderId);
        return tripId;
    }

    public int updateTripMaster(OperationTO operationTO, String tripId, int userId) throws FPRuntimeException, FPBusinessException {
        int updateTripMaster = 0;
        updateTripMaster = operationDAO.updateTripMaster(operationTO, tripId, userId);
        return updateTripMaster;
    }

    public int deleteTripPoints(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int deleteStatus = 0;
        deleteStatus = operationDAO.deleteTripPoints(operationTO, userId);
        return deleteStatus;
    }

    public int insertTripPoints(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = operationDAO.insertTripPoints(operationTO, userId);
        return insertStatus;
    }

    public int updateTripConsignmentArticles(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 0;
        updateStatus = operationDAO.updateTripConsignmentArticles(operationTO, userId);
        return updateStatus;
    }

    /**
     * This method used to insertcontractProductDetails .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertcontractProductDetails(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = operationDAO.insertcontractProductDetails(operationTO, userId);
        return insertStatus;
    }

    /**
     * This method used to getCustomerProductCategoryList .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getCustomerProductCategoryList(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        ArrayList customerProductCategoryList = new ArrayList();
        customerProductCategoryList = operationDAO.getCustomerProductCategoryList(operationTO);
        return customerProductCategoryList;
    }

    /**
     * This method used to deleteCustomerProduct .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int deleteCustomerProduct(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 0;
        //updateStatus = operationDAO.deleteCustomerProduct(operationTO, userId);
        return updateStatus;
    }

    public String getRouteId(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        String routeId = "";
        routeId = operationDAO.getRouteId(operationTO);
        return routeId;
    }

    public ArrayList getapprovalEmptyTripDetails(double totalKM) throws FPBusinessException, FPRuntimeException {
        ArrayList emptyTripDetails = new ArrayList();
        emptyTripDetails = operationDAO.getapprovalEmptyTripDetails(totalKM);

        return emptyTripDetails;
    }

    public int insertConsignmentNoteForEmptyTrip(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = operationDAO.insertConsignmentNoteForEmptyTrip(operationTO, userId);
        return insertStatus;
    }

    public ArrayList getVehicleBPCLCardMapping(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleBPCLCardMapping = new ArrayList();
        vehicleBPCLCardMapping = operationDAO.getVehicleBPCLCardMapping(operationTO);
        return vehicleBPCLCardMapping;
    }

    public ArrayList getVehicleNoBPCLCardNo(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleNoBPCLCardNo = new ArrayList();
        vehicleNoBPCLCardNo = operationDAO.getVehicleNoBPCLCardNo(operationTO);
        return vehicleNoBPCLCardNo;
    }

    public int insertVehicleBPCLCardMapping(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertVehicleBPCLCardMapping = 0;
        insertVehicleBPCLCardMapping = operationDAO.insertVehicleBPCLCardMapping(operationTO, userId);
        return insertVehicleBPCLCardMapping;
    }

    public ArrayList getPaymentMode() throws FPRuntimeException, FPBusinessException {
        ArrayList paymentMode = new ArrayList();
        paymentMode = operationDAO.getPaymentMode();
        return paymentMode;
    }

    public int insertPaymentDetails(OperationTO operationTO, int userId) {
        int status = 0;
        status = operationDAO.insertPaymentDetails(operationTO, userId);
        return status;
    }

    public ArrayList getPaymentDetails(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList paymentDetails = new ArrayList();
        paymentDetails = operationDAO.getPaymentDetails(operationTO);
        return paymentDetails;
    }

    public int updateRequestDetails(OperationTO operationTO, int userId) {
        int status = 0;
        status = operationDAO.updateRequestDetails(operationTO, userId);
        return status;
    }

    public double totalConsignmentPaid(OperationTO operationTO, int userId) {
        double paidAmount = 0;
        paidAmount = operationDAO.totalConsignmentPaid(operationTO, userId);
        return paidAmount;
    }

    public double totalAmountWaitingForApproval(OperationTO operationTO, int userId) {
        double totalAmountWaitingForApproval = 0;
        totalAmountWaitingForApproval = operationDAO.totalAmountWaitingForApproval(operationTO, userId);
        return totalAmountWaitingForApproval;
    }

    public ArrayList getVehicleDriverMappingForVehicleId(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleDriverMapping = new ArrayList();
        vehicleDriverMapping = operationDAO.getVehicleDriverMappingForVehicleId(operationTO);
        return vehicleDriverMapping;
    }

    public ArrayList getFuleType() throws FPBusinessException, FPRuntimeException {
        ArrayList fuleTypeList = new ArrayList();
        fuleTypeList = operationDAO.getFuleType();
        return fuleTypeList;
    }

    public ArrayList getVehicleRegNoForJobCard() throws FPBusinessException, FPRuntimeException {
        ArrayList vehicleRegNoForJobCard = new ArrayList();
        vehicleRegNoForJobCard = operationDAO.getVehicleRegNoForJobCard();
        return vehicleRegNoForJobCard;
    }

    public ArrayList getVehicleRegNoForWorkOrder() throws FPBusinessException, FPRuntimeException {
        ArrayList vehicleRegNoForWorkOrder = new ArrayList();
        vehicleRegNoForWorkOrder = operationDAO.getVehicleRegNoForWorkOrder();
        return vehicleRegNoForWorkOrder;
    }

    public ArrayList getVehicleRegNoForJobCardList(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        ArrayList vehicleRegNoForJobCard = new ArrayList();
        vehicleRegNoForJobCard = operationDAO.getVehicleRegNoForJobCardList(operationTO);
        return vehicleRegNoForJobCard;
    }

    public ArrayList getVehicleRegNoForMerging(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        ArrayList vehicleRegNoForMerging = new ArrayList();
        vehicleRegNoForMerging = operationDAO.getVehicleRegNoForMerging(operationTO);
        return vehicleRegNoForMerging;
    }

    public ArrayList getJobCardDetails(int jobcardId) throws FPBusinessException, FPRuntimeException {
        ArrayList jobCardList = new ArrayList();
        jobCardList = operationDAO.getJobCardDetails(jobcardId);
        return jobCardList;
    }

    public ArrayList getClosedJobCardDetails(int jobcardId) throws FPBusinessException, FPRuntimeException {
        ArrayList jobCardList = new ArrayList();
        jobCardList = operationDAO.getClosedJobCardDetails(jobcardId);
        return jobCardList;
    }

    public String getJobcardEmail() throws FPBusinessException, FPRuntimeException {
        String jobCardMailList = "";
        jobCardMailList = operationDAO.getJobcardEmail();
        return jobCardMailList;
    }

    public ArrayList getPrimaryDriverName(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList driverName = new ArrayList();
        driverName = operationDAO.getPrimaryDriverName(operationTO);
        return driverName;
    }

    public ArrayList getServiceTypeSection(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList serviceTypeSection = new ArrayList();
        serviceTypeSection = operationDAO.getServiceTypeSection(operationTO);
        return serviceTypeSection;
    }

    public String getTripTypeId(String tripid) throws FPRuntimeException, FPBusinessException {
        String result = "";
        result = operationDAO.getTripTypeId(tripid);
        return result;
    }

    public String getFuelTypeId(String tripid) throws FPRuntimeException, FPBusinessException {
        String result = "";
        result = operationDAO.getFuelTypeId(tripid);
        return result;
    }

    public String getSecondaryApprovalPerson(String tripId) throws FPRuntimeException, FPBusinessException {
        String approvalPersonMail = "";
        approvalPersonMail = operationDAO.getSecondaryApprovalPerson(tripId);
        return approvalPersonMail;
    }

    public String getSecondaryApprovalPersonForEmpty(String tripId) throws FPRuntimeException, FPBusinessException {
        String approvalPersonMail = "";
        approvalPersonMail = operationDAO.getSecondaryApprovalPersonForEmpty(tripId);
        return approvalPersonMail;
    }

    public ArrayList getCreditLimitEmailDetails(String activitycode) throws FPRuntimeException, FPBusinessException {
        ArrayList creditLimitEmailDetails = new ArrayList();
        creditLimitEmailDetails = operationDAO.getCreditLimitEmailDetails(activitycode);
        return creditLimitEmailDetails;
    }

    public int updateConsignmentApprovalMailCount(OperationTO operationTO, int count) {
        int status = 0;
        status = operationDAO.updateConsignmentApprovalMailCount(operationTO, count);
        return status;
    }

    public String getSecondaryRouteApprovalPerson(String customerId) throws FPRuntimeException, FPBusinessException {
        String approvalPersonMail = "";
        approvalPersonMail = operationDAO.getSecondaryRouteApprovalPerson(customerId);
        return approvalPersonMail;
    }

    public String getSecondaryEmptyTripApprovalPerson(String vehicleId) throws FPRuntimeException, FPBusinessException {
        String approvalPersonMail = "";
        approvalPersonMail = operationDAO.getSecondaryEmptyTripApprovalPerson(vehicleId);
        return approvalPersonMail;
    }

    public String getSecondaryFcPerson(String vehicleId) throws FPRuntimeException, FPBusinessException {
        String approvalPersonMail = "";
        approvalPersonMail = operationDAO.getSecondaryFcPerson(vehicleId);
        return approvalPersonMail;
    }

    public int updateConteCount(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = operationDAO.updateConteCount(operationTO, userId);
        return insertStatus;
    }

    public ArrayList getVehicleRegNoForTyer() throws FPBusinessException, FPRuntimeException {
        ArrayList vehicleRegNoForJobCard = new ArrayList();
        vehicleRegNoForJobCard = operationDAO.getVehicleRegNoForTyer();
        return vehicleRegNoForJobCard;
    }
//    <GST>

    public int saveGSTCategoryMaster(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int saveGSTCategoryMaster = 0;
        saveGSTCategoryMaster = operationDAO.saveGSTCategoryMaster(operationTO, userId);
        return saveGSTCategoryMaster;
    }

    public ArrayList getGSTCategoryMasterList() throws FPRuntimeException, FPBusinessException {
        ArrayList GSTCategoryMaster = new ArrayList();
        GSTCategoryMaster = operationDAO.getGSTCategoryMasterList();
        return GSTCategoryMaster;
    }

    public int updateGSTCategoryMaster(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateGSTCategoryMaster = 0;
        updateGSTCategoryMaster = operationDAO.updateGSTCategoryMaster(operationTO, userId);
        return updateGSTCategoryMaster;
    }

    public ArrayList getCategoryName() throws FPRuntimeException, FPBusinessException {
        ArrayList getCategoryName = new ArrayList();
        getCategoryName = operationDAO.getCategoryName();
        return getCategoryName;
    }

    public int updateCategoryMaster(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateCategoryMaster = 0;
        updateCategoryMaster = operationDAO.updateCategoryMaster(operationTO, userId);
        return updateCategoryMaster;
    }

    public ArrayList getCategoryMasterList() throws FPRuntimeException, FPBusinessException {
        ArrayList getCategoryMasterList = new ArrayList();
        getCategoryMasterList = operationDAO.getCategoryMasterList();
        return getCategoryMasterList;
    }

    public int saveCategoryMaster(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int saveCategoryMaster = 0;
        saveCategoryMaster = operationDAO.saveCategoryMaster(operationTO, userId);

        return saveCategoryMaster;
    }

    public ArrayList gstCode() throws FPRuntimeException, FPBusinessException {
        ArrayList gstCode = new ArrayList();
        gstCode = operationDAO.gstCode();
        return gstCode;
    }

    public int saveRateMaster(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        int saveRateMaster = 0;
        saveRateMaster = operationDAO.saveRateMaster(operationTO);
        return saveRateMaster;
    }

    public int saveRateDetails(OperationTO operationTO, String gstCode, String gstPercentage, String validFrom, String validTo, int userId, int gstRateId) throws FPRuntimeException, FPBusinessException {
        int saveRateMaster = 0;
        saveRateMaster = operationDAO.saveRateDetails(operationTO, gstCode, gstPercentage, validFrom, validTo, userId, gstRateId);
        return saveRateMaster;
    }

    public ArrayList getRateMasterList() throws FPRuntimeException, FPBusinessException {
        ArrayList getRateMasterList = new ArrayList();
        getRateMasterList = operationDAO.getRateMasterList();
        return getRateMasterList;
    }

    public ArrayList gstRatemaster() throws FPRuntimeException, FPBusinessException {
        ArrayList gstRatemaster = new ArrayList();
        gstRatemaster = operationDAO.gstRatemaster();
        return gstRatemaster;
    }

    public ArrayList getRateMasterDetails(int gstRateId) throws FPRuntimeException, FPBusinessException {
        ArrayList getRateMasterDetails = new ArrayList();
        getRateMasterDetails = operationDAO.getRateMasterDetails(gstRateId);
        return getRateMasterDetails;
    }

    public int updateRateMaster(OperationTO operationTO, String gstCode, String gstPercentage, String validFrom, String validTo, int userId, String gstRateDetailId, int gstRateId) throws FPRuntimeException, FPBusinessException {
        int updateRateMaster = 0;
        updateRateMaster = operationDAO.updateRateMaster(operationTO, gstCode, gstPercentage, validFrom, validTo, userId, gstRateDetailId, gstRateId);
        return updateRateMaster;
    }

    public int saveProductCategoryList(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int saveProductCategoryList = 0;
        saveProductCategoryList = operationDAO.saveProductCategoryList(operationTO, userId);
        return saveProductCategoryList;
    }

    public ArrayList getProductCategoryDetails() throws FPRuntimeException, FPBusinessException {
        ArrayList getProductCategoryDetails = new ArrayList();
        getProductCategoryDetails = operationDAO.getProductCategoryDetails();
        return getProductCategoryDetails;
    }

    public int updateProductCategoryList(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateProductCategoryList = 0;
        updateProductCategoryList = operationDAO.updateProductCategoryList(operationTO, userId);
        return updateProductCategoryList;
    }

    public ArrayList getProductName() throws FPRuntimeException, FPBusinessException {
        ArrayList getProductName = new ArrayList();
        getProductName = operationDAO.getProductName();
        return getProductName;
    }

    public ArrayList getAllCategoryName() throws FPRuntimeException, FPBusinessException {
        ArrayList getAllCategoryName = new ArrayList();
        getAllCategoryName = operationDAO.getAllCategoryName();
        return getAllCategoryName;
    }

    public int checkRateMaster(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        int checkRateMaster = 0;
        checkRateMaster = operationDAO.checkRateMaster(operationTO);
        return checkRateMaster;
    }
//        CBT

    public ArrayList getTodayBookingDetails(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList todayBookingDetails = new ArrayList();
        todayBookingDetails = operationDAO.getTodayBookingDetails(operationTO);
        return todayBookingDetails;
    }

    public int insertEditConsignmentNote(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        int checkAwbStatus = 0;
        int insertStatusDeatils = 0;
        SqlMapClient session = operationDAO.getSqlMapClient();
        boolean sts = true;
        String awbNo = "";
        boolean transactionCommitStatus = false;
        try {
            session.startTransaction();
            if (operationTO.getAwbType().equals("1")) {
                checkAwbStatus = operationDAO.getAwbNoCheckExists(operationTO, userId, session);
                if (checkAwbStatus == 0 && operationTO.getShipmentType().equals("1")) {
                    sts = true;
                }
                if (checkAwbStatus >= 0 && operationTO.getShipmentType().equals("2")) {
                    sts = true;
                }
            }
            if (sts) {
                insertStatus = operationDAO.insertEditConsignmentNote(operationTO, userId, session);
                operationTO.setConsignmentOrderId(String.valueOf(insertStatus));
                if (insertStatus > 0) {
                    insertStatus = operationDAO.insertEditConsignmentArticle(operationTO, userId, session);
                    insertStatus = operationDAO.insertEditMultiplePoints(operationTO, userId, session);
                    if (insertStatus == 0) {
                        insertStatus = 1;
                        transactionCommitStatus = true;
                    } else {
                        // Choosen Truck has already used, So that your booking not aloowed 
                        insertStatus = 3;
                    }
                }
            } else {
                insertStatus = 2;
            }
            if (transactionCommitStatus) {
                insertStatus = 1;
                session.commitTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
            insertStatus = 0;
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                insertStatus = 0;
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return insertStatus;
    }

    public ArrayList getContractVehicleTypeForPointsList(int contractId,
            String firstPointId, String point1Id, String point2Id, String point3Id, String point4Id, String finalPointId) throws FPRuntimeException, FPBusinessException {
        ArrayList routeVehicleTypeList = new ArrayList();
        int counter = 0;
        OperationTO operationTO = null;
        routeVehicleTypeList = operationDAO.getContractVehicleTypeForPointsList(contractId, firstPointId, point1Id, point2Id, point3Id, point4Id, finalPointId);
        /*
         Iterator itr;
         String routeDestination = "";
         if (routeVehicleTypeList.size() == 0) {
         routeDestination = "";
         } else {
         itr = routeVehicleTypeList.iterator();
         while (itr.hasNext()) {
         operationTO = new OperationTO();
         operationTO = (OperationTO) itr.next();
         if (counter == 0) {
         routeDestination = operationTO.getVehicleTypeId() + "#" + operationTO.getVehicleTypeName();
         counter++;
         } else {
         routeDestination = routeDestination + "~" + operationTO.getVehicleTypeId() + "#" + operationTO.getVehicleTypeName();
         }
         }
         }
         */
        return routeVehicleTypeList;
    }

    public String getCnAirBillBarcodeList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        String FileName = "";
        FileName = operationDAO.getCnAirBillBarcodeList(operationTO);
        return FileName;
    }

    public int updateConsignmentNote(OperationTO operationTO, int userId, String[] grossWG, String[] weightKG, String[] gcnNos, String[] consigDate, String[] invoicedNo, String[] custRefDate, String[] pointPlanDate) throws FPRuntimeException, FPBusinessException {
        int updateConsignmentNote = 0;
        updateConsignmentNote = operationDAO.updateConsignmentNote(operationTO, userId, grossWG, weightKG, gcnNos, consigDate, invoicedNo, custRefDate, pointPlanDate);
        return updateConsignmentNote;
    }

    public int insertpenalitycharges(OperationTO operationTO, String penality, String chargeamount, String pcmremarks, String pcmunit) {
        int insertStatus = 0;
        insertStatus = operationDAO.insertpenalitycharges(operationTO, penality, chargeamount, pcmremarks, pcmunit);
        return insertStatus;
    }

    public int insertdetentioncharges(OperationTO operationTO, String detention, String dcmunit, String dcmToUnit, String chargeamt, String dcmremarks) {
        int insertStatus = 0;
        insertStatus = operationDAO.insertdetentioncharges(operationTO, detention, dcmunit, dcmToUnit, chargeamt, dcmremarks);
        return insertStatus;
    }

    public String getCompanyDetails(OperationTO operationTO) {
        String customerDetails = "";
        customerDetails = operationDAO.getCompanyDetails(operationTO);
        return customerDetails;
    }

    public ArrayList getviewpenalitycharge(OperationTO operationTO) {
        ArrayList viewpenalitycharge = new ArrayList();
        viewpenalitycharge = operationDAO.getviewpenalitycharge(operationTO);
        System.out.println("contract BP");
        return viewpenalitycharge;
    }

    public ArrayList getviewdetentioncharge(OperationTO operationTO) {
        ArrayList viewdetentioncharge = new ArrayList();
        viewdetentioncharge = operationDAO.getviewdetentioncharge(operationTO);
        return viewdetentioncharge;
    }

    public ArrayList getDetaintionTimeSlot() throws FPBusinessException, FPRuntimeException {
        ArrayList detaintionTimeSlot = new ArrayList();
        System.out.println("bh,jsw");
        detaintionTimeSlot = operationDAO.getDetaintionTimeSlot();
        return detaintionTimeSlot;
    }

    public String getCustomerName(String consignmentOrderId) throws FPRuntimeException, FPBusinessException {
        String result = "";
        result = operationDAO.getCustomerName(consignmentOrderId);
        return result;
    }

    public String getActualAdvanceAmount(String tripid) throws FPRuntimeException, FPBusinessException {
        String result = "";
        result = operationDAO.getActualAdvanceAmount(tripid);
        return result;
    }

    public int updateConsignmentUpload(OperationTO opto) {
        int status = 0;
        status = operationDAO.updateConsignmentUpload(opto);
        return status;
    }
    public int updateConsignmentContract(OperationTO opto) {
        int status = 0;
        status = operationDAO.updateConsignmentContract(opto);
        return status;
    }

    public int insertConsignmentUpload(OperationTO opto) {
        int status = 0;
        status = operationDAO.insertConsignmentUpload(opto);
        return status;
    }

    public ArrayList getConsignmentExcelUpload(OperationTO opto) throws FPBusinessException, FPRuntimeException {
        ArrayList getConsignmentExcelUpload = new ArrayList();
        getConsignmentExcelUpload = operationDAO.getConsignmentExcelUpload(opto);
        return getConsignmentExcelUpload;
    }

    public ArrayList getConsignmentExcelUploadIncorect(OperationTO opto) throws FPBusinessException, FPRuntimeException {
        ArrayList getConsignmentExcelUploadIncorect = new ArrayList();
        getConsignmentExcelUploadIncorect = operationDAO.getConsignmentExcelUploadIncorect(opto);
        return getConsignmentExcelUploadIncorect;
    }

    public int insertConsignmentExcel(OperationTO operationTO, int userId) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = operationDAO.insertConsignmentExcel(operationTO, userId);
        return status;
    }

    public int getRouteCheck(OperationTO opTO) throws FPBusinessException, FPRuntimeException {
        int getRouteCheck = 0;
        getRouteCheck = operationDAO.getRouteCheck(opTO);
        return getRouteCheck;
    }

    public ArrayList getConsignmentExcelUploadDetails(OperationTO opto) throws FPBusinessException, FPRuntimeException {
        ArrayList getConsignmentExcelUploadDetails = new ArrayList();
        getConsignmentExcelUploadDetails = operationDAO.getConsignmentExcelUploadDetails(opto);
        return getConsignmentExcelUploadDetails;
    }

    public int saveAdvancePostingDetails(String eFSId, String tripid, String advid, String advancerequestamt, String approvalStatusText, String remarks, String userId, String emailDetails) throws FPBusinessException, FPRuntimeException {
        int getRouteCheck = 0;
        getRouteCheck = operationDAO.saveAdvancePostingDetails(eFSId, tripid, advid, advancerequestamt, approvalStatusText, remarks, userId, emailDetails);
        return getRouteCheck;
    }

    public String existRouteContractData(int contractId, String ptpVehicleTypeId,
            String ptpPickupPointId, String ptpInterimPointId1, String ptpInterimPointId2, String ptpInterimPointId3,
            String ptpInterimPointId4, String ptpDropPointId,String movementType) {
        String data = "";
        data = operationDAO.existRouteContractData(contractId, ptpVehicleTypeId,
                ptpPickupPointId, ptpInterimPointId1, ptpInterimPointId2, ptpInterimPointId3,
                ptpInterimPointId4, ptpDropPointId,movementType);
        return data;
    }

    public int makeConsignmentComplete(int userId, OperationTO operationTO) {
        int status = 0;
        status = operationDAO.makeConsignmentComplete(userId, operationTO);
        return status;
    }

    public String getVendoreFSId(OperationTO operationTO) {
        String getVendoreFSId = "";
        getVendoreFSId = operationDAO.getVendoreFSId(operationTO);
        return getVendoreFSId;
    }
    
public ArrayList getConsignmentReportList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consignmentList = new ArrayList();
        consignmentList = operationDAO.getConsignmentReportList(operationTO);
        return consignmentList;

    }   

//Movement type

   

   public int fileUploadDetails(String actualFilePath, String fileSaved,String remarks,OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        System.out.println("BP");
        int status = 0;
        status = operationDAO.FileUploadDetails(actualFilePath, fileSaved,remarks, operationTO, userId);
        return status;
    }
   
     public ArrayList getCustomerUploadsDetails(OperationTO operationTO) {
        ArrayList customerUploadsDetails = new ArrayList();
        customerUploadsDetails = operationDAO.getcustomerUploadsDetails(operationTO);
        return customerUploadsDetails;
    }
     
      public ArrayList  getdisplayLogoBlobData(String uploadId) {
        ArrayList profileList=new ArrayList();
        profileList = operationDAO.getdisplayLogoBlobData(uploadId);
        return profileList;
    }
      
      public int updateBillStatus(int userId, String invoiceId, String billStatus) throws FPRuntimeException, FPBusinessException {
        int updateBillStatus = 0;
        updateBillStatus = operationDAO.updateBillStatus(userId,invoiceId,billStatus);
        return updateBillStatus;
    }
   
      
         public ArrayList getMovementTypeList() throws FPBusinessException, FPRuntimeException {
        ArrayList movementTypeList = new ArrayList();
        movementTypeList = operationDAO.getMovementTypeList();

        return movementTypeList;
    }

 
 public int insertWalkinConsignmentNote(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        int checkAwbStatus = 0;
        int insertStatusDeatils = 0;
        SqlMapClient session = operationDAO.getSqlMapClient();
        String awbNo = "";
        boolean sts = true;
        boolean transactionCommitStatus = false;
        try {
            session.startTransaction();
//        if(operationTO.getAwbType().equals("1")) {
//        checkAwbStatus = operationDAO.getAwbNoCheckExists(operationTO, userId, session);
//        if(checkAwbStatus == 0 && operationTO.getShipmentType().equals("1")){
//            sts = true;
//        }
//        if(checkAwbStatus >= 0 && operationTO.getShipmentType().equals("2")){
//            sts = true;
//        }
//        }else{
//        awbNo = operationDAO.getAwbNo(session);
//        operationTO.setAirWayBillNo(operationTO.getOrderReferenceAwb()+" "+awbNo.substring(0, 4)+" "+awbNo.substring(4));
//        operationTO.setAwbNo(awbNo);
//        sts = true;
//        }
            if (sts) {
                insertStatus = operationDAO.insertWalkinConsignmentNote(operationTO, userId, session);
                operationTO.setConsignmentOrderId(String.valueOf(insertStatus));
                if (insertStatus > 0) {
                    transactionCommitStatus = true;
                }
//                    insertStatus = operationDAO.insertConsignmentArticle(operationTO, userId, session);
//                    //insertStatus = opsDAO.insertMultiplePoints(operationTO, userId, session);
//                    transactionCommitStatus = true;
//                    System.out.println("InsertStatus Here = " + insertStatus);
//                    System.out.println("transactionCommitStatus Here = " + transactionCommitStatus);
////                if(insertStatus == 0){
//                    insertStatus = 1;
////                }else{
////                    // Choosen Truck has already used, So that your booking not aloowed
////                    insertStatus = 3;
////                }
//                }
            } else {
                insertStatus = 2;
            }
            if (transactionCommitStatus) {
                insertStatus = 1;
                session.commitTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
            insertStatus = 0;
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                insertStatus = 0;
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return insertStatus;
    }
         public int vendorAdvanceRequest(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int manualAdvanceRequest = 0;
//        manualAdvanceRequest = operationDAO.vendorAdvanceRequest(operationTO, userId);
         manualAdvanceRequest = operationDAO.vendorAdvanceRequest(operationTO, userId);
        return manualAdvanceRequest;
    }
         
     public ArrayList getmanualvendorAdvDetails(String tripid) throws FPRuntimeException, FPBusinessException {
        ArrayList manualvendorAdvDetails = new ArrayList();
        manualvendorAdvDetails = operationDAO.getmanualvendorAdvDetails(tripid);
        return manualvendorAdvDetails;
    }
     
      public ArrayList getmanualcustomerAdvDetails(String tripid) throws FPRuntimeException, FPBusinessException {
        ArrayList manualfinanceAdvDetails = new ArrayList();
        manualfinanceAdvDetails = operationDAO.getmanualAdvDetails(tripid);
        return manualfinanceAdvDetails;
    }
   
      
       public int customerAdvanceRequest(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int manualAdvanceRequest = 0;
        manualAdvanceRequest = operationDAO.customerAdvanceRequest(operationTO, userId);
        return manualAdvanceRequest;
    }
       
        public ArrayList getcustomerfinanceAdvice(OperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        ArrayList financeRequest = new ArrayList();
        financeRequest = operationDAO.getcustomerfinanceAdvice(operationTO);
        return financeRequest;
    }
        
         public ArrayList viewcustAdviceDetail(String tripAdvaceId) throws FPBusinessException, FPRuntimeException {
        ArrayList viewAdviceDetails = new ArrayList();
        viewAdviceDetails = operationDAO.viewcustAdviceDetail(tripAdvaceId);
        return viewAdviceDetails;
    }
         
           public int savecustomerAdvanceApproval(OperationTO operationTO, int userId, String advid) throws FPRuntimeException, FPBusinessException {
        int saveManualAdvanceApproval = 0;
        saveManualAdvanceApproval = operationDAO.savecustomerAdvanceApproval(operationTO, userId, advid);

        return saveManualAdvanceApproval;

    }
}
