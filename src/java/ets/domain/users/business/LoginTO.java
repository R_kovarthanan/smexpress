/*------------------------------------------------------------------------------
 * LoginTO.java
 * Mar 26, 2008
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
-----------------------------------------------------------------------------*/
package ets.domain.users.business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.*;

/**
 *
 * Modification Log
 * ---------------------------------------------------------------------
 * Ver   Date              Modified By               Description
 * ---------------------------------------------------------------------
 * 1.0   26 Mar 2008       Srinivasan.R             Initial Version
 *
 ***********************************************************************/
/**
 * <br>LoginTO object -Login Transfer object.
 * @author Srinivasan.R
 * @version 1.0 26 Mar 2008
 * @since   Event Portal Iteration 0
 */
public class LoginTO implements Serializable {

    /**
     * name -Name of the User
     */
    private String insertStatus = "";
    private String userMovementType = "";
    private String[] insertQueryStatus = null;
    private List userSubMenuList = null;
    private String serialNo = "";
    private String lastAccessTime = "";
    private String labelName = "";
    private String iconName = "";
    private String defaultLabelName = "";
    private String subMenuLabelName = "";
    private String subMenuIconName = "";
    private String subMenuDefaultLabelName = "";
    private String methodName = "";
    private String[] menuIds = null;
    private String[] subMenuIds = null;
    private String[] functionIds = null;
    private String[] functionStatus = null;
    private String functionSts = "";
    private String menuName = "";
    private String subMenuName = "";
    private String paramDetails = "";
    private String menuId = "";
    private String subMenuId = "";
    private String accessRoleId = "";
    String name;
    int userId = 0;
    String keyName = null;
    String value = null;
    String userName = null;
    private String password = "";
    int roleId = 0;
    private String appUserCode = "";
    private String appUserName = "";
    private String hubId = "";
    private String emailId = "";
    private String roleName = "";
    private String description = "";
    private String email = "";
    private String firstName = "";
    private String lastName = "";
    private String address = "";
    private String city = "";
    private String pincode = "";
    private String phoneNo = "";
    private String letters = "";
    private String status = "";
    private String createdon = "";
    int createdBy = 0;
    private String functionName = "";
    private int functionId = 0;
    private int staffId = 0;
    private String staffName = "";
    private int deptId = 0;
    private int desigId = 0;
    private int gradeId = 0;
    private String desigName = "";
    private String deptName = "";
    private String whatRequest = "";
    private String companyName = "";
    private String companyId = "";
    private String companyTypeName = "";
    private String subMenu = "";
    private int companyTypeId = 0;
    private String driName = "";
    private String embarkId = "";
    private String driverId = "";
    private String empName = "";
    private String vehicleNo = "";
    private String embarkDate = "";
    private String alightDate = "";
    private String alghtingStatus = "";
    private String actInd = "";
    private String remarks = "";
    private String regno = "";
    private String embarkmentDate = "";
    private String remark = "";
    private String empId = "";
    private String embId = "";
    private String driverName = "";
    private String embDate = "";
    private String aliVehileNo = "";
    private String alightingDateA = "";
    private String alighingRemark = "";
    private String routeName = "";
    private String addTrip = "";
    private String fromDate = "";
    private String toDate = "";
    private String routeid = "";
    private String registerNo = "";
    private String vehicleId = "";
    private ArrayList hrms = new ArrayList();
    private ArrayList customer = new ArrayList();
    private ArrayList operation = new ArrayList();
    private ArrayList renderService = new ArrayList();
    private ArrayList security = new ArrayList();
    private ArrayList service = new ArrayList();
    private ArrayList stores = new ArrayList();
    private ArrayList users = new ArrayList();
    private ArrayList vehicles = new ArrayList();
    private ArrayList vendor = new ArrayList();
    private ArrayList fuelManagement = new ArrayList();
    private String relationId = "";
    private String relationName = "";
    private String activeInd = "";
    private String actIndR = "";

    public String getSubMenu() {
        return subMenu;
    }

    public void setSubMenu(String subMenu) {
        this.subMenu = subMenu;
    }

    public ArrayList getCustomer() {
        return customer;
    }

    public void setCustomer(ArrayList customer) {
        this.customer = customer;
    }

    public ArrayList getHrms() {
        return hrms;
    }

    public void setHrms(ArrayList hrms) {
        this.hrms = hrms;
    }

    public ArrayList getOperation() {
        return operation;
    }

    public void setOperation(ArrayList operation) {
        this.operation = operation;
    }

    public ArrayList getRenderService() {
        return renderService;
    }

    public void setRenderService(ArrayList renderService) {
        this.renderService = renderService;
    }

    public ArrayList getSecurity() {
        return security;
    }

    public void setSecurity(ArrayList security) {
        this.security = security;
    }

    public ArrayList getService() {
        return service;
    }

    public void setService(ArrayList service) {
        this.service = service;
    }

    public ArrayList getStores() {
        return stores;
    }

    public void setStores(ArrayList stores) {
        this.stores = stores;
    }

    public ArrayList getUsers() {
        return users;
    }

    public void setUsers(ArrayList users) {
        this.users = users;
    }

    public ArrayList getVehicles() {
        return vehicles;
    }

    public void setVehicles(ArrayList vehicles) {
        this.vehicles = vehicles;
    }

    public ArrayList getVendor() {
        return vendor;
    }

    public void setVendor(ArrayList vendor) {
        this.vendor = vendor;
    }

    public int getCompanyTypeId() {
        return companyTypeId;
    }

    public void setCompanyTypeId(int companyTypeId) {
        this.companyTypeId = companyTypeId;
    }

    public String getCompanyTypeName() {
        return companyTypeName;
    }

    public void setCompanyTypeName(String companyTypeName) {
        this.companyTypeName = companyTypeName;
    }

    public String getWhatRequest() {
        return whatRequest;
    }

    public void setWhatRequest(String whatRequest) {
        this.whatRequest = whatRequest;
    }

    public int getDeptId() {
        return deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public int getDesigId() {
        return desigId;
    }

    public void setDesigId(int desigId) {
        this.desigId = desigId;
    }

    public String getDesigName() {
        return desigName;
    }

    public void setDesigName(String desigName) {
        this.desigName = desigName;
    }

    public int getGradeId() {
        return gradeId;
    }

    public void setGradeId(int gradeId) {
        this.gradeId = gradeId;
    }

    public int getStaffId() {
        return staffId;
    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public int getFunctionId() {
        return functionId;
    }

    public void setFunctionId(int functionId) {
        this.functionId = functionId;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLetters() {
        return letters;
    }

    public void setLetters(String letters) {
        this.letters = letters;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    /**
     * age -Age of the User
     */
    /**
     * address -Address of the User
     */
    /**
     * @return Returns the address.
     */
    public void setUserId(int userid) {
        this.userId = userid;
    }

    public int getUserId() {
        return this.userId;
    }

    /**
     * @return Returns the password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password The password to set.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return this.userName;
    }

    /**
     * @param userName The userName to set.
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCreatedon() {
        return createdon;
    }

    public void setCreatedon(String createdon) {
        this.createdon = createdon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public ArrayList getFuelManagement() {
        return fuelManagement;
    }

    public void setFuelManagement(ArrayList fuelManagement) {
        this.fuelManagement = fuelManagement;
    }

    public String getActInd() {
        return actInd;
    }

    public void setActInd(String actInd) {
        this.actInd = actInd;
    }

    public String getAddTrip() {
        return addTrip;
    }

    public void setAddTrip(String addTrip) {
        this.addTrip = addTrip;
    }

    public String getAlghtingStatus() {
        return alghtingStatus;
    }

    public void setAlghtingStatus(String alghtingStatus) {
        this.alghtingStatus = alghtingStatus;
    }

    public String getAliVehileNo() {
        return aliVehileNo;
    }

    public void setAliVehileNo(String aliVehileNo) {
        this.aliVehileNo = aliVehileNo;
    }

    public String getAlighingRemark() {
        return alighingRemark;
    }

    public void setAlighingRemark(String alighingRemark) {
        this.alighingRemark = alighingRemark;
    }

    public String getAlightDate() {
        return alightDate;
    }

    public void setAlightDate(String alightDate) {
        this.alightDate = alightDate;
    }

    public String getAlightingDateA() {
        return alightingDateA;
    }

    public void setAlightingDateA(String alightingDateA) {
        this.alightingDateA = alightingDateA;
    }

    public String getDriName() {
        return driName;
    }

    public void setDriName(String driName) {
        this.driName = driName;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getEmbDate() {
        return embDate;
    }

    public void setEmbDate(String embDate) {
        this.embDate = embDate;
    }

    public String getEmbId() {
        return embId;
    }

    public void setEmbId(String embId) {
        this.embId = embId;
    }

    public String getEmbarkDate() {
        return embarkDate;
    }

    public void setEmbarkDate(String embarkDate) {
        this.embarkDate = embarkDate;
    }

    public String getEmbarkId() {
        return embarkId;
    }

    public void setEmbarkId(String embarkId) {
        this.embarkId = embarkId;
    }

    public String getEmbarkmentDate() {
        return embarkmentDate;
    }

    public void setEmbarkmentDate(String embarkmentDate) {
        this.embarkmentDate = embarkmentDate;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getRouteid() {
        return routeid;
    }

    public void setRouteid(String routeid) {
        this.routeid = routeid;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getRegisterNo() {
        return registerNo;
    }

    public void setRegisterNo(String registerNo) {
        this.registerNo = registerNo;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getRelationId() {
        return relationId;
    }

    public void setRelationId(String relationId) {
        this.relationId = relationId;
    }

    public String getRelationName() {
        return relationName;
    }

    public void setRelationName(String relationName) {
        this.relationName = relationName;
    }

    public String getActIndR() {
        return actIndR;
    }

    public void setActIndR(String actIndR) {
        this.actIndR = actIndR;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getHubId() {
        return hubId;
    }

    public void setHubId(String hubId) {
        this.hubId = hubId;
    }

    public String getKeyName() {
        return keyName;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getAppUserCode() {
        return appUserCode;
    }

    public void setAppUserCode(String appUserCode) {
        this.appUserCode = appUserCode;
    }

    public String getAppUserName() {
        return appUserName;
    }

    public void setAppUserName(String appUserName) {
        this.appUserName = appUserName;
    }

    public String getAccessRoleId() {
        return accessRoleId;
    }

    public void setAccessRoleId(String accessRoleId) {
        this.accessRoleId = accessRoleId;
    }

    public String getDefaultLabelName() {
        return defaultLabelName;
    }

    public void setDefaultLabelName(String defaultLabelName) {
        this.defaultLabelName = defaultLabelName;
    }

    public String[] getFunctionIds() {
        return functionIds;
    }

    public void setFunctionIds(String[] functionIds) {
        this.functionIds = functionIds;
    }

    public String[] getFunctionStatus() {
        return functionStatus;
    }

    public void setFunctionStatus(String[] functionStatus) {
        this.functionStatus = functionStatus;
    }

    public String getFunctionSts() {
        return functionSts;
    }

    public void setFunctionSts(String functionSts) {
        this.functionSts = functionSts;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String[] getMenuIds() {
        return menuIds;
    }

    public void setMenuIds(String[] menuIds) {
        this.menuIds = menuIds;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSubMenuDefaultLabelName() {
        return subMenuDefaultLabelName;
    }

    public void setSubMenuDefaultLabelName(String subMenuDefaultLabelName) {
        this.subMenuDefaultLabelName = subMenuDefaultLabelName;
    }

    public String getSubMenuIconName() {
        return subMenuIconName;
    }

    public void setSubMenuIconName(String subMenuIconName) {
        this.subMenuIconName = subMenuIconName;
    }

    public String getSubMenuId() {
        return subMenuId;
    }

    public void setSubMenuId(String subMenuId) {
        this.subMenuId = subMenuId;
    }

    public String[] getSubMenuIds() {
        return subMenuIds;
    }

    public void setSubMenuIds(String[] subMenuIds) {
        this.subMenuIds = subMenuIds;
    }

    public String getSubMenuLabelName() {
        return subMenuLabelName;
    }

    public void setSubMenuLabelName(String subMenuLabelName) {
        this.subMenuLabelName = subMenuLabelName;
    }

    public String getSubMenuName() {
        return subMenuName;
    }

    public void setSubMenuName(String subMenuName) {
        this.subMenuName = subMenuName;
    }

    public List getUserSubMenuList() {
        return userSubMenuList;
    }

    public void setUserSubMenuList(List userSubMenuList) {
        this.userSubMenuList = userSubMenuList;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getLastAccessTime() {
        return lastAccessTime;
    }

    public void setLastAccessTime(String lastAccessTime) {
        this.lastAccessTime = lastAccessTime;
    }

    public String[] getInsertQueryStatus() {
        return insertQueryStatus;
    }

    public void setInsertQueryStatus(String[] insertQueryStatus) {
        this.insertQueryStatus = insertQueryStatus;
    }

    public String getInsertStatus() {
        return insertStatus;
    }

    public void setInsertStatus(String insertStatus) {
        this.insertStatus = insertStatus;
    }

    public String getParamDetails() {
        return paramDetails;
    }

    public void setParamDetails(String paramDetails) {
        this.paramDetails = paramDetails;
    }

    public String getUserMovementType() {
        return userMovementType;
    }

    public void setUserMovementType(String userMovementType) {
        this.userMovementType = userMovementType;
    }

    
}

