/*---------------------------------------------------------------------------
 * LoginBP.java
 * Mar 31, 2008
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
-------------------------------------------------------------------------*/
package ets.domain.users.business;

/****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver              Date                      Author                    Change
 * ---------------------------------------------------------------------------
 * 1.0           Mar 31, 2008                 ES			       Created
 *
 ******************************************************************************/
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import java.util.ArrayList;

import ets.domain.users.data.LoginDAO;
import ets.domain.util.ThrottleConstants;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Login BP object - Login Business Process Object.
 * @author Srinivasan Ramakrishnan
 * @version 1.0 31 Mar 2008
 * @since   Event Portal Iteration 0
 */
public class LoginBP {

    private LoginDAO loginDAO;
    /**
     * loginUserId - Login User Id
     */
    int loginUserId = 0;

    /**
     * @return loginDAO
     */
    public LoginDAO getLoginDAO() {
        return loginDAO;
    }

    /**
     * This method used to Check User Name.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String checkUserName(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        String password = "";
        password = loginDAO.checkUserName(loginTO);
        return password;
    }

    /**
     * This method used to Modify Role Details .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int deleteroles(String[] roles, String user, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = loginDAO.deleteroles(roles, user, userId);
        return status;
    }



    /**
     * This method used to Get Role Details .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getMinRoles(String user) throws FPRuntimeException, FPBusinessException {
        ArrayList userlists = new ArrayList();
        userlists = loginDAO.getMinRoles(user);
        return userlists;
    }

    /**
     * This method used to Get Role Details .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getRoles() throws FPRuntimeException, FPBusinessException {
        ArrayList rolelist = new ArrayList();
        rolelist = loginDAO.getRoles();
        return rolelist;
    }

    /**
     * This method used to Get User Details .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getUserList(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        ArrayList userlist = new ArrayList();
        userlist = loginDAO.getUserList(loginTO);
        return userlist;
    }


    /**
     * This method used to Get User Roles .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getUserRoles(String user) throws FPRuntimeException, FPBusinessException {
        ArrayList userlist = new ArrayList();
        userlist = loginDAO.getUserRoles(user);
        return userlist;
    }

    /**
     * This method used to Get User Functions .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getUserFunctions(int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList getUserFunctions = new ArrayList();
        getUserFunctions = loginDAO.getUserFunctions(userId);
        return getUserFunctions;
    }

    /**
     * This method used to Get User Unavailable Fucntion .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getUserUnavailableFunctions(int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList unavailableFunctions = new ArrayList();
        unavailableFunctions = loginDAO.getUserUnavailableFunctions(userId);
        return unavailableFunctions;
    }

    /**
     * This method used to Add Role Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertRoleDetails(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = loginDAO.insertRoleDetails(loginTO);
        return insertStatus;
    }

    /**
     * This method used to Modify Role Details .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int modifyRoleDetails(ArrayList rolelist, int UserId) throws FPRuntimeException, FPBusinessException {
        int Status = 0;
        Status = loginDAO.modifyRoleDetails(rolelist, UserId);
        return Status;
    }

    /**
     * This method used to Modify User Details .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int modifyUserDetails(ArrayList userlist, int UserId) throws FPRuntimeException, FPBusinessException {
        int Status = 0;
        Status = loginDAO.modifyUserDetails(userlist, UserId);
        return Status;
    }

    /**
     * This method used to Modify user Role Details .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int saveuserroles(String[] roles, String user, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = loginDAO.saveuserroles(roles, user, userId);
        return status;
    }

    /**
     * @param loginDAO -The Login DAO
     */
    public void setLoginDAO(LoginDAO loginDAO) {
        this.loginDAO = loginDAO;
    }

    /**
     * This method used to Check Password Exists .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int checkExistUser(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        int loginStatus = 0;
        loginStatus = loginDAO.checkExistUser(loginTO);
        return loginStatus;
    }

    /**
     * This method used to Insert user Details .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertUserDetails(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = loginDAO.insertUserDetails(loginTO);
        return insertStatus;
    }

    /**
     * This method used to Get User Password
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getUserPassword(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        String password = "";
        password = loginDAO.getUserPassword(loginTO);
        return password;
    }

    /**
     * This method used to Use to Change Password .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int changePassword(LoginTO loginTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 0;
        updateStatus = loginDAO.changePassword(loginTO, userId);
        return updateStatus;
    }

    /**
     * This method used to Get Active Role Details .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getActiveRoles() throws FPRuntimeException, FPBusinessException {
        ArrayList rolelist = new ArrayList();
        rolelist = loginDAO.getActiveRoles();
        return rolelist;
    }

    /**
     * This method used to Get  Assigned Functions.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getAssignedFucntions(int roleId) throws FPRuntimeException, FPBusinessException {
        ArrayList getfuncrole = new ArrayList();
        getfuncrole = loginDAO.getAssignedFucntions(roleId);
        return getfuncrole;
    }

    /**
     * This method used to Get Available Functions .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getAvailableFunction(int roleId) throws FPRuntimeException, FPBusinessException {
        ArrayList getfuncrole = new ArrayList();
        getfuncrole = loginDAO.getAvailableFunction(roleId);
        return getfuncrole;
    }

    /**
     * This method used to Modify Functions.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int deletefunctions(String[] assignedFucntion, int roleId, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = loginDAO.deletefunctions(assignedFucntion, roleId, userId);
        return status;
    }

    /**
     * This method used to Inser Role Functions .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertfunctions(String[] assignedFucntion, int roleId, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = loginDAO.delrolesfunc(assignedFucntion, roleId, userId);
        return status;
    }

    /**
     * This method used to get User Details
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getUserDetails(int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList userDetails = new ArrayList();
        userDetails = loginDAO.getUserDetails(userId);
        return userDetails;
    }

    /**
     * This method used to get User Functions
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getUserAvailableRoles(int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList getUserAvailableRoles = new ArrayList();
        getUserAvailableRoles = loginDAO.getUserAvailableRoles(userId);
        return getUserAvailableRoles;
    }

    /**
     * This method used to Check Authorisation.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public boolean checkAuthorisation(ArrayList userFunction, String whatRequest) throws FPRuntimeException, FPBusinessException {
        boolean status = false;
        LoginTO loginTO = null;        
        Iterator itr = userFunction.iterator();
//        System.out.println("srini what request:"+whatRequest);
        while (itr.hasNext()) {
            loginTO = (LoginTO) itr.next();
//            System.out.println("function Name  "+loginTO.getFunctionName());
            if (loginTO.getFunctionName().equals(whatRequest)) {
                status = true;
                break;
            }
        }
        return true;
    }


    public String getUserNameSuggestions(String userName) {
        String suggestion = "";
        suggestion = loginDAO.getUserNameSuggestions(userName);
        return suggestion;
    }

    public boolean checkUserName(String userName) {
        boolean status = false;
        status = loginDAO.checkUserName(userName);
        return status;
    }    
    
        public ArrayList getUserMenus(int userId) {
          ArrayList userMenus = new ArrayList();
          LoginTO loginTO=null;
        userMenus = loginDAO.getUserMenus(userId);
        Iterator itr=userMenus.iterator();
        while(itr.hasNext()){
          loginTO=new LoginTO();  
          loginTO=(LoginTO)itr.next();
          if((loginTO.getName()).equalsIgnoreCase("Hrms")){
              loginTO.setHrms(loginDAO.getSubMenus(loginTO.getName(),userId));
//              System.out.println("hrsmlist"+loginTO.getHrms().size());
          }else if((loginTO.getName()).equalsIgnoreCase("Customer")){
              loginTO.setCustomer(loginDAO.getSubMenus(loginTO.getName(),userId));
//              System.out.println("customer"+loginTO.getCustomer().size());
          }else if((loginTO.getName()).equalsIgnoreCase("Operation")){
              loginTO.setOperation(loginDAO.getSubMenus(loginTO.getName(),userId));
//              System.out.println("setOperation"+loginTO.getOperation().size());
          }else if((loginTO.getName()).equalsIgnoreCase("RenderService")){
              loginTO.setRenderService(loginDAO.getSubMenus(loginTO.getName(),userId));
//              System.out.println("setRenderService"+(loginTO.getRenderService()).size());
          }else if((loginTO.getName()).equalsIgnoreCase("Security")){
              loginTO.setSecurity(loginDAO.getSubMenus(loginTO.getName(),userId));
              System.out.println("setSecurity"+(loginTO.getSecurity()).size());
          }else if((loginTO.getName()).equalsIgnoreCase("Service")){
              loginTO.setService(loginDAO.getSubMenus(loginTO.getName(),userId));
//              System.out.println("setService"+(loginTO.getService()).size());
          }else if((loginTO.getName()).equalsIgnoreCase("Stores")){
              loginTO.setStores(loginDAO.getSubMenus(loginTO.getName(),userId));
              System.out.println("setStores"+(loginTO.getStores()).size());
          }
          else if((loginTO.getName()).equalsIgnoreCase("User")){
              loginTO.setUsers(loginDAO.getSubMenus(loginTO.getName(),userId));
//              System.out.println("setUsers"+(loginTO.getUsers()).size());
          }
          else if((loginTO.getName()).equalsIgnoreCase("vehicle")){
              loginTO.setVehicles(loginDAO.getSubMenus(loginTO.getName(),userId));
//              System.out.println("setVehicles"+(loginTO.getVehicles()).size());
          }
          else if((loginTO.getName()).equalsIgnoreCase("Vendor")){
              loginTO.setVendor(loginDAO.getSubMenus(loginTO.getName(),userId));
             System.out.println("setVendor"+(loginTO.getVendor()).size());
          }
          else if((loginTO.getName()).equalsIgnoreCase("FuelManagement")){
              loginTO.setFuelManagement(loginDAO.getSubMenus(loginTO.getName(),userId));
              System.out.println("FuelManagement"+(loginTO.getFuelManagement()).size());
          } 
          
        }
        return userMenus;
    }
        
        
    public int loginHistory(int userId, String sessionId, String ip, String loginTime, String logoutTime) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = loginDAO.loginHistory(userId, sessionId , ip, loginTime, logoutTime );
        return status;
    }
    
    
    public int usageHistory(int userId, String sessionId, String screenName) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = loginDAO.usageHistory(userId, sessionId, screenName);
        return status;
    }    
        
    public String handleRouteName(String routeName) {
        String routeNames = "";
        routeName = routeName + "%";
        routeNames = loginDAO.handleRouteName(routeName);
        return routeNames;
    }

public ArrayList getDriverEmbarkDetail(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {

        ArrayList driverEmbDetail = new ArrayList();
        driverEmbDetail = loginDAO.getDriverEmbarkDetail(loginTO);
//        if (driverEmbDetail.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
//        }
        System.out.println("tallyXMLReportSize in BP:" + driverEmbDetail.size());
        return driverEmbDetail;
    }

    public int getsaveDriverV(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {

        int saveDriverEmb = 0;
        saveDriverEmb = loginDAO.getsaveDriverV(loginTO);
        System.out.println("tallyXMLReportSize in BP:" + saveDriverEmb);
        return saveDriverEmb;
    }

    public int getAlightsaveDriverV(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {

        //ArrayList alightSaveDriverV = new ArrayList();
        int alightSaveDriverV =0;
        alightSaveDriverV = loginDAO.getAlightsaveDriverV(loginTO);
//        if (alightSaveDriverV.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
//        }
        System.out.println("tallyXMLReportSize in BP:" + alightSaveDriverV);
        return alightSaveDriverV;
    }

    public ArrayList getDriverRemarks(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {

        ArrayList driverRemarks = new ArrayList();
        driverRemarks = loginDAO.getDriverRemarks(loginTO);
        return driverRemarks;
    }
    public int getRemarkSaveDriverV(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {

        int remarkSaveDriverV = 0;
        remarkSaveDriverV = loginDAO.getRemarkSaveDriverV(loginTO);
        //if (remarkSaveDriverV.size() == 0) {
         //   throw new FPBusinessException("EM-GEN-01");
       // }
        System.out.println("remarkSaveDriverV in BP:" + remarkSaveDriverV);
        return remarkSaveDriverV;
    }
    public ArrayList getDriverAdditionalTrip(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {

        ArrayList driverAdditional = new ArrayList();
        driverAdditional = loginDAO.getDriverAdditionalTrip(loginTO);
//        if (driverAdditional.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
//        }
        System.out.println("tallyXMLReportSize in BP:" + driverAdditional.size());
        return driverAdditional;
    }
    public int getsaveAdditionalTripV(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {

        int additionalTripV = 0;
        additionalTripV = loginDAO.getsaveAdditionalTripV(loginTO);
//        if (additionalTripV.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
//        }
        System.out.println("----------- in BP:" + additionalTripV);
        return additionalTripV;
    }
    public String driverVehicle(String regno) {
        String vehileRegNo = "";
        regno = regno + "%";
        vehileRegNo = loginDAO.driverVehicle(regno);
        return vehileRegNo;
    }
/**
     * This method used to Get Relation Details .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getRelations() throws FPRuntimeException, FPBusinessException {
        ArrayList relationlist = new ArrayList();
        relationlist = loginDAO.getRelations();
        return relationlist;
    }
     /**
     * This method used to Get User Details .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getUser(String staffId) throws FPRuntimeException, FPBusinessException {
        ArrayList userDetails = new ArrayList();
        userDetails = loginDAO.getUser(staffId);
        return userDetails;
    }
    
    public void updatePops() {
        ArrayList list = new ArrayList();
        try {
            list = loginDAO.LoadProps();
            ThrottleConstants.PROPS_VALUES = new HashMap<String, String>();
            for (Iterator it = list.iterator(); it.hasNext();) {
                LoginTO object = (LoginTO) it.next();
                try {
                    System.out.println("object.getKeyName() = " + object.getKeyName());
                    System.out.println("object.getValue() = " + object.getValue());
                    ThrottleConstants.PROPS_VALUES.put(object.getKeyName().trim(), object.getValue().trim());
                } catch (Exception e) {
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public ArrayList getCompanyList() throws FPRuntimeException, FPBusinessException {
        ArrayList companyList = new ArrayList();
        companyList = loginDAO.getCompanyList();
        return companyList;
    }

public ArrayList getRoleMenuList(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        ArrayList roleMenuList = new ArrayList();
        roleMenuList = loginDAO.getRoleMenuList(loginTO);
        return roleMenuList;
    }

    public int saveRoleMenu(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        int saveRoleStatus = 0;
        saveRoleStatus = loginDAO.saveRoleMenu(loginTO);
        return saveRoleStatus;
    }

    public ArrayList getUserMenuList(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        ArrayList roleMenuList = new ArrayList();
        roleMenuList = loginDAO.getUserMenuList(loginTO);
        return roleMenuList;
    }

    public ArrayList getUserSubMenuList(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        ArrayList roleSubMenuList = new ArrayList();
        roleSubMenuList = loginDAO.getUserSubMenuList(loginTO);
        return roleSubMenuList;
    }

    public String checkRoleMenuStatus(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        String checkRoleNameStatus = "0";
        checkRoleNameStatus = loginDAO.checkRoleMenuStatus(loginTO);
        return checkRoleNameStatus;
    }

    public ArrayList getMenuList(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        ArrayList menuList = new ArrayList();
        menuList = loginDAO.getMenuList(loginTO);
        return menuList;
    }

    public ArrayList getSubMenuList(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        ArrayList subMenuList = new ArrayList();
        subMenuList = loginDAO.getSubMenuList(loginTO);
        return subMenuList;
    }
    public ArrayList LoadProps() throws FPRuntimeException, FPBusinessException {
        ArrayList loadProperties = new ArrayList();
        loadProperties = loginDAO.LoadProps();
        return loadProperties;
    }
}

