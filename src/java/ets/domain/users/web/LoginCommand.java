/*------------------------------------------------------------------------------
 * LoginCommand.java
 * Mar 31, 2008
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
-----------------------------------------------------------------------------*/
package ets.domain.users.web;

/*************************************************************************
 *
 * Modification Log:
 * ---------------------------------------------------------------------------
 * Ver      Date                               Author                   Change
 * --------------------------------------------------------------------------
 * 1.0    Mar 31, 2008                       ES			        Created
 *
 ******************************************************************************/
/**
 * <br>LoginCommand object - This class have all the Login command varibles.
 * @author Srinivasan.R
 * @version 1.0 31 Mar 2008
 * @since   event Portal Iteration 0
 */
public class LoginCommand {

    private String userName = "";
    private String password = "";
    private String letters = "";
    private String[] userIds = null;
    private String[] userNames = null;
    private String[] activeInds = null;
    private String[] selectedUsers = null;
    private String roletype = null;
    private String roledesc = null;
    private String[] roleId = null;
    private String[] roleName = null;
    private String[] desc = null;
    private String[] assignedRole = null;
    private String userId = null;
    private String newPassword = null;
    private String rolesId = "";
    private String[] assignedfunc = null;
    private String whatRequest = "";

     private String driName = "";
      private String regno = "";
      private String embarkmentDate = "";
      private String remark = "";
    private String empId = "";
    private String embId = "";
    private String driverName = "";
    private String embDate = "";
    private String aliVehileNo = "";
    private String alightingDateA = "";
    private String alighingRemark = "";
    private String routeName = "";
    private String addTrip = "";
    private String fromDate = "";
    private String toDate = "";
    private String routeid = "";

    private String actInd  = "";

    


    public String getWhatRequest() {
        return whatRequest;
    }

    public void setWhatRequest(String whatRequest) {
        this.whatRequest = whatRequest;
    }
            

    public String[] getAssignedfunc() {
        return assignedfunc;
    }

    public void setAssignedfunc(String[] assignedfunc) {
        this.assignedfunc = assignedfunc;
    }

    public String getRolesId() {
        return rolesId;
    }

    public void setRolesId(String rolesId) {
        this.rolesId = rolesId;
    }

    public String[] getAssignedRole() {
        return assignedRole;
    }

    public void setAssignedRole(String[] assignedRole) {
        this.assignedRole = assignedRole;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String[] getDesc() {
        return desc;
    }

    public void setDesc(String[] desc) {
        this.desc = desc;
    }

    public String[] getRoleId() {
        return roleId;
    }

    public void setRoleId(String[] RoleId) {
        this.roleId = RoleId;
    }

    public String[] getRoleName() {
        return roleName;
    }

    public void setRoleName(String[] RoleName) {
        this.roleName = RoleName;
    }

    public String[] getActiveInds() {
        return activeInds;
    }

    public String getRoledesc() {
        return roledesc;
    }

    public void setRoledesc(String roledesc) {
        this.roledesc = roledesc;
    }

    public String getRoletype() {
        return roletype;
    }

    public void setRoletype(String roletype) {
        this.roletype = roletype;
    }

    public void setActiveInds(String[] activeInds) {
        this.activeInds = activeInds;
    }

    public String[] getSelectedUsers() {
        return selectedUsers;
    }

    public void setSelectedUsers(String[] selectedUsers) {
        this.selectedUsers = selectedUsers;
    }

    public String[] getUserIds() {
        return userIds;
    }

    public void setUserIds(String[] userIds) {
        this.userIds = userIds;
    }

    public String[] getUserNames() {
        return userNames;
    }

    public void setUserNames(String[] userNames) {
        this.userNames = userNames;
    }

    public String getLetters() {
        return letters;
    }

    public void setLetters(String letters) {
        this.letters = letters;
    }

    /**
     * @return String - returns the password.
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * @param password The password to set.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return String - returns the userName.
     */
    public String getUserName() {

        return this.userName;
    }

    /**
     * @param userName The userName to set.
     */
    public void setUserName(String userName) {
        this.userName = userName;

    }

    public String getAddTrip() {
        return addTrip;
    }

    public void setAddTrip(String addTrip) {
        this.addTrip = addTrip;
    }

    public String getAliVehileNo() {
        return aliVehileNo;
    }

    public void setAliVehileNo(String aliVehileNo) {
        this.aliVehileNo = aliVehileNo;
    }

    public String getAlighingRemark() {
        return alighingRemark;
    }

    public void setAlighingRemark(String alighingRemark) {
        this.alighingRemark = alighingRemark;
    }

    public String getAlightingDateA() {
        return alightingDateA;
    }

    public void setAlightingDateA(String alightingDateA) {
        this.alightingDateA = alightingDateA;
    }

    public String getDriName() {
        return driName;
    }

    public void setDriName(String driName) {
        this.driName = driName;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getEmbDate() {
        return embDate;
    }

    public void setEmbDate(String embDate) {
        this.embDate = embDate;
    }

    public String getEmbId() {
        return embId;
    }

    public void setEmbId(String embId) {
        this.embId = embId;
    }

    public String getEmbarkmentDate() {
        return embarkmentDate;
    }

    public void setEmbarkmentDate(String embarkmentDate) {
        this.embarkmentDate = embarkmentDate;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getRouteid() {
        return routeid;
    }

    public void setRouteid(String routeid) {
        this.routeid = routeid;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getActInd() {
        return actInd;
    }

    public void setActInd(String actInd) {
        this.actInd = actInd;
    }
    
}

