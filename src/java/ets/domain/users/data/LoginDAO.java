/*------------------------------------------------------------------------------
 * LoginDAO.java
 * Jul 26, 2008
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
-----------------------------------------------------------------------------*/
package ets.domain.users.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;


import ets.domain.users.business.LoginTO;
import ets.domain.util.FPLogUtils;
import com.ibatis.sqlmap.engine.execution.SqlExecutor;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;

/**
 *
 * Modification Log
 * ---------------------------------------------------------------------
 * Ver   Date              Modified By               Description
 * ---------------------------------------------------------------------
 * 1.0   26 Jul 2008       Srinivasan.R             Initial Version
 *
 ***********************************************************************/
/**
 * <br>Login DAO object.
 * @author Srinivasan.R
 * @version 1.0 26 Jul 2008
 * @since   Event Portal Iteration 0
 */
public class LoginDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "LoginDAO";
    /**
     * userId -The Identifier of the User
     */
    int userId = 0;

    /**
     * constructor
     */
    public LoginDAO() {
        /*
         *
         */
    }

    /**
     * This method used to Check Password Exists .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int checkExistUser(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userName", loginTO.getUserName());
        map.put("password", loginTO.getPassword());
        System.out.println("password " + loginTO.getPassword());
        System.out.println("userName " + loginTO.getUserName());
        int status = 0;

        try {
            String retVal = (String) getSqlMapClientTemplate().queryForObject("login.checkUser", map);
            if(retVal != null){
                status = Integer.parseInt(retVal);
            }
           System.out.println("status...for check user"+status);

        } catch (Exception sqlException) {
//            System.out.println(status);
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    /**
     * This method used to Check User Name.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String checkUserName(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userName", loginTO.getUserName());

//        System.out.println("userName " + loginTO.getUserName());
        String status = "";

        try {
            status = (String) getSqlMapClientTemplate().queryForObject("login.checkUserName", map);
//            System.out.println("status " + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkUserName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkUserName", sqlException);
        }
        return status;
    }

    /**
     * This method used to Get User Details .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getUserList(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        String letter = loginTO.getLetters() + "%";
//        System.out.println("letters " + letter);
        map.put("letters", letter);
        ArrayList userList = new ArrayList();
        try {
//            System.out.println("letters " + loginTO.getLetters());
            userList = (ArrayList) getSqlMapClientTemplate().queryForList("login.getUser", map);
//            System.out.println("status is in insertdao " + userList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return userList;
    }

    /**
     * This method used to Insert user Details .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertUserDetails(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userName", loginTO.getUserName());
        map.put("password", loginTO.getPassword());
        map.put("UserId", loginTO.getUserId());
//        System.out.println("Username & pwd " + loginTO.getUserName() + "," + loginTO.getPassword() + "," + loginTO.getUserId());
        int status = 0;
        try {

            status = (Integer) getSqlMapClientTemplate().update("login.insertUser", map);
//            System.out.println("status is in insertdao " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    /**
     * This method used to Add Role Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertRoleDetails(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("roletype", loginTO.getRoleName());
        map.put("roledesc", loginTO.getDescription());
        map.put("UserId", loginTO.getUserId());

        int status = 0;
        try {

            status = (Integer) getSqlMapClientTemplate().update("login.insertRole", map);
//            System.out.println("status is in insertdao " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    /**
     * This method used to Modify User Details .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int modifyUserDetails(ArrayList userlist, int UserId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        try {
            Iterator itr = userlist.iterator();
            LoginTO loginTO = new LoginTO();
            while (itr.hasNext()) {
                loginTO = (LoginTO) itr.next();
                map.put("userIds", loginTO.getUserId());
                map.put("userName", loginTO.getUserName());
                map.put("status", loginTO.getStatus());
                map.put("userId", UserId);
                status = (Integer) getSqlMapClientTemplate().update("login.updateUser", map);
            }
            System.out.println("status is in updatedao " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    /**
     * This method used to Get Role Details .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getRoles() throws FPRuntimeException, FPBusinessException {
        ArrayList roleList = new ArrayList();
        Map map = new HashMap();
        try {

            roleList = (ArrayList) getSqlMapClientTemplate().queryForList("login.getRoles", map);
            System.out.println("Size in viewroles " + roleList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return roleList;
    }

    /**
     * This method used to Modify Role Details .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int modifyRoleDetails(ArrayList rolelist, int UserId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        try {
            Iterator itr = rolelist.iterator();
            LoginTO loginTO = new LoginTO();
            while (itr.hasNext()) {
                loginTO = (LoginTO) itr.next();
                map.put("roleIds", loginTO.getRoleId());
                map.put("rolename", loginTO.getRoleName());
                map.put("desc", loginTO.getDescription());
                map.put("status", loginTO.getStatus());
                map.put("userId", UserId);
                status = (Integer) getSqlMapClientTemplate().update("login.updateRole", map);
            }
            System.out.println("status is in updateRoledao " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    /**
     * This method used to Get User Password
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getUserPassword(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userName", loginTO.getUserName());
        String password = "";
        try {
            System.out.println("status is in insertdao userNames  " + loginTO.getUserName());
            password = (String) getSqlMapClientTemplate().queryForObject("login.getPassword", map);
            System.out.println("status is in insertdao  sa " + password);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserPassword", sqlException);
        }
        return password;
    }

    /**
     * This method used to Use to Change Password .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int changePassword(LoginTO loginTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 0;
        Map map = new HashMap();
        try {
            //System.out.println("i am in insert Dao");
            //map.put("operatorId", operatorTO.getOperatorId());
            map.put("userName", loginTO.getUserName());
            map.put("password", loginTO.getPassword());
            map.put("userId", loginTO.getUserId());
            System.out.println("loginTO.getPassword() " + loginTO.getPassword());
            updateStatus = (Integer) getSqlMapClientTemplate().update("login.updatePassword", map);
            System.out.println("return status in modifyUser " + updateStatus);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "modifyUser", sqlException);
        }
        return updateStatus;
    }

    /**
     * This method used to Get Role Details .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getMinRoles(String user) throws FPRuntimeException, FPBusinessException {
        ArrayList userlists = new ArrayList();
        Map map = new HashMap();
        int updateStatus = 0;
        try {
            map.put("userName", user);
            System.out.println("username in dao" + user);
            updateStatus = (Integer) getSqlMapClientTemplate().queryForObject("login.getuserid", map);
            System.out.println("user id " + updateStatus);
            if (updateStatus != 0) {
                map.put("userId", updateStatus);
                userlists = (ArrayList) getSqlMapClientTemplate().queryForList("login.getMinRoles", map);
                System.out.println("Size in getMinRoles " + userlists.size());
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return userlists;
    }

    /**
     * This method used to Get User Roles .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getUserRoles(String user) throws FPRuntimeException, FPBusinessException {
        ArrayList userlist = new ArrayList();
        Map map = new HashMap();
        int updateStatus = 0;
        try {
            map.put("userName", user);
            System.out.println("username in dao" + user);
            updateStatus = (Integer) getSqlMapClientTemplate().queryForObject("login.getuserid", map);
            System.out.println("user id " + updateStatus);
            if (updateStatus != 0) {
                map.put("userId", updateStatus);
                userlist = (ArrayList) getSqlMapClientTemplate().queryForList("login.getUserRoles", map);
                System.out.println("Size in getUserRoles " + userlist.size());
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return userlist;
    }

    /**
     * This method used to Get Active Role Details .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getActiveRoles() throws FPRuntimeException, FPBusinessException {
        ArrayList roleList = new ArrayList();
        Map map = new HashMap();
        try {

            roleList = (ArrayList) getSqlMapClientTemplate().queryForList("login.getActiveRoles", map);
            //System.out.println("Size in viewroles " + roleList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return roleList;
    }

    /**
     * This method used to Get  Assigned Functions.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getAssignedFucntions(int roleId) throws FPRuntimeException, FPBusinessException {
        ArrayList assignedFunctions = new ArrayList();
        Map map = new HashMap();
//String sqlQuery = "select Function_Id from Role_function where Role_Id="+roleId+"";

        try {
            map.put("roleId", roleId);

            assignedFunctions = (ArrayList) getSqlMapClientTemplate().queryForList("login.getAssignedFunctions", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return assignedFunctions;
    }

    /**
     * This method used to Get Available Functions .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getAvailableFunction(int roleId) throws FPRuntimeException, FPBusinessException {
        ArrayList availableFunction = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("roleId", roleId);
            availableFunction = (ArrayList) getSqlMapClientTemplate().queryForList("login.availableFunction", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return availableFunction;
    }

    /**
     * This method used to Modify Functions.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int deletefunctions(String[] assignedFucntion, int roleId, int userId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        try {

            map.put("roleId", roleId);
            map.put("userId", userId);
            status = (Integer) getSqlMapClientTemplate().update("login.updateRoleFunction", map);

            System.out.println("status is in deletefunctions " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("deletefunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "deletefunctions", sqlException);
        }
        return status;
    }

    /**
     * This method used to Inser Role Functions .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int delrolesfunc(String[] assignedFucntion, int roleId, int userId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        int status = 0;
        try {
            map.put("roleId", roleId);
            map.put("userId", userId);
            status = (Integer) getSqlMapClientTemplate().update("login.updateRoleFunction", map);

            for (int i = 0; i < assignedFucntion.length; i++) {
                ArrayList getFunction = new ArrayList();
                map.put("fucntionId", assignedFucntion[i]);
                getFunction = (ArrayList) getSqlMapClientTemplate().queryForList("login.getDetails", map);
                System.out.println("size is " + getFunction.size());
                if (getFunction.size() != 0) {
                    System.out.println("i am here in update");
                    status = (Integer) getSqlMapClientTemplate().update("login.updateRoleFunctionDetails", map);
                } else {
                    System.out.println("i am in insert");
                    status = (Integer) getSqlMapClientTemplate().update("login.insertRoleFunction", map);
                }
            }

            System.out.println("status is in deletefunctions " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("deletefunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "deletefunctions", sqlException);
        }
        return status;
    }

    /**
     * This method used to Modify Role Details .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int deleteroles(String[] roles, String user, int userId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("userName", user);
            System.out.println("username in dao" + user);
            status = (Integer) getSqlMapClientTemplate().queryForObject("login.getuserid", map);
            System.out.println("User id " + status);
            if (status != 0) {
                map.put("userId", status);
                map.put("userid", userId);

                status = (Integer) getSqlMapClientTemplate().update("login.updateUserRoles", map);

                System.out.println("status is in deleteuserroles " + status);

            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("deleteuser roles Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "deleteuserRoles", sqlException);
        }
        return status;
    }

    /**
     * This method used to Modify user Role Details .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int saveuserroles(String[] roles, String user, int userId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        int status = 0;
        try {
            map.put("userName", user);
            System.out.println("username in dao" + user);
            status = (Integer) getSqlMapClientTemplate().queryForObject("login.getuserid", map);
            if (status != 0) {
                map.put("userId", status);
                map.put("userid", userId);
                status = (Integer) getSqlMapClientTemplate().update("login.updateUserRoles", map);

                for (int i = 0; i < roles.length; i++) {
                    ArrayList getroles = new ArrayList();
                    map.put("roleId", roles[i]);
                    getroles = (ArrayList) getSqlMapClientTemplate().queryForList("login.userroleDetails", map);
                    System.out.println("size is " + getroles.size());
                    if (getroles.size() != 0) {
                        System.out.println("i am in update");
                        status = (Integer) getSqlMapClientTemplate().update("login.updateUserRoleDetails", map);
                    } else {
                        System.out.println("i am in insert");
                        status = (Integer) getSqlMapClientTemplate().update("login.insertUserRole", map);
                    }
                }
            }
            System.out.println("status is in deleterolesuser " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("delete&save userroles Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "delete&save userroles", sqlException);
        }
        return status;
    }

    /**
     * This method used to get User Details
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getUserDetails(int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList userDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("userId", userId);
            userDetails = (ArrayList) getSqlMapClientTemplate().queryForList("login.getUserDetails", map);
            System.out.println("userDetails:"+userDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getUserDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserDetails", sqlException);
        }
        return userDetails;
    }

    /**
     * This method used to Get User Functions .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getUserFunctions(int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList userDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("userId", userId);
            userDetails = (ArrayList) getSqlMapClientTemplate().queryForList("login.getUserFucntion", map);
            System.out.println("userFunction in DAO :  " + userDetails.size());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getUserDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserDetails", sqlException);
        }
        return userDetails;
    }

    /**
     * This method used to get User Functions
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getUserAvailableRoles(int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList userRoles = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("userId", userId);
            userRoles = (ArrayList) getSqlMapClientTemplate().queryForList("login.userRoles", map);
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getUserDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserDetails", sqlException);
        }
        return userRoles;
    }

    /**
     * This method used to Get User Unavailable Fucntion .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getUserUnavailableFunctions(int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList userDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("userId", userId);
            userDetails = (ArrayList) getSqlMapClientTemplate().queryForList("login.getUnavailableFunction", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getUserDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserDetails", sqlException);
        }
        return userDetails;
    }

    public String getUserNameSuggestions(String userName) {
        Map map = new HashMap();
        String name = userName + "%";
        System.out.println("Name  " + name);
        map.put("userName", name);
        String suggestions = "";
        try {
            ArrayList getUserList = new ArrayList();
            getUserList = (ArrayList) getSqlMapClientTemplate().queryForList("login.getUserNameList", map);
            Iterator itr = getUserList.iterator();
            LoginTO loginTO = null;
            while (itr.hasNext()) {
                loginTO = (LoginTO) itr.next();
                suggestions = loginTO.getName() + "~" + suggestions;
            }
            System.out.println("userName  " + suggestions);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserNameSuggestions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserNameSuggestions", sqlException);
        }
        return suggestions;
    }

    public boolean checkUserName(String userName) {
        boolean status = false;
        ArrayList userId = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("userName", userName);
            userId = (ArrayList) getSqlMapClientTemplate().queryForList("login.chkUserNam", map);
            System.out.println("userid size  " + userId.size());
            if (userId.size() == 0) {
                status = true;
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getActiveCourses Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getActiveCourses", sqlException);
        }
        return status;
    }

    public ArrayList getUserMenus(int userId) {
        ArrayList userMenus = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("userId", userId);
            userMenus = (ArrayList) getSqlMapClientTemplate().queryForList("login.getUserMenus", map);
            System.out.println("userMenus" + userMenus.size());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getUserMenus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserMenus", sqlException);
        }
        return userMenus;
    }

    public ArrayList getSubMenus(String menuName, int userId) {
        ArrayList userSubMenus = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("menuName", menuName);
            map.put("userId", userId);
            userSubMenus = (ArrayList) getSqlMapClientTemplate().queryForList("login.getSubMenus", map);
            System.out.println("userSubMenus" + userSubMenus.size());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getSubMenus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getSubMenus", sqlException);
        }
        return userSubMenus;
    }

    public int loginHistory(int userId, String sessionId, String ip, String loginTime, String logoutTime) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("userId", userId);
            map.put("sessionId", sessionId);
            map.put("ip", ip);
            if (!loginTime.equals("")) {
                status = getSqlMapClientTemplate().update("login.loginHistory", map);
            }
            if (!logoutTime.equals("")) {
                status = getSqlMapClientTemplate().update("login.logoutHistory", map);
            }

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getSubMenus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getSubMenus", sqlException);
        }
        return status;
    }

    public int usageHistory(int userId, String sessionId, String screenName) {
        Map map = new HashMap();
        int status = 0;
        try {

            map.put("userId", userId);
            map.put("sessionId", sessionId);
            map.put("screenName", screenName);

            status = getSqlMapClientTemplate().update("login.usageHistory", map);

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getSubMenus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getSubMenus", sqlException);
        }
        return status;
    }

public ArrayList getDriverEmbarkDetail(LoginTO loginTO) {
        Map map = new HashMap();

        ArrayList driverEmbDetail = new ArrayList();
        try {

            map.put("driName", loginTO.getDriName());
            //System.out.println("map ---------= " + map);
            driverEmbDetail = (ArrayList) getSqlMapClientTemplate().queryForList("login.getDriverEmbarkDetail", map);
            //System.out.println("driverEmbDetail||||||||||||||||" + driverEmbDetail.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverEmbarkDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getDriverEmbarkDetail", sqlException);
        }
        return driverEmbDetail;
    }

    public int getsaveDriverV(LoginTO loginTO) {
        Map map = new HashMap();
        int status = 0;
        int status1 = 0;
        try {

            map.put("driName", loginTO.getDriName());
            map.put("regno", loginTO.getRegno());
            map.put("embarkmentDate", loginTO.getEmbarkmentDate());
            map.put("remark", loginTO.getRemark());
           // System.out.println("map --------1112222333-------" + map);
            status = (Integer) getSqlMapClientTemplate().update("login.getsaveDriver", map);
            if (status != 0) {
                status1 = (Integer) getSqlMapClientTemplate().update("login.getsaveDriverRemark", map);
            }
            //SaveDriverV = (ArrayList) getSqlMapClientTemplate().insert("login.getsaveDriverV", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getsaveDriverV Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getsaveDriverV", sqlException);
        }
        return status1;
    }

    public int getAlightsaveDriverV(LoginTO loginTO) {
        Map map = new HashMap();
        int status = 0;
        int status1 = 0;
        //ArrayList alightSaveDriverV = new ArrayList();
        try {
            map.put("empId", loginTO.getEmpId());
            map.put("embId", loginTO.getEmbId());
            map.put("driverName", loginTO.getDriverName());
            map.put("embDate", loginTO.getEmbDate());
            map.put("vehiNo", loginTO.getAliVehileNo());
            map.put("aliDate", loginTO.getAlightingDateA());
            map.put("aliRemark", loginTO.getAlighingRemark());
           // System.out.println("map ---------------" + map);

            status = (Integer) getSqlMapClientTemplate().update("login.alightSaveDriverV", map);
            if (status != 0) {
                status1 = (Integer) getSqlMapClientTemplate().update("login.alightSaveDriverVRemark", map);
            }
            //SaveDriverV = (ArrayList) getSqlMapClientTemplate().insert("login.getsaveDriverV", map);
            //System.out.println("SaveDriverV=" + alightSaveDriverV.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getsaveDriverV Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getsaveDriverV", sqlException);
        }
        return status1;
    }

    public ArrayList getDriverRemarks(LoginTO loginTO) {
        Map map = new HashMap();
        ArrayList driverRemarks = new ArrayList();
        try {

            map.put("driName", loginTO.getDriName());
            map.put("regno", loginTO.getRegno());
            System.out.println("map ---------= " + map);
            driverRemarks = (ArrayList) getSqlMapClientTemplate().queryForList("login.getDriverRemarks", map);
            System.out.println("driverRemarks ++=" + driverRemarks.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverEmbarkDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getDriverRemarks", sqlException);
        }

        System.out.println("driverRemarks ++=" + driverRemarks.size());
        return driverRemarks;
    }



public int getRemarkSaveDriverV(LoginTO loginTO) {
        Map map = new HashMap();
        int status = 0;
        String testEmbID = "";
        String ActInd = "";
        //  int status1 = 0;
      //  ArrayList remarkSaveDriverV = new ArrayList();
        try {
            map.put("driName", loginTO.getDriName());
            map.put("empId", loginTO.getEmpId());
            map.put("embId", loginTO.getEmbId());
            map.put("driverName", loginTO.getDriverName());
            map.put("vehiNo", loginTO.getAliVehileNo());
            map.put("aliRemark", loginTO.getAlighingRemark());
            System.out.println("map ---------------" + map);
            testEmbID = loginTO.getEmbId();
            ActInd = loginTO.getActInd();
            System.out.println("testEmbID ======> " + testEmbID);
//            if (!testDriID.equalsIgnoreCase("")) {
            if (testEmbID.equalsIgnoreCase("") || ActInd.equalsIgnoreCase("Y")) {
            System.out.println("1==="+map);
                status = (Integer) getSqlMapClientTemplate().update("login.getRemarkSaveDriver", map);
            } else {
            System.out.println("2===" + map);
                status = (Integer) getSqlMapClientTemplate().update("login.getRemarkSaveDriverV", map);
            }

            //SaveDriverV = (ArrayList) getSqlMapClientTemplate().insert("login.getsaveDriverV", map);
           // System.out.println("SaveDriverV=" + remarkSaveDriverV);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getsaveDriverV Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getsaveDriverV", sqlException);
        }
        return status;
    }

    public ArrayList getDriverAdditionalTrip(LoginTO loginTO) {
        Map map = new HashMap();
        ArrayList driverAdditional = new ArrayList();
        try {

            map.put("driName", loginTO.getDriName());
            System.out.println("map ---------= " + map);
            driverAdditional = (ArrayList) getSqlMapClientTemplate().queryForList("login.getDriverAdditionalTrip", map);
            System.out.println("driverEmbDetail=" + driverAdditional.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverAdditionalTrip Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getDriverAdditionalTrip", sqlException);
        }
        return driverAdditional;
    }

    public int getsaveAdditionalTripV(LoginTO loginTO) {
        Map map = new HashMap();
        int status = 0;
        int status1 = 0;
       // ArrayList additionalTripV = new ArrayList();
        try {
            map.put("embId", loginTO.getEmbId());
            map.put("driName", loginTO.getDriName());
            map.put("regno", loginTO.getRegno());
            map.put("embarkmentDate", loginTO.getEmbarkmentDate());
            map.put("remark", loginTO.getRemark());
            map.put("routeName", loginTO.getRouteName());
            map.put("addTrip", loginTO.getAddTrip());
            map.put("fromDate", loginTO.getFromDate());
            map.put("toDate", loginTO.getToDate());

            System.out.println("map ---------------" + map);

            status = (Integer) getSqlMapClientTemplate().update("login.getsaveAdditionalTripV", map);
            if(status != 0){
            status1 = (Integer) getSqlMapClientTemplate().update("login.getsaveAdditionalTripR", map);
            }
            //SaveDriverV = (ArrayList) getSqlMapClientTemplate().insert("login.getsaveDriverV", map);
            System.out.println("SaveDriverV=" + status1);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getsaveDriverV Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getsaveDriverV", sqlException);
        }
        return status1;
    }

    public String handleRouteName(String routeName) {
        Map map = new HashMap();
        map.put("routeName", routeName);
        String suggestions = "";
        LoginTO loginTO = new LoginTO();
        try {
            ArrayList routeNamesV = new ArrayList();
            System.out.println("map = " + map);
            routeNamesV = (ArrayList) getSqlMapClientTemplate().queryForList("login.getRouteName", map);
            Iterator itr = routeNamesV.iterator();
            while (itr.hasNext()) {
                loginTO = new LoginTO();
                loginTO = (LoginTO) itr.next();
                suggestions = loginTO.getRouteName() + "~" + suggestions;
            }
            if ("".equals(suggestions)) {
                suggestions = "no match found";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRouteName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getRouteName", sqlException);
        }
        return suggestions;
    }

    public String driverVehicle(String regno) {
        Map map = new HashMap();
        map.put("regno", regno);
        String suggestions = "";
        LoginTO loginTO = new LoginTO();
        try {
            ArrayList vehicleNo = new ArrayList();
            System.out.println("map = " + map);
            vehicleNo = (ArrayList) getSqlMapClientTemplate().queryForList("login.driverVehicle", map);
            Iterator itr = vehicleNo.iterator();
            while (itr.hasNext()) {
                loginTO = new LoginTO();
                loginTO = (LoginTO) itr.next();
                suggestions = loginTO.getRegisterNo() + "~" + suggestions;
            }
            if("".equals(suggestions)){
                suggestions="no match found";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleNo", sqlException);
        }
        return suggestions;
    }
/**
     * This method used to Get Relation Details .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getRelations() throws FPRuntimeException, FPBusinessException {
        ArrayList relationList = new ArrayList();
        Map map = new HashMap();
        try {

            relationList = (ArrayList) getSqlMapClientTemplate().queryForList("login.getRelations", map);
            System.out.println("Size in RelationList " + relationList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRelations Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getRelations", sqlException);
        }
        return relationList;
    }
/**
     * This method used to Get User Details .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getUser(String staffId) throws FPRuntimeException, FPBusinessException {
        ArrayList staffDetails = new ArrayList();
        Map map = new HashMap();
        map.put("staffId",staffId);
        int userId = 0 ;
        try {

            userId = (Integer) getSqlMapClientTemplate().queryForObject("login.getStaffUserId", map);
            map.put("userId",userId);
            if(userId != 0){
            staffDetails = (ArrayList) getSqlMapClientTemplate().queryForList("login.getStaffUserNameDetails", map);
            System.out.println("Size in RelationList " + staffDetails.size());
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRelations Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getRelations", sqlException);
        }
        return staffDetails;
    }
    
    public ArrayList LoadProps() {
        Map map = new HashMap();
        ArrayList list = new ArrayList();
        try {
            list = (ArrayList) getSqlMapClientTemplate().queryForList("login.getAllProps", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("updateSupplierPassword Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateSupplierPassword List", sqlException);
        }
        map.clear();
        return list;
    }

    public ArrayList getCompanyList() throws FPRuntimeException, FPBusinessException {
        ArrayList companyList = new ArrayList();
        Map map = new HashMap();
        try {
            companyList = (ArrayList) getSqlMapClientTemplate().queryForList("login.getCompanyList", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getCompanyList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCompanyList", sqlException);
        }
        return companyList;
    }


    public ArrayList getRoleMenuList(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        ArrayList roleMenuList = new ArrayList();
        Map map = new HashMap();
        map.put("roleId", loginTO.getAccessRoleId());
        map.put("menuId", loginTO.getMenuId());
        map.put("subMenuId", loginTO.getSubMenuId());
        try {
            roleMenuList = (ArrayList) getSqlMapClientTemplate().queryForList("login.getRoleMenuList", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getRoleMenuList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getRoleMenuList", sqlException);
        }
        return roleMenuList;
    }

    public int saveRoleMenu(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        int saveRoleStatus = 0;
        Map map = new HashMap();
        map.put("roleId", loginTO.getAccessRoleId());
        String[] menuIds = loginTO.getMenuIds();
        String[] subMenuIds = loginTO.getSubMenuIds();
        String[] functionIds = loginTO.getFunctionIds();
        String[] functionStatus = loginTO.getFunctionStatus();
        String[] insertQueryStatus = loginTO.getInsertQueryStatus();
        try {
            for (int i = 0; i < menuIds.length; i++) {
                map.put("menuId", menuIds[i]);
                map.put("subMenuId", subMenuIds[i]);
                map.put("functionId", functionIds[i]);
                map.put("functionStatus", functionStatus[i]);
                if(insertQueryStatus[i].equals("2")){
                saveRoleStatus = (Integer) getSqlMapClientTemplate().update("login.insertRoleMenu", map);
                }else{
                saveRoleStatus = (Integer) getSqlMapClientTemplate().update("login.updateRoleMenu", map);
                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveRoleMenu Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "saveRoleMenu", sqlException);
        }
        return saveRoleStatus;
    }

    public ArrayList getUserMenuList(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        ArrayList roleMenuList = new ArrayList();
        Map map = new HashMap();
        map.put("roleId", loginTO.getRoleId());
        try {
            roleMenuList = (ArrayList) getSqlMapClientTemplate().queryForList("login.getUserMenuList", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getUserMenuList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserMenuList", sqlException);
        }
        return roleMenuList;
    }

    public ArrayList getUserSubMenuList(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        ArrayList roleMenuList = new ArrayList();
        Map map = new HashMap();
        map.put("menuId", loginTO.getMenuId());
        map.put("roleId", loginTO.getRoleId());
        try {
            roleMenuList = (ArrayList) getSqlMapClientTemplate().queryForList("login.getUserSubMenuList", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getUserSubMenuList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserSubMenuList", sqlException);
        }
        return roleMenuList;
    }

    public String checkRoleMenuStatus(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        String roleNameStatus = "";
        Map map = new HashMap();
        map.put("roleId", loginTO.getRoleId());
        map.put("methodName", loginTO.getMethodName());
        System.out.println("Throttle Here For CheckRoleMenuStatus === " + map);
        try {
            roleNameStatus = (String) getSqlMapClientTemplate().queryForObject("login.checkRoleMenuStatus", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("checkRoleMenuStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkRoleMenuStatus", sqlException);
        }
        return roleNameStatus;
    }

    public ArrayList getMenuList(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        ArrayList menuList = new ArrayList();
        Map map = new HashMap();
        try {
            menuList = (ArrayList) getSqlMapClientTemplate().queryForList("login.getMenuList", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getMenuList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getMenuList", sqlException);
        }
        return menuList;
    }

    public ArrayList getSubMenuList(LoginTO loginTO) throws FPRuntimeException, FPBusinessException {
        ArrayList subMenuList = new ArrayList();
        Map map = new HashMap();
        map.put("menuId", loginTO.getMenuId());
        try {
            subMenuList = (ArrayList) getSqlMapClientTemplate().queryForList("login.getSubMenuList", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getSubMenuList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getSubMenuList", sqlException);
        }
        return subMenuList;
    }
    

}
