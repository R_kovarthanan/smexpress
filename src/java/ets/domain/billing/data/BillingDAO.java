/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.billing.data;

/**
 *
 * @author vinoth
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPRuntimeException;
import ets.domain.billing.business.BillingTO;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.ibatis.sqlmap.client.SqlMapClient;

public class BillingDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "LoginDAO";

    public BillingDAO() {
    }

    public ArrayList getOrdersForBilling(BillingTO billingTO) {
        Map map = new HashMap();
        ArrayList clisedTrips = null;

        try {
            map.put("fromDate", billingTO.getFromDate());
            map.put("toDate", billingTO.getToDate());
            if (!"".equals(billingTO.getCustomerName())) {
                billingTO.setCustomerName(billingTO.getCustomerName() + "%");
            }
            map.put("custName", billingTO.getCustomerName());
            map.put("userId", billingTO.getUserId());
            map.put("customerId", billingTO.getCustomerId());
            map.put("billOfEntry", billingTO.getBillOfEntryNo());
            map.put("shippingBillNo", billingTO.getShipingBillNo());
            map.put("billingType", billingTO.getBillingType());
            map.put("movementType", billingTO.getMovementType());
            System.out.println("closed Trip map is::" + map);
//            if(billingTO.getCustomerId() != null){
//            }
            clisedTrips = new ArrayList();
            clisedTrips = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getOrdersForBilling", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosedTrips Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getClosedTrips List", sqlException);
        }
        return clisedTrips;
    }

    public ArrayList getRepoOrdersForBilling(BillingTO billingTO) {
        Map map = new HashMap();
        ArrayList clisedTrips = null;

        try {
            map.put("fromDate", billingTO.getFromDate());
            map.put("toDate", billingTO.getToDate());
            if (!"".equals(billingTO.getCustomerName())) {
                billingTO.setCustomerName(billingTO.getCustomerName() + "%");
            }
            map.put("custName", billingTO.getCustomerName());
            map.put("userId", billingTO.getUserId());
            map.put("customerId", billingTO.getCustomerId());
            map.put("billOfEntry", billingTO.getBillOfEntryNo());
            map.put("shippingBillNo", billingTO.getShipingBillNo());
            map.put("billingType", billingTO.getBillingType());
            map.put("movementType", billingTO.getMovementType());
            System.out.println("closed Trip map is::" + map);
//            if(billingTO.getCustomerId() != null){
//            }
            clisedTrips = new ArrayList();
            clisedTrips = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getRepoOrdersForBilling", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosedTrips Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getClosedTrips List", sqlException);
        }
        return clisedTrips;
    }

    public ArrayList getOrdersToBeBillingDetails(BillingTO billingTO) {
        Map map = new HashMap();

        String[] tripId = billingTO.getTripSheetId().split(",");
        List tripIds = new ArrayList(tripId.length);
        for (int i = 0; i < tripId.length; i++) {
            System.out.println("value:" + tripId[i]);
            tripIds.add(tripId[i]);
        }
        map.put("tripIds", tripIds);
        ArrayList orderBillingDetails = new ArrayList();
        try {
            orderBillingDetails = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getOrdersToBeBillingDetails", map);
            System.out.println("tripDetails size=" + orderBillingDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return orderBillingDetails;
    }

    public ArrayList getOrdersToBeBillingDetails1(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();

        String[] tripId = billingTO.getTripSheetId().split(",");
        List tripIds = new ArrayList(tripId.length);
        for (int i = 0; i < tripId.length; i++) {
            System.out.println("value:" + tripId[i]);
            tripIds.add(tripId[i]);
        }
        map.put("tripIds", tripIds);
        ArrayList orderBillingDetails = new ArrayList();
        try {
            orderBillingDetails = (ArrayList) session.queryForList("billing.getOrdersToBeBillingDetails1", map);
            System.out.println("tripDetails size=" + orderBillingDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return orderBillingDetails;
    }

    public String getBillingTotalRevenue(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();

        String[] tripId = billingTO.getTripSheetId().split(",");
        List tripIds = new ArrayList(tripId.length);
        for (int i = 0; i < tripId.length; i++) {
            System.out.println("value:" + tripId[i]);
            tripIds.add(tripId[i]);
        }
        map.put("tripIds", tripIds);
        String totalRevenue = "";
        try {
            totalRevenue = (String) session.queryForObject("billing.getBillingTotalRevenue", map);
            System.out.println("tripDetails size=" + totalRevenue);
            if (totalRevenue == null) {
                totalRevenue = "0";
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return totalRevenue;
    }

    public String getBillingTotalExpense(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();

        String[] tripId = billingTO.getTripSheetId().split(",");
        List tripIds = new ArrayList(tripId.length);
        for (int i = 0; i < tripId.length; i++) {
            System.out.println("value:" + tripId[i]);
            tripIds.add(tripId[i]);
        }
        map.put("tripIds", tripIds);
        String totalExpense = "";
        try {
            totalExpense = (String) session.queryForObject("billing.getBillingTotalExpense", map);
            System.out.println("tripDetails size=" + totalExpense);
            if (totalExpense == null) {
                totalExpense = "0";
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return totalExpense;
    }

    public ArrayList getOrderOtherExpenseDetails(BillingTO billingTO) {
        Map map = new HashMap();

        map.put("tripId", billingTO.getTripId());

        String[] tripId = billingTO.getTripSheetId().split(",");
        List tripIds = new ArrayList(tripId.length);
        for (int i = 0; i < tripId.length; i++) {
            System.out.println("value:" + tripId[i]);
            tripIds.add(tripId[i]);
        }
        map.put("tripIds", tripIds);

        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getOrderOtherExpenseDetails", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getOrderOtherExpenseDetails1(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();

        map.put("tripId", billingTO.getTripId());
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) session.queryForList("billing.getOrderOtherExpenseDetails1", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getTripDetails(BillingTO billingTO) {
        Map map = new HashMap();
        map.put("invoiceId", billingTO.getInvoiceId());
        ArrayList tripDetails = new ArrayList();
        System.out.println("map:---" + map);
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getTripDetails", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return tripDetails;
    }

    public int saveBillHeader(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();
        map.put("fromDate", billingTO.getFromDate());
        map.put("billingPartyId", billingTO.getBillingPartyId());
        map.put("userId", billingTO.getUserId());
        map.put("invoiceCode", billingTO.getInvoiceCode());
        map.put("invoiceNo", billingTO.getInvoiceNo());
        map.put("customerId", billingTO.getCustomerId());
        map.put("billType", "1");
        map.put("noOfTrips", billingTO.getNoOfTrips());
        map.put("grandTotal", billingTO.getGrandTotal());
        map.put("totalRevenue", billingTO.getTotalRevenue());
        map.put("totalExpToBeBilled", billingTO.getTotalExpToBeBilled());
        map.put("orderCount", billingTO.getOrderCount());
        map.put("remarks", billingTO.getRemarks());
        //   map.put("grandTotal", billingTO.getGrandTotal());
        map.put("otherExpense", billingTO.getOtherExpense());
        System.out.println("the saveBillHeader:" + map);
        int status = 0;
        try {
            status = (Integer) session.insert("billing.saveBillHeader", map);
            System.out.println("saveBillHeader size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveBillHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveBillHeader List", sqlException);
        }

        return status;
    }

    public int saveBillDetails(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();

        map.put("userId", billingTO.getUserId());
        map.put("invoiceNo", billingTO.getInvoiceNo());
        map.put("invoiceCode", billingTO.getInvoiceCode());
        map.put("invoiceId", billingTO.getInvoiceId());
        map.put("tripId", billingTO.getTripId());
        map.put("customerId", billingTO.getCustomerId());
        map.put("totalRevenue", billingTO.getEstimatedRevenue());
        map.put("totalExpToBeBilled", billingTO.getExpenseToBeBilledToCustomer());
        map.put("grandTotal", String.valueOf(Double.parseDouble(billingTO.getEstimatedRevenue()) + Double.parseDouble(billingTO.getExpenseToBeBilledToCustomer())));
        map.put("otherExpense", billingTO.getOtherExpense());
        System.out.println("the saveBillDetails:" + map);
        int status = 0;
        try {
            status = (Integer) session.insert("billing.saveBillDetails", map);
            System.out.println("saveBillDetails size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveBillDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveBillDetails List", sqlException);
        }

        return status;
    }

    public int saveBillDetailExpense(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();

        map.put("userId", billingTO.getUserId());
        map.put("invoiceCode", billingTO.getInvoiceCode());
        map.put("invoiceDetailId", billingTO.getInvoiceDetailId());
        map.put("tripId", billingTO.getTripId());
        map.put("expenseId", billingTO.getExpenseId());
        map.put("expenseName", billingTO.getExpenseName() + "; " + billingTO.getExpenseRemarks());
        map.put("marginValue", billingTO.getMarginValue());
        map.put("taxPercentage", billingTO.getTaxPercentage());
        map.put("expenseValue", billingTO.getExpenseValue());
        Float totalValue = 0.00F;
        Float taxValue = 0.00F;
        Float nettValue = 0.00F;
        String marginValue = "0";
        String expenseValue = "0";
        String taxPercentage = "0";
        if (billingTO.getMarginValue() == null || "".equals(billingTO.getMarginValue())) {
            marginValue = "0";
        } else {
            marginValue = billingTO.getMarginValue();
        }
        if (billingTO.getTaxPercentage() == null || "".equals(billingTO.getTaxPercentage())) {
            taxPercentage = "0";
        } else {
            taxPercentage = billingTO.getTaxPercentage();
        }
        if (billingTO.getExpenseValue() == null || "".equals(billingTO.getExpenseValue())) {
            expenseValue = "0";
        } else {
            expenseValue = billingTO.getExpenseValue();
        }
        totalValue = (Float.parseFloat(expenseValue) + Float.parseFloat(marginValue));
        taxValue = ((Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                * Float.parseFloat(taxPercentage) / 100);
        nettValue = (Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                + ((Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                * Float.parseFloat(taxPercentage) / 100);
        map.put("taxValue", taxValue);
        map.put("totalValue", totalValue);
        map.put("nettValue", nettValue);
        System.out.println("the saveBillDetailExpense:" + map);
        int status = 0;
        try {
            status = (Integer) session.update("billing.saveBillDetailExpense", map);
            System.out.println("saveBillDetailExpense size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveBillDetailExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveBillDetailExpense List", sqlException);
        }

        return status;
    }

    public int updateStatus(BillingTO billingTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        map.put("tripSheetId", billingTO.getTripSheetId());
        map.put("tripId", billingTO.getTripSheetId());
        map.put("userId", userId);
        map.put("statusId", billingTO.getStatusId());
        System.out.println("the updateEndTripSheet" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        int tripVehicleCount = 0;
        int tripClosureCount = 0;
        try {

            map.put("KM", 0);
            map.put("HM", 0);
            map.put("remarks", "system update");
            status = (Integer) session.update("billing.insertTripStatus", map);
            status = (Integer) session.update("billing.updateTripStatus", map);

            String ConsignmentIdList = (String) session.queryForObject("billing.getTripConsignment", map);
            System.out.println("the ConsignmentIdList size :" + ConsignmentIdList.length());
            String[] ConsignmentId = ConsignmentIdList.split(",");
            for (int i = 0; i < ConsignmentId.length; i++) {
                map.put("consignmentId", ConsignmentId[i]);
                map.put("consignmentStatus", "13");
                System.out.println("the updated map:" + map);
                status = (Integer) session.update("billing.updateConsignmentStatus", map);
                System.out.println("the status1:" + status);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStatus List", sqlException);
        }

        return status;
    }

    public ArrayList viewBillForSubmission(String customerId, String fromDate, String toDate, String tripType, String submitStatus, String billNo, String grNo) {
        Map map = new HashMap();
        ArrayList closedBillList = null;
        try {
            map.put("customerId", customerId);
            if (fromDate == null) {
                fromDate = "";
            }
            if (toDate == null) {
                toDate = "";
            }
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("tripType", tripType);
            map.put("submitStatus", submitStatus);
            if (billNo != null && !"".equals(billNo)) {
                map.put("billNo", "%" + billNo + "%");
            } else {
                map.put("billNo", "");
            }
            if (grNo != null && !"".equals(grNo)) {
                map.put("grNo", "%" + grNo + "%");
            } else {
                map.put("grNo", "");
            }
            System.out.println("getClosedbill Value map" + map);
            closedBillList = (ArrayList) getSqlMapClientTemplate().queryForList("billing.viewBillForSubmission", map);
            System.out.println("getbil details size is ::::" + closedBillList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return closedBillList;
    }

    public int submitBill(String invoiceId, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("invoiceId", invoiceId);
            map.put("userId", userId);
            System.out.println("map::::" + map);
            status = (Integer) getSqlMapClientTemplate().update("billing.updateTripStatusForBillingSubmission", map);
            ArrayList billingTripIds = new ArrayList();
            billingTripIds = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getInvoiceTripIds", map);
            if (billingTripIds.size() > 0) {
                Iterator itr = billingTripIds.iterator();
                BillingTO billingTO = new BillingTO();
                while (itr.hasNext()) {
                    billingTO = (BillingTO) itr.next();
                    map.put("tripId", billingTO.getTripId());
                    status = (Integer) getSqlMapClientTemplate().update("billing.saveTripStatusDetailsForBillingSubmission", map);
                }
            }
            status = (Integer) getSqlMapClientTemplate().update("billing.updateBillSubmitStatusFinanceHeader", map);

            System.out.println("status = " + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("submitBill Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "submitBill List", sqlException);
        }
        return status;
    }

    public int updateCancel(String billingNo, int userId) {
        Map map = new HashMap();
        map.put("billingNo", billingNo);
        map.put("userId", userId);
        System.out.println("the updateCancel" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("billing.updateInvoiceHeader", map);
            System.out.println("statusInvoiceHeaderUpdate" + status);

            status = (Integer) getSqlMapClientTemplate().update("billing.updateInvoiceDetail", map);
            System.out.println("statusInvoiceDetailsUpdate" + status);

            System.out.println("updateStatus size=" + tripDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStatus List", sqlException);
        }

        return status;
    }

    public ArrayList getPreviewHeader(BillingTO billingTO) {
        Map map = new HashMap();
        ArrayList getPreviewHeader = null;
        try {
            String[] tripId = billingTO.getTripSheetIds().split(",");
            List tripIds = new ArrayList(tripId.length);
            for (int i = 0; i < tripId.length; i++) {
                System.out.println("value:" + tripId[i]);
                tripIds.add(tripId[i]);
            }

            map.put("tripSheetIds", tripIds);

            System.out.println("getPreviewHeader Value map" + map);
            getPreviewHeader = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getPreviewHeader", map);
            System.out.println("getPreviewHeader size is ::::" + getPreviewHeader.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return getPreviewHeader;
    }

    public ArrayList getBillingDetailsForPreview(BillingTO billingTO) {
        Map map = new HashMap();
        ArrayList getBillingDetailsForPreview = null;
        try {
            String[] tripId = billingTO.getTripSheetIds().split(",");
            List tripIds = new ArrayList(tripId.length);
            for (int i = 0; i < tripId.length; i++) {
                System.out.println("value:" + tripId[i]);
                tripIds.add(tripId[i]);
            }

            map.put("tripSheetIds", tripIds);

            System.out.println("getBillingDetailsForPreview map" + map);
            getBillingDetailsForPreview = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getBillingDetailsForPreview", map);
            System.out.println("getBillingDetailsForPreview size is ::::" + getBillingDetailsForPreview.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return getBillingDetailsForPreview;
    }

    public String getInvoiceCodeSequence(SqlMapClient session) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        try {
            codeSequence = (Integer) session.insert("trip.getInvoiceCodeSequence", map);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "000000" + invoiceCode;
                System.out.println("invoice no 1.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "00000" + invoiceCode;
                System.out.println("invoice lenght 2.." + invoiceCode);
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 5) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 6) {
                invoiceCode = "0" + invoiceCode;
            }
            System.out.println("getInvoiceCodeSequence=" + codeSequence);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

    public String getInvoiceCodeSequence1718(SqlMapClient session) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        try {
            codeSequence = (Integer) session.insert("billing.getInvoiceCodeSequence1718", map);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "000000" + invoiceCode;
                System.out.println("invoice no 1.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "00000" + invoiceCode;
                System.out.println("invoice lenght 2.." + invoiceCode);
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 5) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 6) {
                invoiceCode = "0" + invoiceCode;
            }

            System.out.println("getInvoiceCodeSequence=" + invoiceCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

}
