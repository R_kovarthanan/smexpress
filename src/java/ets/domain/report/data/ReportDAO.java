/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.report.data;

import ets.arch.exception.FPBusinessException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPRuntimeException;
import ets.domain.operation.business.OperationTO;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import ets.domain.report.business.ReportTO;
import ets.domain.report.business.DprTO;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sabreesh
 */
public class ReportDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "ReportDAO";

    public ArrayList getBillList(ReportTO repTO, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList mrsList = new ArrayList();
        String regNo = repTO.getRegNo();
        try {
//            map.put("companyId", mrsTO.getCompanyId());
            regNo = regNo.replace(" ", "");
            System.out.println("regNo=" + regNo);
            System.out.println("repTO.getCompanyName()=" + repTO.getCompanyName());
            System.out.println("repTO.getServicePointId()=" + repTO.getServicePointId());
            System.out.println("repTO.getServiceTypeId()=" + repTO.getServiceTypeId());
            System.out.println("repTO.getReportType()=" + repTO.getReportType());
            int reportType = repTO.getReportType();
            System.out.println("Report Type In DAO -->" + reportType);
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("regNo", regNo);
            map.put("reportPoint", repTO.getCompanyName());
            map.put("servicePointId", repTO.getServicePointId());
            map.put("serviceType", repTO.getServiceTypeId());
            map.put("customerType", repTO.getCustomerTypeId());
            if (reportType == 1) {

                mrsList = (ArrayList) getSqlMapClientTemplate().queryForList("report.jobCardBillList", map);

            } else {

                mrsList = (ArrayList) getSqlMapClientTemplate().queryForList("report.BillList", map);

            }
            System.out.println("getBillList size=" + mrsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBillList", sqlException);
        }
        return mrsList;
    }

    public ArrayList getSalesBillTaxSummary(ReportTO repTO, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList mrsList = new ArrayList();
        String regNo = repTO.getRegNo();
        try {
//            map.put("companyId", mrsTO.getCompanyId());
            int reportType = repTO.getReportType();
            System.out.println("Report Type In Sales Tax DAO -->" + reportType);
            regNo = regNo.replace(" ", "");
            System.out.println("regNo=" + regNo);
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("regNo", regNo);
            map.put("reportPoint", repTO.getCompanyName());
            map.put("serviceType", repTO.getServiceTypeId());
            map.put("servicePointId", repTO.getServicePointId());
            System.out.println("sp" + repTO.getServicePointId());
            System.out.println("op" + repTO.getCompanyName());
            System.out.println("fromDate" + fromDate);
            System.out.println("toDate" + toDate);
            if (reportType == 1) {
                //System.out.println("JobcardSale list exec Before");
                mrsList = (ArrayList) getSqlMapClientTemplate().queryForList("report.jobCardSalesTaxSummary", map);
                // System.out.println("JobcardSale list exec After");
            } else {

                // System.out.println("Bill DateSale list exec Before");
                mrsList = (ArrayList) getSqlMapClientTemplate().queryForList("report.salesTaxSummary", map);
                // System.out.println("Bill DateSale list exec After");
            }
            System.out.println("salesTaxSummary=" + mrsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBillList", sqlException);
        }
        return mrsList;
    }

    public ArrayList getBillParticulars(ReportTO repTO, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList mrsList = new ArrayList();
        String regNo = repTO.getRegNo();
        try {
//            map.put("companyId", mrsTO.getCompanyId());
            regNo = regNo.replace(" ", "");
            System.out.println("regNo=" + regNo);
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("regNo", regNo);
            map.put("reportPoint", repTO.getCompanyName());
            map.put("serviceType", repTO.getServiceTypeId());
            mrsList = (ArrayList) getSqlMapClientTemplate().queryForList("report.billList", map);
            System.out.println("getBillList size=" + mrsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBillList", sqlException);
        }
        return mrsList;
    }

    public ArrayList getPurchaseList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList purchaseList = new ArrayList();
        try {
            System.out.println("repTO.getVendorId()" + repTO.getVendorId());
            System.out.println("repTO.getSectionId()" + repTO.getCategoryId());
            System.out.println("repTO.getPurType()" + repTO.getPurType());

            map.put("vendorId", repTO.getVendorId());
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("categoryId", repTO.getCategoryId());
            map.put("companyId", repTO.getCompanyId());
            map.put("mfrId", repTO.getMfrId());
            map.put("modelId", repTO.getModelId());
            map.put("purType", repTO.getPurType());
            map.put("poId", repTO.getPoId());
            map.put("itemName", repTO.getItemName());
            map.put("mfrCode", repTO.getMfrCode());
            map.put("paplCode", repTO.getPaplCode());
            purchaseList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getPurchaseReport", map);
            System.out.println("getpurchaseList Report size=" + purchaseList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "purchaseList", sqlException);
        }
        return purchaseList;
    }

    public ArrayList getComplaintList(ReportTO repTO, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList complaintList = new ArrayList();
        try {
            map.put("vehicleNumber", repTO.getRegNo());
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("sectionId", repTO.getSectionId());
            map.put("problemId", repTO.getProblem());
            map.put("technicianId", repTO.getTechnicianId());
            System.out.println("repTO.getSectionId())" + repTO.getSectionId());
            complaintList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleComplaints", map);
            System.out.println("getVehicleComplaints size=" + complaintList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "VehicleComplaintList", sqlException);
        }
        return complaintList;
    }

    public ArrayList getStockWorthList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList stockWorthList = new ArrayList();
        try {

            map.put("fromDate", repTO.getFromDate());
            map.put("sectionId", repTO.getSectionId());
            map.put("companyId", repTO.getCompanyId());
            map.put("mfrId", repTO.getMfrId());
            map.put("modelId", repTO.getModelId());
            map.put("itemName", repTO.getItemName());
            map.put("mfrCode", repTO.getMfrCode());
            map.put("paplCode", repTO.getPaplCode());
            if ("New".equalsIgnoreCase(repTO.getItemType())) {
                stockWorthList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getStockWorthReportNew", map);
            } else {
                stockWorthList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getStockWorthReportRc", map);
            }

            System.out.println("stockWorthList size=" + stockWorthList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("stockWorthList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "stockWorthList", sqlException);
        }
        return stockWorthList;
    }

    public ArrayList getRateList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList rateList = new ArrayList();
        try {

            map.put("itemName", repTO.getItemName());
            map.put("processId", repTO.getProcessId());

            rateList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getRateList", map);
            System.out.println("rateList size=" + rateList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("rateList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "rateList", sqlException);
        }
        return rateList;
    }

    public ArrayList getPeriodicServiceList(int compId, String regNo, String fromDate) {
        Map map = new HashMap();
        ArrayList serviceList = new ArrayList();
        try {
            map.put("compId", compId);
            map.put("date", fromDate);
            map.put("regNo", regNo);

            serviceList = (ArrayList) getSqlMapClientTemplate().queryForList("report.periodicServiceList", map);
            System.out.println("rateList size=" + serviceList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPeriodicServiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getPeriodicServiceList", sqlException);
        }
        return serviceList;
    }

    public ArrayList getbodyBillList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList bodyBillList = new ArrayList();
        try {
            map.put("companyId", repTO.getCompanyId());
            map.put("vendorId", repTO.getVendorId());
            map.put("jobCardId", repTO.getJobCardId());
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("woId", repTO.getWoId());
            map.put("regNo", repTO.getRegNo());
            bodyBillList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getbodyBillList", map);
            System.out.println("bodyBillList size=" + bodyBillList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("bodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getbodyBillList", sqlException);
        }
        return bodyBillList;
    }

    public ArrayList getContractorActivities(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList contractorActivities = new ArrayList();
        try {
            map.put("companyId", repTO.getCompanyId());
            map.put("vendorId", repTO.getVendorId());
            map.put("jobCardId", repTO.getJobCardId());
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("woId", repTO.getWoId());
            map.put("regNo", repTO.getRegNo());
            contractorActivities = (ArrayList) getSqlMapClientTemplate().queryForList("report.getContractorActivities", map);
            System.out.println("contractorActivities size=" + contractorActivities.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("bodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getbodyBillList", sqlException);
        }
        return contractorActivities;
    }

    public ArrayList getbodyBillDetails(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList bodyBillDetails = new ArrayList();
        try {
            map.put("vendorId", repTO.getVendorId());
            map.put("jobCardId", repTO.getJobCardId());
            bodyBillDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getbodyBillDetails", map);
            System.out.println("getbodyBillDetails size=" + bodyBillDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("bodyBillDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "bodyBillDetails", sqlException);
        }
        return bodyBillDetails;
    }

    public ArrayList getHikedbodyBillDetails(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList bodyBillDetails = new ArrayList();
        try {
            map.put("vendorId", repTO.getVendorId());
            map.put("jobCardId", repTO.getJobCardId());
            bodyBillDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getHikedbodyBillDetails", map);
            System.out.println("getbodyBillDetails size=" + bodyBillDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("bodyBillDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "HikedbodyBillDetails", sqlException);
        }
        return bodyBillDetails;
    }

    public ArrayList getBillDetails(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList BillDetails = new ArrayList();
        try {

            map.put("billId", repTO.getBillNo());
            map.put("jobCardId", repTO.getJobCardId());

            BillDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getBillDetails", map);
            System.out.println("BillDetails size=" + BillDetails);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("BillDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "BillDetails", sqlException);
        }
        return BillDetails;
    }

    public ArrayList getActivityList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList ActivityList = new ArrayList();
        try {

            map.put("billId", repTO.getBillNo());

            ActivityList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getActivityList", map);
            System.out.println("ActivityList size=" + ActivityList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("BillDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "ActivityList", sqlException);
        }
        return ActivityList;
    }

    public ArrayList getTotalPrices(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList TotalPrices = new ArrayList();
        try {

            map.put("billId", repTO.getBillNo());

            TotalPrices = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTotalPrices", map);
            System.out.println("TotalPrices size=" + TotalPrices);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("TotalPrices Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "TotalPrices", sqlException);
        }
        return TotalPrices;
    }

    public ArrayList getStockIssueReport(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList issueList = new ArrayList();
        try {
            System.out.println("from date:" + repTO.getFromDate());
            System.out.println("to date:" + repTO.getToDate());
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("companyId", repTO.getCompanyId());
            map.put("categoryId", repTO.getCategoryId());
            map.put("mfrId", repTO.getMfrId());
            map.put("itemName", repTO.getItemName());
            map.put("mfrCode", repTO.getMfrCode());
            map.put("paplCode", repTO.getPaplCode());
            map.put("regNo", repTO.getRegNo());
            map.put("rcWorkorderId", repTO.getRcWorkorderId());
            map.put("counterId", repTO.getCounterId());

            System.out.println("categoryid" + repTO.getCategoryId());
            System.out.println("mfrid" + repTO.getMfrId());
            System.out.println("Company Id" + repTO.getCompanyId());
            System.out.println("Item Name" + repTO.getItemName());
            System.out.println("Mfr Code" + repTO.getMfrCode());
            System.out.println("PAPL Code" + repTO.getPaplCode());
            System.out.println("CounterId" + repTO.getCounterId());
            System.out.println("RcWorkorder" + repTO.getRcWorkorderId());
            issueList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getStockIssueReport", map);
            System.out.println("getStockIssueReport Report size=" + issueList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("StockIssueReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "StockIssueReport", sqlException);
        }
        return issueList;
    }

    public ArrayList getStoresEffReport(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList issueList = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("companyId", repTO.getCompanyId());
            map.put("categoryId", repTO.getCategoryId());
            System.out.println("categoryid" + repTO.getCategoryId());
            map.put("mfrId", repTO.getMfrId());
            map.put("modelId", repTO.getModelId());
            map.put("itemName", repTO.getItemName());
            map.put("mfrCode", repTO.getMfrCode());
            map.put("paplCode", repTO.getPaplCode());
            issueList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getStoresEffReport", map);
            System.out.println("getStoresEffReport size=" + issueList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("StoresEffReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "StoresEffReport", sqlException);
        }
        return issueList;
    }

    public ArrayList getReceivedStockReport(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList receivedList = new ArrayList();
        ReportTO tax = new ReportTO();
        try {
            map.put("vendorId", repTO.getVendorId());
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("companyId", repTO.getCompanyId());
            map.put("poId", repTO.getPoId());
            map.put("inVoiceId", repTO.getInvoiceId());
            map.put("billType", repTO.getBillType());
            System.out.println("repTO.getVendorId()=" + repTO.getVendorId());
            receivedList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getReceivedStockReport", map);
            System.out.println("getReceivedStockReport size=" + receivedList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ReceivedStockReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "ReceivedStockReport", sqlException);
        }
        return receivedList;
    }

    public ArrayList getReceivedStockTaxSummary(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList receivedList = new ArrayList();
        try {
            map.put("vendorId", repTO.getVendorId());
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("companyId", repTO.getCompanyId());
            map.put("poId", repTO.getPoId());
            map.put("inVoiceId", repTO.getInvoiceId());
            map.put("hikePercentage", repTO.getHikePercentage());
            map.put("billType", repTO.getBillType());
            System.out.println("tax in resources=" + repTO.getHikePercentage());
            receivedList = (ArrayList) getSqlMapClientTemplate().queryForList("report.taxSummary", map);
            System.out.println("getReceivedStockReport size=" + receivedList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ReceivedStockReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "ReceivedStockReport", sqlException);
        }
        return receivedList;
    }

    public ArrayList getInvoiceItems(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList InvoiceItems = new ArrayList();
        try {
            map.put("supplyId", repTO.getSupplyId());
            InvoiceItems = (ArrayList) getSqlMapClientTemplate().queryForList("report.getInvoiceReport", map);
            System.out.println("InvoiceItems size=" + InvoiceItems.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ReceivedStockReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "ReceivedStockReport", sqlException);
        }
        return InvoiceItems;
    }

    public ArrayList getRCWOItems(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList woDetail = new ArrayList();
        try {
            map.put("woId", repTO.getSupplyId());
            System.out.println(" repTO.getSupplyId()   " + repTO.getSupplyId());
            woDetail = (ArrayList) getSqlMapClientTemplate().queryForList("report.woDetail", map);
            System.out.println("woDetail size=" + woDetail.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ReceivedStockReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "woDetail", sqlException);
        }
        return woDetail;
    }

    public ArrayList getGdDetail(String gdId) {
        Map map = new HashMap();
        ArrayList reqList = new ArrayList();
        try {
            map.put("gdId", gdId);

            reqList = (ArrayList) getSqlMapClientTemplate().queryForList("report.gdDetail", map);
            System.out.println("getGdDetail size=" + reqList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getStDetail", sqlException);
        }
        return reqList;
    }

    public ArrayList getServiceSummary(String usageTypeId, String ServiceTypId, String custId, String compId, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList InvoiceItems = new ArrayList();
        try {
            map.put("compId", compId);
            map.put("usageTypeId", usageTypeId);
            map.put("custId", custId);
            map.put("serviceTypeId", ServiceTypId);
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            InvoiceItems = (ArrayList) getSqlMapClientTemplate().queryForList("report.getServiceSummary", map);
            System.out.println("getServiceSummary size=" + InvoiceItems.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getServiceSummary Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getServiceSummary", sqlException);
        }
        return InvoiceItems;
    }

    public ArrayList getMovingAvgReport(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList issueList = new ArrayList();
        try {

            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("companyId", repTO.getCompanyId());
            map.put("categoryId", repTO.getCategoryId());
            System.out.println("categoryid" + repTO.getCategoryId());
            System.out.println("mfrid" + repTO.getMfrId());
            System.out.println("modelid" + repTO.getModelId());
            map.put("mfrId", repTO.getMfrId());
            map.put("modelId", repTO.getModelId());
            map.put("itemName", repTO.getItemName());
            map.put("mfrCode", repTO.getMfrCode());
            map.put("paplCode", repTO.getPaplCode());
            issueList = (ArrayList) getSqlMapClientTemplate().queryForList("report.movingAvgReport", map);
            System.out.println("getStockIssueReport Report size=" + issueList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("StockIssueReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "StockIssueReport", sqlException);
        }
        return issueList;
    }

    public ArrayList getSerEffReport(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList serEffList = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("companyId", repTO.getCompanyId());
            map.put("mfrId", repTO.getMfrId());
            map.put("modelId", repTO.getModelId());
            map.put("regNo", repTO.getRegNo());
            map.put("woId", repTO.getWoId());
            map.put("jobCardId", repTO.getJobCardId());
            serEffList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getSerEffReport", map);
            System.out.println("serEffList Report size=" + serEffList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("serEffList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "serEffList", sqlException);
        }
        return serEffList;
    }

    public ArrayList getStRequestList(String fromDate, String toDate, int fromId, int toId, String type) {
        Map map = new HashMap();
        ArrayList reqList = new ArrayList();
        try {
            System.out.println("type" + type);
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("fromId", fromId);
            map.put("toId", toId);
            System.out.println("fromId" + fromId);
            System.out.println("toId" + toId);
            System.out.println("fromDate" + fromDate);
            System.out.println("toDate" + toDate);
            if (type.equalsIgnoreCase("GR")) {
                reqList = (ArrayList) getSqlMapClientTemplate().queryForList("report.stockTransferList", map);
                System.out.println("stockTransferList GR Report size=" + reqList.size());
            } else {
                System.out.println("in else");
                reqList = (ArrayList) getSqlMapClientTemplate().queryForList("report.stockTransferGDList", map);
            }
            System.out.println("stockTransferList GD Report size=" + reqList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("serEffList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "reqList", sqlException);
        }
        return reqList;
    }

    public ArrayList getStDetail(String reqId) {
        Map map = new HashMap();
        ArrayList reqList = new ArrayList();
        try {
            map.put("requestId", reqId);

            reqList = (ArrayList) getSqlMapClientTemplate().queryForList("report.stockTransferDetails", map);
            System.out.println("stockTransferDetails size=" + reqList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getStDetail", sqlException);
        }
        return reqList;
    }

    public ArrayList getRcBillList(String vendorId, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList reqList = new ArrayList();
        try {
            map.put("vendorId", vendorId);
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);

            reqList = (ArrayList) getSqlMapClientTemplate().queryForList("report.rcBillList", map);
            System.out.println("getRcBillList size=" + reqList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRcBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getRcBillList", sqlException);
        }
        return reqList;
    }

    public ArrayList getRcBillTaxSummary(String vendorId, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList reqList = new ArrayList();
        ReportTO repTo = new ReportTO();
        try {
            map.put("vendorId", vendorId);
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("hikePercentage", repTo.getHikePercentage());

            reqList = (ArrayList) getSqlMapClientTemplate().queryForList("report.rcBillTaxSummary", map);
            System.out.println("getRcBillList size=" + reqList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRcBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getRcBillList", sqlException);
        }
        return reqList;
    }

    public ArrayList getRcBillDetail(String billId) {
        Map map = new HashMap();
        ArrayList reqList = new ArrayList();
        try {
            map.put("billId", billId);

            reqList = (ArrayList) getSqlMapClientTemplate().queryForList("report.rcBillDetail", map);
            System.out.println("rcBillDetail size=" + reqList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("rcBillDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "rcBillDetail", sqlException);
        }
        return reqList;
    }

    public ArrayList getServiceCostList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList serviceCostList = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("companyId", repTO.getCompanyId());
            map.put("regNo", repTO.getRegNo());
            map.put("usageTypeId", repTO.getUsageType());
            map.put("custId", repTO.getCustId());
            map.put("servicePointId", repTO.getServicePointId());
            System.out.println("from-->" + repTO.getFromDate() + "TO date-->" + repTO.getToDate()
                    + "companyId" + repTO.getCompanyId() + "regno-->" + repTO.getRegNo()
                    + "usage type-->" + repTO.getUsageType() + "custId-->" + repTO.getCustId()
                    + "service" + repTO.getServicePointId());
            serviceCostList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getServiceCostList", map);
            System.out.println("serviceCostList size=" + serviceCostList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("serviceCostList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "serviceCostList", sqlException);
        }
        return serviceCostList;
    }

    public ArrayList getWoList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList woList = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("companyId", repTO.getCompanyId());

            woList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getWoList", map);
            System.out.println("woList Report size=" + woList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("woList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "woList", sqlException);
        }
        return woList;
    }

    public ArrayList getServiceChartData(String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList woList = new ArrayList();
        try {
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            System.out.println(" repTO.getFromDate()=" + fromDate + "()" + toDate);
            woList = (ArrayList) getSqlMapClientTemplate().queryForList("report.serviceChartQry", map);
            System.out.println("serviceChartQry Report size=" + woList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getServiceChartData Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getServiceChartData", sqlException);
        }
        return woList;
    }

    public ArrayList handleServiceDailyMIS(String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList woList = new ArrayList();
        try {
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            System.out.println(" repTO.getFromDate()=" + fromDate + "()" + toDate);
            woList = (ArrayList) getSqlMapClientTemplate().queryForList("report.handleServiceDailyMIS", map);
            System.out.println("serviceChartQry Report size=" + woList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getServiceChartData Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getServiceChartData", sqlException);
        }
        return woList;
    }

    public ArrayList getWarrantyServiceList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList serviceList = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("companyId", repTO.getCompanyId());
            map.put("regNo", repTO.getRegNo());
            map.put("status", repTO.getStatus());
            serviceList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getWarrantyServiceList", map);
            System.out.println("serviceList size=" + serviceList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("serviceCostList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "WarrantyServiceList", sqlException);
        }
        return serviceList;
    }

    public ArrayList techniciansList() {
        Map map = new HashMap();
        ArrayList techniciansList = new ArrayList();

        try {

            techniciansList = (ArrayList) getSqlMapClientTemplate().queryForList("report.techniciansList", map);
            System.out.println("techniciansList size=" + techniciansList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("techniciansList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "techniciansList", sqlException);
        }
        return techniciansList;

    }

    public ArrayList getRcItemList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList RcItemList = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("companyId", repTO.getCompanyId());
            map.put("rcId", repTO.getRcId());
            map.put("itemName", repTO.getItemName());
            map.put("paplCode", repTO.getPaplCode());
            map.put("categoryId", repTO.getCategoryId());
            System.out.println("repTO.getCompanyId()" + repTO.getCompanyId());
            System.out.println("repTO.getCategoryId()" + repTO.getCategoryId());
            System.out.println("rcId" + repTO.getRcId());
            System.out.println("repTO.getPaplCode()" + repTO.getPaplCode());
            System.out.println("itemName" + repTO.getItemName());
            RcItemList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getRcItemList", map);
            System.out.println("RcItemList size=" + RcItemList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("RcItemList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "RcItemList", sqlException);
        }
        return RcItemList;
    }

    public ArrayList getRcHistoryList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList RcHistoryList = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("rcId", repTO.getRcId());
            RcHistoryList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getRcHistoryList", map);
            System.out.println("RcHistoryList size=" + RcHistoryList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("RcHistoryList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "RcHistoryList", sqlException);
        }
        return RcHistoryList;
    }

    public ArrayList getTyreList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList tyreList = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("tyreNo", repTO.getTyreNo());
            map.put("status", repTO.getStatus());
            System.out.println("repTO.getStatus()" + repTO.getStatus());
            tyreList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTyreList", map);
            System.out.println("tyreList size=" + tyreList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tyreList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "tyreList", sqlException);
        }
        return tyreList;
    }

    public ArrayList getOrdersList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList tyreList = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("poId", repTO.getPoId());
            map.put("vendorId", repTO.getVendorId());
            map.put("companyId", repTO.getCompanyId());

            if (repTO.getOrderType().equals("PO")) {
                tyreList = (ArrayList) getSqlMapClientTemplate().queryForList("report.poList", map);
            } else {
                tyreList = (ArrayList) getSqlMapClientTemplate().queryForList("report.ordersList", map);
            }
            System.out.println("orders size=" + tyreList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrders Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getOrders", sqlException);
        }
        return tyreList;
    }

    public ArrayList getStockPurchase(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList InvoiceItems = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("vendorId", repTO.getVendorId());
            map.put("billType", repTO.getBillType());
            map.put("companyId", repTO.getCompanyId());
            map.put("purpose", repTO.getPurpose());
            InvoiceItems = (ArrayList) getSqlMapClientTemplate().queryForList("report.stockPurchaseReport", map);
            System.out.println("InvoiceItems size=" + InvoiceItems.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ReceivedStockReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "ReceivedStockReport", sqlException);
        }
        return InvoiceItems;
    }

    public ArrayList getRcBillsReport(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList InvoiceItems = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("vendorId", repTO.getVendorId());
            map.put("purpose", repTO.getPurpose());

            InvoiceItems = (ArrayList) getSqlMapClientTemplate().queryForList("report.rcBillsReport", map);
            System.out.println("InvoiceItems size=" + InvoiceItems.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ReceivedStockReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "ReceivedStockReport", sqlException);
        }
        return InvoiceItems;
    }

    public ArrayList getStatusReport(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList List = new ArrayList();
        try {

            map.put("regNo", repTO.getRegNo());
            String[] dat = repTO.getFromDate().split("-");

            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            System.out.println("fromDate" + repTO.getFromDate());
            System.out.println("toDate" + repTO.getToDate());
            List = (ArrayList) getSqlMapClientTemplate().queryForList("report.vehicleStatusList", map);
            System.out.println("vehicleStatusList size=" + List.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleStatusList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "vehicleStatusList", sqlException);
        }
        return List;
    }

    public ArrayList getPrevious(String jobCardId, String regNo) {
        Map map = new HashMap();
        ArrayList List = new ArrayList();
        try {

            map.put("jobCardId", jobCardId);
            map.put("regNo", regNo);

            List = (ArrayList) getSqlMapClientTemplate().queryForList("report.vehicleLastStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleLastStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "vehicleLastStatus", sqlException);
        }
        return List;
    }

    public String getLastProblem(String jobCardId) {
        Map map = new HashMap();
        String problem = "";
        try {

            map.put("jobCardId", jobCardId);

            problem = (String) getSqlMapClientTemplate().queryForObject("report.vehicleLastProblem", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleLastStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "vehicleLastStatus", sqlException);
        }
        return problem;
    }

    public ArrayList getTaxwiseItems(ReportTO rep) {
        Map map = new HashMap();
        ArrayList itemList = new ArrayList();
        try {
            map.put("vendorId", rep.getVendorId());
            map.put("tax", rep.getTax());
            map.put("paplCode", rep.getPaplCode());
            map.put("itemName", rep.getItemName());

            itemList = (ArrayList) getSqlMapClientTemplate().queryForList("report.taxWiseItems", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleLastStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "vehicleLastStatus", sqlException);
        }
        return itemList;
    }

    public ArrayList getMfrVehComparisionReport(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList list = new ArrayList();
        try {

            String[] dat = repTO.getFromDate().split("-");
            String date = dat[2] + "-" + dat[1] + "-" + dat[0];
            map.put("date", date);
            list = (ArrayList) getSqlMapClientTemplate().queryForList("report.getMfrVehComparisionReport", map);
            System.out.println("getMfrVehComparisionReport" + list.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return list;
    }

    public ArrayList getModelVehComparisionReport(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList list = new ArrayList();
        try {

            String[] dat = repTO.getFromDate().split("-");
            String date = dat[2] + "-" + dat[1] + "-" + dat[0];
            map.put("date", date);
            map.put("mfrId", repTO.getMfrId());
            System.out.println("repTO.getMfrId()" + repTO.getMfrId());
            list = (ArrayList) getSqlMapClientTemplate().queryForList("report.getModelVehComparisionReport", map);
            System.out.println("list" + list.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return list;
    }

    public ArrayList getVehAgeComparisionReport(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList list = new ArrayList();
        try {

            String[] dat = repTO.getFromDate().split("-");
            String date = dat[2] + "-" + dat[1] + "-" + dat[0];
            map.put("date", date);
            map.put("mfrId", repTO.getMfrId());
            System.out.println("fromdate" + date);
            System.out.println("repTO.getMfrId()" + repTO.getMfrId());
            list = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehAgeComparisionReport", map);
            System.out.println("list" + list.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return list;
    }

    public String getVehAgeCount(ReportTO repTO) {
        Map map = new HashMap();
        String count = "";
        try {

            String[] dat = repTO.getFromDate().split("-");
            String date = dat[2] + "-" + dat[1] + "-" + dat[0];
            map.put("date", date);
            map.put("mfrId", repTO.getMfrId());
            System.out.println("repTO.getMfrId()" + repTO.getMfrId());
            count = (String) getSqlMapClientTemplate().queryForObject("report.getVehAgeCount", map);
            System.out.println("count" + count);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return count;
    }

    public String getMfrVehCount(String mfrId) {
        Map map = new HashMap();
        String count = "";
        try {

            map.put("mfrId", mfrId);

            count = (String) getSqlMapClientTemplate().queryForObject("report.getMfrVehCount", map);
            System.out.println("getMfrVehCount" + count);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMfrVehCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getMfrVehCount", sqlException);
        }
        return count;
    }

    public String getModelVehCount(String mfrId, String modelId) {
        Map map = new HashMap();
        String count = "";
        try {

            map.put("mfrId", mfrId);
            map.put("modelId", modelId);
            System.out.println("mfrId" + mfrId);
            System.out.println("modelId" + modelId);
            count = (String) getSqlMapClientTemplate().queryForObject("report.getModelVehCount", map);
            System.out.println("getMfrVehCount" + count);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMfrVehCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getModelVehCount", sqlException);
        }
        return count;
    }

    public ArrayList salesTrendGraph(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList salesTrendGraph = new ArrayList();
        try {

            String[] fdate = reportTO.getFromDate().split("-");
            String fromDate = fdate[2] + "-" + fdate[1] + "-" + fdate[0];
            map.put("fromDate", fromDate);

            String[] edate = reportTO.getToDate().split("-");
            String toDate = edate[2] + "-" + edate[1] + "-" + edate[0];
            map.put("toDate", toDate);

            salesTrendGraph = (ArrayList) getSqlMapClientTemplate().queryForList("report.salesTrendGraph", map);
            System.out.println("salesTrendGraph list" + salesTrendGraph.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return salesTrendGraph;
    }

    public ArrayList vendorTrendGraph(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList vendorTrendGraph = new ArrayList();
        try {

            String[] fdate = reportTO.getFromDate().split("-");
            String fromDate = fdate[2] + "-" + fdate[1] + "-" + fdate[0];
            map.put("fromDate", fromDate);

            String[] edate = reportTO.getToDate().split("-");
            String toDate = edate[2] + "-" + edate[1] + "-" + edate[0];
            map.put("toDate", toDate);

            map.put("itemCode", reportTO.getItemCode());
            map.put("itemName", reportTO.getItemName());
            map.put("vendorName", reportTO.getVendorName());

            vendorTrendGraph = (ArrayList) getSqlMapClientTemplate().queryForList("report.vendorTrendGraph", map);
            System.out.println("vendorTrendGraph list" + vendorTrendGraph.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return vendorTrendGraph;
    }

    public ArrayList mfr() {
        Map map = new HashMap();
        ArrayList mfr = new ArrayList();
        try {

            mfr = (ArrayList) getSqlMapClientTemplate().queryForList("report.mfr", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return mfr;
    }

    public ArrayList usage() {
        Map map = new HashMap();
        ArrayList usage = new ArrayList();
        try {

            usage = (ArrayList) getSqlMapClientTemplate().queryForList("report.usage", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return usage;
    }

    public ArrayList vehicleType() {
        Map map = new HashMap();
        ArrayList vehicleType = new ArrayList();
        try {

            vehicleType = (ArrayList) getSqlMapClientTemplate().queryForList("report.vehicleType", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return vehicleType;
    }

    public String getItemSuggests(String vendorName) {
        Map map = new HashMap();
        map.put("vendorName", vendorName);
        String suggestions = "";
        ReportTO repTO = new ReportTO();
        try {
            ArrayList getItemList = new ArrayList();
            getItemList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getItemNames", map);
            Iterator itr = getItemList.iterator();
            while (itr.hasNext()) {
                repTO = new ReportTO();
                repTO = (ReportTO) itr.next();
                suggestions = repTO.getVendorName() + "~" + suggestions;
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getItemSuggests Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getItemSuggests", sqlException);
        }
        return suggestions;
    }

    public String getproblemSuggests(String problem) {
        Map map = new HashMap();
        map.put("problem", problem);
        String suggestions = "";
        ReportTO repTO = new ReportTO();
        try {
            ArrayList getItemList = new ArrayList();
            getItemList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getproblemNames", map);
            Iterator itr = getItemList.iterator();
            while (itr.hasNext()) {
                repTO = new ReportTO();
                repTO = (ReportTO) itr.next();
                suggestions = repTO.getProblemName() + "~" + suggestions;
                System.out.println("-----------------------" + suggestions);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getItemSuggests Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getItemSuggests", sqlException);
        }
        return suggestions;
    }

    public ArrayList problemDistributionGraph(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList problemDistributionGraph = new ArrayList();
        try {
            String mfrId = "";
            String usage = "";
            String vehicleType = "";
            String[] fdate = reportTO.getFromDate().split("-");
            String fromDate = fdate[2] + "-" + fdate[1] + "-" + fdate[0];
            map.put("fromDate", fromDate);

            String[] edate = reportTO.getToDate().split("-");
            String toDate = edate[2] + "-" + edate[1] + "-" + edate[0];
            map.put("toDate", toDate);
            if (!reportTO.getMfrId().equals("0")) {
                map.put("mfrId", reportTO.getMfrId());
            } else {
                map.put("mfrId", mfrId);
            }
            if (!reportTO.getUsage().equals("0")) {
                map.put("usage", reportTO.getUsage());
            } else {
                map.put("usage", usage);
            }
            if (!reportTO.getVehicleType().equals("0")) {
                map.put("vehicleType", reportTO.getVehicleType());
            } else {
                map.put("vehicleType", vehicleType);
            }

            problemDistributionGraph = (ArrayList) getSqlMapClientTemplate().queryForList("report.problemDistributionGraph", map);
            System.out.println("problemDistributionGraph list" + problemDistributionGraph.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return problemDistributionGraph;
    }

    public ArrayList getColorList(int sizeValue) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        map.put("sizeValue", sizeValue);

        ArrayList colorList = new ArrayList();

        try {
            colorList = (ArrayList) getSqlMapClientTemplate().queryForList("report.colorList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return colorList;
    }

    public ArrayList categoryRcReport(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList categoryRcReport = new ArrayList();
        try {

            String[] fdate = reportTO.getFromDate().split("-");
            String fromDate = fdate[2] + "-" + fdate[1] + "-" + fdate[0];
            map.put("fromDate", fromDate);

            String[] edate = reportTO.getToDate().split("-");
            String toDate = edate[2] + "-" + edate[1] + "-" + edate[0];
            map.put("toDate", toDate);

            categoryRcReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.categoryRcReport", map);
            System.out.println("categoryRcReport list" + categoryRcReport.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return categoryRcReport;
    }

    public ArrayList categoryNewReport(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList categoryNewReport = new ArrayList();
        try {

            String[] fdate = reportTO.getFromDate().split("-");
            String fromDate = fdate[2] + "-" + fdate[1] + "-" + fdate[0];
            map.put("fromDate", fromDate);

            String[] edate = reportTO.getToDate().split("-");
            String toDate = edate[2] + "-" + edate[1] + "-" + edate[0];
            map.put("toDate", toDate);

            categoryNewReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.categoryNewReport", map);
            System.out.println("categoryNewReport list" + categoryNewReport.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return categoryNewReport;
    }
    // shankar

    public ArrayList getActiveCategories() {
        Map map = new HashMap();
        ArrayList categories = new ArrayList();
        try {

            categories = (ArrayList) getSqlMapClientTemplate().queryForList("report.getActiveCategories", map);
            System.out.println("categories size in DAO is " + categories.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStockWorthSearch Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getStockWorthSearch", sqlException);
        }
        return categories;
    }

    public ArrayList stockWorthGraph(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList stockWorthGraph = new ArrayList();
        try {

            String[] fdate = reportTO.getFromDate().split("-");
            String fromDate = fdate[2] + "-" + fdate[1] + "-" + fdate[0];
            map.put("fromDate", fromDate);
            String[] edate = reportTO.getToDate().split("-");
            String toDate = edate[2] + "-" + edate[1] + "-" + edate[0];
            map.put("toDate", toDate);
            map.put("categoryId", reportTO.getCategoryId());
            stockWorthGraph = (ArrayList) getSqlMapClientTemplate().queryForList("report.stockWorthGraph", map);
            System.out.println("stockWorthGraph size in DAO is : " + stockWorthGraph.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getstockWorthGraph Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getstockWorthGraph", sqlException);
        }
        return stockWorthGraph;
    }

    public ArrayList CompanyNameList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList companyNameList = new ArrayList();
        try {

            String[] fdate = reportTO.getFromDate().split("-");
            String fromDate = fdate[2] + "-" + fdate[1] + "-" + fdate[0];
            map.put("fromDate", fromDate);
            String[] edate = reportTO.getToDate().split("-");
            String toDate = edate[2] + "-" + edate[1] + "-" + edate[0];
            map.put("toDate", toDate);
            map.put("categoryId", reportTO.getCategoryId());
            companyNameList = (ArrayList) getSqlMapClientTemplate().queryForList("report.CompanyNameList", map);
            System.out.println("stockWorthGraph size in DAO is : " + companyNameList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getstockWorthGraph Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getstockWorthGraph", sqlException);
        }
        return companyNameList;
    }

    public ArrayList monthNameList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList monthNameList = new ArrayList();
        try {

            String[] fdate = reportTO.getFromDate().split("-");
            String fromDate = fdate[2] + "-" + fdate[1] + "-" + fdate[0];
            map.put("fromDate", fromDate);
            String[] edate = reportTO.getToDate().split("-");
            String toDate = edate[2] + "-" + edate[1] + "-" + edate[0];
            map.put("toDate", toDate);
            map.put("categoryId", reportTO.getCategoryId());
            monthNameList = (ArrayList) getSqlMapClientTemplate().queryForList("report.monthNameList", map);
            System.out.println("stockWorthGraph size in DAO is : " + monthNameList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getstockWorthGraph Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getstockWorthGraph", sqlException);
        }
        return monthNameList;
    }

    public ArrayList getManufacturerList() {
        Map map = new HashMap();
        ArrayList manufacturerList = new ArrayList();
        try {

            manufacturerList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getManufacturerList", map);
            System.out.println("manufacturerList size in DAO is " + manufacturerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getManufacturerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getManufacturerList", sqlException);
        }
        return manufacturerList;
    }

    public ArrayList getUsageTypeList() {
        Map map = new HashMap();
        ArrayList usageTypeList = new ArrayList();
        try {

            usageTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getUsageTypeList", map);
            System.out.println("UsageTypeList size in DAO is " + usageTypeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUsageTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getUsageTypeList", sqlException);
        }
        return usageTypeList;
    }

    public ArrayList getVehicleTypeList() {
        Map map = new HashMap();
        ArrayList vehicleTypeList = new ArrayList();
        try {

            vehicleTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleTypeList", map);
            System.out.println("vehicleTypeList size in DAO is " + vehicleTypeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleTypeList", sqlException);
        }
        return vehicleTypeList;
    }

    public ArrayList processCategoryStRequestList(String fromDate, String toDate, int fromId, int toId) {
        Map map = new HashMap();
        ArrayList reqList = new ArrayList();
        try {

            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("fromId", fromId);
            map.put("toId", toId);
            System.out.println("fromId" + fromId);
            System.out.println("toId" + toId);
            System.out.println("fromDate" + fromDate);
            System.out.println("toDate" + toDate);
//      if(type.equalsIgnoreCase("GR")){
//            reqList = (ArrayList) getSqlMapClientTemplate().queryForList("report.stockTransferList", map);
//             System.out.println("stockTransferList GR Report size=" + reqList.size());
//      }else{
            System.out.println("in else");
            reqList = (ArrayList) getSqlMapClientTemplate().queryForList("report.stockTransferCategoryGDList", map);
            //}
            System.out.println("stockTransferList GD Report size=" + reqList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("serEffList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "reqList", sqlException);
        }
        return reqList;
    }

    // end shankar
    public ArrayList processTaxwiseServiceCostList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList serviceCostList = new ArrayList();
        try {
            int type = repTO.getReportType();
            System.out.println("Report Type In Tax-->" + type);
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("regNo", repTO.getRegNo());
            map.put("custId", repTO.getCustId());
            if (type == 1) {

                serviceCostList = (ArrayList) getSqlMapClientTemplate().queryForList("report.processTaxwiseServiceCostList", map);

            } else {

                serviceCostList = (ArrayList) getSqlMapClientTemplate().queryForList("report.processTaxwiseServiceCostLists", map);

            }
            System.out.println("tax wise serviceCostList size=" + serviceCostList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processTaxwiseServiceCostList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "processTaxwiseServiceCostList", sqlException);
        }
        return serviceCostList;
    }

    public String getVatPercentages() {
        Map map = new HashMap();
        String vat = "";
        try {
            vat = (String) getSqlMapClientTemplate().queryForObject("report.getVatPercentages", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVatPercentages Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVatPercentages", sqlException);
        }
        return vat;
    }

    public float getVatTotalAmount(String[] billNo, String vatPer) {
        Map map = new HashMap();
        String vat = "";
        float vatValue = 0.0f;

        try {
            map.put("billNo", billNo);
            map.put("vatPer", vatPer);
            vat = (String) getSqlMapClientTemplate().queryForObject("report.getVatTotalAmount", map);
            if (vat != null) {
                vatValue = Float.parseFloat(vat);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVatPercentages Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVatPercentages", sqlException);
        }
        return vatValue;
    }

    public float getContractAmount(String[] billNo) {
        Map map = new HashMap();
        String vat = "";
        float vatValue = 0.0f;
        for (int i = 0; i < billNo.length; i++) {
            System.out.println("val" + billNo[i]);
        }
        try {
            map.put("billNo", billNo);
            vat = (String) getSqlMapClientTemplate().queryForObject("report.getContractAmount", map);
            if (vat != null) {
                vatValue = Float.parseFloat(vat);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVatPercentages Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVatPercentages", sqlException);
        }
        return vatValue;
    }

    public ArrayList processTaxwisePO(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList serviceCostList = new ArrayList();

        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("VendorId", repTO.getCustId());

            serviceCostList = (ArrayList) getSqlMapClientTemplate().queryForList("report.processTaxwisePO", map);
            System.out.println("tax wise serviceCostList size=" + serviceCostList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processTaxwiseServiceCostList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "processTaxwiseServiceCostList", sqlException);
        }
        return serviceCostList;
    }

    public float getPOVatTotalAmount(String supplyId, String vatPer) {
        Map map = new HashMap();
        String vat = "";
        float vatValue = 0.0f;

        try {
            map.put("supplyId", supplyId);
            map.put("vatPer", vatPer);
            vat = (String) getSqlMapClientTemplate().queryForObject("report.getPOVatTotalAmount", map);
            if (vat != null) {
                vatValue = Float.parseFloat(vat);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVatPercentages Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getPOVatTotalAmount", sqlException);
        }
        return vatValue;
    }

    public ArrayList gettopProblem(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList gettopProblem = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("mfrId", Integer.parseInt(repTO.getMfrId()));
            map.put("usageTypeId", repTO.getUsageTypeId());
            map.put("age", Integer.parseInt(repTO.getAge()));

            gettopProblem = (ArrayList) getSqlMapClientTemplate().queryForList("report.gettopProblem", map);
            System.out.println("gettopProblem" + gettopProblem.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("gettopProblem Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "gettopProblem", sqlException);
        }
        return gettopProblem;
    }

    //Hari
    public ArrayList getRcExpenseData(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList rcExpenseSummary = new ArrayList();
        try {
            System.out.println("In getRcExpenseData DAO");
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("vendorId", repTO.getVendorId());

            rcExpenseSummary = (ArrayList) getSqlMapClientTemplate().queryForList("report.getRcExpenseData", map);
            System.out.println("rcExpenseSummary" + rcExpenseSummary.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRcExpenseData Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getRcExpenseData", sqlException);
        }
        return rcExpenseSummary;
    }

    public ArrayList getColourList() {
        Map map = new HashMap();
        ArrayList colourList = new ArrayList();
        try {
            System.out.println("In getColourList DAO");

            colourList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getColourList", map);
            System.out.println("colourList Report size=" + colourList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getColourList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getColourList", sqlException);
        }
        return colourList;
    }

    public ArrayList getScrapGraphData(String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList scrapValue = new ArrayList();
        try {
            System.out.println("In getScrapGraphData DAO");
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            System.out.println(" repTO.getFromDate()=" + fromDate + "()" + toDate);
            scrapValue = (ArrayList) getSqlMapClientTemplate().queryForList("report.getScrapGraphData", map);
            System.out.println("scrapValue Report size=" + scrapValue.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getScrapGraphData Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getScrapGraphData", sqlException);
        }
        return scrapValue;
    }

    public ArrayList getContractVendor(int vendorId) {
        Map map = new HashMap();
        ArrayList contractVendor = new ArrayList();
        try {
            System.out.println("In getContractVendor DAO");
            map.put("vendorId", vendorId);

            System.out.println(" repTO.vendorId()=" + vendorId);
            contractVendor = (ArrayList) getSqlMapClientTemplate().queryForList("report.getContractVendor", map);
            System.out.println("contractVendor Report size=" + contractVendor.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractVendor Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getContractVendor", sqlException);
        }
        return contractVendor;
    }

    public ArrayList getExternalLabourBillGraphData(String fromDate, String toDate, int vendorId) {
        Map map = new HashMap();
        ArrayList externalLabour = new ArrayList();
        try {
            System.out.println("In getExternalLabourBillGraphData DAO");
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("vendorId", vendorId);
            System.out.println(" repTO.getFromDate()=" + fromDate + "()" + toDate);
            externalLabour = (ArrayList) getSqlMapClientTemplate().queryForList("report.getExternalLabourBillGraphData", map);
            System.out.println("externalLabour Report size=" + externalLabour.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getExternalLabourBillGraphData Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getExternalLabourBillGraphData", sqlException);
        }
        return externalLabour;
    }

    public ArrayList getRcTrendGraphData(String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList rcServiceData = new ArrayList();
        try {
            System.out.println("In getRcTrendGraphData DAO");
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);

            System.out.println(" repTO.getFromDate()=" + fromDate + "()" + toDate);
            rcServiceData = (ArrayList) getSqlMapClientTemplate().queryForList("report.getRcTrendGraphData", map);
            System.out.println("rcServiceData Report size=" + rcServiceData.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRcTrendGraphData Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getRcTrendGraphData", sqlException);
        }
        return rcServiceData;
    }

    public ArrayList getVehicleServiceGraphData(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList vehicleServiceData = new ArrayList();
        try {
            System.out.println("In getVehicleServiceGraphData DAO");
            System.out.println("reportTO.getFromDate()" + reportTO.getFromDate() + "-"
                    + reportTO.getToDate() + "-" + reportTO.getMfrid() + "-"
                    + reportTO.getUsageid() + "-" + reportTO.getTypeId()
                    + "-" + reportTO.getLife());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("mfrId", reportTO.getMfrid());
            map.put("usageType", reportTO.getUsageid());
            map.put("vehicleType", reportTO.getTypeId());
            map.put("year", reportTO.getLife());

            if (reportTO.getMfrid() != 0 && reportTO.getUsageid() == 0 && reportTO.getTypeId() == 0 && reportTO.getLife() == 0) {
                System.out.println("Mfr Value is Only given Here");
                vehicleServiceData = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleModelServiceGraphData", map);
            } else {
                System.out.println("Mfr Value is O Here anything to be executed");
                vehicleServiceData = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleServiceGraphData", map);
            }

            System.out.println("vehicleServiceData Report size=" + vehicleServiceData.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleServiceGraphData Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleServiceGraphData", sqlException);
        }
        return vehicleServiceData;
    }

    public ArrayList getMileageGraphData(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList mileageData = new ArrayList();
        try {
            System.out.println("In getMileageGraphData DAO");
            System.out.println(
                    reportTO.getToDate() + "-" + reportTO.getMfrid() + "-"
                    + reportTO.getUsageid() + "-" + reportTO.getTypeId()
                    + "-" + reportTO.getLife());

            map.put("toDate", reportTO.getToDate());
            map.put("mfrId", reportTO.getMfrid());
            map.put("usageType", reportTO.getUsageid());
            map.put("vehicleType", reportTO.getTypeId());
            map.put("year", reportTO.getLife());

            //  if(reportTO.getMfrid()!=0 && reportTO.getUsageid()==0 && reportTO.getTypeId()==0 && reportTO.getLife()==0)
            {
                System.out.println("Mfr Value is Only given Here");
                mileageData = (ArrayList) getSqlMapClientTemplate().queryForList("report.getMileageGraphData", map);
            }
            //else
            {
                //   System.out.println("Mfr Value is O Here anything to be executed");
                // mileageData = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleServiceGraphData", map);
            }

            System.out.println("mileageData Report size=" + mileageData.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleServiceGraphData Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleServiceGraphData", sqlException);
        }
        return mileageData;
    }

    public ArrayList getRcVendorList() {
        Map map = new HashMap();
        ArrayList rcVendorList = new ArrayList();
        try {
            System.out.println("In getRcVendorList DAO");
            rcVendorList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getRcVendorList", map);
            System.out.println("getRcVendorList  size=" + rcVendorList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRcVendorList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getRcVendorList", sqlException);
        }
        return rcVendorList;
    }

    public ArrayList getRcExpenseAmount(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList rcItemIdList = new ArrayList();
        try {
            System.out.println("In getRcExpenseAmount DAO");
            System.out.println(
                    reportTO.getToDate() + "-" + reportTO.getFromDate() + "-"
                    + reportTO.getVendorId());
            int vendorId = Integer.valueOf(reportTO.getVendorId()).intValue();
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("vendorId", vendorId);

            rcItemIdList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getRcExpenseAmount", map);
            System.out.println("getRcExpenseAmount  size=" + rcItemIdList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRcExpenseItemList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getRcExpenseItemList", sqlException);
        }
        return rcItemIdList;
    }

    public Float getActualPrice(ReportTO reportTO) {
        Map map = new HashMap();
        Float actualPrice = 0.0f;
        Float price = 0.0f;
        int count = 0;
        try {
            System.out.println("In getActualPrice DAO");

            map.put("itemId", reportTO.getItemId());
            System.out.println("reportTO.getItemId()-->" + reportTO.getItemId());
            map.put("createdDate", reportTO.getCreatedDate());
            System.out.println("reportTO.getCreatedDate()-->" + reportTO.getCreatedDate());

            count = (Integer) getSqlMapClientTemplate().queryForObject("report.getItemCount", map);
            System.out.println("Count Of Items-->" + count);
            map.put("count", count);
            if (getSqlMapClientTemplate().queryForObject("report.getActualPrice", map) != null) {
                price = (Float) getSqlMapClientTemplate().queryForObject("report.getActualPrice", map);
            } else {
                price = 0.0f;
            }
            actualPrice = price * count;
            System.out.println("getActualPrice  size=" + actualPrice);

            System.out.println("Normal Exit");

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getActualPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getActualPrice", sqlException);
        }
        return actualPrice;
    }

    public int getItemId(String paplCode) {
        int itemId = 0;
        Map map = new HashMap();
        map.put("paplCode", paplCode);
        System.out.println("B4 Query" + paplCode);
        itemId = (Integer) getSqlMapClientTemplate().queryForObject("report.getItemId", map);
        System.out.println("A4 Query");
        System.out.println("Item_id-->" + itemId);
        return itemId;
    }

//bala
    public ArrayList getUsageTypewiseData(ReportTO report) {
        Map map = new HashMap();
        ArrayList usageList = new ArrayList();
        try {
            map.put("fromDate", report.getFromDate());
            map.put("toDate", report.getToDate());
            map.put("companyId", report.getCompanyId());
            System.out.println("From Date" + report.getFromDate() + "To Date" + report.getToDate() + "CompanyId" + report.getCompanyId());
            usageList = (ArrayList) getSqlMapClientTemplate().queryForList("report.usageTypewiseChartQry", map);
            System.out.println("UsageTypewiseData Report size=" + usageList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUsageTypewiseData Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getUsageTypewiseData", sqlException);
        }
        return usageList;
    }

//    public ArrayList getServiceTypewiseData(ReportTO report) {
//            Map map = new HashMap();
//            ArrayList serviceList = new ArrayList();
//            ArrayList serviceTypeListAll = new ArrayList();
//            ArrayList usageTypeName = new ArrayList();
//            ArrayList serviceType = null;
//            ArrayList mainServiceList=null;
//            try {
//                map.put("fromDate", report.getFromDate());
//                map.put("toDate", report.getToDate());
//                map.put("companyId", report.getCompanyId());
//                System.out.println("From Date"+report.getFromDate()+"To Date"+report.getToDate()+"CompanyId"+report.getCompanyId());
//                serviceType= (ArrayList) getSqlMapClientTemplate().queryForList("report.getServiceTypeList", map);
//                usageTypeName= (ArrayList) getSqlMapClientTemplate().queryForList("report.getUsageTypeName", map);
//                System.out.println("serviceType size="+serviceType.size());
//                System.out.println("usageType size="+usageTypeName.size());
//                Iterator itr=serviceType.iterator();
//                Iterator itr1=null;
//                while(itr.hasNext())
//                {
//                    report=new ReportTO();
//                    report=(ReportTO) itr.next();
//                    map.put("servieTypeId",report.getServiceTypeId());
//                    System.out.println("servieTypeId"+report.getServiceTypeId());
//                    itr1=usageTypeName.iterator();
//                    while(itr1.hasNext())
//                    {
//                    report=new ReportTO();
//                    report=(ReportTO) itr1.next();
//                    map.put("usageTypeId",report.getUsageTypeId());
//                    System.out.println("usageTypeId"+report.getUsageTypeId());
//                    serviceList = new ArrayList();
//                    serviceList = (ArrayList) getSqlMapClientTemplate().queryForList("report.serviceTypewiseChartQry", map);
//                    System.out.println("UsageTypewiseData serviceList size=" + serviceList.size());
//                    System.out.println("UsageTypewiseData serviceList Data=" + serviceList);
//                    serviceTypeListAll.addAll(serviceList);
//                    report.setServiceTypeListAll(serviceTypeListAll);
//                    System.out.println("serviceTypeListAll size=" + serviceTypeListAll.size());
//                    }
//                    System.out.println("serviceTypeListAll size="+serviceTypeListAll.size());
//                    mainServiceList = new ArrayList();
//                    mainServiceList.addAll(serviceTypeListAll);
//                    System.out.println("mainServiceList="+mainServiceList.size());
//                }
//                System.out.println("mainServiceList Size"+mainServiceList.size());
//               } catch (Exception sqlException) {
//                /*
//                 * Log the exception and propagate to the calling class
//                 */
//                FPLogUtils.fpDebugLog("getUsageTypewiseData Error" + sqlException.toString());
//                FPLogUtils.fpErrorLog("sqlException" + sqlException);
//                throw new FPRuntimeException("EM-GEN-01", CLASS, "getServiceTypewiseData", sqlException);
//            }
//            return mainServiceList;
//        }
// bala ends
    public ArrayList getTallyXMLSummary(int companyId, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList tallyXMLSummary = new ArrayList();
        try {
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("companyId", Integer.valueOf(companyId));
            System.out.println("From Date" + fromDate + "To Date" + toDate + "CompanyId" + Integer.valueOf(companyId));
            tallyXMLSummary = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTallyXMLDetails", map);
            System.out.println("tallyXMLSummary Report size=" + tallyXMLSummary.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTallyXMLSummary Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTallyXMLSummary", sqlException);
        }
        return tallyXMLSummary;
    }

    public int getTallyXMLReport(ReportTO report) {
        Map map = new HashMap();
        int tallyXMLId = 0;
        try {
            map.put("fromDate", report.getFromDate());
            map.put("toDate", report.getToDate());
            map.put("reqDate", report.getRequiredDate());
            map.put("dataType", report.getDataType());
            map.put("companyId", report.getCompanyId());
            map.put("activeInd", "Y");
            System.out.println("From Date" + report.getFromDate() + "To Date" + report.getToDate() + "ReqDate" + report.getRequiredDate() + "CompanyId" + report.getCompanyId() + "DataType" + report.getDataType());
            System.out.println("insertStatus B4--->" + tallyXMLId);
            tallyXMLId = Integer.valueOf(getSqlMapClientTemplate().update("report.getTallyXMLId", map)).intValue();
            System.out.println("insertStatus B4--->" + tallyXMLId);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTallyXMLId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTallyXMLId", sqlException);
        }
        return tallyXMLId;
    }

    public ArrayList getModifyTallyXMLPage(String xmlId) {
        Map map = new HashMap();
        ArrayList modifyTallyXMLPage = new ArrayList();
        try {
            map.put("xmlId", xmlId);
            System.out.println("xmlId" + xmlId);
            modifyTallyXMLPage = (ArrayList) getSqlMapClientTemplate().queryForList("report.getModifyTallyXMLPage", map);
            System.out.println("modifyTallyXMLPage Report size=" + modifyTallyXMLPage.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getModifyTallyXMLPage Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getModifyTallyXMLPage", sqlException);
        }
        return modifyTallyXMLPage;
    }

    public int getModifyTallyXMLReport(ReportTO report, String xmlId) {
        Map map = new HashMap();
        int modifyTallyXMLId = 0;
        try {
            map.put("fromDate", report.getFromDate());
            map.put("toDate", report.getToDate());
            map.put("reqDate", report.getRequiredDate());
            map.put("dataType", report.getDataType());
            map.put("xmlId", xmlId);
            System.out.println("From Date" + report.getFromDate() + "To Date" + report.getToDate() + "ReqDate" + report.getRequiredDate() + "DataType" + report.getDataType());
            System.out.println("insertStatus B4--->" + modifyTallyXMLId);
            modifyTallyXMLId = Integer.valueOf(getSqlMapClientTemplate().update("report.getModifyTallyXMLId", map)).intValue();
            System.out.println("insertStatus B4--->" + modifyTallyXMLId);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getModifyTallyXMLId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getModifyTallyXMLId", sqlException);
        }
        return modifyTallyXMLId;
    }

    public String handleDriverSettlement(String driName) {
        Map map = new HashMap();
        map.put("driName", driName);
        String suggestions = "";
        ReportTO repTO = new ReportTO();
        try {
            ArrayList driverNames = new ArrayList();
            System.out.println("map = " + map);
            driverNames = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDriverNames", map);
            Iterator itr = driverNames.iterator();
            while (itr.hasNext()) {
                repTO = new ReportTO();
                repTO = (ReportTO) itr.next();
                suggestions = repTO.getDriName() + "~" + suggestions;
            }
            if ("".equals(suggestions)) {
                suggestions = "no match found";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverNames Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getDriverNames", sqlException);
        }
        return suggestions;
    }

    public String handleVehicleNo(String regno) {
        Map map = new HashMap();
        map.put("regno", regno);
        String suggestions = "";
        ReportTO repTO = new ReportTO();
        try {
            ArrayList vehicleNo = new ArrayList();
            System.out.println("map = " + map);
            vehicleNo = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleNo", map);
            Iterator itr = vehicleNo.iterator();
            while (itr.hasNext()) {
                repTO = new ReportTO();
                repTO = (ReportTO) itr.next();
                suggestions = repTO.getRegisterNo() + "~" + suggestions;
            }
            if ("".equals(suggestions)) {
                suggestions = "no match found";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleNo", sqlException);
        }
        return suggestions;
    }

    public ArrayList getDriverSettlementReport(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList driverSettlementReport = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            String driName = "%" + reportTO.getDriName() + "%";
            map.put("driName", driName);
            map.put("registerNo", reportTO.getRegisterNo());
            System.out.println("map = " + map);
            driverSettlementReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDriverSettlementReport", map);
            System.out.println("driverSettlementReport=" + driverSettlementReport.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverSettlementReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getDriverSettlementReport", sqlException);
        }
        return driverSettlementReport;
    }

    //Rathimeena Code Start
    public ArrayList searchProDriverSettlement(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList proDriverSettlement = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);

        System.out.println("searchProDriverSettlement....: " + map);
        System.out.println("fromDate" + fromDate);
        System.out.println("toDate" + toDate);
        try {
            if (getSqlMapClientTemplate().queryForList("report.proDriverSettlement", map) != null) {
                proDriverSettlement = (ArrayList) getSqlMapClientTemplate().queryForList("report.proDriverSettlement", map);
            }
            System.out.println("searchProDriverSettlement size=" + proDriverSettlement.size());
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("searchProDriverSettlement Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "searchProDriverSettlement", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("searchProDriverSettlement Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "searchProDriverSettlement", sqlException);
        }
        return proDriverSettlement;
    }

    public ArrayList cleanerTripDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList cleanerTrip = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        try {
            if (getSqlMapClientTemplate().queryForList("report.cleanerTripDetails", map) != null) {
                cleanerTrip = (ArrayList) getSqlMapClientTemplate().queryForList("report.cleanerTripDetails", map);
            }
            System.out.println("searchProDriverSettlement size=" + cleanerTrip.size());
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("cleanerTripDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "cleanerTripDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("cleanerTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "cleanerTripDetails", sqlException);
        }
        return cleanerTrip;
    }

    public ArrayList getFixedExpDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList fixedExpDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);

        try {
            if (getSqlMapClientTemplate().queryForList("report.getFixedExpDetails", map) != null) {
                fixedExpDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getFixedExpDetails", map);
                System.out.println("fixedExpDetails.size().. DAO : " + fixedExpDetails.size());
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAdvDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAdvDetails", sqlException);
        }
        return fixedExpDetails;
    }

    public ArrayList getDriverExpDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList driverExpDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        try {
            if (getSqlMapClientTemplate().queryForList("report.getDriverExpDetails", map) != null) {
                driverExpDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDriverExpDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAdvDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAdvDetails", sqlException);
        }
        return driverExpDetails;
    }

    public ArrayList getAdvDetails(String fromDate, String toDate, String regNo, int driId, String tripIds) {
        Map map = new HashMap();
        ArrayList AdvDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        try {
            if (getSqlMapClientTemplate().queryForList("report.getAdvDetails", map) != null) {
                AdvDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getAdvDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAdvDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAdvDetails", sqlException);
        }
        return AdvDetails;
    }

    public ArrayList getFuelDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList fuelDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        try {
            /*if (getSqlMapClientTemplate().queryForList("operation.getAdvDetails", map) != null) {
             fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getFuelDetails", map);
             }*/
            if (getSqlMapClientTemplate().queryForList("report.getFuelsDetail", map) != null) {
                fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getFuelsDetail", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelDetails", sqlException);
        }
        return fuelDetails;
    }

    public ArrayList getHaltDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList fuelDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        try {
            /*if (getSqlMapClientTemplate().queryForList("operation.getAdvDetails", map) != null) {
             fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getFuelDetails", map);
             }*/
            if (getSqlMapClientTemplate().queryForList("report.getHaltDetails", map) != null) {
                fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getHaltDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return fuelDetails;
    }

    public ArrayList getRemarkDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList fuelDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        try {
            /*if (getSqlMapClientTemplate().queryForList("operation.getAdvDetails", map) != null) {
             fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getFuelDetails", map);
             }*/
            if (getSqlMapClientTemplate().queryForList("report.getRemarkDetails", map) != null) {
                fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getRemarkDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRemarkDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRemarkDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRemarkDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRemarkDetails", sqlException);
        }
        return fuelDetails;
    }

    public ArrayList getSummaryDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList fuelDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        try {
            /*if (getSqlMapClientTemplate().queryForList("operation.getAdvDetails", map) != null) {
             fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getFuelDetails", map);
             }*/
            if (getSqlMapClientTemplate().queryForList("report.getSummaryDetails", map) != null) {
                fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getSummaryDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRemarkDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRemarkDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRemarkDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRemarkDetails", sqlException);
        }
        return fuelDetails;
    }

    public int insertEposTripExpenses(String tripId, String expenseDesc, String expenseAmt, String expenseDate, String expenseRemarks, int userId) {
        Map map = new HashMap();
        int lastInsertedId = 0;
        try {
            //map.put("tripId", tripId);
            map.put("tripId", tripId);
            map.put("expenseDesc", expenseDesc);
            map.put("expenseAmt", expenseAmt);
            map.put("expenseDate", expenseDate);
            map.put("expenseRemarks", expenseRemarks);
            map.put("userId", userId);
            map.put("flag", "0");
            System.out.println("map in insertEposTripExpenses " + map);
            lastInsertedId = (Integer) getSqlMapClientTemplate().update("report.insertEposTripExpenses", map);
            System.out.println("lastInsertedId -->" + lastInsertedId);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return lastInsertedId;
    }

    public int getTripCountDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int tripCount = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("report.getTripCountDetails", map) != null) {
                tripCount = (Integer) getSqlMapClientTemplate().queryForObject("report.getTripCountDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return tripCount;
    }

    public int getTotalTonnageDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList settleDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int totalTonnage = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("report.getTotalTonnageDetails", map) != null) {
                totalTonnage = (Integer) getSqlMapClientTemplate().queryForObject("report.getTotalTonnageDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return totalTonnage;
    }

    public int getOutKmDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int outKm = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("report.getOutKmDetails", map) != null) {
                outKm = (Integer) getSqlMapClientTemplate().queryForObject("report.getOutKmDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return outKm;
    }

    public int getInKmDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int inKm = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("report.getInKmDetails", map) != null) {
                inKm = (Integer) getSqlMapClientTemplate().queryForObject("report.getInKmDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return inKm;
    }

    public ArrayList getTotFuelDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList totalFuelDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int totFuel = 0;
        try {
            if (getSqlMapClientTemplate().queryForList("report.getTotFuelDetails", map) != null) {
                totalFuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTotFuelDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return totalFuelDetails;
    }

    public int getDriverExpenseDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int driExpense = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("report.getDriverExpenseDetails", map) != null) {
                driExpense = (Integer) getSqlMapClientTemplate().queryForObject("report.getDriverExpenseDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return driExpense;
    }

    public int getDriverSalaryDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int driExpense = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("report.getDriverSalaryDetails", map) != null) {
                driExpense = (Integer) getSqlMapClientTemplate().queryForObject("report.getDriverSalaryDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return driExpense;
    }

    public int getGeneralExpenseDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int genExpense = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("operation.getGeneralExpenseDetails", map) != null) {
                genExpense = (Integer) getSqlMapClientTemplate().queryForObject("operation.getGeneralExpenseDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return genExpense;
    }

    public int getDriverAdvanceDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int driAdvance = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("report.getDriverAdvanceDetails", map) != null) {
                driAdvance = (Integer) getSqlMapClientTemplate().queryForObject("report.getDriverAdvanceDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return driAdvance;
    }

    public int getDriverBataDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int driAdvance = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("report.getDriverBataDetails", map) != null) {
                driAdvance = (Integer) getSqlMapClientTemplate().queryForObject("report.getDriverBataDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return driAdvance;
    }

    public ArrayList getSettleCloseDetails(String fromDate, String toDate, String regNo, String driName) {
        Map map = new HashMap();
        ArrayList fuelDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        String dName = "%" + driName + "%";
        map.put("driName", dName);
        try {
            /*if (getSqlMapClientTemplate().queryForList("report.getAdvDetails", map) != null) {
             fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getFuelDetails", map);
             }*/
            if (getSqlMapClientTemplate().queryForList("report.getSettlementCloseDetails", map) != null) {
                fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getSettlementCloseDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRemarkDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRemarkDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRemarkDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRemarkDetails", sqlException);
        }
        return fuelDetails;
    }
    //Rathimeena Code End

    public ArrayList getVehicleCurrentStatus(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList vehicleCurrentStatus = new ArrayList();
        try {
            map.put("statusDate", reportTO.getStatusDate());
            map.put("statusId", reportTO.getStatusId());
            map.put("locationId", reportTO.getLocationId());
            String regNo = "%" + reportTO.getRegisterNo() + "%";
            map.put("regNo", regNo);
            System.out.println("Vehicle Status map = " + map);

            vehicleCurrentStatus = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleCurrentStatus", map);
            System.out.println("vehicleCurrentStatus=" + vehicleCurrentStatus.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleCurrentStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleCurrentStatus", sqlException);
        }
        return vehicleCurrentStatus;
    }

    public ArrayList getCompanyWiseTripReport(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList companyWiseTripReport = new ArrayList();
        try {
            System.out.println("reportTO.getCompanyId(): " + reportTO.getCompanyId());
            System.out.println("reportTO.getFromDate(): " + reportTO.getFromDate());
            System.out.println("reportTO.getToDate(): " + reportTO.getToDate());
            map.put("customerId", reportTO.getCompanyId());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            System.out.println("Company Wise Trip map = " + map);

            companyWiseTripReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCompanyWiseTripReport", map);
            System.out.println("companyWiseTripReport=" + companyWiseTripReport.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCompanyWiseTripReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getCompanyWiseTripReport", sqlException);
        }
        return companyWiseTripReport;
    }

    public ArrayList getVehicleList() {
        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        try {
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCompanyWiseTripReport", map);
            System.out.println("vehicleList=" + vehicleList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getCompanyWiseTripReport", sqlException);
        }
        return vehicleList;
    }

    public ArrayList getVehiclePerformance(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList vehiclePerformance = new ArrayList();
        String regNo = "%" + reportTO.getRegNo() + "%";
        map.put("regNo", regNo);
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        try {
            System.out.println("getVehiclePerformance...map: " + map);
            vehiclePerformance = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehiclePerformance", map);
            System.out.println("vehiclePerformance alist: " + vehiclePerformance.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehiclePerformance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehiclePerformance", sqlException);
        }
        return vehiclePerformance;
    }

    public ArrayList getDriverPerformance(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList driverPerformance = new ArrayList();
        map.put("driId", reportTO.getDriverId());
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        try {
            System.out.println("getDriverPerformance...map: " + map);
            driverPerformance = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDriverPerformance", map);
            System.out.println("driverPerformance alist: " + driverPerformance.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverPerformance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getDriverPerformance", sqlException);
        }
        return driverPerformance;
    }

    public String getDriverEmbarkedDate(ReportTO reportTO) {
        Map map = new HashMap();
        String driEmbarked = "";
        map.put("driId", reportTO.getDriId());
        try {
            System.out.println("getDriverEmbarkedDate...map: " + map);
            driEmbarked = (String) getSqlMapClientTemplate().queryForObject("report.getDriverEmbarkedDate", map);
            System.out.println("getDriverEmbarkedDate alist: " + driEmbarked);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverEmbarkedDate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getDriverEmbarkedDate", sqlException);
        }
        return driEmbarked;
    }

    public ArrayList getPeriodicServiceVehicleList(int compId, String regNo, String fromDate) {
        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        ArrayList periodicServiceList = new ArrayList();
        ArrayList finalVehicleList = new ArrayList();
        try {
            OperationTO repTO = null;
            map.put("regNo", regNo);
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getPeriodicServiceVehicleList", map);
            System.out.println("getPeriodicServiceVehicleList size=" + vehicleList.size());

            Iterator itr = vehicleList.iterator();
            while (itr.hasNext()) {
                periodicServiceList = new ArrayList();
                repTO = new OperationTO();
                repTO = (OperationTO) itr.next();
                map.put("mfrId", repTO.getMfrId());
                map.put("modelId", repTO.getModel());
                map.put("vehicleTypeId", repTO.getVehicleTypeId());
                map.put("vehicleId", repTO.getVehicleId());
                map.put("currentKM", repTO.getCurrentKM());
                System.out.println("repTO.getVehicleId():" + repTO.getVehicleId());
                System.out.println("repTO.getCurrentKM():" + repTO.getCurrentKM());
                periodicServiceList = (ArrayList) getSqlMapClientTemplate().queryForList("report.vehiclePeriodicServiceList", map);
                System.out.println("periodicServiceList size=" + periodicServiceList.size());
                repTO.setPeriodicServiceList(periodicServiceList);
                //finalVehicleList.add(repTO);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPeriodicServiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getPeriodicServiceList", sqlException);
        }
        return vehicleList;
    }

    public ArrayList getCompanyList() {
        Map map = new HashMap();
        ArrayList companyList = new ArrayList();
        try {
            companyList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCompanyList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCompanyList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getCompanyList", sqlException);
        }
        return companyList;
    }

    public ArrayList getDueList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList dueList = new ArrayList();
        try {
            map.put("regNo", repTO.getRegNo());
            map.put("companyId", repTO.getCompanyId());
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("dueIn", repTO.getDueIn());
            System.out.println("regno" + repTO.getRegNo());
            System.out.println("companyId" + repTO.getCompanyId());
            System.out.println("fromDate" + repTO.getFromDate());
            System.out.println("toDate" + repTO.getToDate());
            dueList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getFcDues", map);
            System.out.println("dueList size=" + dueList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("dueList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "dueList", sqlException);
        }
        return dueList;
    }

    public ArrayList getInsuranceDueList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList dueList = new ArrayList();
        try {
            map.put("regNo", repTO.getRegNo());
            map.put("companyId", repTO.getCompanyId());
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("dueIn", repTO.getDueIn());
            System.out.println("regno" + repTO.getRegNo());
            System.out.println("companyId" + repTO.getCompanyId());
            System.out.println("fromDate" + repTO.getFromDate());
            System.out.println("toDate" + repTO.getToDate());
            dueList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getInsuranceDues", map);
            System.out.println("dueList size=" + dueList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("dueList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "dueList", sqlException);
        }
        return dueList;
    }

    public ArrayList getRoadTaxDueList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList dueList = new ArrayList();
        try {
            map.put("regNo", repTO.getRegNo());
            map.put("companyId", repTO.getCompanyId());
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("dueIn", repTO.getDueIn());
            System.out.println("regno" + repTO.getRegNo());
            System.out.println("companyId" + repTO.getCompanyId());
            System.out.println("fromDate" + repTO.getFromDate());
            System.out.println("toDate" + repTO.getToDate());
            dueList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getRoadTaxDueList", map);
            System.out.println("dueList size=" + dueList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("dueList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "dueList", sqlException);
        }
        return dueList;
    }

    public ArrayList getPermitDueList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList dueList = new ArrayList();
        try {
            map.put("regNo", repTO.getRegNo());
            map.put("companyId", repTO.getCompanyId());
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("dueIn", repTO.getDueIn());
            System.out.println("regno" + repTO.getRegNo());
            System.out.println("companyId" + repTO.getCompanyId());
            System.out.println("fromDate" + repTO.getFromDate());
            System.out.println("toDate" + repTO.getToDate());
            dueList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getPermitDueList", map);
            System.out.println("dueList size=" + dueList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("dueList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "dueList", sqlException);
        }
        return dueList;
    }

    public ArrayList processAMCDueList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList dueList = new ArrayList();
        try {
            map.put("regNo", repTO.getRegNo());
            map.put("companyId", repTO.getCompanyId());
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("dueIn", repTO.getDueIn());
            System.out.println("regno" + repTO.getRegNo());
            System.out.println("companyId" + repTO.getCompanyId());
            System.out.println("fromDate" + repTO.getFromDate());
            System.out.println("toDate" + repTO.getToDate());
            dueList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getAMCDueList", map);
            System.out.println("dueList size=" + dueList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("dueList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "dueList", sqlException);
        }
        return dueList;
    }

    /**
     * This method used to LPS Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getlpsCount(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList lpsCount = new ArrayList();
        try {
            map.put("LpsId", reportTO.getLpsId());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            System.out.println("map = " + map);
            lpsCount = (ArrayList) getSqlMapClientTemplate().queryForList("report.getlpsCount", map);
            System.out.println("lpsCount size=" + lpsCount.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("lpsCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "lpsCount", sqlException);
        }
        return lpsCount;
    }

    public ArrayList getlpsTrippedList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList lpsTrippedList = new ArrayList();
        try {
            map.put("LpsId", reportTO.getLpsId());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            System.out.println("map = " + map);
            lpsTrippedList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getlpsTrippedList", map);
            System.out.println("lpsTrippedList size=" + lpsTrippedList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("lpsTrippedList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "lpsTrippedList", sqlException);
        }
        return lpsTrippedList;
    }

    public ArrayList gettripList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList tripList = new ArrayList();
        try {
            map.put("LpsId", reportTO.getLpsId());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            System.out.println("map = " + map);
            tripList = (ArrayList) getSqlMapClientTemplate().queryForList("report.gettripList", map);
            System.out.println("lpsTrippedList size=" + tripList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tripList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "tripList", sqlException);
        }
        return tripList;
    }

    /**
     * This method used to LPS Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getlocationList() {
        Map map = new HashMap();
        ArrayList locationList = new ArrayList();
        try {
            locationList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getlocationList", map);
            System.out.println("locationList size=" + locationList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("locationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "locationList", sqlException);
        }
        return locationList;
    }

    public ArrayList getTripWisePandL(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList tripWiseList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("companyId", reportTO.getCompanyId());
            map.put("userId", userId);
            System.out.println("map = " + map);
            tripWiseList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripWisePandL", map);
            System.out.println("tripWiseList size=" + tripWiseList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripWisePandL Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripWisePandL", sqlException);
        }
        return tripWiseList;
    }

    public ArrayList getTripWisetotal(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList tripWiseTotal = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("companyId", reportTO.getCompanyId());
            map.put("userId", userId);
            System.out.println("map = " + map);
            tripWiseTotal = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripWisetotal", map);
            System.out.println("tripWiseList size=" + tripWiseTotal.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripWisePandL Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripWisePandL", sqlException);
        }
        return tripWiseTotal;
    }

    /**
     * This method used to LPS Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleWisePandL(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleWiseList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("companyId", reportTO.getCompanyId());
            map.put("vehicleType", reportTO.getVehicleType());
            map.put("vehicleId", reportTO.getVehicleId());
            map.put("userId", userId);
            System.out.println("map =44444444====> " + map);
            vehicleWiseList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleWisePandL", map);
            System.out.println("tripWiseList size=" + vehicleWiseList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleWisePandL Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleWisePandL", sqlException);
        }
        return vehicleWiseList;
    }

    public ArrayList getVehicleWisetotal(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList tripWiseTotal = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("companyId", reportTO.getCompanyId());
            map.put("vehicleType", reportTO.getVehicleType());
            map.put("vehicleId", reportTO.getVehicleId());
            map.put("userId", userId);
            System.out.println("map = " + map);
            tripWiseTotal = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleWisetotal", map);
            System.out.println("tripWiseList size=" + tripWiseTotal.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleWisetotal Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleWisetotal", sqlException);
        }
        return tripWiseTotal;
    }

    public ArrayList getCustomerList() {
        Map map = new HashMap();
        ArrayList customerList = new ArrayList();
        try {
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCustomerList", map);
            //  System.out.println("customerTypes size=" + customerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getServiceTypes Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getServiceTypes List", sqlException);
        }

        return customerList;
    }

    public ArrayList getDistrictNameList() {
        Map map = new HashMap();
        ArrayList districtNameList = new ArrayList();
        try {
            districtNameList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDistrictNameList", map);
            //System.out.println("districtNameList size=" + districtNameList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("districtNameList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "districtNameList List", sqlException);
        }

        return districtNameList;
    }

    /**
     * This method used to get District Wise Summart Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getDistrictSummaryList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList districtSummaryList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("ownership", reportTO.getOwnership());
            map.put("district", reportTO.getDistrict());
            map.put("consignmentType", reportTO.getConsignmentType());
            map.put("userId", userId);
            // System.out.println("map =44444444====> " + map);
            districtSummaryList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDistrictSummaryList", map);
            //   System.out.println("districtSummaryList size=" + districtSummaryList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDistrictSummaryList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getDistrictSummaryList", sqlException);
        }
        return districtSummaryList;
    }

    /**
     * This method used to get District Wise Summart Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getDistrictDetailsList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList districtDetailsList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("ownership", reportTO.getOwnership());
            map.put("consignmentType", reportTO.getConsignmentType());
            map.put("district", reportTO.getDistrict());
            map.put("userId", userId);
            //  System.out.println("map =44444444====> " + map);
            districtDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDistrictDetailsList", map);
            //  System.out.println("districtDetailsList size=" + districtDetailsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDistrictDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getDistrictDetailsList", sqlException);
        }
        return districtDetailsList;
    }

    /**
     * This method used to get Tripsheet Wise Summart Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getTripsheetDetailsList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList tripSheetDetailsList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("ownership", reportTO.getOwnership());
            map.put("consignmentType", reportTO.getConsignmentType());
            map.put("userId", userId);
            //System.out.println("map =44444444====> " + map);
            tripSheetDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("report.tripSheetDetailsList", map);
            // System.out.println("tripSheetDetailsList size=" + tripSheetDetailsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsheetDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripsheetDetailsList", sqlException);
        }
        return tripSheetDetailsList;
    }

    /**
     * This method used to get Consignee Wise Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getConsigneeWiseList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList consigneeWiseDetailsList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            String consigneeName = "%" + reportTO.getConsigneeName() + "%";
            map.put("consigneeName", consigneeName);
            map.put("userId", userId);
            // System.out.println("map =44444444====> " + map);
            consigneeWiseDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getConsigneeWiseList", map);
            //  System.out.println("consigneeWiseDetailsList size=" + consigneeWiseDetailsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsigneeWiseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getConsigneeWiseList", sqlException);
        }
        return consigneeWiseDetailsList;
    }

    /**
     * This method used to get Tripsettlement Wise Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getTripsettlementWiseList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList tripsettlementDetailsList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("tripId", reportTO.getTripId());
            map.put("vehicleNo", reportTO.getVehicleNo());
            map.put("ownership", reportTO.getOwnership());
            map.put("userId", userId);
            // System.out.println("map =44444444====> " + map);
            tripsettlementDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripsettlementWiseList", map);
            //  System.out.println("tripsettlementDetailsList size=" + tripsettlementDetailsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsettlementWiseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripsettlementWiseList", sqlException);
        }
        return tripsettlementDetailsList;
    }

    public ArrayList getDieselReportList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList dieselReportList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("tripId", reportTO.getTripId());
            map.put("vehicleNo", reportTO.getVehicleNo());
            map.put("userId", userId);
            // System.out.println("map =44444444====> " + map);
            dieselReportList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDieselReportList", map);
            System.out.println("dieselReportList size=" + dieselReportList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDieselReportList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getDieselReportList", sqlException);
        }
        return dieselReportList;
    }

    /**
     * This method used to get ConsigneeName Suggestions..
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getConsigneeNameSuggestions(String consigneeName) {
        Map map = new HashMap();
        map.put("consigneeName", "%" + consigneeName + "%");
        String suggestions = "";
        ReportTO reportTO = new ReportTO();
        try {
            ArrayList consigneeNameList = new ArrayList();
            //System.out.println("map = " + map);
            consigneeNameList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getConsigneeNameSuggestions", map);
            Iterator itr = consigneeNameList.iterator();
            while (itr.hasNext()) {
                reportTO = new ReportTO();
                reportTO = (ReportTO) itr.next();
                //System.out.println("repTO.getConsigneeName() = " + reportTO.getConsigneeName());
                suggestions = reportTO.getConsigneeName() + "~" + suggestions;
                //System.out.println("suggestions = " + suggestions);
            }
            if ("".equals(suggestions)) {
                suggestions = "no match found";
            } else if (!"".equals(suggestions)) {
                suggestions = consigneeName + "~" + suggestions;
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsigneeNameSuggestions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getConsigneeNameSuggestions", sqlException);
        }
        return suggestions;
    }

    /**
     * This method used to get Trip Id Suggestions..
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getTripIdSuggestions(String tripId) {
        Map map = new HashMap();
        map.put("tripId", "%" + tripId + "%");
        String suggestions = "";
        ReportTO reportTO = new ReportTO();
        try {
            ArrayList tripIdList = new ArrayList();
            System.out.println("map = " + map);
            tripIdList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripIdSuggestions", map);
            Iterator itr = tripIdList.iterator();
            while (itr.hasNext()) {
                reportTO = new ReportTO();
                reportTO = (ReportTO) itr.next();
                suggestions = reportTO.getTripId() + "~" + suggestions;
            }
            if ("".equals(suggestions)) {
                suggestions = "no match found";
            } else if (!"".equals(suggestions)) {
                suggestions = tripId + "~" + suggestions;
            }
            System.out.println("suggestions = " + suggestions);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripIdSuggestions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripIdSuggestions", sqlException);
        }
        return suggestions;
    }

    /**
     * This method used to get Trip Id Suggestions..
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getVehicleNumberSuggestions(String vehicleNo) {
        Map map = new HashMap();
        map.put("vehicleNo", "%" + vehicleNo + "%");
        String suggestions = "";
        ReportTO reportTO = new ReportTO();
        try {
            ArrayList vehicleNoList = new ArrayList();
            //System.out.println("map = " + map);
            vehicleNoList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleNumberSuggestions", map);
            Iterator itr = vehicleNoList.iterator();
            while (itr.hasNext()) {
                reportTO = new ReportTO();
                reportTO = (ReportTO) itr.next();
                suggestions = reportTO.getVehicleNo() + "~" + suggestions;
                //System.out.println("suggestions = " + suggestions);
            }
            if ("".equals(suggestions)) {
                suggestions = "no match found";
            } else if (!"".equals(suggestions)) {
                suggestions = vehicleNo + "~" + suggestions;
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleNumberSuggestions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleNumberSuggestions", sqlException);
        }
        return suggestions;
    }

    /**
     * This method used to get Account Head List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    /**
     * This method used to get Customer Wise Profit List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getCustomerOverDueList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList customerOverDueList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("customerId", reportTO.getCustomerId());
            if ("paid".equals(reportTO.getInvoiceType())) {
                map.put("invoiceType", "Paid");
            } else {
                map.put("invoiceType", "Not Paid");
            }
            System.out.println("map = " + map);
            customerOverDueList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCustomerOverDueList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAccountReceivableList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getAccountReceivableList", sqlException);
        }
        return customerOverDueList;
    }

    public ArrayList getSalesDashboard(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList response = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            System.out.println("map = " + map);
            response = (ArrayList) getSqlMapClientTemplate().queryForList("report.getSalesDashboard", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAccountReceivableList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getSalesDashboard", sqlException);
        }
        return response;
    }

    public ArrayList getOpsDashboard(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList response = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            System.out.println("map = " + map);
            response = (ArrayList) getSqlMapClientTemplate().queryForList("report.getOpsDashboard", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOpsDashboard Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getOpsDashboard", sqlException);
        }
        return response;
    }

    /**
     * This method used to get Account Head List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getAccountReceivableList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList accountReceivableList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("customerId", reportTO.getCustomerId());
            if ("paid".equals(reportTO.getInvoiceType())) {
                map.put("invoiceType", "Paid");
            } else {
                map.put("invoiceType", "Not Paid");
            }
            System.out.println("map = " + map);
            accountReceivableList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getAccountReceivableList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAccountReceivableList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getAccountReceivableList", sqlException);
        }
        return accountReceivableList;
    }

    /**
     * This method used to get Customer Wise Profit List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getCustomerWiseProfitList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList customerWiseProfitList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("customerId", reportTO.getCustomerId());
            System.out.println("map = " + map);
            customerWiseProfitList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCustomerWiseProfitList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerWiseProfitList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getCustomerWiseProfitList", sqlException);
        }
        return customerWiseProfitList;
    }

    /**
     * This method used to get Vehicle Wise Profit List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleDetailsList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleDetailsList = new ArrayList();
        try {
            map.put("vehicleId", reportTO.getVehicleId());
            map.put("fleetCenterId", reportTO.getFleetCenterId());
            System.out.println("map = " + map);
            vehicleDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleDetailsList", map);
            System.out.println("vehicleDetailsList.size() = " + vehicleDetailsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleDetailsList", sqlException);
        }
        return vehicleDetailsList;
    }

    /**
     * This method used to get Vehicle Earnings List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleEarningsList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleEarnings = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("vehicleId", reportTO.getVehicleId());
            System.out.println("map = " + map);
            vehicleEarnings = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleEarningsList", map);
            System.out.println("vehicleEarnings.size() = " + vehicleEarnings.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleEarningsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleEarningsList", sqlException);
        }
        return vehicleEarnings;
    }

    /**
     * This method used to get Vehicle Fixed Expense List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleFixedExpenseList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleEarnings = new ArrayList();
        double insuranceAmount = 0;
        double fcAmount = 0;
        double roadTaxAmount = 0;
        double permitAmount = 0;
        double emiAmount = 0;
        DprTO dprTO = new DprTO();
        String[] dateList = getDateList(reportTO.getFromDate(), reportTO.getToDate());
        ReportTO rpTO1 = null;
        try {
            map.put("vehicleId", reportTO.getVehicleId());
            int i = 0;
            for (int j = 0; j < dateList.length; j++) {
                rpTO1 = new ReportTO();
                rpTO1.setDate(dateList[j]);
                map.put("date", dateList[j]);
                //System.out.println("map = " + map);
                insuranceAmount += (Double) getSqlMapClientTemplate().queryForObject("report.getVehicleInsurance", map);
                //System.out.println("insuranceAmount = " + insuranceAmount);
                fcAmount += (Double) getSqlMapClientTemplate().queryForObject("report.getVehicleFc", map);
                //System.out.println("fcAmount = " + fcAmount);
                roadTaxAmount += (Double) getSqlMapClientTemplate().queryForObject("report.getVehicleRoadTax", map);
                //System.out.println("roadTaxAmount = " + roadTaxAmount);
                permitAmount += (Double) getSqlMapClientTemplate().queryForObject("report.getVehiclePermit", map);
                //System.out.println("permitAmount = " + permitAmount);
                emiAmount += (Double) getSqlMapClientTemplate().queryForObject("report.getVehicleEmi", map);
                //System.out.println("emiAmount = " + emiAmount);
                i++;
            }
            System.out.println("i in the date function= " + i);
            dprTO.setInsuranceAmount(insuranceAmount);
            dprTO.setFcAmount(fcAmount);
            dprTO.setRoadTaxAmount(roadTaxAmount);
            dprTO.setPermitAmount(permitAmount);
            dprTO.setEmiAmount(emiAmount);
            dprTO.setVehicleDriverSalary(reportTO.getVehicleDriverSalary() * i);
            dprTO.setFixedExpensePerDay((insuranceAmount + fcAmount + roadTaxAmount + permitAmount + emiAmount) / i);
            dprTO.setTotlalFixedExpense(dprTO.getFixedExpensePerDay() * i);
            vehicleEarnings.add(dprTO);
            //vehicleEarnings = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleFixedExpenseList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleFixedExpenseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleFixedExpenseList", sqlException);
        }
        return vehicleEarnings;
    }

    /**
     * This method used to get Vehicle Driver Salary..
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public double getVehicleDriverSalary(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        double driverSalary = 0;
        try {
            map.put("vehicleId", reportTO.getVehicleId());
            System.out.println("map = " + map);
            driverSalary = (Double) getSqlMapClientTemplate().queryForObject("report.getVehicleDriverSalary", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDriverSalary Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleDriverSalary", sqlException);
        }
        return driverSalary;
    }

    /**
     * This method used to get Vehicle Fixed Expense List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleOperationExpenseList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleOperationExpenses = new ArrayList();
        try {
            map.put("vehicleId", reportTO.getVehicleId());
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            vehicleOperationExpenses = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleOperationExpenses", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleOperationExpenses Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleOperationExpenses", sqlException);
        }
        return vehicleOperationExpenses;
    }

    /**
     * This method used to get Vehicle Maintain Expense List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleMaintainExpenses(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleMaintainExpenses = new ArrayList();
        try {
            map.put("vehicleId", reportTO.getVehicleId());
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            vehicleMaintainExpenses = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleMaintainExpenses", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleMaintainExpenses Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleMaintainExpenses", sqlException);
        }
        return vehicleMaintainExpenses;
    }

    public String[] getDateList(String startDate, String endDate) {
        String[] dateList = new String[100];
        String[] datList = null;
        try {
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            Date startDt = df.parse(startDate);
            Date endDt = df.parse(endDate);
            Calendar startCal, endCal;
            startCal = Calendar.getInstance();
            startCal.setTime(startDt);
            endCal = Calendar.getInstance();
            endCal.setTime(endDt);
            //Just in case the dates were transposed this prevents infinite loop
            if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
                startCal.setTime(endDt);
                endCal.setTime(startDt);
            }
            startCal.add(Calendar.DATE, -1);
            int i = 0;
            do {
                startCal.add(Calendar.DAY_OF_MONTH, 1);
                //System.out.println("Day: " + df.format(startCal.getTime()));
                dateList[i] = df.format(startCal.getTime());
                i++;
            } while (startCal.getTimeInMillis() < endCal.getTimeInMillis());
            datList = new String[i];
            for (int m = 0; m < datList.length; m++) {
                datList[m] = dateList[m];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return datList;
    }

    public ArrayList getVehicleUtilizationList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleUtilizationList = new ArrayList();

        try {

            map.put("vehicleId", reportTO.getVehicleId());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("fleetCenterId", reportTO.getFleetCenterId());
            System.out.println("map = " + map);

            vehicleUtilizationList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleUtilizationList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleUtilizationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleUtilizationList", sqlException);
        }
        return vehicleUtilizationList;
    }

    public ArrayList getTripSheetDetails(ReportTO reportTO) {
        Map map = new HashMap();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("tripSheetId", reportTO.getTripSheetId());
        map.put("fleetCenterId", reportTO.getFleetCenterId());
        System.out.println("map = " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripSheetDetails", map);
            System.out.println("getTripSheetDetail1234s size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripSheetDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getTripEarningsList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList vehicleEarnings = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("tripId", reportTO.getTripId());
            System.out.println("map = " + map);
            vehicleEarnings = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripEarningsList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripEarningsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripEarningsList", sqlException);
        }
        return vehicleEarnings;
    }

    public ArrayList getTripOperationExpenseList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList vehicleOperationExpenses = new ArrayList();
        try {
            map.put("tripId", reportTO.getTripId());
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            vehicleOperationExpenses = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripOperationExpenses", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripOperationExpenseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripOperationExpenseList", sqlException);
        }
        return vehicleOperationExpenses;
    }

    public double getTripDriverSalary(ReportTO reportTO) {
        Map map = new HashMap();
        double driverSalary = 0;
        try {
            map.put("tripId", reportTO.getTripId());
            System.out.println("map = " + map);
            driverSalary = (Double) getSqlMapClientTemplate().queryForObject("report.getTripDriverSalary", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDriverSalary Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripDriverSalary", sqlException);
        }
        return driverSalary;
    }

    public ArrayList getPopupCustomerProfitReportDetails(ReportTO reportTO) {
        ArrayList popupCustomerProfitReport = new ArrayList();
        Map map = new HashMap();
        try {
            if (!reportTO.getTripId().contains(",")) {
                reportTO.setTripId(reportTO.getTripId() + "," + 0);
            }
            if (reportTO.getTripId().contains(",")) {
                String[] tripId = reportTO.getTripId().split(",");
                List tripIds = new ArrayList(tripId.length);
                for (int i = 0; i < tripId.length; i++) {
                    System.out.println("value:" + tripId[i]);
                    tripIds.add(tripId[i]);
                }
                map.put("tripId", tripIds);
            } else {
                map.put("tripId", reportTO.getTripId());
            }
            popupCustomerProfitReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.getPopupCustomerProfitReportDetails", map);
            System.out.println("map size=" + map);
            System.out.println("getPopupTripDetails size=" + popupCustomerProfitReport.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("PopupCustomerProfitReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "PopupCustomerProfitReport List", sqlException);
        }

        return popupCustomerProfitReport;
    }

    public ArrayList getGPSLogDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList logDetails = new ArrayList();
        try {
            map.put("vehicleId", reportTO.getVehicleId());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("tripId", reportTO.getTripCode());
            System.out.println("map = " + map);
            logDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getGPSLogDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getGPSLogDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getGPSLogDetails", sqlException);
        }
        return logDetails;
    }

    public ArrayList getDriverSettlementDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList logDetails = new ArrayList();
        try {
            map.put("vehicleId", reportTO.getVehicleId());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("tripId", reportTO.getTripCode());
            map.put("empId", reportTO.getEmpId());
            System.out.println("map = " + map);
            logDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDriverSettlementDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getGPSLogDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getGPSLogDetails", sqlException);
        }
        return logDetails;
    }

    public ArrayList getTripLogDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList tripLogDetails = new ArrayList();
        try {
            map.put("tripId", reportTO.getTripCode());
            System.out.println("map = " + map);
            tripLogDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripLogDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getGPSLogDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getGPSLogDetails", sqlException);
        }
        return tripLogDetails;
    }

    public ArrayList getBPCLTransactionDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList BPCLTransactionDetails = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            System.out.println("map = " + map);
            BPCLTransactionDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getBPCLTransactionDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBPCLTransactionDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getBPCLTransactionDetails", sqlException);
        }
        return BPCLTransactionDetails;
    }

    public ArrayList getTripGpsStatusDetails(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList tripGpsStatusDetails = new ArrayList();
        try {
            map.put("tripId", reportTO.getTripId());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("tripStatus", reportTO.getTripStatus());
            System.out.println("map = " + map);
            tripGpsStatusDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripGpsStatusDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripGpsStatusDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripGpsStatusDetails", sqlException);
        }
        return tripGpsStatusDetails;
    }

    public ArrayList getTripCodeList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList getTripCodeList = new ArrayList();
        map.put("tripCode", reportTO.getTripCode() + "%");
        try {
            System.out.println(" getTripCodeList = map is tht" + map);
            getTripCodeList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripCodeList", map);
            System.out.println(" getTripCodeList =" + getTripCodeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getZoneList List", sqlException);
        }

        return getTripCodeList;
    }

    public ArrayList getTripDetails(ReportTO reportTO) {
        Map map = new HashMap();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("tripSheetId", reportTO.getTripSheetId());
        map.put("fleetCenterId", reportTO.getFleetCenterId());
        System.out.println("map = " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripDetails", map);
            System.out.println("getTripSheetDetail1234s size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getWflList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList wflList = new ArrayList();
        try {
            wflList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getWflList", map);
            System.out.println("getWflList size=" + wflList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWflList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWflList List", sqlException);
        }

        return wflList;
    }

    public ArrayList getWfuList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList wfuList = new ArrayList();
        try {
            wfuList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getWfuList", map);
            System.out.println("getWfuList size=" + wfuList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWfuList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWfuList List", sqlException);
        }

        return wfuList;
    }

    public ArrayList getTripInProgressList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList tripInProgressList = new ArrayList();
        try {
            tripInProgressList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripInProgressList", map);
            System.out.println("getWfuList size=" + tripInProgressList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripInProgressList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripInProgressList List", sqlException);
        }

        return tripInProgressList;
    }

    public ArrayList getTripNotStartedList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList tripNotStartedList = new ArrayList();
        try {
            tripNotStartedList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripNotStartedList", map);
            System.out.println("tripNotStartedList size=" + tripNotStartedList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripNotStartedList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripNotStartedList List", sqlException);
        }

        return tripNotStartedList;
    }

    public ArrayList getJobCardList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList jobCardList = new ArrayList();
        try {
            jobCardList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getJobCardList", map);
            System.out.println("getJobCardList size=" + jobCardList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJobCardList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getJobCardList List", sqlException);
        }

        return jobCardList;
    }

    public ArrayList getFutureTripList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList futureTripList = new ArrayList();
        try {
            futureTripList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getFutureTripList", map);
            System.out.println("getFutureTripList size=" + futureTripList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJobCardList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFutureTripList List", sqlException);
        }

        return futureTripList;
    }

    public int getWflWfuStatus(String currentDate) {
        Map map = new HashMap();
        int wflwfuStatus = 0;
        map.put("currentDate", currentDate);
        try {
            System.out.println("getWflWfuStatus...map: " + map);
            wflwfuStatus = (Integer) getSqlMapClientTemplate().queryForObject("report.getWflWfuStatus", map);
            System.out.println("wflwfuStatus alist: " + wflwfuStatus);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("wflwfuStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "wflwfuStatus", sqlException);
        }
        return wflwfuStatus;
    }

    public int getWflWfuLogId(String currentDate) {
        Map map = new HashMap();
        int wflwfuLogId = 0;
        map.put("currentDate", currentDate);
        try {
            System.out.println("getWflWfuLogId...map: " + map);
            wflwfuLogId = (Integer) getSqlMapClientTemplate().queryForObject("report.getWflWfuLogId", map);
            System.out.println("wflwfuLogId alist: " + wflwfuLogId);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWflWfuLogId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getWflWfuLogId", sqlException);
        }
        return wflwfuLogId;
    }

    public String getTotalTripSummaryDetails(ReportTO reportTO) {
        Map map = new HashMap();
        String totalTripSummary = "";
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("queryType", reportTO.getQueryType());
        map.put("operationPointId", reportTO.getOperationPointId());
        map.put("businessType", reportTO.getBusinessType());
        map.put("tripType", reportTO.getTripType());
        map.put("tripStatusId", reportTO.getTripStatusId());
        map.put("extraExpenseStatusId", reportTO.getExtraExpense());
        try {
            System.out.println("getWflWfuLogId...map: " + map);
            totalTripSummary = (String) getSqlMapClientTemplate().queryForObject("report.getTotalTripSummaryDetails", map);
            System.out.println("totalTripSummary alist: " + totalTripSummary);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTotalTripSummaryDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTotalTripSummaryDetails", sqlException);
        }
        return totalTripSummary;
    }

    public ArrayList getTripSheetList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList tripSheetList = new ArrayList();

        map.put("vehicleId", reportTO.getVehicleId());
        map.put("vehicleTypeId", reportTO.getVehicleTypeId());
        map.put("customerId", reportTO.getCustomerId());
        map.put("tripStatusId", reportTO.getTripStatusId());
        map.put("startDateFrom", reportTO.getStartDateFrom());
        map.put("startDateTo", reportTO.getStartDateTo());

        try {
            System.out.println("map = " + map);
            tripSheetList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripSheetList", map);
            System.out.println("getTripSheetList size=" + tripSheetList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripSheetList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripSheetList List", sqlException);
        }

        return tripSheetList;
    }

    public String getMarginWiseTripSummary(ReportTO reportTO) {
        Map map = new HashMap();
        String marginWiseTripSummary = "";
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("percent", reportTO.getPercent());
        map.put("customerId", reportTO.getCustomerId());
        try {
            System.out.println("getMarginWiseTripSummary...map: " + map);
            marginWiseTripSummary = (String) getSqlMapClientTemplate().queryForObject("report.getMarginWiseTripSummary", map);
            System.out.println("getMarginWiseTripSummary alist: " + marginWiseTripSummary);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMarginWiseTripSummary Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getMarginWiseTripSummary", sqlException);
        }
        return marginWiseTripSummary;
    }

    public ArrayList getMonthWiseEmptyRunSummary(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList monthWiseEmptyRunSummary = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("percent", reportTO.getPercent());
        map.put("customerId", reportTO.getCustomerId());
        try {
            System.out.println("getMonthWiseEmptyRunSummary...map: " + map);
            monthWiseEmptyRunSummary = (ArrayList) getSqlMapClientTemplate().queryForList("report.getMonthWiseEmptyRunSummary", map);
            System.out.println("getMonthWiseEmptyRunSummary alist: " + monthWiseEmptyRunSummary);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMonthWiseEmptyRunSummary Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getMonthWiseEmptyRunSummary", sqlException);
        }
        return monthWiseEmptyRunSummary;
    }

    public ArrayList getJobcardSummary(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList jobcardSummary = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("serviceTypeId", reportTO.getServiceTypeId());
        try {
            System.out.println("getJobcardSummary...map: " + map);
            jobcardSummary = (ArrayList) getSqlMapClientTemplate().queryForList("report.getJobcardSummary", map);
            System.out.println("getJobcardSummary alist: " + jobcardSummary);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJobcardSummary Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getJobcardSummary", sqlException);
        }
        return jobcardSummary;
    }

    public ArrayList getTripMergingDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList TripMergingDetails = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            System.out.println("map = " + map);
            TripMergingDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getMergeTripDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBPCLTransactionDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getBPCLTransactionDetails", sqlException);
        }
        return TripMergingDetails;
    }

    public ArrayList getVehicleReadingDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleReadingDetails = new ArrayList();
        try {
            //  map.put("fromDate", reportTO.getFromDate());
            //  map.put("toDate", reportTO.getToDate());
            map.put("vehicletype", reportTO.getVehicletype());
            System.out.println("map = " + map);
            vehicleReadingDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getvehicleReadingDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBPCLTransactionDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getBPCLTransactionDetails", sqlException);
        }
        return vehicleReadingDetails;
    }

    public ArrayList getTripMergingList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList tripMergingList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("customerId", reportTO.getCustomerId());
            System.out.println("map = " + map);
            if (!reportTO.getCustomerId().equals("1040")) {
                tripMergingList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripMergingList", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripMergingList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripMergingList", sqlException);
        }
        return tripMergingList;
    }

    public ArrayList getTripNotMergingList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList tripNotMergingList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("customerId", reportTO.getCustomerId());
            System.out.println("map = " + map);
            if (!reportTO.getCustomerId().equals("1040")) {
                tripNotMergingList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripNotMergingList", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripNotMergingList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripNotMergingList", sqlException);
        }
        return tripNotMergingList;
    }

    public ArrayList getCustomerMergingList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList customerWiseProfitList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("customerId", reportTO.getCustomerId());
            map.put("tripMergingId", reportTO.getTripMergingId());
            customerWiseProfitList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCustomerMergingtList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerWiseProfitList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getCustomerWiseProfitList", sqlException);
        }
        return customerWiseProfitList;
    }

    public ArrayList getCustomerTripNotMergingList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList customerTripNotMergingList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("customerId", reportTO.getCustomerId());
            map.put("tripId", reportTO.getTripId());
            System.out.println("map = " + map);
            customerTripNotMergingList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCustomerTripNotMergingList", map);

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerTripNotMergingList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getCustomerTripNotMergingList", sqlException);
        }
        return customerTripNotMergingList;
    }

    public ArrayList getCustomerLists(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList customerList = new ArrayList();
        try {
            map.put("customerId", reportTO.getCustomerId());
            System.out.println("map = " + map);
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCustomerLists", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerLists Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getCustomerLists", sqlException);
        }
        return customerList;
    }

    public ArrayList getPopupCustomerMergingProfitReportDetails(ReportTO reportTO) {
        ArrayList popupCustomerProfitReport = new ArrayList();
        Map map = new HashMap();
        try {
            if (!reportTO.getTripId().contains(",")) {
                reportTO.setTripId(reportTO.getTripId() + "," + 0);
            }
            if (reportTO.getTripId().contains(",")) {
                String[] tripId = reportTO.getTripId().split(",");
                List tripIds = new ArrayList(tripId.length);
                for (int i = 0; i < tripId.length; i++) {
                    System.out.println("value:" + tripId[i]);
                    tripIds.add(tripId[i]);
                }
                map.put("tripId", tripIds);
            } else {
                map.put("tripId", reportTO.getTripId());
            }
            popupCustomerProfitReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.getPopupCustomerMergingProfitReportDetails", map);
            System.out.println("map size=" + map);
            System.out.println("getPopupTripDetails size=" + popupCustomerProfitReport.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPopupCustomerMergingProfitReportDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPopupCustomerMergingProfitReportDetails List", sqlException);
        }

        return popupCustomerProfitReport;
    }

    public ArrayList getTripExtraExpenseDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList TripExtraExpenseDetails = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            System.out.println("map = " + map);
            TripExtraExpenseDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripExtraExpenseDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripExtraExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripExtraExpenseDetails", sqlException);
        }
        return TripExtraExpenseDetails;
    }

    public ArrayList getVehicleDriverAdvanceDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleDriverAdvanceDetails = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("expensetype", reportTO.getExpenseType());
            map.put("usagetype", reportTO.getUsageType());
            System.out.println("map = " + map);
            vehicleDriverAdvanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleDriverAdvanceReport", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBPCLTransactionDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getBPCLTransactionDetails", sqlException);
        }
        return vehicleDriverAdvanceDetails;
    }

    public ArrayList getVehicleList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        try {
            map.put("fleetCenterId", reportTO.getFleetCenterId());
            map.put("vehicleId", reportTO.getVehicleId());
            map.put("vehicleTypeId", reportTO.getVehicleTypeId());
            System.out.println((new StringBuilder()).append("map = ").append(map).toString());
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripVehicleList", map);
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleList", sqlException);
        }
        return vehicleList;
    }

    public ArrayList getTripVehicleList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        try {
            map.put("fleetCenterId", reportTO.getFleetCenterId());
            map.put("vehicleId", "");
            map.put("vehicleTypeId", reportTO.getVehicleTypeId());
            System.out.println((new StringBuilder()).append("map = ").append(map).toString());
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripVehicleList", map);
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getTripVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripVehicleList", sqlException);
        }
        return vehicleList;
    }

    public ArrayList getTripSheetListWfl(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList tripSheetList = new ArrayList();
        map.put("fleetCenterId", reportTO.getFleetCenterId());
        map.put("vehicleId", reportTO.getVehicleId());
        map.put("vehicleTypeId", reportTO.getVehicleTypeId());
        map.put("customerId", reportTO.getCustomerId());
        map.put("tripStatusId", reportTO.getTripStatusId());
        map.put("tripStatusIdTo", reportTO.getTripStatusIdTo());
        map.put("startDateFrom", reportTO.getStartDateFrom());
        map.put("startDateTo", reportTO.getStartDateTo());
        map.put("endDateFrom", reportTO.getEndDateFrom());
        map.put("endDateTo", reportTO.getEndDateTo());
        map.put("not", reportTO.getNot());
        try {
            tripSheetList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripSheetListWfl", map);

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getTripSheetListWfl Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripSheetListWfl", sqlException);
        }
        return tripSheetList;
    }

    public String getWflHours(ReportTO reportTO) {
        Map map = new HashMap();
        String totalTripSummary = "";
        if (Integer.parseInt(reportTO.getCustomerId()) == 1040) {

            map.put("startDateTime", reportTO.getStartDateTime());
        } else {
            map.put("startDateTime", reportTO.getReportDateTime());
        }
        map.put("vehicleId", reportTO.getVehicleId());
        map.put("tripId", reportTO.getTripId());
        System.out.println("map for WFL :" + map);
        try {
            System.out.println((new StringBuilder()).append("getWflWfuLogId...map: ").append(map).toString());
            totalTripSummary = (String) getSqlMapClientTemplate().queryForObject("report.getWflHours", map);
            System.out.println((new StringBuilder()).append("totalTripSummary alist: ").append(totalTripSummary).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getWflHours Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getWflHours", sqlException);
        }
        return totalTripSummary;
    }

    public ArrayList getLatestUpdates(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList tripSheetList = new ArrayList();
        try {
            tripSheetList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getLatestUpdates", map);

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getLatestUpdates Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getLatestUpdates", sqlException);
        }
        return tripSheetList;
    }

    public ArrayList getVmrList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList tripvmrList = new ArrayList();
        try {
            tripvmrList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVmrList", map);

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getVmrList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVmrList", sqlException);
        }
        return tripvmrList;
    }

    public ArrayList getToPayCustomerTripDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList ToPayCustomerTripDetails = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            System.out.println("map = " + map);
            ToPayCustomerTripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getToPayCustomerTripDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ToPayCustomerTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getToPayCustomerTripDetails", sqlException);
        }
        return ToPayCustomerTripDetails;
    }

    public ArrayList getTripVmrDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList TripVmrDetails = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
//                map.put("startDateFrom", reportTO.getStartDateFrom());
//	        map.put("startDateTo", reportTO.getStartDateTo());
            map.put("endDateFrom", reportTO.getEndDateFrom());
            map.put("endDateTo", reportTO.getEndDateTo());
            System.out.println("map = " + map);
            TripVmrDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripVmrDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripVmrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripVmrDetails", sqlException);
        }
        return TripVmrDetails;
    }

    public ArrayList getFcWiseTripBudgetDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList budgetListsDetails = new ArrayList();
        ArrayList budgetDetails = new ArrayList();
        ArrayList driverAdvanceDetails = new ArrayList();
        ArrayList vehicleLists = new ArrayList();
        ArrayList totalKmLists = new ArrayList();
        String wfuAmount = "";
        String emptyTripKM = "";
        String noOfVehicles = "";
        float totalkms = 0;
        float totalhms = 0;
        float foodingAdvance = 0;
        float rnmAdvance = 0;
        try {
            map.put("fromDate", reportTO.getFromDate() + " " + "0000");
            map.put("toDate", reportTO.getToDate() + " " + "2300");
            System.out.println("map in dao = " + map);

            budgetDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripExpenseAndTotalCostDetails", map);
            ReportTO rpTO = new ReportTO();
            Iterator itr1 = budgetDetails.iterator();
            ReportTO repTO1 = new ReportTO();
            while (itr1.hasNext()) {
                ReportTO reportTo = new ReportTO();
                repTO1 = (ReportTO) itr1.next();
                reportTo.setCompanyId(repTO1.getCompanyId());
                reportTo.setCompanyName(repTO1.getCompanyName());
                System.out.println("company name = " + repTO1.getCompanyName());
                reportTo.setTotalCost(repTO1.getTotalCost());
                System.out.println("total cost = " + repTO1.getTotalCost());
                reportTo.setRcm(repTO1.getRcm());
                reportTo.setTotalTrip(repTO1.getTotalTrip());
                System.out.println("total trips = " + repTO1.getTotalTrip());
                map.put("companyId", repTO1.getCompanyId());
                wfuAmount = (String) getSqlMapClientTemplate().queryForObject("report.getTripWfuAdvanceForBudgetDetails", map);
                reportTo.setWfuAmount(wfuAmount);
                emptyTripKM = (String) getSqlMapClientTemplate().queryForObject("report.getEmptyKMForBudgetDetails", map);
                noOfVehicles = (String) getSqlMapClientTemplate().queryForObject("report.getTotalVehicleForBudgetDetails", map);
                reportTo.setNoOfVehicles(Integer.parseInt(noOfVehicles));
                reportTo.setEmptyTripKM(emptyTripKM);
                vehicleLists = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleIdForBudgetReport", map);
                System.out.println("map = " + vehicleLists.size());
                Iterator itr2 = vehicleLists.iterator();
                ReportTO repTO3 = new ReportTO();
                totalkms = 0;
                totalhms = 0;
                foodingAdvance = 0;
                rnmAdvance = 0;
                while (itr2.hasNext()) {
                    repTO3 = (ReportTO) itr2.next();
                    reportTo.setCompanyId(repTO3.getCompanyId());
                    map.put("vehicleId", repTO3.getVehicleId());
                    System.out.println("vehicleId for = " + repTO3.getVehicleId());
                    totalKmLists = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTotalKmForBudgetReport", map);
                    Iterator itr3 = totalKmLists.iterator();
                    ReportTO repTO4 = new ReportTO();

                    while (itr3.hasNext()) {
                        repTO4 = (ReportTO) itr3.next();
                        totalkms = totalkms + Float.parseFloat(repTO4.getTotalkms());
                        totalhms = totalhms + Float.parseFloat(repTO4.getTotalHms());
                        System.out.println("Total Km for = " + totalkms);
                    }
                    driverAdvanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDriverAdvanceForBudgetDetails", map);
                    if (driverAdvanceDetails.size() > 0) {
                        Iterator itr4 = driverAdvanceDetails.iterator();
                        ReportTO repTO5 = new ReportTO();
                        while (itr4.hasNext()) {
                            repTO5 = (ReportTO) itr4.next();
                            foodingAdvance = foodingAdvance + Float.parseFloat(repTO5.getFoodingAdvance());
                            rnmAdvance = rnmAdvance + Float.parseFloat(repTO5.getRepairAdvance());

                        }
                    }
                }
                reportTo.setTotalkms(String.valueOf(totalkms));
                reportTo.setTotalHms(String.valueOf(totalhms));
                reportTo.setFoodingAdvance(String.valueOf(foodingAdvance));
                reportTo.setRepairAdvance(String.valueOf(rnmAdvance));
                budgetListsDetails.add(reportTo);
            }
            System.out.println("budgetListsDetails = " + budgetListsDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFcWiseTripBudgetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getFcWiseTripBudgetDetails", sqlException);
        }
        return budgetListsDetails;
    }

    public ArrayList getAccountMgrPerformanceReportDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList accountMgrLists = new ArrayList();
        ArrayList budgetListsDetails = new ArrayList();
        ArrayList revenueCostLists = new ArrayList();
        ArrayList totalKmLists = new ArrayList();
        String customerId = "";
        String emptyTripKM = "";
        String noOfVehicles = "";

        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            accountMgrLists = (ArrayList) getSqlMapClientTemplate().queryForList("report.getAccountMgrId", map);
            ReportTO rpTO = new ReportTO();
            Iterator itr1 = accountMgrLists.iterator();
            ReportTO repTO1 = new ReportTO();
            while (itr1.hasNext()) {
                float totalkms = 0;
                float totalhms = 0;
                int tripNos = 0;
                int wfuHours = 0;
                float revenue = 0;
                float totalCost = 0;
                int loadingTurnTime = 0;
                ReportTO reportTo = new ReportTO();
                repTO1 = (ReportTO) itr1.next();
                reportTo.setEmpId(repTO1.getEmpId());
                reportTo.setEmpName(repTO1.getEmpName());
                map.put("empId", repTO1.getEmpId());
                System.out.println("repTO1.getEmpId() = " + repTO1.getEmpId());
                customerId = "";
                customerId = (String) getSqlMapClientTemplate().queryForObject("report.getCustomerId", map);
                System.out.println("customerId = " + customerId);
                if (!"".equals(customerId) && customerId != null) {
                    System.out.println("customerId not null condition = " + customerId);
                    String[] customerIds = customerId.split(",");
                    List customerIdsList = new ArrayList(customerIds.length);
                    for (int i = 0; i < customerIds.length; i++) {
                        customerIdsList.add(customerIds[i]);
                    }
                    map.put("customerId", customerIdsList);
                    revenueCostLists = (ArrayList) getSqlMapClientTemplate().queryForList("report.getRevenueAndCost", map);
                    System.out.println("customerId = " + customerId);
                    System.out.println("map = " + map);
                    System.out.println("revenueCostListssize = " + revenueCostLists.size());
                    totalkms = 0;
                    totalhms = 0;
                    tripNos = 0;
                    wfuHours = 0;
                    revenue = 0;
                    totalCost = 0;
                    loadingTurnTime = 0;
                    String tripId = "";
                    ReportTO repTO3 = new ReportTO();
                    Iterator itr2 = revenueCostLists.iterator();
                    while (itr2.hasNext()) {
                        repTO3 = (ReportTO) itr2.next();
                        System.out.println("repTO3.getTripNo() = " + repTO3.getTripNos());
                        System.out.println("repTO3.getLoadingTurnHours() = " + repTO3.getLoadingTransitHours());
                        tripNos = tripNos + Integer.parseInt(repTO3.getTripNos());
                        wfuHours = wfuHours + Integer.parseInt(repTO3.getWfuHours());
                        revenue = revenue + Float.parseFloat(repTO3.getRevenue());
                        totalCost = totalCost + Float.parseFloat(repTO3.getTotalCost());
                        loadingTurnTime = loadingTurnTime + Integer.parseInt(repTO3.getLoadingTransitHours());
                        tripId = repTO3.getTripId();
                        System.out.println("getTripId() = " + repTO3.getTripId());
                        if (tripId != null) {
                            String[] tripIds = tripId.split(",");
                            List tripIdList = new ArrayList(tripIds.length);
                            for (int i = 0; i < tripIds.length; i++) {
                                tripIdList.add(tripIds[i]);
                            }
                            map.put("tripId", tripIdList);
                            System.out.println("repTO3.getTripId() = " + repTO3.getTripId());
                            totalKmLists = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTotalKm", map);
                            Iterator itr3 = totalKmLists.iterator();
                            ReportTO repTO4 = new ReportTO();
                            while (itr3.hasNext()) {
                                repTO4 = (ReportTO) itr3.next();
                                totalkms = totalkms + Float.parseFloat(repTO4.getTotalkms());
                                totalhms = totalhms + Float.parseFloat(repTO4.getTotalHms());
                            }
                        }
                    }
                    reportTo.setTripNos(String.valueOf(tripNos));
                    reportTo.setWfuHours(String.valueOf(wfuHours));
                    reportTo.setRevenue(String.valueOf(revenue));
                    reportTo.setTotalCost(String.valueOf(totalCost));
                    reportTo.setTotalkms(String.valueOf(totalkms));
                    reportTo.setTotalHms(String.valueOf(totalhms));
                    reportTo.setLoadingTurnHours(String.valueOf(loadingTurnTime));
                } else {
                    reportTo.setTripNos(String.valueOf(tripNos));
                    reportTo.setWfuHours(String.valueOf(wfuHours));
                    reportTo.setRevenue(String.valueOf(revenue));
                    reportTo.setTotalCost(String.valueOf(totalCost));
                    reportTo.setTotalkms(String.valueOf(totalkms));
                    reportTo.setTotalHms(String.valueOf(totalhms));
                    reportTo.setLoadingTurnHours(String.valueOf(loadingTurnTime));
                }
                budgetListsDetails.add(reportTo);
            }
            System.out.println("budgetListsDetails = " + budgetListsDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFcWiseTripBudgetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            sqlException.printStackTrace();
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getFcWiseTripBudgetDetails", sqlException);
        }
        return budgetListsDetails;
    }

    public ArrayList getRNMExpenseReportDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        ArrayList rnmListsDetails = new ArrayList();
        ArrayList rnmAdvanceLists = new ArrayList();
        String jobCardId = "";

        String rnmAdvance = "";

        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("type", reportTO.getType());
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleIdForRNMReport", map);
            Iterator itr1 = vehicleList.iterator();
            ReportTO repTO1 = new ReportTO();
            while (itr1.hasNext()) {
                String serviceTypeId = "17,16,15";
                String temp[] = serviceTypeId.split(",");
                ReportTO reportTo = new ReportTO();
                repTO1 = (ReportTO) itr1.next();
                reportTo.setVehicleId(repTO1.getVehicleId());
                reportTo.setRegNo(repTO1.getRegNo());
                map.put("vehicleId", repTO1.getVehicleId());
                rnmAdvanceLists = (ArrayList) getSqlMapClientTemplate().queryForList("report.getJobCardIdForRNMReport", map);
                if (rnmAdvanceLists.size() > 0) {
                    Iterator itr4 = rnmAdvanceLists.iterator();
                    ReportTO repTO5 = new ReportTO();
                    while (itr4.hasNext()) {
                        repTO5 = (ReportTO) itr4.next();
                        reportTo.setChasisAmount(repTO5.getChasisAmount());
                        reportTo.setReferAmount(repTO5.getReferAmount());
                        reportTo.setContainerAmount(repTO5.getContainerAmount());
                    }
                } else {
                    reportTo.setChasisAmount("0.00");
                    reportTo.setReferAmount("0.00");
                    reportTo.setContainerAmount("0.00");
                }
                rnmListsDetails.add(reportTo);
            }
            System.out.println("getRNMExpenseReportDetails = " + rnmListsDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRNMExpenseReportDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getRNMExpenseReportDetails", sqlException);
        }
        return rnmListsDetails;
    }

    public ArrayList getTyerExpenseReportDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        ArrayList rnmListsDetails = new ArrayList();
        ArrayList rnmAdvanceLists = new ArrayList();
        String jobCardId = "";

        String rnmAdvance = "";

        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("type", reportTO.getType());
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleIdForRNMReport", map);
            Iterator itr1 = vehicleList.iterator();
            ReportTO repTO1 = new ReportTO();
            while (itr1.hasNext()) {
                String TyreType = "Newtyer,Rethread";
                String temp[] = TyreType.split(",");
                ReportTO reportTo = new ReportTO();
                repTO1 = (ReportTO) itr1.next();
                reportTo.setVehicleId(repTO1.getVehicleId());
                reportTo.setRegNo(repTO1.getRegNo());
                map.put("vehicleId", repTO1.getVehicleId());
                rnmAdvanceLists = (ArrayList) getSqlMapClientTemplate().queryForList("report.getJobCardIdForRNMReport", map);
                if (rnmAdvanceLists.size() > 0) {
                    Iterator itr4 = rnmAdvanceLists.iterator();
                    ReportTO repTO5 = new ReportTO();
                    while (itr4.hasNext()) {
                        repTO5 = (ReportTO) itr4.next();
                        reportTo.setChasisAmount(repTO5.getChasisAmount());
                        reportTo.setReferAmount(repTO5.getReferAmount());
                        reportTo.setContainerAmount(repTO5.getContainerAmount());
                    }
                } else {
                    reportTo.setChasisAmount("0.00");
                    reportTo.setReferAmount("0.00");
                    reportTo.setContainerAmount("0.00");
                }
                rnmListsDetails.add(reportTo);
            }
            System.out.println("getRNMExpenseReportDetails = " + rnmListsDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRNMExpenseReportDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getRNMExpenseReportDetails", sqlException);
        }
        return rnmListsDetails;
    }

    public ArrayList getRNMVehicleList(String type) {
        Map map = new HashMap();
        map.put("type", type);
        ArrayList vehicleList = new ArrayList();
        try {
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleIdForRNMReport", map);
            System.out.println("getRNMVehicleList size=" + vehicleList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRNMVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRNMVehicleList", sqlException);
        }
        return vehicleList;
    }

    public ArrayList getVehicleTyreList(String type) {
        Map map = new HashMap();
        map.put("type", type);
        ArrayList vehicleList = new ArrayList();
        try {
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleTyreList", map);
            System.out.println("getVehicleTyreList size=" + vehicleList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRNMVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleTyreList", sqlException);
        }
        return vehicleList;
    }

    public ArrayList getRNMReportList(String type) {
        Map map = new HashMap();
        map.put("type", type);
        ArrayList rnmList = new ArrayList();
        try {
            rnmList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getRNMReportList", map);
            System.out.println("rnmList size=" + rnmList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRNMVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "rnmList", sqlException);
        }
        return rnmList;
    }

    public ArrayList getTyreAgeingList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        //  map.put("type", type);
        ArrayList tyreAgingList1 = new ArrayList();
        ArrayList tyreAgingList2 = new ArrayList();
        ArrayList tyreAgeingList = new ArrayList();
        try {
            tyreAgingList1 = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTyreAgingList1", map);
            System.out.println("tyreAgingList1 size=" + tyreAgingList1.size());

            Iterator itr1 = tyreAgingList1.iterator();
            ReportTO repTO1 = new ReportTO();
            while (itr1.hasNext()) {
                ReportTO reportTo = new ReportTO();
                repTO1 = (ReportTO) itr1.next();
                reportTo.setTyreNo(repTO1.getTyreNo());
                reportTo.setTyerType(repTO1.getTyerType());
                reportTo.setInstallationDate(repTO1.getInstallationDate());
                reportTo.setInstallationKM(repTO1.getInstallationKM());
                reportTo.setTyreAmount(repTO1.getTyreAmount());
                reportTo.setRegNo(repTO1.getRegNo());
                map.put("newTyreNo", repTO1.getTyreNo());
                System.out.println("newTyreNo=" + map);
                tyreAgingList2 = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTyreAgingList2", map);
                if (tyreAgingList2.size() > 0) {
                    Iterator itr4 = tyreAgingList2.iterator();
                    ReportTO repTO5 = new ReportTO();
                    while (itr4.hasNext()) {
                        repTO5 = (ReportTO) itr4.next();
                        reportTo.setUnInstallationDate(repTO5.getUnInstallationDate());
                        reportTo.setUnInstallationKM(repTO5.getUnInstallationKM());

                    }
                } else {
                    reportTo.setUnInstallationDate("0.00");
                    reportTo.setUnInstallationKM("0.00");

                }
                tyreAgeingList.add(reportTo);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRNMVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "tyreAgingList", sqlException);
        }
        return tyreAgeingList;
    }

    public ArrayList getVehicleUtilizeList(String month, String year) {
        Map map = new HashMap();
        ArrayList vehileUtilList = new ArrayList();
        ArrayList vehicleList = new ArrayList();
        int partialAvailableHours = 0;
        int availableDays = 0;
        int driverIssueHours = 0;
        int driverIssueHoursNotClosed = 0;
        int rnmHours = 0;
        int exactWflHours = 0;
        int totalJobCardHours = 0;
        int firstTripWfl = 0;
        int lastTripWfl = 0;
        int rnmHoursNotClosed = 0;
        int month1 = Integer.parseInt(month);
        String day = "";
        try {
            // month="11";
            if (month1 == 1 || month1 == 3 || month1 == 5 || month1 == 7 || month1 == 8 || month1 == 10 || month1 == 12) {
                day = "31";

            } else if (month1 == 2) {
                day = "28";
            } else {
                day = "30";
            }
            map.put("month", month);
            map.put("year", year);
            map.put("day", day);
            System.out.println("map:" + map);
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleTypeUtilizationList", map);
            if (vehicleList != null) {

                for (int i = 0; i < vehicleList.size(); i++) {
                    HashMap vehicleMap = new HashMap();
                    ReportTO reportTo = null;
                    reportTo = (ReportTO) vehicleList.get(i);///getting from arraylist
                    ReportTO setReportTo = new ReportTO();///storing to  arraylist to page

                    if (reportTo != null) {

                        String vehicleId = reportTo.getVehicleId();
                        vehicleMap.put("vehicleId", vehicleId);
                        vehicleMap.put("month", month);
                        vehicleMap.put("year", year);
                        vehicleMap.put("day", day);
                        System.out.println("vehicleMap" + vehicleMap);

                        if (getSqlMapClientTemplate().queryForObject("report.getVehicleUtilizationDetails", vehicleMap) != null) {
                            ReportTO detailReportTo = (ReportTO) getSqlMapClientTemplate().queryForObject("report.getVehicleUtilizationDetails", vehicleMap);
                            if (detailReportTo != null) {
                                int totalWflHours = 0;
                                partialAvailableHours = 0;
                                availableDays = 0;
                                driverIssueHours = 0;
                                rnmHours = 0;
                                exactWflHours = 0;
                                totalJobCardHours = 0;
                                ArrayList tripIdList = new ArrayList();
                                tripIdList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleTripId", vehicleMap);
                                System.out.println("tripIdList" + tripIdList.size());
                                if (tripIdList != null) {
                                    HashMap tripIdMap = new HashMap();
                                    for (int n = 0; n < tripIdList.size(); n++) {
                                        ReportTO firstTripId = null;
                                        ReportTO lastTripId = null;
                                        if (n == tripIdList.size() - 1) {
                                            lastTripId = (ReportTO) tripIdList.get(n);
                                            tripIdMap.put("lastTripId", lastTripId.getTripId());
                                            tripIdMap.put("month", month);
                                            tripIdMap.put("year", year);
                                            System.out.println("tripid map 2:" + tripIdMap);
                                            ReportTO lastTripWflReportTo = (ReportTO) getSqlMapClientTemplate().queryForObject("report.getVehicleLastWfl", tripIdMap);
                                            lastTripWfl = Integer.parseInt(lastTripWflReportTo.getWfl());
                                            System.out.println("last Wfl:" + lastTripWfl);
                                        } else if (n == 0) {

                                            firstTripId = (ReportTO) tripIdList.get(0);
                                            tripIdMap.put("firstTripId", firstTripId.getTripId());
                                            tripIdMap.put("month", month);
                                            tripIdMap.put("year", year);
                                            System.out.println("tripid map 1:" + tripIdMap);
                                            ReportTO firstTripWflReportTo = (ReportTO) getSqlMapClientTemplate().queryForObject("report.getVehicleFirstTripWfl", tripIdMap);
                                            firstTripWfl = Integer.parseInt(firstTripWflReportTo.getWfl());
                                            System.out.println("first Wfl:" + firstTripWfl);
                                        }
                                    }
                                    if (tripIdList != null && tripIdList.size() > 1) {
                                        for (int k = 0; k < tripIdList.size() - 1; k++) {
                                            try {
                                                ReportTO tripA = (ReportTO) tripIdList.get(k);
                                                tripIdMap.put("tripA", tripA.getTripId());
                                                ReportTO tripB = (ReportTO) tripIdList.get(k + 1);
                                                tripIdMap.put("tripB", tripB.getTripId());
                                                System.out.println("tripIdMap====" + tripIdMap);
                                                ReportTO wflReportTo = (ReportTO) getSqlMapClientTemplate().queryForObject("report.getVehicleWflDetails", tripIdMap);
                                                if (Integer.parseInt(wflReportTo.getWfl()) > 0) {
                                                    totalWflHours += Integer.parseInt(wflReportTo.getWfl());
                                                }
                                                System.out.println("Total Wfl 1:" + totalWflHours);
                                            } catch (Exception e) {
                                                System.out.println("eeeee" + e);
                                            }
                                        }

                                    }
                                    totalWflHours = totalWflHours + firstTripWfl + lastTripWfl;
                                    System.out.println("Total Wfl:" + totalWflHours);
                                }
                                ReportTO jobReportTo = (ReportTO) getSqlMapClientTemplate().queryForObject("report.getVehicleJodCardDetails", vehicleMap);
                                System.out.println("jobReportTo" + jobReportTo);
                                if (jobReportTo != null) {
                                    partialAvailableHours = Integer.parseInt(detailReportTo.getPartialHours());
                                    if (jobReportTo.getDriverIssueHours() == null | "".equals(jobReportTo.getDriverIssueHours())) {
                                        driverIssueHours = 0;
                                    } else {
                                        driverIssueHours = Integer.parseInt(jobReportTo.getDriverIssueHours());
                                    }
                                    if (jobReportTo.getRnmHours() == null | "".equals(jobReportTo.getRnmHours())) {
                                        rnmHours = 0;
                                    } else {
                                        rnmHours = Integer.parseInt(jobReportTo.getRnmHours());
                                    }
                                    if (jobReportTo.getTotalJobcardHours() == null | "".equals(jobReportTo.getTotalJobcardHours())) {
                                        totalJobCardHours = 0;
                                    } else {
                                        totalJobCardHours = Integer.parseInt(jobReportTo.getTotalJobcardHours());
                                    }
                                    availableDays = partialAvailableHours + driverIssueHours + rnmHours + totalWflHours;

                                    System.out.println("availableDays=====" + availableDays);

                                    System.out.println("totalWflHours=====" + totalWflHours);
                                    System.out.println("totalJobCardHours=====" + totalJobCardHours);
                                } else {
                                    System.out.println("totaljobHours is null=====");
                                    partialAvailableHours = Integer.parseInt(detailReportTo.getPartialHours());
                                    driverIssueHours = 0;
                                    rnmHours = 0;
                                    totalJobCardHours = 0;

                                }
                                ReportTO jobNotClosedReportTo = (ReportTO) getSqlMapClientTemplate().queryForObject("report.getVehicleJodCardNotClosedDetails", vehicleMap);
                                if (jobNotClosedReportTo != null) {
                                    if (jobNotClosedReportTo.getDriverIssueHours() == null | "".equals(jobNotClosedReportTo.getDriverIssueHours())) {
                                        driverIssueHoursNotClosed = 0;
                                    } else {
                                        driverIssueHoursNotClosed = Integer.parseInt(jobNotClosedReportTo.getDriverIssueHours());
                                    }
                                    if (jobNotClosedReportTo.getRnmHours() == null | "".equals(jobNotClosedReportTo.getRnmHours())) {
                                        rnmHoursNotClosed = 0;
                                    } else {
                                        rnmHoursNotClosed = Integer.parseInt(jobNotClosedReportTo.getRnmHours());
                                    }
                                    if (jobNotClosedReportTo.getTotalJobcardHours() == null | "".equals(jobNotClosedReportTo.getTotalJobcardHours())) {
                                        totalJobCardHours = 0;
                                    } else {
                                        totalJobCardHours = Integer.parseInt(jobNotClosedReportTo.getTotalJobcardHours());
                                    }
                                    System.out.println("not closed Di:" + driverIssueHoursNotClosed);
                                    System.out.println("not closed Rnm:" + rnmHoursNotClosed);
                                }
                                rnmHours = rnmHours + rnmHoursNotClosed;
                                driverIssueHours = driverIssueHours + driverIssueHoursNotClosed;
                                //  availableDays=partialAvailableHours+driverIssueHours+rnmHours+totalWflHours;
                                availableDays = Integer.parseInt(day) * 24;
                                exactWflHours = totalWflHours - (rnmHours + driverIssueHours);
                                ///setting property starts
                                setReportTo.setRegNo(reportTo.getRegNo());
                                setReportTo.setAvailableDays(String.valueOf(availableDays));
                                setReportTo.setTripTransitHours(detailReportTo.getTripTransitHours());
                                setReportTo.setLoadingTransitHours(detailReportTo.getLoadingTransitHours());
                                setReportTo.setUnloadingTurnHours(detailReportTo.getUnloadingTurnHours());
                                setReportTo.setWfl(String.valueOf(exactWflHours));
                                setReportTo.setDriverIssueHours(String.valueOf(driverIssueHours));
                                setReportTo.setRnmHours(String.valueOf(rnmHours));
                                setReportTo.setTransitDelay(detailReportTo.getTransitDelay());
                                vehileUtilList.add(setReportTo);
                                System.out.println("vehileUtilList Size From Dao=====" + vehileUtilList.size());
                                ///setting property End                   
                            }

                        }
                    }
                }
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog("getRNMVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehileUtilList", sqlException);
        }

        return vehileUtilList;
    }

    public ArrayList getBrokerCustomerDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList brokerCustomerDetails = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());

        try {
            brokerCustomerDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getBrokerCustomerDetails", map);

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getTripSheetListWfl Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripSheetListWfl", sqlException);
        }
        return brokerCustomerDetails;
    }

    public ArrayList getDashboardopsTruckNos(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList getDashboardopsTruckNos = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("dashboardopstrukCount", reportTO.getDashboardopstrukCount() + "%");
        //////System.out.println("map = " + map);
        try {
            getDashboardopsTruckNos = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDashboardopsTruckNos", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDashboardopsTruckNos Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getDashboardopsTruckNos", sqlException);
        }
        return getDashboardopsTruckNos;
    }

    public ArrayList getDashBoardOperationNos(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList getDashboardopsTruckNos = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("dashboardopstrukCount", reportTO.getDashboardopstrukCount() + "%");
        //////System.out.println("map = " + map);
        try {
            getDashboardopsTruckNos = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDashBoardOperationNos", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDashboardopsTruckNos Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getDashboardopsTruckNos", sqlException);
        }
        return getDashboardopsTruckNos;
    }

    public ArrayList getOverAllTripNos(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList getOverAllTripNos = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("dashboardopstrukCount", reportTO.getDashboardopstrukCount() + "%");
        System.out.println("today arun mapped 27-11-2018 = " + map);
        try {
            getOverAllTripNos = (ArrayList) getSqlMapClientTemplate().queryForList("report.getOverAllTripNos", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOverAllTripNos Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getOverAllTripNos", sqlException);
        }
        return getOverAllTripNos;
    }

    public ArrayList getStatusWiseTripNos(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList getStatusWiseTripNos = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("dashboardopstrukCount", reportTO.getDashboardopstrukCount() + "%");
        //////System.out.println("map = " + map);
        try {
            getStatusWiseTripNos = (ArrayList) getSqlMapClientTemplate().queryForList("report.getStatusWiseTripNos", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStatusWiseTripNos Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getStatusWiseTripNos", sqlException);
        }
        return getStatusWiseTripNos;
    }

    public ArrayList getRouteWiseTripNos(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList getRouteWiseTripNos = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("dashboardopstrukCount", reportTO.getDashboardopstrukCount() + "%");
        //////System.out.println("map = " + map);
        try {
            getRouteWiseTripNos = (ArrayList) getSqlMapClientTemplate().queryForList("report.getRouteWiseTripNos", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRouteWiseTripNos Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getRouteWiseTripNos", sqlException);
        }
        return getRouteWiseTripNos;
    }

    public ArrayList getDashboardWorkShop(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList getDashboardWorkShop = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("dashboardopstrukCount", reportTO.getDashboardopstrukCount() + "%");
        //////System.out.println("map = " + map);
        try {
            getDashboardWorkShop = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDashboardWorkShop", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDashboardWorkShop Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getDashboardWorkShop", sqlException);
        }
        return getDashboardWorkShop;
    }

    public ArrayList getVehicleUT() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList vehicleUT = new ArrayList();
        try {
            vehicleUT = (ArrayList) getSqlMapClientTemplate().queryForList("report.vehicleUT", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getDriverList", sqlException);
        }
        return vehicleUT;
    }

    public ArrayList dbTrailerMake() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList trailerMake = new ArrayList();
        try {
            trailerMake = (ArrayList) getSqlMapClientTemplate().queryForList("report.dbTrailerMake", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("trailerMake Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "trailerMake", sqlException);
        }
        return trailerMake;
    }

    public ArrayList dbTruckType() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList truckType = new ArrayList();
        try {
            truckType = (ArrayList) getSqlMapClientTemplate().queryForList("report.dbTruckType", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("truckType Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "truckType", sqlException);
        }
        return truckType;
    }

    public ArrayList dbTrailerType() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList trailerType = new ArrayList();
        try {
            trailerType = (ArrayList) getSqlMapClientTemplate().queryForList("report.dbTrailerType", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("dbTrailerType Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "dbTrailerType", sqlException);
        }
        return trailerType;
    }

    public ArrayList getTrailerJobCardMTD() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList getTrailerJobCardMTD = new ArrayList();
        try {
            getTrailerJobCardMTD = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTrailerJobCardMTD", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTrailerjobcardMTD Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTrailerjobcardMTD", sqlException);
        }
        return getTrailerJobCardMTD;
    }

    public ArrayList getTruckJobCardMTD() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList getTruckJobCardMTD = new ArrayList();
        try {
            getTruckJobCardMTD = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTruckJobCardMTD", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTruckJobCardMTD Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTruckJobCardMTD", sqlException);
        }
        return getTruckJobCardMTD;
    }

    public ArrayList nodReport(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList statusList = new ArrayList();
        ArrayList dayDataList = new ArrayList();
        String[] dayArray = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8"};
        ReportTO statusTO = new ReportTO();
        ReportTO statusTO1 = new ReportTO();
        Iterator itr = null;
        Iterator itr1 = null;
        try {
            statusList = (ArrayList) getSqlMapClientTemplate().queryForList("report.statusList", map);
            for (int i = 0; i < dayArray.length; i++) {
                System.out.println(dayArray[i]);
                map.put("day", dayArray[i]);
                System.out.println("map  for day data list = " + map);
                dayDataList = (ArrayList) getSqlMapClientTemplate().queryForList("report.dayDataList", map);
                itr = statusList.iterator();
                itr1 = dayDataList.iterator();
                while (itr.hasNext()) {
                    statusTO = new ReportTO();
                    statusTO = (ReportTO) itr.next();
                    itr1 = dayDataList.iterator();
                    while (itr1.hasNext()) {
                        statusTO1 = new ReportTO();
                        statusTO1 = (ReportTO) itr1.next();
                        if (statusTO.getStatusId().equals(statusTO1.getNodstatusId())) {
                            if (dayArray[i].equals("0")) {
                                statusTO.setZeroDay(statusTO1.getNodTotalCount());
                                System.out.println("zero day=====" + statusTO1.getNodTotalCount());
                            } else if (dayArray[i].equals("1")) {
                                statusTO.setOneDay(statusTO1.getNodTotalCount());
                                System.out.println("one day=====" + statusTO1.getNodTotalCount());
                            } else if (dayArray[i].equals("2")) {
                                statusTO.setTwoDay(statusTO1.getNodTotalCount());
                                System.out.println("two day=====" + statusTO1.getNodTotalCount());
                            } else if (dayArray[i].equals("3")) {
                                statusTO.setThreeDay(statusTO1.getNodTotalCount());
                                System.out.println("three day=====" + statusTO1.getNodTotalCount());
                            } else if (dayArray[i].equals("4")) {
                                statusTO.setFourDay(statusTO1.getNodTotalCount());
                                System.out.println("four day=====" + statusTO1.getNodTotalCount());
                            } else if (dayArray[i].equals("5")) {
                                statusTO.setFiveDay(statusTO1.getNodTotalCount());
                                System.out.println("five day=====" + statusTO1.getNodTotalCount());
                            } else if (dayArray[i].equals("6")) {
                                statusTO.setSixDay(statusTO1.getNodTotalCount());
                                System.out.println("six day=====" + statusTO1.getNodTotalCount());
                            } else if (dayArray[i].equals("7")) {
                                statusTO.setSevenDay(statusTO1.getNodTotalCount());
                                System.out.println("seven day=====" + statusTO1.getNodTotalCount());
                            } else if (dayArray[i].equals("8")) {
                                statusTO.setMoreThanSevenDay(statusTO1.getNodTotalCount());
                                System.out.println("eight day=====" + statusTO1.getNodTotalCount());
                            }
                            break;
                        }
                    }
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("dayDataList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "nodReport", sqlException);
        }
        return statusList;

    }

    public ArrayList nodReports(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList statusLists = new ArrayList();
        ArrayList dayDataList1 = new ArrayList();
        String[] dayArrays = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8"};
        ReportTO statusTO = new ReportTO();
        ReportTO statusTO1 = new ReportTO();
        Iterator itr = null;
        Iterator itr1 = null;
        try {
            statusLists = (ArrayList) getSqlMapClientTemplate().queryForList("report.statusLists", map);
            for (int j = 0; j < dayArrays.length; j++) {
                System.out.println(dayArrays[j]);
                map.put("days", dayArrays[j]);
                System.out.println("map  for day data listssssss = " + map);
                dayDataList1 = (ArrayList) getSqlMapClientTemplate().queryForList("report.dayDataList1", map);
                itr = statusLists.iterator();
                itr1 = dayDataList1.iterator();
                while (itr.hasNext()) {
                    statusTO = new ReportTO();
                    statusTO = (ReportTO) itr.next();
                    itr1 = dayDataList1.iterator();
                    while (itr1.hasNext()) {
                        statusTO1 = new ReportTO();
                        statusTO1 = (ReportTO) itr1.next();
                        if (statusTO.getStatusId().equals(statusTO1.getNodstatusIds())) {
                            if (dayArrays[j].equals("0")) {
                                statusTO.setZeroDay(statusTO1.getNodTotalCounts());
                                System.out.println("zero day=ss====" + statusTO1.getNodTotalCounts());
                            } else if (dayArrays[j].equals("1")) {
                                statusTO.setOneDay(statusTO1.getNodTotalCounts());
                                System.out.println("one day==ss===" + statusTO1.getNodTotalCounts());
                            } else if (dayArrays[j].equals("2")) {
                                statusTO.setTwoDay(statusTO1.getNodTotalCounts());
                                System.out.println("two day==ss===" + statusTO1.getNodTotalCounts());
                            } else if (dayArrays[j].equals("3")) {
                                statusTO.setThreeDay(statusTO1.getNodTotalCounts());
                                System.out.println("three day==ss===" + statusTO1.getNodTotalCounts());
                            } else if (dayArrays[j].equals("4")) {
                                statusTO.setFourDay(statusTO1.getNodTotalCounts());
                                System.out.println("four day==ss===" + statusTO1.getNodTotalCounts());
                            } else if (dayArrays[j].equals("5")) {
                                statusTO.setFiveDay(statusTO1.getNodTotalCounts());
                                System.out.println("five day=ss====" + statusTO1.getNodTotalCounts());
                            } else if (dayArrays[j].equals("6")) {
                                statusTO.setSixDay(statusTO1.getNodTotalCounts());
                                System.out.println("six day==ss===" + statusTO1.getNodTotalCounts());
                            } else if (dayArrays[j].equals("7")) {
                                statusTO.setSevenDay(statusTO1.getNodTotalCounts());
                                System.out.println("seven day==ss===" + statusTO1.getNodTotalCounts());
                            } else if (dayArrays[j].equals("8")) {
                                statusTO.setMoreThanSevenDay(statusTO1.getNodTotalCounts());
                                System.out.println("eight day===ss==" + statusTO1.getNodTotalCounts());
                            }
                            break;
                        }
                    }
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("dayDataList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "nodReport", sqlException);
        }
        return statusLists;

    }

    public ArrayList nodReportLevel2(ReportTO reportTO) {
        Map map = new HashMap();
        System.out.println("im in DAO Muthu...");
        ArrayList nodLevel2List = new ArrayList();
        map.put("statusId", reportTO.getStatusId());
        map.put("day", reportTO.getNoOfDays());
        System.out.println("map = " + map);
        try {
            nodLevel2List = (ArrayList) getSqlMapClientTemplate().queryForList("report.nodReportLevel2", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAgentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "nodReportLevel2", sqlException);
        }
        return nodLevel2List;

    }

    public ArrayList nodReportLevel3(ReportTO reportTO) {
        Map map = new HashMap();
        System.out.println("im in DAO Muthu.222..");
        ArrayList nodLevel3List = new ArrayList();
        map.put("statusId", reportTO.getStatusId());
        map.put("day", reportTO.getNoOfDays());
        System.out.println("map = " + map);
        try {
            nodLevel3List = (ArrayList) getSqlMapClientTemplate().queryForList("report.nodReportLevel3", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAgentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "nodReportLevel2", sqlException);
        }
        return nodLevel3List;
    }

    public ArrayList getVendorList() {
        Map map = new HashMap();
        ArrayList vendorList = new ArrayList();

        try {

            vendorList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVendorNameDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vendorList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "vendorList", sqlException);
        }
        return vendorList;
    }

    public ArrayList getVendorPerformanceList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList VendorPerformanceList = new ArrayList();

        try {

            map.put("vehicleTypeId", reportTO.getVehicleTypeId());
            map.put("vendorId", reportTO.getVendorId());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("expenseType", reportTO.getExpenseType());

            if (!"".equals(reportTO.getExpenseType())) {
                System.out.println("vendor performance map : " + map);
                VendorPerformanceList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVendorPerformanceList", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVendorPerformanceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVendorPerformanceList", sqlException);
        }
        return VendorPerformanceList;
    }

    public ArrayList getVendorVehicleList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList getVendorVehicleList = new ArrayList();

        try {

            map.put("vehicleTypeId", reportTO.getVehicleTypeId());
            map.put("vendorId", reportTO.getVendorId());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("expenseType", reportTO.getExpenseType());

            if (!"".equals(reportTO.getExpenseType())) {
                System.out.println("vendor performance map : " + map);
                getVendorVehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVendorVehicleList", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVendorVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVendorVehicleList", sqlException);
        }
        return getVendorVehicleList;
    }

    public ArrayList getOverAllProfitability(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList getOverAllProfitability = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("dashboardopstrukCount", reportTO.getDashboardopstrukCount() + "%");
        //////System.out.println("map = " + map);
        try {
            getOverAllProfitability = (ArrayList) getSqlMapClientTemplate().queryForList("report.getOverAllProfitability", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOverAllProfitability Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getOverAllProfitability", sqlException);
        }
        return getOverAllProfitability;
    }

    public ArrayList getEmailList() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList emailList = new ArrayList();
        try {
            emailList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getEmailList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmailList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getEmailList", sqlException);
        } finally {
            map = null;
        }
        return emailList;
    }

    public ArrayList dsrReport(ReportTO reportTO) {

        Map map = new HashMap();
        ArrayList dsrReport = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());

        if (reportTO.getCustomerId().equals("0")) {
            map.put("customerId", "");
        } else {
            map.put("customerId", reportTO.getCustomerId());
        }

        if (reportTO.getVehicleTypeId().equals("0")) {
            map.put("vehicleTypeId", "");
        } else {
            map.put("vehicleTypeId", reportTO.getVehicleTypeId());
        }

        System.out.println("map = " + map);

        try {
            dsrReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.dsrReport", map);
            System.out.println("dsrReport=" + dsrReport.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("dsrReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "dsrReport", sqlException);
        }
        return dsrReport;

    }

    public ArrayList dsrReportMail(ReportTO reportTO) {

        Map map = new HashMap();
        ArrayList dsrReport = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());

        if (reportTO.getCustomerId().equals("0")) {
            map.put("customerId", "");
        } else {
            map.put("customerId", reportTO.getCustomerId());
        }

        if (reportTO.getVehicleTypeId().equals("0")) {
            map.put("vehicleTypeId", "");
        } else {
            map.put("vehicleTypeId", reportTO.getVehicleTypeId());
        }
        if (reportTO.getOrigin().equals("0") || reportTO.getOrigin() == null) {
            map.put("origin", "");
        } else {
            map.put("origin", reportTO.getOrigin());
        }
        
        if(reportTO.getFleetCenterId()!=null){
        map.put("fleetcenterId", reportTO.getFleetCenterId());
        }
        else{
        map.put("fleetcenterId", "");        
        }

        System.out.println("map = " + map);

        try {
            dsrReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.dsrReportMail", map);
            System.out.println("dsrReport=" + dsrReport.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("dsrReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "dsrReport", sqlException);
        }
        return dsrReport;

    }

    public ArrayList custDSROrigin(ReportTO reportTO) {

        Map map = new HashMap();
        ArrayList dsrReport = new ArrayList();
        
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());        
        
        if(reportTO.getFleetCenterId()!=null){
        map.put("fleetcenterId", reportTO.getFleetCenterId());
        }
        else{
        map.put("fleetcenterId", "");        
        }
        
        if (reportTO.getCustomerId().equals("0")) {
            map.put("customerId", "");
        } else {
            map.put("customerId", reportTO.getCustomerId());
        }

        if (reportTO.getVehicleTypeId().equals("0")) {
            map.put("vehicleTypeId", "");
        } else {
            map.put("vehicleTypeId", reportTO.getVehicleTypeId());
        }

        System.out.println("custDSROrigin map = " + map);

        try {
            dsrReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.custDSROrigin", map);
            System.out.println("dsrReport=" + dsrReport.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("dsrReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "dsrReport", sqlException);
        }
        return dsrReport;

    }

    public int insertMailDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        int insertMailDetails = 0;
        map.put("mailTypeId", reportTO.getMailTypeId());
        map.put("mailSubjectTo", reportTO.getMailSubjectTo());
        map.put("mailSubjectCc", reportTO.getMailSubjectCc());
        map.put("mailSubjectBcc", reportTO.getMailSubjectCc());
        map.put("mailContentTo", reportTO.getMailContentTo());
        map.put("mailContentCc", reportTO.getMailContentCc());
        map.put("mailContentBcc", reportTO.getMailContentCc());
        map.put("mailTo", reportTO.getMailIdTo());
        map.put("mailCc", reportTO.getMailIdCc());
        map.put("mailBcc", "");
        map.put("userId", userId);
        map.put("mailDeliveredStatus", 0);
        map.put("customerId", reportTO.getCustomerId());

        if (!reportTO.getFilePath().equals("") && reportTO.getFilePath() != null) {
            map.put("filePath", reportTO.getFilePath());
            map.put("fileName", reportTO.getFileName());
        } else {
            map.put("filePath", "");
            map.put("fileName", "");
        }

        System.out.println(" mail  map = " + map);
        try {
            insertMailDetails = (Integer) getSqlMapClientTemplate().insert("report.insertMailDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertMailDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertMailDetails", sqlException);
        } finally {
            map = null;
        }
        return insertMailDetails;

    }

    public ArrayList getTripPODReportAlert() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList tripPODReportAlert = new ArrayList();
        try {
            tripPODReportAlert = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripPODReportAlert", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripPODReportAlert Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripPODReportAlert", sqlException);
        } finally {
            map = null;
        }
        return tripPODReportAlert;
    }

    public ArrayList getEmailCustomerList() {
        Map map = new HashMap();
        ArrayList custList = new ArrayList();
        try {

            custList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getEmailCustomerList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmailCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getEmailCustomerList", sqlException);
        }
        return custList;
    }

    public ArrayList vendorOutStandingReport(ReportTO reportTO) {

        Map map = new HashMap();
        ArrayList vendorOutStandingReport = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("vendorId", reportTO.getVendorName());
        map.put("invoiceNo", reportTO.getInvoiceNo());
        try {
            System.out.println("map: " + map);
            vendorOutStandingReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.vendorOutStandingReport", map);
            System.out.println("vendorOutStandingReport=" + vendorOutStandingReport.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vendorOutStandingReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "dsrReport", sqlException);
        }
        return vendorOutStandingReport;

    }

    public ArrayList custommerOutStandingReport(ReportTO reportTO) {

        Map map = new HashMap();
        ArrayList custommerOutStandingReport = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("customerId", reportTO.getCustomerId());
        try {
            System.out.println("map: " + map);
            custommerOutStandingReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.custommerOutStandingReport", map);
            System.out.println("custommerOutStandingReport=" + custommerOutStandingReport.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("custommerOutStandingReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "dsrReport", sqlException);
        }
        return custommerOutStandingReport;

    }

    public String getCallConsignment() {

        Map map = new HashMap();
        String getCallConsignment = "";
        try {

            System.out.println("map: " + map);
            //custommerOutStandingReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.custommerOutStandingReport", map);
            //System.out.println("getCallConsignment=" + custommerOutStandingReport.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCallConsignment Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "dsrReport", sqlException);
        }
        return getCallConsignment;

    }

    public ArrayList getCustRevenu(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList getCustRevenu = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("dashboardopstrukCount", reportTO.getDashboardopstrukCount() + "%");
        System.out.println("today arun mapped 27-11-2018 = " + map);
        try {
            getCustRevenu = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCustRevenu", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustRevenu Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getOverAllTripNos", sqlException);
        }
        return getCustRevenu;
    }

    public ArrayList getCustProfit(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList getCustProfit = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("dashboardopstrukCount", reportTO.getDashboardopstrukCount() + "%");
        System.out.println("today arun mapped 27-11-2018 = " + map);
        try {
            getCustProfit = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCustProfit", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustProfit Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getOverAllTripNos", sqlException);
        }
        return getCustProfit;
    }

    public ArrayList invoiceUnpostedReport() {
        Map map = new HashMap();
        ArrayList invoiceUnpostedReport = new ArrayList();

        try {
            invoiceUnpostedReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.getInvoiceUnpostedReport", map);
            System.out.println("invoiceUnpostedReport = " + invoiceUnpostedReport.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("invoiceUnpostedReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "invoiceUnpostedReport", sqlException);
        } finally {
            map = null;
        }
        return invoiceUnpostedReport;

    }

    public ArrayList advanceUnpostedReport() {
        Map map = new HashMap();
        ArrayList advanceUnpostedReport = new ArrayList();

        try {
            advanceUnpostedReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.getInvoiceUnpostedReport", map);
            System.out.println("advanceUnpostedReport = " + advanceUnpostedReport.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("advanceUnpostedReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "advanceUnpostedReport", sqlException);
        } finally {
            map = null;
        }
        return advanceUnpostedReport;
    }

    public int saveMailAlert(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        int lastInsertedId = 0;
        try {

            map.put("idNo", reportTO.getIdentityNo());
            map.put("reportType", reportTO.getReporttype());
            map.put("mailIdTo", reportTO.getMailIdTo());
            map.put("mailIdCc", reportTO.getMailIdCc());
            map.put("customerId", reportTO.getCustomerId());
            map.put("active_ind", reportTO.getActiveInd());
            map.put("userId", userId);
            map.put("fleetCenterId", reportTO.getFleetCenterId());

            System.out.println("map in  " + map);

            if (reportTO.getIdentityNo() == null || reportTO.getIdentityNo().equals("")) {

                int checkFlag = (Integer) getSqlMapClientTemplate().queryForObject("report.CheckMailAlertId", map);

                if (checkFlag == 0) {
                    lastInsertedId = (Integer) getSqlMapClientTemplate().update("report.saveMailAlert", map);
                }
            } else {
                lastInsertedId = (Integer) getSqlMapClientTemplate().update("report.updateMailAlert", map);
            }

            System.out.println("lastInsertedId -->" + lastInsertedId);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveMailAlert Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveMailAlert ", sqlException);
        }
        return lastInsertedId;
    }

    public ArrayList getReport(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList getReport = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("getReport", reportTO.getDashboardopstrukCount() + "%");
        System.out.println("getReport = " + map);
        try {
            getReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.getReport", map);

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getReport", sqlException);
        }
        return getReport;
    }

    public String getMailAlertIds(ReportTO reportTO) {

        Map map = new HashMap();
        String checkMailIds = "";
        try {
            map.put("reportType", reportTO.getReporttype());
            map.put("customerId", reportTO.getCustomerId());
            System.out.println("map: " + map);
            checkMailIds = (String) getSqlMapClientTemplate().queryForObject("report.getMailAlertIds", map);
            System.out.println("checkMailIds=" + checkMailIds);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkMailIds Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "checkMailIds", sqlException);
        }
        return checkMailIds;

    }

    public ArrayList getGPSVehicleList() {
        Map map = new HashMap();
        ArrayList getVehicleList = new ArrayList();
        try {
            getVehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getGPSVehicleNo", map);
            System.out.println(" getGPSVehicleList===" + getVehicleList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getGPSVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getGPSVehicleList List", sqlException);
        }

        return getVehicleList;
    }

    public ArrayList getGPSTrip(String vehicleRtoNo) {
        Map map = new HashMap();
        ArrayList getGPSTrip = new ArrayList();
        try {
            map.put("vehicleNo", vehicleRtoNo);
            getGPSTrip = (ArrayList) getSqlMapClientTemplate().queryForList("report.getGPSTrip", map);
            System.out.println(" vehicleNo===" + getGPSTrip.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getGPSTrip Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getGPSTrip List", sqlException);
        }

        return getGPSTrip;
    }

    public int updateGPSVehicleList(ReportTO reportTO) {
        int status = 0;
        Map map = new HashMap();
        map.put("latitude", reportTO.getLatitude());
        map.put("longitude", reportTO.getLongitude());
        map.put("location", reportTO.getLocation());
        map.put("speed", "0");
        map.put("lastupdatedTime", reportTO.getLastUpdateTime());
        map.put("vehicleId", reportTO.getVehicleId());
        map.put("vehicleNo", reportTO.getVehicleNo());
        map.put("tripId", reportTO.getTripId());
        map.put("deviceId", reportTO.getIdentityNo());
        map.put("remKM", reportTO.getRemaingKM());
        map.put("RemTime", reportTO.getRemaingTime());
        map.put("ETA", reportTO.getCurrentETA());
        map.put("mobile", reportTO.getMobileNo());

        System.out.println("map updateGPSVehicleList : " + map);

        try {

            int checkVehicle = (Integer) getSqlMapClientTemplate().queryForObject("report.checkVehicleLocation", map);

            if (checkVehicle == 0) {

                status = (Integer) getSqlMapClientTemplate().update("report.insertVehicleLocation", map);
            } else {
                status = (Integer) getSqlMapClientTemplate().update("report.updateVehicleLocation", map);
            }

             //   status = (Integer) getSqlMapClientTemplate().update("report.insertVehicleLocationLog", map);
            System.out.println("status:::" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateGPSVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateGPSVehicleList", sqlException);
        }

        return status;
    }

    public ArrayList getTripDataList() {
        Map map = new HashMap();
        ArrayList getTripDataList = new ArrayList();
        try {
            
            getTripDataList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripDataList", map);
            System.out.println(" getTripDataList===" + getTripDataList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDataList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDataList List", sqlException);
        }

        return getTripDataList;
    }

        public int updateGPSData(String reportId) {
        Map map = new HashMap();
        
        map.put("reportId", reportId);
        int tripCount = 0;
        try {
                tripCount = (Integer) getSqlMapClientTemplate().update("report.updateGPSData", map);
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return tripCount;
    }
        
         public ArrayList getTripEndReportAlert() {
            Map map = new HashMap();
            ArrayList getTripDataList = new ArrayList();
            try {
                getTripDataList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripEndReportAlert", map);
                System.out.println(" getTripDataList===" + getTripDataList.size());
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getTripDataList Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDataList List", sqlException);
            }
    
            return getTripDataList;
    }
    
    
}