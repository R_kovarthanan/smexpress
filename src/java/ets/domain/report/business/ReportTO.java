/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.report.business;

import java.util.ArrayList;
import ets.arch.util.FPUtil;
import java.io.ByteArrayOutputStream;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ReportTO {

    private String latitude = null;
    private String sealno = null;
    private String lastUpdateTime = null;
    private String longitude = null;
    private String remaingTime = null;
    private String remaingKM = null;
    private String currentETA = null;
    private String cgstAmt = null;
    private String mailTime = null;
    private String activeInd = null;
    private String sgstAmt = null;
    private String igstAmt = null;
    private String cgst = null;
    private String sgst = null;
    private String igst = null;
    private String reportId = null;
    private String vendorType = null;
    private String customerGSTNo = null;
    private int postId=0;
    private int advanceId=0;
    private String customerErpId = null;
    private String emailDetails = null;
    
    private String initialTripAdvance = null;
    private String invoiceAmount = null;
    // Email Content
    private String serverName = null;
    private String serverIpAddress = null;
    private HSSFWorkbook myWorkbook = null;
    private String totalCount = "";
    private String fileName = "";
    private String filePath = "";
    private ByteArrayOutputStream outputStream = null;
    private String mailSendingId = null;
    private String mailSubjectTo = null;
    private String mailSubjectCc = null;
    private String mailContentTo = null;
    private String mailContentCc = null;
    private String mailIdTo = null;

    private String mailIdCc = null;
    private String mailIdBcc = null;
    private String mailDeliveredStatus = null;
    private String mailTypeId = null;

//    NOD report
    private String segmentName = null;
    private String consignmentDate = null;
    private String nodstatusId = null;
    private String nodStatusName = null;
    private String nodTotalCount = null;
    private String zeroDay = null;
    private String oneDay = null;
    private String twoDay = null;
    private String threeDay = null;
    private String fourDay = null;
    private String fiveDay = null;
    private String sixDay = null;
    private String sevenDay = null;
    private String moreThanSevenDay = null;
    private String nodstatusIds = "";
    private String nodTotalCounts = "";
//    NOD level2
    private String noOfDays = "";
    private String freightCharges = "";
    private String orderRefNo = "";
    private String consignmentNo = "";
    //    NOD Level3
    private String tripEnd = "";
    private String tripStart = "";

    private String vehicleMake = "";
    private String count = "";
    private String dashboardopstrukCount = "";
    private String reportDateTime = "";
    private String tyerType = "";
    private String unInstallationDate = "";
    private String installationDate = "";
    private String installationKM = "";
    private String unInstallationKM = "";
    private String tyreAmount = "";
    private String regularVehicleService = "";
    private String breakDownReeferWorks = "";
    private String breakDownVehicleWorks = "";
    private String accidental = "";
    private String asset = "";
    private String regularReeferService = "";
    private String month1RegularVehicleService = "";
    private String month1BreakDownReeferWorks = "";
    private String month1BreakDownVehicleWorks = "";
    private String month1Accidental = "";
    private String month1Asset = "";
    private String month1RegularReeferService = "";
    private String month2RegularVehicleService = "";
    private String month2BreakDownReeferWorks = "";
    private String month2BreakDownVehicleWorks = "";
    private String month2Accidental = "";
    private String month2Asset = "";
    private String month2RegularReeferService = "";
    private String month3RegularVehicleService = "";
    private String month3BreakDownReeferWorks = "";
    private String month3BreakDownVehicleWorks = "";
    private String month3Accidental = "";
    private String month3Asset = "";
    private String month3RegularReeferService = "";
    private String newTyreAmount = "";
    private String retreadTyreAmount = "";
    private String retreadTyreAmount1 = "";
    private String retreadTyreAmount2 = "";
    private String retreadTyreAmount3 = "";
    private String newTyreAmount1 = "";
    private String newTyreAmount2 = "";
    private String newTyreAmount3 = "";
    private String startCount = "";
    private String startRevenue = "";
    private String endCount = "";
    private String endRevenue = "";
    private String previoisStartCount = "";
    private String previoisStartRevenue = "";
    private String previoisEndCount = "";
    private String previoisEndRevenue = "";
    private String totalVehicle = "";
    private String inProgress = "";
    private String driverIssue = "";
    private String repairMaintenance = "";
    private String type = "";
    private String totalTrip = "";
    private String containerAmount3 = "";
    private String referAmount3 = "";
    private String chasisAmount3 = "";
    private String containerAmount1 = "";
    private String referAmount1 = "";
    private String chasisAmount1 = "";
    private String containerAmount2 = "";
    private String referAmount2 = "";
    private String chasisAmount2 = "";
    private String containerAmount = "";
    private String referAmount = "";
    private String chasisAmount = "";
    private String rnmAdvance = "";
    private String foodingAdvance = "";
    private String repairAdvance = "";
    private String accountMgrId = "";
    private String wfuAmount = "";
    private String emptyTripKM = "";
    private String totalCost = "";
    private String tripendtime = "";
    private String customerType = "";
    private String tripEndTime = "";
    private String requestAmount;
    private String paidAmount;
    private String wflHours;
    private String wfuHours;
    private String transitHours;
    private String loadingTransitHours;
    private String endDateTime;
    private String originReportingDateTime;
    private String destinationReportingDateTime;
    private String totalRunKm;
    private String totalRunHm;
    private String rcm;
    private String actualExpense;
    private String startDateTime = "";
    private String primaryDriver = "";
    private String secondaryDriver = "";
    private String advanceRemarks = "";
    private String advancePaidDate = "";
    private String expenseType = "";
    private String expenseName = "";
    private String expenseRemarks = "";
    private String expenseDriverName = "";
    private String expenseDate = "";
    private String orderSequence = "";
    private String emptyExpense = "";
    private String loadedExpense = "";
    private String earnings = "";
    private String tripExtraExpense = "";
    private String tripMergingId = "";
    private String vehicletype = "";
    private String kmReading = "";
    private String kmReadingDate = "";
    private String hmReading = "";
    private String hmReadingDate = "";
    private String closeDate = "";
    private String settledDate = "";
    private String mergingDate = "";
    private String startDate = "";
    private String endDate = "";
    private String issueType = "";
    private String jobCardDays = "";
    private String plannedCompleteDays = "";
    private String outStandingDays = "";
    private String jobCardNo = "";
    private int amountSpend = 0;
    private int tripCount = 0;
    private int percent = 0;
    private String tripRunKm = "";
    private String tripRunHm = "";
    private String tripDays = "";
    private String unLoadingDate = "";
    private String loadingDateTime = "";
    private String not = "";
    private String tripStatusIdTo = "";
    private String startDateFrom = "";
    private String startDateTo = "";
    private String endDateFrom = "";
    private String endDateTo = "";
    private String closedDateFrom = "";
    private String closedDateTo = "";
    private String settledDateFrom = "";
    private String settledDateTo = "";
    private String queryType = "";
    private String serviceTypeName = "";
    private String estimatedRevenue = "";
    private String tripstarttime = "";
    private String location = "";
    private String vehiclewfudate = "";
    private String vehiclewfutime = "";
    private String statusName = "";
    private String status = "";
    private String tonnage = "";
    private String vehicleNo = "";
    private String cityName = "";
    private String zoneName = "";
    private String operationPoint = "";
    private String fleetCenterHead = "";
    private String oldVehicleNo = "";
    private String oldDriverName = "";
    private String businessType = "";
    private String customerOrderReferenceNo = "";
    private String cNotes = "";
    private String routeInfo = "";
    private String fleetCenterName = "";
    private String orderRevenue = "";
    private String orderExpense = "";
    private String actualAdvancePaid = "";
    private String vehicleTonnage = "";
    private String gpsLocation = "";
    private String plannedStartDateTime = "";
    private String actualStartDateTime = "";
    private String plannedEndDateTime = "";
    private String actualEndDateTime = "";
    private String wfuDateTime = "";
    private String wfuRemarks = "";
    private String wfuCreatedOn = "";
    private String endedBy = "";
    private String closedBy = "";
    private String settledBy = "";
    private String fleetCenterNo = "";
    private String mobileNo = "";
    private String extraExpenseValue = "";
    private String expectedArrivalDateTime = "";
//    tripDetails  

    private int userId = 0;
    private String cityFromId = "";
    private String zoneId = "";
    private String podStatus = "";
    private String requestedAdvance = "";
    private String paidAdvance = "";
    private String rmDays = "";
    private String days = "";
    private String months = "";
    private String gpsEndAttempt = "";
    private String gpsEndErrorCode = "";
    private String gpsEndErrorMsg = "";
    private String gpsSystem = "";
    private String tripStartDateTime = "";
    private String tripEndDateTime = "";
    private String tsStartInd = "";
    private String gpsStartInd = "";
    private String gpsStartUpdateTime = "";
    private String tsEndInd = "";
    private String gpsEndInd = "";
    private String gpsEndUpdateTime = "";
    private String distanceTravelled = "";
    private String reeferRunHours = "";
    private String reeferAvgTemp = "";
    private String gpsStartAttempt = "";
    private String gpsStartErrorCode = "";
    private String gpsStartErrorMsg = "";
    private String gpsTripDetailsAttempt = "";
    private String gpsTripDetailsErrorCode = "";
    private String gpsTripDetailsErrorMsg = "";
    private String gpsTripDetailsInd = "";
    private String gpsTripDetailsDatetime = "";
    private String transactionHistoryId = "";
    private String bPCLAccountId = "";
    private String dealerName = "";
    private String dealerCity = "";
    private String transactionDate = "";
    private String accountingDate = "";
    private String transactionType = "";
    private String currency = "";
    private String transactionAmount = "";
    private String volumeDocumentNo = "";
    private String amountBalance = "";
    private String petromilesEarned = "";
    private String odometerReading = "";
    private String fuelPrice = "", runHour = "", dieselUsed = "", rcmAllocation = "", bpclAllocation = "", extraExpense = "", totalMiscellaneous = "", bhatta = "", startingBalance = "", endingBalance = "", payMode = "";
    private String currentTemperature = "";
    private String logDate = "";
    private String logTime = "";
    private String billableKM = "";
    private String daysWithOverDue = "";
    private String daysToOverDue = "";
    private String overDueOn = "";
    private String consignmentNote = "";
    private double totalAmountReceivable = 0;
    private double creditLimit = 0;
    private String customerCode = "";
    private double overDueGrandTotal = 0;
    private double profitValue = 0;
    private double profitPercent = 0;
    private String operationTypeId = "";
    private String profitType = "";
    private String approvalstatus = "";
    private String consignmentNoteNo = "";
    String invoiceType = "";
    private String utilisedDays = "";
    private String tripStatusId = "";
    private String fleetCenterId = "";
    private String vehicleCapUtil = "";
    private double vehicleDriverSalary = 0;
    private double tripOtherExpense = 0;
    private double miscAmount = 0;
    private String totalWeight = "";
    private String tripNos = "";
    private String date = "";
    private double insuranceAmount = 0;
    private double fcAmount = 0;
    private double roadTaxAmount = 0;
    private double permitAmount = 0;
    private double emiAmount = 0;
    private double tollAmount = 0;
    private double fuelAmount = 0;
    private double driverIncentive = 0;
    private double driverBata = 0;
    private double driverExpense = 0;
    private double routeExpenses = 0;
    private double maintainExpense = 0;
    private double netExpense = 0;
    private double netProfit = 0;
    private int timePeriod = 0;
    private double fixedExpensePerDay = 0;
    private double totlalFixedExpense = 0;
    private double totlalOperationExpense = 0;
    private String reeferRequired = "";
    private String invoiceCode = "";
    private String invRefCode = "";
    private String invoiceDate = "";
    private String billingType = "";
    private String ownerShip = "";
    private String freightAmount = "";
    private String otherExpenseAmount = "";
    private String grandTotal = "";
    private String invoiceStatus = "";
    private String numberOfTrip = "";
    private String fromDate = "";
    private String toDate = "";
    private String remarks = "";
    private String customerName = "";
    private String CNAmount = "";
    private String DNAmount = "";
    //Throttle Ends
    //Throttle
    // Brattle Foods  end
    // Brattle Foods starts
    //Throttle starts
    //Throttle Ends
    //Throttle
    // Brattle Foods  end
    private String jobCardId = "";
    private String dueIn = "";
    private String bayNo = "";
    private String groupName = "";
    ArrayList serviceTypeListAll = null;
    
    private String secondaryInvoice = "";
    private String secondaryDocketNo = "";
    private String docketNo = "";
    private String origin = "";
    private String boxes = "";
    private String totalPackages = "";
    private String invoiceWeight = "";
    private String eWayBillNo = "";
    private String consignorName = "";
    private String tripmode = "";
    private String planeDate = "";
    
    private String consignorCode = "";
    private String consigneeCode = "";    
    
    private String distanceCovered = "";
    private String distanceRemaining = "";
    private String delayReason = "";

    public String getDistanceCovered() {
        return distanceCovered;
    }

    public void setDistanceCovered(String distanceCovered) {
        this.distanceCovered = distanceCovered;
    }

    public String getDistanceRemaining() {
        return distanceRemaining;
    }

    public void setDistanceRemaining(String distanceRemaining) {
        this.distanceRemaining = distanceRemaining;
    }

    public String getDelayReason() {
        return delayReason;
    }

    public void setDelayReason(String delayReason) {
        this.delayReason = delayReason;
    }


    public String getSecondaryInvoice() {
        return secondaryInvoice;
    }

    public void setSecondaryInvoice(String secondaryInvoice) {
        this.secondaryInvoice = secondaryInvoice;
    }

    public String getSecondaryDocketNo() {
        return secondaryDocketNo;
    }

    public void setSecondaryDocketNo(String secondaryDocketNo) {
        this.secondaryDocketNo = secondaryDocketNo;
    }

    public String getDocketNo() {
        return docketNo;
    }

    public void setDocketNo(String docketNo) {
        this.docketNo = docketNo;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getBoxes() {
        return boxes;
    }

    public void setBoxes(String boxes) {
        this.boxes = boxes;
    }

    public String getTotalPackages() {
        return totalPackages;
    }

    public void setTotalPackages(String totalPackages) {
        this.totalPackages = totalPackages;
    }

    public String getInvoiceWeight() {
        return invoiceWeight;
    }

    public void setInvoiceWeight(String invoiceWeight) {
        this.invoiceWeight = invoiceWeight;
    }

    public String geteWayBillNo() {
        return eWayBillNo;
    }

    public void seteWayBillNo(String eWayBillNo) {
        this.eWayBillNo = eWayBillNo;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getTripmode() {
        return tripmode;
    }

    public void setTripmode(String tripmode) {
        this.tripmode = tripmode;
    }

    public String getPlaneDate() {
        return planeDate;
    }

    public void setPlaneDate(String planeDate) {
        this.planeDate = planeDate;
    }
    
    

    public ArrayList getServiceTypeListAll() {
        return serviceTypeListAll;
    }

    public void setServiceTypeListAll(ArrayList serviceTypeListAll) {
        this.serviceTypeListAll = serviceTypeListAll;
    }
    private String lastProblem = "";
    private String lastTech = "";
    private String lastStatus = "";
    private String lastKm = "";
    private String lastRemarks = "";
    private String regNo = "";
    private String billNo = "";
    private String totalAmount = "";
    private String laborAmount = "";
    private String spareAmount = "";
    private String woAmount = "";
    private String discount = "";
    private String billDate = "";
    private String companyName = "";
    private String problemName = "";
    private String empName = "";
    private String user = "";
    private String companyId = "";
    private String serviceName = "";
    private String serviceTypeId = "";
    private String problem = "";
    private String problemStatus = "";
    private String createdDate = "";
    private String rcCode = "";
    private String quantity = "";
    private String phone = "";
    private String rcWoId = "";
    private String categoryName = "";
    private String pcd = "";
    private String acd = "";
    private String technician = "";
    private String technicianId = "";
    private String sectionId = "";
    private String section = "";
    private String activity = "";
    private String vendorId = "";
    private String period = "";
    private String mfrName = "";
    private String modelName = "";
    private String purchaseAmt = "";
    private String paplCode = "";
    private String itemName = "";
    private String vendorName = "";
    private String poId = "";
    private String aqty = "";
    private String purDate = "";
    private String mfrCode = "";
    private String mfrId = "";
    private String modelId = "";
    private String newQty = "";
    private String rcQty = "";
    private String totalQty = "";
    private String stockValue = "";
    private String processId = "";
    private String newPrice = "";
    private String unitPrice = "";
    private String unusedQty = "";
    private String purType = "";
    private String billAmount = "";
    private String netAmount = "";
    private String scheduledDate = "";
    private String itemAmount = "";
    private String address = "";
    private String itemPrice = "";
    private String spares = "";
    private String labour = "";
    private String tax = "";
    private String contractAmnt = "";
    private String serviceTaxAmnt = "";
    private String itemQty = "";
    private String itemId = "";
    private String activityAmount = "";
    private String woId = "";
    private String nextFc = "";
    private String due = "";
    private String uomName = "";
    private String categoryId = "";
    private String mrsId = "";
    private String day = "";
    private String supplyId = "";
    private String invoiceId = "";
    private String rqty = "";
    private String issueQty = "";
    private String retQty = "";
    private String issueDate = "";
    private String raisedProp = "";
    private String completedProp = "";
    private String completedDate = "";
    private String custName = "";
    private String usageType = "";
    private String totalIssued = "";
    private String schedule = "";
    private String diff = "";
    private String lastDate = "";
    private String arrival = "";
    private String buyDate = "";
    private String buyPrice = "";
    private String sellPrice = "";
    private String profit = "";
    private String nettProfit = "";
    private String age = "";
    private String vehicleId = "";
    private String mrp = "";
    private String km = "";
    private String description = "";
    private String month = "";
    private String newTyre = "";
    private String reTreadTyre = "";
    private String rcId = "";
    private String rcTime = "";
    private String inTime = "";
    private String tyreNo = "";
    private String posName = "";
    private String hikedAmount = "";
    private String margin = "";
    private String payableTax = "";
    static FPUtil fpUtil = FPUtil.getInstance();
    private float hikePercentage = Float.parseFloat(fpUtil.getInstance().getProperty("HIKE_PERCENTAGE"));
    private String sparesAmount = "";
    private String sparesWithTax = "";
    private String freight = "";
    private String approver = "";
    private String orderType = "PO";
    private String[] addressSplit = null;
    private String[] remarksSplit = null;
    private String billType = "";
    private String dcNo = "";
    private String purpose = "";
    private String custId = "";
    private String totJc = "";
    private String itemType = "";
    private String vehCount = "";
    private String year = "";

    private String month1 = "";
    private String month2 = "";
    private String month3 = "";
    private String month1ReTread = "";
    private String month2ReTread = "";
    private String month3ReTread = "";
    private String month1New = "";
    private String month2New = "";
    private String month3New = "";

    private String year1 = "";
    private String year2 = "";
    private String year3 = "";

    private String servicePointId = "";
    private String monthName = "";
    private float amount = 0.0f;
    private String usageTypeIds = "";
    private String usageTypeName = "";
    private String vehicleTypeId = "";
    private String vehicleTypeName = "";
    private String colour = "";
    private String name = "";
    private String district = "";
    String mfr = "";
    String usage = "";
    String vehicleType = "";
    private String completed = "";
    private String notPlanned = "";
    private String billingCompleted = "";
    private String planned = "";
    //shankar
    String itemCode = "";
    int usageTypeId = 0;
    String itemTypes = "";
    String stworth = "";

    String invoiceNo = "";
    ArrayList vatValues = null;
    float taxAmount = 0.0f;
    //Hari
    private int reportType = 0;
    private String reportAmount = null;
    private String rcSend = null;
    private String rcReceive = null;
    private String reporttype = null;
    private int mfrid = 0;
    private int usageid = 0;
    private int typeId = 0;
    private int life = 0;
    private int itemCount = 0;
    private Float price = 0.0f;
    private String manualMrsNo = null;
    private String manualMrsDate = null;
    private int movingAverage = 0;
    private String firstHalfDays = null;
    private String lastHalfDays = null;
    private String spltQty = null;
    private String monthAverage = null;
    private String materialCostInternal = null;
    private String materialCostExternal = null;
    private String laborCharge = null;
    private int rcWorkorderId = 0;
    private int counterId = 0;
//  bala
    private int noOfVehicles = 0;
    private String customerTypeId = "0";
    private String requiredDate = "";
    private int xmlId = 0;
    private int dataType = 0;
    private int noOfCards = 0;
    private String empId = "";
    private String driName = "";
    private String driId = "";
    private String registerNo = "";
    private String settlementId = "";
    private String driverName = "";
    private String cleanerName = "";
    private String totalDays = "";
    private String endKm = "";
    private String startKM = "";
    private String runningKm = "";
    private String runKm = "";
    private String runHm = "";
    private String noOfTrips = "";
    private String totalTonnageAmount = "";
    private String shortage = "";
    private String totalDiesel = "";
    private String actualKM = "";
    private String dieselAmount = "";
    private String income = "";
    private String driverExpenses = "";
    private String generalExpenses = "";
    private String driverSalary = "";
    private String balance = "";
    private String cleanerSalary = "";
    private String totalExpenses = "";
    private String driverAdvance = "";
    private String shortage1 = "";
    private String driverCleanerExpenses = "";
    private String perDaysIncome = "";
    private String duefromDriver = "";
    private String lossOrDay = "";
    private String driverBataSalary = "";
    private String cleanerBataSalary = "";
    private String totalSalary = "";
    private String cBalance = "";
    private String dDueFromDriver = "";
    private String busExpenses = "";
    private String actualSalary = "";
    private String dBalance = "";
    private String noOfTrips5k = "";
    private String kmpl = "";
    private String speedOMeterKm = "";
    private String sKmpl = "";
    private String aboveBelowKM = "";
    private String cDate = "";
    private String cVNo = "";
    private String cRs = "";
    private String dDate = "";
    private String dVNo = "";
    private String dRs = "";
    private String cFRs = "";
    //Rathimeena Driver Settlement Start
    private String tripSheetId = "";
    private String tripCode = "";
    private String tripRouteId = "";
    private String tripVehicleId = "";
    private String tripScheduleId = "";
    private String tripDriverId = "";
    private String tripDate = "";
    private String tripDepartureDate = "";
    private String tripArrivalDate = "";
    private String tripKmsOut = "";
    private String tripKmsIn = "";
    private String tripTotalLitres = "";
    private String tripFuelAmount = "";
    private String tripTotalAllowances = "";
    private String tripTotalExpenses = "";
    private String tripTotalKms = "";
    private String tripBalanceAmount = "";
    private String tripStatus = "";
    private String tripId = "";
    private String identityNo = "";
    private String deviceId = "";
    private String routeName = "";
    private String outKM = "";
    private String outDateTime = "";
    private String inKM = "";
    private String inDateTime = "";
    private String settlementFlag = "";
    private String issuerName = "";
    private String designation = "";
    private String advDatetime = "";
    private String fuelName = "";
    private String bunkName = "";
    private String liters = "";
    private String fuelDatetime = "";
    private String inOutIndication = "";
    private String inOutDateTime = "";
    private String locationName = "";
    private String totalTonnage = "";
    private String deliveredTonnage = "";
    private String embarkDate = "";
    private String alightDate = "";
    private String alightStatus = "";
    private String createdOn = "";
    private String expensesDesc = "";
    private String compId = "";
    private String regno = "";
    private String driverDue = "";
    private String statusDate = "";
    private String statusId = "";
    private String tripType = "";
    private String totalTonAmount = "";
    private String tripStartDate = "";
    private String tripEndDate = "";
    private String vehicleInOut = "";
    private String knownLocation = "";
    private String currentStatus = "";
    private String tollExpense = "";
    private String dieselExpense = "";
    private String totalExpense = "";
    private String locationId = "";
    private String permitType = "";
    private String amcCompanyName = "";
    private String amcAmount = "";
    private String amcDuration = "";
    private String createdBy = "";
    private int driverId = 0;
    //Rathimeena Driver Settlement End
    //ASHOK Report start
    private String lpsId = "";
    private String trippedLPS = "";
    private String lpsCount = "";
    private String routeId = "";
    private String customerId = "";
    private String pinkSlipID = "";
    private String orderNo = "";
    private String bags = "";
    private String vehicleid = "";
    private String totalallowance = "";
    private String totalliters = "";
    private String totalamount = "";
    private String totalexpenses = "";
    private String balanceamount = "";
    private String totalkms = "";
    private String totalHms = "";
    private String revenue = "";
    private String vendorSettlementFlag = "";
    private String city = "";
    private String pandL = "";
    private String expenses = "";
    private String estimatedExpenses = "";
    private String customertypeId = "";
    private String customertypeName = "";
    private String ownership = "";
    private String billStatus = "";
    private String destination = "";
    private String consigneeName = "";
    private String productName = "";
    private String gpno = "";
    private String distance = "";
    private String totalFreightAmount = "";
    private String consignmentType = "";
    private String settlementAmount = "";
    private String crossing = "";
    private String consignmentName = "";
    private String settlementDate = "";

    private String trips = "";
    private String sales = "";
    private String wfu = "";
    private String wfl = "";

    private String operationPointId = "";
    private String vehicles = "";
    ///created On 11-12-2014////
    private String loadingTurnHours = "";
    private String tripTransitHours = "";
    private String unloadingTurnHours = "";
    private String driverIssueHours = "";
    private String rnmHours = "";
    private String repairHours = "";
    private String partialHours = "";
    private String transitDelay = "";
    private String totalJobcardHours = "";
    private String availableDays = "";

    private String originNameFullTruck = "";
    private String destinationNameFullTruck = "";
    private String additionalCost = "";
    private String point1Name = "";
    private String point2Name = "";
    private String point3Name = "";
    private String point4Name = "";

    public String getOriginNameFullTruck() {
        return originNameFullTruck;
    }

    public void setOriginNameFullTruck(String originNameFullTruck) {
        this.originNameFullTruck = originNameFullTruck;
    }

    public String getDestinationNameFullTruck() {
        return destinationNameFullTruck;
    }

    public void setDestinationNameFullTruck(String destinationNameFullTruck) {
        this.destinationNameFullTruck = destinationNameFullTruck;
    }

    public String getAdditionalCost() {
        return additionalCost;
    }

    public void setAdditionalCost(String additionalCost) {
        this.additionalCost = additionalCost;
    }

    public String getPoint1Name() {
        return point1Name;
    }

    public void setPoint1Name(String point1Name) {
        this.point1Name = point1Name;
    }

    public String getPoint2Name() {
        return point2Name;
    }

    public void setPoint2Name(String point2Name) {
        this.point2Name = point2Name;
    }

    public String getPoint3Name() {
        return point3Name;
    }

    public void setPoint3Name(String point3Name) {
        this.point3Name = point3Name;
    }

    public String getPoint4Name() {
        return point4Name;
    }

    public void setPoint4Name(String point4Name) {
        this.point4Name = point4Name;
    }

    //ASHOK Report end
    public String getAboveBelowKM() {
        return aboveBelowKM;
    }

    public void setAboveBelowKM(String aboveBelowKM) {
        this.aboveBelowKM = aboveBelowKM;
    }

    public String getActualKM() {
        return actualKM;
    }

    public void setActualKM(String actualKM) {
        this.actualKM = actualKM;
    }

    public String getActualSalary() {
        return actualSalary;
    }

    public void setActualSalary(String actualSalary) {
        this.actualSalary = actualSalary;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getBusExpenses() {
        return busExpenses;
    }

    public void setBusExpenses(String busExpenses) {
        this.busExpenses = busExpenses;
    }

    public String getcBalance() {
        return cBalance;
    }

    public void setcBalance(String cBalance) {
        this.cBalance = cBalance;
    }

    public String getcDate() {
        return cDate;
    }

    public void setcDate(String cDate) {
        this.cDate = cDate;
    }

    public String getcFRs() {
        return cFRs;
    }

    public void setcFRs(String cFRs) {
        this.cFRs = cFRs;
    }

    public String getcRs() {
        return cRs;
    }

    public void setcRs(String cRs) {
        this.cRs = cRs;
    }

    public String getcVNo() {
        return cVNo;
    }

    public void setcVNo(String cVNo) {
        this.cVNo = cVNo;
    }

    public String getCleanerBataSalary() {
        return cleanerBataSalary;
    }

    public void setCleanerBataSalary(String cleanerBataSalary) {
        this.cleanerBataSalary = cleanerBataSalary;
    }

    public String getCleanerName() {
        return cleanerName;
    }

    public void setCleanerName(String cleanerName) {
        this.cleanerName = cleanerName;
    }

    public String getCleanerSalary() {
        return cleanerSalary;
    }

    public void setCleanerSalary(String cleanerSalary) {
        this.cleanerSalary = cleanerSalary;
    }

    public String getdBalance() {
        return dBalance;
    }

    public void setdBalance(String dBalance) {
        this.dBalance = dBalance;
    }

    public String getdDate() {
        return dDate;
    }

    public void setdDate(String dDate) {
        this.dDate = dDate;
    }

    public String getdDueFromDriver() {
        return dDueFromDriver;
    }

    public void setdDueFromDriver(String dDueFromDriver) {
        this.dDueFromDriver = dDueFromDriver;
    }

    public String getdRs() {
        return dRs;
    }

    public void setdRs(String dRs) {
        this.dRs = dRs;
    }

    public String getdVNo() {
        return dVNo;
    }

    public void setdVNo(String dVNo) {
        this.dVNo = dVNo;
    }

    public String getDieselAmount() {
        return dieselAmount;
    }

    public void setDieselAmount(String dieselAmount) {
        this.dieselAmount = dieselAmount;
    }

    public String getDriName() {
        return driName;
    }

    public void setDriName(String driName) {
        this.driName = driName;
    }

    public String getDriverAdvance() {
        return driverAdvance;
    }

    public void setDriverAdvance(String driverAdvance) {
        this.driverAdvance = driverAdvance;
    }

    public String getDriverBataSalary() {
        return driverBataSalary;
    }

    public void setDriverBataSalary(String driverBataSalary) {
        this.driverBataSalary = driverBataSalary;
    }

    public String getDriverCleanerExpenses() {
        return driverCleanerExpenses;
    }

    public void setDriverCleanerExpenses(String driverCleanerExpenses) {
        this.driverCleanerExpenses = driverCleanerExpenses;
    }

    public String getDriverExpenses() {
        return driverExpenses;
    }

    public void setDriverExpenses(String driverExpenses) {
        this.driverExpenses = driverExpenses;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverSalary() {
        return driverSalary;
    }

    public void setDriverSalary(String driverSalary) {
        this.driverSalary = driverSalary;
    }

    public String getDuefromDriver() {
        return duefromDriver;
    }

    public void setDuefromDriver(String duefromDriver) {
        this.duefromDriver = duefromDriver;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getEndKm() {
        return endKm;
    }

    public void setEndKm(String endKm) {
        this.endKm = endKm;
    }

    public String getGeneralExpenses() {
        return generalExpenses;
    }

    public void setGeneralExpenses(String generalExpenses) {
        this.generalExpenses = generalExpenses;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getKmpl() {
        return kmpl;
    }

    public void setKmpl(String kmpl) {
        this.kmpl = kmpl;
    }

    public String getLossOrDay() {
        return lossOrDay;
    }

    public void setLossOrDay(String lossOrDay) {
        this.lossOrDay = lossOrDay;
    }

    public String getNoOfTrips() {
        return noOfTrips;
    }

    public void setNoOfTrips(String noOfTrips) {
        this.noOfTrips = noOfTrips;
    }

    public String getNoOfTrips5k() {
        return noOfTrips5k;
    }

    public void setNoOfTrips5k(String noOfTrips5k) {
        this.noOfTrips5k = noOfTrips5k;
    }

    public String getPerDaysIncome() {
        return perDaysIncome;
    }

    public void setPerDaysIncome(String perDaysIncome) {
        this.perDaysIncome = perDaysIncome;
    }

    public String getRegisterNo() {
        return registerNo;
    }

    public void setRegisterNo(String registerNo) {
        this.registerNo = registerNo;
    }

    public String getRunningKm() {
        return runningKm;
    }

    public void setRunningKm(String runningKm) {
        this.runningKm = runningKm;
    }

    public String getsKmpl() {
        return sKmpl;
    }

    public void setsKmpl(String sKmpl) {
        this.sKmpl = sKmpl;
    }

    public String getSettlementId() {
        return settlementId;
    }

    public void setSettlementId(String settlementId) {
        this.settlementId = settlementId;
    }

    public String getShortage() {
        return shortage;
    }

    public void setShortage(String shortage) {
        this.shortage = shortage;
    }

    public String getShortage1() {
        return shortage1;
    }

    public void setShortage1(String shortage1) {
        this.shortage1 = shortage1;
    }

    public String getSpeedOMeterKm() {
        return speedOMeterKm;
    }

    public void setSpeedOMeterKm(String speedOMeterKm) {
        this.speedOMeterKm = speedOMeterKm;
    }

    public String getStartKM() {
        return startKM;
    }

    public void setStartKM(String startKM) {
        this.startKM = startKM;
    }

    public String getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(String totalDays) {
        this.totalDays = totalDays;
    }

    public String getTotalDiesel() {
        return totalDiesel;
    }

    public void setTotalDiesel(String totalDiesel) {
        this.totalDiesel = totalDiesel;
    }

    public String getTotalExpenses() {
        return totalExpenses;
    }

    public void setTotalExpenses(String totalExpenses) {
        this.totalExpenses = totalExpenses;
    }

    public String getTotalSalary() {
        return totalSalary;
    }

    public void setTotalSalary(String totalSalary) {
        this.totalSalary = totalSalary;
    }

    public String getTotalTonnageAmount() {
        return totalTonnageAmount;
    }

    public void setTotalTonnageAmount(String totalTonnageAmount) {
        this.totalTonnageAmount = totalTonnageAmount;
    }

    public int getNoOfCards() {
        return noOfCards;
    }

    public void setNoOfCards(int noOfCards) {
        this.noOfCards = noOfCards;
    }

    public int getDataType() {
        return dataType;
    }

    public void setDataType(int dataType) {
        this.dataType = dataType;
    }

    public int getXmlId() {
        return xmlId;
    }

    public void setXmlId(int xmlId) {
        this.xmlId = xmlId;
    }

    public String getRequiredDate() {
        return requiredDate;
    }

    public void setRequiredDate(String requiredDate) {
        this.requiredDate = requiredDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }
//  bala ends

    public int getNoOfVehicles() {
        return noOfVehicles;
    }

    public void setNoOfVehicles(int noOfVehicles) {
        this.noOfVehicles = noOfVehicles;
    }

    public int getCounterId() {
        return counterId;
    }

    public void setCounterId(int counterId) {
        this.counterId = counterId;
    }

    public int getRcWorkorderId() {
        return rcWorkorderId;
    }

    public void setRcWorkorderId(int rcWorkorderId) {
        this.rcWorkorderId = rcWorkorderId;
    }

    public String getLaborCharge() {
        return laborCharge;
    }

    public void setLaborCharge(String laborCharge) {
        this.laborCharge = laborCharge;
    }

    public String getMaterialCostExternal() {
        return materialCostExternal;
    }

    public void setMaterialCostExternal(String materialCostExternal) {
        this.materialCostExternal = materialCostExternal;
    }

    public String getMaterialCostInternal() {
        return materialCostInternal;
    }

    public void setMaterialCostInternal(String materialCostInternal) {
        this.materialCostInternal = materialCostInternal;
    }

    public void setSpltQty(String spltQty) {
        this.spltQty = spltQty;
    }

    public String getSpltQty() {
        return spltQty;
    }

    public int getMovingAverage() {
        return movingAverage;
    }

    public String getFirstHalfDays() {
        return firstHalfDays;
    }

    public void setFirstHalfDays(String firstHalfDays) {
        this.firstHalfDays = firstHalfDays;
    }

    public String getLastHalfDays() {
        return lastHalfDays;
    }

    public void setLastHalfDays(String lastHalfDays) {
        this.lastHalfDays = lastHalfDays;
    }

    public String getMonthAverage() {
        return monthAverage;
    }

    public void setMonthAverage(String monthAverage) {
        this.monthAverage = monthAverage;
    }

    public void setMovingAverage(int movingAverage) {
        this.movingAverage = movingAverage;
    }

    public String getManualMrsDate() {
        return manualMrsDate;
    }

    public void setManualMrsDate(String manualMrsDate) {
        this.manualMrsDate = manualMrsDate;
    }

    public String getManualMrsNo() {
        return manualMrsNo;
    }

    public void setManualMrsNo(String manualMrsNo) {
        this.manualMrsNo = manualMrsNo;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public String getRcReceive() {
        return rcReceive;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public int getMfrid() {
        return mfrid;
    }

    public void setMfrid(int mfrid) {
        this.mfrid = mfrid;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public int getUsageid() {
        return usageid;
    }

    public void setUsageid(int usageid) {
        this.usageid = usageid;
    }

    public void setRcReceive(String rcReceive) {
        this.rcReceive = rcReceive;
    }

    public String getRcSend() {
        return rcSend;
    }

    public void setRcSend(String rcSend) {
        this.rcSend = rcSend;
    }

    public String getReportAmount() {
        return reportAmount;
    }

    public void setReportAmount(String reportAmount) {
        this.reportAmount = reportAmount;
    }

    public int getReportType() {
        return reportType;
    }

    public void setReportType(int reportType) {
        this.reportType = reportType;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getMfr() {
        return mfr;
    }

    public void setMfr(String mfr) {
        this.mfr = mfr;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getUsageTypeIds() {
        return usageTypeIds;
    }

    public void setUsageTypeIds(String usageTypeIds) {
        this.usageTypeIds = usageTypeIds;
    }

    public String getUsageTypeName() {
        return usageTypeName;
    }

    public void setUsageTypeName(String usageTypeName) {
        this.usageTypeName = usageTypeName;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    // end shankar
    public int getUsageTypeId() {
        return usageTypeId;
    }

    public void setUsageTypeId(int usageTypeId) {
        this.usageTypeId = usageTypeId;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getPosName() {
        return posName;
    }

    public void setPosName(String posName) {
        this.posName = posName;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public String getTyreNo() {
        return tyreNo;
    }

    public void setTyreNo(String tyreNo) {
        this.tyreNo = tyreNo;
    }

    public String getRcTime() {
        return rcTime;
    }

    public void setRcTime(String rcTime) {
        this.rcTime = rcTime;
    }

    public String getRcId() {
        return rcId;
    }

    public void setRcId(String rcId) {
        this.rcId = rcId;
    }

    public String getTechnicianId() {
        return technicianId;
    }

    public void setTechnicianId(String technicianId) {
        this.technicianId = technicianId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        this.km = km;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getBuyDate() {
        return buyDate;
    }

    public void setBuyDate(String buyDate) {
        this.buyDate = buyDate;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public String getDiff() {
        return diff;
    }

    public void setDiff(String diff) {
        this.diff = diff;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getTotalIssued() {
        return totalIssued;
    }

    public void setTotalIssued(String totalIssued) {
        this.totalIssued = totalIssued;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDue() {
        return due;
    }

    public void setDue(String due) {
        this.due = due;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getIssueQty() {
        return issueQty;
    }

    public void setIssueQty(String issueQty) {
        this.issueQty = issueQty;
    }

    public String getMrsId() {
        return mrsId;
    }

    public void setMrsId(String mrsId) {
        this.mrsId = mrsId;
    }

    public String getNextFc() {
        return nextFc;
    }

    public void setNextFc(String nextFc) {
        this.nextFc = nextFc;
    }

    public String getRqty() {
        return rqty;
    }

    public void setRqty(String rqty) {
        this.rqty = rqty;
    }

    public String getSupplyId() {
        return supplyId;
    }

    public void setSupplyId(String supplyId) {
        this.supplyId = supplyId;
    }

    public String getWoId() {
        return woId;
    }

    public void setWoId(String woId) {
        this.woId = woId;
    }

    public String getActivityAmount() {
        return activityAmount;
    }

    public void setActivityAmount(String activityAmount) {
        this.activityAmount = activityAmount;
    }

    public String getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(String billAmount) {
        this.billAmount = billAmount;
    }

    public String getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(String itemAmount) {
        this.itemAmount = itemAmount;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemQty() {
        return itemQty;
    }

    public void setItemQty(String itemQty) {
        this.itemQty = itemQty;
    }

    public String getLabour() {
        return labour;
    }

    public void setLabour(String labour) {
        this.labour = labour;
    }

    public String getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(String netAmount) {
        this.netAmount = netAmount;
    }

    public String getSpares() {
        return spares;
    }

    public void setSpares(String spares) {
        this.spares = spares;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getJobCardId() {
        return jobCardId;
    }

    public void setJobCardId(String jobCardId) {
        this.jobCardId = jobCardId;
    }

    public String getLaborAmount() {
        return laborAmount;
    }

    public void setLaborAmount(String laborAmount) {
        this.laborAmount = laborAmount;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getSpareAmount() {
        return spareAmount;
    }

    public void setSpareAmount(String spareAmount) {
        this.spareAmount = spareAmount;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getWoAmount() {
        return woAmount;
    }

    public void setWoAmount(String woAmount) {
        this.woAmount = woAmount;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(String serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public String getAcd() {
        return acd;
    }

    public void setAcd(String acd) {
        this.acd = acd;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getAqty() {
        return aqty;
    }

    public void setAqty(String aqty) {
        this.aqty = aqty;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getMfrCode() {
        return mfrCode;
    }

    public void setMfrCode(String mfrCode) {
        this.mfrCode = mfrCode;
    }

    public String getMfrId() {
        return mfrId;
    }

    public void setMfrId(String mfrId) {
        this.mfrId = mfrId;
    }

    public String getMfrName() {
        return mfrName;
    }

    public void setMfrName(String mfrName) {
        this.mfrName = mfrName;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(String newPrice) {
        this.newPrice = newPrice;
    }

    public String getNewQty() {
        return newQty;
    }

    public void setNewQty(String newQty) {
        this.newQty = newQty;
    }

    public String getPaplCode() {
        return paplCode;
    }

    public void setPaplCode(String paplCode) {
        this.paplCode = paplCode;
    }

    public String getPcd() {
        return pcd;
    }

    public void setPcd(String pcd) {
        this.pcd = pcd;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getPoId() {
        return poId;
    }

    public void setPoId(String poId) {
        this.poId = poId;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getProblemStatus() {
        return problemStatus;
    }

    public void setProblemStatus(String problemStatus) {
        this.problemStatus = problemStatus;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getPurDate() {
        return purDate;
    }

    public void setPurDate(String purDate) {
        this.purDate = purDate;
    }

    public String getPurType() {
        return purType;
    }

    public void setPurType(String purType) {
        this.purType = purType;
    }

    public String getPurchaseAmt() {
        return purchaseAmt;
    }

    public void setPurchaseAmt(String purchaseAmt) {
        this.purchaseAmt = purchaseAmt;
    }

    public String getRcQty() {
        return rcQty;
    }

    public void setRcQty(String rcQty) {
        this.rcQty = rcQty;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getStockValue() {
        return stockValue;
    }

    public void setStockValue(String stockValue) {
        this.stockValue = stockValue;
    }

    public String getTechnician() {
        return technician;
    }

    public void setTechnician(String technician) {
        this.technician = technician;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(String totalQty) {
        this.totalQty = totalQty;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getUnusedQty() {
        return unusedQty;
    }

    public void setUnusedQty(String unusedQty) {
        this.unusedQty = unusedQty;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getCompletedDate() {
        return completedDate;
    }

    public void setCompletedDate(String completedDate) {
        this.completedDate = completedDate;
    }

    public String getCompletedProp() {
        return completedProp;
    }

    public void setCompletedProp(String completedProp) {
        this.completedProp = completedProp;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getRaisedProp() {
        return raisedProp;
    }

    public void setRaisedProp(String raisedProp) {
        this.raisedProp = raisedProp;
    }

    public String getUsageType() {
        return usageType;
    }

    public void setUsageType(String usageType) {
        this.usageType = usageType;
    }

    public String getHikedAmount() {
        return hikedAmount;
    }

    public void setHikedAmount(String hikedAmount) {
        this.hikedAmount = hikedAmount;
    }

    public String getMargin() {
        return margin;
    }

    public void setMargin(String margin) {
        this.margin = margin;
    }

    public String getPayableTax() {
        return payableTax;
    }

    public void setPayableTax(String payableTax) {
        this.payableTax = payableTax;
    }

    public float getHikePercentage() {
        return hikePercentage;
    }

    public void setHikePercentage(float hikePercentage) {
        this.hikePercentage = hikePercentage;
    }

    public String getSparesAmount() {
        return sparesAmount;
    }

    public void setSparesAmount(String sparesAmount) {
        this.sparesAmount = sparesAmount;
    }

    public String getSparesWithTax() {
        return sparesWithTax;
    }

    public void setSparesWithTax(String sparesWithTax) {
        this.sparesWithTax = sparesWithTax;
    }

    public String getFreight() {
        return freight;
    }

    public void setFreight(String freight) {
        this.freight = freight;
    }

    public String getApprover() {
        return approver;
    }

    public void setApprover(String approver) {
        this.approver = approver;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getRcCode() {
        return rcCode;
    }

    public void setRcCode(String rcCode) {
        this.rcCode = rcCode;
    }

    public String getRcWoId() {
        return rcWoId;
    }

    public void setRcWoId(String rcWoId) {
        this.rcWoId = rcWoId;
    }

    public String[] getAddressSplit() {
        return addressSplit;
    }

    public void setAddressSplit(String[] addressSplit) {
        this.addressSplit = addressSplit;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String[] getRemarksSplit() {
        return remarksSplit;
    }

    public void setRemarksSplit(String[] remarksSplit) {
        this.remarksSplit = remarksSplit;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public String getDcNo() {
        return dcNo;
    }

    public void setDcNo(String dcNo) {
        this.dcNo = dcNo;
    }

    public static FPUtil getFpUtil() {
        return fpUtil;
    }

    public static void setFpUtil(FPUtil fpUtil) {
        ReportTO.fpUtil = fpUtil;
    }

    public String getLastKm() {
        return lastKm;
    }

    public void setLastKm(String lastKm) {
        this.lastKm = lastKm;
    }

    public String getLastProblem() {
        return lastProblem;
    }

    public void setLastProblem(String lastProblem) {
        this.lastProblem = lastProblem;
    }

    public String getLastRemarks() {
        return lastRemarks;
    }

    public void setLastRemarks(String lastRemarks) {
        this.lastRemarks = lastRemarks;
    }

    public String getLastStatus() {
        return lastStatus;
    }

    public void setLastStatus(String lastStatus) {
        this.lastStatus = lastStatus;
    }

    public String getLastTech() {
        return lastTech;
    }

    public void setLastTech(String lastTech) {
        this.lastTech = lastTech;
    }

    public String getProblemName() {
        return problemName;
    }

    public void setProblemName(String problemName) {
        this.problemName = problemName;
    }

    public String getScheduledDate() {
        return scheduledDate;
    }

    public void setScheduledDate(String scheduledDate) {
        this.scheduledDate = scheduledDate;
    }

    public String getLastDate() {
        return lastDate;
    }

    public void setLastDate(String lastDate) {
        this.lastDate = lastDate;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getTotJc() {
        return totJc;
    }

    public void setTotJc(String totJc) {
        this.totJc = totJc;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getVehCount() {
        return vehCount;
    }

    public void setVehCount(String vehCount) {
        this.vehCount = vehCount;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getServicePointId() {
        return servicePointId;
    }

    public void setServicePointId(String servicePointId) {
        this.servicePointId = servicePointId;
    }

    public String getBillingCompleted() {
        return billingCompleted;
    }

    public void setBillingCompleted(String billingCompleted) {
        this.billingCompleted = billingCompleted;
    }

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    public String getNotPlanned() {
        return notPlanned;
    }

    public void setNotPlanned(String notPlanned) {
        this.notPlanned = notPlanned;
    }

    public String getPlanned() {
        return planned;
    }

    public void setPlanned(String planned) {
        this.planned = planned;
    }

    public String getItemTypes() {
        return itemTypes;
    }

    public void setItemTypes(String itemTypes) {
        this.itemTypes = itemTypes;
    }

    public String getStworth() {
        return stworth;
    }

    public void setStworth(String stworth) {
        this.stworth = stworth;
    }

    public ArrayList getVatValues() {
        return vatValues;
    }

    public void setVatValues(ArrayList vatValues) {
        this.vatValues = vatValues;
    }

    public float getTaxAmount() {

        return taxAmount;
    }

    public void setTaxAmount(float taxAmount) {

        this.taxAmount = taxAmount;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getBayNo() {
        return bayNo;
    }

    public void setBayNo(String bayNo) {
        this.bayNo = bayNo;
    }

    public String getContractAmnt() {
        return contractAmnt;
    }

    public void setContractAmnt(String contractAmnt) {
        this.contractAmnt = contractAmnt;
    }

    public String getServiceTaxAmnt() {
        return serviceTaxAmnt;
    }

    public void setServiceTaxAmnt(String serviceTaxAmnt) {
        this.serviceTaxAmnt = serviceTaxAmnt;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(String buyPrice) {
        this.buyPrice = buyPrice;
    }

    public String getNettProfit() {
        return nettProfit;
    }

    public void setNettProfit(String nettProfit) {
        this.nettProfit = nettProfit;
    }

    public String getProfit() {
        return profit;
    }

    public void setProfit(String profit) {
        this.profit = profit;
    }

    public String getRetQty() {
        return retQty;
    }

    public void setRetQty(String retQty) {
        this.retQty = retQty;
    }

    public String getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(String sellPrice) {
        this.sellPrice = sellPrice;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    //Rathimeena Driver Settlement Start
    public String getAdvDatetime() {
        return advDatetime;
    }

    public void setAdvDatetime(String advDatetime) {
        this.advDatetime = advDatetime;
    }

    public String getAlightDate() {
        return alightDate;
    }

    public void setAlightDate(String alightDate) {
        this.alightDate = alightDate;
    }

    public String getAlightStatus() {
        return alightStatus;
    }

    public void setAlightStatus(String alightStatus) {
        this.alightStatus = alightStatus;
    }

    public String getBunkName() {
        return bunkName;
    }

    public void setBunkName(String bunkName) {
        this.bunkName = bunkName;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getDeliveredTonnage() {
        return deliveredTonnage;
    }

    public void setDeliveredTonnage(String deliveredTonnage) {
        this.deliveredTonnage = deliveredTonnage;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getEmbarkDate() {
        return embarkDate;
    }

    public void setEmbarkDate(String embarkDate) {
        this.embarkDate = embarkDate;
    }

    public String getExpensesDesc() {
        return expensesDesc;
    }

    public void setExpensesDesc(String expensesDesc) {
        this.expensesDesc = expensesDesc;
    }

    public String getFuelDatetime() {
        return fuelDatetime;
    }

    public void setFuelDatetime(String fuelDatetime) {
        this.fuelDatetime = fuelDatetime;
    }

    public String getFuelName() {
        return fuelName;
    }

    public void setFuelName(String fuelName) {
        this.fuelName = fuelName;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    public String getInDateTime() {
        return inDateTime;
    }

    public void setInDateTime(String inDateTime) {
        this.inDateTime = inDateTime;
    }

    public String getInKM() {
        return inKM;
    }

    public void setInKM(String inKM) {
        this.inKM = inKM;
    }

    public String getInOutDateTime() {
        return inOutDateTime;
    }

    public void setInOutDateTime(String inOutDateTime) {
        this.inOutDateTime = inOutDateTime;
    }

    public String getInOutIndication() {
        return inOutIndication;
    }

    public void setInOutIndication(String inOutIndication) {
        this.inOutIndication = inOutIndication;
    }

    public String getIssuerName() {
        return issuerName;
    }

    public void setIssuerName(String issuerName) {
        this.issuerName = issuerName;
    }

    public String getLiters() {
        return liters;
    }

    public void setLiters(String liters) {
        this.liters = liters;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getOutDateTime() {
        return outDateTime;
    }

    public void setOutDateTime(String outDateTime) {
        this.outDateTime = outDateTime;
    }

    public String getOutKM() {
        return outKM;
    }

    public void setOutKM(String outKM) {
        this.outKM = outKM;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getSettlementFlag() {
        return settlementFlag;
    }

    public void setSettlementFlag(String settlementFlag) {
        this.settlementFlag = settlementFlag;
    }

    public String getTotalTonnage() {
        return totalTonnage;
    }

    public void setTotalTonnage(String totalTonnage) {
        this.totalTonnage = totalTonnage;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getCompId() {
        return compId;
    }

    public void setCompId(String compId) {
        this.compId = compId;
    }

    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getDriverDue() {
        return driverDue;
    }

    public void setDriverDue(String driverDue) {
        this.driverDue = driverDue;
    }

    public String getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getTripArrivalDate() {
        return tripArrivalDate;
    }

    public void setTripArrivalDate(String tripArrivalDate) {
        this.tripArrivalDate = tripArrivalDate;
    }

    public String getTripBalanceAmount() {
        return tripBalanceAmount;
    }

    public void setTripBalanceAmount(String tripBalanceAmount) {
        this.tripBalanceAmount = tripBalanceAmount;
    }

    public String getTripCode() {
        return tripCode;
    }

    public void setTripCode(String tripCode) {
        this.tripCode = tripCode;
    }

    public String getTripDate() {
        return tripDate;
    }

    public void setTripDate(String tripDate) {
        this.tripDate = tripDate;
    }

    public String getTripDepartureDate() {
        return tripDepartureDate;
    }

    public void setTripDepartureDate(String tripDepartureDate) {
        this.tripDepartureDate = tripDepartureDate;
    }

    public String getTripDriverId() {
        return tripDriverId;
    }

    public void setTripDriverId(String tripDriverId) {
        this.tripDriverId = tripDriverId;
    }

    public String getTripFuelAmount() {
        return tripFuelAmount;
    }

    public void setTripFuelAmount(String tripFuelAmount) {
        this.tripFuelAmount = tripFuelAmount;
    }

    public String getTripKmsIn() {
        return tripKmsIn;
    }

    public void setTripKmsIn(String tripKmsIn) {
        this.tripKmsIn = tripKmsIn;
    }

    public String getTripKmsOut() {
        return tripKmsOut;
    }

    public void setTripKmsOut(String tripKmsOut) {
        this.tripKmsOut = tripKmsOut;
    }

    public String getTripRouteId() {
        return tripRouteId;
    }

    public void setTripRouteId(String tripRouteId) {
        this.tripRouteId = tripRouteId;
    }

    public String getTripScheduleId() {
        return tripScheduleId;
    }

    public void setTripScheduleId(String tripScheduleId) {
        this.tripScheduleId = tripScheduleId;
    }

    public String getTripSheetId() {
        return tripSheetId;
    }

    public void setTripSheetId(String tripSheetId) {
        this.tripSheetId = tripSheetId;
    }

    public String getTripStatus() {
        return tripStatus;
    }

    public void setTripStatus(String tripStatus) {
        this.tripStatus = tripStatus;
    }

    public String getTripTotalAllowances() {
        return tripTotalAllowances;
    }

    public void setTripTotalAllowances(String tripTotalAllowances) {
        this.tripTotalAllowances = tripTotalAllowances;
    }

    public String getTripTotalExpenses() {
        return tripTotalExpenses;
    }

    public void setTripTotalExpenses(String tripTotalExpenses) {
        this.tripTotalExpenses = tripTotalExpenses;
    }

    public String getTripTotalKms() {
        return tripTotalKms;
    }

    public void setTripTotalKms(String tripTotalKms) {
        this.tripTotalKms = tripTotalKms;
    }

    public String getTripTotalLitres() {
        return tripTotalLitres;
    }

    public void setTripTotalLitres(String tripTotalLitres) {
        this.tripTotalLitres = tripTotalLitres;
    }

    public String getTripVehicleId() {
        return tripVehicleId;
    }

    public void setTripVehicleId(String tripVehicleId) {
        this.tripVehicleId = tripVehicleId;
    }

    public String getTripEndDate() {
        return tripEndDate;
    }

    public void setTripEndDate(String tripEndDate) {
        this.tripEndDate = tripEndDate;
    }

    public String getTripStartDate() {
        return tripStartDate;
    }

    public void setTripStartDate(String tripStartDate) {
        this.tripStartDate = tripStartDate;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getKnownLocation() {
        return knownLocation;
    }

    public void setKnownLocation(String knownLocation) {
        this.knownLocation = knownLocation;
    }

    public String getVehicleInOut() {
        return vehicleInOut;
    }

    public void setVehicleInOut(String vehicleInOut) {
        this.vehicleInOut = vehicleInOut;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getDriId() {
        return driId;
    }

    public void setDriId(String driId) {
        this.driId = driId;
    }

    public String getDieselExpense() {
        return dieselExpense;
    }

    public void setDieselExpense(String dieselExpense) {
        this.dieselExpense = dieselExpense;
    }

    public String getTollExpense() {
        return tollExpense;
    }

    public void setTollExpense(String tollExpense) {
        this.tollExpense = tollExpense;
    }

    public String getTotalExpense() {
        return totalExpense;
    }

    public void setTotalExpense(String totalExpense) {
        this.totalExpense = totalExpense;
    }

    public String getTotalTonAmount() {
        return totalTonAmount;
    }

    public void setTotalTonAmount(String totalTonAmount) {
        this.totalTonAmount = totalTonAmount;
    }

    public String getDueIn() {
        return dueIn;
    }

    public void setDueIn(String dueIn) {
        this.dueIn = dueIn;
    }

    public String getPermitType() {
        return permitType;
    }

    public void setPermitType(String permitType) {
        this.permitType = permitType;
    }

    public String getAmcAmount() {
        return amcAmount;
    }

    public void setAmcAmount(String amcAmount) {
        this.amcAmount = amcAmount;
    }

    public String getAmcCompanyName() {
        return amcCompanyName;
    }

    public void setAmcCompanyName(String amcCompanyName) {
        this.amcCompanyName = amcCompanyName;
    }

    public String getAmcDuration() {
        return amcDuration;
    }

    public void setAmcDuration(String amcDuration) {
        this.amcDuration = amcDuration;
    }

    public int getDriverId() {
        return driverId;
    }

    public void setDriverId(int driverId) {
        this.driverId = driverId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLpsId() {
        return lpsId;
    }

    public void setLpsId(String lpsId) {
        this.lpsId = lpsId;
    }

    public String getTrippedLPS() {
        return trippedLPS;
    }

    public void setTrippedLPS(String trippedLPS) {
        this.trippedLPS = trippedLPS;
    }

    public String getLpsCount() {
        return lpsCount;
    }

    public void setLpsCount(String lpsCount) {
        this.lpsCount = lpsCount;
    }

    public String getBags() {
        return bags;
    }

    public void setBags(String bags) {
        this.bags = bags;
    }

    public String getBalanceamount() {
        return balanceamount;
    }

    public void setBalanceamount(String balanceamount) {
        this.balanceamount = balanceamount;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getPinkSlipID() {
        return pinkSlipID;
    }

    public void setPinkSlipID(String pinkSlipID) {
        this.pinkSlipID = pinkSlipID;
    }

    public String getRevenue() {
        return revenue;
    }

    public void setRevenue(String revenue) {
        this.revenue = revenue;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getTotalallowance() {
        return totalallowance;
    }

    public void setTotalallowance(String totalallowance) {
        this.totalallowance = totalallowance;
    }

    public String getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(String totalamount) {
        this.totalamount = totalamount;
    }

    public String getTotalexpenses() {
        return totalexpenses;
    }

    public void setTotalexpenses(String totalexpenses) {
        this.totalexpenses = totalexpenses;
    }

    public String getTotalkms() {
        return totalkms;
    }

    public void setTotalkms(String totalkms) {
        this.totalkms = totalkms;
    }

    public String getTotalliters() {
        return totalliters;
    }

    public void setTotalliters(String totalliters) {
        this.totalliters = totalliters;
    }

    public String getVehicleid() {
        return vehicleid;
    }

    public void setVehicleid(String vehicleid) {
        this.vehicleid = vehicleid;
    }

    public String getVendorSettlementFlag() {
        return vendorSettlementFlag;
    }

    public void setVendorSettlementFlag(String vendorSettlementFlag) {
        this.vendorSettlementFlag = vendorSettlementFlag;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPandL() {
        return pandL;
    }

    public void setPandL(String pandL) {
        this.pandL = pandL;
    }

    public String getExpenses() {
        return expenses;
    }

    public void setExpenses(String expenses) {
        this.expenses = expenses;
    }

    public String getCustomertypeId() {
        return customertypeId;
    }

    public void setCustomertypeId(String customertypeId) {
        this.customertypeId = customertypeId;
    }

    public String getCustomertypeName() {
        return customertypeName;
    }

    public void setCustomertypeName(String customertypeName) {
        this.customertypeName = customertypeName;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getOwnership() {
        return ownership;
    }

    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    public String getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getGpno() {
        return gpno;
    }

    public void setGpno(String gpno) {
        this.gpno = gpno;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getTotalFreightAmount() {
        return totalFreightAmount;
    }

    public void setTotalFreightAmount(String totalFreightAmount) {
        this.totalFreightAmount = totalFreightAmount;
    }

    public String getConsignmentType() {
        return consignmentType;
    }

    public void setConsignmentType(String consignmentType) {
        this.consignmentType = consignmentType;
    }

    public String getCrossing() {
        return crossing;
    }

    public void setCrossing(String crossing) {
        this.crossing = crossing;
    }

    public String getSettlementAmount() {
        return settlementAmount;
    }

    public void setSettlementAmount(String settlementAmount) {
        this.settlementAmount = settlementAmount;
    }

    public String getConsignmentName() {
        return consignmentName;
    }

    public void setConsignmentName(String consignmentName) {
        this.consignmentName = consignmentName;
    }

    public String getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(String settlementDate) {
        this.settlementDate = settlementDate;
    }

    public String getCNAmount() {
        return CNAmount;
    }

    public void setCNAmount(String CNAmount) {
        this.CNAmount = CNAmount;
    }

    public String getDNAmount() {
        return DNAmount;
    }

    public void setDNAmount(String DNAmount) {
        this.DNAmount = DNAmount;
    }

    public String getBillingType() {
        return billingType;
    }

    public void setBillingType(String billingType) {
        this.billingType = billingType;
    }

    public String getFreightAmount() {
        return freightAmount;
    }

    public void setFreightAmount(String freightAmount) {
        this.freightAmount = freightAmount;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getInvRefCode() {
        return invRefCode;
    }

    public void setInvRefCode(String invRefCode) {
        this.invRefCode = invRefCode;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public String getNumberOfTrip() {
        return numberOfTrip;
    }

    public void setNumberOfTrip(String numberOfTrip) {
        this.numberOfTrip = numberOfTrip;
    }

    public String getOtherExpenseAmount() {
        return otherExpenseAmount;
    }

    public void setOtherExpenseAmount(String otherExpenseAmount) {
        this.otherExpenseAmount = otherExpenseAmount;
    }

    public String getOwnerShip() {
        return ownerShip;
    }

    public void setOwnerShip(String ownerShip) {
        this.ownerShip = ownerShip;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getDriverBata() {
        return driverBata;
    }

    public void setDriverBata(double driverBata) {
        this.driverBata = driverBata;
    }

    public double getDriverExpense() {
        return driverExpense;
    }

    public void setDriverExpense(double driverExpense) {
        this.driverExpense = driverExpense;
    }

    public double getDriverIncentive() {
        return driverIncentive;
    }

    public void setDriverIncentive(double driverIncentive) {
        this.driverIncentive = driverIncentive;
    }

    public double getEmiAmount() {
        return emiAmount;
    }

    public void setEmiAmount(double emiAmount) {
        this.emiAmount = emiAmount;
    }

    public double getFcAmount() {
        return fcAmount;
    }

    public void setFcAmount(double fcAmount) {
        this.fcAmount = fcAmount;
    }

    public double getFixedExpensePerDay() {
        return fixedExpensePerDay;
    }

    public void setFixedExpensePerDay(double fixedExpensePerDay) {
        this.fixedExpensePerDay = fixedExpensePerDay;
    }

    public double getFuelAmount() {
        return fuelAmount;
    }

    public void setFuelAmount(double fuelAmount) {
        this.fuelAmount = fuelAmount;
    }

    public double getInsuranceAmount() {
        return insuranceAmount;
    }

    public void setInsuranceAmount(double insuranceAmount) {
        this.insuranceAmount = insuranceAmount;
    }

    public double getMaintainExpense() {
        return maintainExpense;
    }

    public void setMaintainExpense(double maintainExpense) {
        this.maintainExpense = maintainExpense;
    }

    public double getNetExpense() {
        return netExpense;
    }

    public void setNetExpense(double netExpense) {
        this.netExpense = netExpense;
    }

    public double getNetProfit() {
        return netProfit;
    }

    public void setNetProfit(double netProfit) {
        this.netProfit = netProfit;
    }

    public double getPermitAmount() {
        return permitAmount;
    }

    public void setPermitAmount(double permitAmount) {
        this.permitAmount = permitAmount;
    }

    public double getRoadTaxAmount() {
        return roadTaxAmount;
    }

    public void setRoadTaxAmount(double roadTaxAmount) {
        this.roadTaxAmount = roadTaxAmount;
    }

    public double getRouteExpenses() {
        return routeExpenses;
    }

    public void setRouteExpenses(double routeExpenses) {
        this.routeExpenses = routeExpenses;
    }

    public int getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(int timePeriod) {
        this.timePeriod = timePeriod;
    }

    public double getTollAmount() {
        return tollAmount;
    }

    public void setTollAmount(double tollAmount) {
        this.tollAmount = tollAmount;
    }

    public double getTotlalFixedExpense() {
        return totlalFixedExpense;
    }

    public void setTotlalFixedExpense(double totlalFixedExpense) {
        this.totlalFixedExpense = totlalFixedExpense;
    }

    public double getTotlalOperationExpense() {
        return totlalOperationExpense;
    }

    public void setTotlalOperationExpense(double totlalOperationExpense) {
        this.totlalOperationExpense = totlalOperationExpense;
    }

    public String getTripNos() {
        return tripNos;
    }

    public void setTripNos(String tripNos) {
        this.tripNos = tripNos;
    }

    public double getMiscAmount() {
        return miscAmount;
    }

    public void setMiscAmount(double miscAmount) {
        this.miscAmount = miscAmount;
    }

    public double getTripOtherExpense() {
        return tripOtherExpense;
    }

    public void setTripOtherExpense(double tripOtherExpense) {
        this.tripOtherExpense = tripOtherExpense;
    }

    public double getVehicleDriverSalary() {
        return vehicleDriverSalary;
    }

    public void setVehicleDriverSalary(double vehicleDriverSalary) {
        this.vehicleDriverSalary = vehicleDriverSalary;
    }

    public String getReeferRequired() {
        return reeferRequired;
    }

    public void setReeferRequired(String reeferRequired) {
        this.reeferRequired = reeferRequired;
    }

    public String getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(String totalWeight) {
        this.totalWeight = totalWeight;
    }

    public String rep() {
        return fleetCenterId;
    }

    public String getFleetCenterId() {
        return fleetCenterId;
    }

    public void setFleetCenterId(String fleetCenterId) {
        this.fleetCenterId = fleetCenterId;
    }

    public String getTripStatusId() {
        return tripStatusId;
    }

    public void setTripStatusId(String tripStatusId) {
        this.tripStatusId = tripStatusId;
    }

    public String getVehicleCapUtil() {
        return vehicleCapUtil;
    }

    public void setVehicleCapUtil(String vehicleCapUtil) {
        this.vehicleCapUtil = vehicleCapUtil;
    }

    public String getUtilisedDays() {
        return utilisedDays;
    }

    public void setUtilisedDays(String utilisedDays) {
        this.utilisedDays = utilisedDays;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getApprovalstatus() {
        return approvalstatus;
    }

    public void setApprovalstatus(String approvalstatus) {
        this.approvalstatus = approvalstatus;
    }

    public String getConsignmentNoteNo() {
        return consignmentNoteNo;
    }

    public void setConsignmentNoteNo(String consignmentNoteNo) {
        this.consignmentNoteNo = consignmentNoteNo;
    }

    public String getEstimatedRevenue() {
        return estimatedRevenue;
    }

    public void setEstimatedRevenue(String estimatedRevenue) {
        this.estimatedRevenue = estimatedRevenue;
    }

    public double getProfitPercent() {
        return profitPercent;
    }

    public void setProfitPercent(double profitPercent) {
        this.profitPercent = profitPercent;
    }

    public double getProfitValue() {
        return profitValue;
    }

    public void setProfitValue(double profitValue) {
        this.profitValue = profitValue;
    }

    public String getOperationTypeId() {
        return operationTypeId;
    }

    public void setOperationTypeId(String operationTypeId) {
        this.operationTypeId = operationTypeId;
    }

    public String getProfitType() {
        return profitType;
    }

    public void setProfitType(String profitType) {
        this.profitType = profitType;
    }

    public String getConsignmentNote() {
        return consignmentNote;
    }

    public void setConsignmentNote(String consignmentNote) {
        this.consignmentNote = consignmentNote;
    }

    public double getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(double creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getDaysToOverDue() {
        return daysToOverDue;
    }

    public void setDaysToOverDue(String daysToOverDue) {
        this.daysToOverDue = daysToOverDue;
    }

    public String getDaysWithOverDue() {
        return daysWithOverDue;
    }

    public void setDaysWithOverDue(String daysWithOverDue) {
        this.daysWithOverDue = daysWithOverDue;
    }

    public double getOverDueGrandTotal() {
        return overDueGrandTotal;
    }

    public void setOverDueGrandTotal(double overDueGrandTotal) {
        this.overDueGrandTotal = overDueGrandTotal;
    }

    public String getOverDueOn() {
        return overDueOn;
    }

    public void setOverDueOn(String overDueOn) {
        this.overDueOn = overDueOn;
    }

    public double getTotalAmountReceivable() {
        return totalAmountReceivable;
    }

    public void setTotalAmountReceivable(double totalAmountReceivable) {
        this.totalAmountReceivable = totalAmountReceivable;
    }

    public String getRunHm() {
        return runHm;
    }

    public void setRunHm(String runHm) {
        this.runHm = runHm;
    }

    public String getRunKm() {
        return runKm;
    }

    public void setRunKm(String runKm) {
        this.runKm = runKm;
    }

    public String getCurrentTemperature() {
        return currentTemperature;
    }

    public void setCurrentTemperature(String currentTemperature) {
        this.currentTemperature = currentTemperature;
    }

    public String getLogDate() {
        return logDate;
    }

    public void setLogDate(String logDate) {
        this.logDate = logDate;
    }

    public String getBhatta() {
        return bhatta;
    }

    public void setBhatta(String bhatta) {
        this.bhatta = bhatta;
    }

    public String getBpclAllocation() {
        return bpclAllocation;
    }

    public void setBpclAllocation(String bpclAllocation) {
        this.bpclAllocation = bpclAllocation;
    }

    public String getDieselUsed() {
        return dieselUsed;
    }

    public void setDieselUsed(String dieselUsed) {
        this.dieselUsed = dieselUsed;
    }

    public String getEndingBalance() {
        return endingBalance;
    }

    public void setEndingBalance(String endingBalance) {
        this.endingBalance = endingBalance;
    }

    public String getExtraExpense() {
        return extraExpense;
    }

    public void setExtraExpense(String extraExpense) {
        this.extraExpense = extraExpense;
    }

    public String getFuelPrice() {
        return fuelPrice;
    }

    public void setFuelPrice(String fuelPrice) {
        this.fuelPrice = fuelPrice;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getRcmAllocation() {
        return rcmAllocation;
    }

    public void setRcmAllocation(String rcmAllocation) {
        this.rcmAllocation = rcmAllocation;
    }

    public String getRunHour() {
        return runHour;
    }

    public void setRunHour(String runHour) {
        this.runHour = runHour;
    }

    public String getStartingBalance() {
        return startingBalance;
    }

    public void setStartingBalance(String startingBalance) {
        this.startingBalance = startingBalance;
    }

    public String getTotalMiscellaneous() {
        return totalMiscellaneous;
    }

    public void setTotalMiscellaneous(String totalMiscellaneous) {
        this.totalMiscellaneous = totalMiscellaneous;
    }

    public String getLogTime() {
        return logTime;
    }

    public void setLogTime(String logTime) {
        this.logTime = logTime;
    }

    public String getBillableKM() {
        return billableKM;
    }

    public void setBillableKM(String billableKM) {
        this.billableKM = billableKM;
    }

    public String getAccountingDate() {
        return accountingDate;
    }

    public void setAccountingDate(String accountingDate) {
        this.accountingDate = accountingDate;
    }

    public String getAmountBalance() {
        return amountBalance;
    }

    public void setAmountBalance(String amountBalance) {
        this.amountBalance = amountBalance;
    }

    public String getbPCLAccountId() {
        return bPCLAccountId;
    }

    public void setbPCLAccountId(String bPCLAccountId) {
        this.bPCLAccountId = bPCLAccountId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDealerCity() {
        return dealerCity;
    }

    public void setDealerCity(String dealerCity) {
        this.dealerCity = dealerCity;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getOdometerReading() {
        return odometerReading;
    }

    public void setOdometerReading(String odometerReading) {
        this.odometerReading = odometerReading;
    }

    public String getPetromilesEarned() {
        return petromilesEarned;
    }

    public void setPetromilesEarned(String petromilesEarned) {
        this.petromilesEarned = petromilesEarned;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionHistoryId() {
        return transactionHistoryId;
    }

    public void setTransactionHistoryId(String transactionHistoryId) {
        this.transactionHistoryId = transactionHistoryId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getVolumeDocumentNo() {
        return volumeDocumentNo;
    }

    public void setVolumeDocumentNo(String volumeDocumentNo) {
        this.volumeDocumentNo = volumeDocumentNo;
    }

    public String getDistanceTravelled() {
        return distanceTravelled;
    }

    public void setDistanceTravelled(String distanceTravelled) {
        this.distanceTravelled = distanceTravelled;
    }

    public String getGpsEndAttempt() {
        return gpsEndAttempt;
    }

    public void setGpsEndAttempt(String gpsEndAttempt) {
        this.gpsEndAttempt = gpsEndAttempt;
    }

    public String getGpsEndErrorCode() {
        return gpsEndErrorCode;
    }

    public void setGpsEndErrorCode(String gpsEndErrorCode) {
        this.gpsEndErrorCode = gpsEndErrorCode;
    }

    public String getGpsEndErrorMsg() {
        return gpsEndErrorMsg;
    }

    public void setGpsEndErrorMsg(String gpsEndErrorMsg) {
        this.gpsEndErrorMsg = gpsEndErrorMsg;
    }

    public String getGpsEndInd() {
        return gpsEndInd;
    }

    public void setGpsEndInd(String gpsEndInd) {
        this.gpsEndInd = gpsEndInd;
    }

    public String getGpsEndUpdateTime() {
        return gpsEndUpdateTime;
    }

    public void setGpsEndUpdateTime(String gpsEndUpdateTime) {
        this.gpsEndUpdateTime = gpsEndUpdateTime;
    }

    public String getGpsStartAttempt() {
        return gpsStartAttempt;
    }

    public void setGpsStartAttempt(String gpsStartAttempt) {
        this.gpsStartAttempt = gpsStartAttempt;
    }

    public String getGpsStartErrorCode() {
        return gpsStartErrorCode;
    }

    public void setGpsStartErrorCode(String gpsStartErrorCode) {
        this.gpsStartErrorCode = gpsStartErrorCode;
    }

    public String getGpsStartErrorMsg() {
        return gpsStartErrorMsg;
    }

    public void setGpsStartErrorMsg(String gpsStartErrorMsg) {
        this.gpsStartErrorMsg = gpsStartErrorMsg;
    }

    public String getGpsStartInd() {
        return gpsStartInd;
    }

    public void setGpsStartInd(String gpsStartInd) {
        this.gpsStartInd = gpsStartInd;
    }

    public String getGpsStartUpdateTime() {
        return gpsStartUpdateTime;
    }

    public void setGpsStartUpdateTime(String gpsStartUpdateTime) {
        this.gpsStartUpdateTime = gpsStartUpdateTime;
    }

    public String getGpsSystem() {
        return gpsSystem;
    }

    public void setGpsSystem(String gpsSystem) {
        this.gpsSystem = gpsSystem;
    }

    public String getGpsTripDetailsAttempt() {
        return gpsTripDetailsAttempt;
    }

    public void setGpsTripDetailsAttempt(String gpsTripDetailsAttempt) {
        this.gpsTripDetailsAttempt = gpsTripDetailsAttempt;
    }

    public String getGpsTripDetailsDatetime() {
        return gpsTripDetailsDatetime;
    }

    public void setGpsTripDetailsDatetime(String gpsTripDetailsDatetime) {
        this.gpsTripDetailsDatetime = gpsTripDetailsDatetime;
    }

    public String getGpsTripDetailsErrorCode() {
        return gpsTripDetailsErrorCode;
    }

    public void setGpsTripDetailsErrorCode(String gpsTripDetailsErrorCode) {
        this.gpsTripDetailsErrorCode = gpsTripDetailsErrorCode;
    }

    public String getGpsTripDetailsErrorMsg() {
        return gpsTripDetailsErrorMsg;
    }

    public void setGpsTripDetailsErrorMsg(String gpsTripDetailsErrorMsg) {
        this.gpsTripDetailsErrorMsg = gpsTripDetailsErrorMsg;
    }

    public String getGpsTripDetailsInd() {
        return gpsTripDetailsInd;
    }

    public void setGpsTripDetailsInd(String gpsTripDetailsInd) {
        this.gpsTripDetailsInd = gpsTripDetailsInd;
    }

    public String getReeferAvgTemp() {
        return reeferAvgTemp;
    }

    public void setReeferAvgTemp(String reeferAvgTemp) {
        this.reeferAvgTemp = reeferAvgTemp;
    }

    public String getReeferRunHours() {
        return reeferRunHours;
    }

    public void setReeferRunHours(String reeferRunHours) {
        this.reeferRunHours = reeferRunHours;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getTripEndDateTime() {
        return tripEndDateTime;
    }

    public void setTripEndDateTime(String tripEndDateTime) {
        this.tripEndDateTime = tripEndDateTime;
    }

    public String getTripStartDateTime() {
        return tripStartDateTime;
    }

    public void setTripStartDateTime(String tripStartDateTime) {
        this.tripStartDateTime = tripStartDateTime;
    }

    public String getTsEndInd() {
        return tsEndInd;
    }

    public void setTsEndInd(String tsEndInd) {
        this.tsEndInd = tsEndInd;
    }

    public String getTsStartInd() {
        return tsStartInd;
    }

    public void setTsStartInd(String tsStartInd) {
        this.tsStartInd = tsStartInd;
    }

    public String getSales() {
        return sales;
    }

    public void setSales(String sales) {
        this.sales = sales;
    }

    public String getTrips() {
        return trips;
    }

    public void setTrips(String trips) {
        this.trips = trips;
    }

    public String getWfl() {
        return wfl;
    }

    public void setWfl(String wfl) {
        this.wfl = wfl;
    }

    public String getWfu() {
        return wfu;
    }

    public void setWfu(String wfu) {
        this.wfu = wfu;
    }

    public String getOperationPointId() {
        return operationPointId;
    }

    public void setOperationPointId(String operationPointId) {
        this.operationPointId = operationPointId;
    }

    public String getVehicles() {
        return vehicles;
    }

    public void setVehicles(String vehicles) {
        this.vehicles = vehicles;
    }

    public String getEstimatedExpenses() {
        return estimatedExpenses;
    }

    public void setEstimatedExpenses(String estimatedExpenses) {
        this.estimatedExpenses = estimatedExpenses;
    }

    public String getRmDays() {
        return rmDays;
    }

    public void setRmDays(String rmDays) {
        this.rmDays = rmDays;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getMonths() {
        return months;
    }

    public void setMonths(String months) {
        this.months = months;
    }

    //Rathimeena Driver Settlement End
    public String getRequestedAdvance() {
        return requestedAdvance;
    }

    public void setRequestedAdvance(String requestedAdvance) {
        this.requestedAdvance = requestedAdvance;
    }

    public String getPaidAdvance() {
        return paidAdvance;
    }

    public void setPaidAdvance(String paidAdvance) {
        this.paidAdvance = paidAdvance;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCityFromId() {
        return cityFromId;
    }

    public void setCityFromId(String cityFromId) {
        this.cityFromId = cityFromId;
    }

    public String getPodStatus() {
        return podStatus;
    }

    public void setPodStatus(String podStatus) {
        this.podStatus = podStatus;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getOldVehicleNo() {
        return oldVehicleNo;
    }

    public void setOldVehicleNo(String oldVehicleNo) {
        this.oldVehicleNo = oldVehicleNo;
    }

    public String getOldDriverName() {
        return oldDriverName;
    }

    public void setOldDriverName(String oldDriverName) {
        this.oldDriverName = oldDriverName;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getCustomerOrderReferenceNo() {
        return customerOrderReferenceNo;
    }

    public void setCustomerOrderReferenceNo(String customerOrderReferenceNo) {
        this.customerOrderReferenceNo = customerOrderReferenceNo;
    }

    public String getcNotes() {
        return cNotes;
    }

    public void setcNotes(String cNotes) {
        this.cNotes = cNotes;
    }

    public String getRouteInfo() {
        return routeInfo;
    }

    public void setRouteInfo(String routeInfo) {
        this.routeInfo = routeInfo;
    }

    public String getFleetCenterName() {
        return fleetCenterName;
    }

    public void setFleetCenterName(String fleetCenterName) {
        this.fleetCenterName = fleetCenterName;
    }

    public String getExpectedArrivalDateTime() {
        return expectedArrivalDateTime;
    }

    public void setExpectedArrivalDateTime(String expectedArrivalDateTime) {
        this.expectedArrivalDateTime = expectedArrivalDateTime;
    }

    public String getOrderRevenue() {
        return orderRevenue;
    }

    public void setOrderRevenue(String orderRevenue) {
        this.orderRevenue = orderRevenue;
    }

    public String getOrderExpense() {
        return orderExpense;
    }

    public void setOrderExpense(String orderExpense) {
        this.orderExpense = orderExpense;
    }

    public String getActualAdvancePaid() {
        return actualAdvancePaid;
    }

    public void setActualAdvancePaid(String actualAdvancePaid) {
        this.actualAdvancePaid = actualAdvancePaid;
    }

    public String getVehicleTonnage() {
        return vehicleTonnage;
    }

    public void setVehicleTonnage(String vehicleTonnage) {
        this.vehicleTonnage = vehicleTonnage;
    }

    public String getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(String gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    public String getPlannedStartDateTime() {
        return plannedStartDateTime;
    }

    public void setPlannedStartDateTime(String plannedStartDateTime) {
        this.plannedStartDateTime = plannedStartDateTime;
    }

    public String getActualStartDateTime() {
        return actualStartDateTime;
    }

    public void setActualStartDateTime(String actualStartDateTime) {
        this.actualStartDateTime = actualStartDateTime;
    }

    public String getPlannedEndDateTime() {
        return plannedEndDateTime;
    }

    public void setPlannedEndDateTime(String plannedEndDateTime) {
        this.plannedEndDateTime = plannedEndDateTime;
    }

    public String getActualEndDateTime() {
        return actualEndDateTime;
    }

    public void setActualEndDateTime(String actualEndDateTime) {
        this.actualEndDateTime = actualEndDateTime;
    }

    public String getWfuDateTime() {
        return wfuDateTime;
    }

    public void setWfuDateTime(String wfuDateTime) {
        this.wfuDateTime = wfuDateTime;
    }

    public String getWfuRemarks() {
        return wfuRemarks;
    }

    public void setWfuRemarks(String wfuRemarks) {
        this.wfuRemarks = wfuRemarks;
    }

    public String getWfuCreatedOn() {
        return wfuCreatedOn;
    }

    public void setWfuCreatedOn(String wfuCreatedOn) {
        this.wfuCreatedOn = wfuCreatedOn;
    }

    public String getEndedBy() {
        return endedBy;
    }

    public void setEndedBy(String endedBy) {
        this.endedBy = endedBy;
    }

    public String getClosedBy() {
        return closedBy;
    }

    public void setClosedBy(String closedBy) {
        this.closedBy = closedBy;
    }

    public String getSettledBy() {
        return settledBy;
    }

    public void setSettledBy(String settledBy) {
        this.settledBy = settledBy;
    }

    public String getFleetCenterNo() {
        return fleetCenterNo;
    }

    public void setFleetCenterNo(String fleetCenterNo) {
        this.fleetCenterNo = fleetCenterNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getExtraExpenseValue() {
        return extraExpenseValue;
    }

    public void setExtraExpenseValue(String extraExpenseValue) {
        this.extraExpenseValue = extraExpenseValue;
    }

    public String getTonnage() {
        return tonnage;
    }

    public void setTonnage(String tonnage) {
        this.tonnage = tonnage;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getOperationPoint() {
        return operationPoint;
    }

    public void setOperationPoint(String operationPoint) {
        this.operationPoint = operationPoint;
    }

    public String getFleetCenterHead() {
        return fleetCenterHead;
    }

    public void setFleetCenterHead(String fleetCenterHead) {
        this.fleetCenterHead = fleetCenterHead;
    }

    public String getVehiclewfudate() {
        return vehiclewfudate;
    }

    public void setVehiclewfudate(String vehiclewfudate) {
        this.vehiclewfudate = vehiclewfudate;
    }

    public String getVehiclewfutime() {
        return vehiclewfutime;
    }

    public void setVehiclewfutime(String vehiclewfutime) {
        this.vehiclewfutime = vehiclewfutime;
    }

    public String getTripstarttime() {
        return tripstarttime;
    }

    public void setTripstarttime(String tripstarttime) {
        this.tripstarttime = tripstarttime;
    }

    public String getServiceTypeName() {
        return serviceTypeName;
    }

    public void setServiceTypeName(String serviceTypeName) {
        this.serviceTypeName = serviceTypeName;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getClosedDateFrom() {
        return closedDateFrom;
    }

    public void setClosedDateFrom(String closedDateFrom) {
        this.closedDateFrom = closedDateFrom;
    }

    public String getClosedDateTo() {
        return closedDateTo;
    }

    public void setClosedDateTo(String closedDateTo) {
        this.closedDateTo = closedDateTo;
    }

    public String getEndDateFrom() {
        return endDateFrom;
    }

    public void setEndDateFrom(String endDateFrom) {
        this.endDateFrom = endDateFrom;
    }

    public String getEndDateTo() {
        return endDateTo;
    }

    public void setEndDateTo(String endDateTo) {
        this.endDateTo = endDateTo;
    }

    public String getSettledDateFrom() {
        return settledDateFrom;
    }

    public void setSettledDateFrom(String settledDateFrom) {
        this.settledDateFrom = settledDateFrom;
    }

    public String getSettledDateTo() {
        return settledDateTo;
    }

    public void setSettledDateTo(String settledDateTo) {
        this.settledDateTo = settledDateTo;
    }

    public String getStartDateFrom() {
        return startDateFrom;
    }

    public void setStartDateFrom(String startDateFrom) {
        this.startDateFrom = startDateFrom;
    }

    public String getStartDateTo() {
        return startDateTo;
    }

    public void setStartDateTo(String startDateTo) {
        this.startDateTo = startDateTo;
    }

    public String getTripStatusIdTo() {
        return tripStatusIdTo;
    }

    public void setTripStatusIdTo(String tripStatusIdTo) {
        this.tripStatusIdTo = tripStatusIdTo;
    }

    public String getNot() {
        return not;
    }

    public void setNot(String not) {
        this.not = not;
    }

    public String getLoadingDateTime() {
        return loadingDateTime;
    }

    public void setLoadingDateTime(String loadingDateTime) {
        this.loadingDateTime = loadingDateTime;
    }

    public String getUnLoadingDate() {
        return unLoadingDate;
    }

    public void setUnLoadingDate(String unLoadingDate) {
        this.unLoadingDate = unLoadingDate;
    }

    public String getTripDays() {
        return tripDays;
    }

    public void setTripDays(String tripDays) {
        this.tripDays = tripDays;
    }

    public String getTripRunHm() {
        return tripRunHm;
    }

    public void setTripRunHm(String tripRunHm) {
        this.tripRunHm = tripRunHm;
    }

    public String getTripRunKm() {
        return tripRunKm;
    }

    public void setTripRunKm(String tripRunKm) {
        this.tripRunKm = tripRunKm;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public int getAmountSpend() {
        return amountSpend;
    }

    public void setAmountSpend(int amountSpend) {
        this.amountSpend = amountSpend;
    }

    public int getTripCount() {
        return tripCount;
    }

    public void setTripCount(int tripCount) {
        this.tripCount = tripCount;
    }

    public String getJobCardDays() {
        return jobCardDays;
    }

    public void setJobCardDays(String jobCardDays) {
        this.jobCardDays = jobCardDays;
    }

    public String getJobCardNo() {
        return jobCardNo;
    }

    public void setJobCardNo(String jobCardNo) {
        this.jobCardNo = jobCardNo;
    }

    public String getOutStandingDays() {
        return outStandingDays;
    }

    public void setOutStandingDays(String outStandingDays) {
        this.outStandingDays = outStandingDays;
    }

    public String getPlannedCompleteDays() {
        return plannedCompleteDays;
    }

    public void setPlannedCompleteDays(String plannedCompleteDays) {
        this.plannedCompleteDays = plannedCompleteDays;
    }

    public String getIssueType() {
        return issueType;
    }

    public void setIssueType(String issueType) {
        this.issueType = issueType;
    }

    public String getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(String closeDate) {
        this.closeDate = closeDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getMergingDate() {
        return mergingDate;
    }

    public void setMergingDate(String mergingDate) {
        this.mergingDate = mergingDate;
    }

    public String getSettledDate() {
        return settledDate;
    }

    public void setSettledDate(String settledDate) {
        this.settledDate = settledDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getHmReading() {
        return hmReading;
    }

    public void setHmReading(String hmReading) {
        this.hmReading = hmReading;
    }

    public String getHmReadingDate() {
        return hmReadingDate;
    }

    public void setHmReadingDate(String hmReadingDate) {
        this.hmReadingDate = hmReadingDate;
    }

    public String getKmReading() {
        return kmReading;
    }

    public void setKmReading(String kmReading) {
        this.kmReading = kmReading;
    }

    public String getKmReadingDate() {
        return kmReadingDate;
    }

    public void setKmReadingDate(String kmReadingDate) {
        this.kmReadingDate = kmReadingDate;
    }

    public String getVehicletype() {
        return vehicletype;
    }

    public void setVehicletype(String vehicletype) {
        this.vehicletype = vehicletype;
    }

    public String getTripMergingId() {
        return tripMergingId;
    }

    public void setTripMergingId(String tripMergingId) {
        this.tripMergingId = tripMergingId;
    }

    public String getTripExtraExpense() {
        return tripExtraExpense;
    }

    public void setTripExtraExpense(String tripExtraExpense) {
        this.tripExtraExpense = tripExtraExpense;
    }

    public String getEmptyExpense() {
        return emptyExpense;
    }

    public void setEmptyExpense(String emptyExpense) {
        this.emptyExpense = emptyExpense;
    }

    public String getEarnings() {
        return earnings;
    }

    public void setEarnings(String earnings) {
        this.earnings = earnings;
    }

    public String getLoadedExpense() {
        return loadedExpense;
    }

    public void setLoadedExpense(String loadedExpense) {
        this.loadedExpense = loadedExpense;
    }

    public String getOrderSequence() {
        return orderSequence;
    }

    public void setOrderSequence(String orderSequence) {
        this.orderSequence = orderSequence;
    }

    public String getExpenseDate() {
        return expenseDate;
    }

    public void setExpenseDate(String expenseDate) {
        this.expenseDate = expenseDate;
    }

    public String getExpenseDriverName() {
        return expenseDriverName;
    }

    public void setExpenseDriverName(String expenseDriverName) {
        this.expenseDriverName = expenseDriverName;
    }

    public String getExpenseName() {
        return expenseName;
    }

    public void setExpenseName(String expenseName) {
        this.expenseName = expenseName;
    }

    public String getExpenseRemarks() {
        return expenseRemarks;
    }

    public void setExpenseRemarks(String expenseRemarks) {
        this.expenseRemarks = expenseRemarks;
    }

    public String getAdvancePaidDate() {
        return advancePaidDate;
    }

    public void setAdvancePaidDate(String advancePaidDate) {
        this.advancePaidDate = advancePaidDate;
    }

    public String getAdvanceRemarks() {
        return advanceRemarks;
    }

    public void setAdvanceRemarks(String advanceRemarks) {
        this.advanceRemarks = advanceRemarks;
    }

    public String getExpenseType() {
        return expenseType;
    }

    public void setExpenseType(String expenseType) {
        this.expenseType = expenseType;
    }

    public String getPrimaryDriver() {
        return primaryDriver;
    }

    public void setPrimaryDriver(String primaryDriver) {
        this.primaryDriver = primaryDriver;
    }

    public String getSecondaryDriver() {
        return secondaryDriver;
    }

    public void setSecondaryDriver(String secondaryDriver) {
        this.secondaryDriver = secondaryDriver;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getActualExpense() {
        return actualExpense;
    }

    public void setActualExpense(String actualExpense) {
        this.actualExpense = actualExpense;
    }

    public String getDestinationReportingDateTime() {
        return destinationReportingDateTime;
    }

    public void setDestinationReportingDateTime(String destinationReportingDateTime) {
        this.destinationReportingDateTime = destinationReportingDateTime;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getLoadingTransitHours() {
        return loadingTransitHours;
    }

    public void setLoadingTransitHours(String loadingTransitHours) {
        this.loadingTransitHours = loadingTransitHours;
    }

    public String getOriginReportingDateTime() {
        return originReportingDateTime;
    }

    public void setOriginReportingDateTime(String originReportingDateTime) {
        this.originReportingDateTime = originReportingDateTime;
    }

    public String getRcm() {
        return rcm;
    }

    public void setRcm(String rcm) {
        this.rcm = rcm;
    }

    public String getTotalRunHm() {
        return totalRunHm;
    }

    public void setTotalRunHm(String totalRunHm) {
        this.totalRunHm = totalRunHm;
    }

    public String getTotalRunKm() {
        return totalRunKm;
    }

    public void setTotalRunKm(String totalRunKm) {
        this.totalRunKm = totalRunKm;
    }

    public String getTransitHours() {
        return transitHours;
    }

    public void setTransitHours(String transitHours) {
        this.transitHours = transitHours;
    }

    public String getWflHours() {
        return wflHours;
    }

    public void setWflHours(String wflHours) {
        this.wflHours = wflHours;
    }

    public String getWfuHours() {
        return wfuHours;
    }

    public void setWfuHours(String wfuHours) {
        this.wfuHours = wfuHours;
    }

    public String getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(String paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getRequestAmount() {
        return requestAmount;
    }

    public void setRequestAmount(String requestAmount) {
        this.requestAmount = requestAmount;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getTripEndTime() {
        return tripEndTime;
    }

    public void setTripEndTime(String tripEndTime) {
        this.tripEndTime = tripEndTime;
    }

    public String getTripendtime() {
        return tripendtime;
    }

    public void setTripendtime(String tripendtime) {
        this.tripendtime = tripendtime;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public String getTotalHms() {
        return totalHms;
    }

    public void setTotalHms(String totalHms) {
        this.totalHms = totalHms;
    }

    public String getWfuAmount() {
        return wfuAmount;
    }

    public void setWfuAmount(String wfuAmount) {
        this.wfuAmount = wfuAmount;
    }

    public String getEmptyTripKM() {
        return emptyTripKM;
    }

    public void setEmptyTripKM(String emptyTripKM) {
        this.emptyTripKM = emptyTripKM;
    }

    public String getAccountMgrId() {
        return accountMgrId;
    }

    public void setAccountMgrId(String accountMgrId) {
        this.accountMgrId = accountMgrId;
    }

    public String getFoodingAdvance() {
        return foodingAdvance;
    }

    public void setFoodingAdvance(String foodingAdvance) {
        this.foodingAdvance = foodingAdvance;
    }

    public String getRepairAdvance() {
        return repairAdvance;
    }

    public void setRepairAdvance(String repairAdvance) {
        this.repairAdvance = repairAdvance;
    }

    public String getRnmAdvance() {
        return rnmAdvance;
    }

    public void setRnmAdvance(String rnmAdvance) {
        this.rnmAdvance = rnmAdvance;
    }

    public String getChasisAmount() {
        return chasisAmount;
    }

    public void setChasisAmount(String chasisAmount) {
        this.chasisAmount = chasisAmount;
    }

    public String getContainerAmount() {
        return containerAmount;
    }

    public void setContainerAmount(String containerAmount) {
        this.containerAmount = containerAmount;
    }

    public String getReferAmount() {
        return referAmount;
    }

    public void setReferAmount(String referAmount) {
        this.referAmount = referAmount;
    }

    public String getChasisAmount2() {
        return chasisAmount2;
    }

    public void setChasisAmount2(String chasisAmount2) {
        this.chasisAmount2 = chasisAmount2;
    }

    public String getContainerAmount2() {
        return containerAmount2;
    }

    public void setContainerAmount2(String containerAmount1) {
        this.containerAmount2 = containerAmount1;
    }

    public String getReferAmount2() {
        return referAmount2;
    }

    public void setReferAmount2(String referAmount2) {
        this.referAmount2 = referAmount2;
    }

    public String getChasisAmount1() {
        return chasisAmount1;
    }

    public void setChasisAmount1(String chasisAmount1) {
        this.chasisAmount1 = chasisAmount1;
    }

    public String getContainerAmount1() {
        return containerAmount1;
    }

    public void setContainerAmount1(String containerAmount1) {
        this.containerAmount1 = containerAmount1;
    }

    public String getReferAmount1() {
        return referAmount1;
    }

    public void setReferAmount1(String referAmount1) {
        this.referAmount1 = referAmount1;
    }

    public String getChasisAmount3() {
        return chasisAmount3;
    }

    public void setChasisAmount3(String chasisAmount3) {
        this.chasisAmount3 = chasisAmount3;
    }

    public String getContainerAmount3() {
        return containerAmount3;
    }

    public void setContainerAmount3(String containerAmount3) {
        this.containerAmount3 = containerAmount3;
    }

    public String getReferAmount3() {
        return referAmount3;
    }

    public void setReferAmount3(String referAmount3) {
        this.referAmount3 = referAmount3;
    }

    public String getNewTyre() {
        return newTyre;
    }

    public void setNewTyre(String newTyre) {
        this.newTyre = newTyre;
    }

    public String getMonth1() {
        return month1;
    }

    public void setMonth1(String month1) {
        this.month1 = month1;
    }

    public String getMonth1New() {
        return month1New;
    }

    public void setMonth1New(String month1New) {
        this.month1New = month1New;
    }

    public String getMonth1ReTread() {
        return month1ReTread;
    }

    public void setMonth1ReTread(String month1ReTread) {
        this.month1ReTread = month1ReTread;
    }

    public String getMonth2() {
        return month2;
    }

    public void setMonth2(String month2) {
        this.month2 = month2;
    }

    public String getMonth2New() {
        return month2New;
    }

    public void setMonth2New(String month2New) {
        this.month2New = month2New;
    }

    public String getMonth2ReTread() {
        return month2ReTread;
    }

    public void setMonth2ReTread(String month2ReTread) {
        this.month2ReTread = month2ReTread;
    }

    public String getMonth3() {
        return month3;
    }

    public void setMonth3(String month3) {
        this.month3 = month3;
    }

    public String getMonth3New() {
        return month3New;
    }

    public void setMonth3New(String month3New) {
        this.month3New = month3New;
    }

    public String getMonth3ReTread() {
        return month3ReTread;
    }

    public void setMonth3ReTread(String month3ReTread) {
        this.month3ReTread = month3ReTread;
    }

    public String getYear1() {
        return year1;
    }

    public void setYear1(String year1) {
        this.year1 = year1;
    }

    public String getYear2() {
        return year2;
    }

    public void setYear2(String year2) {
        this.year2 = year2;
    }

    public String getYear3() {
        return year3;
    }

    public void setYear3(String year3) {
        this.year3 = year3;
    }

    public String getReTreadTyre() {
        return reTreadTyre;
    }

    public void setReTreadTyre(String reTreadTyre) {
        this.reTreadTyre = reTreadTyre;
    }

    public String getTotalTrip() {
        return totalTrip;
    }

    public void setTotalTrip(String totalTrip) {
        this.totalTrip = totalTrip;
    }

    public String getDriverIssueHours() {
        return driverIssueHours;
    }

    public void setDriverIssueHours(String driverIssueHours) {
        this.driverIssueHours = driverIssueHours;
    }

    public String getLoadingTurnHours() {
        return loadingTurnHours;
    }

    public void setLoadingTurnHours(String loadingTurnHours) {
        this.loadingTurnHours = loadingTurnHours;
    }

    public String getPartialHours() {
        return partialHours;
    }

    public void setPartialHours(String partialHours) {
        this.partialHours = partialHours;
    }

    public String getRepairHours() {
        return repairHours;
    }

    public void setRepairHours(String repairHours) {
        this.repairHours = repairHours;
    }

    public String getRnmHours() {
        return rnmHours;
    }

    public void setRnmHours(String rnmHours) {
        this.rnmHours = rnmHours;
    }

    public String getUnloadingTurnHours() {
        return unloadingTurnHours;
    }

    public void setUnloadingTurnHours(String unloadingTurnHours) {
        this.unloadingTurnHours = unloadingTurnHours;
    }

    public String getTransitDelay() {
        return transitDelay;
    }

    public void setTransitDelay(String transitDelay) {
        this.transitDelay = transitDelay;
    }

    public String getTripTransitHours() {
        return tripTransitHours;
    }

    public void setTripTransitHours(String tripTransitHours) {
        this.tripTransitHours = tripTransitHours;
    }

    public String getTotalJobcardHours() {
        return totalJobcardHours;
    }

    public void setTotalJobcardHours(String totalJobcardHours) {
        this.totalJobcardHours = totalJobcardHours;
    }

    public String getAvailableDays() {
        return availableDays;
    }

    public void setAvailableDays(String availableDays) {
        this.availableDays = availableDays;
    }

    public String getDriverIssue() {
        return driverIssue;
    }

    public void setDriverIssue(String driverIssue) {
        this.driverIssue = driverIssue;
    }

    public String getEndCount() {
        return endCount;
    }

    public void setEndCount(String endCount) {
        this.endCount = endCount;
    }

    public String getEndRevenue() {
        return endRevenue;
    }

    public void setEndRevenue(String endRevenue) {
        this.endRevenue = endRevenue;
    }

    public String getInProgress() {
        return inProgress;
    }

    public void setInProgress(String inProgress) {
        this.inProgress = inProgress;
    }

    public String getPrevioisEndCount() {
        return previoisEndCount;
    }

    public void setPrevioisEndCount(String previoisEndCount) {
        this.previoisEndCount = previoisEndCount;
    }

    public String getPrevioisEndRevenue() {
        return previoisEndRevenue;
    }

    public void setPrevioisEndRevenue(String previoisEndRevenue) {
        this.previoisEndRevenue = previoisEndRevenue;
    }

    public String getPrevioisStartCount() {
        return previoisStartCount;
    }

    public void setPrevioisStartCount(String previoisStartCount) {
        this.previoisStartCount = previoisStartCount;
    }

    public String getPrevioisStartRevenue() {
        return previoisStartRevenue;
    }

    public void setPrevioisStartRevenue(String previoisStartRevenue) {
        this.previoisStartRevenue = previoisStartRevenue;
    }

    public String getRepairMaintenance() {
        return repairMaintenance;
    }

    public void setRepairMaintenance(String repairMaintenance) {
        this.repairMaintenance = repairMaintenance;
    }

    public String getStartCount() {
        return startCount;
    }

    public void setStartCount(String startCount) {
        this.startCount = startCount;
    }

    public String getStartRevenue() {
        return startRevenue;
    }

    public void setStartRevenue(String startRevenue) {
        this.startRevenue = startRevenue;
    }

    public String getTotalVehicle() {
        return totalVehicle;
    }

    public void setTotalVehicle(String totalVehicle) {
        this.totalVehicle = totalVehicle;
    }

    public String getNewTyreAmount() {
        return newTyreAmount;
    }

    public void setNewTyreAmount(String newTyreAmount) {
        this.newTyreAmount = newTyreAmount;
    }

    public String getNewTyreAmount1() {
        return newTyreAmount1;
    }

    public void setNewTyreAmount1(String newTyreAmount1) {
        this.newTyreAmount1 = newTyreAmount1;
    }

    public String getNewTyreAmount2() {
        return newTyreAmount2;
    }

    public void setNewTyreAmount2(String newTyreAmount2) {
        this.newTyreAmount2 = newTyreAmount2;
    }

    public String getNewTyreAmount3() {
        return newTyreAmount3;
    }

    public void setNewTyreAmount3(String newTyreAmount3) {
        this.newTyreAmount3 = newTyreAmount3;
    }

    public String getRetreadTyreAmount3() {
        return retreadTyreAmount3;
    }

    public void setRetreadTyreAmount3(String retreadTyreAmount3) {
        this.retreadTyreAmount3 = retreadTyreAmount3;
    }

    public String getRetreadTyreAmount() {
        return retreadTyreAmount;
    }

    public void setRetreadTyreAmount(String retreadTyreAmount) {
        this.retreadTyreAmount = retreadTyreAmount;
    }

    public String getRetreadTyreAmount1() {
        return retreadTyreAmount1;
    }

    public void setRetreadTyreAmount1(String retreadTyreAmount1) {
        this.retreadTyreAmount1 = retreadTyreAmount1;
    }

    public String getRetreadTyreAmount2() {
        return retreadTyreAmount2;
    }

    public void setRetreadTyreAmount2(String retreadTyreAmount2) {
        this.retreadTyreAmount2 = retreadTyreAmount2;
    }

    public String getAccidental() {
        return accidental;
    }

    public void setAccidental(String accidental) {
        this.accidental = accidental;
    }

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    public String getBreakDownReeferWorks() {
        return breakDownReeferWorks;
    }

    public void setBreakDownReeferWorks(String breakDownReeferWorks) {
        this.breakDownReeferWorks = breakDownReeferWorks;
    }

    public String getBreakDownVehicleWorks() {
        return breakDownVehicleWorks;
    }

    public void setBreakDownVehicleWorks(String breakDownVehicleWorks) {
        this.breakDownVehicleWorks = breakDownVehicleWorks;
    }

    public String getRegularReeferService() {
        return regularReeferService;
    }

    public void setRegularReeferService(String regularReeferService) {
        this.regularReeferService = regularReeferService;
    }

    public String getRegularVehicleService() {
        return regularVehicleService;
    }

    public void setRegularVehicleService(String regularVehicleService) {
        this.regularVehicleService = regularVehicleService;
    }

    public String getMonth1Accidental() {
        return month1Accidental;
    }

    public void setMonth1Accidental(String month1Accidental) {
        this.month1Accidental = month1Accidental;
    }

    public String getMonth1Asset() {
        return month1Asset;
    }

    public void setMonth1Asset(String month1Asset) {
        this.month1Asset = month1Asset;
    }

    public String getMonth1BreakDownReeferWorks() {
        return month1BreakDownReeferWorks;
    }

    public void setMonth1BreakDownReeferWorks(String month1BreakDownReeferWorks) {
        this.month1BreakDownReeferWorks = month1BreakDownReeferWorks;
    }

    public String getMonth1BreakDownVehicleWorks() {
        return month1BreakDownVehicleWorks;
    }

    public void setMonth1BreakDownVehicleWorks(String month1BreakDownVehicleWorks) {
        this.month1BreakDownVehicleWorks = month1BreakDownVehicleWorks;
    }

    public String getMonth1RegularReeferService() {
        return month1RegularReeferService;
    }

    public void setMonth1RegularReeferService(String month1RegularReeferService) {
        this.month1RegularReeferService = month1RegularReeferService;
    }

    public String getMonth1RegularVehicleService() {
        return month1RegularVehicleService;
    }

    public void setMonth1RegularVehicleService(String month1RegularVehicleService) {
        this.month1RegularVehicleService = month1RegularVehicleService;
    }

    public String getMonth2Accidental() {
        return month2Accidental;
    }

    public void setMonth2Accidental(String month2Accidental) {
        this.month2Accidental = month2Accidental;
    }

    public String getMonth2Asset() {
        return month2Asset;
    }

    public void setMonth2Asset(String month2Asset) {
        this.month2Asset = month2Asset;
    }

    public String getMonth2BreakDownReeferWorks() {
        return month2BreakDownReeferWorks;
    }

    public void setMonth2BreakDownReeferWorks(String month2BreakDownReeferWorks) {
        this.month2BreakDownReeferWorks = month2BreakDownReeferWorks;
    }

    public String getMonth2BreakDownVehicleWorks() {
        return month2BreakDownVehicleWorks;
    }

    public void setMonth2BreakDownVehicleWorks(String month2BreakDownVehicleWorks) {
        this.month2BreakDownVehicleWorks = month2BreakDownVehicleWorks;
    }

    public String getMonth2RegularReeferService() {
        return month2RegularReeferService;
    }

    public void setMonth2RegularReeferService(String month2RegularReeferService) {
        this.month2RegularReeferService = month2RegularReeferService;
    }

    public String getMonth2RegularVehicleService() {
        return month2RegularVehicleService;
    }

    public void setMonth2RegularVehicleService(String month2RegularVehicleService) {
        this.month2RegularVehicleService = month2RegularVehicleService;
    }

    public String getMonth3Accidental() {
        return month3Accidental;
    }

    public void setMonth3Accidental(String month3Accidental) {
        this.month3Accidental = month3Accidental;
    }

    public String getMonth3Asset() {
        return month3Asset;
    }

    public void setMonth3Asset(String month3Asset) {
        this.month3Asset = month3Asset;
    }

    public String getMonth3BreakDownReeferWorks() {
        return month3BreakDownReeferWorks;
    }

    public void setMonth3BreakDownReeferWorks(String month3BreakDownReeferWorks) {
        this.month3BreakDownReeferWorks = month3BreakDownReeferWorks;
    }

    public String getMonth3BreakDownVehicleWorks() {
        return month3BreakDownVehicleWorks;
    }

    public void setMonth3BreakDownVehicleWorks(String month3BreakDownVehicleWorks) {
        this.month3BreakDownVehicleWorks = month3BreakDownVehicleWorks;
    }

    public String getMonth3RegularReeferService() {
        return month3RegularReeferService;
    }

    public void setMonth3RegularReeferService(String month3RegularReeferService) {
        this.month3RegularReeferService = month3RegularReeferService;
    }

    public String getMonth3RegularVehicleService() {
        return month3RegularVehicleService;
    }

    public void setMonth3RegularVehicleService(String month3RegularVehicleService) {
        this.month3RegularVehicleService = month3RegularVehicleService;
    }

    public String getInstallationDate() {
        return installationDate;
    }

    public void setInstallationDate(String installationDate) {
        this.installationDate = installationDate;
    }

    public String getInstallationKM() {
        return installationKM;
    }

    public void setInstallationKM(String installationKM) {
        this.installationKM = installationKM;
    }

    public String getTyerType() {
        return tyerType;
    }

    public void setTyerType(String tyerType) {
        this.tyerType = tyerType;
    }

    public String getTyreAmount() {
        return tyreAmount;
    }

    public void setTyreAmount(String tyreAmount) {
        this.tyreAmount = tyreAmount;
    }

    public String getUnInstallationDate() {
        return unInstallationDate;
    }

    public void setUnInstallationDate(String unInstallationDate) {
        this.unInstallationDate = unInstallationDate;
    }

    public String getUnInstallationKM() {
        return unInstallationKM;
    }

    public void setUnInstallationKM(String unInstallationKM) {
        this.unInstallationKM = unInstallationKM;
    }

    public String getReportDateTime() {
        return reportDateTime;
    }

    public void setReportDateTime(String reportDateTime) {
        this.reportDateTime = reportDateTime;
    }

    public String getDashboardopstrukCount() {
        return dashboardopstrukCount;
    }

    public void setDashboardopstrukCount(String dashboardopstrukCount) {
        this.dashboardopstrukCount = dashboardopstrukCount;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getNodstatusId() {
        return nodstatusId;
    }

    public void setNodstatusId(String nodstatusId) {
        this.nodstatusId = nodstatusId;
    }

    public String getNodStatusName() {
        return nodStatusName;
    }

    public void setNodStatusName(String nodStatusName) {
        this.nodStatusName = nodStatusName;
    }

    public String getNodTotalCount() {
        return nodTotalCount;
    }

    public void setNodTotalCount(String nodTotalCount) {
        this.nodTotalCount = nodTotalCount;
    }

    public String getZeroDay() {
        return zeroDay;
    }

    public void setZeroDay(String zeroDay) {
        this.zeroDay = zeroDay;
    }

    public String getOneDay() {
        return oneDay;
    }

    public void setOneDay(String oneDay) {
        this.oneDay = oneDay;
    }

    public String getTwoDay() {
        return twoDay;
    }

    public void setTwoDay(String twoDay) {
        this.twoDay = twoDay;
    }

    public String getThreeDay() {
        return threeDay;
    }

    public void setThreeDay(String threeDay) {
        this.threeDay = threeDay;
    }

    public String getFourDay() {
        return fourDay;
    }

    public void setFourDay(String fourDay) {
        this.fourDay = fourDay;
    }

    public String getFiveDay() {
        return fiveDay;
    }

    public void setFiveDay(String fiveDay) {
        this.fiveDay = fiveDay;
    }

    public String getSixDay() {
        return sixDay;
    }

    public void setSixDay(String sixDay) {
        this.sixDay = sixDay;
    }

    public String getSevenDay() {
        return sevenDay;
    }

    public void setSevenDay(String sevenDay) {
        this.sevenDay = sevenDay;
    }

    public String getMoreThanSevenDay() {
        return moreThanSevenDay;
    }

    public void setMoreThanSevenDay(String moreThanSevenDay) {
        this.moreThanSevenDay = moreThanSevenDay;
    }

    public String getNodstatusIds() {
        return nodstatusIds;
    }

    public void setNodstatusIds(String nodstatusIds) {
        this.nodstatusIds = nodstatusIds;
    }

    public String getNodTotalCounts() {
        return nodTotalCounts;
    }

    public void setNodTotalCounts(String nodTotalCounts) {
        this.nodTotalCounts = nodTotalCounts;
    }

    public String getNoOfDays() {
        return noOfDays;
    }

    public void setNoOfDays(String noOfDays) {
        this.noOfDays = noOfDays;
    }

    public String getFreightCharges() {
        return freightCharges;
    }

    public void setFreightCharges(String freightCharges) {
        this.freightCharges = freightCharges;
    }

    public String getOrderRefNo() {
        return orderRefNo;
    }

    public void setOrderRefNo(String orderRefNo) {
        this.orderRefNo = orderRefNo;
    }

    public String getConsignmentNo() {
        return consignmentNo;
    }

    public void setConsignmentNo(String consignmentNo) {
        this.consignmentNo = consignmentNo;
    }

    public String getTripEnd() {
        return tripEnd;
    }

    public void setTripEnd(String tripEnd) {
        this.tripEnd = tripEnd;
    }

    public String getTripStart() {
        return tripStart;
    }

    public void setTripStart(String tripStart) {
        this.tripStart = tripStart;
    }

    public String getConsignmentDate() {
        return consignmentDate;
    }

    public void setConsignmentDate(String consignmentDate) {
        this.consignmentDate = consignmentDate;
    }

    public String getSegmentName() {
        return segmentName;
    }

    public void setSegmentName(String segmentName) {
        this.segmentName = segmentName;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getServerIpAddress() {
        return serverIpAddress;
    }

    public void setServerIpAddress(String serverIpAddress) {
        this.serverIpAddress = serverIpAddress;
    }

    public String getMailSendingId() {
        return mailSendingId;
    }

    public void setMailSendingId(String mailSendingId) {
        this.mailSendingId = mailSendingId;
    }

    public String getMailSubjectTo() {
        return mailSubjectTo;
    }

    public void setMailSubjectTo(String mailSubjectTo) {
        this.mailSubjectTo = mailSubjectTo;
    }

    public String getMailSubjectCc() {
        return mailSubjectCc;
    }

    public void setMailSubjectCc(String mailSubjectCc) {
        this.mailSubjectCc = mailSubjectCc;
    }

    public String getMailContentTo() {
        return mailContentTo;
    }

    public void setMailContentTo(String mailContentTo) {
        this.mailContentTo = mailContentTo;
    }

    public String getMailContentCc() {
        return mailContentCc;
    }

    public void setMailContentCc(String mailContentCc) {
        this.mailContentCc = mailContentCc;
    }

    public String getMailIdTo() {
        return mailIdTo;
    }

    public void setMailIdTo(String mailIdTo) {
        this.mailIdTo = mailIdTo;
    }

    public String getMailIdCc() {
        return mailIdCc;
    }

    public void setMailIdCc(String mailIdCc) {
        this.mailIdCc = mailIdCc;
    }

    public String getMailIdBcc() {
        return mailIdBcc;
    }

    public void setMailIdBcc(String mailIdBcc) {
        this.mailIdBcc = mailIdBcc;
    }

    public String getMailDeliveredStatus() {
        return mailDeliveredStatus;
    }

    public void setMailDeliveredStatus(String mailDeliveredStatus) {
        this.mailDeliveredStatus = mailDeliveredStatus;
    }

    public String getMailTypeId() {
        return mailTypeId;
    }

    public void setMailTypeId(String mailTypeId) {
        this.mailTypeId = mailTypeId;
    }

    public HSSFWorkbook getMyWorkbook() {
        return myWorkbook;
    }

    public void setMyWorkbook(HSSFWorkbook myWorkbook) {
        this.myWorkbook = myWorkbook;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public ByteArrayOutputStream getOutputStream() {
        return outputStream;
    }

    public void setOutputStream(ByteArrayOutputStream outputStream) {
        this.outputStream = outputStream;
    }

    public String getInitialTripAdvance() {
        return initialTripAdvance;
    }

    public void setInitialTripAdvance(String initialTripAdvance) {
        this.initialTripAdvance = initialTripAdvance;
    }

    public String getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(String invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public String getCgstAmt() {
        return cgstAmt;
    }

    public void setCgstAmt(String cgstAmt) {
        this.cgstAmt = cgstAmt;
    }

    public String getSgstAmt() {
        return sgstAmt;
    }

    public void setSgstAmt(String sgstAmt) {
        this.sgstAmt = sgstAmt;
    }

    public String getIgstAmt() {
        return igstAmt;
    }

    public void setIgstAmt(String igstAmt) {
        this.igstAmt = igstAmt;
    }

    public String getCgst() {
        return cgst;
    }

    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    public String getSgst() {
        return sgst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public String getIgst() {
        return igst;
    }

    public void setIgst(String igst) {
        this.igst = igst;
    }

    public String getCustomerGSTNo() {
        return customerGSTNo;
    }

    public void setCustomerGSTNo(String customerGSTNo) {
        this.customerGSTNo = customerGSTNo;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getCustomerErpId() {
        return customerErpId;
    }

    public void setCustomerErpId(String customerErpId) {
        this.customerErpId = customerErpId;
    }

    public int getAdvanceId() {
        return advanceId;
    }

    public void setAdvanceId(int advanceId) {
        this.advanceId = advanceId;
    }

    public String getEmailDetails() {
        return emailDetails;
    }

    public void setEmailDetails(String emailDetails) {
        this.emailDetails = emailDetails;
    }

    public String getConsignorCode() {
        return consignorCode;
    }

    public void setConsignorCode(String consignorCode) {
        this.consignorCode = consignorCode;
    }

    public String getConsigneeCode() {
        return consigneeCode;
    }

    public void setConsigneeCode(String consigneeCode) {
        this.consigneeCode = consigneeCode;
    }

    public String getVendorType() {
        return vendorType;
    }

    public void setVendorType(String vendorType) {
        this.vendorType = vendorType;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getMailTime() {
        return mailTime;
    }

    public void setMailTime(String mailTime) {
        this.mailTime = mailTime;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getReporttype() {
        return reporttype;
    }

    public void setReporttype(String reporttype) {
        this.reporttype = reporttype;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getRemaingTime() {
        return remaingTime;
    }

    public void setRemaingTime(String remaingTime) {
        this.remaingTime = remaingTime;
    }

    public String getRemaingKM() {
        return remaingKM;
    }

    public void setRemaingKM(String remaingKM) {
        this.remaingKM = remaingKM;
    }

    public String getCurrentETA() {
        return currentETA;
    }

    public void setCurrentETA(String currentETA) {
        this.currentETA = currentETA;
    }

    public String getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(String lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getSealno() {
        return sealno;
    }

    public void setSealno(String sealno) {
        this.sealno = sealno;
    }
    
    

}