 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.report.web;

import org.json.JSONArray;
import org.json.JSONObject;
import ets.domain.report.business.ReportTO;
import ets.domain.report.web.ReportCommand;
import ets.domain.purchase.web.PurchaseCommand;
import ets.domain.section.web.SectionCommand;
import ets.arch.web.BaseController;
import ets.domain.users.business.LoginBP;
import ets.domain.operation.business.OperationBP;
import ets.domain.vehicle.business.VehicleBP;
import ets.domain.company.business.CompanyTO;
import ets.domain.users.business.LoginBP;

import ets.domain.util.FPLogUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import ets.domain.finance.business.FinanceBP;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;
import java.util.Iterator;
import java.util.ArrayList;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.util.ParveenErrorConstants;
import ets.domain.report.business.ReportBP;
import ets.domain.section.business.SectionBP;
import ets.domain.vehicle.business.VehicleBP;
import ets.domain.company.business.CompanyBP;
import ets.domain.vendor.business.VendorBP;
import ets.domain.renderservice.business.RenderServiceBP;
import ets.domain.purchase.business.PurchaseBP;
import ets.domain.customer.business.CustomerBP;
import ets.arch.business.PaginationHelper;
import ets.domain.mrs.business.MrsBP;
import ets.domain.operation.business.OperationTO;
import ets.domain.purchase.business.PurchaseTO;
import ets.domain.report.business.DprTO;
import ets.domain.section.business.SectionTO;
import ets.domain.stockTransfer.business.StockTransferTO;
import ets.domain.trip.business.TripBP;
import ets.domain.util.ThrottleConstants;
import ets.domain.vehicle.business.VehicleTO;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import net.sf.json.JSONException;

/**
 *
 * @author Rajasekhar
 */
public class ReportController extends BaseController {

    ReportBP reportBP;
    ReportCommand reportCommand;
    OperationBP operationBP;
    VehicleBP vehicleBP;
    SectionBP sectionBP;
    CompanyBP companyBP;
    VendorBP vendorBP;
    RenderServiceBP renderServiceBP;
    PurchaseBP purchaseBP;
    CustomerBP customerBP;
    PurchaseCommand purchaseCommand;
    SectionCommand sectionCommand;
    MrsBP mrsBP;
    LoginBP loginBP;
    TripBP tripBP;
    FinanceBP financeBP;

    public MrsBP getMrsBP() {
        return mrsBP;
    }

    public void setMrsBP(MrsBP mrsBP) {
        this.mrsBP = mrsBP;
    }

    public PurchaseCommand getPurchaseCommand() {
        return purchaseCommand;
    }

    public void setPurchaseCommand(PurchaseCommand purchaseCommand) {
        this.purchaseCommand = purchaseCommand;
    }

    public SectionCommand getSectionCommand() {
        return sectionCommand;
    }

    public void setSectionCommand(SectionCommand sectionCommand) {
        this.sectionCommand = sectionCommand;
    }

    public PurchaseBP getPurchaseBP() {
        return purchaseBP;
    }

    public void setPurchaseBP(PurchaseBP purchaseBP) {
        this.purchaseBP = purchaseBP;
    }

    public RenderServiceBP getRenderServiceBP() {
        return renderServiceBP;
    }

    public void setRenderServiceBP(RenderServiceBP renderServiceBP) {
        this.renderServiceBP = renderServiceBP;
    }

    public CustomerBP getCustomerBP() {
        return customerBP;
    }

    public void setCustomerBP(CustomerBP custBP) {
        this.customerBP = custBP;
    }

    public ReportBP getReportBP() {
        return reportBP;
    }

    public void setReportBP(ReportBP repBP) {
        this.reportBP = repBP;
    }

    public ReportCommand getReportCommand() {
        return reportCommand;
    }

    public void setReportCommad(ReportCommand repcomm) {
        this.reportCommand = repcomm;
    }

    public OperationBP getOperationBP() {
        return operationBP;
    }

    public void setOperationBP(OperationBP operationBP) {
        this.operationBP = operationBP;
    }

    public CompanyBP getCompanyBP() {
        return companyBP;
    }

    public void setCompanyBP(CompanyBP companyBP) {
        this.companyBP = companyBP;
    }

    public SectionBP getSectionBP() {
        return sectionBP;
    }

    public void setSectionBP(SectionBP sectionBP) {
        this.sectionBP = sectionBP;
    }

    public VendorBP getVendorBP() {
        return vendorBP;
    }

    public void setVendorBP(VendorBP vendorBP) {
        this.vendorBP = vendorBP;
    }

    public VehicleBP getVehicleBP() {
        return vehicleBP;
    }

    public void setVehicleBP(VehicleBP vehicleBP) {
        this.vehicleBP = vehicleBP;
    }

    public LoginBP getLoginBP() {
        return this.loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public TripBP getTripBP() {
        return tripBP;
    }

    public void setTripBP(TripBP tripBP) {
        this.tripBP = tripBP;
    }

    public FinanceBP getFinanceBP() {
        return financeBP;
    }

    public void setFinanceBP(FinanceBP financeBP) {
        this.financeBP = financeBP;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();
        initialize(request);
    }

    public ModelAndView handleBillListPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList serviceTypes = new ArrayList();
        ArrayList customerTypes = new ArrayList();
        ArrayList operationPointList = new ArrayList();
        ArrayList servicePointList = new ArrayList();
        ArrayList billList = new ArrayList();
        ArrayList taxSummary = new ArrayList();

        String menuPath = "Reports >>  Vehicle >> Jobcard Bills";
        path = "content/report/billList.jsp";
        try {
            String fromDate = (String) session.getAttribute("currentDate");
            String toDate = (String) session.getAttribute("currentDate");
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            String pageTitle = "View Jobcard Bills";
            ReportTO repTO = new ReportTO();
            repTO.setCompanyName("0");
            repTO.setServiceTypeId("0");
            repTO.setReportType(0);

            serviceTypes = operationBP.getServiceTypes();
            customerTypes = operationBP.getCustomerTypes();
            operationPointList = vehicleBP.processGetopList();
            request.setAttribute("operationPointList", operationPointList);
            request.setAttribute("pageTitle", pageTitle);
            request.setAttribute("serviceTypes", serviceTypes);
            request.setAttribute("customerTypes", customerTypes);

            servicePointList = vehicleBP.processGetspList();
            request.setAttribute("servicePointList", servicePointList);

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

//                billList = reportBP.processBillList(repTO,fromDate,toDate);
//                taxSummary = reportBP.processSalesTaxSummary(repTO,fromDate,toDate);
//                request.setAttribute("taxSummary", taxSummary );
//                request.setAttribute("billList", billList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleBillList(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList serviceTypes = new ArrayList();
        ArrayList customerTypes = new ArrayList();
        ArrayList operationPointList = new ArrayList();
        ArrayList servicePointList = new ArrayList();
        ArrayList taxSummary = new ArrayList();
        String menuPath = "Reports >>  Vehicle >> Jobcard Bills";
        path = "content/report/billList.jsp";
        try {
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");

            serviceTypes = operationBP.getServiceTypes();
            customerTypes = operationBP.getCustomerTypes();
            operationPointList = vehicleBP.processGetopList();
            request.setAttribute("serviceTypes", serviceTypes);
            request.setAttribute("customerTypes", customerTypes);
            request.setAttribute("operationPointList", operationPointList);

            servicePointList = vehicleBP.processGetspList();
            request.setAttribute("servicePointList", servicePointList);

            System.out.println("regNo command=" + reportCommand.getRegNo());
            ReportTO repTO = new ReportTO();
            repTO.setCompanyName(reportCommand.getOpId());
            repTO.setServicePointId(reportCommand.getSpId());
            repTO.setServiceTypeId(reportCommand.getServiceType());
            repTO.setCustomerTypeId(reportCommand.getCustomerType());
            System.out.println("Selected CustomerType--->" + reportCommand.getCustomerType());
            repTO.setRegNo(reportCommand.getRegNo());
            repTO.setReportType(Integer.parseInt(reportCommand.getReportType()));
            System.out.println("" + repTO.getReportType());

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Section";

            request.setAttribute("pageTitle", pageTitle);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("regNo", reportCommand.getRegNo());
            request.setAttribute("opId", reportCommand.getOpId());
            request.setAttribute("spId", reportCommand.getSpId());
            request.setAttribute("serviceType", reportCommand.getServiceType());
            request.setAttribute("customerType", reportCommand.getCustomerType());
            System.out.println("customerTypeId in cont" + reportCommand.getCustomerType());
            request.setAttribute("reportType", reportCommand.getReportType());

            ArrayList billList = new ArrayList();
            billList = reportBP.processBillList(repTO, fromDate, toDate);
            System.out.println("Bill List.size() in Controller" + billList.size());

            taxSummary = reportBP.processSalesTaxSummary(repTO, fromDate, toDate);
            System.out.println("taxSummary.size() in Controller" + taxSummary.size());

            request.setAttribute("taxSummary", taxSummary);
            request.setAttribute("billList", billList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleBillDetails(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList serviceTypes = new ArrayList();
        ArrayList operationPointList = new ArrayList();
        String menuPath = "STORES >>  Manage Section";
        path = "content/report/generatedBill.jsp";
        try {
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            serviceTypes = operationBP.getServiceTypes();
            operationPointList = vehicleBP.processGetopList();
            request.setAttribute("serviceTypes", serviceTypes);
            request.setAttribute("operationPointList", operationPointList);

            System.out.println("regNo command=" + reportCommand.getRegNo());
            ReportTO repTO = new ReportTO();
            repTO.setCompanyName(reportCommand.getOpId());
            repTO.setServiceTypeId(reportCommand.getServiceType());
            repTO.setRegNo(reportCommand.getRegNo());
            repTO.setJobCardId(reportCommand.getJobCardId());
            repTO.setBillNo(request.getParameter("billId"));
            System.out.println("billid=" + request.getParameter("billId"));
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("regNo", reportCommand.getRegNo());
            request.setAttribute("opId", reportCommand.getOpId());
            request.setAttribute("serviceType", reportCommand.getServiceType());

            ArrayList billDetails = new ArrayList();
            ArrayList jcList = new ArrayList();
            ArrayList bodyBillDetails = new ArrayList();
            ArrayList activityList = new ArrayList();
            ArrayList totalPrices = new ArrayList();
            jcList = renderServiceBP.getJobCardsBillDetails(Integer.parseInt(reportCommand.getJobCardId()));
            billDetails = reportBP.processBillDetails(repTO);
            bodyBillDetails = reportBP.processHikedBodyBillDetails(repTO);
            activityList = reportBP.processActivityList(repTO);
            totalPrices = reportBP.processTotalPrices(repTO);
            request.setAttribute("jcList", jcList);
            request.setAttribute("billDetails", billDetails);
            request.setAttribute("bodyBillDetails", bodyBillDetails);
            request.setAttribute("ActivityList", activityList);
            request.setAttribute("totalPrices", totalPrices);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handlePurchaseReportPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        System.out.println("Enter Sucess");
        HttpSession session = request.getSession();
        ArrayList categoryList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList vendorList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Stores >>  Purchase Report";
        String pageTitle = "Purchase Report ";
        request.setAttribute("pageTitle", pageTitle);
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        try {
            path = "content/report/stockPurchaseRep.jsp";
            categoryList = sectionBP.processGetCategoryList();
            mfrList = vehicleBP.processGetMfrList();
            VehicleTO vto = new VehicleTO();
            modelList = vehicleBP.processGetModelList(vto);
            locationList = companyBP.processGetCompanyList();
            vendorList = vendorBP.processGetVendorList();
            System.out.println("categoryList" + categoryList.size());
            request.setAttribute("categoryList", categoryList);
            request.setAttribute("MfrLists", mfrList);
            request.setAttribute("ModelLists", modelList);
            request.setAttribute("LocationLists", locationList);
            request.setAttribute("VendorLists", vendorList);
            request.setAttribute("companyId", companyId);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handlePurchaseReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList categoryList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList vendorList = new ArrayList();
        ArrayList purchaseReportList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Stores >>  Purchase Report";
        String pageTitle = "Purchase Report ";
        request.setAttribute("pageTitle", pageTitle);
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/stockPurchaseRep.jsp";
            categoryList = sectionBP.processGetCategoryList();
            mfrList = vehicleBP.processGetMfrList();
            VehicleTO vto = new VehicleTO();
            modelList = vehicleBP.processGetModelList(vto);
            locationList = companyBP.processGetCompanyList();
            vendorList = vendorBP.processGetVendorList();
            request.setAttribute("categoryList", categoryList);
            request.setAttribute("MfrLists", mfrList);
            request.setAttribute("ModelLists", modelList);
            request.setAttribute("LocationLists", locationList);
            request.setAttribute("VendorLists", vendorList);
            request.setAttribute("vendorId", reportCommand.getVendorId());
            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("mfrId", reportCommand.getMfrId());
            request.setAttribute("modelId", reportCommand.getModelId());
            request.setAttribute("mfrCode", reportCommand.getMfrCode());
            request.setAttribute("paplCode", reportCommand.getPaplCode());
            request.setAttribute("categoryId", reportCommand.getCategoryId());
            request.setAttribute("itemName", reportCommand.getItemName());
            request.setAttribute("purType", reportCommand.getPurType());
            request.setAttribute("poId", reportCommand.getPoId());
            ReportTO reportTO = new ReportTO();
            reportTO.setCompanyId(reportCommand.getCompanyId());
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());
            if (Integer.parseInt(reportCommand.getCategoryId()) != 0) {
                reportTO.setCategoryId(reportCommand.getCategoryId());
            }
            if (Integer.parseInt(reportCommand.getVendorId()) != 0) {
                reportTO.setVendorId(reportCommand.getVendorId());
            }
            if (Integer.parseInt(reportCommand.getMfrId()) != 0) {
                reportTO.setMfrId(reportCommand.getMfrId());
            }
            if (Integer.parseInt(reportCommand.getModelId()) != 0) {
                reportTO.setModelId(reportCommand.getModelId());
            }
            if ((reportCommand.getPurType()).equalsIgnoreCase("Y") || (reportCommand.getPurType()).equalsIgnoreCase("N")) {
                reportTO.setPurType(reportCommand.getPurType());
            }
            reportTO.setPoId(reportCommand.getPoId());
            reportTO.setItemName(reportCommand.getItemName());
            reportTO.setMfrCode(reportCommand.getMfrCode());
            reportTO.setPaplCode(reportCommand.getPaplCode());
            purchaseReportList = reportBP.processPurchaseReport(reportTO);
            request.setAttribute("purchaseReportList", purchaseReportList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleSearchVehicleComplaints(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList sectionList = new ArrayList();
        ArrayList technicianList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Vehicle>>Mechnical Performance";
        String pageTitle = "Vehicle Complaints ";
        request.setAttribute("pageTitle", pageTitle);
        try {

            path = "content/report/VehicleComplaintHist.jsp";
            sectionList = sectionBP.processGetSectionList();
            System.out.println("section" + sectionList.size());
            request.setAttribute("SectionList", sectionList);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            technicianList = reportBP.processTechniciansList();
            request.setAttribute("TechnicianList", technicianList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVehicleComplaints(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList complaintList = new ArrayList();
        ArrayList sectionList = new ArrayList();
        ArrayList technicianList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Vehicle>>Mechnical Performance";
        String pageTitle = "Vehicle Complaints ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            System.out.println("fromDate" + fromDate);
            System.out.println("toDate" + toDate);
            ReportTO repTO = new ReportTO();
            repTO.setRegNo(reportCommand.getRegNo());
            if (Integer.parseInt(reportCommand.getSectionId()) != 0) {
                repTO.setSectionId(reportCommand.getSectionId());
            }
            repTO.setProblem(request.getParameter("probId"));
            repTO.setTechnicianId(reportCommand.getTechnicianId());
            path = "content/report/VehicleComplaintHist.jsp";
            sectionList = sectionBP.processGetSectionList();
            System.out.println("section" + sectionList.size());
            request.setAttribute("SectionList", sectionList);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("regNo", reportCommand.getRegNo());
            request.setAttribute("sectionId", reportCommand.getSectionId());
            request.setAttribute("problemId", request.getParameter("probId"));
            request.setAttribute("technicianId", reportCommand.getTechnicianId());
            technicianList = reportBP.processTechniciansList();
            request.setAttribute("TechnicianList", technicianList);
            complaintList = reportBP.processComplaintList(repTO, fromDate, toDate);
            request.setAttribute("complaintList", complaintList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Complaint List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleStockWorthPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList sectionList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Stores >>  StockWorth Report";
        String pageTitle = "Stock Worth Report ";
        request.setAttribute("pageTitle", pageTitle);
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/stockWorthRep.jsp";
            sectionList = sectionBP.processGetCategoryList();
            mfrList = vehicleBP.processGetMfrList();
            VehicleTO vto = new VehicleTO();
            modelList = vehicleBP.processGetModelList(vto);
            locationList = companyBP.processGetCompanyList();
            System.out.println("section" + sectionList.size());
            request.setAttribute("SectionList", sectionList);
            request.setAttribute("MfrLists", mfrList);
            request.setAttribute("ModelLists", modelList);
            request.setAttribute("LocationLists", locationList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleStockWorthReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList sectionList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList stockWorthList = new ArrayList();
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        String path = "";
        String menuPath = "Reports >>Stores >>  StockWorth Report";
        String pageTitle = "Stock Worth Report ";
        request.setAttribute("pageTitle", pageTitle);
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/stockWorthRep.jsp";
            sectionList = sectionBP.processGetCategoryList();
            mfrList = vehicleBP.processGetMfrList();
            VehicleTO vto = new VehicleTO();
            modelList = vehicleBP.processGetModelList(vto);
            locationList = companyBP.processGetCompanyList();
            System.out.println("section" + sectionList.size());

            request.setAttribute("SectionList", sectionList);
            request.setAttribute("MfrLists", mfrList);
            request.setAttribute("ModelLists", modelList);
            request.setAttribute("LocationLists", locationList);
            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("mfrId", reportCommand.getMfrId());
            request.setAttribute("modelId", reportCommand.getModelId());
            request.setAttribute("mfrCode", reportCommand.getMfrCode());
            request.setAttribute("paplCode", reportCommand.getPaplCode());
            request.setAttribute("sectionId", reportCommand.getSectionId());
            request.setAttribute("itemName", reportCommand.getItemName());
            reportTO.setCompanyId(reportCommand.getCompanyId());
            reportTO.setFromDate(reportCommand.getFromDate());
            if (Integer.parseInt(reportCommand.getSectionId()) != 0) {
                reportTO.setSectionId(reportCommand.getSectionId());
            }
            if (Integer.parseInt(reportCommand.getMfrId()) != 0) {
                reportTO.setMfrId(reportCommand.getMfrId());
            }
            if (Integer.parseInt(reportCommand.getModelId()) != 0) {
                reportTO.setModelId(reportCommand.getModelId());
            }
            reportTO.setItemType(reportCommand.getItemType());

            reportTO.setItemName(reportCommand.getItemName());
            reportTO.setMfrCode(reportCommand.getMfrCode());
            reportTO.setPaplCode(reportCommand.getPaplCode());
            stockWorthList = reportBP.processStockWorthReport(reportTO);
            request.setAttribute("stockWorthList", stockWorthList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleStockWorthRates(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList rateList = new ArrayList();
        ReportTO reportTO = new ReportTO();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Stores >>  StockWorth Report";
        String pageTitle = "Stock Worth Report ";
        request.setAttribute("pageTitle", pageTitle);
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/stockWorthRates.jsp";
            reportTO.setProcessId(reportCommand.getProcessId());
            reportTO.setItemName(reportCommand.getItemName());
            rateList = reportBP.processRateList(reportTO);
            request.setAttribute("rateList", rateList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve rateList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleBodyPartsBillPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList vendorList = new ArrayList();
        ArrayList locationList = new ArrayList();
        reportCommand = command;
        int vendorType = 1014;
        String path = "";
        String menuPath = "Reports >>Vehicle >>  Contractors Bill";
        try {
            System.out.println("in cont");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/bodyPartsBillReport.jsp";
            locationList = companyBP.processGetCompanyList();
            vendorList = purchaseBP.processVendorList(vendorType);
            request.setAttribute("LocationLists", locationList);
            request.setAttribute("VendorLists", vendorList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleBodyPartsBill(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList vendorList = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList bodyBillList = new ArrayList();
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        int vendorType = 1014;
        String path = "";
        String menuPath = "Reports >>Vehicles >> Contractors Bill";
        try {
            System.out.println("in cont");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/bodyPartsBillReport.jsp";
            locationList = companyBP.processGetCompanyList();
            vendorList = purchaseBP.processVendorList(vendorType);
            request.setAttribute("LocationLists", locationList);
            request.setAttribute("VendorLists", vendorList);
            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("woId", reportCommand.getWoId());
            request.setAttribute("regNo", reportCommand.getRegNo());
            request.setAttribute("jobCardId", reportCommand.getJobCardId());
            request.setAttribute("vendorId", reportCommand.getVendorId());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            reportTO.setRegNo(reportCommand.getRegNo());
            reportTO.setCompanyId(reportCommand.getCompanyId());
            reportTO.setWoId(reportCommand.getWoId());
            reportTO.setJobCardId(reportCommand.getJobCardId());
            if (Integer.parseInt(reportCommand.getVendorId()) != 0) {
                reportTO.setVendorId(reportCommand.getVendorId());
            }
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());
            bodyBillList = reportBP.processBodyBillList(reportTO);
            request.setAttribute("BodyBillList", bodyBillList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleBodyPartsBillDetails(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList vendorList = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList bodyBillDetails = new ArrayList();
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        int vendorType = 1014;
        String path = "";
        String menuPath = "Reports >>Vehicle >>  Contractors Bill";
        try {
            System.out.println("in cont");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/bodyBillDetails.jsp";
            reportTO.setVendorId(reportCommand.getVendorId());
            System.out.println("vendorId" + reportCommand.getVendorId());
            reportTO.setJobCardId(request.getParameter("jobCardId"));
            System.out.println("jobCardid=" + reportTO.getJobCardId());
            bodyBillDetails = reportBP.processBodyBillDetails(reportTO);
            request.setAttribute("BodyBillDetails", bodyBillDetails);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handlePeriodicServiceListPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList locationList = new ArrayList();
        String path = "";
        String menuPath = "Reports >> Render Services >> Service Due";
        String pageTitle = "Stock Worth Report ";
        request.setAttribute("pageTitle", pageTitle);
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/vehicleServiceDue.jsp";
            locationList = companyBP.processGetCompanyList();
            request.setAttribute("LocationLists", locationList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve rateList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handlePeriodicServiceList(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList serviceList = new ArrayList();
        ArrayList locationList = new ArrayList();
        String regno = command.getRegno();
        String compId = command.getOpId();
        String tillDate = command.getToDate();
        String path = "";
        String menuPath = "Reports >> Render Services >> Service Due";
        String pageTitle = "Stock Worth Report ";
        request.setAttribute("pageTitle", pageTitle);
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/vehicleServiceDue.jsp";

            request.setAttribute("compId", compId);
            request.setAttribute("tillDate", tillDate);
            request.setAttribute("regno", regno);

            locationList = companyBP.processGetCompanyList();
            request.setAttribute("LocationLists", locationList);

            serviceList = reportBP.processPeriodicServiceList(Integer.parseInt(compId), regno, tillDate);
            if (serviceList.size() != 0) {
                request.setAttribute("serviceList", serviceList);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve rateList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleFCDuePage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList sectionList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Vehicle >Vehicle FC Due ";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/vehicleFCDue.jsp";
            locationList = companyBP.processGetCompanyList();
            System.out.println("section" + sectionList.size());
            request.setAttribute("LocationLists", locationList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleFCDuesReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList sectionList = new ArrayList();
        ArrayList dueList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        String path = "";
        String menuPath = "Reports >>Vehicle >Vehicle FC Due ";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/vehicleFCDue.jsp";
            locationList = companyBP.processGetCompanyList();
            request.setAttribute("LocationLists", locationList);
            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("regNo", reportCommand.getRegNo());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("dueIn", reportCommand.getDueIn());
            reportTO.setRegNo(reportCommand.getRegNo());

            if (Integer.parseInt(reportCommand.getCompanyId()) != 0) {
                reportTO.setCompanyId(reportCommand.getCompanyId());
            }
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());
            String dueIn = reportCommand.getDueIn();
            if (dueIn == null || "".equals(dueIn)) {
                dueIn = "0";
            }
            reportTO.setDueIn(dueIn);
            dueList = reportBP.processDueList(reportTO);
            request.setAttribute("DueList", dueList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleRoadTaxDuePage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList sectionList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Vehicle >Vehicle Road Tax Due ";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/vehicleRoadTaxDue.jsp";
            locationList = companyBP.processGetCompanyList();
            System.out.println("section" + sectionList.size());
            request.setAttribute("LocationLists", locationList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleRoadTaxDuesReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList sectionList = new ArrayList();
        ArrayList dueList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        String path = "";
        String menuPath = "Reports >>Vehicle >Vehicle Road Tax Due";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/vehicleRoadTaxDue.jsp";
            locationList = companyBP.processGetCompanyList();
            request.setAttribute("LocationLists", locationList);
            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("regNo", reportCommand.getRegNo());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("dueIn", reportCommand.getDueIn());
            reportTO.setRegNo(reportCommand.getRegNo());

            if (Integer.parseInt(reportCommand.getCompanyId()) != 0) {
                reportTO.setCompanyId(reportCommand.getCompanyId());
            }
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());
            String dueIn = reportCommand.getDueIn();
            if (dueIn == null || "".equals(dueIn)) {
                dueIn = "0";
            }
            reportTO.setDueIn(dueIn);
            dueList = reportBP.processRoadTaxDueList(reportTO);
            request.setAttribute("DueList", dueList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleInsuranceDuePage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList sectionList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Vehicle >Vehicle Insurance Due ";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/vehicleInsuranceDue.jsp";
            locationList = companyBP.processGetCompanyList();
            System.out.println("section" + sectionList.size());
            request.setAttribute("LocationLists", locationList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleInsuranceDuesReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList sectionList = new ArrayList();
        ArrayList dueList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        String path = "";
        String menuPath = "Reports >>Vehicle >Vehicle Insurance Due ";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/vehicleInsuranceDue.jsp";
            locationList = companyBP.processGetCompanyList();
            request.setAttribute("LocationLists", locationList);
            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("regNo", reportCommand.getRegNo());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("dueIn", reportCommand.getDueIn());
            reportTO.setRegNo(reportCommand.getRegNo());

            if (Integer.parseInt(reportCommand.getCompanyId()) != 0) {
                reportTO.setCompanyId(reportCommand.getCompanyId());
            }
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());
            String dueIn = reportCommand.getDueIn();
            if (dueIn == null || "".equals(dueIn)) {
                dueIn = "0";
            }
            reportTO.setDueIn(dueIn);
            dueList = reportBP.processInsuranceDueList(reportTO);
            request.setAttribute("DueList", dueList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handlePermitDuePage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList sectionList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Vehicle >Vehicle Permit Due ";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/vehiclePermitDue.jsp";
            locationList = companyBP.processGetCompanyList();
            System.out.println("section" + sectionList.size());
            request.setAttribute("LocationLists", locationList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAMCDuePage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList sectionList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Vehicle >Vehicle AMC Due ";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/vehicleAMCDue.jsp";
            locationList = companyBP.processGetCompanyList();
            System.out.println("section" + sectionList.size());
            request.setAttribute("LocationLists", locationList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handlePermitDuesReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList sectionList = new ArrayList();
        ArrayList dueList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        String path = "";
        String menuPath = "Reports >>Vehicle >Vehicle Permit Due ";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/vehiclePermitDue.jsp";
            locationList = companyBP.processGetCompanyList();
            request.setAttribute("LocationLists", locationList);
            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("regNo", reportCommand.getRegNo());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("dueIn", reportCommand.getDueIn());
            reportTO.setRegNo(reportCommand.getRegNo());

            if (Integer.parseInt(reportCommand.getCompanyId()) != 0) {
                reportTO.setCompanyId(reportCommand.getCompanyId());
            }
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());
            String dueIn = reportCommand.getDueIn();
            if (dueIn == null || "".equals(dueIn)) {
                dueIn = "0";
            }
            reportTO.setDueIn(dueIn);
            dueList = reportBP.processPermitDueList(reportTO);
            request.setAttribute("DueList", dueList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAMCDuesReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList sectionList = new ArrayList();
        ArrayList dueList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        String path = "";
        String menuPath = "Reports >>Vehicle >Vehicle Permit Due ";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/vehicleAMCDue.jsp";
            locationList = companyBP.processGetCompanyList();
            request.setAttribute("LocationLists", locationList);
            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("regNo", reportCommand.getRegNo());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("dueIn", reportCommand.getDueIn());
            reportTO.setRegNo(reportCommand.getRegNo());

            if (Integer.parseInt(reportCommand.getCompanyId()) != 0) {
                reportTO.setCompanyId(reportCommand.getCompanyId());
            }
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());
            String dueIn = reportCommand.getDueIn();
            if (dueIn == null || "".equals(dueIn)) {
                dueIn = "0";
            }
            reportTO.setDueIn(dueIn);
            dueList = reportBP.processAMCDueList(reportTO);
            request.setAttribute("DueList", dueList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleStockIssuePage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList categoryList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList vendorList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Stores >>  Stock Issue Report";
        String pageTitle = "Purchase Report ";
        request.setAttribute("pageTitle", pageTitle);
        try {
            path = "content/report/stockIssueRep.jsp";
            categoryList = sectionBP.processGetCategoryList();
            mfrList = vehicleBP.processGetMfrList();
            VehicleTO vto = new VehicleTO();
            modelList = vehicleBP.processGetModelList(vto);
            locationList = companyBP.processGetCompanyList();
            System.out.println("categoryList" + categoryList.size());
            request.setAttribute("categoryList", categoryList);
            request.setAttribute("MfrLists", mfrList);
            request.setAttribute("ModelLists", modelList);
            request.setAttribute("LocationLists", locationList);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleStockIssueReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList categoryList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList stokIssueList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Stores >>  StockIssue Report";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/stockIssueRep.jsp";
            categoryList = sectionBP.processGetCategoryList();
            mfrList = vehicleBP.processGetMfrList();
            VehicleTO vto = new VehicleTO();
            modelList = vehicleBP.processGetModelList(vto);
            locationList = companyBP.processGetCompanyList();
            request.setAttribute("MfrLists", mfrList);
            request.setAttribute("ModelLists", modelList);
            request.setAttribute("LocationLists", locationList);
            request.setAttribute("categoryList", categoryList);
            request.setAttribute("categoryId", reportCommand.getCategoryId());
            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("regNo", reportCommand.getRegNo());
            request.setAttribute("mfrId", reportCommand.getMfrId());
            request.setAttribute("paplCode", reportCommand.getPaplCode());
            request.setAttribute("itemName", reportCommand.getItemName());
            request.setAttribute("rcWorkorderId", reportCommand.getRcWorkorderId());
            request.setAttribute("counterId", reportCommand.getCounterId());
            ReportTO reportTO = new ReportTO();

            reportTO.setCompanyId(reportCommand.getCompanyId());
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());
            reportTO.setCategoryId(reportCommand.getCategoryId());
            reportTO.setMfrId(reportCommand.getMfrId());
            reportTO.setItemName(reportCommand.getItemName());
            reportTO.setMfrCode(reportCommand.getMfrCode());
            reportTO.setPaplCode(reportCommand.getPaplCode());

            reportTO.setRegNo(reportCommand.getRegNo());
            System.out.println("RcWorkorderId" + reportCommand.getRcWorkorderId());
            System.out.println("CounterId" + reportCommand.getCounterId());
            System.out.println("Papl Code" + reportCommand.getPaplCode());

            if (reportCommand.getRcWorkorderId() != null && !"".equals(reportCommand.getRcWorkorderId()) && reportCommand.getRcWorkorderId() != "0") {
                reportTO.setRcWorkorderId(Integer.parseInt(reportCommand.getRcWorkorderId()));
            }

            if (reportCommand.getCounterId() != null && !"".equals(reportCommand.getCounterId()) && reportCommand.getCounterId() != "0") {
                reportTO.setCounterId(Integer.parseInt(reportCommand.getCounterId()));
            }

            stokIssueList = reportBP.processStockIssueReport(reportTO);
            request.setAttribute("stokIssueList", stokIssueList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleStockIssueExcelRepNew(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList categoryList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList stokIssueList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Stores >>  StockIssue Report";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/stockIssueExcelRep.jsp";
            categoryList = sectionBP.processGetCategoryList();
            mfrList = vehicleBP.processGetMfrList();
            VehicleTO vto = new VehicleTO();
            modelList = vehicleBP.processGetModelList(vto);
            locationList = companyBP.processGetCompanyList();
            request.setAttribute("MfrLists", mfrList);
            request.setAttribute("ModelLists", modelList);
            request.setAttribute("LocationLists", locationList);
            request.setAttribute("categoryList", categoryList);
            request.setAttribute("categoryId", reportCommand.getCategoryId());
            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("regNo", reportCommand.getRegNo());
            request.setAttribute("mfrId", reportCommand.getMfrId());
            request.setAttribute("paplCode", reportCommand.getPaplCode());
            request.setAttribute("itemName", reportCommand.getItemName());
            request.setAttribute("rcWorkorderId", reportCommand.getRcWorkorderId());
            request.setAttribute("counterId", reportCommand.getCounterId());
            ReportTO reportTO = new ReportTO();

            reportTO.setCompanyId(reportCommand.getCompanyId());
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());
            reportTO.setCategoryId(reportCommand.getCategoryId());
            reportTO.setMfrId(reportCommand.getMfrId());
            reportTO.setItemName(reportCommand.getItemName());
            reportTO.setMfrCode(reportCommand.getMfrCode());
            reportTO.setPaplCode(reportCommand.getPaplCode());

            reportTO.setRegNo(reportCommand.getRegNo());
            System.out.println("RcWorkorderId" + reportCommand.getRcWorkorderId());
            System.out.println("CounterId" + reportCommand.getCounterId());
            System.out.println("Papl Code" + reportCommand.getPaplCode());

            if (reportCommand.getRcWorkorderId() != null && !"".equals(reportCommand.getRcWorkorderId()) && reportCommand.getRcWorkorderId() != "0") {
                reportTO.setRcWorkorderId(Integer.parseInt(reportCommand.getRcWorkorderId()));
            }

            if (reportCommand.getCounterId() != null && !"".equals(reportCommand.getCounterId()) && reportCommand.getCounterId() != "0") {
                reportTO.setCounterId(Integer.parseInt(reportCommand.getCounterId()));
            }

            stokIssueList = reportBP.processStockIssueReport(reportTO);
            request.setAttribute("stokIssueList", stokIssueList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleStoresEffPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList categoryList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList vendorList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Stores >>  Stores Efficiency Report";
        try {
            path = "content/report/storesEff.jsp";
            categoryList = sectionBP.processGetCategoryList();
            mfrList = vehicleBP.processGetMfrList();
            VehicleTO vto = new VehicleTO();
            modelList = vehicleBP.processGetModelList(vto);
            locationList = companyBP.processGetCompanyList();
            System.out.println("categoryList" + categoryList.size());
            request.setAttribute("categoryList", categoryList);
            request.setAttribute("MfrLists", mfrList);
            request.setAttribute("ModelLists", modelList);
            request.setAttribute("LocationLists", locationList);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleStoresEffReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList categoryList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList storesEffList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Stores >> Stores Eficiency Report";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/storesEff.jsp";
            categoryList = sectionBP.processGetCategoryList();
            mfrList = vehicleBP.processGetMfrList();
            VehicleTO vto = new VehicleTO();
            modelList = vehicleBP.processGetModelList(vto);
            locationList = companyBP.processGetCompanyList();
            request.setAttribute("MfrLists", mfrList);
            request.setAttribute("ModelLists", modelList);
            request.setAttribute("LocationLists", locationList);
            request.setAttribute("categoryList", categoryList);
            request.setAttribute("categoryId", reportCommand.getCategoryId());
            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("mfrId", reportCommand.getMfrId());
            request.setAttribute("modelId", reportCommand.getModelId());
            request.setAttribute("mfrCode", reportCommand.getMfrCode());
            request.setAttribute("paplCode", reportCommand.getPaplCode());
            request.setAttribute("sectionId", reportCommand.getSectionId());
            request.setAttribute("itemName", reportCommand.getItemName());
            ReportTO reportTO = new ReportTO();
            reportTO.setCompanyId(reportCommand.getCompanyId());
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());
            if (Integer.parseInt(reportCommand.getCategoryId()) != 0) {
                reportTO.setCategoryId(reportCommand.getCategoryId());
            }
            if (Integer.parseInt(reportCommand.getMfrId()) != 0) {
                reportTO.setMfrId(reportCommand.getMfrId());
            }
            if (Integer.parseInt(reportCommand.getModelId()) != 0) {
                reportTO.setModelId(reportCommand.getModelId());
            }
            System.out.println("categoryid" + reportTO.getCategoryId());
            reportTO.setItemName(reportCommand.getItemName());
            reportTO.setMfrCode(reportCommand.getMfrCode());
            reportTO.setPaplCode(reportCommand.getPaplCode());
            storesEffList = reportBP.processStoresEffReport(reportTO);
            request.setAttribute("storesEffList", storesEffList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleReceivedStockPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList categoryList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList vendorList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Stores >>  Received Stock Report";

        try {
            path = "content/report/receivedStockRpt.jsp";
            locationList = companyBP.processGetCompanyList();
            vendorList = vendorBP.processGetVendorList();
            request.setAttribute("LocationLists", locationList);
            request.setAttribute("VendorLists", vendorList);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleReceivedStockReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();

        ArrayList locationList = new ArrayList();
        ArrayList vendorList = new ArrayList();
        ArrayList receivedList = new ArrayList();
        ArrayList taxSummary = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Stores >>  Received Stock Report";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/receivedStockRpt.jsp";

            locationList = companyBP.processGetCompanyList();
            vendorList = vendorBP.processGetVendorList();

            request.setAttribute("LocationLists", locationList);
            request.setAttribute("VendorLists", vendorList);
            request.setAttribute("vendorId", reportCommand.getVendorId());
            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("poId", reportCommand.getPoId());
            request.setAttribute("inVoiceId", reportCommand.getInVoiceId());
            request.setAttribute("billType", reportCommand.getBillType());
            ReportTO reportTO = new ReportTO();
            reportTO.setCompanyId(reportCommand.getCompanyId());
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());
            System.out.println("vendor id recently=" + reportCommand.getVendorId());
            if (Integer.parseInt(reportCommand.getVendorId()) != 0) {
                reportTO.setVendorId(reportCommand.getVendorId());
            } else {
                reportTO.setVendorId("");
            }
            reportTO.setInvoiceId(reportCommand.getInVoiceId());
            reportTO.setPoId(reportCommand.getPoId());
            System.out.println("invoiceid" + reportCommand.getInVoiceId());
            reportTO.setItemName(reportCommand.getItemName());
            reportTO.setMfrCode(reportCommand.getMfrCode());
            reportTO.setPaplCode(reportCommand.getPaplCode());
            reportTO.setBillType(reportCommand.getBillType());
            receivedList = reportBP.processReceivedStockReport(reportTO);
            request.setAttribute("receivedList", receivedList);
            taxSummary = reportBP.processReceivedStockTaxSummary(reportTO);
            request.setAttribute("taxSummary", taxSummary);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleInVoiceReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList inVoiceItems = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Stores >> InVoice Report";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/viewGRN.jsp";
            ReportTO reportTO = new ReportTO();
            reportTO.setSupplyId(request.getParameter("supplyId"));
            inVoiceItems = reportBP.processInVoiceItems(reportTO);
            request.setAttribute("InVoiceItems", inVoiceItems);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleServiceSummaryPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList serviceTypes = new ArrayList();
        ArrayList operationPointList = new ArrayList();
        ArrayList billList = new ArrayList();
        ArrayList taxSummary = new ArrayList();

        String menuPath = "Reports >> Render Service >> Service Summary";
        path = "content/report/serviceSummary.jsp";
        try {
            String fromDate = (String) session.getAttribute("currentDate");
            String toDate = (String) session.getAttribute("currentDate");
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            String pageTitle = "Service Summary";
            ReportTO repTO = new ReportTO();
            repTO.setCompanyName("0");
            repTO.setServiceTypeId("0");

            ArrayList usageList = new ArrayList();
            usageList = vehicleBP.processGetUsageList();
            request.setAttribute("UsageList", usageList);

            ArrayList customerList = new ArrayList();
            customerList = customerBP.processActiveCustomerList();
            request.setAttribute("customerList", customerList);

            serviceTypes = operationBP.getServiceTypes();
            operationPointList = vehicleBP.processGetopList();

            request.setAttribute("pageTitle", pageTitle);
            request.setAttribute("serviceTypes", serviceTypes);
            request.setAttribute("operationPointList", operationPointList);

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            billList = reportBP.processBillList(repTO, fromDate, toDate);
            taxSummary = reportBP.processSalesTaxSummary(repTO, fromDate, toDate);
            request.setAttribute("taxSummary", taxSummary);
            request.setAttribute("billList", billList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleServiceSummary(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList serviceTypes = new ArrayList();
        ArrayList operationPointList = new ArrayList();
        ArrayList serviceList = new ArrayList();

        String menuPath = "Reports >> Render Service >> Service Summary";
        path = "content/report/serviceSummary.jsp";
        try {
            String fromDate = (String) request.getParameter("fromDate");
            String toDate = (String) request.getParameter("toDate");
            String usageTypeId = (String) request.getParameter("usageId");
            String serviceType = (String) request.getParameter("serviceType");
            String custId = (String) request.getParameter("custId");
            String opId = (String) request.getParameter("opId");

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("usageTypeId", usageTypeId);
            request.setAttribute("serviceType", serviceType);
            request.setAttribute("custId", custId);
            request.setAttribute("opId", opId);

            String pageTitle = "Service Summary";

            System.out.println("fromDate=" + fromDate);
            System.out.println("toDate=" + toDate);
            System.out.println("usageTypeId=" + usageTypeId);
            System.out.println("serviceType=" + serviceType);
            System.out.println("custId=" + custId);
            System.out.println("opId=" + opId);

            ArrayList usageList = new ArrayList();
            usageList = vehicleBP.processGetUsageList();
            request.setAttribute("UsageList", usageList);

            ArrayList customerList = new ArrayList();
            customerList = customerBP.processActiveCustomerList();
            request.setAttribute("customerList", customerList);

            serviceTypes = operationBP.getServiceTypes();
            operationPointList = vehicleBP.processGetopList();

            request.setAttribute("pageTitle", pageTitle);
            request.setAttribute("serviceTypes", serviceTypes);
            request.setAttribute("operationPointList", operationPointList);

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            serviceList = reportBP.processServiceSummary(usageTypeId, serviceType, custId, opId, fromDate, toDate);
            request.setAttribute("serviceList", serviceList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleStockMovingAvgPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList categoryList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList vendorList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Stores >>  Moving Average Report";
        try {
            path = "content/report/movingAve.jsp";
            categoryList = sectionBP.processGetCategoryList();
            mfrList = vehicleBP.processGetMfrList();
            VehicleTO vto = new VehicleTO();
            modelList = vehicleBP.processGetModelList(vto);
            locationList = companyBP.processGetCompanyList();
            System.out.println("categoryList" + categoryList.size());
            request.setAttribute("categoryList", categoryList);
            request.setAttribute("MfrLists", mfrList);
            request.setAttribute("ModelLists", modelList);
            request.setAttribute("LocationLists", locationList);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleMovingAvgReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList categoryList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList stokIssueList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Stores >> Moving Average Report";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/movingAve.jsp";
            categoryList = sectionBP.processGetCategoryList();
            mfrList = vehicleBP.processGetMfrList();
            VehicleTO vto = new VehicleTO();
            modelList = vehicleBP.processGetModelList(vto);
            locationList = companyBP.processGetCompanyList();
            request.setAttribute("MfrLists", mfrList);
            request.setAttribute("ModelLists", modelList);
            request.setAttribute("LocationLists", locationList);
            request.setAttribute("categoryList", categoryList);
            request.setAttribute("categoryId", reportCommand.getCategoryId());
            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("mfrId", reportCommand.getMfrId());
            request.setAttribute("modelId", reportCommand.getModelId());
            request.setAttribute("mfrCode", reportCommand.getMfrCode());
            request.setAttribute("paplCode", reportCommand.getPaplCode());
            request.setAttribute("sectionId", reportCommand.getSectionId());
            request.setAttribute("itemName", reportCommand.getItemName());
            ReportTO reportTO = new ReportTO();
            reportTO.setCompanyId(reportCommand.getCompanyId());
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());
            if (Integer.parseInt(reportCommand.getCategoryId()) != 0) {
                reportTO.setCategoryId(reportCommand.getCategoryId());
            }
            if (Integer.parseInt(reportCommand.getMfrId()) != 0) {
                reportTO.setMfrId(reportCommand.getMfrId());
            }
            if (Integer.parseInt(reportCommand.getModelId()) != 0) {
                reportTO.setModelId(reportCommand.getModelId());
            }
            System.out.println("categoryid" + reportTO.getCategoryId());
            reportTO.setItemName(reportCommand.getItemName());
            reportTO.setMfrCode(reportCommand.getMfrCode());
            reportTO.setPaplCode(reportCommand.getPaplCode());
            stokIssueList = reportBP.processMovingAvgReport(reportTO);
            request.setAttribute("stokIssueList", stokIssueList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleServiceEffPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList categoryList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList vendorList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Render Service>>  Service Efficiency Report";
        try {
            path = "content/report/serEff.jsp";
            mfrList = vehicleBP.processGetMfrList();
            VehicleTO vto = new VehicleTO();
            modelList = vehicleBP.processGetModelList(vto);
            locationList = companyBP.processGetCompanyList();
            request.setAttribute("categoryList", categoryList);
            request.setAttribute("MfrLists", mfrList);
            request.setAttribute("ModelLists", modelList);
            request.setAttribute("LocationLists", locationList);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleServiceEffReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList mfrList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList serviceEffList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Render Service>>  Service Efficiency Report";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/serEff.jsp";

            mfrList = vehicleBP.processGetMfrList();
            VehicleTO vto = new VehicleTO();
            modelList = vehicleBP.processGetModelList(vto);
            locationList = companyBP.processGetCompanyList();
            request.setAttribute("MfrLists", mfrList);
            request.setAttribute("ModelLists", modelList);
            request.setAttribute("LocationLists", locationList);
            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("mfrId", reportCommand.getMfrId());
            request.setAttribute("modelId", reportCommand.getModelId());
            request.setAttribute("woId", reportCommand.getWoId());
            request.setAttribute("jobCardId", reportCommand.getJobCardId());
            request.setAttribute("regNo", reportCommand.getRegNo());
            ReportTO reportTO = new ReportTO();
            reportTO.setCompanyId(reportCommand.getCompanyId());
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());
            if (Integer.parseInt(reportCommand.getMfrId()) != 0) {
                reportTO.setMfrId(reportCommand.getMfrId());
            }
            if (Integer.parseInt(reportCommand.getModelId()) != 0) {
                reportTO.setModelId(reportCommand.getModelId());
            }
            reportTO.setJobCardId(reportCommand.getJobCardId());
            reportTO.setWoId(reportCommand.getWoId());
            reportTO.setRegNo(reportCommand.getRegNo());
            reportTO.setMfrCode(reportCommand.getMfrCode());
            serviceEffList = reportBP.processSerEffReport(reportTO);
            request.setAttribute("serviceEffList", serviceEffList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleStList(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList stReqList = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList spList = new ArrayList();
        StockTransferTO repTO = null;
        CompanyTO reportTO = null;
        reportCommand = command;
        String path = "";
        String fromDate, toDate;
        String menuPath = "Reports >> Stores >> Stock Transfer Report";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/stReqList.jsp";
            if (!reportCommand.getFromDate().equals("")) {
                fromDate = reportCommand.getFromDate();
                toDate = reportCommand.getToDate();
            } else {
                fromDate = (String) session.getAttribute("currentDate");
                toDate = (String) session.getAttribute("currentDate");
            }
            locationList = companyBP.processGetCompanyList();
            int fromSpId = Integer.parseInt(reportCommand.getFromId());
            int toSpId = Integer.parseInt(reportCommand.getToId());
            String type = reportCommand.getType();
            spList = vehicleBP.processGetspList();

            request.setAttribute("fromId", reportCommand.getFromId());
            request.setAttribute("toId", reportCommand.getToId());
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("type", type);
            request.setAttribute("operationPointList", spList);

            stReqList = reportBP.processStRequestList(fromDate, toDate, fromSpId, toSpId, type);

            //summary list
            float tot = 0.0f;
            Iterator itr = spList.iterator();
            while (itr.hasNext()) {
                reportTO = new CompanyTO();
                reportTO = (CompanyTO) itr.next();
                tot = 0.0f;
                Iterator itr1 = stReqList.iterator();
                while (itr1.hasNext()) {
                    repTO = new StockTransferTO();
                    repTO = (StockTransferTO) itr1.next();
                    if (reportTO.getSpId().equalsIgnoreCase(repTO.getToSpId())) {
                        tot = tot + Float.parseFloat(repTO.getAmount());
                    }
                }
                reportTO.setAmount(tot);
            }
            request.setAttribute("operationPointList", spList);
            request.setAttribute("stReqList", stReqList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleStDetail(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList stReqDetail = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList spList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> Stores >> Stock Transfer Details";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/gdDisplay.jsp";

            locationList = companyBP.processGetCompanyList();
            spList = vehicleBP.processGetspList();
            request.setAttribute("operationPointList", spList);

            stReqDetail = reportBP.processStDetail(reportCommand.getReqId());
            request.setAttribute("stkTransferDetail", stReqDetail);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleGdDetail(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String gdId = request.getParameter("gdId");
        System.out.println("Gd Id-->" + gdId);
        ArrayList stReqDetail = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList spList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> Stores >> Stock Transfer Details";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/gdDisplay.jsp";

            locationList = companyBP.processGetCompanyList();
            spList = vehicleBP.processGetspList();
            request.setAttribute("operationPointList", spList);

            stReqDetail = reportBP.processGdDetail(gdId);
            request.setAttribute("stkTransferDetail", stReqDetail);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleRcBillList(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList billList = new ArrayList();
        ArrayList vendorList = new ArrayList();
        ArrayList billTaxSummary = new ArrayList();
        reportCommand = command;
        String path = "";
        String fromDate, toDate;
        String menuPath = "Reports >> Stores >> RC Bills";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/rcBillList.jsp";
            if (!reportCommand.getFromDate().equals("")) {
                fromDate = reportCommand.getFromDate();
                toDate = reportCommand.getToDate();
            } else {
                fromDate = (String) session.getAttribute("currentDate");
                toDate = (String) session.getAttribute("currentDate");
            }

            vendorList = vendorBP.processGetVendorList();
            request.setAttribute("vendorList", vendorList);
            request.setAttribute("vendorId", reportCommand.getVendorId());
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            billList = reportBP.processRcBillList(reportCommand.getVendorId(), fromDate, toDate);
            billTaxSummary = reportBP.processRcBillTaxSummary(reportCommand.getVendorId(), fromDate, toDate);
            request.setAttribute("billList", billList);
            request.setAttribute("billTaxSummary", billTaxSummary);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleRcBillDetail(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList billDetail = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> Stores >> RC Bill Detail";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/rcBillDetail.jsp";
            billDetail = reportBP.processRcBillDetail(request.getParameter("billNo"));
            request.setAttribute("billDetail", billDetail);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVehicleServiceCostPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList serviceTypes = new ArrayList();
        ArrayList operationPointList = new ArrayList();
        ArrayList servicePointList = new ArrayList();
        ArrayList billList = new ArrayList();

        String menuPath = "Reports >>Vehicle >> Vehicle Service Analysis";
        path = "content/report/vehicleServiceCost.jsp";
        try {
            String fromDate = (String) session.getAttribute("currentDate");
            String toDate = (String) session.getAttribute("currentDate");
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            ReportTO repTO = new ReportTO();
            repTO.setCompanyName("0");
            repTO.setServiceTypeId("0");

            ArrayList usageList = new ArrayList();
            ArrayList mfrList = new ArrayList();
            ArrayList modelList = new ArrayList();
            ArrayList customerList = new ArrayList();
//            mfrList = vehicleBP.processGetMfrList();
//            modelList = vehicleBP.processGetModelList();
            customerList = customerBP.processActiveCustomerList();
            usageList = vehicleBP.processGetUsageList();
            operationPointList = vehicleBP.processGetopList();
            servicePointList = vehicleBP.processGetspList();
            request.setAttribute("customerList", customerList);
            request.setAttribute("UsageList", usageList);
            request.setAttribute("servicePointList", servicePointList);
//            request.setAttribute("MfrLists", mfrList);
//            request.setAttribute("ModelLists", modelList);
            request.setAttribute("operationPointList", operationPointList);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVehicleServiceCostReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        System.out.println("handleVehicleServiceCostReportn ccndcd");
        HttpSession session = request.getSession();
        String path = "";
        ArrayList serviceTypes = new ArrayList();
        ArrayList operationPointList = new ArrayList();
        ArrayList billList = new ArrayList();

        String menuPath = "Reports >>Vehicle >> Vehicle Service Analysis";
        path = "content/report/vehicleServiceCost.jsp";
        try {
            reportCommand = command;
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ReportTO repTO = new ReportTO();
            ArrayList usageList = new ArrayList();
            ArrayList serviceCostList = new ArrayList();
            ArrayList servicePointList = new ArrayList();
            ArrayList customerList = new ArrayList();

            usageList = vehicleBP.processGetUsageList();
            request.setAttribute("UsageList", usageList);
            operationPointList = vehicleBP.processGetopList();
            servicePointList = vehicleBP.processGetspList();
            request.setAttribute("usageList", usageList);
            request.setAttribute("operationPointList", operationPointList);
            request.setAttribute("servicePointList", servicePointList);
            // repTO = new ReportTO();
            repTO.setRegNo(reportCommand.getRegNo());
            System.out.println("regno" + repTO.getRegNo());
            repTO.setCompanyId(reportCommand.getCompanyId());
            repTO.setFromDate(reportCommand.getFromDate());
            repTO.setToDate(reportCommand.getToDate());
            repTO.setUsageType(reportCommand.getUsageType());
            repTO.setCustId(reportCommand.getCustId());
            repTO.setServicePointId(reportCommand.getSpId());

            customerList = customerBP.processActiveCustomerList();
            usageList = vehicleBP.processGetUsageList();
            operationPointList = vehicleBP.processGetopList();

            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("usageType", reportCommand.getUsageType());
            request.setAttribute("custId", reportCommand.getCustId());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("regNo", reportCommand.getRegNo());
            request.setAttribute("spId", reportCommand.getSpId());
            System.out.println("Service Cost List B4-->");
            serviceCostList = reportBP.processServiceCostList(repTO);
            System.out.println("Service Cost List-->" + serviceCostList.size());
            request.setAttribute("serviceCostList", serviceCostList);

            request.setAttribute("customerList", customerList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Service Cost List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleServiceBillDetails(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList serviceTypes = new ArrayList();
        ArrayList operationPointList = new ArrayList();
        String menuPath = "Report >>  ServiceCost Bill";
        path = "content/report/vehicleServiceCostBill.jsp";
        try {
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");

            System.out.println("regNo command=" + reportCommand.getRegNo());
            ReportTO repTO = new ReportTO();
            repTO.setRegNo(reportCommand.getRegNo());

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("regNo", reportCommand.getRegNo());
            request.setAttribute("companyId", reportCommand.getCompanyId());

            ArrayList billList = new ArrayList();
            ArrayList taxSummary = new ArrayList();
            billList = reportBP.processBillList(repTO, fromDate, toDate);
            taxSummary = reportBP.processSalesTaxSummary(repTO, fromDate, toDate);
            request.setAttribute("taxSummary", taxSummary);
            request.setAttribute("billList", billList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Bill data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleWOListPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/LoginScreen.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList locationList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Operation Point>>  work Order Report";
        try {
            path = "content/report/workOrderReport.jsp";
            locationList = companyBP.processGetCompanyList();
            request.setAttribute("LocationLists", locationList);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleWOList(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/LoginScreen.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList locationList = new ArrayList();
        ArrayList woList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Operation Point>>  work Order Report";
        try {
            path = "content/report/workOrderReport.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            locationList = companyBP.processGetCompanyList();
            request.setAttribute("LocationLists", locationList);
            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            ReportTO reportTO = new ReportTO();
            reportTO.setCompanyId(reportCommand.getCompanyId());
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());
            woList = reportBP.processWoList(reportTO);
            request.setAttribute("woList", woList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleServiceGraphData(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/LoginScreen.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList serviceSummary = new ArrayList();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        int MILLIS_IN_DAY = 15 * (1000 * 60 * 60 * 24);

        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> Management Report";
        try {
            path = "charts.jsp";
            String fromDate = "";
            String toDate = "";
            if (reportCommand.getFromDate() == "") {
                toDate = (String) session.getAttribute("currentDate");
                System.out.println("MILLIS_IN_DAY=" + MILLIS_IN_DAY);
                fromDate = dateFormat.format(dateFormat.parse(toDate).getTime() - MILLIS_IN_DAY);
                fromDate = dateFormat.format(dateFormat.parse(fromDate).getTime() - MILLIS_IN_DAY);
            } else {
                fromDate = reportCommand.getFromDate();
                toDate = reportCommand.getToDate();
            }
            System.out.println("fromDate=" + fromDate);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            serviceSummary = reportBP.processServiceChartData(fromDate, toDate);
            System.out.println("serviceSummary  in controler=" + serviceSummary.size());
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("serviceSummary", serviceSummary);
            request.setAttribute("summ", "Service Summary From " + fromDate + " to " + toDate);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleServiceDailyMIS(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/LoginScreen.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList serviceSummary = new ArrayList();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        int MILLIS_IN_DAY = 15 * (1000 * 60 * 60 * 24);

        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> Management Report";
        try {
            path = "/content/report/serviceDailyMIS.jsp";
            String fromDate = "";
            String toDate = "";
            if (reportCommand.getFromDate() == "") {
                toDate = (String) session.getAttribute("currentDate");
                System.out.println("MILLIS_IN_DAY=" + MILLIS_IN_DAY);
                fromDate = dateFormat.format(dateFormat.parse(toDate).getTime() - MILLIS_IN_DAY);
                fromDate = dateFormat.format(dateFormat.parse(fromDate).getTime() - MILLIS_IN_DAY);
            } else {
                fromDate = reportCommand.getFromDate();
                toDate = reportCommand.getToDate();
            }
            System.out.println("fromDate=" + fromDate);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            serviceSummary = reportBP.handleServiceDailyMIS(fromDate, toDate);
            System.out.println("serviceSummary  in controler=" + serviceSummary.size());
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("serviceSummary", serviceSummary);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleReqItemsPage(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        purchaseCommand = command;
        String menuPath = "Reports >> Required Items ";
        String path = "/content/report/requiredItems.jsp";
        HttpSession session = request.getSession();
        int servicePtId = Integer.parseInt((String) session.getAttribute("companyId"));

        String companyId = ((String) session.getAttribute("companyId"));
        int compId = Integer.parseInt(companyId);
        System.out.println("companyID=" + companyId);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList vendorItemList = new ArrayList();
        ArrayList requiredItemsList = new ArrayList();
        ArrayList jobCardList = new ArrayList();
        try {

            path = "content/report/requiredItems.jsp";
            PurchaseTO compTO = new PurchaseTO();
            compTO.setCompanyId(companyId);

            ArrayList categoryList = new ArrayList();
            categoryList = sectionBP.processGetCategoryList();
            //jobCardList = mrsBP.getJobCardList(servicePtId);
            System.out.println("jobCardList=" + jobCardList.size());
            request.setAttribute("jobCardlist", jobCardList);
            request.setAttribute("CategoryList", categoryList);

            //requiredItemsList = purchaseBP.processGetRequiredItems(compTO,compId);
            //request.setAttribute("requiredItemsList",requiredItemsList);
//            vendorItemList = purchaseBP.processVendorItems(1011);
            // vendorItemList = purchaseBP.processVendorMfrCat(1011);
            //System.out.println("requiredItemsList=" + requiredItemsList.size());
            // System.out.println("vendorItemList=" + vendorItemList.size());
            // request.setAttribute("vendorItemList", vendorItemList);
            int pageNo = 0;
            int totalPages = 0;
            int startIndex = 0;
            int endIndex = 0;
            int totalRecords = 0;
            //requiredItemsList = purchaseBP.processGetRequiredItems(compTO, compId, 0, 0);
            totalRecords = requiredItemsList.size();    //call your method
            PaginationHelper pagenation = new PaginationHelper();
            pagenation.setTotalRecords(totalRecords);

            String buttonClicked = "";
            session.setAttribute("totalRecords", totalRecords);
////////////////////////

            pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
            totalPages = pagenation.getTotalNoOfPages();
            request.setAttribute("pageNo", pageNo);
            System.out.println("pageNo" + pageNo);
            request.setAttribute("totalPages", totalPages);
            startIndex = pagenation.getStartIndex();
            endIndex = pagenation.getEndIndex();

            //requiredItemsList = purchaseBP.processGetRequiredItems(compTO, compId, startIndex, endIndex);
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception1) {
            FPLogUtils.fpErrorLog("Business exception --> " + exception1.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY, exception1.getErrorMessage());
        } catch (Exception exception2) {
            exception2.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to handleGenerateMpr --> " + exception2);
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleReqItemsReport(HttpServletRequest request, HttpServletResponse response, PurchaseCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        purchaseCommand = command;
        String menuPath = "Stores >> Required Items ";
        String pageTitle = "Required Items";
        request.setAttribute("pageTitle", pageTitle);
        String path = "";
        HttpSession session = request.getSession();
        int servicePtId = Integer.parseInt((String) session.getAttribute("companyId"));

        String companyId = ((String) session.getAttribute("companyId"));
        int compId = Integer.parseInt(companyId);
        System.out.println("companyID=" + companyId);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList vendorItemList = new ArrayList();
        ArrayList requiredItemsList = new ArrayList();
        ArrayList recordsWithLimit = new ArrayList();
        ArrayList jobCardlist = new ArrayList();

        try {

            int pageNo = 0;
            int totalPages = 0;
            int startIndex = 0;
            int endIndex = 0;
            int totalRecords = 0;
            path = "content/report/requiredItems.jsp";
            PurchaseTO compTO = new PurchaseTO();
            compTO.setCompanyId(companyId);

            compTO.setMfrCode(purchaseCommand.getMfrCode());
            compTO.setPaplCode(purchaseCommand.getPaplCode());
            compTO.setCategoryId(purchaseCommand.getCategoryId());
            compTO.setSearchAll(purchaseCommand.getSearchAll());

            request.setAttribute("mfrCode", compTO.getMfrCode());
            request.setAttribute("paplCode", compTO.getPaplCode());
            request.setAttribute("categoryId", compTO.getCategoryId());
            request.setAttribute("searchAll", compTO.getSearchAll());

            requiredItemsList = purchaseBP.processTotalGetRequiredItems(compTO, compId, 0, 0);
            totalRecords = requiredItemsList.size();    //call your method
            PaginationHelper pagenation = new PaginationHelper();
            pagenation.setTotalRecords(totalRecords);

            String buttonClicked = "";

            if (request.getParameter("button") != null) {
                buttonClicked = request.getParameter("button");
            }
            pageNo = Integer.parseInt(request.getParameter("pageNo"));
            session.setAttribute("totalRecords", totalRecords);
            pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
            totalPages = pagenation.getTotalNoOfPages();
            request.setAttribute("pageNo", pageNo);
            request.setAttribute("totalPages", totalPages);
            startIndex = pagenation.getStartIndex();
            endIndex = pagenation.getEndIndex();

            requiredItemsList = purchaseBP.processGetRequiredItems(compTO, compId, startIndex, endIndex);

            ArrayList categoryList = new ArrayList();
            categoryList = sectionBP.processGetCategoryList();
            request.setAttribute("CategoryList", categoryList);

            request.setAttribute("requiredItemsList", requiredItemsList);
            System.out.println("requiredItemsList size=" + requiredItemsList.size());

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception1) {
            FPLogUtils.fpErrorLog("Business exception --> " + exception1.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY, exception1.getErrorMessage());
        } catch (Exception exception2) {
            exception2.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to handleGenerateMpr --> " + exception2);
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleStockAvbPage(HttpServletRequest request, HttpServletResponse response, SectionCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        sectionCommand = command;
        String path = "";
        String menuPath = "Reports >> Stores  >>  Stock Availability";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        HttpSession session = request.getSession();
        try {

            path = "content/report/stockAvailabilityReport.jsp";

            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);

            ArrayList sectionList = new ArrayList();
            sectionList = sectionBP.processGetSectionList();
            request.setAttribute("SectionList", sectionList);
            request.removeAttribute("PartsList");

            ArrayList categoryList = new ArrayList();
            categoryList = sectionBP.processGetCategoryList();
            request.setAttribute("CategoryList", categoryList);

            int pageNo = 0;
            int totalPages = 0;
            int startIndex = 0;
            int endIndex = 0;

            int totalRecords = 0;
            PaginationHelper pagenation = new PaginationHelper();

            pagenation.setTotalRecords(totalRecords);
            String buttonClicked = "";
            if (request.getParameter("button") != null) {
                buttonClicked = request.getParameter("button");
            }

            pagenation.setTotalRecords(totalRecords);
            startIndex = pagenation.getStartIndex();
            endIndex = pagenation.getEndIndex();
            pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
            totalPages = pagenation.getTotalNoOfPages();
            request.setAttribute("pageNo", pageNo);
            request.setAttribute("totalPages", totalPages);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }
    //handleSearch

    /**
     * This method caters to search Models
     *
     * @param request - Http request object
     *
     * @param response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public ModelAndView handleStockAvbRep(HttpServletRequest request, HttpServletResponse response, SectionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "Report   >>  Stores >>  Stock Availability";
        sectionCommand = command;
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String mfrId = request.getParameter("mfrId");
        try {

            path = "content/report/stockAvailabilityReport.jsp";

            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);

            ArrayList categoryList = new ArrayList();
            categoryList = sectionBP.processGetCategoryList();
            request.setAttribute("CategoryList", categoryList);

            ArrayList sectionList = new ArrayList();
            sectionList = sectionBP.processGetSectionList();
            request.setAttribute("SectionList", sectionList);

            ArrayList ModelList = new ArrayList();
            ModelList = vehicleBP.processActiveMfrList();
            request.setAttribute("ModelList", ModelList);

            SectionTO sectionTO = new SectionTO();
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList partsList = new ArrayList();

            String mrfId = sectionCommand.getMfrId();
            String modelId = sectionCommand.getModelId();
            String itemCode = sectionCommand.getItemCode();
            String paplCode = sectionCommand.getPaplCode();
            String itemName = sectionCommand.getItemName();
            String reConditionable = sectionCommand.getReConditionable();
            String categoryId = sectionCommand.getCategoryId();

            System.out.println("sectionCommand.getItemCode()" + sectionCommand.getItemCode());
            System.out.println("sectionCommand.getPaplCode()" + sectionCommand.getPaplCode());

            if (sectionCommand.getMfrId().equals("0")) {
                sectionTO.setMfrId(null);
            } else {
                sectionTO.setMfrId(mrfId);
            }
            if (sectionCommand.getModelId().equals("0")) {

                sectionTO.setModelId(null);
            } else {

                sectionTO.setModelId(modelId);
            }

            if (sectionCommand.getItemCode() == "") {
                sectionTO.setItemCode(null);
            } else {
                sectionTO.setItemCode(itemCode);
            }
            if (sectionCommand.getPaplCode() == "") {

                sectionTO.setPaplCode(null);
            } else {

                sectionTO.setPaplCode(paplCode);
            }
            if (sectionCommand.getItemName() == "") {

                sectionTO.setItemName(null);
            } else {

                sectionTO.setItemName(itemName);
            }
            if (sectionCommand.getReConditionable() == "") {

                sectionTO.setReConditionable(null);
            } else {

                sectionTO.setReConditionable(reConditionable);
            }
            if (sectionCommand.getCategoryId().equals("0")) {
                sectionTO.setCategoryId(null);
            } else {
                sectionTO.setCategoryId(categoryId);
            }
            sectionTO.setSectionId(null);

            System.out.println("sectionCommand.getPaplCode()" + sectionTO.getPaplCode());
            System.out.println("sectionCommand.getItemCode()" + sectionTO.getItemCode());

            request.setAttribute("sectionId", sectionCommand.getSectionId());
            request.setAttribute("itemCode", sectionCommand.getItemCode());
            request.setAttribute("paplCode", sectionCommand.getPaplCode());
            request.setAttribute("itemName", sectionCommand.getItemName());
            request.setAttribute("mfrId", sectionCommand.getMfrId());
            request.setAttribute("modelId", sectionCommand.getModelId());
            request.setAttribute("reConditionable", sectionCommand.getReConditionable());
            request.setAttribute("categoryId", sectionCommand.getCategoryId());

            //pagenation start
//variables
            int totalRecords = 0;
            int pageNo = 0;
            int totalPages = 0;

            ArrayList recordsWithLimit = new ArrayList();

//to get totalRecords
            totalRecords = sectionBP.getTotalParts(sectionTO);    //call your method

            session.setAttribute("totalRecords", totalRecords);

            PaginationHelper pagenation = new PaginationHelper();
            pagenation.setTotalRecords(totalRecords);
            String buttonClicked = "";
            if (request.getParameter("button") != null) {
                buttonClicked = request.getParameter("button");
            }

            pageNo = Integer.parseInt(request.getParameter("pageNo"));

            pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
            totalPages = pagenation.getTotalNoOfPages();
            int startIndex = pagenation.getStartIndex();
            int endIndex = pagenation.getEndIndex();

            //to get  records with limits
            //  partsList = sectionBP.getpartsDetails(sectionTO);
            //  request.setAttribute("PartsList", partsList);
            recordsWithLimit = sectionBP.getpartsDetails(startIndex, endIndex, sectionTO, companyId);//call your method
            if (recordsWithLimit.size() != 0) {
                request.setAttribute("IndexedPartsDetail", recordsWithLimit);
            } else {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");
            }
            request.setAttribute("totalPages", totalPages);
            request.setAttribute("pageNo", pageNo);
            //pagenation end

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    public ModelAndView handleFreeServiceDuePage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        ArrayList operationPointList = new ArrayList();
        ArrayList serviceList = new ArrayList();

        String menuPath = "Reports >>  Operation Point >> Warranty Service Dues";
        path = "content/report/FreeServiceScheduleRpt.jsp";
        try {

            ReportTO repTO = new ReportTO();
            repTO.setCompanyId("0");
            operationPointList = vehicleBP.processGetopList();
            request.setAttribute("operationPointList", operationPointList);
            request.setAttribute("companyId", repTO.getCompanyId());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleFreeServiceDueRpt(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ArrayList operationPointList = new ArrayList();
        ArrayList serviceList = new ArrayList();
        reportCommand = command;
        String menuPath = "Reports >>  Operation Point >> Warranty Service Dues";
        path = "content/report/FreeServiceScheduleRpt.jsp";
        try {

            ReportTO repTO = new ReportTO();
            repTO.setCompanyId(reportCommand.getCompanyId());
            repTO.setRegNo(reportCommand.getRegNo());
            repTO.setFromDate(reportCommand.getFromDate());
            repTO.setToDate(reportCommand.getToDate());
            repTO.setStatus(reportCommand.getStatus());
            operationPointList = vehicleBP.processGetopList();
            request.setAttribute("operationPointList", operationPointList);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            request.setAttribute("status", reportCommand.getStatus());
            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("regNo", reportCommand.getRegNo());
            serviceList = reportBP.processWarrantyServiceList(repTO);
            request.setAttribute("serviceList", serviceList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleRCItemListPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        ArrayList categoryList = new ArrayList();
        ArrayList locationList = new ArrayList();

        String menuPath = "Reports >> RC Parts >> RC Parts Report";
        path = "content/report/rcItem.jsp";
        try {

            ReportTO repTO = new ReportTO();
            categoryList = sectionBP.processGetCategoryList();
            locationList = companyBP.processGetCompanyList();
            request.setAttribute("categoryList", categoryList);
            request.setAttribute("locationList", locationList);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleRCItemRpt(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ArrayList locationList = new ArrayList();
        ArrayList categoryList = new ArrayList();
        ArrayList rcItemList = new ArrayList();
        reportCommand = command;
        String menuPath = "Reports >> RC Parts >> RC Parts Report";
        path = "content/report/rcItem.jsp";
        try {

            ReportTO repTO = new ReportTO();
            repTO.setCompanyId(reportCommand.getCompanyId());
            repTO.setPaplCode(reportCommand.getPaplCode());
            repTO.setItemName(reportCommand.getItemName());
            repTO.setRcId(reportCommand.getRcId());
            repTO.setFromDate(reportCommand.getFromDate());
            repTO.setToDate(reportCommand.getToDate());
            repTO.setCategoryId(reportCommand.getCategoryId());
            categoryList = sectionBP.processGetCategoryList();
            locationList = companyBP.processGetCompanyList();
            request.setAttribute("categoryList", categoryList);
            request.setAttribute("locationList", locationList);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            request.setAttribute("categoryId", reportCommand.getCategoryId());
            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("paplCode", reportCommand.getPaplCode());
            request.setAttribute("itemName", reportCommand.getItemName());
            request.setAttribute("rcId", reportCommand.getRcId());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            rcItemList = reportBP.processRcItemList(repTO);
            request.setAttribute("RcItemList", rcItemList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve rc Item data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleRCHistoryRpt(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ArrayList locationList = new ArrayList();
        ArrayList rcHistoryList = new ArrayList();
        reportCommand = command;
        String menuPath = "Reports >> RC Parts >> RC History Report";
        path = "content/report/viewRcHistory.jsp";
        try {

            ReportTO repTO = new ReportTO();

            repTO.setRcId(reportCommand.getRcId());
            repTO.setFromDate(reportCommand.getFromDate());
            repTO.setToDate(reportCommand.getToDate());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            rcHistoryList = reportBP.processRcHistoryList(repTO);
            request.setAttribute("RcHistoryList", rcHistoryList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve rc History List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleContractorActivityPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList vendorList = new ArrayList();
        ArrayList locationList = new ArrayList();
        reportCommand = command;
        int vendorType = 1014;
        String path = "";
        String menuPath = "Reports >>Vehicle >>  Contractors Activity";
        try {
            System.out.println("in cont");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/contractorActivities.jsp";
            locationList = companyBP.processGetCompanyList();
            vendorList = purchaseBP.processVendorList(vendorType);
            request.setAttribute("LocationLists", locationList);
            request.setAttribute("VendorLists", vendorList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleContractorActivityRpt(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList vendorList = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList contractorActList = new ArrayList();
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        int vendorType = 1014;
        String path = "";
        String menuPath = "Reports >>Vehicles >> Contractors Activity";
        try {
            System.out.println("in cont");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/contractorActivities.jsp";
            locationList = companyBP.processGetCompanyList();
            vendorList = purchaseBP.processVendorList(vendorType);
            request.setAttribute("LocationLists", locationList);
            request.setAttribute("VendorLists", vendorList);
            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("woId", reportCommand.getWoId());
            request.setAttribute("regNo", reportCommand.getRegNo());
            request.setAttribute("jobCardId", reportCommand.getJobCardId());
            request.setAttribute("vendorId", reportCommand.getVendorId());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            reportTO.setRegNo(reportCommand.getRegNo());
            reportTO.setCompanyId(reportCommand.getCompanyId());
            reportTO.setWoId(reportCommand.getWoId());
            reportTO.setJobCardId(reportCommand.getJobCardId());
            if (Integer.parseInt(reportCommand.getVendorId()) != 0) {
                reportTO.setVendorId(reportCommand.getVendorId());
            }
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());
            contractorActList = reportBP.processContractorActivities(reportTO);
            request.setAttribute("contractorActList", contractorActList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //Tyre Life Cycle Controller Start
    public ModelAndView handleTyrePoPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList mfrList = new ArrayList();
        ArrayList locationList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Tyres >>  Tyre Po-Wo Report";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/tyrePO-WO.jsp";

            //Remove Start - add new methoda for this
            locationList = companyBP.processGetCompanyList();
            mfrList = vehicleBP.processGetMfrList();
            System.out.println("mfrlist" + mfrList.size());
            request.setAttribute("LocationLists", locationList);
            request.setAttribute("MfrLists", mfrList);
            //Remove End - add new methoda for this

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleTyreLifeLoadData(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        ArrayList makeList = new ArrayList();

        String menuPath = "Reports >> Tyres >> Tyre Life Cycle Report";
        path = "content/report/tyreLifeCycle.jsp";
        try {

            ReportTO repTO = new ReportTO();
            makeList = companyBP.processGetMakeList();
            request.setAttribute("makeList", makeList);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleTyreLifeCyclePage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList mfrList = new ArrayList();
        ArrayList TyreLifeList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> Tyres >> Tyre Life Cycle Report";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/tyreLifeCycle.jsp";

            //Remove Start - add new methoda for this
            TyreLifeList = companyBP.processGetTyreLifeCycle();
            mfrList = vehicleBP.processGetMfrList();

            System.out.println("mfrlist" + mfrList.size());

            request.setAttribute("TyreLifeLists", TyreLifeList);
            request.setAttribute("MfrLists", mfrList);
            //Remove End - add new methoda for this
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    //Tyre Life Cycle Controller End

    public ModelAndView handleTyrePoRpt(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList mfrList = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList tyreList = new ArrayList();
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        String path = "";
        String menuPath = "Reports >>Vehicles >> Contractors Activity";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/tyrePO-WO.jsp";
            locationList = companyBP.processGetCompanyList();
            mfrList = vehicleBP.processGetMfrList();
            request.setAttribute("LocationLists", locationList);
            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("tyreNo", reportCommand.getTyreNo());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("status", reportCommand.getStatus());
            reportTO.setTyreNo(reportCommand.getTyreNo());
            reportTO.setCompanyId(reportCommand.getCompanyId());
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());
            reportTO.setStatus(reportCommand.getStatus());
            tyreList = reportBP.processTyreList(reportTO);
            request.setAttribute("TyreList", tyreList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleOrderListPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList categoryList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList ordersList = new ArrayList();
        ArrayList vendorList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> Stores >> PO/WO List";

        try {
            String fromDate;
            String toDate;
            ReportTO rep = new ReportTO();

            if (reportCommand.getFromDate().equals("")) {
                fromDate = (String) session.getAttribute("currentDate");
                toDate = (String) session.getAttribute("currentDate");
            } else {
                fromDate = reportCommand.getFromDate();
                toDate = reportCommand.getToDate();
            }

            path = "content/report/ordersList.jsp";
            locationList = companyBP.processGetCompanyList();
            vendorList = vendorBP.processGetVendorList();
            rep.setFromDate(fromDate);
            rep.setToDate(toDate);
            rep.setVendorId(reportCommand.getVendorId());
            rep.setPoId(reportCommand.getPoId());
            rep.setCompanyId(reportCommand.getCompanyId());
            if (!reportCommand.getOrderType().equals("")) {
                rep.setOrderType(reportCommand.getOrderType());
            }

            ordersList = reportBP.processOrdersList(rep);
            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("orderType", rep.getOrderType());
            request.setAttribute("toDate", toDate);
            request.setAttribute("LocationLists", locationList);
            request.setAttribute("VendorLists", vendorList);
            request.setAttribute("ordersList", ordersList);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleRCWOPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList RCWODetail = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Stores >> RC WO Report";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/rcWO.jsp";
            ReportTO reportTO = new ReportTO();
            reportTO.setSupplyId(request.getParameter("supplyId"));
            RCWODetail = reportBP.processRCWOItems(reportTO);
            request.setAttribute("RCWODetail", RCWODetail);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleStockPurchasePage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList categoryList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList modelList = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList vendorList = new ArrayList();
        ArrayList servicePointList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Stores >>  Received Stock Report";

        try {
            path = "content/report/stockPurchaseOutput.jsp";
            locationList = companyBP.processGetCompanyList();
            vendorList = vendorBP.processGetVendorList();
            servicePointList = vehicleBP.processGetspList();
            request.setAttribute("LocationLists", locationList);
            request.setAttribute("VendorLists", vendorList);
            request.setAttribute("servicePointList", servicePointList);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleStockPurchase(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList itemList = new ArrayList();
        ArrayList vendorList = new ArrayList();
        ArrayList servicePointList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Stores >> RC WO Report";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/stockPurchaseOutput.jsp";
            ReportTO reportTO = new ReportTO();
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());
            reportTO.setVendorId(reportCommand.getVendorId());
            reportTO.setBillType(reportCommand.getBillType());
            reportTO.setCompanyId(reportCommand.getCompanyId());
            reportTO.setPurpose(reportCommand.getPurpose());
            itemList = reportBP.processStockPurchase(reportTO);
            request.setAttribute("itemList", itemList);
            vendorList = vendorBP.processGetVendorList();
            servicePointList = vehicleBP.processGetspList();
            request.setAttribute("VendorLists", vendorList);
            request.setAttribute("vendorId", reportCommand.getVendorId());
            request.setAttribute("servicePointList", servicePointList);
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("billType", reportCommand.getBillType());
            request.setAttribute("companyId", reportCommand.getCompanyId());
            System.out.println("Finally come out");
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void handleStockPurchaseExcelRep(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        OutputStream out = null;
        HttpSession session = request.getSession();
        ArrayList itemList = new ArrayList();
        String path = "content/report/stockPurchaseOutput.jsp";

        try {
            ReportTO reportTO = new ReportTO();
            ArrayList vendorList = new ArrayList();

            String[] sno = request.getParameterValues("sno");
            String[] poId = request.getParameterValues("poId");
            String[] invoiceNo = request.getParameterValues("invoiceNo");
            String[] totalAmount = request.getParameterValues("totalAmount");
            String[] remarks = request.getParameterValues("remarks");
            String[] companyName = request.getParameterValues("companyName");
            String[] dcNo = request.getParameterValues("dcNo");
            String[] billDate = request.getParameterValues("billDate");
            String[] vendorName = request.getParameterValues("vendorName");
            String[] paplCode = request.getParameterValues("paplCode");
            String[] itemName = request.getParameterValues("itemName");
            String[] aqty = request.getParameterValues("aqty");
            String[] rqty = request.getParameterValues("rqty");
            String[] uom = request.getParameterValues("uom");
            String[] price = request.getParameterValues("price");
            String[] poDate = request.getParameterValues("poDate");

            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=purchaseReport.xls");
            WritableWorkbook w = Workbook.createWorkbook(response.getOutputStream());
            WritableSheet s = w.createSheet("Purchase", 0);
            s.addCell(new Label(0, 0, "SNO"));
            s.addCell(new Label(1, 0, "PO NO"));
            s.addCell(new Label(2, 0, "PO DATE"));
            s.addCell(new Label(3, 0, "INVOICE NO"));
            s.addCell(new Label(4, 0, "DC NO"));
            s.addCell(new Label(5, 0, "BILL DATE"));
            s.addCell(new Label(6, 0, "VENDOR NAME"));
            //s.addCell(new Label(7, 0,"COMPANY  NAME" ));
            s.addCell(new Label(7, 0, "ITEM NAME"));
            s.addCell(new Label(8, 0, "REQ QTY"));
            s.addCell(new Label(9, 0, "RECVD QTY"));
            s.addCell(new Label(10, 0, "PRICE"));

            s.addCell(new Label(11, 0, "TOTAL AMOUNT"));
            s.addCell(new Label(12, 0, "VEHICLE NO"));
            for (int i = 0; i < sno.length; i++) {
                s.addCell(new Label(0, i + 1, sno[i]));
                s.addCell(new Label(1, i + 1, poId[i]));
                s.addCell(new Label(2, i + 1, poDate[i]));
                s.addCell(new Label(3, i + 1, invoiceNo[i]));
                s.addCell(new Label(4, i + 1, dcNo[i]));
                s.addCell(new Label(5, i + 1, billDate[i]));
                s.addCell(new Label(6, i + 1, vendorName[i]));
                //s.addCell(new Label(7, i+1,companyName[i]));
                s.addCell(new Label(7, i + 1, itemName[i]));
                s.addCell(new Label(8, i + 1, rqty[i]));
                s.addCell(new Label(9, i + 1, aqty[i]));
                s.addCell(new Label(10, i + 1, price[i]));
                s.addCell(new Label(11, i + 1, totalAmount[i]));
                s.addCell(new Label(12, i + 1, remarks[i]));
            }
            w.write();
            w.close();

            //vendorList = vendorBP.processGetVendorList();
            //request.setAttribute("VendorLists", vendorList);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ModelAndView handleRcBillsReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList rcItemList = new ArrayList();
        ArrayList vendorList = new ArrayList();
        ArrayList servicePointList = new ArrayList();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Stores >> RC WO Report";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/stockPurchaseOutput.jsp";
            ReportTO reportTO = new ReportTO();
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());
            reportTO.setVendorId(reportCommand.getVendorId());
            reportTO.setPurpose(reportCommand.getPurpose());
            rcItemList = reportBP.processRcBillsReport(reportTO);
            Iterator itr = rcItemList.iterator();

            while (itr.hasNext()) {
                reportTO = (ReportTO) itr.next();
                System.out.println("getMaterialCostExternal" + reportTO.getMaterialCostExternal());
                System.out.println("getMaterialCostInternal" + reportTO.getMaterialCostInternal());
                System.out.println("laborCharge" + reportTO.getLaborCharge());
            }
            request.setAttribute("rcItemList", rcItemList);
            vendorList = vendorBP.processGetVendorList();
            servicePointList = vehicleBP.processGetspList();
            request.setAttribute("VendorLists", vendorList);
            request.setAttribute("vendorId", reportCommand.getVendorId());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("purpose", reportCommand.getPurpose());
            request.setAttribute("servicePointList", servicePointList);
            request.setAttribute("orderType", request.getParameter("orderType"));

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void handleRcBillExcelRep(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        OutputStream out = null;
        HttpSession session = request.getSession();
        ArrayList itemList = new ArrayList();
        String path = "content/report/stockPurchaseOutput.jsp";

        try {
            ReportTO reportTO = new ReportTO();
            ArrayList vendorList = new ArrayList();

            String[] sno = request.getParameterValues("sno");
            String[] poId = request.getParameterValues("rcWoId");
            String[] vehicleNo = request.getParameterValues("vehicleNo");
            String[] invoiceNo = request.getParameterValues("invoiceNo");
            String[] billDate = request.getParameterValues("billDate");
            String[] vendorName = request.getParameterValues("vendorName");
            String[] paplCode = request.getParameterValues("paplCode");
            String[] itemName = request.getParameterValues("itemName");
            String[] aqty = request.getParameterValues("aqty");
            String[] tyreNo = request.getParameterValues("tyreNo");
            String[] uom = request.getParameterValues("uom");
            String[] price = request.getParameterValues("price");
            String[] createdDate = request.getParameterValues("createdDate");
            String[] totalAmount = request.getParameterValues("totalAmount");

            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=purchaseReport.xls");

            WritableWorkbook w = Workbook.createWorkbook(response.getOutputStream());

            WritableSheet s = w.createSheet("Purchase", 0);
            s.addCell(new Label(0, 0, "SNO"));
            s.addCell(new Label(1, 0, "WO NO"));
            s.addCell(new Label(2, 0, "CREATED DATE"));
            s.addCell(new Label(3, 0, "INVOICE NO"));
            s.addCell(new Label(4, 0, "BILL DATE"));
            s.addCell(new Label(5, 0, "VEHICLE NO"));
            s.addCell(new Label(6, 0, "VENDOR NAME"));
            s.addCell(new Label(7, 0, "ITEM NAME"));
            s.addCell(new Label(8, 0, "QTY"));
            s.addCell(new Label(9, 0, "REF NO"));
            s.addCell(new Label(10, 0, "UNIT PRICE"));
            s.addCell(new Label(11, 0, "TOTAL"));
            for (int i = 0; i < sno.length; i++) {
                s.addCell(new Label(0, i + 1, sno[i]));
                s.addCell(new Label(1, i + 1, poId[i]));
                s.addCell(new Label(2, i + 1, createdDate[i]));
                s.addCell(new Label(3, i + 1, invoiceNo[i]));
                s.addCell(new Label(4, i + 1, billDate[i]));
                s.addCell(new Label(5, i + 1, vehicleNo[i]));
                s.addCell(new Label(6, i + 1, vendorName[i]));
                s.addCell(new Label(7, i + 1, itemName[i]));
                s.addCell(new Label(8, i + 1, aqty[i]));
                s.addCell(new Label(9, i + 1, tyreNo[i]));
                s.addCell(new Label(10, i + 1, price[i]));
                s.addCell(new Label(11, i + 1, totalAmount[i]));
            }
            w.write();
            w.close();
            //vendorList = vendorBP.processGetVendorList();
            //request.setAttribute("VendorLists", vendorList);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ModelAndView handleVehicleStatusPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        try {
            String menuPath = "Reports >>  Vehicle >> Vehicle Status";
            path = "content/report/vehicleStatus.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVehicleStatus(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList serviceTypes = new ArrayList();
        ArrayList operationPointList = new ArrayList();
        ArrayList statusDetails = new ArrayList();

        try {
            String menuPath = "Reports >>  Vehicle >> Vehicle Status";
            path = "content/report/vehicleStatus.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());
            reportTO.setRegNo(reportCommand.getRegNo());
            statusDetails = reportBP.processStatusReport(reportTO);
            request.setAttribute("StatusDetails", statusDetails);
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("regNo", reportCommand.getRegNo());
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void handleVehicleStatusExcel(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        OutputStream out = null;
        HttpSession session = request.getSession();
        ArrayList itemList = new ArrayList();
        String path = "content/report/vehicleStatus.jsp";

        try {
            ReportTO reportTO = new ReportTO();
            ArrayList List = new ArrayList();

            String[] regNo = request.getParameterValues("regNos");
            String[] model = request.getParameterValues("models");
            String[] jobCard = request.getParameterValues("jobCards");
            String[] complaint = request.getParameterValues("complaints");
            String[] inDate = request.getParameterValues("inDates");
            String[] dueDate = request.getParameterValues("dueDates");
            String[] remak = request.getParameterValues("remark");
            String[] currentKm = request.getParameterValues("currentKms");
            String[] lastKm = request.getParameterValues("lastKms");
            String[] lastDate = request.getParameterValues("lastDates");
            String[] lastComp = request.getParameterValues("lastComps");

            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=purchaseReport.xls");

            WritableWorkbook w = Workbook.createWorkbook(response.getOutputStream());

            WritableSheet s = w.createSheet("VehicleStatus", 0);

            s.addCell(new Label(0, 0, "SNO"));
            s.addCell(new Label(1, 0, "VEHICLE NO"));
            s.addCell(new Label(2, 0, "MODEL"));
            s.addCell(new Label(3, 0, "JOBCARD NO"));
            s.addCell(new Label(4, 0, "COMPLAINTS"));
            s.addCell(new Label(5, 0, "IN DATE"));
            s.addCell(new Label(6, 0, "DUE DATE"));
            s.addCell(new Label(7, 0, "REMARKS"));
            s.addCell(new Label(8, 0, "CURRENT KM"));
            s.addCell(new Label(9, 0, "LAST KM"));
            s.addCell(new Label(10, 0, "DATE"));
            s.addCell(new Label(11, 0, "LAST COMPLAINTS"));
            for (int i = 0; i < regNo.length; i++) {

                s.addCell(new Label(0, i + 1, String.valueOf(i + 1)));
                s.addCell(new Label(1, i + 1, regNo[i]));
                s.addCell(new Label(2, i + 1, model[i]));
                s.addCell(new Label(3, i + 1, jobCard[i]));
                s.addCell(new Label(4, i + 1, complaint[i]));
                s.addCell(new Label(5, i + 1, inDate[i]));
                s.addCell(new Label(6, i + 1, dueDate[i]));
                s.addCell(new Label(7, i + 1, remak[i]));
                s.addCell(new Label(8, i + 1, currentKm[i]));
                s.addCell(new Label(9, i + 1, lastKm[i]));
                s.addCell(new Label(10, i + 1, lastDate[i]));
                s.addCell(new Label(11, i + 1, lastComp[i]));
            }
            w.write();
            w.close();
            //vendorList = vendorBP.processGetVendorList();
            //request.setAttribute("VendorLists", vendorList);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ModelAndView handleTaxWiseItemsPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList vendorList = new ArrayList();
        ArrayList itemList = new ArrayList();
        ArrayList vatList = new ArrayList();

        try {
            String menuPath = "Reports >>  Stores >> Vat wise Items";
            path = "content/report/vatWiseItems.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            vendorList = purchaseBP.processVendorList(1011);
            vatList = purchaseBP.processActiveVatList();
            request.setAttribute("vendorList", vendorList);
            request.setAttribute("vatList", vatList);
            request.setAttribute("vat", reportCommand.getVat());
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleTaxWiseItems(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList vendorList = new ArrayList();
        ArrayList itemList = new ArrayList();
        ArrayList vatList = new ArrayList();

        try {
            String menuPath = "Reports >>  Stores >> Vat wise Items";
            path = "content/report/vatWiseItems.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            vendorList = purchaseBP.processVendorList(1011);
            vatList = purchaseBP.processActiveVatList();
            reportTO.setPaplCode(reportCommand.getPaplCode());
            reportTO.setItemName(reportCommand.getItemName());
            reportTO.setVendorId(reportCommand.getVendorId());
            reportTO.setTax(reportCommand.getVat());
            itemList = reportBP.processTaxwiseItems(reportTO);
            request.setAttribute("vendorList", vendorList);
            request.setAttribute("vatList", vatList);
            request.setAttribute("paplCode", reportCommand.getPaplCode());
            request.setAttribute("itemName", reportCommand.getItemName());
            request.setAttribute("vendorId", reportCommand.getVendorId());
            request.setAttribute("vat", reportCommand.getVat());
            request.setAttribute("itemList", itemList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVehicleComparisionReportPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList serviceTypes = new ArrayList();
        ArrayList operationPointList = new ArrayList();
        ArrayList statusDetails = new ArrayList();

        try {
            String menuPath = "Reports >>  MIS >> Manufacturer Comparision";
            path = "content/report/vehicleComparision.jsp";
            ArrayList mfrList = new ArrayList();
            ArrayList vehicleList = new ArrayList();
            mfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", mfrList);
            request.setAttribute("VehicleList", vehicleList);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVehicleComparisionReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList serviceTypes = new ArrayList();
        ArrayList operationPointList = new ArrayList();
        ArrayList vehicleList = new ArrayList();

        try {
            String menuPath = "Reports >> MIS >> Manufacturer Comparision";
            path = "content/report/vehicleComparision.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            reportTO.setFromDate(reportCommand.getFromDate());
            System.out.println("reportTO.getMfrId()" + reportCommand.getMfrId());
            reportTO.setMfrId(reportCommand.getMfrId());
            ArrayList mfrList = new ArrayList();
            mfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", mfrList);
            request.setAttribute("mfrId", reportCommand.getMfrId());
            if (reportTO.getMfrId().equalsIgnoreCase("0")) {
                vehicleList = reportBP.processVehMfrComparisionReport(reportTO);
                System.out.println("vehicleListmfr" + vehicleList.size());
            } else {
                vehicleList = reportBP.processVehModelComparisionReport(reportTO);
                System.out.println("vehicleListmodel" + vehicleList.size());
            }
            request.setAttribute("VehicleList", vehicleList);
            request.setAttribute("date", reportCommand.getFromDate());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVehicleAgeComparisionReportPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;
        ReportTO reportTO = new ReportTO();

        ArrayList statusDetails = new ArrayList();

        try {
            String menuPath = "Reports >>  MIS >> Vehicle Age  Comparision";
            path = "content/report/vehicleAgeComparision.jsp";
            ArrayList mfrList = new ArrayList();
            ArrayList vehicleList = new ArrayList();
            mfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", mfrList);
            request.setAttribute("VehicleList", vehicleList);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVehicleAgeComparisionReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;
        ReportTO reportTO = new ReportTO();

        ArrayList vehicleList = new ArrayList();

        try {
            String menuPath = "Reports >> MIS >> Vehicle  Age Comparision";
            path = "content/report/vehicleAgeComparision.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            reportTO.setFromDate(reportCommand.getFromDate());
            System.out.println("reportTO.getMfrId()" + reportCommand.getMfrId());
            reportTO.setMfrId(reportCommand.getMfrId());
            ArrayList mfrList = new ArrayList();
            mfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", mfrList);
            request.setAttribute("VehicleList", vehicleList);
            vehicleList = reportBP.processVehAgeComparisionReport(reportTO);
            System.out.println("vehicleListmfr" + vehicleList.size());
            request.setAttribute("VehicleList", vehicleList);
            request.setAttribute("date", reportCommand.getFromDate());
            request.setAttribute("mfrId", reportCommand.getMfrId());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    // rajesh
    public ModelAndView salesTrendSearch(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        try {
            String menuPath = "Reports >>  MIS >> Sales Bill";
            path = "content/report/salesBillTrend.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView salesTrendGraph(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList serviceTypes = new ArrayList();
        ArrayList operationPointList = new ArrayList();
        ArrayList salesTrendGraph = new ArrayList();

        try {
            String menuPath = "Reports >> MIS >> Sales Bill";
            path = "content/report/salesBillTrend.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());

            salesTrendGraph = reportBP.salesTrendGraph(reportTO);
            System.out.println("salesTrendGraph" + salesTrendGraph.size());

            request.setAttribute("salesTrendGraph", salesTrendGraph);
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView vendorTrendSearch(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;

        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList serviceTypes = new ArrayList();
        ArrayList operationPointList = new ArrayList();
        ArrayList vendorTrendSearch = new ArrayList();

        try {
            String menuPath = "Reports >> MIS >> Vendor Trend";
            path = "content/report/vendorTrend.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView vendorTrendGraph(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList serviceTypes = new ArrayList();
        ArrayList operationPointList = new ArrayList();
        ArrayList vendorTrendGraph = new ArrayList();

        try {
            String menuPath = "Reports >> MIS >> Vendor Trend";
            path = "content/report/vendorTrend.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());

            reportTO.setItemCode(reportCommand.getItemCode());
            reportTO.setItemName(reportCommand.getItemName());
            reportTO.setVendorName(reportCommand.getVendorName());

            vendorTrendGraph = reportBP.vendorTrendGraph(reportTO);
            System.out.println("vendorTrendGraph" + vendorTrendGraph.size());

            request.setAttribute("salesTrendGraph", vendorTrendGraph);
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("vendorName", reportCommand.getVendorName());
            request.setAttribute("itemName", reportCommand.getItemName());
            request.setAttribute("itemCode", reportCommand.getItemCode());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView problemDistributionSearch(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;

        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList mfr = new ArrayList();
        ArrayList usage = new ArrayList();
        ArrayList vehicleType = new ArrayList();

        try {
            String menuPath = "Reports >> MIS >> Problem Distribution";
            path = "content/report/problemDistribution.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            mfr = reportBP.mfr();
            request.setAttribute("mfr", mfr);

            usage = reportBP.usage();
            request.setAttribute("usage", usage);

            vehicleType = reportBP.vehicleType();
            request.setAttribute("vehicleType", vehicleType);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView problemDistributionGraph(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;

        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList mfr = new ArrayList();
        ArrayList usage = new ArrayList();
        ArrayList vehicleType = new ArrayList();
        ArrayList problemDistributionGraph = new ArrayList();
        ReportTO reportTO = new ReportTO();
        try {
            String menuPath = "Reports >> MIS >> Problem Distribution";
            path = "content/report/problemDistribution.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());
            reportTO.setMfrId(reportCommand.getMfrId());
            reportTO.setUsage(reportCommand.getUsage());
            reportTO.setAge(reportCommand.getAge());
            reportTO.setProblem(reportCommand.getProblem());
            reportTO.setVehicleType(reportCommand.getVehicleType());

            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("mfrId", reportCommand.getMfrId());
            request.setAttribute("usageId", reportCommand.getUsage());
            request.setAttribute("age", reportCommand.getAge());
            request.setAttribute("problem", reportCommand.getProblem());
            request.setAttribute("vehicleTypeId", reportCommand.getVehicleType());

            problemDistributionGraph = reportBP.problemDistributionGraph(reportTO);
            request.setAttribute("problemDistributionGraph", problemDistributionGraph);

            int sizeValue = problemDistributionGraph.size();
            ArrayList colorList = new ArrayList();
            colorList = reportBP.getColorList(sizeValue);
            request.setAttribute("ColorList", colorList);

            mfr = reportBP.mfr();
            request.setAttribute("mfr", mfr);

            usage = reportBP.usage();
            request.setAttribute("usage", usage);

            vehicleType = reportBP.vehicleType();
            request.setAttribute("vehicleType", vehicleType);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void vendorName(HttpServletRequest request, HttpServletResponse response) throws IOException {
        System.out.println("i am in ajax ");
        HttpSession session = request.getSession();
        String vendorName = request.getParameter("vendorName");
        System.out.println("vendorName" + vendorName);
        String suggestions = reportBP.processItemSuggests(vendorName);
        System.out.println("suggestions" + suggestions);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.println(suggestions);
        writer.close();
    }

    public void problemName(HttpServletRequest request, HttpServletResponse response) throws IOException {
        System.out.println("i am in ajax ");
        HttpSession session = request.getSession();
        String problem = request.getParameter("problem");
        System.out.println("problem---------" + problem);
        String suggestions = reportBP.problemItemSuggests(problem);
        System.out.println("suggestions" + suggestions);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.println(suggestions);
        writer.close();
    }

    //end rajesh
    // shankar
    public ModelAndView stockWorthSearch(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList categories = new ArrayList();

        try {
            String menuPath = "Reports >>  MIS >> StockWorth";
            path = "content/report/stockWorth.jsp";
            categories = reportBP.getActiveCategories();
            request.setAttribute("CategoryList", categories);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView stockWorthGraph(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList serviceTypes = new ArrayList();
        ArrayList operationPointList = new ArrayList();
        ArrayList stockWorthGraph = new ArrayList();

        try {
            String menuPath = "Reports >> MIS >> Sales Bill";
            path = "content/report/stockWorth.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());
            reportTO.setCategoryId(reportCommand.getCategoryId());
            if (reportCommand.getItemName() != null && reportCommand.getItemName() != "") {
                reportTO.setItemName(reportCommand.getItemName());
                request.setAttribute("itemName", reportCommand.getItemName());
            }

            stockWorthGraph = reportBP.stockWorthGraph(reportTO);
            request.setAttribute("stockWorthGraph", stockWorthGraph);

            ArrayList companyNameList = new ArrayList();
            companyNameList = reportBP.CompanyNameList(reportTO);
            request.setAttribute("companyNameList", companyNameList);

            ArrayList monthNameList = new ArrayList();
            monthNameList = reportBP.monthNameList(reportTO);
            request.setAttribute("monthNameList", monthNameList);

            int sizeValue = stockWorthGraph.size();
            ArrayList colorList = new ArrayList();
            colorList = reportBP.getColorList(sizeValue);
            request.setAttribute("ColorList", colorList);

            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleTopProblemSearch(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList manufacturerList = new ArrayList();
        ArrayList usageTypeList = new ArrayList();
        ArrayList vehicleTypeList = new ArrayList();
        try {
            String menuPath = "Reports >>  MIS >> Top Problem";
            path = "content/report/topProblem.jsp";
            manufacturerList = reportBP.getManufacturerList();
            System.out.println("man size is " + manufacturerList.size());
            usageTypeList = reportBP.getUsageTypeList();
            //vehicleTypeList = reportBP.getVehicleTypeList();
            request.setAttribute("ManufacturerList", manufacturerList);
            request.setAttribute("UsageTypeList", usageTypeList);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView categorySearch(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        try {
            String menuPath = "Reports >>  Stores >> Categorywise";
            path = "content/stores/categoryReport.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView categoryReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList serviceTypes = new ArrayList();
        ArrayList operationPointList = new ArrayList();
        ArrayList categoryRcReport = new ArrayList();

        try {
            String menuPath = "Reports >>  Stores >> Categorywise";
            path = "content/stores/categoryReport.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());

            categoryRcReport = reportBP.categoryRcReport(reportTO);
            System.out.println("categoryRcReport" + categoryRcReport.size());

            request.setAttribute("categoryRcReport", categoryRcReport);
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("itemType", reportCommand.getItemType());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView categoryNewReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList serviceTypes = new ArrayList();
        ArrayList operationPointList = new ArrayList();
        ArrayList categoryNewReport = new ArrayList();

        try {
            String menuPath = "Reports >>  Stores >> Categorywise";
            path = "content/stores/categoryReport.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());

            categoryNewReport = reportBP.categoryNewReport(reportTO);
            System.out.println("categoryNewReport" + categoryNewReport.size());

            request.setAttribute("categoryRcReport", categoryNewReport);
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("itemType", reportCommand.getItemType());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void handleStockWorthExcelRep(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        OutputStream out = null;
        HttpSession session = request.getSession();
        ArrayList itemList = new ArrayList();
        String path = "content/report/vehicleStatus.jsp";

        try {
            String sno[] = request.getParameterValues("sno");
            String paplcode[] = request.getParameterValues("paplcodes");
            String itemName[] = request.getParameterValues("itemNames");
            String categoryName[] = request.getParameterValues("categoryNames");
            String itemTypes[] = request.getParameterValues("itemTypes");
            String itemPrice[] = request.getParameterValues("itemPrice");
            String totalQty[] = request.getParameterValues("totalQty");
            String stworth[] = request.getParameterValues("stworth");
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=stockworthReport.xls");
            WritableWorkbook w = Workbook.createWorkbook(response.getOutputStream());
            WritableSheet s = w.createSheet("stockworth", 0);
            s.addCell(new Label(0, 0, "SNO"));
            s.addCell(new Label(1, 0, "paplcodes"));
            s.addCell(new Label(2, 0, "itemNames"));
            s.addCell(new Label(3, 0, "categoryNames"));
            s.addCell(new Label(4, 0, "itemType"));
            s.addCell(new Label(5, 0, "itemPrice"));
            s.addCell(new Label(6, 0, "totalQty"));
            s.addCell(new Label(7, 0, "stworth"));
            for (int i = 0; i < sno.length; i++) {
                s.addCell(new Label(0, i + 1, sno[i]));
                s.addCell(new Label(1, i + 1, paplcode[i]));
                s.addCell(new Label(2, i + 1, itemName[i]));
                s.addCell(new Label(3, i + 1, categoryName[i]));
                s.addCell(new Label(4, i + 1, itemTypes[i]));
                s.addCell(new Label(5, i + 1, itemPrice[i]));
                s.addCell(new Label(6, i + 1, totalQty[i]));
                s.addCell(new Label(7, i + 1, stworth[i]));
            }
            w.write();
            w.close();
            //vendorList = vendorBP.processGetVendorList();
            //request.setAttribute("VendorLists", vendorList);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ModelAndView handlecategorywiseGD(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        System.out.println("vijayakanth");
        HttpSession session = request.getSession();
        ArrayList stReqList = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList spList = new ArrayList();
        StockTransferTO repTO = null;
        CompanyTO reportTO = null;
        reportCommand = command;
        String path = "";
        String fromDate, toDate;
        String menuPath = "Reports >> Stores >> CategoryWise ST Report";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/report/categorystReqList.jsp";
            if (!reportCommand.getFromDate().equals("")) {
                fromDate = reportCommand.getFromDate();
                toDate = reportCommand.getToDate();
            } else {
                fromDate = (String) session.getAttribute("currentDate");
                toDate = (String) session.getAttribute("currentDate");
            }

            int fromSpId = Integer.parseInt(reportCommand.getFromId());
            int toSpId = Integer.parseInt(reportCommand.getToId());

            request.setAttribute("fromId", reportCommand.getFromId());
            request.setAttribute("toId", reportCommand.getToId());
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            System.out.println("vijay to date" + toDate);

            stReqList = reportBP.processCategoryStRequestList(fromDate, toDate, fromSpId, toSpId);

            request.setAttribute("categoryRcReport", stReqList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVehicleTaxServiceCost(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList serviceTypes = new ArrayList();
        ArrayList operationPointList = new ArrayList();
        ArrayList servicePointList = new ArrayList();
        ArrayList billList = new ArrayList();

        String menuPath = "Reports >>Vehicle >> Vehicle Service Analysis";
        path = "content/report/taxwisevehicleServiceCost.jsp";
        try {
            String fromDate = (String) session.getAttribute("currentDate");
            String toDate = (String) session.getAttribute("currentDate");
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            ReportTO repTO = new ReportTO();
            repTO.setCompanyName("0");
            repTO.setServiceTypeId("0");
            ArrayList customerList = new ArrayList();
            customerList = customerBP.processActiveCustomerList();
            operationPointList = vehicleBP.processGetopList();
            servicePointList = vehicleBP.processGetspList();
            request.setAttribute("customerList", customerList);
            request.setAttribute("servicePointList", servicePointList);

            request.setAttribute("operationPointList", operationPointList);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve handleVehicleTaxServiceCost data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handlevehicleTaxWiseServiceCostReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ArrayList serviceTypes = new ArrayList();
        ArrayList operationPointList = new ArrayList();
        ArrayList billList = new ArrayList();

        String menuPath = "Reports >>Vehicle >> Vehicle Service Analysis";
        path = "content/report/taxwisevehicleServiceCost.jsp";
        try {
            reportCommand = command;
            ArrayList vatlist = new ArrayList();
            String VatValues = "";
            String[] len = null;
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ReportTO repTO = new ReportTO();
            ArrayList serviceCostList = new ArrayList();
            ArrayList customerList = new ArrayList();
            repTO.setRegNo(reportCommand.getRegNo());
            repTO.setFromDate(reportCommand.getFromDate());
            repTO.setToDate(reportCommand.getToDate());
            repTO.setCustId(reportCommand.getCustId());
            repTO.setReportType(Integer.parseInt(reportCommand.getReportType()));

            customerList = customerBP.processActiveCustomerList();

            VatValues = reportBP.getVatPercentages();
            len = VatValues.split(",");
            System.out.println("lenth is" + len.length);
            ReportTO reportTO1 = null;
            for (int i = 0; i < len.length; i++) {
                reportTO1 = new ReportTO();
                reportTO1.setAcd(len[i]);
                vatlist.add(reportTO1);
            }

            request.setAttribute("VatValues", vatlist);

            request.setAttribute("len", len);

            request.setAttribute("custId", reportCommand.getCustId());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("regNo", reportCommand.getRegNo());
            String rep = reportCommand.getReportType();
            System.out.println("rep" + rep);
            request.setAttribute("reportType", rep);
            serviceCostList = reportBP.processTaxwiseServiceCostList(repTO);
            request.setAttribute("serviceCostList", serviceCostList);
            request.setAttribute("customerList", customerList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Service Cost List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handlevehicleTaxWiseServiceCostExcelReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ArrayList serviceTypes = new ArrayList();
        ArrayList operationPointList = new ArrayList();
        ArrayList billList = new ArrayList();

        String menuPath = "Reports >>Vehicle >> Vehicle Service Analysis";
        path = "content/report/taxwisevehicleServiceCostExcel.jsp";
        try {
            reportCommand = command;
            ArrayList vatlist = new ArrayList();
            String VatValues = "";
            String[] len = null;
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ReportTO repTO = new ReportTO();
            ArrayList serviceCostList = new ArrayList();
            ArrayList customerList = new ArrayList();
            repTO.setRegNo(reportCommand.getRegNo());
            repTO.setFromDate(reportCommand.getFromDate());
            repTO.setToDate(reportCommand.getToDate());
            repTO.setCustId(reportCommand.getCustId());
            repTO.setReportType(Integer.parseInt(reportCommand.getReportType()));

            customerList = customerBP.processActiveCustomerList();

            VatValues = reportBP.getVatPercentages();
            len = VatValues.split(",");
            System.out.println("lenth is" + len.length);
            ReportTO reportTO1 = null;
            for (int i = 0; i < len.length; i++) {
                reportTO1 = new ReportTO();
                reportTO1.setAcd(len[i]);
                vatlist.add(reportTO1);
            }

            request.setAttribute("VatValues", vatlist);

            request.setAttribute("len", len);

            request.setAttribute("custId", reportCommand.getCustId());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("regNo", reportCommand.getRegNo());
            String rep = reportCommand.getReportType();
            System.out.println("rep" + rep);
            request.setAttribute("reportType", rep);
            serviceCostList = reportBP.processTaxwiseServiceCostList(repTO);
            request.setAttribute("serviceCostList", serviceCostList);
            request.setAttribute("customerList", customerList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Service Cost List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView taxWisePODetails(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList serviceTypes = new ArrayList();
        ArrayList operationPointList = new ArrayList();
        ArrayList servicePointList = new ArrayList();
        ArrayList billList = new ArrayList();

        String menuPath = "Reports >>Store >> Taxwise Purcahse Report";
        path = "content/report/taxwisepurchase.jsp";
        try {

            ReportTO repTO = new ReportTO();
            repTO.setCompanyName("0");
            repTO.setServiceTypeId("0");
            ArrayList vendorList = new ArrayList();
            vendorList = vendorBP.processGetVendorList();
            request.setAttribute("vendorList", vendorList);
            reportCommand = command;
            ArrayList vatlist = new ArrayList();
            String VatValues = "";
            String[] len = null;
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList serviceCostList = new ArrayList();
            repTO.setFromDate(reportCommand.getFromDate());
            repTO.setToDate(reportCommand.getToDate());
            repTO.setCustId(reportCommand.getCustId());

            VatValues = reportBP.getVatPercentages();
            len = VatValues.split(",");
            System.out.println("lenth is" + len.length);
            ReportTO reportTO1 = null;
            for (int i = 0; i < len.length; i++) {
                reportTO1 = new ReportTO();
                reportTO1.setAcd(len[i]);
                vatlist.add(reportTO1);
            }

            request.setAttribute("VatValues", vatlist);

            request.setAttribute("len", len);

            request.setAttribute("custId", reportCommand.getCustId());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());

            serviceCostList = reportBP.processTaxwisePO(repTO);
            request.setAttribute("serviceCostList", serviceCostList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve taxwisePoReport data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView taxwisePoReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList serviceTypes = new ArrayList();
        ArrayList operationPointList = new ArrayList();
        ArrayList servicePointList = new ArrayList();
        ArrayList billList = new ArrayList();

        String menuPath = "Reports >>Store >> Taxwise Purcahse Report";
        path = "content/report/taxwisepurchase.jsp";
        try {
            String fromDate = (String) session.getAttribute("currentDate");
            String toDate = (String) session.getAttribute("currentDate");
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            ReportTO repTO = new ReportTO();
            repTO.setCompanyName("0");
            repTO.setServiceTypeId("0");
            ArrayList vendorList = new ArrayList();
            vendorList = vendorBP.processGetVendorList();
            request.setAttribute("vendorList", vendorList);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve taxwisePoReport data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleTopProblemGraph(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        ArrayList topProblemGraph = new ArrayList();
        ArrayList manufacturerList = new ArrayList();
        ArrayList usageTypeList = new ArrayList();

        try {
            String menuPath = "Reports >> MIS >> Sales Bill";
            path = "content/report/topProblem.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            manufacturerList = reportBP.getManufacturerList();
            System.out.println("man size is " + manufacturerList.size());
            usageTypeList = reportBP.getUsageTypeList();
            //vehicleTypeList = reportBP.getVehicleTypeList();
            request.setAttribute("ManufacturerList", manufacturerList);
            request.setAttribute("UsageTypeList", usageTypeList);
            reportTO.setFromDate(reportCommand.getFromDate());
            reportTO.setToDate(reportCommand.getToDate());
            reportTO.setMfrId(reportCommand.getMfr());
            reportTO.setUsageTypeId(Integer.parseInt(reportCommand.getUsageType()));
            reportTO.setAge(reportCommand.getAge());
            topProblemGraph = reportBP.gettopProblem(reportTO);
            request.setAttribute("topProblemGraph", topProblemGraph);
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //Hari
    public ModelAndView scheduleDeviationSearch(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/LoginScreen.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList serviceSummary = new ArrayList();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        int MILLIS_IN_DAY = 15 * (1000 * 60 * 60 * 24);

        System.out.println("scheduleDeviationSearch Method");
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> Schedule Deviation Report";
        try {
            path = "content/report/scheduleDeviation.jsp";
            String fromDate = "";
            String toDate = "";
            if (reportCommand.getFromDate() == "") {
                toDate = (String) session.getAttribute("currentDate");
                System.out.println("MILLIS_IN_DAY=" + MILLIS_IN_DAY);

                fromDate = toDate;
            } else {
                fromDate = reportCommand.getFromDate();
                toDate = reportCommand.getToDate();
            }
            System.out.println("fromDate=" + fromDate);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("summ", "Service Summary From " + fromDate + " to " + toDate);

            serviceSummary = reportBP.processServiceChartData(fromDate, toDate);
            if (serviceSummary.size() != 0) {
                request.setAttribute("serviceSummary", serviceSummary);
                System.out.println("serviceSummary  in controler=" + serviceSummary.size());
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView scheduleDeviationGraph(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/LoginScreen.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList serviceSummary = new ArrayList();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        int MILLIS_IN_DAY = 15 * (1000 * 60 * 60 * 24);
        System.out.println("In scheduleDeviationGraph Method");
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> Schedule Deviation Report";
        try {
            path = "content/report/scheduleDeviation.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String fromDate = "";
            String toDate = "";
            if (reportCommand.getFromDate() == "") {
                toDate = (String) session.getAttribute("currentDate");
                System.out.println("MILLIS_IN_DAY=" + MILLIS_IN_DAY);
                fromDate = toDate;
            } else {
                fromDate = reportCommand.getFromDate();
                toDate = reportCommand.getToDate();
            }
            System.out.println("fromDate=" + fromDate);
            serviceSummary = reportBP.processServiceChartData(fromDate, toDate);

            if (serviceSummary.size() != 0) {
                request.setAttribute("serviceSummary", serviceSummary);
            }

            System.out.println("serviceSummary  in controler=" + serviceSummary.size());
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("summ", "Service Summary From " + fromDate + " to " + toDate);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView rcExpenseSearch(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/LoginScreen.jsp");
        }
        System.out.println("rcExpenseSearch Method");
        reportCommand = command;
        String path = "";
        String fromDate = null;
        String toDate = null;
        String menuPath = "Reports >> RC Expense Report";
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        int MILLIS_IN_DAY = 15 * (1000 * 60 * 60 * 24);
        try {
            HttpSession session = request.getSession();
            path = "/content/report/RCExpense.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            if (reportCommand.getFromDate() == "") {
                toDate = (String) session.getAttribute("currentDate");
                System.out.println("MILLIS_IN_DAY=" + MILLIS_IN_DAY);
                fromDate = toDate;
            } else {
                fromDate = reportCommand.getFromDate();
                toDate = reportCommand.getToDate();
            }

            ArrayList rcVendorList = new ArrayList();
            rcVendorList = vendorBP.processGetRcVendorList();
            System.out.println("rcVendorList-->" + rcVendorList.size());
            request.setAttribute("rcVendorList", rcVendorList);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {

            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView rcExpenseGraph(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/LoginScreen.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList rcExpenseSummary = new ArrayList();

        System.out.println("In rcExpenseGraph Method");
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> RC Expense Report";
        String fromDate = "";
        String toDate = "";
        String vendorId = "";
        try {
            ReportTO reportTO = new ReportTO();
            path = "content/report/RCExpense.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            if (reportCommand.getFromDate() != null && !"".equals(reportCommand.getFromDate())) {
                fromDate = reportCommand.getFromDate();
                reportTO.setFromDate(reportCommand.getFromDate());
                System.out.println("reportTO.getFromDate()" + reportTO.getFromDate());
            }

            if (reportCommand.getToDate() != null && !"".equals(reportCommand.getToDate())) {
                toDate = reportCommand.getToDate();
                reportTO.setToDate(reportCommand.getToDate());
                System.out.println("reportTO.getToDate()" + reportTO.getToDate());
            }

            if (reportCommand.getVendorId() != null && !"".equals(reportCommand.getVendorId())) {
                vendorId = reportCommand.getVendorId();
                reportTO.setVendorId(reportCommand.getVendorId());
                System.out.println("reportTO.getVendorId()" + reportTO.getVendorId());
            }

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("vendorId", vendorId);
            request.setAttribute("summ", "rcExpenseSummary From " + fromDate + " to " + toDate);

            ArrayList rcVendorList = new ArrayList();
            rcVendorList = vendorBP.processGetRcVendorList();
            request.setAttribute("rcVendorList", rcVendorList);
            System.out.println("rcVendorList-->" + rcVendorList.size());

            rcExpenseSummary = reportBP.processRcExpenseGraph(reportTO);

            if (rcExpenseSummary.size() != 0) {
                request.setAttribute("rcExpenseSummary", rcExpenseSummary);
                System.out.println("rcExpenseSummary  in controler=" + rcExpenseSummary.size());
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView scrapGraphData(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/LoginScreen.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList scrapValue = new ArrayList();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        int MILLIS_IN_DAY = 15 * (1000 * 60 * 60 * 24);

        System.out.println("scrapGraphData Method");
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> Scrap Value Report";
        String fromDate = "";
        String toDate = "";
        try {
            path = "content/report/scrapValue.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            if (reportCommand.getFromDate() == "") {
                toDate = (String) session.getAttribute("currentDate");
                System.out.println("MILLIS_IN_DAY=" + MILLIS_IN_DAY);

                fromDate = toDate;
            } else {
                fromDate = reportCommand.getFromDate();
                toDate = reportCommand.getToDate();
            }
            System.out.println("fromDate=" + fromDate);

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("summ", "scrapValue From " + fromDate + " to " + toDate);

            scrapValue = reportBP.processScrapGraphData(fromDate, toDate);
            if (scrapValue.size() != 0) {
                request.setAttribute("scrapValue", scrapValue);
            }
            System.out.println("scrapValue  in controler=" + scrapValue.size());
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView externalLabourBillGraphData(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/LoginScreen.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList externalLabour = new ArrayList();
        ArrayList contractVendor = new ArrayList();
        ArrayList colourList = new ArrayList();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        int MILLIS_IN_DAY = 15 * (1000 * 60 * 60 * 24);
        int vendorId = 0;
        System.out.println("externalLabourBillGraphData Method");
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> External Labour Bill Trend";
        try {
            path = "content/report/externalLabourBill.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String fromDate = "";
            String toDate = "";
            if (reportCommand.getFromDate() == "") {
                toDate = (String) session.getAttribute("currentDate");
                System.out.println("MILLIS_IN_DAY=" + MILLIS_IN_DAY);

                fromDate = toDate;

            } else {
                fromDate = reportCommand.getFromDate();
                toDate = reportCommand.getToDate();
                vendorId = Integer.parseInt(reportCommand.getVendorId());
            }
            System.out.println("fromDate=" + fromDate + "todate-->" + toDate + "VendorId-->" + vendorId);

            colourList = reportBP.processGetColourList();
            contractVendor = reportBP.processGetContractVendor(vendorId);

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("colourList", colourList);
            request.setAttribute("vendorId", vendorId);
            request.setAttribute("contractVendor", contractVendor);
            externalLabour = reportBP.processExternalLabourBillGraphData(fromDate, toDate, vendorId);

            if (externalLabour.size() != 0) {
                request.setAttribute("externalLabour", externalLabour);
                System.out.println("externalLabour  in controler=" + externalLabour.size());
            }

            request.setAttribute("summ", "externalLabour From " + fromDate + " to " + toDate);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }


    /*
     * This Method Return the No of Rc Parts Send for Services and  How much Received to Service Point
     */
    public ModelAndView rcTrendGraphData(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/LoginScreen.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList rcServiceData = new ArrayList();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        int MILLIS_IN_DAY = 15 * (1000 * 60 * 60 * 24);

        System.out.println("rcTrendGraphData Method");
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> RC Trend Reports";
        try {
            path = "content/report/rcTrendReport.jsp";
            String fromDate = "";
            String toDate = "";
            if (reportCommand.getFromDate() == "") {
                toDate = (String) session.getAttribute("currentDate");
                System.out.println("MILLIS_IN_DAY=" + MILLIS_IN_DAY);

                fromDate = toDate;
            } else {
                fromDate = reportCommand.getFromDate();
                toDate = reportCommand.getToDate();

            }
            System.out.println("fromDate=" + fromDate + "todate-->" + toDate);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("summ", "rcServiceData From " + fromDate + " to " + toDate);

            rcServiceData = reportBP.processRcTrendGraphData(fromDate, toDate);

            if (rcServiceData.size() != 0) {
                request.setAttribute("rcServiceData", rcServiceData);
                System.out.println("rcServiceData  in controler=" + rcServiceData.size());
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }


    /*
     * Return The Cost Spend for Service depend upon Mfr,Vehicle Type,Usage Type of Vehicle
     */
    public ModelAndView vehicleServiceGraphData(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/LoginScreen.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList vehicleServiceData = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList vehicleUsage = new ArrayList();
        ArrayList vehicleType = new ArrayList();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        int MILLIS_IN_DAY = 15 * (1000 * 60 * 60 * 24);

        System.out.println("vehicleServiceGraphData Method");
        reportCommand = command;
        String path = "";

        String menuPath = "Reports >> Cost Of Service Report";
        try {
            path = "content/report/vehicleServiceCost.jsp";
            String fromDate = "";
            String toDate = "";
            ReportTO reportTO = new ReportTO();

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            if (reportCommand.getFromDate() == "") {
                toDate = (String) session.getAttribute("currentDate");
                System.out.println("MILLIS_IN_DAY=" + MILLIS_IN_DAY);

                fromDate = toDate;
                reportTO.setFromDate(fromDate);
                reportTO.setToDate(toDate);
            } else {

                fromDate = reportCommand.getFromDate();
                toDate = reportCommand.getToDate();

                reportTO.setFromDate(reportCommand.getFromDate());
                System.out.println("reportCommand.getFromDate()" + reportCommand.getFromDate());
                reportTO.setToDate(reportCommand.getToDate());
                System.out.println("reportCommand.getToDate()" + reportCommand.getToDate());
                if (reportCommand.getMfrId() != null && !"".equals(reportCommand.getMfrId())) {

                    reportTO.setMfrid(Integer.parseInt(reportCommand.getMfrId()));
                    System.out.println("reportCommand.getMfrId()" + reportCommand.getMfrId());
                }
                if (reportCommand.getUsageType() != null && !"".equals(reportCommand.getUsageType())) {
                    reportTO.setUsageid(Integer.parseInt(reportCommand.getUsageType()));
                    System.out.println("reportCommand.getUsageType()" + reportCommand.getUsageType());
                }
                if (reportCommand.getVehicleType() != null && !"".equals(reportCommand.getVehicleType())) {
                    reportTO.setTypeId(Integer.parseInt(reportCommand.getVehicleType()));
                    System.out.println("reportCommand.getVehicleType()" + reportCommand.getVehicleType());
                }
                if (reportCommand.getYear() != null && !"".equals(reportCommand.getYear())) {
                    reportTO.setLife(Integer.parseInt(reportCommand.getYear()));
                    System.out.println("reportCommand.getYear()" + reportCommand.getYear());
                }
            }
            mfrList = vehicleBP.processGetMfrList();
            vehicleType = vehicleBP.processGetTypeList();
            vehicleUsage = vehicleBP.processActiveUsageList();

            ArrayList colourList = new ArrayList();
            colourList = reportBP.processGetColourList();

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("mfrId", reportTO.getMfrid());
            request.setAttribute("usageType", reportTO.getUsageid());
            request.setAttribute("vehicleType", reportTO.getTypeId());
            request.setAttribute("year", reportTO.getLife());
            request.setAttribute("colourList", colourList);

            request.setAttribute("mfrList", mfrList);
            request.setAttribute("vehicleTypes", vehicleType);
            request.setAttribute("vehicleUsage", vehicleUsage);

            vehicleServiceData = reportBP.processVehicleServiceGraphData(reportTO);
            if (vehicleServiceData.size() != 0) {
                System.out.println("vehicleServiceData  in controler=" + vehicleServiceData.size());
                request.setAttribute("vehicleServiceData", vehicleServiceData);
            }

            request.setAttribute("summ", "vehicleServiceData From " + reportCommand.getFromDate() + " to " + reportCommand.getToDate());

        } catch (FPRuntimeException exception) {

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {

            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /*
     * Return The Milage of Vehicle reading depend upon Mfr,Vehicle Type,Usage Type of Vehicle
     */
    public ModelAndView mileageReportGraphData(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/LoginScreen.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList mileageData = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList vehicleUsage = new ArrayList();
        ArrayList vehicleType = new ArrayList();

        System.out.println("mileageReportGraphData Method");
        reportCommand = command;
        String path = "";
        String fromDate = "";
        String toDate = "";

        String menuPath = "Reports >> Mileage Report";
        try {
            path = "content/report/mileageReport.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ReportTO reportTO = new ReportTO();
            if (reportCommand.getToDate() == "") {
                toDate = (String) session.getAttribute("currentDate");

                reportTO.setToDate(toDate);
            } else {
                toDate = reportCommand.getToDate();
                reportTO.setToDate(reportCommand.getToDate());

                System.out.println("reportCommand.getToDate()" + reportCommand.getToDate());

                if (reportCommand.getMfrId() != null && !"".equals(reportCommand.getMfrId())) {

                    reportTO.setMfrid(Integer.parseInt(reportCommand.getMfrId()));
                    System.out.println("reportCommand.getMfrId()" + reportCommand.getMfrId());
                }
                if (reportCommand.getUsageType() != null && !"".equals(reportCommand.getUsageType())) {
                    reportTO.setUsageid(Integer.parseInt(reportCommand.getUsageType()));
                    System.out.println("reportCommand.getUsageType()" + reportCommand.getUsageType());
                }
                if (reportCommand.getVehicleType() != null && !"".equals(reportCommand.getVehicleType())) {
                    reportTO.setTypeId(Integer.parseInt(reportCommand.getVehicleType()));
                    System.out.println("reportCommand.getVehicleType()" + reportCommand.getVehicleType());
                }
                if (reportCommand.getYear() != null && !"".equals(reportCommand.getYear())) {
                    reportTO.setLife(Integer.parseInt(reportCommand.getYear()));
                    System.out.println("reportCommand.getYear()" + reportCommand.getYear());
                }
            }
            mfrList = vehicleBP.processGetMfrList();
            vehicleType = vehicleBP.processGetTypeList();
            vehicleUsage = vehicleBP.processActiveUsageList();
            ArrayList colourList = new ArrayList();
            colourList = reportBP.processGetColourList();

            request.setAttribute("toDate", toDate);
            request.setAttribute("mfrId", reportTO.getMfrid());
            request.setAttribute("usageType", reportTO.getUsageid());
            request.setAttribute("vehicleType", reportTO.getTypeId());
            request.setAttribute("year", reportTO.getLife());
            request.setAttribute("colourList", colourList);

            request.setAttribute("mfrList", mfrList);
            request.setAttribute("vehicleTypes", vehicleType);
            request.setAttribute("vehicleUsage", vehicleUsage);

            request.setAttribute("summ", "mileageData From " + " to " + reportCommand.getToDate());

            mileageData = reportBP.processMileageGraphData(reportTO);
            if (mileageData.size() != 0) {
                System.out.println("mileageData  in controler=" + mileageData.size());
                request.setAttribute("mileageData", mileageData);
            }

        } catch (FPRuntimeException exception) {

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {

            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /*
     * Return The Rc Expense And Saving Amount from RCMaterial by Month wise
     */
    public ModelAndView rcSavingReportGraphData(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/LoginScreen.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList rcVendorList = new ArrayList();
        ArrayList rcItemIdList = new ArrayList();
        ArrayList rcExpenseAmount = new ArrayList();
        ArrayList actualPrice = new ArrayList();
        ArrayList actualPriceList = new ArrayList();
        ArrayList rcSavingAmount = new ArrayList();

        System.out.println("rcSavingReportGraphData Method");
        reportCommand = command;
        String path = "";
        String fromDate = "";
        String toDate = "";
        int vendorId = 0;

        String menuPath = "Reports >> Saving By RC";
        try {
            path = "content/report/RCsaving.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ReportTO reportTO = new ReportTO();
            if (reportCommand.getToDate() == "") {
                toDate = (String) session.getAttribute("currentDate");
                fromDate = (String) session.getAttribute("currentDate");

                reportTO.setToDate(toDate);
                reportTO.setFromDate(fromDate);
                reportTO.setVendorId("0");
            } else {
                fromDate = reportCommand.getFromDate();
                toDate = reportCommand.getToDate();
                reportTO.setToDate(toDate);
                reportTO.setFromDate(fromDate);

                System.out.println("reportCommand.getToDate()" + reportCommand.getToDate());
                System.out.println("reportCommand.getFromDate()" + reportCommand.getFromDate());

                if (reportCommand.getVendorId() != null && !"".equals(reportCommand.getVendorId())) {
                    reportTO.setVendorId(reportCommand.getVendorId());
                    vendorId = Integer.parseInt(reportCommand.getVendorId());
                    System.out.println("reportCommand.getVendorId()" + reportCommand.getVendorId());

                }

            }
            rcVendorList = reportBP.processRcVendorList();
            System.out.println("In rcSavingReportGraphData Controller rcVendorList" + rcVendorList.size());

            ArrayList colourList = new ArrayList();
            colourList = reportBP.processGetColourList();

            request.setAttribute("toDate", toDate);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("vendorId", vendorId);
            request.setAttribute("rcVendorList", rcVendorList);
            request.setAttribute("colourList", colourList);

            rcExpenseAmount = reportBP.processRcExpenseAmount(reportTO);
            System.out.println("In rcSavingReportGraphData Controller rcExpenseAmount" + rcExpenseAmount.size());

            Iterator itr = rcExpenseAmount.iterator();
            ReportTO report = null;
            int i = 0;
            int count = 0;
            Float actualAmount = 0.0f;
            String date = "";

            while (itr.hasNext()) {
                report = (ReportTO) itr.next();
                String item[] = report.getItemId().split(",");
                reportTO.setCreatedDate(report.getCreatedDate());
                for (i = 0; i < item.length; i++) {
                    reportTO.setItemId(item[i]);
                    actualAmount += (Float) reportBP.processActualPrice(reportTO);
                    System.out.println("actualAmount" + actualAmount);
                }
                report.setPrice(actualAmount);
                actualAmount = 0.0f;

            }

            if (rcExpenseAmount.size() != 0) {
                System.out.println("rcExpenseAmount  in controler=" + rcExpenseAmount.size());
                request.setAttribute("rcExpenseAmount", rcExpenseAmount);
            }
            System.out.println("normal exit");

        } catch (FPRuntimeException exception) {

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {

            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
//    bala

    public ModelAndView usageTypewiseReportData(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/LoginScreen.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList usageTypewiseSummary = new ArrayList();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        int MILLIS_IN_DAY = 15 * (1000 * 60 * 60 * 24);

        reportCommand = command;
        String path = "";
        int servicePtId = Integer.parseInt((String) session.getAttribute("companyId"));
        String companyId = ((String) session.getAttribute("companyId"));
        //int compId = Integer.parseInt(companyId);
        String menuPath = "Reports >> UsageTypewise Report";
        try {
            path = "content/report/usageTypewiseData.jsp";
            String fromDate = "";
            String toDate = "";
            if (reportCommand.getFromDate() == "") {
                toDate = (String) session.getAttribute("currentDate");
                System.out.println("MILLIS_IN_DAY=" + MILLIS_IN_DAY);
                fromDate = dateFormat.format(dateFormat.parse(toDate).getTime() - MILLIS_IN_DAY);
                fromDate = dateFormat.format(dateFormat.parse(fromDate).getTime() - MILLIS_IN_DAY);
            } else {
                fromDate = reportCommand.getFromDate();
                toDate = reportCommand.getToDate();
            }
            System.out.println("fromDate=" + fromDate);
            System.out.println("toDate=" + toDate);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ReportTO report = new ReportTO();
            report.setFromDate(fromDate);
            report.setToDate(toDate);
            report.setCompanyId(companyId);
            usageTypewiseSummary = reportBP.processUsageTypewiseData(report);
            System.out.println("usageTypewise  in controler=" + usageTypewiseSummary.size());
            request.setAttribute("usageTypewiseSummary", usageTypewiseSummary);
            request.setAttribute("summ", "Service Summary From " + fromDate + " to " + toDate);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

//    public ModelAndView serviceTypewiseReportData(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/LoginScreen.jsp");
//        }
//        HttpSession session = request.getSession();
//        ArrayList serviceTypewiseSummary = new ArrayList();
//        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
//        int MILLIS_IN_DAY = 15 * (1000 * 60 * 60 * 24);
//
//        reportCommand = command;
//        String path = "";
//        int servicePtId = Integer.parseInt((String) session.getAttribute("companyId"));
//        String companyId = ((String) session.getAttribute("companyId"));
//        //int compId = Integer.parseInt(companyId);
//        String menuPath = "Reports >> Service Typewise Report";
//        try {
//            path = "content/report/serviceTypewiseData.jsp";
//            String fromDate = "";
//            String toDate = "";
//            if (reportCommand.getFromDate() == "") {
//                toDate = (String) session.getAttribute("currentDate");
//                System.out.println("MILLIS_IN_DAY=" + MILLIS_IN_DAY);
//                fromDate = dateFormat.format(dateFormat.parse(toDate).getTime() - MILLIS_IN_DAY);
//                fromDate = dateFormat.format(dateFormat.parse(fromDate).getTime() - MILLIS_IN_DAY);
//            } else {
//                fromDate = reportCommand.getFromDate();
//                toDate = reportCommand.getToDate();
//            }
//            System.out.println("fromDate=" + fromDate);
//            System.out.println("toDate="+toDate);
//            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//            ReportTO report =new ReportTO();
//            report.setFromDate(fromDate);
//            report.setToDate(toDate);
//            report.setCompanyId(companyId);
//            serviceTypewiseSummary = reportBP.processServiceTypewiseSummary(report);
//            System.out.println("usageTypewise  in controler=" + serviceTypewiseSummary.size());
//            request.setAttribute("fromDate", fromDate);
//            request.setAttribute("toDate", toDate);
//            request.setAttribute("serviceTypewiseSummary", serviceTypewiseSummary);
//            request.setAttribute("summ", "Service Summary From " + fromDate + " to " + toDate);
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,exception.getErrorMessage());
//        }
//        catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }
//    bala ends
    public ModelAndView tallyXMLPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String fromDate = request.getParameter("fromDate");
        String toDate = request.getParameter("toDate");
        System.out.println("tallyXML Report in controller");
        String menuPath = "";
        String pageTitle = "tallyXML Page";
        request.setAttribute("pageTitle", pageTitle);
        ArrayList tallyXMLSummary = new ArrayList();
        menuPath = "Report >> TallyXML Report ";
        request.setAttribute("menuPath", menuPath);
        try {
            if (fromDate == null) {
                fromDate = (String) session.getAttribute("currentDate");
            }
            if (toDate == null) {
                toDate = (String) session.getAttribute("currentDate");
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            System.out.println((new StringBuilder()).append("fromDate=").append(fromDate).toString());
            System.out.println((new StringBuilder()).append("toDate=").append(toDate).toString());
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Mrs-List")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "content/report/tallyXMLPage.jsp";
                if (companyId == 1022) {
                    if (request.getParameter("compId") != null) {
                        companyId = Integer.parseInt(request.getParameter("compId"));
                    }
                    request.setAttribute("compId", companyId);
                    System.out.println("fromDate in controller:" + fromDate);
                    System.out.println("toDate in controller:" + toDate);
                    tallyXMLSummary = reportBP.processTallyXMLSummary(companyId, fromDate, toDate);
                    request.setAttribute("tallyXMLSummary", tallyXMLSummary);
                }
                System.out.println((new StringBuilder()).append("tallyXMLSummary size").append(tallyXMLSummary.size()).toString());
            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to tallyXMLPage --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView tallyXMLReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int tallyXMLReport = 0;
        String companyId = ((String) session.getAttribute("companyId"));
        reportCommand = command;
        String path = "";
        String dataType = null;
        String menuPath = "Reports >> TallyXML Generate ";
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/report/tallyXMLData.jsp";
            String fromDate = request.getParameter("fromDate");
            if (reportCommand.getFromDate() != null && !"".equals(reportCommand.getFromDate())) {
                System.out.println("From Date-->" + fromDate);
            }
            String toDate = request.getParameter("toDate");
            if (reportCommand.getToDate() != null && !"".equals(reportCommand.getToDate())) {
                System.out.println("to Date-->" + toDate);
            }
            String reqDate = request.getParameter("reqDate");
            if (reportCommand.getReqDate() != null && !"".equals(reportCommand.getReqDate())) {
                System.out.println("ReqDate-->" + reqDate);
            }
            if (reportCommand.getDataType() != null && !"".equals(reportCommand.getDataType())) {
                System.out.println("DataType-->" + reportCommand.getDataType());
                dataType = reportCommand.getDataType();
                ReportTO report = new ReportTO();
                report.setFromDate(fromDate);
                report.setToDate(toDate);
                report.setRequiredDate(reqDate);
                report.setDataType(Integer.parseInt(dataType));
//                if(request.getParameter("compId")!=null)
//                companyId = Integer.parseInt(request.getParameter("compId"));
                report.setCompanyId(companyId);
                tallyXMLReport = reportBP.processTallyXMLReport(report);
                request.setAttribute("tallyXMLReport", tallyXMLReport);
                System.out.println((new StringBuilder()).append("tallyXMLReport size").append(tallyXMLReport).toString());
                if (tallyXMLReport == 1) {
                    fromDate = (String) session.getAttribute("currentDate");
                    toDate = (String) session.getAttribute("currentDate");
                    request.setAttribute("fromDate", fromDate);
                    request.setAttribute("toDate", toDate);
                    path = "content/report/tallyXMLPage.jsp";
                    // System.out.println("inside tally 1");
                    //mv = tallyXMLPage(request, response, command);
                    request.setAttribute("successMessage", "Request Added Successfully");
                }
            }
            //  System.out.println("path.."+path);
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } //        catch (FPBusinessException exception) {
        //            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
        //            request.setAttribute("errorMessage", exception.getErrorMessage());
        //        }
        catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to tallyXMLReport --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView modifyTallyXMLPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String companyId = ((String) session.getAttribute("companyId"));
        reportCommand = command;
        ArrayList modifyTallyXMLPage = new ArrayList();
        String xmlId = null;
        String path = "";
        String menuPath = "Reports >> Modify TallyXML Page ";
        request.setAttribute("menuPath", menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Mrs-List")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "content/report/modifyTallyXMLPage.jsp";
                xmlId = request.getParameter("xmlId");
                System.out.println("xmlId-->" + xmlId);
                modifyTallyXMLPage = reportBP.processModifyTallyXMLPage(xmlId);
                request.setAttribute("modifyTallyXMLPage", modifyTallyXMLPage);
            }
            System.out.println((new StringBuilder()).append("modifyTallyXMLPage size").append(modifyTallyXMLPage.size()).toString());

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to modifyTallyXMLPage --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView modifyTallyXMLReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int modifyTallyXMLReport = 0;
        reportCommand = command;
        String path = "";
        String dataType = null;
        String xmlId = null;
        String menuPath = "Reports >> TallyXML Generate ";
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/report/modifyTallyXMLPage.jsp";
            xmlId = request.getParameter("xmlId");
            String fromDate = request.getParameter("fromDate");
            if (reportCommand.getFromDate() != null && !"".equals(reportCommand.getFromDate())) {
                System.out.println("From Date modify-->" + fromDate);
            }
            String toDate = request.getParameter("toDate");
            if (reportCommand.getToDate() != null && !"".equals(reportCommand.getToDate())) {
                System.out.println("to Date modify-->" + toDate);
            }
            String reqDate = request.getParameter("reqDate");
            if (reportCommand.getReqDate() != null && !"".equals(reportCommand.getReqDate())) {
                System.out.println("ReqDate modify-->" + reqDate);
            }
            if (reportCommand.getDataType() != null && !"".equals(reportCommand.getDataType())) {
                System.out.println("DataType modify-->" + reportCommand.getDataType());
                dataType = reportCommand.getDataType();
                ReportTO report = new ReportTO();
                report.setFromDate(fromDate);
                report.setToDate(toDate);
                report.setRequiredDate(reqDate);
                report.setDataType(Integer.parseInt(dataType));
                System.out.println("modify xmlId-->" + xmlId);
                System.out.println("modifyXMLReport in cont B4-->");
                modifyTallyXMLReport = reportBP.processModifyTallyXMLReport(report, xmlId);
                request.setAttribute("modifyTallyXMLReport", modifyTallyXMLReport);
                System.out.println((new StringBuilder()).append("modifyTallyXMLReport size").append(modifyTallyXMLReport).toString());
                if (modifyTallyXMLReport == 1) {
                    fromDate = (String) session.getAttribute("currentDate");
                    toDate = (String) session.getAttribute("currentDate");
                    request.setAttribute("fromDate", fromDate);
                    request.setAttribute("toDate", toDate);
                    path = "content/report/tallyXMLPage.jsp";
                    request.setAttribute("successMessage", "TallyXML Modified Successfully");
                }
            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to tallyXMLReport --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void handleDriverSettlement(HttpServletRequest request, HttpServletResponse response) throws IOException {
        System.out.println("i am in ajax ");
        HttpSession session = request.getSession();
        String driName = request.getParameter("driName");
        System.out.println("driName" + driName);
        String suggestions = reportBP.handleDriverSettlement(driName);
        System.out.println("suggestions" + suggestions);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.println(suggestions);
        writer.close();
    }

    public void handleVehicleNo(HttpServletRequest request, HttpServletResponse response) throws IOException {
        System.out.println("i am in ajax ");
        HttpSession session = request.getSession();
        String regno = request.getParameter("regno");
        System.out.println("regno" + regno);
        String suggestions = reportBP.handleVehicleNo(regno);
        System.out.println("suggestions" + suggestions);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.println(suggestions);
        writer.close();
    }

    //Rathimeena 
    public ModelAndView driverSettlementReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList settlementReport = new ArrayList();
        ReportTO reportTO = new ReportTO();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> Driver Settlement Report ";
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/report/driverSettlementReport.jsp";

            if (reportCommand.getFromDate() != null && !"".equals(reportCommand.getFromDate())) {
                reportTO.setFromDate(reportCommand.getFromDate());
            }
            if (reportCommand.getToDate() != null && !"".equals(reportCommand.getToDate())) {
                reportTO.setToDate(reportCommand.getToDate());
            }
            if (reportCommand.getDriName() != null && !"".equals(reportCommand.getDriName())) {
                System.out.println("reportCommand.getDriName(): " + reportCommand.getDriName());
                String[] dName = reportCommand.getDriName().split("\\.");
                //System.out.println("dName: "+dName[0]);
                /*if(!"".equals(dName[0])){
                 System.out.println("if");
                 reportTO.setDriName(dName[0]);
                 } else{
                 System.out.println("else");
                 String[] hName = reportCommand.getDriName().split("-");
                 reportTO.setDriName(hName[0]);
                 }*/
            }
            System.out.println("reportTO.getDriName : " + reportTO.getDriName());

            if (reportCommand.getRegno() != null && !"".equals(reportCommand.getRegno())) {
                reportTO.setRegisterNo(reportCommand.getRegno());
            }
            if (reportCommand.getSettlementId() != null && !"".equals(reportCommand.getSettlementId())) {
                request.setAttribute("settleId", reportCommand.getSettlementId());
                System.out.println("reportCommand.getSettlementId() = " + reportCommand.getSettlementId());
            }
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            String regNo = request.getParameter("regno");
            String driName = request.getParameter("driName");

            String[] driNameId = null;
            driNameId = driName.split("-");
            int driId = Integer.parseInt(driNameId[1]);
            System.out.println("driName1: " + driName);
            String tripIds = "";
            OperationTO operationTO = null;
            System.out.println("driId: " + driId);
            ArrayList searchProDriverSettlement = new ArrayList();
            searchProDriverSettlement = reportBP.searchProDriverSettlement(fromDate, toDate, regNo, driId);
            request.setAttribute("searchProDriverSettlement", searchProDriverSettlement);
            System.out.println("searchProDriverSettlement.size() : " + searchProDriverSettlement.size());
            if (searchProDriverSettlement.size() != 0) {
                ArrayList cleanerTrip = new ArrayList();
                cleanerTrip = reportBP.cleanerTripDetails(fromDate, toDate, regNo, driId);
                request.setAttribute("cleanerTrip", cleanerTrip);
                System.out.println("cleanerTrip.size() : " + cleanerTrip.size());

                ArrayList fixedExpDetails = new ArrayList();
                fixedExpDetails = reportBP.getFixedExpDetails(fromDate, toDate, regNo, driId);
                request.setAttribute("fixedExpDetails", fixedExpDetails);
                System.out.println("fixedExpDetails.size() : " + fixedExpDetails.size());

                ArrayList driverExpDetails = new ArrayList();
                driverExpDetails = reportBP.getDriverExpDetails(fromDate, toDate, regNo, driId);
                request.setAttribute("driverExpDetails", driverExpDetails);
                System.out.println("driverExpDetails.size() : " + driverExpDetails.size());

                ArrayList AdvDetails = new ArrayList();
                AdvDetails = reportBP.getAdvDetails(fromDate, toDate, regNo, driId, tripIds);

                request.setAttribute("AdvDetails", AdvDetails);
                System.out.println("AdvDetails.size() : " + AdvDetails.size());

                ArrayList fuelDetails = new ArrayList();
                fuelDetails = reportBP.getFuelDetails(fromDate, toDate, regNo, driId);
                request.setAttribute("fuelDetails", fuelDetails);
                System.out.println("fuelDetails.size() : " + fuelDetails.size());

                ArrayList haltDetails = new ArrayList();
                haltDetails = reportBP.getHaltDetails(fromDate, toDate, regNo, driId);
                request.setAttribute("haltDetails", haltDetails);
                System.out.println("haltDetails.size() : " + haltDetails.size());

                ArrayList remarkDetails = new ArrayList();
                remarkDetails = reportBP.getRemarkDetails(fromDate, toDate, regNo, driId);
                request.setAttribute("remarkDetails", remarkDetails);
                System.out.println("remarkDetails.size() : " + remarkDetails.size());

                ArrayList summaryDetails = new ArrayList();
                summaryDetails = reportBP.getSummaryDetails(fromDate, toDate, regNo, driId);
                request.setAttribute("summaryDetails", summaryDetails);
                System.out.println("summaryDetails.size() : " + summaryDetails.size());

                int clenaerCount = 0;
                clenaerCount = reportBP.getTripCountDetails(fromDate, toDate, regNo, driId);
                request.setAttribute("clenaerCount", clenaerCount);
                System.out.println("clenaerCount: " + clenaerCount);
                System.out.println("test1");

                int totalTonnage = 0;
                totalTonnage = reportBP.getTotalTonnageDetails(fromDate, toDate, regNo, driId);
                request.setAttribute("totalTonnage", totalTonnage);
                System.out.println("totalTonnage : " + totalTonnage);

                int outKm = 0;
                outKm = reportBP.getOutKmDetails(fromDate, toDate, regNo, driId);
                request.setAttribute("outKm", outKm);
                System.out.println("outKm: " + outKm);

                int inKm = 0;
                inKm = reportBP.getInKmDetails(fromDate, toDate, regNo, driId);
                request.setAttribute("inKm", inKm);
                System.out.println("inKm: " + inKm);

                ArrayList fuelDetail = new ArrayList();
                double totalFuel = 0d, totalFuelAmount = 0d;
                fuelDetail = reportBP.getTotFuelDetails(fromDate, toDate, regNo, driId);
                if (fuelDetail.size() != 0) {
                    Iterator it = fuelDetail.iterator();
                    operationTO = new OperationTO();
                    while (it.hasNext()) {
                        operationTO = (OperationTO) it.next();
                        totalFuel = Double.parseDouble(operationTO.getTotalFuel());
                        totalFuelAmount = Double.parseDouble(operationTO.getTotalFuelAmount());
                    }
                }
                request.setAttribute("totalFuel", totalFuel);
                request.setAttribute("totalFuelAmount", totalFuelAmount);

                int driExpense = 0;
                driExpense = reportBP.getDriverExpenseDetails(fromDate, toDate, regNo, driId);
                request.setAttribute("driExpense", driExpense);
                System.out.println("driExpense: " + driExpense);

                int driSalary = 0;
                driSalary = reportBP.getDriverSalaryDetails(fromDate, toDate, regNo, driId);
                request.setAttribute("driSalary", driSalary);
                System.out.println("driSalary: " + driSalary);

                int genExpense = 0;
                genExpense = reportBP.getGeneralExpenseDetails(fromDate, toDate, regNo, driId);
                request.setAttribute("genExpense", genExpense);
                System.out.println("genExpense: " + genExpense);

                int driAdvance = 0;
                driAdvance = reportBP.getDriverAdvanceDetails(fromDate, toDate, regNo, driId);
                request.setAttribute("driAdvance", driAdvance);

                int driBata = 0;
                driBata = reportBP.getDriverBataDetails(fromDate, toDate, regNo, driId);
                request.setAttribute("driBata", driBata);
            } else {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found...");
            }
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("driName", reportCommand.getDriName());
            request.setAttribute("regno", reportCommand.getRegno());

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to Driver Settlement Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /*public ModelAndView driverSettlementReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
     if (request.getSession().isNew()) {
     return new ModelAndView("content/common/login.jsp");
     }
     HttpSession session = request.getSession();
     ArrayList settlementReport = new ArrayList();
     ReportTO reportTO = new ReportTO();
     reportCommand = command;
     String path = "";
     String menuPath = "Reports >> Driver Settlement Report ";
     request.setAttribute("menuPath", menuPath);
     try {
     path = "content/report/SettlementReport.jsp";
    
     if(reportCommand.getFromDate()!=null && !"".equals(reportCommand.getFromDate())){
     reportTO.setFromDate(reportCommand.getFromDate());
     }
     if(reportCommand.getToDate()!=null && !"".equals(reportCommand.getToDate())){
     reportTO.setToDate(reportCommand.getToDate());
     }
     if(reportCommand.getDriName()!=null && !"".equals(reportCommand.getDriName())){
     System.out.println("reportCommand.getDriName(): "+reportCommand.getDriName());
     String[] dName = reportCommand.getDriName().split("\\.");
     //System.out.println("dName: "+dName[0]);
     if(!"".equals(dName[0])){
     System.out.println("if");
     reportTO.setDriName(dName[0]);
     } else{
     System.out.println("else");
     String[] hName = reportCommand.getDriName().split("-");
     reportTO.setDriName(hName[0]);
     }
     }
     System.out.println("reportTO.getDriName : "+ reportTO.getDriName());
    
     if(reportCommand.getRegno()!=null && !"".equals(reportCommand.getRegno())){
     reportTO.setRegisterNo(reportCommand.getRegno());
     }
    
     settlementReport = reportBP.driverSettlementReport(reportTO);
    
     if(settlementReport.size()!=0){
     request.setAttribute("settlementReport", settlementReport);
     }
     request.setAttribute("fromDate", reportCommand.getFromDate());
     request.setAttribute("toDate", reportCommand.getToDate());
     request.setAttribute("driName", reportCommand.getDriName());
     request.setAttribute("regno", reportCommand.getRegno());
    
     }catch (FPRuntimeException exception) {
     FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
     return new ModelAndView("content/common/error.jsp");
     }catch (FPBusinessException exception) {
     FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
     request.setAttribute("errorMessage", exception.getErrorMessage());
     }
     catch (Exception exception) {
     exception.printStackTrace();
     FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to Driver Settlement Report --> ").append(exception).toString());
     return new ModelAndView("content/common/error.jsp");
     }
     return new ModelAndView(path);
     }*/
    public ModelAndView viewDriverSettlement(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Driver Settlement";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Driver Settlement >> Driver Settlement";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/Driver/ViewDriverSettlement.jsp";
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            System.out.println("size is  " + userFunctions.size());
            /*if (!loginBP.checkAuthorisation(userFunctions, "ProceedtoDriverSettlement-Add")) {
             path = "content/common/NotAuthorized.jsp";
             } else {*/
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } /*catch (FPBusinessException exception) {
        
         FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
         request.setAttribute(ParveenErrorConstants.ERROR_KEY,
         exception.getErrorMessage());
         }*/ catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Proceed to Driver Settlement--> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /*public ModelAndView statusDriverSettlement(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
     if (request.getSession().isNew()) {
     return new ModelAndView("content/common/login.jsp");
     }
     HttpSession session = request.getSession();
     ArrayList settlementReport = new ArrayList();
     ReportTO report = new ReportTO();
     reportCommand = command;
     String path = "";
     String menuPath = "Reports >> Driver Settlement Report ";
     request.setAttribute("menuPath", menuPath);
     try {
     path = "content/Driver/ViewDriverSettlement.jsp";
     ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
     System.out.println("size is  " + userFunctions.size());
    
     String fromDate = request.getParameter("fromDate");
     String toDate = request.getParameter("toDate");
     String regNo = request.getParameter("regno");
     String driName = request.getParameter("driName");
    
     ArrayList searchProDriverSettlement = new ArrayList();
     searchProDriverSettlement = reportBP.searchProDriverSettlement(fromDate, toDate, regNo, driName);
     request.setAttribute("searchProDriverSettlement", searchProDriverSettlement);
    
     ArrayList fixedExpDetails = new ArrayList();
     fixedExpDetails = reportBP.getFixedExpDetails(fromDate, toDate, regNo, driName);
     request.setAttribute("fixedExpDetails", fixedExpDetails);
    
     ArrayList driverExpDetails = new ArrayList();
     driverExpDetails = reportBP.getDriverExpDetails(fromDate, toDate, regNo, driName);
     request.setAttribute("driverExpDetails", driverExpDetails);
    
     ArrayList AdvDetails = new ArrayList();
     AdvDetails = reportBP.getAdvDetails(fromDate, toDate, regNo, driName);
     request.setAttribute("AdvDetails", AdvDetails);
    
     ArrayList fuelDetails = new ArrayList();
     fuelDetails = reportBP.getFuelDetails(fromDate, toDate, regNo, driName);
     request.setAttribute("fuelDetails", fuelDetails);
    
     ArrayList haltDetails = new ArrayList();
     haltDetails = reportBP.getHaltDetails(fromDate, toDate, regNo, driName);
     request.setAttribute("haltDetails", haltDetails);
    
     ArrayList remarkDetails = new ArrayList();
     remarkDetails = reportBP.getRemarkDetails(fromDate, toDate, regNo, driName);
     request.setAttribute("remarkDetails", remarkDetails);
    
     ArrayList settleColseDetails = new ArrayList();
     settleColseDetails = reportBP.getSettleCloseDetails(fromDate, toDate, regNo, driName);
     request.setAttribute("settleColseDetails", settleColseDetails);
    
     int tripCount=0;
     tripCount = operationBP.getTripCountDetails(fromDate, toDate, regNo, driName);
     request.setAttribute("tripCount", tripCount);
     System.out.println("tripCount: "+tripCount);
    
     int totalTonnage=0;
     totalTonnage = operationBP.getTotalTonnageDetails(fromDate, toDate, regNo, driName);
     request.setAttribute("totalTonnage", totalTonnage);
     System.out.println("totalTonnage: "+totalTonnage);
    
     int outKm=0;
     outKm = operationBP.getOutKmDetails(fromDate, toDate, regNo, driName);
     request.setAttribute("outKm", outKm);
     System.out.println("outKm: "+outKm);
    
     int inKm=0;
     inKm = operationBP.getInKmDetails(fromDate, toDate, regNo, driName);
     request.setAttribute("inKm", inKm);
     System.out.println("inKm: "+inKm);
    
     int totFuel=0;
     totFuel = operationBP.getTotFuelDetails(fromDate, toDate, regNo, driName);
     request.setAttribute("totFuel", totFuel);
     System.out.println("totFuel: "+totFuel);
    
     int driExpense=0;
     driExpense = operationBP.getDriverExpenseDetails(fromDate, toDate, regNo, driName);
     request.setAttribute("driExpense", driExpense);
     System.out.println("driExpense: "+driExpense);
    
     int genExpense=0;
     genExpense = operationBP.getGeneralExpenseDetails(fromDate, toDate, regNo, driName);
     request.setAttribute("genExpense", genExpense);
     System.out.println("genExpense: "+genExpense);
    
     int driAdvance=0;
     driAdvance = operationBP.getDriverAdvanceDetails(fromDate, toDate, regNo, driName);
     request.setAttribute("driAdvance", driAdvance);
    
     int driBata=0;
     driBata = operationBP.getDriverBataDetails(fromDate, toDate, regNo, driName);
     request.setAttribute("driBata", driBata);
    
     request.setAttribute("fromDate", fromDate);
     request.setAttribute("toDate", toDate);
     request.setAttribute("regNo", regNo);
     request.setAttribute("driName", driName);
     }
     } catch (FPRuntimeException exception) {
    
     FPLogUtils.fpErrorLog("Run time exception --> " + exception);
     return new ModelAndView("content/common/error.jsp");
     } catch (Exception exception) {
     exception.printStackTrace();
     FPLogUtils.fpErrorLog("Failed to searchProDriverSettlement--> " + exception);
     return new ModelAndView("content/common/error.jsp");
     }
     return new ModelAndView(path);
     }*/
    /**
     * This method used to view Company Wise Report Page.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView companyWiseReportPage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList customerList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> Company Wise Trip Report";
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/report/companyWiseTripReport.jsp";

            customerList = reportBP.getCustomerList();
            request.setAttribute("customerList", customerList);

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to tallyXMLReport --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to view Company Wise Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView companyWiseReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList customerList = new ArrayList();
        ArrayList companyWiseTripReport = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> Company Wise Trip Report";
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/report/companyWiseTripReport.jsp";

            customerList = operationBP.getCustomerTypes();
            request.setAttribute("customerList", customerList);

            if (reportCommand.getCompanyId() != null && !"".equals(reportCommand.getCompanyId())) {
                report.setCompanyId(reportCommand.getCompanyId());
            }
            String[] temp = null;
            String frDate = "", tDt = "";
            if (reportCommand.getFromDate() != "" && reportCommand.getToDate() != "") {
                /*temp = reportCommand.getFromDate().split("-");
                 String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
                 frDate = sDate;
                
                 temp = reportCommand.getToDate().split("-");
                 String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
                 tDt = eDate;*/
                report.setFromDate(reportCommand.getFromDate());
                report.setToDate(reportCommand.getToDate());
            }
            System.out.println("reportCommand.getCompanyId(): " + reportCommand.getCompanyId());
            System.out.println("reportCommand.getFromDate(): " + reportCommand.getFromDate());
            System.out.println("reportCommand.getToDate(): " + reportCommand.getToDate());
            companyWiseTripReport = reportBP.getCompanyWiseTripReport(report);
            if (companyWiseTripReport.size() != 0) {
                request.setAttribute("companyWiseTripReport", companyWiseTripReport);
            }
            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to tallyXMLReport --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to view Vehicle Performance Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView vehiclePerformancePage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList vehicleList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> Vehicle Performance Report";
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/report/vehiclePerformance.jsp";

            vehicleList = reportBP.getVehicleList();
            request.setAttribute("vehicleList", vehicleList);
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to tallyXMLReport --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to view Company Wise Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView vehiclePerformanceReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList vehicleList = new ArrayList();
        ArrayList vehiclePerformance = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> Vehicle Performance Report";
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/report/vehiclePerformance.jsp";

            vehicleList = reportBP.getVehicleList();
            request.setAttribute("vehicleList", vehicleList);

            if (reportCommand.getRegNo() != null && !"".equals(reportCommand.getRegNo())) {
                report.setRegNo(reportCommand.getRegNo());
            }
            String[] temp = null;
            String frDate = "", tDt = "";
            if (reportCommand.getFromDate() != "" && reportCommand.getToDate() != "") {
                /*temp = reportCommand.getFromDate().split("-");
                 String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
                 frDate = sDate;
                
                 temp = reportCommand.getToDate().split("-");
                 String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
                 tDt = eDate;*/
                report.setFromDate(reportCommand.getFromDate());
                report.setToDate(reportCommand.getToDate());
            }
            if (frDate != null && !"".equals(frDate)) {
            }
            if (tDt != null && !"".equals(tDt)) {
            }
            vehiclePerformance = reportBP.getVehiclePerformance(report);
            if (vehiclePerformance.size() != 0) {
                request.setAttribute("vehiclePerformance", vehiclePerformance);
            }
            request.setAttribute("regNo", reportCommand.getRegNo());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to tallyXMLReport --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to view Vehicle Performance Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView vehicleCurrentStatus(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList vehicleList = new ArrayList();
        ArrayList locationList = new ArrayList();
        ArrayList opLocation = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> Vehicle Performance Report";
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/report/vehicleCurrentStatus.jsp";

            locationList = reportBP.getlocationList();
            request.setAttribute("locationList", locationList);

            vehicleList = reportBP.getVehicleList();
            request.setAttribute("vehicleList", vehicleList);

            opLocation = operationBP.getOperationLocation();
            request.setAttribute("opLocation", opLocation);

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to tallyXMLReport --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to view Vehicle Current Status Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView vehicleCurrentStatusReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList vehicleCurrentStatus = new ArrayList();
        ReportTO reportTO = new ReportTO();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> Vehicle Current Status Report";
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/report/vehicleCurrentStatus.jsp";

            String[] temp = null;
            String frDate = "";
            if (reportCommand.getStatusDate() != "" && reportCommand.getStatusDate() != "") {
                /*temp = reportCommand.getStatusDate().split("-");
                 String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
                 frDate = sDate;*/
                reportTO.setStatusDate(reportCommand.getStatusDate());
            }
            /*if(frDate!=null && !"".equals(frDate)){
             reportTO.setStatusDate(frDate);
             }*/
            if (reportCommand.getStatusId() != null && !"".equals(reportCommand.getStatusId())) {
                reportTO.setStatusId(reportCommand.getStatusId());
            }
            if (reportCommand.getRegno() != null && !"".equals(reportCommand.getRegno())) {
                reportTO.setRegisterNo(reportCommand.getRegno());
            }
            if (reportCommand.getLocationId() != null && !"".equals(reportCommand.getLocationId())) {
                reportTO.setLocationId(reportCommand.getLocationId());
            }
            vehicleCurrentStatus = reportBP.getVehicleCurrentStatus(reportTO);
            request.setAttribute("vehicleCurrentStatus", vehicleCurrentStatus);

            ArrayList opLocation = new ArrayList();
            opLocation = operationBP.getOperationLocation();
            request.setAttribute("opLocation", opLocation);

            System.out.println("reportCommand.getStatusDate(): " + reportCommand.getStatusDate());
            System.out.println("reportCommand.getStatusId(): " + reportCommand.getStatusId());
            System.out.println("reportCommand.getRegno(): " + reportCommand.getRegno());
            System.out.println("reportCommand.getLocationId(): " + reportCommand.getLocationId());
            request.setAttribute("statusDate", reportCommand.getStatusDate());
            request.setAttribute("statusId", reportCommand.getStatusId());
            request.setAttribute("regno", reportCommand.getRegno());
            request.setAttribute("loctId", reportCommand.getLocationId());

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to tallyXMLReport --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to view Vehicle Performance Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView driverPerformancePage(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList vehicleList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> Driver Performance Report";
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/report/driverPerformance.jsp";

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to tallyXMLReport --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to view Company Wise Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView driverPerformanceReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList vehicleList = new ArrayList();
        ArrayList driverPerformance = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> Driver Performance Report";
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/report/driverPerformance.jsp";

            if (reportCommand.getDriName() != null && !"".equals(reportCommand.getDriName())) {
                /*String[] dName = reportCommand.getDriName().split("\\.");
                 //System.out.println("dName: "+dName[0]);
                 if(!"".equals(dName[0])){
                 System.out.println("if");
                 report.setDriName(dName[0]);
                 } else{
                 System.out.println("else");
                 String[] hName = reportCommand.getDriName().split("-");
                 report.setDriName(hName[0]);
                 }
                 String[] driId = reportCommand.getDriName().split("-");
                 report.setDriId(driId[1]);*/
                String[] driNameId = null;
                driNameId = reportCommand.getDriName().split("-");
                int driId = Integer.parseInt(driNameId[1]);
                System.out.println("driName1: " + reportCommand.getDriName());
                report.setDriverId(driId);
            }

            String[] temp = null;
            String frDate = "", tDt = "";
            if (reportCommand.getFromDate() != "" && reportCommand.getToDate() != "") {
                /*temp = reportCommand.getFromDate().split("-");
                 String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
                 frDate = sDate;
                
                 temp = reportCommand.getToDate().split("-");
                 String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
                 tDt = eDate;*/
                report.setFromDate(reportCommand.getFromDate());
                report.setToDate(reportCommand.getToDate());
            }
            driverPerformance = reportBP.getDriverPerformance(report);
            if (driverPerformance.size() != 0) {
                request.setAttribute("driverPerformance", driverPerformance);
            }
            String driEmbarked = reportBP.getDriverEmbarkedDate(report);
            request.setAttribute("driEmbarked", driEmbarked);

            request.setAttribute("driName", reportCommand.getDriName());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to driverPerformanceReport --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to LPS Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView lpsReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList vehicleList = new ArrayList();
        ArrayList driverPerformance = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> LPS Report";
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/report/LPSReport.jsp";

            String driEmbarked = reportBP.getDriverEmbarkedDate(report);
            request.setAttribute("driEmbarked", driEmbarked);

            request.setAttribute("driName", reportCommand.getDriName());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to driverPerformanceReport --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView displyLpsReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList lpsCount = new ArrayList();
        ArrayList lpsTrippedList = new ArrayList();
        ArrayList tripList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> LPS Report";
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/report/displyLpsReport.jsp";

            String[] temp = null;
            String frDate = "", tDt = "";
            if (reportCommand.getFromDate() != "" && reportCommand.getToDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                report.setToDate(reportCommand.getToDate());
            }
            if (reportCommand.getLpsId() != "" && reportCommand.getLpsId() != "") {
                report.setLpsId(reportCommand.getLpsId());
            }
            lpsCount = reportBP.getlpsCount(report);
            request.setAttribute("lpsCount", lpsCount);

            lpsTrippedList = reportBP.getlpsTrippedList(report);
            request.setAttribute("lpsTrippedList", lpsTrippedList);

            tripList = reportBP.gettripList(report);
            request.setAttribute("tripList", tripList);

            request.setAttribute("lpsId", reportCommand.getLpsId());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to driverPerformanceReport --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to trip Wise PandL Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView tripWisePandLReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList locationList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        System.out.println("userId = " + userId);
        String menuPath = "Reports >> LPS Report";
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/report/tripWisePandL.jsp";

            locationList = reportBP.getlocationList();
            request.setAttribute("locationList", locationList);

            request.setAttribute("locationId", reportCommand.getLocationId());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to driverPerformanceReport --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView displyTripWisePandL(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList tripWiseList = new ArrayList();
        ArrayList tripWiseTotal = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String sCompanyID = (String) request.getParameter("sCompanyID");
        String menuPath = "Reports >> Trip Wise Profit and loss Report";
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/report/displyTripWisePandL.jsp";

            String[] temp = null;
            String frDate = "", tDt = "";
            if (reportCommand.getFromDate() != "" && reportCommand.getToDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                report.setToDate(reportCommand.getToDate());
                report.setCompanyId(reportCommand.getCompanyId());
            }

            tripWiseList = reportBP.getTripWisePandL(report, userId);
            request.setAttribute("tripWiseList", tripWiseList);

            tripWiseTotal = reportBP.getTripWisetotal(report, userId);
            request.setAttribute("tripWiseTotal", tripWiseTotal);
//
//            lpsTrippedList = reportBP.getlpsTrippedList(report);
//            request.setAttribute("lpsTrippedList", lpsTrippedList);
//
//            tripList = reportBP.gettripList(report);
//            request.setAttribute("tripList", tripList);
//
            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("sCompanyID", sCompanyID);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to driverPerformanceReport --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to Vehicle Wise PandL Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView VehicleWisePandL(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList locationList = new ArrayList();
        ArrayList vehicleList = new ArrayList();
        ArrayList driverPerformance = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "";
        String menuPath = "Reports >> LPS Report";
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/report/VehicleWisePandL.jsp";

            ArrayList vehicleRegNos = new ArrayList();
            vehicleRegNos = vehicleBP.getVehicleRegNos();
            System.out.println("vehicleRegNos " + vehicleRegNos.size());
            request.setAttribute("vehicleRegNos", vehicleRegNos);

            locationList = reportBP.getlocationList();
            request.setAttribute("locationList", locationList);

            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to driverPerformanceReport --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView displyVehicleWisePandL(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList vehicleWiseList = new ArrayList();
        ArrayList vehicleWiseTotal = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String sVehicleNo = (String) request.getParameter("sVehicleNo");
        System.out.println("sVehicleNo = " + sVehicleNo);
        String sCompanyID = (String) request.getParameter("sCompanyID");
        System.out.println("sCompanyID = " + sCompanyID);
        String sVehicleType = (String) request.getParameter("sVehicleType");
        System.out.println("sVehicleType= " + sVehicleType);

        String menuPath = "Reports >> Vehicle Wise Report";
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/report/displyVehicleWisePandL.jsp";

            String[] temp = null;
            String frDate = "", tDt = "";
            if (reportCommand.getFromDate() != "" && reportCommand.getToDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                report.setToDate(reportCommand.getToDate());
                report.setVehicleId(reportCommand.getVehicleId());
                report.setCompanyId(reportCommand.getCompanyId());
                report.setVehicleType(reportCommand.getVehicleType());
            }

            ArrayList vehicleRegNos = new ArrayList();
            vehicleRegNos = vehicleBP.getVehicleRegNos();
            System.out.println("vehicleRegNos " + vehicleRegNos.size());
            request.setAttribute("vehicleRegNos", vehicleRegNos);

            ArrayList locationList = new ArrayList();
            locationList = reportBP.getlocationList();
            request.setAttribute("locationList", locationList);

            vehicleWiseList = reportBP.getVehicleWisePandL(report, userId);
            request.setAttribute("vehicleWiseList", vehicleWiseList);

            vehicleWiseTotal = reportBP.getVehicleWisetotal(report, userId);
            request.setAttribute("vehicleWiseTotal", vehicleWiseTotal);

            request.setAttribute("companyId", reportCommand.getCompanyId());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("vehicleId", reportCommand.getVehicleId());
            request.setAttribute("sVehicleNo", sVehicleNo);
            request.setAttribute("sCompanyID", sCompanyID);
            request.setAttribute("sVehicleType", sVehicleType);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to driverPerformanceReport --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    // Hari Code Starts

    public ModelAndView districtWiseReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList districtNameList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");

        String menuPath = "Reports >> District Wise Report";
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/report/displayDistrictWise.jsp";
            districtNameList = reportBP.getDistrictNameList();
            request.setAttribute("districtNameList", districtNameList);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to displayDistrictWise Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView displayDistrictWiseSummary(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList districtNameList = new ArrayList();
        ArrayList districtSummaryList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", ownerShip = "", district = "";
        String consignmentType = "";
        int userId = (Integer) session.getAttribute("userId");
        String distcheck = request.getParameter("distcheck");
        // System.out.println("distcheck = " + distcheck);

        String menuPath = "Reports >> District Wise Report";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/displayDistrictWiseExcelview.jsp";

            } else {
                // System.out.println("222 = ");
                path = "content/report/displayDistrictWise.jsp";

            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getOwnership() != null && reportCommand.getOwnership() != "") {
                report.setOwnership(reportCommand.getOwnership());
                ownerShip = reportCommand.getOwnership();
            }
            if (reportCommand.getDistrict() != null && reportCommand.getDistrict() != "") {
                report.setDistrict(reportCommand.getDistrict());
                district = reportCommand.getDistrict();
            }
            if (reportCommand.getConsignmentType() != null && reportCommand.getConsignmentType() != "") {
                report.setConsignmentType(reportCommand.getConsignmentType());
                consignmentType = reportCommand.getConsignmentType();
            }
            districtSummaryList = reportBP.getDistrictSummaryList(report, userId);
            if (districtSummaryList.size() == 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found.");
            }
            System.out.println("district is::" + district);
            districtNameList = reportBP.getDistrictNameList();
            request.setAttribute("districtNameList", districtNameList);
            request.setAttribute("districtSummaryList", districtSummaryList);
            request.setAttribute("distcheck", distcheck);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("ownerShip", ownerShip);
            request.setAttribute("district", district);
            request.setAttribute("consignmentType", consignmentType);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to displayDistrictWise Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView displayDistrictWiseDetails(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList districtNameList = new ArrayList();
        ArrayList districtDetailsList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", ownerShip = "", district = "";
        String consignmentType = "";
        int userId = (Integer) session.getAttribute("userId");
        String distcheck = request.getParameter("distcheck");
        // System.out.println("distcheck = " + distcheck);

        String menuPath = "Reports >> District Wise Report";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");

            if (param.equals("ExportExcel")) {
                // System.out.println("111 = ");
                path = "content/report/displayDistrictWiseExcelview.jsp";

            } else {
                // System.out.println("222 = ");
                path = "content/report/displayDistrictWise.jsp";

            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getOwnership() != null && reportCommand.getOwnership() != "") {
                report.setOwnership(reportCommand.getOwnership());
                ownerShip = reportCommand.getOwnership();
            }
            if (reportCommand.getDistrict() != null && reportCommand.getDistrict() != "") {
                report.setDistrict(reportCommand.getDistrict());
                district = reportCommand.getDistrict();
            }
            if (reportCommand.getConsignmentType() != null && reportCommand.getConsignmentType() != "") {
                report.setConsignmentType(reportCommand.getConsignmentType());
                consignmentType = reportCommand.getConsignmentType();
            }
            districtDetailsList = reportBP.getDistrictDetailsList(report, userId);
            if (districtDetailsList.size() == 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found.");
            }
            districtNameList = reportBP.getDistrictNameList();
            request.setAttribute("districtNameList", districtNameList);
            request.setAttribute("districtDetailsList", districtDetailsList);
            request.setAttribute("distcheck", distcheck);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("ownerShip", ownerShip);
            request.setAttribute("district", district);
            request.setAttribute("consignmentType", consignmentType);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to displayDistrictWise Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView tripSheetDetailsReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();

        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");

        String menuPath = "Reports >> Trip Sheet Wise Report";
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/report/displayTripSheetWise.jsp";

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to tripSheetDetailsReport Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView displaySearchTripWiseDetails(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList tripSheetDetailsList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", ownerShip = "", consignmentType = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> TripSheet Wise Report";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/displayTripSheetWiseExcel.jsp";
            } else {
                //  System.out.println("222 = ");
                path = "content/report/displayTripSheetWise.jsp";
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getOwnership() != null && reportCommand.getOwnership() != "") {
                report.setOwnership(reportCommand.getOwnership());
                ownerShip = reportCommand.getOwnership();
            }
            if (reportCommand.getConsignmentType() != null && reportCommand.getConsignmentType() != "") {
                report.setConsignmentType(reportCommand.getConsignmentType());
                consignmentType = reportCommand.getConsignmentType();
            }
            tripSheetDetailsList = reportBP.getTripsheetDetailsList(report, userId);
            if (tripSheetDetailsList.size() == 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found.");
            }
            request.setAttribute("tripSheetDetailsList", tripSheetDetailsList);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("ownerShip", ownerShip);
            request.setAttribute("consignmentType", consignmentType);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to displaySearchTripWiseDetails Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView consigneeWiseDetailsReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();

        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");

        String menuPath = "Reports >> Consignee Wise Report";
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/report/displayConsigneetWise.jsp";

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to displayConsigneewiseDetails Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView displayConsigneewiseDetails(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList consigneeWiseDetailsList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", consignmentName = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Consignee Wise Report";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                // System.out.println("111 = ");
                path = "content/report/displayConsigneeWiseExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/displayConsigneetWise.jsp";
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getConsignmentName() != null && reportCommand.getConsignmentName() != "") {
                report.setConsigneeName(reportCommand.getConsignmentName());
                consignmentName = reportCommand.getConsignmentName();
            }
            consigneeWiseDetailsList = reportBP.getConsigneeWiseList(report, userId);
            if (consigneeWiseDetailsList.size() == 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found.");
            }

            request.setAttribute("consigneeWiseDetailsList", consigneeWiseDetailsList);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("consignmentName", consignmentName);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to displayConsigneewiseDetails Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView tripSettlementDetailsReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();

        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");

        String menuPath = "Reports >> Trip Settlement Wise Report";
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/report/displayTripSettlement.jsp";

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to tripSettlementDetailsReport Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView dieselReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();

        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");

        String menuPath = "Reports >> Diesel Report";
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/report/dieselReport.jsp";

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to dieselReport Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView dieselReportDetails(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList dieselReportList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", tripId = "", vehicleNo = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Trip Settlement Wise Report";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/dieselReportExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/dieselReport.jsp";
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getVehicleNo() != null && reportCommand.getVehicleNo() != "") {
                report.setVehicleNo(reportCommand.getVehicleNo());
                vehicleNo = reportCommand.getVehicleNo();
            }
            if (reportCommand.getTripId() != null && reportCommand.getTripId() != "") {
                report.setTripId(reportCommand.getTripId());
                tripId = reportCommand.getTripId();
            }
            dieselReportList = reportBP.getDieselReportList(report, userId);
            if (dieselReportList.size() == 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found.");
            }
            request.setAttribute("dieselReportList", dieselReportList);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("tripId", tripId);
            request.setAttribute("vehicleNo", vehicleNo);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to dieselReportList Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView displayTripsettlementwiseDetails(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList tripsettlementDetailsList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", tripId = "", vehicleNo = "";
        String ownerShip = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Trip Settlement Wise Report";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/displayTripSettlementExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/displayTripSettlement.jsp";
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getVehicleNo() != null && reportCommand.getVehicleNo() != "") {
                report.setVehicleNo(reportCommand.getVehicleNo());
                vehicleNo = reportCommand.getVehicleNo();
            }
            if (reportCommand.getTripId() != null && reportCommand.getTripId() != "") {
                report.setTripId(reportCommand.getTripId());
                tripId = reportCommand.getTripId();
            }
            if (reportCommand.getOwnership() != null && reportCommand.getOwnership() != "") {
                report.setOwnership(reportCommand.getOwnership());
                ownerShip = reportCommand.getOwnership();
            }

            tripsettlementDetailsList = reportBP.getTripsettlementWiseList(report, userId);
            if (tripsettlementDetailsList.size() == 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found.");
            }
            request.setAttribute("tripsettlementDetailsList", tripsettlementDetailsList);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("tripId", tripId);
            request.setAttribute("vehicleNo", vehicleNo);
            request.setAttribute("ownerShip", ownerShip);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to displayTripsettlementwiseDetails Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle ConsigneeName Suggestions..
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public void handleConsigneeNameSuggestions(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //System.out.println("i am in consignee name ajax ");
        HttpSession session = request.getSession();
        String consignmentName = request.getParameter("consignmentName");
        //System.out.println("consignmentName" + consignmentName);
        String suggestions = reportBP.getConsigneeNameSuggestions(consignmentName);
        //System.out.println("suggestions" + suggestions);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.println(suggestions);
        writer.close();

    }

    /**
     * This method used to handle Trip Id Suggestions..
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public void handleTripIdSuggestions(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //System.out.println("i am in consignee name ajax ");
        HttpSession session = request.getSession();
        String tripId = request.getParameter("tripId");
        //System.out.println("consignmentName" + consignmentName);
        String suggestions = reportBP.getTripIdSuggestions(tripId);
        //System.out.println("suggestions final" + suggestions);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.println(suggestions);
        writer.close();
    }

    /**
     * This method used to handle Vehicle Number Suggestions..
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public void handleVehicleNumberSuggestions(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //System.out.println("i am in consignee name ajax ");
        HttpSession session = request.getSession();
        String vehicleNo = request.getParameter("vehicleNo");
        //System.out.println("consignmentName" + consignmentName);
        String suggestions = reportBP.getVehicleNumberSuggestions(vehicleNo);
        //System.out.println("suggestions" + suggestions);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.println(suggestions);
        writer.close();

    }

//Brattle Foods Reports Starts Here
//Brattle Foods Reports Starts Here
    //Throttle Starts Here 13-12-2013
    /**
     * This method used to view accounts details for all not paid customer save
     * .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAccountsReceivableReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList accountReceivableList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", customerId = "";
        String ownerShip = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Vehicle Wise Profit Report";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/accountsReceivableExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/customerOverDueList.jsp";
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getCustomerId() != null && reportCommand.getCustomerId() != "") {
                report.setCustomerId(reportCommand.getCustomerId());
                customerId = reportCommand.getCustomerId();
            }
            String invoiceType = request.getParameter("invoiceType");
            report.setInvoiceType(invoiceType);
            System.out.println("invoiceType = " + invoiceType);
            request.setAttribute("invoiceType", invoiceType);
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("page", 1);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleAccountsReceivable Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView dashboardReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList accountReceivableList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Dashboard Report";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/dashboardReportExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/dashboardReport.jsp";
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("page", 1);

            ArrayList salesDashboard = new ArrayList();
            salesDashboard = reportBP.getSalesDashboard(report, userId);
            request.setAttribute("salesDashboard", salesDashboard);

            ArrayList opsDashboard = new ArrayList();
            opsDashboard = reportBP.getOpsDashboard(report, userId);
            request.setAttribute("opsDashboard", opsDashboard);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to dashboardReport Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to view accounts details for all not paid customer save
     * .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAccountsReceivable(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList accountReceivableList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", customerId = "";
        String ownerShip = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Accounts Receivable";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/customerOverDueListExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/accountsReceivable.jsp";
                path = "content/report/BrattleFoods/customerOverDueList.jsp";
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getCustomerId() != null && reportCommand.getCustomerId() != "") {
                report.setCustomerId(reportCommand.getCustomerId());
                customerId = reportCommand.getCustomerId();
            }
            String invoiceType = request.getParameter("invoiceType");
            report.setInvoiceType(invoiceType);
            System.out.println("invoiceType = " + invoiceType);
            request.setAttribute("invoiceType", invoiceType);

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);

            ArrayList customerOverDueList = new ArrayList();
            System.out.println("customerList Size Is :::" + customerList.size());
            if (!"".equals(fromDate) && !"".equals(toDate)) {
                customerOverDueList = reportBP.getCustomerOverDueList(report, userId);
                System.out.println("customerOverDueList.size() = " + customerOverDueList.size());
                if (customerOverDueList.size() == 0) {
                    request.setAttribute("customerOverDueListSize", 0);
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");
                } else {
                    request.setAttribute("customerOverDueList", customerOverDueList);
                }
                request.setAttribute("fromDate", fromDate);
                request.setAttribute("toDate", toDate);
                request.setAttribute("customerId", customerId);
            }

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleAccountsReceivable Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to view accounts details for all not paid customer save
     * .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAccountsReceivableDetails(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList accountReceivableList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", customerId = "";
        String ownerShip = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Accounts Receivable";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            System.out.println("param" + param);
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/accountsReceivableExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/accountsReceivable.jsp";
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            //            if (reportCommand.getCustomerId() != null && reportCommand.getCustomerId() != "") {
            //                report.setCustomerId(reportCommand.getCustomerId());
            //                customerId = reportCommand.getCustomerId();
            //            }
            if (reportCommand.getCustomerId() != null && reportCommand.getCustomerId() != "") {
                report.setCustomerId(reportCommand.getCustomerId());
                request.setAttribute("customerId", reportCommand.getCustomerId());
            } else {
                report.setCustomerId("");
            }
            System.out.println("customerId = " + reportCommand.getCustomerId());
            String invoiceType = request.getParameter("invoiceType");
            report.setInvoiceType(invoiceType);
            System.out.println("invoiceType = " + invoiceType);
            request.setAttribute("invoiceType", invoiceType);

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);

            ArrayList customerOverDueList = new ArrayList();
            System.out.println("customerList Size Is :::" + customerList.size());
            if (!"".equals(fromDate) && !"".equals(toDate)) {
                accountReceivableList = reportBP.getAccountReceivableList(report, userId);
                if (accountReceivableList.size() == 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found.");
                } else {
                    request.setAttribute("accountReceivableList", accountReceivableList);
                }
                request.setAttribute("fromDate", fromDate);
                request.setAttribute("toDate", toDate);
                request.setAttribute("customerId", reportCommand.getCustomerId());
                request.setAttribute("invoiceType", reportCommand.getInvoiceType());
            }

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleAccountsReceivable Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to view Vehicle Wise Profit Report Page .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleVehicleWiseProfitReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList accountReceivableList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", vehicleId = "", vehicleNo = "";
        String ownerShip = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Vehicle Wise Profit Report";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/vehicleWiseProfitExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/vehicleWiseProfit.jsp";
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getVehicleNo() != null && reportCommand.getVehicleNo() != "") {
                report.setVehicleId(reportCommand.getVehicleId());
                vehicleNo = reportCommand.getVehicleNo();
                vehicleId = reportCommand.getVehicleId();
            }
//            ArrayList processGetCompanyList = new ArrayList();
//            processGetCompanyList = companyBP.processGetCompanyList();
//            request.setAttribute("companyList", processGetCompanyList);

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("vehicleNo", vehicleNo);
            request.setAttribute("vehicleId", vehicleId);
            request.setAttribute("page", 1);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleAccountsReceivable Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to view vehicle wise profit report save .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleVehicleWiseProfit(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList vehicleDetailsList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", vehicleId = "", vehicleNo = "";
        String ownerShip = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Vehicle Wise Profit";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/vehicleWiseProfitExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/vehicleWiseProfit.jsp";
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getVehicleNo() != null && reportCommand.getVehicleNo() != "") {
                report.setVehicleId(reportCommand.getVehicleId());
                vehicleId = reportCommand.getVehicleId();
                vehicleNo = reportCommand.getVehicleNo();
            }

            String fleetCenterId = request.getParameter("fleetCenterId");
            String operationTypeId = request.getParameter("operationTypeId");
            String profitType = request.getParameter("profitType");
            report.setFleetCenterId(fleetCenterId);
            report.setOperationTypeId(operationTypeId);
            report.setProfitType(profitType);

            ArrayList processGetCompanyList = new ArrayList();
            processGetCompanyList = companyBP.processGetCompanyList();
            request.setAttribute("companyList", processGetCompanyList);

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            ArrayList vehicleWiseProfitList = new ArrayList();
            double netProfitTotal = 0;
            if (!"".equals(fromDate) && !"".equals(toDate)) {
                vehicleDetailsList = reportBP.getVehicleDetailsList(report, userId);
                if (vehicleDetailsList.size() == 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found.");
                    request.setAttribute("vehicleDetailsListSize", 0);
                } else {
                    request.setAttribute("vehicleDetailsList", vehicleDetailsList);
                }
                vehicleWiseProfitList = reportBP.getVehicleWiseProfitList(report, userId);
                System.out.println("vehicleWiseProfitList.size() = " + vehicleWiseProfitList.size());
                if (vehicleWiseProfitList.size() > 0) {
                    request.setAttribute("vehicleWiseProfitList", vehicleWiseProfitList);
                } else {
                    request.setAttribute("vehicleWiseProfitListSize", 0);
                }
                Iterator itr = vehicleWiseProfitList.iterator();
                ReportTO repTO = new ReportTO();
                int totalTrip = 0;
                double totalIncome = 0;
                double totalFixedExpense = 0;
                double totalOperationExpense = 0;
                double totalNetExpense = 0;

                while (itr.hasNext()) {
                    repTO = (ReportTO) itr.next();
                    totalIncome = Double.parseDouble(repTO.getFreightAmount());
                    netProfitTotal += repTO.getNetProfit();
                }
            }

            request.setAttribute("netProfitTotal", netProfitTotal);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("vehicleId", vehicleId);
            request.setAttribute("vehicleNo", vehicleNo);
            if (fleetCenterId != null) {
                request.setAttribute("fleetCenterId", fleetCenterId);
            }
            if (operationTypeId != null) {
                request.setAttribute("operationTypeId", operationTypeId);
            }
            if (profitType != null) {
                request.setAttribute("profitType", profitType);
            } else {
                request.setAttribute("profitType", "All");
            }

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleAccountsReceivable Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewCustomerWiseProfitDetails(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList customerWiseProfitList = new ArrayList();
        ReportTO reportTO = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", customerId = "";
        String ownerShip = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Customer Wise Profitability";
        request.setAttribute("menuPath", menuPath);
        ArrayList popupCustomerProfitReport = new ArrayList();
        try {
            String param = request.getParameter("param");
            System.out.println("param" + param);
            if (param.equals("ExportExcel")) {

                path = "content/report/BrattleFoods/popupCustomerProfitDetailsExcel.jsp";
            } else {
                path = "content/report/BrattleFoods/popUpCustomerWiseProfit.jsp";
            }
            String tripId = request.getParameter("tripId");
            request.setAttribute("tripId", tripId);
            reportTO.setTripId(tripId);
            popupCustomerProfitReport = reportBP.getPopupCustomerProfitReportDetails(reportTO);
            System.out.println("popupCustomerProfitReport" + popupCustomerProfitReport.size());
            request.setAttribute("popupCustomerProfitReport", popupCustomerProfitReport);
            //            System.out.println("getPopupCustomerProfitReportDetails.size() in Controller" + getPopupCustomerProfitReportDetails.size());
            //            path = "content/report/BrattleFoods/popUpCustomerWiseProfit.jsp";

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleAccountsReceivable Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to view customer wise profit report save .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleCustomerWiseProfit(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList customerWiseProfitList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", customerId = "";
        String ownerShip = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Customer Wise Profitability";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/customerWiseProfitExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/customerWiseProfit.jsp";
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getCustomerId() != null && reportCommand.getCustomerId() != "") {
                report.setCustomerId(reportCommand.getCustomerId());
                request.setAttribute("customerId", reportCommand.getCustomerId());
            } else {
                report.setCustomerId("");
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }

            ReportTO repTO = new ReportTO();
            int i = 0;
            double earnings = 0.0;
            double netExpenses = 0.0;
            double netProfit = 0.0;
            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("fromDate", reportCommand.getFromDate());
            customerWiseProfitList = reportBP.getCustomerWiseProfitList(report, userId);
            request.setAttribute("customerWiseProfitList", customerWiseProfitList);

            if (!"".equals(fromDate) && !"".equals(toDate)) {
                System.out.println("customerList Size Is :::" + customerList.size());
                customerWiseProfitList = reportBP.getCustomerWiseProfitList(report, userId);
                System.out.println("customerWiseProfitList.size() = " + customerWiseProfitList.size());
                if (customerWiseProfitList.size() == 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found.");
                    request.setAttribute("customerWiseProfitListSize", 0);
                } else {
                    request.setAttribute("customerWiseProfitList", customerWiseProfitList);
                    Iterator itr = customerWiseProfitList.iterator();
                    while (itr.hasNext()) {
                        repTO = (ReportTO) itr.next();
                        i++;
                        earnings += Double.parseDouble(repTO.getFreightAmount());
                        netExpenses += Double.parseDouble(repTO.getTotalExpenses());
                        netProfit += repTO.getNetProfit();
                    }
                }
                request.setAttribute("tripNos", i);
                request.setAttribute("earnings", earnings);
                request.setAttribute("netExpenses", netExpenses);
                request.setAttribute("netProfit", netProfit);
                request.setAttribute("fromDate", fromDate);
                request.setAttribute("toDate", toDate);

            }

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleAccountsReceivable Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to view customer wise profit report save .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleCustomerWiseProfitReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList customerWiseProfitList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", customerId = "";
        String ownerShip = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Customer Wise Profitability";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/customerWiseProfitExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/customerWiseProfit.jsp";
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getCustomerId() != null && reportCommand.getCustomerId() != "") {
                report.setCustomerId(reportCommand.getCustomerId());
                customerId = reportCommand.getCustomerId();
            }
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("customerId", customerId);
            request.setAttribute("page", 1);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleAccountsReceivable Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to view customer wise profit report save .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    //Throttle Starts Here
    //Brattle Foods Reports Ends Here
    public ModelAndView handleVehicleUtilization(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList vehicleDetailsList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", vehicleId = "", vehicleNo = "";
        String ownerShip = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Vehicle Utilization Report";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/vehicleUtilizationReportExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/vehicleUtilizationReport.jsp";
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getVehicleNo() != null && reportCommand.getVehicleNo() != "") {
                report.setVehicleId(reportCommand.getVehicleId());
                vehicleId = reportCommand.getVehicleId();
                vehicleNo = reportCommand.getVehicleNo();
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }

            String fleetCenterId = request.getParameter("fleetCenterId");
            report.setFleetCenterId(fleetCenterId);
            ArrayList vehicleUtilizationList = new ArrayList();
            vehicleUtilizationList = reportBP.getVehicleUtilizationList(report, userId);
            System.out.println("vehicleUtilizationList.size() = " + vehicleUtilizationList.size());
            request.setAttribute("vehicleUtilizationList", vehicleUtilizationList);
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            Date fDate = new Date();
            Date tDate = new Date();
            fDate = (java.util.Date) format.parse(fromDate);
            tDate = (java.util.Date) format.parse(endDate);
            long totalDay = (long) ((tDate.getTime() - fDate.getTime()) / (24 * 60 * 60 * 1000));
            String totalDays = String.valueOf(totalDay + 1);
            request.setAttribute("totalDays", totalDays);
            report.setTotalDays(totalDays);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("vehicleId", vehicleId);
            request.setAttribute("vehicleNo", vehicleNo);

            report.setFleetCenterId(fleetCenterId);
            ArrayList companyList = new ArrayList();
            companyList = companyBP.processGetCompanyList();
            request.setAttribute("companyList", companyList);
            request.setAttribute("fleetCenterId", fleetCenterId);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleVehicleUtilization Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleTripWiseProfitReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList accountReceivableList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", vehicleId = "", vehicleNo = "";
        String ownerShip = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Vehicle Utilization Report";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/tripWiseProfitExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/tripWiseProfit.jsp";
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getVehicleNo() != null && reportCommand.getVehicleNo() != "") {
                report.setVehicleId(reportCommand.getVehicleId());
                vehicleNo = reportCommand.getVehicleNo();
                vehicleId = reportCommand.getVehicleId();
            }
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("vehicleNo", vehicleNo);
            request.setAttribute("vehicleId", vehicleId);
            request.setAttribute("page", 1);

            String fleetCenterId = request.getParameter("fleetCenterId");
            report.setFleetCenterId(fleetCenterId);
            ArrayList companyList = new ArrayList();
            companyList = companyBP.processGetCompanyList();
            request.setAttribute("companyList", companyList);
            request.setAttribute("fleetCenterId", fleetCenterId);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleVehicleUtilization Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleTripWiseProfitReportEndStatus(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList accountReceivableList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", vehicleId = "", vehicleNo = "";
        String ownerShip = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Vehicle Utilization Report";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/tripWiseProfitExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/tripWiseProfitEndStatus.jsp";
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getVehicleNo() != null && reportCommand.getVehicleNo() != "") {
                report.setVehicleId(reportCommand.getVehicleId());
                vehicleNo = reportCommand.getVehicleNo();
                vehicleId = reportCommand.getVehicleId();
            }
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("vehicleNo", vehicleNo);
            request.setAttribute("vehicleId", vehicleId);
            request.setAttribute("page", 1);

            String fleetCenterId = request.getParameter("fleetCenterId");
            report.setFleetCenterId(fleetCenterId);
            ArrayList companyList = new ArrayList();
            companyList = companyBP.processGetCompanyList();
            request.setAttribute("companyList", companyList);
            request.setAttribute("fleetCenterId", fleetCenterId);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleVehicleUtilization Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleTripWiseProfit(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        String menuPath = "Operation  >>  View Trip Sheet ";
        String fromDate = "", toDate = "", tripCode = "";
        try {
            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/tripWiseProfitExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/tripWiseProfit.jsp";
            }
            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getTripId() != null && !reportCommand.getTripId().equals("")) {
                report.setTripSheetId(reportCommand.getTripId());
                tripCode = reportCommand.getTripId();
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }

            String fleetCenterId = request.getParameter("fleetCenterId");
            report.setFleetCenterId(fleetCenterId);
            ArrayList tripDetails = new ArrayList();
            tripDetails = reportBP.getTripSheetDetails(report);
            System.out.println("tripdetils" + tripDetails.size());
            request.setAttribute("tripDetails", tripDetails);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("tripId", tripCode);

            report.setFleetCenterId(fleetCenterId);
            ArrayList companyList = new ArrayList();
            companyList = companyBP.processGetCompanyList();
            request.setAttribute("companyList", companyList);
            request.setAttribute("fleetCenterId", fleetCenterId);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleTripWiseProfitEndStatus(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        String menuPath = "Operation  >>  View Trip Sheet ";
        String fromDate = "", toDate = "", tripCode = "";
        try {
            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/tripWiseProfitExcel.jsp";
            } else {
                System.out.println("222 = ");
                path = "content/report/BrattleFoods/tripWiseProfitEndStatus.jsp";
            }
            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getTripId() != null && !reportCommand.getTripId().equals("")) {
                report.setTripSheetId(reportCommand.getTripId());
                tripCode = reportCommand.getTripId();
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }

            String fleetCenterId = request.getParameter("fleetCenterId");
            report.setFleetCenterId(fleetCenterId);
            ArrayList tripDetails = new ArrayList();
            tripDetails = reportBP.getTripSheetDetails(report);
            System.out.println("tripdetils" + tripDetails.size());
            request.setAttribute("tripDetails", tripDetails);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("tripId", tripCode);

            report.setFleetCenterId(fleetCenterId);
            ArrayList companyList = new ArrayList();
            companyList = companyBP.processGetCompanyList();
            request.setAttribute("companyList", companyList);
            request.setAttribute("fleetCenterId", fleetCenterId);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVehicleUtilizationReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList accountReceivableList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", vehicleId = "", vehicleNo = "";
        String ownerShip = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Vehicle Utilization Report";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/vehicleUtilizationReportExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/vehicleUtilizationReport.jsp";
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getVehicleNo() != null && reportCommand.getVehicleNo() != "") {
                report.setVehicleId(reportCommand.getVehicleId());
                vehicleNo = reportCommand.getVehicleNo();
                vehicleId = reportCommand.getVehicleId();
            }
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("vehicleNo", vehicleNo);
            request.setAttribute("vehicleId", vehicleId);
            request.setAttribute("page", 1);

            String fleetCenterId = request.getParameter("fleetCenterId");
            report.setFleetCenterId(fleetCenterId);
            ArrayList companyList = new ArrayList();
            companyList = companyBP.processGetCompanyList();
            request.setAttribute("companyList", companyList);
            request.setAttribute("fleetCenterId", fleetCenterId);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleVehicleUtilization Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleGPSLogReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList accountReceivableList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", vehicleId = "", vehicleNo = "", tripId = "";
        String ownerShip = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Vehicle Utilization Report";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/gpsLogReportExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/gpsLogReport.jsp";
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getTripId() != null && reportCommand.getTripId() != "") {
                report.setTripCode(reportCommand.getTripId());
                tripId = reportCommand.getTripId();
            }
            if (reportCommand.getVehicleNo() != null && reportCommand.getVehicleNo() != "") {
                report.setVehicleId(reportCommand.getVehicleId());
                vehicleNo = reportCommand.getVehicleNo();
                vehicleId = reportCommand.getVehicleId();
            }
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -1);
                startDate = dateFormat.format(cal.getTime());
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("vehicleNo", vehicleNo);
            request.setAttribute("vehicleId", vehicleId);
            request.setAttribute("tripId", tripId);
            request.setAttribute("page", 1);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleVehicleUtilization Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleGPSLog(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View Trip Sheet ";
        String fromDate = "", toDate = "", vehicleId = "", vehicleNo = "", tripId = "";
        try {
            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/gpsLogReportExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/gpsLogReport.jsp";
            }
            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getTripId() != null && reportCommand.getTripId() != "") {
                report.setTripCode(reportCommand.getTripId());
                tripId = reportCommand.getTripId();
            }
            if (reportCommand.getVehicleNo() != null && reportCommand.getVehicleNo() != "") {
                report.setVehicleId(reportCommand.getVehicleId());
                vehicleNo = reportCommand.getVehicleNo();
                vehicleId = reportCommand.getVehicleId();
            }
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -1);
                startDate = dateFormat.format(cal.getTime());
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }

            ArrayList logDetails = new ArrayList();
            logDetails = reportBP.getGPSLogDetails(report, userId);
            System.out.println("logDetails" + logDetails.size());
            request.setAttribute("logDetails", logDetails);
            request.setAttribute("logDetailsSize", logDetails.size());
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("vehicleNo", vehicleNo);
            request.setAttribute("vehicleId", vehicleId);
            request.setAttribute("tripId", tripId);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleDriverSettlement(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View Trip Sheet ";
        String fromDate = "", toDate = "", vehicleId = "", vehicleNo = "", tripId = "";
        try {
            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/driverSettlementReportExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/driverSettlementReport.jsp";
            }
            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getTripId() != null && reportCommand.getTripId() != "") {
                report.setTripCode(reportCommand.getTripId());
                tripId = reportCommand.getTripId();
            }
            if (reportCommand.getVehicleNo() != null && reportCommand.getVehicleNo() != "") {
                report.setVehicleId(reportCommand.getVehicleId());
                vehicleNo = reportCommand.getVehicleNo();
                vehicleId = reportCommand.getVehicleId();
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }

            ArrayList driverSettlementDetails = new ArrayList();
            driverSettlementDetails = reportBP.getDriverSettlementDetails(report, userId);
            System.out.println("driverSettlementDetails" + driverSettlementDetails.size());
            if (driverSettlementDetails.size() > 0) {
                request.setAttribute("driverSettlementDetails", driverSettlementDetails);
            }
            request.setAttribute("driverSettlementDetailsSize", driverSettlementDetails.size());
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("vehicleNo", vehicleNo);
            request.setAttribute("vehicleId", vehicleId);
            request.setAttribute("tripId", tripId);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleDriverSettlementReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList customerWiseProfitList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "";
        String fromDate = "", toDate = "", vehicleId = "", vehicleNo = "", tripId = "", primaryDriverName = "", primaryDriverId = "";
        String ownerShip = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Customer Wise Profitability";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/driverSettlementReportExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/driverSettlementReport.jsp";
            }

            if (reportCommand.getTripId() != null && reportCommand.getTripId() != "") {
                report.setTripCode(reportCommand.getTripId());
                tripId = reportCommand.getTripId();
            }
            if (reportCommand.getVehicleNo() != null && reportCommand.getVehicleNo() != "") {
                report.setVehicleId(reportCommand.getVehicleId());
                vehicleNo = reportCommand.getVehicleNo();
                vehicleId = reportCommand.getVehicleId();
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }

            if (request.getParameter("primaryDriverId") != null && request.getParameter("primaryDriverId") != "") {
                report.setEmpId(request.getParameter("primaryDriverId"));
                primaryDriverName = request.getParameter("primaryDriver");
                primaryDriverId = request.getParameter("primaryDriverId");
            }

            ArrayList driverSettlementDetails = new ArrayList();
            driverSettlementDetails = reportBP.getDriverSettlementDetails(report, userId);
            System.out.println("driverSettlementDetails" + driverSettlementDetails.size());
            if (driverSettlementDetails.size() > 0) {
                request.setAttribute("driverSettlementDetails", driverSettlementDetails);
            }
            request.setAttribute("driverSettlementDetailsSize", driverSettlementDetails.size());
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("vehicleNo", vehicleNo);
            request.setAttribute("vehicleId", vehicleId);
            request.setAttribute("tripId", tripId);
            request.setAttribute("primaryDriverName", primaryDriverName);
            request.setAttribute("primaryDriverId", primaryDriverId);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleAccountsReceivable Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleDriverSettlementDetails(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View Trip Sheet ";
        String fromDate = "", toDate = "", vehicleId = "", vehicleNo = "", tripId = "";
        try {
            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/driverSettlementReportExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/driverSettlementReport.jsp";
            }
            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getTripId() != null && reportCommand.getTripId() != "") {
                report.setTripCode(reportCommand.getTripId());
                tripId = reportCommand.getTripId();
            }
            if (reportCommand.getVehicleNo() != null && reportCommand.getVehicleNo() != "") {
                report.setVehicleId(reportCommand.getVehicleId());
                vehicleNo = reportCommand.getVehicleNo();
                vehicleId = reportCommand.getVehicleId();
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }

            ArrayList driverSettlementDetails = new ArrayList();
            driverSettlementDetails = reportBP.getDriverSettlementDetails(report, userId);
            System.out.println("driverSettlementDetails" + driverSettlementDetails.size());
            if (driverSettlementDetails.size() > 0) {
                request.setAttribute("driverSettlementDetails", driverSettlementDetails);
            }
            request.setAttribute("driverSettlementDetailsSize", driverSettlementDetails.size());
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("vehicleNo", vehicleNo);
            request.setAttribute("vehicleId", vehicleId);
            request.setAttribute("tripId", tripId);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewTripLogDetails(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View Trip Sheet ";
        String tripId = "";
        try {
            String param = request.getParameter("param");
            System.out.println("the param value is" + param);
            if (param != null && !"".equals(param)) {
                if (param.equals("ExportExcel")) {
                    System.out.println("111 = ");
                    path = "content/report/BrattleFoods/tripLogReportExcel.jsp";
                }
            } else {
                path = "content/report/BrattleFoods/tripLogReport.jsp";
            }
            tripId = request.getParameter("tripId");
//            String timeValues = "9,10,11,12,1,2,3,4,5";
//            String tempValues = "7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3";
            report.setTripCode(tripId);
            ArrayList tripLogDetails = new ArrayList();
            tripLogDetails = reportBP.getTripLogDetails(report, userId);
            System.out.println("tripLogDetailsSize" + tripLogDetails.size());

            Iterator itr;
            itr = tripLogDetails.iterator();
            ReportTO reportTO = null;
            String timeValues = "";
            String tempValues = "";
            while (itr.hasNext()) {
                reportTO = new ReportTO();
                reportTO = (ReportTO) itr.next();
                if (tempValues == "" && timeValues == "") {
                    tempValues = reportTO.getCurrentTemperature();
                    timeValues = reportTO.getLogTime();
                } else {
                    tempValues = tempValues + "," + reportTO.getCurrentTemperature();
                    timeValues = timeValues + "," + reportTO.getLogTime();
                }
            }
            request.setAttribute("tripLogDetails", tripLogDetails);
            request.setAttribute("tripLogDetailsSize", tripLogDetails.size());
            request.setAttribute("tripId", tripId);
            request.setAttribute("timeValues", timeValues);
            request.setAttribute("tempValues", tempValues);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getTemperatureTimeDetails(HttpServletRequest request, HttpServletResponse response, ReportCommand command) throws IOException {
        HttpSession session = request.getSession();
        ReportTO report = new ReportTO();
        reportCommand = command;
//        JsonTO jsonTO = new JsonTO();
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View Trip Sheet ";
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String tripId = "";
            response.setContentType("text/html");
            tripId = request.getParameter("tripId");
            report.setTripCode(tripId);
            ArrayList tripLogDetails = new ArrayList();
            tripLogDetails = reportBP.getTripLogDetails(report, userId);
            System.out.println("tripLogDetailsSize" + tripLogDetails.size());
            System.out.println("getContractRouteCourse.size() = " + tripLogDetails.size());
            JSONArray jsonArray = new JSONArray();
            JSONArray jsonArray1 = new JSONArray();
            JSONArray jsonArray2 = new JSONArray();
            Iterator itr = tripLogDetails.iterator();
            Object temperature = "";
            Object dateTime = "";
            int cntr = 0;
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                report = (ReportTO) itr.next();

                dateTime = (Object) report.getLogTime();
                temperature = (Object) report.getCurrentTemperature();
//                System.out.println("dateTime:" + dateTime);
//                System.out.println("temperature:" + temperature);
//                jsonObject.put("dateTime", dateTime);
                System.out.println("jsonObject = " + jsonObject);
                jsonArray1.put(dateTime);
                jsonArray2.put(temperature);
            }
            jsonArray = concatArray(jsonArray1, jsonArray2);
            System.out.println("jsonArray = " + jsonArray);
            String data = "[10,2],[11,4],[15,6],[18,10]";
            pw.print(data);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    private JSONArray concatArray(JSONArray arr1, JSONArray arr2) throws JSONException, org.json.JSONException {
        JSONArray result = new JSONArray();
        for (int i = 0; i < arr1.length(); i++) {
            result.put(arr1.get(i));
        }
        for (int i = 0; i < arr2.length(); i++) {
            result.put(arr2.get(i));
        }
        return result;
    }

    public ModelAndView handleBPCLTransactionDetails(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View BPCL Transaction Sheet ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            path = "content/report/BrattleFoods/BPCLTransactionReport.jsp";
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleBPCLTransactionDetailsExcel(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View BPCL Transaction Sheet ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            String param = request.getParameter("param");
            System.out.println("the param value is" + param);
            if (param != null && !"".equals(param)) {
                if (param.equals("ExportExcel")) {
                    System.out.println("111 = ");
                    path = "content/report/BrattleFoods/BPCLTransactionReportExcel.jsp";
                } else {
                    path = "content/report/BrattleFoods/BPCLTransactionReport.jsp";
                }
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            ArrayList BPCLTransactionDetails = new ArrayList();
            BPCLTransactionDetails = reportBP.getBPCLTransactionDetails(report, userId);
            System.out.println("BPCLTransactionDetailsSize" + BPCLTransactionDetails.size());
            request.setAttribute("BPCLTransactionDetails", BPCLTransactionDetails);
            request.setAttribute("BPCLTransactionDetailsSize", BPCLTransactionDetails.size());
            if (BPCLTransactionDetails.size() == 0) {
                path = "content/report/BrattleFoods/BPCLTransactionReport.jsp";
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");
            }

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewFinanceAdviceDetais(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO reportTO = new ReportTO();
        OperationTO operationTO = new OperationTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View Finance Report Sheet ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            Date dNow = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(dNow);
            cal.add(Calendar.DATE, 0);
            dNow = cal.getTime();

            Date dNow1 = new Date();
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(dNow1);
            cal1.add(Calendar.DATE, -6);
            dNow1 = cal1.getTime();

            SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy");
            String today = ft.format(dNow);
            String fromday = ft.format(dNow1);

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                operationTO.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            } else {
                operationTO.setFromDate(fromday);
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                operationTO.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            } else {
                operationTO.setToDate(today);
            }
            path = "content/report/BrattleFoods/FinanceReportDetails.jsp";
            ArrayList financeAdvice = new ArrayList();
            financeAdvice = operationBP.getfinanceAdvice(operationTO);
            request.setAttribute("financeAdvice", financeAdvice);
            request.setAttribute("fromdate", operationTO.getFromDate());
            request.setAttribute("todate", operationTO.getToDate());
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handlefinanceAdviceExcel(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
//        ArrayList financeAdviceDetails = new ArrayList();
        ArrayList financeAdvice = new ArrayList();
//        HttpSession session = request.getSession();
//        operationCommand = command;
//        String menuPath = "";
        HttpSession session = request.getSession();
        String path = "";
        ReportTO reportTO = new ReportTO();
        OperationTO operationTO = new OperationTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View Finance Report Sheet ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            String param = request.getParameter("param");
            System.out.println("the param value is" + param);
            if (param != null && !"".equals(param)) {
                if (param.equals("ExportExcel")) {
                    System.out.println("111 = ");
                    path = "content/report/BrattleFoods/FinanceReportDetailsExcel.jsp";
                } else {
                    path = "content/report/BrattleFoods/FinanceReportDetails.jsp";
                }
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                operationTO.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                operationTO.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                reportTO.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                reportTO.setToDate(toDate);
            }
            financeAdvice = operationBP.getfinanceAdvice(operationTO);
            request.setAttribute("financeAdvice", financeAdvice);
            request.setAttribute("fromdate", operationTO.getFromDate());
            request.setAttribute("todate", operationTO.getToDate());
            request.setAttribute("financeAdvice.size", financeAdvice.size());
            if (financeAdvice.size() == 0) {
                path = "content/report/BrattleFoods/FinanceReportDetails.jsp";
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");
            }

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handlefinanceAdviceReportExcel(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        ArrayList financeAdviceDetails = new ArrayList();
        HttpSession session = request.getSession();
        ReportTO reportTO = new ReportTO();
        reportCommand = command;
        String menuPath = "";
        String pageTitle = "View Finance Details";
        menuPath = "Finance  >> Finance Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/report/BrattleFoods/financeAdviceReportExcel.jsp";
            String dateval = request.getParameter("dateval");
            String type = request.getParameter("type");
            String tripType = request.getParameter("tripType");
            financeAdviceDetails = operationBP.getfinanceAdviceDetails(dateval, type, tripType);
            OperationTO operationTO = null;
            String tripid = "";
            Iterator itr = financeAdviceDetails.iterator();
            if (itr.hasNext()) {
                operationTO = new OperationTO();
                operationTO = (OperationTO) itr.next();
                tripid = operationTO.getTripId();
            }

            request.setAttribute("financeAdviceDetails", financeAdviceDetails);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewFinanceReportAdviceDetais(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList financeAdviceDetails = new ArrayList();
        HttpSession session = request.getSession();
        ReportTO reportTO = new ReportTO();
        reportCommand = command;
        String menuPath = "";
        String pageTitle = "View Finance Details";
        menuPath = "Finance  >> Finance Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/report/BrattleFoods/FinanceAdviceReportDetails.jsp";
            String dateval = request.getParameter("dateval");
            String type = request.getParameter("type");
            String tripType = request.getParameter("tripType");
            financeAdviceDetails = operationBP.getfinanceAdviceDetails(dateval, type, tripType);
            OperationTO operationTO = null;
            String tripid = "";
            Iterator itr = financeAdviceDetails.iterator();
            if (itr.hasNext()) {
                operationTO = new OperationTO();
                operationTO = (OperationTO) itr.next();
                tripid = operationTO.getTripId();
            }

            request.setAttribute("financeAdviceDetails", financeAdviceDetails);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView viewGpsStatusDetais(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        //        ArrayList financeAdviceDetails = new ArrayList();
        ArrayList tripGpsStatusDetails = new ArrayList();
        //        HttpSession session = request.getSession();
        //        operationCommand = command;
        //        String menuPath = "";
        HttpSession session = request.getSession();
        String path = "";
        ReportTO reportTO = new ReportTO();
        //        OperationTO operationTO = new OperationTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  Trip GPS Status ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            String param = request.getParameter("param");
            System.out.println("the param value is" + param);
            if (param != null && !"".equals(param)) {
                if (param.equals("ExportExcel")) {
                    System.out.println("111 = ");
                    path = "content/report/BrattleFoods/tripGpsStatusDetailsExcel.jsp";
                } else {
                    path = "content/report/BrattleFoods/tripGpsStatusDetails.jsp";
                }
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                reportTO.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
                System.out.println("fromDate = " + fromDate);
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                reportTO.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
                System.out.println("toDate = " + toDate);
            }
            if (reportCommand.getTripStatus() != null && reportCommand.getTripStatus() != "") {
                reportTO.setTripStatus(reportCommand.getTripStatus());
                System.out.println("tripStatus = " + reportTO.getTripStatus());
            }
            if (reportCommand.getTripId() != null && reportCommand.getTripId() != "") {
                reportTO.setTripId(reportCommand.getTripId());
                System.out.println("TripId = " + reportTO.getTripId());
            }
            if (reportCommand.getTripCode() != null && reportCommand.getTripCode() != "") {
                reportTO.setTripCode(reportCommand.getTripCode());
                System.out.println("tripCode = " + reportTO.getTripCode());
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                reportTO.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                reportTO.setToDate(toDate);
            }
            tripGpsStatusDetails = reportBP.getTripGpsStatusDetails(reportTO);
            request.setAttribute("tripGpsStatusDetails", tripGpsStatusDetails);
            request.setAttribute("tripId", reportTO.getTripId());
            request.setAttribute("tripCode", reportTO.getTripCode());
            request.setAttribute("fromdate", reportTO.getFromDate());
            request.setAttribute("tripStatus", reportTO.getTripStatus());
            request.setAttribute("todate", reportTO.getToDate());
            request.setAttribute("financeAdvice.size", tripGpsStatusDetails.size());
            if (tripGpsStatusDetails.size() == 0) {
                path = "content/report/BrattleFoods/tripGpsStatusDetails.jsp";
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");
            }

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getTripCode(HttpServletRequest request, HttpServletResponse response, ReportCommand command) throws IOException {
        HttpSession session = request.getSession();
        ReportTO reportTO = new ReportTO();
        reportCommand = command;
        OperationTO operationTO = new OperationTO();
        //        JsonTO jsonTO = new JsonTO();
        ArrayList getTripCodeList = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String tripCode = "";
            response.setContentType("text/html");
            tripCode = request.getParameter("tripCode");
            System.out.println("the trip code is " + tripCode);
            reportTO.setTripCode(tripCode);
            getTripCodeList = reportBP.getTripCodeList(reportTO);
            System.out.println("getTripCodeList.size() = " + getTripCodeList.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = getTripCodeList.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                reportTO = (ReportTO) itr.next();
                jsonObject.put("Name", reportTO.getTripId() + "," + reportTO.getTripCode());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView viewWFLWFUDetais(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        //        ArrayList financeAdviceDetails = new ArrayList();
        ArrayList tripGpsStatusDetails = new ArrayList();
        //        HttpSession session = request.getSession();
        //        operationCommand = command;
        //        String menuPath = "";
        HttpSession session = request.getSession();
        String path = "";
        ReportTO reportTO = new ReportTO();
        //        OperationTO operationTO = new OperationTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Report  >>  WFl_WFU Reports";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            String param = request.getParameter("param");
            System.out.println("the param value is" + param);
            if (param != null && !"".equals(param)) {
                if (param.equals("ExportExcel")) {
                    System.out.println("111 = ");
                    path = "content/report/WflWfuAlertsExcel.jsp";
                } else {
                    path = "content/report/WflWfuAlerts.jsp";
                    ArrayList wflList = new ArrayList();
                    wflList = reportBP.getWflList(reportTO);
                    if (wflList.size() > 0) {
                        request.setAttribute("wflList", wflList);
                    }
                    ArrayList wfuList = new ArrayList();
                    wfuList = reportBP.getWfuList(reportTO);
                    if (wfuList.size() > 0) {
                        request.setAttribute("wfuList", wfuList);
                    }
                    ArrayList tripInProgressList = new ArrayList();
                    tripInProgressList = reportBP.getTripInProgressList(reportTO);
                    if (tripInProgressList.size() > 0) {
                        request.setAttribute("tripInProgressList", tripInProgressList);
                    }
                    ArrayList tripNotStartedList = new ArrayList();
                    tripNotStartedList = reportBP.getTripNotStartedList(reportTO);
                    if (tripNotStartedList.size() > 0) {
                        request.setAttribute("tripNotStartedList", tripNotStartedList);
                    }
                    ArrayList jobCardList = new ArrayList();
                    jobCardList = reportBP.getJobCardList(reportTO);
                    if (jobCardList.size() > 0) {
                        request.setAttribute("jobCardList", jobCardList);
                    }
                    ArrayList futureTripList = new ArrayList();
                    futureTripList = reportBP.getFutureTripList(reportTO);
                    if (futureTripList.size() > 0) {
                        request.setAttribute("futureTripList", futureTripList);
                    }

                    Iterator itr = wflList.iterator();
                    ReportTO repTO = new ReportTO();
                    Iterator itr1 = wfuList.iterator();
                    ReportTO repTO1 = new ReportTO();
                    Iterator itr2 = tripInProgressList.iterator();
                    ReportTO repTO2 = new ReportTO();
                    Iterator itr3 = tripNotStartedList.iterator();
                    ReportTO repTO3 = new ReportTO();
                    Iterator itr4 = jobCardList.iterator();
                    ReportTO repTO4 = new ReportTO();
                    Iterator itr5 = futureTripList.iterator();
                    ReportTO repTO5 = new ReportTO();
                    int cntr = 1;
                    int wfl_North_West_Corridor = 0;
                    int wfl_North_South_Corridor = 0;
                    int wfl_Dedicate = 0;
                    int wfl_General = 0;
                    int wfu_North_West_Corridor = 0;
                    int wfu_North_South_Corridor = 0;
                    int wfu_Dedicate = 0;
                    int wfu_General = 0;
                    int progress_North_West_Corridor = 0;
                    int progress_North_South_Corridor = 0;
                    int progress_Dedicate = 0;
                    int progress_General = 0;
                    int notStart_North_West_Corridor = 0;
                    int notStart_North_South_Corridor = 0;
                    int notStart_Dedicate = 0;
                    int notStart_General = 0;
                    int job_North_West_Corridor = 0;
                    int job_North_South_Corridor = 0;
                    int job_Dedicate = 0;
                    int job_General = 0;
                    int future_North_West_Corridor = 0;
                    int future_North_South_Corridor = 0;
                    int future_Dedicate = 0;
                    int future_General = 0;
                    while (itr.hasNext()) {
                        repTO = (ReportTO) itr.next();
                        if ("North-West-Corridor".equals(repTO.getOperationPoint())) {
                            wfl_North_West_Corridor++;
                        } else if ("North-South-Corridor".equals(repTO.getOperationPoint())) {
                            wfl_North_South_Corridor++;
                        } else if ("Dedicate".equals(repTO.getOperationPoint())) {
                            wfl_Dedicate++;
                        } else if ("General".equals(repTO.getOperationPoint())) {
                            wfl_General++;
                        }
                    }
                    while (itr1.hasNext()) {
                        repTO1 = (ReportTO) itr1.next();
                        if ("North-West-Corridor".equals(repTO1.getOperationPoint())) {
                            wfu_North_West_Corridor++;
                        } else if ("North-South-Corridor".equals(repTO1.getOperationPoint())) {
                            wfu_North_South_Corridor++;
                        } else if ("Dedicate".equals(repTO1.getOperationPoint())) {
                            wfu_Dedicate++;
                        } else if ("General".equals(repTO1.getOperationPoint())) {
                            wfu_General++;
                        }
                    }
                    while (itr2.hasNext()) {
                        repTO2 = (ReportTO) itr2.next();
                        if ("North-West-Corridor".equals(repTO2.getOperationPoint())) {
                            progress_North_West_Corridor++;
                        } else if ("North-South-Corridor".equals(repTO2.getOperationPoint())) {
                            progress_North_South_Corridor++;
                        } else if ("Dedicate".equals(repTO2.getOperationPoint())) {
                            progress_Dedicate++;
                        } else if ("General".equals(repTO2.getOperationPoint())) {
                            progress_General++;
                        }
                    }
                    while (itr3.hasNext()) {
                        repTO3 = (ReportTO) itr3.next();
                        if ("North-West-Corridor".equals(repTO3.getOperationPoint())) {
                            notStart_North_West_Corridor++;
                        } else if ("North-South-Corridor".equals(repTO3.getOperationPoint())) {
                            notStart_North_South_Corridor++;
                        } else if ("Dedicate".equals(repTO3.getOperationPoint())) {
                            notStart_Dedicate++;
                        } else if ("General".equals(repTO3.getOperationPoint())) {
                            notStart_General++;
                        }
                    }
                    while (itr4.hasNext()) {
                        repTO4 = (ReportTO) itr4.next();
                        if ("North-West-Corridor".equals(repTO4.getOperationPoint())) {
                            job_North_West_Corridor++;
                        } else if ("North-South-Corridor".equals(repTO4.getOperationPoint())) {
                            job_North_South_Corridor++;
                        } else if ("Dedicate".equals(repTO4.getOperationPoint())) {
                            job_Dedicate++;
                        } else if ("General".equals(repTO4.getOperationPoint())) {
                            job_General++;
                        }
                    }
                    while (itr5.hasNext()) {
                        repTO5 = (ReportTO) itr5.next();
                        if ("North-West-Corridor".equals(repTO5.getOperationPoint())) {
                            future_North_West_Corridor++;
                        } else if ("North-South-Corridor".equals(repTO5.getOperationPoint())) {
                            future_North_South_Corridor++;
                        } else if ("Dedicate".equals(repTO5.getOperationPoint())) {
                            future_Dedicate++;
                        } else if ("General".equals(repTO5.getOperationPoint())) {
                            future_General++;
                        }
                    }
                    request.setAttribute("wfl_North_West_Corridor", wfl_North_West_Corridor);
                    request.setAttribute("wfl_North_South_Corridor", wfl_North_South_Corridor);
                    request.setAttribute("wfl_Dedicate", wfl_Dedicate);
                    request.setAttribute("wfl_General", wfl_General);
                    request.setAttribute("wfu_North_West_Corridor", wfu_North_West_Corridor);
                    request.setAttribute("wfu_North_South_Corridor", wfu_North_South_Corridor);
                    request.setAttribute("wfu_Dedicate", wfu_Dedicate);
                    request.setAttribute("wfu_General", wfu_General);
                    request.setAttribute("progress_North_West_Corridor", progress_North_West_Corridor);
                    request.setAttribute("progress_North_South_Corridor", progress_North_South_Corridor);
                    request.setAttribute("progress_Dedicate", progress_Dedicate);
                    request.setAttribute("progress_General", progress_General);
                    request.setAttribute("notStart_North_West_Corridor", notStart_North_West_Corridor);
                    request.setAttribute("notStart_North_South_Corridor", notStart_North_South_Corridor);
                    request.setAttribute("notStart_Dedicate", notStart_Dedicate);
                    request.setAttribute("notStart_General", notStart_General);
                    request.setAttribute("job_North_West_Corridor", job_North_West_Corridor);
                    request.setAttribute("job_North_South_Corridor", job_North_South_Corridor);
                    request.setAttribute("job_Dedicate", job_Dedicate);
                    request.setAttribute("job_General", job_General);
                    request.setAttribute("future_North_West_Corridor", future_North_West_Corridor);
                    request.setAttribute("future_North_South_Corridor", future_North_South_Corridor);
                    request.setAttribute("future_Dedicate", future_Dedicate);
                    request.setAttribute("future_General", future_General);
                }
            }
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView sendWFLWFUAlertsMail(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        //        ArrayList financeAdviceDetails = new ArrayList();
        ArrayList tripGpsStatusDetails = new ArrayList();
        //        HttpSession session = request.getSession();
        //        operationCommand = command;
        //        String menuPath = "";
        HttpSession session = request.getSession();
        String path = "";
        ReportTO reportTO = new ReportTO();
        //        OperationTO operationTO = new OperationTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  Trip GPS Status ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        Runtime run = Runtime.getRuntime();
        Process p = null;
        try {
            SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd h:m:s");
            Date dNow = new Date();
            System.out.println("ft.Format(dNow()) = " + ft.format(dNow));
            String currentDate = ft.format(dNow);
//          Process process = Runtime.getRuntime().exec("  cmd.exe /c start C:\\Apps1\\BrattleFoods\\batchprogram\\throttlealerts\\version1\\throttlealerts\\batchScripts\\downloadWflWfuAlerts.bat  exit");
            Process process = Runtime.getRuntime().exec("  cmd.exe /c start C:\\BrattleBatch\\alerts\\downloadWflWfuAlerts.bat  exit");
            path = "content/report/WflWfuAlerts.jsp";
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "WFL AND  WFU Email sent sucessfully");
            String param = request.getParameter("param");
            System.out.println("the param value is" + param);
            if (param != null && !"".equals(param)) {
                if (param.equals("ExportExcel")) {
                    System.out.println("111 = ");
                    path = "content/report/WflWfuAlertsExcel.jsp";
                } else {
                    path = "content/report/WflWfuAlerts.jsp";
                    ArrayList wflList = new ArrayList();
                    wflList = reportBP.getWflList(reportTO);
                    if (wflList.size() > 0) {
                        request.setAttribute("wflList", wflList);
                    }
                    ArrayList wfuList = new ArrayList();
                    wfuList = reportBP.getWfuList(reportTO);
                    if (wfuList.size() > 0) {
                        request.setAttribute("wfuList", wfuList);
                    }
                    ArrayList tripInProgressList = new ArrayList();
                    tripInProgressList = reportBP.getTripInProgressList(reportTO);
                    if (tripInProgressList.size() > 0) {
                        request.setAttribute("tripInProgressList", tripInProgressList);
                    }
                    ArrayList tripNotStartedList = new ArrayList();
                    tripNotStartedList = reportBP.getTripNotStartedList(reportTO);
                    if (tripNotStartedList.size() > 0) {
                        request.setAttribute("tripNotStartedList", tripNotStartedList);
                    }
                    ArrayList jobCardList = new ArrayList();
                    jobCardList = reportBP.getJobCardList(reportTO);
                    if (jobCardList.size() > 0) {
                        request.setAttribute("jobCardList", jobCardList);
                    }
                    ArrayList futureTripList = new ArrayList();
                    futureTripList = reportBP.getFutureTripList(reportTO);
                    if (futureTripList.size() > 0) {
                        request.setAttribute("futureTripList", futureTripList);
                    }

                    Iterator itr = wflList.iterator();
                    ReportTO repTO = new ReportTO();
                    Iterator itr1 = wfuList.iterator();
                    ReportTO repTO1 = new ReportTO();
                    Iterator itr2 = tripInProgressList.iterator();
                    ReportTO repTO2 = new ReportTO();
                    Iterator itr3 = tripNotStartedList.iterator();
                    ReportTO repTO3 = new ReportTO();
                    Iterator itr4 = jobCardList.iterator();
                    ReportTO repTO4 = new ReportTO();
                    Iterator itr5 = futureTripList.iterator();
                    ReportTO repTO5 = new ReportTO();
                    int cntr = 1;
                    int wfl_North_West_Corridor = 0;
                    int wfl_North_South_Corridor = 0;
                    int wfl_Dedicate = 0;
                    int wfl_General = 0;
                    int wfu_North_West_Corridor = 0;
                    int wfu_North_South_Corridor = 0;
                    int wfu_Dedicate = 0;
                    int wfu_General = 0;
                    int progress_North_West_Corridor = 0;
                    int progress_North_South_Corridor = 0;
                    int progress_Dedicate = 0;
                    int progress_General = 0;
                    int notStart_North_West_Corridor = 0;
                    int notStart_North_South_Corridor = 0;
                    int notStart_Dedicate = 0;
                    int notStart_General = 0;
                    int job_North_West_Corridor = 0;
                    int job_North_South_Corridor = 0;
                    int job_Dedicate = 0;
                    int job_General = 0;
                    int future_North_West_Corridor = 0;
                    int future_North_South_Corridor = 0;
                    int future_Dedicate = 0;
                    int future_General = 0;
                    while (itr.hasNext()) {
                        repTO = (ReportTO) itr.next();
                        if ("North-West-Corridor".equals(repTO.getOperationPoint())) {
                            wfl_North_West_Corridor++;
                        } else if ("North-South-Corridor".equals(repTO.getOperationPoint())) {
                            wfl_North_South_Corridor++;
                        } else if ("Dedicate".equals(repTO.getOperationPoint())) {
                            wfl_Dedicate++;
                        } else if ("General".equals(repTO.getOperationPoint())) {
                            wfl_General++;
                        }
                    }
                    while (itr1.hasNext()) {
                        repTO1 = (ReportTO) itr1.next();
                        if ("North-West-Corridor".equals(repTO1.getOperationPoint())) {
                            wfu_North_West_Corridor++;
                        } else if ("North-South-Corridor".equals(repTO1.getOperationPoint())) {
                            wfu_North_South_Corridor++;
                        } else if ("Dedicate".equals(repTO1.getOperationPoint())) {
                            wfu_Dedicate++;
                        } else if ("General".equals(repTO1.getOperationPoint())) {
                            wfu_General++;
                        }
                    }
                    while (itr2.hasNext()) {
                        repTO2 = (ReportTO) itr2.next();
                        if ("North-West-Corridor".equals(repTO2.getOperationPoint())) {
                            progress_North_West_Corridor++;
                        } else if ("North-South-Corridor".equals(repTO2.getOperationPoint())) {
                            progress_North_South_Corridor++;
                        } else if ("Dedicate".equals(repTO2.getOperationPoint())) {
                            progress_Dedicate++;
                        } else if ("General".equals(repTO2.getOperationPoint())) {
                            progress_General++;
                        }
                    }
                    while (itr3.hasNext()) {
                        repTO3 = (ReportTO) itr3.next();
                        if ("North-West-Corridor".equals(repTO3.getOperationPoint())) {
                            notStart_North_West_Corridor++;
                        } else if ("North-South-Corridor".equals(repTO3.getOperationPoint())) {
                            notStart_North_South_Corridor++;
                        } else if ("Dedicate".equals(repTO3.getOperationPoint())) {
                            notStart_Dedicate++;
                        } else if ("General".equals(repTO3.getOperationPoint())) {
                            notStart_General++;
                        }
                    }
                    while (itr4.hasNext()) {
                        repTO4 = (ReportTO) itr4.next();
                        if ("North-West-Corridor".equals(repTO4.getOperationPoint())) {
                            job_North_West_Corridor++;
                        } else if ("North-South-Corridor".equals(repTO4.getOperationPoint())) {
                            job_North_South_Corridor++;
                        } else if ("Dedicate".equals(repTO4.getOperationPoint())) {
                            job_Dedicate++;
                        } else if ("General".equals(repTO4.getOperationPoint())) {
                            job_General++;
                        }
                    }
                    while (itr5.hasNext()) {
                        repTO5 = (ReportTO) itr5.next();
                        if ("North-West-Corridor".equals(repTO5.getOperationPoint())) {
                            future_North_West_Corridor++;
                        } else if ("North-South-Corridor".equals(repTO5.getOperationPoint())) {
                            future_North_South_Corridor++;
                        } else if ("Dedicate".equals(repTO5.getOperationPoint())) {
                            future_Dedicate++;
                        } else if ("General".equals(repTO5.getOperationPoint())) {
                            future_General++;
                        }
                    }
                    request.setAttribute("wfl_North_West_Corridor", wfl_North_West_Corridor);
                    request.setAttribute("wfl_North_South_Corridor", wfl_North_South_Corridor);
                    request.setAttribute("wfl_Dedicate", wfl_Dedicate);
                    request.setAttribute("wfl_General", wfl_General);
                    request.setAttribute("wfu_North_West_Corridor", wfu_North_West_Corridor);
                    request.setAttribute("wfu_North_South_Corridor", wfu_North_South_Corridor);
                    request.setAttribute("wfu_Dedicate", wfu_Dedicate);
                    request.setAttribute("wfu_General", wfu_General);
                    request.setAttribute("progress_North_West_Corridor", progress_North_West_Corridor);
                    request.setAttribute("progress_North_South_Corridor", progress_North_South_Corridor);
                    request.setAttribute("progress_Dedicate", progress_Dedicate);
                    request.setAttribute("progress_General", progress_General);
                    request.setAttribute("notStart_North_West_Corridor", notStart_North_West_Corridor);
                    request.setAttribute("notStart_North_South_Corridor", notStart_North_South_Corridor);
                    request.setAttribute("notStart_Dedicate", notStart_Dedicate);
                    request.setAttribute("notStart_General", notStart_General);
                    request.setAttribute("job_North_West_Corridor", job_North_West_Corridor);
                    request.setAttribute("job_North_South_Corridor", job_North_South_Corridor);
                    request.setAttribute("job_Dedicate", job_Dedicate);
                    request.setAttribute("job_General", job_General);
                    request.setAttribute("future_North_West_Corridor", future_North_West_Corridor);
                    request.setAttribute("future_North_South_Corridor", future_North_South_Corridor);
                    request.setAttribute("future_Dedicate", future_Dedicate);
                    request.setAttribute("future_General", future_General);
                }
            }
            int wfuWflCount = 0;
            boolean sts = true;
            while (sts) {
                wfuWflCount = reportBP.getWflWfuStatus(currentDate);
                if (wfuWflCount > 0) {
                    sts = false;
                }
            }

            int wfuWflLogId = reportBP.getWflWfuLogId(currentDate);
            if (wfuWflLogId > 0) {
                request.setAttribute("wfuWflLogId", wfuWflLogId);
            }
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewTripSheetReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;
        String menuPath = "Operation  >>  View Trip Sheet Report";
        String statusId1 = request.getParameter("statusId");
        System.out.println("pass value:" + request.getParameter("successMessage"));
        request.setAttribute("errorMessage", request.getParameter("msg"));

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet Repor";
            request.setAttribute("pageTitle", pageTitle);
            ReportTO reportTO = new ReportTO();
//            if (reportCommand.getFleetCenterId() != null && !"".equals(reportCommand.getFleetCenterId())) {
//                reportTO.setFleetCenterId(reportCommand.getFleetCenterId());
//                request.setAttribute("fleetCenterId", reportTO.getFleetCenterId());
//            }
            if (reportCommand.getVehicleId() != null && !"".equals(reportCommand.getVehicleId())) {
                reportTO.setVehicleId(reportCommand.getVehicleId());
                request.setAttribute("vehicleId", reportTO.getVehicleId());
            }
            if (reportCommand.getVehicleTypeId() != null && !"".equals(reportCommand.getVehicleTypeId())) {
                reportTO.setVehicleTypeId(reportCommand.getVehicleTypeId());
                request.setAttribute("vehicleTypeId", reportTO.getVehicleTypeId());
            }
            if (reportCommand.getCustomerId() != null && !"".equals(reportCommand.getCustomerId())) {
                reportTO.setCustomerId(reportCommand.getCustomerId());
                request.setAttribute("customerId", reportTO.getCustomerId());
            }
            if (reportCommand.getTripStatusId() != null && !"".equals(reportCommand.getTripStatusId())) {
                reportTO.setTripStatusId(reportCommand.getTripStatusId());
                request.setAttribute("tripStatusId", reportTO.getTripStatusId());
            }
//            if (reportCommand.getTripStatusIdTo() != null && !"".equals(reportCommand.getTripStatusIdTo())) {
//                reportTO.setTripStatusIdTo(reportCommand.getTripStatusIdTo());
//                request.setAttribute("tripStatusIdTo", reportTO.getTripStatusIdTo());
//            }
            if (reportCommand.getStartDateFrom() != null && !"".equals(reportCommand.getStartDateFrom())) {
                reportTO.setStartDateFrom(reportCommand.getStartDateFrom());
                request.setAttribute("fromDate", reportTO.getStartDateFrom());
            }
            if (reportCommand.getStartDateTo() != null && !"".equals(reportCommand.getStartDateTo())) {
                reportTO.setStartDateTo(reportCommand.getStartDateTo());
                request.setAttribute("toDate", reportTO.getStartDateTo());
            }
            if (reportCommand.getEndDateFrom() != null && !"".equals(reportCommand.getEndDateFrom())) {
                reportTO.setEndDateFrom(reportCommand.getEndDateFrom());
                request.setAttribute("endDateFrom", reportTO.getEndDateFrom());
            }
            if (reportCommand.getEndDateTo() != null && !"".equals(reportCommand.getEndDateTo())) {
                reportTO.setEndDateTo(reportCommand.getEndDateTo());
                request.setAttribute("endDateTo", reportTO.getEndDateTo());
            }
//            if (reportCommand.getClosedDateFrom() != null && !"".equals(reportCommand.getClosedDateFrom())) {
//                reportTO.setClosedDateFrom(reportCommand.getClosedDateFrom());
//                request.setAttribute("closedDateFrom", reportTO.getClosedDateFrom());
//            }
//            if (reportCommand.getClosedDateTo() != null && !"".equals(reportCommand.getClosedDateTo())) {
//                reportTO.setClosedDateTo(reportCommand.getClosedDateTo());
//                request.setAttribute("closedDateTo", reportTO.getClosedDateTo());
//            }
//            if (reportCommand.getSettledDateFrom() != null && !"".equals(reportCommand.getSettledDateFrom())) {
//                reportTO.setSettledDateFrom(reportCommand.getSettledDateFrom());
//                request.setAttribute("settledDateFrom", reportTO.getSettledDateFrom());
//            }
//            if (reportCommand.getSettledDateTo() != null && !"".equals(reportCommand.getSettledDateTo())) {
//                reportTO.setSettledDateTo(reportCommand.getSettledDateTo());
//                request.setAttribute("settledDateTo", reportTO.getSettledDateTo());
//            }
//            if (reportCommand.getNot() != null) {
//                reportTO.setNot(reportCommand.getNot());
//                request.setAttribute("not", reportTO.getNot());
//            }
            ArrayList tripDetails = new ArrayList();
            tripDetails = reportBP.getTripSheetList(reportTO);
            request.setAttribute("tripDetails", tripDetails);

            ArrayList statusDetails = new ArrayList();
            statusDetails = tripBP.getStatusDetails();

            ArrayList companyList = new ArrayList();
            companyList = companyBP.processGetCompanyList();
            request.setAttribute("companyList", companyList);

            ArrayList vehicleList = new ArrayList();
            vehicleList = tripBP.getVehicleList();
            request.setAttribute("vehicleList", vehicleList);

            ArrayList customerList = new ArrayList();
            customerList = tripBP.getCustomerList();
            request.setAttribute("customerList", customerList);

            ArrayList zoneList = new ArrayList();
            zoneList = tripBP.getZoneList();
            request.setAttribute("zoneList", zoneList);

            ArrayList vehicleTypeList = new ArrayList();
            vehicleTypeList = vehicleBP.getVehicleTypeList();
            request.setAttribute("vehicleTypeList", vehicleTypeList);

            request.setAttribute("statusDetails", statusDetails);
            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                path = "content/report/viewTripSheetExcel.jsp";
            } else {
                path = "content/report/viewTripSheet.jsp";
            }
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to view accounts details for all not paid customer save
     * .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleViewTripSheet(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList accountReceivableList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", customerId = "";
        String ownerShip = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> View Trip Sheet Report";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/viewTripSheetExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/viewTripSheet.jsp";
            }
            System.out.println("path = " + path);
            if (reportCommand.getStartDateFrom() != null && reportCommand.getStartDateFrom() != "") {
                report.setStartDateFrom(reportCommand.getStartDateFrom());
                fromDate = reportCommand.getStartDateFrom();
            }
            if (reportCommand.getStartDateTo() != null && reportCommand.getStartDateTo() != "") {
                report.setStartDateTo(reportCommand.getStartDateTo());
                toDate = reportCommand.getStartDateTo();
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setStartDateFrom(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setStartDateTo(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("page", 1);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleViewTripSheet Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleFCWiseTripSummary(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList accountReceivableList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", customerId = "";
        String ownerShip = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> FC Wise Trip Summary";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/fcWiseTripSummaryExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/fcWiseTripSummary.jsp";
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("page", 1);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleFCWiseTripSummary Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleFCWiseTripSummaryReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View Trip Sheet ";
        String fromDate = "", toDate = "", vehicleId = "", vehicleNo = "", tripId = "";
        try {
            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/fcWiseTripSummaryExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/fcWiseTripSummary.jsp";
            }
            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -1);
                startDate = dateFormat.format(cal.getTime());
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }

            String totalSummaryDetails = "";

            //Operated Trips
            report.setOperationPointId("1024");
            report.setBusinessType("1");
            report.setTripType("1");
            report.setTripStatusId("");
            report.setExtraExpense("");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("totalSummaryDetailsNWLoaded = " + totalSummaryDetails);
            request.setAttribute("totalSummaryDetailsNWLoaded", totalSummaryDetails);
            report.setTripType("0");
            report.setTripStatusId("");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("totalSummaryDetailsNWEmpty = " + totalSummaryDetails);
            request.setAttribute("totalSummaryDetailsNWEmpty", totalSummaryDetails);

            report.setOperationPointId("1025");
            report.setBusinessType("1");
            report.setTripType("1");
            report.setTripStatusId("");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("totalSummaryDetailsNSLoaded = " + totalSummaryDetails);
            request.setAttribute("totalSummaryDetailsNSLoaded", totalSummaryDetails);
            report.setTripType("0");
            report.setTripStatusId("");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("totalSummaryDetailsNSEmpty = " + totalSummaryDetails);
            request.setAttribute("totalSummaryDetailsNSEmpty", totalSummaryDetails);

            report.setOperationPointId("1027");
            report.setBusinessType("1");
            report.setTripType("1");
            report.setTripStatusId("");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("totalSummaryDetailsGNLoaded = " + totalSummaryDetails);
            request.setAttribute("totalSummaryDetailsGNLoaded", totalSummaryDetails);
            report.setTripType("0");
            report.setTripStatusId("");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("totalSummaryDetailsGNEmpty = " + totalSummaryDetails);
            request.setAttribute("totalSummaryDetailsGNEmpty", totalSummaryDetails);

            //Operated Trips
            //Inprogress Trips
            report.setOperationPointId("1024");
            report.setBusinessType("1");
            report.setTripType("1");
            report.setTripStatusId("10");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("inProgressSummaryDetailsNWLoaded = " + totalSummaryDetails);
            request.setAttribute("inProgresstotalSummaryDetailsNWLoaded", totalSummaryDetails);
            report.setTripType("0");
            report.setTripStatusId("10");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("inProgressSummaryDetailsNWEmpty = " + totalSummaryDetails);
            request.setAttribute("inProgressSummaryDetailsNWEmpty", totalSummaryDetails);

            report.setOperationPointId("1025");
            report.setBusinessType("1");
            report.setTripType("1");
            report.setTripStatusId("10");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("inProgressSummaryDetailsNSLoaded = " + totalSummaryDetails);
            request.setAttribute("inProgressSummaryDetailsNSLoaded", totalSummaryDetails);
            report.setTripType("0");
            report.setTripStatusId("10");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("inProgressSummaryDetailsNSEmpty = " + totalSummaryDetails);
            request.setAttribute("inProgressSummaryDetailsNSEmpty", totalSummaryDetails);

            report.setOperationPointId("1027");
            report.setBusinessType("1");
            report.setTripType("1");
            report.setTripStatusId("10");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("inProgressSummaryDetailsGNLoaded = " + totalSummaryDetails);
            request.setAttribute("inProgressSummaryDetailsGNLoaded", totalSummaryDetails);
            report.setTripType("0");
            report.setTripStatusId("10");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("inProgressSummaryDetailsGNEmpty = " + totalSummaryDetails);
            request.setAttribute("inProgressSummaryDetailsGNEmpty", totalSummaryDetails);
            //Inprogress Trips

            //WFU Trips
            report.setOperationPointId("1024");
            report.setBusinessType("1");
            report.setTripType("1");
            report.setTripStatusId("18");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("wfuSummaryDetailsNWLoaded = " + totalSummaryDetails);
            request.setAttribute("wfutotalSummaryDetailsNWLoaded", totalSummaryDetails);
            report.setTripType("0");
            report.setTripStatusId("18");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("wfuSummaryDetailsNWEmpty = " + totalSummaryDetails);
            request.setAttribute("wfuSummaryDetailsNWEmpty", totalSummaryDetails);

            report.setOperationPointId("1025");
            report.setBusinessType("1");
            report.setTripType("1");
            report.setTripStatusId("18");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("wfuSummaryDetailsNSLoaded = " + totalSummaryDetails);
            request.setAttribute("wfuSummaryDetailsNSLoaded", totalSummaryDetails);
            report.setTripType("0");
            report.setTripStatusId("18");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("wfuSummaryDetailsNSEmpty = " + totalSummaryDetails);
            request.setAttribute("wfuSummaryDetailsNSEmpty", totalSummaryDetails);

            report.setOperationPointId("1027");
            report.setBusinessType("1");
            report.setTripType("1");
            report.setTripStatusId("18");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("wfuSummaryDetailsGNLoaded = " + totalSummaryDetails);
            request.setAttribute("wfuSummaryDetailsGNLoaded", totalSummaryDetails);
            report.setTripType("0");
            report.setTripStatusId("18");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("wfuSummaryDetailsGNEmpty = " + totalSummaryDetails);
            request.setAttribute("wfuSummaryDetailsGNEmpty", totalSummaryDetails);
            //WFU Trips

            //Ended Trips
            report.setOperationPointId("1024");
            report.setBusinessType("1");
            report.setTripType("1");
            report.setTripStatusId("12");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("endSummaryDetailsNWLoaded = " + totalSummaryDetails);
            request.setAttribute("endSummaryDetailsNWLoaded", totalSummaryDetails);
            report.setTripType("0");
            report.setTripStatusId("12");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("endSummaryDetailsNWEmpty = " + totalSummaryDetails);
            request.setAttribute("endSummaryDetailsNWEmpty", totalSummaryDetails);

            report.setOperationPointId("1025");
            report.setBusinessType("1");
            report.setTripType("1");
            report.setTripStatusId("12");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("endSummaryDetailsNSLoaded = " + totalSummaryDetails);
            request.setAttribute("endSummaryDetailsNSLoaded", totalSummaryDetails);
            report.setTripType("0");
            report.setTripStatusId("12");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("endSummaryDetailsNSEmpty = " + totalSummaryDetails);
            request.setAttribute("endSummaryDetailsNSEmpty", totalSummaryDetails);

            report.setOperationPointId("1027");
            report.setBusinessType("1");
            report.setTripType("1");
            report.setTripStatusId("12");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("endSummaryDetailsGNLoaded = " + totalSummaryDetails);
            request.setAttribute("endSummaryDetailsGNLoaded", totalSummaryDetails);
            report.setTripType("0");
            report.setTripStatusId("12");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("endSummaryDetailsGNEmpty = " + totalSummaryDetails);
            request.setAttribute("endSummaryDetailsGNEmpty", totalSummaryDetails);

            //Ended Trips
            //Closed Trips
            report.setOperationPointId("1024");
            report.setBusinessType("1");
            report.setTripType("1");
            report.setTripStatusId("13");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("closedSummaryDetailsNWLoaded = " + totalSummaryDetails);
            request.setAttribute("closedSummaryDetailsNWLoaded", totalSummaryDetails);
            report.setTripType("0");
            report.setTripStatusId("13");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("closedSummaryDetailsNWEmpty = " + totalSummaryDetails);
            request.setAttribute("closedSummaryDetailsNWEmpty", totalSummaryDetails);

            report.setOperationPointId("1025");
            report.setBusinessType("1");
            report.setTripType("1");
            report.setTripStatusId("13");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("closedSummaryDetailsNSLoaded = " + totalSummaryDetails);
            request.setAttribute("closedSummaryDetailsNSLoaded", totalSummaryDetails);
            report.setTripType("0");
            report.setTripStatusId("13");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("closedSummaryDetailsNSEmpty = " + totalSummaryDetails);
            request.setAttribute("closedSummaryDetailsNSEmpty", totalSummaryDetails);

            report.setOperationPointId("1027");
            report.setBusinessType("1");
            report.setTripType("1");
            report.setTripStatusId("13");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("closedSummaryDetailsGNLoaded = " + totalSummaryDetails);
            request.setAttribute("closedSummaryDetailsGNLoaded", totalSummaryDetails);
            report.setTripType("0");
            report.setTripStatusId("13");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("closedSummaryDetailsGNEmpty = " + totalSummaryDetails);
            request.setAttribute("closedSummaryDetailsGNEmpty", totalSummaryDetails);

            //Closed Trips
            //Settled Trips
            report.setOperationPointId("1024");
            report.setBusinessType("1");
            report.setTripType("1");
            report.setTripStatusId("14");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("settledSummaryDetailsNWLoaded = " + totalSummaryDetails);
            request.setAttribute("settledSummaryDetailsNWLoaded", totalSummaryDetails);
            report.setTripType("0");
            report.setTripStatusId("14");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("settledSummaryDetailsNWEmpty = " + totalSummaryDetails);
            request.setAttribute("settledSummaryDetailsNWEmpty", totalSummaryDetails);

            report.setOperationPointId("1025");
            report.setBusinessType("1");
            report.setTripType("1");
            report.setTripStatusId("14");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("settledSummaryDetailsNSLoaded = " + totalSummaryDetails);
            request.setAttribute("settledSummaryDetailsNSLoaded", totalSummaryDetails);
            report.setTripType("0");
            report.setTripStatusId("14");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("settledSummaryDetailsNSEmpty = " + totalSummaryDetails);
            request.setAttribute("settledSummaryDetailsNSEmpty", totalSummaryDetails);

            report.setOperationPointId("1027");
            report.setBusinessType("1");
            report.setTripType("1");
            report.setTripStatusId("14");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("settledSummaryDetailsGNLoaded = " + totalSummaryDetails);
            request.setAttribute("settledSummaryDetailsGNLoaded", totalSummaryDetails);
            report.setTripType("0");
            report.setTripStatusId("14");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("settledSummaryDetailsGNEmpty = " + totalSummaryDetails);
            request.setAttribute("settledSummaryDetailsGNEmpty", totalSummaryDetails);

            //Settled Trips
            //Extra Expense
            report.setOperationPointId("1024");
            report.setBusinessType("1");
            report.setTripType("1");
            report.setTripStatusId("");
            report.setExtraExpense("1");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("expSummaryDetailsNWLoadedExp = " + totalSummaryDetails);
            request.setAttribute("expSummaryDetailsNWLoadedExp", totalSummaryDetails);
            report.setTripType("0");
            report.setTripStatusId("");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("expSummaryDetailsNWEmptyExp = " + totalSummaryDetails);
            request.setAttribute("expSummaryDetailsNWEmptyExp", totalSummaryDetails);

            report.setOperationPointId("1025");
            report.setBusinessType("1");
            report.setTripType("1");
            report.setTripStatusId("");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("expSummaryDetailsNSLoaded = " + totalSummaryDetails);
            request.setAttribute("expSummaryDetailsNSLoaded", totalSummaryDetails);
            report.setTripType("0");
            report.setTripStatusId("");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("expSummaryDetailsNSEmpty = " + totalSummaryDetails);
            request.setAttribute("expSummaryDetailsNSEmpty", totalSummaryDetails);

            report.setOperationPointId("1027");
            report.setBusinessType("1");
            report.setTripType("1");
            report.setTripStatusId("");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("expSummaryDetailsGNLoaded = " + totalSummaryDetails);
            request.setAttribute("expSummaryDetailsGNLoaded", totalSummaryDetails);
            report.setTripType("0");
            report.setTripStatusId("");
            report.setQueryType("0");
            totalSummaryDetails = reportBP.getTotalTripSummaryDetails(report);
            System.out.println("expSummaryDetailsGNEmpty = " + totalSummaryDetails);
            request.setAttribute("expSummaryDetailsGNEmpty", totalSummaryDetails);
            //Settled Trips

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleMarginWiseReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList accountReceivableList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", customerId = "";
        String ownerShip = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Margin Wise Trip Summary";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/marginWiseTripSummaryExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/marginWiseTripSummary.jsp";
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }

            ArrayList customerList = new ArrayList();
            customerList = tripBP.getCustomerList();
            request.setAttribute("customerList", customerList);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("page", 1);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleMarginWiseTripSummary Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleMarginWiseTripSummaryReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View Trip Sheet ";
        String fromDate = "", toDate = "", customerId = "", customerName = "", tripId = "";
        try {
            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/marginWiseTripSummaryExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/marginWiseTripSummary.jsp";
            }
            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getCustomerId() != null && reportCommand.getCustomerId() != "") {
                report.setCustomerId(reportCommand.getCustomerId());
                customerId = reportCommand.getCustomerId();
            }
            if (reportCommand.getCustomerName() != null && reportCommand.getCustomerName() != "") {
                customerName = reportCommand.getCustomerName();
            }
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -1);
                startDate = dateFormat.format(cal.getTime());
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }

            String totalSummaryDetails = "";
            String[] valueTemp = null;
            int[] percent = new int[10];
            percent = new int[]{40, 30, 20, 10, 0, 100};
            for (int i = 0; i < percent.length; i++) {
                System.out.println("i = " + percent[i]);
                report.setPercent(percent[i]);
                totalSummaryDetails = reportBP.getMarginWiseTripSummary(report);
                if (totalSummaryDetails != null) {
                    valueTemp = totalSummaryDetails.split("~");
                    request.setAttribute("margin" + percent[i], valueTemp[0]);
                    request.setAttribute("trip" + percent[i], valueTemp[1]);
                } else {
                    request.setAttribute("margin" + percent[i], 0);
                    request.setAttribute("trip" + percent[i], 0);
                }
                request.setAttribute("showDetail", 1);
            }
            ArrayList customerList = new ArrayList();
            customerList = tripBP.getCustomerList();
            request.setAttribute("customerList", customerList);

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("customerId", customerId);
            request.setAttribute("customerName", customerName);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleMonthWiseEmptyRunSummary(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList accountReceivableList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", customerId = "";
        String ownerShip = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Month Wise Emty Run Summary";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/monthWiseEmptyRunSummaryExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/monthWiseEmptyRunSummary.jsp";
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("page", 1);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleMonthWiseEmptyRunSummary Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleMonthWiseEmptyRunSummaryReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports  >>  Month Wise Emty Run Summary ";
        String fromDate = "", toDate = "", customerId = "", customerName = "", tripId = "";
        try {
            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/monthWiseEmptyRunSummaryExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/monthWiseEmptyRunSummary.jsp";
            }
            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -1);
                startDate = dateFormat.format(cal.getTime());
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }

            ArrayList monthWiseEmptyRunSummary = new ArrayList();
            monthWiseEmptyRunSummary = reportBP.getMonthWiseEmptyRunSummary(report);
            request.setAttribute("monthWiseEmptyRunSummary", monthWiseEmptyRunSummary);

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("handleMonthWiseEmptyRunSummaryReport --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleJobcardSumary(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList accountReceivableList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", customerId = "";
        String ownerShip = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Jobcard Summary";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/jobcardSummaryExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/jobcardSummary.jsp";
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("page", 1);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleFCWiseTripSummary Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleJobcardSumaryReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports  >>  Jobcard Summary ";
        String fromDate = "", toDate = "", customerId = "", customerName = "", tripId = "";
        try {
            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/jobcardSummaryExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/jobcardSummary.jsp";
            }
            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -1);
                startDate = dateFormat.format(cal.getTime());
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }

            ArrayList jobcardSummary = new ArrayList();
            report.setServiceTypeId("14");
            jobcardSummary = reportBP.getJobcardSummary(report);
            request.setAttribute("jobcardSummaryDriverIssue", jobcardSummary);
            report.setServiceTypeId("");
            jobcardSummary = reportBP.getJobcardSummary(report);
            request.setAttribute("jobcardSummary", jobcardSummary);

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("handleMonthWiseEmptyRunSummaryReport --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleTripMergingDetails(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View BPCL Transaction Sheet ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            path = "content/report/BrattleFoods/TripMergingReport.jsp";
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleTripMergingDetailsExcel(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View BPCL Transaction Sheet ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            String param = request.getParameter("param");
            System.out.println("the param value is" + param);
            if (param != null && !"".equals(param)) {
                if (param.equals("ExportExcel")) {
                    System.out.println("111 = ");
                    path = "content/report/BrattleFoods/TripMergingReportExcel.jsp";
                } else {
                    path = "content/report/BrattleFoods/TripMergingReport.jsp";
                }
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            ArrayList TripMergingDetails = new ArrayList();
            TripMergingDetails = reportBP.getTripMergingDetails(report, userId);
            System.out.println("TripMergingDetailsSize" + TripMergingDetails.size());
            request.setAttribute("TripMergingDetails", TripMergingDetails);
            request.setAttribute("TripMergingDetailsSize", TripMergingDetails.size());
            if (TripMergingDetails.size() == 0) {
                path = "content/report/BrattleFoods/TripMergingReport.jsp";
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");
            }

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVehicleOdometerReading(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View BPCL Transaction Sheet ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            path = "content/report/BrattleFoods/vehicleLastKnownReading.jsp";
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVehicleOdometerReadingExcel(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View BPCL Transaction Sheet ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            String param = request.getParameter("param");
            System.out.println("the param value is" + param);
            if (param != null && !"".equals(param)) {
                if (param.equals("ExportExcel")) {
                    System.out.println("111 = ");
                    path = "content/report/BrattleFoods/vehicleLastKnownReadingExcel.jsp";
                } else {
                    path = "content/report/BrattleFoods/vehicleLastKnownReading.jsp";
                }
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            String vehicletype = request.getParameter("vehicletype");
            report.setVehicletype(vehicletype);
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            ArrayList vehicleReadingDetails = new ArrayList();
            vehicleReadingDetails = reportBP.getVehicleReadingDetails(report, userId);
            System.out.println("vehicleReadingDetailsSize" + vehicleReadingDetails.size());
            request.setAttribute("vehicleReadingDetails", vehicleReadingDetails);
            request.setAttribute("vehicleReadingDetailsSize", vehicleReadingDetails.size());
            if (vehicleReadingDetails.size() == 0) {
                path = "content/report/BrattleFoods/vehicleLastKnownReading.jsp";
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");
            }

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleCustomerWiseMergingProfitReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList customerWiseProfitList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", customerId = "";
        String ownerShip = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Customer Wise Profitability";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/customerWiseMergingProfitExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/customerWiseMergingProfit.jsp";
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getCustomerId() != null && reportCommand.getCustomerId() != "") {
                report.setCustomerId(reportCommand.getCustomerId());
                customerId = reportCommand.getCustomerId();
            }
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("customerId", customerId);
            request.setAttribute("page", 1);

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleAccountsReceivable Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleCustomerWiseMergingProfit(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", customerId = "";
        String ownerShip = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Customer Wise Profitability";
        request.setAttribute("menuPath", menuPath);
        try {

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/BrattleFoods/customerWiseMergingProfitExcel.jsp";
            } else {
                //System.out.println("222 = ");
                path = "content/report/BrattleFoods/customerWiseMergingProfit.jsp";
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getCustomerId() != null && reportCommand.getCustomerId() != "") {
                report.setCustomerId(reportCommand.getCustomerId());
                request.setAttribute("customerId", reportCommand.getCustomerId());
            } else {
                report.setCustomerId("");
            }
            request.setAttribute("customerId", reportCommand.getCustomerId());

            int i = 0;
            double earnings = 0.0;
            double totalkms = 0.0;
            double totalHms = 0.0;
            double emptyEarnings = 0.0;
            double loadedEarnings = 0.0;
            double netExpenses = 0.0;
            double emptyExpenses = 0.0;
            double loadedExpenses = 0.0;
            double totalExpenses = 0.0;
            double netProfit = 0.0;

            ArrayList customerLists = new ArrayList();
            customerLists = reportBP.getCustomerLists(report, userId);
            request.setAttribute("customerLists", customerLists);
            System.out.println("customerLists list size::" + customerLists.size());

            ArrayList customerMergingProfitList = new ArrayList();
            request.setAttribute("toDate", reportCommand.getToDate());
            request.setAttribute("fromDate", reportCommand.getFromDate());

            ArrayList tripMergingList = new ArrayList();
            ArrayList tripNotMergingList = new ArrayList();
            ArrayList customerTripMergingList = new ArrayList();
            ArrayList customerTripNotMergingList = new ArrayList();
            ArrayList tripMergingListAll = new ArrayList();
            if (!"".equals(fromDate) && !"".equals(toDate)) {
                //tripMergingList = reportBP.getTripMergingList(report, userId);
//                if (tripMergingList.size() == 0) {
//                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found.");
//                    request.setAttribute("tripMergingList", 0);
//                } else {
                ReportTO repTO = new ReportTO();
                DprTO repTO3 = new DprTO();
                ReportTO rpTO = new ReportTO();
                ReportTO repTO1 = new ReportTO();
                DprTO repTO2 = new DprTO();
                String custId = "";
                String customerName = "";
                String billingType = "";
                String tripId = "";
                Iterator itr1 = customerLists.iterator();
                while (itr1.hasNext()) {
                    i = 0;
                    earnings = 0.0;
                    emptyEarnings = 0.0;
                    loadedEarnings = 0.0;
                    netExpenses = 0.0;
                    emptyExpenses = 0.0;
                    loadedExpenses = 0.0;
                    totalExpenses = 0.0;
                    totalkms = 0.0;
                    totalHms = 0.0;
                    tripId = "";
                    netProfit = 0.0;
                    repTO2 = new DprTO();
                    repTO1 = new ReportTO();
                    repTO1 = (ReportTO) itr1.next();
                    if (!repTO1.getCustomerId().equals("1040")) {
                        report.setCustomerId(repTO1.getCustomerId());
                        System.out.println("repTO1.getCustomerId()= " + repTO1.getCustomerId());
                        tripMergingList = reportBP.getTripMergingList(report, userId);
                        System.out.println("tripMergingList.size() = " + tripMergingList.size());
                        Iterator itr = tripMergingList.iterator();
                        while (itr.hasNext()) {
                            repTO = new ReportTO();
                            repTO = (ReportTO) itr.next();
                            customerTripMergingList = reportBP.getCustomerMergingList(repTO, userId);
                            System.out.println("customerTripMergingList.size() = " + customerTripMergingList.size());
                            Iterator itr2 = customerTripMergingList.iterator();
                            while (itr2.hasNext()) {
                                customerMergingProfitList = new ArrayList();
                                rpTO = new ReportTO();
                                rpTO = (ReportTO) itr2.next();
                                totalkms += Double.parseDouble(rpTO.getTotalkms());
                                totalHms += Double.parseDouble(rpTO.getTotalHms());
                                System.out.println("customerTripMerging total km = " + rpTO.getTotalkms());
                                System.out.println("customerTripMerging total hm = " + rpTO.getTotalHms());
                                if (rpTO.getCustomerId().equals("1040")) {
                                    emptyExpenses += Double.parseDouble(rpTO.getExpenses());
                                } else {
                                    i++;
                                    loadedEarnings += Double.parseDouble(rpTO.getRevenue());
                                    loadedExpenses += Double.parseDouble(rpTO.getExpenses());

                                }
                                if (tripId.equals("")) {
                                    tripId = rpTO.getTripId();
                                } else {
                                    tripId = tripId + "," + rpTO.getTripId();
                                }
                                custId = rpTO.getCustomerId();
                                customerName = rpTO.getCustomerName();
                                billingType = rpTO.getBillingType();
                                System.out.println("customer id in loop 1 = " + custId);
                                System.out.println("customer name in loop 1 = " + customerName);

                            }
                            System.out.println("tripcount = " + i);
                        }
                        tripNotMergingList = reportBP.getTripNotMergingList(report, userId);
                        System.out.println("tripNotMergingList.size() = " + tripNotMergingList.size());
                        if (tripNotMergingList.size() > 0) {
                            Iterator itrNotMerg = tripNotMergingList.iterator();
                            while (itrNotMerg.hasNext()) {
                                repTO = new ReportTO();
                                repTO = (ReportTO) itrNotMerg.next();
                                customerTripNotMergingList = reportBP.getCustomerTripNotMergingList(repTO, userId);
                                System.out.println("customerTripNotMergingList.size() = " + customerTripNotMergingList.size());
                                Iterator itr2 = customerTripNotMergingList.iterator();
                                while (itr2.hasNext()) {
                                    customerTripNotMergingList = new ArrayList();
                                    rpTO = new ReportTO();
                                    rpTO = (ReportTO) itr2.next();
                                    if (rpTO.getCustomerId().equals("1040")) {
                                        emptyExpenses += Double.parseDouble(rpTO.getExpenses());
                                    } else {
                                        i++;
                                        loadedEarnings += Double.parseDouble(rpTO.getRevenue());
                                        loadedExpenses += Double.parseDouble(rpTO.getExpenses());
                                        System.out.println("loadedEaring:" + loadedEarnings);

                                    }
                                    if (tripId.equals("")) {
                                        tripId = rpTO.getTripId();
                                    } else {
                                        tripId = tripId + "," + rpTO.getTripId();
                                    }
                                    custId = rpTO.getCustomerId();
                                    customerName = rpTO.getCustomerName();
                                    System.out.println("customerName in not merge:" + customerName);
                                    billingType = rpTO.getBillingType();
                                    totalkms += Double.parseDouble(rpTO.getTotalkms());
                                    totalHms += Double.parseDouble(rpTO.getTotalHms());
                                }
                            }
                        }
                        if (tripMergingList.size() > 0 || tripNotMergingList.size() > 0) {
                            System.out.println("i m in mergingList");
                            System.out.println("i m in mergingList0:" + custId);
                            System.out.println("i m in mergingList1:" + customerName);
                            repTO2.setCustomerId(custId);
                            repTO2.setCustomerName(customerName);
                            System.out.println("customer name 1:" + repTO.getCustomerName());
                            repTO2.setBillingType(billingType);
                            repTO2.setTotalkms(String.valueOf(totalkms));
                            repTO2.setTotalHms(String.valueOf(totalHms));
                            repTO2.setLoadedExpense(String.valueOf(loadedExpenses));
                            repTO2.setEarnings(String.valueOf(loadedEarnings));
                            repTO2.setEmptyExpense(String.valueOf(emptyExpenses));
                            repTO2.setTripId(tripId);
                            repTO2.setTripCount(String.valueOf(i));
                            tripMergingListAll.add(repTO2);

                        }
                    }

                }
                Iterator itr3 = tripMergingListAll.iterator();
                while (itr3.hasNext()) {
                    repTO2 = new DprTO();
                    repTO2 = (DprTO) itr3.next();
                    System.out.println("totalcustomerList:" + repTO2.getCustomerName());

                }
                System.out.println("tripMergingListAll.size() = " + tripMergingListAll.size());
                request.setAttribute("tripNos", i);
                request.setAttribute("earnings", earnings);
                request.setAttribute("netExpenses", netExpenses);
                request.setAttribute("netProfit", netProfit);
                request.setAttribute("fromDate", fromDate);
                request.setAttribute("toDate", toDate);
                request.setAttribute("tripMergingList", tripMergingListAll);

            }

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleAccountsReceivable Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewCustomerWiseProfitMergingDetails(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList customerWiseProfitList = new ArrayList();
        ReportTO reportTO = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "", customerId = "";
        String ownerShip = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Customer Wise Profitability";
        request.setAttribute("menuPath", menuPath);
        ArrayList popupCustomerProfitReport = new ArrayList();
        try {
            String param = request.getParameter("param");
            System.out.println("param" + param);
            if (param.equals("ExportExcel")) {

                path = "content/report/BrattleFoods/popupCustomerMergingProfitDetailsExcel.jsp";
            } else {
                path = "content/report/BrattleFoods/popUpCustomerWiseMergingProfit.jsp";
            }
            String tripId = request.getParameter("tripId");
            request.setAttribute("tripId", tripId);
            reportTO.setTripId(tripId);
            popupCustomerProfitReport = reportBP.getPopupCustomerMergingProfitReportDetails(reportTO);
            System.out.println("popupCustomerProfitReport" + popupCustomerProfitReport.size());
            request.setAttribute("popupCustomerProfitReport", popupCustomerProfitReport);
            //            System.out.println("getPopupCustomerProfitReportDetails.size() in Controller" + getPopupCustomerProfitReportDetails.size());
            //            path = "content/report/BrattleFoods/popUpCustomerWiseProfit.jsp";

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleAccountsReceivable Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleTripExtraExpenseReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View BPCL Transaction Sheet ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            path = "content/report/BrattleFoods/tripExtraExpenseReport.jsp";
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleTripExtraExpenseReportExcel(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View BPCL Transaction Sheet ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            String param = request.getParameter("param");
            System.out.println("the param value is" + param);
            if (param != null && !"".equals(param)) {
                if (param.equals("ExportExcel")) {
                    System.out.println("111 = ");
                    path = "content/report/BrattleFoods/tripExtraExpenseReportExcel.jsp";
                } else {
                    path = "content/report/BrattleFoods/tripExtraExpenseReport.jsp";
                }
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            ArrayList TripExtraExpenseDetails = new ArrayList();
            TripExtraExpenseDetails = reportBP.getTripExtraExpenseDetails(report, userId);
            System.out.println("TripExtraExpenseDetailsSize:" + TripExtraExpenseDetails.size());
            request.setAttribute("TripExtraExpenseDetails", TripExtraExpenseDetails);
            request.setAttribute("TripExtraExpenseDetailsSize", TripExtraExpenseDetails.size());
            if (TripExtraExpenseDetails.size() == 0) {
                path = "content/report/BrattleFoods/tripExtraExpenseReport.jsp";
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");
            }

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVehicleDriverAdvance(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View Vehicle Driver Advance Sheet ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            path = "content/report/BrattleFoods/vehicleDriverAdvance.jsp";
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVehicleDriverAdvanceExcel(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View Vehicle Driver Advance Sheet ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            String param = request.getParameter("param");
            System.out.println("the param value is" + param);
            if (param != null && !"".equals(param)) {
                if (param.equals("ExportExcel")) {
                    System.out.println("111 = ");
                    path = "content/report/BrattleFoods/vehicleDriverAdvanceExcel.jsp";
                } else {
                    path = "content/report/BrattleFoods/vehicleDriverAdvance.jsp";
                }
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            String expensetype = request.getParameter("expensetype");
            String fleet = request.getParameter("fleet");
            report.setExpenseType(expensetype);
            report.setUsageType(fleet);
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("expensetype", expensetype);
            request.setAttribute("fleet", fleet);
            ArrayList vehicleDriverAdvanceDetails = new ArrayList();
            vehicleDriverAdvanceDetails = reportBP.getVehicleDriverAdvanceDetails(report, userId);
            System.out.println("getVehicleDriverAdvanceDetails" + vehicleDriverAdvanceDetails.size());
            request.setAttribute("vehicleDriverAdvanceDetails", vehicleDriverAdvanceDetails);
            request.setAttribute("vehicleDriverAdvanceDetailssize", vehicleDriverAdvanceDetails.size());
            if (vehicleDriverAdvanceDetails.size() == 0) {
                path = "content/report/BrattleFoods/vehicleDriverAdvance.jsp";
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");
            }

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewTripSheetWfl(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList accountReceivableList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "";
        String fromDate = "";
        String toDate = "";
        String customerId = "";
        String ownerShip = "";
        int userId = ((Integer) session.getAttribute("userId")).intValue();
        String menuPath = "Reports >> View Trip Sheet Report With WFL & WFU";
        request.setAttribute("menuPath", menuPath);
        try {
            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                path = "content/report/viewTripSheetExcelWfl.jsp";
            } else {
                path = "content/report/viewTripSheetWfl.jsp";
            }
            System.out.println((new StringBuilder()).append("path = ").append(path).toString());
            if (reportCommand.getStartDateFrom() != null && reportCommand.getStartDateFrom() != "") {
                report.setStartDateFrom(reportCommand.getStartDateFrom());
                fromDate = reportCommand.getStartDateFrom();
            }
            if (reportCommand.getStartDateTo() != null && reportCommand.getStartDateTo() != "") {
                report.setStartDateTo(reportCommand.getStartDateTo());
                toDate = reportCommand.getStartDateTo();
            }
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String temp[] = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = (new StringBuilder()).append("01-").append(temp[1]).append("-").append(temp[2]).toString();
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setStartDateFrom(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setStartDateTo(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("page", Integer.valueOf(1));
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewTripSheetReportWfl(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;
        String menuPath = "Operation  >>  View Trip Sheet Report";
        String statusId1 = request.getParameter("statusId");
        System.out.println((new StringBuilder()).append("pass value:").append(request.getParameter("successMessage")).toString());
        request.setAttribute("errorMessage", request.getParameter("msg"));
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            request.setAttribute("menuPath", menuPath);
            String pageTitle = "View Trip Sheet Repor";
            request.setAttribute("pageTitle", pageTitle);
            ReportTO reportTO = new ReportTO();
            if (reportCommand.getFleetCenterId() != null && !"".equals(reportCommand.getFleetCenterId())) {
                reportTO.setFleetCenterId(reportCommand.getFleetCenterId());
                request.setAttribute("fleetCenterId", reportTO.getFleetCenterId());
            }
            if (reportCommand.getVehicleId() != null && !"".equals(reportCommand.getVehicleId())) {
                reportTO.setVehicleId(reportCommand.getVehicleId());
                request.setAttribute("vehicleId", reportTO.getVehicleId());
            }
            if (reportCommand.getVehicleTypeId() != null && !"".equals(reportCommand.getVehicleTypeId())) {
                reportTO.setVehicleTypeId(reportCommand.getVehicleTypeId());
                request.setAttribute("vehicleTypeId", reportTO.getVehicleTypeId());
            }
            if (reportCommand.getCustomerId() != null && !"".equals(reportCommand.getCustomerId())) {
                reportTO.setCustomerId(reportCommand.getCustomerId());
                request.setAttribute("customerId", reportTO.getCustomerId());
            }
            if (reportCommand.getTripStatusId() != null && !"".equals(reportCommand.getTripStatusId())) {
                reportTO.setTripStatusId(reportCommand.getTripStatusId());
                request.setAttribute("tripStatusId", reportTO.getTripStatusId());
            }
            if (reportCommand.getTripStatusIdTo() != null && !"".equals(reportCommand.getTripStatusIdTo())) {
                reportTO.setTripStatusIdTo(reportCommand.getTripStatusIdTo());
                request.setAttribute("tripStatusIdTo", reportTO.getTripStatusIdTo());
            }
            if (reportCommand.getStartDateFrom() != null && !"".equals(reportCommand.getStartDateFrom())) {
                reportTO.setStartDateFrom(reportCommand.getStartDateFrom());
                request.setAttribute("fromDate", reportTO.getStartDateFrom());
            }
            if (reportCommand.getStartDateTo() != null && !"".equals(reportCommand.getStartDateTo())) {
                reportTO.setStartDateTo(reportCommand.getStartDateTo());
                request.setAttribute("toDate", reportTO.getStartDateTo());
            }
            if (reportCommand.getEndDateFrom() != null && !"".equals(reportCommand.getEndDateFrom())) {
                reportTO.setEndDateFrom(reportCommand.getEndDateFrom());
                request.setAttribute("endDateFrom", reportTO.getEndDateFrom());
            }
            if (reportCommand.getEndDateTo() != null && !"".equals(reportCommand.getEndDateTo())) {
                reportTO.setEndDateTo(reportCommand.getEndDateTo());
                request.setAttribute("endDateTo", reportTO.getEndDateTo());
            }
            if (reportCommand.getClosedDateFrom() != null && !"".equals(reportCommand.getClosedDateFrom())) {
                reportTO.setClosedDateFrom(reportCommand.getClosedDateFrom());
                request.setAttribute("closedDateFrom", reportTO.getClosedDateFrom());
            }
            if (reportCommand.getClosedDateTo() != null && !"".equals(reportCommand.getClosedDateTo())) {
                reportTO.setClosedDateTo(reportCommand.getClosedDateTo());
                request.setAttribute("closedDateTo", reportTO.getClosedDateTo());
            }
            if (reportCommand.getSettledDateFrom() != null && !"".equals(reportCommand.getSettledDateFrom())) {
                reportTO.setSettledDateFrom(reportCommand.getSettledDateFrom());
                request.setAttribute("settledDateFrom", reportTO.getSettledDateFrom());
            }
            if (reportCommand.getSettledDateTo() != null && !"".equals(reportCommand.getSettledDateTo())) {
                reportTO.setSettledDateTo(reportCommand.getSettledDateTo());
                request.setAttribute("settledDateTo", reportTO.getSettledDateTo());
            }
            if (reportCommand.getNot() != null) {
                reportTO.setNot(reportCommand.getNot());
                request.setAttribute("not", reportTO.getNot());
            }
            ArrayList tripDetailsList = new ArrayList();
            ArrayList tripDetailsListAll = new ArrayList();
            ArrayList vehicleList = new ArrayList();
            String wflHours = "";
            vehicleList = reportBP.getTripVehicleList(reportTO);
            request.setAttribute("vehicleList", vehicleList);
            System.out.println((new StringBuilder()).append("vehicleList.size() = ").append(vehicleList.size()).toString());
            ArrayList tripDetails = new ArrayList();
            vehicleList = reportBP.getVehicleList(reportTO);
            Iterator itr = vehicleList.iterator();
            ReportTO repTO = new ReportTO();
            ReportTO repTO1 = new ReportTO();
            DprTO dprTO = new DprTO();
            while (itr.hasNext()) {
                repTO = new ReportTO();
                repTO = (ReportTO) itr.next();
                reportTO.setVehicleId(repTO.getVehicleId());
                tripDetails = reportBP.getTripSheetListWfl(reportTO);
                System.out.println((new StringBuilder()).append("tripDetails.size() = ").append(tripDetails.size()).toString());
                Iterator itr1 = tripDetails.iterator();
                while (itr1.hasNext()) {
                    dprTO = new DprTO();
                    repTO1 = new ReportTO();
                    repTO1 = (ReportTO) itr1.next();
                    wflHours = reportBP.getWflHours(repTO1);
                    System.out.println((new StringBuilder()).append("wflHours = ").append(wflHours).toString());
                    dprTO.setWflHours(wflHours);
                    dprTO.setWfuHours(repTO1.getWfuHours());
                    dprTO.setTransitHours(repTO1.getTransitHours());
                    dprTO.setLoadingTransitHours(repTO1.getLoadingTransitHours());
                    dprTO.setStartDateTime(repTO1.getStartDateTime());
                    dprTO.setStartDate(repTO1.getStartDate());
                    dprTO.setEndDateTime(repTO1.getEndDateTime());
                    dprTO.setWfuDateTime(repTO1.getWfuDateTime());
                    dprTO.setOriginReportingDateTime(repTO1.getOriginReportingDateTime());
                    dprTO.setDestinationReportingDateTime(repTO1.getDestinationReportingDateTime());
                    dprTO.setTotalRunKm(repTO1.getTotalRunKm());
                    dprTO.setTotalRunHm(repTO1.getTotalRunHm());
                    dprTO.setEstimatedRevenue(repTO1.getEstimatedRevenue());
                    dprTO.setRcm(repTO1.getRcm());
                    dprTO.setActualAdvancePaid(repTO1.getActualAdvancePaid());
                    dprTO.setActualExpense(repTO1.getActualExpense());
                    dprTO.setTripCode(repTO1.getTripCode());
                    dprTO.setCustomerName(repTO1.getCustomerName());
                    dprTO.setRouteInfo(repTO1.getRouteInfo());
                    dprTO.setVehicleTypeName(repTO1.getVehicleTypeName());
                    dprTO.setRegno(repTO1.getRegno());
                    dprTO.setFleetCenterName(repTO1.getFleetCenterName());
                    dprTO.setStatusName(repTO1.getStatusName());
                    tripDetailsList.add(dprTO);
                }
            }
            request.setAttribute("tripDetails", tripDetailsList);
            ArrayList statusDetails = new ArrayList();
            statusDetails = tripBP.getStatusDetails();
            ArrayList companyList = new ArrayList();
            companyList = companyBP.processGetCompanyList();
            request.setAttribute("companyList", companyList);
            ArrayList customerList = new ArrayList();
            customerList = tripBP.getCustomerList();
            request.setAttribute("customerList", customerList);
            ArrayList zoneList = new ArrayList();
            zoneList = tripBP.getZoneList();
            request.setAttribute("zoneList", zoneList);
            ArrayList vehicleTypeList = new ArrayList();
            vehicleTypeList = vehicleBP.getVehicleTypeList();
            request.setAttribute("vehicleTypeList", vehicleTypeList);
            request.setAttribute("statusDetails", statusDetails);
            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                path = "content/report/viewTripSheetExcelWfl.jsp";
            } else {
                path = "content/report/viewTripSheetWfl.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleLatestUpdates(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View Latest Updates ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            ArrayList latestUpdates = new ArrayList();
            latestUpdates = reportBP.getLatestUpdates(report);

            if (latestUpdates.size() > 0) {
                request.setAttribute("latestUpdates", latestUpdates);
            }
            path = "content/report/BrattleFoods/latestUpdates.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVmrList(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View Latest Updates ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            ArrayList vmrList = new ArrayList();
            vmrList = reportBP.getVmrList(report);
            System.out.println("vmrList size = " + vmrList.size());
            if (vmrList.size() > 0) {
                request.setAttribute("vmrList", vmrList);
            }
            path = "content/report/BrattleFoods/vmrList.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleToPayCustomerTripDetails(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View BPCL Transaction Sheet ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            path = "content/report/BrattleFoods/ToPayCustomerTripDetailsReport.jsp";
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleToPayCustomerTripDetailsExcel(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View BPCL Transaction Sheet ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            String param = request.getParameter("param");
            System.out.println("the param value is" + param);
            if (param != null && !"".equals(param)) {
                if (param.equals("ExportExcel")) {
                    System.out.println("111 = ");
                    path = "content/report/BrattleFoods/ToPayCustomerTripDetailsReportExcel.jsp";
                } else {
                    path = "content/report/BrattleFoods/ToPayCustomerTripDetailsReport.jsp";
                }
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            ArrayList ToPayCustomerTripDetails = new ArrayList();
            ToPayCustomerTripDetails = reportBP.getToPayCustomerTripDetails(report, userId);
            System.out.println("ToPayCustomerTripDetailsSize" + ToPayCustomerTripDetails.size());
            request.setAttribute("ToPayCustomerTripDetails", ToPayCustomerTripDetails);
            request.setAttribute("ToPayCustomerTripDetailsSize", ToPayCustomerTripDetails.size());
            if (ToPayCustomerTripDetails.size() == 0) {
                path = "content/report/BrattleFoods/ToPayCustomerTripDetailsReport.jsp";
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");
            }

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleTripVmrReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View Vmr Reprot   ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setStartDateFrom(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setStartDateTo(toDate);
            }
            //  request.setAttribute("fromDate", fromDate);
            //  request.setAttribute("toDate", toDate);
            path = "content/report/BrattleFoods/tripVmrReport.jsp";
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleTripVmrReportExcel(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Operation  >>  View trip vmr ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        String endDateFrom = "";
        String endDateTo = "";
        try {
            String param = request.getParameter("param");
            System.out.println("the param value is" + param);
            if (param != null && !"".equals(param)) {
                if (param.equals("ExportExcel")) {
                    System.out.println("111 = ");
                    path = "content/report/BrattleFoods/tripVmrReportExcel.jsp";
                } else {
                    path = "content/report/BrattleFoods/tripVmrReport.jsp";
                }
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();

            }

//                      if (reportCommand.getStartDateFrom() != null && !"".equals(reportCommand.getStartDateFrom())) {
//	                report.setStartDateFrom(reportCommand.getStartDateFrom());
//	               // request.setAttribute("fromDate", report.getStartDateFrom());
//	            }
//	            if (reportCommand.getStartDateTo() != null && !"".equals(reportCommand.getStartDateTo())) {
//	                report.setStartDateTo(reportCommand.getStartDateTo());
//	              //  request.setAttribute("toDate", report.getStartDateTo());
//	            }
            if (reportCommand.getEndDateFrom() != null && !"".equals(reportCommand.getEndDateFrom())) {
                report.setEndDateFrom(reportCommand.getEndDateFrom());
                endDateFrom = reportCommand.getEndDateFrom();
            }
            if (reportCommand.getEndDateTo() != null && !"".equals(reportCommand.getEndDateTo())) {
                report.setEndDateTo(reportCommand.getEndDateTo());
                endDateTo = reportCommand.getEndDateTo();
            }
            System.out.println("endDateFrom:" + endDateFrom);
            System.out.println("endDateTo:" + endDateTo);
//           DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
//            Date curDate = new Date();
//            System.out.println(dateFormat.format(curDate));
//            String endDate = dateFormat.format(curDate);
//            String startDate = "";
//            String[] temp = null;
//            if (!"".equals(endDate)) {
//                temp = endDate.split("-");
//                startDate = "01" + "-" + temp[1] + "-" + temp[2];
//            }
//            if ("".equals(fromDate)) {
//                fromDate = startDate;
//                report.setStartDateFrom(fromDate);
//            }
//            if ("".equals(toDate)) {
//               toDate = endDate;
//                report.setStartDateTo(toDate);
//            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("endDateFrom", endDateFrom);
            request.setAttribute("endDateTo", endDateTo);
            ArrayList TripVmrDetails = new ArrayList();
            TripVmrDetails = reportBP.getTripVmrDetails(report, userId);
            System.out.println("TripVmrDetails:" + TripVmrDetails.size());
            request.setAttribute("TripVmrDetails", TripVmrDetails);
            request.setAttribute("TripVmrDetailsSize", TripVmrDetails.size());
            if (TripVmrDetails.size() == 0) {
                path = "content/report/BrattleFoods/tripVmrReport.jsp";
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");
            }

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleFCTripBudgetReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "Report  >>  View FC Wise Trip Performance Summary";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Report  >>  View FC Wise Trip Performance Summary ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            path = "content/report/BrattleFoods/fcWiseTripBudgetReport.jsp";
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleFCTripBudgetReportExcel(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "Report  >>  View FC Wise Trip Performance Summary";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Report  >>  View FC Wise Trip Performance Summary ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            String param = request.getParameter("param");
            System.out.println("the param value is" + param);
            if (param != null && !"".equals(param)) {
                if (param.equals("ExportExcel")) {
                    System.out.println("111 = ");
                    path = "content/report/BrattleFoods/fcWiseTripBudgetReportExcel.jsp";
                } else {
                    path = "content/report/BrattleFoods/fcWiseTripBudgetReport.jsp";
                }
            }
            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            ArrayList FcWiseTripBudgetDetails = new ArrayList();
            FcWiseTripBudgetDetails = reportBP.getFcWiseTripBudgetDetails(report, userId);
            System.out.println("FcWiseTripBudgetDetails:" + FcWiseTripBudgetDetails.size());
            request.setAttribute("FcWiseTripBudgetDetails", FcWiseTripBudgetDetails);
            request.setAttribute("FcWiseTripBudgetDetailsSize", FcWiseTripBudgetDetails.size());

            if (FcWiseTripBudgetDetails.size() == 0) {
                path = "content/report/BrattleFoods/fcWiseTripBudgetReport.jsp";
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");
            }
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAccountMgrPerformanceReportDetails(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "Report  >>  View  Account Mgr Performance Summary";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Report >> View  Account Mgr Performance Summary ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            path = "content/report/BrattleFoods/accountMgrPerformanceReport.jsp";
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAccountMgrPerformanceReportDetailsExcel(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "Report  >>  View  Account Mgr Performance Summary";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Report  >>  View  Account Mgr Performance Summary";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            String param = request.getParameter("param");
            System.out.println("the param value is" + param);
            if (param != null && !"".equals(param)) {
                if (param.equals("ExportExcel")) {
                    System.out.println("111 = ");
                    path = "content/report/BrattleFoods/accountMgrPerformanceReportExcel.jsp";
                } else {
                    path = "content/report/BrattleFoods/accountMgrPerformanceReport.jsp";
                }
            }

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            ArrayList AccountMgrPerformanceReportDetails = new ArrayList();
            AccountMgrPerformanceReportDetails = reportBP.getAccountMgrPerformanceReportDetails(report, userId);
            System.out.println("AccountMgrPerformanceReportDetails:" + AccountMgrPerformanceReportDetails.size());
            request.setAttribute("AccountMgrPerformanceReportDetails", AccountMgrPerformanceReportDetails);
            request.setAttribute("AccountMgrPerformanceReportDetailsSize", AccountMgrPerformanceReportDetails.size());

            if (AccountMgrPerformanceReportDetails.size() == 0) {
                path = "content/report/BrattleFoods/accountMgrPerformanceReport.jsp";
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");
            }

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleRNMExpenseReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "Report  >>  View  R&M Expense Report";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Report  >>  View  R&M Expense Report ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            path = "content/report/BrattleFoods/rnmExpenseReport.jsp";
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleRNMExpenseReportExcel(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "Report  >>   View  R&M Expense Report";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Report  >>   View  R&M Expense Report";
        ArrayList RnMExpenseDetails = new ArrayList();
        ArrayList RnMExpenseList1 = new ArrayList();
        ArrayList RnMExpenseList2 = new ArrayList();
        ArrayList RnMExpenseList3 = new ArrayList();
        String firstMonth = "";
        String secondMonth = "";
        String thirdMonth = "";
        String type = request.getParameter("type");
        report.setType(type);
        System.out.println("transportation = " + type);
        try {
            String param = request.getParameter("param");
            System.out.println("the param value is" + param);
            if (param != null && !"".equals(param)) {
                if (param.equals("ExportExcel")) {
                    System.out.println("111 = ");
                    path = "content/report/BrattleFoods/rnmExpenseReportExcel.jsp";
                } else {
                    path = "content/report/BrattleFoods/rnmExpenseReport.jsp";
                }
            }
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            int month = Integer.parseInt(temp[1]) - 1;
            String monthId = "" + month + "," + (month - 1) + "," + (month - 2) + "";
            System.out.println("The monthId value is :" + monthId);
            String temp1[] = monthId.split(",");
            System.out.println("temp1[0] :" + temp1[0]);
            System.out.println("temp1[1] :" + temp1[1]);
            System.out.println("temp1[2] :" + temp1[2]);
            String monthName = "Jan,Feb,March,April,May,June,July,Aug,Sep,Oct,Nov,Dec";
            String temp2[] = monthName.split(",");
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < temp2.length; j++) {
                    if (j == Integer.parseInt(temp1[i])) {
                        String fromDate = "";
                        String toDate = "";
                        if (Integer.parseInt(temp1[i]) == 1 || Integer.parseInt(temp1[i]) == 3 || Integer.parseInt(temp1[i]) == 5 || Integer.parseInt(temp1[i]) == 7 || Integer.parseInt(temp1[i]) == 8 || Integer.parseInt(temp1[i]) == 10 || Integer.parseInt(temp1[i]) == 12) {
                            fromDate = "01-" + temp1[i] + "-" + temp[2];
                            toDate = "31-" + temp1[i] + "-" + temp[2];
                            report.setFromDate(fromDate);
                            report.setToDate(toDate);
                        } else {
                            fromDate = "01-" + temp1[i] + "-" + temp[2];
                            toDate = "30-" + temp1[i] + "-" + temp[2];
                            report.setFromDate(fromDate);
                            report.setToDate(toDate);
                        }
                        RnMExpenseDetails = reportBP.getRNMExpenseReportDetails(report, userId);
                        Iterator itr1 = RnMExpenseDetails.iterator();
                        ReportTO repTO1 = new ReportTO();
                        while (itr1.hasNext()) {
                            ReportTO reportTo = new ReportTO();
                            repTO1 = (ReportTO) itr1.next();
                            reportTo.setVehicleId(repTO1.getVehicleId());
                            reportTo.setMonthName(temp2[j - 1]);
                            reportTo.setRegNo(repTO1.getRegNo());
                            if (Integer.parseInt(temp1[0]) == j) {
                                firstMonth = reportTo.getMonthName();
                                reportTo.setChasisAmount1(repTO1.getChasisAmount());
                                reportTo.setReferAmount1(repTO1.getReferAmount());
                                reportTo.setContainerAmount1(repTO1.getContainerAmount());

                                RnMExpenseList1.add(reportTo);
                                Iterator itr2 = RnMExpenseList1.iterator();
                                ReportTO repTO2 = new ReportTO();
                                while (itr2.hasNext()) {
                                    repTO2 = (ReportTO) itr2.next();
                                    System.out.println("repTO2.getVehicleId():" + repTO2.getVehicleId());
                                    System.out.println("repTO2.getChasisAmount1():" + repTO2.getChasisAmount1());
                                    System.out.println("repTO2.getReferAmount1():" + repTO2.getReferAmount1());
                                    System.out.println("repTO2.getContainerAmount1():" + repTO2.getContainerAmount1());
                                }
                            } else if (Integer.parseInt(temp1[1]) == j) {
                                secondMonth = reportTo.getMonthName();
                                reportTo.setChasisAmount2(repTO1.getChasisAmount());
                                reportTo.setReferAmount2(repTO1.getReferAmount());
                                reportTo.setContainerAmount2(repTO1.getContainerAmount());

                                RnMExpenseList2.add(reportTo);
                                Iterator itr2 = RnMExpenseList2.iterator();
                                ReportTO repTO3 = new ReportTO();
                                while (itr2.hasNext()) {
                                    repTO3 = (ReportTO) itr2.next();
                                    System.out.println("repTO3.getVehicleId():" + repTO3.getVehicleId());
                                    System.out.println("repTO3.getChasisAmount2():" + repTO3.getChasisAmount2());
                                    System.out.println("repTO3.getReferAmount2():" + repTO3.getReferAmount2());
                                    System.out.println("repTO3.getContainerAmount2():" + repTO3.getContainerAmount2());
                                }
                            } else if (Integer.parseInt(temp1[2]) == j) {
                                thirdMonth = reportTo.getMonthName();
                                reportTo.setChasisAmount3(repTO1.getChasisAmount());
                                reportTo.setReferAmount3(repTO1.getReferAmount());
                                reportTo.setContainerAmount3(repTO1.getContainerAmount());

                                RnMExpenseList3.add(reportTo);
                                Iterator itr3 = RnMExpenseList3.iterator();
                                ReportTO repTO4 = new ReportTO();
                                while (itr3.hasNext()) {
                                    repTO4 = (ReportTO) itr3.next();
                                    System.out.println("repTO4.getVehicleId():" + repTO4.getVehicleId());
                                    System.out.println("repTO4.getChasisAmount3():" + repTO4.getChasisAmount3());
                                    System.out.println("repTO4.getReferAmount3():" + repTO4.getReferAmount3());
                                    System.out.println("repTO4.getContainerAmount3():" + repTO4.getContainerAmount3());
                                }
                            }
                        }

                    }
                }

            }
            System.out.println("RnMExpenseList1:" + RnMExpenseList1.size());
            request.setAttribute("firstmonth", firstMonth);
            request.setAttribute("secondMonth", secondMonth);
            request.setAttribute("thirdMonth", thirdMonth);

            ArrayList vehicleList = new ArrayList();
            vehicleList = reportBP.getRNMVehicleList(type);
            request.setAttribute("vehicleList", vehicleList);

            System.out.println("RnMExpenseList1:" + RnMExpenseList1.size());
            System.out.println("RnMExpenseList2:" + RnMExpenseList2.size());
            System.out.println("RnMExpenseList3:" + RnMExpenseList3.size());
            System.out.println("RnMExpenseList1:" + RnMExpenseList1);
            request.setAttribute("RnMExpenseList1", RnMExpenseList1);
            request.setAttribute("RnMExpenseList2", RnMExpenseList2);
            request.setAttribute("RnMExpenseList3", RnMExpenseList3);
            request.setAttribute("RnMExpenseDetails", RnMExpenseDetails);
            request.setAttribute("RnMExpenseDetailsSize", RnMExpenseDetails.size());
            if (RnMExpenseList1.size() == 0) {
                path = "content/report/BrattleFoods/rnmExpenseReport.jsp";
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");
            }
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleTyerExpenseReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "Report  >>  View  Tyer Analysis Report";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Report  >>  View  Tyer Analysis Report ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            path = "content/report/BrattleFoods/tyreExpenseReport.jsp";
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleTyerExpenseReportExcel(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "Report  >>   View  Tyer Analysis Report";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Report  >>   View  Tyer Analysis Report";

        String firstMonth = "";
        String secondMonth = "";
        String thirdMonth = "";
        String type = request.getParameter("type");
        report.setType(type);
        System.out.println("transportation = " + type);

        try {
            String param = request.getParameter("param");
            System.out.println("the param value is" + param);
            if (param != null && !"".equals(param)) {
                if (param.equals("ExportExcel")) {
                    System.out.println("111 = ");
                    path = "content/report/BrattleFoods/tyreExpenseReportExcel.jsp";
                } else {
                    path = "content/report/BrattleFoods/tyreExpenseReport.jsp";
                }
            }
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            int month = Integer.parseInt(temp[1]) - 1;
            int monthTemp = Integer.parseInt(temp[1]);

            int month1 = 0;
            int month2 = 0;
            int month3 = 0;
            int year1 = 0;
            int year2 = 0;
            int year3 = 0;

            String firstmonth = "";
            String monthId = "" + month + "," + (month - 1) + "," + (month - 2) + "";
            System.out.println("The monthId value is :" + monthId);
            String temp1[] = monthId.split(",");
            String monthName = "Jan,Feb,March,April,May,June,July,Aug,Sep,Oct,Nov,Dec";
            String temp2[] = monthName.split(",");
            System.out.println("monthTemp value:" + monthTemp);
            if (monthTemp > 3) {
                month1 = monthTemp - 1;
                month2 = monthTemp - 2;
                month3 = monthTemp - 3;
                year1 = Integer.parseInt(temp[2]);
                year2 = year1;
                year3 = year1;
            } else {
                if (monthTemp == 1) {
                    month1 = 12;
                    month2 = month1 - 1;
                    month3 = month2 - 1;
                    year1 = Integer.parseInt(temp[2]) - 1;
                    year2 = year1;
                    year3 = year2;
                } else if (monthTemp == 2) {
                    month1 = monthTemp - 1;
                    month2 = 12;
                    month3 = month2 - 1;
                    year1 = Integer.parseInt(temp[2]);
                    year2 = year1 - 1;
                    year3 = year2;
                } else if (monthTemp == 3) {
                    month1 = monthTemp - 1;
                    month2 = month1 - 1;
                    month3 = 12;
                    year1 = Integer.parseInt(temp[2]);
                    year2 = year1;
                    year3 = year2 - 1;

                }
            }

            firstmonth = temp2[month1 - 1];
            secondMonth = temp2[month2 - 1];
            thirdMonth = temp2[month3 - 1];

            ArrayList vehicleList = new ArrayList();
            ArrayList vehicleListNew = new ArrayList();
            vehicleList = reportBP.getVehicleTyreList(type);
            request.setAttribute("vehicleList", vehicleList);

            ReportTO repTO = new ReportTO();
            ReportTO repTONew = null;

            Iterator itr = vehicleList.iterator();
            String vehicleId = "";
            boolean added = false;

            System.out.println("month1:" + month1);
            System.out.println("month2:" + month2);
            System.out.println("month3:" + month3);
            while (itr.hasNext()) {
                repTO = new ReportTO();
                repTO = (ReportTO) itr.next();
                System.out.println("vehicleId:" + vehicleId);
                System.out.println("repTO.getVehicleId():" + repTO.getVehicleId());
                System.out.println("repTO.getMonth():" + repTO.getMonth());
                System.out.println("repTO.getNewTyre():" + repTO.getNewTyre());
                System.out.println("repTO.getReTreadTyre():" + repTO.getReTreadTyre());
                if (vehicleId.equals(repTO.getVehicleId())) {
                    if (month1 == Integer.parseInt(repTO.getMonth())) {
                        System.out.println("4");
                        repTONew.setMonth1(repTO.getMonthName());
                        repTONew.setMonth1New(repTO.getNewTyre());
                        repTONew.setMonth1ReTread(repTO.getReTreadTyre());
                        repTONew.setNewTyreAmount1(repTO.getNewTyreAmount());
                        repTONew.setRetreadTyreAmount1(repTO.getRetreadTyreAmount());
                    }
                    if (month2 == Integer.parseInt(repTO.getMonth())) {
                        System.out.println("5");
                        repTONew.setMonth2(repTO.getMonthName());
                        repTONew.setMonth2New(repTO.getNewTyre());
                        repTONew.setMonth2ReTread(repTO.getReTreadTyre());
                        repTONew.setNewTyreAmount2(repTO.getNewTyreAmount());
                        repTONew.setRetreadTyreAmount2(repTO.getRetreadTyreAmount());
                    }
                    if (month3 == Integer.parseInt(repTO.getMonth())) {
                        System.out.println("6");
                        repTONew.setMonth3(repTO.getMonthName());
                        repTONew.setMonth3New(repTO.getNewTyre());
                        repTONew.setMonth3ReTread(repTO.getReTreadTyre());
                        repTONew.setNewTyreAmount3(repTO.getNewTyreAmount());
                        repTONew.setRetreadTyreAmount3(repTO.getRetreadTyreAmount());
                    }
                    System.out.println("1repTONew.getMonth1New():" + repTONew.getMonth1New());
                    System.out.println("1repTONew.getMonth1ReTread():" + repTONew.getMonth1ReTread());
                    System.out.println("1repTONew.getMonth2New():" + repTONew.getMonth2New());
                    System.out.println("1repTONew.getMonth2ReTread():" + repTONew.getMonth2ReTread());
                    System.out.println("1repTONew.getMonth3New():" + repTONew.getMonth3New());
                    System.out.println("1repTONew.getMonth3ReTread():" + repTONew.getMonth3ReTread());
                    added = false;

                } else {
                    if (repTONew != null) {
                        vehicleListNew.add(repTONew);
                        added = true;
                    }
                    repTONew = new ReportTO();
                    repTONew.setRegNo(repTO.getRegNo());
                    vehicleId = repTO.getVehicleId();

                    if (month1 == Integer.parseInt(repTO.getMonth())) {
                        System.out.println("1");
                        repTONew.setMonth1(repTO.getMonthName());
                        repTONew.setMonth1New(repTO.getNewTyre());
                        repTONew.setMonth1ReTread(repTO.getReTreadTyre());
                        repTONew.setNewTyreAmount1(repTO.getNewTyreAmount());
                        repTONew.setRetreadTyreAmount1(repTO.getRetreadTyreAmount());
                    }
                    if (month2 == Integer.parseInt(repTO.getMonth())) {
                        System.out.println("2");
                        repTONew.setMonth2(repTO.getMonthName());
                        repTONew.setMonth2New(repTO.getNewTyre());
                        repTONew.setMonth2ReTread(repTO.getReTreadTyre());
                        repTONew.setNewTyreAmount2(repTO.getNewTyreAmount());
                        repTONew.setRetreadTyreAmount2(repTO.getRetreadTyreAmount());
                    }
                    if (month3 == Integer.parseInt(repTO.getMonth())) {
                        System.out.println("3");
                        repTONew.setMonth3(repTO.getMonthName());
                        repTONew.setMonth3New(repTO.getNewTyre());
                        repTONew.setMonth3ReTread(repTO.getReTreadTyre());
                        repTONew.setNewTyreAmount3(repTO.getNewTyreAmount());
                        repTONew.setRetreadTyreAmount3(repTO.getRetreadTyreAmount());
                    }
                    System.out.println("repTONew.getMonth1New():" + repTONew.getMonth1New());
                    System.out.println("repTONew.getMonth1ReTread():" + repTONew.getMonth1ReTread());
                    System.out.println("repTONew.getMonth2New():" + repTONew.getMonth2New());
                    System.out.println("repTONew.getMonth2ReTread():" + repTONew.getMonth2ReTread());
                    System.out.println("repTONew.getMonth3New():" + repTONew.getMonth3New());
                    System.out.println("repTONew.getMonth3ReTread():" + repTONew.getMonth3ReTread());
                    added = false;
                }
            }
            if (!added) {
                vehicleListNew.add(repTONew);
            }
            request.setAttribute("vehicleList", vehicleListNew);
            request.setAttribute("firstmonth", firstmonth);
            request.setAttribute("secondMonth", secondMonth);
            request.setAttribute("thirdMonth", thirdMonth);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleRNMReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "Report  >>  View  Repair & Maintainance Report";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Report  >>  View  Repair & Maintainance Report ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            path = "content/report/BrattleFoods/rnmReport.jsp";
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleRNMReportExcel(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "Report  >>   View  Repair & Maintainance Report";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Report  >>   View  Repair & Maintainance Report";

        String firstMonth = "";
        String secondMonth = "";
        String thirdMonth = "";
        String type = request.getParameter("type");
        report.setType(type);
        System.out.println("transportation = " + type);

        try {
            String param = request.getParameter("param");
            System.out.println("the param value is" + param);
            if (param != null && !"".equals(param)) {
                if (param.equals("ExportExcel")) {
                    System.out.println("111 = ");
                    path = "content/report/BrattleFoods/rnmReportExcel.jsp";
                } else {
                    path = "content/report/BrattleFoods/rnmReport.jsp";
                }
            }
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            int month = Integer.parseInt(temp[1]) - 1;
            int monthTemp = Integer.parseInt(temp[1]);

            int month1 = 0;
            int month2 = 0;
            int month3 = 0;
            int year1 = 0;
            int year2 = 0;
            int year3 = 0;

            String firstmonth = "";
            String monthId = "" + month + "," + (month - 1) + "," + (month - 2) + "";
            System.out.println("The monthId value is :" + monthId);
            String temp1[] = monthId.split(",");
            String monthName = "Jan,Feb,March,April,May,June,July,Aug,Sep,Oct,Nov,Dec";
            String temp2[] = monthName.split(",");
            System.out.println("monthTemp value:" + monthTemp);
            if (monthTemp > 3) {
                month1 = monthTemp - 1;
                month2 = monthTemp - 2;
                month3 = monthTemp - 3;
                year1 = Integer.parseInt(temp[2]);
                year2 = year1;
                year3 = year1;
            } else {
                if (monthTemp == 1) {
                    month1 = 12;
                    month2 = month1 - 1;
                    month3 = month2 - 1;
                    year1 = Integer.parseInt(temp[2]) - 1;
                    year2 = year1;
                    year3 = year2;
                } else if (monthTemp == 2) {
                    month1 = monthTemp - 1;
                    month2 = 12;
                    month3 = month2 - 1;
                    year1 = Integer.parseInt(temp[2]);
                    year2 = year1 - 1;
                    year3 = year2;
                } else if (monthTemp == 3) {
                    month1 = monthTemp - 1;
                    month2 = month1 - 1;
                    month3 = 12;
                    year1 = Integer.parseInt(temp[2]);
                    year2 = year1;
                    year3 = year2 - 1;

                }
            }

            firstmonth = temp2[month1 - 1];
            secondMonth = temp2[month2 - 1];
            thirdMonth = temp2[month3 - 1];

            ArrayList rnmList = new ArrayList();
            ArrayList rnmListNew = new ArrayList();
            rnmList = reportBP.getRNMReportList(type);
            request.setAttribute("rnmList", rnmList);

            ReportTO repTO = new ReportTO();
            ReportTO repTONew = null;

            Iterator itr = rnmList.iterator();
            String vehicleId = "";
            boolean added = false;

            System.out.println("month1:" + month1);
            System.out.println("month2:" + month2);
            System.out.println("month3:" + month3);
            while (itr.hasNext()) {
                repTO = new ReportTO();
                repTO = (ReportTO) itr.next();
                System.out.println("vehicleId:" + vehicleId);
                System.out.println("repTO.getVehicleId():" + repTO.getVehicleId());
                System.out.println("repTO.getMonth():" + repTO.getMonth());
                System.out.println("repTO.getBreakDownReeferWorks():" + repTO.getBreakDownReeferWorks());
                System.out.println("repTO.getBreakDownVehicleWorks():" + repTO.getBreakDownVehicleWorks());
                System.out.println("repTO.getRegularReeferService():" + repTO.getRegularReeferService());
                System.out.println("repTO.getRegularVehicleService():" + repTO.getRegularVehicleService());
                System.out.println("repTO.getAsset():" + repTO.getAsset());
                System.out.println("repTO.getAccidental():" + repTO.getAccidental());
                if (vehicleId.equals(repTO.getVehicleId())) {
                    if (month1 == Integer.parseInt(repTO.getMonth())) {
                        System.out.println("4");
                        repTONew.setMonth1(repTO.getMonthName());
                        repTONew.setMonth1Accidental(repTO.getAccidental());
                        repTONew.setMonth1Asset(repTO.getAsset());
                        repTONew.setMonth1BreakDownReeferWorks(repTO.getBreakDownReeferWorks());
                        repTONew.setMonth1BreakDownVehicleWorks(repTO.getBreakDownVehicleWorks());
                        repTONew.setMonth1RegularReeferService(repTO.getRegularReeferService());
                        repTONew.setMonth1RegularVehicleService(repTO.getRegularVehicleService());
                    }
                    if (month2 == Integer.parseInt(repTO.getMonth())) {
                        System.out.println("5");
                        repTONew.setMonth2(repTO.getMonthName());
                        repTONew.setMonth2Accidental(repTO.getAccidental());
                        repTONew.setMonth2Asset(repTO.getAsset());
                        repTONew.setMonth2BreakDownReeferWorks(repTO.getBreakDownReeferWorks());
                        repTONew.setMonth2BreakDownVehicleWorks(repTO.getBreakDownVehicleWorks());
                        repTONew.setMonth2RegularReeferService(repTO.getRegularReeferService());
                        repTONew.setMonth2RegularVehicleService(repTO.getRegularVehicleService());
                    }
                    if (month3 == Integer.parseInt(repTO.getMonth())) {
                        System.out.println("6");
                        repTONew.setMonth3(repTO.getMonthName());
                        repTONew.setMonth3Accidental(repTO.getAccidental());
                        repTONew.setMonth3Asset(repTO.getAsset());
                        repTONew.setMonth3BreakDownReeferWorks(repTO.getBreakDownReeferWorks());
                        repTONew.setMonth3BreakDownVehicleWorks(repTO.getBreakDownVehicleWorks());
                        repTONew.setMonth3RegularReeferService(repTO.getRegularReeferService());
                        repTONew.setMonth3RegularVehicleService(repTO.getRegularVehicleService());
                    }
//                    System.out.println("1repTONew.getMonth1New():" + repTONew.getMonth1New());
//                    System.out.println("1repTONew.getMonth1ReTread():" + repTONew.getMonth1ReTread());
//                    System.out.println("1repTONew.getMonth2New():" + repTONew.getMonth2New());
//                    System.out.println("1repTONew.getMonth2ReTread():" + repTONew.getMonth2ReTread());
//                    System.out.println("1repTONew.getMonth3New():" + repTONew.getMonth3New());
//                    System.out.println("1repTONew.getMonth3ReTread():" + repTONew.getMonth3ReTread());
                    added = false;

                } else {
                    if (repTONew != null) {
                        rnmListNew.add(repTONew);
                        added = true;
                    }
                    repTONew = new ReportTO();
                    repTONew.setRegNo(repTO.getRegNo());
                    vehicleId = repTO.getVehicleId();

                    if (month1 == Integer.parseInt(repTO.getMonth())) {
                        System.out.println("1");
                        repTONew.setMonth1(repTO.getMonthName());
                        repTONew.setMonth1Accidental(repTO.getAccidental());
                        repTONew.setMonth1Asset(repTO.getAsset());
                        repTONew.setMonth1BreakDownReeferWorks(repTO.getBreakDownReeferWorks());
                        repTONew.setMonth1BreakDownVehicleWorks(repTO.getBreakDownVehicleWorks());
                        repTONew.setMonth1RegularReeferService(repTO.getRegularReeferService());
                        repTONew.setMonth1RegularVehicleService(repTO.getRegularVehicleService());
                    }
                    if (month2 == Integer.parseInt(repTO.getMonth())) {
                        System.out.println("2");
                        repTONew.setMonth2(repTO.getMonthName());
                        repTONew.setMonth2Accidental(repTO.getAccidental());
                        repTONew.setMonth2Asset(repTO.getAsset());
                        repTONew.setMonth2BreakDownReeferWorks(repTO.getBreakDownReeferWorks());
                        repTONew.setMonth2BreakDownVehicleWorks(repTO.getBreakDownVehicleWorks());
                        repTONew.setMonth2RegularReeferService(repTO.getRegularReeferService());
                        repTONew.setMonth2RegularVehicleService(repTO.getRegularVehicleService());
                    }
                    if (month3 == Integer.parseInt(repTO.getMonth())) {
                        System.out.println("3");
                        repTONew.setMonth3(repTO.getMonthName());
                        repTONew.setMonth3Accidental(repTO.getAccidental());
                        repTONew.setMonth3Asset(repTO.getAsset());
                        repTONew.setMonth3BreakDownReeferWorks(repTO.getBreakDownReeferWorks());
                        repTONew.setMonth3BreakDownVehicleWorks(repTO.getBreakDownVehicleWorks());
                        repTONew.setMonth3RegularReeferService(repTO.getRegularReeferService());
                        repTONew.setMonth3RegularVehicleService(repTO.getRegularVehicleService());
                    }
//                    System.out.println("repTONew.getMonth1New():" + repTONew.getMonth1New());
//                    System.out.println("repTONew.getMonth1ReTread():" + repTONew.getMonth1ReTread());
//                    System.out.println("repTONew.getMonth2New():" + repTONew.getMonth2New());
//                    System.out.println("repTONew.getMonth2ReTread():" + repTONew.getMonth2ReTread());
//                    System.out.println("repTONew.getMonth3New():" + repTONew.getMonth3New());
//                    System.out.println("repTONew.getMonth3ReTread():" + repTONew.getMonth3ReTread());
                    added = false;
                }
            }
            if (!added) {
                rnmListNew.add(repTONew);
            }
            request.setAttribute("rnmList", rnmListNew);
            request.setAttribute("firstmonth", firstmonth);
            request.setAttribute("secondMonth", secondMonth);
            request.setAttribute("thirdMonth", thirdMonth);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVehileUtilize(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "Report  >>  View  vehicle Report";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Report  >>  View  vehicle Report ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            path = "content/report/BrattleFoods/vehicleUtalizeReport.jsp";
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVehileUtilizeExcel(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "Report  >>   View  Tyer Analysis Report";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Report  >>   View  Vehicle Utilize Report";
        String utilizeMonth = "";
        String utilizeYear = "";

        try {
            String param = request.getParameter("param");
            utilizeMonth = request.getParameter("month");
            utilizeYear = request.getParameter("year");
            request.setAttribute("month", utilizeMonth);
            request.setAttribute("year", utilizeYear);
            System.out.println("the param value is" + param);
            if (param != null && !"".equals(param)) {
                if (param.equals("ExportExcel")) {
                    System.out.println("111 = ");
                    path = "content/report/BrattleFoods/vehicleUtalizeReportExcel.jsp";
                } else {
                    path = "content/report/BrattleFoods/vehicleUtalizeReport.jsp";
                }
            }

            ArrayList vehicleUtilizeList = new ArrayList();
            vehicleUtilizeList = reportBP.getVehicleUtilizeList(utilizeMonth, utilizeYear);
            request.setAttribute("vehicleUtilizeList", vehicleUtilizeList);
            System.out.println("vehicleUtilizeList Size " + vehicleUtilizeList.size());

            // path = "content/report/BrattleFoods/vehicleUtalizeReport.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleTyreAgeingeReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "Report  >>  View  Tyer Ageing Report";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Report  >>  View  Tyer Ageing Report";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            path = "content/report/BrattleFoods/tyreAgeingReport.jsp";
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleTyreAgeingeReportExcel(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "Report  >>   View  Tyer Ageing Report";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Report  >>   View  Tyer Ageing Report";
        String utilizeMonth = "";

        try {
            String param = request.getParameter("param");
            utilizeMonth = request.getParameter("month");
            request.setAttribute("month", utilizeMonth);
            System.out.println("the param value is" + param);
            if (param != null && !"".equals(param)) {
                if (param.equals("ExportExcel")) {
                    System.out.println("111 = ");
                    path = "content/report/BrattleFoods/tyreAgeingReportExcel.jsp";
                } else {
                    path = "content/report/BrattleFoods/tyreAgeingReport.jsp";
                }
            }

            ArrayList tyreAgeingList = new ArrayList();
            tyreAgeingList = reportBP.getTyreAgeingList(report, userId);
            request.setAttribute("tyreAgeingList", tyreAgeingList);
            System.out.println("tyreAgeingList Size " + tyreAgeingList.size());

            // path = "content/report/BrattleFoods/vehicleUtalizeReport.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleBrokerCustomerReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "Report  >>  View  Broker Vustomer Report";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Report  >>  View  Broker Customer Report ";
        String tripId = "";
        String fromDate = "";
        String toDate = "";
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            path = "content/report/BrattleFoods/brokerCustomerReport.jsp";
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleBrokerCustomerReportExcel(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "Report  >>   View  Broker Customer Report";
        ReportTO report = new ReportTO();
        reportCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Report  >>   View  Broker Customer Report";
        String utilizeMonth = "";
        String utilizeYear = "";
        String fromDate = "";
        String toDate = "";

        try {
            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            String param = request.getParameter("param");
            utilizeMonth = request.getParameter("month");
            utilizeYear = request.getParameter("year");
            request.setAttribute("month", utilizeMonth);
            request.setAttribute("year", utilizeYear);
            System.out.println("the param value is" + param);
            if (param != null && !"".equals(param)) {
                if (param.equals("ExportExcel")) {
                    System.out.println("111 = ");
                    path = "content/report/BrattleFoods/brokerCustomerReporExcel.jsp";
                } else {
                    path = "content/report/BrattleFoods/brokerCustomerReport.jsp";
                }
            }

            ArrayList brokerCustomerDetails = new ArrayList();
            brokerCustomerDetails = reportBP.getBrokerCustomerDetails(report, userId);
            request.setAttribute("brokerCustomerDetails", brokerCustomerDetails);
            System.out.println("brokerCustomerDetails Size " + brokerCustomerDetails.size());

            // path = "content/report/BrattleFoods/vehicleUtalizeReport.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView dashboardMaster(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList accountReceivableList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Dashboard Report";
        request.setAttribute("menuPath", menuPath);
        try {
            System.out.println("am here......");
            path = "content/report/dashboardMaster.jsp";

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to dashboardReport Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView dashboardOperation(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList accountReceivableList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Dashboard Report";
        request.setAttribute("menuPath", menuPath);
        try {
            System.out.println("am here......");
            path = "content/report/dashboardOperation.jsp";

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to dashboardReport Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView dashboardWorkshop(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList accountReceivableList = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Dashboard Report";
        request.setAttribute("menuPath", menuPath);
        try {

            path = "content/report/dashboardWorkshop.jsp";

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to dashboardReport Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getDashBoardTruckNos(HttpServletRequest request, HttpServletResponse response, ReportCommand command) throws IOException {
        HttpSession session = request.getSession();
        ArrayList dashboardOpstrucknos = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
//        JsonTO jsonTO = new JsonTO();
//        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
//            String dashboardopstrukCount = "";
            response.setContentType("text/html");
//            dashboardopstrukCount = request.getParameter("dashboardopstrukCount");
//            report.setDashboardopstrukCount(dashboardopstrukCount);
//            System.out.println("yyn"+dashboardopstrukCount);
            dashboardOpstrucknos = reportBP.getDashboardopsTruckNos(report);
            System.out.println("benjamin111" + dashboardOpstrucknos);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = dashboardOpstrucknos.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                report = (ReportTO) itr.next();
                jsonObject.put("Name", report.getName());
                jsonObject.put("Count", report.getCount());
//                jsonObject.put("Count", ReportTO.getTotalVehicle());
                jsonArray.put(jsonObject);
            }
            pw.print(jsonArray);
            System.out.println("jsonArraytruckno = " + jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getDashBoardOperationNos(HttpServletRequest request, HttpServletResponse response, ReportCommand command) throws IOException {
        HttpSession session = request.getSession();
        ArrayList dashboardOpstrucknos = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
//        JsonTO jsonTO = new JsonTO();
//        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
//            String dashboardopstrukCount = "";
            response.setContentType("text/html");
//            dashboardopstrukCount = request.getParameter("dashboardopstrukCount");
//            report.setDashboardopstrukCount(dashboardopstrukCount);
//            System.out.println("yyn"+dashboardopstrukCount);
            dashboardOpstrucknos = reportBP.getDashBoardOperationNos(report);
            System.out.println("benjamin111" + dashboardOpstrucknos);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = dashboardOpstrucknos.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                report = (ReportTO) itr.next();
                jsonObject.put("Name", report.getName());
                jsonObject.put("Count", report.getCount());
//                jsonObject.put("Count", ReportTO.getTotalVehicle());
                jsonArray.put(jsonObject);
            }
            pw.print(jsonArray);
            System.out.println("jsonArraytruckno = " + jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getOverAllTripNos(HttpServletRequest request, HttpServletResponse response, ReportCommand command) throws IOException {
        HttpSession session = request.getSession();
        ArrayList dashboardOpstrucknos = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
//        JsonTO jsonTO = new JsonTO();
//        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
//            String dashboardopstrukCount = "";
            response.setContentType("text/html");
//            dashboardopstrukCount = request.getParameter("dashboardopstrukCount");
//            report.setDashboardopstrukCount(dashboardopstrukCount);
//            System.out.println("yyn"+dashboardopstrukCount);
            dashboardOpstrucknos = reportBP.getOverAllTripNos(report);
            System.out.println(" " + dashboardOpstrucknos);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = dashboardOpstrucknos.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                report = (ReportTO) itr.next();
                jsonObject.put("Name", report.getName());
                jsonObject.put("Count", report.getCount());
//                jsonObject.put("Count", ReportTO.getTotalVehicle());
                jsonArray.put(jsonObject);
            }
            pw.print(jsonArray);
            System.out.println("jsonArraytruckno = " + jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getStatusWiseTripNos(HttpServletRequest request, HttpServletResponse response, ReportCommand command) throws IOException {
        HttpSession session = request.getSession();
        ArrayList getStatusWiseTripNos = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
//        JsonTO jsonTO = new JsonTO();
//        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
//            String dashboardopstrukCount = "";
            response.setContentType("text/html");
//            dashboardopstrukCount = request.getParameter("dashboardopstrukCount");
//            report.setDashboardopstrukCount(dashboardopstrukCount);
//            System.out.println("yyn"+dashboardopstrukCount);
            getStatusWiseTripNos = reportBP.getStatusWiseTripNos(report);
            System.out.println("benjamin111" + getStatusWiseTripNos);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = getStatusWiseTripNos.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                report = (ReportTO) itr.next();
                jsonObject.put("Name", report.getName());
                jsonObject.put("Count", report.getCount());
//                jsonObject.put("Count", ReportTO.getTotalVehicle());
                jsonArray.put(jsonObject);
            }
            pw.print(jsonArray);
            System.out.println("jsonArraytruckno = " + jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getRouteWiseTripNos(HttpServletRequest request, HttpServletResponse response, ReportCommand command) throws IOException {
        HttpSession session = request.getSession();
        ArrayList getRouteWiseTripNos = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
//        JsonTO jsonTO = new JsonTO();
//        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
//            String dashboardopstrukCount = "";
            response.setContentType("text/html");
//            dashboardopstrukCount = request.getParameter("dashboardopstrukCount");
//            report.setDashboardopstrukCount(dashboardopstrukCount);
//            System.out.println("yyn"+dashboardopstrukCount);
            getRouteWiseTripNos = reportBP.getRouteWiseTripNos(report);
            System.out.println("benjamin111" + getRouteWiseTripNos);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = getRouteWiseTripNos.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                report = (ReportTO) itr.next();
                jsonObject.put("Name", report.getName());
                jsonObject.put("Count", report.getCount());
//                jsonObject.put("Count", ReportTO.getTotalVehicle());
                jsonArray.put(jsonObject);
            }
            pw.print(jsonArray);
            System.out.println("jsonArraytruckno = " + jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getDashBoardWorkShop(HttpServletRequest request, HttpServletResponse response, ReportCommand command) throws IOException {
        HttpSession session = request.getSession();
        ArrayList getDashboardWorkShop = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
//        JsonTO jsonTO = new JsonTO();
//        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
//            String dashboardopstrukCount = "";
            response.setContentType("text/html");
//            dashboardopstrukCount = request.getParameter("dashboardopstrukCount");
//            report.setDashboardopstrukCount(dashboardopstrukCount);
//            System.out.println("yyn"+dashboardopstrukCount);
            getDashboardWorkShop = reportBP.getDashboardWorkShop(report);
            System.out.println("benjamin1111" + getDashboardWorkShop);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = getDashboardWorkShop.iterator();

            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                report = (ReportTO) itr.next();
                jsonObject.put("Name", report.getName());
                jsonObject.put("Count", report.getCount());
//                jsonObject.put("Count", ReportTO.getTotalVehicle());
                jsonArray.put(jsonObject);
            }
            pw.print(jsonArray);
            System.out.println("jsonArrayWorkshopnoo = " + jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void vehicleMake(HttpServletRequest request, HttpServletResponse response, ReportCommand command) throws IOException {
        HttpSession session = request.getSession();
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String driverName = "";
            response.setContentType("text/html");
//            driverName = request.getParameter("driverName");
//            operationTO.setDriverName(driverName);
            userDetails = reportBP.getVehicleUT();
            System.out.println("benjamin" + userDetails);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = userDetails.iterator();
            JSONObject jsonTotObject = new JSONObject();
            float percentage = 0.00F;
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                reportTO = (ReportTO) itr.next();
                jsonObject.put("Make", reportTO.getVehicleMake());
                jsonObject.put("Count", reportTO.getTotalVehicle());
                System.out.println("operationTO.getGrandTotal():" + reportTO.getGrandTotal());
                percentage = (float) ((Float.parseFloat(reportTO.getTotalVehicle()) * 100) / Float.parseFloat(reportTO.getGrandTotal()));
                System.out.println("Percent:" + percentage);
                jsonObject.put("Percent", percentage);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);
            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void dbTrailerMake(HttpServletRequest request, HttpServletResponse response, ReportCommand command) throws IOException {
        HttpSession session = request.getSession();
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String driverName = "";
            response.setContentType("text/html");
//            driverName = request.getParameter("driverName");
//            operationTO.setDriverName(driverName);
            userDetails = reportBP.dbTrailerMake();
            System.out.println("benjamin" + userDetails);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = userDetails.iterator();
            JSONObject jsonTotObject = new JSONObject();
            float percentage = 0.00F;
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                reportTO = (ReportTO) itr.next();
                jsonObject.put("Make", reportTO.getVehicleMake());
                jsonObject.put("Count", reportTO.getTotalVehicle());
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);
            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void dbTruckType(HttpServletRequest request, HttpServletResponse response, ReportCommand command) throws IOException {
        HttpSession session = request.getSession();
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        ArrayList truckType = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String driverName = "";
            response.setContentType("text/html");
            truckType = reportBP.dbTruckType();
            JSONArray jsonArray = new JSONArray();
            Iterator itr = truckType.iterator();

            float percentage = 0.00F;
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                reportTO = (ReportTO) itr.next();
                jsonObject.put("Make", reportTO.getVehicleMake());
                jsonObject.put("Count", reportTO.getTotalVehicle());
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);
            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void dbTrailerType(HttpServletRequest request, HttpServletResponse response, ReportCommand command) throws IOException {
        HttpSession session = request.getSession();
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
        ArrayList trailerType = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String driverName = "";
            response.setContentType("text/html");
            trailerType = reportBP.dbTrailerType();
            JSONArray jsonArray = new JSONArray();
            Iterator itr = trailerType.iterator();

            float percentage = 0.00F;
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                reportTO = (ReportTO) itr.next();
                jsonObject.put("Make", reportTO.getVehicleMake());
                jsonObject.put("Count", reportTO.getTotalVehicle());
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);
            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getTrailerJobCardMTD(HttpServletRequest request, HttpServletResponse response, ReportCommand command) throws IOException {
        HttpSession session = request.getSession();
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String driverName = "";
            response.setContentType("text/html");
//            driverName = request.getParameter("driverName");
//            operationTO.setDriverName(driverName);
            userDetails = reportBP.getTrailerJobCardMTD();
            System.out.println("benjamin" + userDetails);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = userDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                reportTO = (ReportTO) itr.next();
                jsonObject.put("Make", reportTO.getVehicleMake());
                jsonObject.put("Count", reportTO.getTotalVehicle());
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);
            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getTruckJobCardMTD(HttpServletRequest request, HttpServletResponse response, ReportCommand command) throws IOException {
        HttpSession session = request.getSession();
        reportCommand = command;
        ReportTO reportTO = new ReportTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String driverName = "";
            response.setContentType("text/html");
//            driverName = request.getParameter("driverName");
//            operationTO.setDriverName(driverName);
            userDetails = reportBP.getTruckJobCardMTD();
            System.out.println("benjamin" + userDetails);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = userDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                reportTO = (ReportTO) itr.next();
                jsonObject.put("Make", reportTO.getVehicleMake());
                jsonObject.put("Count", reportTO.getTotalVehicle());
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);
            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView handleNODreport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        OperationTO operationTO = new OperationTO();
        ReportTO reportTO = new ReportTO();
        reportCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> NOD Report";
        request.setAttribute("menuPath", menuPath);
        try {
            System.out.println("NOD Report......");
            ArrayList nodList = new ArrayList();
            ArrayList nodLists = new ArrayList();

            nodList = reportBP.nodReport(reportTO);
            request.setAttribute("nodList", nodList);
            System.out.println("nodList... " + nodList.size());

            nodLists = reportBP.nodReports(reportTO);
            request.setAttribute("nodLists", nodLists);
            System.out.println("nodLists111... " + nodLists.size());

            if (nodList.size() > 0) {
                System.out.println("IF i'm here....");
                request.setAttribute("nodList", nodList);
                if (nodLists.size() > 0) {

                    request.setAttribute("nodLists", nodLists);

                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "No Record Found!");
                }
            } else {
                System.out.println("ELSE i'm here....");
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "No Record Found!");
            }
            path = "content/report/nodReport.jsp";

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to nodReport Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView nodReportLevel2(HttpServletRequest request, HttpServletResponse reponse, ReportCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        reportCommand = command;
        String path = "";
        ReportTO reportTO = new ReportTO();
        String menuPath = " Report >> NOD Report";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        if (session.getAttribute("message") != null && session.getAttribute("message") != "") {
            session.removeAttribute("message");
        }
        int userId = (Integer) session.getAttribute("userId");
        reportTO.setUserId(userId);
        ArrayList nodLevel2List = new ArrayList();
        try {

            String statusId = request.getParameter("statusId");
            String statusName = request.getParameter("statusName");
            String day = request.getParameter("day");
            reportTO.setStatusId(statusId);
            reportTO.setNoOfDays(day);

            nodLevel2List = reportBP.nodReportLevel2(reportTO);
            request.setAttribute("nodLevel2List", nodLevel2List);
            request.setAttribute("statusName", statusName);
            request.setAttribute("day", day);
            request.setAttribute("statusId", statusId);
            System.out.println("nodLevel2List" + nodLevel2List.size());

            if (nodLevel2List.size() > 0) {
                System.out.println("NOD List level2 size loopp::::");
                request.setAttribute("nodListLevel2", nodLevel2List);

            } else {
                System.out.println("else loop in size");
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "No Record Found!");
            }

            path = "content/report/nodReportLevel2.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed nodReportLevel2 --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView nodReportLevel3(HttpServletRequest request, HttpServletResponse reponse, ReportCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        reportCommand = command;
        String path = "";
        ReportTO reportTO = new ReportTO();
        String menuPath = " Report >> NOD Report";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        if (session.getAttribute("message") != null && session.getAttribute("message") != "") {
            session.removeAttribute("message");
        }
        int userId = (Integer) session.getAttribute("userId");
        reportTO.setUserId(userId);
        ArrayList nodLevel3List = new ArrayList();
        try {
            String statusId = request.getParameter("statusId");
            String statusName = request.getParameter("statusName");
            String day = request.getParameter("day");
            reportTO.setStatusId(statusId);
            reportTO.setNoOfDays(day);

            nodLevel3List = reportBP.nodLevel3List(reportTO);
            request.setAttribute("nodLevel3List", nodLevel3List);
            request.setAttribute("statusName", statusName);
            request.setAttribute("day", day);
            request.setAttribute("statusId", statusId);
            System.out.println("nodLevel3List" + nodLevel3List.size());

            if (nodLevel3List.size() > 0) {
                System.out.println("NOD List level2 size loopp::::");
                request.setAttribute("nodListLevel2", nodLevel3List);

            } else {
                System.out.println("else loop in size");
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "No Record Found!");
            }
            path = "content/report/nodReportLevel3.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed nodReportLevel2 --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVendorPerformance(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        HttpSession session = request.getSession();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Vendor Performance Report";
        request.setAttribute("menuPath", menuPath);

        try {

            path = "content/report/BrattleFoods/vendorPerformanceReport.jsp";

            if (reportCommand.getFromDate() != null && reportCommand.getFromDate() != "") {
                report.setFromDate(reportCommand.getFromDate());
                fromDate = reportCommand.getFromDate();
            }
            if (reportCommand.getToDate() != null && reportCommand.getToDate() != "") {
                report.setToDate(reportCommand.getToDate());
                toDate = reportCommand.getToDate();
            }
            if (reportCommand.getVehicleTypeId() != null && reportCommand.getVehicleTypeId() != "") {
                report.setVehicleTypeId(reportCommand.getVehicleTypeId());
            }
            if (reportCommand.getVendorId() != null && reportCommand.getVendorId() != "") {
                report.setVendorId(reportCommand.getVendorId());
            }

            String expenseType = request.getParameter("expenseType");
            String vendorId = request.getParameter("vendorId");

            if (vendorId == null) {
                vendorId = "";
            }

            if (expenseType == null) {
                expenseType = "";
            }

            report.setExpenseType(expenseType);
            request.setAttribute("expenseType", expenseType);

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                report.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                report.setToDate(toDate);
            }

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            ArrayList VendorList = new ArrayList();
            VendorList = reportBP.getVendorList();
            request.setAttribute("VendorList", VendorList);

            ArrayList VehicleTypeList = new ArrayList();
            VehicleTypeList = reportBP.vehicleType();
            request.setAttribute("VehicleTypeList", VehicleTypeList);

            request.setAttribute("vendorId", vendorId);
            request.setAttribute("vehicleTypeId", report.getVehicleTypeId());

            ArrayList VendorPerformanceList = new ArrayList();
            ArrayList VendorVehicleList = new ArrayList();

            if (expenseType.equalsIgnoreCase("VEH")) {
                VendorVehicleList = reportBP.getVendorVehicleList(report, userId);
                request.setAttribute("VendorVehicleList", VendorVehicleList);
                System.out.println("VendorVehicleList.size() = " + VendorVehicleList.size());
            } else {
                VendorPerformanceList = reportBP.getVendorPerformanceList(report, userId);
                request.setAttribute("VendorPerformanceList", VendorPerformanceList);
                System.out.println("VendorPerformanceList.size() = " + VendorPerformanceList.size());
            }

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleVendorPerformance Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getOverAllProfitability(HttpServletRequest request, HttpServletResponse response, ReportCommand command) throws IOException {
        HttpSession session = request.getSession();
        ArrayList dashboardOpstrucknos = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
        String path = "";
        PrintWriter pw = response.getWriter();
        try {

            response.setContentType("text/html");
            dashboardOpstrucknos = reportBP.getOverAllProfitability(report);
            System.out.println("customer profit : " + dashboardOpstrucknos);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = dashboardOpstrucknos.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                report = (ReportTO) itr.next();
                jsonObject.put("Name", report.getName());
                jsonObject.put("Count", report.getCount());
                jsonArray.put(jsonObject);
            }
            pw.print(jsonArray);
            System.out.println("jsonArraytruckno = " + jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView handleDSRReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList accountReceivableList = new ArrayList();
        ReportTO reportTO = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Dashboard Report";
        request.setAttribute("menuPath", menuPath);
        try {
            if (reportCommand.getFromDate() != null && !"".equals(reportCommand.getFromDate())) {
                reportTO.setFromDate(reportCommand.getFromDate());
                request.setAttribute("fromDate", reportCommand.getFromDate());
            }
            if (reportCommand.getToDate() != null && !"".equals(reportCommand.getToDate())) {
                reportTO.setToDate(reportCommand.getToDate());
                request.setAttribute("toDate", reportCommand.getToDate());
            }

            if (reportCommand.getCustomerId() != null && !"".equals(reportCommand.getCustomerId())) {
                reportTO.setCustomerId(reportCommand.getCustomerId());
                request.setAttribute("customerId", reportCommand.getCustomerId());
            }
            if (reportCommand.getVehicleTypeId() != null && !"".equals(reportCommand.getVehicleTypeId())) {
                reportTO.setVehicleTypeId(reportCommand.getVehicleTypeId());
                request.setAttribute("vehicleTypeId", reportCommand.getVehicleTypeId());
            }

            ArrayList dsrReport = new ArrayList();
            dsrReport = reportBP.dsrReport(reportTO);
            request.setAttribute("dsrReport", dsrReport);

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);
            System.out.println("customerList Size Is :::" + customerList.size());

            ArrayList TypeList = new ArrayList();
            TypeList = vehicleBP.processGetTypeList();
            request.setAttribute("TypeList", TypeList);

            System.out.println("am here......");
            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                //System.out.println("111 = ");
                path = "content/report/dsrReportExcel.jsp";

            } else {
                // System.out.println("222 = ");
                path = "content/report/dsrReport.jsp";

            }

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to dashboardReport Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVendorOutstandingReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList accountReceivableList = new ArrayList();
        ReportTO reportTO = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Dashboard Report";
        request.setAttribute("menuPath", menuPath);
        String vendorType = ThrottleConstants.leasingVendorTypeId;
        String param = request.getParameter("param");
        System.out.println("param  " + param);
        try {
            if (reportCommand.getFromDate() != null && !"".equals(reportCommand.getFromDate())) {
                reportTO.setFromDate(reportCommand.getFromDate());
                request.setAttribute("fromDate", reportCommand.getFromDate());
            }
            if (reportCommand.getToDate() != null && !"".equals(reportCommand.getToDate())) {
                reportTO.setToDate(reportCommand.getToDate());
                request.setAttribute("toDate", reportCommand.getToDate());
            }
            if (reportCommand.getVendorName() != null && !"".equals(reportCommand.getVendorName())) {
                reportTO.setVendorName(reportCommand.getVendorName());
                request.setAttribute("vendorName", reportCommand.getVendorName());
            }
            if (reportCommand.getInvoiceNo() != null && !"".equals(reportCommand.getVendorName())) {
                reportTO.setVendorName(reportCommand.getVendorName());
            }
            ArrayList vendorList = new ArrayList();
            vendorList = financeBP.processVendorList(vendorType);
            request.setAttribute("vendorList", vendorList);

            if (!"".equals(reportCommand.getVendorName())) {
                ArrayList vendorOutStandingReport = new ArrayList();
                vendorOutStandingReport = reportBP.vendorOutStandingReport(reportTO);
                request.setAttribute("vendorOutStandingReport", vendorOutStandingReport);
            }

            if (param.equals("ExportExcel")) {
                System.out.println("111 = ");
                path = "content/report/vendorOutstandingReportExcel.jsp";
            } else {
                path = "content/report/vendorOutstandingReport.jsp";
            }
//            path = "content/report/vendorOutstandingReport.jsp";

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to dashboardReport Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleCustomerOutstandingReport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList accountReceivableList = new ArrayList();
        ReportTO reportTO = new ReportTO();
        reportCommand = command;
        String path = "", fromDate = "", toDate = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Reports >> Dashboard Report";
        request.setAttribute("menuPath", menuPath);
        String vendorType = ThrottleConstants.leasingVendorTypeId;
        String param = request.getParameter("param");
        System.out.println("param  " + param);
        try {
            if (reportCommand.getFromDate() != null && !"".equals(reportCommand.getFromDate())) {
                reportTO.setFromDate(reportCommand.getFromDate());
                request.setAttribute("fromDate", reportCommand.getFromDate());
            }
            if (reportCommand.getToDate() != null && !"".equals(reportCommand.getToDate())) {
                reportTO.setToDate(reportCommand.getToDate());
                request.setAttribute("toDate", reportCommand.getToDate());
            }
            if (reportCommand.getCustomerId() != null && !"".equals(reportCommand.getCustomerId())) {
                reportTO.setCustomerId(reportCommand.getCustomerId());
                request.setAttribute("custId", reportCommand.getCustomerId());
            }

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);
            System.out.println("customerList Size Is :::" + customerList.size());

            if (!"".equals(reportCommand.getCustomerId())) {
                ArrayList custommerOutStandingReport = new ArrayList();
                custommerOutStandingReport = reportBP.custommerOutStandingReport(reportTO);
                request.setAttribute("custommerOutStandingReport", custommerOutStandingReport);
            }

            if (param.equals("ExportExcel")) {
                System.out.println("111 = ");
                path = "content/report/custommerOutStandingReportExcel.jsp";
            } else {
                path = "content/report/custommerOutStandingReport.jsp";
            }
//            path = "content/report/vendorOutstandingReport.jsp";

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to custommerOutStandingReport Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getCustRevenu(HttpServletRequest request, HttpServletResponse response, ReportCommand command) throws IOException {
        HttpSession session = request.getSession();
        ArrayList custRevenu = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
//        JsonTO jsonTO = new JsonTO();
//        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
//            String dashboardopstrukCount = "";
            response.setContentType("text/html");
//            dashboardopstrukCount = request.getParameter("dashboardopstrukCount");
//            report.setDashboardopstrukCount(dashboardopstrukCount);
//            System.out.println("yyn"+dashboardopstrukCount);
            custRevenu = reportBP.getCustRevenu(report);
            System.out.println(" " + custRevenu);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = custRevenu.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                report = (ReportTO) itr.next();
                jsonObject.put("CustName", report.getCustName());
                jsonObject.put("Revenu", report.getRevenue());
//                jsonObject.put("Count", ReportTO.getTotalVehicle());
                jsonArray.put(jsonObject);
            }
            pw.print(jsonArray);
            System.out.println("jsonArraytruckno = " + jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getCustProfit(HttpServletRequest request, HttpServletResponse response, ReportCommand command) throws IOException {
        HttpSession session = request.getSession();
        ArrayList custProfit = new ArrayList();
        ReportTO report = new ReportTO();
        reportCommand = command;
//        JsonTO jsonTO = new JsonTO();
//        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
//            String dashboardopstrukCount = "";
            response.setContentType("text/html");
//            dashboardopstrukCount = request.getParameter("dashboardopstrukCount");
//            report.setDashboardopstrukCount(dashboardopstrukCount);
//            System.out.println("yyn"+dashboardopstrukCount);
            custProfit = reportBP.getCustProfit(report);
            System.out.println(" " + custProfit);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = custProfit.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                report = (ReportTO) itr.next();
                jsonObject.put("CustName", report.getCustName());
                jsonObject.put("Profit", report.getProfit());
//                jsonObject.put("Count", ReportTO.getTotalVehicle());
                jsonArray.put(jsonObject);
            }
            pw.print(jsonArray);
            System.out.println("jsonArraytruckno = " + jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView configureMailAlert(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";
        ReportTO report = new ReportTO();
        reportCommand = command;

        try {

            ArrayList customerList = new ArrayList();
            customerList = customerBP.processActiveCustomerList();
            request.setAttribute("customerList", customerList);

            ArrayList companyList = new ArrayList();
            companyList = reportBP.getCompanyList();
            request.setAttribute("companyList", companyList);

            ArrayList reportList = new ArrayList();
            reportList = reportBP.getReport(report);
            request.setAttribute("reportList", reportList);

            path = "content/report/reportSchedule.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveMailAlert(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        reportCommand = command;
        String menuPath = "Reports >> Dashboard Report";
        request.setAttribute("menuPath", menuPath);
        int userId = (Integer) session.getAttribute("userId");
        int status = 0;

        try {

            String customerId = request.getParameter("customerId");
            String reportType = request.getParameter("reportType");
            String mailIdTo = request.getParameter("mailIdTo");
            String mailIdCc = request.getParameter("mailIdCc");
            String active_ind = request.getParameter("active_ind");
            String idNo = request.getParameter("idNo");
            String fleetCenterId = request.getParameter("fleetCenterId");

            ReportTO reportTO = new ReportTO();
            reportTO.setCustomerId(customerId);
            reportTO.setReporttype(reportType);
            reportTO.setMailIdTo(mailIdTo);
            reportTO.setMailIdCc(mailIdCc);
            reportTO.setActiveInd(active_ind);
            reportTO.setIdentityNo(idNo);
            reportTO.setFleetCenterId(fleetCenterId);

            status = reportBP.saveMailAlert(reportTO, userId);

            if (status >= 0) {
                request.setAttribute("successMessage", "<font color='green'>Added Successfully</font>");
            } else {
                request.setAttribute("successMessage", "<font color='red'>Added Failed</font>");
            }

            ArrayList customerList = new ArrayList();
            customerList = customerBP.processActiveCustomerList();
            request.setAttribute("customerList", customerList);

            ArrayList reportList = new ArrayList();
            reportList = reportBP.getReport(reportTO);
            request.setAttribute("reportList", reportList);

            ArrayList companyList = new ArrayList();
            companyList = reportBP.getCompanyList();
            request.setAttribute("companyList", companyList);

            path = "content/report/reportSchedule.jsp";

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to dashboardReport Report --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView viewconsignmentreport(HttpServletRequest request, HttpServletResponse response, ReportCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        reportCommand = command;
        String menuPath = "";
        OperationTO operationTO = new OperationTO();
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  Route Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String roleId = "" + (Integer) session.getAttribute("RoleId");
        int userId = (Integer) session.getAttribute("userId");
        operationTO.setRoleId(roleId);
        operationTO.setUserId(userId);

        ArrayList consignmentList = new ArrayList();
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String customerId = request.getParameter("customerId");
            String param = request.getParameter("param");
            String tripId = request.getParameter("tripId");
            String tripCode = request.getParameter("tripCode");
            String orderType = request.getParameter("orderType");
            String customerName = request.getParameter("customerName");
            String consignmentOrderReferenceNo = request.getParameter("consignmentOrderReferenceNo");
            String status = request.getParameter("status");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            request.setAttribute("customerId", customerId);
            request.setAttribute("orderType", orderType);                                                                                                                                                                                                                                                                                   
            request.setAttribute("customerName", customerName);
            request.setAttribute("consignmentOrderReferenceNo", consignmentOrderReferenceNo);
            request.setAttribute("status", status);
            operationTO.setCustomerId(customerId);
            operationTO.setOrderType(orderType);
            operationTO.setConsignmentNoteNo(consignmentOrderReferenceNo);
            operationTO.setStatus(status);
            operationTO.setTripId(tripId);
            
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate) || fromDate==null) {
                fromDate = startDate;
            }
            if ("".equals(toDate) || toDate==null) {
                toDate = endDate;
            }
            
            System.out.println("From Date : "+fromDate);
            System.out.println("To Date : "+toDate);
            
            operationTO.setFromDate(fromDate);
            operationTO.setToDate(toDate);
            request.setAttribute("tripCode", tripCode);
            request.setAttribute("tripId", tripId);
            request.setAttribute("fromDate", operationTO.getFromDate());
            request.setAttribute("toDate", operationTO.getToDate());
            
            consignmentList = operationBP.getConsignmentReportList(operationTO);
            System.out.println("consignmentList " + consignmentList.size());
            request.setAttribute("consignmentList", consignmentList);

            ArrayList statusDetails = new ArrayList();
            statusDetails = operationBP.getTripStatus();
            request.setAttribute("statusDetails", statusDetails);
            if("ExportExcel".equals(param)){   
            path = "content/trip/viewConsignmentNoteReportExportExcel.jsp";
            }else{
            path = "content/trip/viewConsignmentNoteReport.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

}
