/*------------------------------------------------------------------------------
 * BharathConstants.java
 * Mar 19, 2008
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
 -----------------------------------------------------------------------------*/

package ets.domain.util;

/******************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver     Date                               Author                    Change
 * ----------------------------------------------------------------------------
 * 1.0    Mar 19, 2008                       ES		            Created
 *
 *****************************************************************************/

/**
 * <br>BharathConstants object :This class have all the Event Protal constants.
 * @author Satheeshkumar P
*/
public class ParveenConstants {

	/*
	 * the values need to be configured in the Properties file and read from
	 * there.
	 *
	 */
	/**
	 * DEFAULT_COMMAND_OBJECT- DEFAULT_COMMAND_OBJECT
	 */
	public static final String DEFAULT_COMMAND_OBJECT = "command";




}
