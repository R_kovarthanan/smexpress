package ets.domain.security.business;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.security.data.SecurityDAO;
import ets.domain.vehicle.data.VehicleDAO;
//import ets.domain.vendor.data.VendorDAO;
import java.util.ArrayList;
import java.util.Iterator;

public class SecurityBP {

    public SecurityBP() {
    }
    SecurityDAO securityDAO;

    public SecurityDAO getSecurityDAO() {
        return securityDAO;
    }



    public void setSecurityDAO(SecurityDAO securityDAO) {
        this.securityDAO = securityDAO;
    }

    /**
     * This method used to Get Vendor Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String processGetVendorList() throws FPRuntimeException, FPBusinessException {


        String status = "";

        status = securityDAO.getVendorList();
        // woDate = securityDAO.getWoDate();
        if (status.equals("null")) {
            throw new FPBusinessException("EM-SEC-01");
        }
        return status;
    }

    //  assignedList = vendorDAO.getAssignedList(vendorId);
    /**
     * This method used to Get Vendor Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processWorkorderDate(int compId) throws FPRuntimeException, FPBusinessException {

        ArrayList workorderDate = new ArrayList();
        System.out.println("i am in bp");
        workorderDate = securityDAO.getworkorderDate(compId);
        if (workorderDate.size() == 0) {
            throw new FPBusinessException("EM-SEC-02");
        }
        return workorderDate;
    }
    //processDateList
    /**
     * This method used to Get Vendor Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processDateList() throws FPRuntimeException, FPBusinessException {

        ArrayList dateList = new ArrayList();
        System.out.println("i am in bp");
        dateList = securityDAO.getDateList();
        if (dateList.size() == 0) {
            throw new FPBusinessException("EM-SEC-02");
        }
        return dateList;
    }

    // processOutList  
    /**
     * This method used to Get Vendor Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processOutList() throws FPRuntimeException, FPBusinessException {

        ArrayList outList = new ArrayList();
        System.out.println("i am in bp");
        outList = securityDAO.getOutList();
        if (outList.size() == 0) {
            throw new FPBusinessException("EM-SEC-02");
        }
        return outList;
    }

    // InAndOut
    /**
     * This method used to Get Vendor Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processInAndOut() throws FPRuntimeException, FPBusinessException {

        ArrayList inAndOut = new ArrayList();
        System.out.println("i am in bp");
        inAndOut = securityDAO.getInAndOut();
        if (inAndOut.size() == 0) {
            throw new FPBusinessException("EM-SEC-02");
        }
        return inAndOut;
    }
    //processSaveVehicle
    /**
     * This method used to Modify Desgintion Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int processSaveVehicle(ArrayList List, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = securityDAO.doProcessSaveVehicle(List, UserId);
        //create job card vijay mar 16 
        //create job card by vijay
        if (insertStatus != 0) {
            int jobCardId = 0;
            SecurityTO securityTO = null;
            int workOrderId = 0;
            int km = 0;
            int hm = 0;
            Iterator itr = List.iterator();
            while (itr.hasNext()) {
                securityTO = new SecurityTO();
                securityTO = (SecurityTO) itr.next();
                workOrderId = Integer.parseInt(securityTO.getWoId());
                km = Integer.parseInt(securityTO.getKm());
                hm = securityTO.getHourMeter();
                if (!"0".equals(securityTO.getWoId())) {                    
                    jobCardId = securityDAO.insertJobCard(workOrderId, km, hm);
                }
            }
        }
        return insertStatus;
    }
    // processSaveVehicleOutTime
    /**
     * This method used to Modify Desgintion Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int processSaveVehicleOutTime(ArrayList List, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = securityDAO.doProcessSaveVehicleOutTime(List, UserId);

        if (insertStatus == 0) {
            throw new FPBusinessException("EM-SEC-02");
        }
        return insertStatus;
    }

    // processGetVehicleType 
    /**
     * This method used to Modify Desgintion Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processGetVehicleType() throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleType = new ArrayList();
        vehicleType = securityDAO.doProcessGetVehicleType();

        if (vehicleType.size() == 0) {
            throw new FPBusinessException("EM-SEC-02");
        }
        return vehicleType;
    }

    // processSaveAddVehicle 
    /**
     * This method used to Insert Vendor Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises  ===
     */
    public int processSaveAddVehicle(SecurityTO securityTO, int UserId, int compId) throws FPRuntimeException, FPBusinessException {


        ArrayList workorderDat = new ArrayList();
        workorderDat = securityDAO.getworkorderDate(compId);

        //
        Iterator itr = workorderDat.iterator();
        SecurityTO listTO = null;
        while (itr.hasNext()) {
            listTO = (SecurityTO) itr.next();
            System.out.println("listTO.getVehicleNumber()" + listTO.getVehicleNumber());
            System.out.println("securityTO.getVehicleNumber()" + securityTO.getVehicleNumber());





            if (listTO.getVehicleNumber().equals(securityTO.getVehicleNumber())) {
                securityTO.setWoDate(listTO.getWoId());

            } else {
                System.out.println("not found");
            }
        // securityTO.setWoId(listTO.getWoId());
        //  System.out.println("listTO.getWoId()--listTO.getWoId()"+listTO.getWoId());
        //   if(listTO.getVehicleNumber().equals(securityTO.getVehicleNumber()){

        //     System.out.println("equals"+vendorTO.getItemId());

        }

        int status = 0;
        status = securityDAO.doSaveAddVehicleSecurity(securityTO, UserId);


        if (status == 0) {
            throw new FPBusinessException("EM-VND-03");
        }

        return status;
    }

    /**
     * This method used to Get vehicle in purpose list from db.
     *
     *
     * @throws Exception -Throws when a Exception arises
     */
    public ArrayList getVehicleInPurposeList() throws FPRuntimeException, FPBusinessException {

        ArrayList vehicleInPurposeList = new ArrayList();
        vehicleInPurposeList = securityDAO.getVehicleInPurposeList();
        return vehicleInPurposeList;
    }

    public ArrayList getOpList() throws FPRuntimeException, FPBusinessException {

        ArrayList opList = new ArrayList();
        opList = securityDAO.getOpList();
        return opList;
    }

    /**
     * This method used to Get vehicle list from db.
     *
     *
     * @throws Exception -Throws when a Exception arises
     */
    public ArrayList getVehicleList(int compId, SecurityTO securityTO) throws FPRuntimeException, FPBusinessException {

        ArrayList vehicleList = new ArrayList();

        if (securityTO.getVehicleStatus().equals("SCHEDULED")) {
            securityTO.setVehicleInPurpose("4");
            vehicleList = securityDAO.getScheduleVehicleList(compId, securityTO);
        } else if (securityTO.getVehicleStatus().equals("IN")) {
            vehicleList = securityDAO.getInVehicleList(compId, securityTO);
        } else if (securityTO.getVehicleStatus().equals("OUT")) {
            vehicleList = securityDAO.getOutVehicleList(compId, securityTO);
        } else if (securityTO.getVehicleStatus().equals("BOTH")) {
            vehicleList = securityDAO.getInOutVehicleList(compId, securityTO);
        }

        return vehicleList;
    }
    
        public String isThisVehicleIn(String regNo) {
        String  vehicleNo="";
        vehicleNo = securityDAO.isThisVehicleIn(regNo);
        return vehicleNo;
    }
}  
    

   

