/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.finance.web;

/**
 *
 * @author ASHOK
 */
public class FinanceCommand {

    private String vendorName = "";
    private String vendorId = "";
    private String customerId = "";
    private String paymentMode = "";
    private String referenceNo = "";
    private String receiptDate = "";
    private String receiptAmount = "";
    private String userName = "";
    private String bankName = "";
    private String bankCode = "";
    private String description = "";
    private String bankid = "";
    private String groupName = "";
    private String groupCode = "";
    private String address = "";
    private String phoneNo = "";
    private String accntCode = "";
    private String bankMappingCode = "";
    private String openingBalance = "";
    private String acctType = "";

//Finance ledger
    private String ledgerID = "";
    private String ledgerCode = "";
    private String ledgerName = "";
    private String amountType = "";
    private String active_ind = "";

//    Finanace Level master
    private String fullName = "";
    private String levelgroupId = "";
    private String primaryID = "";
    private String LevelName = "";

//Finance Voucher
    private String voucherTypeID = "";
    private String voucherTypeCode = "";
    private String voucherTypeName = "";
//    Account Type
    private String accountEntryTypeID = "";
    private String accountEntryTypeCode = "";
    private String accountEntryTypeName = "";
//tax master
    private String taxId = "";
    private String taxCode = "";
    private String taxName = "";
// country master
    private String countryID = "";
    private String countryCode = "";
    private String countryName = "";
//State master
    private String stateID = "";
    private String stateCode = "";
    private String stateName = "";
    //District master
    private String districtID = "";
    private String districtName = "";
    private String districtCode = "";
//contra entry
//    private String address = "";
//    private String phoneNo = "";
//    private String accntCode = "";
//    private String bankMappingCode = "";
    private String date = "";

    private String[] bankid1 = null;
    private String[] bankid2 = null;
    private String[] amount = null;
    private String[] accType = null;
    private String[] narration = null;

//    journal entry
    private String invoiceId = "";
    private String invoiceCode = "";
    private String invRefCode = "";
    private String invoiceDate = "";
    private String grandTotal = "";
    private String invoiceStatus = "";
    private String numberOfTrip = "";
    private String remarks = "";
    private String invoiceFor = "";
    private String cNAmt = "";
    private String dNAmt = "";

//  Credit Note
    private String Date = "";
    private String ledger = "";
    private String headerNarration = "";

    //  VAT Master
    private String vat = "";
    private String effectiveDate = "";
    private String Description = "";
    private String Active_ind = "";

    private String levelID = "";

    private String chequeNo = "";
    private String chequeDate = "";
    private String BankHead = "";

    private String fromDate = "";
    private String toDate = "";
    private String finYear = "";

    private String primaryBankId = "";
    private String primaryBankName = "";
    private String primaryBankCode = "";

    private String accountYearId = "";
    private String branchId = "";
    private String IFSCCode = "";
    private String BSRCode = "";
    private String taxPercent = "";
    private String chargeCode = "";
    private String vendorTypeId = "";
    private String bankId = "";
    private String branchName = "";
    
    private String cofStatus = "";
    private String tripId = "";

    public String getCofStatus() {
        return cofStatus;
    }

    public void setCofStatus(String cofStatus) {
        this.cofStatus = cofStatus;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }
    
    

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getVendorTypeId() {
        return vendorTypeId;
    }

    public void setVendorTypeId(String vendorTypeId) {
        this.vendorTypeId = vendorTypeId;
    }

    public String getChargeCode() {
        return chargeCode;
    }

    public void setChargeCode(String chargeCode) {
        this.chargeCode = chargeCode;
    }

    public String getTaxPercent() {
        return taxPercent;
    }

    public void setTaxPercent(String taxPercent) {
        this.taxPercent = taxPercent;
    }

    public String getIFSCCode() {
        return IFSCCode;
    }

    public void setIFSCCode(String IFSCCode) {
        this.IFSCCode = IFSCCode;
    }

    public String getBSRCode() {
        return BSRCode;
    }

    public void setBSRCode(String BSRCode) {
        this.BSRCode = BSRCode;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getPrimaryBankId() {
        return primaryBankId;
    }

    public void setPrimaryBankId(String primaryBankId) {
        this.primaryBankId = primaryBankId;
    }

    public String getPrimaryBankName() {
        return primaryBankName;
    }

    public void setPrimaryBankName(String primaryBankName) {
        this.primaryBankName = primaryBankName;
    }

    public String getPrimaryBankCode() {
        return primaryBankCode;
    }

    public void setPrimaryBankCode(String primaryBankCode) {
        this.primaryBankCode = primaryBankCode;
    }

    public String getAccountYearId() {
        return accountYearId;
    }

    public void setAccountYearId(String accountYearId) {
        this.accountYearId = accountYearId;
    }

    public String getFinYear() {
        return finYear;
    }

    public void setFinYear(String finYear) {
        this.finYear = finYear;
    }

    public String getHeaderNarration() {
        return headerNarration;
    }

    public void setHeaderNarration(String headerNarration) {
        this.headerNarration = headerNarration;
    }

    public String getLedger() {
        return ledger;
    }

    public void setLedger(String ledger) {
        this.ledger = ledger;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBankid() {
        return bankid;
    }

    public void setBankid(String bankid) {
        this.bankid = bankid;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getActive_ind() {
        return active_ind;
    }

    public void setActive_ind(String active_ind) {
        this.active_ind = active_ind;
    }

    public String getAmountType() {
        return amountType;
    }

    public void setAmountType(String amountType) {
        this.amountType = amountType;
    }

    public String getLedgerCode() {
        return ledgerCode;
    }

    public void setLedgerCode(String ledgerCode) {
        this.ledgerCode = ledgerCode;
    }

    public String getLedgerID() {
        return ledgerID;
    }

    public void setLedgerID(String ledgerID) {
        this.ledgerID = ledgerID;
    }

    public String getLedgerName() {
        return ledgerName;
    }

    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public String getVoucherTypeCode() {
        return voucherTypeCode;
    }

    public void setVoucherTypeCode(String voucherTypeCode) {
        this.voucherTypeCode = voucherTypeCode;
    }

    public String getVoucherTypeID() {
        return voucherTypeID;
    }

    public void setVoucherTypeID(String voucherTypeID) {
        this.voucherTypeID = voucherTypeID;
    }

    public String getVoucherTypeName() {
        return voucherTypeName;
    }

    public void setVoucherTypeName(String voucherTypeName) {
        this.voucherTypeName = voucherTypeName;
    }

    public String getAccountEntryTypeCode() {
        return accountEntryTypeCode;
    }

    public void setAccountEntryTypeCode(String accountEntryTypeCode) {
        this.accountEntryTypeCode = accountEntryTypeCode;
    }

    public String getAccountEntryTypeID() {
        return accountEntryTypeID;
    }

    public void setAccountEntryTypeID(String accountEntryTypeID) {
        this.accountEntryTypeID = accountEntryTypeID;
    }

    public String getAccountEntryTypeName() {
        return accountEntryTypeName;
    }

    public void setAccountEntryTypeName(String accountEntryTypeName) {
        this.accountEntryTypeName = accountEntryTypeName;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public String getTaxName() {
        return taxName;
    }

    public void setTaxName(String taxName) {
        this.taxName = taxName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryID() {
        return countryID;
    }

    public void setCountryID(String countryID) {
        this.countryID = countryID;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getStateID() {
        return stateID;
    }

    public void setStateID(String stateID) {
        this.stateID = stateID;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public String getDistrictID() {
        return districtID;
    }

    public void setDistrictID(String districtID) {
        this.districtID = districtID;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getAccntCode() {
        return accntCode;
    }

    public void setAccntCode(String accntCode) {
        this.accntCode = accntCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBankMappingCode() {
        return bankMappingCode;
    }

    public void setBankMappingCode(String bankMappingCode) {
        this.bankMappingCode = bankMappingCode;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String[] getAccType() {
        return accType;
    }

    public void setAccType(String[] accType) {
        this.accType = accType;
    }

    public String[] getAmount() {
        return amount;
    }

    public void setAmount(String[] amount) {
        this.amount = amount;
    }

    public String[] getBankid1() {
        return bankid1;
    }

    public void setBankid1(String[] bankid1) {
        this.bankid1 = bankid1;
    }

    public String[] getBankid2() {
        return bankid2;
    }

    public void setBankid2(String[] bankid2) {
        this.bankid2 = bankid2;
    }

    public String[] getNarration() {
        return narration;
    }

    public void setNarration(String[] narration) {
        this.narration = narration;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getcNAmt() {
        return cNAmt;
    }

    public void setcNAmt(String cNAmt) {
        this.cNAmt = cNAmt;
    }

    public String getdNAmt() {
        return dNAmt;
    }

    public void setdNAmt(String dNAmt) {
        this.dNAmt = dNAmt;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getInvRefCode() {
        return invRefCode;
    }

    public void setInvRefCode(String invRefCode) {
        this.invRefCode = invRefCode;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInvoiceFor() {
        return invoiceFor;
    }

    public void setInvoiceFor(String invoiceFor) {
        this.invoiceFor = invoiceFor;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public String getNumberOfTrip() {
        return numberOfTrip;
    }

    public void setNumberOfTrip(String numberOfTrip) {
        this.numberOfTrip = numberOfTrip;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getLevelName() {
        return LevelName;
    }

    public void setLevelName(String LevelName) {
        this.LevelName = LevelName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getLevelgroupId() {
        return levelgroupId;
    }

    public void setLevelgroupId(String levelgroupId) {
        this.levelgroupId = levelgroupId;
    }

    public String getPrimaryID() {
        return primaryID;
    }

    public void setPrimaryID(String primaryID) {
        this.primaryID = primaryID;
    }

    public String getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(String openingBalance) {
        this.openingBalance = openingBalance;
    }

    public String getLevelID() {
        return levelID;
    }

    public void setLevelID(String levelID) {
        this.levelID = levelID;
    }

    public String getBankHead() {
        return BankHead;
    }

    public void setBankHead(String BankHead) {
        this.BankHead = BankHead;
    }

    public String getChequeDate() {
        return chequeDate;
    }

    public void setChequeDate(String chequeDate) {
        this.chequeDate = chequeDate;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getReceiptAmount() {
        return receiptAmount;
    }

    public void setReceiptAmount(String receiptAmount) {
        this.receiptAmount = receiptAmount;
    }

    public String getReceiptDate() {
        return receiptDate;
    }

    public void setReceiptDate(String receiptDate) {
        this.receiptDate = receiptDate;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getAcctType() {
        return acctType;
    }

    public void setAcctType(String acctType) {
        this.acctType = acctType;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

}
