/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.finance.web;

import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.Part;
import ets.domain.operation.business.OperationBP;
//import ets.domain.billing.business.BillingBP;
import ets.domain.trip.business.TripBP;
import org.json.JSONArray;
import org.json.JSONObject;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.web.BaseController;
import ets.domain.finance.business.FinanceBP;
import ets.domain.finance.business.FinanceTO;
import ets.domain.users.business.LoginBP;
import ets.domain.util.FPLogUtils;
import ets.domain.util.ParveenErrorConstants;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;

import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;
import com.oreilly.servlet.multipart.Part;

import ets.arch.business.PaginationHelper;
import ets.arch.util.sendVendorInvoice;
import ets.domain.billing.business.BillingTO;
import ets.domain.racks.business.RackBP;
import ets.domain.racks.business.RackTO;
import java.util.Iterator;
import ets.domain.users.web.CryptoLibrary;
import ets.domain.vehicle.business.VehicleBP;
import java.io.*;
import ets.domain.section.business.SectionBP;
import ets.domain.purchase.business.PurchaseBP;
import ets.domain.section.business.SectionTO;
import ets.domain.util.ThrottleConstants;
import ets.domain.vehicle.business.VehicleTO;
import ets.domain.vehicle.web.VehicleCommand;
import ets.domain.vendor.business.VendorBP;
import ets.domain.vendor.business.VendorTO;
import java.rmi.UnknownHostException;
import java.util.List;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author ASHOK
 */
public class FinanceController extends BaseController {

//    BillingBP billingBP;
    TripBP tripBP;
    FinanceCommand financeCommand;
    LoginBP loginBP;
    FinanceBP financeBP;
    OperationBP operationBP;
    VendorBP vendorBP;

    public FinanceCommand getFinanceCommand() {
        return financeCommand;
    }

    public void setFinanceCommand(FinanceCommand financeCommand) {
        this.financeCommand = financeCommand;
    }

    public TripBP getTripBP() {
        return tripBP;
    }

    public void setTripBP(TripBP tripBP) {
        this.tripBP = tripBP;
    }

    public VendorBP getVendorBP() {
        return vendorBP;
    }

    public void setVendorBP(VendorBP vendorBP) {
        this.vendorBP = vendorBP;
    }

    public OperationBP getOperationBP() {
        return operationBP;
    }

    public void setOperationBP(OperationBP operationBP) {
        this.operationBP = operationBP;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public FinanceBP getFinanceBP() {
        return financeBP;
    }

    public void setFinanceBP(FinanceBP financeBP) {
        this.financeBP = financeBP;
    }

//    public BillingBP getBillingBP() {
//        return billingBP;
//    }
//
//    public void setBillingBP(BillingBP billingBP) {
//        this.billingBP = billingBP;
//    }
    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        //////System.out.println("request.getRequestURI() = " + request.getRequestURI());
        binder.closeNoCatch();
        initialize(request);

    }

    /**
     * This method used to handle finance Group.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView financeGroup(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList groupList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Group";
        menuPath = "Finance  >> Group Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/financeGroup.jsp";
            groupList = financeBP.getGroupList();
            request.setAttribute("groupLists", groupList);
            //  //////System.out.println("groupList ===========>>>>>>> " + groupList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView addGroupPage(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        menuPath = "Finance >> Group >> Add Group ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Add Group";
            request.setAttribute("pageTitle", pageTitle);
            path = "content/Finance/newGroup.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Group --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveNewGroup(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList groupList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> Group  ";
        String pageTitle = "Manage Group";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (financeCommand.getGroupName() != null && financeCommand.getGroupName() != "") {
                financeTO.setGroupname(financeCommand.getGroupName());
            }
            if (financeCommand.getGroupCode() != null && financeCommand.getGroupCode() != "") {
                financeTO.setGroupcode(financeCommand.getGroupCode());
            }
            if (financeCommand.getDescription() != null && financeCommand.getDescription() != "") {
                financeTO.setGroupdesc(financeCommand.getDescription());
            }
            path = "content/Finance/financeGroup.jsp";
            status = financeBP.saveNewGroup(financeTO, userId);
            groupList = financeBP.getGroupList();
            request.setAttribute("groupLists", groupList);
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Group Added Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bank Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView alterGroupDetail(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList groupalterList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Alter Group";
        menuPath = "Finance  >>Group Alter ";
        request.setAttribute("pageTitle", pageTitle);
        String groupid = request.getParameter("groupid");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/alterGroup.jsp";
            groupalterList = financeBP.processGroupalterList(groupid);
            request.setAttribute("groupalterList", groupalterList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewGroup --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveAlterGroup(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList groupList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> Group  ";
        String pageTitle = "Manage Group";
        request.setAttribute("pageTitle", pageTitle);
        String groupid = request.getParameter("groupid");
        //////System.out.println("groupid 1======> " + groupid);
//        int bankid = (Integer) request.getParameter("bankid");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (financeCommand.getGroupName() != null && financeCommand.getGroupName() != "") {
                financeTO.setGroupname(financeCommand.getGroupName());
            }
            if (financeCommand.getGroupCode() != null && financeCommand.getGroupCode() != "") {
                financeTO.setGroupcode(financeCommand.getGroupCode());
            }
            if (financeCommand.getDescription() != null && financeCommand.getDescription() != "") {
                financeTO.setGroupdesc(financeCommand.getDescription());
            }

            path = "content/Finance/financeGroup.jsp";
            status = financeBP.saveAlterGroup(financeTO, groupid, userId);
            groupList = financeBP.getGroupList();
            request.setAttribute("groupLists", groupList);

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Group Modified Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Alter Group Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle finance level master.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView levelMaster(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList groupList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Level Master";
        menuPath = "Finance  >> Level Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/financeLevelMaster.jsp";
            ArrayList primaryLevelMasterList = new ArrayList();
            primaryLevelMasterList = financeBP.getPrimaryLevelMasterList();
            request.setAttribute("primaryLevelMasterList", primaryLevelMasterList);
            ArrayList levelMasterList = new ArrayList();
            levelMasterList = getLevelComplete();
            request.setAttribute("levelMasterList", levelMasterList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ArrayList getLevelComplete() {
        //////System.out.println("in size....");
        ArrayList levelMasterList = new ArrayList();
        ArrayList level1 = new ArrayList();
        ArrayList level2 = new ArrayList();
        ArrayList level3 = new ArrayList();
        ArrayList level4 = new ArrayList();
        ArrayList level5 = new ArrayList();
        ArrayList level6 = new ArrayList();

        try {
            //////System.out.println("1");
            ArrayList primaryList = financeBP.handlePrimaryList();
            //////System.out.println("catList.size(): " + primaryList.size());
            FinanceTO lData1 = null, rsParent = null;
            Iterator itr = primaryList.iterator();
            int cntr = 0;
            while (itr.hasNext()) { //running through roots
                //////System.out.println("1");
                level2 = new ArrayList();
                level3 = new ArrayList();
                level4 = new ArrayList();
                level5 = new ArrayList();
                level6 = new ArrayList();
                lData1 = new FinanceTO();

                lData1 = (FinanceTO) itr.next();
                lData1.setLevelgroupId(lData1.getGroupID());
                //////System.out.println("lData1_primaryId: " + lData1.getGroupID());
                //////System.out.println("lData1_PrimaryName: " + lData1.getGroupName());
                cntr++;

                ArrayList levelList = financeBP.getLevelList(lData1);
                //////System.out.println("Top levelList.size(): " + levelList.size());

                //getting parents for each root
                Iterator itr1 = null;
                int cntr1 = 0;
                level2 = new ArrayList();
                if (levelList.size() > 0) {
                    itr1 = levelList.iterator();
                    FinanceTO lData2 = null;
                    while (itr1.hasNext()) { //running through parents
                        cntr1++;
                        lData2 = new FinanceTO();
                        lData2 = (FinanceTO) itr1.next();
//                        FinanceTO rsChild1 = null;

                        //////System.out.println("lData2_levelID: " + lData2.getLevelgroupId());
                        //////System.out.println("lData2_levelName: " + lData2.getLevelgroupName());
                        ArrayList levelList2 = financeBP.getLevelList(lData2);
                        //////System.out.println("levelList2.size() = " + levelList2.size());
                        Iterator itr2 = null;
                        int cntr2 = 0;
                        level3 = new ArrayList();
                        if (levelList2.size() > 0) {
                            itr2 = levelList2.iterator();
                            FinanceTO lData3 = null;
                            while (itr2.hasNext()) { //running through children
                                cntr2++;
                                lData3 = new FinanceTO();
                                lData3 = (FinanceTO) itr2.next();
                                //////System.out.println("lData3_levelID: " + lData3.getLevelgroupId());
                                //////System.out.println("lData3_levelName: " + lData3.getLevelgroupName());
                                ArrayList levelList3 = financeBP.getLevelList(lData3);
                                //////System.out.println("levelList3.size() = " + levelList3.size());
                                Iterator itr3 = null;
                                int cntr3 = 0;
                                level4 = new ArrayList();
                                if (levelList3.size() > 0) {
                                    itr3 = levelList3.iterator();
                                    FinanceTO lData4 = null;
                                    while (itr3.hasNext()) {
                                        cntr3++;
                                        lData4 = new FinanceTO();
                                        lData4 = (FinanceTO) itr3.next();
                                        //////System.out.println("lData4_levelID: " + lData4.getLevelgroupId());
                                        //////System.out.println("lData4_levelName: " + lData4.getLevelgroupName());
                                        ArrayList levelList4 = financeBP.getLevelList(lData4);
                                        //////System.out.println("levelList4.size() = " + levelList4.size());
                                        Iterator itr4 = null;
                                        int cntr4 = 0;
                                        level5 = new ArrayList();
                                        if (levelList4.size() > 0) {
                                            itr4 = levelList4.iterator();
                                            FinanceTO lData5 = null;
                                            while (itr4.hasNext()) {
                                                cntr4++;
                                                lData5 = new FinanceTO();
                                                lData5 = (FinanceTO) itr4.next();
                                                //////System.out.println("lData5_levelID: " + lData5.getLevelgroupId());
                                                //////System.out.println("lData5_levelName: " + lData5.getLevelgroupName());
                                                ArrayList levelList5 = financeBP.getLevelList(lData5);
                                                //////System.out.println("levelList5.size() = " + levelList5.size());
                                                Iterator itr5 = null;
                                                int cntr5 = 0;
                                                level6 = new ArrayList();
                                                if (levelList5.size() > 0) {
                                                    itr5 = levelList5.iterator();
                                                    FinanceTO lData6 = null;
                                                    while (itr5.hasNext()) {
                                                        cntr5++;
                                                        lData6 = new FinanceTO();
                                                        lData6 = (FinanceTO) itr5.next();
                                                        level6.add(lData6);
                                                    }
                                                }
                                                if (cntr5 > 0) {
                                                    lData5.setChildList(level6);
                                                }
                                                level5.add(lData5);
                                            }
                                        }
                                        if (cntr4 > 0) {
                                            lData4.setChildList(level5);
                                        }
                                        level4.add(lData4);
                                    }
                                }
                                if (cntr3 > 0) {
                                    lData3.setChildList(level4);
                                }
                                level3.add(lData3);
                            }
                        }
                        if (cntr2 > 0) {
                            lData2.setChildList(level3);
                        }
                        level2.add(lData2);
                    }
                }
                if (cntr1 > 0) {
                    lData1.setChildList(level2);
                }
                level1.add(lData1);
            }
            Iterator itr1 = level1.iterator();
            Iterator itr2 = level1.iterator();
            Iterator itr3 = level1.iterator();
            Iterator itr4 = level1.iterator();
            Iterator itr5 = level1.iterator();
            Iterator itr6 = level1.iterator();
            FinanceTO lLevel1 = null;
            FinanceTO lLevel2 = null;
            FinanceTO lLevel3 = null;
            FinanceTO lLevel4 = null;
            FinanceTO lLevel5 = null;
            FinanceTO lLevel6 = null;
            FinanceTO levelMaster = null;

            //////System.out.println("22222");
            while (itr1.hasNext()) {
                //////System.out.println("333");
                lLevel1 = new FinanceTO();
                lLevel1 = (FinanceTO) itr1.next();
                //////System.out.println("lLevel1: " + lLevel1.getGroupName());

                levelMaster = new FinanceTO();
                levelMaster.setGroupID(lLevel1.getGroupID());
                levelMaster.setLevelgroupId(lLevel1.getGroupID());
                levelMaster.setLevelgroupName(lLevel1.getGroupName());
                levelMasterList.add(levelMaster);

                itr2 = lLevel1.getChildList().iterator();
                while (itr2.hasNext()) {
                    lLevel2 = new FinanceTO();
                    lLevel2 = (FinanceTO) itr2.next();
                    //////System.out.println("  lLevel2: " + lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName());

                    levelMaster = new FinanceTO();
                    levelMaster.setGroupID(lLevel1.getGroupID());
                    levelMaster.setLevelgroupId(lLevel2.getLevelgroupId());
                    levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName());
                    levelMasterList.add(levelMaster);

                    itr3 = lLevel2.getChildList().iterator();
                    while (itr3.hasNext()) {
                        lLevel3 = new FinanceTO();
                        lLevel3 = (FinanceTO) itr3.next();
                        //////System.out.println("   lLevel3: " + lLevel1.getGroupName() + "-" + lLevel2.getLevelgroupName() + "-" + lLevel3.getLevelgroupName());
                        levelMaster = new FinanceTO();
                        levelMaster.setGroupID(lLevel1.getGroupID());
                        levelMaster.setLevelgroupId(lLevel3.getLevelgroupId());
                        levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName());
                        levelMasterList.add(levelMaster);
                        itr4 = lLevel3.getChildList().iterator();
                        while (itr4.hasNext()) {
                            lLevel4 = new FinanceTO();
                            lLevel4 = (FinanceTO) itr4.next();
                            //////System.out.println("    lLevel4: " + lLevel1.getGroupName() + "-" + lLevel2.getLevelgroupName() + "-" + lLevel3.getLevelgroupName() + "-" + lLevel4.getLevelgroupName());
                            levelMaster = new FinanceTO();
                            levelMaster.setGroupID(lLevel1.getGroupID());
                            levelMaster.setLevelgroupId(lLevel4.getLevelgroupId());
                            levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName() + "->" + lLevel4.getLevelgroupName());
                            levelMasterList.add(levelMaster);
                            itr5 = lLevel4.getChildList().iterator();
                            while (itr5.hasNext()) {
                                lLevel5 = new FinanceTO();
                                lLevel5 = (FinanceTO) itr5.next();
                                //////System.out.println("     lLevel5: " + lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName() + "->" + lLevel4.getLevelgroupName() + "->" + lLevel5.getLevelgroupName());
                                levelMaster = new FinanceTO();
                                levelMaster.setGroupID(lLevel1.getGroupID());
                                levelMaster.setLevelgroupId(lLevel5.getLevelgroupId());
                                levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName() + "->" + lLevel4.getLevelgroupName() + "->" + lLevel5.getLevelgroupName());
                                levelMasterList.add(levelMaster);
                                itr6 = lLevel5.getChildList().iterator();
                                while (itr6.hasNext()) {
                                    lLevel6 = new FinanceTO();
                                    lLevel6 = (FinanceTO) itr6.next();
                                    //////System.out.println("     lLevel6: " + lLevel1.getGroupName() + "-" + lLevel2.getLevelgroupName() + "-" + lLevel3.getLevelgroupName() + "-" + lLevel4.getLevelgroupName() + "-" + lLevel5.getLevelgroupName() + "-" + lLevel6.getLevelgroupName());
                                    levelMaster = new FinanceTO();
                                    levelMaster.setGroupID(lLevel1.getGroupID());
                                    levelMaster.setLevelgroupId(lLevel6.getLevelgroupId());
                                    levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName() + "->" + lLevel4.getLevelgroupName() + "->" + lLevel5.getLevelgroupName() + "->" + lLevel6.getLevelgroupName());
                                    levelMasterList.add(levelMaster);
                                }
                            }
                        }
                    }
                }
            }
            //////System.out.println("levelMasterList.size() 1= " + levelMasterList.size());
        } catch (Exception e) {
        }
        //////System.out.println("levelMasterList.size() 2= " + levelMasterList.size());
        return levelMasterList;
    }

    public ArrayList getLevelCompleteForTrialBalance(String fromDate, String toDate) {
        //////System.out.println("in size....");
        ArrayList levelMasterList = new ArrayList();
        ArrayList level1 = new ArrayList();
        ArrayList level2 = new ArrayList();
        ArrayList level3 = new ArrayList();
        ArrayList level4 = new ArrayList();
        ArrayList level5 = new ArrayList();
        ArrayList level6 = new ArrayList();

        try {
            ArrayList primaryList = financeBP.handlePrimaryList();
            //////System.out.println("catList.size(): " + primaryList.size());
            FinanceTO lData1 = null, rsParent = null;
            Iterator itr = primaryList.iterator();
            int cntr = 0;
            while (itr.hasNext()) { //running through roots
                //////System.out.println("1 = ");
                level2 = new ArrayList();
                level3 = new ArrayList();
                level4 = new ArrayList();
                level5 = new ArrayList();
                level6 = new ArrayList();
                lData1 = new FinanceTO();

                lData1 = (FinanceTO) itr.next();
                lData1.setLevelgroupId(lData1.getGroupID());
                lData1.setFromDate(fromDate);
                lData1.setToDate(toDate);
                //////System.out.println("lData1_primaryId: " + lData1.getGroupID());
                //////System.out.println("lData1_PrimaryName: " + lData1.getGroupName());
                cntr++;

                ArrayList levelList = financeBP.getLevelList(lData1);
                //////System.out.println("Top levelList.size(): " + levelList.size());

                //getting parents for each root
                Iterator itr1 = null;
                int cntr1 = 0;
                level2 = new ArrayList();
                if (levelList.size() > 0) {
                    itr1 = levelList.iterator();
                    FinanceTO lData2 = null;
                    while (itr1.hasNext()) { //running through parents
                        //////System.out.println("2 = ");
                        cntr1++;
                        lData2 = new FinanceTO();
                        lData2 = (FinanceTO) itr1.next();
//                        FinanceTO rsChild1 = null;

                        //////System.out.println("lData2_levelID: " + lData2.getLevelgroupId());
                        //////System.out.println("lData2_levelName: " + lData2.getLevelgroupName());
                        lData2.setFromDate(fromDate);
                        lData2.setToDate(toDate);
                        ArrayList levelList2 = financeBP.getLevelList(lData2);
                        //////System.out.println("levelList2.size() = " + levelList2.size());
                        Iterator itr2 = null;
                        int cntr2 = 0;
                        level3 = new ArrayList();
                        if (levelList2.size() > 0) {
                            itr2 = levelList2.iterator();
                            FinanceTO lData3 = null;
                            while (itr2.hasNext()) { //running through children
                                //////System.out.println("3 = ");
                                cntr2++;
                                lData3 = new FinanceTO();
                                lData3 = (FinanceTO) itr2.next();
                                //////System.out.println("lData3_levelID: " + lData3.getLevelgroupId());
                                //////System.out.println("lData3_levelName: " + lData3.getLevelgroupName());
                                lData3.setFromDate(fromDate);
                                lData3.setToDate(toDate);
                                ArrayList levelList3 = financeBP.getLevelList(lData3);
                                //////System.out.println("levelList3.size() = " + levelList3.size());
                                Iterator itr3 = null;
                                int cntr3 = 0;
                                level4 = new ArrayList();
                                if (levelList3.size() > 0) {
                                    itr3 = levelList3.iterator();
                                    FinanceTO lData4 = null;
                                    while (itr3.hasNext()) {
                                        //////System.out.println("4 = ");
                                        cntr3++;
                                        lData4 = new FinanceTO();
                                        lData4 = (FinanceTO) itr3.next();
                                        //////System.out.println("lData4_levelID: " + lData4.getLevelgroupId());
                                        //////System.out.println("lData4_levelName: " + lData4.getLevelgroupName());
                                        lData4.setFromDate(fromDate);
                                        lData4.setToDate(toDate);
                                        ArrayList levelList4 = financeBP.getLevelList(lData4);
                                        //////System.out.println("levelList4.size() = " + levelList4.size());
                                        Iterator itr4 = null;
                                        int cntr4 = 0;
                                        level5 = new ArrayList();
                                        if (levelList4.size() > 0) {
                                            itr4 = levelList4.iterator();
                                            FinanceTO lData5 = null;
                                            while (itr4.hasNext()) {
                                                //////System.out.println("5 = ");
                                                cntr4++;
                                                lData5 = new FinanceTO();
                                                lData5 = (FinanceTO) itr4.next();
                                                //////System.out.println("lData5_levelID: " + lData5.getLevelgroupId());
                                                //////System.out.println("lData5_levelName: " + lData5.getLevelgroupName());
                                                lData5.setFromDate(fromDate);
                                                lData5.setToDate(toDate);
                                                ArrayList levelList5 = financeBP.getLevelList(lData5);
                                                //////System.out.println("levelList5.size() = " + levelList5.size());
                                                Iterator itr5 = null;
                                                int cntr5 = 0;
                                                level6 = new ArrayList();
                                                if (levelList5.size() > 0) {
                                                    //////System.out.println("6 = ");
                                                    itr5 = levelList5.iterator();
                                                    FinanceTO lData6 = null;
                                                    while (itr5.hasNext()) {
                                                        cntr5++;
                                                        lData6 = new FinanceTO();
                                                        lData6 = (FinanceTO) itr5.next();
                                                        level6.add(lData6);
                                                    }
                                                }
                                                if (cntr5 > 0) {
                                                    lData5.setChildList(level6);
                                                }
                                                level5.add(lData5);
                                            }
                                        }
                                        if (cntr4 > 0) {
                                            lData4.setChildList(level5);
                                        }
                                        level4.add(lData4);
                                    }
                                }
                                if (cntr3 > 0) {
                                    lData3.setChildList(level4);
                                }
                                level3.add(lData3);
                            }
                        }
                        if (cntr2 > 0) {
                            lData2.setChildList(level3);
                        }
                        level2.add(lData2);
                    }
                }
                if (cntr1 > 0) {
                    lData1.setChildList(level2);
                }
                level1.add(lData1);
            }

        } catch (Exception e) {
        }

        return level1;
    }

    public ModelAndView saveNewLevel(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList ledgerList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> Level  ";
        String pageTitle = "Manage Level";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (financeCommand.getPrimaryID() != null && financeCommand.getPrimaryID() != "") {
                //////System.out.println("financeCommand.getPrimaryID() = " + financeCommand.getPrimaryID());
                financeTO.setPrimaryID(financeCommand.getPrimaryID());
            }
            if (financeCommand.getLevelgroupId() != null && financeCommand.getLevelgroupId() != "") {
                //////System.out.println("financeCommand.getLevelgroupId() = " + financeCommand.getLevelgroupId());
                financeTO.setLevelgroupId(financeCommand.getLevelgroupId());
            }
            if (financeCommand.getDescription() != null && financeCommand.getDescription() != "") {
                //////System.out.println("financeCommand.getDescription() = " + financeCommand.getDescription());
                financeTO.setDescription(financeCommand.getDescription());
            }
            if (financeCommand.getOpeningBalance() != null && financeCommand.getOpeningBalance() != "") {
                //////System.out.println("financeCommand.getOpeningBalance() = " + financeCommand.getOpeningBalance());
                financeTO.setOpeningBalance(financeCommand.getOpeningBalance());
            }
            String LevelName = request.getParameter("LevelName");
            //////System.out.println("LevelName = " + LevelName);
            financeTO.setLevelName(LevelName);

            String fullName = request.getParameter("fullName");
            //////System.out.println("fullName= " + fullName + "->" + LevelName);
            String Fname = fullName + "->" + LevelName;
            financeTO.setFullName(Fname);
            status = financeBP.saveNewLevel(financeTO, userId);
            //////System.out.println("status = " + status);

            path = "content/Finance/financeLevelMaster.jsp";
            ArrayList levelMasterList = new ArrayList();
            levelMasterList = getLevelComplete();
            request.setAttribute("levelMasterList", levelMasterList);

            ArrayList primaryLevelMasterList = new ArrayList();
            primaryLevelMasterList = financeBP.getPrimaryLevelMasterList();
            request.setAttribute("primaryLevelMasterList", primaryLevelMasterList);

            //ledgerList = financeBP.getLedgerList();
            //request.setAttribute("ledgerLists", ledgerList);
            ////////System.out.println("ledgerList.size() = " + ledgerList.size());
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Level Added Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView ledgerReport(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList ledgerList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        String[] temp = null;
        String sDate = "";
        String eDate = "";
        menuPath = "Finance >> >> Reports >> Ledger Report  ";
        String pageTitle = "Manage Level";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/Finance/ledgerReport.jsp";

            ArrayList journalLedgerList = financeBP.getJournalLedgerList();
            System.out.println("journalLedgerList.lenth() = " + journalLedgerList.size());
            request.setAttribute("journalLedgerList", journalLedgerList);
            String ledgerId = request.getParameter("ledgerId");
            String ledgerName = request.getParameter("ledgerName");
            request.setAttribute("ledgerId", ledgerId);
            request.setAttribute("ledgerName", ledgerName);
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            System.out.println("fromDate = " + fromDate);
            System.out.println("toDate = " + toDate);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            if (ledgerId != null && !"0".equals(ledgerId)) {

//                if (fromDate != "") {
//                    temp = fromDate.split("-");
//                    sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
                sDate = fromDate;
                eDate = toDate;
//                }
//                if (toDate != "") {
//                    temp = toDate.split("-");
//                    eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
//                }
                String creditOB;
                String creditCB;
                String openingBalance = financeBP.getOpeningBalance(ledgerId, sDate);
                System.out.println("openingBalance:" + openingBalance);
                String closingBalance = financeBP.getClosingBalance(ledgerId, eDate);
                System.out.println("closingBalance:" + closingBalance);

                /*String amountType = "";
                 if (openingBalance != null && !"".equals(openingBalance)) {
                 String[] temp1 = openingBalance.split("~");
                 openingBalance = temp1[0];
                 amountType = temp1[1];
                 } else {
                 openingBalance = "0";
                 amountType = "N";
                 }
                 request.setAttribute("openingBalance", openingBalance);
                 request.setAttribute("amountType", amountType);
                 */
                if (openingBalance.contains("-")) {
                    creditOB = openingBalance;
                    String temp1[] = creditOB.split("-");
                    request.setAttribute("openingBalanceCredit", temp1[1]);
                    request.setAttribute("openingBalanceDebit", "");
                } else {
                    request.setAttribute("openingBalanceCredit", "");
                    request.setAttribute("openingBalanceDebit", openingBalance);
                }
                if (closingBalance.contains("-")) {
                    creditCB = closingBalance;
                    String temp1[] = creditCB.split("-");
                    request.setAttribute("closingBalanceCredit", temp1[1]);
                    request.setAttribute("closingBalanceDebit", "");
                } else {
                    request.setAttribute("closingBalanceCredit", "");
                    request.setAttribute("closingBalanceDebit", closingBalance);
                }

                ArrayList ledgerTransactionList = financeBP.getLedgerTransactionList(ledgerId, sDate, eDate);
                System.out.println("ledgerTransactionList.lenth() = " + ledgerTransactionList.size());
                request.setAttribute("ledgerTransactionList", ledgerTransactionList);

            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    /*
     public ModelAndView dayBook(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

     HttpSession session = request.getSession();
     int status = 0;
     String path = "";
     financeCommand = command;
     int userId = (Integer) session.getAttribute("userId");
     ArrayList ledgerList = new ArrayList();
     FinanceTO financeTO = new FinanceTO();
     String menuPath = "";
     String[] temp = null;
     String sDate = "";
     String eDate = "";
     menuPath = "Finance >> >> Reports >> DayBook  ";
     String pageTitle = "Manage Level";
     request.setAttribute("pageTitle", pageTitle);
     request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
     try {

     path = "content/Finance/dayBook.jsp";

     String fromDate = request.getParameter("fromDate");
     String toDate = request.getParameter("toDate");
     //////System.out.println("fromDate:" + fromDate);
     //////System.out.println("toDate:" + toDate);

     ArrayList ledgerTransactionList = new ArrayList();
     if (fromDate != null && !"".equals(fromDate)) {
     request.setAttribute("fromDate", fromDate);
     request.setAttribute("toDate", toDate);
     ledgerTransactionList = financeBP.getDayBook(fromDate, toDate);
     } else {
     ledgerTransactionList = financeBP.getDayBook("", "");
     }
     //////System.out.println("ledgerTransactionList.lenth() = " + ledgerTransactionList.size());
     request.setAttribute("ledgerTransactionList", ledgerTransactionList);

     } catch (FPRuntimeException exception) {

     FPLogUtils.fpErrorLog("Run time exception --> " + exception);
     return new ModelAndView("content/common/error.jsp");
     } catch (Exception exception) {
     exception.printStackTrace();
     FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
     return new ModelAndView("content/common/error.jsp");
     }
     return new ModelAndView(path);
     }
     */

    public ModelAndView viewTBDetails(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList ledgerList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        String[] temp = null;
        String sDate = "";
        String eDate = "";
        menuPath = "Finance >> >> Reports >> TB Details  ";
        String pageTitle = "Manage Level";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            path = "content/Finance/viewTBDetails.jsp";

            String levelId = request.getParameter("levelId");
            request.setAttribute("levelId", levelId);
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            //////System.out.println("fromDate = " + fromDate);
            //////System.out.println("toDate = " + toDate);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            if (levelId != null && !"0".equals(levelId)) {

//                if (fromDate != "") {
//                    temp = fromDate.split("-");
//                    sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
//                }
//                if (toDate != "") {
//                    temp = toDate.split("-");
//                    eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
//                }
                ArrayList ledgerTransactionList = financeBP.getTBTransactionList(levelId, sDate, eDate);
                //////System.out.println("ledgerTransactionList.lenth() = " + ledgerTransactionList.size());
                request.setAttribute("ledgerTransactionList", ledgerTransactionList);

            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView exportLedgerReport(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList ledgerList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        String[] temp = null;
        String sDate = "";
        String eDate = "";
        menuPath = "Finance >> Reports >> Export Ledger Report  ";
        String pageTitle = "Manage Level";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            path = "content/Finance/exportLedgerReport.jsp";

            ArrayList journalLedgerList = financeBP.getJournalLedgerList();
            //////System.out.println("journalLedgerList.lenth() = " + journalLedgerList.size());
            request.setAttribute("journalLedgerList", journalLedgerList);

            String ledgerId = request.getParameter("ledgerId");
            request.setAttribute("ledgerId", ledgerId);

            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            //////System.out.println("fromDate = " + fromDate);
            //////System.out.println("toDate = " + toDate);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            if (ledgerId != null && !"0".equals(ledgerId)) {

                if (fromDate != "") {
                    temp = fromDate.split("-");
                    sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
                }
                if (toDate != "") {
                    temp = toDate.split("-");
                    eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
                }
                String openingBalance = financeBP.getOpeningBalance(ledgerId, sDate);
                //////System.out.println("openingBalance:" + openingBalance);
                String[] temp1 = openingBalance.split("~");
                openingBalance = temp1[0];
                String amountType = temp1[1];
                request.setAttribute("openingBalance", openingBalance);
                request.setAttribute("amountType", amountType);
                ArrayList ledgerTransactionList = financeBP.getLedgerTransactionList(ledgerId, sDate, eDate);
                //////System.out.println("ledgerTransactionList.lenth() = " + ledgerTransactionList.size());
                request.setAttribute("ledgerTransactionList", ledgerTransactionList);

            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView ledgerReportOld(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList ledgerList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> >> Reports >> Ledger Report  ";
        String pageTitle = "Manage Level";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            path = "content/Finance/ledgerReportOld.jsp";

            ArrayList journalLedgerList = financeBP.getJournalLedgerList();
            //////System.out.println("journalLedgerList.lenth() = " + journalLedgerList.size());
            request.setAttribute("journalLedgerList", journalLedgerList);

            String ledgerId = request.getParameter("ledgerId");
            request.setAttribute("ledgerId", ledgerId);
            if (ledgerId != null && !"0".equals(ledgerId)) {
                ArrayList ledgerTransactionList = financeBP.getLedgerTransactionListOld(ledgerId);
                //////System.out.println("ledgerTransactionList.lenth() = " + ledgerTransactionList.size());
                request.setAttribute("ledgerTransactionList", ledgerTransactionList);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView trialBalance(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList ledgerList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> >> Reports >> Trial Balance  ";
        String pageTitle = "Manage Level";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            path = "content/Finance/trialBalance.jsp";

            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");

            ArrayList levelMasterList = new ArrayList();
            //////System.out.println("fromDate:" + fromDate);
            //////System.out.println("toDate:" + toDate);
            if (fromDate != null && !"".equals(fromDate)) {
                levelMasterList = getLevelCompleteForTrialBalance(fromDate, toDate);
                request.setAttribute("fromDate", fromDate);
                request.setAttribute("toDate", toDate);
            }
            request.setAttribute("levelMasterList", levelMasterList);
            ArrayList ledgerTransactionList = financeBP.getTBTransactionList("", fromDate, toDate);
            request.setAttribute("ledgerTransactionList", ledgerTransactionList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /* public ModelAndView profitAndLoss(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

     HttpSession session = request.getSession();
     int status = 0;
     String path = "";
     financeCommand = command;
     int userId = (Integer) session.getAttribute("userId");
     ArrayList ledgerList = new ArrayList();
     FinanceTO financeTO = new FinanceTO();
     String menuPath = "";
     menuPath = "Finance >> >> Reports >> P & L  ";
     String pageTitle = "Manage Level";
     request.setAttribute("pageTitle", pageTitle);
     request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
     try {

     path = "content/Finance/profitAndLoss.jsp";

     String fromDate = request.getParameter("fromDate");
     String toDate = request.getParameter("toDate");

     ArrayList levelMasterList = new ArrayList();
     //////System.out.println("fromDate:" + fromDate);
     //////System.out.println("toDate:" + toDate);
     if (fromDate != null && !"".equals(fromDate)) {
     levelMasterList = getLevelCompleteForTrialBalance(fromDate, toDate);
     request.setAttribute("fromDate", fromDate);
     request.setAttribute("toDate", toDate);
     }
     request.setAttribute("levelMasterList", levelMasterList);

     } catch (FPRuntimeException exception) {
     FPLogUtils.fpErrorLog("Run time exception --> " + exception);
     return new ModelAndView("content/common/error.jsp");
     } catch (Exception exception) {
     exception.printStackTrace();
     FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
     return new ModelAndView("content/common/error.jsp");
     }
     return new ModelAndView(path);
     }

     public ModelAndView balanceSheet(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

     HttpSession session = request.getSession();
     int status = 0;
     String path = "";
     financeCommand = command;
     int userId = (Integer) session.getAttribute("userId");
     ArrayList ledgerList = new ArrayList();
     FinanceTO financeTO = new FinanceTO();
     String menuPath = "";
     menuPath = "Finance >> >> Reports >> Balance  Sheet";
     String pageTitle = "Manage Level";
     request.setAttribute("pageTitle", pageTitle);
     request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
     try {

     path = "content/Finance/balanceSheet.jsp";

     String fromDate = request.getParameter("fromDate");
     String toDate = request.getParameter("toDate");

     ArrayList levelMasterList = new ArrayList();
     //////System.out.println("fromDate:" + fromDate);
     //////System.out.println("toDate:" + toDate);
     if (fromDate != null && !"".equals(fromDate)) {
     levelMasterList = getLevelCompleteForTrialBalance(fromDate, toDate);
     request.setAttribute("fromDate", fromDate);
     request.setAttribute("toDate", toDate);
     }
     request.setAttribute("levelMasterList", levelMasterList);

     } catch (FPRuntimeException exception) {
     FPLogUtils.fpErrorLog("Run time exception --> " + exception);
     return new ModelAndView("content/common/error.jsp");
     } catch (Exception exception) {
     exception.printStackTrace();
     FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
     return new ModelAndView("content/common/error.jsp");
     }
     return new ModelAndView(path);
     }
     */
    /**
     * This method used to handle financeLedger.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView financeLedger(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        //////System.out.println("11111111");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList ledgerList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Ledger";
        menuPath = "Finance  >> Ledger Master";
        //////System.out.println("pageTitle = " + pageTitle);
        request.setAttribute("pageTitle", pageTitle);
        //////System.out.println("pageTitle = " + pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            ledgerList = financeBP.getLedgerList();
            request.setAttribute("ledgerLists", ledgerList);
            //////System.out.println("ledgerList ===========>>>>>>> " + ledgerList);

            ArrayList ledgerlevelList = new ArrayList();
            ledgerlevelList = getLevelComplete();
            request.setAttribute("ledgerlevelList", ledgerlevelList);

            path = "content/Finance/financeLedger.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to ledger --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView addLedgerPage(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        ArrayList groupList = new ArrayList();
        financeCommand = command;
        String menuPath = "";
        menuPath = "Finance >> Ledger >> Add Ledger ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Add Ledger";
            request.setAttribute("pageTitle", pageTitle);
            path = "content/Finance/newLedger.jsp";
            groupList = financeBP.getGroupList();

            ArrayList ledgerlevelList = new ArrayList();
            ledgerlevelList = getLevelComplete();
            request.setAttribute("ledgerlevelList", ledgerlevelList);

            request.setAttribute("groupLists", groupList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Ledger --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveNewLedger(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        //////System.out.println("11111111111");
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList ledgerList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> Ledger";
        String pageTitle = "Manage Ledger";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (financeCommand.getPrimaryID() != null && financeCommand.getPrimaryID() != "") {
//                System.out.println("PrimaryID==== " + financeCommand.getPrimaryID());
                financeTO.setPrimaryID(financeCommand.getPrimaryID());
            }
            if (financeCommand.getLedgerID() != null && financeCommand.getLedgerID() != "") {
//                System.out.println("LedgerID====" + financeCommand.getLedgerID());
                financeTO.setLedgerID(financeCommand.getLedgerID());
            }
            if (financeCommand.getLevelgroupId() != null && financeCommand.getLevelgroupId() != "") {
//                System.out.println("LevelgroupId==== " + financeCommand.getLevelgroupId());
                financeTO.setLevelgroupId(financeCommand.getLevelgroupId());
            }
            if (financeCommand.getDescription() != null && financeCommand.getDescription() != "") {
//                System.out.println("Description==== " + financeCommand.getDescription());
                financeTO.setDescription(financeCommand.getDescription());
            }
            if (financeCommand.getOpeningBalance() != null && financeCommand.getOpeningBalance() != "") {
//                System.out.println("OpeningBalance==== " + financeCommand.getOpeningBalance());
                financeTO.setOpeningBalance(financeCommand.getOpeningBalance());
            }

            String acctType = request.getParameter("acctType");
            String ledgerName = request.getParameter("ledgerName");
            String fullName = request.getParameter("fullName");
            String Fname = fullName + "->" + ledgerName;

            financeTO.setLedgerName(ledgerName);
            financeTO.setAcctType(acctType);
            financeTO.setFullName(Fname);

            System.out.println("acctType==== " + acctType);
//            System.out.println("ledgerName ==== " + ledgerName);
//            System.out.println("fullName==== " + fullName + "->" + ledgerName);
            status = financeBP.saveNewLedger(financeTO, userId);
            //////System.out.println("status = " + status);

            ArrayList ledgerlevelList = new ArrayList();
            ledgerlevelList = getLevelComplete();
            request.setAttribute("ledgerlevelList", ledgerlevelList);

            path = "content/Finance/newLedger.jsp";
            ledgerList = financeBP.getLedgerList();
            request.setAttribute("ledgerLists", ledgerList);
            //ledgerList = financeBP.getLedgerList();
            //request.setAttribute("ledgerLists", ledgerList);
            ////////System.out.println("ledgerList.size() = " + ledgerList.size());

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Ledger Added Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

//    public ModelAndView saveNewLedger(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
//
//        HttpSession session = request.getSession();
//        int status = 0;
//        String path = "";
//        financeCommand = command;
//        int userId = (Integer) session.getAttribute("userId");
//        ArrayList ledgerList = new ArrayList();
//        FinanceTO financeTO = new FinanceTO();
//        String menuPath = "";
//        menuPath = "Finance >> Ledger  ";
//        String pageTitle = "Manage Ledger";
//        request.setAttribute("pageTitle", pageTitle);
//        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        try {
//            if (financeCommand.getLedgerName() != null && financeCommand.getLedgerName() != "") {
//                financeTO.setLedgerName(financeCommand.getLedgerName());
//            }
//            if (financeCommand.getLedgerCode() != null && financeCommand.getLedgerCode() != "") {
//                financeTO.setLedgerCode(financeCommand.getLedgerCode());
//            }
//            if (financeCommand.getGroupCode() != null && financeCommand.getGroupCode() != "") {
//                financeTO.setGroupCode(financeCommand.getGroupCode());
//            }
//            if (financeCommand.getAmountType() != null && financeCommand.getAmountType() != "") {
//                financeTO.setAmountType(financeCommand.getAmountType());
//            }
//            path = "content/Finance/financeLedger.jsp";
//            status = financeBP.saveNewLedger(financeTO, userId);
//            //////System.out.println("status = " + status);
//            ledgerList = financeBP.getLedgerList();
//            request.setAttribute("ledgerLists", ledgerList);
//            //////System.out.println("ledgerList.size() = " + ledgerList.size());
//            if (status > 0) {
//                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Leadger Added Successfully");
//            }
//
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }
    public ModelAndView alterLedgerDetail(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList ledgeralterList = new ArrayList();
        ArrayList groupList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Alter Ledger";
        menuPath = "Finance  >> Alter  Ledger ";
        request.setAttribute("pageTitle", pageTitle);
        String ledgerID = request.getParameter("ledgerID");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/alterLedger.jsp";
            ledgeralterList = financeBP.processLedgeralterList(ledgerID);
            request.setAttribute("ledgeralterList", ledgeralterList);

            groupList = financeBP.getGroupList();
            request.setAttribute("groupLists", groupList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewLedger --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveAlterLedger(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList ledgerList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> Ledger  ";
        String pageTitle = "Manage Ledger";
        request.setAttribute("pageTitle", pageTitle);
        String ledgerID = request.getParameter("ledgerID");
        //////System.out.println("ledgerID 1======> " + ledgerID);
//        int bankid = (Integer) request.getParameter("bankid");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (financeCommand.getLedgerName() != null && financeCommand.getLedgerName() != "") {
                financeTO.setLedgerName(financeCommand.getLedgerName());
            }
            if (financeCommand.getGroupCode() != null && financeCommand.getGroupCode() != "") {
                financeTO.setGroupcode(financeCommand.getGroupCode());
            }
            if (financeCommand.getAmountType() != null && financeCommand.getAmountType() != "") {
                financeTO.setAmountType(financeCommand.getAmountType());
            }
            if (financeCommand.getOpeningBalance() != null && financeCommand.getOpeningBalance() != "") {
                financeTO.setOpeningBalance(financeCommand.getOpeningBalance());
            }
            if (financeCommand.getDate() != null && financeCommand.getDate() != "") {
                financeTO.setDate(financeCommand.getDate());
            }

            path = "content/Finance/financeLedger.jsp";
            status = financeBP.saveAlterLedger(financeTO, ledgerID, userId);
            ledgerList = financeBP.getLedgerList();
            request.setAttribute("ledgerLists", ledgerList);

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Ledger Modified Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Alter Group Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //processGetLevel1
    public void getLevel1Data(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        //////System.out.println("innnnnnnnnnnnnnnnnnnnnnn");
        // FinanceCommand = command;
        String groupCode = request.getParameter("groupCode");
        //////System.out.println("groupCode = " + groupCode);
        String suggestions = "";

        try {
            if (groupCode != null && !"".equals(groupCode)) {
                suggestions = financeBP.getLevel1Data(groupCode);
            }
            PrintWriter writer = response.getWriter();

            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(suggestions);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Model data in Ajax --> " + exception);
        }
    }
    //process Get Level2

    public void getLevel2Data(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        //////System.out.println("in 222222222222");
        // FinanceCommand = command;
        String level1Code = request.getParameter("level1Code");
        //////System.out.println("level1Code = " + level1Code);
        String suggestions = "";

        try {
            if (level1Code != null && !"".equals(level1Code)) {
                suggestions = financeBP.getLevel2Data(level1Code);
            }
            PrintWriter writer = response.getWriter();

            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(suggestions);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Model data in Ajax --> " + exception);
        }
    }

    //process Get Level3
    public void getLevel3Data(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        //////System.out.println("in 222222222222");
        // FinanceCommand = command;
        String level2Code = request.getParameter("level2Code");
        //////System.out.println("level2Code = " + level2Code);
        String suggestions = "";

        try {
            if (level2Code != null && !"".equals(level2Code)) {
                suggestions = financeBP.getLevel3Data(level2Code);
            }
            PrintWriter writer = response.getWriter();

            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(suggestions);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Model data in Ajax --> " + exception);
        }
    }

    /**
     * This method used to handle financeVoucher.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView financeVoucher(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList voucherList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Voucher";
        menuPath = "Finance  >> Voucher Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/financeVoucher.jsp";
            voucherList = financeBP.getVoucherList();
            request.setAttribute("voucherLists", voucherList);
            //////System.out.println("voucherList ===========>>>>>>> " + voucherList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to voucherList --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView addVoucherPage(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        ArrayList groupList = new ArrayList();
        financeCommand = command;
        String menuPath = "";
        menuPath = "Finance >> Voucher >> Add Voucher ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Add Voucher";
            request.setAttribute("pageTitle", pageTitle);
            path = "content/Finance/newVoucher.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Ledger --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveNewVoucher(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList voucherList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> Voucher  ";
        String pageTitle = "Manage Voucher";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (financeCommand.getVoucherTypeName() != null && financeCommand.getVoucherTypeName() != "") {
                financeTO.setVoucherTypeName(financeCommand.getVoucherTypeName());
            }
            if (financeCommand.getVoucherTypeCode() != null && financeCommand.getVoucherTypeCode() != "") {
                financeTO.setVoucherTypeCode(financeCommand.getVoucherTypeCode());
            }
            if (financeCommand.getDescription() != null && financeCommand.getDescription() != "") {
                financeTO.setDescription(financeCommand.getDescription());
            }
            path = "content/Finance/financeVoucher.jsp";
            status = financeBP.saveNewVoucher(financeTO, userId);
            //////System.out.println("status = " + status);
            voucherList = financeBP.getVoucherList();
            request.setAttribute("voucherLists", voucherList);
            //////System.out.println("ledgerList.size() = " + voucherList.size());
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "New Voucher Added Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView alterVoucherDetail(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList voucheralterList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Alter Voucher";
        menuPath = "Finance  >> Alter  Voucher ";
        request.setAttribute("pageTitle", pageTitle);
        String voucherTypeID = request.getParameter("voucherTypeID");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/alterVoucher.jsp";
            voucheralterList = financeBP.processVoucheralterList(voucherTypeID);
            request.setAttribute("voucheralterLists", voucheralterList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to AlterVoucher --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveAlterVoucher(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList voucherList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> Voucher  ";
        String pageTitle = "Manage Voucher";
        request.setAttribute("pageTitle", pageTitle);
        String voucherTypeID = request.getParameter("voucherTypeID");
        //////System.out.println("voucherTypeID 1======> " + voucherTypeID);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (financeCommand.getVoucherTypeName() != null && financeCommand.getVoucherTypeName() != "") {
                financeTO.setVoucherTypeName(financeCommand.getVoucherTypeName());
            }
            if (financeCommand.getVoucherTypeCode() != null && financeCommand.getVoucherTypeCode() != "") {
                financeTO.setVoucherTypeCode(financeCommand.getVoucherTypeCode());
            }
            if (financeCommand.getDescription() != null && financeCommand.getDescription() != "") {
                financeTO.setDescription(financeCommand.getDescription());
            }

            path = "content/Finance/financeVoucher.jsp";
            status = financeBP.saveAlterVoucher(financeTO, voucherTypeID, userId);

            voucherList = financeBP.getVoucherList();
            request.setAttribute("voucherLists", voucherList);

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Voucher Alted Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Alter Group Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle financeAccountEntryType.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView financeAccountEntryType(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList accountTypeList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Account Type";
        menuPath = "Finance  >> Account Type Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/financeAccountType.jsp";
            accountTypeList = financeBP.getAccountTypeList();
            request.setAttribute("accountTypeLists", accountTypeList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to voucherList --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView addAccountTypePage(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        ArrayList accountTypeList = new ArrayList();
        financeCommand = command;
        String menuPath = "";
        menuPath = "Finance >> Add Account Type ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Add Account Type";
            request.setAttribute("pageTitle", pageTitle);
            path = "content/Finance/newAccountType.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Account Type --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveNewAccountType(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList accountTypeList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> Account Type  ";
        String pageTitle = "Manage Account Type";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (financeCommand.getAccountEntryTypeName() != null && financeCommand.getAccountEntryTypeName() != "") {
                financeTO.setAccountEntryTypeName(financeCommand.getAccountEntryTypeName());
            }
            if (financeCommand.getAccountEntryTypeCode() != null && financeCommand.getAccountEntryTypeCode() != "") {
                financeTO.setAccountEntryTypeCode(financeCommand.getAccountEntryTypeCode());
            }
            if (financeCommand.getDescription() != null && financeCommand.getDescription() != "") {
                financeTO.setDescription(financeCommand.getDescription());
            }
            path = "content/Finance/financeAccountType.jsp";
            status = financeBP.saveNewAccountType(financeTO, userId);
            //////System.out.println("status = " + status);
            accountTypeList = financeBP.getAccountTypeList();
            request.setAttribute("accountTypeLists", accountTypeList);
            //////System.out.println("accountTypeList = " + accountTypeList.size());
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "New Account Type Added Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView alterAccountTypeDetail(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList accountTypealterList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Alter Account Type";
        menuPath = "Finance  >> Alter  Account Type ";
        request.setAttribute("pageTitle", pageTitle);
        String accountEntryTypeID = request.getParameter("accountEntryTypeID");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/alterAccountType.jsp";
            accountTypealterList = financeBP.processAccountTypealterList(accountEntryTypeID);
            request.setAttribute("accountTypealterLists", accountTypealterList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to accountTypealterList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveAlterAccountType(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        //////System.out.println("ssssssssssssssssssssssssssss");
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList accountTypeList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> Account Type  ";
        String pageTitle = "Manage Account Type";
        request.setAttribute("pageTitle", pageTitle);
        String accountEntryTypeID = request.getParameter("accountEntryTypeID");
        //////System.out.println("accountEntryTypeID 1======> " + accountEntryTypeID);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (financeCommand.getAccountEntryTypeName() != null && financeCommand.getAccountEntryTypeName() != "") {
                financeTO.setAccountEntryTypeName(financeCommand.getAccountEntryTypeName());
                //////System.out.println("getAccountEntryTypeName() = " + financeCommand.getAccountEntryTypeName());
            }
            if (financeCommand.getAccountEntryTypeCode() != null && financeCommand.getAccountEntryTypeCode() != "") {
                financeTO.setAccountEntryTypeCode(financeCommand.getAccountEntryTypeCode());
                //////System.out.println("getAccountEntryTypeCode() = " + financeCommand.getAccountEntryTypeCode());
            }
            if (financeCommand.getDescription() != null && financeCommand.getDescription() != "") {
                financeTO.setDescription(financeCommand.getDescription());
                //////System.out.println("getDescription() = " + financeCommand.getDescription());
            }

            path = "content/Finance/financeAccountType.jsp";
            status = financeBP.saveAlterAccountType(financeTO, accountEntryTypeID, userId);

            accountTypeList = financeBP.getAccountTypeList();
            request.setAttribute("accountTypeLists", accountTypeList);
            //////System.out.println("accountTypeList = " + accountTypeList.size());

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Account Type Alted Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Alter Account Type Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle finance tax.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView financeTax(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList taxList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Tax Master";
        menuPath = "Finance  >> Tax Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/financeTax.jsp";
            taxList = financeBP.getTaxList();
            request.setAttribute("taxLists", taxList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to voucherList --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView addTaxPage(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        ArrayList taxList = new ArrayList();
        financeCommand = command;
        String menuPath = "";
        menuPath = "Finance >> Add Tax Master";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Add Tax Master ";
            request.setAttribute("pageTitle", pageTitle);
            path = "content/Finance/newTax.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Tax master --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void checkTaxName(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException {
        financeCommand = command;
        String suggestions = "";
        String taxName = request.getParameter("taxName");
        FinanceTO financeTO = new FinanceTO();
        financeTO.setTaxName(taxName);
        try {
            System.out.println("taxName = " + taxName);
            suggestions = financeBP.checktaxName(financeTO);
            System.out.println("suggestions = " + suggestions);
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(suggestions);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Model data in Ajax --> " + exception);
        }
    }

    public ModelAndView saveNewTax(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList taxList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> Tax Master";
        String pageTitle = "Manage Tax Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (financeCommand.getTaxName() != null && financeCommand.getTaxName() != "") {
                financeTO.setTaxName(financeCommand.getTaxName());
            }
            if (financeCommand.getTaxCode() != null && financeCommand.getTaxCode() != "") {
                financeTO.setTaxCode(financeCommand.getTaxCode());
            }
            if (financeCommand.getDescription() != null && financeCommand.getDescription() != "") {
                financeTO.setDescription(financeCommand.getDescription());
            }
            if (financeCommand.getEffectiveDate() != null && financeCommand.getEffectiveDate() != "") {
                financeTO.setEffectiveDate(financeCommand.getEffectiveDate());
            }
            if (financeCommand.getTaxPercent() != null && financeCommand.getTaxPercent() != "") {
                financeTO.setTaxPercent(financeCommand.getTaxPercent());
            }

            if (financeCommand.getTaxId() != null && financeCommand.getTaxId() != "") {
                financeTO.setTaxId(financeCommand.getTaxId());
                System.out.println("Tax Id = " + financeCommand.getTaxId());
                status = financeBP.saveAlterTax(financeTO, financeCommand.getTaxId(), userId);

                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Tax Updated Successfully");
                }

            } else {

                status = financeBP.saveNewTax(financeTO, userId);

                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "New Tax Added Successfully");
                }

            }

            path = "content/Finance/financeTax.jsp";

            taxList = financeBP.getTaxList();
            request.setAttribute("taxLists", taxList);
            //////System.out.println("taxlist = " + taxList.size());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView alterTaxDetail(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList taxAlterList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Alter Tax master";
        menuPath = "Finance  >> Alter  Tax master";
        request.setAttribute("pageTitle", pageTitle);
        String taxId = request.getParameter("taxId");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/alterTax.jsp";
            taxAlterList = financeBP.processTaxAlterList(taxId);
            request.setAttribute("taxAlterLists", taxAlterList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to accountTypealterList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveAlterTax(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList taxList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> Tax Master  ";
        String pageTitle = "Manage Tax Master";
        request.setAttribute("pageTitle", pageTitle);
        String taxId = request.getParameter("taxId");
        //////System.out.println("taxId 1======> " + taxId);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (financeCommand.getTaxName() != null && financeCommand.getTaxName() != "") {
                financeTO.setTaxName(financeCommand.getTaxName());
            }
            if (financeCommand.getTaxCode() != null && financeCommand.getTaxCode() != "") {
                financeTO.setTaxCode(financeCommand.getTaxCode());
            }
            if (financeCommand.getDescription() != null && financeCommand.getDescription() != "") {
                financeTO.setDescription(financeCommand.getDescription());
            }

            path = "content/Finance/financeTax.jsp";
            status = financeBP.saveAlterTax(financeTO, taxId, userId);
            taxList = financeBP.getTaxList();
            request.setAttribute("taxLists", taxList);

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Tax Alted Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Alter Tax Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle finance country.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView financeCountry(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList CountryList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Country Master";
        menuPath = "Finance  >> Country Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/financeCountry.jsp";
            CountryList = financeBP.getCountryList();
            request.setAttribute("CountryLists", CountryList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Country List --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView addCountryPage(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        ArrayList countryList = new ArrayList();
        financeCommand = command;
        String menuPath = "";
        menuPath = "Finance >> Add Country";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Add Country Master ";
            request.setAttribute("pageTitle", pageTitle);
            path = "content/Finance/newCountry.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Tax master --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveNewCountry(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList countryList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> country";
        String pageTitle = "Manage country Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (financeCommand.getCountryName() != null && financeCommand.getCountryName() != "") {
                financeTO.setCountryName(financeCommand.getCountryName());
            }
            if (financeCommand.getCountryCode() != null && financeCommand.getCountryCode() != "") {
                financeTO.setCountryCode(financeCommand.getCountryCode());
            }
            path = "content/Finance/financeCountry.jsp";
            status = financeBP.saveNewCountry(financeTO, userId);
            //////System.out.println("status = " + status);

            countryList = financeBP.getCountryList();
            request.setAttribute("CountryLists", countryList);
            //////System.out.println("Country = " + countryList.size());
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Country Added Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView alterCountryDetail(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList countryAlterList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Alter Country master";
        menuPath = "Finance  >> Alter  Country master";
        request.setAttribute("pageTitle", pageTitle);
        String countryID = request.getParameter("countryID");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/alterCountry.jsp";
            countryAlterList = financeBP.processCountryAlterList(countryID);
            request.setAttribute("countryAlterLists", countryAlterList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to accountTypealterList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveAlterCountry(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList CountryList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> Country ";
        String pageTitle = "Manage Country Master";
        request.setAttribute("pageTitle", pageTitle);
        String countryID = request.getParameter("countryID");
        //////System.out.println("countryID 1======> " + countryID);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (financeCommand.getCountryName() != null && financeCommand.getCountryName() != "") {
                financeTO.setCountryName(financeCommand.getCountryName());
            }
            if (financeCommand.getCountryCode() != null && financeCommand.getCountryCode() != "") {
                financeTO.setCountryCode(financeCommand.getCountryCode());
            }

            path = "content/Finance/financeCountry.jsp";
            status = financeBP.saveAlterCountry(financeTO, countryID, userId);
            CountryList = financeBP.getCountryList();
            request.setAttribute("CountryLists", CountryList);

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Country Modified Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Alter Tax Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle finance state.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView financeState(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList StateList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View State Master";
        menuPath = "Finance  >> State Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/financeState.jsp";
            StateList = financeBP.getStateList();
            request.setAttribute("StateLists", StateList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to State List --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView addStatePage(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        ArrayList CountryList = new ArrayList();
        financeCommand = command;
        String menuPath = "";
        menuPath = "Finance >> State >> Add State ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Add State";
            request.setAttribute("pageTitle", pageTitle);
            path = "content/Finance/newState.jsp";
            CountryList = financeBP.getCountryList();
            request.setAttribute("CountryLists", CountryList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Ledger --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveNewState(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList StateList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> State  ";
        String pageTitle = "Manage State";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (financeCommand.getStateName() != null && financeCommand.getStateName() != "") {
                financeTO.setStateName(financeCommand.getStateName());
            }
            if (financeCommand.getStateCode() != null && financeCommand.getStateCode() != "") {
                financeTO.setStateCode(financeCommand.getStateCode());
            }
            if (financeCommand.getCountryID() != null && financeCommand.getCountryID() != "") {
                financeTO.setCountryID(financeCommand.getCountryID());
            }
            if (financeCommand.getDescription() != null && financeCommand.getDescription() != "") {
                financeTO.setDescription(financeCommand.getDescription());
            }
            path = "content/Finance/financeState.jsp";
            status = financeBP.saveNewState(financeTO, userId);
            //////System.out.println("status = " + status);

            StateList = financeBP.getStateList();
            request.setAttribute("StateLists", StateList);

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " State Added Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView alterStateDetail(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList statealterList = new ArrayList();
        ArrayList CountryList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Alter State";
        menuPath = "Finance  >> Alter  State ";
        request.setAttribute("pageTitle", pageTitle);
        String stateID = request.getParameter("stateID");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/alterState.jsp";
            statealterList = financeBP.processStatealterList(stateID);
            request.setAttribute("statealterList", statealterList);

            CountryList = financeBP.getCountryList();
            request.setAttribute("CountryLists", CountryList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewLedger --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveAlterState(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList StateList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> State Master  ";
        String pageTitle = "Manage State Master";
        request.setAttribute("pageTitle", pageTitle);
        String stateID = request.getParameter("stateID");
        //////System.out.println("stateID 1======> " + stateID);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (financeCommand.getStateName() != null && financeCommand.getStateName() != "") {
                financeTO.setStateName(financeCommand.getStateName());
            }
            if (financeCommand.getStateCode() != null && financeCommand.getStateCode() != "") {
                financeTO.setStateCode(financeCommand.getStateCode());
            }
            if (financeCommand.getCountryID() != null && financeCommand.getCountryID() != "") {
                financeTO.setCountryID(financeCommand.getCountryID());
            }
            if (financeCommand.getDescription() != null && financeCommand.getDescription() != "") {
                financeTO.setDescription(financeCommand.getDescription());
            }
            path = "content/Finance/financeState.jsp";
            status = financeBP.saveAlterState(financeTO, stateID, userId);
            StateList = financeBP.getStateList();
            request.setAttribute("StateLists", StateList);

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " State Modified Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to modified state Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle finance District.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView financeDistrict(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList DistrictList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View District Master";
        menuPath = "Finance  >> District Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/financeDistrict.jsp";
            DistrictList = financeBP.getDistrictList();
            request.setAttribute("DistrictLists", DistrictList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to District List --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);

    }

    public ModelAndView addDistrictPage(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        ArrayList DistrictList = new ArrayList();
        ArrayList CountryList = new ArrayList();
        ArrayList StateList = new ArrayList();
        financeCommand = command;
        String menuPath = "";
        menuPath = "Finance >> District >> Add District ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Add District";
            request.setAttribute("pageTitle", pageTitle);
            path = "content/Finance/newDistrict.jsp";

            CountryList = financeBP.getCountryList();
            request.setAttribute("CountryLists", CountryList);

            StateList = financeBP.getStateList();
            request.setAttribute("StateLists", StateList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view district --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveNewDistrict(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList DistrictList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> District";
        String pageTitle = "Manage District";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (financeCommand.getDistrictName() != null && financeCommand.getDistrictName() != "") {
                financeTO.setDistrictName(financeCommand.getDistrictName());
            }
            if (financeCommand.getDistrictCode() != null && financeCommand.getDistrictCode() != "") {
                financeTO.setDistrictCode(financeCommand.getDistrictCode());
            }
            if (financeCommand.getCountryID() != null && financeCommand.getCountryID() != "") {
                financeTO.setCountryID(financeCommand.getCountryID());
            }
            if (financeCommand.getStateID() != null && financeCommand.getStateID() != "") {
                financeTO.setStateID(financeCommand.getStateID());
            }
            if (financeCommand.getDescription() != null && financeCommand.getDescription() != "") {
                financeTO.setDescription(financeCommand.getDescription());
            }
            path = "content/Finance/financeDistrict.jsp";
            status = financeBP.saveNewDistrict(financeTO, userId);
            //////System.out.println("status = " + status);

            DistrictList = financeBP.getDistrictList();
            request.setAttribute("DistrictLists", DistrictList);

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " District Added Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView alterDistrictDetail(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList districtalterList = new ArrayList();
        ArrayList CountryList = new ArrayList();
        ArrayList StateList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Alter District";
        menuPath = "Finance  >> Alter  District ";
        request.setAttribute("pageTitle", pageTitle);
        String districtID = request.getParameter("districtID");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/alterDistrict.jsp";
            districtalterList = financeBP.processDistrictalterList(districtID);
            request.setAttribute("districtalterList", districtalterList);

            CountryList = financeBP.getCountryList();
            request.setAttribute("CountryLists", CountryList);
            //////System.out.println("CountryList.size() = " + CountryList.size());

            StateList = financeBP.getStateList();
            request.setAttribute("StateLists", StateList);
            //////System.out.println("StateList.size() = " + StateList.size());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view district --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView updateDistrict(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList DistrictList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> District  ";
        String pageTitle = "Manage District";
        request.setAttribute("pageTitle", pageTitle);
        String districtID = (String) request.getAttribute("districtID");
        //////System.out.println("districtID................... = " + districtID);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (financeCommand.getDistrictName() != null && financeCommand.getDistrictName() != "") {
                financeTO.setDistrictName(financeCommand.getDistrictName());
                //////System.out.println("financeCommand.getDistrictName() = " + financeCommand.getDistrictName());
            }
            if (financeCommand.getDistrictCode() != null && financeCommand.getDistrictCode() != "") {
                financeTO.setDistrictCode(financeCommand.getDistrictCode());
                //////System.out.println("financeCommand.getDistrictCode() = " + financeCommand.getDistrictCode());
            }
            if (financeCommand.getCountryID() != null && financeCommand.getCountryID() != "") {
                financeTO.setCountryID(financeCommand.getCountryID());
                //////System.out.println("financeCommand.getCountryID() = " + financeCommand.getCountryID());
            }
            if (financeCommand.getStateID() != null && financeCommand.getStateID() != "") {
                financeTO.setStateID(financeCommand.getStateID());
                //////System.out.println("financeCommand.getStateID() = " + financeCommand.getStateID());
            }
            if (financeCommand.getDistrictID() != null && financeCommand.getDistrictID() != "") {
                financeTO.setDistrictID(financeCommand.getDistrictID());
                //////System.out.println("financeCommand.getDistrictID() = " + financeCommand.getDistrictID());
            }
            if (financeCommand.getDescription() != null && financeCommand.getDescription() != "") {
                financeTO.setDescription(financeCommand.getDescription());
                //////System.out.println("financeCommand.getDescription() = " + financeCommand.getDescription());
            }
            path = "content/Finance/financeDistrict.jsp";
            status = financeBP.saveAlterDistrict(financeTO, districtID, userId);
            //////System.out.println("status = " + status);
            DistrictList = financeBP.getDistrictList();
            request.setAttribute("DistrictLists", DistrictList);

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " District Modified Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle finance Contra Entry.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView contraEntry(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList paymentLedgerList = new ArrayList();
//        ArrayList bankLedgerList = new ArrayList();
        ArrayList contraEntryCreditList = new ArrayList();
        ArrayList contraEntryDebitList = new ArrayList();
        ArrayList contraCodeList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Contra Entry";
        menuPath = "Finance  >> Contra Entry";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/contraEntry.jsp";
            paymentLedgerList = financeBP.getContraPaymentLedgerList();
//            paymentLedgerList = financeBP.getPaymentLedgerList();
            System.out.println("paymentLedgerList.lenth() = " + paymentLedgerList.size());
            request.setAttribute("paymentLedgerList", paymentLedgerList);
            contraCodeList = financeBP.getContraCodeList();
            request.setAttribute("contraCodeList", contraCodeList);
            contraEntryCreditList = financeBP.getContraEntryCreditList();
            request.setAttribute("contraEntryCreditList", contraEntryCreditList);
            contraEntryDebitList = financeBP.getContraEntryDebitList();
            request.setAttribute("contraEntryDebitList", contraEntryDebitList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to contraEntry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle finance Print Contra Entry.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView printContraEntry(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankLedgerList = new ArrayList();
        ArrayList contraEntryCreditList = new ArrayList();
        ArrayList contraEntryDebitList = new ArrayList();
        ArrayList contraEntry = new ArrayList();
        ArrayList contraCodeList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Journal Entry";
        menuPath = "Finance  >> Journal Entry";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/contraEntryPrint.jsp";
            bankLedgerList = financeBP.getBankLedgerList();
            request.setAttribute("bankLedgerList", bankLedgerList);
            contraCodeList = financeBP.getContraCodeList();
            request.setAttribute("contraCodeList", contraCodeList);
            contraEntryCreditList = financeBP.getContraEntryCreditList();
            request.setAttribute("contraEntryCreditList", contraEntryCreditList);
            contraEntryDebitList = financeBP.getContraEntryDebitList();
            request.setAttribute("contraEntryDebitList", contraEntryDebitList);

            request.setAttribute("voucherNo", request.getParameter("voucherNo"));
            String voucherCode = "";
            if (request.getParameter("voucherCode") != null && !"".equals(request.getParameter("voucherCode"))) {
                voucherCode = request.getParameter("voucherCode");
                request.setAttribute("voucherCode", voucherCode);
                contraEntry = financeBP.getContraEntry(voucherCode);
                request.setAttribute("contraEntry", contraEntry);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView insertContraEntry(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Finance >> Contra Entry ";
        String pageTitle = "Manage ContraEntry";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList contraEntryCreditList = new ArrayList();
        ArrayList contraEntryDebitList = new ArrayList();
        ArrayList contraCodeList = new ArrayList();
        ArrayList paymentLedgerList = new ArrayList();
        try {
            int status = 0;
            path = "content/Finance/contraEntry.jsp";
            int userId = (Integer) session.getAttribute("userId");
            String fromDate = request.getParameter("fromDate");
//            String date = "";
//            String[] temp = null;
//            if (date1 != "") {
//                temp = date1.split("-");
//                String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
//                date = sDate;
//            }
            String[] bankid1Val = request.getParameterValues("bankid1");
            String[] amountVal = request.getParameterValues("amount");
            String[] accTypeVal = request.getParameterValues("accType");
            String[] narrationVal = request.getParameterValues("narration");
            System.out.println("bankid1 = " + bankid1Val.length);
            String voucherCode = request.getParameter("voucherCode");
            String voucherIdEdit = "";
            if (request.getParameter("voucherIdEdit") != null) {
                voucherIdEdit = request.getParameter("voucherIdEdit");
            }
            System.out.println("voucherIdEdit" + voucherIdEdit);

            status = financeBP.insertContraEntry(userId, fromDate, bankid1Val, amountVal, accTypeVal, narrationVal, voucherIdEdit, voucherCode);

//            System.out.println("----1-----");
//            
//            String bankid1Values = null;
//            String amountValues = null;
//            String accTypeValues = null;
//            String narrationValues = null;
//            int detailCode = 0;
//            String voucherCode = "";
//            voucherCode = financeBP.getVoucherCode("contra");
//            for (int i = 0; i < bankid1Val.length; i++) {
//                detailCode++;
//                bankid1Values = bankid1Val[i];
//                amountValues = amountVal[i];
//                accTypeValues = accTypeVal[i];
//                narrationValues = narrationVal[i];
//                status = financeBP.insertContraEntry(detailCode, userId, date, bankid1Values, amountValues, accTypeValues, narrationValues, voucherCode);
//            }
            System.out.println("----1-----");

            if (status > 0) {
                System.out.println("-----2----");
                System.out.println("status = " + status);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Contra Entry Added Successfully");
                contraCodeList = financeBP.getContraCodeList();
                request.setAttribute("contraCodeList", contraCodeList);
                contraEntryCreditList = financeBP.getContraEntryCreditList();
                request.setAttribute("contraEntryCreditList", contraEntryCreditList);
                contraEntryDebitList = financeBP.getContraEntryDebitList();
                request.setAttribute("contraEntryDebitList", contraEntryDebitList);
//                bankLedgerList = financeBP.getBankLedgerList();
//                request.setAttribute("bankLedgerList", bankLedgerList);
                paymentLedgerList = financeBP.getContraPaymentLedgerList();
                System.out.println("paymentLedgerList.lenth() = " + paymentLedgerList.size());
                request.setAttribute("paymentLedgerList", paymentLedgerList);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Cust Contract Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle Credit Note.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView creditNote(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankLedgerList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View CreditNote";
        menuPath = "Finance  >> CreditNote";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/creditNote.jsp";
            bankLedgerList = financeBP.getBankLedgerList();
            request.setAttribute("bankLedgerList", bankLedgerList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to contraEntry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView insertCreditNote(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Finance >>Credit Note >> Manage Credit Note";
        String pageTitle = "Manage Credit Note";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            int status = 0;
            path = "content/Finance/creditNote.jsp";
            int userId = (Integer) session.getAttribute("userId");
            String date = request.getParameter("date");
            String[] bankid1Val = request.getParameterValues("ledger");
            String[] amountVal = request.getParameterValues("amount");
//          String[] accTypeVal = request.getParameterValues("accType");
            String[] narrationVal = request.getParameterValues("narration");
            //////System.out.println("bankid1 = " + bankid1Val.length);

            String bankid1Values = null;
            String amountValues = null;
            String accTypeValues = null;
            String narrationValues = null;
            int detailCode = 0;
            for (int i = 0; i < bankid1Val.length; i++) {
                detailCode++;
                bankid1Values = bankid1Val[i];
                amountValues = amountVal[i];
//                accTypeValues = accTypeVal[i];
                narrationValues = narrationVal[i];
                status = financeBP.insertCreditNote(detailCode, userId, date, bankid1Values, amountValues, accTypeValues, narrationValues);
            }
            //////System.out.println("-----1------");

            if (status > 0) {
                //////System.out.println("-----2-----");
                //////System.out.println("status = " + status);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Credit Note Added Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Credit Note Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle Debit Note.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView debitNote(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankLedgerList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View DebitNote";
        menuPath = "Finance  >> DebitNote";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/debitNote.jsp";
            bankLedgerList = financeBP.getBankLedgerList();
            request.setAttribute("bankLedgerList", bankLedgerList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to DebitNote List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView insertDebitNote(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Finance >>Debit Note";
        String pageTitle = "Manage Debit Note";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            int status = 0;
            path = "content/Finance/debitNote.jsp";
            int userId = (Integer) session.getAttribute("userId");
            String date = request.getParameter("date");
            String[] bankid1Val = request.getParameterValues("ledger");
            String[] amountVal = request.getParameterValues("amount");
//          String[] accTypeVal = request.getParameterValues("accType");
            String[] narrationVal = request.getParameterValues("narration");
            //////System.out.println("bankid1 = " + bankid1Val.length);

            String bankid1Values = null;
            String amountValues = null;
            String accTypeValues = null;
            String narrationValues = null;
            int detailCode = 0;
            for (int i = 0; i < bankid1Val.length; i++) {
                detailCode++;
                bankid1Values = bankid1Val[i];
                amountValues = amountVal[i];
//                accTypeValues = accTypeVal[i];
                narrationValues = narrationVal[i];
                status = financeBP.insertDebitNote(detailCode, userId, date, bankid1Values, amountValues, accTypeValues, narrationValues);
            }
            //////System.out.println("1");

            if (status > 0) {
                //////System.out.println("2");
                //////System.out.println("status = " + status);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Debit Note Added Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Debit Note Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle finance Journal Entry.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView journalEntry(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList journalLedgerList = new ArrayList();
        ArrayList journalEntryCreditList = new ArrayList();
        ArrayList journalEntryDebitList = new ArrayList();
        ArrayList journalCodeList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Journal Entry";
        menuPath = "Finance  >> Journal Entry";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/journalEntry.jsp";
            journalLedgerList = financeBP.getJournalLedgerList();
            System.out.println("journalLedgerList.lenth() = " + journalLedgerList.size());
            request.setAttribute("journalLedgerList", journalLedgerList);
            journalCodeList = financeBP.getJournalCodeList();
            request.setAttribute("journalCodeList", journalCodeList);
            journalEntryCreditList = financeBP.getJournalEntryCreditList();
            request.setAttribute("journalEntryCreditList", journalEntryCreditList);
            journalEntryDebitList = financeBP.getJournalEntryDebitList();
            request.setAttribute("journalEntryDebitList", journalEntryDebitList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle finance Print Journal Entry.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView printJournalEntry(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList journalLedgerList = new ArrayList();
        ArrayList journalEntryCreditList = new ArrayList();
        ArrayList journalEntryDebitList = new ArrayList();
        ArrayList journalEntry = new ArrayList();
        ArrayList journalCodeList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Journal Entry";
        menuPath = "Finance  >> Journal Entry";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/journalEntryPrint.jsp";
            journalLedgerList = financeBP.getJournalLedgerList();
            System.out.println("journalLedgerList.lenth() = " + journalLedgerList.size());
            request.setAttribute("journalLedgerList", journalLedgerList);
            journalCodeList = financeBP.getJournalCodeList();
            request.setAttribute("journalCodeList", journalCodeList);
            journalEntryCreditList = financeBP.getJournalEntryCreditList();
            request.setAttribute("journalEntryCreditList", journalEntryCreditList);
            journalEntryDebitList = financeBP.getJournalEntryDebitList();
            request.setAttribute("journalEntryDebitList", journalEntryDebitList);

            request.setAttribute("voucherNo", request.getParameter("voucherNo"));
            String voucherCode = "";
            if (request.getParameter("voucherCode") != null && !"".equals(request.getParameter("voucherCode"))) {
                voucherCode = request.getParameter("voucherCode");
                request.setAttribute("voucherCode", voucherCode);
                journalEntry = financeBP.getJournalEntry(voucherCode);
                request.setAttribute("journalEntry", journalEntry);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView insertJournalEntry(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Finance >> JournalEntry ";
        String pageTitle = "Manage JournalEntry";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList journalEntryCreditList = new ArrayList();
        ArrayList journalEntryDebitList = new ArrayList();
        ArrayList journalCodeList = new ArrayList();
        ArrayList journalLedgerList = new ArrayList();
        try {
            int status = 0;
            path = "content/Finance/journalEntry.jsp";
            int userId = (Integer) session.getAttribute("userId");
            String fromDate = request.getParameter("fromDate");
//            String date = "";
//            String[] temp = null;
//            if (date1 != "") {
//                temp = date1.split("-");
//                String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
//                date = sDate;
//            }

            String[] bankid1Val = request.getParameterValues("bankId");
            String[] amountVal = request.getParameterValues("amount");
            String[] accTypeVal = request.getParameterValues("accType");
            String[] narrationVal = request.getParameterValues("narration");
            String voucherCode = request.getParameter("voucherCode");
            String voucherIdEdit = "";
            if (request.getParameter("voucherIdEdit") != null) {
                voucherIdEdit = request.getParameter("voucherIdEdit");
            }
            System.out.println("voucherIdEdit" + voucherIdEdit);

            status = financeBP.insertJournalEntry(userId, fromDate, bankid1Val, amountVal, accTypeVal, narrationVal, voucherIdEdit, voucherCode);

//            String bankId = null;
//            String amount = null;
//            String accType = null;
//            String narration = null;
//            int detailCode = 0;
//            String voucherCode = "";
//           // voucherCode = financeBP.getVoucherCode("journal");
//            for (int i = 0; i < bankIds.length; i++) {
//                detailCode++;
//                bankId = bankIds[i];
//                amount = amounts[i];
//                accType = accTypes[i];
//                narration = narrations[i];
//                status = financeBP.insertJournalEntry(detailCode, userId, date, bankId, amount, accType, narration, voucherCode);
//            }
//            System.out.println("----1-----");
            if (status > 0) {
                System.out.println("-----2----");
                System.out.println("status = " + status);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "JournalEntry Entry Added Successfully");
                journalCodeList = financeBP.getJournalCodeList();
                request.setAttribute("journalCodeList", journalCodeList);
                journalEntryCreditList = financeBP.getJournalEntryCreditList();
                request.setAttribute("journalEntryCreditList", journalEntryCreditList);
                journalEntryDebitList = financeBP.getJournalEntryDebitList();
                request.setAttribute("journalEntryDebitList", journalEntryDebitList);
                System.out.println("totalCreditAmt ++:" + "");
                journalLedgerList = financeBP.getJournalLedgerList();
                System.out.println("journalLedgerList.lenth() = " + journalLedgerList.size());
                request.setAttribute("journalLedgerList", journalLedgerList);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert JournalEntry Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle VAT Master.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView vatMaster(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList vatList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View vatMaster";
        menuPath = "Finance  >> vatMaster";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/vatMaster.jsp";
            vatList = financeBP.getDistrictList();
            request.setAttribute("vatList", vatList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to vatList --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle finance payment Entry.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView paymentEntry(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList paymentLedgerList = new ArrayList();
        ArrayList cashPaymentList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Payment Entry";
        menuPath = "Finance  >> Payment Entry";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/paymentEntry.jsp";
            paymentLedgerList = financeBP.getPaymentLedgerList();
            System.out.println("paymentLedgerList.lenth() = " + paymentLedgerList.size());
            request.setAttribute("paymentLedgerList", paymentLedgerList);
            cashPaymentList = financeBP.getCashPaymentList();
            request.setAttribute("cashPaymentList", cashPaymentList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle print cash payment Entry.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView printCashPaymentEntry(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList paymentLedgerList = new ArrayList();
        ArrayList cashPaymentList = new ArrayList();
        ArrayList cashPaymentEntry = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Payment Entry";
        menuPath = "Finance  >> Payment Entry";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/paymentEntryPrint.jsp";
            paymentLedgerList = financeBP.getPaymentLedgerList();
            System.out.println("paymentLedgerList.lenth() = " + paymentLedgerList.size());
            request.setAttribute("paymentLedgerList", paymentLedgerList);
            cashPaymentList = financeBP.getCashPaymentList();
            request.setAttribute("cashPaymentList", cashPaymentList);
            request.setAttribute("voucherNo", request.getParameter("voucherNo"));

            String voucherCode = "";
            if (request.getParameter("voucherCode") != null && !"".equals(request.getParameter("voucherCode"))) {
                voucherCode = request.getParameter("voucherCode");
                request.setAttribute("voucherCode", voucherCode);
                cashPaymentEntry = financeBP.getCashPaymentEntry(voucherCode);
                request.setAttribute("cashPaymentEntry", cashPaymentEntry);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView insertPaymentEntry(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        financeCommand = command;
        menuPath = "Finance >> PaymentEntry ";
        String pageTitle = "Manage paymentEntry";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList cashPaymentList = new ArrayList();
        ArrayList paymentLedgerList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        try {
            int status = 0;
            path = "content/Finance/paymentEntry.jsp";
            int userId = (Integer) session.getAttribute("userId");
            String fromDate = request.getParameter("fromDate");
            // String date = "";
            String[] temp = null;
//            if (date1 != "") {
//                temp = date1.split("-");
//                String sDate = temp[2]+"-"+temp[1]+"-"+temp[0];
//                date = sDate;
//            }
            String totalCreditAmt = request.getParameter("totalCreditAmt");
            System.out.println("totalCreditAmt = " + totalCreditAmt);
            String[] bankid1Val = request.getParameterValues("bankid1");
            String[] amountVal = request.getParameterValues("amount");
            String[] accTypeVal = request.getParameterValues("accType");
            String[] narrationVal = request.getParameterValues("narration");
            System.out.println("bankid1 = " + bankid1Val.length);
            //edit
//            if (financeCommand.getVoucherCode() != null && financeCommand.getVoucherCode() != "") {
//                financeTO.setVoucherCode(financeCommand.getVoucherCode());
//            }
            String voucherCode = request.getParameter("voucherCode");
            String voucherIdEdit = "";
            if (request.getParameter("voucherIdEdit") != null) {
                voucherIdEdit = request.getParameter("voucherIdEdit");
            }
            System.out.println("voucherIdEdit" + voucherIdEdit);
            //cost center
            String activeInd = "";
            String vehicleId = "";
            String costCenterNarration = "";
            activeInd = request.getParameter("activeInd");
            vehicleId = request.getParameter("vehicleId");
            costCenterNarration = request.getParameter("costCenterNarration");
//
            status = financeBP.insertPaymentEntry(financeTO, totalCreditAmt, userId, fromDate, bankid1Val, amountVal, accTypeVal,
                    narrationVal, voucherIdEdit, voucherCode, activeInd, vehicleId, costCenterNarration);

            System.out.println("----1-----");

            if (status > 0) {
                System.out.println("-----2----");
                System.out.println("status = " + status);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Payment Entry Added Successfully");
                cashPaymentList = financeBP.getCashPaymentList();
                request.setAttribute("cashPaymentList", cashPaymentList);
                paymentLedgerList = financeBP.getPaymentLedgerList();
                System.out.println("paymentLedgerList.lenth() = " + paymentLedgerList.size());
                request.setAttribute("paymentLedgerList", paymentLedgerList);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert JournalEntry Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle finance bank payment Entry.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView bankPaymentEntry(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankPaymentLedgerList = new ArrayList();
        ArrayList bankLedgerList = new ArrayList();
        ArrayList bankPaymentList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Bank Payment Entry";
        menuPath = "Finance  >> Bank Payment Entry";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/bankPaymentEntry.jsp";

            bankLedgerList = financeBP.getBankLedgerList();
            System.out.println("bankLedgerList.lenth() = " + bankLedgerList.size());
            request.setAttribute("bankLedgerList", bankLedgerList);

            bankPaymentLedgerList = financeBP.getBankPaymentLedgerList();
            System.out.println("bankPaymentLedgerList = " + bankPaymentLedgerList.size());
            request.setAttribute("bankPaymentLedgerList", bankPaymentLedgerList);

            bankPaymentList = financeBP.getBankPaymentList();
            System.out.println("bankPaymentList = " + bankPaymentList.size());
            request.setAttribute("bankPaymentList", bankPaymentList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle print bank payment Entry.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView printBankPaymentEntry(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankPaymentLedgerList = new ArrayList();
        ArrayList bankLedgerList = new ArrayList();
        ArrayList bankPaymentList = new ArrayList();
        ArrayList bankPaymentEntry = new ArrayList();

        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Bank Payment Entry";
        menuPath = "Finance  >> Bank Payment Entry";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/bankPaymentEntryPrint.jsp";

            bankLedgerList = financeBP.getBankLedgerList();
            System.out.println("bankLedgerList.lenth() = " + bankLedgerList.size());
            request.setAttribute("bankLedgerList", bankLedgerList);

            bankPaymentLedgerList = financeBP.getBankPaymentLedgerList();
            System.out.println("bankPaymentLedgerList = " + bankPaymentLedgerList.size());
            request.setAttribute("bankPaymentLedgerList", bankPaymentLedgerList);

            bankPaymentList = financeBP.getBankPaymentList();
            System.out.println("bankPaymentList = " + bankPaymentList.size());
            request.setAttribute("bankPaymentList", bankPaymentList);
            request.setAttribute("voucherNo", request.getParameter("voucherNo"));

            String voucherCode = "";
            if (request.getParameter("voucherCode") != null && !"".equals(request.getParameter("voucherCode"))) {
                voucherCode = request.getParameter("voucherCode");
                request.setAttribute("voucherCode", voucherCode);
                bankPaymentEntry = financeBP.getBankPaymentEntryPrint(voucherCode);
                request.setAttribute("bankPaymentEntry", bankPaymentEntry);

            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView insertBankPaymentEntry(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Finance >> Bank Payment Entry ";
        String pageTitle = "Manage Bank payment Entry";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList bankPaymentList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        try {
            int status = 0;
            path = "content/Finance/bankPaymentEntry.jsp";
            int userId = (Integer) session.getAttribute("userId");
            String fromDate = request.getParameter("fromDate");
//            String date = "";
//            String[] temp = null;
//            if (date1 != "") {
//                temp = date1.split("-");
//                String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
//                date = sDate;
//            }

            String BankHead = request.getParameter("BankHead");
            System.out.println("BankHead= " + BankHead);
            String totalCreditAmt = request.getParameter("totalCreditAmt");
            System.out.println("totalCreditAmt = " + totalCreditAmt);
            String chequeNo = request.getParameter("chequeNo");
            System.out.println("chequeNo = " + chequeNo);
            String chequeDate = request.getParameter("chequeDate");
//            String chequeDate = "";
//            if (chequeDate1 != "") {
//                temp = chequeDate1.split("-");
//                String cDate = temp[2] + "-" + temp[1] + "-" + temp[0];
//                chequeDate = cDate;
//                System.out.println("chequeDate = " + chequeDate);
//            }

            String[] bankid1Val = request.getParameterValues("bankid1");
            String[] amountVal = request.getParameterValues("amount");
            String[] accTypeVal = request.getParameterValues("accType");
            String[] narrationVal = request.getParameterValues("narration");
            System.out.println("bankid1 = " + bankid1Val.length);
            String voucherCode = request.getParameter("voucherCode");
            String voucherIdEdit = "";
            if (request.getParameter("voucherIdEdit") != null) {
                voucherIdEdit = request.getParameter("voucherIdEdit");
            }
            System.out.println("voucherIdEdit" + voucherIdEdit);
            //cost center
            String activeInd = "";
            String vehicleId = "";
            String costCenterNarration = "";
            activeInd = request.getParameter("activeInd");
            vehicleId = request.getParameter("vehicleId");
            costCenterNarration = request.getParameter("costCenterNarration");

            status = financeBP.insertBankPaymentEntry(financeTO, chequeDate, chequeNo, BankHead, totalCreditAmt, userId, fromDate,
                    bankid1Val, amountVal, accTypeVal, narrationVal, voucherIdEdit, voucherCode, activeInd, vehicleId, costCenterNarration);

//            String bankid1Values = null;
//            String amountValues = null;
//            String accTypeValues = null;
//            String narrationValues = null;
//            int detailCode = 0;
//            int cashDetailCode = (bankid1Val.length + 1);
//            System.out.println("bankid1Val.length = " + (bankid1Val.length + 1));
//            for (int i = 0; i < bankid1Val.length; i++) {
//                detailCode++;
//                bankid1Values = bankid1Val[i];
//                amountValues = amountVal[i];
//                accTypeValues = accTypeVal[i];
//                narrationValues = narrationVal[i];
//                status = financeBP.insertBankPaymentEntry(chequeDate, chequeNo, BankHead, cashDetailCode, totalCreditAmt, detailCode, userId, date, bankid1Values, amountValues, accTypeValues, narrationValues);
//            }
            System.out.println("----1-----");

            if (status > 0) {
                System.out.println("-----2----");
                System.out.println("status = " + status);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Payment Entry Added Successfully");
                bankPaymentList = financeBP.getBankPaymentList();
                System.out.println("bankPaymentList = " + bankPaymentList.size());
                request.setAttribute("bankPaymentList", bankPaymentList);
                ArrayList bankPaymentLedgerList = new ArrayList();
                ArrayList bankLedgerList = new ArrayList();
                bankPaymentLedgerList = financeBP.getBankPaymentLedgerList();
                System.out.println("bankPaymentLedgerList = " + bankPaymentLedgerList.size());
                request.setAttribute("bankPaymentLedgerList", bankPaymentLedgerList);
                bankLedgerList = financeBP.getBankLedgerList();
                System.out.println("bankLedgerList.lenth() = " + bankLedgerList.size());
                request.setAttribute("bankLedgerList", bankLedgerList);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert JournalEntry Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle finance Receipt Entry.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView receiptEntry(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList receiptLedgerList = new ArrayList();
        ArrayList cashReceiptList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Receipt Entry";
        menuPath = "Finance  >> Receipt Entry";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/receiptEntry.jsp";

            cashReceiptList = financeBP.getCashReceiptList();
            System.out.println("cashReceiptList.lenth() = " + cashReceiptList.size());
            request.setAttribute("cashReceiptList", cashReceiptList);
            receiptLedgerList = financeBP.receiptEntry();
            System.out.println("receiptLedgerList.lenth() = " + receiptLedgerList.size());
            request.setAttribute("receiptLedgerList", receiptLedgerList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view receiptEntry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle print cash Receipt Entry.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView printCashReceiptEntry(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList receiptLedgerList = new ArrayList();
        ArrayList cashReceiptList = new ArrayList();
        ArrayList cashReceiptEntry = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Receipt Entry";
        menuPath = "Finance  >> Receipt Entry";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/cashReceiptEntryPrint.jsp";
            receiptLedgerList = financeBP.receiptEntry();
            System.out.println("receiptLedgerList.lenth() = " + receiptLedgerList.size());
            request.setAttribute("receiptLedgerList", receiptLedgerList);
            cashReceiptList = financeBP.getCashReceiptList();
            System.out.println("cashReceiptList.lenth() = " + cashReceiptList.size());
            request.setAttribute("cashReceiptList", cashReceiptList);

            request.setAttribute("voucherNo", request.getParameter("voucherNo"));
            String voucherCode = "";
            if (request.getParameter("voucherCode") != null && !"".equals(request.getParameter("voucherCode"))) {
                voucherCode = request.getParameter("voucherCode");
                request.setAttribute("voucherCode", voucherCode);
                cashReceiptEntry = financeBP.getCashReceiptEntry(voucherCode);
                request.setAttribute("cashReceiptEntry", cashReceiptEntry);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view receiptEntry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView insertReceiptEntry(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Finance >> ReceiptEntry ";
        String pageTitle = "Manage Receipt Entry";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList cashReceiptList = new ArrayList();
        try {
            int status = 0;
            path = "content/Finance/receiptEntry.jsp";
            int userId = (Integer) session.getAttribute("userId");
            String fromDate = request.getParameter("fromDate");
//            String date1 = request.getParameter("date");
            String date = "";
            String[] temp = null;
//            if (date1 != "") {
//                temp = date1.split("-");
//                String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
//                date = sDate;
//            }
            String totalDebitAmt = request.getParameter("totalDebitAmt");
            System.out.println("totalDebitAmt = " + totalDebitAmt);
            String[] bankid1Val = request.getParameterValues("bankid1");
            String[] amountVal = request.getParameterValues("amount");
            String[] accTypeVal = request.getParameterValues("accType");
            String[] narrationVal = request.getParameterValues("narration");
            System.out.println("bankid1 = " + bankid1Val.length);

            String voucherCode = request.getParameter("voucherCode");
            String voucherIdEdit = "";
            if (request.getParameter("voucherIdEdit") != null) {
                voucherIdEdit = request.getParameter("voucherIdEdit");
            }
            System.out.println("voucherIdEdit" + voucherIdEdit);
            status = financeBP.insertReceiptEntry(totalDebitAmt, userId, fromDate, bankid1Val, amountVal, accTypeVal, narrationVal, voucherIdEdit, voucherCode);

            if (status > 0) {
                System.out.println("-----2----");
                System.out.println("status = " + status);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Receipt Entry Added Successfully");
                cashReceiptList = financeBP.getCashReceiptList();
                System.out.println("cashReceiptList.lenth() = " + cashReceiptList.size());
                request.setAttribute("cashReceiptList", cashReceiptList);
                ArrayList receiptLedgerList = new ArrayList();
                receiptLedgerList = financeBP.receiptEntry();
                System.out.println("receiptLedgerList.lenth() = " + receiptLedgerList.size());
                request.setAttribute("receiptLedgerList", receiptLedgerList);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert JournalEntry Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle finance bank Receipt Entry.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView bankReceiptEntry(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankPaymentLedgerList = new ArrayList();
        ArrayList bankLedgerList = new ArrayList();
        ArrayList bankReceiptList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Bank Receipt Entry";
        menuPath = "Finance  >> Bank Receipt Entry";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/bankReceiptEntry.jsp";

            bankLedgerList = financeBP.getBankLedgerList();
            System.out.println("bankLedgerList.lenth() = " + bankLedgerList.size());
            request.setAttribute("bankLedgerList", bankLedgerList);

            bankPaymentLedgerList = financeBP.getBankPaymentLedgerList();
            System.out.println("bankPaymentLedgerList = " + bankPaymentLedgerList.size());
            request.setAttribute("bankPaymentLedgerList", bankPaymentLedgerList);

            bankReceiptList = financeBP.getBankReceiptList();
            System.out.println("bankReceiptList = " + bankReceiptList.size());
            request.setAttribute("bankReceiptList", bankReceiptList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle print bank Receipt Entry.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView printBankReceiptEntry(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankPaymentLedgerList = new ArrayList();
        ArrayList bankLedgerList = new ArrayList();
        ArrayList bankReceiptList = new ArrayList();
        ArrayList bankReceiptEntry = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Bank Receipt Entry";
        menuPath = "Finance  >> Bank Receipt Entry";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/bankReceiptEntryPrint.jsp";

            bankLedgerList = financeBP.getBankLedgerList();
            System.out.println("bankLedgerList.lenth() = " + bankLedgerList.size());
            request.setAttribute("bankLedgerList", bankLedgerList);

            bankPaymentLedgerList = financeBP.getBankPaymentLedgerList();
            System.out.println("bankPaymentLedgerList = " + bankPaymentLedgerList.size());
            request.setAttribute("bankPaymentLedgerList", bankPaymentLedgerList);

            bankReceiptList = financeBP.getBankReceiptList();
            System.out.println("bankReceiptList = " + bankReceiptList.size());
            request.setAttribute("bankReceiptList", bankReceiptList);
            request.setAttribute("voucherNo", request.getParameter("voucherNo"));

            String voucherCode = "";
            if (request.getParameter("voucherCode") != null && !"".equals(request.getParameter("voucherCode"))) {
                voucherCode = request.getParameter("voucherCode");
                request.setAttribute("voucherCode", voucherCode);
                bankReceiptEntry = financeBP.getBankReceiptEntry(voucherCode);
                request.setAttribute("bankReceiptEntry", bankReceiptEntry);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView insertBankReceiptEntry(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Finance >> Bank Receipt Entry ";
        String pageTitle = "Manage Bank Receipt Entry";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList bankReceiptList = new ArrayList();
        try {
            int status = 0;
            path = "content/Finance/bankReceiptEntry.jsp";
            int userId = (Integer) session.getAttribute("userId");
            String fromDate = request.getParameter("fromDate");
//            String date = "";
//            String[] temp = null;
//            if (date1 != "") {
//                temp = date1.split("-");
//                String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
//                date = sDate;
//            }

            String BankHead = request.getParameter("BankHead");
            System.out.println("BankHead= " + BankHead);
            String totalDebitAmt = request.getParameter("totalDebitAmt");
            System.out.println("totalDebitAmt = " + totalDebitAmt);
            String chequeNo = request.getParameter("chequeNo");
            System.out.println("chequeNo = " + chequeNo);
            String chequeDate = request.getParameter("chequeDate");
//            String chequeDate = "";
//            if (chequeDate1 != "") {
//                temp = chequeDate1.split("-");
//                String cDate = temp[2] + "-" + temp[1] + "-" + temp[0];
//                chequeDate = cDate;
//                System.out.println("chequeDate = " + chequeDate);
//            }

            String[] bankid1Val = request.getParameterValues("bankid1");
            String[] amountVal = request.getParameterValues("amount");
            String[] accTypeVal = request.getParameterValues("accType");
            String[] narrationVal = request.getParameterValues("narration");
            System.out.println("bankid1 = " + bankid1Val.length);
            String voucherCode = request.getParameter("voucherCode");
            String voucherIdEdit = "";
            if (request.getParameter("voucherIdEdit") != null) {
                voucherIdEdit = request.getParameter("voucherIdEdit");
            }
            System.out.println("voucherIdEdit" + voucherIdEdit);
            status = financeBP.insertBankReceiptEntry(chequeDate, chequeNo, BankHead, totalDebitAmt, userId, fromDate, bankid1Val, amountVal, accTypeVal, narrationVal, voucherIdEdit, voucherCode);

//            String bankid1Values = null;
//            String amountValues = null;
//            String accTypeValues = null;
//            String narrationValues = null;
//            int detailCode = 0;
//            int cashDetailCode = (bankid1Val.length + 1);
//            System.out.println("bankid1Val.length = " + (bankid1Val.length + 1));
//            for (int i = 0; i < bankid1Val.length; i++) {
//                detailCode++;
//                bankid1Values = bankid1Val[i];
//                amountValues = amountVal[i];
//                accTypeValues = accTypeVal[i];
//                narrationValues = narrationVal[i];
//                status = financeBP.insertBankReceiptEntry(chequeDate, chequeNo, BankHead, cashDetailCode, totalDebitAmt, detailCode, userId, date, bankid1Values, amountValues, accTypeValues, narrationValues);
//            }
            System.out.println("----1-----");

            if (status > 0) {
                System.out.println("-----2----");
                System.out.println("status = " + status);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Payment Entry Added Successfully");
                bankReceiptList = financeBP.getBankReceiptList();
                System.out.println("bankReceiptList = " + bankReceiptList.size());
                request.setAttribute("bankReceiptList", bankReceiptList);
                ArrayList bankPaymentLedgerList = new ArrayList();
                ArrayList bankLedgerList = new ArrayList();
                bankPaymentLedgerList = financeBP.getBankPaymentLedgerList();
                System.out.println("bankPaymentLedgerList = " + bankPaymentLedgerList.size());
                request.setAttribute("bankPaymentLedgerList", bankPaymentLedgerList);
                bankLedgerList = financeBP.getBankLedgerList();
                System.out.println("bankLedgerList.lenth() = " + bankLedgerList.size());
                request.setAttribute("bankLedgerList", bankLedgerList);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert JournalEntry Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handlePaymentDetailsReport(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankPaymentLedgerList = new ArrayList();

        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Payements";
        menuPath = "Finance  >> View Payments Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            ArrayList vendorList = new ArrayList();
            vendorList = operationBP.processVendorList();//0 indicates all customers

            //pavi
//             ArrayList VendorTypeList = new ArrayList();
//            VendorTypeList = vendorBP.processGetVendorTypeList();
//            request.setAttribute("VendorTypeList", VendorTypeList);
            //////System.out.println("vendorList --" + vendorList.size());
            request.setAttribute("vendorList", vendorList);

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/viewPaymentReport.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handlePaymentPendingReport(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankPaymentLedgerList = new ArrayList();

        HttpSession session = request.getSession();
        financeCommand = command;
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        String pageTitle = "View Payements";
        menuPath = "Finance  >> View Pending Payments Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            ArrayList vendorList = new ArrayList();
            vendorList = operationBP.processVendorList();//0 indicates all customers

            //////System.out.println("vendorList --" + vendorList.size());
            request.setAttribute("vendorList", vendorList);

            if (financeCommand.getVendorId() != null && financeCommand.getVendorId() != "") {
                financeTO.setVendorId(financeCommand.getVendorId());
            }

            if (financeCommand.getFromDate() != null && financeCommand.getFromDate() != "") {
                financeTO.setFromDate(financeCommand.getFromDate());
            }

            if (financeCommand.getToDate() != null && financeCommand.getToDate() != "") {
                financeTO.setToDate(financeCommand.getToDate());
            }
            ArrayList invoiceList = new ArrayList();
            invoiceList = financeBP.getPendingPurchaseDetails(financeTO);
            System.out.println("invoiceList" + invoiceList.size());
            if (invoiceList.size() > 0) {
                request.setAttribute("invoiceList", invoiceList);
            }

            request.setAttribute("toDate", financeCommand.getToDate());
            request.setAttribute("fromDate", financeCommand.getFromDate());
            request.setAttribute("vendorId", financeCommand.getVendorId());

            path = "content/Finance/paymentPendingReport.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handlePaymentPaidReport(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankPaymentLedgerList = new ArrayList();

        HttpSession session = request.getSession();
        financeCommand = command;
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        String pageTitle = "View Payements";
        menuPath = "Finance  >> View Payments Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            ArrayList vendorList = new ArrayList();
            vendorList = operationBP.processVendorList();//0 indicates all customers

            //////System.out.println("vendorList --" + vendorList.size());
            request.setAttribute("vendorList", vendorList);

            if (financeCommand.getVendorId() != null && financeCommand.getVendorId() != "") {
                financeTO.setVendorId(financeCommand.getVendorId());
            }

            if (financeCommand.getFromDate() != null && financeCommand.getFromDate() != "") {
                financeTO.setFromDate(financeCommand.getFromDate());
            }

            if (financeCommand.getToDate() != null && financeCommand.getToDate() != "") {
                financeTO.setToDate(financeCommand.getToDate());
            }
            ArrayList invoiceList = new ArrayList();
            invoiceList = financeBP.getPendingPurchaseDetails(financeTO);
            System.out.println("invoiceList" + invoiceList.size());
            if (invoiceList.size() > 0) {
                request.setAttribute("invoiceList", invoiceList);
            }

            request.setAttribute("toDate", financeCommand.getToDate());
            request.setAttribute("fromDate", financeCommand.getFromDate());
            request.setAttribute("vendorId", financeCommand.getVendorId());

            path = "content/Finance/paymentPaidReport.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleCreditNote(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankPaymentLedgerList = new ArrayList();

        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Credit Note";
        menuPath = "Finance  >> Credit Note";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getCustomerList();//0 indicates all customers

            //////System.out.println("customerList --" + customerList.size());
            request.setAttribute("customerList", customerList);

            path = "content/Finance/creditNoteDetails.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView creditNoteDetails(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankPaymentLedgerList = new ArrayList();

        HttpSession session = request.getSession();
        financeCommand = command;
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        String pageTitle = "Credit Note";
        menuPath = "Finance  >> Credit Note";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            String customerId = request.getParameter("customerId");
            String ledgerId = request.getParameter("ledgerId");
            String customerName = request.getParameter("customerName");

            //////System.out.println("customerId:" + customerId);
            //////System.out.println("ledgerId:" + ledgerId);
            //////System.out.println("customerName:" + customerName);
            financeTO.setCustomerId(customerId);
            ArrayList invoiceList = new ArrayList();
            invoiceList = financeBP.getInvoiceList(financeTO);
            request.setAttribute("invoiceList", invoiceList);

            request.setAttribute("customerName", customerName);
            request.setAttribute("customerId", customerId);
            request.setAttribute("customerLedgerId", ledgerId);

            ArrayList bankList = financeBP.getBankList();
            request.setAttribute("bankLists", bankList);

            path = "content/Finance/viewCreditNote.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleCreditSearch(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankPaymentLedgerList = new ArrayList();

        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Credit Search";
        menuPath = "Finance  >> Credit Search";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        FinanceTO financeTO = new FinanceTO();
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/creditSearchDetails.jsp";
            if (financeCommand.getCustomerId() != null && financeCommand.getCustomerId() != "") {
                financeTO.setCustomerId(financeCommand.getCustomerId());
                request.setAttribute("customerId", financeCommand.getCustomerId());
            }
            if (financeCommand.getFromDate() != null && financeCommand.getFromDate() != "") {
                financeTO.setFromDate(financeCommand.getFromDate());
                request.setAttribute("fromDate", financeCommand.getFromDate());
            }
            if (financeCommand.getToDate() != null && financeCommand.getToDate() != "") {
                financeTO.setToDate(financeCommand.getToDate());
                request.setAttribute("toDate", financeCommand.getToDate());
            }
            ArrayList customerList = new ArrayList();
            customerList = operationBP.getCustomerList();//0 indicates all customers
            //////System.out.println("customerList --" + customerList.size());
            request.setAttribute("customerList", customerList);
            ArrayList creditNoteSearchList = new ArrayList();
            creditNoteSearchList = financeBP.getCreditNoteSearchList(financeTO);//0 indicates all customers
            //////System.out.println("creditNoteSearchList --" + creditNoteSearchList.size());
            if (creditNoteSearchList.size() > 0) {
                request.setAttribute("creditNoteSearchList", creditNoteSearchList);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleDebitNote(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankPaymentLedgerList = new ArrayList();

        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Debit Note";
        menuPath = "Finance  >> Debit Note";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            ArrayList vendorList = new ArrayList();
            vendorList = operationBP.processVendorList();//0 indicates all customers

            //////System.out.println("vendorList --" + vendorList.size());
            request.setAttribute("vendorList", vendorList);

            path = "content/Finance/debitNoteDetails.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView debitNoteDetails(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankPaymentLedgerList = new ArrayList();

        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        financeCommand = command;
        FinanceTO financeTO = new FinanceTO();
        String pageTitle = "Debit Note";
        menuPath = "Finance  >> Debit Note";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            int status = 0;
            path = "content/Finance/viewdebitNote.jsp";
            String vendorId = request.getParameter("vendorId");
            String ledgerId = request.getParameter("ledgerId");
            String vendorName = request.getParameter("vendorName");

            //////System.out.println("vendorId:" + vendorId);
            //////System.out.println("ledgerId:" + ledgerId);
            //////System.out.println("vendorName:" + vendorName);
            financeTO.setVendorId(vendorId);
            ArrayList invoiceList = new ArrayList();
            invoiceList = financeBP.getPurchaseList(financeTO);
            request.setAttribute("invoiceList", invoiceList);

            request.setAttribute("vendorName", vendorName);
            request.setAttribute("vendorId", vendorId);
            request.setAttribute("vendorLedgerId", ledgerId);

            ArrayList bankList = financeBP.getBankList();
            request.setAttribute("bankLists", bankList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleDebitSearch(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankPaymentLedgerList = new ArrayList();

        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Debit Search";
        menuPath = "Finance  >> Debit Search";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/debitSearchDetails.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView invoiceReceipts(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Finance >> Invoice Receipts ";
        String pageTitle = "invoiceReceipts";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList bankReceiptList = new ArrayList();
        try {

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getCustomerList();//0 indicates all customers

            //////System.out.println("customerList --" + customerList.size());
            request.setAttribute("customerList", customerList);

            int status = 0;
            path = "content/Finance/payment.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert JournalEntry Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView invoicePendingReceipts(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        financeCommand = command;
        FinanceTO financeTO = new FinanceTO();
        menuPath = "Finance >> Invoice Pending Details ";
        String pageTitle = "invoicePendingReceipts";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList bankReceiptList = new ArrayList();
        try {
            int status = 0;

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getCustomerList();
            request.setAttribute("customerList", customerList);

            if (financeCommand.getCustomerId() != null && financeCommand.getCustomerId() != "") {
                financeTO.setCustomerId(financeCommand.getCustomerId());
            }

            if (financeCommand.getFromDate() != null && financeCommand.getFromDate() != "") {
                financeTO.setFromDate(financeCommand.getFromDate());
            }

            if (financeCommand.getToDate() != null && financeCommand.getToDate() != "") {
                financeTO.setToDate(financeCommand.getToDate());
            }

            ArrayList pendingInvoice = new ArrayList();
            pendingInvoice = financeBP.getPendingInvoiceDetails(financeTO);
            if (pendingInvoice.size() > 0) {
                request.setAttribute("pendingInvoice", pendingInvoice);
            }

            request.setAttribute("toDate", financeCommand.getToDate());
            request.setAttribute("fromDate", financeCommand.getFromDate());
            request.setAttribute("customerId", financeCommand.getCustomerId());

            path = "content/Finance/paidInvoice.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert JournalEntry Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView invoiceReceived(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        financeCommand = command;
        FinanceTO financeTO = new FinanceTO();
        menuPath = "Finance >> Invoice Received Details ";
        String pageTitle = "invoiceReceived";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            if (financeCommand.getCustomerId() != null && financeCommand.getCustomerId() != "") {
                financeTO.setCustomerId(financeCommand.getCustomerId());
            }

            if (financeCommand.getFromDate() != null && financeCommand.getFromDate() != "") {
                financeTO.setFromDate(financeCommand.getFromDate());
            }

            if (financeCommand.getToDate() != null && financeCommand.getToDate() != "") {
                financeTO.setToDate(financeCommand.getToDate());
            }
            ArrayList customerList = new ArrayList();
            customerList = operationBP.getCustomerList();
            request.setAttribute("customerList", customerList);
            //////System.out.println("customerList size" + customerList.size());

            ArrayList pendingInvoice = new ArrayList();
            pendingInvoice = financeBP.getPendingInvoiceDetails(financeTO);
            request.setAttribute("pendingInvoice", pendingInvoice);
            //////System.out.println("pendingInvoice size" + pendingInvoice.size());

            request.setAttribute("toDate", financeCommand.getToDate());
            request.setAttribute("fromDate", financeCommand.getFromDate());
            request.setAttribute("customerId", financeCommand.getCustomerId());

            path = "content/Finance/receivedInvoice.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert JournalEntry Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView pLAccount(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Finance >> P & L Accounts ";
        String pageTitle = "invoiceReceived";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList bankReceiptList = new ArrayList();
        try {
            int status = 0;
            path = "content/Finance/PLAccounts.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert JournalEntry Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView paymentInvoiceDetails(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Finance >> P & L Accounts ";
        String pageTitle = "invoiceReceived";
        FinanceTO financeTO = new FinanceTO();
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList bankReceiptList = new ArrayList();
        try {
            int status = 0;
            path = "content/Finance/paymentInvoiceDetails.jsp";
            String customerId = request.getParameter("customerId");
            String ledgerId = request.getParameter("ledgerId");
            String customerName = request.getParameter("customerName");
            //////System.out.println("customerId:" + customerId);
            //////System.out.println("ledgerId:" + ledgerId);
            //////System.out.println("customerName:" + customerName);
            financeTO.setCustomerId(customerId);
            ArrayList invoiceList = new ArrayList();
            invoiceList = financeBP.getInvoiceList(financeTO);
            request.setAttribute("invoiceList", invoiceList);

            request.setAttribute("customerName", customerName);
            request.setAttribute("customerId", customerId);
            request.setAttribute("customerLedgerId", ledgerId);

//            ArrayList bankList = financeBP.getBankList();
//            request.setAttribute("bankLists", bankList);
            ArrayList primarybankList = financeBP.getBankList();
            primarybankList = financeBP.getPrimaryBankList();
            request.setAttribute("primarybankList", primarybankList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert JournalEntry Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView paymentPurchaseDetails(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Finance >> Vendor Payment Details";
        String pageTitle = "Vendor Payment Details";
        FinanceTO financeTO = new FinanceTO();
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList bankReceiptList = new ArrayList();
        try {
            int status = 0;
            path = "content/Finance/paymentPurchaseDetails.jsp";
            String vendorId = request.getParameter("vendorId");
            String ledgerId = request.getParameter("ledgerId");
            String vendorName = request.getParameter("vendorName");
//            String vendorType = request.getParameter("vendorType");
//            System.out.println("vendorType"+vendorType);
            //////System.out.println("vendorId:" + vendorId);
            //////System.out.println("ledgerId:" + ledgerId);
            //////System.out.println("vendorName:" + vendorName);
            financeTO.setVendorId(vendorId);
//            financeTO.setVendorTypeId(vendorType);
            ArrayList invoiceList = new ArrayList();
            invoiceList = financeBP.getPurchaseList(financeTO);
            request.setAttribute("invoiceList", invoiceList);

            request.setAttribute("vendorName", vendorName);
            request.setAttribute("vendorId", vendorId);
            request.setAttribute("vendorLedgerId", ledgerId);
//            request.setAttribute("vendorType", vendorType);

//            ArrayList bankList = financeBP.getBankList();
//            request.setAttribute("bankLists", bankList);
            ArrayList primarybankList = new ArrayList();
            primarybankList = financeBP.getPrimaryBankList();
            request.setAttribute("primarybankList", primarybankList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert JournalEntry Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getInvoiceDetails(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException {
        HttpSession session = request.getSession();
        financeCommand = command;
        FinanceTO financeTO = new FinanceTO();
        ArrayList invoiceDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String invoiceId = "";
            response.setContentType("text/html");
            invoiceId = request.getParameter("invoiceId");
            financeTO.setInvoiceId(invoiceId);
            invoiceDetails = financeBP.getInvoiceDetails(financeTO);
            //////System.out.println("invoiceDetails.size() = " + invoiceDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = invoiceDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                financeTO = (FinanceTO) itr.next();
                jsonObject.put("Name", financeTO.getGrandTotal() + "-" + financeTO.getPendingAmount());
                //////System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            //////System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void insertPaymentDetails(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        financeCommand = command;
        HttpSession session = request.getSession();
        System.out.println("pavi......");
        FinanceTO financeTO = new FinanceTO();
        int userId = (Integer) session.getAttribute("userId");
        int inpId = 0;
        try {
            System.out.println("insertPaymentDetails");
            String vendorId = request.getParameter("vendorId");
            System.out.println("vendorId" + vendorId);

            if (financeCommand.getPaymentMode() != null && financeCommand.getPaymentMode() != "") {
                financeTO.setPaymentMode(financeCommand.getPaymentMode());
            }
            if (financeCommand.getReferenceNo() != null && financeCommand.getReferenceNo() != "") {
                financeTO.setReferenceNo(financeCommand.getReferenceNo());
            }
            if (financeCommand.getReceiptAmount() != null && financeCommand.getReceiptAmount() != "") {
                financeTO.setReceiptAmount(financeCommand.getReceiptAmount());
            }
            if (financeCommand.getReceiptDate() != null && financeCommand.getReceiptDate() != "") {
                financeTO.setReceiptDate(financeCommand.getReceiptDate());
            }
//            if (financeCommand.getBankName() != null && financeCommand.getBankName() != "") {
//                financeTO.setBankname(financeCommand.getBankName());
//            }
//            if (financeCommand.getVendorTypeId() != null && financeCommand.getVendorTypeId() != "") {
//                financeTO.setVendorTypeId(financeCommand.getVendorTypeId());
//            }
            String branchId = "";
            String bankLedgerId = "";
            String bankBranchId = request.getParameter("bankBranchId");
            String bankId = request.getParameter("bankId");
            String temp1[] = bankBranchId.split("~");
            branchId = temp1[0];
            bankLedgerId = temp1[1];
//            String receiveBankLedgerId = request.getParameter("receiveBankLedgerId");
            String vendorLedgerId = request.getParameter("vendorLedgerId");

            financeTO.setVendorId(vendorId);

            financeTO.setBranchId(branchId);
            financeTO.setBankLedgerId(bankLedgerId);
            financeTO.setVendorLedgerId(vendorLedgerId);
            financeTO.setBankId(bankId);

            String invoiceCodes[] = request.getParameterValues("invoiceCodeVal[]");
            String invoiceIds[] = request.getParameterValues("invoiceIdVal[]");
            String paidAmounts[] = request.getParameterValues("payAmountVal[]");
            String narration[] = request.getParameterValues("narrationVal[]");
            financeTO.setInvoiceCodes(invoiceCodes);
            financeTO.setInvoiceIds(invoiceIds);
            financeTO.setPaidAmounts(paidAmounts);
            financeTO.setNarration(narration);
            inpId = financeBP.savePurchasePaymentDetails(financeTO, userId);
            System.out.println("invoice Payment id inpId" + inpId);
            response.setContentType("text/css");
            if (inpId > 0) {
                response.getWriter().println(inpId);
            } else {
                response.getWriter().println(0);
            }
        } catch (UnknownHostException ex) {
            //ex.printStackTrace();
        }
    }

    public void insertInvoiceDetails(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        financeCommand = command;
        HttpSession session = request.getSession();
        System.out.println("pavi......");
        FinanceTO financeTO = new FinanceTO();
        int userId = (Integer) session.getAttribute("userId");
        int inrId = 0;
        try {
            if (financeCommand.getCustomerId() != null && financeCommand.getCustomerId() != "") {
                financeTO.setCustomerId(financeCommand.getCustomerId());
            }
            if (financeCommand.getPaymentMode() != null && financeCommand.getPaymentMode() != "") {
                financeTO.setPaymentMode(financeCommand.getPaymentMode());
            }
            if (financeCommand.getReferenceNo() != null && financeCommand.getReferenceNo() != "") {
                financeTO.setReferenceNo(financeCommand.getReferenceNo());
            }
            if (financeCommand.getReceiptAmount() != null && financeCommand.getReceiptAmount() != "") {
                financeTO.setReceiptAmount(financeCommand.getReceiptAmount());
            }
            if (financeCommand.getReceiptDate() != null && financeCommand.getReceiptDate() != "") {
                financeTO.setReceiptDate(financeCommand.getReceiptDate());
            }
//            if (financeCommand.getBankName() != null && financeCommand.getBankName() != "") {
//                financeTO.setBankname(financeCommand.getBankName());
//            }
            if (financeCommand.getBankId() != null && financeCommand.getBankId() != "") {
                financeTO.setBankId(financeCommand.getBankId());
            }
            String branchId = "";
            String bankLedgerId = "";
            String bankBranchId = request.getParameter("bankBranchId");
            String bankId = request.getParameter("bankId");
            String temp1[] = bankBranchId.split("~");
            branchId = temp1[0];
            bankLedgerId = temp1[1];
//            String receiveBankLedgerId = request.getParameter("bankLedgerId");
            String customerLedgerId = request.getParameter("customerLedgerId");
            financeTO.setBankLedgerId(bankLedgerId);
            financeTO.setBranchId(branchId);
            financeTO.setCustomerLedgerId(customerLedgerId);

            String invoiceCodes[] = request.getParameterValues("invoiceCodeVal[]");
            String invoiceIds[] = request.getParameterValues("invoiceIdVal[]");
            String paidAmounts[] = request.getParameterValues("payAmountVal[]");
            String narration[] = request.getParameterValues("narrationVal[]");
            financeTO.setInvoiceCodes(invoiceCodes);
            financeTO.setInvoiceIds(invoiceIds);
            financeTO.setPaidAmounts(paidAmounts);
            financeTO.setNarration(narration);
            inrId = financeBP.saveInvoicePaymentDetails(financeTO, userId);
            System.out.println("invoice Receipt id inrId" + inrId);
            response.setContentType("text/css");
            if (inrId > 0) {
                response.getWriter().println(inrId);
            } else {
                response.getWriter().println(0);
            }
        } catch (UnknownHostException ex) {
            //ex.printStackTrace();
        }
    }
    /*
     public ModelAndView insertInvoiceDetails(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

     HttpSession session = request.getSession();
     int status = 0;
     String path = "";
     financeCommand = command;
     int userId = (Integer) session.getAttribute("userId");
     ArrayList bankList = new ArrayList();
     FinanceTO financeTO = new FinanceTO();
     String menuPath = "";
     menuPath = "Finance >> Bank  ";
     String pageTitle = "Manage Bank";
     request.setAttribute("pageTitle", pageTitle);
     request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
     ModelAndView mv = null;
     try {
     if (financeCommand.getCustomerId() != null && financeCommand.getCustomerId() != "") {
     financeTO.setCustomerId(financeCommand.getCustomerId());
     }
     if (financeCommand.getPaymentMode() != null && financeCommand.getPaymentMode() != "") {
     financeTO.setPaymentMode(financeCommand.getPaymentMode());
     }
     if (financeCommand.getReferenceNo() != null && financeCommand.getReferenceNo() != "") {
     financeTO.setReferenceNo(financeCommand.getReferenceNo());
     }
     if (financeCommand.getReceiptAmount() != null && financeCommand.getReceiptAmount() != "") {
     financeTO.setReceiptAmount(financeCommand.getReceiptAmount());
     }
     if (financeCommand.getReceiptDate() != null && financeCommand.getReceiptDate() != "") {
     financeTO.setReceiptDate(financeCommand.getReceiptDate());
     }
     //            if (financeCommand.getBankName() != null && financeCommand.getBankName() != "") {
     //                financeTO.setBankname(financeCommand.getBankName());
     //            }
     if (financeCommand.getBankId() != null && financeCommand.getBankId() != "") {
     financeTO.setBankId(financeCommand.getBankId());
     }
     String branchId = "";
     String bankLedgerId = "";
     String bankBranchId = request.getParameter("bankBranchId");
     String bankId = request.getParameter("bankId");
     String temp1[] = bankBranchId.split("~");
     branchId = temp1[0];
     bankLedgerId = temp1[1];
     //            String receiveBankLedgerId = request.getParameter("bankLedgerId");
     String customerLedgerId = request.getParameter("customerLedgerId");
     financeTO.setBankLedgerId(bankLedgerId);
     financeTO.setBranchId(branchId);
     financeTO.setCustomerLedgerId(customerLedgerId);
     String[] invoiceIds = request.getParameterValues("invoiceIds");
     String[] paidAmounts = request.getParameterValues("paidAmounts");
     financeTO.setInvoiceIds(invoiceIds);
     financeTO.setPaidAmounts(paidAmounts);
     status = financeBP.saveInvoicePaymentDetails(financeTO, userId);
     mv = invoiceReceipts(request, reponse, command);
     path = "content/Finance/finBank.jsp";
     bankList = financeBP.getBankList();
     request.setAttribute("bankLists", bankList);
     if (status > 0) {
     request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Invoice Received Successfully ! ");
     }

     } catch (FPRuntimeException exception) {

     FPLogUtils.fpErrorLog("Run time exception --> " + exception);
     return new ModelAndView("content/common/error.jsp");
     } catch (FPBusinessException exception) {

     FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
     request.setAttribute(ParveenErrorConstants.ERROR_KEY,
     exception.getErrorMessage());
     } catch (Exception exception) {
     exception.printStackTrace();
     FPLogUtils.fpErrorLog("Failed to insert Bank Details --> " + exception);
     return new ModelAndView("content/common/error.jsp");
     }
     //        return new ModelAndView(path);
     return mv;
     }

     */

    public ModelAndView saveDebitNote(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList bankList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> Bank  ";
        String pageTitle = "Manage Bank";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ModelAndView mv = null;
        try {
            String vendorId = request.getParameter("vendorId");
            financeTO.setVendorId(vendorId);

            String invoiceId = request.getParameter("invoiceId");
            String payAmount = request.getParameter("payAmount");//discountAmt
            String invoiceCode = request.getParameter("invoiceCode");
//            String receiptAmount = request.getParameter("receiptAmount");//totalAmt
            //pavi
//            String debitedAmount = request.getParameter("debitedAmount");//actualAmt

            financeTO.setInvoiceId(invoiceId);
            financeTO.setPayAmount(payAmount);
            financeTO.setInvoiceCode(invoiceCode);
//            financeTO.setReceiptAmount(receiptAmount);
//            financeTO.setDebitedAmount(debitedAmount);

            String remarks = request.getParameter("remarks");
            String receiptDate = request.getParameter("receiptDate");

            financeTO.setReceiptDate(receiptDate);
            financeTO.setRemarks(remarks);

            status = financeBP.saveDebitNote(financeTO, userId);

            ArrayList vendorList = new ArrayList();
            vendorList = operationBP.processVendorList();//0 indicates all customers

            //////System.out.println("vendorList --" + vendorList.size());
            request.setAttribute("vendorList", vendorList);

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Debit Note Added Successfully ! ");
            }

            mv = handleDebitNote(request, reponse, command);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bank Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
//        return new ModelAndView(path);
        return mv;
    }

    public ModelAndView saveCreditNote(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList bankList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> Bank  ";
        String pageTitle = "Manage Bank";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ModelAndView mv = null;
        try {
            String customerId = request.getParameter("customerId");
            financeTO.setCustomerId(customerId);

            // String receiptAmount = request.getParameter("receiptAmount");
            String invoiceId = request.getParameter("invoiceId");
            String payAmount = request.getParameter("payAmount");
            String remarks = request.getParameter("remarks");
            String receiptDate = request.getParameter("receiptDate");
            String invoiceCode = request.getParameter("invoiceCode");

            financeTO.setReceiptDate(receiptDate);
            financeTO.setRemarks(remarks);
            financeTO.setInvoiceId(invoiceId);
            financeTO.setPayAmount(payAmount);
            financeTO.setInvoiceCode(invoiceCode);
            //financeTO.setReceiptAmount(receiptAmount);

            status = financeBP.saveCreditNote(financeTO, userId);

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Credit Note Added Successfully ! ");
            }

            mv = handleCreditNote(request, reponse, command);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bank Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
//        return new ModelAndView(path);
        return mv;
    }

    /**
     * This method used to handle financeYear.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView financeYear(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList financeYearList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Financial Year List ";
        menuPath = "Finance  >> Financial Year";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/finYear.jsp";
            financeYearList = financeBP.getFinanceYearList();
            request.setAttribute("financeYearList", financeYearList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView saveFinYear(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList financeYearList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> Year  ";
        String pageTitle = "Fin Year";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {

            if (financeCommand.getFinYear() != null && financeCommand.getFinYear() != "") {
                financeTO.setFinYear(financeCommand.getFinYear());
            }
            if (financeCommand.getFromDate() != null && financeCommand.getFromDate() != "") {
                financeTO.setFromDate(financeCommand.getFromDate());
            }
            if (financeCommand.getToDate() != null && financeCommand.getToDate() != "") {
                financeTO.setToDate(financeCommand.getToDate());
            }
            if (financeCommand.getActive_ind() != null && financeCommand.getActive_ind() != "") {
                financeTO.setActive_ind(financeCommand.getActive_ind());
            }
            if (financeCommand.getDescription() != null && financeCommand.getDescription() != "") {
                financeTO.setDescription(financeCommand.getDescription());
            }
            if (financeCommand.getAccountYearId() != null && financeCommand.getAccountYearId() != "") {
                financeTO.setAccountYearId(financeCommand.getAccountYearId());

                status = financeBP.updateFinYear(financeTO, userId);

                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Finance Year Updated Successfully ! ");
                }

            } else {
                status = financeBP.saveFinYear(financeTO, userId);
                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Finance Year Added Successfully ! ");
                }
            }

            path = "content/Finance/finYear.jsp";

            financeYearList = financeBP.getFinanceYearList();
            request.setAttribute("financeYearList", financeYearList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bank Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView alterAccountYear(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList getFinanceYear = new ArrayList();
        HttpSession session = request.getSession();
        FinanceTO financeTO = new FinanceTO();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Alter Financial Year  ";
        menuPath = "Finance  >> Alter Financial Year";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            if (request.getParameter("accountYearId") != null && request.getParameter("accountYearId") != "") {
                financeTO.setAccountYearId(request.getParameter("accountYearId"));
            }

            getFinanceYear = financeBP.getFinanceYear(financeTO);
            request.setAttribute("getFinanceYear", getFinanceYear);
            System.out.println("getFinanceYear = " + getFinanceYear);
            path = "content/Finance/AlterfinYear.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public void checkFinYear(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException {
        financeCommand = command;
        String suggestions = "";
        String finYear = request.getParameter("finYear");
        FinanceTO financeTO = new FinanceTO();
        financeTO.setFinYear(finYear);
        try {
            System.out.println("finYear = " + finYear);
            suggestions = financeBP.checkFinYear(financeTO);
            System.out.println("suggestions = " + suggestions);
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(suggestions);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Model data in Ajax --> " + exception);
        }
    }

    public void checkBankAccountCode(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException {
        financeCommand = command;
        String suggestions = "";
        String bankCode = request.getParameter("bankCode");
        FinanceTO financeTO = new FinanceTO();
        financeTO.setBankCode(bankCode);
        try {
            System.out.println("bankCode = " + bankCode);
            suggestions = financeBP.checkBankAccountCode(financeTO);
            System.out.println("suggestions = " + suggestions);
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(suggestions);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Model data in Ajax --> " + exception);
        }
    }

    public ModelAndView updateFinYear(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList financeYearList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> Year  ";
        String pageTitle = "Fin Year";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {

            if (financeCommand.getFinYear() != null && financeCommand.getFinYear() != "") {
                financeTO.setFinYear(financeCommand.getFinYear());
            }
            if (financeCommand.getFromDate() != null && financeCommand.getFromDate() != "") {
                financeTO.setFromDate(financeCommand.getFromDate());
            }
            if (financeCommand.getToDate() != null && financeCommand.getToDate() != "") {
                financeTO.setToDate(financeCommand.getToDate());
            }
            if (financeCommand.getActive_ind() != null && financeCommand.getActive_ind() != "") {
                financeTO.setActive_ind(financeCommand.getActive_ind());
            }
            if (financeCommand.getDescription() != null && financeCommand.getDescription() != "") {
                financeTO.setDescription(financeCommand.getDescription());
            }
            if (financeCommand.getAccountYearId() != null && financeCommand.getAccountYearId() != "") {
                financeTO.setAccountYearId(financeCommand.getAccountYearId());
            }

            status = financeBP.updateFinYear(financeTO, userId);

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Finance Year Updated Successfully ! ");
            }

            path = "content/Finance/finYear.jsp";

            financeYearList = financeBP.getFinanceYearList();
            request.setAttribute("financeYearList", financeYearList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bank Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void checkBankAccountName(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException {
        financeCommand = command;
        String suggestions = "";
        String bankName = request.getParameter("bankName");
        FinanceTO financeTO = new FinanceTO();
        financeTO.setBankName(bankName);
        try {
            System.out.println("bankName = " + bankName);
            suggestions = financeBP.checkBankAccountName(financeTO);
            System.out.println("suggestions = " + suggestions);
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(suggestions);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Model data in Ajax --> " + exception);
        }
    }

    public ModelAndView financePrimaryBank(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList primarybankList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Bank";
        menuPath = "Finance  >> Bank Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/finPrimaryBank.jsp";
            primarybankList = financeBP.getPrimaryBankList();
            request.setAttribute("primarybankList", primarybankList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView addPrimaryBankPage(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        ArrayList bankTypes = new ArrayList();
        financeCommand = command;
        String menuPath = "";
        menuPath = "Finance >> Bank >> Add Bank ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Add Bank";
            request.setAttribute("pageTitle", pageTitle);
            ArrayList bankList = new ArrayList();
            path = "content/Finance/newPrimaryBank.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBank --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView savePrimaryBank(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList primarybankList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> Bank  ";
        String pageTitle = "Manage Bank";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (financeCommand.getBankName() != null && financeCommand.getBankName() != "") {
                financeTO.setBankname(financeCommand.getBankName());
            }
            if (financeCommand.getBankCode() != null && financeCommand.getBankCode() != "") {
                financeTO.setBankcode(financeCommand.getBankCode());
            }
            if (financeCommand.getActive_ind() != null && financeCommand.getActive_ind() != "") {
                financeTO.setActive_ind(financeCommand.getActive_ind());
            }

            path = "content/Finance/finPrimaryBank.jsp";

            status = financeBP.savePrimaryBank(financeTO, userId);
            primarybankList = financeBP.getPrimaryBankList();

            request.setAttribute("primarybankList", primarybankList);

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Bank Added Successfully ! ");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bank Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle financeBank.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView financeBank(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";

        ArrayList primarybankList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Bank Branch";
        menuPath = "Finance  >> Bank Branch Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/finBank.jsp";

            primarybankList = financeBP.getPrimaryBankList();
            request.setAttribute("primarybankList", primarybankList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView addBankPage(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        ArrayList bankTypes = new ArrayList();
        financeCommand = command;
        String menuPath = "";
        menuPath = "Finance >> Bank >> Add Bank Branch";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            FinanceTO financeTO = new FinanceTO();
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "Add Bank Branch";
            request.setAttribute("pageTitle", pageTitle);
            ArrayList bankBranchLists = new ArrayList();

            String bankId = request.getParameter("bankId");
            String bankName = request.getParameter("bankName");

            if (bankId != null && !bankId.equals("")) {
                financeTO.setBankId(bankId);
            }

            bankBranchLists = financeBP.getBankBranchList(financeTO);
            request.setAttribute("bankBranchLists", bankBranchLists);
            request.setAttribute("bankName", bankName);
            request.setAttribute("bankId", bankId);

            path = "content/Finance/bankBranchList.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBank --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveNewBank(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList primarybankList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> Bank  ";
        String pageTitle = "Manage Bank";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (financeCommand.getBankName() != null && financeCommand.getBankName() != "") {
                financeTO.setBankname(financeCommand.getBankName());
            }
            if (financeCommand.getBankCode() != null && financeCommand.getBankCode() != "") {
                financeTO.setBankcode(financeCommand.getBankCode());
            }
            if (financeCommand.getAddress() != null && financeCommand.getAddress() != "") {
                financeTO.setAddress(financeCommand.getAddress());
            }
            if (financeCommand.getPhoneNo() != null && financeCommand.getPhoneNo() != "") {
                financeTO.setPhoneNo(financeCommand.getPhoneNo());
            }
            if (financeCommand.getAccntCode() != null && financeCommand.getAccntCode() != "") {
                financeTO.setAccntCode(financeCommand.getAccntCode());
            }
            if (financeCommand.getDescription() != null && financeCommand.getDescription() != "") {
                financeTO.setDescription(financeCommand.getDescription());
            }

            if (financeCommand.getBankid() != null && financeCommand.getBankid() != "") {
                financeTO.setBankid(financeCommand.getBankid());
            }

            if (financeCommand.getBSRCode() != null && financeCommand.getBSRCode() != "") {
                financeTO.setBSRCode(financeCommand.getBSRCode());
            }

            if (financeCommand.getIFSCCode() != null && financeCommand.getIFSCCode() != "") {
                financeTO.setIFSCCode(financeCommand.getIFSCCode());
            }
            if (financeCommand.getBranchName() != null && financeCommand.getBranchName() != "") {
                financeTO.setBranchName(financeCommand.getBranchName());
            }

            if (financeCommand.getBranchId() != null && financeCommand.getBranchId() != "") {
                financeTO.setBranchId(financeCommand.getBranchId());
                status = financeBP.saveAlterBank(financeTO, userId);
                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Bank Branch Modified Successfully ! ");
                }

            } else {
                status = financeBP.saveNewBank(financeTO, userId);
                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Bank Branch Added Successfully ! ");
                }
            }

            path = "content/Finance/finBank.jsp";

            primarybankList = financeBP.getPrimaryBankList();
            request.setAttribute("primarybankList", primarybankList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bank Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView alterBankDetail(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankalterList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Alter Bunk";
        menuPath = "Finance  >>Bank Alter ";
        request.setAttribute("pageTitle", pageTitle);
        String bankid = request.getParameter("bankid");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/alterBank.jsp";
            bankalterList = financeBP.processBankalterList(bankid);
            //////System.out.println("test ========== ");
            request.setAttribute("bankalterList", bankalterList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveAlterBank(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList bankList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> Bank  ";
        String pageTitle = "Manage Bank";
        request.setAttribute("pageTitle", pageTitle);
        String bankid = request.getParameter("bankid");
        //////System.out.println("bankid 11111= " + bankid);
//        int bankid = (Integer) request.getParameter("bankid");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (financeCommand.getBankName() != null && financeCommand.getBankName() != "") {
                financeTO.setBankname(financeCommand.getBankName());
            }
            if (financeCommand.getBankCode() != null && financeCommand.getBankCode() != "") {
                financeTO.setBankcode(financeCommand.getBankCode());
            }
            if (financeCommand.getAddress() != null && financeCommand.getAddress() != "") {
                financeTO.setAddress(financeCommand.getAddress());
            }
            if (financeCommand.getPhoneNo() != null && financeCommand.getPhoneNo() != "") {
                financeTO.setPhoneNo(financeCommand.getPhoneNo());
            }
            if (financeCommand.getAccntCode() != null && financeCommand.getAccntCode() != "") {
                financeTO.setAccntCode(financeCommand.getAccntCode());
            }
            if (financeCommand.getDescription() != null && financeCommand.getDescription() != "") {
                financeTO.setDescription(financeCommand.getDescription());
            }
            path = "content/Finance/finBank.jsp";
            //////System.out.println("bankid = " + bankid);
            //status = financeBP.saveAlterBank(financeTO, bankid);
            bankList = financeBP.getBankList();
            request.setAttribute("bankLists", bankList);
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Bank Modified Successfully ! ");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bank Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView chargeCodeTax(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList taxList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Charge Code Tax Map";
        menuPath = "Finance  >> Charge Code Tax Map";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/chargeCodeTaxMap.jsp";
            taxList = financeBP.getTaxList();
            request.setAttribute("taxLists", taxList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to voucherList --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public void getChargeCodeList(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException {
        financeCommand = command;
        String suggestions = "";
        String chargeCode = request.getParameter("chargeCode");
        FinanceTO financeTO = new FinanceTO();
        financeTO.setChargeCode(chargeCode);
        try {
            System.out.println("chargeCode = " + chargeCode);
            suggestions = financeBP.getChargeCodeList(financeTO);
            System.out.println("suggestions = " + suggestions);
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(suggestions);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Model data in Ajax --> " + exception);
        }
    }

    public void getLedgerDetails(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException {
        HttpSession session = request.getSession();
        financeCommand = command;
        FinanceTO financeTO = new FinanceTO();
        ArrayList getLedgerDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String ledgerName = "";
            String entryType = "";
            ledgerName = request.getParameter("ledgerName");
            System.out.println("ledgerName = " + ledgerName);
            getLedgerDetails = financeBP.getLedgerDetailsGrp(ledgerName, entryType);
            //////System.out.println("invoiceDetails.size() = " + invoiceDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = getLedgerDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                financeTO = (FinanceTO) itr.next();
                jsonObject.put("ledgerId", financeTO.getLedgerID());
                jsonObject.put("groupCode", financeTO.getGroupCode());
                jsonObject.put("levelId", financeTO.getLevelID());
                jsonObject.put("ledgerCode", financeTO.getLedgerCode());
                jsonObject.put("ledgerName", financeTO.getLedgerName());
                //////System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            //////System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getCashLedgerDetails(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException {
        HttpSession session = request.getSession();
        financeCommand = command;
        FinanceTO financeTO = new FinanceTO();
        ArrayList getLedgerDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();

        try {

            String cashHeadName = "";
            String entryType = "";
            cashHeadName = request.getParameter("ledgerName");
            System.out.println("cashHeadName = " + cashHeadName);

            getLedgerDetails = financeBP.getCashLedgerDetails(cashHeadName, entryType);
            //////System.out.println("invoiceDetails.size() = " + invoiceDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = getLedgerDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                financeTO = (FinanceTO) itr.next();
                jsonObject.put("ledgerId", financeTO.getLedgerID());
                jsonObject.put("groupCode", financeTO.getGroupCode());
                jsonObject.put("levelId", financeTO.getLevelID());
                jsonObject.put("ledgerCode", financeTO.getLedgerCode());
                jsonObject.put("ledgerName", financeTO.getLedgerName());
                //////System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            //////System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getBankLedgerDetails(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException {
        HttpSession session = request.getSession();
        financeCommand = command;
        FinanceTO financeTO = new FinanceTO();
        ArrayList getLedgerDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();

        try {

            String cashHeadName = "";
            String entryType = "";
            cashHeadName = request.getParameter("ledgerName");
            System.out.println("cashHeadName = " + cashHeadName);

            getLedgerDetails = financeBP.getBankLedgerDetails(cashHeadName, entryType);
            //////System.out.println("invoiceDetails.size() = " + invoiceDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = getLedgerDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                financeTO = (FinanceTO) itr.next();
                jsonObject.put("ledgerId", financeTO.getLedgerID());
                jsonObject.put("groupCode", financeTO.getGroupCode());
                jsonObject.put("levelId", financeTO.getLevelID());
                jsonObject.put("ledgerCode", financeTO.getLedgerCode());
                jsonObject.put("ledgerName", financeTO.getLedgerName());
                jsonObject.put("openingBalance", financeTO.getOpeningBalance());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView handlevendorPayments(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");

        }
        String path = "";
        ArrayList vendorList = new ArrayList();
        ArrayList VendorTypeList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        String pageTitle = " Vendor Payment ";
        menuPath = "Operation >> Vendor Payment";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        PrintWriter pw = response.getWriter();
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/handleVendorPayment.jsp";
            VendorTypeList = vendorBP.processGetVendorTypeList();
            request.setAttribute("VendorTypeList", VendorTypeList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);

            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);

    }

    public void handlevendorPaymentsJson(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException {
        String path = "";
        ArrayList vendorList = new ArrayList();
        ArrayList VendorTypeList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        String pageTitle = " Vendor Payment ";
        menuPath = "Operation >> Vendor Payment";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        PrintWriter pw = response.getWriter();
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/handleVendorPayment.jsp";
            VendorTypeList = vendorBP.processGetVendorTypeList();
            request.setAttribute("VendorTypeList", VendorTypeList);

            String vendorType = request.getParameter("vendorType");
            System.out.println("vendorType" + vendorType);
            request.setAttribute("vendorType", vendorType);
            vendorList = financeBP.processVendorList(vendorType);
            request.setAttribute("vendorList", vendorList);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = vendorList.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                financeTO = (FinanceTO) itr.next();
                jsonObject.put("vendorId", financeTO.getVendorId());
                jsonObject.put("vendorName", financeTO.getVendorName());
                jsonObject.put("ledgerId", financeTO.getLedgerID());
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);
            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }

    }

    public ModelAndView handlevendorPaymentView(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");

        }
        String path = "";
        ArrayList vendorList = new ArrayList();
        ArrayList VendorTypeList = new ArrayList();
        ArrayList vendorInvoiceTripDetailsList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        String pageTitle = " Vendor Payment ";
        menuPath = "Operation >> Vendor Payment";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        PrintWriter pw = response.getWriter();
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String param = request.getParameter("param");
            String vendorType = request.getParameter("vendorType");
            String contractTypeId = request.getParameter("contractTypeId");
            String virCode = request.getParameter("virCode");
            String vendorNames = request.getParameter("vendorName");
            String billingState = request.getParameter("billingState");
            System.out.println("billingState---" + billingState);
            System.out.println("vendorNames" + vendorNames);
            String temp[] = vendorNames.split("~");
            String vendorId = temp[0];
            String vendorName = temp[1];
            String invoiceNo = "";
            String invoiceAmount = "";
            String invoiceDate = "";
            System.out.println("virCode" + virCode);
            financeTO.setVirCode(virCode);
            financeTO.setContractTypeId(contractTypeId);
            financeTO.setVendorId(vendorId);
            
            if ((ThrottleConstants.leasingVendorTypeId).equals(vendorType)) {
                invoiceNo = request.getParameter("invoiceNo");
                invoiceAmount = request.getParameter("invoiceAmount");
                invoiceDate = request.getParameter("invoiceDate");
                if ("process".equals(param)) {
                    path = "content/Finance/processVendorInvoice.jsp";
                } else if ("view".equals(param)) {
                    path = "content/Finance/viewProcessedVendorInvoice.jsp";
                }
                
                ArrayList VIRdetailList = new ArrayList();
                VIRdetailList = financeBP.getVirdetailList(financeTO);
                request.setAttribute("VIRdetailList", VIRdetailList);

                ArrayList gstDetails = new ArrayList();
                gstDetails = financeBP.getGSTTaxDetails(financeTO);

                if (gstDetails.size() > 0) {
                    Iterator it = gstDetails.iterator();
                    while (it.hasNext()) {
                        BillingTO billTO = (BillingTO) it.next();
                        if ("CGST".equalsIgnoreCase(billTO.getGstName())) {
                            System.out.println("CGST:" + billTO.getGstPercentage());
                            request.setAttribute("CGST", billTO.getGstPercentage());
                        } else if ("SGST".equalsIgnoreCase(billTO.getGstName())) {
                            System.out.println("SGST:" + billTO.getGstPercentage());
                            request.setAttribute("SGST", billTO.getGstPercentage());
                        } else if ("IGST".equalsIgnoreCase(billTO.getGstName())) {
                            System.out.println("IGST:" + billTO.getGstPercentage());
                            request.setAttribute("IGST", billTO.getGstPercentage());
                        }
                    }
                }

                vendorInvoiceTripDetailsList = financeBP.getVendorInvoiceTripDetailsList(financeTO);
                request.setAttribute("vendorInvoiceTripDetailsList", vendorInvoiceTripDetailsList);
            } else {
                request.setAttribute("vendorInvoiceTripDetailsList", null);
            }
            ArrayList invoiceTax = financeBP.getInvoiceTax(financeTO);
            request.setAttribute("invoiceTax", invoiceTax);
            request.setAttribute("contractTypeId", contractTypeId);
            request.setAttribute("vendorName", vendorName);
            request.setAttribute("vendorId", temp[0]);
            request.setAttribute("vendorType", vendorType);
            request.setAttribute("invoiceNo", invoiceNo);
            request.setAttribute("invoiceAmount", invoiceAmount);
            request.setAttribute("invoiceDate", invoiceDate);
            request.setAttribute("virCode", virCode);
            request.setAttribute("billingState", billingState);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);

            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);

    }

    public ModelAndView handleContractVendorPaymentDetails(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");

        }
        String path = "";
        ArrayList vendorPaymentList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        String pageTitle = " Vendor Payment ";
        menuPath = "Operation >> Vendor Payment";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        PrintWriter pw = response.getWriter();
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            String vendorName = request.getParameter("vendorName");
            String vendorId = request.getParameter("vendorId");
            System.out.println("vendorId 3333" + vendorId);
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            String vendorType = request.getParameter("vendorType");
            String contractTypeId = request.getParameter("contractTypeId");
            String invoiceNo = request.getParameter("invoiceNo");
            String invoiceDate = request.getParameter("invoiceDate");
            String invoiceAmount = request.getParameter("invoiceAmount");
            String virCode = request.getParameter("virCode");
            System.out.println("virCode" + virCode);

            path = "content/Finance/processVendorInvoice.jsp";
            request.setAttribute("vendorName", vendorName);
            request.setAttribute("vendorId", vendorId);
            request.setAttribute("startDate", startDate);
            request.setAttribute("endDate", endDate);
            request.setAttribute("vendorType", vendorType);
            request.setAttribute("contractTypeId", contractTypeId);
            request.setAttribute("invoiceNo", invoiceNo);
            request.setAttribute("invoiceDate", invoiceDate);
            request.setAttribute("invoiceAmount", invoiceAmount);
            request.setAttribute("virCode", virCode);

            vendorPaymentList = financeBP.getContractVendorPaymentDetails(vendorId, startDate, endDate, contractTypeId);
            request.setAttribute("vendorPaymentList", vendorPaymentList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);

            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);

    }

    public void saveVendorPaymentDetails(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        financeCommand = command;
        HttpSession session = request.getSession();
        System.out.println("pavi......");
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        int status = 0;
        FinanceTO financeTO = new FinanceTO();
        try {
            String invoiceAmount = request.getParameter("invoiceAmount");
            String invoiceDate = request.getParameter("invoiceDate");
            String invoiceNo = request.getParameter("invoiceNo");
            String description = request.getParameter("description");
            String vendorId = request.getParameter("vendorId");
            String contractTypeId = request.getParameter("contractTypeId");
            String vendorType = request.getParameter("vendorType");
            //status = financeBP.saveVendorPayment(financeTO, userId, vendorId, vendorType, contractTypeId, invoiceNo, invoiceDate, invoiceAmount, description);
            System.out.println("status" + status);
            response.setContentType("text/css");
            if (status > 0) {
                response.getWriter().println(status);
            } else {
                response.getWriter().println(0);
            }
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
    }

    public void getNextLevelList(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException {
        String path = "";
        ArrayList groupList = new ArrayList();
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Level Master";
        menuPath = "Finance  >> Level Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        PrintWriter pw = response.getWriter();
        try {
            response.setContentType("text/html");
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            ArrayList nextLevelMasterList = new ArrayList();
            String levelId = request.getParameter("levelId");
            nextLevelMasterList = financeBP.getNextLevelList(levelId);
            request.setAttribute("nextLevelMasterList", nextLevelMasterList);
            FinanceTO financeTO = new FinanceTO();
            JSONArray jsonArray = new JSONArray();
            Iterator itr = nextLevelMasterList.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                financeTO = (FinanceTO) itr.next();
                jsonObject.put("groupId", financeTO.getGroupid());
                jsonObject.put("groupName", financeTO.getGroupname());
                jsonObject.put("credit", financeTO.getCredit());
                jsonObject.put("debit", financeTO.getDebit());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);
            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }

    }

    public void getAccountTypeDetails(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException {
        String path = "";
        HttpSession session = request.getSession();
        financeCommand = command;
        FinanceTO financeTO = new FinanceTO();

        String menuPath = "";
        String pageTitle = " Payment Entry";
        menuPath = "Operation >> Payment Entry";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        PrintWriter pw = response.getWriter();
        ArrayList paymentList = new ArrayList();

        try {

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/PaymentEntry.jsp";

            String accountType = request.getParameter("accountType");
            String searchCode = request.getParameter("searchCode");
            System.out.println("accountType" + accountType);

            paymentList = financeBP.getAccountTypeDetails(accountType, searchCode);

            JSONArray jsonArray = new JSONArray();
            Iterator itr = paymentList.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                financeTO = (FinanceTO) itr.next();
                jsonObject.put("searchCode", financeTO.getSearchCode());
                jsonObject.put("tripId", financeTO.getTripId());
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);
            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }

    }

    public ModelAndView viewTrialBalance(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList ledgerList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> >> Reports >> Trial Balance  ";
        String pageTitle = "Manage Level";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/Finance/viewTrialBalance.jsp";
            ArrayList primaryLevelMasterList = new ArrayList();
            primaryLevelMasterList = financeBP.getPrimaryLevelMasterList();
            request.setAttribute("primaryLevelMasterList", primaryLevelMasterList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //pavi
    public ModelAndView handleCRJPayment(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankPaymentLedgerList = new ArrayList();

        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View CRJ";
        menuPath = "Finance  >> View CRJ Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        FinanceTO financeTO = new FinanceTO();
        try {
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            if (financeCommand.getVendorName() != null && financeCommand.getVendorName() != "") {
                financeTO.setVendorName(financeCommand.getVendorName());
            }

            String vendorTypeId = request.getParameter("vendorType");
            String crjCode = request.getParameter("crjCode");
            financeTO.setFromDate(fromDate);
            financeTO.setToDate(toDate);
            financeTO.setVendorTypeId(vendorTypeId);
            financeTO.setCrjCode(crjCode);

            ArrayList vendorList = new ArrayList();
            vendorList = operationBP.processVendorList();//0 indicates all customers
            request.setAttribute("vendorList", vendorList);

            ArrayList VendorTypeList = new ArrayList();
            VendorTypeList = vendorBP.processGetVendorTypeList();
            request.setAttribute("VendorTypeList", VendorTypeList);

            ArrayList CRJDetailList = new ArrayList();
            CRJDetailList = financeBP.getCRJDetailList(financeTO);

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("vendorTypeId", vendorTypeId);
            request.setAttribute("crjCode", crjCode);

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/viewCRJ.jsp";

            request.setAttribute("CRJDetailList", CRJDetailList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void insertCRJ(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        financeCommand = command;
        HttpSession session = request.getSession();
        System.out.println("pavi......");
        FinanceTO financeTO = new FinanceTO();
        int userId = (Integer) session.getAttribute("userId");

        String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        String newFileName = "", actualFilePath = "";
        int i = 0;
        int k = 0;
        int j = 0;
        String[] fileSaved = new String[10];
        String[] uploadedFileName = new String[10];
        String[] tempFilePath = new String[10];
        PrintWriter pw = response.getWriter();

        System.out.println("hai");
        String vendorTypeId = request.getParameter("vendorTypeId");
        System.out.println("vendorTypeId");
        String vendorName = request.getParameter("vendorNameId");
        String temp[] = vendorName.split("~");
        String vendorNameId = temp[0];
        String ledgerId = temp[1];
        System.out.println("ledgerId" + ledgerId);
        System.out.println("vendorNameId" + vendorNameId);
        String crjDate = request.getParameter("crjDate");
        String invoiceNo = request.getParameter("invoiceNo");
        String invoiceDate = request.getParameter("invoiceDate");
        String invoiceAmount = request.getParameter("invoiceAmount");
        String remarks = request.getParameter("remarks");
        String debitLedgerId = request.getParameter("debitLedgerId");
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        Part partObj = null;
        FilePart fPart = null;
        if (isMultipart) {
            // Create a factory for disk-based file items
            FileItemFactory factory = new DiskFileItemFactory();

            // Create a new file upload handler
            ServletFileUpload upload = new ServletFileUpload(factory);
            try {
                // Parse the request
                List /* FileItem */ items = upload.parseRequest(request);
                Iterator iterator = items.iterator();
                while (iterator.hasNext()) {
                    if (k == 0) {
                        FileItem item = (FileItem) iterator.next();
                        if (!item.isFormField()) {
                            fileName = item.getName();
                            System.out.println("fileName" + fileName);
                            String root = getServletContext().getRealPath("/");
                            File path = new File(root + "/uploads");
                            if (!path.exists()) {
                                boolean status = path.mkdirs();
                            }

                            File uploadedFile = new File(path + "/" + fileName);
                            System.out.println("uploadedFile" + uploadedFile);
                            item.write(uploadedFile);
                            response.setContentType("text/css");
                            response.getWriter().println(uploadedFile);
                            tempFilePath[j] = tempServerFilePath + "//" + uploadedFile;
                            actualFilePath = actualServerFilePath + "//" + tempFilePath;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
//                            long fileSize = fPart.writeTo(new java.io.File(tempFilePath[j]));
//                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath[j]));
                            System.out.println("tempPath = " + tempFilePath);
//                            System.out.println("actPath = " + actualFilePath)
                            System.out.println(uploadedFile.getAbsolutePath());
                        }
                        String file = "";
                        String saveFile = "";
                        file = tempFilePath[j];
                        saveFile = fileSaved[j];
                        int crjId = 0;
                        crjId = financeBP.insertCRJ(financeTO, userId, vendorTypeId, vendorNameId, crjDate, invoiceNo, invoiceDate,
                                invoiceAmount, remarks, ledgerId, file, fileName, debitLedgerId);
                        System.out.println("crjId controller" + crjId);
                        response.setContentType("text/css");
                        JSONArray jsonArray = new JSONArray();
                        JSONObject jsonObject = new JSONObject();
                        if (crjId > 0) {
                            jsonObject.put("CrjId", crjId);

//                response.getWriter().println(crjId);
                        } else {
                            jsonObject.put("CrjId", 0);
//                response.getWriter().println(0);
                        }
                        jsonArray.put(jsonObject);
                        System.out.println("jsonObject = " + jsonObject);
                        pw.print(jsonArray);
                    }
                }
            } catch (FileUploadException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void getBankBranchDetails(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException {
        HttpSession session = request.getSession();
        financeCommand = command;
        FinanceTO financeTO = new FinanceTO();
        ArrayList getLedgerDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String bankId = request.getParameter("bankId");

            if (bankId != null && !bankId.equals("")) {
                financeTO.setBankId(bankId);
            }

            ArrayList bankBranchLists = financeBP.getBankList();
            bankBranchLists = financeBP.getBankBranchList(financeTO);
            request.setAttribute("bankBranchLists", bankBranchLists);
            ;
            JSONArray jsonArray = new JSONArray();
            Iterator itr = bankBranchLists.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                financeTO = (FinanceTO) itr.next();
                jsonObject.put("bankLedgerId", financeTO.getLedgerId());
                jsonObject.put("bankLedgerCode", financeTO.getBankMappingCode());
                jsonObject.put("branchId", financeTO.getBranchId());
                jsonObject.put("branchName", financeTO.getBranchName());
                jsonObject.put("bankName", financeTO.getBranchName());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView bankPaymentClearance(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankPaymentLedgerList = new ArrayList();

        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View Payements";
        menuPath = "Finance  >> View Payments Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        FinanceTO financeTO = new FinanceTO();
        try {
            System.out.println("hai");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            String bankId = request.getParameter("bankId");
            String bankBranchId = request.getParameter("bankBranchId");
            String referenceCode = request.getParameter("referenceCode");
            financeTO.setFromDate(fromDate);
            financeTO.setToDate(toDate);
            financeTO.setBankId(bankId);
            if (!"".equals(bankBranchId) && bankBranchId != null) {
                String temp[] = bankBranchId.split("~");
                financeTO.setBranchId(temp[0]);
            } else {
                financeTO.setBranchId("0");
            }
            financeTO.setReferenceNo(referenceCode);
            ArrayList primarybankList = new ArrayList();
            primarybankList = financeBP.getPrimaryBankList();
            request.setAttribute("primarybankList", primarybankList);

            ArrayList BPClearanceDetails = new ArrayList();
            BPClearanceDetails = financeBP.getBPClearanceDetails(financeTO);
            request.setAttribute("BPClearanceDetails", BPClearanceDetails);

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("bankId", bankId);
            request.setAttribute("bankBranchId", bankBranchId);
            request.setAttribute("referenceCode", referenceCode);

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/bankPaymentClearance.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void clearChequeUpdate(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        financeCommand = command;
        HttpSession session = request.getSession();
        System.out.println("pavi......");
        FinanceTO financeTO = new FinanceTO();
        int userId = (Integer) session.getAttribute("userId");
        int status = 0;
        try {
            System.out.println("hai");
            String clearanceDate = request.getParameter("clearanceDate");
            String clearanceStatus = request.getParameter("clearanceStatus");
            String clearanceremark = request.getParameter("clearanceremark");
            System.out.println("clearanceDate" + clearanceDate);

            String bankPaymentId = request.getParameter("bankPaymentId");
            String debitLedgerId = request.getParameter("debitLedgerId");
            String creditLedgerId = request.getParameter("creditLedgerId");
            String chequeAmount = request.getParameter("chequeAmount");
            String chequeNo = request.getParameter("chequeNo");
            String chequeDate = request.getParameter("chequeDate");
            String entryType = request.getParameter("entryType");
            String formReferenceCode = request.getParameter("formReferenceCode");

            status = financeBP.updateClearnceDate(financeTO, userId, clearanceDate, clearanceStatus, clearanceremark, bankPaymentId, debitLedgerId, creditLedgerId, chequeAmount,
                    chequeNo, chequeDate, entryType, formReferenceCode);
            System.out.println("clearChequeUpdate" + status);
            response.setContentType("text/css");
            if (status > 0) {
                response.getWriter().println(status);
            } else {
                response.getWriter().println(0);
            }
        } catch (UnknownHostException ex) {
            //ex.printStackTrace();
        }
    }

    public ModelAndView bankReconciliationStatement(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Bank Reconciliation Statement";
        menuPath = "Finance  >> Bank Reconciliation Statement";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/bankReconciliationStatement.jsp";
            ArrayList bankList = new ArrayList();
            bankList = financeBP.getPrimaryBankList();
            request.setAttribute("bankList", bankList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception bankReconciliationStatement --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView searchBRSDetails(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Bank Reconciliation Statement";
        menuPath = "Finance  >> Bank Reconciliation Statement";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        FinanceTO financeTO = new FinanceTO();
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            String bankId = request.getParameter("bankId");
            String bankBranchId = request.getParameter("bankBranchId");
            String temp[] = bankBranchId.split("~");
            String branchId = temp[0];
            financeTO.setFromDate(fromDate);
            financeTO.setToDate(toDate);
            financeTO.setBranchId(branchId);
            financeTO.setBankid(bankId);
            financeTO.setBankLedgerId(temp[1]);
            ArrayList chequeIssusedList = new ArrayList();
            chequeIssusedList = financeBP.getBRSChequeList(financeTO, "PAYMENT");
            request.setAttribute("chequeIssusedList", chequeIssusedList);

            ArrayList chequeReceivedList = new ArrayList();
            chequeReceivedList = financeBP.getBRSChequeList(financeTO, "RECEIPT");
            request.setAttribute("chequeReceivedList", chequeReceivedList);

            String bankBalance = financeBP.getBankBalance(financeTO);
            if (bankBalance != null) {
                request.setAttribute("bankBalance", bankBalance);
            } else {
                request.setAttribute("bankBalance", "0");
            }

            request.setAttribute("bankId", bankId);
            request.setAttribute("bankBranchId", bankBranchId);

            path = "content/Finance/bankReconciliationStatement.jsp";

            ArrayList bankList = new ArrayList();
            bankList = financeBP.getPrimaryBankList();
            request.setAttribute("bankList", bankList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception bankReconciliationStatement --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public void insertBRSDetails(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        financeCommand = command;
        HttpSession session = request.getSession();
        System.out.println("pavi......");
        FinanceTO financeTO = new FinanceTO();
        int userId = (Integer) session.getAttribute("userId");
        int BRSid = 0;
        try {
            System.out.println("hai");
            String fromDate = request.getParameter("fromDate");
            String bankBranchId = request.getParameter("bankBranchId");
            String temp[] = bankBranchId.split("~");
            String branchId = temp[0];
            String bankledgerId = temp[1];
            String toDate = request.getParameter("toDate");
            String bankId = request.getParameter("bankId");
            String perStatement = request.getParameter("perStatement");
            String bankBalance = request.getParameter("bankBalance");
            String diffrenceAmt = request.getParameter("diffrenceAmt");
            String grandTotal = request.getParameter("grandTotal");
            String totalReceivedAmount = request.getParameter("totalReceivedAmount");
            String totalIssuedAmount = request.getParameter("totalIssuedAmount");
            System.out.println("hai 2");

            String[] issuedVoucherCode = request.getParameterValues("issuedVoucherCodeval[]");
            String[] issuedLedgerNameId = request.getParameterValues("issuedLedgerNameIdval[]");
            String[] issuedChequeNo = request.getParameterValues("issuedChequeNoval[]");
            String[] issuedChequeDate = request.getParameterValues("issuedChequeDateval[]");
            String[] issuedClearanceDate = request.getParameterValues("issuedClearanceDateval[]");
            String[] issuedChequeAmount = request.getParameterValues("issuedChequeAmountval[]");
            String[] issuedClearanceId = request.getParameterValues("issuedClearanceIdval[]");
            System.out.println("hai 3");
            String[] receivedVoucherCode = request.getParameterValues("receivedVoucherCodeval[]");
            String[] receivedLedgerNameId = request.getParameterValues("receivedLedgerNameIdval[]");
            String[] receivedChequeNo = request.getParameterValues("receivedChequeNoval[]");
            String[] receivedChequeDate = request.getParameterValues("receivedChequeDateval[]");
            String[] receivedClearanceDate = request.getParameterValues("receivedClearanceDateval[]");
            String[] receivedChequeAmount = request.getParameterValues("receivedChequeAmountval[]");
            String[] receivedClearanceId = request.getParameterValues("receivedClearanceIdval[]");

            //credit bank
            String[] creditPaidTo = request.getParameterValues("creditPaidToVal[]");
            String[] creditDate = request.getParameterValues("creditDateVal[]");
            String[] creditChequeNo = request.getParameterValues("creditChequeNoVal[]");
            String[] creditChequeDate = request.getParameterValues("creditChequeDateVal[]");
            String[] creditReference = request.getParameterValues("creditReferenceVal[]");
            String[] creditAmount = request.getParameterValues("creditAmountsVal[]");
            System.out.println("hai 4");
            //debit bank
            String[] debitReceivedFrom = request.getParameterValues("debitReceivedFromVal[]");
            String[] debitDate = request.getParameterValues("debitDateVal[]");
            String[] debitChequeNo = request.getParameterValues("debitChequeNoVal[]");
            String[] debitChequeDate = request.getParameterValues("debitChequeDateVal[]");
            String[] debitReference = request.getParameterValues("debitReferenceVal[]");
            String[] debitAmount = request.getParameterValues("debitAmountVal[]");

            System.out.println("hai 5");

            BRSid = financeBP.insertBRSDetails(financeTO, userId, fromDate, branchId, bankledgerId, toDate, perStatement,
                    bankBalance, diffrenceAmt, grandTotal, totalReceivedAmount, totalIssuedAmount, bankId, issuedVoucherCode, issuedLedgerNameId, issuedChequeNo, issuedChequeDate, issuedClearanceDate, issuedChequeAmount,
                    receivedVoucherCode, receivedLedgerNameId, receivedChequeNo, receivedChequeDate, receivedClearanceDate, receivedChequeAmount, issuedClearanceId, receivedClearanceId,
                    creditPaidTo, creditDate, creditChequeNo, creditChequeDate, creditReference, creditAmount,
                    debitReceivedFrom, debitDate, debitChequeNo, debitChequeDate, debitReference, debitAmount);
            System.out.println("BRSid" + BRSid);
            response.setContentType("text/css");
            if (BRSid > 0) {
                response.getWriter().println(BRSid);
            } else {
                response.getWriter().println(0);
            }
        } catch (UnknownHostException ex) {
            //ex.printStackTrace();
        }
    }

    public ModelAndView dayBook(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList ledgerList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        String[] temp = null;
        String sDate = "";
        String eDate = "";
        menuPath = "Finance >> >> Reports >> DayBook  ";
        String pageTitle = "Manage Level";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            //pavi
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            System.out.println("fromDate:" + fromDate);
            System.out.println("toDate:" + toDate);
            ArrayList totalAmountList = new ArrayList();
            double totalDebitAmount = 0;
            double totalCreditAmount = 0;
            if (fromDate != null && !"".equals(fromDate)) {
                totalAmountList = financeBP.getDayBookAmountList(fromDate, toDate);
            } else {
                totalAmountList = financeBP.getDayBookAmountList("", "");
            }
            Iterator itr = totalAmountList.iterator();
            while (itr.hasNext()) {
                financeTO = (FinanceTO) itr.next();
                if ("DEBIT".equals(financeTO.getAccountType())) {
                    totalDebitAmount = Double.parseDouble(financeTO.getAccountAmount()) + totalDebitAmount;
//                    totalDebitAmount = Float.parseFloat(financeTO.getAccountAmount()) + totalDebitAmount;
                } else if ("CREDIT".equals(financeTO.getAccountType())) {
                    totalCreditAmount = Double.parseDouble(financeTO.getAccountAmount()) + totalCreditAmount;
//                    totalCreditAmount = Float.parseFloat(financeTO.getAccountAmount()) + totalCreditAmount;
                }
            }
            request.setAttribute("totalAmountList", totalAmountList);
            request.setAttribute("totalCreditAmount", totalCreditAmount);
            request.setAttribute("totalDebitAmount", totalDebitAmount);

            String param = request.getParameter("param");
            if (param.equals("search")) {
                path = "content/Finance/dayBook.jsp";

                //
                int totalRecords = 0;
                int pageNo = 0;
                int totalPages = 0;
                PaginationHelper pagenation = new PaginationHelper();
                int startIndex = 0;
                int endIndex = 0;

                //
                ArrayList ledgerTransactionList = new ArrayList();
                if (fromDate != null && !"".equals(fromDate)) {
                    request.setAttribute("fromDate", fromDate);
                    request.setAttribute("toDate", toDate);
                    ledgerTransactionList = financeBP.getDayBook(fromDate, toDate, startIndex, endIndex, financeTO);
                } else {
                    ledgerTransactionList = financeBP.getDayBook("", "", startIndex, endIndex, financeTO);
                }
                System.out.println("ledgerTransactionList.lenth() = " + ledgerTransactionList.size());
                totalRecords = ledgerTransactionList.size();
                session.setAttribute("totalRecords", totalRecords);
                pagenation.setTotalRecords(totalRecords);

                String buttonClicked = "";
                if (request.getParameter("button") != null && request.getParameter("button") != "") {
                    buttonClicked = request.getParameter("button");
                    System.out.println("button0" + buttonClicked);
                }
                if (request.getParameter("pageNo") != null && request.getParameter("pageNo") != "") {
                    pageNo = Integer.parseInt(request.getParameter("pageNo"));
                    System.out.println("pageNo" + pageNo);
                }

                pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
                totalPages = pagenation.getTotalNoOfPages();
                startIndex = pagenation.getStartIndex();
                endIndex = pagenation.getEndIndex();
                System.out.println("startIndex +++++++++" + startIndex);
                System.out.println("endIndex +++++++++" + endIndex);

                if (fromDate != null && !"".equals(fromDate)) {
                    request.setAttribute("fromDate", fromDate);
                    request.setAttribute("toDate", toDate);
                    ledgerTransactionList = financeBP.getDayBook(fromDate, toDate, startIndex, endIndex, financeTO);
                } else {
                    ledgerTransactionList = financeBP.getDayBook("", "", startIndex, endIndex, financeTO);
                }
                if (totalRecords <= 0) {
                    ledgerTransactionList = null;
                }
                request.setAttribute("pageNo", pageNo);
                request.setAttribute("totalPages", totalPages);
                request.setAttribute("ledgerTransactionList", ledgerTransactionList);
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            } else if (param.equals("export")) {
                path = "content/Finance/dayBookExcel.jsp";

                ArrayList ledgerTransactionList = new ArrayList();
                if (fromDate != null && !"".equals(fromDate)) {
                    request.setAttribute("fromDate", fromDate);
                    request.setAttribute("toDate", toDate);
                    ledgerTransactionList = financeBP.getDayBook(fromDate, toDate, 0, 0, financeTO);
                } else {
                    ledgerTransactionList = financeBP.getDayBook("", "", 0, 0, financeTO);
                }
                System.out.println("ledgerTransactionList.lenth() = " + ledgerTransactionList.size());
                request.setAttribute("ledgerTransactionList", ledgerTransactionList);

            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView dayBookSummary(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList ledgerList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> >> Reports >> Day Book";
        String pageTitle = "Manage Level";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            //pavi
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            System.out.println("fromDate" + fromDate);
            System.out.println("toDate" + toDate);
            String param = request.getParameter("param");

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            if (param.equals("search")) {
                path = "content/Finance/dayBookSummary.jsp";
            } else if (param.equals("export")) {
                path = "content/Finance/dayBookSummaryExcel.jsp";
            }
            ArrayList voucherMasterList = financeBP.getVoucherMasterList();
            System.out.println("voucherMasterList.lenth() = " + voucherMasterList.size());
            request.setAttribute("voucherMasterList", voucherMasterList);

            ArrayList ledgerTransactionList = financeBP.getTBTransactionDayBkSummary(fromDate, toDate, financeTO);
            System.out.println("ledgerTransactionList.lenth() = " + ledgerTransactionList.size());
            //for ledger
            ArrayList ledgerTransactionListNew = new ArrayList();
            String creditAmmount;
            Iterator itr = ledgerTransactionList.iterator();
            FinanceTO ftTo = null;
            while (itr.hasNext()) {
                ftTo = new FinanceTO();
                financeTO = (FinanceTO) itr.next();
                if (financeTO.getOpeningBalance().contains("-")) {
                    creditAmmount = financeTO.getOpeningBalance();
                    String temp[] = creditAmmount.split("-");
                    ftTo.setCreditAmmount(temp[1]);
                    ftTo.setDebitAmmount("0.00");
                } else {
                    ftTo.setDebitAmmount(financeTO.getOpeningBalance());
                    ftTo.setCreditAmmount("0.00");
                }
                ftTo.setLedgerName(financeTO.getLedgerName());
                ftTo.setLevelID(financeTO.getLevelID());
                ftTo.setFormId(financeTO.getFormId());
                ledgerTransactionListNew.add(ftTo);
            }
            System.out.println("ledgerTransactionListNew size sec" + ledgerTransactionListNew.size());
            request.setAttribute("ledgerTransactionList", ledgerTransactionListNew);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView trialBalanceNew(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList ledgerList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> >> Reports >> Trial Balance  ";
        String pageTitle = "Manage Level";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            //pavi
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            System.out.println("fromDate" + fromDate);
            System.out.println("toDate" + toDate);
            String param = request.getParameter("param");
            String val = "";

//            if (fromDate != null && !"".equals(fromDate)) {
//
//                if (param.equals("search")) {
//                    path = "content/Finance/trialBalanceDate.jsp";
//                } else if (param.equals("export")) {
//                    path = "content/Finance/trialBalanceDateExcel.jsp";
//                }
//                val = "dateWise";
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
//            } else {
            if (param.equals("search")) {
                path = "content/Finance/trialBalanceNew.jsp";
            } else if (param.equals("export")) {
                path = "content/Finance/trialBalanceExcel.jsp";
            }
            val = "summary";
//            }

            ArrayList levelMasterList = new ArrayList();
            levelMasterList = getLevelCompleteForTrialBalanceNew(fromDate, toDate);
            request.setAttribute("levelMasterList", levelMasterList);

            ArrayList ledgerTransactionList = financeBP.getTBTransactionNewList("", fromDate, toDate, val, financeTO);
            System.out.println("ledgerTransactionList.lenth() = " + ledgerTransactionList.size());
            //for ledger
            String creditAmmount;
            Iterator itr = ledgerTransactionList.iterator();
            FinanceTO ftTo = null;
            ArrayList ledgerTransactionListNew = new ArrayList();
            while (itr.hasNext()) {
                ftTo = new FinanceTO();
                financeTO = (FinanceTO) itr.next();
                if (financeTO.getOpeningBalance().contains("-")) {
                    creditAmmount = financeTO.getOpeningBalance();
                    String temp[] = creditAmmount.split("-");
                    ftTo.setCreditAmmount(temp[1]);
                    ftTo.setDebitAmmount("0.00");
                } else {
                    ftTo.setDebitAmmount(financeTO.getOpeningBalance());
                    ftTo.setCreditAmmount("0.00");
                }
                ftTo.setLedgerName(financeTO.getLedgerName());
                ftTo.setLevelID(financeTO.getLevelID());
                ledgerTransactionListNew.add(ftTo);
            }
            System.out.println("ledgerTransactionListNew size sec" + ledgerTransactionListNew.size());
            request.setAttribute("ledgerTransactionList", ledgerTransactionListNew);
            //for Ledger

            //for Group
            ArrayList groupLedgerList = new ArrayList();
            ArrayList getPrimaryHistoryList = new ArrayList();
            getPrimaryHistoryList = financeBP.getPrimaryHistoryList(toDate);
            System.out.println("getPrimaryHistoryList size" + getPrimaryHistoryList.size());
            double debitGroup = 0.0f;
            double creditGroup = 0.0f;
            String creditAmmount1;
            String groupid = "0";
            Iterator itr1 = getPrimaryHistoryList.iterator();
            FinanceTO ftTo1 = null;
            int index = 0;
            while (itr1.hasNext()) {
                ftTo1 = new FinanceTO();
                financeTO = (FinanceTO) itr1.next();
//                System.out.println("financeTO.getGroupCode()" + financeTO.getGroupCode());
                if (index == 0) {
                    groupid = financeTO.getGroupCode();
//                    System.out.println("groupid index"+groupid);
                }
//                System.out.println("Integer.parseInt(groupid) == Integer.parseInt(financeTO.getGroupCode()" + Integer.parseInt(groupid) + "---->" + financeTO.getGroupCode());
                if (Integer.parseInt(groupid) == Integer.parseInt(financeTO.getGroupCode())) {
                    if (financeTO.getOpeningBalance().contains("-")) {
                        creditAmmount1 = financeTO.getOpeningBalance();
                        String temp[] = creditAmmount1.split("-");
                        creditGroup = creditGroup + Double.parseDouble(temp[1]);
                    } else {
                        debitGroup = debitGroup + Double.parseDouble(financeTO.getOpeningBalance());
                    }
                    groupid = financeTO.getGroupCode();
                } else {
                    ftTo1.setGroupCode(groupid);
                    ftTo1.setCreditAmt(creditGroup);
                    ftTo1.setDebitAmt(debitGroup);
                    groupLedgerList.add(ftTo1);
                    debitGroup = 0.0f;
                    creditGroup = 0.0f;
                    if (financeTO.getOpeningBalance().contains("-")) {
                        creditAmmount1 = financeTO.getOpeningBalance();
                        String temp[] = creditAmmount1.split("-");
                        creditGroup = creditGroup + Double.parseDouble(temp[1]);
                    } else {
                        debitGroup = debitGroup + Double.parseDouble(financeTO.getOpeningBalance());
                    }
                    groupid = financeTO.getGroupCode();
//                      System.out.println("groupid else"+groupid);
                }
                index++;
                // System.out.println("index" + index + "  getPrimaryHistoryList.size()---->" + getPrimaryHistoryList.size());
                if (index == getPrimaryHistoryList.size()) {
                    ftTo1 = new FinanceTO();
                    ftTo1.setGroupCode(groupid);
                    ftTo1.setCreditAmt(creditGroup);
                    ftTo1.setDebitAmt(debitGroup);
                    groupLedgerList.add(ftTo1);
                }
            }
            System.out.println("groupLedgerList size sec" + groupLedgerList.size());
            request.setAttribute("groupLedgerList", groupLedgerList);
            // for group

            //level
            ArrayList getGroupHistoryList = new ArrayList();
            getGroupHistoryList = financeBP.getGroupHistoryList(toDate);
            System.out.println("getList size" + getGroupHistoryList.size());

            double debitSub = 0;
            double creditSub = 0.0f;
            String creditAmt;
            String subLevelId = "0";
            Iterator itr2 = getGroupHistoryList.iterator();
            FinanceTO ftTo2 = null;
            ArrayList subPrimaryLedgerList = new ArrayList();
            int index1 = 0;
            while (itr2.hasNext()) {
                ftTo2 = new FinanceTO();
                financeTO = (FinanceTO) itr2.next();
                if (index1 == 0) {
                    subLevelId = financeTO.getLevelID();
                }
                if (Integer.parseInt(subLevelId) == Integer.parseInt(financeTO.getLevelID())) {
                    if (financeTO.getOpeningBalance().contains("-")) {
                        creditAmmount = financeTO.getOpeningBalance();
                        String temp[] = creditAmmount.split("-");
                        creditSub = creditSub + Double.parseDouble(temp[1]);
                    } else {
                        debitSub = debitSub + Double.parseDouble(financeTO.getOpeningBalance());
                    }
                    subLevelId = financeTO.getLevelID();
                } else {
                    ftTo2.setLevelID(subLevelId);
                    ftTo2.setCreditAmt(creditSub);
                    ftTo2.setDebitAmt(debitSub);
//                     System.out.println("creditSub"+creditSub);
                    subPrimaryLedgerList.add(ftTo2);
                    debitSub = 0.0f;
                    creditSub = 0.0f;
                    if (financeTO.getOpeningBalance().contains("-")) {
                        creditAmmount = financeTO.getOpeningBalance();
                        String temp[] = creditAmmount.split("-");
                        creditSub = creditSub + Double.parseDouble(temp[1]);
                    } else {
                        debitSub = debitSub + Double.parseDouble(financeTO.getOpeningBalance());
                    }
                    subLevelId = financeTO.getLevelID();
                }
//                System.out.println("length"+subPrimaryLedgerList.size());
                index1++;
                if (index1 == getGroupHistoryList.size()) {
                    ftTo2 = new FinanceTO();
                    ftTo2.setLevelID(subLevelId);
                    ftTo2.setCreditAmt(creditSub);
                    ftTo2.setDebitAmt(debitSub);
                    subPrimaryLedgerList.add(ftTo2);
                }
            }
            System.out.println("subPrimaryLedgerList size sec" + subPrimaryLedgerList.size());
            request.setAttribute("subPrimaryLedgerList", subPrimaryLedgerList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ArrayList getLevelCompleteForTrialBalanceNew(String fromDate, String toDate) {
        System.out.println("in size....");
        ArrayList levelMasterList = new ArrayList();
        ArrayList level1 = new ArrayList();
        ArrayList level2 = new ArrayList();
        ArrayList level3 = new ArrayList();
        ArrayList level4 = new ArrayList();
        ArrayList level5 = new ArrayList();
        ArrayList level6 = new ArrayList();
        float debitnew = 0.0f;
        float creditnew = 0.0f;

        try {
            ArrayList primaryList = financeBP.handlePrimaryList(fromDate, toDate);
            System.out.println("catList.size(): " + primaryList.size());
            FinanceTO lData1 = null, rsParent = null;
            Iterator itr = primaryList.iterator();
            int cntr = 0;
            while (itr.hasNext()) { //running through roots
                System.out.println("1 = ");
                level2 = new ArrayList();
                level3 = new ArrayList();
                level4 = new ArrayList();
                level5 = new ArrayList();
                level6 = new ArrayList();
                lData1 = new FinanceTO();

                lData1 = (FinanceTO) itr.next();
                lData1.setLevelgroupId(lData1.getGroupID());
                lData1.setFromDate(fromDate);
                lData1.setToDate(toDate);
//                lData1.setCredit(lData1.getCredit());
//                lData1.setDebit(lData1.getDebit());
                System.out.println("lData1_primaryId: " + lData1.getGroupID());
                System.out.println("lData1_PrimaryName: " + lData1.getGroupName());
                cntr++;

                ArrayList levelList = financeBP.getLevelListNew(lData1);
                System.out.println("Top levelList.size(): " + levelList.size());

                //getting parents for each root
                Iterator itr1 = null;
                int cntr1 = 0;
                level2 = new ArrayList();
                if (levelList.size() > 0) {
                    itr1 = levelList.iterator();
                    FinanceTO lData2 = null;
                    while (itr1.hasNext()) { //running through parents
                        System.out.println("2 = ");
                        cntr1++;
                        lData2 = new FinanceTO();
                        lData2 = (FinanceTO) itr1.next();
                        System.out.println("lData2_levelID: " + lData2.getLevelgroupId());
                        System.out.println("lData2_levelName: " + lData2.getLevelgroupName());
                        System.out.println("lData2.getDebitAmount()****************************: " + lData2.getDebitAmount());
                        System.out.println("lData2.getCreditAmount()****************************: " + lData2.getCreditAmount());
                        lData2.setFromDate(fromDate);
                        lData2.setToDate(toDate);

                        ArrayList levelList2 = financeBP.getLevelListNew(lData2);
                        System.out.println("levelList2.size() = " + levelList2.size());
                        Iterator itr2 = null;
                        int cntr2 = 0;
                        level3 = new ArrayList();
                        if (levelList2.size() > 0) {
                            itr2 = levelList2.iterator();
                            FinanceTO lData3 = null;
                            while (itr2.hasNext()) { //running through children
                                System.out.println("3 = ");
                                cntr2++;
                                lData3 = new FinanceTO();
                                lData3 = (FinanceTO) itr2.next();
                                System.out.println("lData3_levelID: " + lData3.getLevelgroupId());
                                System.out.println("lData3_levelName: " + lData3.getLevelgroupName());
                                lData3.setFromDate(fromDate);
                                lData3.setToDate(toDate);

                                ArrayList levelList3 = financeBP.getLevelListNew(lData3);
                                System.out.println("levelList3.size() = " + levelList3.size());
                                Iterator itr3 = null;
                                int cntr3 = 0;
                                level4 = new ArrayList();
                                if (levelList3.size() > 0) {
                                    itr3 = levelList3.iterator();
                                    FinanceTO lData4 = null;
                                    while (itr3.hasNext()) {
                                        System.out.println("4 = ");
                                        cntr3++;
                                        lData4 = new FinanceTO();
                                        lData4 = (FinanceTO) itr3.next();
                                        System.out.println("lData4_levelID: " + lData4.getLevelgroupId());
                                        System.out.println("lData4_levelName: " + lData4.getLevelgroupName());
                                        lData4.setFromDate(fromDate);
                                        lData4.setToDate(toDate);

                                        ArrayList levelList4 = financeBP.getLevelListNew(lData4);
                                        System.out.println("levelList4.size() = " + levelList4.size());
                                        Iterator itr4 = null;
                                        int cntr4 = 0;
                                        level5 = new ArrayList();
                                        if (levelList4.size() > 0) {
                                            itr4 = levelList4.iterator();
                                            FinanceTO lData5 = null;
                                            while (itr4.hasNext()) {
                                                System.out.println("5 = ");
                                                cntr4++;
                                                lData5 = new FinanceTO();
                                                lData5 = (FinanceTO) itr4.next();
                                                System.out.println("lData5_levelID: " + lData5.getLevelgroupId());
                                                System.out.println("lData5_levelName: " + lData5.getLevelgroupName());
                                                lData5.setFromDate(fromDate);
                                                lData5.setToDate(toDate);
                                                ArrayList levelList5 = financeBP.getLevelListNew(lData5);
                                                System.out.println("levelList5.size() = " + levelList5.size());
                                                Iterator itr5 = null;
                                                int cntr5 = 0;
                                                level6 = new ArrayList();
                                                if (levelList5.size() > 0) {
                                                    System.out.println("6 = ");
                                                    itr5 = levelList5.iterator();
                                                    FinanceTO lData6 = null;
                                                    while (itr5.hasNext()) {
                                                        cntr5++;
                                                        lData6 = new FinanceTO();
                                                        lData6 = (FinanceTO) itr5.next();
                                                        level6.add(lData6);
                                                    }
                                                }
                                                if (cntr5 > 0) {
                                                    lData5.setChildList(level6);
                                                }
                                                level5.add(lData5);
                                            }
                                        }
                                        if (cntr4 > 0) {
                                            lData4.setChildList(level5);
                                        }
                                        level4.add(lData4);
                                    }
                                }
                                if (cntr3 > 0) {
                                    lData3.setChildList(level4);
                                }
                                level3.add(lData3);
                            }
                        }
                        if (cntr2 > 0) {
                            lData2.setChildList(level3);
                        }
                        level2.add(lData2);
                    }
                }
                if (cntr1 > 0) {
                    lData1.setChildList(level2);
                }
                level1.add(lData1);
            }

        } catch (Exception e) {
        }

        return level1;
    }

    public ModelAndView profitAndLoss(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList ledgerList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> >> Reports >> P & L  ";
        String pageTitle = "Manage Level";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            //pavi
            String param = request.getParameter("param");
            String val = "";
            if (param.equals("search")) {
                path = "content/Finance/profitAndLossNew.jsp";
//                path = "content/Finance/profitAndLoss.jsp";
            } else if (param.equals("export")) {
                path = "content/Finance/profitAndLossExcel.jsp";
            }

            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");

            ArrayList levelMasterList = new ArrayList();
            System.out.println("fromDate:" + fromDate);
            System.out.println("toDate:" + toDate);
            val = "summary";
//            if (fromDate != null && !"".equals(fromDate)) {
            levelMasterList = getLevelCompleteForTrialBalanceNew(fromDate, toDate);
//                levelMasterList = getLevelCompleteForTrialBalance(fromDate, toDate);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            //            }

            ArrayList ledgerTransactionList = financeBP.getTBTransactionNewList("", fromDate, toDate, val, financeTO);
            System.out.println("ledgerTransactionList.lenth() = " + ledgerTransactionList.size());
            request.setAttribute("ledgerTransactionList", ledgerTransactionList);

            request.setAttribute("levelMasterList", levelMasterList);

            //for ledger
            String creditAmmount;
            Iterator itr = ledgerTransactionList.iterator();
            FinanceTO ftTo = null;
            ArrayList ledgerTransactionListNew = new ArrayList();
            while (itr.hasNext()) {
                ftTo = new FinanceTO();
                financeTO = (FinanceTO) itr.next();
                if (financeTO.getOpeningBalance().contains("-")) {
                    creditAmmount = financeTO.getOpeningBalance();
                    String temp[] = creditAmmount.split("-");
                    ftTo.setCreditAmmount(temp[1]);
                    ftTo.setDebitAmmount("0.00");
                } else {
                    ftTo.setDebitAmmount(financeTO.getOpeningBalance());
                    ftTo.setCreditAmmount("0.00");
                }
                ftTo.setLedgerName(financeTO.getLedgerName());
                ftTo.setLevelID(financeTO.getLevelID());
                ledgerTransactionListNew.add(ftTo);
            }
            System.out.println("ledgerTransactionListNew size sec" + ledgerTransactionListNew.size());
            request.setAttribute("ledgerTransactionList", ledgerTransactionListNew);
            //for Ledger

            //for Group
            ArrayList groupLedgerList = new ArrayList();
            ArrayList getPrimaryHistoryList = new ArrayList();
            getPrimaryHistoryList = financeBP.getPrimaryHistoryList(toDate);
            System.out.println("getPrimaryHistoryList size" + getPrimaryHistoryList.size());
            double debitGroup = 0.0f;
            double creditGroup = 0.0f;
            String creditAmmount1;
            String groupid = "0";
            Iterator itr1 = getPrimaryHistoryList.iterator();
            FinanceTO ftTo1 = null;
            int index = 0;
            while (itr1.hasNext()) {
                ftTo1 = new FinanceTO();
                financeTO = (FinanceTO) itr1.next();
//                System.out.println("financeTO.getGroupCode()" + financeTO.getGroupCode());
                if (index == 0) {
                    groupid = financeTO.getGroupCode();
//                    System.out.println("groupid index"+groupid);
                }
//                System.out.println("Integer.parseInt(groupid) == Integer.parseInt(financeTO.getGroupCode()" + Integer.parseInt(groupid) + "---->" + financeTO.getGroupCode());
                if (Integer.parseInt(groupid) == Integer.parseInt(financeTO.getGroupCode())) {
                    if (financeTO.getOpeningBalance().contains("-")) {
                        creditAmmount1 = financeTO.getOpeningBalance();
                        String temp[] = creditAmmount1.split("-");
                        creditGroup = creditGroup + Double.parseDouble(temp[1]);
                    } else {
                        debitGroup = debitGroup + Double.parseDouble(financeTO.getOpeningBalance());
                    }
                    groupid = financeTO.getGroupCode();
                } else {
                    ftTo1.setGroupCode(groupid);
                    ftTo1.setCreditAmt(creditGroup);
                    ftTo1.setDebitAmt(debitGroup);
                    groupLedgerList.add(ftTo1);
                    debitGroup = 0.0f;
                    creditGroup = 0.0f;
                    if (financeTO.getOpeningBalance().contains("-")) {
                        creditAmmount1 = financeTO.getOpeningBalance();
                        String temp[] = creditAmmount1.split("-");
                        creditGroup = creditGroup + Double.parseDouble(temp[1]);
                    } else {
                        debitGroup = debitGroup + Double.parseDouble(financeTO.getOpeningBalance());
                    }
                    groupid = financeTO.getGroupCode();
//                      System.out.println("groupid else"+groupid);
                }
                index++;
                // System.out.println("index" + index + "  getPrimaryHistoryList.size()---->" + getPrimaryHistoryList.size());
                if (index == getPrimaryHistoryList.size()) {
                    ftTo1 = new FinanceTO();
                    ftTo1.setGroupCode(groupid);
                    ftTo1.setCreditAmt(creditGroup);
                    ftTo1.setDebitAmt(debitGroup);
                    groupLedgerList.add(ftTo1);
                }
            }
            System.out.println("groupLedgerList size sec" + groupLedgerList.size());
            request.setAttribute("groupLedgerList", groupLedgerList);
            // for group

            //level
            ArrayList getGroupHistoryList = new ArrayList();
            getGroupHistoryList = financeBP.getGroupHistoryList(toDate);
            System.out.println("getList size" + getGroupHistoryList.size());

            double debitSub = 0;
            double creditSub = 0.0f;
            String creditAmt;
            String subLevelId = "0";
            Iterator itr2 = getGroupHistoryList.iterator();
            FinanceTO ftTo2 = null;
            ArrayList subPrimaryLedgerList = new ArrayList();
            int index1 = 0;
            while (itr2.hasNext()) {
                ftTo2 = new FinanceTO();
                financeTO = (FinanceTO) itr2.next();
                if (index1 == 0) {
                    subLevelId = financeTO.getLevelID();
                }
                if (Integer.parseInt(subLevelId) == Integer.parseInt(financeTO.getLevelID())) {
                    if (financeTO.getOpeningBalance().contains("-")) {
                        creditAmmount = financeTO.getOpeningBalance();
                        String temp[] = creditAmmount.split("-");
                        creditSub = creditSub + Double.parseDouble(temp[1]);
                    } else {
                        debitSub = debitSub + Double.parseDouble(financeTO.getOpeningBalance());
                    }
                    subLevelId = financeTO.getLevelID();
                } else {
                    ftTo2.setLevelID(subLevelId);
                    ftTo2.setCreditAmt(creditSub);
                    ftTo2.setDebitAmt(debitSub);
//                     System.out.println("creditSub"+creditSub);
                    subPrimaryLedgerList.add(ftTo2);
                    debitSub = 0.0f;
                    creditSub = 0.0f;
                    if (financeTO.getOpeningBalance().contains("-")) {
                        creditAmmount = financeTO.getOpeningBalance();
                        String temp[] = creditAmmount.split("-");
                        creditSub = creditSub + Double.parseDouble(temp[1]);
                    } else {
                        debitSub = debitSub + Double.parseDouble(financeTO.getOpeningBalance());
                    }
                    subLevelId = financeTO.getLevelID();
                }
//                System.out.println("length"+subPrimaryLedgerList.size());
                index1++;
                if (index1 == getGroupHistoryList.size()) {
                    ftTo2 = new FinanceTO();
                    ftTo2.setLevelID(subLevelId);
                    ftTo2.setCreditAmt(creditSub);
                    ftTo2.setDebitAmt(debitSub);
                    subPrimaryLedgerList.add(ftTo2);
                }
            }
            System.out.println("subPrimaryLedgerList size sec" + subPrimaryLedgerList.size());
            request.setAttribute("subPrimaryLedgerList", subPrimaryLedgerList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView balanceSheet(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList ledgerList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> >> Reports >> Balance  Sheet";
        String pageTitle = "Manage Level";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            //pavi
            String param = request.getParameter("param");
            String val = "";
            if (param.equals("search")) {
                path = "content/Finance/balanceSheetNew.jsp";
//                path = "content/Finance/balanceSheet.jsp";
            } else if (param.equals("export")) {
                path = "content/Finance/balanceSheetExcel.jsp";
            }

            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");

            ArrayList levelMasterList = new ArrayList();
            System.out.println("fromDate:" + fromDate);
            System.out.println("toDate:" + toDate);
            val = "summary";
//            if (fromDate != null && !"".equals(fromDate)) {
            levelMasterList = getLevelCompleteForTrialBalanceNew(fromDate, toDate);
//                levelMasterList = getLevelCompleteForTrialBalance(fromDate, toDate);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
//            }
            ArrayList ledgerTransactionList = financeBP.getTBTransactionNewList("", fromDate, toDate, val, financeTO);
            System.out.println("ledgerTransactionList.lenth() = " + ledgerTransactionList.size());
            request.setAttribute("ledgerTransactionList", ledgerTransactionList);
            request.setAttribute("levelMasterList", levelMasterList);

            //for ledger
            String creditAmmount;
            Iterator itr = ledgerTransactionList.iterator();
            FinanceTO ftTo = null;
            ArrayList ledgerTransactionListNew = new ArrayList();
            while (itr.hasNext()) {
                ftTo = new FinanceTO();
                financeTO = (FinanceTO) itr.next();
                if (financeTO.getOpeningBalance().contains("-")) {
                    creditAmmount = financeTO.getOpeningBalance();
                    String temp[] = creditAmmount.split("-");
                    ftTo.setCreditAmmount(temp[1]);
                    ftTo.setDebitAmmount("0.00");
                } else {
                    ftTo.setDebitAmmount(financeTO.getOpeningBalance());
                    ftTo.setCreditAmmount("0.00");
                }
                ftTo.setLedgerName(financeTO.getLedgerName());
                ftTo.setLevelID(financeTO.getLevelID());
                ledgerTransactionListNew.add(ftTo);
            }
            System.out.println("ledgerTransactionListNew size sec" + ledgerTransactionListNew.size());
            request.setAttribute("ledgerTransactionList", ledgerTransactionListNew);
            //for Ledger

            //for Group
            ArrayList groupLedgerList = new ArrayList();
            ArrayList getPrimaryHistoryList = new ArrayList();
            getPrimaryHistoryList = financeBP.getPrimaryHistoryList(toDate);
            System.out.println("getPrimaryHistoryList size" + getPrimaryHistoryList.size());
            double debitGroup = 0.0f;
            double creditGroup = 0.0f;
            String creditAmmount1;
            String groupid = "0";
            Iterator itr1 = getPrimaryHistoryList.iterator();
            FinanceTO ftTo1 = null;
            int index = 0;
            while (itr1.hasNext()) {
                ftTo1 = new FinanceTO();
                financeTO = (FinanceTO) itr1.next();
//                System.out.println("financeTO.getGroupCode()" + financeTO.getGroupCode());
                if (index == 0) {
                    groupid = financeTO.getGroupCode();
//                    System.out.println("groupid index"+groupid);
                }
//                System.out.println("Integer.parseInt(groupid) == Integer.parseInt(financeTO.getGroupCode()" + Integer.parseInt(groupid) + "---->" + financeTO.getGroupCode());
                if (Integer.parseInt(groupid) == Integer.parseInt(financeTO.getGroupCode())) {
                    if (financeTO.getOpeningBalance().contains("-")) {
                        creditAmmount1 = financeTO.getOpeningBalance();
                        String temp[] = creditAmmount1.split("-");
                        creditGroup = creditGroup + Double.parseDouble(temp[1]);
                    } else {
                        debitGroup = debitGroup + Double.parseDouble(financeTO.getOpeningBalance());
                    }
                    groupid = financeTO.getGroupCode();
                } else {
                    ftTo1.setGroupCode(groupid);
                    ftTo1.setCreditAmt(creditGroup);
                    ftTo1.setDebitAmt(debitGroup);
                    groupLedgerList.add(ftTo1);
                    debitGroup = 0.0f;
                    creditGroup = 0.0f;
                    if (financeTO.getOpeningBalance().contains("-")) {
                        creditAmmount1 = financeTO.getOpeningBalance();
                        String temp[] = creditAmmount1.split("-");
                        creditGroup = creditGroup + Double.parseDouble(temp[1]);
                    } else {
                        debitGroup = debitGroup + Double.parseDouble(financeTO.getOpeningBalance());
                    }
                    groupid = financeTO.getGroupCode();
//                      System.out.println("groupid else"+groupid);
                }
                index++;
                // System.out.println("index" + index + "  getPrimaryHistoryList.size()---->" + getPrimaryHistoryList.size());
                if (index == getPrimaryHistoryList.size()) {
                    ftTo1 = new FinanceTO();
                    ftTo1.setGroupCode(groupid);
                    ftTo1.setCreditAmt(creditGroup);
                    ftTo1.setDebitAmt(debitGroup);
                    groupLedgerList.add(ftTo1);
                }
            }
            System.out.println("groupLedgerList size sec" + groupLedgerList.size());
            request.setAttribute("groupLedgerList", groupLedgerList);
            // for group

            //level
            ArrayList getGroupHistoryList = new ArrayList();
            getGroupHistoryList = financeBP.getGroupHistoryList(toDate);
            System.out.println("getList size" + getGroupHistoryList.size());

            double debitSub = 0;
            double creditSub = 0.0f;
            String creditAmt;
            String subLevelId = "0";
            Iterator itr2 = getGroupHistoryList.iterator();
            FinanceTO ftTo2 = null;
            ArrayList subPrimaryLedgerList = new ArrayList();
            int index1 = 0;
            while (itr2.hasNext()) {
                ftTo2 = new FinanceTO();
                financeTO = (FinanceTO) itr2.next();
                if (index1 == 0) {
                    subLevelId = financeTO.getLevelID();
                }
                if (Integer.parseInt(subLevelId) == Integer.parseInt(financeTO.getLevelID())) {
                    if (financeTO.getOpeningBalance().contains("-")) {
                        creditAmmount = financeTO.getOpeningBalance();
                        String temp[] = creditAmmount.split("-");
                        creditSub = creditSub + Double.parseDouble(temp[1]);
                    } else {
                        debitSub = debitSub + Double.parseDouble(financeTO.getOpeningBalance());
                    }
                    subLevelId = financeTO.getLevelID();
                } else {
                    ftTo2.setLevelID(subLevelId);
                    ftTo2.setCreditAmt(creditSub);
                    ftTo2.setDebitAmt(debitSub);
//                     System.out.println("creditSub"+creditSub);
                    subPrimaryLedgerList.add(ftTo2);
                    debitSub = 0.0f;
                    creditSub = 0.0f;
                    if (financeTO.getOpeningBalance().contains("-")) {
                        creditAmmount = financeTO.getOpeningBalance();
                        String temp[] = creditAmmount.split("-");
                        creditSub = creditSub + Double.parseDouble(temp[1]);
                    } else {
                        debitSub = debitSub + Double.parseDouble(financeTO.getOpeningBalance());
                    }
                    subLevelId = financeTO.getLevelID();
                }
//                System.out.println("length"+subPrimaryLedgerList.size());
                index1++;
                if (index1 == getGroupHistoryList.size()) {
                    ftTo2 = new FinanceTO();
                    ftTo2.setLevelID(subLevelId);
                    ftTo2.setCreditAmt(creditSub);
                    ftTo2.setDebitAmt(debitSub);
                    subPrimaryLedgerList.add(ftTo2);
                }
            }
            System.out.println("subPrimaryLedgerList size sec" + subPrimaryLedgerList.size());
            request.setAttribute("subPrimaryLedgerList", subPrimaryLedgerList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView entriesReport(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Entries Report";
        menuPath = "Finance  >> Entries Report";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/entriesReport.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView searchEntriesDetails(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Entries Report";
        menuPath = "Finance  >> Entries Report";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList entriesReportList = new ArrayList();
        ArrayList entriesReportCreditList = new ArrayList();
        ArrayList entriesReportDebitList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        try {
            String param = request.getParameter("Param");
            System.out.println("param" + param);
            if ("ExportExcel".equals(param)) {
                path = "content/Finance/entriesExcelReport.jsp";
            } else {
                path = "content/Finance/entriesReport.jsp";
            }

            String entryType = request.getParameter("entryType");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            String ledgerId = request.getParameter("ledgerId");
            String ledgerName = request.getParameter("ledgerName");
            String distcheck = request.getParameter("distcheck");
            financeTO.setFromDate(fromDate);
            financeTO.setToDate(toDate);
            financeTO.setEntryType(entryType);
            financeTO.setLedgerId(ledgerId);

            entriesReportList = financeBP.getEntriesReport(financeTO, distcheck);
            System.out.println("entriesReportList.lenth() = " + entriesReportList.size());
            request.setAttribute("entriesReportList", entriesReportList);

            entriesReportCreditList = financeBP.getEntriesReportCreditList(financeTO, distcheck);
            System.out.println("entriesReportCreditList" + entriesReportCreditList.size());
            request.setAttribute("entriesReportCreditList", entriesReportCreditList);

            entriesReportDebitList = financeBP.getEntriesReportDebitList(financeTO, distcheck);
            System.out.println("entriesReportDebitList" + entriesReportDebitList.size());
            request.setAttribute("entriesReportDebitList", entriesReportDebitList);

            request.setAttribute("distcheck", distcheck);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("ledgerId", ledgerId);
            request.setAttribute("ledgerName", ledgerName);
            request.setAttribute("entryType", entryType);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleCostCenterReport(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Cost Center Report";
        menuPath = "Finance  >> Cost Center Report";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            System.out.println("hai");
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/costCenterReport.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView searchCostCenterDetails(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Cost Center Report";
        menuPath = "Finance  >> Cost Center Report";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList costCenterReportList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        try {
            String param = request.getParameter("Param");
            System.out.println("param" + param);
            if ("ExportExcel".equals(param)) {
                System.out.println("if");
                path = "content/Finance/costCenterExcelReport.jsp";
            } else {
                System.out.println("else");
                path = "content/Finance/costCenterReport.jsp";
            }

//            String voucherNo = request.getParameter("voucherNo");
            String vehicleId = request.getParameter("vehicleId");
            String regno = request.getParameter("regno");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            String ledgerId = request.getParameter("ledgerId");
            String ledgerName = request.getParameter("ledgerName");
            String distcheck = request.getParameter("distcheck");
            String amountMode = request.getParameter("amountMode");
            financeTO.setFromDate(fromDate);
            financeTO.setToDate(toDate);
//            financeTO.setVoucherCode(voucherNo);
            financeTO.setVehicleId(vehicleId);
            financeTO.setLedgerId(ledgerId);
            financeTO.setAccountType(amountMode);

            costCenterReportList = financeBP.getCostCenterReport(financeTO, distcheck);
            System.out.println("costCenterReportList.lenth() = " + costCenterReportList.size());
            request.setAttribute("costCenterReportList", costCenterReportList);

            request.setAttribute("distcheck", distcheck);
            request.setAttribute("vehicleId", vehicleId);
            request.setAttribute("regno", regno);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("ledgerId", ledgerId);
            request.setAttribute("ledgerName", ledgerName);
            request.setAttribute("amountMode", amountMode);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewTBDetailsNew(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList ledgerList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        String[] temp = null;
        String sDate = "";
        String eDate = "";
        menuPath = "Finance >> >> Reports >> TB Details  ";
        String pageTitle = "Manage Level";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String param = request.getParameter("param");
            String levelId = request.getParameter("levelId");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            System.out.println("toDate" + toDate);
            System.out.println("fromDate" + fromDate);
            String val = "popup";
            request.setAttribute("levelId", levelId);
            if (param == null) {
                request.setAttribute("parameterType", "search");
            } else {
                request.setAttribute("parameterType", param);
            }
            path = "content/Finance/viewTBDetailsNew.jsp";
            //
            int totalRecords = 0;
            int pageNo = 0;
            int totalPages = 0;
            PaginationHelper pagenation = new PaginationHelper();
            int startIndex = 0;
            int endIndex = 0;
            //
            financeTO.setStartIndex(startIndex);
            financeTO.setEndIndex(endIndex);
            ArrayList ledgerTransactionList = financeBP.getTBTransactionNewList(levelId, fromDate, toDate, val, financeTO);
            if (levelId != null && !"0".equals(levelId)) {
                System.out.println("ledgerTransactionList.lenth() = " + ledgerTransactionList.size());
                request.setAttribute("ledgerTransactionList", ledgerTransactionList);
            }
            System.out.println("ledgerTransactionList.lenth() = " + ledgerTransactionList.size());
            totalRecords = ledgerTransactionList.size();
            session.setAttribute("totalRecords", totalRecords);
            pagenation.setTotalRecords(totalRecords);

            String buttonClicked = "";
            if (request.getParameter("button") != null && request.getParameter("button") != "") {
                buttonClicked = request.getParameter("button");
                System.out.println("button0" + buttonClicked);
            }
            if (request.getParameter("pageNo") != null && request.getParameter("pageNo") != "") {
                pageNo = Integer.parseInt(request.getParameter("pageNo"));
                System.out.println("pageNo" + pageNo);
            }
            pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
            totalPages = pagenation.getTotalNoOfPages();
            startIndex = pagenation.getStartIndex();
            endIndex = pagenation.getEndIndex();
            System.out.println("startIndex +++++++++" + startIndex);
            System.out.println("endIndex +++++++++" + endIndex);
            financeTO.setStartIndex(startIndex);
            financeTO.setEndIndex(endIndex);
            if (levelId != null && !"0".equals(levelId)) {
                ledgerTransactionList = financeBP.getTBTransactionNewList(levelId, fromDate, toDate, val, financeTO);
                System.out.println("ledgerTransactionList.lenth() = " + ledgerTransactionList.size());
                request.setAttribute("ledgerTransactionList", ledgerTransactionList);
            }
            if (totalRecords <= 0) {
                ledgerTransactionList = null;
            }
//             BigDecimal debitNo = new BigDecimal(debitAmount);
//                    BigDecimal creditNo = new BigDecimal(creditAmount);
//                      debitNo=debitNo.setScale(2, BigDecimal.ROUND_UP);
//                                    creditNo=creditNo.setScale(2, BigDecimal.ROUND_UP);
            request.setAttribute("pageNo", pageNo);
            request.setAttribute("totalPages", totalPages);
            request.setAttribute("ledgerTransactionList", ledgerTransactionList);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleBankReconciliationReport(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Bank Reconciliation Statement";
        menuPath = "Finance  >> Bank Reconciliation Statement";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        FinanceTO financeTO = new FinanceTO();
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String param = request.getParameter("param");
            System.out.println("handleBankReconciliationReport" + param);
            if ("search".equalsIgnoreCase(param)) {
                System.out.println("if");
                String fromDate = request.getParameter("fromDate");
                String toDate = request.getParameter("toDate");
                String bankId = request.getParameter("bankId");
                String bankBranchId = request.getParameter("bankBranchId");
                String temp[] = bankBranchId.split("~");
                String branchId = temp[0];
                financeTO.setFromDate(fromDate);
                financeTO.setToDate(toDate);
                financeTO.setBranchId(branchId);
                financeTO.setBankid(bankId);
                financeTO.setBankLedgerId(temp[1]);

                ArrayList brsHeaderList = new ArrayList();
                brsHeaderList = financeBP.getBRSHeaderList(financeTO, "HEADER");
                request.setAttribute("brsHeaderList", brsHeaderList);
                String brsId = "0";
                Iterator itr = brsHeaderList.iterator();
                FinanceTO fto = null;
                while (itr.hasNext()) {
                    fto = new FinanceTO();
                    fto = (FinanceTO) itr.next();
                    System.out.println("fto.getBrsId():" + fto.getBrsId());
                    brsId = fto.getBrsId();
                }
                financeTO.setBrsId(brsId);
                ArrayList brsIssuedList = new ArrayList();
                brsIssuedList = financeBP.getBRSHeaderList(financeTO, "PAYMENT");
                request.setAttribute("brsIssuedList", brsIssuedList);

                ArrayList brsReceivedList = new ArrayList();
                brsReceivedList = financeBP.getBRSHeaderList(financeTO, "RECEIPT");
                request.setAttribute("brsReceivedList", brsReceivedList);

                ArrayList brsCreditList = new ArrayList();
                brsCreditList = financeBP.getBRSHeaderList(financeTO, "CREDIT");
                request.setAttribute("brsCreditList", brsCreditList);

                ArrayList brsDebitList = new ArrayList();
                brsDebitList = financeBP.getBRSHeaderList(financeTO, "DEBIT");
                request.setAttribute("brsDebitList", brsDebitList);

                path = "content/Finance/bankReconciliationReport.jsp";

                request.setAttribute("bankId", bankId);
                request.setAttribute("bankBranchId", bankBranchId);
            } else {
                System.out.println("else");
                path = "content/Finance/bankReconciliationReport.jsp";
                request.setAttribute("bankId", 0);
                request.setAttribute("bankBranchId", 0);
                request.setAttribute("brsDebitList", null);
                request.setAttribute("brsCreditList", null);
                request.setAttribute("brsReceivedList", null);
                request.setAttribute("brsIssuedList", null);
                request.setAttribute("brsHeaderList", null);
            }
            ArrayList bankList = new ArrayList();
            bankList = financeBP.getPrimaryBankList();
            request.setAttribute("bankList", bankList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception bankReconciliationReport --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView viewAddCRJ(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankPaymentLedgerList = new ArrayList();

        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Creditor Journal Voucher";
        menuPath = "Finance  >> Creditor Journal Voucher";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            ArrayList VendorTypeList = new ArrayList();
            VendorTypeList = vendorBP.processGetVendorTypeList();
            request.setAttribute("VendorTypeList", VendorTypeList);

//                ArrayList primarybankList = financeBP.getBankList();
//                primarybankList = financeBP.getPrimaryBankList();
//                request.setAttribute("primarybankList", primarybankList);
            ArrayList expenseGroupList = new ArrayList();
            expenseGroupList = financeBP.getExpenseGroupList();
            request.setAttribute("expenseGroupList", expenseGroupList);

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/handleCRJPayment.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getLedger(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException {
        HttpSession session = request.getSession();
        financeCommand = command;
        FinanceTO financeTO = new FinanceTO();
        ArrayList ledgerList = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String ledgerName = "";
            response.setContentType("text/html");
            ledgerName = request.getParameter("ledgerName");
            financeTO.setLedgerName(ledgerName);
            ledgerList = financeBP.getLedger(financeTO);
            System.out.println("ledgerList.size() = " + ledgerList.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = ledgerList.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                financeTO = (FinanceTO) itr.next();
                jsonObject.put("Name", financeTO.getLedgerId() + "~" + financeTO.getLedgerName());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getExpenseLedgerList(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException {
        HttpSession session = request.getSession();
        financeCommand = command;
        FinanceTO financeTO = new FinanceTO();
        ArrayList ledgerList = new ArrayList();
        System.out.println("cvsw");
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String levelId = request.getParameter("levelId");
            System.out.println("getExpenseLedgerList" + levelId);

            if (levelId != null && !levelId.equals("")) {
                financeTO.setLevelID(levelId);
            }

            ledgerList = financeBP.getExpenseLedgerList(financeTO);
            request.setAttribute("ledgerList", ledgerList);;
            JSONArray jsonArray = new JSONArray();
            Iterator itr = ledgerList.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                financeTO = (FinanceTO) itr.next();
                jsonObject.put("ledgerID", financeTO.getLedgerID());
                jsonObject.put("ledgerName", financeTO.getLedgerName());

                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView handleContractRatesApproval(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println("Contract rate approva");
        String path = "";
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        FinanceTO financeTO = new FinanceTO();
        String pageTitle = "Contract rate approval";
        menuPath = "Finance >>  Contract Rate Approval";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String fromDate = "";
        String toDate = "";

        try {

            path = "content/Finance/contractRatesApproval.jsp";
//            int companyId = (Integer) session.getAttribute("companyId");
            String companyId = (String) session.getAttribute("companyId");
            int userId = (Integer) session.getAttribute("userId");
            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");

            ArrayList contractRateApproveList = new ArrayList();
            contractRateApproveList = financeBP.getContractRateApproveList(companyId);
            request.setAttribute("contractRateApproveList", contractRateApproveList);
            System.out.println("contractRateApproveList Size Is :::" + contractRateApproveList.size());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView approveRejectContractRate(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println("Contract rate approva");
        String path = "";
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        FinanceTO financeTO = new FinanceTO();
        String pageTitle = "Contract rate approval";
        menuPath = "Finance >>  Contract Rate Approval";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {

            path = "content/Finance/contractRatesApproval.jsp";
            String companyId = (String) session.getAttribute("companyId");
            int userId = (Integer) session.getAttribute("userId");
            System.out.println("companyId" + companyId);
            String status = request.getParameter("status");
            String remarks = request.getParameter("remarks");
            String[] activeInd = request.getParameterValues("activeInd");
            String[] contractRateIds = request.getParameterValues("contractRateIds");
            System.out.println(" contractRateIds.length" + contractRateIds.length);
            for (int k = 0; k < contractRateIds.length; k++) {
                if ("Y".equals(activeInd[k])) {
                    int insertStatus = financeBP.saveApproveRejectContract(contractRateIds[k], status, remarks, userId);
                }
            }

            ArrayList contractRateApproveList = new ArrayList();
            contractRateApproveList = financeBP.getContractRateApproveList(companyId);
            request.setAttribute("contractRateApproveList", contractRateApproveList);
            System.out.println("contractRateApproveList Size Is :::" + contractRateApproveList.size());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVendorRatesApproval(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println("Contract rate approva");
        String path = "";
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        FinanceTO financeTO = new FinanceTO();
        String pageTitle = "Contract rate approval";
        menuPath = "Finance >>  Contract Rate Approval";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String fromDate = "";
        String toDate = "";

        try {

            path = "content/Finance/vendorRatesApproval.jsp";
//            int companyId = (Integer) session.getAttribute("companyId");
            String companyId = (String) session.getAttribute("companyId");
            int userId = (Integer) session.getAttribute("userId");
            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");

            ArrayList vendorRateApproveList = new ArrayList();
            vendorRateApproveList = financeBP.getVendorRateApproveList(companyId);
            request.setAttribute("vendorRateApproveList", vendorRateApproveList);
            System.out.println("vendorRateApproveList Size Is :::" + vendorRateApproveList.size());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView approveRejectVendorRate(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println("Vendor rate approval");
        String path = "";
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        FinanceTO financeTO = new FinanceTO();
        String pageTitle = "Contract rate approval";
        menuPath = "Finance >>  Contract Rate Approval";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {

            path = "content/Finance/vendorRatesApproval.jsp";
            String companyId = (String) session.getAttribute("companyId");
            int userId = (Integer) session.getAttribute("userId");
            System.out.println("companyId" + companyId);
            String status = request.getParameter("status");
            String remarks = request.getParameter("remarks");
            String[] activeInd = request.getParameterValues("activeInd");
            String[] contractRateIds = request.getParameterValues("contractRateIds");
            System.out.println(" contractRateIds.length" + contractRateIds.length);
            for (int k = 0; k < contractRateIds.length; k++) {
                if ("Y".equals(activeInd[k])) {
                    int insertStatus = financeBP.saveApproveRejectVendor(contractRateIds[k], status, remarks, userId);
                    System.out.println("insertStatus#@#@" + insertStatus);
                }
            }

            ArrayList vendorRateApproveList = new ArrayList();
            vendorRateApproveList = financeBP.getVendorRateApproveList(companyId);
            request.setAttribute("vendorRateApproveList", vendorRateApproveList);
            System.out.println("vendorRateApproveList Size Is :::" + vendorRateApproveList.size());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView vendorInvoiceReceipt(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankPaymentLedgerList = new ArrayList();

        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View VIR";
        menuPath = "Finance  >> View VIR Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        FinanceTO financeTO = new FinanceTO();
        ArrayList vendorList = new ArrayList();
        try {
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            String contractTypeId = "2";

            String vendorTypeId = ThrottleConstants.leasingVendorTypeId;
//            String vendorTypeId = request.getParameter("vendorType");
            String vendorName = request.getParameter("vendorName");
            String vendorId = "";

            String virCode = request.getParameter("virCode");
            if (!"".equals(vendorName) && vendorName != null) {
                String temp[] = vendorName.split("~");
                vendorId = temp[0];
                System.out.println("vendorNameId" + vendorId);
                request.setAttribute("vendorName", vendorName);
                financeTO.setVendorName(vendorId);
            }
            if (!"".equals(fromDate) && fromDate != null) {
                request.setAttribute("fromDate", fromDate);
                financeTO.setFromDate(fromDate);
            }
            if (!"".equals(toDate) && toDate != null) {
                request.setAttribute("toDate", toDate);
                financeTO.setToDate(toDate);
            }
            if (!"".equals(vendorTypeId) && vendorTypeId != null) {
                request.setAttribute("vendorType", vendorTypeId);
                financeTO.setVendorTypeId(vendorTypeId);
            }
            if (!"".equals(virCode) && virCode != null) {
                request.setAttribute("virCode", virCode);
                financeTO.setVirCode(virCode);
            }

            request.setAttribute("vendorList", null);

            vendorList = financeBP.processVendorList(vendorTypeId);
            request.setAttribute("vendorList", vendorList);

//            ArrayList VendorTypeList = new ArrayList();
//            VendorTypeList = vendorBP.processGetVendorTypeList();
//            request.setAttribute("VendorTypeList", VendorTypeList);
            financeTO.setContractTypeId(contractTypeId);
            ArrayList VIRdetailList = new ArrayList();
            VIRdetailList = financeBP.getVirdetailList(financeTO);

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/vendorInvoiceReceipt.jsp";

            request.setAttribute("VIRdetailList", VIRdetailList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView addVendorInvoiceReceipt(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Vendor Invoice Receipt";
        menuPath = "Finance  >>Add Vendor Invoice Receipt";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        FinanceTO financeTO = new FinanceTO();
        try {
            String param = request.getParameter("param");
            System.out.println("param" + param);
            ArrayList VendorTypeList = new ArrayList();
            VendorTypeList = vendorBP.processGetVendorTypeList();
            request.setAttribute("VendorTypeList", VendorTypeList);

//            ArrayList expenseGroupList = new ArrayList();
//            expenseGroupList = financeBP.getExpenseGroupList();
//            request.setAttribute("expenseGroupList", expenseGroupList);
            ArrayList vendorPaymentList = new ArrayList();
            ArrayList vendorPaymentListApproval = new ArrayList();
            ArrayList vendorList = new ArrayList();
            String vendorType = ThrottleConstants.leasingVendorTypeId;
            if ("search".equalsIgnoreCase(param)) {
                String vendorName = request.getParameter("vendorName");
                System.out.println("vendorName********" + vendorName);
                String startDate = "";
                String contractTypeId = request.getParameter("contractTypeId");
                String endDate = request.getParameter("invoiceDate");
                String temp[] = vendorName.split("~");
                String vendorId = temp[0];
                System.out.println("vendorNameId" + vendorId);
//              String vendorType = request.getParameter("vendorType");
                
                String invoiceNo = request.getParameter("invoiceNo");
                String invoiceAmount = request.getParameter("invoiceAmount");
                String remarks = request.getParameter("remarks");
                
                vendorPaymentList = financeBP.getContractVendorPaymentDetails(vendorId, startDate, endDate, contractTypeId);
                request.setAttribute("vendorPaymentList", vendorPaymentList);
                
                vendorPaymentListApproval = financeBP.getContractVendorPaymentDetailsApproval(vendorId, startDate, endDate, contractTypeId);                
                request.setAttribute("vendorPaymentListApproval", vendorPaymentListApproval);
                
                request.setAttribute("vendorName", vendorName);
                request.setAttribute("invoiceDate", endDate);

                request.setAttribute("contractTypeId", contractTypeId);
                if (!"".equals(invoiceNo) && invoiceNo != null) {
                    request.setAttribute("invoiceNo", invoiceNo);
                }
                if (!"".equals(invoiceAmount) && invoiceAmount != null) {
                    request.setAttribute("invoiceAmount", invoiceAmount);
                }
                if (!"".equals(remarks) && remarks != null) {
                    request.setAttribute("remarks", remarks);
                }

                System.out.println("vendorName*********" + vendorName);

            } else {
                request.setAttribute("vendorPaymentList", null);

                System.out.println("else");
            }
            vendorList = financeBP.processVendorList(vendorType);
            request.setAttribute("vendorList", vendorList);
            request.setAttribute("vendorType", vendorType);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/addVendorInvoiceReceipt.jsp";
            
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView editVendorInvoiceReceipt(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Vendor Invoice Receipt";
        menuPath = "Finance  >> Vendor Invoice Receipt";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        FinanceTO financeTO = new FinanceTO();
        try {
            String param = request.getParameter("param");
            String contractTypeId = request.getParameter("contractTypeId");
            System.out.println("param" + param);
            String virId = request.getParameter("virId");
            String virCode = request.getParameter("virCode");
            if (!"".equals(virId) && virId != null) {
                request.setAttribute("virId", virId);
            }
            if (!"".equals(virCode) && virCode != null) {
                financeTO.setVirCode(virCode);
                request.setAttribute("virCode", virCode);
            }
            if (!"".equals(contractTypeId) && contractTypeId != null) {
                financeTO.setContractTypeId(contractTypeId);
                request.setAttribute("contractTypeId", contractTypeId);
            }

            path = "content/Finance/editVendorInvoiceReceipt.jsp";

            ArrayList VIRdetailList = new ArrayList();
            VIRdetailList = financeBP.getVirdetailList(financeTO);
            request.setAttribute("VIRdetailList", VIRdetailList);

            ArrayList vendorInvoiceTripDetailsList = new ArrayList();
            vendorInvoiceTripDetailsList = financeBP.getVendorInvoiceTripDetailsList(financeTO);
            request.setAttribute("vendorInvoiceTripDetailsList", vendorInvoiceTripDetailsList);

            ArrayList VendorTypeList = new ArrayList();
            VendorTypeList = vendorBP.processGetVendorTypeList();
            request.setAttribute("VendorTypeList", VendorTypeList);

            ArrayList vendorList = new ArrayList();
            vendorList = operationBP.processVendorList();
            request.setAttribute("vendorList", vendorList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView insertVendorInvoiceReceipt(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        financeCommand = command;
        HttpSession session = request.getSession();
        System.out.println("pavi......");
        FinanceTO financeTO = new FinanceTO();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", tempFilePath = "";
        String newFileName = "", actualFilePath = "";
        String vendorTypeId = null, vendorName = null, vendorNameId = null, totalAmount = null,
                gender = null, ledgerId = null, invoiceNo = null, invoiceDate = null, invoiceAmount = null;
        String debitLedgerId = "0~0", contractTypeId = "", vendorInvoiceFile = "", remarks = null, vendorInvoiceId = null, vendorLedgerId = null;
        String virCode = "";
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        int i = 0;
        int j = 0;
        int virId = 0;
        int count = 0;
        count = Integer.parseInt(request.getParameter("count"));
        System.out.println("count" + request.getParameter("count"));

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            String filename[] = new String[1];
            String fileSavedAs1[] = new String[1];
            int k = 0;
            int l = 0;
            int m = 0;
            int n = 0;
            int o = 0;
            int p = 0;
            int q = 0;
            int r = 0;
            String[] activeInds = new String[count];
            String[] vehicleIds = new String[count];
            String[] vehicleTypeIds = new String[count];
            String[] containerQtyHires = new String[count];
            String[] containerHires = new String[count];
            String[] amounts = new String[count];
            String[] routeHires = new String[count];
            String[] tripIdHires = new String[count];

            isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    String filePath = "";
                    filePath = getServletContext().getRealPath("/uploadFiles/Files");
                    System.out.println("REAL filePath = " + filePath);
                    String filePath1 = filePath.replace("\\", "\\\\");
                    Date now = new Date();
                    String date = now.getDate() + "" + (now.getMonth() + 1) + "" + (now.getYear() + 1900);
                    String time = now.getHours() + "" + now.getMinutes() + "" + now.getSeconds() + userId;

                    //System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        fPart = (FilePart) partObj;
                        filename[j] = fPart.getFileName();

                        System.out.println("filename" + filename[0]);
                        if (!"".equals(filename[j]) && filename[j] != null) {
                            String[] fp = filename[j].split("\\.");
                            System.out.println("fp" + fp[0]);
                            System.out.println("fp" + fp[1]);
                            fileSavedAs = fp[0] + "." + fp[1];
                            fileSavedAs1[j] = fp[0] + "." + fp[1];
                            if ("vendorInvoiceFile".equals(partObj.getName())) {
                                vendorInvoiceFile = filePath1 + "\\" + fileSavedAs1[j];
                                System.out.println("vendorInvoiceFile" + vendorInvoiceFile);
                            }

                            String tempPath = filePath1 + "\\" + fileSavedAs1[j];
                            System.out.println("tempPath" + tempPath);
                            String actPath = filePath + "\\" + filename[j];
                            long fileSize = fPart.writeTo(new java.io.File(actPath));
                            File f1 = new File(actPath);
                            f1.renameTo(new File(tempPath));

                        }
                        fileName = filename[j];
                        //j++;

                    } else if (partObj.isParam()) {
                        ParamPart paramPart = (ParamPart) partObj;
                        if (partObj.getName().equals("activeIndHire")) {
                            activeInds[k] = paramPart.getStringValue();
                            System.out.println("paramPart.getStringValue()" + paramPart.getStringValue());
                            k++;
                        }
                        if (partObj.getName().equals("tripIdHire")) {
                            tripIdHires[l] = paramPart.getStringValue();
                            l++;
                        }
                        if (partObj.getName().equals("vehicleIdHire")) {
                            vehicleIds[m] = paramPart.getStringValue();
                            m++;
                        }
                        if (partObj.getName().equals("vehicleTypeIdHire")) {
                            vehicleTypeIds[n] = paramPart.getStringValue();
                            n++;
                        }
                        if (partObj.getName().equals("routeHire")) {
                            routeHires[o] = paramPart.getStringValue();
                            o++;
                        }
                        if (partObj.getName().equals("containerHire")) {
                            containerHires[p] = paramPart.getStringValue();
                            p++;
                        }
                        if (partObj.getName().equals("containerQtyHire")) {
                            containerQtyHires[q] = paramPart.getStringValue();
                            q++;
                        }
                        if (partObj.getName().equals("totAmtHire")) {
                            amounts[r] = paramPart.getStringValue();
                            r++;
                        }

                        if (partObj.getName().equals("vendorType")) {
                            vendorTypeId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("totalAmtHire")) {
                            totalAmount = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vendorName")) {
                            vendorName = paramPart.getStringValue();
                            String temp[] = vendorName.split("~");
                            vendorNameId = temp[0];
                            ledgerId = temp[1];
                            vendorLedgerId = ledgerId;
                            System.out.println("ledgerId" + ledgerId);
                            System.out.println("vendorNameId" + vendorNameId);
                        }
                        if (partObj.getName().equals("invoiceNo")) {
                            invoiceNo = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("invoiceDate")) {
                            invoiceDate = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("invoiceAmount")) {
                            invoiceAmount = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("remarks")) {
                            remarks = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("virId")) {
                            vendorInvoiceId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("virCode")) {
                            virCode = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("contractTypeId")) {
                            contractTypeId = paramPart.getStringValue();
                        }

                    }
                }
            }
            financeTO.setVehicleIds(vehicleIds);
            financeTO.setVehicleTypeIds(vehicleTypeIds);
            financeTO.setActiveInds(activeInds);
            financeTO.setContainerTypes(containerHires);
            financeTO.setContainerQtys(containerQtyHires);
            financeTO.setAmounts(amounts);
            financeTO.setRoutes(routeHires);
            financeTO.setTripIds(tripIdHires);
            System.out.println("vendorType" + vendorTypeId);

            String vendorId = financeBP.getVendorFSId(vendorNameId);
            String[] vId = vendorId.split("~");
            
            String GSTId = vId[0];
            String eFSId = vId[1];

//            if (virId != 0 && ("0".equals(vendorInvoiceId)) && eFSId != null && (!"".equals(eFSId))) {
                if (("0".equals(vendorInvoiceId)) && eFSId != null && (!"".equals(eFSId))) {
                virId = financeBP.insertVendorInvoiceReceipt(financeTO, userId, vendorTypeId, vendorNameId,
                        invoiceNo, invoiceDate, invoiceAmount, remarks, ledgerId, vendorInvoiceFile, fileName, debitLedgerId,
                        contractTypeId, totalAmount, vendorLedgerId,
                        vendorInvoiceId);
      
                virCode = ThrottleConstants.vendorInvoiceReceipt + ThrottleConstants.financialYear + "-" + virId;
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Added Successfully & VIR Code is " + virCode);

            } else if ((virId == 0) && ("0".equals(vendorInvoiceId))) {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Failed to Add");
            } else if (!"0".equals(vendorInvoiceId) && (virId > 0)) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Updated Successfully & VIR Code is " + virCode);
            } else if ((virId == 0) && (!"0".equals(vendorInvoiceId))) {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Failed to Update");
            } else if (eFSId == null || ("".equals(eFSId))) {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Vendor eFS Id Not Mapped. Cannot Process Now");
            }

            if (!"".equals(virCode) && virCode != null) {
                request.setAttribute("virCode", virCode);
                financeTO.setVirCode(virCode);
            }

            ArrayList VIRdetailList = new ArrayList();
            VIRdetailList = financeBP.getVirdetailList(financeTO);
            request.setAttribute("VIRdetailList", VIRdetailList);

            path = "content/Finance/vendorInvoiceReceipt.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to add Lecture data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView updateVendorInvoiceTrips(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        
        String path = "";
        financeCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList groupList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        String menuPath = "";
        menuPath = "Finance >> Vendor Invoice Receipt  ";
        String pageTitle = "Vendor Invoice Receipt";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String contractTypeId = request.getParameter("contractTypeId");            
            String serviceTax = "", sgstHire = "", cgstHire = "", igstHire = "";
            String tds = "", sgstDedicated = "", cgstDedicated = "", igstDedicated = "";
            String actualInvoiceAmount = "", billingState = "";
            String tdsAmt = "", tdsDedicatedAmt = "";
            String description = "", igst = "", sgst = "", cgst = "";
            String activeInds[] = null;
            billingState = request.getParameter("billingState");
            igst = request.getParameter("igst");
            String invoiceid = request.getParameter("invoiceid");
            sgst = request.getParameter("sgst");
            cgst = request.getParameter("cgst");
            financeTO.setBillingState(billingState);
            financeTO.setIgstTax(igst);
            financeTO.setSgstTax(sgst);
            financeTO.setCgstTax(cgst);
            if ("1".equals(contractTypeId)) {
                serviceTax = request.getParameter("serviceTaxDedicate");
                tds = request.getParameter("tdsDedicate");
                actualInvoiceAmount = request.getParameter("totalDedicatedInvoiceAmt");
                tdsAmt = request.getParameter("tdsDedicatedAmount");
                description = request.getParameter("descriptionDedicate");
                activeInds = request.getParameterValues("activeIndDedicate");
                sgstDedicated = request.getParameter("sgstDedicated");
                cgstDedicated = request.getParameter("cgstDedicated");
                igstDedicated = request.getParameter("igstDedicated");
                financeTO.setIgstDedicated(igstDedicated);
                financeTO.setSgstDedicated(sgstDedicated);
                financeTO.setCgstDedicated(cgstDedicated);
            } else if ("2".equals(contractTypeId)) {
                serviceTax = request.getParameter("serviceTaxHire");
                tds = request.getParameter("tdsHire");
                //  actualInvoiceAmount = request.getParameter("actualInvoiceAmountHire");
                actualInvoiceAmount = request.getParameter("totalHireInvAmount");
                tdsAmt = request.getParameter("tdsHireAmount");
                description = request.getParameter("description");
                activeInds = request.getParameterValues("activeIndHire");                
                sgstHire = request.getParameter("sgstHire");
                cgstHire = request.getParameter("cgstHire");
                igstHire = request.getParameter("igstHire");
                financeTO.setIgstHire(igstHire);
                financeTO.setCgstHire(cgstHire);
                financeTO.setSgstHire(sgstHire);
            }

            String virCode = request.getParameter("virCode");            
            String virIds[] = request.getParameterValues("virId");

            status = financeBP.updateVendorInvoiceTrips(financeTO, userId,
                    serviceTax, tds, tdsAmt, actualInvoiceAmount, description, virCode, contractTypeId, virIds, activeInds);
            System.out.println("status" + status);
            
            path = "content/Finance/processVendorInvoiceBill.jsp";
            
            financeTO.setVirCode(virCode);
            ArrayList VIRdetailList = new ArrayList();
            VIRdetailList = financeBP.getVirdetailList(financeTO);
            request.setAttribute("VIRdetailList", VIRdetailList);

            ArrayList vendorList = new ArrayList();
            vendorList = operationBP.processVendorList();//0 indicates all customers
            request.setAttribute("vendorList", vendorList);

            ArrayList VendorTypeList = new ArrayList();
            VendorTypeList = vendorBP.processGetVendorTypeList();
            request.setAttribute("VendorTypeList", VendorTypeList);

            if (status > 0) {
                
               ////////////////////// Invoice Web service Part //////////////////////////////////////////////////////
                
                double cgstAmt = 0;
                double sgstAmt = 0;
                double igstAmt = 0;
                String igstPer = "0";
                String sgstPer = "0";
                String cgstPer = "0";
                String invoiceNo = request.getParameter("invoiceNo");
                String invoiceDate = request.getParameter("invoiceDate");
                String vendorNameId = request.getParameter("vendorId");
                String invoiceAmount = request.getParameter("invoiceAmount");
                
                if ("37".equalsIgnoreCase(financeTO.getBillingState())) {
                    cgstAmt = Double.parseDouble(cgstHire);
                    sgstAmt = Double.parseDouble(sgstHire);                    
                    sgstPer = sgst;
                    cgstPer = cgst;
                }else{                    
                    igstAmt = Double.parseDouble(igstHire);
                    igstPer = igst;
                }                

                double net_amount = Double.parseDouble(invoiceAmount) + cgstAmt + sgstAmt + igstAmt;
                net_amount = Math.round(net_amount);
                
                double totalTaxAmt = Math.round(Double.parseDouble(invoiceAmount));

                System.out.println("cgstAmt : " + cgstHire + " per:" + cgstPer);
                System.out.println("sgstAmt : " + sgstHire + " per:" + sgstPer);
                System.out.println("igstAmt : " + igstHire + " per:" + igstPer);
                
                String vendorId = financeBP.getVendorFSId(vendorNameId);
                String[] vId = vendorId.split("~");

                String GSTId = vId[0];
                String eFSId = vId[1];
                int postId = 0;
                
             //   postId = financeBP.saveInvoicePostDetails(eFSId, invoiceNo, invoiceDate, invoiceAmount + "", totalTaxAmt + "", cgstPer, cgstAmt + "", sgstPer, sgstAmt + "", igstPer, igstAmt + "", net_amount + "",GSTId,invoiceid,"Vendor");

                if(postId>0){
                  //  new sendVendorInvoice(eFSId, invoiceNo, invoiceDate, invoiceAmount + "", totalTaxAmt + "", cgstPer, cgstAmt + "", sgstPer, sgstAmt + "", igstPer, igstAmt + "", net_amount + "",GSTId, postId).start();
                }
                
                ////////////////////////////////////////////// Vendor Invoice //////////////////////////////////////////   
                
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Processed Successfully");
            } else {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, " Failed to Process");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bank Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView processVendorInvoiceBill(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankPaymentLedgerList = new ArrayList();

        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View VIR";
        menuPath = "Finance  >> View VIR Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        FinanceTO financeTO = new FinanceTO();
        ArrayList vendorList = new ArrayList();
        try {
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            String contractTypeId = "2";

            String vendorTypeId = ThrottleConstants.leasingVendorTypeId;
//            String vendorTypeId = request.getParameter("vendorType");
            String vendorName = request.getParameter("vendorName");
            String vendorId = "";

            String virCode = request.getParameter("virCode");
            if (!"".equals(vendorName) && vendorName != null) {
                String temp[] = vendorName.split("~");
                vendorId = temp[0];
                System.out.println("vendorNameId" + vendorId);
                request.setAttribute("vendorName", vendorName);
                financeTO.setVendorName(vendorId);
            }
            if (!"".equals(fromDate) && fromDate != null) {
                request.setAttribute("fromDate", fromDate);
                financeTO.setFromDate(fromDate);
            }
            if (!"".equals(toDate) && toDate != null) {
                request.setAttribute("toDate", toDate);
                financeTO.setToDate(toDate);
            }
            if (!"".equals(vendorTypeId) && vendorTypeId != null) {
                request.setAttribute("vendorType", vendorTypeId);
                financeTO.setVendorTypeId(vendorTypeId);
            }
            if (!"".equals(virCode) && virCode != null) {
                request.setAttribute("virCode", virCode);
                financeTO.setVirCode(virCode);
            }

            request.setAttribute("vendorList", null);

            vendorList = financeBP.processVendorList(vendorTypeId);
            request.setAttribute("vendorList", vendorList);

//            ArrayList VendorTypeList = new ArrayList();
//            VendorTypeList = vendorBP.processGetVendorTypeList();
//            request.setAttribute("VendorTypeList", VendorTypeList);
            financeTO.setContractTypeId(contractTypeId);
            ArrayList VIRdetailList = new ArrayList();
            VIRdetailList = financeBP.getVirdetailList(financeTO);

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            request.setAttribute("VIRdetailList", VIRdetailList);
            path = "content/Finance/processVendorInvoiceBill.jsp";

            request.setAttribute("VIRdetailList", VIRdetailList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView leasingVendorInvoiceReceipt(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankPaymentLedgerList = new ArrayList();

        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View VIR";
        menuPath = "Finance  >> View VIR Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        FinanceTO financeTO = new FinanceTO();
        ArrayList vendorList = new ArrayList();
        try {
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            String contractTypeId = "1";

            String vendorTypeId = ThrottleConstants.leasingVendorTypeId;
//            String vendorTypeId = request.getParameter("vendorType");
            String vendorName = request.getParameter("vendorName");
            String vendorId = "";

            String virCode = request.getParameter("virCode");
            if (!"".equals(vendorName) && vendorName != null) {
                String temp[] = vendorName.split("~");
                vendorId = temp[0];
                System.out.println("vendorNameId" + vendorId);
                request.setAttribute("vendorName", vendorName);
                financeTO.setVendorName(vendorId);
            }
            if (!"".equals(fromDate) && fromDate != null) {
                request.setAttribute("fromDate", fromDate);
                financeTO.setFromDate(fromDate);
            }
            if (!"".equals(toDate) && toDate != null) {
                request.setAttribute("toDate", toDate);
                financeTO.setToDate(toDate);
            }
            if (!"".equals(vendorTypeId) && vendorTypeId != null) {
                request.setAttribute("vendorType", vendorTypeId);
                financeTO.setVendorTypeId(vendorTypeId);
            }
            if (!"".equals(virCode) && virCode != null) {
                request.setAttribute("virCode", virCode);
                financeTO.setVirCode(virCode);
            }

            request.setAttribute("vendorList", null);

            vendorList = financeBP.processVendorList(vendorTypeId);
            request.setAttribute("vendorList", vendorList);

//            ArrayList VendorTypeList = new ArrayList();
//            VendorTypeList = vendorBP.processGetVendorTypeList();
//            request.setAttribute("VendorTypeList", VendorTypeList);
            financeTO.setContractTypeId(contractTypeId);
            ArrayList VIRdetailList = new ArrayList();
            VIRdetailList = financeBP.getVirdetailList(financeTO);

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/leasingVendorInvoiceReceipt.jsp";

            request.setAttribute("VIRdetailList", VIRdetailList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView processLeasingVendorInvoiceBill(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankPaymentLedgerList = new ArrayList();

        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "View VIR";
        menuPath = "Finance  >> View VIR Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        FinanceTO financeTO = new FinanceTO();
        ArrayList vendorList = new ArrayList();
        try {
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            String contractTypeId = "1";

            String vendorTypeId = ThrottleConstants.leasingVendorTypeId;
//            String vendorTypeId = request.getParameter("vendorType");
            String vendorName = request.getParameter("vendorName");
            String vendorId = "";

            String virCode = request.getParameter("virCode");
            if (!"".equals(vendorName) && vendorName != null) {
                String temp[] = vendorName.split("~");
                vendorId = temp[0];
                System.out.println("vendorNameId" + vendorId);
                request.setAttribute("vendorName", vendorName);
                financeTO.setVendorName(vendorId);
            }
            if (!"".equals(fromDate) && fromDate != null) {
                request.setAttribute("fromDate", fromDate);
                financeTO.setFromDate(fromDate);
            }
            if (!"".equals(toDate) && toDate != null) {
                request.setAttribute("toDate", toDate);
                financeTO.setToDate(toDate);
            }
            if (!"".equals(vendorTypeId) && vendorTypeId != null) {
                request.setAttribute("vendorType", vendorTypeId);
                financeTO.setVendorTypeId(vendorTypeId);
            }
            if (!"".equals(virCode) && virCode != null) {
                request.setAttribute("virCode", virCode);
                financeTO.setVirCode(virCode);
            }

            request.setAttribute("vendorList", null);

            vendorList = financeBP.processVendorList(vendorTypeId);
            request.setAttribute("vendorList", vendorList);

//            ArrayList VendorTypeList = new ArrayList();
//            VendorTypeList = vendorBP.processGetVendorTypeList();
//            request.setAttribute("VendorTypeList", VendorTypeList);
            financeTO.setContractTypeId(contractTypeId);
            ArrayList VIRdetailList = new ArrayList();
            VIRdetailList = financeBP.getVirdetailList(financeTO);

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/processLeasingVendorInvoiceBill.jsp";

            request.setAttribute("VIRdetailList", VIRdetailList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView addLeasingVendorInvoiceReceipt(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Vendor Invoice Receipt";
        menuPath = "Finance  >>Add Vendor Invoice Receipt";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        FinanceTO financeTO = new FinanceTO();
        try {
            String param = request.getParameter("param");
            System.out.println("param" + param);
            ArrayList VendorTypeList = new ArrayList();
            VendorTypeList = vendorBP.processGetVendorTypeList();
            request.setAttribute("VendorTypeList", VendorTypeList);

//            ArrayList expenseGroupList = new ArrayList();
//            expenseGroupList = financeBP.getExpenseGroupList();
//            request.setAttribute("expenseGroupList", expenseGroupList);
            ArrayList vendorPaymentList = new ArrayList();
            ArrayList vendorList = new ArrayList();
            String vendorType = ThrottleConstants.leasingVendorTypeId;
            if ("search".equalsIgnoreCase(param)) {
                String vendorName = request.getParameter("vendorName");
                System.out.println("vendorName********" + vendorName);
                String startDate = "";
                String contractTypeId = request.getParameter("contractTypeId");
                String invoiceMonth = request.getParameter("invoiceMonth");
                String invoiceYear = request.getParameter("invoiceYear");
                String invoiceMonthYear = invoiceMonth + "-" + invoiceYear;
                String endDate = request.getParameter("invoiceDate");
                String temp[] = vendorName.split("~");
                String vendorId = temp[0];
                System.out.println("vendorNameId" + vendorId);
//                String vendorType = request.getParameter("vendorType");
                String invoiceNo = request.getParameter("invoiceNo");
                String invoiceAmount = request.getParameter("invoiceAmount");
                String remarks = request.getParameter("remarks");
                vendorPaymentList = financeBP.getContractVendorPaymentDetails(vendorId, invoiceMonthYear, endDate, contractTypeId);
                request.setAttribute("vendorPaymentList", vendorPaymentList);
                request.setAttribute("vendorName", vendorName);
                request.setAttribute("invoiceDate", endDate);

                request.setAttribute("contractTypeId", contractTypeId);
                if (!"".equals(invoiceNo) && invoiceNo != null) {
                    request.setAttribute("invoiceNo", invoiceNo);
                }
                if (!"".equals(invoiceAmount) && invoiceAmount != null) {
                    request.setAttribute("invoiceAmount", invoiceAmount);
                }
                if (!"".equals(remarks) && remarks != null) {
                    request.setAttribute("remarks", remarks);
                }

                if (!"".equals(invoiceMonth) && invoiceMonth != null) {
                    request.setAttribute("invoiceMonth", invoiceMonth);
                }
                if (!"".equals(invoiceYear) && invoiceYear != null) {
                    request.setAttribute("invoiceYear", invoiceYear);
                }

                System.out.println("vendorName*********" + vendorName);

            } else {
                request.setAttribute("vendorPaymentList", null);

                System.out.println("else");
            }
            vendorList = financeBP.processVendorList(vendorType);
            request.setAttribute("vendorList", vendorList);
            request.setAttribute("vendorType", vendorType);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Finance/addLeasingVendorInvoiceReceipt.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView editLeasingVendorInvoiceReceipt(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Vendor Invoice Receipt";
        menuPath = "Finance  >> Vendor Invoice Receipt";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        FinanceTO financeTO = new FinanceTO();
        try {
            String param = request.getParameter("param");
            String contractTypeId = request.getParameter("contractTypeId");
            System.out.println("param" + param);
            String virId = request.getParameter("virId");
            String virCode = request.getParameter("virCode");
            if (!"".equals(virId) && virId != null) {
                request.setAttribute("virId", virId);
            }
            if (!"".equals(virCode) && virCode != null) {
                financeTO.setVirCode(virCode);
                request.setAttribute("virCode", virCode);
            }
            if (!"".equals(contractTypeId) && contractTypeId != null) {
                financeTO.setContractTypeId(contractTypeId);
                request.setAttribute("contractTypeId", contractTypeId);
            }

            path = "content/Finance/editLeasingVendorInvoiceReceipt.jsp";

            ArrayList VIRdetailList = new ArrayList();
            VIRdetailList = financeBP.getVirdetailList(financeTO);
            request.setAttribute("VIRdetailList", VIRdetailList);
            System.out.println("editLeasingVendorInvoiceReceipt VIRdetailList." + VIRdetailList.size());
            ArrayList vendorInvoiceTripDetailsList = new ArrayList();
            vendorInvoiceTripDetailsList = financeBP.getVendorInvoiceTripDetailsList(financeTO);
            request.setAttribute("vendorInvoiceTripDetailsList", vendorInvoiceTripDetailsList);

            ArrayList VendorTypeList = new ArrayList();
            VendorTypeList = vendorBP.processGetVendorTypeList();
            request.setAttribute("VendorTypeList", VendorTypeList);

            ArrayList vendorList = new ArrayList();
            vendorList = operationBP.processVendorList();
            request.setAttribute("vendorList", vendorList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView insertVendorInvoiceReceiptDedicate(HttpServletRequest request, HttpServletResponse response, FinanceCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        financeCommand = command;
        HttpSession session = request.getSession();
        System.out.println("pavi......");
        FinanceTO financeTO = new FinanceTO();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", tempFilePath = "";
        String newFileName = "", actualFilePath = "";
        String vendorTypeId = null, vendorName = null, vendorNameId = null, totalAmount = null,
                gender = null, ledgerId = null, invoiceNo = null, invoiceDate = null, invoiceAmount = null;
        String debitLedgerId = "0~0", contractTypeId = "", vendorInvoiceFile = "", remarks = null, vendorInvoiceId = null, vendorLedgerId = null;
        String virCode = "";
        String invoiceMonth = "";
        String invoiceYear = "";
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        int i = 0;
        int j = 0;
        int virId = 0;
        int count = 0;
        count = Integer.parseInt(request.getParameter("count"));
        System.out.println("count" + request.getParameter("count"));

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            String filename[] = new String[1];
            String fileSavedAs1[] = new String[1];
            int k = 0;
            int l = 0;
            int m = 0;
            int n = 0;
            int o = 0;
            int p = 0;
            int q = 0;
            int r = 0;
            int s = 0;
            int t = 0;
            int u = 0;
            int v = 0;
            int w = 0;
            int x = 0;
            String[] activeInds = new String[count];
            String[] vehicleIds = new String[count];
            String[] vehicleTypeIds = new String[count];
            String[] tripIdDedicates = new String[count];
            String[] noOfTripsDedicates = new String[count];
            String[] amounts = new String[count];
            String[] contractCategorys = new String[count];
            String[] totRunKMs = new String[count];
            String[] allowedKMs = new String[count];
            String[] totExtraRunKms = new String[count];
            String[] fixedCosts = new String[count];
            String[] ratePerExtraKms = new String[count];
            String[] totExtraKMRates = new String[count];
            String[] actualRatePerKMs = new String[count];

            isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    String filePath = "";
                    filePath = getServletContext().getRealPath("/uploadFiles/Files");
                    System.out.println("REAL filePath = " + filePath);
                    String filePath1 = filePath.replace("\\", "\\\\");
                    Date now = new Date();
                    String date = now.getDate() + "" + (now.getMonth() + 1) + "" + (now.getYear() + 1900);
                    String time = now.getHours() + "" + now.getMinutes() + "" + now.getSeconds() + userId;

                    //System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        fPart = (FilePart) partObj;
                        filename[j] = fPart.getFileName();

                        System.out.println("filename" + filename[0]);
                        if (!"".equals(filename[j]) && filename[j] != null) {
                            String[] fp = filename[j].split("\\.");
                            System.out.println("fp" + fp[0]);
                            System.out.println("fp" + fp[1]);
                            fileSavedAs = fp[0] + "." + fp[1];
                            fileSavedAs1[j] = fp[0] + "." + fp[1];
                            if ("vendorInvoiceFile".equals(partObj.getName())) {
                                vendorInvoiceFile = filePath1 + "\\" + fileSavedAs1[j];
                                System.out.println("vendorInvoiceFile" + vendorInvoiceFile);
                            }

                            String tempPath = filePath1 + "\\" + fileSavedAs1[j];
                            System.out.println("tempPath" + tempPath);
                            String actPath = filePath + "\\" + filename[j];
                            long fileSize = fPart.writeTo(new java.io.File(actPath));
                            File f1 = new File(actPath);
                            f1.renameTo(new File(tempPath));

                        }
                        fileName = filename[j];
                        //j++;

                    } else if (partObj.isParam()) {
                        ParamPart paramPart = (ParamPart) partObj;
                        if (partObj.getName().equals("activeIndDedicate")) {
                            activeInds[k] = paramPart.getStringValue();
                            System.out.println("paramPart.getStringValue()" + paramPart.getStringValue());
                            k++;
                        }
                        if (partObj.getName().equals("tripIdDedicate")) {
                            tripIdDedicates[l] = paramPart.getStringValue();
                            l++;
                        }
                        if (partObj.getName().equals("vehicleIdDedicate")) {
                            vehicleIds[m] = paramPart.getStringValue();
                            m++;
                        }
                        if (partObj.getName().equals("vehicleTypeIdDedicate")) {
                            vehicleTypeIds[n] = paramPart.getStringValue();
                            n++;
                        }
                        if (partObj.getName().equals("totRunKM")) {
                            totRunKMs[o] = paramPart.getStringValue();
                            o++;
                        }
                        if (partObj.getName().equals("allowedKM")) {
                            allowedKMs[p] = paramPart.getStringValue();
                            p++;
                        }
                        if (partObj.getName().equals("contractCategory")) {
                            contractCategorys[q] = paramPart.getStringValue();
                            q++;
                        }
                        if (partObj.getName().equals("noOfTripsDedicate")) {
                            noOfTripsDedicates[r] = paramPart.getStringValue();
                            r++;
                        }
                        if (partObj.getName().equals("totAmtDedicate")) {
                            amounts[s] = paramPart.getStringValue();
                            s++;
                        }
                        if (partObj.getName().equals("totExtraRunKm")) {
                            totExtraRunKms[t] = paramPart.getStringValue();
                            t++;
                        }
                        if (partObj.getName().equals("fixedCost")) {
                            fixedCosts[u] = paramPart.getStringValue();
                            u++;
                        }
                        if (partObj.getName().equals("ratePerExtraKm")) {
                            ratePerExtraKms[v] = paramPart.getStringValue();
                            v++;
                        }
                        if (partObj.getName().equals("totExtraKMRate")) {
                            totExtraKMRates[w] = paramPart.getStringValue();
                            w++;
                        }
                        if (partObj.getName().equals("actualRatePerKM")) {
                            actualRatePerKMs[x] = paramPart.getStringValue();
                            x++;
                        }

                        if (partObj.getName().equals("vendorType")) {
                            vendorTypeId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("totalAmtDedicate")) {
                            totalAmount = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vendorName")) {
                            vendorName = paramPart.getStringValue();
                            String temp[] = vendorName.split("~");
                            vendorNameId = temp[0];
                            ledgerId = temp[1];
                            vendorLedgerId = ledgerId;
                            System.out.println("ledgerId" + ledgerId);
                            System.out.println("vendorNameId" + vendorNameId);
                        }
                        if (partObj.getName().equals("invoiceNo")) {
                            invoiceNo = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("invoiceDate")) {
                            invoiceDate = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("invoiceAmount")) {
                            invoiceAmount = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("remarks")) {
                            remarks = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("virId")) {
                            vendorInvoiceId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("virCode")) {
                            virCode = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("contractTypeId")) {
                            contractTypeId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("invoiceMonth")) {
                            invoiceMonth = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("invoiceYear")) {
                            invoiceYear = paramPart.getStringValue();
                        }

                    }
                }
            }
            System.out.println("vendorType" + vendorTypeId);
            financeTO.setVehicleIds(vehicleIds);
            financeTO.setVehicleTypeIds(vehicleTypeIds);
            financeTO.setActiveInds(activeInds);
            financeTO.setTotRunKMs(totRunKMs);
            financeTO.setAllowedKMs(allowedKMs);
            financeTO.setTotExtraRunKms(totExtraRunKms);
            financeTO.setTotExtraKMRates(totExtraKMRates);
            financeTO.setActualRatePerKMs(actualRatePerKMs);
            financeTO.setRatePerExtraKms(ratePerExtraKms);
            financeTO.setFixedCosts(fixedCosts);

            financeTO.setAmounts(amounts);
            financeTO.setNoOfTrips(noOfTripsDedicates);
            financeTO.setTripIds(tripIdDedicates);
            financeTO.setContractCategorys(contractCategorys);

            financeTO.setInvoiceMonth(invoiceMonth);
            financeTO.setInvoiceYear(invoiceYear);
            virId = financeBP.insertVendorInvoiceReceipt(financeTO, userId, vendorTypeId, vendorNameId,
                    invoiceNo, invoiceDate, invoiceAmount, remarks, ledgerId, vendorInvoiceFile, fileName, debitLedgerId,
                    contractTypeId, totalAmount, vendorLedgerId,
                    vendorInvoiceId);

            if (virId != 0 && ("0".equals(vendorInvoiceId))) {
                virCode = ThrottleConstants.vendorInvoiceReceipt + ThrottleConstants.financialYear + "-" + virId;
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Added Successfully & VIR Code is " + virCode);
            } else if ((virId == 0) && ("0".equals(vendorInvoiceId))) {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Failed to Add");
            } else if (!"0".equals(vendorInvoiceId) && (virId > 0)) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Updated Successfully & VIR Code is " + virCode);
            } else if ((virId == 0) && (!"0".equals(vendorInvoiceId))) {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Failed to Update");
            }
            if (!"".equals(virCode) && virCode != null) {
                request.setAttribute("virCode", virCode);
                financeTO.setVirCode(virCode);
            }
            ArrayList VIRdetailList = new ArrayList();
            VIRdetailList = financeBP.getVirdetailList(financeTO);
            request.setAttribute("VIRdetailList", VIRdetailList);

            path = "content/Finance/leasingVendorInvoiceReceipt.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to add Lecture data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewInvoiceTripDetails(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        HttpSession session = request.getSession();
        String path = "";
        financeCommand = command;
        FinanceTO financeTO = new FinanceTO();
        ArrayList tripDetails = new ArrayList();
        try {
            path = "content/Finance/viewInvoiceTripDetails.jsp";
            String tripIds = request.getParameter("tripIds");
            System.out.println("viewInvoiceTripDetails tripIds" + tripIds);
            //
            financeTO.setTripId(tripIds);

            tripDetails = financeBP.getTripDetails(financeTO);
            System.out.println("viewInvoiceTripDetails = " + tripDetails.size());
            request.setAttribute("tripDetails", tripDetails);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Ledger Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewApproalForCOF(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Vendor Invoice Receipt";
        menuPath = "Finance  >> Vendor Invoice Receipt";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        FinanceTO financeTO = new FinanceTO();
        try {
            String tripId = request.getParameter("tripSheetId");
            financeTO.setTripId(tripId);
            System.out.println("tripId : " + tripId);

            Date dNow = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(dNow);
            cal.add(Calendar.DATE, 0);
            dNow = cal.getTime();

            Date dNow1 = new Date();
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(dNow1);
            cal1.add(Calendar.DATE, -6);
            dNow1 = cal1.getTime();

            SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy");
            String today = ft.format(dNow);
            String fromday = ft.format(dNow1);
            System.out.println("the from date is " + fromday);

            if (financeCommand.getFromDate() != null && financeCommand.getFromDate() != "") {
                financeTO.setFromDate(financeCommand.getFromDate());
            } else {
                financeTO.setFromDate(fromday);
            }
            if (financeCommand.getToDate() != null && financeCommand.getToDate() != "") {
                financeTO.setToDate(financeCommand.getToDate());
            } else {
                financeTO.setToDate(today);
            }

            path = "content/Finance/viewApprovalForCOF.jsp";

            ArrayList COFApproval = new ArrayList();
            COFApproval = financeBP.getCOFApproval(financeTO);
            request.setAttribute("COFApproval", COFApproval);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView updateCOFApproval(HttpServletRequest request, HttpServletResponse reponse, FinanceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        ModelAndView mv = null;
        String path = "";
        HttpSession session = request.getSession();
        financeCommand = command;
        String menuPath = "";
        String pageTitle = "Vendor Invoice Receipt";
        menuPath = "Finance  >> Vendor Invoice Receipt";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        FinanceTO financeTO = new FinanceTO();
        int userId = (Integer) session.getAttribute("userId");
        try {
            String tripId = request.getParameter("tripSheetId");
            financeTO.setTripId(tripId);
            System.out.println("tripId : " + tripId);
            if (financeCommand.getTripId() != null && financeCommand.getTripId() != "") {
                financeTO.setTripId(financeCommand.getTripId());
            }
            if (financeCommand.getCofStatus() != null && financeCommand.getCofStatus() != "") {
                financeTO.setCofStatus(financeCommand.getCofStatus());
            }

            ArrayList COFApproval = new ArrayList();
            COFApproval = financeBP.getCOFApproval(financeTO);
            request.setAttribute("COFApproval", COFApproval);

            int status = financeBP.updateCOFApproval(financeTO, userId);
//            path = "content/Finance/addVendorInvoiceReceipt.jsp";
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " COF Approved Successfully");
            }
            mv = addVendorInvoiceReceipt(request, reponse, command);

        } catch (FPRuntimeException exception) {
            exception.printStackTrace();
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }
}