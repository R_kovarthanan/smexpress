/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.finance.business;

import com.ibatis.sqlmap.client.SqlMapClient;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.finance.data.FinanceDAO;
import java.util.ArrayList;
import ets.domain.racks.business.RackTO;
import ets.domain.company.business.CompanyTO;
import ets.domain.finance.business.FinanceTO;
import ets.domain.section.data.SectionDAO;
import ets.domain.util.ThrottleConstants;
import ets.domain.vehicle.data.VehicleDAO;
import static java.lang.Integer.parseInt;
import java.util.Iterator;

/**
 *
 * @author ASHOK
 */
public class FinanceBP {

    private FinanceDAO financeDAO;

    public FinanceDAO getFinanceDAO() {
        return financeDAO;
    }

    public void setFinanceDAO(FinanceDAO financeDAO) {
        this.financeDAO = financeDAO;
    }

    /**
     * This method used to Get Finance Bank.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList financeBank(String bank) throws FPRuntimeException, FPBusinessException {
        ArrayList financeBank = new ArrayList();
        financeBank = financeDAO.financeBank(bank);
        return financeBank;
    }

    public ArrayList getBankList() throws FPBusinessException, FPRuntimeException {
        ArrayList bankList = new ArrayList();
        bankList = financeDAO.getBankList();

        return bankList;
    }

     public ArrayList getBankBranchList(FinanceTO financeTO) throws FPBusinessException, FPRuntimeException {
        ArrayList bankList = new ArrayList();
        bankList = financeDAO.getBankBranchList(financeTO);
        return bankList;
    }

    public ArrayList getPrimaryBankList() throws FPBusinessException, FPRuntimeException {
        ArrayList primarybankList = new ArrayList();
        primarybankList = financeDAO.getPrimaryBankList();
        return primarybankList;
    }

    public int saveNewBank(FinanceTO financeTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.saveNewBank(financeTO, userId);
        if (status == 0) {
            // throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public int savePrimaryBank(FinanceTO financeTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.savePrimaryBank(financeTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public int saveFinYear(FinanceTO financeTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.saveFinYear(financeTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public int updateFinYear(FinanceTO financeTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.updateFinYear(financeTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public String checkBankAccountCode(FinanceTO financeTO) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = financeDAO.checkBankAccountCode(financeTO);
        return status;
    }

    public String checkFinYear(FinanceTO financeTO) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = financeDAO.checkFinYear(financeTO);
        return status;
    }

    public ArrayList processBankalterList(String bankid) throws FPBusinessException, FPRuntimeException {
        ArrayList bankList = new ArrayList();
        bankList = financeDAO.processBankalterList(bankid);
        if (bankList.size() == 0) {
            throw new FPBusinessException("EM-CUST-01");
        }
        return bankList;
    }

    public int saveAlterBank(FinanceTO financeTO, int userid) throws FPRuntimeException, FPBusinessException {
        //////System.out.println("bankid BP= " + bankid);
        int status = 0;
        status = financeDAO.saveAlterBank(financeTO, userid);
        if (status == 0) {
            // throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    /**
     * This method used to get Group List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getGroupList() throws FPBusinessException, FPRuntimeException {
        ArrayList groupList = new ArrayList();
        groupList = financeDAO.getGroupList();
//         if (groupList.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
//        }
        return groupList;
    }

    public int saveNewGroup(FinanceTO financeTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.saveNewGroup(financeTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public ArrayList processGroupalterList(String groupid) throws FPBusinessException, FPRuntimeException {
        ArrayList groupList = new ArrayList();
        groupList = financeDAO.processGroupalterList(groupid);
        if (groupList.size() == 0) {
            throw new FPBusinessException("EM-CUST-01");
        }
        return groupList;
    }

    public int saveAlterGroup(FinanceTO financeTO, String groupid, int userId) throws FPRuntimeException, FPBusinessException {
        //////System.out.println("Group BP= " + groupid);
        //////System.out.println("userId BP= " + userId);
        int status = 0;
        status = financeDAO.saveAlterGroup(financeTO, groupid, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    /**
     * This method used to Process primary list
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList handlePrimaryList() throws FPBusinessException, FPRuntimeException {
        //////System.out.println("in BP :::::");
        ArrayList primaryList = new ArrayList();
        primaryList = financeDAO.handlePrimaryList();
        return primaryList;
    }

    public ArrayList getLevelList(FinanceTO lData1) throws FPBusinessException, FPRuntimeException {
        //////System.out.println("BP ------------------");

        ArrayList levelList = new ArrayList();
        ArrayList levelListNew = new ArrayList();
        levelList = financeDAO.getLevelList(lData1);

        Iterator itr = levelList.iterator();
        FinanceTO fto = null;
        FinanceTO ftoNew = new FinanceTO();
        Float amount = 0.00F;
        String accountsType = "";
        String accountsAmount = "";
        int cntr = 0;
        boolean addStatus = false;
        try {
            while (itr.hasNext()) {
                fto = new FinanceTO();
                fto = (FinanceTO) itr.next();
                //////System.out.println("fto.getGroupID():" + fto.getGroupID() + " fto.getLevelgroupId():" + fto.getLevelgroupId() + " fto.credit():" + fto.getCredit() + " fto.debit():" + fto.getDebit());
                fto.resetCreditDebit();
                levelListNew.add(fto);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return levelListNew;
    }

    /**
     * This method used to get Ledger List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getLedgerList() throws FPBusinessException, FPRuntimeException {
        ArrayList ledgerList = new ArrayList();
        //////System.out.println("BP---");
        ledgerList = financeDAO.getLedgerList();
//         if (groupList.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
//        }
        return ledgerList;
    }

//    public int saveNewLedger(FinanceTO financeTO, int userId) throws FPRuntimeException, FPBusinessException {
//        int status = 0;
//        status = financeDAO.saveNewLedger(financeTO, userId);
//        if (status == 0) {
//            throw new FPBusinessException("EM-CM-03");
//        }
//        return status;
//    }
     public int saveNewLedger(FinanceTO financeTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        SqlMapClient session = financeDAO.getSqlMapClient();
        try {
            session.startTransaction();
            status = financeDAO.saveNewLedger(financeTO, userId,session);
           

            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }

    public int saveNewLevel(FinanceTO financeTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.saveNewLevel(financeTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public ArrayList processLedgeralterList(String ledgerID) throws FPBusinessException, FPRuntimeException {
        ArrayList ledgerList = new ArrayList();
        ledgerList = financeDAO.processledgeralerList(ledgerID);
        if (ledgerList.size() == 0) {
            throw new FPBusinessException("EM-CUST-01");
        }
        return ledgerList;
    }

   /* public int saveAlterLedger(FinanceTO financeTO, String ledgerID, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.saveAlterLedger(financeTO, ledgerID, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }*/
    public int saveAlterLedger(FinanceTO financeTO, String ledgerID, int userId) throws FPRuntimeException, FPBusinessException {

        int status = 0;
        SqlMapClient session = financeDAO.getSqlMapClient();
        try {
            session.startTransaction();
            status = financeDAO.saveAlterLedger(financeTO, ledgerID, userId,session);
            
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }

//getAjax Level1 lList
    /**
     * This method used to Get parts throw ajax Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getLevel1Data(String groupCode) throws FPRuntimeException, FPBusinessException {
        //////System.out.println("in bpppppppppppppppppp");
        ArrayList lvels1 = new ArrayList();
        int counter = 0;
        FinanceTO rack = null;
        lvels1 = financeDAO.getLevel1Data(groupCode);

        Iterator itr;
        String level1 = "";
        if (lvels1.size() == 0) {
            level1 = "";
        } else {
            itr = lvels1.iterator();
            while (itr.hasNext()) {
                rack = new FinanceTO();
                rack = (FinanceTO) itr.next();
                if (counter == 0) {
                    level1 = rack.getLevel1Code() + "^" + rack.getLevel1Name();
                    counter++;
                } else {
                    level1 = level1 + "~" + rack.getLevel1Code() + "^" + rack.getLevel1Name();
                }
                //////System.out.println("level1 in BP= " + level1);
            }
        }
        return level1;
    }

    /**
     * This method used to Get level 2 master ajax Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getLevel2Data(String level1Code) throws FPRuntimeException, FPBusinessException {
        //////System.out.println("in BP 2222222");
        ArrayList lvels2 = new ArrayList();
        int counter = 0;
        FinanceTO rack = null;
        lvels2 = financeDAO.getLeve21Data(level1Code);

        Iterator itr;
        String level2 = "";
        if (lvels2.size() == 0) {
            level2 = "";
        } else {
            itr = lvels2.iterator();
            while (itr.hasNext()) {
                rack = new FinanceTO();
                rack = (FinanceTO) itr.next();
                if (counter == 0) {
                    level2 = rack.getLevel2Code() + "^" + rack.getLevel2Name();
                    counter++;
                } else {
                    level2 = level2 + "~" + rack.getLevel2Code() + "^" + rack.getLevel2Name();
                }
                //////System.out.println("level2 in BP= " + level2);
            }
        }
        return level2;
    }

    /**
     * This method used to Get level 3 master ajax Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getLevel3Data(String level2Code) throws FPRuntimeException, FPBusinessException {
        //////System.out.println("in BP 2222222");
        ArrayList lvels3 = new ArrayList();
        int counter = 0;
        FinanceTO rack = null;
        lvels3 = financeDAO.getLeve21Data(level2Code);
        Iterator itr;
        String level3 = "";
        if (lvels3.size() == 0) {
            level3 = "";
        } else {
            itr = lvels3.iterator();
            while (itr.hasNext()) {
                rack = new FinanceTO();
                rack = (FinanceTO) itr.next();
                if (counter == 0) {
                    level3 = rack.getLevel3Code() + "^" + rack.getLevel3Name();
                    counter++;
                } else {
                    level3 = level3 + "~" + rack.getLevel3Code() + "^" + rack.getLevel3Name();
                }
                //////System.out.println("level3 in BP= " + level3);
            }
        }
        return level3;
    }

    /**
     * This method used to get Voucher List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getVoucherList() throws FPBusinessException, FPRuntimeException {
        ArrayList voucherList = new ArrayList();
        voucherList = financeDAO.getVoucherList();
//         if (groupList.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
//        }
        return voucherList;
    }

    public int saveNewVoucher(FinanceTO financeTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.saveNewVoucher(financeTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public ArrayList processVoucheralterList(String voucherTypeID) throws FPBusinessException, FPRuntimeException {
        ArrayList voucherList = new ArrayList();
        voucherList = financeDAO.processVoucheralterList(voucherTypeID);
        if (voucherList.size() == 0) {
            throw new FPBusinessException("EM-CUST-01");
        }
        return voucherList;
    }

    public int saveAlterVoucher(FinanceTO financeTO, String voucherTypeID, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.saveAlterVoucher(financeTO, voucherTypeID, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    /**
     * This method used to get Account Type List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getAccountTypeList() throws FPBusinessException, FPRuntimeException {
        ArrayList accountTypeList = new ArrayList();
        accountTypeList = financeDAO.getAccountTypeList();
//         if (groupList.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
//        }
        return accountTypeList;
    }

    public int saveNewAccountType(FinanceTO financeTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.saveNewAccountType(financeTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public ArrayList processAccountTypealterList(String accountEntryTypeID) throws FPBusinessException, FPRuntimeException {
        ArrayList accountTypeList = new ArrayList();
        accountTypeList = financeDAO.processAccountTypealterList(accountEntryTypeID);
        if (accountTypeList.size() == 0) {
            throw new FPBusinessException("EM-CUST-01");
        }
        return accountTypeList;
    }

    public int saveAlterAccountType(FinanceTO financeTO, String accountEntryTypeID, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.saveAlterAccountType(financeTO, accountEntryTypeID, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    /**
     * This method used to get Account Type List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getTaxList() throws FPBusinessException, FPRuntimeException {
        ArrayList taxList = new ArrayList();
        taxList = financeDAO.getTaxList();
//         if (groupList.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
//        }
        return taxList;
    }

    public int saveNewTax(FinanceTO financeTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.saveNewTax(financeTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public ArrayList processTaxAlterList(String taxId) throws FPBusinessException, FPRuntimeException {
        ArrayList accountTypeList = new ArrayList();
        accountTypeList = financeDAO.processTaxAlterList(taxId);
        if (accountTypeList.size() == 0) {
            throw new FPBusinessException("EM-CUST-01");
        }
        return accountTypeList;
    }

    public int saveAlterTax(FinanceTO financeTO, String taxId, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.saveAlterTax(financeTO, taxId, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    /**
     * This method used to get Country List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getCountryList() throws FPBusinessException, FPRuntimeException {
        ArrayList countryList = new ArrayList();
        countryList = financeDAO.getCountryList();
//         if (groupList.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
//        }
        return countryList;
    }

    public int saveNewCountry(FinanceTO financeTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.saveNewCountry(financeTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public ArrayList processCountryAlterList(String countryID) throws FPBusinessException, FPRuntimeException {
        ArrayList countryList = new ArrayList();
        countryList = financeDAO.processCountryAlterList(countryID);
        if (countryList.size() == 0) {
            throw new FPBusinessException("EM-CUST-01");
        }
        return countryList;
    }

    public int saveAlterCountry(FinanceTO financeTO, String countryID, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.saveAlterCountry(financeTO, countryID, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    /**
     * This method used to get State List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getStateList() throws FPBusinessException, FPRuntimeException {
        ArrayList countryList = new ArrayList();
        countryList = financeDAO.getStateList();
//         if (groupList.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
//        }
        return countryList;
    }

    public int saveNewState(FinanceTO financeTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.saveNewState(financeTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public ArrayList processStatealterList(String stateID) throws FPBusinessException, FPRuntimeException {
        ArrayList stateList = new ArrayList();
        stateList = financeDAO.processStatealterList(stateID);
        if (stateList.size() == 0) {
            throw new FPBusinessException("EM-CUST-01");
        }
        return stateList;
    }

    public int saveAlterState(FinanceTO financeTO, String stateID, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.saveAlterState(financeTO, stateID, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    /**
     * This method used to get District List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getDistrictList() throws FPBusinessException, FPRuntimeException {
        ArrayList DistrictList = new ArrayList();
        DistrictList = financeDAO.getDistrictList();
//         if (groupList.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
//        }
        return DistrictList;
    }

    public int saveNewDistrict(FinanceTO financeTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.saveNewDistrict(financeTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public ArrayList processDistrictalterList(String districtID) throws FPBusinessException, FPRuntimeException {
        ArrayList stateList = new ArrayList();
        stateList = financeDAO.processDistrictalterList(districtID);
        if (stateList.size() == 0) {
            throw new FPBusinessException("EM-CUST-01");
        }
        return stateList;
    }

    public int saveAlterDistrict(FinanceTO financeTO, String districtID, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.saveAlterDistrict(financeTO, districtID, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    /**
     * This method used to get Contra Entry.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getcontraEntryList() throws FPBusinessException, FPRuntimeException {
        ArrayList contraEntryList = new ArrayList();
        contraEntryList = financeDAO.getcontraEntryList();
//         if (groupList.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
//        }
        return contraEntryList;
    }

    public ArrayList getBankLedgerList() throws FPBusinessException, FPRuntimeException {
        //////System.out.println("in BP");
        ArrayList bankLedgerList = new ArrayList();
        bankLedgerList = financeDAO.getBankLedgerList();
        //////System.out.println("bankLedgerList  BP === " + bankLedgerList);
        return bankLedgerList;

    }

    public int insertContraEntry(int detailCode, int userId, String date, String bankid1Values, String amountValues, String accTypeValues, String narrationValues, String voucherCode) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.insertContraEntry(detailCode, userId, date, bankid1Values, amountValues, accTypeValues, narrationValues, voucherCode);
        //////System.out.println("status in BP= " + status);
        return status;
    }

    /**
     * This method used to get journal Entry.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getInvoiceList() throws FPBusinessException, FPRuntimeException {
        //////System.out.println("in BP");
        ArrayList invoiceList = new ArrayList();
        invoiceList = financeDAO.getInvoiceList();
        //////System.out.println("getInvoiceList  BP === " + invoiceList);
        return invoiceList;

    }

    /**
     * This method used to get Credit Note.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getCreditNoteList() throws FPBusinessException, FPRuntimeException {
        ArrayList bankLedgerList = new ArrayList();
        bankLedgerList = financeDAO.getBankLedgerList();
//         if (groupList.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
//        }
        return bankLedgerList;
    }

    public int insertCreditNote(int detailCode, int userId, String date, String bankid1Values, String amountValues, String accTypeValues, String narrationValues) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.insertCreditNote(detailCode, userId, date, bankid1Values, amountValues, accTypeValues, narrationValues);
        //////System.out.println("status in BP= " + status);
        return status;
    }

    /**
     * This method used to get Debit Note.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getdebitNoteList() throws FPBusinessException, FPRuntimeException {
        ArrayList bankLedgerList = new ArrayList();
        bankLedgerList = financeDAO.getBankLedgerList();
//         if (groupList.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
//        }
        return bankLedgerList;
    }

    public int insertDebitNote(int detailCode, int userId, String date, String bankid1Values, String amountValues, String accTypeValues, String narrationValues) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.insertDebitNote(detailCode, userId, date, bankid1Values, amountValues, accTypeValues, narrationValues);
        //////System.out.println("status in BP= " + status);
        return status;
    }

    /**
     * This method used to get Contra Entry.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getjournalEntryList() throws FPBusinessException, FPRuntimeException {
        ArrayList journalEntryList = new ArrayList();
        journalEntryList = financeDAO.getjournalEntryList();
//         if (groupList.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
//        }
        return journalEntryList;
    }

    /**
     * This method used to get Journal Entry.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getVoucherCode() throws FPBusinessException, FPRuntimeException {
        String voucherCode = "";
        voucherCode = financeDAO.getVoucherCode();
        return voucherCode;
    }

    public int insertJournalEntry(int detailCode, int userId, String date, String bankid1Values, String amountValues, String accTypeValues, String narrationValues, String voucherCode) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.insertJournalEntry(detailCode, userId, date, bankid1Values, amountValues, accTypeValues, narrationValues, voucherCode);
        //////System.out.println("status in BP= " + status);
        return status;
    }

    /**
     * This method used to get Journal Code List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getJournalCodeList() throws FPBusinessException, FPRuntimeException {
        ArrayList journalCodeList = new ArrayList();
        journalCodeList = financeDAO.getJournalCodeList();
        return journalCodeList;
    }

    /**
     * This method used to get Journal Entry Credit List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getJournalEntryCreditList() throws FPBusinessException, FPRuntimeException {
        ArrayList journalEntryCreditList = new ArrayList();
        journalEntryCreditList = financeDAO.getJournalEntryCreditList();
        return journalEntryCreditList;
    }

    /**
     * This method used to get Journal Entry Debit List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getJournalEntryDebitList() throws FPBusinessException, FPRuntimeException {
        ArrayList journalEntryDebitList = new ArrayList();
        journalEntryDebitList = financeDAO.getJournalEntryDebitList();
        return journalEntryDebitList;
    }

    /**
     * This method used to get Contra Code List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getContraCodeList() throws FPBusinessException, FPRuntimeException {
        ArrayList contraCodeList = new ArrayList();
        contraCodeList = financeDAO.getContraCodeList();
        return contraCodeList;
    }

    /**
     * This method used to get Contra Entry Credit List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getContraEntryCreditList() throws FPBusinessException, FPRuntimeException {
        ArrayList contraEntryCreditList = new ArrayList();
        contraEntryCreditList = financeDAO.getContraEntryCreditList();
        return contraEntryCreditList;
    }

    /**
     * This method used to get Contra Entry Debit List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getContraEntryDebitList() throws FPBusinessException, FPRuntimeException {
        ArrayList contraEntryDebitList = new ArrayList();
        contraEntryDebitList = financeDAO.getContraEntryDebitList();
        return contraEntryDebitList;
    }

    /**
     * This method used to get Cash Payment List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getCashPaymentList() throws FPBusinessException, FPRuntimeException {
        ArrayList cashPaymentList = new ArrayList();
        cashPaymentList = financeDAO.getCashPaymentList();
        return cashPaymentList;
    }

    /**
     * This method used to get Bank Payment List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getBankPaymentList() throws FPBusinessException, FPRuntimeException {
        ArrayList bankPaymentList = new ArrayList();
        bankPaymentList = financeDAO.getBankPaymentList();
        return bankPaymentList;
    }

    /**
     * This method used to get Cash Receipt List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getCashReceiptList() throws FPBusinessException, FPRuntimeException {
        ArrayList cashReceiptList = new ArrayList();
        cashReceiptList = financeDAO.getCashReceiptList();
        return cashReceiptList;
    }

    /**
     * This method used to get Bank Receipt List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getBankReceiptList() throws FPBusinessException, FPRuntimeException {
        ArrayList bankReceiptList = new ArrayList();
        bankReceiptList = financeDAO.getBankReceiptList();
        return bankReceiptList;
    }

    /**
     * This method used to get Journal Entry.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getJournalEntry(String voucherCode) throws FPBusinessException, FPRuntimeException {
        ArrayList journalEntry = new ArrayList();
        journalEntry = financeDAO.getJournalEntry(voucherCode);
        return journalEntry;
    }

    /**
     * This method used to get Bank Payment Entry.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getBankPaymentEntryPrint(String voucherCode) throws FPBusinessException, FPRuntimeException {
        ArrayList bankPaymentEntry = new ArrayList();
        bankPaymentEntry = financeDAO.getBankPaymentEntryPrint(voucherCode);
        return bankPaymentEntry;
    }

    /**
     * This method used to get Cash Payment Entry.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getCashPaymentEntry(String voucherCode) throws FPBusinessException, FPRuntimeException {
        ArrayList cashPaymentEntry = new ArrayList();
        cashPaymentEntry = financeDAO.getCashPaymentEntry(voucherCode);
        return cashPaymentEntry;
    }

    /**
     * This method used to get Cash Receipt Entry.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getCashReceiptEntry(String voucherCode) throws FPBusinessException, FPRuntimeException {
        ArrayList cashReceiptEntry = new ArrayList();
        cashReceiptEntry = financeDAO.getCashReceiptEntry(voucherCode);
        return cashReceiptEntry;
    }

    /**
     * This method used to get Bank Receipt Entry.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getBankReceiptEntry(String voucherCode) throws FPBusinessException, FPRuntimeException {
        ArrayList bankReceiptEntry = new ArrayList();
        bankReceiptEntry = financeDAO.getBankReceiptEntry(voucherCode);
        return bankReceiptEntry;
    }

    /**
     * This method used to get Contra Entry.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getContraEntry(String voucherCode) throws FPBusinessException, FPRuntimeException {
        ArrayList contraEntry = new ArrayList();
        contraEntry = financeDAO.getContraEntry(voucherCode);
        return contraEntry;
    }

    /**
     * This method used to get VAT List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getVatList() throws FPBusinessException, FPRuntimeException {
        ArrayList vatList = new ArrayList();
        vatList = financeDAO.getVatList();
//         if (groupList.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
//        }
        return vatList;
    }

    public ArrayList getJournalLedgerList() throws FPBusinessException, FPRuntimeException {
        //////System.out.println("in BP");
        ArrayList journalLedgerList = new ArrayList();
        journalLedgerList = financeDAO.getJournalLedgerList();
        //////System.out.println("bankLedgerList  BP === " + journalLedgerList);
        return journalLedgerList;

    }

    public ArrayList getLedgerTransactionList(String ledgerId, String sDate, String eDate) throws FPBusinessException, FPRuntimeException {
        //////System.out.println("in BP");
        ArrayList ledgerTransactionList = new ArrayList();
        ledgerTransactionList = financeDAO.getLedgerTransactionList(ledgerId, sDate, eDate);
        //////System.out.println("ledgerTransactionList  BP === " + ledgerTransactionList);
        return ledgerTransactionList;

    }

    public ArrayList getDayBook(String fromDate, String toDate) throws FPBusinessException, FPRuntimeException {
        //////System.out.println("in BP");
        ArrayList ledgerTransactionList = new ArrayList();
        ledgerTransactionList = financeDAO.getDayBook(fromDate, toDate);
        //////System.out.println("ledgerTransactionList  BP === " + ledgerTransactionList);
        return ledgerTransactionList;

    }

    public ArrayList getTBTransactionList(String levelId, String sDate, String eDate) throws FPBusinessException, FPRuntimeException {
        //////System.out.println("in BP");
        ArrayList ledgerTransactionList = new ArrayList();
        ArrayList ledgerTransactionListNew = new ArrayList();
        ledgerTransactionList = financeDAO.getTBTransactionList(levelId, sDate, eDate);
        //////System.out.println("getTBTransactionList  BP === " + ledgerTransactionList);

        Iterator itr;
        itr = ledgerTransactionList.iterator();
        FinanceTO fto = null;
        FinanceTO newfto = new FinanceTO();
        float credit = 0.00F;
        float debit = 0.00F;
        boolean addStatus = true;
        while (itr.hasNext()) {
            fto = new FinanceTO();
            fto = (FinanceTO) itr.next();
            //////System.out.println("fto.getLedgerName():" + fto.getLedgerName());
            //////System.out.println("newfto.getLedgerName():" + newfto.getLedgerName());
            //////System.out.println("addStatus:" + addStatus);
            if (fto.getLedgerName().equals(newfto.getLedgerName())) {
                //////System.out.println("am here 1");
                if (fto.getAccountType().equalsIgnoreCase("Credit")) {
                    newfto.setCredit(fto.getAccountAmount());
                } else {
                    newfto.setDebit(fto.getAccountAmount());

                }
                newfto.resetAccountType();
                ledgerTransactionListNew.add(newfto);
                addStatus = true;
                newfto = new FinanceTO();
            } else //////System.out.println("am here 2");
            if (addStatus) {
                //////System.out.println("am here 3");
                newfto.setLedgerName(fto.getLedgerName());
                newfto.setAccountEntryDate(fto.getAccountEntryDate());
                newfto.setAccountAmount(fto.getAccountAmount());
                newfto.setAccountType(fto.getAccountType());
                newfto.setAccountNarration(fto.getAccountNarration());
                newfto.setTripId(fto.getTripId());

                if (fto.getAccountType().equalsIgnoreCase("Credit")) {
                    newfto.setCredit(fto.getAccountAmount());
                } else {
                    newfto.setDebit(fto.getAccountAmount());

                }
                addStatus = false;
            } else {
                //////System.out.println("am here 4");
                newfto.resetAccountType();
                ledgerTransactionListNew.add(newfto);

                newfto = new FinanceTO();
                newfto.setLedgerName(fto.getLedgerName());
                newfto.setAccountEntryDate(fto.getAccountEntryDate());
                newfto.setAccountAmount(fto.getAccountAmount());
                newfto.setAccountType(fto.getAccountType());
                newfto.setAccountNarration(fto.getAccountNarration());
                newfto.setTripId(fto.getTripId());

                if (fto.getAccountType().equalsIgnoreCase("Credit")) {
                    newfto.setCredit(fto.getAccountAmount());
                } else {
                    newfto.setDebit(fto.getAccountAmount());

                }
                addStatus = false;
            }
        }
        if (!addStatus) {
            //////System.out.println("am here 5");
            newfto.resetAccountType();
            ledgerTransactionListNew.add(newfto);
        }
        return ledgerTransactionListNew;

    }

    public String getOpeningBalance(String ledgerId, String sDate) throws FPBusinessException, FPRuntimeException {
        //////System.out.println("in BP");
        String openingBalance = financeDAO.getOpeningBalance(ledgerId, sDate);
        return openingBalance;

    }
     public String getClosingBalance(String ledgerId, String sDate) throws FPBusinessException, FPRuntimeException {
        System.out.println("in BP");
        String closingBalance = financeDAO.getClosingBalance(ledgerId, sDate);
        return closingBalance;

    }

    public ArrayList getLedgerTransactionListOld(String ledgerId) throws FPBusinessException, FPRuntimeException {
        //////System.out.println("in BP");
        ArrayList ledgerTransactionList = new ArrayList();
        ledgerTransactionList = financeDAO.getLedgerTransactionListOld(ledgerId);
        //////System.out.println("ledgerTransactionList  BP === " + ledgerTransactionList);
        return ledgerTransactionList;

    }

    public ArrayList getPaymentLedgerList() throws FPBusinessException, FPRuntimeException {
        //////System.out.println("in BP");
        ArrayList paymentLedgerList = new ArrayList();
        paymentLedgerList = financeDAO.getPaymentLedgerList();
        //////System.out.println("bankLedgerList  BP === " + paymentLedgerList);
        return paymentLedgerList;
    }
    public ArrayList getContraPaymentLedgerList() throws FPBusinessException, FPRuntimeException {
        //////System.out.println("in BP");
        ArrayList paymentLedgerList = new ArrayList();
        paymentLedgerList = financeDAO.getContraPaymentLedgerList();
        //////System.out.println("bankLedgerList  BP === " + paymentLedgerList);
        return paymentLedgerList;
    }

    public ArrayList getBankPaymentLedgerList() throws FPBusinessException, FPRuntimeException {
        //////System.out.println("in BP");
        ArrayList bankPaymentLedgerList = new ArrayList();
        bankPaymentLedgerList = financeDAO.getBankPaymentLedgerList();
        //////System.out.println("bankLedgerList  BP === " + bankPaymentLedgerList);
        return bankPaymentLedgerList;

    }

    /* public int insertPaymentEntry(int cashDetailCode, String totalCreditAmt, int detailCode, int userId, String date, String bankid1Values, String amountValues, String accTypeValues, String narrationValues, String cashLedgerValues, String searchCode, String accountType) throws FPRuntimeException, FPBusinessException {
     int status = 0;
     SqlMapClient session = financeDAO.getSqlMapClient();
     try{
     session.startTransaction();
     status = financeDAO.insertPaymentEntry(cashDetailCode, totalCreditAmt, detailCode, userId, date, bankid1Values, amountValues, accTypeValues, narrationValues, cashLedgerValues, searchCode, accountType, session);
     session.commitTransaction();
     } catch (Exception e) {
     e.printStackTrace();
     } finally {
     //System.out.println("am here 7");
     try {
     session.endTransaction();
     } catch (Exception ex) {
     ex.printStackTrace();
     //System.out.println("am here 8" + ex.getMessage());
     } finally {
     session.getSession().close();
     }
     }
     return status;
     }

     public int insertBankPaymentEntry(String chequeDate, String chequeNo, String BankHead, int cashDetailCode, String totalCreditAmt, int detailCode, int userId, String date, String bankid1Values, String amountValues, String accTypeValues, String narrationValues) throws FPRuntimeException, FPBusinessException {
     int status = 0;
     SqlMapClient session = financeDAO.getSqlMapClient();
     try{
     session.startTransaction();
     status = financeDAO.insertBankPaymentEntry(chequeDate, chequeNo, BankHead, cashDetailCode, totalCreditAmt, detailCode, userId, date, bankid1Values, amountValues, accTypeValues, narrationValues, session);
     session.commitTransaction();
     } catch (Exception e) {
     e.printStackTrace();
     } finally {
     //System.out.println("am here 7");
     try {
     session.endTransaction();
     } catch (Exception ex) {
     ex.printStackTrace();
     //System.out.println("am here 8" + ex.getMessage());
     } finally {
     session.getSession().close();
     }
     }
     return status;
     }
     */
    public ArrayList receiptEntry() throws FPBusinessException, FPRuntimeException {
        //////System.out.println("in BP");
        ArrayList receiptLedgerList = new ArrayList();
        receiptLedgerList = financeDAO.receiptEntry();
        //////System.out.println("receiptLedgerList  BP === " + receiptLedgerList);
        return receiptLedgerList;

    }
    /*
     public int insertReceiptEntry(String totalDebitAmt, int cashDetailCode, int detailCode, int userId, String date, String bankid1Values, String amountValues, String accTypeValues, String narrationValues, String cashLedgerval) throws FPRuntimeException, FPBusinessException {
     int status = 0;
     status = financeDAO.insertReceiptEntry(totalDebitAmt, cashDetailCode, detailCode, userId, date, bankid1Values, amountValues, accTypeValues, narrationValues, cashLedgerval);
     //////System.out.println("status in BP= " + status);
     return status;
     }

     public int insertBankReceiptEntry(String chequeDate, String chequeNo, String BankHead, int cashDetailCode, String totalDebitAmt, int detailCode, int userId, String date, String bankid1Values, String amountValues, String accTypeValues, String narrationValues) throws FPRuntimeException, FPBusinessException {
     int status = 0;
     status = financeDAO.insertBankReceiptEntry(chequeDate, chequeNo, BankHead, cashDetailCode, totalDebitAmt, detailCode, userId, date, bankid1Values, amountValues, accTypeValues, narrationValues);
     //////System.out.println("status in BP= " + status);
     return status;
     }
     */

    public ArrayList getInvoiceList(FinanceTO financeTO) throws FPBusinessException, FPRuntimeException {
        ArrayList invoiceList = new ArrayList();
        invoiceList = financeDAO.getInvoiceList(financeTO);
        return invoiceList;

    }

    public ArrayList getPurchaseList(FinanceTO financeTO) throws FPBusinessException, FPRuntimeException {
        ArrayList invoiceList = new ArrayList();
        invoiceList = financeDAO.getPurchaseList(financeTO);
        return invoiceList;

    }

    public ArrayList getPendingPurchaseDetails(FinanceTO financeTO) throws FPBusinessException, FPRuntimeException {
        ArrayList invoiceList = new ArrayList();
        invoiceList = financeDAO.getPendingPurchaseDetails(financeTO);
        return invoiceList;

    }

    public ArrayList getInvoiceDetails(FinanceTO financeTO) throws FPBusinessException, FPRuntimeException {
        ArrayList invoiceDetails = new ArrayList();
        invoiceDetails = financeDAO.getInvoiceDetails(financeTO);
        return invoiceDetails;

    }
/*
    public int saveInvoicePaymentDetails(FinanceTO financeTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        SqlMapClient session = financeDAO.getSqlMapClient();
        status = financeDAO.saveInvoicePaymentDetails(financeTO, userId, session);
        financeTO.setInvoiceDetailId(String.valueOf(status));
        String[] invoiceIds = financeTO.getInvoiceIds();
        String[] paidAmounts = financeTO.getPaidAmounts();
        for (int i = 0; i < invoiceIds.length; i++) {
            financeTO.setInvoiceId(invoiceIds[i]);
            financeTO.setPayAmount(paidAmounts[i]);
            status = financeDAO.saveInvoicePayment(financeTO, userId, session);
        }
        return status;
    }*/

    public int saveInvoicePaymentDetails(FinanceTO financeTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        SqlMapClient session = financeDAO.getSqlMapClient();
        System.out.println("bp>>>>>>>>>>>>>>>>>");
        try {
            session.startTransaction();
            status = financeDAO.saveInvoicePaymentDetails(financeTO, userId, session);

            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                session.getSession().close();
            }
        }

        return status;
    }
    public int savePurchasePaymentDetails(FinanceTO financeTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        SqlMapClient session = financeDAO.getSqlMapClient();
        System.out.println("bp>>>>>>>>>>>>>>>>>");
        try {
            session.startTransaction();
            status = financeDAO.savePurchasePaymentDetails(financeTO, userId, session);

            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                session.getSession().close();
            }
        }

        return status;
    }

    public ArrayList getPendingInvoiceDetails(FinanceTO financeTO) throws FPBusinessException, FPRuntimeException {
        ArrayList invoiceList = new ArrayList();
        invoiceList = financeDAO.getPendingInvoiceDetails(financeTO);
        return invoiceList;

    }

//    public int saveInvoicePayment(FinanceTO financeTO, int userId) throws FPRuntimeException, FPBusinessException {
//        int status = 0;
//        SqlMapClient session = financeDAO.getSqlMapClient();
//        status = financeDAO.saveInvoicePayment(financeTO, userId, session);
//        return status;
//    }
//
//    public int savePurchasePayment(FinanceTO financeTO, int userId) throws FPRuntimeException, FPBusinessException {
//        int status = 0;
//        status = financeDAO.savePurchasePayment(financeTO, userId);
//        return status;
//    }

    public int saveDebitNote(FinanceTO financeTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.saveDebitNote(financeTO, userId);
        return status;
    }

    public int saveCreditNote(FinanceTO financeTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.saveCreditNote(financeTO, userId);
        return status;
    }

    public ArrayList getFinanceYearList() throws FPBusinessException, FPRuntimeException {
        ArrayList financeYearList = new ArrayList();
        financeYearList = financeDAO.getFinanceYearList();
        return financeYearList;
    }

    public ArrayList getFinanceYear(FinanceTO financeTO) throws FPBusinessException, FPRuntimeException {
        ArrayList financeYear = new ArrayList();
        financeYear = financeDAO.getFinanceYear(financeTO);
        return financeYear;
    }

    public String checkBankAccountName(FinanceTO financeTO) throws FPBusinessException, FPRuntimeException {
        String bankName = financeDAO.checkBankAccountName(financeTO);
        return bankName;

    }

    public String getChargeCodeList(FinanceTO financeTO) throws FPBusinessException, FPRuntimeException {
        String chargecode = financeDAO.getChargeCodeList(financeTO);
        return chargecode;

    }

    public String checktaxName(FinanceTO financeTO) throws FPBusinessException, FPRuntimeException {
        String taxName = financeDAO.checktaxName(financeTO);
        return taxName;

    }

    public ArrayList getCreditNoteSearchList(FinanceTO financeTO) throws FPBusinessException, FPRuntimeException {
        ArrayList creditNoteSearchList = new ArrayList();
        creditNoteSearchList = financeDAO.getCreditNoteSearchList(financeTO);
        return creditNoteSearchList;
    }

    public ArrayList getLedgerDetailsGrp(String ledgerName, String entryType) {
        ArrayList getLedgerDetailsGrp = new ArrayList();
        getLedgerDetailsGrp = financeDAO.getLedgerDetailsGrp(ledgerName, entryType);
        return getLedgerDetailsGrp;
    }

    public ArrayList getGroupLevelList(FinanceTO financeTO) throws FPBusinessException, FPRuntimeException {
        ArrayList groupLevelList = new ArrayList();
        boolean status = true;
        ArrayList list = new ArrayList();
        ArrayList levelOneList = new ArrayList();
        ArrayList levelTwoList = new ArrayList();
        ArrayList levelThreeList = new ArrayList();
        ArrayList levelFourList = new ArrayList();

        FinanceTO levelOneFinTO = new FinanceTO();
        FinanceTO levelTwoFinTO = new FinanceTO();
        FinanceTO levelThreeFinTO = new FinanceTO();
        FinanceTO levelFourFinTO = new FinanceTO();
        int size = 4;
        while (status) {
            groupLevelList = financeDAO.getGroupLevelList(financeTO);
            Iterator itr = groupLevelList.iterator();
            int levelOneSize = 0;
            levelOneSize = groupLevelList.size();
            while (itr.hasNext()) {
                levelOneSize--;
                levelOneFinTO = (FinanceTO) itr.next();
                financeTO.setReference(levelOneFinTO.getLevelgroupId());
                levelOneList.add(levelOneFinTO);

                if (levelOneSize == 0) {
                    groupLevelList = financeDAO.getGroupLevelList(financeTO);
                    itr = groupLevelList.iterator();
                    while (itr.hasNext()) {
                        levelTwoFinTO = (FinanceTO) itr.next();
                    }

                }
            }
            size--;
            if (size == 1) {
                status = false;
            }
        }
        return list;
    }

    public ArrayList getCashLedgerDetails(String cashHeadName, String entryType) {
        ArrayList getCashLedgerDetails = new ArrayList();
        getCashLedgerDetails = financeDAO.getCashLedgerDetails(cashHeadName, entryType);
        return getCashLedgerDetails;
    }

    public ArrayList processVendorList(String vendorType)
            throws FPBusinessException, FPRuntimeException {
        ArrayList vendorList = new ArrayList();
        vendorList = financeDAO.getVendorList(vendorType);
       // if (vendorList.size() == 0) {
        // throw new FPBusinessException("EM-MPR-02");
        //} else {
        return vendorList;
        // }
    }

    

    public ArrayList getPrimaryLevelMasterList() throws FPRuntimeException, FPBusinessException {
        ArrayList levelMasterList = new ArrayList();
        levelMasterList = financeDAO.getPrimaryLevelMasterList();
        return levelMasterList;
    }

    public ArrayList getNextLevelList(String levelId) throws FPRuntimeException, FPBusinessException {
        ArrayList levelMasterList = new ArrayList();
        levelMasterList = financeDAO.getNextLevelList(levelId);
        return levelMasterList;
    }


    public ArrayList getAccountTypeDetails(String accountType, String searchCode)
            throws FPBusinessException, FPRuntimeException {
        ArrayList getAccountTypeDetails = new ArrayList();
        getAccountTypeDetails = financeDAO.getAccountTypeDetails(accountType, searchCode);
        return getAccountTypeDetails;
    }

    public ArrayList getBankLedgerDetails(String cashHeadName, String entryType) {
        ArrayList getBankLedgerDetails = new ArrayList();
        getBankLedgerDetails = financeDAO.getBankLedgerDetails(cashHeadName, entryType);
        return getBankLedgerDetails;
    }

    //pavi
    public int insertCRJ(FinanceTO financeTO, int userId, String vendorTypeId, String vendorNameId, String crjDate, String invoiceNo, String invoiceDate,
            String invoiceAmount, String remarks, String ledgerId,String actualFilePath, String fileSaved,String debitLedgerId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        SqlMapClient session = financeDAO.getSqlMapClient();
        System.out.println("bp>>>>>>>>>>>>>>>>>");
        try {
            session.startTransaction();
            status = financeDAO.insertCRJ(financeTO, userId, vendorTypeId, vendorNameId, crjDate, invoiceNo, invoiceDate,
                    invoiceAmount, remarks, ledgerId,actualFilePath, fileSaved,debitLedgerId, session);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                session.getSession().close();
            }
        }

        return status;
    }

    public ArrayList getExpenseGroupList() throws FPRuntimeException, FPBusinessException {
        ArrayList expenseGroupList = new ArrayList();
        expenseGroupList = financeDAO.getExpenseGroupList();
        return expenseGroupList;
    }

 public ArrayList getExpenseLedgerList(FinanceTO financeTO) throws FPRuntimeException, FPBusinessException {
        ArrayList expenseGroupList = new ArrayList();
        expenseGroupList = financeDAO.getExpenseLedgerList(financeTO);
        return expenseGroupList;
    }

    public int updateClearnceDate(FinanceTO financeTO, int userId, String clearanceDate, String clearanceStatus, String clearanceremark, String bankPaymentId, String debitLedgerId,
            String creditLedgerId, String chequeAmount, String chequeNo, String chequeDate, String entryType,String formReferenceCode) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        SqlMapClient session = financeDAO.getSqlMapClient();
        System.out.println("bp>>>>>>>>>>>>>>>>>");
        try {
            session.startTransaction();
            status = financeDAO.updateClearnceDate(financeTO, userId, clearanceDate, clearanceStatus, clearanceremark, bankPaymentId, debitLedgerId, creditLedgerId, chequeAmount,
                    chequeNo, chequeDate, entryType, formReferenceCode,session);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                session.getSession().close();
            }
        }

        return status;
    }

    public ArrayList getBPClearanceDetails(FinanceTO financeTO) throws FPBusinessException, FPRuntimeException {
        ArrayList BPClearanceDetails = new ArrayList();
        BPClearanceDetails = financeDAO.getBPClearanceDetails(financeTO);
        return BPClearanceDetails;
    }

    public ArrayList getBRSChequeList(FinanceTO financeTO, String val) throws FPBusinessException, FPRuntimeException {
        ArrayList list = new ArrayList();
        list = financeDAO.getBRSChequeList(financeTO, val);
        return list;
    }

    public int insertPaymentEntry(FinanceTO financeTO,String totalCreditAmt, int userId, String fromDate, String[] bankid1Val, String[] amountVal,
            String[] accTypeVal, String[] narrationVal, String voucherIdEdit, String voucherCode,
            String activeInd,String vehicleId,String costCenterNarration) throws FPRuntimeException, FPBusinessException {
        SqlMapClient session = financeDAO.getSqlMapClient();
        int status = 0;
        String bankid1Values = null;
        String amountValues = null;
        String accTypeValues = null;
        String narrationValues = null;
        int detailCode = 0;
        int formId = 0;
        int voucherNo = 0;
        int reversalStatus = 0;
        int saveCostCenter = 0;
        try {
            session.startTransaction();
            int cashDetailCode = (bankid1Val.length + 1);
            System.out.println("bankid1Val.length = " + (bankid1Val.length + 1));
            /**
             * ***************getFormVoucherNo******************
             */
            formId = Integer.parseInt(ThrottleConstants.CashPaymentFormId);
            if ("".equals(voucherIdEdit)) {
                voucherNo = financeDAO.getFormVoucherNo(formId, session);
            } else {
                voucherNo = Integer.parseInt(voucherIdEdit);
                reversalStatus = financeDAO.reverseAccountEntry(voucherCode, fromDate, formId, userId, voucherNo, session);
            }

            String voucherCodeNo = ThrottleConstants.CashPaymentVoucherCode + voucherNo;
            System.out.println(" voucherCodeNo" + voucherCodeNo);
            System.out.println(" voucherNo insertStatus" + voucherNo);
            /**
             * *******************
             */
            for (int i = 0; i < bankid1Val.length; i++) {
                detailCode++;
                bankid1Values = bankid1Val[i];
                amountValues = amountVal[i];
                accTypeValues = accTypeVal[i];
                narrationValues = narrationVal[i];
                status = financeDAO.insertPaymentEntry(cashDetailCode, totalCreditAmt, detailCode, userId, fromDate, bankid1Values, amountValues, accTypeValues, narrationValues, voucherNo, formId, voucherCodeNo, session);
            }
            if(!"0".equalsIgnoreCase(activeInd) && status !=0){
                saveCostCenter=financeDAO.insertCostCenter(financeTO,userId,status,fromDate,amountValues,vehicleId,costCenterNarration,"DEBIT",session);
            }
            System.out.println("status in BP= " + status);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }

    public int insertReceiptEntry(String totalDebitAmt, int userId, String fromDate, String[] bankid1Val, String[] amountVal, String[] accTypeVal, String[] narrationVal, String voucherIdEdit, String voucherCode) throws FPRuntimeException, FPBusinessException {
        SqlMapClient session = financeDAO.getSqlMapClient();
        int status = 0;
        String bankid1Values = null;
        String amountValues = null;
        String accTypeValues = null;
        String narrationValues = null;
        int detailCode = 0;
        int formId = 0;
        int voucherNo = 0;
        int reversalStatus = 0;
        try {
            session.startTransaction();
            int cashDetailCode = (bankid1Val.length + 1);
            System.out.println("bankid1Val.length = " + (bankid1Val.length + 1));
            /**
             * ***************getFormVoucherNo******************
             */
            formId = Integer.parseInt(ThrottleConstants.CashReceiptFormId);
            if ("".equals(voucherIdEdit)) {
                voucherNo = financeDAO.getFormVoucherNo(formId, session);
            } else {
                voucherNo = Integer.parseInt(voucherIdEdit);
                reversalStatus = financeDAO.reverseAccountEntry(voucherCode, fromDate, formId, userId, voucherNo, session);
            }

            String voucherCodeNo = ThrottleConstants.CashReceiptVoucherCode + voucherNo;
            System.out.println("voucherCodeNo" + voucherCodeNo);
            System.out.println(" voucherNo insertStatus" + voucherNo);
            /**
             * *******************
             */
            for (int i = 0; i < bankid1Val.length; i++) {
                detailCode++;
                bankid1Values = bankid1Val[i];
                amountValues = amountVal[i];
                accTypeValues = accTypeVal[i];
                narrationValues = narrationVal[i];
                status = financeDAO.insertReceiptEntry(totalDebitAmt, cashDetailCode, detailCode, userId, fromDate, bankid1Values, amountValues, accTypeValues, narrationValues, voucherNo, formId, voucherCodeNo, session);
            }
            System.out.println("status in BP= " + status);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }

    public int insertBankPaymentEntry(FinanceTO financeTO,String chequeDate, String chequeNo, String BankHead, String totalCreditAmt, int userId,
            String fromDate, String[] bankid1Val, String[] amountVal, String[] accTypeVal, String[] narrationVal, 
            String voucherIdEdit, String voucherCode,String activeInd,String vehicleId,String costCenterNarration) throws FPRuntimeException, FPBusinessException {
        SqlMapClient session = financeDAO.getSqlMapClient();
        int status = 0;
        String bankid1Values = null;
        String amountValues = null;
        String accTypeValues = null;
        String narrationValues = null;
        int detailCode = 0;
        int formId = 0;
        int voucherNo = 0;
        int reversalStatus = 0;
        int saveCostCenter = 0;
        try {
            session.startTransaction();
            int cashDetailCode = (bankid1Val.length + 1);
            System.out.println("bankid1Val.length = " + (bankid1Val.length + 1));
            /**
             * ***************getFormVoucherNo******************
             */
            formId = Integer.parseInt(ThrottleConstants.BankPaymentFormId);
            if ("".equals(voucherIdEdit)) {
                voucherNo = financeDAO.getFormVoucherNo(formId, session);
            } else {
                voucherNo = Integer.parseInt(voucherIdEdit);
                reversalStatus = financeDAO.reverseAccountEntry(voucherCode, fromDate, formId, userId, voucherNo, session);
            }
            String voucherCodeNo = ThrottleConstants.BankPaymentVoucherCode + voucherNo;
            System.out.println(" voucherNo insertStatus" + voucherNo);
            /**
             * *******************
             */
            for (int i = 0; i < bankid1Val.length; i++) {
                detailCode++;
                bankid1Values = bankid1Val[i];
                amountValues = amountVal[i];
                accTypeValues = accTypeVal[i];
                narrationValues = narrationVal[i];
                status = financeDAO.insertBankPaymentEntry(chequeDate, chequeNo, BankHead, totalCreditAmt, cashDetailCode, detailCode, userId, fromDate, bankid1Values, amountValues, accTypeValues, narrationValues, voucherNo, formId, voucherCodeNo, session);
            }
            if(!"0".equalsIgnoreCase(activeInd) && status !=0){
                saveCostCenter=financeDAO.insertCostCenter(financeTO,userId,status,fromDate,amountValues,vehicleId,costCenterNarration,"DEBIT",session);
            }
            System.out.println("status in BP= " + status);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }

    public int insertBankReceiptEntry(String chequeDate, String chequeNo, String BankHead, String totalDebitAmt, int userId, String fromDate, String[] bankid1Val, String[] amountVal, String[] accTypeVal, String[] narrationVal, String voucherIdEdit, String voucherCode) throws FPRuntimeException, FPBusinessException {
        SqlMapClient session = financeDAO.getSqlMapClient();
        int status = 0;
        String bankid1Values = null;
        String amountValues = null;
        String accTypeValues = null;
        String narrationValues = null;
        int detailCode = 0;
        int formId = 0;
        int voucherNo = 0;
        int reversalStatus = 0;
        try {
            session.startTransaction();
            int cashDetailCode = (bankid1Val.length + 1);
            System.out.println("bankid1Val.length = " + (bankid1Val.length + 1));
            /**
             * ***************getFormVoucherNo******************
             */
            formId = Integer.parseInt(ThrottleConstants.BankReceiptFormId);
            if ("".equals(voucherIdEdit)) {
                voucherNo = financeDAO.getFormVoucherNo(formId, session);
            } else {
                voucherNo = Integer.parseInt(voucherIdEdit);
                reversalStatus = financeDAO.reverseAccountEntry(voucherCode, fromDate, formId, userId, voucherNo, session);
            }
            String voucherCodeNo = ThrottleConstants.BankReceiptVoucherCode + voucherNo;
            System.out.println(" voucherNo insertStatus" + voucherNo);
            /**
             * *******************
             */
            for (int i = 0; i < bankid1Val.length; i++) {
                detailCode++;
                bankid1Values = bankid1Val[i];
                amountValues = amountVal[i];
                accTypeValues = accTypeVal[i];
                narrationValues = narrationVal[i];
                status = financeDAO.insertBankReceiptEntry(chequeDate, chequeNo, BankHead, totalDebitAmt, cashDetailCode, detailCode, userId, fromDate, bankid1Values, amountValues, accTypeValues, narrationValues, voucherNo, formId, voucherCodeNo, session);
            }
            System.out.println("status in BP= " + status);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }

    public int insertContraEntry(int userId, String fromDate, String[] bankid1Val, String[] amountVal, String[] accTypeVal, String[] narrationVal, String voucherIdEdit, String voucherCode) throws FPRuntimeException, FPBusinessException {
        SqlMapClient session = financeDAO.getSqlMapClient();
        int status = 0;
        String bankid1Values = null;
        String amountValues = null;
        String accTypeValues = null;
        String narrationValues = null;
        int detailCode = 0;
        int formId = 0;
        int voucherNo = 0;
        int reversalStatus = 0;
        try {
            session.startTransaction();
            int cashDetailCode = (bankid1Val.length + 1);
            System.out.println("bankid1Val.length = " + (bankid1Val.length + 1));
            /**
             * ***************getFormVoucherNo******************
             */
            formId = Integer.parseInt(ThrottleConstants.ContraFormId);
            if ("".equals(voucherIdEdit)) {
                voucherNo = financeDAO.getFormVoucherNo(formId, session);
            } else {
                voucherNo = Integer.parseInt(voucherIdEdit);
                reversalStatus = financeDAO.reverseAccountEntry(voucherCode, fromDate, formId, userId, voucherNo, session);
            }
            String voucherCodeNo = ThrottleConstants.ContraVoucherCode + voucherNo;
            System.out.println(" voucherNo insertStatus" + voucherNo);
            /**
             * *******************
             */
//             String voucherCode = "";
            voucherCode = financeDAO.getVoucherCode("contra", session);
            for (int i = 0; i < bankid1Val.length; i++) {
                detailCode++;
                bankid1Values = bankid1Val[i];
                amountValues = amountVal[i];
                accTypeValues = accTypeVal[i];
                narrationValues = narrationVal[i];
                status = financeDAO.insertContraEntry(cashDetailCode, detailCode, userId, fromDate, bankid1Values, amountValues, accTypeValues, narrationValues, voucherNo, formId, voucherCode, voucherCodeNo, session);
            }
            System.out.println("status in BP= " + status);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }

    public int insertJournalEntry(int userId, String fromDate, String[] bankid1Val, String[] amountVal, String[] accTypeVal, String[] narrationVal, String voucherIdEdit, String voucherCode) throws FPRuntimeException, FPBusinessException {
        SqlMapClient session = financeDAO.getSqlMapClient();
        int status = 0;
        String bankid1Values = null;
        String amountValues = null;
        String accTypeValues = null;
        String narrationValues = null;
        int detailCode = 0;
        int formId = 0;
        int voucherNo = 0;
        int reversalStatus = 0;
        int costCenterStatus = 0;
        try {
            session.startTransaction();
            int cashDetailCode = (bankid1Val.length + 1);
            System.out.println("bankid1Val.length = " + (bankid1Val.length + 1));
            /**
             * ***************getFormVoucherNo******************
             */
            formId = Integer.parseInt(ThrottleConstants.JournalFormId);
            if ("".equals(voucherIdEdit)) {
                voucherNo = financeDAO.getFormVoucherNo(formId, session);
            } else {
                voucherNo = Integer.parseInt(voucherIdEdit);
                reversalStatus = financeDAO.reverseAccountEntry(voucherCode, fromDate, formId, userId, voucherNo, session);
            }
            String voucherCodeNo = ThrottleConstants.JournalVoucherCode + voucherNo;
            System.out.println(" voucherNo insertStatus" + voucherNo);
            /**
             * *******************
             */
//             String voucherCode = "";
            voucherCode = financeDAO.getVoucherCode("journal", session);
            for (int i = 0; i < bankid1Val.length; i++) {
                detailCode++;
                bankid1Values = bankid1Val[i];
                amountValues = amountVal[i];
                accTypeValues = accTypeVal[i];
                narrationValues = narrationVal[i];
                status = financeDAO.insertJournalEntry(cashDetailCode, detailCode, userId, fromDate, bankid1Values, amountValues, accTypeValues, narrationValues, voucherNo, formId, voucherCode, voucherCodeNo, session);
            }
            System.out.println("status in BP= " + status);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }

    public String getBankBalance(FinanceTO financeTO) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = financeDAO.getBankBalance(financeTO);
        return status;
    }

   public int insertBRSDetails(FinanceTO financeTO, int userId, String fromDate, String branchId, String bankledgerId, String toDate, String perStatement,
            String bankBalance, String diffrenceAmt, String grandTotal, String totalReceivedAmount, String totalIssuedAmount, String bankId, String issuedVoucherCode[], String issuedLedgerNameId[], String issuedChequeNo[], String issuedChequeDate[], String issuedClearanceDate[], String issuedChequeAmount[],
            String receivedVoucherCode[], String receivedLedgerNameId[], String receivedChequeNo[], String receivedChequeDate[], String receivedClearanceDate[], String receivedChequeAmount[],
            String issuedClearanceId[], String receivedClearanceId[],String creditPaidTo[],String creditDate[],String creditChequeNo[],String creditChequeDate[],String creditReference[],String creditAmount[],
            String debitReceivedFrom[],String debitDate[],String debitChequeNo[],String debitChequeDate[],String debitReference[],String debitAmount[]) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        SqlMapClient session = financeDAO.getSqlMapClient();
        System.out.println("bp>>>>>>>>>>>>>>>>>");
        try {
            session.startTransaction();
            status = financeDAO.insertBRSDetails(financeTO, userId, fromDate, branchId, bankledgerId, toDate, perStatement,
                    bankBalance, diffrenceAmt, grandTotal, totalReceivedAmount, totalIssuedAmount, bankId, issuedVoucherCode, issuedLedgerNameId, issuedChequeNo, issuedChequeDate, issuedClearanceDate, issuedChequeAmount,
                    receivedVoucherCode, receivedLedgerNameId, receivedChequeNo, receivedChequeDate, receivedClearanceDate, receivedChequeAmount, issuedClearanceId, receivedClearanceId,
                    creditPaidTo,creditDate,creditChequeNo,creditChequeDate,creditReference,creditAmount,debitReceivedFrom,debitDate,debitChequeNo,debitChequeDate,debitReference,
                    debitAmount,session);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                session.getSession().close();
            }
        }

        return status;
    }
    public ArrayList getDayBookAmountList(String fromDate, String toDate) throws FPBusinessException, FPRuntimeException {
        System.out.println("in BP");
        ArrayList dayBookAmountList = new ArrayList();
        dayBookAmountList = financeDAO.getDayBookAmountList(fromDate, toDate);
//        System.out.println("dayBookAmountList  BP === " + dayBookAmountList);
        return dayBookAmountList;

    }

    public ArrayList getDayBook(String fromDate, String toDate, int startIndex, int endIndex, FinanceTO financeTO) throws FPBusinessException, FPRuntimeException {
        System.out.println("in BP");
        ArrayList ledgerTransactionList = new ArrayList();
        ledgerTransactionList = financeDAO.getDayBook(fromDate, toDate, startIndex, endIndex, financeTO);
        System.out.println("ledgerTransactionList  BP === " + ledgerTransactionList);
        return ledgerTransactionList;

    }

    public ArrayList getVoucherMasterList() throws FPBusinessException, FPRuntimeException {
        System.out.println("in BP");
        ArrayList voucherMasterList = new ArrayList();
        voucherMasterList = financeDAO.getVoucherMasterList();
        return voucherMasterList;

    }

    public ArrayList getTBTransactionNewList(String levelId, String sDate, String eDate, String val, FinanceTO financeTO) throws FPBusinessException, FPRuntimeException {
        System.out.println("in BP");
        ArrayList dayBookAmountList = new ArrayList();
        dayBookAmountList = financeDAO.getTBTransactionNewList(levelId, sDate, eDate, val, financeTO);
        return dayBookAmountList;

    }

    public ArrayList getPrimaryHistoryList(String toDate) throws FPBusinessException, FPRuntimeException {
        ArrayList ledgerList = new ArrayList();
        System.out.println("BP---");
        ledgerList = financeDAO.getPrimaryHistoryList(toDate);
        return ledgerList;
    }

    public ArrayList getGroupHistoryList(String toDate) throws FPBusinessException, FPRuntimeException {
        ArrayList ledgerList = new ArrayList();
        System.out.println("BP---");
        ledgerList = financeDAO.getGroupHistoryList(toDate);
        return ledgerList;
    }

    public ArrayList getEntriesReport(FinanceTO financeTO, String distcheck) throws FPRuntimeException, FPBusinessException {
        ArrayList entriesList = new ArrayList();
        entriesList = financeDAO.getEntriesReport(financeTO, distcheck);
        return entriesList;
    }

    public ArrayList getEntriesReportCreditList(FinanceTO financeTO, String distcheck) throws FPRuntimeException, FPBusinessException {
        ArrayList entriesList = new ArrayList();
        entriesList = financeDAO.getEntriesReportCreditList(financeTO, distcheck);
        return entriesList;
    }

    public ArrayList getEntriesReportDebitList(FinanceTO financeTO, String distcheck) throws FPRuntimeException, FPBusinessException {
        ArrayList entriesList = new ArrayList();
        entriesList = financeDAO.getEntriesReportDebitList(financeTO, distcheck);
        return entriesList;
    }

    public ArrayList getLevelListNew(FinanceTO lData1) throws FPBusinessException, FPRuntimeException {
        System.out.println("BP ------------------");

        ArrayList levelList = new ArrayList();
        levelList = financeDAO.getLevelListNew(lData1);
        System.out.println("levelList size" + levelList.size());
        return levelList;
    }

    public ArrayList getTBTransactionDayBkSummary(String sDate, String eDate, FinanceTO financeTO) throws FPBusinessException, FPRuntimeException {
        System.out.println("in BP");
        ArrayList dayBookAmountList = new ArrayList();
        dayBookAmountList = financeDAO.getTBTransactionDayBkSummary(sDate, eDate, financeTO);
        return dayBookAmountList;

    }

    public ArrayList handlePrimaryList(String fromDate, String toDate) throws FPBusinessException, FPRuntimeException {
        System.out.println("in BP :::::");
        ArrayList primaryList = new ArrayList();
        primaryList = financeDAO.handlePrimaryList(fromDate, toDate);
        return primaryList;
    }
    public ArrayList getCostCenterReport(FinanceTO financeTO,String distcheck) throws FPRuntimeException, FPBusinessException {
        ArrayList costCenterReportList = new ArrayList();
        costCenterReportList = financeDAO.getCostCenterReport(financeTO,distcheck);
        return costCenterReportList;
    }
    
     public ArrayList getBankBranchLists() throws FPBusinessException, FPRuntimeException {
        ArrayList bankList = new ArrayList();
        bankList = financeDAO.getBankBranchLists();
        return bankList;
    }
 public ArrayList getBRSHeaderList(FinanceTO financeTO,String val) throws FPBusinessException, FPRuntimeException {
            ArrayList list = new ArrayList();
            list = financeDAO.getBRSHeaderList(financeTO,val);
            return list;
    }
 public ArrayList getCRJDetailList(FinanceTO financeTO) throws FPBusinessException, FPRuntimeException {
            ArrayList CRJDetailList = new ArrayList();
            CRJDetailList = financeDAO.getCRJDetailList(financeTO);
            return CRJDetailList;
    }
 public ArrayList getLedger(FinanceTO financeTO) throws FPRuntimeException, FPBusinessException {
        ArrayList ledgerList = new ArrayList();
        ledgerList = financeDAO.getLedger(financeTO);
        return ledgerList;
    }
 public ArrayList getContractRateApproveList(String companyId) throws FPRuntimeException, FPBusinessException {
        ArrayList contractRateApproveList = new ArrayList();
        contractRateApproveList = financeDAO.getContractRateApproveList(companyId);
        return contractRateApproveList;
    }
 public int saveApproveRejectContract(String contractRateId,String approvalStatus,String remarks, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.saveApproveRejectContract(contractRateId, approvalStatus,remarks,userId);
        return status;
    }

 public ArrayList getVendorRateApproveList(String companyId) throws FPRuntimeException, FPBusinessException {
        ArrayList contractRateApproveList = new ArrayList();
        contractRateApproveList = financeDAO.getVendorRateApproveList(companyId);
        return contractRateApproveList;
    }
 public int saveApproveRejectVendor(String contractRateId,String approvalStatus,String remarks, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.saveApproveRejectVendor(contractRateId, approvalStatus,remarks,userId);
        return status;
    }

 public ArrayList getContractVendorPaymentDetails(String vendorId, String startDate, String endDate, String contractTypeId) throws FPRuntimeException, FPBusinessException {
        ArrayList vendorPaymentList = new ArrayList();
        vendorPaymentList = financeDAO.getContractVendorPaymentDetails(vendorId, startDate, endDate, contractTypeId);
        return vendorPaymentList;
    }

    public ArrayList getVendorInvoiceTripDetailsList(FinanceTO financeTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vendorPaymentList = new ArrayList();
        System.out.println("getVendorInvoiceTripDetailsList Bp");
        vendorPaymentList = financeDAO.getVendorInvoiceTripDetailsList(financeTO);
        return vendorPaymentList;
    }

    public int insertVendorInvoiceReceipt(FinanceTO financeTO, int userId, String vendorTypeId, String vendorNameId, String invoiceNo,
            String invoiceDate, String invoiceAmount, String remarks, String ledgerId, String vendorInvoiceFile, String fileName,
            String debitLedgerId, String contractTypeId, String totalAmount, String vendorLedgerId,
            String vendorInvoiceId) throws FPRuntimeException, FPBusinessException {
        int virId = 0;
        int status = 0;
        SqlMapClient session = financeDAO.getSqlMapClient();
        System.out.println("bp>>>>>>>>>>>>>>>>>");
        try {
            session.startTransaction();
            virId = financeDAO.insertVendorInvoiceReceipt(financeTO, userId, vendorTypeId, vendorNameId, invoiceNo, invoiceDate,
                    invoiceAmount, remarks, ledgerId, vendorInvoiceFile, fileName, debitLedgerId, vendorInvoiceId, contractTypeId, session);
          
            if (virId > 0) {
                status = financeDAO.saveVendorInvoiceTripDetails(financeTO, userId, virId, contractTypeId, invoiceDate, totalAmount,
                        vendorLedgerId, debitLedgerId, remarks, vendorInvoiceId, session);
            }
            System.out.println("status BP"+status);
            if (status > 0) {
                if ("0".equals(vendorInvoiceId)) {
                    virId = virId;
                } else {
                    virId = status;
                }
            } else {
                virId = 0;
            }
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                session.getSession().close();
            }
        }

        return virId;
    }

      public ArrayList getVirdetailList(FinanceTO financeTO) throws FPBusinessException, FPRuntimeException {
        ArrayList VirdetailList = new ArrayList();
        VirdetailList = financeDAO.getVirdetailList(financeTO);
        return VirdetailList;
    }

    public int updateVendorInvoiceTrips(FinanceTO financeTO, int userId,
            String serviceTax, String tds, String tdsAmt,String actualInvoiceAmount, String description, String virCode,
            String contractTypeId, String virIds[],
            String activeInds[]) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        SqlMapClient session = financeDAO.getSqlMapClient();
        System.out.println("bp>>>>>>>>>>>>>>>>>");
        try {
            session.startTransaction();
            status = financeDAO.updateVendorInvoiceTrips(financeTO, userId,
                    serviceTax, tds, tdsAmt, actualInvoiceAmount, description, virCode, contractTypeId, virIds, activeInds, session);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                session.getSession().close();
            }
        }

        return status;
    }
    public int saveVendorPayment(FinanceTO financeTO, int userId,
            String virCode, String contractTypeId, String startDate, String endDate, String description,
            String totalAmount, String serviceTax, String rtgs, String actualInvoiceAmount,
            String vendorLedgerId, String debitLedgerId, String accountEntryDate,
            String vehicleIds[], String vehicleTypeIds[],
            String activeInds[], String maxAllowableKMs[], String amounts[], String fixedKmDedicates[], String extraKmDedicates[],
            String ratePerExtraKmDedicates[], String totalFixedCostDedicates[], String additionalCostDedicates[], String noOfTripss[],
            String routeIdFullTruckHires[], String tripIdHires[]) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        SqlMapClient session = financeDAO.getSqlMapClient();
        System.out.println("bp>>>>>>>>>>>>>>>>>");
        try {
            session.startTransaction();
            status = financeDAO.saveVendorPayment(financeTO, userId,
                    virCode, contractTypeId, startDate, endDate, description, totalAmount, serviceTax, rtgs, actualInvoiceAmount,
                    vendorLedgerId, debitLedgerId, accountEntryDate,
                    vehicleIds, vehicleTypeIds, activeInds, maxAllowableKMs, amounts, fixedKmDedicates, extraKmDedicates,
                    ratePerExtraKmDedicates, totalFixedCostDedicates, additionalCostDedicates, noOfTripss, routeIdFullTruckHires,
                    tripIdHires, session);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                session.getSession().close();
            }
        }

        return status;
    }
    public ArrayList getTripDetails(FinanceTO financeTO) throws FPBusinessException, FPRuntimeException {
        ArrayList getTripDetails = new ArrayList();
        getTripDetails = financeDAO.getTripDetails(financeTO);
        return getTripDetails;
    }

    public ArrayList getGSTTaxDetails(FinanceTO financeTO) throws FPBusinessException, FPRuntimeException {
        ArrayList GSTDetails = new ArrayList();
        GSTDetails = financeDAO.getGSTTaxDetails(financeTO);
        return GSTDetails;
    }
      public ArrayList getInvoiceTax(FinanceTO financeTO) throws FPBusinessException, FPRuntimeException {
        ArrayList GSTDetails = new ArrayList();
        GSTDetails = financeDAO.getInvoiceTax(financeTO);
        return GSTDetails;
    }
      
     public String getVendorFSId(String vendorId) throws FPBusinessException, FPRuntimeException {
        String response = "";
        response = financeDAO.getVendorFSId(vendorId);
        return response;
    }
     
    public ArrayList getCOFApproval(FinanceTO financeTO) throws FPBusinessException, FPRuntimeException {
        ArrayList getCOFApproval = new ArrayList();
        getCOFApproval = financeDAO.getCOFApproval(financeTO);
        return getCOFApproval;
    }
    public int updateCOFApproval(FinanceTO financeTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = financeDAO.updateCOFApproval(financeTO, userId);
        return status;
    }
 
    public ArrayList getContractVendorPaymentDetailsApproval(String vendorId, String startDate, String endDate, String contractTypeId) throws FPRuntimeException, FPBusinessException {
        ArrayList vendorPaymentListApproval = new ArrayList();
        vendorPaymentListApproval = financeDAO.getContractVendorPaymentDetailsApproval(vendorId, startDate, endDate, contractTypeId);
        return vendorPaymentListApproval;
    }
    
    public int saveInvoicePostDetails(String eFSId, String invoiceNo, String invoiceDate, String invoiceAmount, String totalTaxAmt, String cgstPer, String cgstAmt, String sgstPer, String sgstAmt , String igstPer, String igstAmt, String net_amount , String GSTId, String invoiceId, String type) {
        int status = 0;
        status = financeDAO.saveInvoicePostDetails(eFSId, invoiceNo, invoiceDate, invoiceAmount, totalTaxAmt, cgstPer, cgstAmt, sgstPer, sgstAmt, igstPer, igstAmt, net_amount, GSTId, invoiceId, type);
        return status;
    }
     
}
