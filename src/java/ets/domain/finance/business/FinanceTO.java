/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.finance.business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASHOK
 */
public class FinanceTO implements Serializable {

    private String tdsPercentage = "";
    private String igstTax = "";
    private String cgstTax = "";
    private String sgstTax = "";
    private String igstDedicated = "";
    private String cgstDedicated = "";
    private String sgstDedicated = "";
    private String igstHire = "";
    private String cgstHire = "";
    private String sgstHire = "";
    private String billingState = "";
    private String tdsAmount = "";
    private String serviceTax = "";
    private String invoiceMonth = "";
    private String previousMonth = "";
    private String currentMonth = "";
    private String invoiceYear = "";
    private String previousYear = "";
    private String containerQty = "";
    private String containerNo = "";
    private String containerType = "";
    private String loadType = "";
    private String contractTypeId = "";
    private String fileName = "";
    private String[] vehicleIds = null;
    private String[] vehicleTypeIds = null;
    private String[] activeInds = null;
    private String[] containerTypes = null;
    private String[] containerQtys = null;
    private String[] amounts = null;
    private String[] routes = null;
    private String[] tripIds = null;

    private String[] totRunKMs = null;
    private String[] allowedKMs = null;
    private String[] totExtraRunKms = null;
    private String[] totExtraKMRates = null;
    private String[] actualRatePerKMs = null;
    private String[] ratePerExtraKms = null;
    private String[] fixedCosts = null;

    private String[] noOfTrips = null;
    private String[] contractCategorys = null;

    private String vehicleType = "";
    private String route = "";
    private String virCode = "";
    private String virDate = "";
    private String virId = "";
    private String vehicleTypeId = "";
    private String organisationRate = "";
    private String rateWithReefer = "";
    private String rateWithoutReefer = "";

//    finance bank
    private String[] invoiceIds = null;
    private String[] paidAmounts = null;

    private String contractRateId = "";
    private String custName = "";
    private String routeName = "";
    private String orgWithReeferRate = "";
    private String reqWithReeferRate = "";
    private String orgWithoutReeferRate = "";
    private String reqWithoutReeferRate = "";
    private String requestedBy = "";
    private String requestOn = "";
    private String requestRemarks = "";

    private List levelOneList = null;
    private List levelTwoList = null;
    private List levelThreeList = null;
    private List levelFourList = null;
    private String levelGroupName = "";

    private String asPerStatement = "";
    private String totalDifference = "";
    private String code = "";
    private String vendorTypeName = "";
    private String crjDate = "";
    private String invoiceNo = "";
    private String invoiceAmount = "";
    private String paidAmount = "";

    private String crjCode = "";
    private String brsId = "";
    private String invoiceCode = "";
    private String remarks = "";
    private String accountsType = "";
    private String accountsName = "";
    private String debitNoteAmount = "";
    private String creditNoteAmount = "";
    private String bankName = "";
    private String createdOn = "";
    private String createdBy = "";
    private String statusName = "";
    private String discription = "";
    private String accountYear = "";
    private String invoiceDetailId = "";
    private String customerName = "";
    private String customerId = "";
    private String vendorId = "";
    private String payAmount = "";
    private String pendingAmount = "";
    private String paymentMode = "";
    private String referenceNo = "";
    private String receiptDate = "";
    private String receiptAmount = "";
    private String bankid = "";
    private String openingBalanceDate = "";
    private String bankname = "";
    private String accountsAmount = "";
    private String bankcode = "";
    private String description = "";
    private String activeind = "";
    private String address = "";
    private String phoneNo = "";
    private String accntCode = "";
    private String bankMappingCode = "";
    private String openingBalance = "";
    private String acctType = "";
//    Finance Group    
    private String groupid = "";
    private String groupcode = "";
    private String groupname = "";
    private String groupdesc = "";
    private String grouptypeid = "";
//  finance Ledger
    private String ledgerID = "";
    private String ledgerCode = "";
    private String ledgerName = "";
    private String groupCode = "";
    private String amountType = "";
    private String active_ind = "";
    private String groupName = "";
    private String groupID = "";
    private String levelgroupId = "";
    private String levelgroupName = "";
    private String primarygroupId = "";
    private String fullName = "";
    private String primaryID = "";
    private String LevelName = "";
    private ArrayList childList = new ArrayList();
    private String level1Name = "";
    private String level1Code = "";
    private String level2Name = "";
    private String level2Code = "";
    private String level3Name = "";
    private String level3Code = "";
    private String level4Name = "";
    private String level4Code = "";
    //Finance Voucher
    private String voucherTypeID = "";
    private String voucherTypeCode = "";
    private String voucherTypeName = "";
    //    Account Type
    private String accountEntryTypeID = "";
    private String accountEntryTypeCode = "";
    private String accountEntryTypeName = "";
    private String accountEntryDate = "";
    private String accountAmount = "";
    private String accountType = "";
    private String accountNarration = "";
    private String tripId = "";
//tax master
    private String taxId = "";
    private String taxCode = "";
    private String taxName = "";
//Country master
    private String countryID = "";
    private String countryCode = "";
    private String countryName = "";
//State master
    private String stateID = "";
    private String stateCode = "";
    private String stateName = "";
//District master
    private String districtID = "";
    private String districtName = "";
    private String districtCode = "";
//Contra entry
//    private String address = "";
//    private String phoneNo = "";
//    private String accntCode = "";
//    private String bankMappingCode = "";
    private String date = "";
    private String[] bankid1 = null;
    private String[] bankid2 = null;
    private String[] amount = null;
    private String[] accType = null;
    private String[] narration = null;
//journal entry
    private String invoiceId = "";
    private String invRefCode = "";
    private String invoiceDate = "";
    private String grandTotal = "";
    private String invoiceStatus = "";
    private String numberOfTrip = "";
    private String invoiceFor = "";
    private String cNAmt = "";
    private String dNAmt = "";
    private String accountEntryId = "";
    private String detailCode = "";
    private String voucherCode = "";
    private String mainEntryType = "";
    private String entryType = "";
    private String ledgerId = "";
    private String customerLedgerId = "";
    private String bankLedgerId = "";
    private String vendorLedgerId = "";
    private String particularsId = "";
    private String accountAmmount = "";
    private String bankCode = "";
    private String reference = "";
    private String searchCode = "";
    private String branchCode = "";
    private String creditAmmount = "";
    private String debitAmmount = "";
    private String creditLedgerName = "";
    private String creditVoucherName = "";
    private String debitLedgerName = "";
    private String debitVoucherName = "";
    private String creditAmount = "";
    private String debitAmount = "";
    private String creditVoucherCode = "";
    private String debitVoucherCode = "";
    //  Credit Note
    private String Date = "";
    private String ledger = "";
    private String headerNarration = "";
    private String credit = "0";
    private String debit = "0";
//  VAT Master
    private String vat = "";
    private String effectiveDate = "";
    private String Description = "";
    private String Active_ind = "";
    private String levelID = "";
    private String chequeNo = "";
    private String chequeDate = "";
    private String BankHead = "";
    private String fromDate = "";
    private String toDate = "";
    private String chequeNumber = "";
    private String finYear = "";
    private String accountYearId = "";

    private String primaryBankId = "";
    private String primaryBankName = "";
    private String primaryBankCode = "";
    private String branchId = "";
    private String IFSCCode = "";
    private String BSRCode = "";
    private String taxPercent = "";
    private String chargeCode = "";
    private String vendorName = "";
    private String vendorTypeId = "";
    //pavi
    private String branchName = "";
    private String chequeAmount = "";
    private String bankPaymentId = "";
    private String formReferenceCode = "";
    private String chequeClearanceDate = "";
    private String creditLedgerId = "";
    private String debitLedgerId = "";
    private String entryStatus = "";
    private String formId = "";
    private String voucherNo = "";
    private String status = "";
    private String entryNarration = "";
    private String accountEntryIds = "";
    private String costId = "";
    private String costEntryDate = "";
    private String costAmount = "";
    private String costAmountType = "";
    private String costNarration = "";
    private String vehicleNo = "";
    private String vehicleId = "";
    private String param = "";
    private String voucherId = "";
    private String fullNamee = "";
    private String voucherCodeNo = "";
    private String bankId = "";
    private String debitedAmount = "";
    private String referenceId = "";
    private String[] invoiceCodes = null;
    int startIndex = 0;
    int endIndex = 0;
    int creditCount = 0;
    int debitCount = 0;
    private double creditValue = 0;
    private double debitValue = 0;
    private double creditAmt = 0;
    private double debitAmt = 0;

    private String cnotes = "";
    private String vehicleTypeName = "";
    private String tripCode = "";
    private String cofId = "";
    private String claimNo = "";
    private String natureOfLoss = "";
    private String actualewight = "";
    private String deliveredWeight = "";
    private String outwardCondition = "";
    private String claimAmount = "";
    private String amountLoss = "";
    private String cofNo = "";
    private String cofDate = "";
    private String cofStatus = "";
    private String pallets = "";

    public String getCnotes() {
        return cnotes;
    }

    public void setCnotes(String cnotes) {
        this.cnotes = cnotes;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public String getTripCode() {
        return tripCode;
    }

    public void setTripCode(String tripCode) {
        this.tripCode = tripCode;
    }

    public String getCofId() {
        return cofId;
    }

    public void setCofId(String cofId) {
        this.cofId = cofId;
    }

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getNatureOfLoss() {
        return natureOfLoss;
    }

    public void setNatureOfLoss(String natureOfLoss) {
        this.natureOfLoss = natureOfLoss;
    }

    public String getActualewight() {
        return actualewight;
    }

    public void setActualewight(String actualewight) {
        this.actualewight = actualewight;
    }

    public String getDeliveredWeight() {
        return deliveredWeight;
    }

    public void setDeliveredWeight(String deliveredWeight) {
        this.deliveredWeight = deliveredWeight;
    }

    public String getOutwardCondition() {
        return outwardCondition;
    }

    public void setOutwardCondition(String outwardCondition) {
        this.outwardCondition = outwardCondition;
    }

    public String getClaimAmount() {
        return claimAmount;
    }

    public void setClaimAmount(String claimAmount) {
        this.claimAmount = claimAmount;
    }

    public String getAmountLoss() {
        return amountLoss;
    }

    public void setAmountLoss(String amountLoss) {
        this.amountLoss = amountLoss;
    }

    public String getCofNo() {
        return cofNo;
    }

    public void setCofNo(String cofNo) {
        this.cofNo = cofNo;
    }

    public String getCofDate() {
        return cofDate;
    }

    public void setCofDate(String cofDate) {
        this.cofDate = cofDate;
    }

    public String getCofStatus() {
        return cofStatus;
    }

    public void setCofStatus(String cofStatus) {
        this.cofStatus = cofStatus;
    }

    public String getPallets() {
        return pallets;
    }

    public void setPallets(String pallets) {
        this.pallets = pallets;
    }
    
    

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getDebitedAmount() {
        return debitedAmount;
    }

    public void setDebitedAmount(String debitedAmount) {
        this.debitedAmount = debitedAmount;
    }

    public String getBankId() {
        return bankId;
    }

    public String[] getInvoiceCodes() {
        return invoiceCodes;
    }

    public void setInvoiceCodes(String[] invoiceCodes) {
        this.invoiceCodes = invoiceCodes;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getCostId() {
        return costId;
    }

    public void setCostId(String costId) {
        this.costId = costId;
    }

    public String getCostEntryDate() {
        return costEntryDate;
    }

    public void setCostEntryDate(String costEntryDate) {
        this.costEntryDate = costEntryDate;
    }

    public String getCostAmount() {
        return costAmount;
    }

    public void setCostAmount(String costAmount) {
        this.costAmount = costAmount;
    }

    public String getCostAmountType() {
        return costAmountType;
    }

    public void setCostAmountType(String costAmountType) {
        this.costAmountType = costAmountType;
    }

    public String getCostNarration() {
        return costNarration;
    }

    public void setCostNarration(String costNarration) {
        this.costNarration = costNarration;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }

    public String getFullNamee() {
        return fullNamee;
    }

    public void setFullNamee(String fullNamee) {
        this.fullNamee = fullNamee;
    }

    public String getVoucherCodeNo() {
        return voucherCodeNo;
    }

    public void setVoucherCodeNo(String voucherCodeNo) {
        this.voucherCodeNo = voucherCodeNo;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }

    public int getCreditCount() {
        return creditCount;
    }

    public void setCreditCount(int creditCount) {
        this.creditCount = creditCount;
    }

    public int getDebitCount() {
        return debitCount;
    }

    public void setDebitCount(int debitCount) {
        this.debitCount = debitCount;
    }

    public double getCreditValue() {
        return creditValue;
    }

    public void setCreditValue(double creditValue) {
        this.creditValue = creditValue;
    }

    public double getDebitValue() {
        return debitValue;
    }

    public void setDebitValue(double debitValue) {
        this.debitValue = debitValue;
    }

    public double getCreditAmt() {
        return creditAmt;
    }

    public void setCreditAmt(double creditAmt) {
        this.creditAmt = creditAmt;
    }

    public double getDebitAmt() {
        return debitAmt;
    }

    public void setDebitAmt(double debitAmt) {
        this.debitAmt = debitAmt;
    }

    public String getAccountEntryIds() {
        return accountEntryIds;
    }

    public void setAccountEntryIds(String accountEntryIds) {
        this.accountEntryIds = accountEntryIds;
    }

    public String getEntryNarration() {
        return entryNarration;
    }

    public void setEntryNarration(String entryNarration) {
        this.entryNarration = entryNarration;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getEntryStatus() {
        return entryStatus;
    }

    public void setEntryStatus(String entryStatus) {
        this.entryStatus = entryStatus;
    }

    public String getCreditLedgerId() {
        return creditLedgerId;
    }

    public void setCreditLedgerId(String creditLedgerId) {
        this.creditLedgerId = creditLedgerId;
    }

    public String getDebitLedgerId() {
        return debitLedgerId;
    }

    public void setDebitLedgerId(String debitLedgerId) {
        this.debitLedgerId = debitLedgerId;
    }

    public String getChequeAmount() {
        return chequeAmount;
    }

    public void setChequeAmount(String chequeAmount) {
        this.chequeAmount = chequeAmount;
    }

    public String getBankPaymentId() {
        return bankPaymentId;
    }

    public void setBankPaymentId(String bankPaymentId) {
        this.bankPaymentId = bankPaymentId;
    }

    public String getFormReferenceCode() {
        return formReferenceCode;
    }

    public void setFormReferenceCode(String formReferenceCode) {
        this.formReferenceCode = formReferenceCode;
    }

    public String getChequeClearanceDate() {
        return chequeClearanceDate;
    }

    public void setChequeClearanceDate(String chequeClearanceDate) {
        this.chequeClearanceDate = chequeClearanceDate;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getVendorTypeId() {
        return vendorTypeId;
    }

    public void setVendorTypeId(String vendorTypeId) {
        this.vendorTypeId = vendorTypeId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getChargeCode() {
        return chargeCode;
    }

    public void setChargeCode(String chargeCode) {
        this.chargeCode = chargeCode;
    }

    public String getTaxPercent() {
        return taxPercent;
    }

    public void setTaxPercent(String taxPercent) {
        this.taxPercent = taxPercent;
    }

    public String getIFSCCode() {
        return IFSCCode;
    }

    public void setIFSCCode(String IFSCCode) {
        this.IFSCCode = IFSCCode;
    }

    public String getBSRCode() {
        return BSRCode;
    }

    public void setBSRCode(String BSRCode) {
        this.BSRCode = BSRCode;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getPrimaryBankId() {
        return primaryBankId;
    }

    public void setPrimaryBankId(String primaryBankId) {
        this.primaryBankId = primaryBankId;
    }

    public String getPrimaryBankName() {
        return primaryBankName;
    }

    public void setPrimaryBankName(String primaryBankName) {
        this.primaryBankName = primaryBankName;
    }

    public String getPrimaryBankCode() {
        return primaryBankCode;
    }

    public void setPrimaryBankCode(String primaryBankCode) {
        this.primaryBankCode = primaryBankCode;
    }

    public String getAccountYearId() {
        return accountYearId;
    }

    public void setAccountYearId(String accountYearId) {
        this.accountYearId = accountYearId;
    }

    public String getFinYear() {
        return finYear;
    }

    public void setFinYear(String finYear) {
        this.finYear = finYear;
    }

    public String getActiveind() {
        return activeind;
    }

    public void setActiveind(String activeind) {
        this.activeind = activeind;
    }

    public String getBankcode() {
        return bankcode;
    }

    public void setBankcode(String bankcode) {
        this.bankcode = bankcode;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getBankid() {
        return bankid;
    }

    public void setBankid(String bankid) {
        this.bankid = bankid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGroupcode() {
        return groupcode;
    }

    public void setGroupcode(String groupcode) {
        this.groupcode = groupcode;
    }

    public String getGroupdesc() {
        return groupdesc;
    }

    public void setGroupdesc(String groupdesc) {
        this.groupdesc = groupdesc;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getGrouptypeid() {
        return grouptypeid;
    }

    public void setGrouptypeid(String grouptypeid) {
        this.grouptypeid = grouptypeid;
    }

    public String getActive_ind() {
        return active_ind;
    }

    public void setActive_ind(String active_ind) {
        this.active_ind = active_ind;
    }

    public String getAmountType() {
        return amountType;
    }

    public void setAmountType(String amountType) {
        this.amountType = amountType;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getLedgerCode() {
        return ledgerCode;
    }

    public void setLedgerCode(String ledgerCode) {
        this.ledgerCode = ledgerCode;
    }

    public String getLedgerID() {
        return ledgerID;
    }

    public void setLedgerID(String ledgerID) {
        this.ledgerID = ledgerID;
    }

    public String getLedgerName() {
        return ledgerName;
    }

    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public String getCountrycode() {
        return bankcode;
    }

    public String getVoucherTypeCode() {
        return voucherTypeCode;
    }

    public void setVoucherTypeCode(String voucherTypeCode) {
        this.voucherTypeCode = voucherTypeCode;
    }

    public String getVoucherTypeID() {
        return voucherTypeID;
    }

    public void setVoucherTypeID(String voucherTypeID) {
        this.voucherTypeID = voucherTypeID;
    }

    public String getVoucherTypeName() {
        return voucherTypeName;
    }

    public void setVoucherTypeName(String voucherTypeName) {
        this.voucherTypeName = voucherTypeName;
    }

    public String getAccountEntryTypeCode() {
        return accountEntryTypeCode;
    }

    public void setAccountEntryTypeCode(String accountEntryTypeCode) {
        this.accountEntryTypeCode = accountEntryTypeCode;
    }

    public String getAccountEntryTypeID() {
        return accountEntryTypeID;
    }

    public void setAccountEntryTypeID(String accountEntryTypeID) {
        this.accountEntryTypeID = accountEntryTypeID;
    }

    public String getAccountEntryTypeName() {
        return accountEntryTypeName;
    }

    public void setAccountEntryTypeName(String accountEntryTypeName) {
        this.accountEntryTypeName = accountEntryTypeName;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public String getTaxName() {
        return taxName;
    }

    public void setTaxName(String taxName) {
        this.taxName = taxName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryID() {
        return countryID;
    }

    public void setCountryID(String countryID) {
        this.countryID = countryID;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getStateID() {
        return stateID;
    }

    public void setStateID(String stateID) {
        this.stateID = stateID;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public String getDistrictID() {
        return districtID;
    }

    public void setDistrictID(String districtID) {
        this.districtID = districtID;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getAccntCode() {
        return accntCode;
    }

    public void setAccntCode(String accntCode) {
        this.accntCode = accntCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBankMappingCode() {
        return bankMappingCode;
    }

    public void setBankMappingCode(String bankMappingCode) {
        this.bankMappingCode = bankMappingCode;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String[] getAccType() {
        return accType;
    }

    public void setAccType(String[] accType) {
        this.accType = accType;
    }

    public String[] getAmount() {
        return amount;
    }

    public void setAmount(String[] amount) {
        this.amount = amount;
    }

    public String[] getBankid1() {
        return bankid1;
    }

    public void setBankid1(String[] bankid1) {
        this.bankid1 = bankid1;
    }

    public String[] getBankid2() {
        return bankid2;
    }

    public void setBankid2(String[] bankid2) {
        this.bankid2 = bankid2;
    }

    public String[] getNarration() {
        return narration;
    }

    public void setNarration(String[] narration) {
        this.narration = narration;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getcNAmt() {
        return cNAmt;
    }

    public void setcNAmt(String cNAmt) {
        this.cNAmt = cNAmt;
    }

    public String getdNAmt() {
        return dNAmt;
    }

    public void setdNAmt(String dNAmt) {
        this.dNAmt = dNAmt;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getInvRefCode() {
        return invRefCode;
    }

    public void setInvRefCode(String invRefCode) {
        this.invRefCode = invRefCode;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInvoiceFor() {
        return invoiceFor;
    }

    public void setInvoiceFor(String invoiceFor) {
        this.invoiceFor = invoiceFor;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public String getNumberOfTrip() {
        return numberOfTrip;
    }

    public void setNumberOfTrip(String numberOfTrip) {
        this.numberOfTrip = numberOfTrip;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getHeaderNarration() {
        return headerNarration;
    }

    public void setHeaderNarration(String headerNarration) {
        this.headerNarration = headerNarration;
    }

    public String getLedger() {
        return ledger;
    }

    public void setLedger(String ledger) {
        this.ledger = ledger;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getLevel1Code() {
        return level1Code;
    }

    public void setLevel1Code(String level1Code) {
        this.level1Code = level1Code;
    }

    public String getLevel1Name() {
        return level1Name;
    }

    public void setLevel1Name(String level1Name) {
        this.level1Name = level1Name;
    }

    public String getLevel2Code() {
        return level2Code;
    }

    public void setLevel2Code(String level2Code) {
        this.level2Code = level2Code;
    }

    public String getLevel2Name() {
        return level2Name;
    }

    public void setLevel2Name(String level2Name) {
        this.level2Name = level2Name;
    }

    public String getLevel3Code() {
        return level3Code;
    }

    public void setLevel3Code(String level3Code) {
        this.level3Code = level3Code;
    }

    public String getLevel3Name() {
        return level3Name;
    }

    public void setLevel3Name(String level3Name) {
        this.level3Name = level3Name;
    }

    public String getLevel4Code() {
        return level4Code;
    }

    public void setLevel4Code(String level4Code) {
        this.level4Code = level4Code;
    }

    public String getLevel4Name() {
        return level4Name;
    }

    public void setLevel4Name(String level4Name) {
        this.level4Name = level4Name;
    }

    public String getGroupID() {
        return groupID;
    }

    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getLevelgroupId() {
        return levelgroupId;
    }

    public void setLevelgroupId(String levelgroupId) {
        this.levelgroupId = levelgroupId;
    }

    public String getLevelgroupName() {
        return levelgroupName;
    }

    public void setLevelgroupName(String levelgroupName) {
        this.levelgroupName = levelgroupName;
    }

    public String getPrimarygroupId() {
        return primarygroupId;
    }

    public void setPrimarygroupId(String primarygroupId) {
        this.primarygroupId = primarygroupId;
    }

    public ArrayList getChildList() {
        return childList;
    }

    public void setChildList(ArrayList childList) {
        this.childList = childList;
    }

    public String getLevelName() {
        return LevelName;
    }

    public void setLevelName(String LevelName) {
        this.LevelName = LevelName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPrimaryID() {
        return primaryID;
    }

    public void setPrimaryID(String primaryID) {
        this.primaryID = primaryID;
    }

    public String getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(String openingBalance) {
        this.openingBalance = openingBalance;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        if (credit == null) {
            credit = "0";
        }
        this.credit = credit;
    }

    public String getDebit() {
        return debit;
    }

    public void setDebit(String debit) {

        if (debit == null) {
            debit = "0";
        }
        this.debit = debit;
    }

    public String getLevelID() {
        return levelID;
    }

    public void setLevelID(String levelID) {
        this.levelID = levelID;
    }

    public String getAccountAmount() {
        return accountAmount;
    }

    public void setAccountAmount(String accountAmount) {
        this.accountAmount = accountAmount;
    }

    public String getAccountEntryDate() {
        return accountEntryDate;
    }

    public void setAccountEntryDate(String accountEntryDate) {
        this.accountEntryDate = accountEntryDate;
    }

    public String getAccountNarration() {
        return accountNarration;
    }

    public void setAccountNarration(String accountNarration) {
        this.accountNarration = accountNarration;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getBankHead() {
        return BankHead;
    }

    public void setBankHead(String BankHead) {
        this.BankHead = BankHead;
    }

    public String getChequeDate() {
        return chequeDate;
    }

    public void setChequeDate(String chequeDate) {
        this.chequeDate = chequeDate;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getAccountAmmount() {
        return accountAmmount;
    }

    public void setAccountAmmount(String accountAmmount) {
        this.accountAmmount = accountAmmount;
    }

    public String getAccountEntryId() {
        return accountEntryId;
    }

    public void setAccountEntryId(String accountEntryId) {
        this.accountEntryId = accountEntryId;
    }

    public String getAccountsType() {
        return accountsType;
    }

    public void setAccountsType(String accountsType) {
        this.accountsType = accountsType;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getDetailCode() {
        return detailCode;
    }

    public void setDetailCode(String detailCode) {
        this.detailCode = detailCode;
    }

    public String getEntryType() {
        return entryType;
    }

    public void setEntryType(String entryType) {
        this.entryType = entryType;
    }

    public String getLedgerId() {
        return ledgerId;
    }

    public void setLedgerId(String ledgerId) {
        this.ledgerId = ledgerId;
    }

    public String getMainEntryType() {
        return mainEntryType;
    }

    public void setMainEntryType(String mainEntryType) {
        this.mainEntryType = mainEntryType;
    }

    public String getParticularsId() {
        return particularsId;
    }

    public void setParticularsId(String particularsId) {
        this.particularsId = particularsId;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getSearchCode() {
        return searchCode;
    }

    public void setSearchCode(String searchCode) {
        this.searchCode = searchCode;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getCreditAmmount() {
        return creditAmmount;
    }

    public void setCreditAmmount(String creditAmmount) {
        this.creditAmmount = creditAmmount;
    }

    public String getDebitAmmount() {
        return debitAmmount;
    }

    public void setDebitAmmount(String debitAmmount) {
        this.debitAmmount = debitAmmount;
    }

    public String getCreditLedgerName() {
        return creditLedgerName;
    }

    public void setCreditLedgerName(String creditLedgerName) {
        this.creditLedgerName = creditLedgerName;
    }

    public String getCreditVoucherName() {
        return creditVoucherName;
    }

    public void setCreditVoucherName(String creditVoucherName) {
        this.creditVoucherName = creditVoucherName;
    }

    public String getDebitLedgerName() {
        return debitLedgerName;
    }

    public void setDebitLedgerName(String debitLedgerName) {
        this.debitLedgerName = debitLedgerName;
    }

    public String getDebitVoucherName() {
        return debitVoucherName;
    }

    public void setDebitVoucherName(String debitVoucherName) {
        this.debitVoucherName = debitVoucherName;
    }

    public String getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(String creditAmount) {
        this.creditAmount = creditAmount;
    }

    public String getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(String debitAmount) {
        this.debitAmount = debitAmount;
    }

    public String getCreditVoucherCode() {
        return creditVoucherCode;
    }

    public void setCreditVoucherCode(String creditVoucherCode) {
        this.creditVoucherCode = creditVoucherCode;
    }

    public String getDebitVoucherCode() {
        return debitVoucherCode;
    }

    public void setDebitVoucherCode(String debitVoucherCode) {
        this.debitVoucherCode = debitVoucherCode;
    }

    public String getChequeNumber() {
        return chequeNumber;
    }

    public void setChequeNumber(String chequeNumber) {
        this.chequeNumber = chequeNumber;
    }

    public String getAccountsAmount() {
        return accountsAmount;
    }

    public void setAccountsAmount(String accountsAmount) {
        this.accountsAmount = accountsAmount;
    }

    public void resetCreditDebit() {
        if (credit == null) {
            credit = "0.00";
        }
        if (debit == null) {
            debit = "0.00";
        }
        Float creditValue = Float.parseFloat(credit);
        Float debitValue = Float.parseFloat(debit);
        credit = "0.00";
        debit = "0.00";

        if (creditValue > debitValue) {
            credit = "" + (creditValue - debitValue);
        } else {
            debit = "" + (debitValue - creditValue);
        }
    }

    public void resetAccountType() {
        if (credit == null) {
            credit = "0.00";
        }
        if (debit == null) {
            debit = "0.00";
        }
        Float creditValue = Float.parseFloat(credit);
        Float debitValue = Float.parseFloat(debit);
        //////System.out.println("creditValue:" + creditValue);
        //////System.out.println("debitValue:" + debitValue);
        credit = "0.00";
        debit = "0.00";

        if (creditValue > debitValue) {
            credit = "" + (creditValue - debitValue);
            accountType = "CREDIT";
        } else {
            debit = "" + (debitValue - creditValue);
            accountType = "DEBIT";
        }
        //////System.out.println("credit:" + credit);
        //////System.out.println("debit:" + debit);
        //////System.out.println("accountType:" + accountType);
    }

    public String getOpeningBalanceDate() {
        return openingBalanceDate;
    }

    public void setOpeningBalanceDate(String openingBalanceDate) {
        this.openingBalanceDate = openingBalanceDate;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(String payAmount) {
        this.payAmount = payAmount;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPendingAmount() {
        return pendingAmount;
    }

    public void setPendingAmount(String pendingAmount) {
        this.pendingAmount = pendingAmount;
    }

    public String getReceiptAmount() {
        return receiptAmount;
    }

    public void setReceiptAmount(String receiptAmount) {
        this.receiptAmount = receiptAmount;
    }

    public String getReceiptDate() {
        return receiptDate;
    }

    public void setReceiptDate(String receiptDate) {
        this.receiptDate = receiptDate;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getBankLedgerId() {
        return bankLedgerId;
    }

    public void setBankLedgerId(String bankLedgerId) {
        this.bankLedgerId = bankLedgerId;
    }

    public String getCustomerLedgerId() {
        return customerLedgerId;
    }

    public void setCustomerLedgerId(String customerLedgerId) {
        this.customerLedgerId = customerLedgerId;
    }

    public String getVendorLedgerId() {
        return vendorLedgerId;
    }

    public void setVendorLedgerId(String vendorLedgerId) {
        this.vendorLedgerId = vendorLedgerId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getInvoiceDetailId() {
        return invoiceDetailId;
    }

    public void setInvoiceDetailId(String invoiceDetailId) {
        this.invoiceDetailId = invoiceDetailId;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public String getAccountYear() {
        return accountYear;
    }

    public void setAccountYear(String accountYear) {
        this.accountYear = accountYear;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getCreditNoteAmount() {
        return creditNoteAmount;
    }

    public void setCreditNoteAmount(String creditNoteAmount) {
        this.creditNoteAmount = creditNoteAmount;
    }

    public String getDebitNoteAmount() {
        return debitNoteAmount;
    }

    public void setDebitNoteAmount(String debitNoteAmount) {
        this.debitNoteAmount = debitNoteAmount;
    }

    public String getAccountsName() {
        return accountsName;
    }

    public void setAccountsName(String accountsName) {
        this.accountsName = accountsName;
    }

    public List getLevelOneList() {
        return levelOneList;
    }

    public void setLevelOneList(List levelOneList) {
        this.levelOneList = levelOneList;
    }

    public List getLevelTwoList() {
        return levelTwoList;
    }

    public void setLevelTwoList(List levelTwoList) {
        this.levelTwoList = levelTwoList;
    }

    public List getLevelThreeList() {
        return levelThreeList;
    }

    public void setLevelThreeList(List levelThreeList) {
        this.levelThreeList = levelThreeList;
    }

    public List getLevelFourList() {
        return levelFourList;
    }

    public void setLevelFourList(List levelFourList) {
        this.levelFourList = levelFourList;
    }

    public String[] getInvoiceIds() {
        return invoiceIds;
    }

    public void setInvoiceIds(String[] invoiceIds) {
        this.invoiceIds = invoiceIds;
    }

    public String[] getPaidAmounts() {
        return paidAmounts;
    }

    public void setPaidAmounts(String[] paidAmounts) {
        this.paidAmounts = paidAmounts;
    }

    public String getAcctType() {
        return acctType;
    }

    public void setAcctType(String acctType) {
        this.acctType = acctType;
    }

    public String getBrsId() {
        return brsId;
    }

    public void setBrsId(String brsId) {
        this.brsId = brsId;
    }

    public String getCrjCode() {
        return crjCode;
    }

    public void setCrjCode(String crjCode) {
        this.crjCode = crjCode;
    }

    public String getAsPerStatement() {
        return asPerStatement;
    }

    public void setAsPerStatement(String asPerStatement) {
        this.asPerStatement = asPerStatement;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCrjDate() {
        return crjDate;
    }

    public void setCrjDate(String crjDate) {
        this.crjDate = crjDate;
    }

    public String getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(String invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(String paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getTotalDifference() {
        return totalDifference;
    }

    public void setTotalDifference(String totalDifference) {
        this.totalDifference = totalDifference;
    }

    public String getVendorTypeName() {
        return vendorTypeName;
    }

    public void setVendorTypeName(String vendorTypeName) {
        this.vendorTypeName = vendorTypeName;
    }

    public String getLevelGroupName() {
        return levelGroupName;
    }

    public void setLevelGroupName(String levelGroupName) {
        this.levelGroupName = levelGroupName;
    }

    public String getContractRateId() {
        return contractRateId;
    }

    public void setContractRateId(String contractRateId) {
        this.contractRateId = contractRateId;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getOrgWithReeferRate() {
        return orgWithReeferRate;
    }

    public void setOrgWithReeferRate(String orgWithReeferRate) {
        this.orgWithReeferRate = orgWithReeferRate;
    }

    public String getReqWithReeferRate() {
        return reqWithReeferRate;
    }

    public void setReqWithReeferRate(String reqWithReeferRate) {
        this.reqWithReeferRate = reqWithReeferRate;
    }

    public String getOrgWithoutReeferRate() {
        return orgWithoutReeferRate;
    }

    public void setOrgWithoutReeferRate(String orgWithoutReeferRate) {
        this.orgWithoutReeferRate = orgWithoutReeferRate;
    }

    public String getReqWithoutReeferRate() {
        return reqWithoutReeferRate;
    }

    public void setReqWithoutReeferRate(String reqWithoutReeferRate) {
        this.reqWithoutReeferRate = reqWithoutReeferRate;
    }

    public String getRequestedBy() {
        return requestedBy;
    }

    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }

    public String getRequestOn() {
        return requestOn;
    }

    public void setRequestOn(String requestOn) {
        this.requestOn = requestOn;
    }

    public String getRequestRemarks() {
        return requestRemarks;
    }

    public void setRequestRemarks(String requestRemarks) {
        this.requestRemarks = requestRemarks;
    }

    public String getOrganisationRate() {
        return organisationRate;
    }

    public void setOrganisationRate(String organisationRate) {
        this.organisationRate = organisationRate;
    }

    public String getRateWithReefer() {
        return rateWithReefer;
    }

    public void setRateWithReefer(String rateWithReefer) {
        this.rateWithReefer = rateWithReefer;
    }

    public String getRateWithoutReefer() {
        return rateWithoutReefer;
    }

    public void setRateWithoutReefer(String rateWithoutReefer) {
        this.rateWithoutReefer = rateWithoutReefer;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getVirCode() {
        return virCode;
    }

    public void setVirCode(String virCode) {
        this.virCode = virCode;
    }

    public String getVirDate() {
        return virDate;
    }

    public void setVirDate(String virDate) {
        this.virDate = virDate;
    }

    public String getVirId() {
        return virId;
    }

    public void setVirId(String virId) {
        this.virId = virId;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getContainerType() {
        return containerType;
    }

    public void setContainerType(String containerType) {
        this.containerType = containerType;
    }

    public String getContainerQty() {
        return containerQty;
    }

    public void setContainerQty(String containerQty) {
        this.containerQty = containerQty;
    }

    public String getContractTypeId() {
        return contractTypeId;
    }

    public void setContractTypeId(String contractTypeId) {
        this.contractTypeId = contractTypeId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String[] getVehicleIds() {
        return vehicleIds;
    }

    public void setVehicleIds(String[] vehicleIds) {
        this.vehicleIds = vehicleIds;
    }

    public String[] getVehicleTypeIds() {
        return vehicleTypeIds;
    }

    public void setVehicleTypeIds(String[] vehicleTypeIds) {
        this.vehicleTypeIds = vehicleTypeIds;
    }

    public String[] getActiveInds() {
        return activeInds;
    }

    public void setActiveInds(String[] activeInds) {
        this.activeInds = activeInds;
    }

    public String[] getContainerTypes() {
        return containerTypes;
    }

    public void setContainerTypes(String[] containerTypes) {
        this.containerTypes = containerTypes;
    }

    public String[] getContainerQtys() {
        return containerQtys;
    }

    public void setContainerQtys(String[] containerQtys) {
        this.containerQtys = containerQtys;
    }

    public String[] getAmounts() {
        return amounts;
    }

    public void setAmounts(String[] amounts) {
        this.amounts = amounts;
    }

    public String[] getRoutes() {
        return routes;
    }

    public void setRoutes(String[] routes) {
        this.routes = routes;
    }

    public String[] getTripIds() {
        return tripIds;
    }

    public void setTripIds(String[] tripIds) {
        this.tripIds = tripIds;
    }

    public String[] getTotRunKMs() {
        return totRunKMs;
    }

    public void setTotRunKMs(String[] totRunKMs) {
        this.totRunKMs = totRunKMs;
    }

    public String[] getAllowedKMs() {
        return allowedKMs;
    }

    public void setAllowedKMs(String[] allowedKMs) {
        this.allowedKMs = allowedKMs;
    }

    public String[] getTotExtraRunKms() {
        return totExtraRunKms;
    }

    public void setTotExtraRunKms(String[] totExtraRunKms) {
        this.totExtraRunKms = totExtraRunKms;
    }

    public String[] getTotExtraKMRates() {
        return totExtraKMRates;
    }

    public void setTotExtraKMRates(String[] totExtraKMRates) {
        this.totExtraKMRates = totExtraKMRates;
    }

    public String[] getActualRatePerKMs() {
        return actualRatePerKMs;
    }

    public void setActualRatePerKMs(String[] actualRatePerKMs) {
        this.actualRatePerKMs = actualRatePerKMs;
    }

    public String[] getRatePerExtraKms() {
        return ratePerExtraKms;
    }

    public void setRatePerExtraKms(String[] ratePerExtraKms) {
        this.ratePerExtraKms = ratePerExtraKms;
    }

    public String[] getFixedCosts() {
        return fixedCosts;
    }

    public void setFixedCosts(String[] fixedCosts) {
        this.fixedCosts = fixedCosts;
    }

    public String[] getNoOfTrips() {
        return noOfTrips;
    }

    public void setNoOfTrips(String[] noOfTrips) {
        this.noOfTrips = noOfTrips;
    }

    public String[] getContractCategorys() {
        return contractCategorys;
    }

    public void setContractCategorys(String[] contractCategorys) {
        this.contractCategorys = contractCategorys;
    }

    public String getLoadType() {
        return loadType;
    }

    public void setLoadType(String loadType) {
        this.loadType = loadType;
    }

    public String getContainerNo() {
        return containerNo;
    }

    public void setContainerNo(String containerNo) {
        this.containerNo = containerNo;
    }

    public String getPreviousMonth() {
        return previousMonth;
    }

    public void setPreviousMonth(String previousMonth) {
        this.previousMonth = previousMonth;
    }

    public String getCurrentMonth() {
        return currentMonth;
    }

    public void setCurrentMonth(String currentMonth) {
        this.currentMonth = currentMonth;
    }

    public String getInvoiceYear() {
        return invoiceYear;
    }

    public void setInvoiceYear(String invoiceYear) {
        this.invoiceYear = invoiceYear;
    }

    public String getPreviousYear() {
        return previousYear;
    }

    public void setPreviousYear(String previousYear) {
        this.previousYear = previousYear;
    }

    public String getInvoiceMonth() {
        return invoiceMonth;
    }

    public void setInvoiceMonth(String invoiceMonth) {
        this.invoiceMonth = invoiceMonth;
    }

    public String getTdsAmount() {
        return tdsAmount;
    }

    public void setTdsAmount(String tdsAmount) {
        this.tdsAmount = tdsAmount;
    }

    public String getServiceTax() {
        return serviceTax;
    }

    public void setServiceTax(String serviceTax) {
        this.serviceTax = serviceTax;
    }

    public String getBillingState() {
        return billingState;
    }

    public void setBillingState(String billingState) {
        this.billingState = billingState;
    }

    public String getIgstTax() {
        return igstTax;
    }

    public void setIgstTax(String igstTax) {
        this.igstTax = igstTax;
    }

    public String getCgstTax() {
        return cgstTax;
    }

    public void setCgstTax(String cgstTax) {
        this.cgstTax = cgstTax;
    }

    public String getSgstTax() {
        return sgstTax;
    }

    public void setSgstTax(String sgstTax) {
        this.sgstTax = sgstTax;
    }

    public String getIgstDedicated() {
        return igstDedicated;
    }

    public void setIgstDedicated(String igstDedicated) {
        this.igstDedicated = igstDedicated;
    }

    public String getCgstDedicated() {
        return cgstDedicated;
    }

    public void setCgstDedicated(String cgstDedicated) {
        this.cgstDedicated = cgstDedicated;
    }

    public String getSgstDedicated() {
        return sgstDedicated;
    }

    public void setSgstDedicated(String sgstDedicated) {
        this.sgstDedicated = sgstDedicated;
    }

    public String getIgstHire() {
        return igstHire;
    }

    public void setIgstHire(String igstHire) {
        this.igstHire = igstHire;
    }

    public String getCgstHire() {
        return cgstHire;
    }

    public void setCgstHire(String cgstHire) {
        this.cgstHire = cgstHire;
    }

    public String getSgstHire() {
        return sgstHire;
    }

    public void setSgstHire(String sgstHire) {
        this.sgstHire = sgstHire;
    }

    public String getTdsPercentage() {
        return tdsPercentage;
    }

    public void setTdsPercentage(String tdsPercentage) {
        this.tdsPercentage = tdsPercentage;
    }

}
