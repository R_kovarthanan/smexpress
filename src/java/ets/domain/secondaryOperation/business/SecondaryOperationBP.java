/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.secondaryOperation.business;

import com.ibatis.common.util.PaginatedList;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapSession;
import com.ibatis.sqlmap.client.event.RowHandler;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.operation.business.OperationTO;
import ets.domain.secondaryOperation.data.SecondaryOperationDAO;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

/**
 *
 * @author Throttle
 */
public class SecondaryOperationBP {

    SecondaryOperationDAO secondaryOperationDAO;

    public SecondaryOperationDAO getSecondaryOperationDAO() {
        return secondaryOperationDAO;
    }

    public void setSecondaryOperationDAO(SecondaryOperationDAO secondaryOperationDAO) {
        this.secondaryOperationDAO = secondaryOperationDAO;
    }

    public ArrayList getCity(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList cityList = new ArrayList();
        cityList = secondaryOperationDAO.getCity(operationTO);
        return cityList;
    }

    public ArrayList getCustomerPoints(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList cityList = new ArrayList();
        cityList = secondaryOperationDAO.getCustomerPoints(operationTO);
        return cityList;
    }

    public ArrayList getSecondaryCustomerDetails(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList customerName = new ArrayList();
        customerName = secondaryOperationDAO.getSecondaryCustomerDetails(operationTO);
        return customerName;
    }

    public String getConfigDetails(SecondaryOperationTO operationTO) {
        String configDetails = "";
        configDetails = secondaryOperationDAO.getConfigDetails(operationTO);
        return configDetails;
    }

    public int insertContractRouteMater(SecondaryOperationTO operationTO) {
        int insertContractRouteMater = 0;
        insertContractRouteMater = secondaryOperationDAO.insertContractRouteMater(operationTO);
        return insertContractRouteMater;
    }

    public int insertContractMaster(SecondaryOperationTO operationTO) {
        int response = 0;
        response = secondaryOperationDAO.insertContractMaster(operationTO);
        return response;
    }

    public int insertContractDetails(SecondaryOperationTO operationTO, int contractId) {
        int response = 0;
        response = secondaryOperationDAO.insertContractDetails(operationTO, contractId);
        return response;
    }

    public int insertContractRouteDetails(SecondaryOperationTO operationTO, int insertContractRouteMater) {
        int insertContractRouteDetails = 0;
        insertContractRouteDetails = secondaryOperationDAO.insertContractRouteDetails(operationTO, insertContractRouteMater);
        return insertContractRouteDetails;
    }

    public int insertContractRouteCostDetails(SecondaryOperationTO operationTO, int insertContractRouteMater) {
        int insertContractRouteCostDetails = 0;
        insertContractRouteCostDetails = secondaryOperationDAO.insertContractRouteCostDetails(operationTO, insertContractRouteMater);
        return insertContractRouteCostDetails;
    }

    public String getSecondaryRouteCode(int insertContractRouteMater) {
        String secondaryRouteCode = "";
        secondaryRouteCode = secondaryOperationDAO.getSecondaryRouteCode(insertContractRouteMater);
        return secondaryRouteCode;
    }

    public int saveSecondaryTripSchedule(SecondaryOperationTO operationTO, int userId, String passedValue) {
        int insertStatus = 0;
        insertStatus = secondaryOperationDAO.saveSecondaryTripSchedule(operationTO, userId, passedValue);
        return insertStatus;
    }

    public ArrayList getSecondaryContractRouteMasterlist(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList SecondaryContractRouteList = new ArrayList();
        SecondaryContractRouteList = secondaryOperationDAO.getSecondaryContractRouteMasterlist(operationTO);
        return SecondaryContractRouteList;
    }

    public ArrayList getSecondaryTripCustomer(SecondaryOperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        ArrayList secondaryTripCustomerList = new ArrayList();
        secondaryTripCustomerList = secondaryOperationDAO.getSecondaryTripCustomer(operationTO);
        return secondaryTripCustomerList;
    }

    public ArrayList getSecondaryTripScheduleList(SecondaryOperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        ArrayList secondaryTripScheduleList = new ArrayList();
        secondaryTripScheduleList = secondaryOperationDAO.getSecondaryTripScheduleList(operationTO);
        return secondaryTripScheduleList;
    }

    public String getTripScheduleStatus(SecondaryOperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        String tripScheduleStatus = "";
        tripScheduleStatus = secondaryOperationDAO.getTripScheduleStatus(operationTO);
        return tripScheduleStatus;
    }

    public String getDateWiseTripScheduleStatus(SecondaryOperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        String tripScheduleStatus = "";
        tripScheduleStatus = secondaryOperationDAO.getDateWiseTripScheduleStatus(operationTO);
        return tripScheduleStatus;
    }

    public ArrayList getSecondaryContract() throws FPBusinessException, FPRuntimeException {
        ArrayList getSecondaryContractView = new ArrayList();
        getSecondaryContractView = secondaryOperationDAO.getSecondaryContract();
        return getSecondaryContractView;
    }

    public ArrayList getContractRouteDetails(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList routeDetails = new ArrayList();
        routeDetails = secondaryOperationDAO.getContractRouteDetails(operationTO);
        return routeDetails;
    }

    public ArrayList getContractRateCostDetails(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList rateDetails = new ArrayList();
        rateDetails = secondaryOperationDAO.getContractRateCostDetails(operationTO);
        return rateDetails;
    }

    public ArrayList getContractRateCostDetailsEdit(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList rateDetails = new ArrayList();
        rateDetails = secondaryOperationDAO.getContractRateCostDetailsEdit(operationTO);
        return rateDetails;
    }

    public ArrayList getRouteContractDetails(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList routeDetails = new ArrayList();
        routeDetails = secondaryOperationDAO.getRouteContractDetails(operationTO);
        return routeDetails;
    }

    public ArrayList getRoutePointDetails(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList routePointDetails = new ArrayList();
        routePointDetails = secondaryOperationDAO.getRoutePointDetails(operationTO);
        return routePointDetails;
    }

    public int updateSecondaryTripSchedule(SecondaryOperationTO operationTO, int userId, String passedValue) {
        int insertStatus = 0;
        insertStatus = secondaryOperationDAO.updateSecondaryTripSchedule(operationTO, userId, passedValue);
        return insertStatus;
    }

    public int updateContractRouteMater(SecondaryOperationTO operationTO) {
        int insertContractRouteMater = 0;
        insertContractRouteMater = secondaryOperationDAO.updateContractRouteMater(operationTO);
        return insertContractRouteMater;
    }

    public int updateContractRouteDetails(SecondaryOperationTO operationTO) {
        int insertContractRouteDetails = 0;
        insertContractRouteDetails = secondaryOperationDAO.updateContractRouteDetails(operationTO);
        return insertContractRouteDetails;
    }

    public int updateContractRouteCostDetails(SecondaryOperationTO operationTO) {
        int insertContractRouteCostDetails = 0;
        insertContractRouteCostDetails = secondaryOperationDAO.updateContractRouteCostDetails(operationTO);
        return insertContractRouteCostDetails;
    }

    public ArrayList getSecondaryVehicleList(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleRegNoList = new ArrayList();
        vehicleRegNoList = secondaryOperationDAO.getSecondaryVehicleList(operationTO);
        return vehicleRegNoList;
    }

    public String getVehicleTypeId(String vehicleId) {
        String vehicleTypeId = "";
        vehicleTypeId = secondaryOperationDAO.getVehicleTypeId(vehicleId);
        return vehicleTypeId;
    }

    public int updateTripScheduleStatus(int tripid, String customerId, String secondaryRouteId, String tripScheduleDate, int userId) {
        int updateTripScheuleStatus = 0;
        updateTripScheuleStatus = secondaryOperationDAO.updateTripScheduleStatus(tripid, customerId, secondaryRouteId, tripScheduleDate, userId);
        return updateTripScheuleStatus;
    }

    /**
     * This method used to saveSecondaryRoutes .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int saveSecondaryRoutes(SecondaryOperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int saveSecondaryRoutes = 0;
        saveSecondaryRoutes = secondaryOperationDAO.saveSecondaryRoutes(operationTO, userId);
        return saveSecondaryRoutes;
    }

    public String getStartingBalance(String driverId) {
        String startingBalance = "";
        startingBalance = secondaryOperationDAO.getStartingBalance(driverId);
        return startingBalance;
    }

    public int insertSecondaryDriverSettlement(SecondaryOperationTO operationTO, int userId) {
        int insertDriverSettlement = 0;
        insertDriverSettlement = secondaryOperationDAO.insertSecondaryDriverSettlement(operationTO, userId);
        return insertDriverSettlement;
    }

    public int insertSecondaryDriverSettlementDetails(SecondaryOperationTO operationTO, int insertDriverSettlement) {
        int insertDriverSettlementDetails = 0;
        insertDriverSettlementDetails = secondaryOperationDAO.insertSecondaryDriverSettlementDetails(operationTO, insertDriverSettlement);
        return insertDriverSettlementDetails;
    }

    public ArrayList getTripClosureDetails(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList closureDetails = new ArrayList();
        closureDetails = secondaryOperationDAO.getTripClosureDetails(operationTO);
        return closureDetails;
    }

    /**
     * This method used to get SecondaryRoutes .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getSecondaryCustomerPoints(SecondaryOperationTO operationTO, String customerId) throws FPRuntimeException, FPBusinessException {
        System.out.println("customerId  " + customerId);
        ArrayList SecondaryCustomerPointsList = new ArrayList();
        SecondaryCustomerPointsList = secondaryOperationDAO.getSecondaryCustomerPoints(operationTO, customerId);
        return SecondaryCustomerPointsList;
    }

    /**
     * This method used to update SecondaryRoutes .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int updateSecondaryRoute(SecondaryOperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateSecondaryRoute = 0;
        updateSecondaryRoute = secondaryOperationDAO.updateSecondaryRoute(operationTO, userId);

        return updateSecondaryRoute;
    }

    public ArrayList getSecondarySettlementDetailsForExpense(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList settlementDetails = new ArrayList();
        ArrayList totalExpenseNew = new ArrayList();
        SecondaryOperationTO operationTo = new SecondaryOperationTO();
        settlementDetails = secondaryOperationDAO.getSecondarySettlementDetails(operationTO);
        String driverIncentive = "";
        String tollAmount = "";
        String driverBatta = "";
        String fuelPrice = "";
        String miscCost = "";
        String secFactor = "";
        Iterator it = settlementDetails.iterator();
        while (it.hasNext()) {
            operationTo = new SecondaryOperationTO();
            operationTo = (SecondaryOperationTO) it.next();
            miscCost = secondaryOperationDAO.getMiscCost();
            secFactor = secondaryOperationDAO.getFactor();
            driverIncentive = secondaryOperationDAO.getDriverIncentiveAmount();
            tollAmount = secondaryOperationDAO.getTollAmount();
            driverBatta = secondaryOperationDAO.getDriverBataAmount();
            fuelPrice = secondaryOperationDAO.getFuelPrice();
            operationTo.setMiscCostKm(miscCost);
            operationTo.setSecFactor(secFactor);
            operationTo.setTollAmount(tollAmount);
            operationTo.setDriverBatta(driverBatta);
            operationTo.setDriverIncentive(driverIncentive);
            operationTo.setFuelPrice(fuelPrice);

            totalExpenseNew.add(operationTo);
        }
        return totalExpenseNew;
    }

    public ArrayList getSecondaryTotalExpenseDetails(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList totalExpense = new ArrayList();
        totalExpense = secondaryOperationDAO.getTotalExpenseDetails(operationTO);
        System.out.println("the secondary troakl expense is" + totalExpense.size());
        return totalExpense;
    }

    public ArrayList getSecondarySettlementDetails(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList settlementDetails = new ArrayList();
        settlementDetails = secondaryOperationDAO.getSecondarySettlementDetails(operationTO);
        return settlementDetails;
    }

    public ArrayList getTripExpenses(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList settlementDetails = new ArrayList();
        settlementDetails = secondaryOperationDAO.getTripExpenses(operationTO);
        return settlementDetails;
    }

    public ArrayList getTripAdvances(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList settlementDetails = new ArrayList();
        settlementDetails = secondaryOperationDAO.getTripAdvances(operationTO);
        return settlementDetails;
    }

    public ArrayList getViewSecondaryCustomerVehicle(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList SecondaryCustomerVehicleList = new ArrayList();
        SecondaryCustomerVehicleList = secondaryOperationDAO.getViewSecondaryCustomerVehicle(operationTO);
        return SecondaryCustomerVehicleList;
    }

    public ArrayList getViewSecondaryCustomerdetails(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList SecondaryCustomerList = new ArrayList();
        SecondaryCustomerList = secondaryOperationDAO.getViewSecondaryCustomerdetails(operationTO);
        return SecondaryCustomerList;
    }

    public ArrayList getCustomerPaymentDetails(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList customerPaymentDetails = new ArrayList();
        customerPaymentDetails = secondaryOperationDAO.getCustomerPaymentDetails(operationTO);
        return customerPaymentDetails;
    }

    public ArrayList getCustomerCollectBillDetails(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList collectBillAmountDetails = new ArrayList();
        collectBillAmountDetails = secondaryOperationDAO.getCustomerCollectBillDetails(operationTO);
        return collectBillAmountDetails;
    }

    public ArrayList getHumanResourceList() throws FPRuntimeException, FPBusinessException {
        ArrayList humanResourceList = new ArrayList();
        humanResourceList = secondaryOperationDAO.getHumanResourceList();
        return humanResourceList;
    }

    public int insertHumanResourceDetails(SecondaryOperationTO operationTO, int contractId) {
        int response = 0;
        response = secondaryOperationDAO.insertHumanResourceDetails(operationTO, contractId);
        return response;
    }

    public ArrayList getViewHumanResourceDetails(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList humanResourceList = new ArrayList();
        humanResourceList = secondaryOperationDAO.getViewHumanResourceDetails(operationTO);
        return humanResourceList;
    }

    public ArrayList getSecondaryDriverSettlementDetails(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList settlementDetails = new ArrayList();
        settlementDetails = secondaryOperationDAO.getSecondaryDriverSettlementDetails(operationTO);
        return settlementDetails;
    }

    public ArrayList getSecondaryDriverTripSettlementDetails(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList settlementDetails = new ArrayList();
        settlementDetails = secondaryOperationDAO.getSecondaryDriverTripSettlementDetails(operationTO);
        return settlementDetails;
    }

    public int updateContractMaster(SecondaryOperationTO operationTO, String[] vehTypeId, String[] noOfVehicle, String[] fixedKm, String[] fixedKmCharge, String[] extraKmCharge, String[] id, String[] hrId, String[] noOfPersons, String[] fixedAmount, String[] updateValue, String[] pointId) {
        int response = 0;
        SqlMapClient session = secondaryOperationDAO.getSqlMapClient();
        try {
            session.startTransaction();
            response = secondaryOperationDAO.updateContractMaster(operationTO);
            int insertContractDetails = 0;
            int insertContractMater = 0;
            int insertHumanResource = 0;
            insertContractMater = Integer.parseInt(operationTO.getContractId());
            if (response > 0) {
                for (int i = 0; i < vehTypeId.length; i++) {
                    operationTO.setVehTypeId(vehTypeId[i]);
                    operationTO.setNoOfVehicle(noOfVehicle[i]);
                    operationTO.setFixedKm(fixedKm[i]);
                    operationTO.setFixedKmCharge(fixedKmCharge[i]);
                    operationTO.setExtraKmCharge(extraKmCharge[i]);
                    operationTO.setId(id[i]);
                    if (!"0".equals(operationTO.getId())) {
                        insertContractDetails = secondaryOperationDAO.updateContractDetails(operationTO);
                    } else if ("0".equals(operationTO.getId())) {
                        insertContractDetails = secondaryOperationDAO.insertContractDetails(operationTO, insertContractMater);
                    }
                }
            }
            if (response > 0) {
                for (int i = 0; i < hrId.length; i++) {
                    operationTO.setHrId(hrId[i]);
                    operationTO.setNoOfPersons(noOfPersons[i]);
                    operationTO.setFixedAmount(fixedAmount[i]);
                    System.out.println("updateValue[i] = " + updateValue[i]);
                    if ("0".equals(updateValue[i])) {
                        operationTO.setPointId(pointId[i]);
                        insertHumanResource = secondaryOperationDAO.updateHumanResourceDetails(operationTO);
                    } else if (!"0".equals(hrId[i])) {
                        insertHumanResource = secondaryOperationDAO.insertHumanResourceDetails(operationTO, insertContractMater);
                    }
                }
            }
            ArrayList secondaryCustomerDetails = new ArrayList();
            secondaryCustomerDetails = secondaryOperationDAO.getViewSecondaryCustomerdetails(operationTO);
            SecondaryOperationTO opTO = new SecondaryOperationTO();
            double currentFuelPrice = 0.0f;
            double currentDieselPrice = 0.0f;
            double newFuelPrice = 0.0f;
            double newDieselPrice = 0.0f;
            Iterator itr = secondaryCustomerDetails.iterator();
            while (itr.hasNext()) {
                opTO = new SecondaryOperationTO();
                opTO = (SecondaryOperationTO) itr.next();
                currentFuelPrice = Double.parseDouble(opTO.getContractCngCost());
                currentDieselPrice = Double.parseDouble(opTO.getContractDieselCost());
            }
            newFuelPrice = Double.parseDouble(operationTO.getContractCngCost());
            newDieselPrice = Double.parseDouble(operationTO.getContractDieselCost());
            int fuelPrice = Double.compare(currentFuelPrice, newFuelPrice);
            int dieselPrice = Double.compare(currentDieselPrice, newDieselPrice);
            if (fuelPrice == 0 || dieselPrice == 0) {
                ArrayList contractRouteDetails = new ArrayList();
                ArrayList rateCostDetails = new ArrayList();
                contractRouteDetails = secondaryOperationDAO.getContractRouteDetails(operationTO);
                Iterator itr1 = contractRouteDetails.iterator();
                SecondaryOperationTO opTO1 = new SecondaryOperationTO();
                int secondaryRouteId = 0;
                int totalTravelKm = 0;
                while (itr1.hasNext()) {
                    opTO1 = new SecondaryOperationTO();
                    opTO1 = (SecondaryOperationTO) itr1.next();
                    secondaryRouteId = Integer.parseInt(opTO1.getSecondaryRouteId());
                    totalTravelKm = Integer.parseInt(opTO1.getTotalTravelKm());
                    operationTO.setSecondaryRouteId(opTO1.getSecondaryRouteId());
                    rateCostDetails = secondaryOperationDAO.getContractRateCostDetailsEdit(operationTO);
                    Iterator itr2 = rateCostDetails.iterator();
                    SecondaryOperationTO opTO2 = new SecondaryOperationTO();
                    String secondaryRouteCostId = "";
                    String vehicleTypeId = "";
                    double fuelPriceValue = 0.0f;
                    double milleage = 0.0f;
                    double fuelPricePerKm = 0.0f;
                    double totalFuelPrice = 0.0f;
                    double threeKgCngPrice = 0.0f;
                    double totalExpense = 0.0f;
                    double tollAmounts = 0.0f;
                    double addlTollAmounts = 0.0f;
                    double miscCostKm = 0.0f;
                    double parkingCost = 0.0f;
                    int fuelTypeId = 0;
                    double j = 50.0f;
                    double k = 0;
                    double a = 0;
                    double totExp = 0;
                    double totExp2 = 0;
                    while (itr2.hasNext()) {
                        opTO2 = new SecondaryOperationTO();
                        opTO2 = (SecondaryOperationTO) itr2.next();
                        secondaryRouteCostId = opTO2.getSecondaryRouteCostId();
                        operationTO.setSecondaryRouteCostId(secondaryRouteCostId);
                        fuelPriceValue = Double.parseDouble(opTO2.getFuelPrice());
                        milleage = Double.parseDouble(opTO2.getVehicleMileage());
                        fuelPricePerKm = fuelPriceValue / milleage;
                        totalFuelPrice = fuelPricePerKm * totalTravelKm;
                        fuelTypeId = Integer.parseInt(opTO2.getFuelTypeId());
                        if (fuelTypeId == 1002) {

                        } else if (fuelTypeId == 1003) {
                            threeKgCngPrice = fuelPriceValue * 3;
                            totalFuelPrice = totalFuelPrice + threeKgCngPrice;
                        }
                        tollAmounts = Double.parseDouble(opTO2.getTollAmounts());
                        addlTollAmounts = Double.parseDouble(opTO2.getAddlTollAmounts());
                        miscCostKm = Double.parseDouble(opTO2.getMiscCostKm());
                        parkingCost = Double.parseDouble(opTO2.getParkingCost());
                        totalExpense = Math.round(tollAmounts + addlTollAmounts + miscCostKm + parkingCost + totalFuelPrice);

                        k = totalExpense % j;
                        System.out.println("k = " + k);
                        if (k < 50 && k > 0) {
                            a = 50 - k;
                            totExp = totalExpense + a;
                            System.out.println("totExp first loop = " + totExp2);
                        } else if (k == 0) {
                            totExp = totalExpense;
                            System.out.println("totExp second loop = " + totExp2);
                        } else {
                            System.out.println("totExp else part = " + totExp2);
                            totExp = totExp2;
                        }
                        totalExpense = totExp;
                        operationTO.setTotalExpense(String.valueOf(totalExpense));
                        operationTO.setFuelCostPerKms(String.valueOf(fuelPricePerKm));
                        operationTO.setVehicleTypeId(opTO2.getVehTypeId());
                        int updateSecondaryRateCost = secondaryOperationDAO.updateSecondaryRateCost(operationTO);

                    }
                }
            }

            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            System.out.println("am here 7");
            try {
                System.out.println("am here 7a");
                session.endTransaction();
                System.out.println("am here 7b");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            System.out.println("am here 8");

        }

        return response;
    }

    public int updateHumanResourceDetails(SecondaryOperationTO operationTO) {
        int response = 0;
        response = secondaryOperationDAO.updateHumanResourceDetails(operationTO);
        return response;
    }

    public int updateContractDetails(SecondaryOperationTO operationTO) {
        int response = 0;
        response = secondaryOperationDAO.updateContractDetails(operationTO);
        return response;
    }

    public String getCngPrice() {
        String cngPrice = "";
        cngPrice = secondaryOperationDAO.getCngPrice();
        return cngPrice;
    }

    public String getCustomer(SecondaryOperationTO operationTO) {
        String customerDetails = "";
        customerDetails = secondaryOperationDAO.getCustomer(operationTO);
        return customerDetails;
    }

    public boolean checkPointName(SecondaryOperationTO operationTO) {
        boolean checkStatus = false;
        checkStatus = secondaryOperationDAO.checkPointName(operationTO);
        return checkStatus;
    }

    public boolean checkRouteName(SecondaryOperationTO operationTO) {
        boolean checkStatus = false;
        checkStatus = secondaryOperationDAO.checkRouteName(operationTO);
        return checkStatus;
    }

    public ArrayList getMileageConfigList(String customerId) {
        ArrayList mileageConfigList = new ArrayList();
        mileageConfigList = secondaryOperationDAO.getMileageConfigList(customerId);
        return mileageConfigList;
    }

    public ArrayList getSecondaryCustomerList() {
        ArrayList secondaryCustomerList = new ArrayList();
        secondaryCustomerList = secondaryOperationDAO.getSecondaryCustomerList();
        return secondaryCustomerList;
    }

    public ArrayList getSecondaryTripNotSettledList(SecondaryOperationTO operationTO) {
        ArrayList secondaryTripNotSettledList = new ArrayList();
        secondaryTripNotSettledList = secondaryOperationDAO.getSecondaryTripNotSettledList(operationTO);
        return secondaryTripNotSettledList;
    }

    public ArrayList getSecondaryBillingList(SecondaryOperationTO operationTO) {
        ArrayList secondaryBillingList = new ArrayList();
        secondaryBillingList = secondaryOperationDAO.getSecondaryBillingList(operationTO);
        return secondaryBillingList;
    }

    public ArrayList getSecondaryBillingTripList(SecondaryOperationTO operationTO) {
        ArrayList secondaryBillingTripList = new ArrayList();
        secondaryBillingTripList = secondaryOperationDAO.getSecondaryBillingTripList(operationTO);
        return secondaryBillingTripList;
    }

    public String checkTripClosure(SecondaryOperationTO operationTO) {
        String checkTripClosure = "";
        checkTripClosure = secondaryOperationDAO.checkTripClosure(operationTO);
        return checkTripClosure;
    }

    public String getVehicleTypeFuelDetails(SecondaryOperationTO operationTO) {
        String vehicleTypeFuelDetails = "";
        vehicleTypeFuelDetails = secondaryOperationDAO.getVehicleTypeFuelDetails(operationTO);
        return vehicleTypeFuelDetails;
    }

    public String getVehicleTypeCngDeviation(SecondaryOperationTO operationTO) {
        String vehicleTypeCngDeviation = "";
        vehicleTypeCngDeviation = secondaryOperationDAO.getVehicleTypeCngDeviation(operationTO);
        return vehicleTypeCngDeviation;
    }

    public String getContractFuelAmount(SecondaryOperationTO operationTO) {
        String contractFuelAmount = "";
        contractFuelAmount = secondaryOperationDAO.getContractFuelAmount(operationTO);
        return contractFuelAmount;
    }

    /**
     * This method used to Get Secondary Driver Name List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getSecondaryDriverName(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList driverName = new ArrayList();
        driverName = secondaryOperationDAO.getSecondaryDriverName(operationTO);
        return driverName;
    }

    public ArrayList getPrimaryDriverName(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList driverName = new ArrayList();
        driverName = secondaryOperationDAO.getPrimaryDriverName(operationTO);
        return driverName;
    }

    public ArrayList getSecondaryFleet(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList fleetName = new ArrayList();
        fleetName = secondaryOperationDAO.getSecondaryFleet(operationTO);
        return fleetName;
    }

    public ArrayList getSecondaryDriverNameForSettelment(SecondaryOperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList driverName = new ArrayList();
        driverName = secondaryOperationDAO.getSecondaryDriverNameForSettelment(operationTO);
        return driverName;
    }

    public String getSecRouteApprovalBy(String routeName) throws FPRuntimeException, FPBusinessException {
        // ArrayList driverName = new ArrayList();
        String status = "";
        status = secondaryOperationDAO.getSecRouteApprovalBy(routeName);
        return status;
    }

    public int UpdateSecondaryRouteApproval(String routeName, String mailId) throws FPRuntimeException, FPBusinessException {
        // ArrayList driverName = new ArrayList();
        int status = 0;
        status = secondaryOperationDAO.UpdateSecondaryRouteApproval(routeName, mailId);
        return status;
    }

    public int UpdateSecondaryRouteRejection(String routeName, String mailId) throws FPRuntimeException, FPBusinessException {
        // ArrayList driverName = new ArrayList();
        int status = 0;
        status = secondaryOperationDAO.UpdateSecondaryRouteRejection(routeName, mailId);
        return status;
    }

    public int saveBill(SecondaryOperationTO operationTO, String customerName, String invoiceCode, String invoiceNo, String invoiceDate, String invoiceAmount, String grandTotal, String invoiceStatus, String payAmount) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = secondaryOperationDAO.saveBill(operationTO, customerName, invoiceCode, invoiceNo, invoiceDate, invoiceAmount, grandTotal, invoiceStatus, payAmount);
        return insertStatus;
    }

    public double saveFuelPriceMaster(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        double fuelPriceMaster = 0.0;
        fuelPriceMaster = secondaryOperationDAO.saveFuelPriceMaster(operationTO, userId);
        return fuelPriceMaster;
    }

    public double saveFuelPriceMasterLog(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        double fuelPriceMaster = 0.0;
        fuelPriceMaster = secondaryOperationDAO.saveFuelPriceMasterLog(operationTO, userId);
        return fuelPriceMaster;
    }

    public int deleteFuelPriceMasterLog() throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = secondaryOperationDAO.deleteFuelPriceMasterLog();
        return insertStatus;
    }
    
       public ArrayList getFuelUplodedLogDetails(int userId,String fuelMonth, String fuelYear) throws FPRuntimeException, FPBusinessException {
        ArrayList getFuelUplodedLogDetails = new ArrayList();
        getFuelUplodedLogDetails = secondaryOperationDAO.getFuelUplodedLogDetails(userId,fuelMonth,fuelYear);
        return getFuelUplodedLogDetails;
    }   
    public int updateFuelUplodedDetails(int userId,String fuelMonth, String fuelYear) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = secondaryOperationDAO.updateFuelUplodedDetails(userId,fuelMonth,fuelYear);
        return insertStatus;
    }


}
