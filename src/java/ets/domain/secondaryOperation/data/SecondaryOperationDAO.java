/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.secondaryOperation.data;

import ets.arch.exception.FPRuntimeException;
import ets.domain.operation.business.OperationTO;
import ets.domain.secondaryOperation.business.SecondaryOperationTO;
import ets.domain.util.FPLogUtils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

/**
 *
 * @author Throttle
 */
public class SecondaryOperationDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "SecondaryOperationDAO";

    public ArrayList getCity(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        map.put("cityName", operationTO.getCityName() + "%");
        ArrayList cityList = new ArrayList();
        try {

            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getCity", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getCity", sqlException);
        }
        return cityList;

    }

    public ArrayList getCustomerPoints(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        map.put("cityName", operationTO.getCityName() + "%");
        map.put("customerId", operationTO.getCustomerId());
        ArrayList cityList = new ArrayList();
        try {

            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getCustomerPoints", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getCity", sqlException);
        }
        return cityList;

    }

    public ArrayList getSecondaryCustomerDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        map.put("customerName", operationTO.getCustomerName() + "%");
        ArrayList customerName = new ArrayList();
        try {

            customerName = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryCustomerDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getSecondaryCustomerDetails", sqlException);
        }
        return customerName;

    }

    public String getConfigDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        String configDetails = "";
        try {
            configDetails = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getConfigDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConfigDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConfigDetails", sqlException);
        }

        return configDetails;
    }

    public int insertContractRouteMater(SecondaryOperationTO operationTO) {
        Map map = new HashMap();

        int insertContractRouteMater = 0;
        try {
            map.put("routeId", insertContractRouteMater);
            String routeCode = "";
            String[] temp = null;
            routeCode = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getSecondaryRouteCode", map);
            if (routeCode == null) {
                routeCode = "SRC-0001";
            } else if (routeCode.length() == 1) {
                routeCode = "SRC-000" + routeCode;
            } else if (routeCode.length() == 2) {
                routeCode = "SRC-00" + routeCode;
            } else if (routeCode.length() == 3) {
                routeCode = "SRC-0" + routeCode;
            }
            map.put("routeCode", routeCode);
            map.put("routeName", operationTO.getRouteName());
            map.put("customerId", operationTO.getCustomerId());
            map.put("fromDate", operationTO.getRouteValidFrom());
            map.put("toDate", operationTO.getRouteValidTo());
            map.put("fuelCost", operationTO.getFuelCost());
            map.put("totalKm", operationTO.getDistance());
            map.put("totalHours", operationTO.getTotalHours());
            map.put("totalMinutes", operationTO.getTotalMinutes());
            map.put("totalReeferHours", operationTO.getTotalReeferHours());
            map.put("totalReeferMinutes", operationTO.getTotalReeferMinutes());
            map.put("fixedKm", operationTO.getFixedKmPerMonth());
            map.put("fixedReeferHours", operationTO.getFixedReeferHours());
            map.put("fixedReeferMinutes", operationTO.getFixedReeferMinutes());
            map.put("totalWaitMinutes", operationTO.getTotalWaitMinutes());
            map.put("averageKM", operationTO.getAverageKM());
            map.put("userId", operationTO.getUserId());
            map.put("totalReferMin", operationTO.getTotalReferMin());
            System.out.println("map = " + map);
            insertContractRouteMater = (Integer) getSqlMapClientTemplate().insert("secondaryOperation.insertContractRouteMater", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractRouteMater Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractRouteMater", sqlException);
        }

        return insertContractRouteMater;
    }

    public int insertContractMaster(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        int response = 0;
        try {

            map.put("customerId", operationTO.getCustomerId());
            map.put("fromDate", operationTO.getRouteValidFrom());
            map.put("toDate", operationTO.getRouteValidTo());
            map.put("extraKmCalculation", operationTO.getExtraKmCalculation());
            map.put("contractCngCost", operationTO.getContractCngCost());
            map.put("contractDieselCost", operationTO.getContractDieselCost());
            map.put("rateChangeOfCng", operationTO.getRateChangeOfCng());
            map.put("rateChangeOfDiesel", operationTO.getRateChangeOfDiesel());
            map.put("userId", operationTO.getUserId());
            System.out.println("map = " + map);
            response = (Integer) getSqlMapClientTemplate().insert("secondaryOperation.insertContractMaster", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractMaster", sqlException);
        }

        return response;
    }

    public int insertContractDetails(SecondaryOperationTO operationTO, int contractId) {
        Map map = new HashMap();
        int response = 0;
        try {

            map.put("contractId", contractId);
            map.put("vehicleTypeId", operationTO.getVehTypeId());
            map.put("noOfVehicle", operationTO.getNoOfVehicle());
            map.put("fixedKm", operationTO.getFixedKm());
            map.put("fixedKmCharge", operationTO.getFixedKmCharge());
            map.put("extraKmCharge", operationTO.getExtraKmCharge());
            map.put("userId", operationTO.getUserId());
            System.out.println("map = " + map);
            response = (Integer) getSqlMapClientTemplate().insert("secondaryOperation.insertContractDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractDetails", sqlException);
        }

        return response;
    }

    public int insertContractRouteDetails(SecondaryOperationTO operationTO, int insertContractRouteMater) {
        Map map = new HashMap();
        int insertContractRouteDetails = 0;
        try {
            map.put("secondaryRouteId", insertContractRouteMater);
            map.put("pointId", operationTO.getCityId());
            map.put("pointName", operationTO.getCityName());
            map.put("pointType", operationTO.getPointType());
            map.put("pointSequence", operationTO.getPointSequence());
            map.put("pointAddress", operationTO.getPointAddresss());
            map.put("latitude", operationTO.getLatitude());
            map.put("longitude", operationTO.getLongitude());
            map.put("travelKm", operationTO.getTravelKm());
            map.put("travelHours", operationTO.getTravelHour());
            map.put("travelMinutes", operationTO.getTravelMinute());
            map.put("reeferHour", operationTO.getReeferHour());
            map.put("reeferMinute", operationTO.getReeferMinute());
            map.put("waitMinute", operationTO.getWaitMinute());
            map.put("userId", operationTO.getUserId());
            System.out.println("map = " + map);
            insertContractRouteDetails = (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertContractRouteDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractRouteDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractRouteDetails", sqlException);
        }

        return insertContractRouteDetails;
    }

    public int insertContractRouteCostDetails(SecondaryOperationTO operationTO, int insertContractRouteMater) {
        Map map = new HashMap();
        int insertContractRouteCostDetails = 0;
        try {
            map.put("secondaryRouteId", insertContractRouteMater);
            map.put("vehTypeId", operationTO.getVehTypeId());
            map.put("fuelCostPerKms", operationTO.getFuelCostPerKms());
            map.put("tollAmounts", operationTO.getTollAmounts());
            map.put("addlTollAmounts", operationTO.getAddlTollAmounts());
            map.put("miscCostKm", operationTO.getMiscCostKm());
            map.put("totExpense", operationTO.getTotExpense());
            map.put("parkingCost", operationTO.getParkingCost());
            map.put("userId", operationTO.getUserId());
            map.put("fuelCostPerMin", operationTO.getFuelCostPerMin());
            map.put("fuelCostPerMin", operationTO.getFuelCostPerMin());
            System.out.println("map = " + map);
            insertContractRouteCostDetails = (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertContractRouteCostDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractRouteCostDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractRouteCostDetails", sqlException);
        }

        return insertContractRouteCostDetails;
    }

    public String getSecondaryRouteCode(int insertContractRouteMater) {
        Map map = new HashMap();
        String secondaryRouteCode = "";
        map.put("routeId", insertContractRouteMater);
        try {
            secondaryRouteCode = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getSecondaryRouteCode", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryRouteCode Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryRouteCode", sqlException);
        }

        return secondaryRouteCode;
    }

    public ArrayList getSecondaryContractRouteMasterlist(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList SecondaryContractRouteList = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        System.out.println("map = " + map);
        try {

            SecondaryContractRouteList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryContractRouteMaster", map);
            System.out.println("SecondaryContractRouteList.size() = " + SecondaryContractRouteList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "MfrList", sqlException);
        }
        return SecondaryContractRouteList;

    }

    public int saveSecondaryTripSchedule(SecondaryOperationTO operationTO, int userId, String passedValue) {
        Map map = new HashMap();
        int routeId = 0;
        int insertStatus = 0;
        try {
            String[] temp1 = null;
            String[] temp2 = null;
            map.put("customerId", operationTO.getCustomerId());
            map.put("tripStatusId", 19);
            map.put("userId", userId);
            String checkSecondaryTripSchedule = "";
            if (passedValue.contains(",")) {
                temp1 = passedValue.split(",");
                for (int i = 0; i < temp1.length; i++) {
                    if (temp1[i].contains("~")) {
                        temp2 = temp1[i].split("~");
                        map.put("scheduleDate", temp2[0]);
                        map.put("routeId", temp2[1]);
                        map.put("status", temp2[2]);
                        System.out.println("map====" + map);
                        checkSecondaryTripSchedule = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.checkSecondaryTripSchedule", map);
                        if (checkSecondaryTripSchedule == null) {
                            insertStatus += (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertSecondaryTripSchedule", map);
                        } else {
                            insertStatus += (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateSecondaryTripSchedule", map);
                        }
                    }
                }
            }
        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveRoute Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveRoute", sqlException);
        }

        return insertStatus;
    }

    public ArrayList getSecondaryTripCustomer(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList secondaryTripCustomerList = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        try {

            secondaryTripCustomerList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryTripCustomer", map);

            System.out.println("secondaryTripCustomerList size=" + secondaryTripCustomerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "bunkList", sqlException);
        }
        return secondaryTripCustomerList;
    }

    public ArrayList getSecondaryTripScheduleList(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList secondaryTripScheduleList = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        map.put("fromDate", operationTO.getFromDate());
        map.put("toDate", operationTO.getToDate());
        System.out.println("map = " + map);
        try {

            secondaryTripScheduleList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryTripScheduleList", map);

            System.out.println("getSecondaryTripScheduleList size=" + secondaryTripScheduleList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryTripScheduleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getSecondaryTripScheduleList", sqlException);
        }
        return secondaryTripScheduleList;
    }

    public String getTripScheduleStatus(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        String secondaryTripSchedule = "";
        map.put("customerId", operationTO.getCustomerId());
        map.put("secondaryRouteId", operationTO.getSecondaryRouteId());
        map.put("scheduleDate", operationTO.getDate());
        System.out.println("map = " + map);
        try {

            secondaryTripSchedule = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getTripScheduleStatus", map);

            System.out.println("getSecondaryTripScheduleList size=" + secondaryTripSchedule);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripScheduleStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getTripScheduleStatus", sqlException);
        }
        return secondaryTripSchedule;
    }

    public String getDateWiseTripScheduleStatus(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        String secondaryTripSchedule = "";
        map.put("customerId", operationTO.getCustomerId());
        map.put("secondaryRouteId", operationTO.getSecondaryRouteId());
        map.put("scheduleDate", operationTO.getDate());
        System.out.println("map = " + map);
        try {

            secondaryTripSchedule = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getDateWiseTripScheduleStatus", map);

            System.out.println("getSecondaryTripScheduleList size=" + secondaryTripSchedule);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripScheduleStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getTripScheduleStatus", sqlException);
        }
        return secondaryTripSchedule;
    }

    public ArrayList getSecondaryContract() {
        Map map = new HashMap();
        ArrayList getSecondaryContractView = new ArrayList();

        try {
            getSecondaryContractView = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryContract", map);
            System.out.println(" getTyreMfr Size :: :: " + getSecondaryContractView.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryContract Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryContract", sqlException);
        }
        return getSecondaryContractView;
    }

    public ArrayList getContractRouteDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList routeDetails = new ArrayList();
        map.put("routeId", operationTO.getSecondaryRouteId());
        map.put("customerId", operationTO.getCustomerId());
        try {
            routeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getContractRouteDetails", map);
            System.out.println(" getContractRouteDetails Size :: :: " + routeDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractRouteDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getContractRouteDetails", sqlException);
        }
        return routeDetails;
    }

    public ArrayList getContractRateCostDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList routeDetails = new ArrayList();
        map.put("routeId", operationTO.getSecondaryRouteId());
        try {
            routeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getContractRateCostDetails", map);
            System.out.println(" getContractRateCostDetails Size :: :: " + routeDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractRateCostDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getContractRateCostDetails", sqlException);
        }
        return routeDetails;
    }

    public ArrayList getContractRateCostDetailsEdit(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList routeDetails = new ArrayList();
        map.put("routeId", operationTO.getSecondaryRouteId());
        map.put("customerId", operationTO.getCustomerId());
        System.out.println("map in the dao = " + map);
        try {
            routeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getContractRateCostDetailsEdit", map);
            System.out.println(" getContractRateCostDetails Size :: :: " + routeDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractRateCostDetailsEdit Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getContractRateCostDetailsEdit", sqlException);
        }
        return routeDetails;
    }

    public int updateSecondaryRateCost(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        int updateSecondaryRateCost = 0;
        map.put("secondaryRouteCostId", operationTO.getSecondaryRouteCostId());
        map.put("secondaryRouteId", operationTO.getSecondaryRouteId());
        map.put("vehicleTypeId", operationTO.getVehicleTypeId());
        map.put("perKmFuelCost", operationTO.getFuelCostPerKms());
        map.put("perKmFuelCost", operationTO.getFuelCostPerKms());
        map.put("totalExpense", operationTO.getTotalExpense());
        map.put("userId", operationTO.getUserId());
        System.out.println("map in the dao= " + map);
        try {
            updateSecondaryRateCost = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateSecondaryRateCost", map);
            System.out.println("updateSecondaryRateCost = " + updateSecondaryRateCost);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateSecondaryRateCost Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateSecondaryRateCost", sqlException);
        }

        return updateSecondaryRateCost;
    }

    public ArrayList getRouteContractDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList routeDetails = new ArrayList();
        map.put("routeId", operationTO.getSecondaryRouteId());
        try {
            routeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getRouteContractDetails", map);
            System.out.println(" getRouteContractDetails Size :: :: " + routeDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRouteContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRouteContractDetails", sqlException);
        }
        return routeDetails;
    }

    public ArrayList getRoutePointDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList routePointDetails = new ArrayList();
        map.put("routeId", operationTO.getSecondaryRouteId());
        System.out.println("map = " + map);
        try {
            routePointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getRoutePointDetails", map);
            System.out.println(" getRouteContractDetails Size :: :: " + routePointDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRouteContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRouteContractDetails", sqlException);
        }
        return routePointDetails;
    }

    public int updateSecondaryTripSchedule(SecondaryOperationTO operationTO, int userId, String passedValue) {
        Map map = new HashMap();
        int routeId = 0;
        int insertStatus = 0;
        try {
            String[] temp1 = null;
            String[] temp2 = null;
            map.put("customerId", operationTO.getCustomerId());
            map.put("tripStatusId", 19);
            map.put("userId", userId);
            String checkSecondaryTripSchedule = "";
            if (passedValue.contains(",")) {
                temp1 = passedValue.split(",");
                for (int i = 0; i < temp1.length; i++) {
                    if (temp1[i].contains("~")) {
                        temp2 = temp1[i].split("~");
                        map.put("scheduleDate", temp2[0]);
                        map.put("routeId", temp2[1]);
                        map.put("status", temp2[2]);
                        System.out.println("map====" + map);
                        checkSecondaryTripSchedule = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.checkSecondaryTripSchedule", map);
                        if (checkSecondaryTripSchedule == null) {
                            insertStatus += (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertSecondaryTripSchedule", map);
                        } else {
                            insertStatus += (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateSecondaryTripSchedule", map);
                        }
                    }
                }
            }
        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveRoute Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveRoute", sqlException);
        }

        return insertStatus;
    }

    public int updateContractRouteMater(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        int insertContractRouteMater = 0;
        try {
            map.put("routeId", insertContractRouteMater);
            String routeCode = "";
            String[] temp = null;
            routeCode = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getSecondaryRouteCode", map);
            if (routeCode == null) {
                routeCode = "SRC-0001";
            } else {
                routeCode = "SRC-0001";
            }
            map.put("secondaryRouteId", operationTO.getSecondaryRouteId());
            map.put("routeCode", routeCode);
            map.put("routeName", operationTO.getRouteName());
            map.put("customerId", operationTO.getCustomerId());
            map.put("fromDate", operationTO.getRouteValidFrom());
            map.put("toDate", operationTO.getRouteValidTo());
            map.put("fuelCost", operationTO.getFuelCost());
            map.put("totalKm", operationTO.getDistance());
            map.put("totalHours", operationTO.getTotalHours());
            map.put("totalMinutes", operationTO.getTotalMinutes());
            map.put("totalReeferHours", operationTO.getTotalReeferHours());
            map.put("totalReeferMinutes", operationTO.getTotalReeferMinutes());
            map.put("fixedKm", operationTO.getFixedKmPerMonth());
            map.put("fixedReeferHours", operationTO.getFixedReeferHours());
            map.put("fixedReeferMinutes", operationTO.getFixedReeferMinutes());
            map.put("totalWaitMinutes", operationTO.getTotalWaitMinutes());
            map.put("averageKM", operationTO.getAverageKM());
            map.put("totalReferMin", operationTO.getTotalReferMin());

            map.put("userId", operationTO.getUserId());
            System.out.println("map = " + map);
            insertContractRouteMater = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateContractRouteMater", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateContractRouteMater Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateContractRouteMater", sqlException);
        }

        return insertContractRouteMater;
    }

    public int updateContractRouteDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        int insertContractRouteDetails = 0;
        try {
            map.put("secondaryRouteDetailId", operationTO.getSecondaryRouteDetailId());
            map.put("pointId", operationTO.getCityId());
            map.put("pointName", operationTO.getCityName());
            map.put("pointType", operationTO.getPointType());
            map.put("pointSequence", operationTO.getPointSequence());
            map.put("pointAddress", operationTO.getPointAddresss());
            map.put("latitude", operationTO.getLatitude());
            map.put("longitude", operationTO.getLongitude());
            map.put("travelKm", operationTO.getTravelKm());
            map.put("travelHours", operationTO.getTravelHour());
            map.put("travelMinutes", operationTO.getTravelMinute());
            map.put("reeferHour", operationTO.getReeferHour());
            map.put("reeferMinute", operationTO.getReeferMinute());
            map.put("waitMinute", operationTO.getWaitMinute());
            map.put("activeStatus", operationTO.getActiveStatus());
            map.put("userId", operationTO.getUserId());
            System.out.println("map = " + map);
            insertContractRouteDetails = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateContractRouteDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateContractRouteDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateContractRouteDetails", sqlException);
        }

        return insertContractRouteDetails;
    }

    public int updateContractRouteCostDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        int insertContractRouteCostDetails = 0;
        try {
            map.put("secondaryRouteCostId", operationTO.getSecondaryRouteCostId());
            map.put("vehTypeId", operationTO.getVehTypeId());
            map.put("fuelCostPerKms", operationTO.getFuelCostPerKms());
            map.put("parkingCost", operationTO.getParkingCost());
            map.put("tollAmounts", operationTO.getTollAmounts());
            map.put("addlTollAmounts", operationTO.getAddlTollAmounts());
            map.put("miscCostKm", operationTO.getMiscCostKm());
            map.put("totExpense", operationTO.getTotExpense());
            map.put("userId", operationTO.getUserId());
            map.put("fuelCostPerMin", operationTO.getFuelCostPerMin());
            System.out.println("map = " + map);
            insertContractRouteCostDetails = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateContractRouteCostDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateContractRouteCostDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateContractRouteCostDetails", sqlException);
        }

        return insertContractRouteCostDetails;
    }

    public ArrayList getSecondaryVehicleList(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList routePointDetails = new ArrayList();
        map.put("customerName", operationTO.getCustomerName());
        System.out.println("map = " + map);
        try {

            routePointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryVehicleList", map);
            System.out.println(" getRouteContractDetails Size :: :: " + routePointDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRouteContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRouteContractDetails", sqlException);
        }
        return routePointDetails;
    }

    public String getVehicleTypeId(String vehicleId) {
        Map map = new HashMap();
        String vehicleTypeId = "";
        map.put("vehicleId", vehicleId);
        try {
            vehicleTypeId = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getVehicleTypeId", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleTypeId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleTypeId", sqlException);
        }

        return vehicleTypeId;
    }

    public int updateTripScheduleStatus(int tripid, String customerId, String secondaryRouteId, String tripScheduleDate, int userId) {
        Map map = new HashMap();
        int updateTripScheduleStatus = 0;
        try {
            map.put("tripStatusId", "6");
            map.put("tripId", tripid);
            map.put("userId", userId);
            map.put("secondaryRouteId", secondaryRouteId);
            map.put("tripScheduleDate", tripScheduleDate);
            map.put("customerId", customerId);
            System.out.println("map = " + map);
            updateTripScheduleStatus = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateTripScheduleStatus", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTripScheduleStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTripScheduleStatus", sqlException);
        }

        return updateTripScheduleStatus;
    }

    /**
     * This method used to Get Route List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getSecondaryCustomerPoints(SecondaryOperationTO operationTO, String customerId) {
        Map map = new HashMap();
        ArrayList SecondaryCustomerPointsList = new ArrayList();
        map.put("customerId", customerId);
        System.out.println("map = " + map);
        try {
            System.out.println("this is City Master");
            SecondaryCustomerPointsList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryCustomerPoints", map);
            System.out.println(" SecondaryCustomerPointsList =" + SecondaryCustomerPointsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getcityMaster List", sqlException);
        }

        return SecondaryCustomerPointsList;
    }

    /**
     * This method used to Modify standardCharge .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int updateSecondaryRoute(SecondaryOperationTO operationTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int userid = userId;

        map.put("pointName", operationTO.getPointName());
        map.put("customerId", operationTO.getCustomerId());
        map.put("pointType", operationTO.getPointType());
        map.put("pointAddress", operationTO.getPointAddress());
        map.put("cityId", operationTO.getCityId());
        map.put("pointId", operationTO.getPointId());
        map.put("latitudePosition", operationTO.getLatitudePosition());
        map.put("longitudePosition", operationTO.getLongitudePosition());
        map.put("status", operationTO.getStatus());
        System.out.println("updateSecondaryRoute = " + map);
        try {

            status = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateSecondaryRoute", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateCityMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateCityMaster", sqlException);
        }

        return status;
    }

    public int saveSecondaryRoutes(SecondaryOperationTO operationTO, int userId) {
        Map map = new HashMap();
        int insertContractRouteMater = 0;
        map.put("pointName", operationTO.getPointName());
        map.put("customerId", operationTO.getCustomerId());
        map.put("pointType", operationTO.getPointType());
        map.put("pointAddress", operationTO.getPointAddress());
        map.put("cityId", operationTO.getCityId());
        map.put("latitudePosition", operationTO.getLatitudePosition());
        map.put("longitudePosition", operationTO.getLongitudePosition());
        map.put("userId", userId);
        try {
            System.out.println("map 1= " + map);
            insertContractRouteMater = (Integer) getSqlMapClientTemplate().update("secondaryOperation.saveSecondaryCustomerPoints", map);
//               System.out.println("map 1= " + insertContractRouteMater);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractRouteMater Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveSecondaryCustomerPoints", sqlException);
        }

        return insertContractRouteMater;
    }

    public ArrayList getSecondarySettlementDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList settlementDetails = new ArrayList();
        map.put("fromDate", operationTO.getFromDate());
        map.put("toDate", operationTO.getToDate());
        map.put("driverId", operationTO.getPrimaryDriverId());
        System.out.println("map = " + map);
        try {
            settlementDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondarySettlementDetails", map);
            System.out.println(" getSecondarySettlementDetails Size :: :: " + settlementDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondarySettlementDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondarySettlementDetails", sqlException);
        }
        return settlementDetails;
    }

    public ArrayList getTotalExpenseDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList totalExpense = new ArrayList();
        map.put("fromDate", operationTO.getFromDate());
        map.put("toDate", operationTO.getToDate());
        map.put("driverId", operationTO.getPrimaryDriverId());
        System.out.println("map = " + map);
        try {
            totalExpense = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getTotalExpenseDetails", map);
            System.out.println(" getTotalExpenseDetails Size :: :: " + totalExpense.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTotalExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTotalExpenseDetails", sqlException);
        }
        return totalExpense;
    }

    public String getDriverIncentiveAmount() {
        Map map = new HashMap();
        String tollAmount = "";
        try {
            tollAmount = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getDriverIncentive", map);
            System.out.println("getDriverIncentive size=" + tollAmount);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverIncentive Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverIncentive List", sqlException);
        }

        return tollAmount;
    }

    public String getDriverBataAmount() {
        Map map = new HashMap();
        String tollAmount = "";
        try {
            tollAmount = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getDriverBatta", map);
            System.out.println("getDriverBataAmount size=" + tollAmount);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverBataAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverBataAmount List", sqlException);
        }

        return tollAmount;
    }

    public String getFuelPrice() {
        Map map = new HashMap();
        String fuelPrice = "";
        try {
            fuelPrice = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getFuelPrice", map);
            System.out.println("getFuelPrice size=" + fuelPrice);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelPrice List", sqlException);
        }

        return fuelPrice;
    }

    public String getTollAmount() {
        Map map = new HashMap();
        String tollAmount = "";
        try {
            tollAmount = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getTollAmount", map);
            System.out.println("getTollAmount size=" + tollAmount);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tollAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "tollAmount List", sqlException);
        }

        return tollAmount;
    }

    public String getMiscCost() {
        Map map = new HashMap();
        String tollAmount = "";
        try {
            tollAmount = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getMiscCost", map);
            System.out.println("getTollAmount size=" + tollAmount);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tollAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "tollAmount List", sqlException);
        }

        return tollAmount;
    }

    public String getFactor() {
        Map map = new HashMap();
        String tollAmount = "";
        try {
            tollAmount = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getFactor", map);
            System.out.println("getTollAmount size=" + tollAmount);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tollAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "tollAmount List", sqlException);
        }

        return tollAmount;
    }

    public ArrayList getTripExpenses(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList settlementDetails = new ArrayList();
        map.put("fromDate", operationTO.getFromDate());
        map.put("toDate", operationTO.getToDate());
        map.put("driverId", operationTO.getPrimaryDriverId());
        System.out.println("map = " + map);
        try {
            settlementDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getTripExpenses", map);
            System.out.println(" getSecondarySettlementDetails Size :: :: " + settlementDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondarySettlementDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondarySettlementDetails", sqlException);
        }
        return settlementDetails;
    }

    public ArrayList getViewSecondaryCustomerVehicle(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList SecondaryCustomerVehicleList = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        System.out.println("map = " + map);
        try {
            System.out.println("this is getViewSecondaryCustomerVehicle");
            SecondaryCustomerVehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getViewSecondaryCustomer", map);
            System.out.println(" SecondaryCustomerVehicleList =" + SecondaryCustomerVehicleList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getViewSecondaryCustomerVehicle List", sqlException);
        }

        return SecondaryCustomerVehicleList;
    }

    public ArrayList getViewSecondaryCustomerdetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList SecondaryCustomerList = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        System.out.println("map = " + map);
        try {
            System.out.println("this is getViewSecondaryCustomerdetails");
            SecondaryCustomerList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getViewSecondaryCustomerdetails", map);
            System.out.println(" getViewSecondaryCustomerdetails =" + SecondaryCustomerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getViewSecondaryCustomerdetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getViewSecondaryCustomerdetails List", sqlException);
        }

        return SecondaryCustomerList;
    }

    public ArrayList getTripAdvances(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList settlementDetails = new ArrayList();
        map.put("fromDate", operationTO.getFromDate());
        map.put("toDate", operationTO.getToDate());
        map.put("driverId", operationTO.getPrimaryDriverId());
        System.out.println("map = " + map);
        try {
            settlementDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getTripAdvances", map);
            System.out.println(" getSecondarySettlementDetails Size :: :: " + settlementDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondarySettlementDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondarySettlementDetails", sqlException);
        }
        return settlementDetails;
    }

    public ArrayList getCustomerPaymentDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList settlementDetails = new ArrayList();
        map.put("fromDate", operationTO.getFromDate());
        map.put("toDate", operationTO.getToDate());
        map.put("customerId", operationTO.getCustomerId());
        System.out.println("map = " + map);
        try {
            settlementDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getCustomerPaymentDetails", map);
            System.out.println(" getCustomerPaymentDetails Size :: :: " + settlementDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerPaymentDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerPaymentDetails", sqlException);
        }
        return settlementDetails;
    }

    public ArrayList getCustomerCollectBillDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList collectBillAmountDetails = new ArrayList();
        map.put("fromDate", operationTO.getFromDate());
        map.put("toDate", operationTO.getToDate());
        map.put("customerId", operationTO.getCustomerId());
        System.out.println("map = " + map);
        try {
            collectBillAmountDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getCustomerCollectBillDetails", map);
            System.out.println(" getCustomerCollectBillDetails Size :: :: " + collectBillAmountDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerPaymentDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerPaymentDetails", sqlException);
        }
        return collectBillAmountDetails;
    }

    public ArrayList getHumanResourceList() {
        Map map = new HashMap();
        ArrayList humanResourceList = new ArrayList();
        System.out.println("map = " + map);
        try {
            humanResourceList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getHumanResourceList", map);
            System.out.println(" getHumanResourceList Size :: :: " + humanResourceList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHumanResourceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHumanResourceList", sqlException);
        }
        return humanResourceList;
    }

    public int insertHumanResourceDetails(SecondaryOperationTO operationTO, int contractId) {
        Map map = new HashMap();
        int response = 0;
        try {

            map.put("contractId", contractId);
            map.put("hrId", operationTO.getHrId());
            map.put("noOfPersons", operationTO.getNoOfPersons());
            map.put("fixedAmount", operationTO.getFixedAmount());
            map.put("userId", operationTO.getUserId());
            System.out.println("map = " + map);
            response = (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertHumanResourceDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertHumanResourceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertHumanResourceDetails", sqlException);
        }

        return response;
    }

    public ArrayList getViewHumanResourceDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList humanResourceList = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        System.out.println("map = " + map);
        try {
            System.out.println("this is getViewHumanResourceDetails");
            humanResourceList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getViewHumanResourceDetails", map);
            System.out.println(" getViewHumanResourceDetails =" + humanResourceList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getViewHumanResourceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getViewHumanResourceDetails List", sqlException);
        }

        return humanResourceList;
    }

    public ArrayList getTripClosureDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList closureDetails = new ArrayList();
        map.put("fromDate", operationTO.getFromDate());
        map.put("toDate", operationTO.getToDate());
        map.put("driverId", operationTO.getPrimaryDriverId());
        System.out.println("map = " + map);
        try {
            System.out.println("this is getTripClosureDetails");
            closureDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getTripClosureDetails", map);
            System.out.println(" getTripClosureDetails =" + closureDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripClosureDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripClosureDetails List", sqlException);
        }

        return closureDetails;
    }

    public int insertSecondaryDriverSettlement(SecondaryOperationTO operationTO, int userId) {
        Map map = new HashMap();
        int insertSecondaryDriverSettlement = 0;
        try {
            map.put("payAmount", operationTO.getPayAmount());
            map.put("cfAmount", operationTO.getCfAmount());
            map.put("primaryDriverId", operationTO.getPrimaryDriverId());
            map.put("primaryDriverId", operationTO.getPrimaryDriverId());
            map.put("totalRcmAllocation", operationTO.getTotalRcmAllocation());
            map.put("totalBpclAllocation", operationTO.getTotalBpclAllocation());
            map.put("totalExtraExpense", operationTO.getTotalExtraExpense());
            map.put("totalMiscellaneousExpense", operationTO.getTotalMiscellaneousExpense());
            map.put("totalDriverBhatta", operationTO.getTotalDriverBhatta());
            map.put("totalExpenses", operationTO.getTotalExpenses());
            map.put("balanceAmount", operationTO.getBalanceAmount());
            map.put("startingBalance", operationTO.getStartingBalance());
            map.put("endingBalance", operationTO.getEndingBalance());
            map.put("paymentMode", operationTO.getPaymentMode());
            map.put("settlementRemarks", operationTO.getSettlementRemarks());
            map.put("userId", userId);
            System.out.println("map = " + map);
            insertSecondaryDriverSettlement = (Integer) getSqlMapClientTemplate().insert("secondaryOperation.insertSecondaryDriverSettlement", map);
            System.out.println("insertSecondaryDriverSettlement value is = " + insertSecondaryDriverSettlement);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractRouteCostDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractRouteCostDetails", sqlException);
        }

        return insertSecondaryDriverSettlement;
    }

    public String getStartingBalance(String driverId) {
        Map map = new HashMap();
        String startingBalance = "";
        map.put("driverId", driverId);
        System.out.println("map = " + map);
        try {
            startingBalance = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getStartingBalance", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConfigDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConfigDetails", sqlException);
        }

        return startingBalance;
    }

    public int insertSecondaryDriverSettlementDetails(SecondaryOperationTO operationTO, int insertDriverSettlement) {
        Map map = new HashMap();
        int insertSecondaryDriverSettlement = 0;
        try {
            map.put("insertDriverSettlement", insertDriverSettlement);
            map.put("tripId", operationTO.getTripId());
            map.put("fuelCost", operationTO.getFuelCost());
            map.put("totalKm", operationTO.getTotalKm());
            map.put("totalHours", operationTO.getTotalHours());
            map.put("fuelConsumption", operationTO.getFuelConsumption());
            if ("".equals(operationTO.getRcmExpense())) {
                map.put("rcmExpense", 0);
            } else {
                map.put("rcmExpense", operationTO.getRcmExpense());
            }
            map.put("advanceAmount", operationTO.getAdvanceAmount());
            map.put("extraExpense", operationTO.getExtraExpense());
            map.put("miscExpense", operationTO.getMiscExpense());
            map.put("driverBatta", operationTO.getDriverBatta());
            map.put("totalExpense", operationTO.getTotalExpense());
            map.put("userId", operationTO.getUserId());
            map.put("balanceAmount", operationTO.getTripBalance());
            map.put("paymentMode", operationTO.getPaymentMode());
            map.put("settlementRemarks", operationTO.getSettlementRemarks());
            System.out.println("map = " + map);
            insertSecondaryDriverSettlement = (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertSecondaryDriverSettlementDetails", map);
            System.out.println("insertSecondaryDriverSettlementDetails value is = " + insertSecondaryDriverSettlement);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertSecondaryDriverSettlementDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertSecondaryDriverSettlementDetails", sqlException);
        }

        return insertSecondaryDriverSettlement;
    }

    public ArrayList getSecondaryDriverSettlementDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList settlementDetails = new ArrayList();
        map.put("fromDate", operationTO.getFromDate());
        map.put("toDate", operationTO.getToDate());
        map.put("driverId", operationTO.getPrimaryDriverId());
        map.put("settlementId", operationTO.getTripSettlementId());
        System.out.println("map = " + map);
        try {
            System.out.println("this is getSecondaryDriverSettlementDetails");
            settlementDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryDriverSettlementDetails", map);
            System.out.println(" getSecondaryDriverSettlementDetails =" + settlementDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverSettlementDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverSettlementDetails List", sqlException);
        }

        return settlementDetails;
    }

    public ArrayList getSecondaryDriverTripSettlementDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList settlementDetails = new ArrayList();
        map.put("settlementId", operationTO.getTripSettlementId());
        System.out.println("map = " + map);
        try {
            System.out.println("this is getSecondaryDriverTripSettlementDetails");
            settlementDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryDriverTripSettlementDetails", map);
            System.out.println(" getSecondaryDriverTripSettlementDetails =" + settlementDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverSettlementDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverSettlementDetails List", sqlException);
        }

        return settlementDetails;
    }

    public int updateContractMaster(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        int response = 0;
        try {
            map.put("contractId", operationTO.getContractId());
            map.put("customerId", operationTO.getCustomerId());
            map.put("fromDate", operationTO.getRouteValidFrom());
            map.put("toDate", operationTO.getRouteValidTo());
            map.put("extraKmCalculation", operationTO.getExtraKmCalculation());
            map.put("contractCngCost", operationTO.getContractCngCost());
            map.put("contractDieselCost", operationTO.getContractDieselCost());
            map.put("rateChangeOfCng", operationTO.getRateChangeOfCng());
            map.put("rateChangeOfDiesel", operationTO.getRateChangeOfDiesel());
            map.put("userId", operationTO.getUserId());
            System.out.println("map = " + map);
            response = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateContractMaster", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractMaster", sqlException);
        }

        return response;
    }

    public int updateContractDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        int response = 0;
        try {

            map.put("contractId", operationTO.getContractId());
            map.put("id", operationTO.getId());
            map.put("vehicleTypeId", operationTO.getVehTypeId());
            map.put("noOfVehicle", operationTO.getNoOfVehicle());
            map.put("fixedKm", operationTO.getFixedKm());
            map.put("fixedKmCharge", operationTO.getFixedKmCharge());
            map.put("extraKmCharge", operationTO.getExtraKmCharge());
            map.put("userId", operationTO.getUserId());
            System.out.println("map = " + map);
            response = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateContractDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractDetails", sqlException);
        }

        return response;
    }

    public int updateHumanResourceDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        int response = 0;
        try {

            map.put("pointId", operationTO.getPointId());
            map.put("contractId", operationTO.getContractId());
            map.put("hrId", operationTO.getHrId());
            map.put("noOfPersons", operationTO.getNoOfPersons());
            map.put("fixedAmount", operationTO.getFixedAmount());
            map.put("userId", operationTO.getUserId());
            System.out.println("map = " + map);
            response = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateHumanResourceDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertHumanResourceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertHumanResourceDetails", sqlException);
        }

        return response;
    }

    public String getCngPrice() {
        Map map = new HashMap();
        String cngPrice = "";
        try {
            cngPrice = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getCngPrice", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCngPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCngPrice", sqlException);
        }

        return cngPrice;
    }

    public String getCustomer(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        String customerDetails = "";
        try {
            map.put("customerId", operationTO.getCustomerId());
            customerDetails = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getCustomer", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomer Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomer", sqlException);
        }

        return customerDetails;
    }

    public boolean checkPointName(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        boolean checkStatus = false;
        String pointName = "";
        try {
            map.put("pointName", operationTO.getPointName());
            map.put("customerId", operationTO.getCustomerId());
            System.out.println("map = " + map);
            pointName = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.checkPointName", map);
            if (pointName != null) {
                checkStatus = true;
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomer Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomer", sqlException);
        }

        return checkStatus;
    }

    public boolean checkRouteName(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        boolean checkStatus = false;
        String pointName = "";
        try {
            map.put("routeName", operationTO.getRouteName());
            map.put("customerId", operationTO.getCustomerId());
            System.out.println("map = " + map);
            pointName = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.checkRouteName", map);
            if (pointName != null) {
                checkStatus = true;
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomer Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomer", sqlException);
        }

        return checkStatus;
    }

    public ArrayList getMileageConfigList(String customerId) {
        Map map = new HashMap();
        map.put("customerId", customerId);
        ArrayList mileageConfigList = new ArrayList();
        try {
            mileageConfigList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getMilleageConfigList", map);
            System.out.println("mileageConfigList " + mileageConfigList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMileageConfigList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getMileageConfigList List", sqlException);
        }

        return mileageConfigList;
    }

    public ArrayList getSecondaryCustomerList() {
        Map map = new HashMap();
        ArrayList secondaryCustomerList = new ArrayList();
        try {
            secondaryCustomerList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryCustomerList", map);
            System.out.println("mileageConfigList " + secondaryCustomerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryCustomerList List", sqlException);
        }

        return secondaryCustomerList;
    }

    public ArrayList getSecondaryTripNotSettledList(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList secondaryTripNotSettledList = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        map.put("billTimePeriod", operationTO.getBilledMonth() + "-" + operationTO.getBillYear());
        map.put("vehicleTypeId", operationTO.getVehicleTypeId());
        try {
            System.out.println("map in the secondary controller = " + map);
            secondaryTripNotSettledList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryTripNotSettledList", map);
            System.out.println("secondaryTripNotSettledList " + secondaryTripNotSettledList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryTripNotSettledList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryTripNotSettledList List", sqlException);
        }

        return secondaryTripNotSettledList;
    }

    public ArrayList getSecondaryBillingList(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList secondaryBillingList = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        map.put("billTimePeriod", operationTO.getBilledMonth() + "-" + operationTO.getBillYear());
        map.put("vehicleTypeId", operationTO.getVehicleTypeId());
        try {
            System.out.println("map in the secondary controller = " + map);
            secondaryBillingList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryBillingList", map);
            System.out.println("secondaryBillingList " + secondaryBillingList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryBillingList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryBillingList List", sqlException);
        }

        return secondaryBillingList;
    }

    public ArrayList getSecondaryBillingTripList(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList secondaryBillingTripList = new ArrayList();
        map.put("vehicleTypeId", operationTO.getVehicleTypeId());
        map.put("customerId", operationTO.getCustomerId());
        map.put("billTimePeriod", operationTO.getBilledMonth() + "-" + operationTO.getBillYear());
        try {
            System.out.println("map in the  trip bill list = " + map);
            secondaryBillingTripList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryBillingTripList", map);
            System.out.println("secondaryBillingList " + secondaryBillingTripList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryBillingTripList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryBillingTripList List", sqlException);
        }

        return secondaryBillingTripList;
    }

    public String checkTripClosure(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        String checkTripClosure = "";
        try {
            map.put("customerId", operationTO.getCustomerId());
            map.put("billTimePeriod", operationTO.getBilledMonth() + "-" + operationTO.getBillYear());
            System.out.println("map = " + map);
            checkTripClosure = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.checkTripClosure", map);
            System.out.println("checkTripClosure = " + checkTripClosure);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkTripClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkTripClosure", sqlException);
        }

        return checkTripClosure;
    }

    public String getVehicleTypeFuelDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        String vehicleTypeFuelDetails = "";
        try {
            map.put("vehicleTypeId", operationTO.getVehicleTypeId());
            map.put("customerId", operationTO.getCustomerId());
            map.put("billTimePeriod", operationTO.getBilledMonth() + "-" + operationTO.getBillYear());
            System.out.println("map = " + map);
            vehicleTypeFuelDetails = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getVehicleTypeFuelDetails", map);
            System.out.println("checkTripClosure = " + vehicleTypeFuelDetails);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleTypeFuelDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleTypeFuelDetails", sqlException);
        }

        return vehicleTypeFuelDetails;
    }

    public String getVehicleTypeCngDeviation(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        String vehicleTypeCngDeviation = "";
        try {
            map.put("vehicleTypeId", operationTO.getVehicleTypeId());
            map.put("fuelTypeId", operationTO.getFuelTypeId());
            map.put("fuelPrice", operationTO.getFuelPrice());
            map.put("customerId", operationTO.getCustomerId());
            map.put("billTimePeriod", operationTO.getBilledMonth() + "-" + operationTO.getBillYear());
            System.out.println("map = " + map);
            if (operationTO.getFuelTypeId().equals("1002")) {
                vehicleTypeCngDeviation = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getVehicleTypeDieselDeviation", map);
            } else if (operationTO.getFuelTypeId().equals("1003")) {
                vehicleTypeCngDeviation = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getVehicleTypeCngDeviation", map);
            }
            System.out.println("vehicleTypeCngDeviation = " + vehicleTypeCngDeviation);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleTypeCngDeviation Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleTypeCngDeviation", sqlException);
        }

        return vehicleTypeCngDeviation;
    }

    public String getContractFuelAmount(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        String contractFuelAmount = "";
        try {
            map.put("vehicleTypeId", operationTO.getVehicleTypeId());
            map.put("fuelTypeId", operationTO.getFuelTypeId());
            map.put("fuelPrice", operationTO.getFuelPrice());
            map.put("customerId", operationTO.getCustomerId());
            map.put("billTimePeriod", operationTO.getBilledMonth() + "-" + operationTO.getBillYear());
            System.out.println("map = " + map);
            if (operationTO.getFuelTypeId().equals("1002")) {
                contractFuelAmount = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getContractDieselAmount", map);
            } else if (operationTO.getFuelTypeId().equals("1003")) {
                contractFuelAmount = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getContractCngAmount", map);
            }
            System.out.println("contractFuelAmount = " + contractFuelAmount);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractFuelAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getContractFuelAmount", sqlException);
        }

        return contractFuelAmount;
    }

    public ArrayList getSecondaryDriverName(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList driverName = new ArrayList();
        map.put("driverName", operationTO.getDriverName() + "%");
        try {
            if (getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryDriverName", map) != null) {
                driverName = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryDriverName", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", sqlException);
        }
        return driverName;
    }

    public ArrayList getPrimaryDriverName(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList driverName = new ArrayList();
        map.put("driverName", operationTO.getDriverName() + "%");
        try {
            if (getSqlMapClientTemplate().queryForList("secondaryOperation.getPrimaryDriverName", map) != null) {
                driverName = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getPrimaryDriverName", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPrimaryDriverName Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPrimaryDriverName", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPrimaryDriverName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPrimaryDriverName", sqlException);
        }
        return driverName;
    }

    public ArrayList getSecondaryFleet(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList fleetName = new ArrayList();

        try {

            fleetName = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryFleet", map);

        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", sqlException);
        }
        return fleetName;
    }

    public ArrayList getSecondaryDriverNameForSettelment(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        map.put("companyName", operationTO.getCompanyName());
        ArrayList driverName = new ArrayList();

        System.out.println("map for driver" + map);

        try {

            driverName = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryDriverNameforSettelment", map);

        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", sqlException);
        }
        return driverName;
    }

    public String getSecondaryRouteApprovalPerson(String customerId) {
        Map map = new HashMap();
        map.put("customerId", customerId);

        System.out.println("map = " + map);
        try {
            customerId = (String) getSqlMapClientTemplate().queryForObject("operation.getSecondaryRouteApprovalPerson", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryApprovalPerson Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getSecondaryApprovalPerson", sqlException);
        }
        return customerId;

    }

    public String getSecRouteApprovalBy(String routeName) {
        Map map = new HashMap();
        map.put("routeName", routeName);

        // ArrayList driverName = new ArrayList();
        String status = "";
        System.out.println("map for RouteApproval:" + map);

        try {

            status = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getSecRouteApprovalBy", map);

        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", sqlException);
        }
        return status;
    }

    public int UpdateSecondaryRouteApproval(String routeName, String mailId) {
        Map map = new HashMap();
        map.put("routeName", routeName);
        map.put("mailId", mailId);
        // ArrayList driverName = new ArrayList();
        int status = 0;
        System.out.println("map for RouteApproval:" + map);

        try {

            status = (Integer) getSqlMapClientTemplate().update("secondaryOperation.UpdateSecondaryRouteApproval", map);

        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", sqlException);
        }
        return status;
    }

    public int UpdateSecondaryRouteRejection(String routeName, String mailId) {
        Map map = new HashMap();
        map.put("routeName", routeName);
        map.put("mailId", mailId);
        // ArrayList driverName = new ArrayList();
        int status = 0;
        System.out.println("map for Route Rejection:" + map);

        try {

            status = (Integer) getSqlMapClientTemplate().update("secondaryOperation.UpdateSecondaryRouteRejection", map);

        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", sqlException);
        }
        return status;
    }

    public int saveBill(SecondaryOperationTO operationTO, String customerName, String invoiceCode, String invoiceNo, String invoiceDate, String invoiceAmount, String grandTotal, String invoiceStatus, String payAmount) {
        Map map = new HashMap();
        int response = 0;
        try {

//            map.put("contractId", userId);
//            map.put("hrId", operationTO.getHrId());
//            map.put("noOfPersons", operationTO.getNoOfPersons());
//            map.put("fixedAmount", operationTO.getFixedAmount());
//            map.put("customerName", operationTO.getCustomerName());
            map.put("customerName", customerName);
            map.put("invoiceCode", invoiceCode);
            map.put("invoiceNo", invoiceNo);
            map.put("invoiceDate", invoiceDate);
            map.put("invoiceAmount", invoiceAmount);
            map.put("grandTotal", grandTotal);
            map.put("invoiceStatus", invoiceStatus);
            map.put("payAmount", payAmount);
//            map.put("invoiceCode", operationTO.getInvoiceCode());
//            map.put("invoiceNo", operationTO.getInvoiceNo());
//            map.put("invoiceDate", operationTO.getInvoiceDate());
//            map.put("invoiceAmount", operationTO.getInvoiceAmount());
//            map.put("grandTotal", operationTO.getGrandTotal());
//            map.put("invoiceStatus", operationTO.getInvoiceStatus());
//            map.put("payAmount", operationTO.getPayAmount());
            System.out.println("map = " + map);
            response = (Integer) getSqlMapClientTemplate().update("secondaryOperation.saveBill", map);
//             response = (Integer) getSqlMapClientTemplate().insert("operation.insertConsignmentNote", map);
            System.out.println("response = " + response);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertHumanResourceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertHumanResourceDetails", sqlException);
        }

        return response;
    }

    public double saveFuelPriceMasterLog(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int insertAvgFuelPrice = 0;
        String[] temp = null;

        try {

            String[] fuelprice = operationTO.getFuelPrice().split("~");
            String[] fueldetail = operationTO.getFuelType().split("~");

            map.put("userId", userId);
            map.put("FuelType", "1002");
            map.put("fuelUnite", "Litre");
            map.put("startDate", operationTO.getEffectiveDate());

            map.put("CHN", fuelprice[0]);
            map.put("MUM", fuelprice[1]);
            map.put("DEL", fuelprice[2]);
            map.put("KOL", fuelprice[3]);
            map.put("month", fuelprice[4]);
            map.put("Year", fuelprice[5]);

            map.put("FuelPrice", fueldetail[0]);
            map.put("average_price", fueldetail[1]);
            map.put("increase_price", fueldetail[2]);
            map.put("esc_per", fueldetail[3]);
            map.put("esc_price", fueldetail[4]);
            map.put("status", operationTO.getStatus());

            System.out.println("map: " + map);

            insertAvgFuelPrice = (Integer) getSqlMapClientTemplate().update("operation.saveAvgFuelPriceLog", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveFuelPriceMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveFuelPriceMaster ", sqlException);
        }
        return insertAvgFuelPrice;
    }

    public double saveFuelPriceMaster(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int insertAvgFuelPrice = 0;
        String[] temp = null;

        try {

            Calendar cal = Calendar.getInstance();
            String currYear = String.valueOf(cal.get(cal.YEAR));
            String currMonth = String.valueOf(cal.get(cal.MONTH) + 1); //zero-based
            System.out.println("cuentyear = " + currYear + "\nmonth = " + currMonth);

            String[] fuelprice = operationTO.getFuelPrice().split("~");
            String[] fueldetail = operationTO.getFuelType().split("~");

            map.put("userId", userId);
            map.put("FuelType", "1002");
            map.put("fuelUnite", "Litre");
            map.put("startDate", operationTO.getEffectiveDate());

            map.put("CHN", fuelprice[0]);
            map.put("MUM", fuelprice[1]);
            map.put("DEL", fuelprice[2]);
            map.put("KOL", fuelprice[3]);
            map.put("month", fuelprice[4]);
            map.put("Year", fuelprice[5]);

            map.put("FuelPrice", fueldetail[0]);
            map.put("average_price", fueldetail[1]);
            map.put("increase_price", fueldetail[2]);
            map.put("esc_per", fueldetail[3]);
            map.put("esc_price", fueldetail[4]);

            System.out.println("map: " + map);

            int updateFuelPrice = getSqlMapClientTemplate().update("operation.updateAvgFuelPrice", map);
            System.out.println("updateFuelPrice : " + updateFuelPrice);

            insertAvgFuelPrice = (Integer) getSqlMapClientTemplate().update("operation.saveAvgFuelPrice", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveFuelPriceMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveFuelPriceMaster ", sqlException);
        }
        return insertAvgFuelPrice;
    }

    public int deleteFuelPriceMasterLog() {
        Map map = new HashMap();
        int response = 0;
        try {

            response = (Integer) getSqlMapClientTemplate().update("operation.deleteFuelPriceMasterLog", map);
            System.out.println("response = " + response);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("deleteFuelPriceMasterLog Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "deleteFuelPriceMasterLog", sqlException);
        }

        return response;
    }

    public ArrayList getFuelUplodedLogDetails(int userId, String fuelMonth, String fuelYear) {
        Map map = new HashMap();
        ArrayList FuelUplodedLogDetails = new ArrayList();

        try {
            map.put("userId", userId);
            map.put("fuelMonth", fuelMonth);
            map.put("fuelYear", fuelYear);
            FuelUplodedLogDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getFuelUplodedLogDetails", map);

        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("FuelUplodedLogDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "FuelUplodedLogDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("FuelUplodedLogDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "FuelUplodedLogDetails", sqlException);
        }
        return FuelUplodedLogDetails;
    }

    public int updateFuelUplodedDetails(int userId, String fuelMonth, String fuelYear) {
        Map map = new HashMap();
        int response = 0;

        double chenTotal = 0;
        double kolTotal = 0;
        double delhiTotal = 0;
        double mumTotal = 0;
        double ovrTotal = 0;
        double avgTotal = 0;
        double incrTotal = 0;
        double escTotal = 0;
        String fuelType = "";
        String fuelTypeUnite = "";
        String fuelDate = "";
        String fuelPrice = "";
        String fuelPercent = "";

        try {
            map.put("userId", userId);
            map.put("month", fuelMonth);
            map.put("Year", fuelYear);

            int updateFuelPrice = getSqlMapClientTemplate().update("operation.updateAvgFuelPrice", map);
            System.out.println("updateFuelPrice : " + updateFuelPrice);
            
            int updateFuelPricedet = getSqlMapClientTemplate().update("operation.updateAvgFuelPriceDetails", map);

            map.put("fuelMonth", fuelMonth);
            map.put("fuelYear", fuelYear);

            ArrayList FuelUplodedLogDetails = new ArrayList();
            FuelUplodedLogDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getFuelUplodedLogDetails", map);
            int count=0;
            Iterator itr = FuelUplodedLogDetails.iterator();
            OperationTO operationTO;
            while (itr.hasNext()) {
                operationTO = new OperationTO();
                operationTO = (OperationTO) itr.next();
                count++;
                map.put("userId", userId);
                map.put("FuelType", operationTO.getFuelTypeId());
                map.put("fuelUnite", operationTO.getFuelUnite());
                map.put("startDate", operationTO.getEffectiveDate());

                map.put("CHN", operationTO.getChennaiPrice());
                map.put("MUM", operationTO.getMumbaiPrice());
                map.put("DEL", operationTO.getDelhiPrice());
                map.put("KOL", operationTO.getKolkataPrice());
                map.put("month", fuelMonth);
                map.put("Year", fuelYear);

                map.put("FuelPrice", operationTO.getFuelPrice());
                map.put("average_price", operationTO.getAveragePrice());
                map.put("increase_price", operationTO.getIncreasePrice());
                map.put("esc_price", operationTO.getEscalationPrice());
                map.put("esc_per", operationTO.getEscalationPercent());

                System.out.println("map: " + map);

                int status = (Integer) getSqlMapClientTemplate().update("operation.saveAvgFuelPriceDetails", map);
                System.out.println("status : " + status);

                chenTotal += Double.parseDouble(operationTO.getChennaiPrice());
                kolTotal += Double.parseDouble(operationTO.getKolkataPrice());
                delhiTotal += Double.parseDouble(operationTO.getDelhiPrice());
                mumTotal += Double.parseDouble(operationTO.getMumbaiPrice());

                fuelType = operationTO.getFuelTypeId();
                fuelTypeUnite = operationTO.getFuelUnite();
                fuelDate = operationTO.getEffectiveDate();
                fuelPrice = operationTO.getFuelPrice();
                fuelPercent = operationTO.getEscalationPercent();
            }
            
            chenTotal = chenTotal/count;
            mumTotal = mumTotal/count;
            delhiTotal = delhiTotal/count;
            kolTotal = kolTotal/count;

            map.put("userId", userId);
            map.put("FuelType", fuelType);
            map.put("fuelUnite", fuelTypeUnite);
            map.put("startDate", fuelDate);
            map.put("CHN", chenTotal);
            map.put("MUM", mumTotal);
            map.put("DEL", delhiTotal);
            map.put("KOL", kolTotal);
            map.put("month", fuelMonth);
            map.put("Year", fuelYear);
            map.put("FuelPrice", fuelPrice);
            map.put("esc_per", fuelPercent);

            ovrTotal = chenTotal + mumTotal + delhiTotal + kolTotal;
            avgTotal = ovrTotal / 4;
            incrTotal = avgTotal - Double.parseDouble(fuelPrice);
            escTotal = incrTotal * Double.parseDouble(fuelPercent);

            map.put("average_price", avgTotal);
            map.put("increase_price", incrTotal);
            map.put("esc_price", escTotal);

            System.out.println("map: " + map);

            int status = (Integer) getSqlMapClientTemplate().update("operation.saveAvgFuelPrice", map);
            System.out.println("status : " + status);

            response = (Integer) getSqlMapClientTemplate().update("operation.deleteFuelPriceMasterLog", map);
            System.out.println("response = " + response);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("deleteFuelPriceMasterLog Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "deleteFuelPriceMasterLog", sqlException);
        }

        return response;
    }

}
