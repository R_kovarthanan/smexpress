/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ets.domain.tyres.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPRuntimeException;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import ets.domain.tyres.business.TyresTO;


/**
 *
 * @author kannan
 */
public class TyresDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "TyresDAO";

    public TyresDAO() {
    }

    
    public ArrayList getVehiclesList() {
        Map map = new HashMap();
        ArrayList vehiclesList = new ArrayList();
        int status = 0;

        try {
             vehiclesList = (ArrayList) getSqlMapClientTemplate().queryForList("tyres.getVehiclesList", map);             
         }  catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehiclesList", sqlException);
        }
        return vehiclesList;
    }

    public ArrayList getMfrList() {
        Map map = new HashMap();
        ArrayList mfrList = new ArrayList();
        int status = 0;

        try {
             mfrList = (ArrayList) getSqlMapClientTemplate().queryForList("tyres.getMfrList", map);
             System.out.println("mfrList in tyres"+mfrList);
         }  catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getMfrList", sqlException);
        }
        return mfrList;
    }


    public ArrayList getVehicleDetails(TyresTO tyresTo) {
        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        int status = 0;
        //map.put("Name", tyre.getName());
        try {
             vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("tyres.getVehiclesDetailList", map);
             System.out.println("getVehicleDetails size -->"+vehicleList.size());
         }  catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleDetails", sqlException);
        }
        return vehicleList;
    }
    
    public ArrayList getTyresReportByTyre(TyresTO tyresTo) {
        Map map = new HashMap();
        ArrayList tyreDetails = new ArrayList();
        int status = 0;
        map.put("vehicleNumber",tyresTo.getVehicleRegNo());
        map.put("tyreNumber",tyresTo.getTyreNumber());
        String fromDate = tyresTo.getFromDate();
        String toDate = tyresTo.getToDate();
        String fromDateSQL = "";
        String toDateSQL = "";
        if(fromDate != null && fromDate.contains("-")){
            fromDateSQL = fromDate.split("-")[2]+"-"+fromDate.split("-")[1]+"-"+fromDate.split("-")[0];
        }
        if(toDate != null && toDate.contains("-")){
            toDateSQL = toDate.split("-")[2]+"-"+toDate.split("-")[1]+"-"+toDate.split("-")[0];
        }


        map.put("fromDate",fromDateSQL);
        map.put("toDate",toDateSQL);
        System.out.println("map in Tyres Report-->"+map);
        try {
            String vehicleNumber = tyresTo.getVehicleRegNo();
            String tyreNumber = tyresTo.getTyreNumber();
            if((vehicleNumber.length() > 0) || (tyreNumber.length() > 0)) {
                 tyreDetails = (ArrayList) getSqlMapClientTemplate().queryForList("tyres.getTyresDetailByTyre", map);                 
            }
             System.out.println("tyreDetails size -->"+tyreDetails.size());
         }  catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTyresReportByTyre", sqlException);
        }
        return tyreDetails;
    }
        public String getTyreSuggests(String tyreNo) {
        Map map = new HashMap();
        map.put("tyreNo", tyreNo);
        
        String suggestions = "";
        TyresTO tyreTO = new TyresTO();
        
        try {
            ArrayList getItemList = new ArrayList();
            getItemList = (ArrayList) getSqlMapClientTemplate().queryForList("tyres.getTyreNos", map);
            Iterator itr = getItemList.iterator();
            while (itr.hasNext()) {
                tyreTO = new TyresTO();
                tyreTO = (TyresTO) itr.next();
                suggestions = tyreTO.getTyreNumber() + "~" + suggestions;
            }
            if("".equals(suggestions)){
                suggestions="no match found";
            }
        } catch (Exception sqlException) {
            
            FPLogUtils.fpDebugLog("getTyreSuggests Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTyreSuggests", sqlException);
        }
        
        return suggestions;
    }
    public ArrayList getTyresReportByVehicle(TyresTO tyresTo) {
        Map map = new HashMap();
        ArrayList tyreDetails = new ArrayList();
        int status = 0;
        map.put("vehicleNumber",tyresTo.getVehicleRegNo());      
        String fromDate = tyresTo.getFromDate();
        String toDate = tyresTo.getToDate();
        String fromDateSQL = "";
        String toDateSQL = "";
        if(fromDate != null && fromDate.contains("-")){
            fromDateSQL = fromDate.split("-")[2]+"-"+fromDate.split("-")[1]+"-"+fromDate.split("-")[0];
        }
        if(toDate != null && toDate.contains("-")){
            toDateSQL = toDate.split("-")[2]+"-"+toDate.split("-")[1]+"-"+toDate.split("-")[0];
        }
        map.put("fromDate",fromDateSQL);
        map.put("toDate",toDateSQL);
        System.out.println("map in Tyres Report-->"+map);
        try {
            String vehicleNumber = tyresTo.getVehicleRegNo();
            String tyreNumber = tyresTo.getTyreNumber();
            if((vehicleNumber.length() > 0) || (tyreNumber.length() > 0)) {
                 tyreDetails = (ArrayList) getSqlMapClientTemplate().queryForList("tyres.getTyresDetailByVehicle", map);
            }
             System.out.println("tyreDetails size -->"+tyreDetails.size());
         }  catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTyresReportByTyre", sqlException);
        }
        return tyreDetails;
    }

    public ArrayList getVehicleDetailsList(TyresTO tyresTo) {
        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        int status = 0;
        map.put("vehicleRegNo",tyresTo.getVehicleRegNo());
        map.put("vehicleId",tyresTo.getVehicleId());
        map.put("mfrId",tyresTo.getMfrId());
        String fromDate = tyresTo.getFromDate();
        String toDate = tyresTo.getToDate();
        String fromDateSQL = "";
        String toDateSQL = "";
        if(fromDate != null && fromDate.contains("-")){
            fromDateSQL = fromDate.split("-")[2]+"-"+fromDate.split("-")[1]+"-"+fromDate.split("-")[0];
        }
        if(toDate != null && toDate.contains("-")){
            toDateSQL = toDate.split("-")[2]+"-"+toDate.split("-")[1]+"-"+toDate.split("-")[0];
        }
        map.put("fromDate",fromDateSQL);
        map.put("toDate",toDateSQL);
        System.out.println("map in vehicle Tyre Details -->"+map);
        try {
             vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("tyres.getVehicleDetailsList", map);
             System.out.println("getVehicleDetails size -->"+vehicleList.size());
         }  catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return vehicleList;
    }

    public ArrayList getTyresList(TyresTO tyresTo) {
        Map map = new HashMap();
        ArrayList tyresList = new ArrayList();
        int status = 0;
        map.put("vehicleId",tyresTo.getVehicleId());
        try {
             tyresList = (ArrayList) getSqlMapClientTemplate().queryForList("tyres.getTyresList", map);            
         }  catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return tyresList;
    }

    public ArrayList getTyreRotation(TyresTO tyresTo) {
        Map map = new HashMap();
        ArrayList tyresList = new ArrayList();
        int status = 0;
        map.put("vehicleId",tyresTo.getVehicleId());
        try {
             tyresList = (ArrayList) getSqlMapClientTemplate().queryForList("tyres.getTyreRotation", map);
         }  catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return tyresList;
    }

    public ArrayList getCurrentTyrePosition(TyresTO tyresTo) {
        Map map = new HashMap();
        ArrayList currentTyrePosition = new ArrayList();
        int status = 0;
        map.put("vehicleId",tyresTo.getVehicleId());
        try {
             System.out.println("currentTyrePosition map ="+currentTyrePosition);
             currentTyrePosition = (ArrayList) getSqlMapClientTemplate().queryForList("tyres.getCurrentTyrePosition", map);
             System.out.println("currentTyrePosition "+currentTyrePosition.size());
         }  catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCurrentTyrePosition", sqlException);
        }
        return currentTyrePosition;
    }

    public int saveTyreRotation(TyresTO tyresTo) {
        Map map = new HashMap();
        ArrayList currentTyrePosition = new ArrayList();
        int rotationId = 0;
        int count = 0;
        map.put("vehicleId",tyresTo.getVehicleId());
        map.put("tyreRotationNo",tyresTo.getTyreRotationNo());
        map.put("tyreRotationDate",tyresTo.getTyreRotationDate());
        map.put("tyreRotationKm",tyresTo.getTyreRotationKm());
        map.put("tyreRotationRemarks",tyresTo.getTyreRotationRemarks());
        
        try {
             System.out.println("currentTyrePosition map ="+map);
             count =  (Integer) getSqlMapClientTemplate().queryForObject("tyres.getTyreRotationCount", map);
             if(count == 0) {
                 rotationId = (Integer) getSqlMapClientTemplate().insert("tyres.insertTyreRotation", map);
            }
         }  catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "saveTyreRotation", sqlException);
        }
        return rotationId;
    }

    public int saveTyreRotationPosition(int rotationId,String vehicleId,String tyreId,String positionId,int userId) {
        Map map = new HashMap();
        int status = 0;
        int count = 0;
        map.put("vehicleId",vehicleId);
        map.put("rotationId",rotationId);
        map.put("tyreId",tyreId);
        map.put("positionId",positionId);
        map.put("userId",userId);
        try {
             status = (Integer) getSqlMapClientTemplate().update("tyres.insertTyreRotationPosition", map);
             System.out.println(" tyre rotation status-->"+status);
         }  catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "saveTyreRotationPosition", sqlException);
        }
        return status;
    }


   }

