package ets.domain.customer.business;

/**
 *
 * @author vidya
 *
 */
import com.ibatis.sqlmap.client.SqlMapClient;
import ets.domain.customer.data.CustomerDAO;
import ets.domain.customer.business.CustomerTO;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.operation.business.OperationTO;
import java.util.ArrayList;
import java.util.Iterator;

public class CustomerBP {

    private CustomerDAO customerDAO;

    public CustomerDAO getCustomerDAO() {
        return customerDAO;
    }

    public void setCustomerDAO(CustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }

    public int processInsertCustomer(CustomerTO customerTO, int userId, String[] billingAddressIds, String[] billingNameAddress, String[] branch, String[] stateId, String[] gstNo, String[] activeInd) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = customerDAO.doInsertCustomer(customerTO, userId, billingAddressIds, billingNameAddress, branch ,stateId, gstNo, activeInd);

        return status;
    }

    public ArrayList processCustomerList() throws FPBusinessException, FPRuntimeException {
        ArrayList customerList = new ArrayList();
        customerList = customerDAO.getCustomerList();

        return customerList;
    }

    public ArrayList lpsCustomerList() throws FPBusinessException, FPRuntimeException {
        ArrayList customerList = new ArrayList();
        customerList = customerDAO.getLPSCustomerList();

        return customerList;
    }

    public ArrayList processActiveCustomerList() throws FPBusinessException, FPRuntimeException {
        ArrayList customerList = new ArrayList();
        ArrayList activeCustomerList = new ArrayList();
        customerList = customerDAO.getCustomerList();
        CustomerTO customerTO = null;
        Iterator itr = customerList.iterator();
//        if (customerList.size() == 0) {
//            throw new FPBusinessException("EM-CUST-01");
//        }
        while (itr.hasNext()) {
            customerTO = new CustomerTO();
            customerTO = (CustomerTO) itr.next();
            if (customerTO.getCustStatus().equalsIgnoreCase("y")) {
                activeCustomerList.add(customerTO);
            }
        }
        return activeCustomerList;
    }

    public int processUpdateCustomer(ArrayList List, int userId) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = customerDAO.doUpdateCustomer(List, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CUST-03");
        }
        return status;
    }

    public int processInsertContract(CustomerTO customerTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = customerDAO.doInsertContract(customerTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CONT-02");
        }
        return status;
    }

    public ArrayList processBunkList(String bunkName) throws FPBusinessException, FPRuntimeException {
        ArrayList bunkList = new ArrayList();
        bunkList = customerDAO.getBunkList(bunkName);
        if (bunkList.size() == 0) {
            throw new FPBusinessException("EM-CUST-01");
        }
        return bunkList;
    }

    public int processInsertBunk(CustomerTO customerTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        System.out.println("BP 1");
        status = customerDAO.doInsertBunk(customerTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CUST-02");
        }
        return status;
    }

    public ArrayList processBunkalterList(String bunkId) throws FPBusinessException, FPRuntimeException {
        ArrayList bunkList = new ArrayList();
        bunkList = customerDAO.getBunkalterList(bunkId);
        if (bunkList.size() == 0) {
            throw new FPBusinessException("EM-CUST-01");
        }
        return bunkList;
    }

    public int processBunkUpdateList(CustomerTO customerTO, int userId, String bunkId) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = customerDAO.doUpdateBunk(customerTO, userId, bunkId);
        if (status == 0) {
            throw new FPBusinessException("EM-CUST-01");
        }
        return status;
    }

    public ArrayList processPackingList() throws FPBusinessException, FPRuntimeException {
        ArrayList packList = new ArrayList();
        packList = customerDAO.getPackingList();
        if (packList.size() == 0) {
            throw new FPBusinessException("EM-CUST-01");
        }
        return packList;
    }

    public String getToDestination(String destination) {
        //String destination = "";
        destination = destination + "%";
        destination = customerDAO.getToDestination(destination);
        return destination;
    }

    public ArrayList getBillingTypeList() throws FPBusinessException, FPRuntimeException {
        ArrayList billingTypeList = new ArrayList();
        billingTypeList = customerDAO.getBillingTypeList();
        return billingTypeList;
    }

    public ArrayList getCustomerGroupList() throws FPBusinessException, FPRuntimeException {
        ArrayList result = new ArrayList();
        result = customerDAO.getCustomerGroupList();
        return result;
    }

    public ArrayList getCustomerName(CustomerTO customerTO) throws FPBusinessException, FPRuntimeException {
        ArrayList getCustomerName = new ArrayList();
        getCustomerName = customerDAO.getCustomerName(customerTO);
        return getCustomerName;
    }

    public ArrayList getCustomerCodes(CustomerTO customerTO) throws FPBusinessException, FPRuntimeException {
        ArrayList getCustomerCodes = new ArrayList();
        getCustomerCodes = customerDAO.getCustomerCodes(customerTO);
        return getCustomerCodes;
    }

    public String getCustomerCode(CustomerTO customerTO, int insertContractRouteMater) {
        String custCode = "";
        custCode = customerDAO.getCustomerCode(customerTO, insertContractRouteMater);
        return custCode;
    }

    public int editUpdateCustomer(CustomerTO customerTO, int userId, String[] billingAddressIds, String[] billingNameAddress, String[] branch , String[] stateId, String[] gstNo, String[] activeInd) {
        int status = 0;
        status = customerDAO.updateCustomer(customerTO, userId, billingAddressIds, billingNameAddress, branch, stateId, gstNo, activeInd);
        return status;
    }

    public ArrayList processCustomerLists(CustomerTO customerTO) throws FPBusinessException, FPRuntimeException {
        ArrayList customerList = new ArrayList();
        customerList = customerDAO.getCustomerLists(customerTO);

        return customerList;
    }

    public ArrayList getCustomerDetails(CustomerTO customerTO) throws FPBusinessException, FPRuntimeException {
        ArrayList customerList = new ArrayList();
        customerList = customerDAO.getCustomerDetails(customerTO);

        return customerList;
    }

    public int processBranchUpdate(CustomerTO customerTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = customerDAO.updateBranchDetails(customerTO, userId);
        return status;
    }

    public ArrayList processBranchList(CustomerTO customerTO) throws FPBusinessException, FPRuntimeException {
        ArrayList branchList = new ArrayList();
        branchList = customerDAO.processBranchList(customerTO);
        return branchList;
    }

    public int processBranchInsert(CustomerTO customerTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        SqlMapClient session = customerDAO.getSqlMapClient();
        try {
            session.startTransaction();
            status = customerDAO.insertBranchDetails(customerTO, userId, session);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }

    public ArrayList getBranchView(CustomerTO customerTO) throws FPRuntimeException, FPBusinessException {
        ArrayList branch = new ArrayList();
        branch = customerDAO.getBranchView(customerTO);
        return branch;
    }

    public int processConsigneeUpdate(CustomerTO customerTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = customerDAO.updateConsigneeDetails(customerTO, userId);
        return status;
    }

    /**
     * This method used to Get The Consignee List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList processContractConsigneeList(CustomerTO customerTO, String custId) throws FPBusinessException, FPRuntimeException {
        ArrayList ConsigneeList = new ArrayList();
        ConsigneeList = customerDAO.processContractConsigneeList(customerTO, custId);
        return ConsigneeList;
    }

    /**
     * This method used to Insert The Consignee.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int processConsigneeInsert(CustomerTO customerTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        SqlMapClient session = customerDAO.getSqlMapClient();
        try {
            session.startTransaction();
            status = customerDAO.insertConsigneeDetails(customerTO, userId, session);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }

    public int getcheckConsigneeName(String consignName, String customerId) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = customerDAO.getcheckConsigneeName(consignName, customerId);
        return status;
    }

    /**
     * This method used to Get The City List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList processCityList() throws FPBusinessException, FPRuntimeException {
        ArrayList cityList = new ArrayList();
        cityList = customerDAO.getCityList();
        return cityList;
    }

    //consignor
    public int processConsignorUpdate(CustomerTO customerTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = customerDAO.updateConsignorDetails(customerTO, userId);
        return status;
    }

    public ArrayList processContractConsignorList(CustomerTO customerTO, String custId) throws FPBusinessException, FPRuntimeException {
        ArrayList consignorList = new ArrayList();
        consignorList = customerDAO.processContractConsignorList(customerTO, custId);
        return consignorList;
    }

    /**
     * This method used to Get The CustomerContract List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList processCustomerContractList() throws FPBusinessException, FPRuntimeException {
        ArrayList customerContractList = new ArrayList();
        customerContractList = customerDAO.getCustomerContractList();
        return customerContractList;
    }

    /**
     * This method used to Insert The Consignor.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int processConsignorInsert(CustomerTO customerTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        SqlMapClient session = customerDAO.getSqlMapClient();
        try {
            session.startTransaction();
            status = customerDAO.insertConsignorDetails(customerTO, userId, session);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }

    public int getcheckConsignorName(String consignName, String customerId) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = customerDAO.getcheckConsignorName(consignName, customerId);
        return status;
    }

    public String processStateName(CustomerTO customerTO) throws FPRuntimeException, FPBusinessException {
        String stateName = "";
        stateName = customerDAO.getStateName(customerTO);
        return stateName;
    }

    public ArrayList getConsignorView(CustomerTO customerTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consignor = new ArrayList();
        consignor = customerDAO.getConsignorView(customerTO);
        return consignor;
    }

    public ArrayList getConsigneeView(CustomerTO customerTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consignee = new ArrayList();
        consignee = customerDAO.getConsigneeView(customerTO);
        return consignee;
    }

// <GST>
    public ArrayList getStateList() throws FPBusinessException, FPRuntimeException {
        ArrayList getStateList = new ArrayList();
        getStateList = customerDAO.getStateList();
        return getStateList;
    }

    public ArrayList getOrganizationList() throws FPBusinessException, FPRuntimeException {
        ArrayList getOrganizationList = new ArrayList();
        getOrganizationList = customerDAO.getOrganizationList();
        return getOrganizationList;
    }

    public ArrayList getCustomerAddressDetails(CustomerTO customerTO) throws FPBusinessException, FPRuntimeException {
        ArrayList getCustomerAddressDetails = new ArrayList();
        getCustomerAddressDetails = customerDAO.getCustomerAddressDetails(customerTO);

        return getCustomerAddressDetails;
    }

    public ArrayList processBranchListEdit(CustomerTO customerTO, String custId, String branchId) throws FPBusinessException, FPRuntimeException {
        ArrayList branchListEdit = new ArrayList();
        branchListEdit = customerDAO.processBranchListEdit(customerTO, custId, branchId);
        return branchListEdit;
    }

    public int processInsertCustContractFiles(CustomerTO customerTO, int UserId, String[] fileSaved, String[] desc) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = customerDAO.processInsertCustContractFiles(customerTO, UserId, fileSaved, desc);
        return status;
    }

    public ArrayList getCustContractFiles(String customerId) throws FPBusinessException, FPRuntimeException {
        ArrayList customerContractFiles = new ArrayList();
        customerContractFiles = customerDAO.getCustContractFiles(customerId);
        return customerContractFiles;
    }

    public int getRouteCheck(CustomerTO customerTO) throws FPBusinessException, FPRuntimeException {
        int getRouteCheck = 0;
        getRouteCheck = customerDAO.getRouteCheck(customerTO);
        return getRouteCheck;
    }

    public int updateConsignmentContract() {
        int status = 0;
        status = customerDAO.updateConsignmentContract();
        return status;
    }

    public int insertContractUpload(CustomerTO customerTO) {
        int status = 0;
        status = customerDAO.insertContractUpload(customerTO);
        return status;
    }
    
     public ArrayList getContractUploadIncorect(CustomerTO customerTO) throws FPBusinessException, FPRuntimeException {
        ArrayList getConsignmentExcelUploadIncorect = new ArrayList();
        getConsignmentExcelUploadIncorect = customerDAO.getContractUploadIncorect(customerTO);
        return getConsignmentExcelUploadIncorect;
    }

    public ArrayList getContractExcelUpload(CustomerTO customerTO) throws FPBusinessException, FPRuntimeException {
        ArrayList getConsignmentExcelUpload = new ArrayList();
        getConsignmentExcelUpload = customerDAO.getContractExcelUpload(customerTO);
        return getConsignmentExcelUpload;
    }
    
    public int insertCustomerContractExcel(int UserId) {
        int status = 0;
        status = customerDAO.insertCustomerContractExcel(UserId);
        return status;
    }
    public ArrayList getCustomerExportExcelList(CustomerTO customerTO) throws FPBusinessException, FPRuntimeException {
        ArrayList customerList = new ArrayList();
        customerList = customerDAO.getCustomerExportExcelList(customerTO);
    
        return customerList;
}
}
