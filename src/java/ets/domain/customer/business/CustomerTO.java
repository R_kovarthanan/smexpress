package ets.domain.customer.business;

/**
 *
 * @author vidya
 */
public class CustomerTO {
    private String origin="";
    private String vehicleTypeName="";
    private String firstPickupName="";
    private String point1Name="";
    private String point2Name="";
    private String point3Name="";
    private String point4Name="";
    private String finalPointName="";
    private String rateWithReefer="";
    private String rateWithoutReefer="";
    private String tatTime="";
    private String touchPoint1="";
    private String destination="";
    private String vehicleType="";
    private String totalKM="";
    private String tat="";
    private String counts="";
    private String flag="";
    private String id="";
    private String contractAmount = "";
    private String customerName;
    private String travelHr;
    private String travelMin;
    
    private String originId="";
    private String destinationId="";
    private String touchPoint1Id="";
    
    private String docType="";
    private String description="";
    private String path="";
    private String billingAddressId="";
    private String orgId = "";
    private String orgName = "";
    private String organizationId = "";
    private String panNo = "";
    private String ttaCustType = "";
    
    private String detentionAmount = "";
    private String detentionPeriod = "";
    private String billingNameAddress = "";
    private String secondaryRouteStatus = "";
    private String primaryContractStatus;
    private String customerGroup;
    private String customerGroupId;
    private String ledgerId;
    // private String groupName;
    // private String creditLimit;
    private String name;
    private String empName;
    // private String creditDays;
    
    private String displayCustName;
    private String custType;
    private String userId;
    private String roleId;
    private String customerId;
    private String secondaryBillingTypeId;
    private String groupId;
    private String groupName;
    private String secondaryBillingTypeName;
    private String paymentType;
    private int productStatus;
    private int secondaryContractSatus;
    private String contractType;
    private int custId;
    private String creditDays;
    private String creditLimit;
    private String custName;
    private String custContactPerson;
    private String custAddress;
    private String custCity;
    private String custState;
    private String custPhone;
    private String custMobile;
    private String custEmail;
    private String custStatus;
    private String contManufacturerName;
    private String contModelName;
    private String contPercentAgeUpSpares;
    private String contPercentAgeUpLabour;
    private String contStatus;
    private String contFromDate;
    private String contToDate;
    private String customerType;
    private String bunkName;
    private String bunkId;
    private String fuelType;
    private String currRate;
    private String currlocation;
    private String bunkState;
    private String bunkStatus;
    private String remarks;
    private double currentrate;
    private String packingId;
    private String packingName;
    private String toLocation;
    private String routeId;
    private String tancem;
    private String customerCode = "";
    private String contractStatus = "";
    private String billingTypeName = "";
    private String enrollDate = "";
    private String billingTypeId = "";
    private String customerTypeId = "";
    private String customerTypeName = "";
    private String accountManagerId = "";
    //raju
    private String consigneeIdAlt = "";
    private String consigneeNameAlt = "";
    private String customerIdAlt = "";
    private String contactPersonAlt = "";
    private String phoneNoAlt = "";
    private String address1Alt = "";
    private String address2Alt = "";
    private String stateIdAlt = "";
    private String cityIdAlt = "";
    private String emailAlt = "";
    private String activeIndAlt = "";
    private String remarksAlt = "";

    private String consigneeId = "";
    private String consigneeName = "";
    private String consigneName = "";
    private String contactPerson = "";
    private String phoneNo = "";
    private String address1 = "";
    private String address2 = "";
    private String stateId = "";
    private String stateName = "";
    private String cityId = "";
    private String cityName = "";
    private String email = "";
    private String activeInd = "";
    private String pinCode = "";
    private String consigneeCode = "";
    private String customerSiteLocation = "";
    private String wayBillCount = "";
    
    private String consignorId = "";
    private String consignorName = "";
    private String pinCodeAdd = "";
    private String consignorCode = "";
    private String contractId = "";
    private String contractDate = "";
    
    private String contractValidFrom = "";
    private String contractValidTo = "";
    private String billingBranchId = "";
    private String rateType = "";
    private String mobileNo = "";
    private String custContractStatus = "";
    private String custConsignorStatus = "";
    private String custConsigneeStatus = "";
    private String companyType = "";
    private String billingBranchName = "";
    private String zoneName = "";
    private String branchId = "";
    private String branchName = "";
    private String aCName = "";
    private String gstNo = "";
    private String branchCode = "";
    





    public String getBunkId() {
        return bunkId;
    }

    public void setBunkId(String bunkId) {
        this.bunkId = bunkId;
    }

    public String getBunkName() {
        return bunkName;
    }

    public void setBunkName(String bunkName) {
        this.bunkName = bunkName;
    }

    public String getBunkState() {
        return bunkState;
    }

    public void setBunkState(String bunkState) {
        this.bunkState = bunkState;
    }

    public String getBunkStatus() {
        return bunkStatus;
    }

    public void setBunkStatus(String bunkStatus) {
        this.bunkStatus = bunkStatus;
    }

    public String getCurrRate() {
        return currRate;
    }

    public void setCurrRate(String currRate) {
        this.currRate = currRate;
    }

    public double getCurrentrate() {
        return currentrate;
    }

    public void setCurrentrate(double currentrate) {
        this.currentrate = currentrate;
    }

    public String getCurrlocation() {
        return currlocation;
    }

    public void setCurrlocation(String currlocation) {
        this.currlocation = currlocation;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCustAddress() {
        return custAddress;
    }

    public void setCustAddress(String custAddress) {
        this.custAddress = custAddress;
    }

    public String getCustCity() {
        return custCity;
    }

    public void setCustCity(String custCity) {
        this.custCity = custCity;
    }

    public String getCustContactPerson() {
        return custContactPerson;
    }

    public void setCustContactPerson(String custContactPerson) {
        this.custContactPerson = custContactPerson;
    }

    public String getCustEmail() {
        return custEmail;
    }

    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }

    public int getCustId() {
        return custId;
    }

    public void setCustId(int custId) {
        this.custId = custId;
    }

    public String getCustMobile() {
        return custMobile;
    }

    public void setCustMobile(String custMobile) {
        this.custMobile = custMobile;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustPhone() {
        return custPhone;
    }

    public void setCustPhone(String custPhone) {
        this.custPhone = custPhone;
    }

    public String getCustState() {
        return custState;
    }

    public void setCustState(String custState) {
        this.custState = custState;
    }

    public String getCustStatus() {
        return custStatus;
    }

    public void setCustStatus(String custStatus) {
        this.custStatus = custStatus;
    }

    public String getContFromDate() {
        return contFromDate;
    }

    public void setContFromDate(String contFromDate) {
        this.contFromDate = contFromDate;
    }

    public String getContManufacturerName() {
        return contManufacturerName;
    }

    public void setContManufacturerName(String contManufacturerName) {
        this.contManufacturerName = contManufacturerName;
    }

    public String getContModelName() {
        return contModelName;
    }

    public void setContModelName(String contModelName) {
        this.contModelName = contModelName;
    }

    public String getContPercentAgeUpLabour() {
        return contPercentAgeUpLabour;
    }

    public void setContPercentAgeUpLabour(String contPercentAgeUpLabour) {
        this.contPercentAgeUpLabour = contPercentAgeUpLabour;
    }

    public String getContPercentAgeUpSpares() {
        return contPercentAgeUpSpares;
    }

    public void setContPercentAgeUpSpares(String contPercentAgeUpSpares) {
        this.contPercentAgeUpSpares = contPercentAgeUpSpares;
    }

    public String getContStatus() {
        return contStatus;
    }

    public void setContStatus(String contStatus) {
        this.contStatus = contStatus;
    }

    public String getContToDate() {
        return contToDate;
    }

    public void setContToDate(String contToDate) {
        this.contToDate = contToDate;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getPackingId() {
        return packingId;
    }

    public void setPackingId(String packingId) {
        this.packingId = packingId;
    }

    public String getPackingName() {
        return packingName;
    }

    public void setPackingName(String packingName) {
        this.packingName = packingName;
    }

    public String getToLocation() {
        return toLocation;
    }

    public void setToLocation(String toLocation) {
        this.toLocation = toLocation;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getTancem() {
        return tancem;
    }

    public void setTancem(String tancem) {
        this.tancem = tancem;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(String contractStatus) {
        this.contractStatus = contractStatus;
    }

    public String getBillingTypeName() {
        return billingTypeName;
    }

    public void setBillingTypeName(String billingTypeName) {
        this.billingTypeName = billingTypeName;
    }

    public String getEnrollDate() {
        return enrollDate;
    }

    public void setEnrollDate(String enrollDate) {
        this.enrollDate = enrollDate;
    }

    public String getBillingTypeId() {
        return billingTypeId;
    }

    public void setBillingTypeId(String billingTypeId) {
        this.billingTypeId = billingTypeId;
    }

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public String getCustomerTypeName() {
        return customerTypeName;
    }

    public void setCustomerTypeName(String customerTypeName) {
        this.customerTypeName = customerTypeName;
    }

    public String getAccountManagerId() {
        return accountManagerId;
    }

    public void setAccountManagerId(String accountManagerId) {
        this.accountManagerId = accountManagerId;
    }

    public String getCreditDays() {
        return creditDays;
    }

    public void setCreditDays(String creditDays) {
        this.creditDays = creditDays;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public int getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(int productStatus) {
        this.productStatus = productStatus;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getSecondaryBillingTypeId() {
        return secondaryBillingTypeId;
    }

    public void setSecondaryBillingTypeId(String secondaryBillingTypeId) {
        this.secondaryBillingTypeId = secondaryBillingTypeId;
    }

    public String getSecondaryBillingTypeName() {
        return secondaryBillingTypeName;
    }

    public void setSecondaryBillingTypeName(String secondaryBillingTypeName) {
        this.secondaryBillingTypeName = secondaryBillingTypeName;
    }

    public int getSecondaryContractSatus() {
        return secondaryContractSatus;
    }

    public void setSecondaryContractSatus(int secondaryContractSatus) {
        this.secondaryContractSatus = secondaryContractSatus;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getCustomerGroup() {
        return customerGroup;
    }

    public void setCustomerGroup(String customerGroup) {
        this.customerGroup = customerGroup;
    }

    public String getCustomerGroupId() {
        return customerGroupId;
    }

    public void setCustomerGroupId(String customerGroupId) {
        this.customerGroupId = customerGroupId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public String getPrimaryContractStatus() {
        return primaryContractStatus;
    }

    public void setPrimaryContractStatus(String primaryContractStatus) {
        this.primaryContractStatus = primaryContractStatus;
    }

    public String getSecondaryRouteStatus() {
        return secondaryRouteStatus;
    }

    public void setSecondaryRouteStatus(String secondaryRouteStatus) {
        this.secondaryRouteStatus = secondaryRouteStatus;
    }

    public String getBillingNameAddress() {
        return billingNameAddress;
    }

    public void setBillingNameAddress(String billingNameAddress) {
        this.billingNameAddress = billingNameAddress;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLedgerId() {
        return ledgerId;
    }

    public void setLedgerId(String ledgerId) {
        this.ledgerId = ledgerId;
    }

    public String getCustType() {
        return custType;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }

    public String getDetentionAmount() {
        return detentionAmount;
    }

    public void setDetentionAmount(String detentionAmount) {
        this.detentionAmount = detentionAmount;
    }

    public String getDetentionPeriod() {
        return detentionPeriod;
    }

    public void setDetentionPeriod(String detentionPeriod) {
        this.detentionPeriod = detentionPeriod;
    }

    public String getConsigneeIdAlt() {
        return consigneeIdAlt;
    }

    public void setConsigneeIdAlt(String consigneeIdAlt) {
        this.consigneeIdAlt = consigneeIdAlt;
    }

    public String getConsigneeNameAlt() {
        return consigneeNameAlt;
    }

    public void setConsigneeNameAlt(String consigneeNameAlt) {
        this.consigneeNameAlt = consigneeNameAlt;
    }

    public String getCustomerIdAlt() {
        return customerIdAlt;
    }

    public void setCustomerIdAlt(String customerIdAlt) {
        this.customerIdAlt = customerIdAlt;
    }

    public String getContactPersonAlt() {
        return contactPersonAlt;
    }

    public void setContactPersonAlt(String contactPersonAlt) {
        this.contactPersonAlt = contactPersonAlt;
    }

    public String getPhoneNoAlt() {
        return phoneNoAlt;
    }

    public void setPhoneNoAlt(String phoneNoAlt) {
        this.phoneNoAlt = phoneNoAlt;
    }

    public String getAddress1Alt() {
        return address1Alt;
    }

    public void setAddress1Alt(String address1Alt) {
        this.address1Alt = address1Alt;
    }

    public String getAddress2Alt() {
        return address2Alt;
    }

    public void setAddress2Alt(String address2Alt) {
        this.address2Alt = address2Alt;
    }

    public String getStateIdAlt() {
        return stateIdAlt;
    }

    public void setStateIdAlt(String stateIdAlt) {
        this.stateIdAlt = stateIdAlt;
    }

    public String getCityIdAlt() {
        return cityIdAlt;
    }

    public void setCityIdAlt(String cityIdAlt) {
        this.cityIdAlt = cityIdAlt;
    }

    public String getEmailAlt() {
        return emailAlt;
    }

    public void setEmailAlt(String emailAlt) {
        this.emailAlt = emailAlt;
    }

    public String getActiveIndAlt() {
        return activeIndAlt;
    }

    public void setActiveIndAlt(String activeIndAlt) {
        this.activeIndAlt = activeIndAlt;
    }

    public String getRemarksAlt() {
        return remarksAlt;
    }

    public void setRemarksAlt(String remarksAlt) {
        this.remarksAlt = remarksAlt;
    }

    public String getConsigneeId() {
        return consigneeId;
    }

    public void setConsigneeId(String consigneeId) {
        this.consigneeId = consigneeId;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsigneName() {
        return consigneName;
    }

    public void setConsigneName(String consigneName) {
        this.consigneName = consigneName;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getConsigneeCode() {
        return consigneeCode;
    }

    public void setConsigneeCode(String consigneeCode) {
        this.consigneeCode = consigneeCode;
    }

    public String getCustomerSiteLocation() {
        return customerSiteLocation;
    }

    public void setCustomerSiteLocation(String customerSiteLocation) {
        this.customerSiteLocation = customerSiteLocation;
    }

    public String getWayBillCount() {
        return wayBillCount;
    }

    public void setWayBillCount(String wayBillCount) {
        this.wayBillCount = wayBillCount;
    }

    public String getConsignorId() {
        return consignorId;
    }

    public void setConsignorId(String consignorId) {
        this.consignorId = consignorId;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getPinCodeAdd() {
        return pinCodeAdd;
    }

    public void setPinCodeAdd(String pinCodeAdd) {
        this.pinCodeAdd = pinCodeAdd;
    }

    public String getConsignorCode() {
        return consignorCode;
    }

    public void setConsignorCode(String consignorCode) {
        this.consignorCode = consignorCode;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getContractDate() {
        return contractDate;
    }

    public void setContractDate(String contractDate) {
        this.contractDate = contractDate;
    }

    public String getContractAmount() {
        return contractAmount;
    }

    public void setContractAmount(String contractAmount) {
        this.contractAmount = contractAmount;
    }

    public String getContractValidFrom() {
        return contractValidFrom;
    }

    public void setContractValidFrom(String contractValidFrom) {
        this.contractValidFrom = contractValidFrom;
    }

    public String getContractValidTo() {
        return contractValidTo;
    }

    public void setContractValidTo(String contractValidTo) {
        this.contractValidTo = contractValidTo;
    }

    public String getBillingBranchId() {
        return billingBranchId;
    }

    public void setBillingBranchId(String billingBranchId) {
        this.billingBranchId = billingBranchId;
    }

    public String getRateType() {
        return rateType;
    }

    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getCustContractStatus() {
        return custContractStatus;
    }

    public void setCustContractStatus(String custContractStatus) {
        this.custContractStatus = custContractStatus;
    }

    public String getCustConsignorStatus() {
        return custConsignorStatus;
    }

    public void setCustConsignorStatus(String custConsignorStatus) {
        this.custConsignorStatus = custConsignorStatus;
    }

    public String getCustConsigneeStatus() {
        return custConsigneeStatus;
    }

    public void setCustConsigneeStatus(String custConsigneeStatus) {
        this.custConsigneeStatus = custConsigneeStatus;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getBillingBranchName() {
        return billingBranchName;
    }

    public void setBillingBranchName(String billingBranchName) {
        this.billingBranchName = billingBranchName;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getaCName() {
        return aCName;
    }

    public void setaCName(String aCName) {
        this.aCName = aCName;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getBillingAddressId() {
        return billingAddressId;
    }

    public void setBillingAddressId(String billingAddressId) {
        this.billingAddressId = billingAddressId;
    }

    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public String getDisplayCustName() {
        return displayCustName;
    }

    public void setDisplayCustName(String displayCustName) {
        this.displayCustName = displayCustName;
    }

    public String getTtaCustType() {
        return ttaCustType;
    }

    public void setTtaCustType(String ttaCustType) {
        this.ttaCustType = ttaCustType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getTouchPoint1() {
        return touchPoint1;
    }

    public void setTouchPoint1(String touchPoint1) {
        this.touchPoint1 = touchPoint1;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getTotalKM() {
        return totalKM;
    }

    public void setTotalKM(String totalKM) {
        this.totalKM = totalKM;
    }

    public String getTat() {
        return tat;
    }

    public void setTat(String tat) {
        this.tat = tat;
    }

    public String getCounts() {
        return counts;
    }

    public void setCounts(String counts) {
        this.counts = counts;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOriginId() {
        return originId;
    }

    public void setOriginId(String originId) {
        this.originId = originId;
    }

    public String getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }

    public String getTouchPoint1Id() {
        return touchPoint1Id;
    }

    public void setTouchPoint1Id(String touchPoint1Id) {
        this.touchPoint1Id = touchPoint1Id;
    }

    public String getTravelHr() {
        return travelHr;
    }

    public void setTravelHr(String travelHr) {
        this.travelHr = travelHr;
    }

    public String getTravelMin() {
        return travelMin;
    }

    public void setTravelMin(String travelMin) {
        this.travelMin = travelMin;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public String getFirstPickupName() {
        return firstPickupName;
    }

    public void setFirstPickupName(String firstPickupName) {
        this.firstPickupName = firstPickupName;
    }

    public String getPoint1Name() {
        return point1Name;
    }

    public void setPoint1Name(String point1Name) {
        this.point1Name = point1Name;
    }

    public String getPoint2Name() {
        return point2Name;
    }

    public void setPoint2Name(String point2Name) {
        this.point2Name = point2Name;
    }

    public String getPoint3Name() {
        return point3Name;
    }

    public void setPoint3Name(String point3Name) {
        this.point3Name = point3Name;
    }

    public String getPoint4Name() {
        return point4Name;
    }

    public void setPoint4Name(String point4Name) {
        this.point4Name = point4Name;
    }

    public String getFinalPointName() {
        return finalPointName;
    }

    public void setFinalPointName(String finalPointName) {
        this.finalPointName = finalPointName;
    }

    public String getRateWithReefer() {
        return rateWithReefer;
    }

    public void setRateWithReefer(String rateWithReefer) {
        this.rateWithReefer = rateWithReefer;
    }

    public String getRateWithoutReefer() {
        return rateWithoutReefer;
    }

    public void setRateWithoutReefer(String rateWithoutReefer) {
        this.rateWithoutReefer = rateWithoutReefer;
    }

    public String getTatTime() {
        return tatTime;
    }

    public void setTatTime(String tatTime) {
        this.tatTime = tatTime;
    }

    
}
