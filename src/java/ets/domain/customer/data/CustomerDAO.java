package ets.domain.customer.data;

/**
 *
 * @author vidya
 */
import com.ibatis.sqlmap.client.SqlMapClient;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPRuntimeException;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import ets.domain.customer.business.CustomerTO;
import java.io.FileInputStream;

;

public class CustomerDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "CustomerDAO";

    public int doInsertCustomer(CustomerTO customerTO, int userId, String[] billingAddressIds, String[] billingNameAddress, String[] branch, 
            String[] stateId, String[] gstNo, String[] activeInd) {
        Map map = new HashMap();
        int status = 0;
        int insertContractRouteMater = 0;
        int insertBiilingDetails = 0;
        String[] state = customerTO.getCustState().split("-");
        System.out.println("temp::::" + state[0]);

        map.put("customerId", insertContractRouteMater);
        String custCode = "";
        String[] temp1 = null;
        custCode = (String) getSqlMapClientTemplate().queryForObject("customer.getCustomerCode", map);
        if (custCode == null) {
            custCode = "CC-0001";
        } else if (custCode.length() == 1) {
            custCode = "CC-000" + custCode;
        } else if (custCode.length() == 2) {
            custCode = "CC-00" + custCode;
        } else if (custCode.length() == 3) {
            custCode = "CC-0" + custCode;
        } else if (custCode.length() == 4) {
            custCode = "CC-" + custCode;
        }
        map.put("custCode", custCode);
        map.put("paymentType", customerTO.getPaymentType());
        map.put("Code", customerTO.getCustomerCode());
        map.put("Name", customerTO.getCustName());
        map.put("displayCustName", customerTO.getDisplayCustName());
        map.put("billingTypeId", customerTO.getBillingTypeId());
        map.put("Address", customerTO.getCustAddress());
        map.put("City", customerTO.getCustCity());
        map.put("Contact", customerTO.getCustContactPerson());
        map.put("State", state[0]);
        map.put("Phone", customerTO.getCustPhone());
        map.put("Mobile", customerTO.getCustMobile());
        map.put("Email", customerTO.getCustEmail());
        map.put("creditDays", customerTO.getCreditDays());
        map.put("creditLimit", customerTO.getCreditLimit());
        map.put("accountManagerId", customerTO.getAccountManagerId());
        map.put("secondaryBillingTypeId", customerTO.getSecondaryBillingTypeId());
//        map.put("billingNameAddress", customerTO.getBillingNameAddress());
        map.put("customerTypeId", customerTO.getCustType());
        map.put("customerGroupId", customerTO.getCustomerGroupId());
        map.put("detentionAmount", customerTO.getDetentionAmount());
        map.put("detentionPeriod", customerTO.getDetentionPeriod());
        map.put("panNo", customerTO.getPanNo());
        map.put("ttaCustType", customerTO.getTtaCustType());
        //        map.put("tancem", customerTO.getTancem());
        map.put("userId", userId);
//        map.put("stateId", customerTO.getStateId());
//        map.put("gstNo", customerTO.getGstNo());
        map.put("organizationId", customerTO.getOrganizationId());
        System.out.print("customerTO.getCustName()" + customerTO.getCustName());
        try {
            System.out.println("test1" + status);
            //            if ("2".equals(customerTO.getCustomerType())) {
            String code = (String) getSqlMapClientTemplate().queryForObject("customer.getLedgerCode", map);
            String[] temp = code.split("-");
            System.out.println("temp[1] = " + temp[1]);
            int codeval = Integer.parseInt(temp[1]);
            int codev = codeval + 1;
            System.out.println("codev = " + codev);
            String ledgercode = "LEDGER-" + codev;
            System.out.println("ledgercode = " + ledgercode);
            map.put("ledgercode", ledgercode);

            //current year and month start
            String accYear = (String) getSqlMapClientTemplate().queryForObject("customer.accYearVal", map);
            System.out.println("accYear:" + accYear);
            map.put("accYear", accYear);
            //current year end

            status = (Integer) getSqlMapClientTemplate().insert("customer.insertCustomerLedger", map);
            map.put("ledgerId", status);
            //            } else {
            //                map.put("ledgerId", "0");
            //            }

            status = (Integer) getSqlMapClientTemplate().insert("customer.insertCustomer", map);
            System.out.println("status=-=-=-=-=-=" + status);
            if (!"0".equals(customerTO.getSecondaryBillingTypeId())) {
                int insertOperationPointForSecondary = (Integer) getSqlMapClientTemplate().update("customer.insertOperationPointForSecondary", map);
            }
            map.put("customerId", status);
            System.out.println("map in the customer adding screen = " + map);
            int insertCustomerOutStand = 0;
            for (int i = 0; i < billingAddressIds.length; i++) {
                System.out.println("im      in------------");
                map.put("billingNameAddress", billingNameAddress[i]);
                map.put("stateId", stateId[i]);
                map.put("branchName", branch[i]);
                map.put("gstNo", gstNo[i]);
                map.put("activeInd", activeInd[i]);
                System.out.println("map" + map);
                insertBiilingDetails = (Integer) getSqlMapClientTemplate().update("customer.insertBiilingAddressDetails", map);
                System.out.println("status in address " + insertBiilingDetails);
            }
            insertCustomerOutStand = (Integer) getSqlMapClientTemplate().update("customer.insertCustomerOutStand", map);
            insertCustomerOutStand = (Integer) getSqlMapClientTemplate().update("customer.insertCustomerOutStandDetails", map);

            int insertCustomerSecondary = 0;
            if (customerTO.getSecondaryBillingTypeId() != null && !customerTO.getSecondaryBillingTypeId().equals("0")) {
                insertCustomerOutStand = (Integer) getSqlMapClientTemplate().update("customer.insertCustomerSecondaryApproval", map);
            }

            System.out.println("status customer details is is" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public ArrayList getCustomerList() {
        Map map = new HashMap();
        ArrayList customerList = new ArrayList();
        try {
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerList", map);
            System.out.println("customerList size=" + customerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "customerList", sqlException);
        }
        return customerList;
    }

    public ArrayList getLPSCustomerList() {
        Map map = new HashMap();
        ArrayList customerList = new ArrayList();
        try {
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getLPSCustomerList", map);
            System.out.println("customerList size=" + customerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "customerList", sqlException);
        }
        return customerList;
    }

    public int doUpdateCustomer(ArrayList List, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            Iterator itr = List.iterator();
            CustomerTO customerTO = null;
            while (itr.hasNext()) {
                customerTO = (CustomerTO) itr.next();
                System.out.println("getCustId" + customerTO.getCustId());
                System.out.println("custName" + customerTO.getCustName());
                System.out.println("custContact" + customerTO.getCustContactPerson());
                System.out.println("custAddress" + customerTO.getCustAddress());
                System.out.println("customerTO.getCustCity());" + customerTO.getCustCity());
                System.out.println("custState" + customerTO.getCustState());
                System.out.println("custPhone" + customerTO.getCustPhone());
                System.out.println("custMobile" + customerTO.getCustMobile());
                System.out.println("custStatus" + customerTO.getCustStatus());
                map.put("custId", customerTO.getCustId());
                map.put("custName", customerTO.getCustName());
                map.put("custType", customerTO.getCustomerType());
                map.put("custContact", customerTO.getCustContactPerson());
                map.put("custAddress", customerTO.getCustAddress());
                map.put("custCity", customerTO.getCustCity());
                map.put("custState", customerTO.getCustState());
                map.put("custPhone", customerTO.getCustPhone());
                map.put("custMobile", customerTO.getCustMobile());
                map.put("custEmail", customerTO.getCustEmail());
                map.put("custStatus", customerTO.getCustStatus());
                map.put("tancem", customerTO.getTancem());
                map.put("userId", userId);
                map.put("gstNo", customerTO.getGstNo());
                map.put("stateId", customerTO.getStateId());
                map.put("organizationId", customerTO.getOrganizationId());
                status = (Integer) getSqlMapClientTemplate().update("customer.updateCustomer", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-03", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;

    }

    public int doInsertContract(CustomerTO customerTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int ManufacturerId = 1023;
        int ModelId = 1512;
        map.put("ManufacturerId", ManufacturerId);
        map.put("ModelId", ModelId);
        map.put("Spare", customerTO.getContPercentAgeUpSpares());
        map.put("Labour", customerTO.getContPercentAgeUpLabour());
        map.put("Status", customerTO.getContStatus());
        map.put("userId", userId);
        System.out.print("customerTO.getContManufacturerName()" + customerTO.getContPercentAgeUpLabour());
        try {
            status = (Integer) getSqlMapClientTemplate().update("customer.insertContract", map);
            System.out.println("status is" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CONT-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public ArrayList getBunkList(String bunkName) {
        Map map = new HashMap();
        ArrayList bunkList = new ArrayList();
        try {
            System.out.println("bunkName.." + bunkName);
            if (bunkName.length() != 0) {
                map.put("bunkname", bunkName);
            }

            System.out.println("map.size().." + map.size());
            if (map.size() > 0) {
                bunkList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getselBunk", map);
            } else {
                bunkList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getBunkList", map);
            }
            System.out.println("bunkselList size=" + bunkList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "bunkList", sqlException);
        }
        return bunkList;
    }

    public int doInsertBunk(CustomerTO customerTO, int userId) {
        Map mapmaster = new HashMap();
        Map mapdetail = new HashMap();
        Map map = new HashMap();
        ArrayList bunkmasterList = new ArrayList();
        int status = 0;
        int bunkid = 0;
        int id = 0;

        try {
            id = (Integer) getSqlMapClientTemplate().queryForObject("customer.getMaxIDList", mapmaster);
            String FuelLocateId = customerTO.getBunkName().substring(0, 1) + id;

            System.out.println("FuelLocateId" + FuelLocateId);

            mapmaster.put("FuelLocateId", FuelLocateId);
            mapmaster.put("bunkname", customerTO.getBunkName());
            mapmaster.put("location", customerTO.getCurrlocation());
            mapmaster.put("state", customerTO.getBunkState());
            mapmaster.put("act_ind", customerTO.getBunkStatus());
            mapmaster.put("createdby", userId);

            System.out.println("test1" + status);
            int bunkId = (Integer) getSqlMapClientTemplate().insert("customer.insertBunkmaster", mapmaster);
            System.out.println("bunkId master is" + bunkId);

            //Ledger Code start
            if (bunkId != 0) {
                String code = (String) getSqlMapClientTemplate().queryForObject("customer.getLedgerCode", mapmaster);
                String[] temp = code.split("-");
                int codeval = Integer.parseInt(temp[1]);
                int codev = codeval + 1;
                String ledgercode = "LEDGER-" + codev;
                mapmaster.put("ledgercode", ledgercode);

                //current year and month start
                String accYear = (String) getSqlMapClientTemplate().queryForObject("customer.accYearVal", mapmaster);
                System.out.println("accYear:" + accYear);
                mapmaster.put("accYear", accYear);
                //current year end

                String bunkName = customerTO.getBunkName() + "-" + bunkId;
                System.out.println("bunkName =====> " + bunkName);

                int VendorLedgerId = (Integer) getSqlMapClientTemplate().insert("customer.insertVendorLedger", mapmaster);
                System.out.println("VendorLedgerId......." + VendorLedgerId);
                if (VendorLedgerId != 0) {
                    mapmaster.put("ledgerId", VendorLedgerId);
                    mapmaster.put("bunkId", bunkId);
                    System.out.println("map update ::::=> " + mapmaster);
                    status = (Integer) getSqlMapClientTemplate().update("customer.updateBunkInfo", mapmaster);
                }
            }
            //Ledger Code end

            //bunkid = (Integer) getSqlMapClientTemplate().queryForObject("customer.getBunkselectedList", mapmaster);
            System.out.println("bunkid..." + bunkId);
            mapdetail.put("bunkid", bunkId);
            mapdetail.put("fueltype", customerTO.getFuelType());
            mapdetail.put("currentrate", customerTO.getCurrRate());
            mapdetail.put("bunkremarks", customerTO.getRemarks());
            mapdetail.put("act_ind", customerTO.getBunkStatus());
            mapdetail.put("createdby", userId);

            System.out.println("test2" + status);
            status = (Integer) getSqlMapClientTemplate().update("customer.insertBunkdetails", mapdetail);
            System.out.println("status details is" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public ArrayList getBunkalterList(String bunkId) {
        Map map = new HashMap();
        ArrayList bunkList = new ArrayList();
        try {
            if (bunkId != null) {
                map.put("bunkid", bunkId);
            }
            bunkList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getalterBunk", map);

            System.out.println("bunkList size=" + bunkList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "bunkList", sqlException);
        }
        return bunkList;
    }

    public int doUpdateBunk(CustomerTO customerTO, int userId, String bunkId) {
        Map mapmaster = new HashMap();
        Map mapdetail = new HashMap();
        Map map = new HashMap();
        ArrayList bunkmasterList = new ArrayList();
        int status = 0;
        mapmaster.put("bunkname", customerTO.getBunkName());
        mapmaster.put("location", customerTO.getCurrlocation());
        mapmaster.put("state", customerTO.getBunkState());
        mapmaster.put("act_ind", customerTO.getBunkStatus());
        mapmaster.put("createdby", userId);
        mapmaster.put("bunkid", bunkId);

        try {
            System.out.println("test1=" + status);
            status = (Integer) getSqlMapClientTemplate().update("customer.updateBunkmaster", mapmaster);
            System.out.println("status master is=" + status);
            System.out.println("bunkid..." + bunkId);
            mapdetail.put("bunkid", bunkId);
            mapdetail.put("fueltype", customerTO.getFuelType());
            mapdetail.put("currentrate", customerTO.getCurrRate());
            mapdetail.put("bunkremarks", customerTO.getRemarks());
            mapdetail.put("act_ind", customerTO.getBunkStatus());
            mapdetail.put("createdby", userId);

            System.out.println("\nbunkid.." + bunkId);
            System.out.println("\ncustomerTO.getFuelType()=" + customerTO.getFuelType());
            System.out.println("\ncustomerTO.getCurrRate()=" + customerTO.getCurrRate());
            System.out.println("\ncustomerTO.getRemarks()=" + customerTO.getRemarks());
            System.out.println("\ncustomerTO.getBunkStatus()=" + customerTO.getBunkStatus());
            System.out.println("\ncreatedby=" + userId);
            status = (Integer) getSqlMapClientTemplate().update("customer.updateBunkdetails", mapdetail);
            System.out.println("status details is" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public ArrayList getPackingList() {
        Map map = new HashMap();
        ArrayList packList = new ArrayList();
        try {
            packList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getPackingList", map);
            System.out.println("packList size=" + packList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "customerList", sqlException);
        }
        return packList;
    }

    public String getToDestination(String destination) {
        Map map = new HashMap();
        map.put("destination", destination);
        String suggestions = "";
        CustomerTO custTO = new CustomerTO();
        try {
            ArrayList destinations = new ArrayList();
            System.out.println("map = " + map);
            destinations = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getDestinations", map);
            Iterator itr = destinations.iterator();
            while (itr.hasNext()) {
                custTO = new CustomerTO();
                custTO = (CustomerTO) itr.next();
                suggestions = custTO.getToLocation() + "~" + suggestions;
                System.out.println("suggestions: " + suggestions);
            }
            if ("".equals(suggestions)) {
                suggestions = "no match found";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getToDestination Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getToDestination", sqlException);
        }
        return suggestions;
    }

    public ArrayList getBillingTypeList() {
        Map map = new HashMap();
        ArrayList billingTypeList = new ArrayList();
        try {
            billingTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getBillingTypeList", map);
            System.out.println("billingTypeList =" + billingTypeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillingTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getBillingTypeList", sqlException);
        }
        return billingTypeList;
    }

    public ArrayList getCustomerGroupList() {
        Map map = new HashMap();
        ArrayList result = new ArrayList();
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerGroupList", map);
            System.out.println("getCustomerGroupList =" + result.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerGroupList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getCustomerGroupList", sqlException);
        }
        return result;
    }

    public String getCustomerCode(CustomerTO customerTO, int insertContractRouteMater) {
        Map map = new HashMap();
        String custCode = "";
        map.put("customerId", customerTO.getCustomerId());
        try {
            custCode = (String) getSqlMapClientTemplate().queryForObject("customer.getCustomerCode", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerCode Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerCode", sqlException);
        }

        return custCode;
    }

    public ArrayList getCustomerName(CustomerTO customerTO) {
        Map map = new HashMap();
        ArrayList getCustomerName = new ArrayList();
        map.put("custName", customerTO.getCustName() + "%");
        try {
            System.out.println("getCustomerName map" + map);
            getCustomerName = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerName", map);
            System.out.println("getCustomerName =" + getCustomerName.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getCustomerName", sqlException);
        }
        return getCustomerName;
    }

    public ArrayList getCustomerCodes(CustomerTO customerTO) {
        Map map = new HashMap();
        ArrayList getCustomerCodes = new ArrayList();
        map.put("customerCode", customerTO.getCustomerCode() + "%");
        try {
            System.out.println("getCustomerCode map" + map);
            getCustomerCodes = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerCodes", map);
            System.out.println("getCustomerCodes =" + getCustomerCodes.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerCodes Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getCustomerCodes", sqlException);
        }
        return getCustomerCodes;
    }

    public ArrayList getCustomerDetails(CustomerTO customerTO) {
        Map map = new HashMap();
        //         CustomerTO customerTO = new CustomerTO();
        ArrayList getCustomerDetails = new ArrayList();
        map.put("customerId", customerTO.getCustomerId());

        try {
            System.out.println("getCustomerDetails map" + map);
            getCustomerDetails = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerDetails", map);
            System.out.println("getCustomerGroupList =" + getCustomerDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getCustomerGroupList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getCustomerGroupList", sqlException);
        }
        return getCustomerDetails;
    }

    public ArrayList getCustomerLists(CustomerTO customerTO) {
        Map map = new HashMap();
        ArrayList customerList = new ArrayList();
        map.put("customerId", customerTO.getCustomerId());
        map.put("secondaryContractSatus", customerTO.getContractType());
        if ("1".equals(customerTO.getCustomerType())) {
            map.put("primaryCustomerType", customerTO.getCustomerType());
        } else if ("2".equals(customerTO.getCustomerType())) {
            map.put("secondaryCustomerType", customerTO.getCustomerType());
        }

        map.put("roleId", customerTO.getRoleId());
        map.put("userId", customerTO.getUserId());
        String empId = (String) getSqlMapClientTemplate().queryForObject("customer.getEmployeeId", map);
        map.put("empId", empId);

        System.out.println("map size=" + map);
        try {
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerList", map);
            System.out.println("customerList size=" + customerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "customerList", sqlException);
        }
        return customerList;
    }

    public int updateCustomer(CustomerTO customerTO, int userId, String[] billingAddressIds, String[] billingNameAddress, String[] branchName, String[] stateId, String[] gstNo, String[] activeInd) {
        Map map = new HashMap();
        int status = 0;
        int insertContractRouteMater = 0;
        map.put("customerId", customerTO.getCustomerId());

        String[] state = customerTO.getCustState().split("-");
        System.out.println("temp::::" + state[0]);
        String custCode = "";
        String[] temp1 = null;
        custCode = (String) getSqlMapClientTemplate().queryForObject("customer.getCustomerCode", map);
        if (custCode == null) {
            custCode = "CC-0001";
        } else {
            custCode = "CC-0001";
        }
        map.put("custCode", custCode);
        map.put("paymentType", customerTO.getPaymentType());
        map.put("Code", customerTO.getCustomerCode());
        map.put("Name", customerTO.getCustomerName());
        map.put("displayCustName", customerTO.getDisplayCustName());
        map.put("billingTypeId", customerTO.getBillingTypeId());
        map.put("Address", customerTO.getCustAddress());
        map.put("City", customerTO.getCustCity());
        map.put("Contact", customerTO.getCustContactPerson());
        map.put("State", state[0]);
        map.put("Phone", customerTO.getCustPhone());
        map.put("Mobile", customerTO.getCustMobile());
        map.put("Email", customerTO.getCustEmail());
        map.put("creditDays", customerTO.getCreditDays());
        map.put("creditLimit", customerTO.getCreditLimit());
        map.put("accountManagerId", customerTO.getAccountManagerId());
        map.put("secondaryBillingTypeId", customerTO.getSecondaryBillingTypeId());
//        map.put("billingNameAddress", customerTO.getBillingNameAddress());
        map.put("customerTypeId", customerTO.getCustType());
        map.put("customerGroupId", customerTO.getCustomerGroupId());
        map.put("detentionAmount", customerTO.getDetentionAmount());
        map.put("detentionPeriod", customerTO.getDetentionPeriod());
        map.put("panNo", customerTO.getPanNo());
        map.put("ttaCustType", customerTO.getTtaCustType());
        map.put("organizationId", customerTO.getOrganizationId());
        map.put("status", customerTO.getCustStatus());
        map.put("userId", userId);
        System.out.print("customerTO.getCustName()" + customerTO.getCustName());
        System.out.print("map" + map);
        try {
            System.out.println("test1" + status);

            status = (Integer) getSqlMapClientTemplate().update("customer.updateEditCustomer", map);
            System.out.println("status customer details is is" + status);

            for (int i = 0; i < billingAddressIds.length; i++) {
                System.out.println("im      in------------");
                map.put("billingAddressIds", billingAddressIds[i]);
                map.put("billingNameAddress", billingNameAddress[i]);
                map.put("stateId", stateId[i]);
                map.put("gstNo", gstNo[i]);
                map.put("branchName", branchName[i]);
                map.put("activeInd", activeInd[i]);
                System.out.println("map" + map);
                if (billingAddressIds[i] != "" && billingAddressIds[i] != null) {
                    System.out.println("im in update");
                    int updateBiilingDetails = (Integer) getSqlMapClientTemplate().update("customer.updateBiilingAddressDetails", map);
                    System.out.println("update in address " + updateBiilingDetails);
                } else {

                    System.out.println("im in insert");
                    int insertBiilingDetails = (Integer) getSqlMapClientTemplate().update("customer.insertBiilingAddressDetails", map);
                    System.out.println("status in address " + insertBiilingDetails);

                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public int updateBranchDetails(CustomerTO customerTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("branchId", customerTO.getBranchId());
        map.put("branchName", customerTO.getBranchName());
        map.put("customerId", customerTO.getCustomerId());
        map.put("contactPerson", customerTO.getContactPerson());
        map.put("phoneNo", customerTO.getPhoneNo());
        map.put("address1", customerTO.getAddress1());
        map.put("address2", customerTO.getAddress2());
        map.put("stateId", customerTO.getStateId());
        map.put("cityId", customerTO.getCityId());
        map.put("email", customerTO.getEmail());
        map.put("pinCode", customerTO.getPinCode());
        map.put("activeInd", customerTO.getActiveInd());
        map.put("remarks", customerTO.getRemarks());
        map.put("gstNo", customerTO.getGstNo());
        map.put("userId", userId);
        try {

            status = (Integer) getSqlMapClientTemplate().update("customer.updateBranchDetails", map);
            int updateBiilingDetails = (Integer) getSqlMapClientTemplate().update("customer.updateBiilingAddressDetails", map);
            //System.out.println("updateConisgnorDetails status is" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateBranchDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS,
                    "updateBranchDetails", sqlException);
        }
        return status;
    }

    public ArrayList processBranchList(CustomerTO customerTO) {
        Map map = new HashMap();
        ArrayList branchList = new ArrayList();

        try {
            if ((customerTO.getCustomerId() == null) && "".equals(customerTO.getCustomerId())) {
                map.put("customerid", "0");
            } else {
                map.put("customerid", customerTO.getCustomerId());
            }
            branchList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getBranchList", map);
            //System.out.println("processContractConsigneeList dao size=" + ConsigneeList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBranchList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getBranchList", sqlException);
        }
        return branchList;
    }

    public int insertBranchDetails(CustomerTO customerTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int insertBiilingDetails = 0;
        int status1 = 0;
        String branchId = "";
        String branchCode = "BRN";
        // map.put("customerName", customerTO.getCustomerName());
        map.put("branchId", customerTO.getBranchId());
        map.put("branchName", customerTO.getBranchName());
        map.put("customerId", customerTO.getCustomerId());
        map.put("contactPerson", customerTO.getContactPerson());
        map.put("phoneNo", customerTO.getPhoneNo());
        map.put("address1", customerTO.getAddress1());
        map.put("address2", customerTO.getAddress2());
        map.put("stateId", customerTO.getStateId());
        map.put("cityId", customerTO.getCityId());
        map.put("email", customerTO.getEmail());
        map.put("activeInd", customerTO.getActiveInd());
        map.put("remarks", customerTO.getRemarks());
        map.put("pinCode", customerTO.getPinCode());
        map.put("gstNo", customerTO.getGstNo());
        map.put("userId", userId);
        System.out.println("map details***************************************" + map);
        try {
            if ("".equals(customerTO.getBranchId())) {
//            if (customerTO.getBranchId().equals("")) {
                branchId = (String) session.queryForObject("customer.getBranchCode", map);
                branchCode = branchCode + branchId;
                map.put("branchCode", branchCode);
                status = (Integer) session.insert("customer.insertBranchDetails", map);
                map.put("branchId", status);
                System.out.println("insertBranchDetails status is" + status);
                insertBiilingDetails = (Integer) session.insert("customer.insertBiilingAddressDetails", map);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertBranchDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS,
                    "insertBranchDetails", sqlException);
        }
        return status;
    }

    public ArrayList getBranchView(CustomerTO customerTO) {
        Map map = new HashMap();
        map.put("customerId", customerTO.getCustomerId());

        ArrayList branch = new ArrayList();
        try {

            branch = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getBranchViewList", map);
            System.out.println("value" + branch.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBranchViewList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getBranchViewList", sqlException);
        }
        return branch;

    }

    public int updateConsigneeDetails(CustomerTO customerTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("consigneeId", customerTO.getConsigneeId());
        map.put("consigneeName", customerTO.getConsigneeName());
        map.put("customerId", customerTO.getCustomerId());
        map.put("contactPerson", customerTO.getContactPerson());
        map.put("phoneNo", customerTO.getPhoneNo());
        map.put("address1", customerTO.getAddress1());
        map.put("address2", customerTO.getAddress2());
        map.put("stateId", customerTO.getStateId());
        map.put("cityId", customerTO.getCityId());
        map.put("email", customerTO.getEmail());
        map.put("activeInd", customerTO.getActiveInd());
        map.put("remarks", customerTO.getRemarks());
        map.put("pinCode", customerTO.getPinCode());
        map.put("userId", userId);
        map.put("gstNo", customerTO.getGstNo());
        map.put("consigneeCode", customerTO.getConsigneeCode());
        //System.out.println("map updateConsigneeDetails = " + map);
        try {

            status = (Integer) getSqlMapClientTemplate().update("customer.updateConisgneeDetails", map);
            //System.out.println("updateConisgneeDetails status is" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("UpdateConisgeerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS,
                    "UpdateConisgneeDetails", sqlException);
        }
        return status;
    }

    /**
     * This method used to Get The Consignee List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList processContractConsigneeList(CustomerTO customerTO, String custId) {
        Map map = new HashMap();
        map.put("custId", custId);
        ArrayList ConsigneeList = new ArrayList();
        System.out.println("map customerId" + map);
        try {
            if ((customerTO.getCustomerId() == null) && "".equals(customerTO.getCustomerId())) {
                map.put("customerid", "0");
            } else {
                map.put("customerid", customerTO.getCustomerId());
            }
            ConsigneeList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getContractConsigneeList", map);
            //System.out.println("processContractConsigneeList dao size=" + ConsigneeList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processContractConsigneeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "processContractConsigneeList", sqlException);
        }
        return ConsigneeList;
    }

    /**
     * This method used to Insert The Consignee.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertConsigneeDetails(CustomerTO customerTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int status1 = 0;
        String ConsigneeId = "";
        String ConsigneeCode = "CSE";
        // map.put("customerName", customerTO.getCustomerName());
        map.put("consigneeId", customerTO.getConsigneeId());
        map.put("consigneeCode", customerTO.getConsigneeCode());
        map.put("consigneeName", customerTO.getConsigneeName());
        map.put("customerId", customerTO.getCustomerId());
        map.put("contactPerson", customerTO.getContactPerson());
        map.put("phoneNo", customerTO.getPhoneNo());
        map.put("address1", customerTO.getAddress1());
        map.put("address2", customerTO.getAddress2());
        map.put("stateId", customerTO.getStateId());
        map.put("cityId", customerTO.getCityId());
        map.put("email", customerTO.getEmail());
        map.put("activeInd", customerTO.getActiveInd());
        map.put("remarks", customerTO.getRemarks());
        map.put("pinCode", customerTO.getPinCode());
        map.put("userId", userId);
        map.put("gstNo", customerTO.getGstNo());
        System.out.println("map details***************************************" + map);
        try {
            if (customerTO.getConsigneeId().equals("")) {
//                ConsigneeId = (String) session.queryForObject("customer.getConsigneeCode", map);
//                ConsigneeCode = ConsigneeCode + ConsigneeId;
//                map.put("consigneeCode", ConsigneeCode);
//                System.out.println("ConsigneeCode" + ConsigneeCode);
                status = (Integer) session.insert("customer.insertConsigneeDetails", map);
                //System.out.println("insertConsignorDetails status is" + status);

            }

            //System.out.println("insertConsigneeDetails status is" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("InsertConsigneeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS,
                    "InsertConsigneeDetails", sqlException);
        } finally {
            ConsigneeId = null;
            ConsigneeCode = null;
            map = null;
        }
        return status;
    }

    public int getcheckConsigneeName(String consignName, String customerId) {
        Map map = new HashMap();
        String suggestions = "";
        int status = 0;
        ArrayList getCustomerName = new ArrayList();
        //            map.put("custName", customerTO.getCustName() + "%");
        map.put("consignName", consignName);
        map.put("customerId", customerId);
        System.out.println("map" + map);
        try {
            //System.out.println("getCustomerName map" + map);
            status = (Integer) getSqlMapClientTemplate().queryForObject("customer.getcheckConsigneeName", map);

            System.out.println("getCustomerName =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getCustomerName", sqlException);
        }
        return status;
    }

    public ArrayList getCityList() {
        Map map = new HashMap();
        ArrayList cityList = new ArrayList();
        try {
            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCityList", map);
            //System.out.println("getCityList dao size=" + cityList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "CityList", sqlException);
        } finally {
            map = null;
        }
        return cityList;
    }

    //consignor
    /**
     * This method used to Update The Consignor.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int updateConsignorDetails(CustomerTO customerTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("consignorId", customerTO.getConsignorId());
        map.put("consignorCode", customerTO.getConsignorCode());
        map.put("consignorName", customerTO.getConsignorName());
        map.put("customerId", customerTO.getCustomerId());
        map.put("contactPerson", customerTO.getContactPerson());
        map.put("phoneNo", customerTO.getPhoneNo());
        map.put("address1", customerTO.getAddress1());
        map.put("address2", customerTO.getAddress2());
        map.put("stateId", customerTO.getStateId());
        map.put("cityId", customerTO.getCityId());
        map.put("email", customerTO.getEmail());
        map.put("activeInd", customerTO.getActiveInd());
        map.put("remarks", customerTO.getRemarks());
        map.put("pinCode", customerTO.getPinCodeAdd());
        map.put("userId", userId);
        map.put("gstNo", customerTO.getGstNo());
        try {

            status = (Integer) getSqlMapClientTemplate().update("customer.updateConisgnorDetails", map);
            //System.out.println("updateConisgnorDetails status is" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("UpdateConisgnorDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS,
                    "UpdateConisgnorDetails", sqlException);
        }
        return status;
    }

    /**
     * This method used to Get The Consignor List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList processContractConsignorList(CustomerTO customerTO, String custId) {
        Map map = new HashMap();
        map.put("custId", custId);
        ArrayList consignorList = new ArrayList();
        int customerId = 0;
        try {

            consignorList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getContractConsignorList", map);
            //System.out.println("processContractConsignorList dao size=" + consignorList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processContractConsignorList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "processContractConsignorList", sqlException);
        }
        return consignorList;
    }

    /**
     * This method used to Get The CustomerContract List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getCustomerContractList() {
        Map map = new HashMap();
        ArrayList customerContractList = new ArrayList();
        try {
            customerContractList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerContractList", map);
            //System.out.println("getCustomerList dao size=" + customerContractList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerContractList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getCustomerContractList", sqlException);
        }
        return customerContractList;
    }

    /**
     * This method used to Insert The Consignor.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertConsignorDetails(CustomerTO customerTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int status1 = 0;
        String consignorId = "";
        String consignorCode = "CSR";
        // map.put("customerName", customerTO.getCustomerName());
        map.put("consignorId", customerTO.getConsignorId());
        map.put("consignorCode", customerTO.getConsignorCode());
        map.put("consignorName", customerTO.getConsignorName());
        map.put("customerId", customerTO.getCustomerId());
        map.put("contactPerson", customerTO.getContactPerson());
        map.put("phoneNo", customerTO.getPhoneNo());
        map.put("address1", customerTO.getAddress1());
        map.put("address2", customerTO.getAddress2());
        map.put("stateId", customerTO.getStateId());
        map.put("cityId", customerTO.getCityId());
        map.put("email", customerTO.getEmail());
        map.put("activeInd", customerTO.getActiveInd());
        map.put("remarks", customerTO.getRemarks());
        map.put("pinCode", customerTO.getPinCodeAdd());
        map.put("userId", userId);
        map.put("gstNo", customerTO.getGstNo());
        System.out.println("map details***************************************" + map);
        try {
            if (customerTO.getConsignorId().equals("")) {
//                consignorId = (String) session.queryForObject("customer.getConsignorCode", map);
//                consignorCode = consignorCode + consignorId;
//                map.put("consignorCode", consignorCode);
                status = (Integer) session.insert("customer.insertConsignorDetails", map);
                //System.out.println("insertConsignorDetails status is" + status);

            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("InsertConsignorDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS,
                    "InsertConsignorDetails", sqlException);
        } finally {
            consignorId = null;
            consignorCode = null;
            map = null;
        }
        return status;
    }

    public int getcheckConsignorName(String consignName, String customerId) {
        Map map = new HashMap();
        String suggestions = "";
        int status = 0;
        ArrayList getCustomerName = new ArrayList();
//            map.put("custName", customerTO.getCustName() + "%");
        map.put("consignName", consignName);
        map.put("customerId", customerId);
        System.out.println("map" + map);
        try {
            //System.out.println("getCustomerName map" + map);
            status = (Integer) getSqlMapClientTemplate().queryForObject("customer.getcheckConsignorName", map);

            System.out.println("getCustomerName =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getCustomerName", sqlException);
        }
        return status;
    }

    public String getStateName(CustomerTO customerTO) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("cityId", customerTO.getCityId());

        //System.out.println("cityId::"+ customerTO.getCityId());
        String stateName = "";

        try {
            stateName = (String) getSqlMapClientTemplate().queryForObject("customer.getStateName", map);
            //System.out.println("stateName " + stateName);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("stateName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "stateName", sqlException);
        }
        return stateName;
    }

    public ArrayList getConsignorView(CustomerTO customerTO) {
        Map map = new HashMap();
        map.put("customerId", customerTO.getCustomerId());

        ArrayList consignor = new ArrayList();
        try {

            consignor = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getConsignorViewList", map);
            System.out.println("value" + consignor.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignorView Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getConsignorView", sqlException);
        }
        return consignor;

    }

    public ArrayList getConsigneeView(CustomerTO customerTO) {
        Map map = new HashMap();
        map.put("customerId", customerTO.getCustomerId());

        ArrayList consignee = new ArrayList();
        try {

            consignee = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getConsigneeViewList", map);
            System.out.println("value" + consignee.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsigneeView Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getConsigneeView", sqlException);
        }
        return consignee;

    }
//<GST>

    public ArrayList getStateList() {
        Map map = new HashMap();
        //         CustomerTO customerTO = new CustomerTO();
        ArrayList getStateList = new ArrayList();

        try {
            System.out.println("getStateList map" + map);
            getStateList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getStateList", map);
            System.out.println("getStateList =" + getStateList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerGroupList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getCustomerGroupList", sqlException);
        }
        return getStateList;
    }

    public ArrayList getOrganizationList() {
        Map map = new HashMap();
        //         CustomerTO customerTO = new CustomerTO();
        ArrayList getOrganizationList = new ArrayList();

        try {
            System.out.println("getOrganizationList map" + map);
            getOrganizationList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getOrganizationList", map);
            System.out.println("getOrganizationList =" + getOrganizationList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrganizationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getOrganizationList", sqlException);
        }
        return getOrganizationList;
    }

    public ArrayList getCustomerAddressDetails(CustomerTO customerTO) {
        Map map = new HashMap();
        //         CustomerTO customerTO = new CustomerTO();
        ArrayList getCustomerAddressDetails = new ArrayList();
        map.put("customerId", customerTO.getCustomerId());

        try {
            System.out.println("getCustomerAddressDetails map" + map);
            getCustomerAddressDetails = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerAddressDetails", map);
            System.out.println("getCustomerAddressDetails =" + getCustomerAddressDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerAddressDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getCustomerAddressDetails", sqlException);
        }
        return getCustomerAddressDetails;
    }

    public ArrayList processBranchListEdit(CustomerTO customerTO, String custId, String branchId) {
        Map map = new HashMap();
        ArrayList branchListEdit = new ArrayList();
        map.put("custId", custId);
        map.put("branchId", branchId);
        try {
            System.out.println("map branchedit=" + map);
            branchListEdit = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getBranchListEdit", map);
            System.out.println("branchListEdit dao size=" + branchListEdit.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBranchList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getBranchList", sqlException);
        }
        return branchListEdit;
    }

    public int processInsertCustContractFiles(CustomerTO customerTO, int UserId, String[] fileSaved, String[] desc) {

        Map map = new HashMap();
        FileInputStream fis = null;
        int status = 0;

        try {
            map.put("customerId", customerTO.getCustomerId());
            System.out.println("map : " + map);

            if (fileSaved.length > 0) {

                for (int x = 0; x < fileSaved.length; x++) {

                    if (fileSaved[x] != null) {

                        System.out.println("FileType[x]" + fileSaved[x]);
                        System.out.println("fileSaved[x]" + fileSaved[x]);
                        map.put("desc", desc[x]);
                        map.put("path", fileSaved[x]);
                        status = (Integer) getSqlMapClientTemplate().insert("customer.insertCustomerContractFiles", map);

                    }
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processInsertLHCDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "processInsertLHCDetails", sqlException);
        }
        return status;

    }

    public ArrayList getCustContractFiles(String customerId) {
        Map map = new HashMap();
        ArrayList customerContractFiles = new ArrayList();
        map.put("customerId", customerId);
        try {
            System.out.println("map customerContractFiles=" + map);
            customerContractFiles = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustContractFiles", map);
            System.out.println("branchListEdit dao size=" + customerContractFiles.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBranchList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getBranchList", sqlException);
        }
        return customerContractFiles;
    }

    public int getRouteCheck(CustomerTO customerTO) {
        Map map = new HashMap();
        int getRouteCheck = 0;
        map.put("customerName", customerTO.getCustName());
        map.put("customerId", customerTO.getCustomerId());
        map.put("origin", customerTO.getOrigin());
        if ("".equals(customerTO.getTouchPoint1()) || "NA".equals(customerTO.getTouchPoint1()) || "0".equals(customerTO.getTouchPoint1())) {
            map.put("touchPoint1", "");
        } else {
            map.put("touchPoint1", customerTO.getTouchPoint1());
        }
        map.put("destination", customerTO.getDestination());
        map.put("vehicleType", customerTO.getVehicleType());
        System.out.println("ContractRatesExcelUpload = " + map);
        try {

            int checkVehType = (Integer) getSqlMapClientTemplate().queryForObject("customer.getContractVehicleType", map);
            System.out.println(" vehicle type check : " + checkVehType);
            if (checkVehType > 0) {
                getRouteCheck = (Integer) getSqlMapClientTemplate().queryForObject("customer.getRouteCheck", map);
                System.out.println(" getRouteCheck check : " + getRouteCheck);
                if (getRouteCheck > 0) {
                    getRouteCheck = (Integer) getSqlMapClientTemplate().queryForObject("customer.getContractCheck", map);
                    System.out.println(" getContractCheck check : " + getRouteCheck);
                    // if value 1 invalid contract
                    // if value 0 valid route
                } else {
                    getRouteCheck = 3; //invalid Route
                }
            } else {
                getRouteCheck = 2; //invalid vehicle type
            }
            System.out.println(" final check : " + getRouteCheck);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRouteCheck Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getRouteCheck", sqlException);
        }
        return getRouteCheck;
    }

    public int updateConsignmentContract() {
        Map map = new HashMap();
        int status = 0;

        try {
            status = (Integer) getSqlMapClientTemplate().update("customer.updateConsignmentContract", map);
            System.out.println("updateConsignmentContract" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateConsignmentContract Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateConsignmentContract", sqlException);
        }
        return status;

    }

    public int insertContractUpload(CustomerTO customerTO) {
        Map map = new HashMap();
        int status = 0;

        try {
            map.put("userId", customerTO.getUserId());
            map.put("customerId", customerTO.getCustomerId());
            map.put("customerName", customerTO.getCustName());
            map.put("origin", customerTO.getOrigin());
            map.put("touchPoint1", customerTO.getTouchPoint1());
            map.put("destination", customerTO.getDestination());
            map.put("amount", customerTO.getContractAmount());
            map.put("vehicleType", customerTO.getVehicleType());
            map.put("KM", customerTO.getTotalKM());
            map.put("TAT", customerTO.getTat());
            map.put("activeInd", 'Y');
            map.put("flag", customerTO.getCounts());

            System.out.println("map : " + map);
            status = (Integer) getSqlMapClientTemplate().insert("customer.insertContractUpload", map);

            System.out.println("insert Trip Planning status" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractUpload Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertConsignmentUpload", sqlException);
        }
        return status;
    }

    public ArrayList getContractUploadIncorect(CustomerTO customerTO) {
        Map map = new HashMap();
        ArrayList getContractUploadIncorect = new ArrayList();
        //  map.put("flag", customerTO.getFlage());
        System.out.println("ConsignmentExcelUpload = " + map);
        try {
            getContractUploadIncorect = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getContractUploadIncorect", map);
            System.out.println("getContractUploadIncorect size():===" + getContractUploadIncorect.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractUploadIncorect Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getContractUploadIncorect", sqlException);
        }
        return getContractUploadIncorect;
    }

    public ArrayList getContractExcelUpload(CustomerTO customerTO) {
        Map map = new HashMap();
        ArrayList getContractExcelUpload = new ArrayList();
        // map.put("flag", customerTO.getFlage());
        System.out.println("getContractExcelUpload = " + map);
        try {
            getContractExcelUpload = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getContractExcelUpload", map);
            System.out.println("getContractExcelUpload size():===" + getContractExcelUpload.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractExcelUpload Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getContractExcelUpload", sqlException);
        }
        return getContractExcelUpload;
    }

    public int insertCustomerContractExcel(int userId) {
        Map map = new HashMap();
        String TAT = "0";
        int routeContractId = 0;
        ArrayList getContractExcelUpload = new ArrayList();
        double loop = 0;
        try {

            getContractExcelUpload = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerContractExcelDetail", map);
            System.out.println("getContractExcelUpload : "+getContractExcelUpload.size());
            
            Iterator itr1 = getContractExcelUpload.iterator();
            CustomerTO tpTO1 = new CustomerTO();
            
            while(itr1.hasNext()) {
                tpTO1 = (CustomerTO) itr1.next();
                loop++;
                map.put("customerId", tpTO1.getCustomerId());                
                map.put("ptpPickupPoint", tpTO1.getOrigin());
                map.put("interimPoint1", tpTO1.getTouchPoint1());
                map.put("ptpDropPoint", tpTO1.getDestination());
                map.put("rateWithoutReefer", tpTO1.getContractAmount());
                map.put("vehicleTypeId", tpTO1.getVehicleType());
                map.put("ptpDropPointKm", tpTO1.getTotalKM());
                map.put("TAT", tpTO1.getTat());
                map.put("tatTime", tpTO1.getTat());

                map.put("rateWithReefer", "68");
                map.put("routeCode", "RC");
                map.put("activeInd", 'Y');
                map.put("userId", userId);

                map.put("contractId", tpTO1.getContractId());
                map.put("ptpPickupPointId", tpTO1.getOriginId());
                map.put("ptpDropPointId", tpTO1.getDestinationId());
                if(tpTO1.getTouchPoint1Id().equals("")){
                map.put("interimPointId1", "0");
                }else{
                map.put("interimPointId1", tpTO1.getTouchPoint1Id());
                }
                map.put("ptpDropPointHrs", tpTO1.getTravelHr());
                map.put("ptpDropPointMinutes", tpTO1.getTravelMin());
                map.put("ptpDropPointRouteId", tpTO1.getRouteId());
                map.put("ptpTotalKm", tpTO1.getTotalKM());
                map.put("ptpTotalHours", tpTO1.getTravelHr());
                map.put("ptpTotalMinutes", tpTO1.getTravelMin());
                map.put("pointCount", 1);

                System.out.println("map : " + map+" : count : "+loop);
                routeContractId = (Integer) getSqlMapClientTemplate().insert("customer.insertContractPTPBillingDetails", map);
                System.out.println("insert Customer Contract route status : " + routeContractId);

                map.put("routeContractId", routeContractId);

                System.out.println("map Contract Rates In the DAO= " + map);
                int insertBillingDetails = (Integer) getSqlMapClientTemplate().update("customer.insertPTPContractRates", map);
                System.out.println("insertBillingDetails : " + insertBillingDetails);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertCustomerContractExcel Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertCustomerContractExcel", sqlException);
        }
        return routeContractId;
    }

    
    public ArrayList getCustomerExportExcelList(CustomerTO customerTO) {
        Map map = new HashMap();
        ArrayList customerList = new ArrayList();
        map.put("customerId", customerTO.getCustomerId());
        map.put("secondaryContractSatus", customerTO.getContractType());

        map.put("roleId", customerTO.getRoleId());
        map.put("userId", customerTO.getUserId());
        String empId = (String) getSqlMapClientTemplate().queryForObject("customer.getEmployeeId", map);
        map.put("empId", empId);

        System.out.println("map size=" + map);
        try {
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerExportExcelList", map);
            System.out.println("customerList size=" + customerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "customerList", sqlException);
}
        return customerList;
    }
}
