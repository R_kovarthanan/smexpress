/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.scheduler.web;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.util.sendInvoice;
import ets.arch.util.sendService;
import ets.arch.util.sendVendorInvoice;
import ets.arch.web.BaseController;
import ets.domain.company.business.CompanyTO;

import ets.domain.report.business.ReportBP;
import ets.domain.report.business.ReportTO;
import ets.domain.scheduler.business.SchedulerBP;
import ets.domain.scheduler.business.SchedulerTO;
import ets.domain.thread.web.SendEmail;
import ets.domain.thread.web.SendEmailDetails;
import ets.domain.thread.web.SendSMS;
import ets.domain.users.business.LoginBP;
import ets.domain.users.business.LoginTO;
import ets.domain.report.business.ReportBP;
import ets.domain.report.business.ReportTO;
import ets.domain.trip.business.TripTO;
import ets.domain.util.FPLogUtils;
import ets.domain.util.ThrottleConstants;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
//import throttlegps.gpsActivity;
//import throttlegps.main.GpsLocationMain;

//excel
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;

/**
 *
 * @author Arun
 */
public class ScheduleController extends BaseController {

    LoginBP loginBP;
    SchedulerBP schedulerBP;
    ReportBP reportBP;

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public SchedulerBP getSchedulerBP() {
        return schedulerBP;
    }

    public void setSchedulerBP(SchedulerBP schedulerBP) {
        this.schedulerBP = schedulerBP;
    }

    public ReportBP getReportBP() {
        return reportBP;
    }

    public void setReportBP(ReportBP reportBP) {
        this.reportBP = reportBP;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();
        System.out.println("executeScheduler : request.getRequestURI() = " + request.getRequestURI());

    }

    public static String applicationServer;
    public static String serverIpAddress;
    int runHour = 0;
    int runMinute = 0;

    public void executeScheduler() throws FPRuntimeException, FPBusinessException {
        ReportTO reportTO = new ReportTO();
        ReportTO reportTO1 = new ReportTO();

        try {

            int smsSchedulerStatus = Integer.parseInt(ThrottleConstants.smsSchedulerStatus);

            if (smsSchedulerStatus == 1) {

                ThrottleConstants.PROPS_VALUES = loadProps();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Calendar c = Calendar.getInstance();
                Date date = new Date();
                c.setTime(date);
                String systemTime = sdf.format(c.getTime()).toString();
                String[] dateTemp = systemTime.split(" ");
                String[] timeTemp = dateTemp[1].split(":");

                runHour = Integer.parseInt(timeTemp[0]);
                runMinute = Integer.parseInt(timeTemp[1]);

                System.out.println("runHour :" + runHour);
                System.out.println("runMinute :" + runMinute);

                serverIpAddress = InetAddress.getLocalHost().getHostAddress();
                ServletContext servletContext = this.getServletContext();
                String path = servletContext.getRealPath(File.separator);

//                System.out.println("path = " + path);
//                System.out.println("serverIpAddress = " + serverIpAddress);
//                System.out.println("smsSchedulerStatus:" + smsSchedulerStatus);
                String nodAlert = ThrottleConstants.NODReportTime;
                String dsrAlert = ThrottleConstants.DSRReportTime;
                String podAlert = ThrottleConstants.PODReportTime;

                String[] daTimeTemp = nodAlert.split(":");
                String[] dsrTimeTemp = dsrAlert.split(":");
                String[] podTimeTemp = podAlert.split(":");

                System.out.println("nodAlert:" + nodAlert);
                System.out.println("dsrAlert:" + dsrAlert);

                                //////////////////////////////////////// NOD REPORT /////////////////////////////////////////////////////
                if (runHour == Integer.parseInt(daTimeTemp[0]) && runMinute == Integer.parseInt(daTimeTemp[1])) {

                    System.out.println("NOD :" + Integer.parseInt(daTimeTemp[0]));
                    System.out.println("NOD :" + Integer.parseInt(daTimeTemp[1]));

                    reportTO = new ReportTO();

                    String content = reportBP.getDailyNodReportAlert(reportTO);

                    reportTO.setMailTypeId("1");
                    reportTO.setMailSubjectTo("NOD Report");
                    reportTO.setMailSubjectCc("NOD Report");
                    reportTO.setMailContentTo(content);
                    reportTO.setMailContentCc(content);
                    reportTO.setReporttype("NOD");

                    reportTO.setMailIdTo("vivek.jaganathan@kerryindev.com, arunkumar.kanagaraj@newage-global.com");
                    reportTO.setMailIdCc("vivek.jaganathan@kerryindev.com, arunkumar.kanagaraj@newage-global.com");

//                    int insertStatus = reportBP.insertMailDetails(reportTO, 1403);
//                    System.out.println("NOD insertStatus :::: " + insertStatus);
                }

                //////////////////////////////////////// NOD REPORT /////////////////////////////////////////////////////
                //////////////////////////////////////// DSR REPORT /////////////////////////////////////////////////////
                if (runHour == Integer.parseInt(dsrTimeTemp[0]) && runMinute == Integer.parseInt(dsrTimeTemp[1])) {

                    // if(true){
                    System.out.println("DSR Hour :" + Integer.parseInt(dsrTimeTemp[0]));
                    System.out.println("DSR Minitute :" + Integer.parseInt(dsrTimeTemp[1]));

                    String content = "";
                    String[] tempContent = null;
                    ArrayList custList = new ArrayList();
                    ReportTO repTO = new ReportTO();

                    custList = reportBP.getEmailCustomerList();
                    System.out.println("DSR custList : " + custList.size());
                    Iterator itr = custList.iterator();

                    while (itr.hasNext()) {
                        repTO = (ReportTO) itr.next();
                        content = reportBP.getCustomerDSRReportAlert(repTO);
//                        System.out.println("DSR content = " + content);

                        if (!"".equals(content)) {

                            tempContent = content.split("~");

                            reportTO.setMailSubjectTo("DSR Report For " + repTO.getCustomerName());
                            reportTO.setMailSubjectCc("DSR Report For " + repTO.getCustomerName());

                            reportTO.setMailTypeId("2");
                            reportTO.setMailContentTo(tempContent[0]);
                            reportTO.setMailContentCc(tempContent[0]);
                            reportTO.setMailIdTo(repTO.getMailSubjectTo());
                            reportTO.setMailIdCc(repTO.getMailSubjectCc());
                            reportTO.setFileName(tempContent[1]);
                            reportTO.setFilePath(tempContent[2]);

                            int insertStatus = reportBP.insertMailDetails(reportTO, 1403);
                            System.out.println("insertStatus==" + insertStatus);

                        }
                    }
                }
                
                //////////////////////////////////////// DSR REPORT /////////////////////////////////////////////////////
                
                //////////////////////////////////////// Trip end REPORT ////////////////////////////////////////////////

                String tripEndAlert = ThrottleConstants.tripEndReportTime;
                String[] tripEndTimeTemp = tripEndAlert.split(":");

                if (runHour == Integer.parseInt(tripEndTimeTemp[0]) && runMinute == Integer.parseInt(tripEndTimeTemp[1])) {

                    // if(true){
                    System.out.println("TripEnd Hour :" + Integer.parseInt(dsrTimeTemp[0]));
                    System.out.println("TripEnd Minitute :" + Integer.parseInt(dsrTimeTemp[1]));

                    String content = "";
                    String[] tempContent = null;
                    ReportTO repTO = new ReportTO();

                    content = reportBP.getTripEndReportAlert();
                    System.out.println("trip end content = " + content);

                    if (!"".equals(content)) {

                        tempContent = content.split("~");

                        reportTO.setMailSubjectTo("Trip End Report ");
                        reportTO.setMailSubjectCc("Trip End Report ");

                        reportTO.setMailTypeId("3");
                        reportTO.setMailContentTo(tempContent[0]);
                        reportTO.setMailContentCc(tempContent[0]);
                        reportTO.setMailIdTo("vivek.jaganathan@kerryindev.com");
                        reportTO.setMailIdCc("arunkumar.kanagaraj@newage-global.com");
                        reportTO.setFileName(tempContent[1]);
                        reportTO.setFilePath(tempContent[2]);

                        int insertStatus = reportBP.insertMailDetails(reportTO, 1403);
                        System.out.println("insertStatus==" + insertStatus);

                    }
                }

                //////////////////////////////////////// Trip end REPORT ////////////////////////////////////////////////
                
                //////////////////////////////////////// POD REPORT /////////////////////////////////////////////////////
                
                if (runHour == Integer.parseInt(podTimeTemp[0]) && runMinute == Integer.parseInt(podTimeTemp[1])) {

                    String content = "";
                    String[] tempContent = null;

                    System.out.println("POD Hour :" + Integer.parseInt(podTimeTemp[0]));
                    System.out.println("POD Minitute :" + Integer.parseInt(podTimeTemp[1]));

                    //  content = reportBP.getTripPODReportAlert(reportTO);
                    System.out.println("content = " + content);

                    if (!"".equals(content)) {

                        tempContent = content.split("~");

                        reportTO.setMailTypeId("3");
                        reportTO.setMailSubjectTo("Trip POD Report");
                        reportTO.setMailSubjectCc("Trip POD Report");

                        reportTO.setMailContentTo(tempContent[0]);
                        reportTO.setMailContentCc(tempContent[0]);

                        System.out.println("tempContent[0] : " + tempContent[0]);
                        System.out.println("tempContent[1] : " + tempContent[1]);
                        System.out.println("tempContent[2] : " + tempContent[2]);

                        reportTO.setMailIdTo(ThrottleConstants.pnrUploadToMailId);
                        reportTO.setMailIdCc(ThrottleConstants.podUploadCcMailId);
                        reportTO.setFileName(tempContent[1]);
                        reportTO.setFilePath(tempContent[2]);

                        //int insertStatus = reportBP.insertMailDetails(reportTO, 1403);
                        //System.out.println("insertStatus==" + insertStatus);
                    }
                }

                //////////////////////////////////////// POD REPORT /////////////////////////////////////////////////////
                //////////////////////////////////////// TRIP DATA /////////////////////////////////////////////////////
                if (runMinute == 5 || runMinute == 15 || runMinute == 25 || runMinute == 35 || runMinute == 45 || runMinute == 55) {

                   boolean callTripData = reportBP.tripData();
                   System.out.println("call callTripData : " + callTripData);

                }

                //////////////////////////////////////// GPS DATA /////////////////////////////////////////////////////
                if (runMinute == 10 || runMinute == 20 || runMinute == 30 || runMinute == 40 || runMinute == 50 || runMinute == 59) {

                    boolean callGPS = reportBP.getGPSData();
                    System.out.println("call GPS : " + callGPS);

                }

                //////////////////////////////////////// Consignment Process  /////////////////////////////////////////////////////
                // String callConsignment = reportBP.getCallConsignment();               
                //////////////////////////////////////// Consignment Process  /////////////////////////////////////////////////////
                ArrayList emailList = new ArrayList();
                Iterator itr = null;

                try {
                    emailList = reportBP.getEmailList();
                    System.out.println("emailList ------------- " + emailList);
                    System.out.println("emailList ------------- " + emailList.size());
                    itr = emailList.iterator();
                    while (itr.hasNext()) {
                        reportTO1 = (ReportTO) itr.next();
                        System.out.println("reportTO ----- mail contenet  " + reportTO1.getMailContentTo());
                        // System.out.println("applicationServer ----------- " + applicationServer);
                        //reportTO1.setServerName("Kerry");
                        // reportTO1.setServerIpAddress(serverIpAddress);
                        new EmailSSL(reportTO1).start();
                    }

                } catch (Exception exp) {
                    //    exp.printStackTrace();

                    ReportTO rTO = new ReportTO();
                    rTO.setMailSubjectTo("Email not delivered");
                    rTO.setMailContentTo(exp.getMessage());
                    rTO.setMailIdTo(ThrottleConstants.developerSupport);
                    rTO.setMailIdCc("throttleerror@gmail.com");

                    rTO = null;
                } finally {
                    emailList = null;
                    itr = null;
                }

            }

        } catch (Exception ex) {
            //ex.printStackTrace();
        } finally {
            reportTO = null;

        }

    }

    public HashMap<String, String> loadProps() throws FPRuntimeException, FPBusinessException {

        try {

            ArrayList list = new ArrayList();
            list = loginBP.LoadProps();
            ThrottleConstants.PROPS_VALUES = new HashMap<String, String>();
            //System.out.println("list -------" + list);
            for (Iterator it = list.iterator(); it.hasNext();) {
                LoginTO object = (LoginTO) it.next();
                try {
                    ThrottleConstants.PROPS_VALUES.put(object.getKeyName().trim(), object.getValue().trim());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return ThrottleConstants.PROPS_VALUES;
    }

    public void invoiceUnpostedProcess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdfNew = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        String fromDate = sdfNew.format(c.getTime()).toString();
        System.out.println("fromDate = " + fromDate);
        System.out.println("systemTime = " + systemTime);
        String[] dateTemp = systemTime.split(" ");
        String[] timeTemp = dateTemp[1].split(":");
        int hours = 0;
        int min = 0;
        if (timeTemp[0] != null) {
            hours = Integer.parseInt(timeTemp[0]);
            min = Integer.parseInt(timeTemp[1]);
        }

        try {

            System.out.println(" System hours" + hours);
            System.out.println(" System min" + min);
            System.out.println(" invoice unpost start hours" + ThrottleConstants.emailAlertExcludingStartHour);
            System.out.println(" invoice unpost  end hours" + ThrottleConstants.emailAlertExcludingEndHour);

            if ((hours >= 0) && (hours <= 23) && min == 10) {

                ReportTO reportTO = new ReportTO();

                ArrayList invoiceUnpostedReport = new ArrayList();
                invoiceUnpostedReport = reportBP.invoicePostDetails();
                Iterator itr = invoiceUnpostedReport.iterator();
                System.out.println("invoiceUnpostPath : " + invoiceUnpostedReport.size());

                while (itr.hasNext()) {
                    reportTO = (ReportTO) itr.next();
                    String erpId = "";
                    String invoiceNo = "";
                    String invoiceDate = "";
                    String totFreightChargeRound = "";
                    String cgst = "";
                    String cgstRoundAmt = "";
                    String sgst = "";
                    String sgstRoundAmt = "";
                    String igst = "";
                    String igstRoundAmt = "";
                    String totalWaybillRound = "";
                    String custGstNo = "";
                    String reportType = "";
                    String consignorCode = "";
                    String consigneeCode = "";

                    int postId = 0;

                    invoiceNo = reportTO.getInvoiceNo();
                    invoiceDate = reportTO.getInvoiceDate();
                    custGstNo = reportTO.getCustomerGSTNo();
                    totFreightChargeRound = reportTO.getFreightAmount();
                    cgst = reportTO.getCgstAmt();
                    sgst = reportTO.getSgstAmt();
                    igst = reportTO.getIgstAmt();
                    cgstRoundAmt = reportTO.getCgst();
                    sgstRoundAmt = reportTO.getSgst();
                    igstRoundAmt = reportTO.getIgst();
                    totalWaybillRound = reportTO.getTotalAmount();
                    postId = reportTO.getPostId();
                    erpId = reportTO.getCustomerErpId();
                    reportType = reportTO.getCustomerType();
                    consignorCode = reportTO.getConsignorCode();
                    consigneeCode = reportTO.getConsigneeCode();

                    if (reportType.equalsIgnoreCase("customer")) {

                        new sendInvoice(
                                erpId, invoiceNo, invoiceDate,
                                totFreightChargeRound, totFreightChargeRound,
                                cgst, cgstRoundAmt,
                                sgst, sgstRoundAmt,
                                igst, igstRoundAmt,
                                totalWaybillRound, custGstNo, postId, consignorCode, consigneeCode).start();
                    } else {

                        new sendVendorInvoice(
                                erpId, invoiceNo, invoiceDate,
                                totFreightChargeRound, totFreightChargeRound,
                                cgst, cgstRoundAmt,
                                sgst, sgstRoundAmt,
                                igst, igstRoundAmt,
                                totalWaybillRound, custGstNo, postId).start();
                    }

                }

            }

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to search Course sems--> " + exception);
            exception.printStackTrace();
        } finally {
            sdf = null;
            sdfNew = null;
            date = null;
            c = null;
            systemTime = null;
            fromDate = null;
            dateTemp = null;
            timeTemp = null;

        }
    }

    public void advanceUnpostedProcess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdfNew = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        String fromDate = sdfNew.format(c.getTime()).toString();
        System.out.println("fromDate = " + fromDate);
        System.out.println("systemTime = " + systemTime);
        String[] dateTemp = systemTime.split(" ");
        String[] timeTemp = dateTemp[1].split(":");
        int hours = 0;
        int min = 0;
        if (timeTemp[0] != null) {
            hours = Integer.parseInt(timeTemp[0]);
            min = Integer.parseInt(timeTemp[1]);
        }

        try {

            System.out.println(" System hours" + hours);
            System.out.println(" System min" + min);
            System.out.println(" invoice unpost start hours" + ThrottleConstants.emailAlertExcludingStartHour);
            System.out.println(" invoice unpost  end hours" + ThrottleConstants.emailAlertExcludingEndHour);

            if ((hours >= 0) && (hours <= 23) && min == 10) {

                ReportTO reportTO = new ReportTO();

                ArrayList advanceUnpostedReport = new ArrayList();
                advanceUnpostedReport = reportBP.advanceUnpostedReport();
                Iterator itr = advanceUnpostedReport.iterator();
                System.out.println("advanceUnpostedReport : " + advanceUnpostedReport.size());

                while (itr.hasNext()) {
                    reportTO = (ReportTO) itr.next();

                    int postId = 0;
                    int advid = 0;
                    String eFSId = "";
                    String tripid = "";
                    String advance_amount = "";
                    String adv_status = "";
                    String remarks = "";
                    String emailDetails = "";

                    tripid = reportTO.getTripId();
                    advid = reportTO.getAdvanceId();
                    advance_amount = reportTO.getRequestAmount();
                    adv_status = reportTO.getStatus();
                    remarks = reportTO.getRemarks();
                    emailDetails = reportTO.getEmailDetails();

                    postId = reportTO.getPostId();
                    eFSId = reportTO.getCustomerErpId();

                    new sendService(eFSId, tripid, advid + "", advance_amount, adv_status, remarks, emailDetails, postId).start();

                }

            }

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to search Course sems--> " + exception);
            exception.printStackTrace();
        } finally {
            sdf = null;
            sdfNew = null;
            date = null;
            c = null;
            systemTime = null;
            fromDate = null;
            dateTemp = null;
            timeTemp = null;

        }
    }

}
