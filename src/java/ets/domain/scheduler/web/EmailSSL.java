/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.scheduler.web;

import ets.domain.report.business.ReportTO;
import ets.domain.thread.web.SendEmail;
import ets.domain.trip.business.TripTO;
import ets.domain.util.ThrottleConstants;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import org.apache.commons.mail.EmailException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 *
 * @author pavithra
 */
class EmailSSL extends Thread {

    String SMTP = "";
    int PORT = 0;
    String FromEmailId = "";
    String Password = "";
    String EmailSubject = "";
    String BodyMessage = "";
    String Toemailid = "";
    String CCemailid = "";
    String BCCemailid = "";
    int Status = 0;
    String MailSendingId = "";
    String UserId = "";
    String ServerName = "";
    String ServerIpAddress = "";
    HSSFWorkbook myWorkbook = null;
    String fileName = "";
    String filePath = "";
    String mailTypeId = "";
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

    public EmailSSL(ReportTO reportTO) {
        SMTP = ThrottleConstants.smtpServer;
        PORT = Integer.parseInt(ThrottleConstants.smtpPort);
        FromEmailId = ThrottleConstants.fromMailId;
        UserId = ThrottleConstants.fromMailId;
        Password = ThrottleConstants.fromMailPassword;
        EmailSubject = reportTO.getMailSubjectTo();
        BodyMessage = reportTO.getMailContentTo();
        Toemailid = reportTO.getMailIdTo();
        CCemailid = reportTO.getMailIdCc();
        BCCemailid = reportTO.getMailIdBcc();
        MailSendingId = reportTO.getMailSendingId();
        ServerName = "";
        ServerIpAddress = "";
        myWorkbook = reportTO.getMyWorkbook();
        fileName = reportTO.getFileName();
        filePath = reportTO.getFilePath();
        mailTypeId = reportTO.getMailTypeId();
        outputStream = reportTO.getOutputStream();
    }

    public void run() {

        try {

            Properties props = new Properties();

            props.put("mail.smtp.host", SMTP);
            //props.put("mail.smtp.socketFactory.port", PORT);
//            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.port", "587");
            props.put("mail.debug", "true");
            props.put("mail.smtp.ehlo","true");
            props.put("mail.transport.protocol", "smtp");
            
            

            Toemailid = Toemailid.startsWith(",") ? Toemailid.replaceFirst(",", "") : Toemailid;
            CCemailid = CCemailid.startsWith(",") ? CCemailid.replaceFirst(",", "") : CCemailid;
            if (BCCemailid != null) {
                BCCemailid = BCCemailid.startsWith(",") ? BCCemailid.replaceFirst(",", "") : BCCemailid;
            }
            // System.out.println("ServerName = " + ServerName);
            boolean isLiveServer = false;
//            if (ServerIpAddress.equals(ThrottleConstants.serverIpAddress)) {
//                isLiveServer = true;
//            }
            if (!isLiveServer) {
                System.out.println("Email sent Successfully....");
                String fileNameNew = "jdbc_url.properties";
                Properties dbProps = new Properties();
                InputStream is = getClass().getResourceAsStream("/" + fileNameNew);
                dbProps.load(is);//this may throw IOException
                String dbClassName = dbProps.getProperty("jdbc.driverClassName");
                String dbUrl = dbProps.getProperty("jdbc.url");
                String dbUserName = dbProps.getProperty("jdbc.username");
                String dbPassword = dbProps.getProperty("jdbc.password");
                int updateStatus = 0;
                Connection con = null;
                try {
                    Class.forName(dbClassName).newInstance();
                    con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
                    try {
                        PreparedStatement updateMailDetails = null;
                        String sql = "UPDATE ts_mail_master SET \n"
                                + " Mail_Deliveried_Status = ?,  \n"
                                + " Mail_Delivered_Log = ?, \n"
                                + " Mail_Resending_Count = IFNULL(Mail_Resending_Count,0)+1 \n"
                                //                            + " MAIL_DELIVERED_TIME = now() \n"
                                + " WHERE  MAIL_SENDING_ID = ?";
                        updateMailDetails = con.prepareStatement(sql);
                        updateMailDetails.setInt(1, 2);//2 indidates that this record is in process
                        updateMailDetails.setString(2, "In Progress");
                        updateMailDetails.setString(3, MailSendingId);
                        updateStatus = updateMailDetails.executeUpdate();
                        System.out.println("NEW CHANGES FOR INPROGRESS = " + updateStatus);

                    } catch (SQLException e) {
                        // e.printStackTrace();
                        // System.out.println("Mail Not Sent.");
                    } finally {
                        con.close();
                    }
                } catch (Exception e) {
                    //  e.printStackTrace();
                }

                String[] to = Toemailid.split(","); // added this line

                Session session = Session.getDefaultInstance(props, null);
                session.setDebug(true);
                MimeMessage message = new MimeMessage(session);

                try {

                    if ("1".equals(mailTypeId)) {
                        message.setFrom(new InternetAddress(FromEmailId, "NOD Alerts"));
                    } else if ("2".equals(mailTypeId)) {
                        message.setFrom(new InternetAddress(FromEmailId, "DSR Alerts"));
                    } else if ("3".equals(mailTypeId)) {
                        message.setFrom(new InternetAddress(FromEmailId, "Trip End Alerts"));
                    } else if ("9".equals(mailTypeId) || "10".equals(mailTypeId)) {
                        message.setFrom(new InternetAddress(FromEmailId, "Mofussil Alerts"));
                    } else {
                        message.setFrom(new InternetAddress(FromEmailId, "PNR Alerts"));
                    }

                } catch (UnsupportedEncodingException ex) {
                    Logger.getLogger(SendEmail.class.getName()).log(Level.SEVERE, null, ex);
                }
                InternetAddress[] toAddress = new InternetAddress[to.length];

                if (!Toemailid.equals("") && !Toemailid.equals("-")) {
                    for (int i = 0; i < to.length; i++) { // changed from a while loop
                        toAddress[i] = new InternetAddress(to[i]);
                    }
                    for (int i = 0; i < toAddress.length; i++) { // changed from a while loop
                        message.addRecipient(Message.RecipientType.TO, toAddress[i]);
                    }
                }

                ////////////// cc part////////////////////
                if (!CCemailid.equals("-") && !CCemailid.equals("")) {
                    String[] cc = CCemailid.split(","); // added this line
                    int ccLen = cc.length;
                    InternetAddress[] ccAddress = new InternetAddress[ccLen];
                    int j = 0;
                    for (j = 0; (j < cc.length && !CCemailid.equals("")); j++) { // changed from a while loop
                        ccAddress[j] = new InternetAddress(cc[j]);
                    }
                    for (j = 0; j < ccAddress.length; j++) { // changed from a while loop
                        message.addRecipient(Message.RecipientType.CC, ccAddress[j]);
                    }
                }

                ////////////// Bcc part////////////////////
                if (BCCemailid != null && (BCCemailid.equals("-") || BCCemailid.equals(""))) {
                    BCCemailid = BCCemailid;
                } else {
                    BCCemailid = "arun@entitlesolutions.com";
                }

                if (BCCemailid != null && (!BCCemailid.equals("-"))) {
                    String[] bcc = BCCemailid.split(","); // added this line
                    int bccLen = bcc.length;
                    InternetAddress[] bccAddress = new InternetAddress[bccLen];
                    int j = 0;
                    for (j = 0; (j < bcc.length && !BCCemailid.equals("")); j++) { // changed from a while loop
                        bccAddress[j] = new InternetAddress(bcc[j]);
                    }
                    for (j = 0; j < bccAddress.length; j++) { // changed from a while loop
                        message.addRecipient(Message.RecipientType.BCC, bccAddress[j]);
                    }

                }

                System.out.println("Toemailid1:" + Toemailid);
                System.out.println("CCemailid1:" + CCemailid);
                System.out.println("BCCemailid1:" + BCCemailid);

                /* Excel File Mail*/
                System.out.println("EmailSubject" + EmailSubject);
                System.out.println("fileName---------------------   " + fileName);
                if (fileName.contains("xlsx") || fileName.contains("xls")) {
//                    javax.mail.util.ByteArrayDataSource ds = null;
//                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                    myWorkbook.write(baos);
//                    byte[] bytes = baos.toByteArray();
//                    try {
//                        ds = new ByteArrayDataSource(bytes, "application/excel");
//                    } catch (Exception err) {
//                        err.printStackTrace();
//                    }

                    //DataHandler dh = new DataHandler(ds);
                    //messageBodyPart.setDataHandler(dh);                    
                    System.out.println("if*********");

                    MimeMultipart multipart = new MimeMultipart();
                    BodyPart messageBodyPart = new MimeBodyPart();
                    BodyPart messageBodyPart1 = new MimeBodyPart();
                    messageBodyPart1.setContent(BodyMessage, "text/html");

                    DataSource dataSource = new FileDataSource(filePath);
                    messageBodyPart.setDataHandler(new DataHandler(dataSource));
                    messageBodyPart.setFileName(new File(filePath).getName());
                    //messageBodyPart.setContent(BodyMessage, "application/excel");
                    multipart.addBodyPart(messageBodyPart);
                    multipart.addBodyPart(messageBodyPart1);
                    message.setContent(multipart);

                } else if (fileName.contains("pdf")) {
                    //construct the pdf body part
                    BodyPart messageBodyPart = new MimeBodyPart();
                    messageBodyPart.setContent(BodyMessage, "text/html");
                    MimeMultipart multipart = new MimeMultipart();
                    multipart.addBodyPart(messageBodyPart);
                    messageBodyPart = new MimeBodyPart();
                    DataSource dataSource = new FileDataSource(filePath);
                    messageBodyPart.setDataHandler(new DataHandler(dataSource));
                    messageBodyPart.setFileName(new File(filePath).getName());
                    multipart.addBodyPart(messageBodyPart);
                    message.setContent(multipart);

                } else {
                    System.out.println("else*********");
                    message.setContent(BodyMessage, "text/html");
                }
                    
                    System.out.println("user id --------------- " + UserId);
                    System.out.println("Password ------------------ " + Password);
                    
                    message.setSubject(EmailSubject);
                try {
                    Transport transport = session.getTransport("smtp");
                    transport.connect(SMTP, UserId, Password);
                    transport.sendMessage(message, message.getAllRecipients());
                    transport.close();
                
                
                System.out.println("Email sent Successfully....");
                fileName = "jdbc_url.properties";
                dbProps = new Properties();
                updateStatus = 0;
                con = null;

                    Class.forName(dbClassName).newInstance();
                    con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
                    try {
                        System.out.println("******-------*************");
                        PreparedStatement updateMailDetails = null;
                        String sql = "UPDATE ts_mail_master SET \n"
                                + " Mail_Deliveried_Status = ?,  \n"
                                + " Mail_Delivered_Log = ?, \n"
                                + " Mail_Resending_Count = IFNULL(Mail_Resending_Count,0)+1 \n"
                                //                            + " MAIL_DELIVERED_TIME = now() \n"
                                + " WHERE  MAIL_SENDING_ID = ?";
                        updateMailDetails = con.prepareStatement(sql);
                        updateMailDetails.setInt(1, 1);
                        updateMailDetails.setString(2, "Delivered Sucessfully");
                        updateMailDetails.setString(3, MailSendingId);
                        updateStatus = updateMailDetails.executeUpdate();
//                    System.out.println("updateStatus = " + updateStatus);

                    } catch (SQLException e) {
                        // e.printStackTrace();
                        // System.out.println("Table doesn't exist.");
                    } finally {
                        con.close();
                    }
                } catch (Exception e) {

                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            String exp = e.toString();
            System.out.println("Unable to send Email....");
//          TripTO tripTO = new TripTO();
            ReportTO reportTO = new ReportTO();
            int userId = 1081;
            Status = 0;
            Status = 1;
            String fileName = "jdbc_url.properties";
            Properties dbProps = new Properties();
            InputStream is = getClass().getResourceAsStream("/" + fileName);

            try {
                dbProps.load(is);//this may throw IOException
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            String dbClassName = dbProps.getProperty("jdbc.driverClassName");
            String dbUrl = dbProps.getProperty("jdbc.url");
            String dbUserName = dbProps.getProperty("jdbc.username");
            String dbPassword = dbProps.getProperty("jdbc.password");
            int updateStatus = 0;
            Connection con = null;

            try {
                Class.forName(dbClassName).newInstance();
                con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
                try {
                    PreparedStatement updateMailDetails = null;
                    String sql = "UPDATE ts_mail_master SET \n"
                            + " Mail_Deliveried_Status = ?,  \n"
                            + " Mail_Delivered_Log = ?, \n"
                            + " Mail_Resending_Count = IFNULL(Mail_Resending_Count,0)+1, \n"
                            + " MAIL_DELIVERED_TIME = now() \n"
                            + " WHERE  MAIL_SENDING_ID = ?";
                    updateMailDetails = con.prepareStatement(sql);
                    updateMailDetails.setInt(1, 0);
                    updateMailDetails.setString(2, exp);
                    updateMailDetails.setString(3, MailSendingId);
                    updateStatus = updateMailDetails.executeUpdate();
//                    System.out.println("updateStatus = " + updateStatus);

                } catch (SQLException ex) {
                    //e.printStackTrace();
                    System.out.println("Table doesn't exist.");
                } finally {
                    con.close();
                }
            } catch (Exception ex) {
                // e.printStackTrace();
            }
        }

    }

    public static void main(String args[]) throws IOException, EmailException {

        String subject = "Entitle";
        String content = "Welcome";

        String to = "aruninfoit84@gmail.com";
        String cc = "";
        String smtp = "smtp.gmail.com";
        int emailPort = 587;
//      String frommailid = "transport@brattlefoods.com";
//      String password = "Brattle@12345";
        String frommailid = "";
        String password = "";
        int mailSendingId = 0;
//      WayBillTO wayBillTO = new WayBillTO();
        ReportTO reportTO = new ReportTO();
//      new SendEmailDetails(wayBillTO).start();

    }

}
