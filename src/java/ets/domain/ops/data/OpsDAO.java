/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ets.domain.ops.data;

import java.lang.*;
import com.ibatis.sqlmap.client.SqlMapClient;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.operation.business.OperationTO;
import ets.domain.ops.business.OpsTO;
import ets.domain.report.business.ReportTO;
import ets.domain.util.FPLogUtils;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.List;
import java.util.Calendar;
import java.sql.Timestamp;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

/**
 *
 * @author vijay
 */
public class OpsDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "OpsDAO";

    public ArrayList getVehicleTypeList() {
        Map map = new HashMap();
        ArrayList serviceTypes = new ArrayList();
        try {
            serviceTypes = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getServiceTypes", map);
            System.out.println("serviceTypes size=" + serviceTypes.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getServiceTypes Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getServiceTypes List", sqlException);
        }

        return serviceTypes;
    }

    /**
     * This method used to Get City Name.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getCityList(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList cityList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("cityName", opsTO.getCityName().trim() + "%");
        if (opsTO.getPreviousCityId() != null && !"".equals(opsTO.getPreviousCityId())) {
            map.put("pointId", opsTO.getPreviousCityId());
        }
        System.out.println("map = " + map);
        try {
            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getCityList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCityList", sqlException);
        }
        return cityList;
    }

    public ArrayList getDestinationCityList(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList cityList = new ArrayList();
        map.put("pointId", opsTO.getPreviousCityId());
        System.out.println("map = " + map);
        try {
            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getDestinationCityList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCityList", sqlException);
        }
        return cityList;
    }

    public ArrayList getAirLineName(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList cityList = new ArrayList();
        map.put("lineId", opsTO.getPreviousCityId());
        System.out.println("map = " + map);
        try {
            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getAirLineName", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCityList", sqlException);
        }
        return cityList;
    }

    public ArrayList getCityCodeList(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList cityList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("cityCode", opsTO.getCityCode() + "%");
        System.out.println("map = " + map);
        try {
            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getCityCodeList", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityCodeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCityCodeList", sqlException);
        }
        return cityList;
    }

    public ArrayList getCityCodeList1(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList cityList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("cityCode", opsTO.getCityCode() + "%");
        System.out.println("map = " + map);
        try {
            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getCityCodeList1", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityCodeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCityCodeList", sqlException);
        }
        return cityList;
    }

    /**
     * This method used to Get Chargeable Weight.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getChargeableWeight(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList chargeableWeight = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        if (opsTO.getLengthCm() != null && !"".equals(opsTO.getLengthCm())) {
            map.put("length", opsTO.getLengthCm() + "%");
        }
        if (opsTO.getWidthCm() != null && !"".equals(opsTO.getWidthCm())) {
            map.put("width", opsTO.getWidthCm() + "%");
        }
        if (opsTO.getHeightCm() != null && !"".equals(opsTO.getHeightCm())) {
            map.put("height", opsTO.getHeightCm() + "%");
        }
        if (opsTO.getUom() != null && !"".equals(opsTO.getUom())) {
            map.put("uom", opsTO.getUom());
        }
        System.out.println("map = " + map);
        try {
            chargeableWeight = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getChargeableWeight", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getChargeableWeight Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getChargeableWeight", sqlException);
        }
        return chargeableWeight;
    }

    public ArrayList getVehTypeVehicleRegNo(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList vehicleRegNo = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */

        if (opsTO.getVehicleTypeId() != null && !"".equals(opsTO.getVehicleTypeId())) {
            map.put("vehicleTypeId", opsTO.getVehicleTypeId());
        }
        if (opsTO.getVehicleRegNo() != null && !"".equals(opsTO.getVehicleRegNo())) {
            map.put("vehicleRegNo", opsTO.getVehicleRegNo() + "%");
        }
        System.out.println("map = " + map);
        try {
            vehicleRegNo = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getVehTypeVehicleRegNo", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehTypeVehicleRegNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehTypeVehicleRegNo", sqlException);
        }
        return vehicleRegNo;
    }

    public int insertCustomerContractDetails(OpsTO opsTO, SqlMapClient session) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertCustomerContract = 0;
        int count = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("contractId", opsTO.getContractId());
        map.put("contractNo", opsTO.getContractNo());
        map.put("customerId", opsTO.getCustomerId());
        map.put("billingTypeId", opsTO.getBillingTypeId());
        map.put("fromDate", opsTO.getContractFrom());
        map.put("toDate", opsTO.getContractTo());
        map.put("activeInd", "Y");
        map.put("userId", opsTO.getUserId());
        System.out.println("map = " + map);
        try {
//            count = (Integer) session.queryForObject("ops.checkCustomerContractId",map);
//            if(count == 0){
            insertCustomerContract = (Integer) session.insert("ops.insertCustomerContractDetails", map);
//            }else{
//            insertCustomerContract = (Integer) session.update("ops.updateCustomerContractDetails", map);
//            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertCustomerContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertCustomerContractDetails", sqlException);
        }
        return insertCustomerContract;
    }

    public int insertContractFullTruck(OpsTO opsTO, SqlMapClient session) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertCustomerContractRoute = 0;
        int insertCustomerContractRate = 0;
        /*
         * set the parameters in the map for sending to ORM
         */

        map.put("userId", opsTO.getUserId());
        map.put("contractId", opsTO.getContractId());
        map.put("contractFrom", opsTO.getContractFrom());
        try {
            if (opsTO.getOriginIdFullTruck() != null && !"".equals(opsTO.getOriginIdFullTruck())) {
                if (opsTO.getOriginIdFullTruck().length > 0) {
                    for (int i = 0; i < opsTO.getOriginIdFullTruck().length; i++) {
                        if (!"".equals(opsTO.getOriginIdFullTruck()[i]) && !"".equals(opsTO.getOriginNameFullTruck()[i]) && !"".equals(opsTO.getDestinationIdFullTruck()[i]) && !"".equals(opsTO.getDestinationNameFullTruck()[i]) && !"".equals(opsTO.getRouteIdFullTruck()[i]) && !"".equals(opsTO.getTravelKmFullTruck()[i]) && !"".equals(opsTO.getTravelHourFullTruck()[i]) && !"".equals(opsTO.getTravelMinuteFullTruck()[i])) {
                            map.put("routeContractCode", opsTO.getOriginNameFullTruck()[i] + "-" + opsTO.getDestinationNameFullTruck()[i]);
                            map.put("originId", opsTO.getOriginIdFullTruck()[i]);
                            map.put("originName", opsTO.getOriginNameFullTruck()[i]);
                            map.put("destinationId", opsTO.getDestinationIdFullTruck()[i]);
                            map.put("destinationName", opsTO.getDestinationNameFullTruck()[i]);
                            map.put("routeId", opsTO.getRouteIdFullTruck()[i]);
                            map.put("travelKm", opsTO.getTravelKmFullTruck()[i]);
                            map.put("travelHour", opsTO.getTravelHourFullTruck()[i]);
                            map.put("travelMinute", opsTO.getTravelMinuteFullTruck()[i]);
                            map.put("totalPoints", 1);
                            String routeId = "";
                            routeId = (String) session.queryForObject("ops.checkRouteContractId", map);
                            if (routeId != null) {
                                insertCustomerContractRoute = Integer.parseInt(routeId);
                            } else {
                                insertCustomerContractRoute = (Integer) session.insert("ops.insertContractRoute", map);
                            }
                            if (opsTO.getMovementTypeIdFullTruck() != null && !"".equals(opsTO.getMovementTypeIdFullTruck())) {
                                if (opsTO.getMovementTypeIdFullTruck().length > 0) {
                                    for (int j = 0; j < opsTO.getMovementTypeIdFullTruck().length; j++) {
                                        if (!"0".equals(opsTO.getMovementTypeIdFullTruck()[j]) && !"0".equals(opsTO.getVehicleTypeIdFullTruck()[j]) && !"".equals(opsTO.getRateWithReeferFullTruck()[j]) && !"".equals(opsTO.getRateWithOutReeferFullTruck()[j])) {
                                            map.put("movementTypeId", opsTO.getMovementTypeIdFullTruck()[j]);
                                            map.put("vehicleTypeId", opsTO.getVehicleTypeIdFullTruck()[j]);
                                            map.put("rateWithReefer", opsTO.getRateWithReeferFullTruck()[j]);
                                            map.put("rateWithOutReefer", opsTO.getRateWithOutReeferFullTruck()[j]);
                                            map.put("contractRouteId", insertCustomerContractRoute);
                                            String contractId = "";
                                            contractId = (String) session.queryForObject("ops.checkRateContractId", map);
                                            if (contractId != null) {
                                                System.out.println("map =dfgsdfg");
                                            } else {
                                                insertCustomerContractRate = (Integer) session.update("ops.insertContractFullTruckRate", map);
                                            }

                                            System.out.println("map = " + map);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertCustomerContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertCustomerContractDetails", sqlException);
        }
        return insertCustomerContractRoute;
    }

    public int insertContractWeightBreak(OpsTO opsTO, SqlMapClient session) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertCustomerContractRoute = 0;
        int insertCustomerContractRate = 0;
        /*
         * set the parameters in the map for sending to ORM
         */

        map.put("userId", opsTO.getUserId());
        map.put("contractId", opsTO.getContractId());
        map.put("contractFrom", opsTO.getContractFrom());
        try {
            if (opsTO.getOriginIdWeightBreak() != null && !"".equals(opsTO.getOriginIdWeightBreak())) {
                if (opsTO.getOriginIdWeightBreak().length > 0) {
                    for (int i = 0; i < opsTO.getOriginIdWeightBreak().length; i++) {
                        if (!"".equals(opsTO.getOriginIdWeightBreak()[i]) && !"".equals(opsTO.getOriginNameWeightBreak()[i]) && !"".equals(opsTO.getDestinationIdWeightBreak()[i]) && !"".equals(opsTO.getDestinationNameWeightBreak()[i]) && !"".equals(opsTO.getRouteIdWeightBreak()[i]) && !"".equals(opsTO.getTravelKmWeightBreak()[i]) && !"".equals(opsTO.getTravelHourWeightBreak()[i]) && !"".equals(opsTO.getTravelMinuteWeightBreak()[i])) {
                            map.put("routeContractCode", opsTO.getOriginNameWeightBreak()[i] + "-" + opsTO.getDestinationNameWeightBreak()[i]);
                            map.put("originId", opsTO.getOriginIdWeightBreak()[i]);
                            map.put("originName", opsTO.getOriginNameWeightBreak()[i]);
                            map.put("destinationId", opsTO.getDestinationIdWeightBreak()[i]);
                            map.put("destinationName", opsTO.getDestinationNameWeightBreak()[i]);
                            map.put("routeId", opsTO.getRouteIdWeightBreak()[i]);
                            map.put("travelKm", opsTO.getTravelKmWeightBreak()[i]);
                            map.put("travelHour", opsTO.getTravelHourWeightBreak()[i]);
                            map.put("travelMinute", opsTO.getTravelMinuteWeightBreak()[i]);
                            String routeId = "";
                            routeId = (String) session.queryForObject("ops.checkRouteContractId", map);
                            if (routeId != null) {
                                insertCustomerContractRoute = Integer.parseInt(routeId);
                            } else {
                                insertCustomerContractRoute = (Integer) session.insert("ops.insertContractRoute", map);
                            }
                            if (opsTO.getMovementTypeIdWeightBreak() != null && !"".equals(opsTO.getMovementTypeIdWeightBreak())) {
                                if (opsTO.getMovementTypeIdWeightBreak().length > 0) {
                                    for (int j = 0; j < opsTO.getMovementTypeIdWeightBreak().length; j++) {
                                        if (!"0".equals(opsTO.getMovementTypeIdWeightBreak()[j]) && !"".equals(opsTO.getFromKgWeightBreak()[j]) && !"".equals(opsTO.getToKgWeightBreak()[j]) && !"".equals(opsTO.getRateWithReeferWeightBreak()[j]) && !"".equals(opsTO.getRateWithOutReeferWeightBreak()[j])) {
                                            map.put("movementTypeId", opsTO.getMovementTypeIdWeightBreak()[j]);
                                            map.put("fromKg", opsTO.getFromKgWeightBreak()[j]);
                                            map.put("toKg", opsTO.getToKgWeightBreak()[j]);
                                            map.put("rateWithReefer", opsTO.getRateWithReeferWeightBreak()[j]);
                                            map.put("rateWithOutReefer", opsTO.getRateWithOutReeferWeightBreak()[j]);
                                            map.put("contractRouteId", insertCustomerContractRoute);

                                            String contractId = "";
                                            contractId = (String) session.queryForObject("ops.checkWeightRateContractId", map);
                                            if (contractId != null) {
                                                System.out.println("map =dfgsdfg");
                                            } else {
                                                insertCustomerContractRate = (Integer) session.update("ops.insertContractWeightBreakRate", map);
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertCustomerContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertCustomerContractDetails", sqlException);
        }
        return insertCustomerContractRoute;
    }

    public ArrayList getCustomerContractDetails(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList contractDetails = new ArrayList();
        map.put("customerId", opsTO.getCustomerId());
        System.out.println("map = " + map);
        try {
            contractDetails = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getCustomerContractDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCustomerContractDetails", sqlException);
        }
        return contractDetails;
    }

    public int insertCustomerContract(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();

        int insertStatus = 0;
        try {
            map.put("cnotractFrom", opsTO.getContractFrom());
            map.put("contractTo", opsTO.getContractTo());
            map.put("customerId", opsTO.getCustomerId());
            map.put("contractNo", opsTO.getContractNo());
            System.out.println("=============Contract" + map);
            insertStatus = (Integer) getSqlMapClientTemplate().update("ops.updateCustomerContract", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCustomerContractDetails", sqlException);
        }
        return insertStatus;
    }

    public ArrayList getContractRouteDetails(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList contractDetails = new ArrayList();
        map.put("customerId", opsTO.getCustomerId());
        System.out.println("map = " + map);
        try {
            contractDetails = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getContractRouteDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractRouteDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getContractRouteDetails", sqlException);
        }
        return contractDetails;
    }

    public ArrayList getContractWeightRouteDetails(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList contractDetails = new ArrayList();
        map.put("customerId", opsTO.getCustomerId());
        System.out.println("map = " + map);
        try {
            contractDetails = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getContractWeightRouteDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractWeightRouteDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getContractWeightRouteDetails", sqlException);
        }
        return contractDetails;
    }

    public ArrayList getChargeableRateValue(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList chargableWeight = new ArrayList();
        map.put("customerId", opsTO.getCustomerId());
        map.put("chargableWeight", opsTO.getChargableWeight());
        map.put("awbDestinationId", opsTO.getAwbDestinationId());
        map.put("awbOriginId", opsTO.getAwbOriginId());
        System.out.println("map = " + map);
        try {
            chargableWeight = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getChargeableRateValue", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getChargeableRateValue Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getChargeableRateValue", sqlException);
        }
        return chargableWeight;
    }

    public ArrayList getFullTruckRateValue(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList chargableWeight = new ArrayList();
        map.put("customerId", opsTO.getCustomerId());
        map.put("awbDestinationId", opsTO.getAwbDestinationId());
        map.put("awbOriginId", opsTO.getAwbOriginId());
        map.put("vehicleTypeId", opsTO.getVehicleTypeId());
        map.put("productRate", opsTO.getProductRate());
        System.out.println("map = " + map);
        try {
            chargableWeight = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getFullTruckRateValue", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getChargeableRateValue Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getChargeableRateValue", sqlException);
        }
        return chargableWeight;
    }

    public ArrayList getAvailableWeightAndVolumeEdit(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList volumeDetails = new ArrayList();
        ArrayList maxWeightVehicleList = new ArrayList();
        double maxFleetCapacity = 0;
        OpsTO opsTo = null;
        String fleetId = "";
        try {
            map.put("vehicleId", opsTO.getVehicleId());
            map.put("truckDepDate", opsTO.getTruckDepDate());
            map.put("truckCurrentVol", opsTO.getTruckCurrentVol());
            map.put("truckCurrentCap", opsTO.getTruckCurrentCap());
            map.put("truckCapacity", opsTO.getTruckCapacity());
            map.put("truckVol", opsTO.getTruckVol());
            map.put("fleetTypeId", opsTO.getFleetTypeId());
            map.put("routeCourseId", opsTO.getRouteContractId());
            map.put("lastusedweight", opsTO.getRouteId());
            map.put("lastusedVol", opsTO.getFleetVolume());

            System.out.println("my is map = " + map);

            // maxWeightVehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getMaxWeightVehicle", map);
            volumeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getAvailableWeightAndVolumeToday", map);
            if (volumeDetails.size() > 0) {
                System.out.println("I am in Edit ");
                OpsTO opsObjTO2 = (OpsTO) volumeDetails.get(0);
                OpsTO opsObjTO = new OpsTO();
                System.out.println("TruckCapacity " + opsTO.getTruckCapacity() + " avalibleOldWeight " + opsObjTO2.getTotalWeight() + " lastusedweight " + opsTO.getRouteId());
                System.out.println("TruckCapacity condition" + (Double.parseDouble(opsTO.getTruckCapacity()) - (Double.parseDouble(opsObjTO2.getTotalWeight()) + Double.parseDouble(opsTO.getRouteId())) >= 0));
                if (Double.parseDouble(opsTO.getTruckCapacity()) - (Double.parseDouble(opsObjTO2.getTotalWeight()) + Double.parseDouble(opsTO.getRouteId())) >= 0) {
                    System.out.println(Double.parseDouble(opsTO.getTruckCapacity()) - (Double.parseDouble(opsObjTO2.getTotalWeight()) + Double.parseDouble(opsTO.getRouteId())));
                    opsObjTO.setTotalWeight("0");
                    opsObjTO.setUsedTruckCapcity((Double.parseDouble(opsObjTO2.getTotalWeight()) + Double.parseDouble(opsTO.getRouteId())) + "");
                    System.out.println("inside if weight wala 1 " + opsObjTO.getTotalWeight() + "used cap" + opsObjTO.getUsedTruckCapcity());
                } else {
                    opsObjTO.setTotalWeight(String.valueOf((Double.parseDouble(opsObjTO2.getTotalWeight()) + Double.parseDouble(opsTO.getRouteId())) - Double.parseDouble(opsTO.getTruckCapacity())));
                    opsObjTO.setUsedTruckCapcity(opsTO.getTruckCapacity());
                    System.out.println((Double.parseDouble(opsObjTO2.getTotalWeight()) + Double.parseDouble(opsTO.getRouteId())) - Double.parseDouble(opsTO.getTruckCapacity()));
                    System.out.println("inside else weight wala 2 " + opsObjTO.getTotalWeight() + "used cap" + opsObjTO.getUsedTruckCapcity());
                    System.out.println("opsTo1.getTotalWeight() 2 " + String.valueOf(Double.parseDouble(opsObjTO2.getTotalWeight()) - Double.parseDouble(opsTO.getTruckCapacity())));
                }
                System.out.println("opsObjTO2.getTotalVolume()");
                System.out.println("TruckVolume " + opsTO.getTruckVol() + " avalibleOldVolume " + opsObjTO2.getTotalVolume() + " lastusedVol " + opsTO.getFleetVolume());
                System.out.println("TruckVolume condition" + (Double.parseDouble(opsTO.getTruckVol()) - (Double.parseDouble(opsObjTO2.getTotalVolume()) + (Double.parseDouble(opsTO.getFleetVolume()))) >= 0));
                if (Double.parseDouble(opsTO.getTruckVol()) - (Double.parseDouble(opsObjTO2.getTotalVolume()) + (Double.parseDouble(opsTO.getFleetVolume()))) >= 0) {
                    opsObjTO.setTotalVolume("0");
                    opsObjTO.setUsedTruckVol((Double.parseDouble(opsObjTO2.getTotalVolume()) + (Double.parseDouble(opsTO.getFleetVolume()))) + "");
                    System.out.println("inside if vol 1 " + opsObjTO.getTotalVolume() + "used vol" + opsObjTO.getUsedTruckVol());
                } else {
                    opsObjTO.setTotalVolume(String.valueOf(Double.parseDouble(opsObjTO2.getTotalVolume()) + (Double.parseDouble(opsTO.getFleetVolume())) - Double.parseDouble(opsTO.getTruckVol())));
                    opsObjTO.setUsedTruckVol(opsTO.getTruckVol());
                    System.out.println("inside if vol 2 " + opsObjTO.getTotalVolume() + "used vol" + opsObjTO.getUsedTruckVol() + "Truck Volume " + opsTO.getTruckVol());
                    System.out.println("======" + String.valueOf(Double.parseDouble(opsObjTO2.getTotalVolume()) - Double.parseDouble(opsTO.getTruckVol())));
                }

                maxWeightVehicleList.add(opsObjTO);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAvailableWeightAndVolume Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getAvailableWeightAndVolume", sqlException);
        }
        return maxWeightVehicleList;
    }

    public ArrayList getTruckCodeList(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList vehicleRegNo = new ArrayList();
        if (opsTO.getVehicleTypeId() != null && !"".equals(opsTO.getVehicleTypeId())) {
            map.put("vehicleTypeId", opsTO.getVehicleTypeId());
        }
        if (opsTO.getVehicleRegNo() != null && !"".equals(opsTO.getVehicleRegNo())) {
            map.put("vehicleRegNo", opsTO.getVehicleRegNo() + "%");
        }
        if (opsTO.getAwbOriginId() != null && !"".equals(opsTO.getAwbOriginId())) {
            map.put("originId", opsTO.getAwbOriginId());
        }
        if (opsTO.getAwbDestinationId() != null && !"".equals(opsTO.getAwbDestinationId())) {
            map.put("destinationId", opsTO.getAwbDestinationId());
        }
        System.out.println("map = " + map);
        try {
            //vehicleRegNo = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getVehTypeVehicleRegNo", map);
            vehicleRegNo = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getTruckCodeList", map);
            System.out.println("vehicleRegNo::::" + vehicleRegNo.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehTypeVehicleRegNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehTypeVehicleRegNo", sqlException);
        }
        return vehicleRegNo;
    }

    public ArrayList getAvailableWeightAndVolume(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList volumeDetails = new ArrayList();
        ArrayList maxWeightVehicleList = new ArrayList();
        double maxFleetCapacity = 0;
        OpsTO opsTo = null;
        String fleetId = "";

        try {
            map.put("vehicleId", opsTO.getVehicleId());
            map.put("truckDepDate", opsTO.getTruckDepDate());
            map.put("truckCurrentVol", opsTO.getTruckCurrentVol());
            map.put("truckCurrentCap", opsTO.getTruckCurrentCap());
            map.put("truckCapacity", opsTO.getTruckCapacity());
            map.put("truckCapacityTemp", opsTO.getTruckCapacityTemp());
            map.put("truckVol", opsTO.getTruckVol());
            map.put("fleetTypeId", opsTO.getFleetTypeId());
            System.out.println("my is map = " + map);

            // maxWeightVehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getMaxWeightVehicle", map);
            volumeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getAvailableWeightAndVolumeToday", map);
            if (volumeDetails.size() > 0) { // Next Load for the given date
                System.out.println("I am in first, Vehicle is already loaded on the given date ");
                OpsTO opsObjTO2 = (OpsTO) volumeDetails.get(0);
                OpsTO opsObjTO = new OpsTO();
                if ("".equals(opsTO.getTruckCapacityTemp())) {
                    opsObjTO.setTotalWeight("0");
                    opsObjTO.setUsedTruckCapcity(opsObjTO2.getTotalWeight());
                } else if (Double.parseDouble(opsObjTO2.getTotalWeight()) >= 0) {
                    opsObjTO.setTotalWeight(opsObjTO2.getTotalWeight() + "");
                    opsObjTO.setUsedTruckCapcity(opsTO.getTruckCapacity());
                } else {
                    opsObjTO.setTotalWeight(opsObjTO2.getAssignedWeight() + "");
                    opsObjTO.setUsedTruckCapcity("0");
                    System.out.println("opsTo1.getTotalWeight()" + String.valueOf(Double.parseDouble(opsObjTO2.getTotalWeight()) - Double.parseDouble(opsTO.getTruckCapacity())));
                }
                System.out.println("opsObjTO2.getTotalVolume()");
                if ((Double.parseDouble(opsTO.getTruckVol()) - Double.parseDouble(opsObjTO2.getTotalVolume())) >= 0) {
                    opsObjTO.setTotalVolume("0");
                    opsObjTO.setUsedTruckVol(opsObjTO2.getTotalVolume());
                } else {
                    opsObjTO.setTotalVolume(String.valueOf(Double.parseDouble(opsObjTO2.getTotalVolume()) - Double.parseDouble(opsTO.getTruckVol())));
                    opsObjTO.setUsedTruckVol(opsTO.getTruckVol());
                    System.out.println("======" + String.valueOf(Double.parseDouble(opsObjTO2.getTotalVolume()) - Double.parseDouble(opsTO.getTruckVol())));
                }

                maxWeightVehicleList.add(opsObjTO);
            }

            if (volumeDetails.size() == 0) { // First load for the given date
                System.out.println("I am in second query, First load for the given date  ");
                boolean processNext = true;
                ArrayList truckWeightLst = new ArrayList();
                //check masterWeight and truck Used Weight
                truckWeightLst = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getUsedWeightAndVolume", map);
                if (truckWeightLst.size() > 0) {
                    OpsTO opsObjTO2 = (OpsTO) truckWeightLst.get(0);
                    OpsTO opsObjTO = new OpsTO();
                    if ("".equals(opsTO.getTruckCapacityTemp())) {
                        if (Double.parseDouble(opsTO.getTruckCapacity()) <= Double.parseDouble(opsObjTO2.getTotalWeight())) {
                            opsObjTO.setTotalWeight("0");
                            opsObjTO.setUsedTruckCapcity(opsTO.getTruckCapacity());
                        } else {
                            opsObjTO.setTotalWeight(Double.parseDouble(opsTO.getTruckCapacity()) - Double.parseDouble(opsObjTO2.getTotalWeight()) + "");
                            opsObjTO.setUsedTruckCapcity(opsObjTO2.getTotalWeight());
                        }
                    } else if (Double.parseDouble(opsObjTO2.getTotalWeight()) >= 0) {
                        opsObjTO.setTotalWeight(opsObjTO2.getTotalWeight() + "");
                        opsObjTO.setUsedTruckCapcity(opsTO.getTruckCapacity());
                    } else {
                        opsObjTO.setTotalWeight(opsObjTO2.getAssignedWeight() + "");
                        opsObjTO.setUsedTruckCapcity("0");
                        System.out.println("opsTo1.getTotalWeight()" + String.valueOf(Double.parseDouble(opsObjTO2.getTotalWeight()) - Double.parseDouble(opsTO.getTruckCapacity())));
                    }
                    System.out.println("opsObjTO2.getTotalVolume()");
                    if ((Double.parseDouble(opsTO.getTruckVol()) - Double.parseDouble(opsObjTO2.getTotalVolume())) >= 0) {
                        opsObjTO.setTotalVolume("0");
                        opsObjTO.setUsedTruckVol(opsObjTO2.getTotalVolume());
                    } else {
                        opsObjTO.setTotalVolume(String.valueOf(Double.parseDouble(opsObjTO2.getTotalVolume()) - Double.parseDouble(opsTO.getTruckVol())));
                        opsObjTO.setUsedTruckVol(opsTO.getTruckVol());
                        System.out.println("======" + String.valueOf(Double.parseDouble(opsObjTO2.getTotalVolume()) - Double.parseDouble(opsTO.getTruckVol())));
                    }
                    maxWeightVehicleList.add(opsObjTO);

                }

            }
            //maxWeightVehicleList=volumeDetails;

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAvailableWeightAndVolume Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getAvailableWeightAndVolume", sqlException);
        }
        return maxWeightVehicleList;
    }

    public ArrayList getAvailableWeightAndVolumeOLD(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList volumeDetails = new ArrayList();
        ArrayList maxWeightVehicleList = new ArrayList();
        double maxFleetCapacity = 0;
        OpsTO opsTo = null;
        String fleetId = "";

        try {
            map.put("vehicleId", opsTO.getVehicleId());
            map.put("truckDepDate", opsTO.getTruckDepDate());
            map.put("truckCurrentVol", opsTO.getTruckCurrentVol());
            map.put("truckCurrentCap", opsTO.getTruckCurrentCap());
            map.put("truckCapacity", opsTO.getTruckCapacity());
            map.put("truckCapacityTemp", opsTO.getTruckCapacityTemp());
            map.put("truckVol", opsTO.getTruckVol());
            map.put("fleetTypeId", opsTO.getFleetTypeId());
            System.out.println("my is map = " + map);

            // maxWeightVehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getMaxWeightVehicle", map);
            volumeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getAvailableWeightAndVolumeToday", map);
            if (volumeDetails.size() > 0) {
                System.out.println("I am in first ");
                OpsTO opsObjTO2 = (OpsTO) volumeDetails.get(0);
                OpsTO opsObjTO = new OpsTO();

                if ((Double.parseDouble(opsTO.getTruckCapacity()) - Double.parseDouble(opsObjTO2.getTotalWeight())) >= 0) {
                    opsObjTO.setTotalWeight(Double.parseDouble(opsTO.getTruckCapacity()) - Double.parseDouble(opsObjTO2.getTotalWeight()) + "");
                    opsObjTO.setUsedTruckCapcity(opsTO.getTruckCapacity());
                } else {
                    opsObjTO.setTotalWeight(String.valueOf(Double.parseDouble(opsObjTO2.getTotalWeight()) - Double.parseDouble(opsTO.getTruckCapacity())));
                    opsObjTO.setUsedTruckCapcity(opsTO.getTruckCapacity());
                    System.out.println("opsTo1.getTotalWeight()" + String.valueOf(Double.parseDouble(opsObjTO2.getTotalWeight()) - Double.parseDouble(opsTO.getTruckCapacity())));
                }
                System.out.println("opsObjTO2.getTotalVolume()");
                if ((Double.parseDouble(opsTO.getTruckVol()) - Double.parseDouble(opsObjTO2.getTotalVolume())) >= 0) {
                    opsObjTO.setTotalVolume("0");
                    opsObjTO.setUsedTruckVol(opsObjTO2.getTotalVolume());
                } else {
                    opsObjTO.setTotalVolume(String.valueOf(Double.parseDouble(opsObjTO2.getTotalVolume()) - Double.parseDouble(opsTO.getTruckVol())));
                    opsObjTO.setUsedTruckVol(opsTO.getTruckVol());
                    System.out.println("======" + String.valueOf(Double.parseDouble(opsObjTO2.getTotalVolume()) - Double.parseDouble(opsTO.getTruckVol())));
                }

                maxWeightVehicleList.add(opsObjTO);
            }

            if (volumeDetails.size() == 0) {
                System.out.println("I am in second query ");
                boolean processNext = true;
                ArrayList truckWeightLst = new ArrayList();
                //check masterWeight and truck Used Weight
                truckWeightLst = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getUsedWeightAndVolume", map);
                if (truckWeightLst.size() > 0) {
                    OpsTO opsObjTO2 = (OpsTO) truckWeightLst.get(0);
                    double truckCapacity = Double.parseDouble(opsObjTO2.getTotalWeight());
                    double truckUsedCapacity = Double.parseDouble(opsObjTO2.getUsedTruckCapcity());
                    if (truckCapacity <= truckUsedCapacity + Double.parseDouble(opsTO.getTruckCapacity())) {
                        processNext = false;
                    }
                }

                if (processNext) {
                    System.out.println("I am in third query");
                    volumeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getAvailableWeightAndVolume", map);
                    OpsTO opsTo1 = (OpsTO) volumeDetails.get(0);
                    OpsTO opsObjTO = new OpsTO();
                    if (Double.parseDouble(opsTO.getTruckCapacity()) - Double.parseDouble(opsTo1.getTotalWeight()) >= 0) {
                        opsObjTO.setTotalWeight("0");

                        opsObjTO.setUsedTruckCapcity(opsTo1.getTotalWeight());
                        System.out.println("opsTo1.getTotalWeight()" + opsTo1.getTotalWeight());

                    } else {
                        opsObjTO.setTotalWeight(String.valueOf(Double.parseDouble(opsTo1.getTotalWeight()) - Double.parseDouble(opsTO.getTruckCapacity())));
                        opsObjTO.setUsedTruckCapcity(opsTO.getTruckCapacity());
                        System.out.println("opsTo1.getTotalWeight()" + opsTO.getTruckCapacity());
                    }

                    System.out.println("i came here ");
                    if (Double.parseDouble(opsTO.getTruckVol()) - Double.parseDouble(opsTo1.getTotalVolume()) >= 0) {
                        opsObjTO.setTotalVolume("0");
                        opsObjTO.setUsedTruckVol(opsTo1.getTotalVolume());
                    } else {
                        opsObjTO.setTotalVolume(String.valueOf(Double.parseDouble(opsTo1.getTotalVolume()) - Double.parseDouble(opsTO.getTruckVol())));
                        opsObjTO.setUsedTruckVol(opsTO.getTruckVol());
                        System.out.println("======" + String.valueOf(Double.parseDouble(opsTo1.getTotalVolume()) - Double.parseDouble(opsTO.getTruckVol())));
                    }

                    maxWeightVehicleList.add(opsObjTO);

                } else {
                    OpsTO opsObjTO = new OpsTO();
                    opsObjTO.setTotalWeight("N");

                    maxWeightVehicleList.add(opsObjTO);
                }
            }
            //maxWeightVehicleList=volumeDetails;

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAvailableWeightAndVolume Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getAvailableWeightAndVolume", sqlException);
        }
        return maxWeightVehicleList;
    }

    public int getAwbStatus(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int flag;
        if (opsTO.getAwbno() != null && !"".equals(opsTO.getAwbno())) {
            map.put("awbno", opsTO.getAwbno());
        } else {
            map.put("awbno", '0');
        }
        System.out.println("map = " + map);
        try {
            //vehicleRegNo = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getVehTypeVehicleRegNo", map);
            flag = (Integer) getSqlMapClientTemplate().queryForObject("ops.getAwbstatus", map);
            System.out.println("AWB Flag::::" + flag);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("AWB no check Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "AWB NO check", sqlException);
        }
        return flag;
    }

    public ArrayList getFleetCode(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList fleetCode = new ArrayList();
        if (opsTO.getVehicleTypeId() != null && !"".equals(opsTO.getVehicleTypeId())) {
            map.put("truckDepDate", opsTO.getVehicleTypeId());
        }
        System.out.println("map = " + map);
        try {
            //vehicleRegNo = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getVehTypeVehicleRegNo", map);
            fleetCode = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getFleetCode", map);
            System.out.println("vehicleRegNo::::" + fleetCode.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehTypeVehicleRegNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehTypeVehicleRegNo", sqlException);
        }
        return fleetCode;
    }

    public int updateContractWeightBreak(String[] weightContractId, String[] weightRateContractId, String[] weightRouteContractId, String[] fromKgWeight, String[] toKgWeight, String[] rateWithReeferWeight, String[] rateWithOutReeferWeight, String[] activeIndWeight) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertStatus = 0;
        /*
         * set the parameters in the map for sending to ORM
         */

        try {
            if (weightContractId.length > 0) {
                for (int j = 0; j < weightContractId.length; j++) {
                    map.put("weightContractId", weightContractId[j]);
                    map.put("weightRateContractId", weightRateContractId[j]);
                    map.put("weightRouteContractId", weightRouteContractId[j]);
                    map.put("fromKgWeight", fromKgWeight[j]);
                    map.put("toKgWeight", toKgWeight[j]);
                    map.put("rateWithReefer", rateWithReeferWeight[j]);
                    map.put("rateWithOutReefer", rateWithOutReeferWeight[j]);
                    map.put("activeIndWeight", activeIndWeight[j]);
                    System.out.println("map = " + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("ops.updateContractWeightBreakRate", map);

                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertCustomerContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertCustomerContractDetails", sqlException);
        }
        return insertStatus;
    }

    public int updateContractFullTruck(String[] contractId, String[] rateContractId, String[] routeContractId, String[] rateWithReeferTruck, String[] rateWithOutReeferTruck, String[] activeIndRate) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertStatus = 0;
        /*
         * set the parameters in the map for sending to ORM
         */

        try {
            if (rateContractId.length > 0) {
                for (int j = 0; j < rateContractId.length; j++) {
                    map.put("rateContractId", rateContractId[j]);
                    map.put("routeContractId", routeContractId[j]);
                    map.put("rateWithReeferTruck", rateWithReeferTruck[j]);
                    map.put("rateWithOutReeferTruck", rateWithOutReeferTruck[j]);
                    map.put("activeIndRate", activeIndRate[j]);
                    System.out.println("map = " + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("ops.updateContractFullTruckRate", map);

                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertCustomerContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertCustomerContractDetails", sqlException);
        }
        return insertStatus;
    }

    public ArrayList getTsaViewList(String consignmnetIds, String tsaType) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList tsiViewList = new ArrayList();
        try {

            String[] consignmentId = consignmnetIds.split(",");
            ArrayList consignmentIdList = new ArrayList();
            for (int i = 0; i < consignmentId.length; i++) {
                consignmentIdList.add(consignmentId[i]);
            }
            map.put("consignmentIdList", consignmentIdList);
            map.put("tsaType", tsaType);

            System.out.println("Map for getTsiViewList" + map);
            tsiViewList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getTsaViewList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTsiViewList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTsiViewList", sqlException);
        }
        return tsiViewList;
    }

    public int insertConsignmentCustomsCheck(OpsTO opsTO, String[] receivedPackages, String[] receivedWeight, String[] routeContractId, String[] igmNo, String[] flightNumber, String[] hawbNo) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertStatus = 0;
        int headerId = 0;
        try {
            map.put("shipmentType", opsTO.getShipmentType());
            map.put("consignmentEfoNo", opsTO.getConsignmentEfoNo());
            System.out.println("consignmentEfoNo" + opsTO.getConsignmentEfoNo());
            map.put("fromAddress", opsTO.getFromAddress().trim());
            map.put("toAddress", opsTO.getToAddress().trim());
            map.put("fleetDetails", opsTO.getFleetDetails().trim());
            map.put("userId", opsTO.getUserId());
            map.put("flightDate", opsTO.getFlightDate());
            map.put("consignmentTpNo", opsTO.getConsignmentTpNo());
            map.put("transhipmentNo", opsTO.getTranshipmentNo());
            map.put("consignmentEgmNo", opsTO.getConsignmentEgmNo());
            if ("".equals(opsTO.getCustomsRemark().trim())) {
                map.put("customsRemark", "0");
            } else {
                map.put("customsRemark", opsTO.getCustomsRemark().trim());
            }
            System.out.println("insertConsignmentCustomsCheck map " + map);
            headerId = (Integer) getSqlMapClientTemplate().insert("ops.insertConsignmentCustomsCheck", map);
            if (headerId > 0) {

                map.put("headerId", headerId);
                map.put("customsCode", "TSA/15-16-" + headerId);

                getSqlMapClientTemplate().update("ops.updateCustomsCode", map);
                String[] consignmentSbNo = opsTO.getConsignmentSbNo();
                String[] consignmentOrderNos = opsTO.getConsignmentOrderNos();
                String[] totalTsaGoods = opsTO.getTotalTsaGoods();
                if (consignmentSbNo.length > 0) {
                    for (int i = 0; i < consignmentSbNo.length; i++) {
                        map.put("igmNo", igmNo[i]);
                        map.put("hawbNo", hawbNo[i]);
                        map.put("flightNumber", flightNumber[i]);
                        map.put("consignmentOrderId", consignmentOrderNos[i]);
                        map.put("consignmentSbNo", consignmentSbNo[i]);
                        map.put("totalTsaGoods", totalTsaGoods[i]);
                        System.out.println("receivedPackages[i]" + receivedPackages[i]);
                        map.put("receivedPackages", receivedPackages[i]);
                        map.put("receivedWeight", receivedWeight[i]);
                        map.put("routeContractId", routeContractId[i]);

                        insertStatus = (Integer) getSqlMapClientTemplate().insert("ops.insertConsignmentCustomsCheckDetails", map);
                        if (insertStatus > 0) {
                            insertStatus = (Integer) getSqlMapClientTemplate().update("ops.updateConsignmentStatus", map);
                            insertStatus = (Integer) getSqlMapClientTemplate().update("ops.updateTsaRouteConsignmentStatus", map);
                        }
                    }

                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertConsignmentCustomsCheck Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertConsignmentCustomsCheck", sqlException);
        }
        return headerId;
    }

    public ArrayList getCustomsCheckViewList(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList tsiViewList = new ArrayList();
        try {
            map.put("customsCheckId", opsTO.getCustomsCheckId());
            map.put("branchId", opsTO.getBranchId());

            System.out.println("Map for getCustomsCheckViewList" + map);
            tsiViewList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getCustomsCheckViewList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTsiViewList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTsiViewList", sqlException);
        }
        return tsiViewList;
    }

    public ArrayList getConsignmentCustomsCheck(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList tsiViewList = new ArrayList();
        try {
            map.put("customsCheckId", opsTO.getCustomsCheckId());

            System.out.println("Map for getConsignmentCustomsCheck" + map);
            tsiViewList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getCustomsCheckHeader", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentCustomsCheck Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getConsignmentCustomsCheck", sqlException);
        }
        return tsiViewList;
    }

    public int insertConsignmentManifest(OpsTO opsTO, String[] receivedPackages, String[] receivedWeight, String[] routeContractId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertStatus = 0;
        int headerId = 0;
        try {
            map.put("consignmentNoteEmno", opsTO.getConsignmentNoteEmno());
            map.put("shipmentType", opsTO.getShipmentType());
            map.put("operatorName", opsTO.getOperatorName());
            map.put("flightNo", opsTO.getFlightNo());
            map.put("manifestDate", opsTO.getManifestDate());
            map.put("lodingPoint", opsTO.getLodingPoint());
            map.put("unLodingPoint", opsTO.getUnLodingPoint());
            map.put("consignmentNoteAtdNo", opsTO.getConsignmentNoteAtdNo());
            map.put("consignmentNoteEmno", opsTO.getConsignmentNoteEmno());
            map.put("userId", opsTO.getUserId());

            headerId = (Integer) getSqlMapClientTemplate().insert("ops.insertConsignmentmanifest", map);
            if (headerId > 0) {
                map.put("headerId", headerId);
                map.put("manifestCode", "MC/15-16-" + headerId);
                getSqlMapClientTemplate().update("ops.updateManifestCode", map);
                String[] consignmentOrderNos = opsTO.getConsignmentOrderNos();
                if (consignmentOrderNos.length > 0) {
                    for (int i = 0; i < consignmentOrderNos.length; i++) {
                        map.put("consignmentOrderId", consignmentOrderNos[i]);
                        map.put("receivedPackages", receivedPackages[i]);
                        map.put("receivedWeight", receivedWeight[i]);
                        map.put("routeContractId", routeContractId[i]);
                        insertStatus = (Integer) getSqlMapClientTemplate().insert("ops.insertConsignmentManifestDetails", map);
                        if (insertStatus > 0) {
                            insertStatus = (Integer) getSqlMapClientTemplate().update("ops.updateConsignmentManifestStatus", map);
                            insertStatus = (Integer) getSqlMapClientTemplate().update("ops.updateManifestRouteConsignmentStatus", map);
                        }
                    }

                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertConsignmentCustomsCheck Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertConsignmentCustomsCheck", sqlException);
        }
        return headerId;
    }

    public ArrayList getTsaPrintList(String fromDate, String toDate, String tsaManifestCode, String branchId, String roleId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList tsaPrintList = new ArrayList();
        try {

            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("tsaManifestCode", tsaManifestCode);
            map.put("tsaManifestCode", tsaManifestCode);
            map.put("branchId", branchId);
            map.put("roleId", roleId);

            System.out.println("Map for tsaPrintList" + map);
            tsaPrintList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getTsaPrintList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTsaPrintList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTsaPrintList", sqlException);
        }
        return tsaPrintList;
    }

    public ArrayList getManifestPrintList(String fromDate, String toDate, String tsaManifestCode, String branchId, String roleId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList manifestPrintList = new ArrayList();
        try {

            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("tsaManifestCode", tsaManifestCode);
            map.put("branchId", branchId);
            map.put("roleId", roleId);

            System.out.println("Map for manifestPrintList" + map);
            manifestPrintList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getManifestPrintList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTsaPrintList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTsaPrintList", sqlException);
        }
        return manifestPrintList;
    }

    public ArrayList getManifestViewList(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList manifestViewList = new ArrayList();
        try {

            map.put("manifestId", opsTO.getManifestId());
            map.put("branchId", opsTO.getBranchId());

            System.out.println("Map for manifestViewList" + map);
            manifestViewList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getManifestViewList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getManifestViewList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getManifestViewList", sqlException);
        }
        return manifestViewList;
    }

    public ArrayList getConsignmentManifest(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList consignmentManifest = new ArrayList();
        try {

            map.put("manifestId", opsTO.getManifestId());

            System.out.println("Map for consignmentManifest" + map);
            consignmentManifest = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getManifestPrintList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentManifest Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "consignmentManifest", sqlException);
        }
        return consignmentManifest;
    }

    public String getSrcFilePath() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        String srcFilePath = "";
        try {

            srcFilePath = (String) getSqlMapClientTemplate().queryForObject("ops.getSourceFilePath", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSrcFilePath Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getSrcFilePath", sqlException);
        }
        return srcFilePath;
    }

    public ArrayList getTruckNameList() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList truckNameList = new ArrayList();
        try {

            System.out.println("Map for truckNameList" + map);
            truckNameList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getTruckNameList", map);
            System.out.println("truckNameList size ::::" + truckNameList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTruckNameList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTruckNameList", sqlException);
        }
        return truckNameList;
    }

    public ArrayList getTruckCapacityList(String truckCodeId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList truckCapacityList = new ArrayList();
        try {
            map.put("truckCodeId", truckCodeId);
            System.out.println("Map for getTruckCapacityList" + map);
            truckCapacityList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getTruckCapacityList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTruckCapacityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTruckCapacityList", sqlException);
        }
        return truckCapacityList;
    }

    public int removeConsignmentNoteRoute(String consignmentNoteRouteId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int updateStatus = 0;
        try {
            map.put("consignmentNoteRouteId", consignmentNoteRouteId);
            System.out.println("Map for removeConsignmentNoteRoute" + map);
            updateStatus = (Integer) getSqlMapClientTemplate().update("ops.removeConsignmentNoteRoute", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("removeConsignmentNoteRoute  Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "removeConsignmentNoteRoute", sqlException);
        }
        return updateStatus;
    }

    public ArrayList getTsaList(String loadDate, String loadToDate, String branchId, String roleId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList getTsaList = new ArrayList();
        try {
            if (loadDate == null || "".equals(loadDate)) {
                map.put("loadDate", "");
            } else {
                map.put("loadDate", loadDate);
            }
            map.put("loadToDate", loadToDate);
            map.put("branchId", branchId);
            map.put("roleId", roleId);

            System.out.println("Map for getTsaList" + map);
            getTsaList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getTsaList", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTsaList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getTsaList", sqlException);
        }
        return getTsaList;
    }

    public ArrayList getFleetCodeGroup(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList fleetCodeGroup = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("fleetCode", opsTO.getFleetCode() + "%");

        System.out.println("map = " + map);
        try {
            fleetCodeGroup = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getFleetCodeGroup", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("fleetCodeGroup Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "fleetCodeGroup", sqlException);
        }
        return fleetCodeGroup;
    }

    public ArrayList getConsignmentOrderRate(String consignmentOrderId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList list = new ArrayList();
        map.put("consignmentOrderId", consignmentOrderId);

        System.out.println("getConsignmentOrderRate map = " + map);
        try {
            list = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getConsignmentOrderRate", map);
            System.out.println(" getConsignmentOrderRate" + list.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentOrderRate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getConsignmentOrderRate", sqlException);
        }
        return list;
    }

    public int saveEditNonContractOrderRate(OpsTO opsTO, SqlMapClient session) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int updateStatus = 0;
        ArrayList consignmentOrderRateList = new ArrayList();
        try {
            map.put("userId", opsTO.getUserId());
            map.put("consignmentOrderId", opsTO.getConsignmentOrderId());
            String status = (String) session.queryForObject("ops.getConsignmentOrderId", map);
            System.out.println("saveEditNonContractOrderRate status" + status);
            if ("0".equals(status)) {
                consignmentOrderRateList = (ArrayList) session.queryForList("ops.getConsignmentOrderRate", map);
                Iterator itr = consignmentOrderRateList.iterator();
                OpsTO opTO = null;
                while (itr.hasNext()) {
                    opTO = new OpsTO();
                    opTO = (OpsTO) itr.next();
                    map.put("rateMode", opTO.getRateMode());
                    map.put("ratePerKg", opTO.getRatePerKg());
                    map.put("rateValue", opTO.getRateValue());
                    map.put("freightCharge", opTO.getRateValue());

                    map.put("tspInput", opTO.getTspInput());
                    map.put("payBillTsp", opTO.getPayBillTsp());
                    map.put("tspOriginPaidTo", opTO.getTspOriginPaidTo());
                    map.put("tspDestinationPaidTo", opTO.getTspDestinationPaidTo());
                    map.put("tspOriginAmount", opTO.getTspOriginAmount());
                    map.put("tspDestinationAmount", opTO.getTspDestinationAmount());

                    map.put("tsrInput", opTO.getTsrInput());
                    map.put("payBillTsr", opTO.getPayBillTsr());
                    map.put("tsrOriginPaidTo", opTO.getTsrOriginPaidTo());
                    map.put("tsrDestinationPaidTo", opTO.getTsrDestinationPaidTo());
                    map.put("tsrOriginPercent", opTO.getTsrOriginPercent());
                    map.put("tsrDestPercent", opTO.getTsrDestPercent());
                    map.put("tsrOriginAmount", opTO.getTsrOriginAmount());
                    map.put("tsrDestinationAmount", opTO.getTsrDestinationAmount());

                    map.put("screeningInput", opTO.getScreeningInput());
                    map.put("payBillScreen", opTO.getPayBillScreen());
                    map.put("scOriginPaidTo", opTO.getScOriginPaidTo());
                    map.put("scDestinationPaidTo", opTO.getScDestinationPaidTo());
                    map.put("scOriginAmount", opTO.getScOriginAmount());
                    map.put("scDestinationAmount", opTO.getScDestinationAmount());

                    map.put("totalChargableWeight", opTO.getTotalChargableWeight());
                    map.put("totTSPrate", opTO.getTotTSPrate());
                    map.put("totTSRrate", opTO.getTotTSRrate());
                    map.put("totSCRrate", opTO.getTotSCRrate());
                    map.put("totIncentiveAmount", opTO.getTotIncentiveAmount());
                    map.put("totCustAmount", opTO.getTotCustAmount());
                    map.put("totTTAamount", opTO.getTotTTAamount());

                }
                updateStatus = (Integer) session.update("ops.insertEditNonContractOrderRate", map);
            }
//            if (!opsTO.getRateMode().equals("0")) {
            if (opsTO.getRateMode().equals("3")) {
                map.put("rateValue", opsTO.getRateValue());
                map.put("freightCharge", opsTO.getRateValue());
                map.put("ratePerKg", opsTO.getRatePerKg());
            } else {
                System.out.println("getFullTruckValue");
                map.put("freightCharge", opsTO.getFullTruckValue());
                map.put("rateValue", opsTO.getFullTruckValue());
                map.put("ratePerKg", "0");
            }
            System.out.println("saveEditNonContractOrderRate map is" + map);
            updateStatus = (Integer) session.update("ops.saveEditNonContractOrderRate", map);

            map.put("rateMode", opsTO.getRateMode());

            map.put("tspInput", opsTO.getTspInput());
            map.put("payBillTsp", opsTO.getPayBillTsp());
            map.put("tspOriginPaidTo", opsTO.getTspOriginPaidTo());
            map.put("tspDestinationPaidTo", opsTO.getTspDestinationPaidTo());
            map.put("tspOriginAmount", opsTO.getTspOriginAmount());
            map.put("tspDestinationAmount", opsTO.getTspDestinationAmount());

            map.put("tsrInput", opsTO.getTsrInput());
            map.put("payBillTsr", opsTO.getPayBillTsr());
            map.put("tsrOriginPaidTo", opsTO.getTsrOriginPaidTo());
            map.put("tsrDestinationPaidTo", opsTO.getTsrDestinationPaidTo());
            map.put("tsrOriginPercent", opsTO.getTsrOriginPercent());
            map.put("tsrDestPercent", opsTO.getTsrDestPercent());
            map.put("tsrOriginAmount", opsTO.getTsrOriginAmount());
            map.put("tsrDestinationAmount", opsTO.getTsrDestinationAmount());

            map.put("screeningInput", opsTO.getScreeningInput());
            map.put("payBillScreen", opsTO.getPayBillScreen());
            map.put("scOriginPaidTo", opsTO.getScOriginPaidTo());
            map.put("scDestinationPaidTo", opsTO.getScDestinationPaidTo());
            map.put("scOriginAmount", opsTO.getScOriginAmount());
            map.put("scDestinationAmount", opsTO.getScDestinationAmount());

            map.put("totalChargableWeight", opsTO.getTotalChargableWeight());
            map.put("totTSPrate", opsTO.getTotTSPrate());
            map.put("totTSRrate", opsTO.getTotTSRrate());
            map.put("totSCRrate", opsTO.getTotSCRrate());
            map.put("totIncentiveAmount", opsTO.getTotIncentiveAmount());
            map.put("totCustAmount", opsTO.getTotCustAmount());
            map.put("totTTAamount", opsTO.getTotTTAamount());

            java.util.Date date = new java.util.Date();
            map.put("createdOn", new Timestamp(date.getTime()));

            System.out.println(" map valie isss" + map);
            updateStatus = (Integer) session.update("ops.saveEditNonContractOrderRate", map);

            String tripId = (String) session.queryForObject("ops.getConsignmentOrderTripId", map);
            tripId = tripId + "," + "0";
            if (tripId.contains(",")) {
                String[] tripIds = tripId.split(",");
                List tripIdsList = new ArrayList(tripIds.length);
                for (int i = 0; i < tripIds.length; i++) {
                    System.out.println("value:" + tripIds[i]);
                    tripIdsList.add(tripIds[i]);
                }
                map.put("tripIds", tripIdsList);
            } else {
                map.put("tripIds", tripId);
            }
            System.out.println("editNonContractOrderRateInTrip map valie isss" + map);
            updateStatus = (Integer) session.update("ops.editNonContractOrderRateInTrip", map);

            updateStatus = (Integer) session.update("ops.insertEditNonContractOrderRate", map);
//            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveEditNonContractOrderRate  Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "saveEditNonContractOrderRate", sqlException);
        }
        return updateStatus;
    }

    public int saveOtlSealNo(String consignmentOrderId, String otlNo) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int updateStatus = 0;
        try {
            map.put("consignmentOrderId", consignmentOrderId);
            map.put("otlNo", otlNo);
            System.out.println("Map for saveOtlSealNo" + map);
            updateStatus = (Integer) getSqlMapClientTemplate().update("ops.saveOtlSealNo", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveOtlSealNo  Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "saveOtlSealNo", sqlException);
        }
        return updateStatus;
    }

    public int insertDelayTime(OpsTO opsTO, int userId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertStatus = 0;
        try {
            map.put("delayTime", opsTO.getDelayTime());
            map.put("delayTimeId", opsTO.getDelayTimeId());
            map.put("statusId", opsTO.getStatusId());
            map.put("userId", userId);
            System.out.println("Map for insertDelayTime" + map);

            if ("".equals(opsTO.getDelayTimeId())) {
                System.out.println("if");
                insertStatus = (Integer) getSqlMapClientTemplate().update("ops.insertDelayTime", map);
                System.out.println("inserted status" + insertStatus);
            } else {
                System.out.println("else");
                //map.put("reasonId", operationTO.getReasonId());
                insertStatus = (Integer) getSqlMapClientTemplate().update("ops.updateDelayTime", map);
                System.out.println("insertDelayTime status" + insertStatus);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertDelayTime  Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertDelayTime", sqlException);
        }
        return insertStatus;
    }

    public int saveRoyaltyPayMaster(OpsTO opsTO, int userId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertStatus = 0;
        try {
            map.put("paidId", opsTO.getPaidId());
            map.put("paidBy", opsTO.getPaidBy());
            map.put("branchId", opsTO.getBranchId());
            map.put("userId", userId);
            System.out.println("Map for saveRoyaltyPayMaster" + map);

            if ("".equals(opsTO.getPaidId())) {
                insertStatus = (Integer) getSqlMapClientTemplate().update("ops.insertRoyaltyPayMaster", map);
            } else {
                insertStatus = (Integer) getSqlMapClientTemplate().update("ops.updateRoyaltyPayMaster", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertDelayTime  Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertDelayTime", sqlException);
        }
        return insertStatus;
    }

    public int saveServiceTaxMaster(OpsTO opsTO, int userId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertStatus = 0;
        try {
            map.put("serviceTaxId", opsTO.getServiceTaxId());
            map.put("validFrom", opsTO.getValidFrom());
            map.put("validTo", opsTO.getValidTo());
            map.put("serviceTaxAmount", opsTO.getServiceTaxAmount());
            map.put("activeInd", opsTO.getActiveInd());
            map.put("userId", userId);
            System.out.println("Map for saveServiceTaxMaster" + map);

            if ("".equals(opsTO.getServiceTaxId())) {
                insertStatus = (Integer) getSqlMapClientTemplate().update("ops.insertServiceTaxMaster", map);
            } else {
                insertStatus = (Integer) getSqlMapClientTemplate().update("ops.updateServiceTaxMaster", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveServiceTaxMaster  Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "saveServiceTaxMaster", sqlException);
        }
        return insertStatus;
    }

    public ArrayList getDelayTimeList(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList delayTimeList = new ArrayList();

        System.out.println("map = " + map);
        try {
            delayTimeList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getDelayTimeList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDelayTimeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getDelayTimeList", sqlException);
        }
        return delayTimeList;
    }

    public ArrayList getZoneList(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList zoneList = new ArrayList();

        System.out.println("map = " + map);
        try {
            zoneList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getZoneList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getZoneList", sqlException);
        }
        return zoneList;
    }

    public ArrayList getBranchList(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList branchList = new ArrayList();

        System.out.println("map = " + map);
        try {
            branchList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getBranchList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBranchList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getBranchList", sqlException);
        }
        return branchList;
    }

    public ArrayList getRoyaltyPayList(String branchId, String roleId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList royaltyPayList = new ArrayList();

        System.out.println("map = " + map);
        try {
            map.put("branchId", branchId);
            map.put("roleId", roleId);
            royaltyPayList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getRoyaltyPayList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRoyaltyPayList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getRoyaltyPayList", sqlException);
        }
        return royaltyPayList;
    }

    public ArrayList getOrderTruckList(String consignmentOrderId, String roleId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList getOrderTruckList = new ArrayList();

        System.out.println("map = " + map);
        try {
            map.put("consignmentOrderId", consignmentOrderId);
            getOrderTruckList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getOrderTruckList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderTruckList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getOrderTruckList", sqlException);
        }
        return getOrderTruckList;
    }

    public ArrayList getServiceTaxList(String branchId, String roleId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList serviceTaxList = new ArrayList();

        System.out.println("map = " + map);
        try {
            map.put("branchId", branchId);
            map.put("roleId", roleId);
            serviceTaxList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getServiceTaxList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getServiceTaxList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getServiceTaxList", sqlException);
        }
        return serviceTaxList;
    }

    public int saveZoneMaster(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertStatus = 0;
        try {
            map.put("zoneId", opsTO.getZoneId());
            map.put("zoneName", opsTO.getZoneName());
            map.put("activeInd", opsTO.getActiveInd());
            System.out.println("Map for insertDelayTime" + map);

            if ("".equals(opsTO.getZoneId()) && opsTO.getZoneId() != null) {
                System.out.println("if");
                insertStatus = (Integer) getSqlMapClientTemplate().update("ops.saveZoneMaster", map);
                System.out.println("inserted status" + insertStatus);
            } else {
                System.out.println("else");
                //map.put("reasonId", operationTO.getReasonId());
                insertStatus = (Integer) getSqlMapClientTemplate().update("ops.updateZoneMaster", map);
                System.out.println("insertDelayTime status" + insertStatus);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveZoneMaster  Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "saveZoneMaster", sqlException);
        }
        return insertStatus;
    }

    public int saveZoneList(String[] zoneName) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertStatus = 0;
        try {

            for (int i = 0; i < zoneName.length; i++) {
                map.put("zoneName", zoneName[i]);
                map.put("activeInd", "Y");
                insertStatus = (Integer) getSqlMapClientTemplate().update("ops.saveZoneMaster", map);
                System.out.println("inserted status" + insertStatus);
            }
            System.out.println("Map for insertDelayTime" + map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveZoneMaster  Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "saveZoneMaster", sqlException);
        }
        return insertStatus;
    }

    public int saveCityList(String[] zoneName, String[] cityName, String[] cityCode, int userId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertStatus = 0;
        try {

            for (int i = 0; i < zoneName.length; i++) {
                map.put("zoneName", zoneName[i]);
                System.out.println("zoneName" + zoneName[i]);
                String zoneId = (String) getSqlMapClientTemplate().queryForObject("ops.getZoneId", map);
                map.put("zoneId", zoneId);
                System.out.println("zoneId" + zoneId);
                map.put("description", cityName[i]);
                map.put("cityName", cityCode[i]);
                System.out.println("Map for saveCityList" + map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("ops.saveCity", map);
                System.out.println("inserted status" + insertStatus);
            }
            System.out.println("Map for insertDelayTime" + map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveZoneMaster  Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "saveZoneMaster", sqlException);
        }
        return insertStatus;
    }

    public int saveBudgetList(String[] branchId, String[] movementType, String[] January, String[] February, String[] March, String[] April, String[] May, String[] June, String[] July, String[] August, String[] September, String[] October, String[] November, String[] December, int userId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ReportTO reportTO = new ReportTO();
        int insertStatus = 0;
        try {
            Calendar now = Calendar.getInstance();
            String currentMonth = (now.get(Calendar.MONTH) + 1) + "";
            String fromYear = "";
            String toYear = "";
            if (Integer.parseInt(currentMonth) > 3) {
                fromYear = now.get(Calendar.YEAR) + "";
                toYear = (now.get(Calendar.YEAR) + 1) + "";
            } else {
                fromYear = (now.get(Calendar.YEAR) - 1) + "";
                toYear = now.get(Calendar.YEAR) + "";
            }
            map.put("fromYear", fromYear);
            map.put("toYear", toYear);
            System.out.println("Hi I am here 4....................");
            ArrayList monthList = new ArrayList();
            monthList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getMonthList", map);
            for (int i = 0; i < branchId.length; i++) {
                System.out.println("Hi I am here 5....................");
                if (!"".equals(branchId[i])) {
                    System.out.println("Hi I am here 6....................");
                    Iterator itr = monthList.iterator();
                    while (itr.hasNext()) {
                        System.out.println("Hi I am here 7....................");
                        reportTO = new ReportTO();
                        reportTO = (ReportTO) itr.next();
                        map.put("branchId", branchId[i]);
                        if ("Bonded".equals(movementType[i])) {
                            map.put("movementType", 5);
                        } else {
                            map.put("movementType", 6);
                        }
                        map.put("year", reportTO.getFromDate());
                        if ("April".equals(reportTO.getMonthName())) {
                            map.put("weight", April[i]);
                        } else if ("May".equals(reportTO.getMonthName())) {
                            map.put("weight", May[i]);
                        } else if ("June".equals(reportTO.getMonthName())) {
                            map.put("weight", June[i]);
                        } else if ("July".equals(reportTO.getMonthName())) {
                            map.put("weight", July[i]);
                        } else if ("August".equals(reportTO.getMonthName())) {
                            map.put("weight", August[i]);
                        } else if ("September".equals(reportTO.getMonthName())) {
                            map.put("weight", September[i]);
                        } else if ("October".equals(reportTO.getMonthName())) {
                            map.put("weight", October[i]);
                        } else if ("November".equals(reportTO.getMonthName())) {
                            map.put("weight", November[i]);
                        } else if ("December".equals(reportTO.getMonthName())) {
                            map.put("weight", December[i]);
                        } else if ("January".equals(reportTO.getMonthName())) {
                            map.put("weight", January[i]);
                        } else if ("February".equals(reportTO.getMonthName())) {
                            map.put("weight", February[i]);
                        } else if ("March".equals(reportTO.getMonthName())) {
                            map.put("weight", March[i]);
                        }
                        System.out.println("saveBudgetList Excel Upload Map " + map);
                        insertStatus = (Integer) getSqlMapClientTemplate().update("ops.saveBudgetList", map);
                        System.out.println("saveBudgetList Excel Upload Status " + insertStatus);
                    }

                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveBudgetList  Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "saveBudgetList", sqlException);
        }
        return insertStatus;
    }

    public int saveExpenditureList(String[] branchId, String[] movementType, String[] January, String[] February, String[] March, String[] April, String[] May, String[] June, String[] July, String[] August, String[] September, String[] October, String[] November, String[] December, int userId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ReportTO reportTO = new ReportTO();
        int insertStatus = 0;
        try {

            Calendar now = Calendar.getInstance();
            String currentMonth = (now.get(Calendar.MONTH) + 1) + "";
            String fromYear = "";
            String toYear = "";
            if (Integer.parseInt(currentMonth) > 3) {
                fromYear = now.get(Calendar.YEAR) + "";
                toYear = (now.get(Calendar.YEAR) + 1) + "";
            } else {
                fromYear = (now.get(Calendar.YEAR) - 1) + "";
                toYear = now.get(Calendar.YEAR) + "";
            }
            map.put("fromYear", fromYear);
            map.put("toYear", toYear);
            ArrayList monthList = new ArrayList();
            monthList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getMonthList", map);
            for (int i = 0; i < branchId.length; i++) {
                if (!"".equals(branchId[i])) {
                    Iterator itr = monthList.iterator();
                    while (itr.hasNext()) {
                        reportTO = new ReportTO();
                        reportTO = (ReportTO) itr.next();
                        map.put("branchId", branchId[i]);
                        map.put("year", reportTO.getFromDate());
                        if ("Bonded".equals(movementType[i])) {
                            map.put("movementType", 5);
                        } else {
                            map.put("movementType", 6);
                        }
                        if ("April".equals(reportTO.getMonthName())) {
                            map.put("expenditure", April[i]);
                        } else if ("May".equals(reportTO.getMonthName())) {
                            map.put("expenditure", May[i]);
                        } else if ("June".equals(reportTO.getMonthName())) {
                            map.put("expenditure", June[i]);
                        } else if ("July".equals(reportTO.getMonthName())) {
                            map.put("expenditure", July[i]);
                        } else if ("August".equals(reportTO.getMonthName())) {
                            map.put("expenditure", August[i]);
                        } else if ("September".equals(reportTO.getMonthName())) {
                            map.put("expenditure", September[i]);
                        } else if ("October".equals(reportTO.getMonthName())) {
                            map.put("expenditure", October[i]);
                        } else if ("November".equals(reportTO.getMonthName())) {
                            map.put("expenditure", November[i]);
                        } else if ("December".equals(reportTO.getMonthName())) {
                            map.put("expenditure", December[i]);
                        } else if ("January".equals(reportTO.getMonthName())) {
                            map.put("expenditure", January[i]);
                        } else if ("February".equals(reportTO.getMonthName())) {
                            map.put("expenditure", February[i]);
                        } else if ("March".equals(reportTO.getMonthName())) {
                            map.put("expenditure", March[i]);
                        }
                        insertStatus = (Integer) getSqlMapClientTemplate().update("ops.saveExpenditureList", map);
                    }

                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveExpenditureList  Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "saveExpenditureList", sqlException);
        }
        return insertStatus;
    }

    public int saveTripList(String[] branchId, String[] movementType, String[] January, String[] February, String[] March, String[] April, String[] May, String[] June, String[] July, String[] August, String[] September, String[] October, String[] November, String[] December, int userId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ReportTO reportTO = new ReportTO();
        int insertStatus = 0;
        try {
            Calendar now = Calendar.getInstance();
            String currentMonth = (now.get(Calendar.MONTH) + 1) + "";
            String fromYear = "";
            String toYear = "";
            if (Integer.parseInt(currentMonth) > 3) {
                fromYear = now.get(Calendar.YEAR) + "";
                toYear = (now.get(Calendar.YEAR) + 1) + "";
            } else {
                fromYear = (now.get(Calendar.YEAR) - 1) + "";
                toYear = now.get(Calendar.YEAR) + "";
            }
            map.put("fromYear", fromYear);
            map.put("toYear", toYear);
            ArrayList monthList = new ArrayList();
            monthList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getMonthList", map);
            for (int i = 0; i < branchId.length; i++) {
                if (!"".equals(branchId[i])) {
                    Iterator itr = monthList.iterator();
                    while (itr.hasNext()) {
                        reportTO = new ReportTO();
                        reportTO = (ReportTO) itr.next();
                        map.put("branchId", branchId[i]);
                        map.put("year", reportTO.getFromDate());
                        if ("Bonded".equals(movementType[i])) {
                            map.put("movementType", 5);
                        } else {
                            map.put("movementType", 6);
                        }
                        if ("April".equals(reportTO.getMonthName())) {
                            map.put("trips", April[i]);
                        } else if ("May".equals(reportTO.getMonthName())) {
                            map.put("trips", May[i]);
                        } else if ("June".equals(reportTO.getMonthName())) {
                            map.put("trips", June[i]);
                        } else if ("July".equals(reportTO.getMonthName())) {
                            map.put("trips", July[i]);
                        } else if ("August".equals(reportTO.getMonthName())) {
                            map.put("trips", August[i]);
                        } else if ("September".equals(reportTO.getMonthName())) {
                            map.put("trips", September[i]);
                        } else if ("October".equals(reportTO.getMonthName())) {
                            map.put("trips", October[i]);
                        } else if ("November".equals(reportTO.getMonthName())) {
                            map.put("trips", November[i]);
                        } else if ("December".equals(reportTO.getMonthName())) {
                            map.put("trips", December[i]);
                        } else if ("January".equals(reportTO.getMonthName())) {
                            map.put("trips", January[i]);
                        } else if ("February".equals(reportTO.getMonthName())) {
                            map.put("trips", February[i]);
                        } else if ("March".equals(reportTO.getMonthName())) {
                            map.put("trips", March[i]);
                        }
                        insertStatus = (Integer) getSqlMapClientTemplate().update("ops.saveTripList", map);
                    }

                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveBudgetList  Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "saveBudgetList", sqlException);
        }
        return insertStatus;
    }

    public int saveRevenueList(String[] branchId, String[] movementType, String[] January, String[] February, String[] March, String[] April, String[] May, String[] June, String[] July, String[] August, String[] September, String[] October, String[] November, String[] December, int userId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertStatus = 0;
        ReportTO reportTO = new ReportTO();
        try {
            Calendar now = Calendar.getInstance();
            String currentMonth = (now.get(Calendar.MONTH) + 1) + "";
            String fromYear = "";
            String toYear = "";
            if (Integer.parseInt(currentMonth) > 3) {
                fromYear = now.get(Calendar.YEAR) + "";
                toYear = (now.get(Calendar.YEAR) + 1) + "";
            } else {
                fromYear = (now.get(Calendar.YEAR) - 1) + "";
                toYear = now.get(Calendar.YEAR) + "";
            }
            map.put("fromYear", fromYear);
            map.put("toYear", toYear);
            System.out.println("Hi I am here 1....................");
            ArrayList monthList = new ArrayList();
            monthList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getMonthList", map);
            for (int i = 0; i < branchId.length; i++) {
                System.out.println("Hi I am here 2....................");
                if (!"".equals(branchId[i])) {
                    System.out.println("Hi I am here 3....................");
                    Iterator itr = monthList.iterator();
                    while (itr.hasNext()) {
                        System.out.println("Hi I am here 4....................");
                        reportTO = new ReportTO();
                        reportTO = (ReportTO) itr.next();
                        map.put("branchId", branchId[i]);
                        map.put("year", reportTO.getFromDate());
                        if ("Bonded".equals(movementType[i])) {
                            map.put("movementType", 5);
                        } else {
                            map.put("movementType", 6);
                        }
                        if ("April".equals(reportTO.getMonthName())) {
                            map.put("revenue", April[i]);
                        } else if ("May".equals(reportTO.getMonthName())) {
                            map.put("revenue", May[i]);
                        } else if ("June".equals(reportTO.getMonthName())) {
                            map.put("revenue", June[i]);
                        } else if ("July".equals(reportTO.getMonthName())) {
                            map.put("revenue", July[i]);
                        } else if ("August".equals(reportTO.getMonthName())) {
                            map.put("revenue", August[i]);
                        } else if ("September".equals(reportTO.getMonthName())) {
                            map.put("revenue", September[i]);
                        } else if ("October".equals(reportTO.getMonthName())) {
                            map.put("revenue", October[i]);
                        } else if ("November".equals(reportTO.getMonthName())) {
                            map.put("revenue", November[i]);
                        } else if ("December".equals(reportTO.getMonthName())) {
                            map.put("revenue", December[i]);
                        } else if ("January".equals(reportTO.getMonthName())) {
                            map.put("revenue", January[i]);
                        } else if ("February".equals(reportTO.getMonthName())) {
                            map.put("revenue", February[i]);
                        } else if ("March".equals(reportTO.getMonthName())) {
                            map.put("revenue", March[i]);
                        }
                        System.out.println("saveRevenueList " + map);
                        insertStatus = (Integer) getSqlMapClientTemplate().update("ops.saveRevenueList", map);
                        System.out.println("saveRevenueList Status...................." + insertStatus);
                    }
                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveRevenueList  Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "saveRevenueList", sqlException);
        }
        return insertStatus;
    }

    public String getAwbNoOLD() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        String awbNo = "";
        int count = 0;
        int status = 0;
        String ind = "N";
        try {
            System.out.println("Map for getAwbNo" + map);
            String oldAwbNo = (String) getSqlMapClientTemplate().queryForObject("ops.getAwbNo", map);
            String newAwbNo = Integer.parseInt(oldAwbNo) + 1 + "";
            for (int i = 0; i < 10; i++) {
                if (count == 0) {
                    if (ind.equals("Y")) {
                        newAwbNo = Integer.parseInt(newAwbNo) + 1 + "";
                    }
                    String firstValue = newAwbNo;
                    System.out.println(" firstValue" + firstValue);
                    int value = Integer.parseInt(firstValue) / 7;
                    System.out.println(" value" + value);
                    int value1 = value * 7;
                    System.out.println("value1" + value1);
                    int value3 = Integer.parseInt(firstValue) - value1;
                    System.out.println("value3" + value3);
                    String awbno = newAwbNo;
                    System.out.println("awbno" + awbno);
                    StringBuilder str = new StringBuilder(awbno);
                    if (value3 < 6) {
                        count = 1;
                        System.out.println("status " + status);
                        str.insert(7, String.valueOf(value3));
                        awbNo = String.valueOf(str);
                        ind = "N";
                    } else {
                        count = 0;
                        ind = "Y";
                    }
                }
            }
            System.out.println("new1111111" + awbNo);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAwbNo  Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getAwbNo", sqlException);
        }
        return awbNo;
    }

    public String getAwbNo() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        String awbNo = "";
        int count = 0;
        boolean lastDigitStatus = true;
        String oldAwbNo = "";
        try {
            System.out.println("Map for getAwbNo" + map);
            oldAwbNo = (String) getSqlMapClientTemplate().queryForObject("ops.getAwbNo", map);
            int rmDigit = 0;
            int value = 0;
            int value1 = 0;
            int value3 = 0;
            int lastDigit = Integer.parseInt(oldAwbNo.substring(oldAwbNo.length() - 1));
            //System.out.println(" lastDigit ===   " + oldAwbNo.length()-1);
            while (lastDigitStatus) {
                if (lastDigit > 6) {
                    lastDigitStatus = true;
                    count = Integer.parseInt(oldAwbNo) + 1;
                    oldAwbNo = count + "";
                    System.out.println("newAwbNo  === " + oldAwbNo);
                    lastDigit = Integer.parseInt(oldAwbNo.substring(oldAwbNo.length() - 1));
                    System.out.println("lastDigit === " + lastDigit);
                } else {
                    System.out.println(" lastDigit" + lastDigit);
                    rmDigit = Integer.parseInt(oldAwbNo.substring(0, oldAwbNo.length() - 1));
                    value = rmDigit / 7;
                    value1 = value * 7;
                    value3 = rmDigit - value1;
                    System.out.println(" value3 " + value3);
                    System.out.println(" lastDigit " + lastDigit);
                    System.out.println(" rmDigit " + rmDigit);
                    if (value3 > 6 || value3 != lastDigit) {
                        lastDigitStatus = true;
                        count = Integer.parseInt(oldAwbNo) + 1;
                        oldAwbNo = count + "";
                        System.out.println("newAwbNo  === " + oldAwbNo);
                        lastDigit = Integer.parseInt(oldAwbNo.substring(oldAwbNo.length() - 1));
                        System.out.println("lastDigit === " + lastDigit);
                    } else {
                        //System.out.println(" str = " + str);
                        //str.insert(7, String.valueOf(value3));
                        //awbNo = String.valueOf(str);
                        System.out.println("Arul Here=== ");
                        awbNo = rmDigit + "" + lastDigit;
                        lastDigitStatus = false;
                    }
                }

            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAwbNo  Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getAwbNo", sqlException);
        }
        return awbNo;
    }

    public String getBranchId(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        String branchId = "";
        try {
            map.put("branchCode", opsTO.getBranchName().trim());
            System.out.println("Map for branchName" + map);
            branchId = (String) getSqlMapClientTemplate().queryForObject("ops.getBranchId", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAwbNo  Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getAwbNo", sqlException);
        }
        return branchId;
    }

    public ArrayList getDepreciationChargeList(String branchId, String roleId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList depreciationChargeList = new ArrayList();

        System.out.println("map = " + map);
        try {
            map.put("branchId", branchId);
            map.put("roleId", roleId);
            depreciationChargeList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getDepreciationChargeList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDepreciationChargeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getDepreciationChargeList", sqlException);
        }
        return depreciationChargeList;
    }

    public int saveDepreciationChargeMaster(OpsTO opsTO, int userId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertStatus = 0;
        try {
            map.put("depreciationId", opsTO.getDepreciationChargeId());
            map.put("depreciationName", opsTO.getDepreciationChargeName());
            map.put("totalCost", opsTO.getTotalCost());
            map.put("percentage", opsTO.getPercentage());
            map.put("activeInd", opsTO.getActiveInd());
            map.put("userId", userId);
            System.out.println("Map for saveDepreciationChargeMaster" + map);

            int count = (Integer) getSqlMapClientTemplate().queryForObject("ops.getDepreciationNameCount", map);
            if ("".equals(opsTO.getDepreciationChargeId()) && count == 0) {
                insertStatus = (Integer) getSqlMapClientTemplate().update("ops.insertDepreciationChargeMaster", map);
            } else {
                insertStatus = (Integer) getSqlMapClientTemplate().update("ops.updateDepreciationChargeMaster", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveDepreciationChargeMaster  Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "saveDepreciationChargeMaster", sqlException);
        }
        return insertStatus;
    }

    public ArrayList getExpenseTypeList(String branchId, String roleId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList expenseList = new ArrayList();

        System.out.println("map = " + map);
        try {
            map.put("branchId", branchId);
            map.put("roleId", roleId);
            expenseList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getExpenseTypeList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getExpenseTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getExpenseTypeList", sqlException);
        }
        return expenseList;
    }

    public ArrayList getGpsDeviceList(String branchId, String roleId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList deviceList = new ArrayList();

        System.out.println("map = " + map);
        try {
            map.put("branchId", branchId);
            map.put("roleId", roleId);
            deviceList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getGpsDeviceList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getGpsDeviceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getGpsDeviceList", sqlException);
        }
        return deviceList;
    }

    public int saveExpenseTypeMaster(OpsTO opsTO, int userId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertStatus = 0;
        try {
            map.put("expenseId", opsTO.getExpenseId());
            map.put("expenseName", opsTO.getExpenseName());
            map.put("expenseType", opsTO.getExpenseType());
            map.put("activeInd", opsTO.getActiveInd());
            map.put("userId", userId);
            map.put("ledgerCode", "LEDGER-32");
            map.put("ledgerId", "40");
            System.out.println("Map for saveExpenseTypeMaster" + map);

            int count = (Integer) getSqlMapClientTemplate().queryForObject("ops.getExpenseNameCount", map);
            if ("".equals(opsTO.getExpenseId()) && count == 0) {
                insertStatus = (Integer) getSqlMapClientTemplate().update("ops.insertExpenseTypeMaster", map);
            } else {
                insertStatus = (Integer) getSqlMapClientTemplate().update("ops.updateExpenseTypeMaster", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveExpenseTypeMaster  Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "saveExpenseTypeMaster", sqlException);
        }
        return insertStatus;
    }

    public int saveGpsDeviceMaster(OpsTO opsTO, int userId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertStatus = 0;
        try {
            map.put("deviceId", opsTO.getDeviceId());
            map.put("deviceName", opsTO.getDeviceName());
            map.put("activeInd", opsTO.getActiveInd());
            map.put("userId", userId);
            System.out.println("Map for saveGpsDeviceMaster" + map);

            int count = (Integer) getSqlMapClientTemplate().queryForObject("ops.getGpsDeviceNameCount", map);
            if ("".equals(opsTO.getDeviceId()) && count == 0) {
                insertStatus = (Integer) getSqlMapClientTemplate().update("ops.insertGpsDeviceMaster", map);
            } else {
                insertStatus = (Integer) getSqlMapClientTemplate().update("ops.updateGpsDeviceMaster", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveExpenseTypeMaster  Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "saveExpenseTypeMaster", sqlException);
        }
        return insertStatus;
    }

    public int saveMappingGpsDevice(OpsTO opsTO, int userId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int insertStatus = 0;
        try {
            map.put("deviceId", opsTO.getDeviceId());
            map.put("branchId", opsTO.getBranchId());
            map.put("effectiveDate", opsTO.getEffectiveDate());
            map.put("statusId", opsTO.getStatusId());
            map.put("activeInd", opsTO.getActiveInd());
            map.put("userId", userId);
            System.out.println("Map for saveGpsDeviceMaster" + map);

            int count = (Integer) getSqlMapClientTemplate().queryForObject("ops.getGpsDeviceId", map);
            if ("".equals(opsTO.getStatusId()) && count == 0) {
                insertStatus = (Integer) getSqlMapClientTemplate().update("ops.insertGpsDeviceMapping", map);
            } else {
                insertStatus = (Integer) getSqlMapClientTemplate().update("ops.updateGpsDeviceMapping", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveExpenseTypeMaster  Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "saveExpenseTypeMaster", sqlException);
        }
        return insertStatus;
    }

    public String getConsignmentDataList(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        String dataList = "";
        try {
            map.put("vehicleRegNo", opsTO.getVehicleRegNo());
            map.put("origin", opsTO.getOrigin());
            map.put("destination", opsTO.getDestination());
            map.put("customerName", opsTO.getCustomerName());
            map.put("vehicleTypeName", opsTO.getVehicleTypeName());
            map.put("vendorName", opsTO.getVendorName());
            map.put("awbDestination", opsTO.getAwbDestinationId());

            String vehicleId = (String) getSqlMapClientTemplate().queryForObject("ops.getVehicleId", map);
            if ("".equals(vehicleId)) {
                vehicleId = "0";
            }
            String originId = (String) getSqlMapClientTemplate().queryForObject("ops.getOriginId", map);
            if ("".equals(originId)) {
                originId = "0";
            }
            String destinationId = (String) getSqlMapClientTemplate().queryForObject("ops.getDestinationId", map);
            if ("".equals(destinationId)) {
                destinationId = "0";
            }
            String customerId = (String) getSqlMapClientTemplate().queryForObject("ops.getCustomerName", map);
            if ("".equals(customerId)) {
                customerId = "0";
            }
            String vehicleTypeId = (String) getSqlMapClientTemplate().queryForObject("ops.getVehicleFleetCode", map);
            if ("".equals(vehicleTypeId)) {
                vehicleTypeId = "0";
            }
            String awbDestination = (String) getSqlMapClientTemplate().queryForObject("ops.getAwbDestinationId", map);
            if ("".equals(awbDestination)) {
                awbDestination = "0";
            }
            String vendorId = (String) getSqlMapClientTemplate().queryForObject("ops.getVendorId", map);
            if ("".equals(vendorId)) {
                vendorId = "0";
            }
            dataList = vehicleId + "~" + originId + "~" + destinationId + "~" + customerId + "~" + vehicleTypeId + "~" + vendorId + "~" + awbDestination;
            System.out.println(" dataList value is" + dataList);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAwbNo  Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getAwbNo", sqlException);
        }
        return dataList;
    }

    public ArrayList getDeviceListDetails(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList deviceList = new ArrayList();

        System.out.println("map = " + map);
        try {
            map.put("effectiveDate", opsTO.getEffectiveDate());
            deviceList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getDeviceListDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDeviceListDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getDeviceListDetails", sqlException);
        }
        return deviceList;
    }

    public ArrayList getCheckAwbNo(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList checkAwbNo = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("awbNo", opsTO.getAwbNo());
        map.put("shipmentType", opsTO.getShipmentType());

        System.out.println("map = " + map);
        try {
            checkAwbNo = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getCheckAwbNo", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("fleetCodeGroup Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "fleetCodeGroup", sqlException);
        }
        return checkAwbNo;
    }

    public ArrayList getPointList() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList getPointList = new ArrayList();
        try {
            getPointList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getPointList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPointList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getPointList", sqlException);
        }
        return getPointList;
    }

    public ArrayList getCustomerList() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList getCustomerList = new ArrayList();
        try {
            getCustomerList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getCustomerList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCustomerList", sqlException);
        }
        return getCustomerList;
    }

    public int insertConsignmentNote(OperationTO operationTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int insertConsignmentNote = 0;
        int updateStandardCharge = 0;
        int index = 0;
        map.put("userId", userId);
//        try {
        System.out.println("operationTO.getCnoteCount() = " + operationTO.getCnoteCount());
        try {
            map.put("allowedTrucks", operationTO.getAllowedTrucks());
            if (operationTO.getTruckNos() == null) {
                operationTO.setTruckNos("0");
            }
            if ("".equals(operationTO.getTruckNos())) {
                operationTO.setTruckNos("0");
            }
            map.put("truckNos", operationTO.getTruckNos());

            if (!"0".equals(operationTO.getAwbNo())) {
                map.put("awbno", operationTO.getAwbNo());
                int status = (Integer) session.insert("ops.insertAWBNo", map);
            }
            map.put("airlineName", operationTO.getAirlineName());
            int retVal = Double.compare(operationTO.getCreditLimitAmount(), operationTO.getOutStanding());
            if (!operationTO.getBillingTypeId().equals("4")) {
//                      if (retVal > 0) {
                if (operationTO.getCnoteCount() == 0) {
                    //System.out.println("operationTO.getCreditLimitAmount() is greater than operationTO.getOutStanding()");
                    map.put("consignmentStatusId", "5");
                    operationTO.setStatus("5");
                } else if (operationTO.getCnoteCount() > 0) {
                    //System.out.println("operationTO.getCreditLimitAmount() is lesser than operationTO.getOutStanding()");
                    map.put("consignmentStatusId", "3");
                    operationTO.setStatus("3");
                } else {
                    //System.out.println("operationTO.getCreditLimitAmount() is equal to operationTO.getOutStanding()");
                    map.put("consignmentStatusId", "5");
                    operationTO.setStatus("5");
                }
            } else {
                map.put("consignmentStatusId", "5");
                operationTO.setStatus("5");
            }
            map.put("consignmentStatusId", "5");
            operationTO.setStatus("5");
            map.put("awbTotalGrossWeight", operationTO.getAwbTotalGrossWeight());
            map.put("awbTotalPackages", operationTO.getAwbTotalPackages());
            map.put("awbReceivedPackages", operationTO.getAwbReceivedPackages());
            map.put("awbPendingPackages", operationTO.getAwbPendingPackages());

            map.put("shipmentType", operationTO.getShipmentType());
            map.put("branchId", operationTO.getBranchId());
            map.put("customerTypeId", operationTO.getCustomerTypeId());
            map.put("billingTypeId", operationTO.getBillingTypeId());
            map.put("entryType", operationTO.getEntryType());
            map.put("consignmentNoteNo", operationTO.getConsignmentNoteNo());
            map.put("consignmentDate", operationTO.getConsignmentDate());
            map.put("orderReferenceNo", operationTO.getOrderReferenceNo());
            map.put("orderReferenceRemarks", operationTO.getBookingRemarks());
//           
            map.put("awbOriginRegion", operationTO.getAwbOriginRegion());

//            map.put("orderReferenceAwbNo", operationTO.getAirWayBillNo());
            map.put("orderReferenceAwbNo", operationTO.getOrderReferenceAwb() + " " + operationTO.getOrderReferenceAwbNo());
            map.put("awbOrigin", operationTO.getAwbOrigin());
            map.put("awbDestination", operationTO.getAwbDestination());
            map.put("awbOrderDeliveryDate", operationTO.getAwbOrderDeliveryDate());
            map.put("awbDestinationRegion", operationTO.getAwbDestinationRegion());
            map.put("orderDeliveryTime", operationTO.getOrderDeliveryHour() + ":" + operationTO.getOrderDeliveryMinute() + ":00");
            map.put("awbMovementType", operationTO.getAwbMovementType());
            map.put("noOfPieces", operationTO.getNoOfPieces());
            map.put("grossWeigtht", operationTO.getGrossWeight());
//            map.put("chargeableWeigtht", operationTO.getChargeableWeight());
            map.put("chargeableWeigthtId", operationTO.getChargeableWeightId());
            map.put("freightReceipt", operationTO.getFreightReceipt());
            map.put("wareHousName", operationTO.getWareHouseName());
            map.put("wareHousLocation", operationTO.getWareHouseLocation());
            map.put("shipmentAcceptanceDate", operationTO.getShipmentAcceptanceDate());
            map.put("shipmentAccpTime", operationTO.getShipmentAccpHour() + ":" + operationTO.getShipmentAccpMinute() + ":00");
            map.put("currencyType", operationTO.getCurrencyType());
            map.put("rateMode", operationTO.getRateMode());
            if (operationTO.getPerKgRate() != null && !"".equals(operationTO.getPerKgRate())) {
                map.put("perKgRate", operationTO.getPerKgRate());
            } else {
                map.put("perKgRate", "0");
            }
            if (operationTO.getRateValue() != null && !"".equals(operationTO.getRateValue())) {
                map.put("rateValue", operationTO.getRateValue());
            } else {
                map.put("rateValue", "0");
            }

            map.put("contract", operationTO.getContract());

            map.put("grossWeight", operationTO.getGrossWeight());
            map.put("chargeableWeight", operationTO.getTotalChargeableWeights());
            map.put("totalVolume", operationTO.getTotalVolume());
            map.put("totalVolumeActual", operationTO.getTotalVolumeActual());
            map.put("commodity", operationTO.getCommodity());
            map.put("productRate", operationTO.getProductRate());

//            map.put("paymentType", operationTO.getPaymentType());
            map.put("paymentType", "1");
            if (operationTO.getProductCategoryId() != "") {
                map.put("productCategoryId", operationTO.getProductCategoryId());
            } else {
                map.put("productCategoryId", 0);
            }
            map.put("origin", operationTO.getOrigin());
            map.put("destinaion", operationTO.getDestination());
            if (operationTO.getBusinessType() != "") {
                map.put("businessType", operationTO.getBusinessType());
            } else {
                map.put("businessType", 0);
            }
            if (operationTO.getMultiPickup() != null) {
                map.put("multiPickup", operationTO.getMultiPickup());
            }
            if (operationTO.getMultiDelivery() != null) {
                map.put("multiDelivery", operationTO.getMultiDelivery());
            }
            map.put("consignmentOrderInstruction", operationTO.getConsignmentOrderInstruction());
            if (operationTO.getTotalPackage() != "") {
                map.put("totalPackage", operationTO.getTotalPackage());
            } else {
                map.put("totalPackage", 0);
            }
            if (operationTO.getTotalWeightage() != "") {
                map.put("totalWeightage", operationTO.getTotalWeightage());
            } else {
                map.put("totalWeightage", 0);
            }
            if (operationTO.getServiceType() != "") {
                map.put("serviceType", operationTO.getServiceType());
            } else {
                map.put("serviceType", 0);
            }
            if (operationTO.getVehicleTypeId() != "") {
                map.put("vehicleTypeId", operationTO.getVehicleTypeId());
            } else {
                map.put("vehicleTypeId", 0);
            }
            map.put("reeferRequired", operationTO.getReeferRequired());
            if (operationTO.getTotalKm() != "") {
                map.put("totalKm", operationTO.getTotalKm());
            } else {
                map.put("totalKm", 0);
            }
            if (operationTO.getTotalHours() != "") {
                map.put("totalHours", operationTO.getTotalHours());
            } else {
                map.put("totalHours", 0);
            }
            if (operationTO.getTotalMinutes() != "") {
                map.put("totalMinutes", operationTO.getTotalMinutes());
            } else {
                map.put("totalMinutes", 0);
            }
            map.put("vehicleRequiredDate", operationTO.getVehicleRequiredDate());
//            map.put("vehicleRequiredTime", operationTO.getVehicleRequiredHour() + ":" + operationTO.getVehicleRequiredMinute() + ":00");
            map.put("vehicleRequiredTime", "00" + ":" + "00" + ":00");
            map.put("vehicleInstruction", operationTO.getVehicleInstruction());
            map.put("consignorName", operationTO.getConsignorName());
            map.put("consignorPhoneNo", operationTO.getConsignorPhoneNo());
            map.put("consignorAddress", operationTO.getConsignorAddress());
            map.put("consigneeName", operationTO.getConsigneeName());
            map.put("consigneePhoneNo", operationTO.getConsigneePhoneNo());
            map.put("consigneeAddress", operationTO.getConsigneeAddress());
            map.put("invoiceType", "");
            if (operationTO.getTotFreightAmount() != "") {
                map.put("totalFreightAmount", operationTO.getTotFreightAmount());
            } else {
                map.put("totalFreightAmount", 0);
            }
            if (operationTO.getSubTotal() != "") {
                map.put("standardChargeTotal", operationTO.getSubTotal());
            } else {
                map.put("standardChargeTotal", 0);
            }
            if (operationTO.getTotalCharges() != "" && !"NaN".equals(operationTO.getTotalCharges())) {
                map.put("totalCharges", operationTO.getTotalCharges());
            } else {
                map.put("totalCharges", 0);
            }
            map.put("totalCharges", 0);
            // Shipment Type
//            insertConsignmentNote = (Integer) session.queryForObject("operation.checkAwbNoExists", map);
            if (operationTO.getCustomerTypeId().equals("1")) {
                if (operationTO.getContractRateId() != "") {
                    map.put("contractRateId", operationTO.getContractRateId());
                } else {
                    map.put("contractRateId", 0);
                }

                map.put("customerId", operationTO.getCustomerId());
                map.put("customerName", operationTO.getCustomerName());
                map.put("customerCode", operationTO.getCustomerCode());
                map.put("customerAddress", operationTO.getCustomerAddress());
                map.put("pincode", operationTO.getPincode());
                map.put("customerMobileNo", operationTO.getCustomerMobileNo());
                map.put("mailId", operationTO.getMailId());
                map.put("customerPhoneNo", operationTO.getCustomerPhoneNo());
                map.put("contractId", operationTO.getContractId());
                float rateWithReefer = 0.00F;
                float rateWithoutReefer = 0.00F;
                if (operationTO.getRateWithReefer() != "") {
                    rateWithReefer = Float.parseFloat(operationTO.getRateWithReefer());
                } else {
                    rateWithReefer = 0;
                }
                if (operationTO.getRateWithoutReefer() != "") {
                    rateWithoutReefer = Float.parseFloat(operationTO.getRateWithoutReefer());
                } else {
                    rateWithoutReefer = 0;
                }
                if (operationTO.getBillingTypeId().equals("1")) {
                    map.put("routeId", 0);
//                    map.put("routeContractId", operationTO.getRouteContractId());
                    map.put("routeContractId", 0);
                    if (operationTO.getReeferRequired().equals("Yes")) {
                        map.put("fixedRate", rateWithReefer);
                    } else if (operationTO.getReeferRequired().equals("No")) {
                        map.put("fixedRate", rateWithoutReefer);
                    } else {
                        map.put("fixedRate", "0");
                    }
                    map.put("ratePerKg", "0");
                    map.put("ratePerKm", "0");
                    System.out.println("map = " + map);

                    insertConsignmentNote = (Integer) session.insert("ops.insertConsignmentNote", map);
                } else if (operationTO.getBillingTypeId().equals("4")) {
                    map.put("routeId", 0);
//                    map.put("routeContractId", operationTO.getRouteContractId());
                    map.put("routeContractId", 0);
                    if (operationTO.getReeferRequired().equals("Yes")) {
                        map.put("fixedRate", rateWithReefer);
                    } else if (operationTO.getReeferRequired().equals("No")) {
                        map.put("fixedRate", rateWithoutReefer);
                    } else {
                        map.put("fixedRate", "0");
                    }
                    map.put("ratePerKg", "0");
                    map.put("ratePerKm", "0");
                    System.out.println("map = " + map);

                    insertConsignmentNote = (Integer) session.insert("ops.insertConsignmentNote", map);
                } else if (operationTO.getBillingTypeId().equals("2")) {
                    map.put("routeId", 0);
//                    map.put("routeContractId", operationTO.getRouteContractId());
                    map.put("routeContractId", 0);
                    if (operationTO.getReeferRequired().equals("Yes")) {
                        map.put("ratePerKg", rateWithReefer);
                    } else if (operationTO.getReeferRequired().equals("No")) {
                        map.put("ratePerKg", rateWithoutReefer);
                    } else {
                        map.put("ratePerKg", "0");
                    }
                    map.put("fixedRate", "0");
                    map.put("ratePerKm", "0");
                    System.out.println("map = " + map);
                    insertConsignmentNote = (Integer) session.insert("ops.insertConsignmentNote", map);
                } else if (operationTO.getBillingTypeId().equals("3")) {
                    map.put("routeId", operationTO.getRouteId());
                    map.put("routeContractId", 0);
                    if (operationTO.getReeferRequired().equals("Yes")) {
                        map.put("ratePerKm", rateWithReefer);
                    } else if (operationTO.getReeferRequired().equals("No")) {
                        map.put("ratePerKm", rateWithoutReefer);
                    } else {
                        map.put("ratePerKm", "0");
                    }
                    map.put("fixedRate", "0");
                    map.put("ratePerKg", "0");
                    System.out.println("map = " + map);
                    insertConsignmentNote = (Integer) session.insert("ops.insertConsignmentNote", map);
                }
            } else if (operationTO.getCustomerTypeId().equals("2")) {
                map.put("customerName", operationTO.getWalkinCustomerName());
                map.put("customerCode", operationTO.getWalkinCustomerCode());
                map.put("customerAddress", operationTO.getWalkinCustomerAddress());
                map.put("pincode", operationTO.getWalkinPincode());
                map.put("customerMobileNo", operationTO.getWalkinCustomerMobileNo());
                map.put("mailId", operationTO.getWalkinMailId());
                map.put("customerPhoneNo", operationTO.getWalkinCustomerPhoneNo());
                map.put("billingTypeId", operationTO.getWalkInBillingTypeId());
                map.put("contractId", 0);
                if (operationTO.getWalkInBillingTypeId().equals("1")) {
                    if (operationTO.getReeferRequired().equals("Yes")) {
                        map.put("fixedRate", operationTO.getWalkinFreightWithReefer());
                    } else if (operationTO.getReeferRequired().equals("No")) {
                        map.put("fixedRate", operationTO.getWalkinFreightWithoutReefer());
                    } else {
                        map.put("fixedRate", "0");
                    }
                    map.put("ratePerKg", "0");
                    map.put("ratePerKm", "0");
                } else if (operationTO.getWalkInBillingTypeId().equals("2")) {
                    if (operationTO.getReeferRequired().equals("Yes")) {
                        map.put("ratePerKg", operationTO.getWalkinRateWithReeferPerKg());
                    } else if (operationTO.getReeferRequired().equals("No")) {
                        map.put("ratePerKg", operationTO.getWalkinRateWithoutReeferPerKg());
                    } else {
                        map.put("ratePerKg", "0");
                    }
                    map.put("fixedRate", "0");
                    map.put("ratePerKm", "0");
                } else if (operationTO.getWalkInBillingTypeId().equals("3")) {
                    if (operationTO.getReeferRequired().equals("Yes")) {
                        map.put("ratePerKm", operationTO.getWalkinRateWithReeferPerKm());
                    } else if (operationTO.getReeferRequired().equals("No")) {
                        map.put("ratePerKm", operationTO.getWalkinRateWithoutReeferPerKm());
                    } else {
                        map.put("ratePerKm", "0");
                    }
                    map.put("fixedRate", "0");
                    map.put("ratePerKg", "0");
                }

                if (operationTO.getRouteId() != "") {
                    map.put("routeId", operationTO.getRouteId());
                } else {
                    map.put("routeId", 0);
                }
                map.put("contractRateId", 0);
                int insertCustomer = 0;
                insertCustomer = (Integer) session.insert("operation.insertCustomerDetails", map);
                if (insertCustomer > 0) {
                    map.put("customerId", insertCustomer);
                }
                map.put("routeContractId", 0);
                System.out.println("map = " + map);
                insertConsignmentNote = (Integer) session.insert("ops.insertConsignmentNote", map);
            }
            map.put("consignmentStatusId", "5");
            map.put("consignmentOrderId", insertConsignmentNote);
            map.put("updateType", "System Update");
            map.put("remarks", "");
            System.out.println("map consignment order status  = " + map);
            int insertConsignmentStatus = (Integer) session.update("operation.insertConsignmentStatus", map);

            //insert into cons route course start
            map.put("consignmentId", insertConsignmentNote);
            map.put("pointId", operationTO.getOriginId());
            map.put("pointType", "Pick Up");
            map.put("pointAddress", operationTO.getOriginName());
            map.put("pointSequence", "1");
            int rcstatus = (Integer) session.update("ops.insertMultiplePoints", map);
            map.put("pointId", operationTO.getDestinationId());
            map.put("pointType", "Drop");
            map.put("pointAddress", operationTO.getDestinationName());
            map.put("pointSequence", "2");
            rcstatus = (Integer) session.update("ops.insertMultiplePoints", map);
                //insert into cons route course end

//            } catch (Exception ex) {
//                Writer writer = new StringWriter();
//                PrintWriter printWriter = new PrintWriter(writer);
//                ex.printStackTrace(printWriter);
//                String s = writer.toString();
//                System.out.println("s = " + s);
//            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertConsignmentNote Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertConsignmentNote", sqlException);
        }

        return insertConsignmentNote;
    }

    public int insertConsignmentArticle(OperationTO operationTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int index = 0;
        map.put("userId", userId);
        String hr = "";
        String min = "";
        int insertConsignmentArticle = 0;
        try {
            map.put("consignmentId", operationTO.getConsignmentOrderId());
            String[] noOfPieces = null;
            String[] grossWeight = null;
            String[] chargeableWeight = null;
            String[] chargeableWeightId = null;
            String[] lengths = null;
            String[] widths = null;
            String[] heights = null;
            String[] batch = null;
            String[] uom = null;
            String[] volume = null;
            noOfPieces = operationTO.getNoOfPieces();
            uom = operationTO.getUom();
            chargeableWeight = operationTO.getChargeableWeight();
            chargeableWeightId = operationTO.getChargeableWeightId();
            lengths = operationTO.getLengths();
            widths = operationTO.getWidths();
            heights = operationTO.getHeights();
            volume = operationTO.getVolumes();
            for (int i = 0; i < noOfPieces.length; i++) {
                map.put("noOfPieces", noOfPieces[i]);
                map.put("uom", uom[i]);
                map.put("chargeableWeight", chargeableWeight[i]);
                map.put("lengths", lengths[i]);
                map.put("widths", widths[i]);
                map.put("heights", heights[i]);
                map.put("volume", volume[i]);
                System.out.println("Article Insert====" + map);
                insertConsignmentArticle = (Integer) session.update("ops.insertConsignmentArticle", map);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertConsignmentArticle Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertConsignmentArticle", sqlException);
        }

        return insertConsignmentArticle;
    }

    public int insertMultiplePoints(OperationTO operationTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int insertMultiplePoints = 0;
        int index = 0;
        map.put("userId", userId);
        String hr = "";
        String min = "";
        int truckInsertStatus = 0;
        try {
//            try {
            String consignmentId = operationTO.getConsignmentOrderId();
            String rateMode = operationTO.getRateMode();
            String perKgRate = operationTO.getPerKgRate();
            String rateMode1 = operationTO.getRateMode1();
            String rateValue = operationTO.getRateValue();
            String Contract = operationTO.getContract();
            String totalChargeableWeights = operationTO.getTotalChargeableWeights();
            String[] pointId = operationTO.getPointId();
            String[] destinationPointId = operationTO.getTruckDestinationId();
            String[] pointRouteId = operationTO.getTruckRouteId();
            String[] pointType = operationTO.getPointType();
            String[] pointSequence = operationTO.getOrder();
            String[] pointAddress = operationTO.getPointAddresss();
            String[] routeId = operationTO.getMultiplePointRouteId();
            String[] pointPlanDate = operationTO.getPointPlanDate();
            String[] truckDepDate = operationTO.getTruckDepDate();
            String[] pointPlanHour = operationTO.getPointPlanHour();
            String[] pointPlanMinute = operationTO.getPointPlanMinute();
            String[] fleetTypeId = operationTO.getFleetTypeId();
            String[] truckTravelKm = operationTO.getTruckTravelKm();
            String[] truckTravelHour = operationTO.getTruckTravelHour();
            String[] truckTravelMinute = operationTO.getTruckTravelMinute();
            String[] vehicleNo = operationTO.getVehicleNos();
            String[] vehicleId = operationTO.getVehicleIds();
            String[] availableWeight = operationTO.getAvailableWeight();
            String[] availableVolume = operationTO.getAvailableVolume();
            String[] usedCapacity = operationTO.getUsedCapacity();
            String[] receivedPcs = operationTO.getReceivedPackages();
            String[] usedVol = operationTO.getUsedVol();
            map.put("consignmentId", consignmentId);
            map.put("userId", userId);
            map.put("pointId", operationTO.getOrigin());
            map.put("destinationPointId", operationTO.getDestination());
            map.put("routeFromId", operationTO.getOrigin());
            map.put("routeNameTo", operationTO.getDestination());
            System.out.println("fleetTypeId.length:" + fleetTypeId.length);
            System.out.println("pointId.length:" + pointRouteId.length);
            ArrayList routeDetails = (ArrayList) session.queryForList("operation.getRouteList", map);
            Iterator itr = routeDetails.iterator();
            OperationTO opTO = null;
            while (itr.hasNext()) {
                opTO = new OperationTO();
                opTO = (OperationTO) itr.next();
                map.put("pointRouteId", opTO.getRouteId());
                map.put("truckTravelKm", opTO.getDistance());
                map.put("truckTravelHour", opTO.getTravelHour());
                map.put("truckTravelMinute", opTO.getTravelMinute());

            }
            boolean insertStatus = false;
            if (fleetTypeId.length > 0) {
                for (int i = 0; i < fleetTypeId.length; i++) {
                    map.put("pointType", "Pickup");
                    map.put("routeId", 0);
                    map.put("pointPlanDate", pointPlanDate[i]);
                    map.put("truckDepDate", truckDepDate[i]);
                    map.put("fleetTypeId", fleetTypeId[i]);
                    if ("".equals(hr)) {
                        hr = "00";
                    }
                    if ("".equals(min)) {
                        min = "00";
                    }
                    map.put("pointPlanTime", 00 + ":" + 00 + ":00");
                    String[] truck_code = vehicleNo[i].split("-");
                    map.put("vehicleNo", truck_code[1]);
                    map.put("vehicleId", vehicleId[i]);
                    map.put("availableWeight", availableWeight[i]);
                    map.put("availableVolume", availableVolume[i]);
                    map.put("usedCapacity", usedCapacity[i]);
                    map.put("receivedPcs", receivedPcs[i]);
                    /* Comment By Arul and Lawrence On 22-06-2016 For to Email Purpose  
                     if ("2".equals(operationTO.getShipmentType())) {
                     } else {
                     map.put("receivedPcs", "0");
                     }
                     */
                    map.put("usedVol", usedVol[i]);
                    double chargeableWeightcal = 0.00;
                    double freightChargesCal = 0.00;
                    double VolCal = 166.67;
                    if ("".equals(perKgRate) || perKgRate == null) {
                        perKgRate = "0.00";
                    }
                    double ratePerKg = Double.valueOf(perKgRate);
//                    chargeableWeightcal = (int) (Double.parseDouble(usedVol[i]) * VolCal);
                    chargeableWeightcal = Math.round((Double.parseDouble(usedVol[i]) * VolCal));
                    freightChargesCal = (int) ((Double.parseDouble(usedVol[i]) * VolCal) * ratePerKg);
                    int chargeableWeightcalCheck = (int) (Double.parseDouble(usedVol[i]) * VolCal);
                    int usedCapacitycalCheck = (int) (Double.parseDouble(usedCapacity[i]));
                    System.out.println("chargeableWeightcalCheck " + chargeableWeightcalCheck + " usedCapacitycalCheck[i] " + usedCapacitycalCheck);
                    if (chargeableWeightcalCheck < usedCapacitycalCheck) {
                        chargeableWeightcal = Double.valueOf(usedCapacity[i]);
                        freightChargesCal = (int) (chargeableWeightcal) * ratePerKg;
                    }

                    // Check Booking Already Taken
                    String getAvailableWeightRoute = (String) session.queryForObject("operation.getAvailableWeightRoute", map);
                    if (getAvailableWeightRoute == null || getAvailableWeightRoute.equals("")) {
                        insertStatus = true;
                    } else {
                        if (getAvailableWeightRoute.equals(opTO.getRouteId())) {
                            String getAvailableWeight = (String) session.queryForObject("operation.getAvailableWeight", map);
                            if (getAvailableWeight != null) {
                                if (Double.parseDouble(getAvailableWeight) >= Double.parseDouble(usedCapacity[i])) {
                                    insertStatus = true;
                                } else {
                                    // 
                                    insertStatus = false;
                                }
                            }

                        } else {
                            insertStatus = false;
                        }
                    }

                    System.out.println(" Contract " + Contract + " rateMode " + rateMode + " perKgRate " + perKgRate + " chargeableWeightcal  " + chargeableWeightcal + " freightChargesCal " + freightChargesCal + " totalChargeableWeights " + totalChargeableWeights + " rateValue " + rateValue + " usedVol[i]) " + usedVol[i]);
                    if ("1".equals(Contract)) {
                        if ("1".equals(rateMode)) {
                            if (fleetTypeId.length > 1) {
                                map.put("chargeableWeight", chargeableWeightcal);
                                map.put("freightCharges", freightChargesCal);
                            } else {
                                map.put("chargeableWeight", totalChargeableWeights);
                                map.put("freightCharges", rateValue);
                            }
                        } else {
                            map.put("chargeableWeight", totalChargeableWeights);
                            map.put("freightCharges", rateValue);
                        }
                    } else if ("2".equals(Contract)) {
                        if ("2".equals(rateMode)) {
                            map.put("chargeableWeight", totalChargeableWeights);
                            map.put("freightCharges", rateValue);
                        } else {
                            if (fleetTypeId.length > 1) {
                                map.put("chargeableWeight", chargeableWeightcal);
                                map.put("freightCharges", freightChargesCal);
                            } else {
                                map.put("chargeableWeight", totalChargeableWeights);
                                map.put("freightCharges", rateValue);
                            }
                        }
                    }
                    System.out.println("map:" + map);
                    if (insertStatus) {
                        insertMultiplePoints = (Integer) session.update("operation.insertMultiplePoints", map);
                    } else {
                        truckInsertStatus = 2;
                    }

                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("insertConsignmentNote Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertConsignmentNote", sqlException);
        }

        return truckInsertStatus;
    }

    public int insertConsignmentDangerousGoods(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int index = 0;
        map.put("userId", userId);
        String hr = "";
        String min = "";
        int insertConsignmentDangerousGoods = 0;
        try {
            map.put("consignmentId", operationTO.getConsignmentOrderId());
            String[] dgHandlingCode = null;
            String[] unIdNo = null;
            String[] classCode = null;
            String[] pkgInstruction = null;
            String[] pkgGroup = null;
            String[] netQuantity = null;
            String[] netUnits = null;
//            productCodes = operationTO.getProductCodes();
//            productNames = operationTO.getProductNames();
//            packageNos = operationTO.getPackagesNos();
//            weights = operationTO.getWeights();
//            batch = operationTO.getBatchCode();
            dgHandlingCode = operationTO.getDgHandlingCode();
            unIdNo = operationTO.getUnIdNo();
            classCode = operationTO.getClassCode();
            pkgInstruction = operationTO.getPkgInstruction();
            pkgGroup = operationTO.getPkgGroup();
            netQuantity = operationTO.getNetQuantity();
            netUnits = operationTO.getNetUnits();
            for (int i = 0; i < dgHandlingCode.length; i++) {
//                map.put("productCode", productCodes[i]);
//                map.put("producName", productNames[i]);
//                map.put("packageNos", packageNos[i]);
//                map.put("weights", weights[i]);
//                map.put("batch", batch[i]);
                map.put("dgHandlingCode", dgHandlingCode[i]);
                map.put("unIdNo", unIdNo[i]);
                map.put("classCode", classCode[i]);
                map.put("pkgInstruction", pkgInstruction[i]);
                map.put("pkgGroup", pkgGroup[i]);
                map.put("netQuantity", netQuantity[i]);
                map.put("netUnits", netUnits[i]);
                insertConsignmentDangerousGoods = (Integer) getSqlMapClientTemplate().update("ops.insertConsignmentDangerousGoods", map);
            }

//            } catch (Exception ex) {
//                Writer writer = new StringWriter();
//                PrintWriter printWriter = new PrintWriter(writer);
//                ex.printStackTrace(printWriter);
//                String s = writer.toString();
//                System.out.println("s = " + s);
//            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertConsignmentDangerousGoods Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertConsignmentDangerousGoods", sqlException);
        }

        return insertConsignmentDangerousGoods;
    }
    
    public ArrayList getConsignmentOrderLists(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList getConsignmentOrderLists = new ArrayList();
        map.put("consignmentOrderId", operationTO.getConsignmentOrderId());
        try {
            getConsignmentOrderLists = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getConsignmentOrderLists", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentOrderLists Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,"getConsignmentOrderLists", sqlException);
        }
        return getConsignmentOrderLists;
    }
    
    public ArrayList getConsignmentArticleList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList getConsignmentArticleList = new ArrayList();
        map.put("consignmentOrderId", operationTO.getConsignmentOrderId());
        try {
            getConsignmentArticleList = (ArrayList) getSqlMapClientTemplate().queryForList("ops.getConsignmentArticleList", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentArticleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,"getConsignmentArticleList", sqlException);
        }
        return getConsignmentArticleList;
    }
    
    public int updateconsignmentList(OpsTO opsTO,OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int updateconsignmentList = 0;
            String[] contractId1 = null;
            String[] noOfPieces = null;
            String[] grossWeight = null;
            String[] chargeableWeight = null;
            String[] chargeableWeightId = null;
            String[] lengths = null;
            String[] widths = null;
            String[] heights = null;
            String[] batch = null;
            String[] uom = null;
            String[] volume = null;
            String[] consignmentArticleId = null;
            noOfPieces = opsTO.getNoOfPieces();
            uom = opsTO.getUoms();
            chargeableWeight = opsTO.getChargeableWeights();
            chargeableWeightId = opsTO.getChargeableWeightId();
            lengths = opsTO.getLengths();
            widths = opsTO.getWidths();
            heights = opsTO.getHeights();
            volume = opsTO.getVolumes();
            contractId1 = opsTO.getContractId1();
            System.out.println("contractId1=="+contractId1[0]);
            
        map.put("consignmentOrderId", opsTO.getConsignmentOrderId());
        map.put("shipmentType", opsTO.getShipmentType());
        map.put("awbMovementType", opsTO.getAwbMovementType());
        map.put("awbType", opsTO.getAwbType());
        map.put("airlineName", opsTO.getAirlineName());
        map.put("orderReferenceAwbNo", opsTO.getOrderReferenceAwb() + " " + opsTO.getOrderReferenceAwbNo() + " " + opsTO.getOrderReferenceEnd());
//        map.put("orderReferenceAwb", opsTO.getOrderReferenceAwb());
//        map.put("orderReferenceAwbNo", opsTO.getOrderReferenceAwbNo());
//        map.put("orderReferenceEnd", opsTO.getOrderReferenceEnd());
        map.put("awbOrderDeliveryDate", opsTO.getAwbOrderDeliveryDate());
        map.put("orderReferenceNo", opsTO.getOrderReferenceNo());
        map.put("commodity", opsTO.getCommodity());
        map.put("totalWeightage", opsTO.getTotalWeightage());
        map.put("totalChargeableWeights", opsTO.getTotalChargeableWeights());
        map.put("totalVolume", opsTO.getTotalVolume());
        map.put("consignmentDate", opsTO.getConsignmentDate());
         if ("1".equals(contractId1[0])) {
             map.put("freightCharges", operationTO.getRateValue());
         }else{
             map.put("freightCharges", operationTO.getRateValue1());
         }
        System.out.println("map===="+map);
        try {
            for (int i = 0; i < noOfPieces.length; i++) {
                map.put("noOfPieces", noOfPieces[i]);
                map.put("uom", uom[i]);
                map.put("chargeableWeight", chargeableWeight[i]);
                map.put("chargeableWeightId", chargeableWeightId[i]);
                map.put("lengths", lengths[i]);
                map.put("widths", widths[i]);
                map.put("heights", heights[i]);
                map.put("volume", volume[i]);
            updateconsignmentList = (Integer) getSqlMapClientTemplate().update("ops.updateconsignmentArticle", map);
            }
            updateconsignmentList = (Integer) getSqlMapClientTemplate().update("ops.updateconsignmentList", map);
            System.out.println("map11==="+map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateconsignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,"updateconsignmentList", sqlException);
        }
        return updateconsignmentList;
    }

    
    
}
