/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.ops.web;

//import java.nio.file.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import jxl.Workbook;
import jxl.*;
import org.apache.poi.hssf.usermodel.HSSFCell;
import jxl.WorkbookSettings;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;
import com.oreilly.servlet.multipart.Part;
//import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import ets.arch.util.SendMail;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import ets.arch.business.PaginationHelper;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.web.BaseController;
import ets.domain.mrs.business.MrsBP;
import ets.domain.mrs.web.MrsCommand;
import ets.domain.operation.business.OperationBP;
import ets.domain.customer.business.CustomerBP;
import ets.domain.vehicle.business.VehicleBP;
import ets.domain.operation.business.OperationTO;
import ets.domain.operation.business.cnoteTO;
import ets.domain.security.business.SecurityBP;
import ets.domain.service.business.ServiceBP;
import ets.domain.users.business.LoginBP;
import ets.domain.util.FPLogUtils;
import ets.domain.util.ParveenErrorConstants; 
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import java.util.List;
import java.util.List;
import java.net.*;
import java.io.*;
import java.util.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONArray;
import org.json.JSONObject;
import ets.domain.trip.business.TripBP;
import ets.domain.trip.business.TripTO;
import ets.domain.company.business.CompanyBP;
import ets.domain.customer.business.CustomerTO;
import ets.domain.ops.business.OpsBP;
import ets.domain.ops.business.OpsTO;
import ets.domain.util.ThrottleConstants;

/**
 *
 * @author vijay
 */
public class OpsController extends BaseController {

    OperationBP operationBP;
    LoginBP loginBP;
    SecurityBP securityBP;
    ServiceBP serviceBP;
    MrsBP mrsBP;
    OpsCommand opsCommand;
    VehicleBP vehicleBP;
    CustomerBP customerBP;
    TripBP tripBP;
    CompanyBP companyBP;
    OpsBP opsBP;

    public CompanyBP getCompanyBP() {
        return companyBP;
    }

    public void setCompanyBP(CompanyBP companyBP) {
        this.companyBP = companyBP;
    }

    public TripBP getTripBP() {
        return tripBP;
    }

    public void setTripBP(TripBP tripBP) {
        this.tripBP = tripBP;
    }

    public CustomerBP getCustomerBP() {
        return customerBP;
    }

    public void setCustomerBP(CustomerBP customerBP) {
        this.customerBP = customerBP;
    }

    public MrsBP getMrsBP() {
        return mrsBP;
    }

    public void setMrsBP(MrsBP mrsBP) {
        this.mrsBP = mrsBP;
    }

    public VehicleBP getVehicleBP() {
        return vehicleBP;
    }

    public void setVehicleBP(VehicleBP vehicleBP) {
        this.vehicleBP = vehicleBP;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public ServiceBP getServiceBP() {
        return serviceBP;
    }

    public void setServiceBP(ServiceBP serviceBP) {
        this.serviceBP = serviceBP;

    }

    public SecurityBP getSecurityBP() {
        return securityBP;
    }

    public void setSecurityBP(SecurityBP securityBP) {
        this.securityBP = securityBP;
    }

    public OpsCommand getOpsCommand() {
        return opsCommand;
    }

    public void setOpsCommand(OpsCommand opsCommand) {
        this.opsCommand = opsCommand;
    }

    public OperationBP getOperationBP() {
        return operationBP;
    }

    public void setOperationBP(OperationBP operationBP) {
        this.operationBP = operationBP;
    }

    public OpsBP getOpsBP() {
        return opsBP;
    }

    public void setOpsBP(OpsBP opsBP) {
        this.opsBP = opsBP;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        System.out.println("request.getRequestURI() = " + request.getRequestURI());
        binder.closeNoCatch();

    }

    public ModelAndView consignmentNoteBonded(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OperationTO operationTO = new OperationTO();
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  Route Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList routeList = new ArrayList();
        String branchId = (String) session.getAttribute("BranchId");
        try {

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            System.out.println(dateFormat.format(date));
            request.setAttribute("curDate", dateFormat.format(date));
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/BrattleFoods/consignmentNoteBonded.jsp";
            /*
             String consignmentOrderNo = operationBP.getConsignmentOrderNo(operationTO);
             String temp[] = null;
             int cnote = 0;
             if (consignmentOrderNo != null) {
             temp = consignmentOrderNo.split("/");
             cnote = Integer.parseInt(temp[2]);
             cnote++;
             request.setAttribute("consignmentOrderNo", temp[0] + "/" + temp[1] + "/" + cnote);
             } else {
             request.setAttribute("consignmentOrderNo", "CO/13-14/200301");
             }
             */
            operationTO.setBranchId(branchId);
            ArrayList todayBooking = new ArrayList();
            todayBooking = operationBP.getTodayBookingDetails(operationTO);
            Iterator itr = todayBooking.iterator();
            OperationTO opsTO = new OperationTO();
            while (itr.hasNext()) {
                opsTO = (OperationTO) itr.next();
                request.setAttribute("totalBooking", opsTO.getTotalBooking());
                request.setAttribute("totalGrossWeight", opsTO.getTotalGrossWeight());

            }

            ArrayList customerTypeList = new ArrayList();
            customerTypeList = operationBP.getCustomerTypeList(operationTO);
            request.setAttribute("customerTypeList", customerTypeList);

            ArrayList vehicleTypeList = new ArrayList();
            vehicleTypeList = operationBP.getVehicleTypeList();
            request.setAttribute("vehicleTypeList", vehicleTypeList);

            ArrayList billingTypeList = new ArrayList();
            billingTypeList = operationBP.getBillingTypeList();
            request.setAttribute("billingTypeList", billingTypeList);
            ArrayList productCategoryList = new ArrayList();
            productCategoryList = operationBP.getProductCategoryList();
            request.setAttribute("productCategoryList", productCategoryList);
            ArrayList getPointList = new ArrayList();
            getPointList = opsBP.getCityCodeList(new OpsTO());
            request.setAttribute("getPointList", getPointList);

            ArrayList getPointList1 = new ArrayList();
            getPointList1 = opsBP.getCityCodeList1(new OpsTO());
            request.setAttribute("getPointList1", getPointList1);

            ArrayList customerList = new ArrayList();
            customerList = opsBP.getCustomerList();
            request.setAttribute("getCustomerList", customerList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    /**
     * This method is used to Get City Ajax.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public void getTruckCityList(HttpServletRequest request, HttpServletResponse response, OpsCommand command) throws IOException {
        HttpSession session = request.getSession();
        opsCommand = command;
        OpsTO opsTO = new OpsTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String cityName = "";
            String originCityId = "";
            response.setContentType("text/html");
            cityName = request.getParameter("cityName");
            if (request.getParameter("cityName") != null && !"".equals(request.getParameter("cityName"))) {
                originCityId = request.getParameter("originCityId");
                opsTO.setPreviousCityId(originCityId);
            }
            opsTO.setCityName(cityName);
            userDetails = opsBP.getCityList(opsTO);

            System.out.println("userDetails.size() = " + userDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = userDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                opsTO = (OpsTO) itr.next();
                jsonObject.put("Id", opsTO.getCityId());
                jsonObject.put("Name", opsTO.getCityName());
                jsonObject.put("cityCode", opsTO.getCityCode());
                jsonObject.put("TravelKm", opsTO.getTravelKm());
                jsonObject.put("TravelHour", opsTO.getTravelHours());
                jsonObject.put("TravelMinute", opsTO.getTravelMinutes());
                jsonObject.put("RouteId", opsTO.getRouteId());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getDestinationCityList(HttpServletRequest request, HttpServletResponse response, OpsCommand command) throws IOException {
        HttpSession session = request.getSession();
        opsCommand = command;
        OpsTO opsTO = new OpsTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String cityName = "";
            String originCityId = "";
            response.setContentType("text/html");
            originCityId = request.getParameter("originCityId");
            opsTO.setPreviousCityId(originCityId);
            userDetails = opsBP.getDestinationCityList(opsTO);
            System.out.println("userDetails.size() = " + userDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = userDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                opsTO = (OpsTO) itr.next();
                jsonObject.put("Id", opsTO.getCityId());
                jsonObject.put("Name", opsTO.getCityName());
                jsonObject.put("cityCode", opsTO.getCityCode());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getTruckCityCodeList(HttpServletRequest request, HttpServletResponse response, OpsCommand command) throws IOException {
        HttpSession session = request.getSession();
        opsCommand = command;
        OpsTO opsTO = new OpsTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String cityCode = "";
            response.setContentType("text/html");
            cityCode = request.getParameter("cityCode");

            opsTO.setCityCode(cityCode);
            userDetails = opsBP.getCityCodeList(opsTO);

            System.out.println("userDetails.size() = " + userDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = userDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                opsTO = (OpsTO) itr.next();
                jsonObject.put("Id", opsTO.getCityId());
                jsonObject.put("Name", opsTO.getCityName());
                jsonObject.put("cityCode", opsTO.getCityCode());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getChargeableRateValue(HttpServletRequest request, HttpServletResponse response, OpsCommand command) throws IOException {
        HttpSession session = request.getSession();
        opsCommand = command;
        OpsTO opsTO = new OpsTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String customerId = "";
            String chargeableWeight = "";
            response.setContentType("text/html");
            customerId = request.getParameter("customerId");
            String productRate = request.getParameter("productRate");
            String awbOriginId = request.getParameter("awbOriginId");
            String awbDestinationId = request.getParameter("awbDestinationId");
            if (request.getParameter("customerId") != null && !"".equals(request.getParameter("customerId"))) {
                chargeableWeight = request.getParameter("chargeableWeight");
                opsTO.setChargableWeight(chargeableWeight);
                opsTO.setAwbOriginId(awbOriginId);
                opsTO.setAwbDestinationId(awbDestinationId);
            }
            opsTO.setCustomerId(customerId);
            userDetails = opsBP.getChargeableRateValue(opsTO);

            System.out.println("userDetails.size() = " + userDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = userDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                opsTO = (OpsTO) itr.next();
                if ("2".equals(productRate)) {
                    jsonObject.put("Name", Float.parseFloat(opsTO.getRateWithoutReefer()) * Float.parseFloat(chargeableWeight));
                    jsonObject.put("perKgAutoRate", Float.parseFloat(opsTO.getRateWithoutReefer()));
                    System.out.println("perKgAutoRate  1  " + Float.parseFloat(opsTO.getRateWithoutReefer()));
                } else if ("1".equals(productRate)) {
                    jsonObject.put("Name", Float.parseFloat(opsTO.getRateWithReefer()) * Float.parseFloat(chargeableWeight));
                    jsonObject.put("perKgAutoRate", Float.parseFloat(opsTO.getRateWithReefer()));
                    System.out.println("perKgAutoRate  2  " + Float.parseFloat(opsTO.getRateWithoutReefer()));
                } else {
                    jsonObject.put("Name", "0");
                    jsonObject.put("perKgAutoRate", "0.00");
                }
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getFullTruckRateValue(HttpServletRequest request, HttpServletResponse response, OpsCommand command) throws IOException {
        HttpSession session = request.getSession();
        opsCommand = command;
        OpsTO opsTO = new OpsTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String customerId = "";
            String chargeableWeight = "";
            response.setContentType("text/html");
            customerId = request.getParameter("customerId");
            String productRate = request.getParameter("productRate");
            String awbOriginId = request.getParameter("awbOriginId");
            String awbDestinationId = request.getParameter("awbDestinationId");
            String vehicleTypeId = request.getParameter("fleetTypeId");
            if (request.getParameter("customerId") != null && !"".equals(request.getParameter("customerId"))) {
                chargeableWeight = request.getParameter("chargeableWeight");
                opsTO.setChargableWeight(chargeableWeight);
                opsTO.setAwbOriginId(awbOriginId);
                opsTO.setAwbDestinationId(awbDestinationId);
                opsTO.setVehicleTypeId(vehicleTypeId);
                opsTO.setProductRate(productRate);// Setted by naved 11:00pm
            }
            opsTO.setCustomerId(customerId);
            userDetails = opsBP.getFullTruckRateValue(opsTO);

            System.out.println("userDetails.size() = " + userDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = userDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                opsTO = (OpsTO) itr.next();
                if ("2".equals(productRate)) {

                    jsonObject.put("Name", Float.parseFloat(opsTO.getRateWithoutReefer()));
                } else if ("1".equals(productRate)) {
                    jsonObject.put("Name", Float.parseFloat(opsTO.getRateWithReefer()));
                } else {
                    jsonObject.put("Name", "0");
                }
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getAvailableWeightAndVolume(HttpServletRequest request, HttpServletResponse response, OpsCommand command) throws IOException {
        HttpSession session = request.getSession();
        opsCommand = command;
        OpsTO opsTO = new OpsTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList volumeDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String vehicleId = "";
            String truckDepDate = "";
            String truckCapacity = "";
            String truckCapacityTemp = "";
            String truckVol = "";
            String fleetTypeId = "";
            response.setContentType("text/html");
            vehicleId = request.getParameter("vehicleId");
            truckDepDate = request.getParameter("truckDepDate");
            truckCapacity = request.getParameter("truckCapacity");
            truckCapacityTemp = request.getParameter("usdCapTemp");
            truckVol = request.getParameter("truckVol");
            fleetTypeId = request.getParameter("fleetTypeId");
            if (request.getParameter("vehicleId") != null && !"".equals(request.getParameter("vehicleId"))) {
                opsTO.setVehicleId(vehicleId);
            }
            if (truckDepDate != null && !"".equals(truckDepDate)) {
                opsTO.setTruckDepDate(truckDepDate);
            }
            if (truckCapacity != null && !"".equals(truckCapacity)) {
                opsTO.setTruckCapacity(truckCapacity);
            }
            if (truckCapacityTemp != null && !"".equals(truckCapacityTemp)) {
                opsTO.setTruckCapacityTemp(truckCapacityTemp);
            }
            if (fleetTypeId != null && !"".equals(fleetTypeId)) {
                opsTO.setFleetTypeId(fleetTypeId);
            }
            if (truckVol != null && !"".equals(truckVol)) {
                opsTO.setTruckVol(truckVol);
            }
            volumeDetails = opsBP.getAvailableWeightAndVolume(opsTO);

            System.out.println("userDetails.size() = " + volumeDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = volumeDetails.iterator();
            if (volumeDetails.size() > 0) {
                for (int i = 0; i < 1; i++) {
                    JSONObject jsonObject = new JSONObject();
                    opsTO = (OpsTO) volumeDetails.get(i);
                    jsonObject.put("Id", opsTO.getTotalVolume());
                    jsonObject.put("Name", opsTO.getTotalWeight());
                    jsonObject.put("usedTruckCapcity", opsTO.getUsedTruckCapcity());
                    jsonObject.put("usedTruckVol", opsTO.getUsedTruckVol());

                    System.out.println("jsonObject = " + jsonObject);
                    jsonArray.put(jsonObject);
                    break;
                }
            }

            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getAvailableWeightAndVolumeEdit(HttpServletRequest request, HttpServletResponse response, OpsCommand command) throws IOException {
        HttpSession session = request.getSession();
        opsCommand = command;
        OpsTO opsTO = new OpsTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList volumeDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String vehicleId = "";
            String truckDepDate = "";
            String truckCapacity = "";
            String truckVol = "";
            String fleetTypeId = "";
            String routeusedVolume = "", routeusedWeight = "";
            response.setContentType("text/html");
            vehicleId = request.getParameter("vehicleId");
            truckDepDate = request.getParameter("truckDepDate");
            truckCapacity = request.getParameter("truckCapacity");
            truckVol = request.getParameter("truckVol");
            fleetTypeId = request.getParameter("fleetTypeId");
            routeusedVolume = request.getParameter("routeusedVolume");
            routeusedWeight = request.getParameter("routeusedWeight");
            if (request.getParameter("vehicleId") != null && !"".equals(request.getParameter("vehicleId"))) {
                opsTO.setVehicleId(vehicleId);
            }
            if (request.getParameter("routeusedWeight") != null && !"".equals(request.getParameter("routeusedWeight"))) {
                opsTO.setRouteId(routeusedWeight); //for route used weight
            }
            if (request.getParameter("routeusedVolume") != null && !"".equals(request.getParameter("routeusedVolume"))) {
                opsTO.setFleetVolume(routeusedVolume); //for route used weight
            }
            System.out.println("use wet" + opsTO.getRouteId() + "used vol" + opsTO.getFleetVolume());
            if (truckDepDate != null && !"".equals(truckDepDate)) {
                opsTO.setTruckDepDate(truckDepDate);
            }
            if (truckCapacity != null && !"".equals(truckCapacity)) {
                opsTO.setTruckCapacity(truckCapacity);
            }
            if (fleetTypeId != null && !"".equals(fleetTypeId)) {
                opsTO.setFleetTypeId(fleetTypeId);
            }
            if (truckVol != null && !"".equals(truckVol)) {
                opsTO.setTruckVol(truckVol);
            }

            volumeDetails = opsBP.getAvailableWeightAndVolumeEdit(opsTO);

            System.out.println("userDetails.size() = " + volumeDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = volumeDetails.iterator();
            if (volumeDetails.size() > 0) {
                for (int i = 0; i < 1; i++) {
                    JSONObject jsonObject = new JSONObject();
                    opsTO = (OpsTO) volumeDetails.get(i);
                    jsonObject.put("Id", opsTO.getTotalVolume());
                    jsonObject.put("Name", opsTO.getTotalWeight());
                    jsonObject.put("usedTruckCapcity", opsTO.getUsedTruckCapcity());
                    jsonObject.put("usedTruckVol", opsTO.getUsedTruckVol());

                    System.out.println("jsonObject = " + jsonObject);
                    jsonArray.put(jsonObject);
                    break;
                }
            }

            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    /**
     * This method is used to Get Chargeable Weight.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public void getChargeableWeight(HttpServletRequest request, HttpServletResponse response, OpsCommand command) throws IOException {
        HttpSession session = request.getSession();
        opsCommand = command;
        OpsTO opsTO = new OpsTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String length = "";
            String width = "";
            String height = "";
            String uom = "";
            String originCityId = "";
            response.setContentType("text/html");
            length = request.getParameter("length");
            width = request.getParameter("width");
            height = request.getParameter("height");
            uom = request.getParameter("uom");
            opsTO.setLengthCm(length);
            opsTO.setWidthCm(width);
            opsTO.setHeightCm(height);
            opsTO.setUom(uom);
            userDetails = opsBP.getChargeableWeight(opsTO);

            System.out.println("userDetails.size() = " + userDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = userDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                opsTO = (OpsTO) itr.next();
                jsonObject.put("Id", opsTO.getCubicWeightId());
                jsonObject.put("length", opsTO.getLengthCm());
                jsonObject.put("width", opsTO.getWidthCm());
                jsonObject.put("height", opsTO.getHeightCm());
                jsonObject.put("totalVolume", opsTO.getTotalVolume());
                jsonObject.put("weightPerVolume", opsTO.getWeightPerVolume());
                jsonObject.put("totalWeight", opsTO.getTotalWeight());
                jsonObject.put("activeInd", opsTO.getActiveInd());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    /**
     * This method is used to Save Consignment Note.
     *
     * @param request - Http request object
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView saveBookingOrder(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {
        System.out.println("entered saveBookingOrder");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        ModelAndView mv = null;
        String path = "";
        HttpSession session = request.getSession();
        System.out.println("userId = " + 0);
        int userId = (Integer) session.getAttribute("userId");

        System.out.println("userId = " + userId);
        opsCommand = command;
        String menuPath = "";
        OperationTO operationTO = new OperationTO();
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  Route Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList routeList = new ArrayList();
        String branchId = "";
        String bookingRemarks = "";
        try {

            String[] availableWeight = request.getParameterValues("availableCap");
            String[] availableVolume = request.getParameterValues("assignedCap");
            String[] remarks = request.getParameterValues("bookingReferenceRemarks");
            if (remarks != null && !"".equals(remarks)) {
                for (int i = 0; i < remarks.length; i++) {
                    if (i == 0) {
                        bookingRemarks = remarks[i];
                    } else {
                        bookingRemarks = bookingRemarks + "~" + remarks[i];
                    }

                }
            }
            operationTO.setBookingRemarks(bookingRemarks);
            operationTO.setAirlineName(request.getParameter("airlineName"));

            branchId = (String) session.getAttribute("BranchId");
            System.out.println("branchId::::" + branchId);
            operationTO.setBranchId(branchId);
            operationTO.setAvailableVolume(availableVolume);
            operationTO.setAvailableWeight(availableWeight);

            operationTO.setOriginId(request.getParameter("truckOriginId"));
            operationTO.setOriginName(request.getParameter("truckOriginName"));
            operationTO.setDestinationId(request.getParameter("truckDestinationId"));
            operationTO.setDestinationName(request.getParameter("truckDestinationName"));

            int insertStatus = 0;
            operationTO.setAwbNo(request.getParameter("awbno"));
            operationTO.setShipmentType(request.getParameter("shipmentType"));
            if (opsCommand.getCustomerTypeId() != null && !"".equals(opsCommand.getCustomerTypeId())) {
                operationTO.setCustomerTypeId(opsCommand.getCustomerTypeId());
            }

            if (opsCommand.getPaymentType() != null && !"".equals(opsCommand.getPaymentType())) {
                operationTO.setPaymentType(opsCommand.getPaymentType());
            }

            if (opsCommand.getEntryType() != null && !"".equals(opsCommand.getEntryType())) {
                operationTO.setEntryType(opsCommand.getEntryType());
            }
            if (opsCommand.getAwbType() != null && !"".equals(opsCommand.getAwbType())) {
                operationTO.setAwbType(opsCommand.getAwbType());
            }
//            if (opsCommand.getConsignmentNoteNo() != null && !"".equals(opsCommand.getConsignmentNoteNo())) {
//                operationTO.setConsignmentNoteNo(opsCommand.getConsignmentNoteNo());
//            }
            if (opsCommand.getConsignmentDate() != null && !"".equals(opsCommand.getConsignmentDate())) {
                operationTO.setConsignmentDate(opsCommand.getConsignmentDate());
            }
            if (opsCommand.getTotalChargeableWeights() != null && !"".equals(opsCommand.getTotalChargeableWeights())) {
                operationTO.setTotalChargeableWeights(opsCommand.getTotalChargeableWeights());
            }
            if (opsCommand.getOrderReferenceNo() != null && !"".equals(opsCommand.getOrderReferenceNo())) {
                operationTO.setOrderReferenceNo(opsCommand.getOrderReferenceNo());
            }
            if (opsCommand.getOrderReferenceEnd() != null && !"".equals(opsCommand.getOrderReferenceEnd())) {
                operationTO.setOrderReferenceEnd(opsCommand.getOrderReferenceEnd());
            }
//            if (opsCommand.getOrderReferenceRemarks() != null && !"".equals(opsCommand.getOrderReferenceRemarks())) {
//                operationTO.setOrderReferenceRemarks(opsCommand.getOrderReferenceRemarks());
//            }
            if (opsCommand.getProductCategoryId() != null && !"".equals(opsCommand.getProductCategoryId())) {
                operationTO.setProductCategoryId(opsCommand.getProductCategoryId());
            }
            if (opsCommand.getAwbOriginRegion() != null && !"".equals(opsCommand.getAwbOriginRegion())) {
                operationTO.setAwbOriginRegion(opsCommand.getAwbOriginRegion());
            }
            //generate tripcode
            String cNoteCode = "CO/" + ThrottleConstants.accountYear + "/";
            String cNoteCodeSequence = operationBP.getCnoteCodeSequence();
            cNoteCode = cNoteCode + cNoteCodeSequence;
            operationTO.setConsignmentNoteNo(cNoteCode);

            if (operationTO.getCustomerTypeId().equals("1")) {
                if (opsCommand.getCustomerId() != null && !"".equals(opsCommand.getCustomerId())) {
                    operationTO.setCustomerId(opsCommand.getCustomerId());
                }
                if (opsCommand.getCustomerName() != null && !"".equals(opsCommand.getCustomerName())) {
                    operationTO.setCustomerName(opsCommand.getCustomerName());
                }
                if (opsCommand.getCustomerCode() != null && !"".equals(opsCommand.getCustomerCode())) {
                    operationTO.setCustomerCode(opsCommand.getCustomerCode());
                }
                if (opsCommand.getCustomerAddress() != null && !"".equals(opsCommand.getCustomerAddress())) {
                    operationTO.setCustomerAddress(opsCommand.getCustomerAddress());
                }
                if (opsCommand.getPincode() != null && !"".equals(opsCommand.getPincode())) {
                    operationTO.setPincode(opsCommand.getPincode());
                }
                if (opsCommand.getCustomerMobileNo() != null && !"".equals(opsCommand.getCustomerMobileNo())) {
                    operationTO.setCustomerMobileNo(opsCommand.getCustomerMobileNo());
                }
                if (opsCommand.getMailId() != null && !"".equals(opsCommand.getMailId())) {
                    operationTO.setMailId(opsCommand.getMailId());
                }
                if (opsCommand.getCustomerPhoneNo() != null && !"".equals(opsCommand.getCustomerPhoneNo())) {
                    operationTO.setCustomerPhoneNo(opsCommand.getCustomerPhoneNo());
                }
                if (opsCommand.getBillingTypeId() != null && !"".equals(opsCommand.getBillingTypeId())) {
                    operationTO.setBillingTypeId(opsCommand.getBillingTypeId());
                }
                if (opsCommand.getContractId() != null && !"".equals(opsCommand.getContractId())) {
                    operationTO.setContractId(Integer.parseInt(opsCommand.getContractId()));
                }
                if (opsCommand.getDestination() != null && !"".equals(opsCommand.getDestination())) {
                    operationTO.setDestination(opsCommand.getDestination());
                }
            }
            if (opsCommand.getOrigin() != null && !"".equals(opsCommand.getOrigin())) {
                operationTO.setOrigin(opsCommand.getOrigin());
            }
            if (opsCommand.getBusinessType() != null && !"".equals(opsCommand.getBusinessType())) {
                operationTO.setBusinessType(opsCommand.getBusinessType());
            }
            if (opsCommand.getMultiPickup() != null && !"".equals(opsCommand.getMultiPickup())) {
                operationTO.setMultiPickup(opsCommand.getMultiPickup());
            }
            if (opsCommand.getMultiDelivery() != null && !"".equals(opsCommand.getMultiDelivery())) {
                operationTO.setMultiDelivery(opsCommand.getMultiDelivery());
            }
            if (opsCommand.getConsignmentOrderInstruction() != null && !"".equals(opsCommand.getConsignmentOrderInstruction())) {
                operationTO.setConsignmentOrderInstruction(opsCommand.getConsignmentOrderInstruction());
            }
            if (opsCommand.getProductCodes() != null && !"".equals(opsCommand.getProductCodes())) {
                operationTO.setProductCodes(opsCommand.getProductCodes());
            }
            if (opsCommand.getBatchCode() != null && !"".equals(opsCommand.getBatchCode())) {
                operationTO.setBatchCode(opsCommand.getBatchCode());
            }
            if (opsCommand.getUom() != null && !"".equals(opsCommand.getUom())) {
                operationTO.setUom(opsCommand.getUom());
            }
            if (opsCommand.getProductNames() != null && !"".equals(opsCommand.getProductNames())) {
                operationTO.setProductNames(opsCommand.getProductNames());
            }
            if (opsCommand.getPackagesNos() != null && !"".equals(opsCommand.getPackagesNos())) {
                operationTO.setPackagesNos(opsCommand.getPackagesNos());
            }
            if (opsCommand.getWeights() != null && !"".equals(opsCommand.getWeights())) {
                operationTO.setWeights(opsCommand.getWeights());
            }
            if (opsCommand.getTotalPackage() != null && !"".equals(opsCommand.getTotalPackage())) {
                operationTO.setTotalPackage(opsCommand.getTotalPackage());
            }
            if (opsCommand.getTotalWeightage() != null && !"".equals(opsCommand.getTotalWeightage())) {
                operationTO.setTotalWeightage(opsCommand.getTotalWeightage());
            }
            if (opsCommand.getTotalChargeableWeights() != null && !"".equals(opsCommand.getTotalChargeableWeights())) {
                operationTO.setTotalChargeableWeights(opsCommand.getTotalChargeableWeights());
            }
            if (opsCommand.getTotalVolume() != null && !"".equals(opsCommand.getTotalVolume())) {
                operationTO.setTotalVolume(opsCommand.getTotalVolume());
            }
            if (opsCommand.getTotalVolumeActual() != null && !"".equals(opsCommand.getTotalVolumeActual())) {
                operationTO.setTotalVolumeActual(opsCommand.getTotalVolumeActual());
            }
            if (opsCommand.getServiceType() != null && !"".equals(opsCommand.getServiceType())) {
                operationTO.setServiceType(opsCommand.getServiceType());
            }
            if (request.getParameter("vehicleTypeId") != null && !"".equals(request.getParameter("vehicleTypeId"))) {
                operationTO.setVehicleTypeId(request.getParameter("vehicleTypeId"));
            }
            if (opsCommand.getReeferRequired() != null && !"".equals(opsCommand.getReeferRequired())) {
                operationTO.setReeferRequired(opsCommand.getReeferRequired());
            }
            if (opsCommand.getRouteContractId() != null && !"".equals(opsCommand.getRouteContractId())) {
                operationTO.setRouteContractId(Integer.parseInt(opsCommand.getRouteContractId()));
            }
            if (opsCommand.getRouteId() != null && !"".equals(opsCommand.getRouteId())) {
                operationTO.setRouteId(opsCommand.getRouteId());
            }
            if (opsCommand.getContractRateId() != null && !"".equals(opsCommand.getContractRateId())) {
                operationTO.setContractRateId(opsCommand.getContractRateId());
            }
            if (opsCommand.getTotalKm() != null && !"".equals(opsCommand.getTotalKm())) {
                operationTO.setTotalKm(opsCommand.getTotalKm());
            }
            if (opsCommand.getTotalHours() != null && !"".equals(opsCommand.getTotalHours())) {
                operationTO.setTotalHours(opsCommand.getTotalHours());
            }
            if (opsCommand.getTotalMinutes() != null && !"".equals(opsCommand.getTotalMinutes())) {
                operationTO.setTotalMinutes(opsCommand.getTotalMinutes());
            }
            if (opsCommand.getRateWithReefer() != null && !"".equals(opsCommand.getRateWithReefer())) {
                operationTO.setRateWithReefer(opsCommand.getRateWithReefer());
            }
            if (opsCommand.getRateWithoutReefer() != null && !"".equals(opsCommand.getRateWithoutReefer())) {
                operationTO.setRateWithoutReefer(opsCommand.getRateWithoutReefer());
            }
            if (opsCommand.getVehicleRequiredDate() != null && !"".equals(opsCommand.getVehicleRequiredDate())) {
                operationTO.setVehicleRequiredDate(opsCommand.getVehicleRequiredDate());
            }
            if (opsCommand.getVehicleRequiredHour() != null && !"".equals(opsCommand.getVehicleRequiredHour())) {
                operationTO.setVehicleRequiredHour(opsCommand.getVehicleRequiredHour());
            }
            if (opsCommand.getVehicleRequiredMinute() != null && !"".equals(opsCommand.getVehicleRequiredMinute())) {
                operationTO.setVehicleRequiredMinute(opsCommand.getVehicleRequiredMinute());
            }
            if (opsCommand.getVehicleInstruction() != null && !"".equals(opsCommand.getVehicleInstruction())) {
                operationTO.setVehicleInstruction(opsCommand.getVehicleInstruction());
            }
            if (opsCommand.getConsignorName() != null && !"".equals(opsCommand.getConsignorName())) {
                operationTO.setConsignorName(opsCommand.getConsignorName());
            }
            if (opsCommand.getConsignorPhoneNo() != null && !"".equals(opsCommand.getConsignorPhoneNo())) {
                operationTO.setConsignorPhoneNo(opsCommand.getConsignorPhoneNo());
            }
            if (opsCommand.getConsignorAddress() != null && !"".equals(opsCommand.getConsignorAddress())) {
                operationTO.setConsignorAddress(opsCommand.getConsignorAddress());
            }
            if (opsCommand.getConsigneeName() != null && !"".equals(opsCommand.getConsigneeName())) {
                operationTO.setConsigneeName(opsCommand.getConsigneeName());
            }
            if (opsCommand.getConsigneePhoneNo() != null && !"".equals(opsCommand.getConsigneePhoneNo())) {
                operationTO.setConsigneePhoneNo(opsCommand.getConsigneePhoneNo());
            }
            if (opsCommand.getConsigneeAddress() != null && !"".equals(opsCommand.getConsigneeAddress())) {
                operationTO.setConsigneeAddress(opsCommand.getConsigneeAddress());
            }
            if (opsCommand.getTotFreightAmount() != null && !"".equals(opsCommand.getTotFreightAmount())) {
                operationTO.setTotFreightAmount(opsCommand.getTotFreightAmount());
            }
            if (opsCommand.getDocCharges() != null && !"".equals(opsCommand.getDocCharges())) {
                operationTO.setDocCharges(opsCommand.getDocCharges());
            }
            if (opsCommand.getOdaCharges() != null && !"".equals(opsCommand.getOdaCharges())) {
                operationTO.setOdaCharges(opsCommand.getOdaCharges());
            }
            if (opsCommand.getMultiPickupCharge() != null && !"".equals(opsCommand.getMultiPickupCharge())) {
                operationTO.setMultiPickupCharge(opsCommand.getMultiPickupCharge());
            }
            if (opsCommand.getMultiDeliveryCharge() != null && !"".equals(opsCommand.getMultiDeliveryCharge())) {
                operationTO.setMultiDeliveryCharge(opsCommand.getMultiDeliveryCharge());
            }
            if (opsCommand.getHandleCharges() != null && !"".equals(opsCommand.getHandleCharges())) {
                operationTO.setHandleCharges(opsCommand.getHandleCharges());
            }
            if (opsCommand.getOtherCharges() != null && !"".equals(opsCommand.getOtherCharges())) {
                operationTO.setOtherCharges(opsCommand.getOtherCharges());
            }
            if (opsCommand.getUnloadingCharges() != null && !"".equals(opsCommand.getUnloadingCharges())) {
                operationTO.setUnloadingCharges(opsCommand.getUnloadingCharges());
            }
            if (opsCommand.getLoadingCharges() != null && !"".equals(opsCommand.getLoadingCharges())) {
                operationTO.setLoadingCharges(opsCommand.getLoadingCharges());
            }
            if (opsCommand.getSubTotal() != null && !"".equals(opsCommand.getSubTotal())) {
                operationTO.setSubTotal(opsCommand.getSubTotal());
            }
            if (opsCommand.getTotalCharges() != null && !"".equals(opsCommand.getTotalCharges())) {
                operationTO.setTotalCharges(opsCommand.getTotalCharges());
            }
            //Walkin Customer
            if (operationTO.getCustomerTypeId().equals("2")) {
                if (opsCommand.getWalkinCustomerName() != null && !"".equals(opsCommand.getWalkinCustomerName())) {
                    operationTO.setWalkinCustomerName(opsCommand.getWalkinCustomerName());
                }
                if (opsCommand.getWalkinCustomerCode() != null && !"".equals(opsCommand.getWalkinCustomerCode())) {
                    operationTO.setWalkinCustomerCode(opsCommand.getWalkinCustomerCode());
                }
                if (opsCommand.getWalkinCustomerAddress() != null && !"".equals(opsCommand.getWalkinCustomerAddress())) {
                    operationTO.setWalkinCustomerAddress(opsCommand.getWalkinCustomerAddress());
                }
                if (opsCommand.getWalkinPincode() != null && !"".equals(opsCommand.getWalkinPincode())) {
                    operationTO.setWalkinPincode(opsCommand.getWalkinPincode());
                }
                if (opsCommand.getWalkinCustomerMobileNo() != null && !"".equals(opsCommand.getWalkinCustomerMobileNo())) {
                    operationTO.setWalkinCustomerMobileNo(opsCommand.getWalkinCustomerMobileNo());
                }
                if (opsCommand.getWalkinMailId() != null && !"".equals(opsCommand.getWalkinMailId())) {
                    operationTO.setWalkinMailId(opsCommand.getWalkinMailId());
                }
                if (opsCommand.getWalkinCustomerPhoneNo() != null && !"".equals(opsCommand.getWalkinCustomerPhoneNo())) {
                    operationTO.setWalkinCustomerPhoneNo(opsCommand.getWalkinCustomerPhoneNo());
                } else {
                    operationTO.setWalkinCustomerPhoneNo("00");
                }
                if (opsCommand.getWalkInBillingTypeId() != null && !"".equals(opsCommand.getWalkInBillingTypeId())) {
                    operationTO.setWalkInBillingTypeId(opsCommand.getWalkInBillingTypeId());
                }
                if (opsCommand.getWalkinFreightWithReefer() != null && !"".equals(opsCommand.getWalkinFreightWithReefer())) {
                    operationTO.setWalkinFreightWithReefer(opsCommand.getWalkinFreightWithReefer());
                }
                if (opsCommand.getWalkinFreightWithoutReefer() != null && !"".equals(opsCommand.getWalkinFreightWithoutReefer())) {
                    operationTO.setWalkinFreightWithoutReefer(opsCommand.getWalkinFreightWithoutReefer());
                }
                if (opsCommand.getWalkinRateWithReeferPerKg() != null && !"".equals(opsCommand.getWalkinRateWithReeferPerKg())) {
                    operationTO.setWalkinRateWithReeferPerKg(opsCommand.getWalkinRateWithReeferPerKg());
                }
                if (opsCommand.getWalkinRateWithoutReeferPerKg() != null && !"".equals(opsCommand.getWalkinRateWithoutReeferPerKg())) {
                    operationTO.setWalkinRateWithoutReeferPerKg(opsCommand.getWalkinRateWithoutReeferPerKg());
                }
                if (opsCommand.getWalkinRateWithReeferPerKm() != null && !"".equals(opsCommand.getWalkinRateWithReeferPerKm())) {
                    operationTO.setWalkinRateWithReeferPerKm(opsCommand.getWalkinRateWithReeferPerKm());
                }
                if (opsCommand.getWalkinRateWithoutReeferPerKm() != null && !"".equals(opsCommand.getWalkinRateWithoutReeferPerKm())) {
                    operationTO.setWalkinRateWithoutReeferPerKm(opsCommand.getWalkinRateWithoutReeferPerKm());
                }
                if (opsCommand.getDestination() != null && !"".equals(opsCommand.getDestination())) {
                    operationTO.setDestination(opsCommand.getDestination());
                }
            }
            //Walkin Customer

            if (opsCommand.getOrderReferenceAwb() != null && !"".equals(opsCommand.getOrderReferenceAwb())) {
                operationTO.setOrderReferenceAwb(opsCommand.getOrderReferenceAwb());
            }
            if (opsCommand.getOrderReferenceAwbNo() != null && !"".equals(opsCommand.getOrderReferenceAwbNo())) {
                operationTO.setOrderReferenceAwbNo(opsCommand.getOrderReferenceAwbNo());
            }
            if (opsCommand.getAwbOrigin() != null && !"".equals(opsCommand.getAwbOrigin())) {
                operationTO.setAwbOrigin(opsCommand.getAwbOrigin());
            }
            if (opsCommand.getAwbDestination() != null && !"".equals(opsCommand.getAwbDestination())) {
                operationTO.setAwbDestination(opsCommand.getAwbDestination());
            }
            if (opsCommand.getAwbOriginId() != null && !"".equals(opsCommand.getAwbOriginId())) {
                operationTO.setAwbOrigin(opsCommand.getAwbOriginId());
            }
            if (opsCommand.getAwbDestinationId() != null && !"".equals(opsCommand.getAwbDestinationId())) {
                operationTO.setAwbDestination(opsCommand.getAwbDestinationId());
            }
            if (opsCommand.getOrderDeliveryHour() != null && !"".equals(opsCommand.getOrderDeliveryHour())) {
                operationTO.setOrderDeliveryHour(opsCommand.getOrderDeliveryHour());
            }
            if (opsCommand.getOrderDeliveryMinute() != null && !"".equals(opsCommand.getOrderDeliveryMinute())) {
                operationTO.setOrderDeliveryMinute(opsCommand.getOrderDeliveryMinute());
            }
            if (opsCommand.getAwbOrderDeliveryDate() != null && !"".equals(opsCommand.getAwbOrderDeliveryDate())) {
                operationTO.setAwbOrderDeliveryDate(opsCommand.getAwbOrderDeliveryDate());
            }
            if (opsCommand.getAwbDestinationRegion() != null && !"".equals(opsCommand.getAwbDestinationRegion())) {
                operationTO.setAwbDestinationRegion(opsCommand.getAwbDestinationRegion());
            }
            if (opsCommand.getAwbMovementType() != null && !"".equals(opsCommand.getAwbMovementType())) {
                operationTO.setAwbMovementType(opsCommand.getAwbMovementType());
            }
            if (opsCommand.getNoOfPieces() != null && !"".equals(opsCommand.getNoOfPieces())) {
                operationTO.setNoOfPieces(opsCommand.getNoOfPieces());
            }
            if (opsCommand.getGrossWeight() != null && !"".equals(opsCommand.getGrossWeight())) {
                operationTO.setGrossWeight(opsCommand.getGrossWeight());
            }
            if (opsCommand.getChargeableWeight() != null && !"".equals(opsCommand.getChargeableWeight())) {
                operationTO.setChargeableWeight(opsCommand.getChargeableWeight());
            }
            if (opsCommand.getChargeableWeightId() != null && !"".equals(opsCommand.getChargeableWeightId())) {
                operationTO.setChargeableWeightId(opsCommand.getChargeableWeightId());
            }
            if (opsCommand.getLengths() != null && !"".equals(opsCommand.getLengths())) {
                operationTO.setLengths(opsCommand.getLengths());
            }
            if (opsCommand.getWidths() != null && !"".equals(opsCommand.getWidths())) {
                operationTO.setWidths(opsCommand.getWidths());
            }
            if (opsCommand.getHeights() != null && !"".equals(opsCommand.getHeights())) {
                operationTO.setHeights(opsCommand.getHeights());
            }
            if (opsCommand.getVolumes() != null && !"".equals(opsCommand.getVolumes())) {
                operationTO.setVolumes(opsCommand.getVolumes());
            }
            if (opsCommand.getDgHandlingCode() != null && !"".equals(opsCommand.getDgHandlingCode())) {
                operationTO.setDgHandlingCode(opsCommand.getDgHandlingCode());
            }
            if (opsCommand.getUnIdNo() != null && !"".equals(opsCommand.getUnIdNo())) {
                operationTO.setUnIdNo(opsCommand.getUnIdNo());
            }
            if (opsCommand.getAwbTotalPackages() != null && !"".equals(opsCommand.getAwbTotalPackages())) {
                operationTO.setAwbTotalPackages(opsCommand.getAwbTotalPackages());
            }
            if (opsCommand.getAwbReceivedPackages() != null && !"".equals(opsCommand.getAwbReceivedPackages())) {
                operationTO.setAwbReceivedPackages(opsCommand.getAwbReceivedPackages());
            }
            if (opsCommand.getAwbPendingPackages() != null && !"".equals(opsCommand.getAwbPendingPackages())) {
                operationTO.setAwbPendingPackages(opsCommand.getAwbPendingPackages());
            }
            if (opsCommand.getAwbTotalGrossWeight() != null && !"".equals(opsCommand.getAwbTotalGrossWeight())) {
                operationTO.setAwbTotalGrossWeight(opsCommand.getAwbTotalGrossWeight());
            }

            System.out.println("opsCommand.getUnIdNo() = " + opsCommand.getUnIdNo().length);
            System.out.println("opsCommand.getDgHandlingCode() = " + opsCommand.getDgHandlingCode().length);
            System.out.println("opsCommand.getClassCode() = " + opsCommand.getClassCode().length);
            System.out.println("opsCommand.getPkgInstruction() = " + opsCommand.getPkgInstruction().length);
            System.out.println("opsCommand.getNetQuantity() = " + opsCommand.getNetQuantity().length);
            System.out.println("opsCommand.getNetUnits() = " + opsCommand.getNetUnits().length);
            if (opsCommand.getClassCode() != null && !"".equals(opsCommand.getClassCode())) {
                operationTO.setClassCode(opsCommand.getClassCode());
            }
            if (opsCommand.getPkgInstruction() != null && !"".equals(opsCommand.getPkgInstruction())) {
                operationTO.setPkgInstruction(opsCommand.getPkgInstruction());
            }
            if (opsCommand.getPkgGroup() != null && !"".equals(opsCommand.getPkgGroup())) {
                operationTO.setPkgGroup(opsCommand.getPkgGroup());
            }
            if (opsCommand.getNetQuantity() != null && !"".equals(opsCommand.getNetQuantity())) {
                operationTO.setNetQuantity(opsCommand.getNetQuantity());
            }
            if (opsCommand.getNetUnits() != null && !"".equals(opsCommand.getNetUnits())) {
                operationTO.setNetUnits(opsCommand.getNetUnits());
            }
            if (opsCommand.getTruckTravelKm() != null && !"".equals(opsCommand.getTruckTravelKm())) {
                operationTO.setTruckTravelKm(opsCommand.getTruckTravelKm());
            }
            if (opsCommand.getTruckTravelHour() != null && !"".equals(opsCommand.getTruckTravelHour())) {
                operationTO.setTruckTravelHour(opsCommand.getTruckTravelHour());
            }
            if (opsCommand.getTruckTravelMinute() != null && !"".equals(opsCommand.getTruckTravelMinute())) {
                operationTO.setTruckTravelMinute(opsCommand.getTruckTravelMinute());
            }
            if (opsCommand.getFleetTypeId() != null && !"".equals(opsCommand.getFleetTypeId())) {
                operationTO.setFleetTypeId(opsCommand.getFleetTypeId());
            }
            System.out.println("opsCommand.getTruckDepDate()" + opsCommand.getTruckDepDate());
            if (opsCommand.getTruckDepDate() != null && !"".equals(opsCommand.getTruckDepDate())) {
                operationTO.setTruckDepDate(opsCommand.getTruckDepDate());
            }
            System.out.println("After Truck Date");
            if (opsCommand.getTruckDestinationId()[0] != null && !"".equals(opsCommand.getTruckOriginId()[0])) {
                operationTO.setOrigin(opsCommand.getTruckOriginId()[0]);
            }
            if (opsCommand.getTruckDestinationId()[0] != null && !"".equals(opsCommand.getTruckDestinationId()[0])) {
                operationTO.setDestination(opsCommand.getTruckDestinationId()[0]);
            }
            if (opsCommand.getTruckRouteId() != null && !"".equals(opsCommand.getTruckRouteId())) {
                operationTO.setTruckRouteId(opsCommand.getTruckRouteId());
            }
            if (opsCommand.getFreightReceipt() != null && !"".equals(opsCommand.getFreightReceipt())) {
                operationTO.setFreightReceipt(opsCommand.getFreightReceipt());
            }
            if (opsCommand.getWareHouseName() != null && !"".equals(opsCommand.getWareHouseName())) {
                operationTO.setWareHouseName(opsCommand.getWareHouseName());
            }
            if (opsCommand.getWareHouseLocation() != null && !"".equals(opsCommand.getWareHouseLocation())) {
                operationTO.setWareHouseLocation(opsCommand.getWareHouseLocation());
            }
            if (opsCommand.getShipmentAcceptanceDate() != null && !"".equals(opsCommand.getShipmentAcceptanceDate())) {
                operationTO.setShipmentAcceptanceDate(opsCommand.getShipmentAcceptanceDate());
            }
            if (opsCommand.getShipmentAccpHour() != null && !"".equals(opsCommand.getShipmentAccpHour())) {
                operationTO.setShipmentAccpHour(opsCommand.getShipmentAccpHour());
            }
            if (opsCommand.getShipmentAccpMinute() != null && !"".equals(opsCommand.getShipmentAccpMinute())) {
                operationTO.setShipmentAccpMinute(opsCommand.getShipmentAccpMinute());
            }
            if (opsCommand.getCurrencyType() != null && !"".equals(opsCommand.getCurrencyType())) {
                operationTO.setCurrencyType(opsCommand.getCurrencyType());
            }
            if (opsCommand.getRateMode() != null && !"".equals(opsCommand.getRateMode())) {
                operationTO.setRateMode(opsCommand.getRateMode());
            }
            if (opsCommand.getPerKgRate() != null && !"".equals(opsCommand.getPerKgRate())) {
                operationTO.setPerKgRate(opsCommand.getPerKgRate());
            }
            if (opsCommand.getBookingReferenceRemarks() != null && !"".equals(opsCommand.getBookingReferenceRemarks())) {
                operationTO.setBookingReferenceRemarks(opsCommand.getBookingReferenceRemarks());
            }
            if (opsCommand.getProductRate() != null && !"".equals(opsCommand.getProductRate())) {
                operationTO.setProductRate(opsCommand.getProductRate());
            }
            if (opsCommand.getVehicleNo() != null && !"".equals(opsCommand.getVehicleNo())) {
                operationTO.setVehicleNos(opsCommand.getVehicleNo());
            }
            if (opsCommand.getVehicleId() != null && !"".equals(opsCommand.getVehicleId())) {
                operationTO.setVehicleIds(opsCommand.getVehicleId());
            }
            if (opsCommand.getContract() != null && !"".equals(opsCommand.getContract())) {
                operationTO.setContract(opsCommand.getContract());
            }
            if (opsCommand.getRateMode() != null && !"".equals(opsCommand.getRateMode())) {
                operationTO.setRateMode(opsCommand.getRateMode());
            }
            if (opsCommand.getRateMode1() != null && !"".equals(opsCommand.getRateMode1())) {
                operationTO.setRateMode1(opsCommand.getRateMode1());
            }
            System.out.println("opsCommand.getPerKgRate" + opsCommand.getPerKgRate());
            if (opsCommand.getPerKgRate() != null && !"".equals(opsCommand.getPerKgRate())) {
                operationTO.setPerKgRate(opsCommand.getPerKgRate());
            }
            if (opsCommand.getCurrencyType() != null && !"".equals(opsCommand.getCurrencyType())) {
                operationTO.setCurrencyType(opsCommand.getCurrencyType());
            }
            String[] contractId1 = request.getParameterValues("contract");
            String[] rateMode1 = request.getParameterValues("rateMode1");
            String[] rateMode = request.getParameterValues("rateMode");
            if ("1".equals(contractId1[0])) {
                if (request.getParameter("rateMode") != null && !"".equals(request.getParameter("rateMode"))) {
                    if ("1".equals(rateMode[0])) {
                        operationTO.setRateMode(rateMode[0]);
                        operationTO.setPerKgRate(request.getParameter("perKgAutoRate"));
                        operationTO.setRateValue(request.getParameter("rateValue"));
                    } else {
                        operationTO.setRateMode(rateMode[0]);
                        operationTO.setRateValue(request.getParameter("rateValue"));
                    }
                }
            } else if ("2".equals(contractId1[0])) {
                if (request.getParameter("rateMode1") != null && !"".equals(request.getParameter("rateMode1"))) {
                    if ("3".equals(rateMode1[0])) {
                        operationTO.setRateMode(rateMode1[0]);
                        operationTO.setRateValue(request.getParameter("rateValue1"));
                    } else {
                        operationTO.setRateMode(rateMode1[0]);
                        operationTO.setRateValue(request.getParameter("rateValue1"));
                    }
                }
            }
            if (opsCommand.getCommodity() != null && !"".equals(opsCommand.getCommodity())) {
                operationTO.setCommodity(opsCommand.getCommodity());
            }

            int rank = Integer.parseInt(request.getParameter("customerRank"));
            int cnoteCount = Integer.parseInt(request.getParameter("approvalStatus"));
            operationTO.setCnoteCount(cnoteCount);
            if (cnoteCount > 0) {
                ///int cnotestatus = operationBP.updateConteCount(operationTO, userId);
            }
            double creditLimit = 0.0f;
            creditLimit = Double.parseDouble(request.getParameter("creditLimit"));
            double outStanding = 0.0f;
            outStanding = Double.parseDouble(request.getParameter("outStanding"));
            String outStandingDate = request.getParameter("outStandingDate");
            operationTO.setCreditLimitAmount(creditLimit);
            operationTO.setOutStandingDate(outStandingDate);
            DecimalFormat df = new DecimalFormat("########.##");
            if (opsCommand.getTruckOriginId() != null && !"".equals(opsCommand.getTruckOriginId())) {
                operationTO.setPointId(opsCommand.getTruckOriginId());
            }
            if (opsCommand.getTruckDestinationId() != null && !"".equals(opsCommand.getTruckDestinationId())) {
                operationTO.setTruckDestinationId(opsCommand.getTruckDestinationId());
            }
            if (opsCommand.getTruckRouteId() != null && !"".equals(opsCommand.getTruckRouteId())) {
                operationTO.setTruckRouteId(opsCommand.getTruckRouteId());
            }
            System.out.println("opsCommand.getTruckRouteId() = " + opsCommand.getTruckRouteId());
            if (opsCommand.getPointName() != null && !"".equals(opsCommand.getPointName())) {
                operationTO.setPointName(opsCommand.getPointName());
            }
            if (opsCommand.getPointType() != null && !"".equals(opsCommand.getPointType())) {
                operationTO.setPointType(opsCommand.getPointType());
            }
            if (opsCommand.getPointOrder() != null && !"".equals(opsCommand.getPointOrder())) {
                operationTO.setOrder(opsCommand.getPointOrder());
            }
            if (opsCommand.getPointAddresss() != null && !"".equals(opsCommand.getPointAddresss())) {
                operationTO.setPointAddresss(opsCommand.getPointAddresss());
            }
            if (opsCommand.getMultiplePointRouteId() != null && !"".equals(opsCommand.getMultiplePointRouteId())) {
                operationTO.setMultiplePointRouteId(opsCommand.getMultiplePointRouteId());
            }
            if (opsCommand.getTruckDepDate() != null && !"".equals(opsCommand.getTruckDepDate())) {
                operationTO.setPointPlanDate(opsCommand.getTruckDepDate());
            }
            if (opsCommand.getPointPlanHour() != null && !"".equals(opsCommand.getPointPlanHour())) {
                operationTO.setPointPlanHour(opsCommand.getPointPlanHour());
            }
            if (opsCommand.getPointPlanMinute() != null && !"".equals(opsCommand.getPointPlanMinute())) {
                operationTO.setPointPlanMinute(opsCommand.getPointPlanMinute());
            }
            if (opsCommand.getFleetTypeId() != null && !"".equals(opsCommand.getFleetTypeId())) {
                operationTO.setFleetTypeId(opsCommand.getFleetTypeId());
            }
            if (opsCommand.getTruckTravelKm() != null && !"".equals(opsCommand.getTruckTravelKm())) {
                operationTO.setTruckTravelKm(opsCommand.getTruckTravelKm());
            }
            if (opsCommand.getTruckTravelHour() != null && !"".equals(opsCommand.getTruckTravelHour())) {
                operationTO.setTruckTravelHour(opsCommand.getTruckTravelHour());
            }
            if (opsCommand.getTruckTravelMinute() != null && !"".equals(opsCommand.getTruckTravelMinute())) {
                operationTO.setTruckTravelMinute(opsCommand.getTruckTravelMinute());
            }
            if (opsCommand.getVehicleNo() != null && !"".equals(opsCommand.getVehicleNo())) {
                operationTO.setVehicleNos(opsCommand.getVehicleNo());
            }
            if (opsCommand.getVehicleId() != null && !"".equals(opsCommand.getVehicleId())) {
                operationTO.setVehicleIds(opsCommand.getVehicleId());
            }
            if (opsCommand.getUsedCapacity() != null && !"".equals(opsCommand.getUsedCapacity())) {
                operationTO.setUsedCapacity(opsCommand.getUsedCapacity());
                operationTO.setReceivedPackages(request.getParameterValues("receivedPackages"));
            }
            if (opsCommand.getUsedVol() != null && !"".equals(opsCommand.getUsedVol())) {
                operationTO.setUsedVol(opsCommand.getUsedVol());
            }
            if (opsCommand.getAllowedTrucks() != null && !"".equals(opsCommand.getAllowedTrucks())) {
                operationTO.setAllowedTrucks(opsCommand.getAllowedTrucks());
            }
            if (opsCommand.getNoOfTrucks() != null && !"".equals(opsCommand.getNoOfTrucks())) {
                operationTO.setTruckNos(opsCommand.getNoOfTrucks());
            }
            ArrayList customerTypeList = new ArrayList();
            customerTypeList = operationBP.getCustomerTypeList(operationTO);
            request.setAttribute("customerTypeList", customerTypeList);
            ArrayList vehicleTypeList = new ArrayList();
            vehicleTypeList = operationBP.getVehicleTypeList();
            request.setAttribute("vehicleTypeList", vehicleTypeList);
            ArrayList billingTypeList = new ArrayList();
            billingTypeList = operationBP.getBillingTypeList();
            request.setAttribute("billingTypeList", billingTypeList);
            ArrayList productCategoryList = new ArrayList();
            productCategoryList = operationBP.getProductCategoryList();
            request.setAttribute("productCategoryList", productCategoryList);

            insertStatus = opsBP.insertConsignmentNote(operationTO, userId);

            System.out.println("insertStatus" + insertStatus);
            System.out.println("operationTO.getStatus() = " + operationTO.getStatus());
            operationTO.setConsignmentOrderId(String.valueOf(insertStatus));
            path = "content/BrattleFoods/consignmentNoteBonded.jsp";
            int insertArticle = 0;
            int insertDangerousArticle = 0;
            //insertDangerousArticle = operationBP.insertConsignmentDangerousGoods(operationTO, userId);
            if (insertStatus == 0) {
                request.setAttribute("errorMessage", "Booking failed for your AWB No: " + operationTO.getOrderReferenceAwb() + " " + operationTO.getAwbNo().substring(0, 4) + " " + operationTO.getAwbNo().substring(4));
            } else if (insertStatus == 2) {
                request.setAttribute("errorMessage", "Already booked for AWB No: " + operationTO.getOrderReferenceAwb() + " " + operationTO.getAwbNo().substring(0, 4) + " " + operationTO.getAwbNo().substring(4));
            } else if (insertStatus == 3) {
                request.setAttribute("errorMessage", "Booking failed, because your choosen truck used by another booking");
            } else {

                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date date = new Date();
                System.out.println(dateFormat.format(date));
                request.setAttribute("curDate", dateFormat.format(date));
                ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
                /*
                 String consignmentOrderNo = operationBP.getConsignmentOrderNo(operationTO);
                 String temp[] = null;
                 int cnote = 0;
                 if (consignmentOrderNo != null) {
                 temp = consignmentOrderNo.split("/");
                 cnote = Integer.parseInt(temp[2]);
                 cnote++;
                 request.setAttribute("consignmentOrderNo", temp[0] + "/" + temp[1] + "/" + cnote);
                 } else {
                 request.setAttribute("consignmentOrderNo", "CO/13-14/200301");
                 }
                 */

                request.setAttribute("successMessage", "Booking  Created Successfully: Booking No is " + operationTO.getConsignmentNoteNo() + "<br>" + " And AWB No :" + operationTO.getOrderReferenceAwb() + " " + operationTO.getOrderReferenceAwbNo());
                //send email
                /////////////////Email Part//////////////////////////////////////////////
                String to = "";
                String activitycode = "EMREQ1";
                //String to = "nipun.kohli@brattlefoods.com,srini@entitlesolutions.com";
                String smtp = "";
                int emailPort = 0;
                String frommailid = "";
                String password = "";

                ArrayList emaildetails = new ArrayList();
                emaildetails = operationBP.getEmailDetails(activitycode);
                Iterator itr = emaildetails.iterator();
                OperationTO operationTO1 = null;
                if (itr.hasNext()) {
                    operationTO1 = new OperationTO();
                    operationTO1 = (OperationTO) itr.next();
                    smtp = operationTO1.getSmtp();
                    emailPort = Integer.parseInt(operationTO1.getPort());
                    frommailid = operationTO1.getEmailId();
                    password = operationTO1.getPassword();
                    to = operationTO1.getTomailId();
                }

                String toEmailId = operationBP.getEmailToListForConsignment(operationTO.getCustomerId());
                if (toEmailId != null) {
                    String[] temp = toEmailId.split(",");
                    to = "";
                    int cntr = 0;
                    for (int i = 0; i < temp.length; i++) {
                        if (!"-".equals(temp[i]) && !"".equals(temp[i])) {
                            if (cntr == 0) {
                                to = temp[i];
                            } else {
                                to = to + "," + temp[i];

                            }
                            cntr++;
                        }
                    }
                }
                String confirmationCode = "CONFIRM0001";
                confirmationCode = operationBP.getConfirmationMail(confirmationCode);
                if (confirmationCode != null && !"".equals(confirmationCode)) {
                    to = to + "," + confirmationCode;
                }
                ArrayList emailConsignmentDetails = operationBP.getConsignmentDetails(operationTO.getConsignmentOrderId());

                itr = emailConsignmentDetails.iterator();
                operationTO1 = null;
                /*
                 <result column="Consignment_Order_No" property="consignmentNoteNo" />
                 <result column="Consignment_Order_Date" property="consignmentOrderDate" />
                 <result column="customerTypeName" property="customerTypeName" />
                 <result column="Customer_Name" property="customerName" />
                 <result column="Consigment_Origin" property="consigmentOrigin" />
                 <result column="Consigment_Destination" property="consigmentDestination" />
                 <result column="scheduleDate" property="tripScheduleDateTime" />
                 <result column="product_category_name" property="productCategoryName" />
                 <result column="reefer_minimum_temprature" property="reeferMinimumTemperature" />
                 <result column="reefer_maximum_temprature" property="reeferMaximumTemperature" />
                 <result column="total_weight" property="totalWeightage" />
                 <result column="total_packages" property="totalPackage" />
                 <result column="total_distance" property="totalDistance" />
                 <result column="total_hours" property="totalHours" />
                 <result column="vehicle_type_name" property="vehicleTypeName" />
                 */
                String vehicleType = "";
                String totalHours = "";
                String totalDistance = "";
                String totalPackage = "";
                String totalWeightage = "";
                String reeferMaximumTemperature = "";
                String reeferMinimumTemperature = "";
                String productCategoryName = "";
                String paymentType = "";
                String route = "";
                if (itr.hasNext()) {
                    operationTO1 = new OperationTO();
                    operationTO1 = (OperationTO) itr.next();
                    vehicleType = operationTO1.getVehicleTypeName();
                    totalHours = operationTO1.getTotalHours();
                    totalDistance = operationTO1.getTotalDistance();
                    totalPackage = operationTO1.getTotalPackage();
                    totalWeightage = operationTO1.getTotalWeightage();
                    route = operationTO1.getConsigmentOrigin() + "-" + operationTO1.getConsigmentDestination();
                    productCategoryName = operationTO1.getProductCategoryName() + "(Temp " + operationTO1.getReeferMinimumTemperature() + " - " + operationTO1.getReeferMaximumTemperature() + ")";
                    paymentType = operationTO1.getPaymentType();
                }

                String emailFormat = "Team, <br><br> New Order Details is given below <br> Consignment Note Created for Customer Order Ref No: " + operationTO.getConsignmentNoteNo();
                emailFormat = emailFormat + "<br> Customer: " + operationTO.getCustomerName();
                emailFormat = emailFormat + "<br> Payment Type: " + paymentType;
                emailFormat = emailFormat + "<br> Vehicle Type: " + vehicleType;
                emailFormat = emailFormat + "<br> Vehicle Required Date: " + operationTO.getVehicleRequiredDate() + " Hour:" + operationTO.getVehicleRequiredHour();
                emailFormat = emailFormat + "<br> Route: " + route;
                emailFormat = emailFormat + "<br> Product Category: " + productCategoryName;
                emailFormat = emailFormat + "<br> Reefer: " + operationTO.getReeferRequired();
                emailFormat = emailFormat + "<br> Total Packages (Nos): " + operationTO.getTotalPackage();
                emailFormat = emailFormat + "<br> Total Weight (Kg): " + operationTO.getTotalWeightage();
                emailFormat = emailFormat + "<br> Total Distance (Km): " + totalDistance;
                emailFormat = emailFormat + "<br> Transit Hours (Hr): " + totalHours;
                emailFormat = emailFormat + "<br><br>  Thanks,<br> Team BrattleFoods. ";
                String subject = "";
                if (!operationTO.getStatus().equals("3")) {
                    subject = "Consignment Note Created For : " + operationTO.getCustomerName() + ". Route " + route + " Vehicle Required On :" + operationTO.getVehicleRequiredDate() + " :" + operationTO.getVehicleRequiredHour();
                } else if (operationTO.getStatus().equals("3")) {
                    subject = "Consignment Note Created With Credit Limit Approval For : " + operationTO.getCustomerName() + ". Route " + route + " Vehicle Required On :" + operationTO.getVehicleRequiredDate() + " :" + operationTO.getVehicleRequiredHour();
                }
                ;
                String content = emailFormat;
                int mailSendingId = 0;
                TripTO tripTO = new TripTO();
                tripTO.setMailTypeId("2");
                tripTO.setMailSubjectTo(subject);
                tripTO.setMailSubjectCc(subject);
                tripTO.setMailSubjectBcc("");
                tripTO.setMailContentTo(content);
                tripTO.setMailContentCc(content);
                tripTO.setMailContentBcc("");
                tripTO.setMailIdTo(to);
                tripTO.setMailIdCc("");
                tripTO.setMailIdBcc("");
//                mailSendingId = tripBP.insertMailDetails(tripTO, userId);
                //mail.sendmail(smtp, emailPort, frommailid, password, subject, content, to);
//                new SendMail(smtp, emailPort, frommailid, password, subject, content, to, "", userId).start();

                int count = 0;
                int consignmentCount = 0;

                if (rank > 0 && rank <= 10) {
                    if (cnoteCount >= 1 && cnoteCount <= 3) {
                        activitycode = "CNOTEAPK1";
                    } else if (cnoteCount > 3 && cnoteCount <= 5) {
                        activitycode = "CNOTEAPK2";
                    } else {
                        activitycode = "CNOTEAPK3";
                    }
                } else if (cnoteCount == 1) {
                    activitycode = "CNOTEAPK1";
                } else if (cnoteCount == 2) {
                    activitycode = "CNOTEAPK2";
                } else {
                    activitycode = "CNOTEAPK3";
                }

                System.out.println("operationTO.getStatus() in the operation Controller= " + operationTO.getStatus());
                if (operationTO.getStatus().equals("3")) {
                    //activitycode = "CREDITLIMIT";
                    ArrayList creditLimitApprovalMailId = new ArrayList();
                    creditLimitApprovalMailId = operationBP.getCreditLimitEmailDetails(activitycode);
                    Iterator itr3 = creditLimitApprovalMailId.iterator();
                    OperationTO operationTO2 = null;
                    while (itr3.hasNext()) {
                        operationTO2 = new OperationTO();
                        operationTO2 = (OperationTO) itr3.next();
                        smtp = operationTO2.getSmtp();
                        emailPort = Integer.parseInt(operationTO2.getPort());
                        frommailid = operationTO2.getEmailId();
                        password = operationTO2.getPassword();
                        to = operationTO2.getTomailId();
                        count++;
                        System.out.println("count = " + count);

                        subject = "Consignment Note Created With Credilt Limit Approval For : " + operationTO.getCustomerName() + ". Route " + route + " Vehicle Required On :" + operationTO.getVehicleRequiredDate() + " :" + operationTO.getVehicleRequiredHour();
                        String emailFormat1 = "<html>"
                                + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                                + "<tr>"
                                + "<th colspan='2'>Consignment Order Created With Credit limit Approval</th>"
                                + "</tr>"
                                + "<tr><td>&nbsp; Consignment Order No:&nbsp;</td><td>&nbsp; " + operationTO.getConsignmentNoteNo() + "&nbsp;</td></tr>"
                                + "<tr><td>&nbsp; Customer:&nbsp;</td><td>&nbsp; " + operationTO.getCustomerName() + "&nbsp;</td></tr>"
                                + "<tr><td>&nbsp; Vehicle Type: &nbsp;</td><td>&nbsp;" + vehicleType + "&nbsp;</td></tr>"
                                + "<tr><td>&nbsp; Payment Type: &nbsp;</td><td>&nbsp;" + paymentType + "&nbsp;</td></tr>"
                                + "<tr><td>&nbsp; Vehicle Required Date:&nbsp; </td><td>&nbsp;" + operationTO.getVehicleRequiredDate() + " Hour:" + operationTO.getVehicleRequiredHour() + "&nbsp;</td></tr>"
                                + "<tr><td> &nbsp;Route: &nbsp;</td><td>&nbsp;" + route + "&nbsp;</td></tr>"
                                + "<tr><td>&nbsp; Product Category: &nbsp;</td><td>&nbsp;" + productCategoryName + "&nbsp;</td></tr>"
                                + "<tr><td>&nbsp; Reefer: &nbsp;</td><td>&nbsp;" + operationTO.getReeferRequired() + "&nbsp;</td></tr>"
                                + "<tr><td>&nbsp; Total Packages (Nos): &nbsp;</td><td>&nbsp;" + operationTO.getTotalPackage() + "&nbsp;</td></tr>"
                                + "<tr><td>&nbsp; Total Weight (Kg): &nbsp;</td><td>&nbsp;" + operationTO.getTotalWeightage() + "&nbsp;</td></tr>"
                                + "<tr><td>&nbsp; Total Distance (Km): &nbsp;</td><td>&nbsp;" + totalDistance + "&nbsp;</td></tr>"
                                + "<tr><td> &nbsp;Transit Hours (Hr): &nbsp;</td><td>&nbsp;" + totalHours + "&nbsp;</td></tr>"
                                + "<tr><td> &nbsp;Revenue: &nbsp;</td><td>&nbsp;" + operationTO.getTotFreightAmount() + "&nbsp;</td></tr>"
                                + "<tr><td> &nbsp;Credit Limit: &nbsp;</td><td>&nbsp;" + creditLimit + "&nbsp;</td></tr>"
                                + "<tr><td> &nbsp;Out Standing Amount: &nbsp;</td><td>&nbsp;" + df.format(outStanding) + "&nbsp;</td></tr>"
                                + "<tr height='25'><td></td><td></td></tr>"
                                + "<tr><td colspan='2' align='center'>"
                                + "<a style='text-decoration: none' href='http://203.124.105.244:8089/throttle/updateConsignmentCreditLimitStatus.do?consignmentOrderId=" + operationTO.getConsignmentOrderId() + "&mailId=" + to + "&creditLimit=" + creditLimit + "&outStanding=" + outStanding + "&step=" + 1 + "&customerId=" + opsCommand.getCustomerId() + "&approvestatus=1'>Approve</a>&nbsp;|&nbsp;"
                                + "<a style='text-decoration: none' href='http://203.124.105.244:8089/throttle/updateConsignmentCreditLimitStatus.do?consignmentOrderId=" + operationTO.getConsignmentOrderId() + "&mailId=" + to + "&creditLimit=" + creditLimit + "&outStanding=" + outStanding + "&step=" + 1 + "&customerId=" + opsCommand.getCustomerId() + "&approvestatus=2'>Reject</a>"
                                + "</td></tr>"
                                + "</table></body></html>";
                        content = emailFormat1;

//                        new SendMail(smtp, emailPort, frommailid, password, subject, content, to, "", userId).start();
                    }
                }
                System.out.println("count = " + count);
                int updateConsignmentNote = operationBP.updateConsignmentApprovalMailCount(operationTO, count);
                //end of email part
                ArrayList contractRouteList = new ArrayList();
                contractRouteList = operationBP.getContractRouteList(operationTO);
                request.setAttribute("contractRouteList", contractRouteList);

            }
            mv = consignmentNoteBonded(request, response, command);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView getCustomerContractDetails(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OperationTO operationTO = new OperationTO();
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  Route Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList routeList = new ArrayList();
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            System.out.println(dateFormat.format(date));
            request.setAttribute("curDate", dateFormat.format(date));
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/contract/contractPointToPointWeight.jsp";
            String customerId = "";
            String customerCode = "";
            String customerName = "";
            String billingTypeId = "";
            customerId = request.getParameter("custId");
            customerCode = request.getParameter("custCode");
            customerName = request.getParameter("custName");
            billingTypeId = request.getParameter("billingTypeId");
            request.setAttribute("customerId", customerId);
            request.setAttribute("customerCode", customerCode);
            request.setAttribute("customerName", customerName);
            request.setAttribute("billingTypeId", billingTypeId);
            ArrayList customerTypeList = new ArrayList();
            customerTypeList = operationBP.getCustomerTypeList(operationTO);
            request.setAttribute("customerTypeList", customerTypeList);

            ArrayList vehicleTypeList = new ArrayList();
            vehicleTypeList = operationBP.getVehicleTypeList();
            request.setAttribute("vehicleTypeList", vehicleTypeList);

            ArrayList billingTypeList = new ArrayList();
            billingTypeList = operationBP.getBillingTypeList();
            request.setAttribute("billingTypeList", billingTypeList);
            ArrayList productCategoryList = new ArrayList();
            productCategoryList = operationBP.getProductCategoryList();
            request.setAttribute("productCategoryList", productCategoryList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method is used to Get Chargeable Weight.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public void handleVehicleTypeVehicleRegNo(HttpServletRequest request, HttpServletResponse response, OpsCommand command) throws IOException {
        HttpSession session = request.getSession();
        opsCommand = command;
        OpsTO opsTO = new OpsTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList vehicleRegNoList = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        String vehilceRegNo = "";
        String vehicleTypeId = "";
        String originId = "";
        String destinationId = "";
        try {
            response.setContentType("text/html");
            vehilceRegNo = request.getParameter("vehicleRegNo");
            vehicleTypeId = request.getParameter("vehicleTypeId");
            originId = request.getParameter("truckOriginId");
            destinationId = request.getParameter("truckDestinationId");
            opsTO.setVehicleRegNo(vehilceRegNo);
            opsTO.setVehicleTypeId(vehicleTypeId);
            opsTO.setAwbOriginId(originId);
            opsTO.setAwbDestinationId(destinationId);
            //vehicleRegNoList = opsBP.getVehTypeVehicleRegNo(opsTO);
            vehicleRegNoList = opsBP.getTruckCodeList(opsTO);

            System.out.println("userDetails.size() = " + vehicleRegNoList.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = vehicleRegNoList.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                opsTO = (OpsTO) itr.next();
                jsonObject.put("Id", opsTO.getVehicleId());
                jsonObject.put("Name", opsTO.getVehicleRegNo());
                jsonObject.put("Tonnage", opsTO.getVehicleTonnage());
                jsonObject.put("Capacity", opsTO.getVehicleCapacity());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void handleCheckAwbNo(HttpServletRequest request, HttpServletResponse response, OpsCommand command) throws IOException {
        HttpSession session = request.getSession();
        opsCommand = command;
        OpsTO opsTO = new OpsTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList vehicleRegNoList = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        String awbNo = "";
        String vehicleTypeId = "";
        String originId = "";
        String destinationId = "";
        try {
            response.setContentType("text/html");
            awbNo = request.getParameter("chAwb");

            opsTO.setAwbno(awbNo);
            //vehicleRegNoList = opsBP.getVehTypeVehicleRegNo(opsTO);
            int flag = opsBP.getAwbStatus(opsTO);

            System.out.println("AWB count = " + flag);
//            JSONArray jsonArray = new JSONArray();
//            Iterator itr = vehicleRegNoList.iterator();
//            while (itr.hasNext()) {
//                JSONObject jsonObject = new JSONObject();
//                opsTO = (OpsTO) itr.next();
//                jsonObject.put("Id", opsTO.getVehicleId());
//                jsonObject.put("Name", opsTO.getVehicleRegNo());
//                System.out.println("jsonObject = " + jsonObject);
//                jsonArray.put(jsonObject);
//            }
            // System.out.println("jsonArray = " + jsonArray);

            pw.print(flag);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void handleFleetCode(HttpServletRequest request, HttpServletResponse response, OpsCommand command) throws IOException {
        HttpSession session = request.getSession();
        opsCommand = command;
        OpsTO opsTO = new OpsTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList vehicleRegNoList = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        String vehilceRegNo = "";
        String truckRequireName = "";
        String originId = "";
        String destinationId = "";
        try {
            response.setContentType("text/html");
            truckRequireName = request.getParameter("vehicleTypeId");
            //  originId = request.getParameter("truckOriginId");
            opsTO.setVehicleTypeId(truckRequireName);
            vehicleRegNoList = opsBP.getFleetCode(opsTO);
            System.out.println("userDetails.size() = " + vehicleRegNoList.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = vehicleRegNoList.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                opsTO = (OpsTO) itr.next();
                jsonObject.put("Id", opsTO.getVehicleId());
                jsonObject.put("Name", opsTO.getVehicleRegNo());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView saveCustomerContractDetails(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OperationTO operationTO = new OperationTO();
        OpsTO opsTO = new OpsTO();
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  Route Details";
        request.setAttribute("pageTitle", pageTitle);
        int userId = (Integer) session.getAttribute("userId");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList routeList = new ArrayList();
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            System.out.println(dateFormat.format(date));
            request.setAttribute("curDate", dateFormat.format(date));
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/contract/contractPointToPointWeight.jsp";
            opsTO.setUserId(userId);
            if (opsCommand.getCustomerId() != null && !"".equals(opsCommand.getCustomerId())) {
                opsTO.setCustomerId(opsCommand.getCustomerId());
            }
            if (opsCommand.getCustomerCode() != null && !"".equals(opsCommand.getCustomerCode())) {
                opsTO.setCustomerCode(opsCommand.getCustomerCode());
            }
            if (opsCommand.getContractFrom() != null && !"".equals(opsCommand.getContractFrom())) {
                opsTO.setContractFrom(opsCommand.getContractFrom());
            }
            if (opsCommand.getContractTo() != null && !"".equals(opsCommand.getContractTo())) {
                opsTO.setContractTo(opsCommand.getContractTo());
            }
            if (opsCommand.getContractNo() != null && !"".equals(opsCommand.getContractNo())) {
                opsTO.setContractNo(opsCommand.getContractNo());
            }
            if (opsCommand.getBillingTypeId() != null && !"".equals(opsCommand.getBillingTypeId())) {
                opsTO.setBillingTypeId(opsCommand.getBillingTypeId());
            }

            //Full Truck Start
            if (opsCommand.getOriginIdFullTruck() != null && !"".equals(opsCommand.getOriginIdFullTruck())) {
                opsTO.setOriginIdFullTruck(opsCommand.getOriginIdFullTruck());
            }
            if (opsCommand.getOriginNameFullTruck() != null && !"".equals(opsCommand.getOriginNameFullTruck())) {
                opsTO.setOriginNameFullTruck(opsCommand.getOriginNameFullTruck());
            }
            if (opsCommand.getDestinationIdFullTruck() != null && !"".equals(opsCommand.getDestinationIdFullTruck())) {
                opsTO.setDestinationIdFullTruck(opsCommand.getDestinationIdFullTruck());
            }
            if (opsCommand.getDestinationNameFullTruck() != null && !"".equals(opsCommand.getDestinationNameFullTruck())) {
                opsTO.setDestinationNameFullTruck(opsCommand.getDestinationNameFullTruck());
            }
            if (opsCommand.getRouteIdFullTruck() != null && !"".equals(opsCommand.getRouteIdFullTruck())) {
                opsTO.setRouteIdFullTruck(opsCommand.getRouteIdFullTruck());
            }
            if (opsCommand.getTravelKmFullTruck() != null && !"".equals(opsCommand.getTravelKmFullTruck())) {
                opsTO.setTravelKmFullTruck(opsCommand.getTravelKmFullTruck());
            }
            if (opsCommand.getTravelHourFullTruck() != null && !"".equals(opsCommand.getTravelHourFullTruck())) {
                opsTO.setTravelHourFullTruck(opsCommand.getTravelHourFullTruck());
            }
            if (opsCommand.getTravelMinuteFullTruck() != null && !"".equals(opsCommand.getTravelMinuteFullTruck())) {
                opsTO.setTravelMinuteFullTruck(opsCommand.getTravelMinuteFullTruck());
            }
            if (opsCommand.getMovementTypeIdFullTruck() != null && !"".equals(opsCommand.getMovementTypeIdFullTruck())) {
                opsTO.setMovementTypeIdFullTruck(opsCommand.getMovementTypeIdFullTruck());
            }
            if (opsCommand.getVehicleTypeIdFullTruck() != null && !"".equals(opsCommand.getVehicleTypeIdFullTruck())) {
                opsTO.setVehicleTypeIdFullTruck(opsCommand.getVehicleTypeIdFullTruck());
            }
            if (opsCommand.getRateWithReeferFullTruck() != null && !"".equals(opsCommand.getRateWithReeferFullTruck())) {
                opsTO.setRateWithReeferFullTruck(opsCommand.getRateWithReeferFullTruck());
            }
            if (opsCommand.getRateWithOutReeferFullTruck() != null && !"".equals(opsCommand.getRateWithOutReeferFullTruck())) {
                opsTO.setRateWithOutReeferFullTruck(opsCommand.getRateWithOutReeferFullTruck());
            }

            //Weight Beak Start
            if (opsCommand.getOriginIdWeightBreak() != null && !"".equals(opsCommand.getOriginIdWeightBreak())) {
                opsTO.setOriginIdWeightBreak(opsCommand.getOriginIdWeightBreak());
            }
            if (opsCommand.getOriginNameWeightBreak() != null && !"".equals(opsCommand.getOriginNameWeightBreak())) {
                opsTO.setOriginNameWeightBreak(opsCommand.getOriginNameWeightBreak());
            }
            if (opsCommand.getDestinationIdWeightBreak() != null && !"".equals(opsCommand.getDestinationIdWeightBreak())) {
                opsTO.setDestinationIdWeightBreak(opsCommand.getDestinationIdWeightBreak());
            }
            if (opsCommand.getDestinationNameWeightBreak() != null && !"".equals(opsCommand.getDestinationNameWeightBreak())) {
                opsTO.setDestinationNameWeightBreak(opsCommand.getDestinationNameWeightBreak());
            }
            if (opsCommand.getRouteIdWeightBreak() != null && !"".equals(opsCommand.getRouteIdWeightBreak())) {
                opsTO.setRouteIdWeightBreak(opsCommand.getRouteIdWeightBreak());
            }
            if (opsCommand.getTravelKmWeightBreak() != null && !"".equals(opsCommand.getTravelKmWeightBreak())) {
                opsTO.setTravelKmWeightBreak(opsCommand.getTravelKmWeightBreak());
            }
            if (opsCommand.getTravelHourWeightBreak() != null && !"".equals(opsCommand.getTravelHourWeightBreak())) {
                opsTO.setTravelHourWeightBreak(opsCommand.getTravelHourWeightBreak());
            }
            if (opsCommand.getTravelMinuteWeightBreak() != null && !"".equals(opsCommand.getTravelMinuteWeightBreak())) {
                opsTO.setTravelMinuteWeightBreak(opsCommand.getTravelMinuteWeightBreak());
            }
            if (opsCommand.getMovementTypeIdWeightBreak() != null && !"".equals(opsCommand.getMovementTypeIdWeightBreak())) {
                opsTO.setMovementTypeIdWeightBreak(opsCommand.getMovementTypeIdWeightBreak());
            }
            if (opsCommand.getFromKgWeightBreak() != null && !"".equals(opsCommand.getFromKgWeightBreak())) {
                opsTO.setFromKgWeightBreak(opsCommand.getFromKgWeightBreak());
            }
            if (opsCommand.getToKgWeightBreak() != null && !"".equals(opsCommand.getToKgWeightBreak())) {
                opsTO.setToKgWeightBreak(opsCommand.getToKgWeightBreak());
            }
            if (opsCommand.getRateWithReeferWeightBreak() != null && !"".equals(opsCommand.getRateWithReeferWeightBreak())) {
                opsTO.setRateWithReeferWeightBreak(opsCommand.getRateWithReeferWeightBreak());
            }
            if (opsCommand.getRateWithOutReeferWeightBreak() != null && !"".equals(opsCommand.getRateWithOutReeferWeightBreak())) {
                opsTO.setRateWithOutReeferWeightBreak(opsCommand.getRateWithOutReeferWeightBreak());
            }
            int insertContract = opsBP.insertCustomerContractDetails(opsTO);
            if (insertContract > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Contract Updated Successfully");
            }

            ArrayList customerTypeList = new ArrayList();
            customerTypeList = operationBP.getCustomerTypeList(operationTO);
            request.setAttribute("customerTypeList", customerTypeList);

            ArrayList vehicleTypeList = new ArrayList();
            vehicleTypeList = operationBP.getVehicleTypeList();
            request.setAttribute("vehicleTypeList", vehicleTypeList);

            ArrayList billingTypeList = new ArrayList();
            billingTypeList = operationBP.getBillingTypeList();
            request.setAttribute("billingTypeList", billingTypeList);
            ArrayList productCategoryList = new ArrayList();
            productCategoryList = operationBP.getProductCategoryList();
            request.setAttribute("productCategoryList", productCategoryList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewCustomerContractDetails(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OperationTO operationTO = new OperationTO();
        OpsTO opsTO = new OpsTO();
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  Route Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList routeList = new ArrayList();
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            System.out.println(dateFormat.format(date));
            request.setAttribute("curDate", dateFormat.format(date));
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/contract/viewCustomerContractDetails.jsp";
            String customerId = "";
            String customerCode = "";
            String customerName = "";
            String billingTypeId = "";
            customerId = request.getParameter("custId");
            customerCode = request.getParameter("custCode");
            customerName = request.getParameter("custName");
            billingTypeId = request.getParameter("billingTypeId");
            request.setAttribute("customerId", customerId);
            request.setAttribute("customerCode", customerCode);
            request.setAttribute("customerName", customerName);
            request.setAttribute("billingTypeId", billingTypeId);

            opsTO.setCustomerId(customerId);
            ArrayList contractDetails = new ArrayList();
            contractDetails = opsBP.getCustomerContractDetails(opsTO);
            request.setAttribute("contractDetails", contractDetails);

            ArrayList contractRouteDetails = new ArrayList();
            contractRouteDetails = opsBP.getContractRouteDetails(opsTO);
            request.setAttribute("contractRouteDetails", contractRouteDetails);

            ArrayList contractWeightDetails = new ArrayList();
            contractWeightDetails = opsBP.getContractWeightRouteDetails(opsTO);
            request.setAttribute("contractWeightDetails", contractWeightDetails);

            ArrayList customerTypeList = new ArrayList();
            customerTypeList = operationBP.getCustomerTypeList(operationTO);
            request.setAttribute("customerTypeList", customerTypeList);

            ArrayList vehicleTypeList = new ArrayList();
            vehicleTypeList = operationBP.getVehicleTypeList();
            request.setAttribute("vehicleTypeList", vehicleTypeList);

            ArrayList billingTypeList = new ArrayList();
            billingTypeList = operationBP.getBillingTypeList();
            request.setAttribute("billingTypeList", billingTypeList);
            ArrayList productCategoryList = new ArrayList();
            productCategoryList = operationBP.getProductCategoryList();
            request.setAttribute("productCategoryList", productCategoryList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method is used to Save Edit Consignment Note.
     *
     * @param request - Http request object
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView saveEditBookingOrder(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        System.out.println("userId = " + 0);
        int userId = (Integer) session.getAttribute("userId");
        System.out.println("userId = " + userId);
        opsCommand = command;
        String menuPath = "";
        OperationTO operationTO = new OperationTO();
        String pageTitle = "Save Edit ConsignmentNote";
        menuPath = "Operation >>  Save Edit ConsignmentNote";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList routeList = new ArrayList();
        String consignmentOrderId = "";
        String branchId = "";
        try {

            String[] availableWeight = request.getParameterValues("availableCap");
            String[] availableVolume = request.getParameterValues("assignedCap");
            consignmentOrderId = request.getParameter("consignmentOrderId");
            branchId = (String) session.getAttribute("BranchId");

            operationTO.setBranchId(branchId);
            operationTO.setAvailableVolume(availableVolume);
            operationTO.setAvailableWeight(availableWeight);
            operationTO.setConsignmentOrderId(consignmentOrderId);
            operationTO.setAirlineName(request.getParameter("airlineName"));
            operationTO.setShipmentType(request.getParameter("shipmentType"));
            int insertStatus = 0;
            if (opsCommand.getCustomerTypeId() != null && !"".equals(opsCommand.getCustomerTypeId())) {
                operationTO.setCustomerTypeId(opsCommand.getCustomerTypeId());
            }
            if (opsCommand.getAwbType() != null && !"".equals(opsCommand.getAwbType())) {
                operationTO.setAwbType(opsCommand.getAwbType());
            }

            if (opsCommand.getPaymentType() != null && !"".equals(opsCommand.getPaymentType())) {
                operationTO.setPaymentType(opsCommand.getPaymentType());
            }

            if (opsCommand.getEntryType() != null && !"".equals(opsCommand.getEntryType())) {
                operationTO.setEntryType(opsCommand.getEntryType());
            }
//            if (opsCommand.getConsignmentNoteNo() != null && !"".equals(opsCommand.getConsignmentNoteNo())) {
//                operationTO.setConsignmentNoteNo(opsCommand.getConsignmentNoteNo());
//            }
            if (opsCommand.getConsignmentDate() != null && !"".equals(opsCommand.getConsignmentDate())) {
                operationTO.setConsignmentDate(opsCommand.getConsignmentDate());
            }
            if (opsCommand.getOrderReferenceNo() != null && !"".equals(opsCommand.getOrderReferenceNo())) {
                operationTO.setOrderReferenceNo(opsCommand.getOrderReferenceNo());
            }
            String[] remarks = request.getParameterValues("bookingReferenceRemarks");
            String bookingRemarks = "";
            if (remarks != null && !"".equals(remarks)) {
                for (int i = 0; i < remarks.length; i++) {
                    if (i == 0) {
                        bookingRemarks = remarks[i];
                    } else {
                        bookingRemarks = bookingRemarks + "~" + remarks[i];
                    }

                }
            }
            operationTO.setOrderReferenceRemarks(bookingRemarks);

//            if (opsCommand.getOrderReferenceRemarks() != null && !"".equals(opsCommand.getOrderReferenceRemarks())) {
//                operationTO.setOrderReferenceRemarks(opsCommand.getOrderReferenceRemarks());
//            }
            if (opsCommand.getProductCategoryId() != null && !"".equals(opsCommand.getProductCategoryId())) {
                operationTO.setProductCategoryId(opsCommand.getProductCategoryId());
            }
            if (opsCommand.getConsignmentOrderId() != null && !"".equals(opsCommand.getConsignmentOrderId())) {
                operationTO.setConsignmentOrderId(opsCommand.getConsignmentOrderId());
            }
            System.out.println("opsCommand.getConsignmentOrderId():::::" + opsCommand.getConsignmentOrderId());
            //generate tripcode
//            String cNoteCode = "CO/13-14/";;
//            String cNoteCodeSequence = operationBP.getCnoteCodeSequence();
//            cNoteCode = cNoteCode + cNoteCodeSequence;
//            operationTO.setConsignmentNoteNo(cNoteCode);

            if (operationTO.getCustomerTypeId().equals("1")) {
                if (opsCommand.getCustomerId() != null && !"".equals(opsCommand.getCustomerId())) {
                    operationTO.setCustomerId(opsCommand.getCustomerId());
                }
                if (opsCommand.getCustomerName() != null && !"".equals(opsCommand.getCustomerName())) {
                    operationTO.setCustomerName(opsCommand.getCustomerName());
                }
                if (opsCommand.getCustomerCode() != null && !"".equals(opsCommand.getCustomerCode())) {
                    operationTO.setCustomerCode(opsCommand.getCustomerCode());
                }
                if (opsCommand.getCustomerAddress() != null && !"".equals(opsCommand.getCustomerAddress())) {
                    operationTO.setCustomerAddress(opsCommand.getCustomerAddress());
                }
                if (opsCommand.getPincode() != null && !"".equals(opsCommand.getPincode())) {
                    operationTO.setPincode(opsCommand.getPincode());
                }
                if (opsCommand.getCustomerMobileNo() != null && !"".equals(opsCommand.getCustomerMobileNo())) {
                    operationTO.setCustomerMobileNo(opsCommand.getCustomerMobileNo());
                }
                if (opsCommand.getMailId() != null && !"".equals(opsCommand.getMailId())) {
                    operationTO.setMailId(opsCommand.getMailId());
                }
                if (opsCommand.getCustomerPhoneNo() != null && !"".equals(opsCommand.getCustomerPhoneNo())) {
                    operationTO.setCustomerPhoneNo(opsCommand.getCustomerPhoneNo());
                }

                if (opsCommand.getContractId() != null && !"".equals(opsCommand.getContractId())) {
                    operationTO.setContractId(Integer.parseInt(opsCommand.getContractId()));
                }
                if (opsCommand.getDestination() != null && !"".equals(opsCommand.getDestination())) {
                    operationTO.setDestination(opsCommand.getDestination());
                }
            }
            if (opsCommand.getBillingTypeId() != null && !"".equals(opsCommand.getBillingTypeId())) {
                operationTO.setBillingTypeId(opsCommand.getBillingTypeId());
            }
            if (opsCommand.getOrigin() != null && !"".equals(opsCommand.getOrigin())) {
                operationTO.setOrigin(opsCommand.getOrigin());
            }
            if (opsCommand.getBusinessType() != null && !"".equals(opsCommand.getBusinessType())) {
                operationTO.setBusinessType(opsCommand.getBusinessType());
            }
            if (opsCommand.getMultiPickup() != null && !"".equals(opsCommand.getMultiPickup())) {
                operationTO.setMultiPickup(opsCommand.getMultiPickup());
            }
            if (opsCommand.getMultiDelivery() != null && !"".equals(opsCommand.getMultiDelivery())) {
                operationTO.setMultiDelivery(opsCommand.getMultiDelivery());
            }
            if (opsCommand.getConsignmentOrderInstruction() != null && !"".equals(opsCommand.getConsignmentOrderInstruction())) {
                operationTO.setConsignmentOrderInstruction(opsCommand.getConsignmentOrderInstruction());
            }
            if (opsCommand.getProductCodes() != null && !"".equals(opsCommand.getProductCodes())) {
                operationTO.setProductCodes(opsCommand.getProductCodes());
            }
            if (opsCommand.getBatchCode() != null && !"".equals(opsCommand.getBatchCode())) {
                operationTO.setBatchCode(opsCommand.getBatchCode());
            }
            if (opsCommand.getUom() != null && !"".equals(opsCommand.getUom())) {
                operationTO.setUom(opsCommand.getUom());
            }
            if (opsCommand.getProductNames() != null && !"".equals(opsCommand.getProductNames())) {
                operationTO.setProductNames(opsCommand.getProductNames());
            }
            if (opsCommand.getPackagesNos() != null && !"".equals(opsCommand.getPackagesNos())) {
                operationTO.setPackagesNos(opsCommand.getPackagesNos());
            }
            if (opsCommand.getWeights() != null && !"".equals(opsCommand.getWeights())) {
                operationTO.setWeights(opsCommand.getWeights());
            }
            if (opsCommand.getTotalPackage() != null && !"".equals(opsCommand.getTotalPackage())) {
                operationTO.setTotalPackage(opsCommand.getTotalPackage());
            }
            if (opsCommand.getTotalWeightage() != null && !"".equals(opsCommand.getTotalWeightage())) {
                operationTO.setTotalWeightage(opsCommand.getTotalWeightage());
            }
            if (opsCommand.getTotalChargeableWeights() != null && !"".equals(opsCommand.getTotalChargeableWeights())) {
                operationTO.setTotalChargeableWeights(opsCommand.getTotalChargeableWeights());
            }
            if (opsCommand.getTotalVolume() != null && !"".equals(opsCommand.getTotalVolume())) {
                operationTO.setTotalVolume(opsCommand.getTotalVolume());
            }
            if (opsCommand.getServiceType() != null && !"".equals(opsCommand.getServiceType())) {
                operationTO.setServiceType(opsCommand.getServiceType());
            }
            if (request.getParameter("vehicleTypeId") != null && !"".equals(request.getParameter("vehicleTypeId"))) {
                operationTO.setVehicleTypeId(request.getParameter("vehicleTypeId"));
            }
            if (opsCommand.getReeferRequired() != null && !"".equals(opsCommand.getReeferRequired())) {
                operationTO.setReeferRequired(opsCommand.getReeferRequired());
            }
            if (opsCommand.getRouteContractId() != null && !"".equals(opsCommand.getRouteContractId())) {
                operationTO.setRouteContractId(Integer.parseInt(opsCommand.getRouteContractId()));
            }
            if (opsCommand.getRouteId() != null && !"".equals(opsCommand.getRouteId())) {
                operationTO.setRouteId(opsCommand.getRouteId());
            }
            if (opsCommand.getContractRateId() != null && !"".equals(opsCommand.getContractRateId())) {
                operationTO.setContractRateId(opsCommand.getContractRateId());
            }
            if (opsCommand.getTotalKm() != null && !"".equals(opsCommand.getTotalKm())) {
                operationTO.setTotalKm(opsCommand.getTotalKm());
            }
            if (opsCommand.getTotalHours() != null && !"".equals(opsCommand.getTotalHours())) {
                operationTO.setTotalHours(opsCommand.getTotalHours());
            }
            if (opsCommand.getTotalMinutes() != null && !"".equals(opsCommand.getTotalMinutes())) {
                operationTO.setTotalMinutes(opsCommand.getTotalMinutes());
            }
            if (opsCommand.getRateWithReefer() != null && !"".equals(opsCommand.getRateWithReefer())) {
                operationTO.setRateWithReefer(opsCommand.getRateWithReefer());
            }
            if (opsCommand.getRateWithoutReefer() != null && !"".equals(opsCommand.getRateWithoutReefer())) {
                operationTO.setRateWithoutReefer(opsCommand.getRateWithoutReefer());
            }
            if (opsCommand.getVehicleRequiredDate() != null && !"".equals(opsCommand.getVehicleRequiredDate())) {
                operationTO.setVehicleRequiredDate(opsCommand.getVehicleRequiredDate());
            }
            if (opsCommand.getVehicleRequiredHour() != null && !"".equals(opsCommand.getVehicleRequiredHour())) {
                operationTO.setVehicleRequiredHour(opsCommand.getVehicleRequiredHour());
            }
            if (opsCommand.getVehicleRequiredMinute() != null && !"".equals(opsCommand.getVehicleRequiredMinute())) {
                operationTO.setVehicleRequiredMinute(opsCommand.getVehicleRequiredMinute());
            }
            if (opsCommand.getVehicleInstruction() != null && !"".equals(opsCommand.getVehicleInstruction())) {
                operationTO.setVehicleInstruction(opsCommand.getVehicleInstruction());
            }
            if (opsCommand.getConsignorName() != null && !"".equals(opsCommand.getConsignorName())) {
                operationTO.setConsignorName(opsCommand.getConsignorName());
            }
            if (opsCommand.getConsignorPhoneNo() != null && !"".equals(opsCommand.getConsignorPhoneNo())) {
                operationTO.setConsignorPhoneNo(opsCommand.getConsignorPhoneNo());
            }
            if (opsCommand.getConsignorAddress() != null && !"".equals(opsCommand.getConsignorAddress())) {
                operationTO.setConsignorAddress(opsCommand.getConsignorAddress());
            }
            if (opsCommand.getConsigneeName() != null && !"".equals(opsCommand.getConsigneeName())) {
                operationTO.setConsigneeName(opsCommand.getConsigneeName());
            }
            if (opsCommand.getConsigneePhoneNo() != null && !"".equals(opsCommand.getConsigneePhoneNo())) {
                operationTO.setConsigneePhoneNo(opsCommand.getConsigneePhoneNo());
            }
            if (opsCommand.getConsigneeAddress() != null && !"".equals(opsCommand.getConsigneeAddress())) {
                operationTO.setConsigneeAddress(opsCommand.getConsigneeAddress());
            }
            if (opsCommand.getTotFreightAmount() != null && !"".equals(opsCommand.getTotFreightAmount())) {
                operationTO.setTotFreightAmount(opsCommand.getTotFreightAmount());
            }
            if (opsCommand.getDocCharges() != null && !"".equals(opsCommand.getDocCharges())) {
                operationTO.setDocCharges(opsCommand.getDocCharges());
            }
            if (opsCommand.getOdaCharges() != null && !"".equals(opsCommand.getOdaCharges())) {
                operationTO.setOdaCharges(opsCommand.getOdaCharges());
            }
            if (opsCommand.getMultiPickupCharge() != null && !"".equals(opsCommand.getMultiPickupCharge())) {
                operationTO.setMultiPickupCharge(opsCommand.getMultiPickupCharge());
            }
            if (opsCommand.getMultiDeliveryCharge() != null && !"".equals(opsCommand.getMultiDeliveryCharge())) {
                operationTO.setMultiDeliveryCharge(opsCommand.getMultiDeliveryCharge());
            }
            if (opsCommand.getHandleCharges() != null && !"".equals(opsCommand.getHandleCharges())) {
                operationTO.setHandleCharges(opsCommand.getHandleCharges());
            }
            if (opsCommand.getOtherCharges() != null && !"".equals(opsCommand.getOtherCharges())) {
                operationTO.setOtherCharges(opsCommand.getOtherCharges());
            }
            if (opsCommand.getUnloadingCharges() != null && !"".equals(opsCommand.getUnloadingCharges())) {
                operationTO.setUnloadingCharges(opsCommand.getUnloadingCharges());
            }
            if (opsCommand.getLoadingCharges() != null && !"".equals(opsCommand.getLoadingCharges())) {
                operationTO.setLoadingCharges(opsCommand.getLoadingCharges());
            }
            if (opsCommand.getSubTotal() != null && !"".equals(opsCommand.getSubTotal())) {
                operationTO.setSubTotal(opsCommand.getSubTotal());
            }
            if (opsCommand.getTotalCharges() != null && !"".equals(opsCommand.getTotalCharges())) {
                operationTO.setTotalCharges(opsCommand.getTotalCharges());
            }
            //Walkin Customer
            if (operationTO.getCustomerTypeId().equals("2")) {
                if (opsCommand.getWalkinCustomerName() != null && !"".equals(opsCommand.getWalkinCustomerName())) {
                    operationTO.setWalkinCustomerName(opsCommand.getWalkinCustomerName());
                }
                if (opsCommand.getWalkinCustomerCode() != null && !"".equals(opsCommand.getWalkinCustomerCode())) {
                    operationTO.setWalkinCustomerCode(opsCommand.getWalkinCustomerCode());
                }
                if (opsCommand.getWalkinCustomerAddress() != null && !"".equals(opsCommand.getWalkinCustomerAddress())) {
                    operationTO.setWalkinCustomerAddress(opsCommand.getWalkinCustomerAddress());
                }
                if (opsCommand.getWalkinPincode() != null && !"".equals(opsCommand.getWalkinPincode())) {
                    operationTO.setWalkinPincode(opsCommand.getWalkinPincode());
                }
                if (opsCommand.getWalkinCustomerMobileNo() != null && !"".equals(opsCommand.getWalkinCustomerMobileNo())) {
                    operationTO.setWalkinCustomerMobileNo(opsCommand.getWalkinCustomerMobileNo());
                }
                if (opsCommand.getWalkinMailId() != null && !"".equals(opsCommand.getWalkinMailId())) {
                    operationTO.setWalkinMailId(opsCommand.getWalkinMailId());
                }
                if (opsCommand.getWalkinCustomerPhoneNo() != null && !"".equals(opsCommand.getWalkinCustomerPhoneNo())) {
                    operationTO.setWalkinCustomerPhoneNo(opsCommand.getWalkinCustomerPhoneNo());
                }
                if (opsCommand.getWalkInBillingTypeId() != null && !"".equals(opsCommand.getWalkInBillingTypeId())) {
                    operationTO.setWalkInBillingTypeId(opsCommand.getWalkInBillingTypeId());
                }
                if (opsCommand.getWalkinFreightWithReefer() != null && !"".equals(opsCommand.getWalkinFreightWithReefer())) {
                    operationTO.setWalkinFreightWithReefer(opsCommand.getWalkinFreightWithReefer());
                }
                if (opsCommand.getWalkinFreightWithoutReefer() != null && !"".equals(opsCommand.getWalkinFreightWithoutReefer())) {
                    operationTO.setWalkinFreightWithoutReefer(opsCommand.getWalkinFreightWithoutReefer());
                }
                if (opsCommand.getWalkinRateWithReeferPerKg() != null && !"".equals(opsCommand.getWalkinRateWithReeferPerKg())) {
                    operationTO.setWalkinRateWithReeferPerKg(opsCommand.getWalkinRateWithReeferPerKg());
                }
                if (opsCommand.getWalkinRateWithoutReeferPerKg() != null && !"".equals(opsCommand.getWalkinRateWithoutReeferPerKg())) {
                    operationTO.setWalkinRateWithoutReeferPerKg(opsCommand.getWalkinRateWithoutReeferPerKg());
                }
                if (opsCommand.getWalkinRateWithReeferPerKm() != null && !"".equals(opsCommand.getWalkinRateWithReeferPerKm())) {
                    operationTO.setWalkinRateWithReeferPerKm(opsCommand.getWalkinRateWithReeferPerKm());
                }
                if (opsCommand.getWalkinRateWithoutReeferPerKm() != null && !"".equals(opsCommand.getWalkinRateWithoutReeferPerKm())) {
                    operationTO.setWalkinRateWithoutReeferPerKm(opsCommand.getWalkinRateWithoutReeferPerKm());
                }
                if (opsCommand.getDestination() != null && !"".equals(opsCommand.getDestination())) {
                    operationTO.setDestination(opsCommand.getDestination());
                }
            }
            if (opsCommand.getAwbTotalPackages() != null && !"".equals(opsCommand.getAwbTotalPackages())) {
                operationTO.setAwbTotalPackages(opsCommand.getAwbTotalPackages());
            }
            if (opsCommand.getAwbReceivedPackages() != null && !"".equals(opsCommand.getAwbReceivedPackages())) {
                operationTO.setAwbReceivedPackages(opsCommand.getAwbReceivedPackages());
            }
            if (opsCommand.getAwbPendingPackages() != null && !"".equals(opsCommand.getAwbPendingPackages())) {
                operationTO.setAwbPendingPackages(opsCommand.getAwbPendingPackages());
            }
            if (opsCommand.getAwbTotalGrossWeight() != null && !"".equals(opsCommand.getAwbTotalGrossWeight())) {
                operationTO.setAwbTotalGrossWeight(opsCommand.getAwbTotalGrossWeight());
            }
            //Walkin Customer

            if (opsCommand.getOrderReferenceAwb() != null && !"".equals(opsCommand.getOrderReferenceAwb())) {
                operationTO.setOrderReferenceAwb(opsCommand.getOrderReferenceAwb());
            }
            if (opsCommand.getOrderReferenceAwbNo() != null && !"".equals(opsCommand.getOrderReferenceAwbNo())) {
                operationTO.setOrderReferenceAwbNo(opsCommand.getOrderReferenceAwbNo());
            }
            if (opsCommand.getOrderReferenceEnd() != null && !"".equals(opsCommand.getOrderReferenceEnd())) {
                operationTO.setOrderReferenceEnd(opsCommand.getOrderReferenceEnd());
            }
            if (opsCommand.getAwbOrigin() != null && !"".equals(opsCommand.getAwbOrigin())) {
                operationTO.setAwbOrigin(opsCommand.getAwbOrigin());
            }
            if (opsCommand.getAwbDestination() != null && !"".equals(opsCommand.getAwbDestination())) {
                operationTO.setAwbDestination(opsCommand.getAwbDestination());
            }
            if (opsCommand.getAwbOriginId() != null && !"".equals(opsCommand.getAwbOriginId())) {
                operationTO.setAwbOrigin(opsCommand.getAwbOriginId());
            }
            if (opsCommand.getAwbDestinationId() != null && !"".equals(opsCommand.getAwbDestinationId())) {
                operationTO.setAwbDestination(opsCommand.getAwbDestinationId());
            }
            if (opsCommand.getOrderDeliveryHour() != null && !"".equals(opsCommand.getOrderDeliveryHour())) {
                operationTO.setOrderDeliveryHour(opsCommand.getOrderDeliveryHour());
            }
            if (opsCommand.getOrderDeliveryMinute() != null && !"".equals(opsCommand.getOrderDeliveryMinute())) {
                operationTO.setOrderDeliveryMinute(opsCommand.getOrderDeliveryMinute());
            }
            if (opsCommand.getAwbOrderDeliveryDate() != null && !"".equals(opsCommand.getAwbOrderDeliveryDate())) {
                operationTO.setAwbOrderDeliveryDate(opsCommand.getAwbOrderDeliveryDate());
            }
            if (opsCommand.getAwbDestinationRegion() != null && !"".equals(opsCommand.getAwbDestinationRegion())) {
                operationTO.setAwbDestinationRegion(opsCommand.getAwbDestinationRegion());
            }
            if (opsCommand.getAwbMovementType() != null && !"".equals(opsCommand.getAwbMovementType())) {
                operationTO.setAwbMovementType(opsCommand.getAwbMovementType());
            }
            if (opsCommand.getNoOfPieces() != null && !"".equals(opsCommand.getNoOfPieces())) {
                operationTO.setNoOfPieces(opsCommand.getNoOfPieces());
            }
            if (opsCommand.getGrossWeight() != null && !"".equals(opsCommand.getGrossWeight())) {
                operationTO.setGrossWeight(opsCommand.getGrossWeight());
            }
            if (opsCommand.getChargeableWeight() != null && !"".equals(opsCommand.getChargeableWeight())) {
                operationTO.setChargeableWeight(opsCommand.getChargeableWeight());
            }
            if (opsCommand.getChargeableWeightId() != null && !"".equals(opsCommand.getChargeableWeightId())) {
                operationTO.setChargeableWeightId(opsCommand.getChargeableWeightId());
            }
            if (opsCommand.getLengths() != null && !"".equals(opsCommand.getLengths())) {
                operationTO.setLengths(opsCommand.getLengths());
            }
            if (opsCommand.getWidths() != null && !"".equals(opsCommand.getWidths())) {
                operationTO.setWidths(opsCommand.getWidths());
            }
            if (opsCommand.getHeights() != null && !"".equals(opsCommand.getHeights())) {
                operationTO.setHeights(opsCommand.getHeights());
            }
            if (opsCommand.getVolumes() != null && !"".equals(opsCommand.getVolumes())) {
                operationTO.setVolumes(opsCommand.getVolumes());
            }
            if (opsCommand.getDgHandlingCode() != null && !"".equals(opsCommand.getDgHandlingCode())) {
                operationTO.setDgHandlingCode(opsCommand.getDgHandlingCode());
            }
            if (opsCommand.getUnIdNo() != null && !"".equals(opsCommand.getUnIdNo())) {
                operationTO.setUnIdNo(opsCommand.getUnIdNo());
            }
            System.out.println("opsCommand.getUnIdNo() = " + opsCommand.getUnIdNo().length);
            System.out.println("opsCommand.getDgHandlingCode() = " + opsCommand.getDgHandlingCode().length);
            System.out.println("opsCommand.getClassCode() = " + opsCommand.getClassCode().length);
            System.out.println("opsCommand.getPkgInstruction() = " + opsCommand.getPkgInstruction().length);
            System.out.println("opsCommand.getNetQuantity() = " + opsCommand.getNetQuantity().length);
            System.out.println("opsCommand.getNetUnits() = " + opsCommand.getNetUnits().length);
            if (opsCommand.getClassCode() != null && !"".equals(opsCommand.getClassCode())) {
                operationTO.setClassCode(opsCommand.getClassCode());
            }
            if (opsCommand.getPkgInstruction() != null && !"".equals(opsCommand.getPkgInstruction())) {
                operationTO.setPkgInstruction(opsCommand.getPkgInstruction());
            }
            if (opsCommand.getPkgGroup() != null && !"".equals(opsCommand.getPkgGroup())) {
                operationTO.setPkgGroup(opsCommand.getPkgGroup());
            }
            if (opsCommand.getNetQuantity() != null && !"".equals(opsCommand.getNetQuantity())) {
                operationTO.setNetQuantity(opsCommand.getNetQuantity());
            }
            if (opsCommand.getNetUnits() != null && !"".equals(opsCommand.getNetUnits())) {
                operationTO.setNetUnits(opsCommand.getNetUnits());
            }
            if (opsCommand.getTruckTravelKm() != null && !"".equals(opsCommand.getTruckTravelKm())) {
                operationTO.setTruckTravelKm(opsCommand.getTruckTravelKm());
            }
            if (opsCommand.getTruckTravelHour() != null && !"".equals(opsCommand.getTruckTravelHour())) {
                operationTO.setTruckTravelHour(opsCommand.getTruckTravelHour());
            }
            if (opsCommand.getTruckTravelMinute() != null && !"".equals(opsCommand.getTruckTravelMinute())) {
                operationTO.setTruckTravelMinute(opsCommand.getTruckTravelMinute());
            }
            if (opsCommand.getFleetTypeId() != null && !"".equals(opsCommand.getFleetTypeId())) {
                operationTO.setFleetTypeId(opsCommand.getFleetTypeId());
            }
            System.out.println("opsCommand.getTruckDepDate()" + opsCommand.getTruckDepDate());
            if (opsCommand.getTruckDepDate() != null && !"".equals(opsCommand.getTruckDepDate())) {
                operationTO.setTruckDepDate(opsCommand.getTruckDepDate());
            }
            System.out.println("After Truck Date");
            if (opsCommand.getTruckDestinationId()[0] != null && !"".equals(opsCommand.getTruckOriginId()[0])) {
                operationTO.setOrigin(opsCommand.getTruckOriginId()[0]);
            }
            if (opsCommand.getTruckDestinationId()[0] != null && !"".equals(opsCommand.getTruckDestinationId()[0])) {
                operationTO.setDestination(opsCommand.getTruckDestinationId()[0]);
            }
            if (opsCommand.getTruckRouteId() != null && !"".equals(opsCommand.getTruckRouteId())) {
                operationTO.setTruckRouteId(opsCommand.getTruckRouteId());
            }
            if (opsCommand.getFreightReceipt() != null && !"".equals(opsCommand.getFreightReceipt())) {
                operationTO.setFreightReceipt(opsCommand.getFreightReceipt());
            }
            if (opsCommand.getWareHouseName() != null && !"".equals(opsCommand.getWareHouseName())) {
                operationTO.setWareHouseName(opsCommand.getWareHouseName());
            }
            if (opsCommand.getWareHouseLocation() != null && !"".equals(opsCommand.getWareHouseLocation())) {
                operationTO.setWareHouseLocation(opsCommand.getWareHouseLocation());
            }
            if (opsCommand.getShipmentAcceptanceDate() != null && !"".equals(opsCommand.getShipmentAcceptanceDate())) {
                operationTO.setShipmentAcceptanceDate(opsCommand.getShipmentAcceptanceDate());
            }
            if (opsCommand.getShipmentAccpHour() != null && !"".equals(opsCommand.getShipmentAccpHour())) {
                operationTO.setShipmentAccpHour(opsCommand.getShipmentAccpHour());
            }
            if (opsCommand.getShipmentAccpMinute() != null && !"".equals(opsCommand.getShipmentAccpMinute())) {
                operationTO.setShipmentAccpMinute(opsCommand.getShipmentAccpMinute());
            }
            if (opsCommand.getCurrencyType() != null && !"".equals(opsCommand.getCurrencyType())) {
                operationTO.setCurrencyType(opsCommand.getCurrencyType());
            }
            if (opsCommand.getRateMode() != null && !"".equals(opsCommand.getRateMode())) {
                operationTO.setRateMode(opsCommand.getRateMode());
            }
            if (opsCommand.getBookingReferenceRemarks() != null && !"".equals(opsCommand.getBookingReferenceRemarks())) {
                operationTO.setBookingReferenceRemarks(opsCommand.getBookingReferenceRemarks());
            }
            if (opsCommand.getProductRate() != null && !"".equals(opsCommand.getProductRate())) {
                operationTO.setProductRate(opsCommand.getProductRate());
            }
            if (opsCommand.getVehicleNo() != null && !"".equals(opsCommand.getVehicleNo())) {
                operationTO.setVehicleNos(opsCommand.getVehicleNo());
            }
            if (opsCommand.getVehicleId() != null && !"".equals(opsCommand.getVehicleId())) {
                operationTO.setVehicleIds(opsCommand.getVehicleId());
            }
            if (opsCommand.getContract() != null && !"".equals(opsCommand.getContract())) {
                operationTO.setContract(opsCommand.getContract());
            }
            if (opsCommand.getRateMode() != null && !"".equals(opsCommand.getRateMode())) {
                operationTO.setRateMode(opsCommand.getRateMode());
            }
            if (opsCommand.getRateMode() != null && !"".equals(opsCommand.getRateMode())) {
                operationTO.setRateMode(opsCommand.getRateMode());
            }
            if (opsCommand.getCurrencyType() != null && !"".equals(opsCommand.getCurrencyType())) {
                operationTO.setCurrencyType(opsCommand.getCurrencyType());
            }
            System.out.println("opsCommand.getPerKgRate" + opsCommand.getPerKgRate());
            if (opsCommand.getPerKgRate() != null && !"".equals(opsCommand.getPerKgRate())) {
                operationTO.setPerKgRate(opsCommand.getPerKgRate());
            }

            System.out.println("operationTO.setRateMode() " + operationTO.getRateMode() + " RateMode1 " + operationTO.getRateMode1()
                    + " perKgRate " + operationTO.getPerKgRate() + " RateValue " + operationTO.getRateValue() + " contract " + operationTO.getContract());

            String[] contractId1 = request.getParameterValues("contract");
            String[] rateMode1 = request.getParameterValues("rateMode");
//            System.out.println("contractId1 " + contractId1[0] + " rateMode1 " + rateMode1[0] + " perKgRate " + request.getParameter("perKgRate") + " rateValue " + request.getParameter("rateValue") + "rateValue1 " + request.getParameter("rateValue1"));
            if ("1".equals(contractId1[0])) {
//                if (request.getParameter("rateValue") != null && !"".equals(request.getParameter("rateValue"))) {
//                    operationTO.setRateValue(request.getParameter("rateValue"));
//                }
                if (request.getParameter("rateMode") != null && !"".equals(request.getParameter("rateMode"))) {
                    if ("1".equals(rateMode1[0])) {
                        operationTO.setRateMode(rateMode1[0]);
                        operationTO.setPerKgRate(request.getParameter("perKgAutoRate"));
                        operationTO.setRateValue(request.getParameter("rateValue"));
                    } else {
                        operationTO.setRateMode(rateMode1[0]);
                        operationTO.setRateValue(request.getParameter("rateValue"));
                    }
                }
            } else if ("2".equals(contractId1[0])) {
                if (request.getParameter("rateMode1") != null && !"".equals(request.getParameter("rateMode1"))) {
                    if ("3".equals(rateMode1[0])) {
                        operationTO.setRateMode(rateMode1[0]);
                        operationTO.setRateValue(request.getParameter("perKgRate"));
                    } else {
                        operationTO.setRateMode(rateMode1[0]);
                        operationTO.setRateValue(request.getParameter("rateValue1"));
                    }
                }
            }
//
//            if ("1".equals(contractId1[0])) {
//                if (request.getParameter("rateValue") != null && !"".equals(request.getParameter("rateValue"))) {
//                    operationTO.setRateValue(request.getParameter("rateValue"));
//                }
//            } else if ("2".equals(contractId1[0])) {
//                if (request.getParameter("rateMode1") != null && !"".equals(request.getParameter("rateMode1"))) {
//                    operationTO.setRateMode(request.getParameter("rateMode1"));
//                    operationTO.setRateValue(request.getParameter("perKgRate"));
//                }
//                if (request.getParameter("rateMode") != null && !"".equals(request.getParameter("rateMode"))) {
//                    operationTO.setRateMode(request.getParameter("rateMode"));
//                    operationTO.setRateValue(request.getParameter("rateValue1"));
//                }
//
//            }
            if (opsCommand.getCommodity() != null && !"".equals(opsCommand.getCommodity())) {
                operationTO.setCommodity(opsCommand.getCommodity());
            }
            if (opsCommand.getConsignmentArticleId() != null && !"".equals(opsCommand.getConsignmentArticleId())) {
                operationTO.setConsArticleId(opsCommand.getConsignmentArticleId());
            }

            int rank = Integer.parseInt(request.getParameter("customerRank"));
            int cnoteCount = Integer.parseInt(request.getParameter("approvalStatus"));
            operationTO.setCnoteCount(cnoteCount);
//            if (cnoteCount > 0) {
//                int cnotestatus = operationBP.updateConteCount(operationTO, userId);
//            }
            double creditLimit = 0.0f;
            creditLimit = Double.parseDouble(request.getParameter("creditLimit"));
            double outStanding = 0.0f;
            outStanding = Double.parseDouble(request.getParameter("outStanding"));
            String outStandingDate = request.getParameter("outStandingDate");
            operationTO.setCreditLimitAmount(creditLimit);
            operationTO.setOutStandingDate(outStandingDate);
            DecimalFormat df = new DecimalFormat("########.##");

            System.out.println("operationTO.getStatus() = " + operationTO.getStatus());
            //operationTO.setConsignmentOrderId(String.valueOf(insertStatus));
            int insertArticle = 0;
            int insertDangerousArticle = 0;
//            if (insertStatus > 0) {
//            insertArticle = operationBP.insertEditConsignmentArticle(operationTO, userId);
            //insertDangerousArticle = operationBP.insertConsignmentDangerousGoods(operationTO, userId);

            int insertMultiplePoints = 0;
            if (opsCommand.getTruckOriginId() != null && !"".equals(opsCommand.getTruckOriginId())) {
                operationTO.setPointId(opsCommand.getTruckOriginId());
            }
            if (opsCommand.getTruckDestinationId() != null && !"".equals(opsCommand.getTruckDestinationId())) {
                operationTO.setTruckDestinationId(opsCommand.getTruckDestinationId());
            }
            if (opsCommand.getTruckRouteId() != null && !"".equals(opsCommand.getTruckRouteId())) {
                operationTO.setTruckRouteId(opsCommand.getTruckRouteId());
            }
            System.out.println("opsCommand.getTruckRouteId() = " + opsCommand.getTruckRouteId());
            if (opsCommand.getPointName() != null && !"".equals(opsCommand.getPointName())) {
                operationTO.setPointName(opsCommand.getPointName());
            }
            if (opsCommand.getPointType() != null && !"".equals(opsCommand.getPointType())) {
                operationTO.setPointType(opsCommand.getPointType());
            }
            if (opsCommand.getPointOrder() != null && !"".equals(opsCommand.getPointOrder())) {
                operationTO.setOrder(opsCommand.getPointOrder());
            }
            if (opsCommand.getPointAddresss() != null && !"".equals(opsCommand.getPointAddresss())) {
                operationTO.setPointAddresss(opsCommand.getPointAddresss());
            }
            if (opsCommand.getMultiplePointRouteId() != null && !"".equals(opsCommand.getMultiplePointRouteId())) {
                operationTO.setMultiplePointRouteId(opsCommand.getMultiplePointRouteId());
            }
            if (opsCommand.getTruckDepDate() != null && !"".equals(opsCommand.getTruckDepDate())) {
                operationTO.setPointPlanDate(opsCommand.getTruckDepDate());
            }
            if (opsCommand.getPointPlanHour() != null && !"".equals(opsCommand.getPointPlanHour())) {
                operationTO.setPointPlanHour(opsCommand.getPointPlanHour());
            }
            if (opsCommand.getPointPlanMinute() != null && !"".equals(opsCommand.getPointPlanMinute())) {
                operationTO.setPointPlanMinute(opsCommand.getPointPlanMinute());
            }
            if (opsCommand.getFleetTypeId() != null && !"".equals(opsCommand.getFleetTypeId())) {
                operationTO.setFleetTypeId(opsCommand.getFleetTypeId());
            }
            if (opsCommand.getTruckTravelKm() != null && !"".equals(opsCommand.getTruckTravelKm())) {
                operationTO.setTruckTravelKm(opsCommand.getTruckTravelKm());
            }
            if (opsCommand.getTruckTravelHour() != null && !"".equals(opsCommand.getTruckTravelHour())) {
                operationTO.setTruckTravelHour(opsCommand.getTruckTravelHour());
            }
            if (opsCommand.getTruckTravelMinute() != null && !"".equals(opsCommand.getTruckTravelMinute())) {
                operationTO.setTruckTravelMinute(opsCommand.getTruckTravelMinute());
            }
            if (opsCommand.getVehicleNo() != null && !"".equals(opsCommand.getVehicleNo())) {
                operationTO.setVehicleNos(opsCommand.getVehicleNo());
            }
            if (opsCommand.getVehicleId() != null && !"".equals(opsCommand.getVehicleId())) {
                operationTO.setVehicleIds(opsCommand.getVehicleId());
            }
            if (opsCommand.getVehicleId() != null && !"".equals(opsCommand.getVehicleId())) {
                operationTO.setVehicleIds(opsCommand.getVehicleId());
            }
            if (opsCommand.getConsignmentRouteCourseId() != null && !"".equals(opsCommand.getConsignmentRouteCourseId())) {
                operationTO.setConsRouteCourseId(opsCommand.getConsignmentRouteCourseId());
            }
            if (opsCommand.getVehicleRegNo() != null && !"".equals(opsCommand.getVehicleRegNo())) {
                operationTO.setVehicleRegNo(opsCommand.getVehicleRegNo());
            }
            if (opsCommand.getUsedCapacity() != null && !"".equals(opsCommand.getUsedCapacity())) {
                operationTO.setUsedCapacity(opsCommand.getUsedCapacity());
                operationTO.setReceivedPackages(request.getParameterValues("receivedPackages"));
            }
            if (opsCommand.getUsedVol() != null && !"".equals(opsCommand.getUsedVol())) {
                operationTO.setUsedVol(opsCommand.getUsedVol());
            }
//            insertMultiplePoints = operationBP.insertEditMultiplePoints(operationTO, userId);

            insertStatus = operationBP.insertEditConsignmentNote(operationTO, userId);

            //}
            operationTO.setConsignmentOrderId(consignmentOrderId);

            ArrayList customerTypeList = new ArrayList();
            customerTypeList = operationBP.getCustomerTypeList(operationTO);
            request.setAttribute("customerTypeList", customerTypeList);

            ArrayList consignmentPoint = new ArrayList();
            consignmentPoint = operationBP.getConsignmentPoints(operationTO);
            System.out.println("consignmentPoint " + consignmentPoint.size());
            request.setAttribute("consignmentPoint", consignmentPoint);

            ArrayList consignmentArticles = new ArrayList();
            consignmentArticles = operationBP.getConsignmentArticles(operationTO);
            request.setAttribute("consignmentArticles", consignmentArticles);

            ArrayList productCategoryList = new ArrayList();
            ArrayList consignmentList = new ArrayList();
            productCategoryList = operationBP.getProductCategoryList();
            request.setAttribute("productCategoryList", productCategoryList);

            OperationTO oprTO = null;
            consignmentList = operationBP.viewConsignmentList(operationTO);
            String contractId = "";
            String vehicleTypeId = "";
            if (consignmentList.size() > 0) {
                request.setAttribute("consignmentList", consignmentList);
                Iterator itr = consignmentList.iterator();
                while (itr.hasNext()) {
                    oprTO = (OperationTO) itr.next();
                    request.setAttribute("consignmentOrderId", oprTO.getConsignmentOrderId());
                    request.setAttribute("consignmentNoteNo", oprTO.getConsignmentNoteNo());
                    request.setAttribute("consignmentOrderDate", oprTO.getConsignmentOrderDate());
                    request.setAttribute("consignmentRefNo", oprTO.getConsignmentRefNo());
                    request.setAttribute("orderReferenceRemarks", oprTO.getConsignmentOrderRefRemarks());
                    request.setAttribute("customerTypeId", oprTO.getCustomerTypeId());
                    request.setAttribute("customerTypeName", oprTO.getCustomerTypeName());
                    request.setAttribute("productCategory", oprTO.getProductCategory());
                    request.setAttribute("productCategoryName", oprTO.getProductCategoryName());
                    request.setAttribute("productCategoryId", oprTO.getProductCategoryId());
                    request.setAttribute("entryType", oprTO.getEntryType());
                    request.setAttribute("customerId", oprTO.getCustomerId());
                    request.setAttribute("customerName", oprTO.getCustomerName());
                    request.setAttribute("customerCode", oprTO.getCustomerCode());
                    request.setAttribute("customerAddress", oprTO.getCustomerAddress());
                    request.setAttribute("customerPincode", oprTO.getCustomerPincode());
                    request.setAttribute("customerMobile", oprTO.getCustomerMobile());
                    request.setAttribute("customerPhone", oprTO.getCustomerPhone());
                    request.setAttribute("customerEmail", oprTO.getCustomerEmail());
                    request.setAttribute("customerBillingType", oprTO.getCustomerBillingType());
                    request.setAttribute("billingTypeName", oprTO.getBillingTypeName());
                    request.setAttribute("customerContractId", oprTO.getCustomerContractId());
                    request.setAttribute("contractNo", oprTO.getContractNo());
                    request.setAttribute("contractFrom", oprTO.getContractFrom());
                    request.setAttribute("contractTo", oprTO.getContractTo());
                    request.setAttribute("routeContractId", oprTO.getRouteContractId());
                    request.setAttribute("routeId", oprTO.getRouteId());
                    request.setAttribute("contractRateId", oprTO.getContractRateId());
                    request.setAttribute("consigmentOrigin", oprTO.getConsigmentOrigin());
                    request.setAttribute("consigmentDestination", oprTO.getConsigmentDestination());
                    request.setAttribute("consigmentInstruction", oprTO.getConsigmentInstruction());
                    request.setAttribute("origin", oprTO.getOrigin());
                    request.setAttribute("destination", oprTO.getDestination());
                    request.setAttribute("vehicleTypeId", oprTO.getVehicleTypeId());
                    request.setAttribute("vehicleTypeName", oprTO.getVehicleTypeName());
                    request.setAttribute("contractVehicleTypeId", oprTO.getContractVehicleTypeId());
                    request.setAttribute("actualKmVehicleTypeId", oprTO.getActualKmVehicleTypeId());
                    request.setAttribute("reeferRequired", oprTO.getReeferRequired());
                    request.setAttribute("vehicleRequiredDate", oprTO.getVehicleRequiredDate());
                    request.setAttribute("vehicleRequiredTime", oprTO.getVehicleRequiredTime());
                    request.setAttribute("vehicleInstruction", oprTO.getVehicleInstruction());
                    request.setAttribute("consignorName", oprTO.getConsignorName());
                    request.setAttribute("consignorMobile", oprTO.getConsignorMobile());
                    request.setAttribute("consignorAddress", oprTO.getConsignorAddress());
                    request.setAttribute("consigneeName", oprTO.getConsigneeName());
                    request.setAttribute("consigneeMobile", oprTO.getConsigneeMobile());
                    request.setAttribute("consigneeAddress", oprTO.getConsigneeAddress());
                    request.setAttribute("totalPackages", oprTO.getTotalPackages());
                    request.setAttribute("totalWeight", oprTO.getTotalWeight());
                    request.setAttribute("totalDistance", oprTO.getTotalDistance());
                    request.setAttribute("totalHours", oprTO.getTotalHours());
                    request.setAttribute("totalMinutes", oprTO.getTotalMinutes());
                    request.setAttribute("freightCharges", oprTO.getFreightCharges());
                    request.setAttribute("orderReferenceAwb", oprTO.getOrderReferenceAwbNo().split(" ")[0]);
                    request.setAttribute("orderReferenceAwbNo", oprTO.getOrderReferenceAwbNo().split(" ")[1]);
                    if (oprTO.getOrderReferenceAwbNo().split(" ").length > 2) {
                        request.setAttribute("orderReferenceEnd", oprTO.getOrderReferenceAwbNo().split(" ")[2]);
                    } else {
                        request.setAttribute("orderReferenceEnd", "0");
                    }

                    request.setAttribute("awbOriginId", oprTO.getAwbOriginId());
                    request.setAttribute("awbDestinationId", oprTO.getAwbDestinationId());
                    request.setAttribute("awbOrderDeliveryDate", oprTO.getAwbOrderDeliveryDate());
//                    request.setAttribute("awbDestinationRegion", oprTO.getAwbDestinationRegion());
                    request.setAttribute("awbMovementType", oprTO.getAwbMovementType());
                    request.setAttribute("awbOriginName", oprTO.getAwbOriginName());
                    request.setAttribute("awbDestinationName", oprTO.getAwbDestinationName());
                    request.setAttribute("orderDeliveryHour", oprTO.getOrderDeliveryHour().split(":")[0]);
                    request.setAttribute("orderDeliveryMinute", oprTO.getOrderDeliveryHour().split(":")[1]);
                    request.setAttribute("awbOriginRegion", oprTO.getAwbOriginRegion());
                    request.setAttribute("awbDestinationRegion", oprTO.getAwbDestinationRegion());
                    request.setAttribute("productRate", oprTO.getProductRate());
                    request.setAttribute("commodity", oprTO.getCommodity());
                    request.setAttribute("chargeAbleWeigth", oprTO.getChargeAbleWeigth());
                    System.out.println("chargeAbleWeigth at controller " + oprTO.getChargeAbleWeigth());
                    request.setAttribute("totalVolume", oprTO.getTotalVolume());
                    request.setAttribute("freightReceipt", oprTO.getFreightReceipt());
                    request.setAttribute("wareHouseName", oprTO.getWareHouseName());
                    request.setAttribute("wareHouseLocation", oprTO.getWareHouseLocation());
                    request.setAttribute("shipmentAccpHour", oprTO.getShipmentAccpHour().split(":")[0]);
                    request.setAttribute("shipmentAccpMinute", oprTO.getShipmentAccpHour().split(":")[1]);
                    request.setAttribute("shipmentAcceptanceDate", oprTO.getShipmentAcceptanceDate());
                    request.setAttribute("contractId", oprTO.getContractId());
                    request.setAttribute("rateMode", oprTO.getRateMode());
                    request.setAttribute("rateValue", oprTO.getRateValue());
                    request.setAttribute("perKgRate", oprTO.getPerKgRate());
                    contractId = oprTO.getCustomerContractId();
                    vehicleTypeId = oprTO.getVehicleTypeId();
                }
            }
            int contract = 0;
            String firstPointId = "";
            String point1Id = "";
            String point2Id = "";
            String point3Id = "";
            String point4Id = "";
            String finalPointId = "";

            OperationTO opr1TO = null;
            if (consignmentPoint.size() > 0) {
                if ("".equals(contractId)) {
                    contract = 0;
                } else {
                    contract = Integer.parseInt(contractId);
                }
                int i = 0;
                Iterator itr1 = consignmentPoint.iterator();
                while (itr1.hasNext()) {
                    opr1TO = (OperationTO) itr1.next();
                    if (i == 0) {
                        firstPointId = opr1TO.getConsignmentPointId();
                    }
                    if (i == 1) {
                        if ((i + 1) == consignmentPoint.size()) {
                            finalPointId = opr1TO.getConsignmentPointId();
                        } else {
                            point1Id = opr1TO.getConsignmentPointId();
                        }
                    }
                    if (i == 2) {
                        if ((i + 1) == consignmentPoint.size()) {
                            finalPointId = opr1TO.getConsignmentPointId();
                        } else {
                            point2Id = opr1TO.getConsignmentPointId();
                        }
                    }
                    if (i == 3) {
                        if ((i + 1) == consignmentPoint.size()) {
                            finalPointId = opr1TO.getConsignmentPointId();
                        } else {
                            point3Id = opr1TO.getConsignmentPointId();
                        }
                    }
                    if (i == 4) {
                        if ((i + 1) == consignmentPoint.size()) {
                            finalPointId = opr1TO.getConsignmentPointId();
                        } else {
                            point4Id = opr1TO.getConsignmentPointId();
                        }
                    }
                    if (i == 5) {
                        finalPointId = opr1TO.getConsignmentPointId();
                    }
                    i++;
                }
            }
            ArrayList editVehicleTypeList = operationBP.getContractVehicleTypeForPointsList(contract, firstPointId, point1Id, point2Id, point3Id, point4Id, finalPointId);
            System.out.println("editVehicleTypeList " + editVehicleTypeList.size());
            request.setAttribute("editVehicleTypeList", editVehicleTypeList);

            ///operationTO.setContractId(Integer.parseInt(contractId));
//            operationTO.setVehicleTypeId(vehicleTypeId);
//            ArrayList vehicleTypeList = new ArrayList();
//            vehicleTypeList = operationBP.getVehicleTypeForActualKM(operationTO);
//            System.out.println("vehicleTypeList " + vehicleTypeList.size());
//            request.setAttribute("vehicleTypeList", vehicleTypeList);
            ArrayList vehicleTypeList = new ArrayList();
            vehicleTypeList = operationBP.getVehicleTypeList();
            request.setAttribute("vehicleTypeList", vehicleTypeList);
            path = "content/BrattleFoods/consignmentNoteBondedEdit1.jsp";

            if (insertStatus == 0) {
                request.setAttribute("errorMessage", "Updtae failed for AWB No: " + operationTO.getOrderReferenceAwb() + " " + operationTO.getOrderReferenceAwbNo() + " " + operationTO.getOrderReferenceEnd());
            } else if (insertStatus == 2) {
                request.setAttribute("errorMessage", "AWB No modify failed, because this AWB No: " + operationTO.getOrderReferenceAwb() + " " + operationTO.getOrderReferenceAwbNo() + " " + operationTO.getOrderReferenceEnd());
            } else if (insertStatus == 3) {
                request.setAttribute("errorMessage", "Booking failed, because your choosen truck used by another booking");
            } else if (insertStatus == 1) {
                request.setAttribute("successMessage", "Updated SuccessFully for AWB No: " + operationTO.getOrderReferenceAwb() + " " + operationTO.getOrderReferenceAwbNo() + " " + operationTO.getOrderReferenceEnd());
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView editCustomerContractDetails(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OperationTO operationTO = new OperationTO();
        OpsTO opsTO = new OpsTO();
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  Route Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList routeList = new ArrayList();
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            System.out.println(dateFormat.format(date));
            request.setAttribute("curDate", dateFormat.format(date));
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/contract/editCustomerContractDetails.jsp";
            String customerId = "";
            String customerCode = "";
            String customerName = "";
            String billingTypeId = "";
            customerId = request.getParameter("custId");
            customerCode = request.getParameter("custCode");
            customerName = request.getParameter("custName");
            billingTypeId = request.getParameter("billingTypeId");
            request.setAttribute("customerId", customerId);
            request.setAttribute("customerCode", customerCode);
            request.setAttribute("customerName", customerName);
            request.setAttribute("billingTypeId", billingTypeId);

            opsTO.setCustomerId(customerId);
            ArrayList contractDetails = new ArrayList();
            contractDetails = opsBP.getCustomerContractDetails(opsTO);
            request.setAttribute("contractDetails", contractDetails);

            ArrayList contractRouteDetails = new ArrayList();
            contractRouteDetails = opsBP.getContractRouteDetails(opsTO);
            request.setAttribute("contractRouteDetails", contractRouteDetails);

            ArrayList contractWeightDetails = new ArrayList();
            contractWeightDetails = opsBP.getContractWeightRouteDetails(opsTO);
            request.setAttribute("contractWeightDetails", contractWeightDetails);

            ArrayList customerTypeList = new ArrayList();
            customerTypeList = operationBP.getCustomerTypeList(operationTO);
            request.setAttribute("customerTypeList", customerTypeList);

            ArrayList vehicleTypeList = new ArrayList();
            vehicleTypeList = operationBP.getVehicleTypeList();
            request.setAttribute("vehicleTypeList", vehicleTypeList);

            ArrayList billingTypeList = new ArrayList();
            billingTypeList = operationBP.getBillingTypeList();
            request.setAttribute("billingTypeList", billingTypeList);
            ArrayList productCategoryList = new ArrayList();
            productCategoryList = operationBP.getProductCategoryList();
            request.setAttribute("productCategoryList", productCategoryList);
        } catch (FPRuntimeException exception) {
            exception.printStackTrace();
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveEditCustomerContractDetails(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OperationTO operationTO = new OperationTO();
        OpsTO opsTO = new OpsTO();
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  Route Details";
        request.setAttribute("pageTitle", pageTitle);
        int userId = (Integer) session.getAttribute("userId");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList routeList = new ArrayList();
        int status = 0;
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            System.out.println(dateFormat.format(date));
            request.setAttribute("curDate", dateFormat.format(date));
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //            path = "content/contract/contractPointToPointWeight.jsp";

            String contractId[] = request.getParameterValues("contractId");
            String rateContractId[] = request.getParameterValues("rateContractId");
            String routeContractId[] = request.getParameterValues("routeContractId");
            String rateWithReeferTruck[] = request.getParameterValues("rateWithReeferTruck");
            String rateWithOutReeferTruck[] = request.getParameterValues("rateWithOutReeferTruck");
            String activeIndRate[] = request.getParameterValues("activeIndRate");
            String contractIds = request.getParameter("contractId");

            if (rateContractId != null && !"".equals(rateContractId)) {
                status = opsBP.updateContractFullTruck(contractId, rateContractId, routeContractId, rateWithReeferTruck, rateWithOutReeferTruck, activeIndRate);
            }
            String weightContractId[] = request.getParameterValues("weightContractId");
            String weightRateContractId[] = request.getParameterValues("weightRateContractId");
            String weightRouteContractId[] = request.getParameterValues("weightRouteContractId");
            String rateWithReeferWeight[] = request.getParameterValues("rateWithReeferWeight");
            String rateWithOutReeferWeight[] = request.getParameterValues("rateWithOutReeferWeight");
            String activeIndWeight[] = request.getParameterValues("activeIndWeight");
            String fromKgWeight[] = request.getParameterValues("fromKgWeight");
            String toKgWeight[] = request.getParameterValues("toKgWeight");

            if (weightRateContractId != null && !"".equals(weightRateContractId)) {
                status = opsBP.updateContractWeightBreak(weightContractId, weightRateContractId, weightRouteContractId, fromKgWeight, toKgWeight, rateWithReeferWeight, rateWithOutReeferWeight, activeIndWeight);
            }

            opsTO.setUserId(userId);
            if (opsCommand.getCustomerId() != null && !"".equals(opsCommand.getCustomerId())) {
                opsTO.setCustomerId(opsCommand.getCustomerId());
            }
            if (opsCommand.getCustomerCode() != null && !"".equals(opsCommand.getCustomerCode())) {
                opsTO.setCustomerCode(opsCommand.getCustomerCode());
            }
            if (opsCommand.getContractFrom() != null && !"".equals(opsCommand.getContractFrom())) {
                opsTO.setContractFrom(opsCommand.getContractFrom());
            }
            if (opsCommand.getContractTo() != null && !"".equals(opsCommand.getContractTo())) {
                opsTO.setContractTo(opsCommand.getContractTo());
            }
            if (opsCommand.getContractNo() != null && !"".equals(opsCommand.getContractNo())) {
                opsTO.setContractNo(opsCommand.getContractNo());
            }
            if (opsCommand.getBillingTypeId() != null && !"".equals(opsCommand.getBillingTypeId())) {
                opsTO.setBillingTypeId(opsCommand.getBillingTypeId());
            }

            //Full Truck Start
            if (opsCommand.getOriginIdFullTruck() != null && !"".equals(opsCommand.getOriginIdFullTruck())) {
                opsTO.setOriginIdFullTruck(opsCommand.getOriginIdFullTruck());
            }
            if (opsCommand.getOriginNameFullTruck() != null && !"".equals(opsCommand.getOriginNameFullTruck())) {
                opsTO.setOriginNameFullTruck(opsCommand.getOriginNameFullTruck());
            }
            if (opsCommand.getDestinationIdFullTruck() != null && !"".equals(opsCommand.getDestinationIdFullTruck())) {
                opsTO.setDestinationIdFullTruck(opsCommand.getDestinationIdFullTruck());
            }
            if (opsCommand.getDestinationNameFullTruck() != null && !"".equals(opsCommand.getDestinationNameFullTruck())) {
                opsTO.setDestinationNameFullTruck(opsCommand.getDestinationNameFullTruck());
            }
            if (opsCommand.getRouteIdFullTruck() != null && !"".equals(opsCommand.getRouteIdFullTruck())) {
                opsTO.setRouteIdFullTruck(opsCommand.getRouteIdFullTruck());
            }
            if (opsCommand.getTravelKmFullTruck() != null && !"".equals(opsCommand.getTravelKmFullTruck())) {
                opsTO.setTravelKmFullTruck(opsCommand.getTravelKmFullTruck());
            }
            if (opsCommand.getTravelHourFullTruck() != null && !"".equals(opsCommand.getTravelHourFullTruck())) {
                opsTO.setTravelHourFullTruck(opsCommand.getTravelHourFullTruck());
            }
            if (opsCommand.getTravelMinuteFullTruck() != null && !"".equals(opsCommand.getTravelMinuteFullTruck())) {
                opsTO.setTravelMinuteFullTruck(opsCommand.getTravelMinuteFullTruck());
            }
            if (opsCommand.getMovementTypeIdFullTruck() != null && !"".equals(opsCommand.getMovementTypeIdFullTruck())) {
                opsTO.setMovementTypeIdFullTruck(opsCommand.getMovementTypeIdFullTruck());
            }
            if (opsCommand.getVehicleTypeIdFullTruck() != null && !"".equals(opsCommand.getVehicleTypeIdFullTruck())) {
                opsTO.setVehicleTypeIdFullTruck(opsCommand.getVehicleTypeIdFullTruck());
            }
            if (opsCommand.getRateWithReeferFullTruck() != null && !"".equals(opsCommand.getRateWithReeferFullTruck())) {
                opsTO.setRateWithReeferFullTruck(opsCommand.getRateWithReeferFullTruck());
            }
            if (opsCommand.getRateWithOutReeferFullTruck() != null && !"".equals(opsCommand.getRateWithOutReeferFullTruck())) {
                opsTO.setRateWithOutReeferFullTruck(opsCommand.getRateWithOutReeferFullTruck());
            }

            //Weight Beak Start
            if (opsCommand.getOriginIdWeightBreak() != null && !"".equals(opsCommand.getOriginIdWeightBreak())) {
                opsTO.setOriginIdWeightBreak(opsCommand.getOriginIdWeightBreak());
            }
            if (opsCommand.getOriginNameWeightBreak() != null && !"".equals(opsCommand.getOriginNameWeightBreak())) {
                opsTO.setOriginNameWeightBreak(opsCommand.getOriginNameWeightBreak());
            }
            if (opsCommand.getDestinationIdWeightBreak() != null && !"".equals(opsCommand.getDestinationIdWeightBreak())) {
                opsTO.setDestinationIdWeightBreak(opsCommand.getDestinationIdWeightBreak());
            }
            if (opsCommand.getDestinationNameWeightBreak() != null && !"".equals(opsCommand.getDestinationNameWeightBreak())) {
                opsTO.setDestinationNameWeightBreak(opsCommand.getDestinationNameWeightBreak());
            }
            if (opsCommand.getRouteIdWeightBreak() != null && !"".equals(opsCommand.getRouteIdWeightBreak())) {
                opsTO.setRouteIdWeightBreak(opsCommand.getRouteIdWeightBreak());
            }
            if (opsCommand.getTravelKmWeightBreak() != null && !"".equals(opsCommand.getTravelKmWeightBreak())) {
                opsTO.setTravelKmWeightBreak(opsCommand.getTravelKmWeightBreak());
            }
            if (opsCommand.getTravelHourWeightBreak() != null && !"".equals(opsCommand.getTravelHourWeightBreak())) {
                opsTO.setTravelHourWeightBreak(opsCommand.getTravelHourWeightBreak());
            }
            if (opsCommand.getTravelMinuteWeightBreak() != null && !"".equals(opsCommand.getTravelMinuteWeightBreak())) {
                opsTO.setTravelMinuteWeightBreak(opsCommand.getTravelMinuteWeightBreak());
            }
            if (opsCommand.getMovementTypeIdWeightBreak() != null && !"".equals(opsCommand.getMovementTypeIdWeightBreak())) {
                opsTO.setMovementTypeIdWeightBreak(opsCommand.getMovementTypeIdWeightBreak());
            }
            if (opsCommand.getFromKgWeightBreak() != null && !"".equals(opsCommand.getFromKgWeightBreak())) {
                opsTO.setFromKgWeightBreak(opsCommand.getFromKgWeightBreak());
            }
            if (opsCommand.getToKgWeightBreak() != null && !"".equals(opsCommand.getToKgWeightBreak())) {
                opsTO.setToKgWeightBreak(opsCommand.getToKgWeightBreak());
            }
            if (opsCommand.getRateWithReeferWeightBreak() != null && !"".equals(opsCommand.getRateWithReeferWeightBreak())) {
                opsTO.setRateWithReeferWeightBreak(opsCommand.getRateWithReeferWeightBreak());
            }
            if (opsCommand.getRateWithOutReeferWeightBreak() != null && !"".equals(opsCommand.getRateWithOutReeferWeightBreak())) {
                opsTO.setRateWithOutReeferWeightBreak(opsCommand.getRateWithOutReeferWeightBreak());
            }
            int insertContractDetails = opsBP.insertCustomerContract(opsTO);
            if (opsCommand.getOriginIdFullTruck() != null && !"".equals(opsCommand.getOriginIdFullTruck()) || opsCommand.getOriginIdWeightBreak() != null && !"".equals(opsCommand.getOriginIdWeightBreak())) {

                int insertContract = opsBP.updateCustomerContractDetails(contractIds, opsTO);
                if (insertContract > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Contract Updated Successfully");
                }
            }

            ArrayList customerTypeList = new ArrayList();
            customerTypeList = operationBP.getCustomerTypeList(operationTO);
            request.setAttribute("customerTypeList", customerTypeList);

            //            ArrayList vehicleTypeList = new ArrayList();
            //            vehicleTypeList = operationBP.getVehicleTypeList();
            //            request.setAttribute("vehicleTypeList", vehicleTypeList);
            //
            //            ArrayList billingTypeList = new ArrayList();
            //            billingTypeList = operationBP.getBillingTypeList();
            //            request.setAttribute("billingTypeList", billingTypeList);
            //            ArrayList productCategoryList = new ArrayList();
            //            productCategoryList = operationBP.getProductCategoryList();
            //            request.setAttribute("productCategoryList", productCategoryList);
            CustomerTO customerTO = new CustomerTO();

            String roleId = "" + (Integer) session.getAttribute("RoleId");
            customerTO.setRoleId(roleId);
            customerTO.setUserId(userId + "");
            customerTO.setCustomerType("1");

            path = "content/Customer/manageCustomer.jsp";

            ArrayList customerList = new ArrayList();
            customerList = customerBP.processCustomerLists(customerTO);
            request.setAttribute("CustomerLists", customerList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView exportConsignmentTsa(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "View Consignment TSI Details";
        menuPath = "Operation >>  View Consignment TSI Details";
        ArrayList tsaViewList = null;
        String consignmnetIds = "";
        try {

            request.setAttribute("pageTitle", pageTitle);
            int userId = (Integer) session.getAttribute("userId");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            System.out.println(dateFormat.format(date));

            consignmnetIds = request.getParameter("consignmentIds");
            String truckDepDate = request.getParameter("truckDepDate");

            tsaViewList = opsBP.getTsaViewList(consignmnetIds, "tsa");
            request.setAttribute("tsiViewList", tsaViewList);
            System.out.println("tsiViewList size is ::::" + tsaViewList.size());

            path = "content/BrattleFoods/exportTsaConsignment.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveExportConsignmentTsa(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "View Consignment TSI Details";
        menuPath = "Operation >>  View Consignment TSI Details";
        request.setAttribute("pageTitle", pageTitle);

        try {
            int userId = (Integer) session.getAttribute("userId");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            System.out.println(dateFormat.format(date));
            request.setAttribute("curDate", dateFormat.format(date));
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/BrattleFoods/exportTsaConsignment.jsp";

            opsTO.setUserId(userId);
            if (opsCommand.getFromAddress() != null && !"".equals(opsCommand.getFromAddress())) {
                opsTO.setFromAddress(opsCommand.getFromAddress());
            }
            if (opsCommand.getToAddress() != null && !"".equals(opsCommand.getToAddress())) {
                opsTO.setToAddress(opsCommand.getToAddress());
            }
            if (opsCommand.getFleetDetails() != null && !"".equals(opsCommand.getFleetDetails())) {
                opsTO.setFleetDetails(opsCommand.getFleetDetails());
            }
            if (opsCommand.getCustomsRemark() != null && !"".equals(opsCommand.getCustomsRemark())) {
                opsTO.setCustomsRemark(opsCommand.getCustomsRemark());
            }
            if (opsCommand.getConsignmentSbNo() != null && !"".equals(opsCommand.getConsignmentSbNo())) {
                opsTO.setConsignmentSbNo(opsCommand.getConsignmentSbNo());
            }
            if (opsCommand.getConsignmentOrderNos() != null && !"".equals(opsCommand.getConsignmentOrderNos())) {
                opsTO.setConsignmentOrderNos(opsCommand.getConsignmentOrderNos());
            }
            if (opsCommand.getFlightDate() != null && !"".equals(opsCommand.getFlightDate())) {
                opsTO.setFlightDate(opsCommand.getFlightDate());
            }
            if (opsCommand.getConsignmentTpNo() != null && !"".equals(opsCommand.getConsignmentTpNo())) {
                opsTO.setConsignmentTpNo(opsCommand.getConsignmentTpNo());
            }
            if (opsCommand.getConsignmentEgmNo() != null && !"".equals(opsCommand.getConsignmentEgmNo())) {
                opsTO.setConsignmentEgmNo(opsCommand.getConsignmentEgmNo());
            }
            if (opsCommand.getTotalTsaGoods() != null && !"".equals(opsCommand.getTotalTsaGoods())) {
                opsTO.setTotalTsaGoods(opsCommand.getTotalTsaGoods());
            }
            if (opsCommand.getShipmentType() != null && !"".equals(opsCommand.getShipmentType())) {
                opsTO.setShipmentType(opsCommand.getShipmentType());
            }
            if (opsCommand.getConsignmentEfoNo() != null && !"".equals(opsCommand.getConsignmentEfoNo())) {
                opsTO.setConsignmentEfoNo(opsCommand.getConsignmentEfoNo());
            }
            if (opsCommand.getTranshipmentNo() != null && !"".equals(opsCommand.getTranshipmentNo())) {
                opsTO.setTranshipmentNo(opsCommand.getTranshipmentNo());
            }
//                opsTO.setReceivedPackages(request.getParameter("receivedPackages"));

            String[] receivedPackages = request.getParameterValues("recievedPackages");
            String[] receivedWeight = request.getParameterValues("receivedWeight");
            String[] routeContractId = request.getParameterValues("routeContractId");
            String[] igmNo = request.getParameterValues("igmNo");
            String[] hawbNo = request.getParameterValues("hawbNo");
            String[] flightNumber = request.getParameterValues("flightNumber");
            System.out.println("receivedPackages" + receivedPackages.length);
            int headerId = opsBP.insertConsignmentCustomsCheck(opsTO, receivedPackages, receivedWeight, routeContractId, igmNo, flightNumber, hawbNo);
            ArrayList customsCheckViewList = new ArrayList();
            opsTO.setCustomsCheckId(String.valueOf(headerId));

            customsCheckViewList = opsBP.getCustomsCheckViewList(opsTO);
            System.out.println("customsCheckViewList size :::" + customsCheckViewList.size());
            request.setAttribute("tsiViewList", customsCheckViewList);

            ArrayList consignmentCustomsCheck = new ArrayList();
            consignmentCustomsCheck = opsBP.getConsignmentCustomsCheck(opsTO);
            System.out.println("consignmentCustomsCheck  size :::" + consignmentCustomsCheck.size());
            if (consignmentCustomsCheck.size() > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Records Saved SuccessFully...");
                OpsTO opsTo = (OpsTO) consignmentCustomsCheck.get(0);
                request.setAttribute("fromAddress", opsTo.getFromAddress());
                request.setAttribute("toAddress", opsTo.getToAddress());
                request.setAttribute("fleetDetails", opsTo.getFleetDetails());
                request.setAttribute("customsRemark", opsTo.getCustomsRemark());
                request.setAttribute("flightDate", opsTo.getFlightDate());
                request.setAttribute("consignmentEgmNo", opsTo.getConsignmentEgmNo());
                request.setAttribute("consignmentTpNo", opsTo.getConsignmentTpNo());
                request.setAttribute("fleetCode", opsTo.getFleetCode());
                System.out.println("fleetCode" + opsTo.getFleetCode());
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView consignmentNoteManifest(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "View Consignment ManiFest";
        menuPath = "Operation >>  View Consignment ManiFest";
        ArrayList tsaViewList = null;
        String consignmnetIds = "";
        try {

            request.setAttribute("pageTitle", pageTitle);
            int userId = (Integer) session.getAttribute("userId");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            System.out.println(dateFormat.format(date));

            consignmnetIds = request.getParameter("consignmentIds");

            tsaViewList = opsBP.getTsaViewList(consignmnetIds, "cargo");
            request.setAttribute("tsiViewList", tsaViewList);
            System.out.println("tsiViewList size is ::::" + tsaViewList.size());

            path = "content/BrattleFoods/consignmentNoteManifest.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveConsignmentNoteManifest(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "View Consignment Manifest Details";
        menuPath = "Operation >>  View Consignment  Manifest Details";
        request.setAttribute("pageTitle", pageTitle);

        try {
            int userId = (Integer) session.getAttribute("userId");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            System.out.println(dateFormat.format(date));
            request.setAttribute("curDate", dateFormat.format(date));
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/BrattleFoods/consignmentNoteManifest.jsp";

            opsTO.setUserId(userId);
            if (opsCommand.getOperatorName() != null && !"".equals(opsCommand.getOperatorName())) {
                opsTO.setOperatorName(opsCommand.getOperatorName());
            }
            if (opsCommand.getFlightNo() != null && !"".equals(opsCommand.getFlightNo())) {
                opsTO.setFlightNo(opsCommand.getFlightNo());
            }
            if (opsCommand.getManifestDate() != null && !"".equals(opsCommand.getManifestDate())) {
                opsTO.setManifestDate(opsCommand.getManifestDate());
            }
            if (opsCommand.getLodingPoint() != null && !"".equals(opsCommand.getLodingPoint())) {
                opsTO.setLodingPoint(opsCommand.getLodingPoint());
            }
            if (opsCommand.getUnLodingPoint() != null && !"".equals(opsCommand.getUnLodingPoint())) {
                opsTO.setUnLodingPoint(opsCommand.getUnLodingPoint());
            }
            if (opsCommand.getConsignmentOrderNos() != null && !"".equals(opsCommand.getConsignmentOrderNos())) {
                opsTO.setConsignmentOrderNos(opsCommand.getConsignmentOrderNos());
            }
            if (opsCommand.getConsignmentNoteAtdNo() != null && !"".equals(opsCommand.getConsignmentNoteAtdNo())) {
                opsTO.setConsignmentNoteAtdNo(opsCommand.getConsignmentNoteAtdNo());
            }
            if (opsCommand.getConsignmentNoteEmno() != null && !"".equals(opsCommand.getConsignmentNoteEmno())) {
                opsTO.setConsignmentNoteEmno(opsCommand.getConsignmentNoteEmno());
            }
            if (opsCommand.getConsignmentNoteEmno() != null && !"".equals(opsCommand.getConsignmentNoteEmno())) {
                opsTO.setConsignmentNoteEmno(opsCommand.getConsignmentNoteEmno());
            }
            if (opsCommand.getShipmentType() != null && !"".equals(opsCommand.getShipmentType())) {
                opsTO.setShipmentType(opsCommand.getShipmentType());
            }
            String[] receivedPackages = request.getParameterValues("recievedPackages");
            String[] receivedWeight = request.getParameterValues("receivedWeight");
            String[] routeContractId = request.getParameterValues("routeContractId");
            int insertStatus = opsBP.insertConsignmentManifest(opsTO, receivedPackages, receivedWeight, routeContractId);

            if (insertStatus > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Records Saved SuccessFully...");
            }
            request.setAttribute("saveStatus", "1");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView tsaManifestPrintView(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "View  Print Details ";
        menuPath = "Operation >>  View Print Details";
        String printType = "";
        String tsaManifestCode = "";
        String fromDate = "";
        String toDate = "";
        try {

            request.setAttribute("pageTitle", pageTitle);
            int userId = (Integer) session.getAttribute("userId");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            System.out.println(dateFormat.format(date));
            printType = request.getParameter("printType");
            tsaManifestCode = request.getParameter("tsmanifestCode");
            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");
            request.setAttribute("tsaManifestCode", tsaManifestCode);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            String branchId = "";
            branchId = (String) session.getAttribute("BranchId");
            System.out.println("branchId::::" + branchId);
            String roleId = "" + (Integer) session.getAttribute("RoleId");

            if ("1".equals(printType)) {
                ArrayList tsaPrintList = new ArrayList();
                tsaPrintList = opsBP.getTsaPrintList(fromDate, toDate, tsaManifestCode, branchId, roleId);
                System.out.println("tsaPrintList size ::::" + tsaPrintList.size());
                if (tsaPrintList.size() > 0) {
                    request.setAttribute("tsaPrintList", tsaPrintList);
                }
            } else if ("2".equals(printType)) {
                ArrayList manifestPrintList = new ArrayList();
                manifestPrintList = opsBP.getManifestPrintList(fromDate, toDate, tsaManifestCode, branchId, roleId);
                System.out.println("manifestPrintList size ::::" + manifestPrintList.size());
                if (manifestPrintList.size() > 0) {
                    request.setAttribute("manifestPrintList", manifestPrintList);
                }
            }
            request.setAttribute("printType", printType);

            path = "content/BrattleFoods/tsaManifestPrint.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView getTsaPrintDetails(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "View  Print Details ";
        menuPath = "Operation >>  View Print Details";
        String customsId = "";
        try {

            request.setAttribute("pageTitle", pageTitle);
            int userId = (Integer) session.getAttribute("userId");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            String branchId = "";
            branchId = (String) session.getAttribute("BranchId");
            System.out.println("branchId::::" + branchId);

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            System.out.println(dateFormat.format(date));
            customsId = request.getParameter("customsId");

            opsTO.setCustomsCheckId(customsId);
            opsTO.setBranchId(branchId);
            ArrayList customsCheckViewList = new ArrayList();
            customsCheckViewList = opsBP.getCustomsCheckViewList(opsTO);
            System.out.println("customsCheckViewList size :::" + customsCheckViewList.size());
            request.setAttribute("tsiViewList", customsCheckViewList);

            ArrayList consignmentCustomsCheck = new ArrayList();
            consignmentCustomsCheck = opsBP.getConsignmentCustomsCheck(opsTO);
            System.out.println("consignmentCustomsCheck size :::" + consignmentCustomsCheck.size());
            if (consignmentCustomsCheck.size() > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Records Saved SuccessFully...");
                OpsTO opsTo = (OpsTO) consignmentCustomsCheck.get(0);
                request.setAttribute("fromAddress", opsTo.getFromAddress());
                request.setAttribute("toAddress", opsTo.getToAddress());
                request.setAttribute("fleetDetails", opsTo.getFleetDetails());
                String[] fleetDetails = opsTo.getFleetDetails().trim().split(",");
                System.out.println("fleetDetails" + fleetDetails[0]);
//                    System.out.println("fleetDetails"+fleetDetails[1]);

                request.setAttribute("flightNo", fleetDetails[0]);
                request.setAttribute("fleetCo", "");
                request.setAttribute("customsRemark", opsTo.getCustomsRemark());
                request.setAttribute("flightDate", opsTo.getFlightDate());
                request.setAttribute("consignmentEgmNo", opsTo.getConsignmentEgmNo());
                request.setAttribute("consignmentTpNo", opsTo.getConsignmentTpNo());
                request.setAttribute("consignmentOrderId", opsTo.getConsignmentOrderId());
                request.setAttribute("fleetCode", opsTo.getFleetCode());
                request.setAttribute("efo_No", opsTo.getEfoNo());
                request.setAttribute("shipType", opsTo.getShipType());
                request.setAttribute("consigneeName", opsTo.getConsigneeName());
                request.setAttribute("transitCode", opsTo.getCityCode());

            }
            System.out.println("branchId" + branchId);
            if (branchId.equals("1")) {
                path = "content/BrattleFoods/tsaPrintView.jsp";
            } else if (branchId.equals("2")) {
                path = "content/BrattleFoods/tsaPrintViewCJB,CCJ,TRV.jsp";
            } else if (branchId.equals("3")) {
                path = "content/BrattleFoods/tsaPrintViewBOM.jsp";
            } else if (branchId.equals("5")) {
                path = "content/BrattleFoods/tsaPrintViewBLR.jsp";
            } else if (branchId.equals("7")) {
                path = "content/BrattleFoods/tsaPrintViewHYD.jsp";
            } else if (branchId.equals("8")) {
                path = "content/BrattleFoods/tsaPrintViewAMD.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

//getManifestPrintDetails
    public ModelAndView getManifestPrintDetails(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "View  Print Details ";
        menuPath = "Operation >>  View Print Details";
        String manifestId = "";
        try {

            request.setAttribute("pageTitle", pageTitle);
            int userId = (Integer) session.getAttribute("userId");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            System.out.println(dateFormat.format(date));

            String branchId = "";
            branchId = (String) session.getAttribute("BranchId");
            opsTO.setBranchId(branchId);
            manifestId = request.getParameter("manifestId");
            opsTO.setManifestId(manifestId);

            ArrayList manifestViewList = new ArrayList();
            manifestViewList = opsBP.getManifestViewList(opsTO);
            System.out.println("manifestViewList size :::" + manifestViewList.size());
            request.setAttribute("manifestViewList", manifestViewList);

            ArrayList consignmentManifest = new ArrayList();
            consignmentManifest = opsBP.getConsignmentManifest(opsTO);
            System.out.println("consignmentManifest size :::" + consignmentManifest.size());
            if (consignmentManifest.size() > 0) {
                OpsTO opsTo = (OpsTO) consignmentManifest.get(0);
                request.setAttribute("manifestId", opsTo.getManifestId());
                request.setAttribute("operatorName", opsTo.getOperatorName());
                request.setAttribute("flightNo", opsTo.getFlightNo());
                request.setAttribute("shipType", opsTo.getShipType());
                request.setAttribute("manifestDate", opsTo.getManifestDate());
                request.setAttribute("lodingPoint", opsTo.getLodingPoint());
                request.setAttribute("unLodingPoint", opsTo.getUnLodingPoint());
                request.setAttribute("consignmentNoteAtdNo", opsTo.getConsignmentNoteAtdNo());
                request.setAttribute("consignmentNoteEmno", opsTo.getConsignmentNoteEmno());
            }
            path = "content/BrattleFoods/manifestPrintView.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

//public ModelAndView cnoteairwayBillloadingDetails(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
//        String path = "";
//        HttpSession session = request.getSession();
//        opsCommand = command;
//        String menuPath = "";
//        OperationTO operationTO = new OperationTO();
//        OpsTO opsTO = new OpsTO();
//        String pageTitle = "View Route Details";
//        menuPath = "Operation >>  Route Details";
//        request.setAttribute("pageTitle", pageTitle);
//        int userId = (Integer) session.getAttribute("userId");
//        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        ArrayList routeList = new ArrayList();
//        try {
//            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
//            Date date = new Date();
//            System.out.println(dateFormat.format(date));
//            request.setAttribute("curDate", dateFormat.format(date));
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            //            path = "content/contract/contractPointToPointWeight.jsp";
//
//            String param = request.getParameter("param");
//            String awdDownloadStatus = request.getParameter("awdDownloadStatus");
//            int consignmentOrderId = Integer.parseInt(request.getParameter("consignmentOrderId"));
//            String orderReferenceAwb = request.getParameter("orderReferenceAwb");
//            String filename = "";
//            String loadingStatus = param;
//
//            if ("1".equals(param)) {
//// csv update the  loading data
//
//                 // CSV File From the Location Want to Read
//                filename = "C:\\Users\\madhan\\Downloads\\" + orderReferenceAwb + ".csv";
//                Path path1 = Paths.get(filename);
//                if (Files.exists(path1)) {
//                    // file exist
//                    System.out.println("filename" + filename);
//                    new ParseCSV2Bean(filename, consignmentOrderId, loadingStatus).start();
//
//                    // CSV File to the Location Want to Save
//                    String output2 = "G:\\" + orderReferenceAwb + ".csv";
//                    System.out.println("Starting to parse CSV file using opencsv");
//                    System.out.println("output2" + output2);
////            WriteCSVFile writeCSVFile = new WriteCSVFile();
////            writeCSVFile.parseUsingOpenCSV(filename, output2);
//                    try {
//                        CSVWriter writer = new CSVWriter(new FileWriter(output2));
//
//                        CSVReader reader = new CSVReader(new FileReader(filename));
//
//                        String[] row;
//                        String[] columns = new String[]{"AWB", "PARCEL", "ORIGIN", "DESTINATION", "BARCODE", "LOAD STATUS", "LOAD TIME", "TURCK NO", "CUSTOMER DOC", "UNLOAD STATUS", "UNLOAD TIME", "POD RECEIVED"};;
//                        System.out.println("reader.readNext()" + reader.readNext());
//
//                        System.out.println("columns" + columns);
//                        writer.writeNext(columns);
//                        while ((row = reader.readNext()) != null) {
//                            System.out.println("row" + row);
//
//                            writer.writeNext(row);
////                    break;
//
//                        }
//                        writer.close();
//
//                    } catch (FileNotFoundException e) {
//                        e.printStackTrace();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Loaded Status Updated Successfully");
//                }
//
//                if (Files.notExists(path1)) {
//
//                    // file is not exist
//                    System.out.println("filename" + "file is not exist");
//                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "File is not exist");
//                }
//            } else {
// // csv update the  Unloading data
//
//                // CSV File From the Location Want to Read
//                filename = "C:\\Users\\madhan\\Downloads\\" + orderReferenceAwb + ".csv";
//                Path path2 = Paths.get(filename);
//                if (Files.exists(path2)) {
//                    // file exist
//                    System.out.println("filename" + filename);
//                    new ParseCSV2Bean(filename, consignmentOrderId, loadingStatus).start();
//
//                // CSV File to the Location Want to Save
//                    String output2 = "G:\\" + orderReferenceAwb + ".csv";
//                    System.out.println("Starting to parse CSV file using opencsv");
//                    System.out.println("output2" + output2);
////            WriteCSVFile writeCSVFile = new WriteCSVFile();
////            writeCSVFile.parseUsingOpenCSV(filename, output2);
//                    try {
//                        CSVWriter writer1 = new CSVWriter(new FileWriter(output2));
//
//                        CSVReader reader1 = new CSVReader(new FileReader(filename));
//
//                        String[] row1;
//                        String[] columns1 = new String[]{"AWB", "PARCEL", "ORIGIN", "DESTINATION", "BARCODE", "LOAD STATUS", "LOAD TIME", "TURCK NO", "CUSTOMER DOC", "UNLOAD STATUS", "UNLOAD TIME", "POD RECEIVED"};;
//                        System.out.println("reader.readNext()" + reader1.readNext());
//
//                        System.out.println("columns" + columns1);
//                        writer1.writeNext(columns1);
//                        while ((row1 = reader1.readNext()) != null) {
//                            System.out.println("row" + row1);
//                            writer1.writeNext(row1);
////                    break;
//
//                        }
//                        writer1.close();
//
//                    } catch (FileNotFoundException e) {
//                        e.printStackTrace();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    System.out.println("filename" + filename);
//                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Unload Status Updated Successfully");
//                }
//
//                if (Files.notExists(path2)) {
//                    // file is not exist
//                    System.out.println("filename" + "file is not exist");
//                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "File is not exist");
//                }
//
//            }
//
//            String roleId = "" + (Integer) session.getAttribute("RoleId");
////            int userId = (Integer) session.getAttribute("userId");
//            operationTO.setRoleId(roleId);
//            operationTO.setUserId(userId);
//            String orderReference = request.getParameter("orderReference");
//            String customerId = request.getParameter("customerId");
//            operationTO.setCustomerId(customerId);
//            ArrayList consignmentList = new ArrayList();
//            consignmentList = operationBP.getConsignmentViewList(operationTO);
//            System.out.println("consignmentList " + consignmentList.size());
//            request.setAttribute("consignmentList", consignmentList);
//
//            ArrayList statusDetails = new ArrayList();
//            statusDetails = operationBP.getTripStatus();
//            request.setAttribute("statusDetails", statusDetails);
//            request.setAttribute("orderReference", orderReference);
//            path = "content/trip/viewConsignmentNote.jsp";
//
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }
//createCsvConsignmentNote
    public void createCsvConsignmentNote(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {
        OperationTO operationTO = null;
        try {
            operationTO = new OperationTO();
            String consignmentOrder = request.getParameter("consignmentOrder");
            operationTO.setConsignmentOrder(consignmentOrder);
            String orderReference = request.getParameter("orderReference");
            ArrayList consignmentCsvList = new ArrayList();
            ArrayList cnAirBillBarcodeList = new ArrayList();
            String fileName = operationBP.getCnAirBillBarcodeList(operationTO);

            response.setContentType("text/html");
            PrintWriter printout = response.getWriter();
            response.setContentType("APPLICATION/OCTET-STREAM");
            response.setHeader("Content-Disposition", "attachment; filename=AWBFiles.csv");
            FileInputStream fileInputStream = new FileInputStream(fileName);
            int i;
            while ((i = fileInputStream.read()) != -1) {
                printout.write(i);
            }
            fileInputStream.close();
            printout.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
        }
    }

    public void importLoadedCsv(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {
        OperationTO operationTO = null;
        String airWaybillNo = "";
        String srcFilePath = "";
        String fileNamePath = "";
        String fileName = "";

        try {
            operationTO = new OperationTO();
            airWaybillNo = request.getParameter("airWaybillNo");
            srcFilePath = opsBP.getSrcFilePath();
            fileNamePath = srcFilePath + "\\" + airWaybillNo.replaceAll(" ", "") + ".csv";
            System.out.println("fileNamePath" + fileNamePath);
            response.setContentType("text/html");
            PrintWriter printout = response.getWriter();
            response.setContentType("APPLICATION/OCTET-STREAM");
            fileName = airWaybillNo.replaceAll(" ", "") + ".csv";
            System.out.println("fileName:::::" + fileName);

            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            //response.setHeader("Content-Disposition", "attachment; filename=AWBFiles.csv");
            try {
                FileInputStream fileInputStream = new FileInputStream(fileNamePath);
                int i;
                while ((i = fileInputStream.read()) != -1) {
                    printout.write(i);
                }
                fileInputStream.close();
                printout.close();
            } catch (Exception excep) {
                System.out.println("No File Found");

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
        }
    }

    public ModelAndView getTruckUsedDetails(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "View Vehicle Utilization";
        menuPath = "Operation >>  View Vehicle Utilization";
        String truckCodeId = "";

        try {

            request.setAttribute("pageTitle", pageTitle);
            int userId = (Integer) session.getAttribute("userId");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            if (request.getParameter("truckCodeId") != null) {
                truckCodeId = request.getParameter("truckCodeId");
                request.setAttribute("truckCodeId", truckCodeId);
            }

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            System.out.println(dateFormat.format(date));

            ArrayList truckNameList = new ArrayList();
            truckNameList = opsBP.getTruckNameList();
            request.setAttribute("truckNameList", truckNameList);

            ArrayList truckCapacityList = new ArrayList();
            truckCapacityList = opsBP.getTruckCapacityList(truckCodeId);
            if (truckCapacityList.size() > 0) {
                request.setAttribute("truckCapacityList", truckCapacityList);
            } else {
                request.setAttribute("errorMessage", "No Records Found");
            }

            path = "content/BrattleFoods/vehicleUtilizationReport.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
//deleteConsignmentRouteCourse

    public void deleteConsignmentRouteCourse(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {
        OperationTO operationTO = null;
        String consignmentNoteRouteId = "";
        int updateStatus = 0;
        PrintWriter printWriter = null;
        try {
            printWriter = response.getWriter();
            operationTO = new OperationTO();
            consignmentNoteRouteId = request.getParameter("consignmentRouteCourseId");
            updateStatus = opsBP.removeConsignmentNoteRoute(consignmentNoteRouteId);

            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("updateStatus", updateStatus);
            jsonArray.put(jsonObject);

            printWriter.print(jsonArray);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
        }
    }

    public ModelAndView handleTsaGeneration(HttpServletRequest request, HttpServletResponse response, OpsCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menupath = "";
        OperationTO operationTO = new OperationTO();
        String pageTitle = "TSA Generation";
        menupath = "Operation >> Booking-O/P >> TSA Generation";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menupath);

        try {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
            Date date = new Date();
            request.setAttribute("curDate", dateFormat.format(date));
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/BrattleFoods/tsageneration.jsp";
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to view TSA List --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }

        return new ModelAndView(path);
    }

    public ModelAndView handleTsaGenView(HttpServletRequest request, HttpServletResponse response, OpsCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menupath = "";
        OperationTO operationTO = new OperationTO();
        String pageTitle = "TSA Generation";
        menupath = "Operation >> Booking-O/P >> TSA Generation";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menupath);

        try {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
            Date date = new Date();
            request.setAttribute("curDate", dateFormat.format(date));
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/BrattleFoods/tsageneration.jsp";
            String loadDate = request.getParameter("loadDate");
            String loadToDate = request.getParameter("loadToDate");
            String branchId = "";
            branchId = (String) session.getAttribute("BranchId");
            System.out.println("branchId::::" + branchId);
            String roleId = "" + (Integer) session.getAttribute("RoleId");
            ArrayList tsaManifestList = new ArrayList();
            request.setAttribute("loadDate", loadDate);
            request.setAttribute("loadToDate", loadToDate);
            tsaManifestList = opsBP.getTsaList(loadDate, loadToDate, branchId, roleId);
            System.out.println("tsaManifestList size ::::" + tsaManifestList.size());
            if (tsaManifestList.size() > 0) {
                request.setAttribute("tsaManifestList", tsaManifestList);
                if (tsaManifestList.size() < 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "No Records Found");
                }
            }

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to view TSA List --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }

        return new ModelAndView(path);
    }

    public void getFleetCodeGroup(HttpServletRequest request, HttpServletResponse response, OpsCommand command) throws IOException {
        HttpSession session = request.getSession();
        opsCommand = command;
        OpsTO opsTO = new OpsTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String fleetCode = "";
            response.setContentType("text/html");
            fleetCode = request.getParameter("fleetCode");
            if (request.getParameter("fleetCode") != null && !"".equals(request.getParameter("fleetCode"))) {
                opsTO.setFleetCode(fleetCode);
            }
            userDetails = opsBP.getFleetCodeGroup(opsTO);

            System.out.println("userDetails.size() = " + userDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = userDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                opsTO = (OpsTO) itr.next();
                jsonObject.put("Id", opsTO.getFleetCodeId());
                jsonObject.put("fleetCode", opsTO.getFleetCode1());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView editNonContractOrderRateValue(HttpServletRequest request, HttpServletResponse response, OpsCommand command) throws IOException {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        OpsTO opsTO = new OpsTO();
        String menupath = "";
        String pageTitle = "TSA Generation";
        menupath = "Operation >> Booking-O/P >> TSA Generation";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menupath);

        try {

            String consignmentOrderId = request.getParameter("consignmentOrderId");
            String consignmentOrderNo = request.getParameter("consignmentOrderNo");
            request.setAttribute("consignmentOrderNo", consignmentOrderNo);
            request.setAttribute("consignmentOrderId", consignmentOrderId);
            String branchId = "";
            branchId = (String) session.getAttribute("BranchId");
            String roleId = "" + (Integer) session.getAttribute("RoleId");

            ArrayList royaltyPayList = new ArrayList();
            royaltyPayList = opsBP.getRoyaltyPayList(branchId, roleId);
            request.setAttribute("royaltyPayList", royaltyPayList);

            ArrayList getOrderTruckList = new ArrayList();
            getOrderTruckList = opsBP.getOrderTruckList(consignmentOrderId, roleId);
            request.setAttribute("getOrderTruckList", getOrderTruckList);

            ArrayList consignmentOrderRateList = new ArrayList();
            consignmentOrderRateList = opsBP.getConsignmentOrderRate(consignmentOrderId);
            Iterator itr = consignmentOrderRateList.iterator();
            while (itr.hasNext()) {
                opsTO = new OpsTO();
                opsTO = (OpsTO) itr.next();
                request.setAttribute("rateMode", opsTO.getRateMode());
                request.setAttribute("ratePerKg", opsTO.getRatePerKg());
                request.setAttribute("rateValue", opsTO.getRateValue());
                request.setAttribute("freightCharge", opsTO.getRateValue());

                request.setAttribute("tspInput", opsTO.getTspInput());
                request.setAttribute("payBillTsp", opsTO.getPayBillTsp());
                request.setAttribute("tspOriginPaidTo", opsTO.getTspOriginPaidTo());
                request.setAttribute("tspDestinationPaidTo", opsTO.getTspDestinationPaidTo());
                request.setAttribute("tspOriginAmount", opsTO.getTspOriginAmount());
                request.setAttribute("tspDestinationAmount", opsTO.getTspDestinationAmount());

                request.setAttribute("tsrInput", opsTO.getTsrInput());
                request.setAttribute("payBillTsr", opsTO.getPayBillTsr());
                request.setAttribute("tsrOriginPaidTo", opsTO.getTsrOriginPaidTo());
                request.setAttribute("tsrDestinationPaidTo", opsTO.getTsrDestinationPaidTo());
                request.setAttribute("tsrOriginPercent", opsTO.getTsrOriginPercent());
                request.setAttribute("tsrDestPercent", opsTO.getTsrDestPercent());
                request.setAttribute("tsrOriginAmount", opsTO.getTsrOriginAmount());
                request.setAttribute("tsrDestinationAmount", opsTO.getTsrDestinationAmount());

                request.setAttribute("screeningInput", opsTO.getScreeningInput());
                request.setAttribute("payBillScreen", opsTO.getPayBillScreen());
                request.setAttribute("scOriginPaidTo", opsTO.getScOriginPaidTo());
                request.setAttribute("scDestinationPaidTo", opsTO.getScDestinationPaidTo());
                request.setAttribute("scOriginAmount", opsTO.getScOriginAmount());
                request.setAttribute("scDestinationAmount", opsTO.getScDestinationAmount());

                request.setAttribute("totalChargableWeight", opsTO.getTotalChargableWeight());
                request.setAttribute("totTSPrate", opsTO.getTotTSPrate());
                request.setAttribute("totTSRrate", opsTO.getTotTSRrate());
                request.setAttribute("totSCRrate", opsTO.getTotSCRrate());
                request.setAttribute("totIncentiveAmount", opsTO.getTotIncentiveAmount());
                request.setAttribute("totCustAmount", opsTO.getTotCustAmount());
                request.setAttribute("totTTAamount", opsTO.getTotTTAamount());
                request.setAttribute("awbNo", opsTO.getAwbNo());

            }
//            String consignmentOrderRate = opsBP.getConsignmentOrderRate(consignmentOrderId);
//            String[] temp = null;
//            if (!"".equals(consignmentOrderRate) && consignmentOrderRate != null) {
//                temp = consignmentOrderRate.split("~");
//                request.setAttribute("rateValue", temp[0]);
//                request.setAttribute("ratePerKg", temp[1]);
//                request.setAttribute("totalChargableWeight", temp[2]);
//                request.setAttribute("payBillTsp", temp[3]);
//                request.setAttribute("tspPerkg", temp[4]);
//                request.setAttribute("tsrPerkg", temp[5]);
//                request.setAttribute("customerNetAmount", temp[6]);
//                request.setAttribute("rateMode", temp[7]);
//                request.setAttribute("payBillTsr", temp[8]);
//                request.setAttribute("freightCharges", temp[9]);
//                request.setAttribute("screenCharge", temp[10]);
//                request.setAttribute("incentiveCharge", temp[11]);
//                request.setAttribute("screenAmtPaid", temp[12]);
//                request.setAttribute("paidByScreenCharge", temp[13]);
//                request.setAttribute("paidByTsr", temp[14]);
//                request.setAttribute("paidByTsp", temp[15]);
//                request.setAttribute("awbNo", temp[18]);
//            }
            String statusId = request.getParameter("statusId");
            request.setAttribute("statusId", statusId);
            String tripType = request.getParameter("tripType");
            request.setAttribute("tripType", tripType);
            String vehicleId = request.getParameter("vehicleId");
            request.setAttribute("vehicleId", vehicleId);
            String tripSheetId = request.getParameter("tripSheetId");
            request.setAttribute("tripSheetId", tripSheetId);
            path = "content/BrattleFoods/editNonContractOrderRate.jsp";

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to view TSA List --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }

        return new ModelAndView(path);
    }

    public ModelAndView saveEditNonContractOrderRate(HttpServletRequest request, HttpServletResponse response, OpsCommand command) throws IOException {
//      pavi
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        OpsTO opsTO = new OpsTO();
        String menupath = "";
        String pageTitle = "TSA Generation";
        menupath = "Operation >> Booking-O/P >> TSA Generation";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menupath);
        int userId = (Integer) session.getAttribute("userId");
        try {

            String consignmentOrderId = request.getParameter("consignmentOrderId");
            String totalChargableWeight = request.getParameter("totalChargableWeight");

            String rateMode = request.getParameter("rateMode");
            String ratePerKg = request.getParameter("ratePerKg");
            String rateValue = request.getParameter("rateValue");
            String fullTruckValue = request.getParameter("fullTruckValue");
            /* tsp */
            String tspInput = request.getParameter("tspInput");
            String payBillTsp = request.getParameter("payBillTsp");
            String tspOriginPaidTo = request.getParameter("tspOriginPaidTo");
            String tspDestinationPaidTo = request.getParameter("tspDestinationPaidTo");
            String tspOriginAmount = request.getParameter("tspOriginAmount");
            String tspDestinationAmount = request.getParameter("tspDestinationAmount");
            /*tsr */
            String tsrInput = request.getParameter("tsrInput");
            String payBillTsr = request.getParameter("payBillTsr");
            String tsrOriginPaidTo = request.getParameter("tsrOriginPaidTo");
            String tsrDestinationPaidTo = request.getParameter("tsrDestinationPaidTo");
            String tsrOriginAmount = request.getParameter("tsrOriginAmount");
            String tsrDestinationAmount = request.getParameter("tsrDestinationAmount");

            String tsrOriginPercent = request.getParameter("originRoyaltyPercent");
            String tsrDestPercent = request.getParameter("destinationRoyaltyPercent");

            /*Scr */
            String screeningInput = request.getParameter("screeningInput");
            String payBillScreen = request.getParameter("payBillScreen");
            String scOriginPaidTo = request.getParameter("scOriginPaidTo");
            String scDestinationPaidTo = request.getParameter("scDestinationPaidTo");
            String scOriginAmount = request.getParameter("scOriginAmount");
            String scDestinationAmount = request.getParameter("scDestinationAmount");

            /* Net Amount */
            String totTSPrate = request.getParameter("totTSPrate");
            String totTSRrate = request.getParameter("totTSRrate");
            String totSCRrate = request.getParameter("totSCRrate");
            String totIncentiveAmount = request.getParameter("totIncentiveAmount");
            String totCustAmount = request.getParameter("customerNetAmount");
            String totTTAamount = request.getParameter("ttaNetAmount");
            opsTO.setUserId(userId);
            opsTO.setConsignmentOrderId(consignmentOrderId);
            opsTO.setTotalChargableWeight(totalChargableWeight);

            opsTO.setRateMode(rateMode);
            opsTO.setRatePerKg(ratePerKg);
            opsTO.setRateValue(rateValue);
            opsTO.setFullTruckValue(fullTruckValue);

            opsTO.setTspInput(tspInput);
            opsTO.setPayBillTsp(payBillTsp);
            opsTO.setTspOriginPaidTo(tspOriginPaidTo);
            opsTO.setTspDestinationPaidTo(tspDestinationPaidTo);
            opsTO.setTspOriginAmount(tspOriginAmount);
            opsTO.setTspDestinationAmount(tspDestinationAmount);

            opsTO.setTsrInput(tsrInput);
            opsTO.setPayBillTsr(payBillTsr);
            opsTO.setTsrOriginPaidTo(tsrOriginPaidTo);
            opsTO.setTsrDestinationPaidTo(tsrDestinationPaidTo);
            opsTO.setTsrOriginAmount(tsrOriginAmount);
            opsTO.setTsrDestinationAmount(tsrDestinationAmount);
            opsTO.setTsrOriginPercent(tsrOriginPercent);
            opsTO.setTsrDestPercent(tsrDestPercent);

            opsTO.setScreeningInput(screeningInput);
            opsTO.setPayBillScreen(payBillScreen);
            opsTO.setScOriginPaidTo(scOriginPaidTo);
            opsTO.setScDestinationPaidTo(scDestinationPaidTo);
            opsTO.setScOriginAmount(scOriginAmount);
            opsTO.setScDestinationAmount(scDestinationAmount);

            opsTO.setTotTSPrate(totTSPrate);
            opsTO.setTotTSRrate(totTSRrate);
            opsTO.setTotSCRrate(totSCRrate);
            opsTO.setTotIncentiveAmount(totIncentiveAmount);
            opsTO.setTotCustAmount(totCustAmount);
            opsTO.setTotTTAamount(totTTAamount);
            int status = opsBP.saveEditNonContractOrderRate(opsTO);

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to view TSA List --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }

        return new ModelAndView("consignmentNoteView.do");
    }

    public void saveOtlSealNo(HttpServletRequest request, HttpServletResponse response, OpsCommand command) throws IOException {
        HttpSession session = request.getSession();
        opsCommand = command;
//        OpsTO opsTO = new OpsTO();
//        JsonTO jsonTO = new JsonTO();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            response.setContentType("text/html");
            String consignmentOrderId = request.getParameter("consignmentOrderId");
            String otlNo = request.getParameter("otlNo");
            System.out.println("consignmentOrderId in ajax " + consignmentOrderId);
            int status = opsBP.saveOtlSealNo(consignmentOrderId, otlNo);

            JSONArray jsonArray = new JSONArray();
//            Iterator itr = userDetails.iterator();
//            while (itr.hasNext()) {
            JSONObject jsonObject = new JSONObject();
//                opsTO = (OpsTO) itr.next();
            jsonObject.put("Name", otlNo);
            jsonObject.put("Name", otlNo);
            System.out.println("jsonObject = " + jsonObject);
            jsonArray.put(jsonObject);
//            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView handleManageTime(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "View delay Time Master";
        menuPath = "Operation >>  View delay Time Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");

        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        try {

            ArrayList delayTimeList = new ArrayList();
            delayTimeList = opsBP.getDelayTimeList(opsTO);
            request.setAttribute("delayTimeList", delayTimeList);

            path = "content/BrattleFoods/manageTimeMaster.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewRoyaltyMaster(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "Royalty Master";
        menuPath = "Operation >>  Royalty Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            ArrayList branchList = new ArrayList();
            branchList = opsBP.getBranchList(opsTO);
            request.setAttribute("branchList", branchList);

            String branchId = "";
            String roleId = "" + (Integer) session.getAttribute("RoleId");

            ArrayList royaltyPayList = new ArrayList();
            royaltyPayList = opsBP.getRoyaltyPayList(branchId, roleId);
            request.setAttribute("royaltyPayList", royaltyPayList);

            path = "content/BrattleFoods/viewRoyaltyMaster.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewServiceTaxMaster(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "Service Tax Master";
        menuPath = "Operation >>  Service Tax Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

//            ArrayList branchList = new ArrayList();
//            branchList = opsBP.getBranchList(opsTO);
//            request.setAttribute("branchList", branchList);
            String branchId = "";
            String roleId = "" + (Integer) session.getAttribute("RoleId");

            ArrayList serviceTaxList = new ArrayList();
            serviceTaxList = opsBP.getServiceTaxList(branchId, roleId);
            request.setAttribute("serviceTaxList", serviceTaxList);

            path = "content/BrattleFoods/viewServiceTaxMaster.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView insertDelayTime(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "View delay Time Master";
        menuPath = "Master >>  View delay Time Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        int insertReason = 0;
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        try {

            if (request.getParameter("delayTimeId") != null && !"".equals(request.getParameter("delayTimeId"))) {
                opsTO.setDelayTimeId(request.getParameter("delayTimeId"));
            }
            if (request.getParameter("delayHour") != null && !"".equals(request.getParameter("delayHour"))) {
                opsTO.setDelayTime(request.getParameter("delayHour") + ":" + request.getParameter("delayMin") + ":00");
            }
            opsTO.setStatusId(request.getParameter("statusId"));

            insertReason = opsBP.insertDelayTime(opsTO, userId);
            System.out.println("insertReason = " + insertReason);

            ArrayList delayTimeList = new ArrayList();
            delayTimeList = opsBP.getDelayTimeList(opsTO);
            request.setAttribute("delayTimeList", delayTimeList);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Delay Time is Sucessfully inserted ");
            path = "content/BrattleFoods/manageTimeMaster.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveRoyaltyPayMaster(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "View delay Time Master";
        menuPath = "Master >>  View delay Time Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {

            if (request.getParameter("paidId") != null && !"".equals(request.getParameter("paidId"))) {
                opsTO.setPaidId(request.getParameter("paidId"));
            }
            if (request.getParameter("paidBy") != null && !"".equals(request.getParameter("paidBy"))) {
                opsTO.setPaidBy(request.getParameter("paidBy"));
            }
            if (request.getParameter("branchId") != null && !"".equals(request.getParameter("branchId"))) {
                opsTO.setBranchId(request.getParameter("branchId"));
            }

            int status = opsBP.saveRoyaltyPayMaster(opsTO, userId);
            System.out.println("status = " + status);

            ArrayList branchList = new ArrayList();
            branchList = opsBP.getBranchList(opsTO);
            request.setAttribute("branchList", branchList);

            String branchId = "";
            String roleId = "" + (Integer) session.getAttribute("RoleId");

            ArrayList royaltyPayList = new ArrayList();
            royaltyPayList = opsBP.getRoyaltyPayList(branchId, roleId);
            request.setAttribute("royaltyPayList", royaltyPayList);

            path = "content/BrattleFoods/viewRoyaltyMaster.jsp";

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Sucessfully inserted ");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveServiceTaxMaster(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "Service Tax Master";
        menuPath = "Master >>  Service Tax Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {

            if (request.getParameter("serviceTaxId") != null && !"".equals(request.getParameter("serviceTaxId"))) {
                opsTO.setServiceTaxId(request.getParameter("serviceTaxId"));
            }
            if (request.getParameter("validFrom") != null && !"".equals(request.getParameter("validFrom"))) {
                opsTO.setValidFrom(request.getParameter("validFrom"));
            }
            if (request.getParameter("validTo") != null && !"".equals(request.getParameter("validTo"))) {
                opsTO.setValidTo(request.getParameter("validTo"));
            }
            if (request.getParameter("serviceTaxAmount") != null && !"".equals(request.getParameter("serviceTaxAmount"))) {
                opsTO.setServiceTaxAmount(request.getParameter("serviceTaxAmount"));
            }
            if (request.getParameter("activeInd") != null && !"".equals(request.getParameter("activeInd"))) {
                opsTO.setActiveInd(request.getParameter("activeInd"));
            }

            int status = opsBP.saveServiceTaxMaster(opsTO, userId);
            System.out.println("status = " + status);

            String branchId = "";
            String roleId = "" + (Integer) session.getAttribute("RoleId");

            ArrayList serviceTaxList = new ArrayList();
            serviceTaxList = opsBP.getServiceTaxList(branchId, roleId);
            request.setAttribute("serviceTaxList", serviceTaxList);

            path = "content/BrattleFoods/viewServiceTaxMaster.jsp";

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Sucessfully inserted ");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewZoneMaster(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "View Zone Master";
        menuPath = "Master >>  View Zone Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        int status = 0;
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        try {

            ArrayList zoneList = new ArrayList();
            zoneList = opsBP.getZoneList(opsTO);
            request.setAttribute("zoneList", zoneList);

            path = "content/BrattleFoods/zoneMaster.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveZoneMaster(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "View Zone Master";
        menuPath = "Master >>  View Zone Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        int status = 0;
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        try {

            if (request.getParameter("zoneId") != null && !"".equals(request.getParameter("zoneId"))) {
                opsTO.setZoneId(request.getParameter("zoneId"));
            }
            if (request.getParameter("zoneName") != null && !"".equals(request.getParameter("zoneName"))) {
                opsTO.setZoneName(request.getParameter("zoneName"));
            }
            if (request.getParameter("activeInd") != null && !"".equals(request.getParameter("activeInd"))) {
                opsTO.setActiveInd(request.getParameter("activeInd"));
            }

            status = opsBP.saveZoneMaster(opsTO);
            System.out.println("insertReason = " + status);

            ArrayList zoneList = new ArrayList();
            zoneList = opsBP.getZoneList(opsTO);
            request.setAttribute("zoneList", zoneList);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Zone is Sucessfully inserted ");
            path = "content/BrattleFoods/zoneMaster.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleUploadZone(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        int userId = 0;
        opsCommand = command;
        String pageTitle = "Upload Zone List";
        menuPath = "Masters >> Upload Zone List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;

        ArrayList zoneList = new ArrayList();
        String zoneName = "";
        int status = 0;
        Map map = new HashMap();
        HashSet hs = new HashSet();
        try {
            Object mapValue = null;
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);
            if (isMultipart) {

                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();

                        if (!"".equals(uploadedFileName) && uploadedFileName != null) {

                            String[] splitFileName = uploadedFileName.split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath = tempServerFilePath + "\\" + fileSavedAs;
                            actualFilePath = actualServerFilePath + "\\" + uploadedFileName;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
                            String part1 = parts.replace("\\", "");

                            WorkbookSettings ws = new WorkbookSettings();
                            ws.setLocale(new Locale("en", "EN"));
                            Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                            Sheet s = workbook.getSheet(0);
                            System.out.println("rows" + userId + s.getRows());

                            for (int i = 1; i < s.getRows(); i++) {
                                OpsTO opsTO = new OpsTO();
                                zoneName = s.getCell(1, i).getContents();
                                opsTO.setZoneName(zoneName);
                                zoneList.add(opsTO);

                            }
                        }
                    }
                }
            }
            request.setAttribute("zoneList", zoneList);

            path = "content/BrattleFoods/viewZoneList.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView saveUploadZone(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        System.out.println("command TESTING = ");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println("command TESTING = ");
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        int userId = 0;
        String pageTitle = "Employee Uploading";
        menuPath = "HRMS >> Upload Employee";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        boolean isMultipart = false;
        int insertStatus = 0;
        OpsTO opsTO = new OpsTO();
        try {
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            //isMultipart = ServletFileUpload.isMultipartContent(request);
            String[] zoneName = request.getParameterValues("zoneName");
            int UserId = (Integer) session.getAttribute("userId");
            insertStatus = opsBP.saveZoneList(zoneName);

            if (insertStatus != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Employee Added Successfully");
            }

            ArrayList zoneList = new ArrayList();
            zoneList = opsBP.getZoneList(opsTO);
            request.setAttribute("zoneList", zoneList);

            path = "content/BrattleFoods/zoneMaster.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView handleUploadCity(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        int userId = 0;
        opsCommand = command;
        String pageTitle = "Upload Zone List";
        menuPath = "Masters >> Upload Zone List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;

        ArrayList cityList = new ArrayList();
        String cityName = "";
        String zoneName = "";
        String cityCode = "";
        int status = 0;
        Map map = new HashMap();
        HashSet hs = new HashSet();
        try {
            Object mapValue = null;
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);
            if (isMultipart) {

                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();

                        if (!"".equals(uploadedFileName) && uploadedFileName != null) {

                            String[] splitFileName = uploadedFileName.split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath = tempServerFilePath + "\\" + fileSavedAs;
                            actualFilePath = actualServerFilePath + "\\" + uploadedFileName;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
                            String part1 = parts.replace("\\", "");

                            WorkbookSettings ws = new WorkbookSettings();
                            ws.setLocale(new Locale("en", "EN"));
                            Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                            Sheet s = workbook.getSheet(0);
                            System.out.println("rows" + userId + s.getRows());

                            for (int i = 1; i < s.getRows(); i++) {
                                OpsTO opsTO = new OpsTO();
                                cityName = s.getCell(1, i).getContents();
                                opsTO.setCityName(cityName);
                                zoneName = s.getCell(2, i).getContents();
                                opsTO.setZoneName(zoneName);
                                cityCode = s.getCell(3, i).getContents();
                                opsTO.setCityCode(cityCode);
                                cityList.add(opsTO);

                            }
                        }
                    }
                }
            }
            request.setAttribute("cityList", cityList);

            path = "content/BrattleFoods/viewCityList.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView handleUploadBudgetWeight(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        int userId = 0;
        opsCommand = command;
        String pageTitle = "Upload Weight";
        menuPath = "Masters >> Upload Weight";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;

        ArrayList weightList = new ArrayList();
        String january = "";
        String febuary = "";
        String march = "";
        String april = "";
        String may = "";
        String june = "";
        String july = "";
        String august = "";
        String september = "";
        String october = "";
        String november = "";
        String december = "";
        String branchName = "";
        String movementType = "";
        try {
            Object mapValue = null;
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);
            if (isMultipart) {

                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();

                        if (!"".equals(uploadedFileName) && uploadedFileName != null) {

                            String[] splitFileName = uploadedFileName.split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath = tempServerFilePath + "\\" + fileSavedAs;
                            actualFilePath = actualServerFilePath + "\\" + uploadedFileName;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
                            String part1 = parts.replace("\\", "");

                            WorkbookSettings ws = new WorkbookSettings();
                            ws.setLocale(new Locale("en", "EN"));
                            Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                            Sheet s = workbook.getSheet(0);
                            System.out.println("userId " + userId + " rows " + s.getRows());

                            for (int i = 1; i < s.getRows(); i++) {
                                OpsTO opsTO = new OpsTO();
                                branchName = s.getCell(1, i).getContents();
                                opsTO.setBranchName(branchName);
                                movementType = s.getCell(2, i).getContents();
                                opsTO.setMovementType(movementType);
                                String branchId = opsBP.getBranchId(opsTO);
                                opsTO.setBranchId(branchId);
                                april = s.getCell(3, i).getContents();
                                opsTO.setApril(april);
                                may = s.getCell(4, i).getContents();
                                opsTO.setMay(may);
                                june = s.getCell(5, i).getContents();
                                opsTO.setJune(june);
                                july = s.getCell(6, i).getContents();
                                opsTO.setJuly(july);
                                august = s.getCell(7, i).getContents();
                                opsTO.setAugust(august);
                                september = s.getCell(8, i).getContents();
                                opsTO.setSeptember(september);
                                october = s.getCell(9, i).getContents();
                                opsTO.setOctober(october);
                                november = s.getCell(10, i).getContents();
                                opsTO.setNovember(november);
                                december = s.getCell(11, i).getContents();
                                opsTO.setDecember(december);
                                january = s.getCell(12, i).getContents();
                                opsTO.setJanuary(january);
                                febuary = s.getCell(13, i).getContents();
                                opsTO.setFebuary(febuary);
                                march = s.getCell(14, i).getContents();
                                opsTO.setMarch(march);
                                weightList.add(opsTO);
                            }
                        }
                    }
                }
            }
            request.setAttribute("weightList", weightList);

            path = "content/BrattleFoods/viewWeightList.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView handleUploadExpenditure(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        int userId = 0;
        opsCommand = command;
        String pageTitle = "Upload Expenditure";
        menuPath = "Masters >> Upload Weight";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;

        ArrayList weightList = new ArrayList();
        String january = "";
        String febuary = "";
        String march = "";
        String april = "";
        String may = "";
        String june = "";
        String july = "";
        String august = "";
        String september = "";
        String october = "";
        String november = "";
        String december = "";
        String branchName = "";
        String movementType = "";
        try {
            Object mapValue = null;
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);
            if (isMultipart) {

                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();

                        if (!"".equals(uploadedFileName) && uploadedFileName != null) {

                            String[] splitFileName = uploadedFileName.split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath = tempServerFilePath + "\\" + fileSavedAs;
                            actualFilePath = actualServerFilePath + "\\" + uploadedFileName;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
                            String part1 = parts.replace("\\", "");

                            WorkbookSettings ws = new WorkbookSettings();
                            ws.setLocale(new Locale("en", "EN"));
                            Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                            Sheet s = workbook.getSheet(0);
                            System.out.println("rows" + userId + s.getRows());

                            for (int i = 1; i < s.getRows(); i++) {
                                OpsTO opsTO = new OpsTO();
                                branchName = s.getCell(1, i).getContents();
                                opsTO.setBranchName(branchName);
                                movementType = s.getCell(2, i).getContents();
                                opsTO.setMovementType(movementType);
                                String branchId = opsBP.getBranchId(opsTO);
                                opsTO.setBranchId(branchId);
                                april = s.getCell(3, i).getContents();
                                opsTO.setApril(april);
                                may = s.getCell(4, i).getContents();
                                opsTO.setMay(may);
                                june = s.getCell(5, i).getContents();
                                opsTO.setJune(june);
                                july = s.getCell(6, i).getContents();
                                opsTO.setJuly(july);
                                august = s.getCell(7, i).getContents();
                                opsTO.setAugust(august);
                                september = s.getCell(8, i).getContents();
                                opsTO.setSeptember(september);
                                october = s.getCell(9, i).getContents();
                                opsTO.setOctober(october);
                                november = s.getCell(10, i).getContents();
                                opsTO.setNovember(november);
                                december = s.getCell(11, i).getContents();
                                opsTO.setDecember(december);
                                january = s.getCell(12, i).getContents();
                                opsTO.setJanuary(january);
                                febuary = s.getCell(13, i).getContents();
                                opsTO.setFebuary(febuary);
                                march = s.getCell(14, i).getContents();
                                opsTO.setMarch(march);
                                weightList.add(opsTO);

                            }
                        }
                    }
                }
            }
            request.setAttribute("weightList", weightList);

            path = "content/BrattleFoods/viewExpenditureList.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView handleUploadTrips(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        int userId = 0;
        opsCommand = command;
        String pageTitle = "Upload Weight";
        menuPath = "Masters >> Upload Weight";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;

        ArrayList tripList = new ArrayList();
        String january = "";
        String movementType = "";
        String febuary = "";
        String march = "";
        String april = "";
        String may = "";
        String june = "";
        String july = "";
        String august = "";
        String september = "";
        String october = "";
        String november = "";
        String december = "";
        String branchName = "";
        try {
            Object mapValue = null;
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);
            if (isMultipart) {

                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();

                        if (!"".equals(uploadedFileName) && uploadedFileName != null) {

                            String[] splitFileName = uploadedFileName.split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath = tempServerFilePath + "\\" + fileSavedAs;
                            actualFilePath = actualServerFilePath + "\\" + uploadedFileName;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
                            String part1 = parts.replace("\\", "");

                            WorkbookSettings ws = new WorkbookSettings();
                            ws.setLocale(new Locale("en", "EN"));
                            Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                            Sheet s = workbook.getSheet(0);
                            System.out.println("rows" + userId + s.getRows());

                            for (int i = 1; i < s.getRows(); i++) {
                                OpsTO opsTO = new OpsTO();
                                branchName = s.getCell(1, i).getContents();
                                opsTO.setBranchName(branchName);
                                movementType = s.getCell(2, i).getContents();
                                opsTO.setMovementType(movementType);
                                String branchId = opsBP.getBranchId(opsTO);
                                opsTO.setBranchId(branchId);
                                april = s.getCell(3, i).getContents();
                                opsTO.setApril(april);
                                may = s.getCell(4, i).getContents();
                                opsTO.setMay(may);
                                june = s.getCell(5, i).getContents();
                                opsTO.setJune(june);
                                july = s.getCell(6, i).getContents();
                                opsTO.setJuly(july);
                                august = s.getCell(7, i).getContents();
                                opsTO.setAugust(august);
                                september = s.getCell(8, i).getContents();
                                opsTO.setSeptember(september);
                                october = s.getCell(9, i).getContents();
                                opsTO.setOctober(october);
                                november = s.getCell(10, i).getContents();
                                opsTO.setNovember(november);
                                december = s.getCell(11, i).getContents();
                                opsTO.setDecember(december);
                                january = s.getCell(12, i).getContents();
                                opsTO.setJanuary(january);
                                febuary = s.getCell(13, i).getContents();
                                opsTO.setFebuary(febuary);
                                march = s.getCell(14, i).getContents();
                                opsTO.setMarch(march);
                                tripList.add(opsTO);

                            }
                        }
                    }
                }
            }
            request.setAttribute("tripList", tripList);

            path = "content/BrattleFoods/viewTripList.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView handleUploadRevenue(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        int userId = 0;
        opsCommand = command;
        String pageTitle = "Upload Weight";
        menuPath = "Masters >> Upload Weight";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;

        ArrayList revenueList = new ArrayList();
        String january = "";
        String febuary = "";
        String march = "";
        String april = "";
        String may = "";
        String june = "";
        String july = "";
        String august = "";
        String september = "";
        String october = "";
        String november = "";
        String december = "";
        String branchName = "";
        String movementType = "";
        try {
            Object mapValue = null;
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);
            if (isMultipart) {

                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();

                        if (!"".equals(uploadedFileName) && uploadedFileName != null) {

                            String[] splitFileName = uploadedFileName.split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath = tempServerFilePath + "\\" + fileSavedAs;
                            actualFilePath = actualServerFilePath + "\\" + uploadedFileName;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
                            String part1 = parts.replace("\\", "");

                            WorkbookSettings ws = new WorkbookSettings();
                            ws.setLocale(new Locale("en", "EN"));
                            Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                            Sheet s = workbook.getSheet(0);
                            System.out.println("rows" + userId + s.getRows());

                            for (int i = 1; i < s.getRows(); i++) {
                                OpsTO opsTO = new OpsTO();
                                branchName = s.getCell(1, i).getContents();
                                opsTO.setBranchName(branchName);
                                movementType = s.getCell(2, i).getContents();
                                opsTO.setMovementType(movementType);
                                String branchId = opsBP.getBranchId(opsTO);
                                opsTO.setBranchId(branchId);
                                april = s.getCell(3, i).getContents();
                                opsTO.setApril(april);
                                may = s.getCell(4, i).getContents();
                                opsTO.setMay(may);
                                june = s.getCell(5, i).getContents();
                                opsTO.setJune(june);
                                july = s.getCell(6, i).getContents();
                                opsTO.setJuly(july);
                                august = s.getCell(7, i).getContents();
                                opsTO.setAugust(august);
                                september = s.getCell(8, i).getContents();
                                opsTO.setSeptember(september);
                                october = s.getCell(9, i).getContents();
                                opsTO.setOctober(october);
                                november = s.getCell(10, i).getContents();
                                opsTO.setNovember(november);
                                december = s.getCell(11, i).getContents();
                                opsTO.setDecember(december);
                                january = s.getCell(12, i).getContents();
                                opsTO.setJanuary(january);
                                febuary = s.getCell(13, i).getContents();
                                opsTO.setFebuary(febuary);
                                march = s.getCell(14, i).getContents();
                                opsTO.setMarch(march);
                                revenueList.add(opsTO);

                            }
                        }
                    }
                }
            }
            request.setAttribute("revenueList", revenueList);

            path = "content/BrattleFoods/viewRevenueList.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView saveUploadCity(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        System.out.println("command TESTING = ");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println("command TESTING = ");
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        int userId = 0;
        String pageTitle = "City Uploading";
        menuPath = "HRMS >> Upload City";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        boolean isMultipart = false;
        int insertStatus = 0;
        OpsTO opsTO = new OpsTO();
        try {
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            //isMultipart = ServletFileUpload.isMultipartContent(request);
            String[] zoneName = request.getParameterValues("zoneName");
            String[] cityName = request.getParameterValues("cityName");
            String[] cityCode = request.getParameterValues("cityCode");
            int UserId = (Integer) session.getAttribute("userId");
            insertStatus = opsBP.saveCityList(zoneName, cityName, cityCode, UserId);

            if (insertStatus != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Employee Added Successfully");
            }

            ArrayList zoneList = new ArrayList();
            zoneList = opsBP.getZoneList(opsTO);
            request.setAttribute("zoneList", zoneList);

            path = "content/BrattleFoods/zoneMaster.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView saveUploadBudgetWeight(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        System.out.println("command TESTING = ");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println("command TESTING = ");
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        int userId = 0;
        String pageTitle = "City Uploading";
        menuPath = "HRMS >> Upload City";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        boolean isMultipart = false;
        int insertStatus = 0;
        OpsTO opsTO = new OpsTO();
        System.out.println("Hi I am here 1....................");
        try {
            System.out.println("Hi I am here 2....................");
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            //isMultipart = ServletFileUpload.isMultipartContent(request);
            String[] branchId = request.getParameterValues("branchId");
            String[] movementType = request.getParameterValues("movementType");
            String[] january = request.getParameterValues("january");
            String[] febuary = request.getParameterValues("febuary");
            String[] march = request.getParameterValues("march");
            String[] april = request.getParameterValues("april");
            String[] may = request.getParameterValues("may");
            String[] june = request.getParameterValues("june");
            String[] july = request.getParameterValues("july");
            String[] august = request.getParameterValues("august");
            String[] september = request.getParameterValues("september");
            String[] october = request.getParameterValues("october");
            String[] november = request.getParameterValues("november");
            String[] december = request.getParameterValues("december");
            int UserId = (Integer) session.getAttribute("userId");
            insertStatus = opsBP.saveBudgetList(branchId, movementType, january, febuary, march, april, may, june, july, august, september, october, november, december, UserId);
            System.out.println("insertStatus " + insertStatus);
            if (insertStatus != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Budget Weight Added Successfully");
            }

            path = "content/BrattleFoods/weightImport.jsp";

        } catch (FPRuntimeException exception) {
            exception.printStackTrace();
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView saveUploadExpenditure(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        System.out.println("command TESTING = ");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println("command TESTING = ");
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        int userId = 0;
        String pageTitle = "City Uploading";
        menuPath = "HRMS >> Upload City";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        boolean isMultipart = false;
        int insertStatus = 0;
        OpsTO opsTO = new OpsTO();
        try {
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            String[] movementType = request.getParameterValues("movementType");
            //isMultipart = ServletFileUpload.isMultipartContent(request);
            String[] branchId = request.getParameterValues("branchId");
            String[] january = request.getParameterValues("january");
            String[] febuary = request.getParameterValues("febuary");
            String[] march = request.getParameterValues("march");
            String[] april = request.getParameterValues("april");
            String[] may = request.getParameterValues("may");
            String[] june = request.getParameterValues("june");
            String[] july = request.getParameterValues("july");
            String[] august = request.getParameterValues("august");
            String[] september = request.getParameterValues("september");
            String[] october = request.getParameterValues("october");
            String[] november = request.getParameterValues("november");
            String[] december = request.getParameterValues("december");
            int UserId = (Integer) session.getAttribute("userId");
            insertStatus = opsBP.saveExpenditureList(branchId, movementType, january, febuary, march, april, may, june, july, august, september, october, november, december, UserId);

            if (insertStatus != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Budget Weight Added Successfully");
            }

            path = "content/BrattleFoods/expenditureImport.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView saveUploadTrips(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        System.out.println("command TESTING = ");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println("command TESTING = ");
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        int userId = 0;
        String pageTitle = "City Uploading";
        menuPath = "HRMS >> Upload City";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        boolean isMultipart = false;
        int insertStatus = 0;
        OpsTO opsTO = new OpsTO();
        try {
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            String[] movementType = request.getParameterValues("movementType");
            //isMultipart = ServletFileUpload.isMultipartContent(request);
            String[] branchId = request.getParameterValues("branchId");
            String[] january = request.getParameterValues("january");
            String[] febuary = request.getParameterValues("febuary");
            String[] march = request.getParameterValues("march");
            String[] april = request.getParameterValues("april");
            String[] may = request.getParameterValues("may");
            String[] june = request.getParameterValues("june");
            String[] july = request.getParameterValues("july");
            String[] august = request.getParameterValues("august");
            String[] september = request.getParameterValues("september");
            String[] october = request.getParameterValues("october");
            String[] november = request.getParameterValues("november");
            String[] december = request.getParameterValues("december");
            int UserId = (Integer) session.getAttribute("userId");
            insertStatus = opsBP.saveTripList(branchId, movementType, january, febuary, march, april, may, june, july, august, september, october, november, december, UserId);

            if (insertStatus != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Trip Weight Added Successfully");
            }

            path = "content/BrattleFoods/weightImport.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView saveUploadRevenue(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        System.out.println("command TESTING = ");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println("command TESTING = ");
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        int userId = 0;
        String pageTitle = "City Uploading";
        menuPath = "HRMS >> Upload City";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        boolean isMultipart = false;
        int insertStatus = 0;
        OpsTO opsTO = new OpsTO();
        try {
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            //isMultipart = ServletFileUpload.isMultipartContent(request);
            String[] branchId = request.getParameterValues("branchId");
            String[] movementType = request.getParameterValues("movementType");
            String[] january = request.getParameterValues("january");
            String[] febuary = request.getParameterValues("febuary");
            String[] march = request.getParameterValues("march");
            String[] april = request.getParameterValues("april");
            String[] may = request.getParameterValues("may");
            String[] june = request.getParameterValues("june");
            String[] july = request.getParameterValues("july");
            String[] august = request.getParameterValues("august");
            String[] september = request.getParameterValues("september");
            String[] october = request.getParameterValues("october");
            String[] november = request.getParameterValues("november");
            String[] december = request.getParameterValues("december");
            int UserId = (Integer) session.getAttribute("userId");
            insertStatus = opsBP.saveRevenueList(branchId, movementType, january, febuary, march, april, may, june, july, august, september, october, november, december, UserId);

            if (insertStatus != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Budget Revenue Added Successfully");
            }

            path = "content/BrattleFoods/revenueImport.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public void getAwbNo(HttpServletRequest request, HttpServletResponse response, OpsCommand command) throws IOException {
        HttpSession session = request.getSession();
        opsCommand = command;
//        OpsTO opsTO = new OpsTO();
//        JsonTO jsonTO = new JsonTO();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            response.setContentType("text/html");
            String consignmentOrderId = request.getParameter("consignmentOrderId");
            String otlNo = request.getParameter("otlNo");
            System.out.println("consignmentOrderId in ajax " + consignmentOrderId);
            String awbNo = opsBP.getAwbNo();

            JSONArray jsonArray = new JSONArray();
//            Iterator itr = userDetails.iterator();
//            while (itr.hasNext()) {
            JSONObject jsonObject = new JSONObject();
//                opsTO = (OpsTO) itr.next();
            jsonObject.put("Name", awbNo);
            jsonObject.put("Name", awbNo);
            System.out.println("jsonObject = " + jsonObject);
            jsonArray.put(jsonObject);
//            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView viewDepreciationChargeMaster(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "Service Tax Master";
        menuPath = "Operation >>  Service Tax Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            String branchId = "";
            String roleId = "" + (Integer) session.getAttribute("RoleId");

            ArrayList depreciationChargeList = new ArrayList();
            depreciationChargeList = opsBP.getDepreciationChargeList(branchId, roleId);
            request.setAttribute("depreciationChargeList", depreciationChargeList);

            path = "content/BrattleFoods/viewDepreciationChargeMaster.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveDepreciationChargeMaster(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "Service Tax Master";
        menuPath = "Master >>  Service Tax Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {

            if (request.getParameter("depreciationChargeId") != null && !"".equals(request.getParameter("depreciationChargeId"))) {
                opsTO.setDepreciationChargeId(request.getParameter("depreciationChargeId"));
            }
            if (request.getParameter("depreciationChargeName") != null && !"".equals(request.getParameter("depreciationChargeName"))) {
                opsTO.setDepreciationChargeName(request.getParameter("depreciationChargeName"));
            }
            if (request.getParameter("totalCost") != null && !"".equals(request.getParameter("totalCost"))) {
                opsTO.setTotalCost(request.getParameter("totalCost"));
            }
            if (request.getParameter("percentage") != null && !"".equals(request.getParameter("percentage"))) {
                opsTO.setPercentage(request.getParameter("percentage"));
            }
            if (request.getParameter("activeInd") != null && !"".equals(request.getParameter("activeInd"))) {
                opsTO.setActiveInd(request.getParameter("activeInd"));
            }

            int status = opsBP.saveDepreciationChargeMaster(opsTO, userId);
            System.out.println("status = " + status);

            String branchId = "";
            String roleId = "" + (Integer) session.getAttribute("RoleId");

            ArrayList depreciationChargeList = new ArrayList();
            depreciationChargeList = opsBP.getDepreciationChargeList(branchId, roleId);
            request.setAttribute("depreciationChargeList", depreciationChargeList);

            path = "content/BrattleFoods/viewDepreciationChargeMaster.jsp";

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Sucessfully inserted ");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewExpenseTypeMaster(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "Expense Type Master";
        menuPath = "Master >>  Expense Type Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            String branchId = "";
            String roleId = "" + (Integer) session.getAttribute("RoleId");

            ArrayList expenseList = new ArrayList();
            expenseList = opsBP.getExpenseTypeList(branchId, roleId);
            request.setAttribute("expenseList", expenseList);

            path = "content/BrattleFoods/viewExpenseMaster.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewGpsDeviceMaster(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "GPS Device Master";
        menuPath = "Master >>  GPS Device Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            String branchId = "";
            String roleId = "" + (Integer) session.getAttribute("RoleId");

            ArrayList deviceList = new ArrayList();
            deviceList = opsBP.getGpsDeviceList(branchId, roleId);
            request.setAttribute("deviceList", deviceList);

            path = "content/BrattleFoods/viewGpsdeviceMaster.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveGpsDeviceMaster(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "GPS Device Master";
        menuPath = "Master >>  GPS Device Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {

            if (request.getParameter("deviceId") != null && !"".equals(request.getParameter("deviceId"))) {
                opsTO.setDeviceId(request.getParameter("deviceId"));
            }
            if (request.getParameter("deviceName") != null && !"".equals(request.getParameter("deviceName"))) {
                opsTO.setDeviceName(request.getParameter("deviceName"));
            }
            if (request.getParameter("activeInd") != null && !"".equals(request.getParameter("activeInd"))) {
                opsTO.setActiveInd(request.getParameter("activeInd"));
            }

            int status = opsBP.saveGpsDeviceMaster(opsTO, userId);
            System.out.println("status = " + status);

            String branchId = "";
            String roleId = "";

            ArrayList deviceList = new ArrayList();
            deviceList = opsBP.getGpsDeviceList(branchId, roleId);
            request.setAttribute("deviceList", deviceList);

            path = "content/BrattleFoods/viewGpsdeviceMaster.jsp";

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Sucessfully inserted ");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveExpenseTypeMaster(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "Expense Type Master";
        menuPath = "Master >>  Expense Type Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {

            if (request.getParameter("expenseId") != null && !"".equals(request.getParameter("expenseId"))) {
                opsTO.setExpenseId(request.getParameter("expenseId"));
            }
            if (request.getParameter("expenseName") != null && !"".equals(request.getParameter("expenseName"))) {
                opsTO.setExpenseName(request.getParameter("expenseName"));
            }
            if (request.getParameter("expenseType") != null && !"".equals(request.getParameter("expenseType"))) {
                opsTO.setExpenseType(request.getParameter("expenseType"));
            }
            if (request.getParameter("activeInd") != null && !"".equals(request.getParameter("activeInd"))) {
                opsTO.setActiveInd(request.getParameter("activeInd"));
            }

            int status = opsBP.saveExpenseTypeMaster(opsTO, userId);
            System.out.println("status = " + status);

            String branchId = "";
            String roleId = "" + (Integer) session.getAttribute("RoleId");

            ArrayList expenseList = new ArrayList();
            expenseList = opsBP.getExpenseTypeList(branchId, roleId);
            request.setAttribute("expenseList", expenseList);

            path = "content/BrattleFoods/viewExpenseMaster.jsp";

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Sucessfully inserted ");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleUploadOldData(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        int userId = 0;
        opsCommand = command;
        String pageTitle = "Upload Zone List";
        menuPath = "Masters >> Upload Zone List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;

        ArrayList consignmentOrderList = new ArrayList();
        String zoneName = "", vehicleNo = "", tripDate = "", origin = "", destination = "", movementType = "", customerName = "";
        String customerType = "", awbDestination = "", vehicleType = "", awbNo = "", totalPackages = "", grossWeight = "";
        String chargeableWeight = "", volume = "", rateValue = "", revenue = "", vendorName = "", departureDate = "", departureTime = "", arrivalDate = "", arrivalTime = "";

        int status = 0;
        Map map = new HashMap();
        HashSet hs = new HashSet();
        try {
            Object mapValue = null;
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);
            if (isMultipart) {

                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();

                        if (!"".equals(uploadedFileName) && uploadedFileName != null) {

                            String[] splitFileName = uploadedFileName.split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath = tempServerFilePath + "\\" + fileSavedAs;
                            actualFilePath = actualServerFilePath + "\\" + uploadedFileName;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
                            String part1 = parts.replace("\\", "");

                            WorkbookSettings ws = new WorkbookSettings();
                            ws.setLocale(new Locale("en", "EN"));
                            Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                            Sheet s = workbook.getSheet(0);
                            System.out.println("rows" + userId + s.getRows());

                            for (int i = 1; i < s.getRows(); i++) {
                                OpsTO opsTO = new OpsTO();
                                vehicleNo = s.getCell(1, i).getContents();
                                opsTO.setVehicleRegNo(vehicleNo);
                                tripDate = s.getCell(2, i).getContents();
                                opsTO.setConsignmentDate(tripDate);
                                origin = s.getCell(3, i).getContents();
                                opsTO.setOrigin(origin);
                                destination = s.getCell(4, i).getContents();
                                opsTO.setDestination(destination);
                                movementType = s.getCell(5, i).getContents();
                                opsTO.setMovementType(movementType);
                                customerName = s.getCell(6, i).getContents();
                                opsTO.setCustomerName(customerName);
                                customerType = s.getCell(7, i).getContents();
                                opsTO.setCustomerCode(customerType);
                                awbDestination = s.getCell(8, i).getContents();
                                opsTO.setAwbDestinationId(awbDestination);
                                vehicleType = s.getCell(9, i).getContents();
                                opsTO.setVehicleTypeName(vehicleType);
                                awbNo = s.getCell(10, i).getContents();
                                opsTO.setAwbno(awbNo);
                                totalPackages = s.getCell(11, i).getContents();
                                opsTO.setTotalPackages(totalPackages);
                                grossWeight = s.getCell(12, i).getContents();
                                opsTO.setTotalWeight(grossWeight);
                                chargeableWeight = s.getCell(13, i).getContents();
                                opsTO.setChargableWeight(chargeableWeight);
                                volume = s.getCell(14, i).getContents();
                                opsTO.setTotalVolume(volume);
                                rateValue = s.getCell(15, i).getContents();
                                opsTO.setRateValue(rateValue);
                                revenue = s.getCell(16, i).getContents();
                                opsTO.setTotalCost(revenue);
                                vendorName = s.getCell(17, i).getContents();
                                opsTO.setVendorName(vendorName);
                                departureDate = s.getCell(18, i).getContents();
                                opsTO.setTruckDepDate(departureDate);
                                departureTime = s.getCell(19, i).getContents();
                                opsTO.setTruckDeptTime(departureTime);
                                arrivalDate = s.getCell(20, i).getContents();
                                opsTO.setTruckArrDate(arrivalDate);
                                arrivalTime = s.getCell(21, i).getContents();
                                opsTO.setTruckArrTime(arrivalTime);
                                String consignmentDataList = "";
                                consignmentDataList = opsBP.getConsignmentDataList(opsTO);
                                String temp[] = null;
                                if (!consignmentDataList.equals("")) {
                                    temp = consignmentDataList.split("~");
                                    opsTO.setVehicleId(temp[0]);
                                    opsTO.setAwbOriginId(temp[1]);
                                    opsTO.setAwbDestinationId(temp[2]);
                                    opsTO.setCustomerId(temp[3]);
                                    opsTO.setFleetCode(temp[4]);
                                    opsTO.setAwbDestinationName(temp[5]);
                                    opsTO.setVendorId(temp[6]);
                                }
                                consignmentOrderList.add(opsTO);

                            }
                        }
                    }
                }
            }
            request.setAttribute("consignmentOrderList", consignmentOrderList);

            path = "content/BrattleFoods/viewOldDataList.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView saveUploadOldData(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        System.out.println("command TESTING = ");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println("command TESTING = ");
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        int userId = 0;
        String pageTitle = "Employee Uploading";
        menuPath = "HRMS >> Upload Employee";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        boolean isMultipart = false;
        int insertStatus = 0;
        OpsTO opsTO = new OpsTO();
        try {
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            //isMultipart = ServletFileUpload.isMultipartContent(request);
            String[] zoneName = request.getParameterValues("zoneName");
            int UserId = (Integer) session.getAttribute("userId");
            insertStatus = opsBP.saveZoneList(zoneName);

            if (insertStatus != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Employee Added Successfully");
            }

            ArrayList zoneList = new ArrayList();
            zoneList = opsBP.getZoneList(opsTO);
            request.setAttribute("zoneList", zoneList);

            path = "content/BrattleFoods/viewOldDataList.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView viewMappingGpsDevice(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "GPS Device Master";
        menuPath = "Master >>  GPS Device Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            String branchId = "";
            String roleId = "";

            opsTO.setEffectiveDate(request.getParameter("effectiveDate"));

            ArrayList deviceList = new ArrayList();
            deviceList = opsBP.getGpsDeviceList(branchId, roleId);
            request.setAttribute("deviceList", deviceList);

            ArrayList branchList = new ArrayList();
            branchList = opsBP.getBranchList(opsTO);
            request.setAttribute("branchList", branchList);

            if (request.getParameter("effectiveDate") != null && !"".equals(request.getParameter("effectiveDate"))) {
                ArrayList deviceListDetails = new ArrayList();
                deviceListDetails = opsBP.getDeviceListDetails(opsTO);
                request.setAttribute("deviceListDetails", deviceListDetails);
                request.setAttribute("effectiveDate", request.getParameter("effectiveDate"));
            }

            path = "content/BrattleFoods/viewMappingGpsDevice.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveMappingGpsDevice(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "GPS Device Master";
        menuPath = "Master >>  GPS Device Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {

            if (request.getParameter("deviceId") != null && !"".equals(request.getParameter("deviceId"))) {
                opsTO.setDeviceId(request.getParameter("deviceId"));
            }
            if (request.getParameter("statusId") != null && !"".equals(request.getParameter("statusId"))) {
                opsTO.setStatusId(request.getParameter("statusId"));
            }
            if (request.getParameter("branchId") != null && !"".equals(request.getParameter("branchId"))) {
                opsTO.setBranchId(request.getParameter("branchId"));
            }
            if (request.getParameter("fromDate") != null && !"".equals(request.getParameter("fromDate"))) {
                opsTO.setEffectiveDate(request.getParameter("fromDate"));
            }
            if (request.getParameter("activeInd") != null && !"".equals(request.getParameter("activeInd"))) {
                opsTO.setActiveInd(request.getParameter("activeInd"));
            }

            int status = opsBP.saveMappingGpsDevice(opsTO, userId);
            System.out.println("status = " + status);

            String branchId = "";
            String roleId = "" + (Integer) session.getAttribute("RoleId");

            ArrayList deviceList = new ArrayList();
            deviceList = opsBP.getGpsDeviceList(branchId, roleId);
            request.setAttribute("deviceList", deviceList);

            ArrayList branchList = new ArrayList();
            branchList = opsBP.getBranchList(opsTO);
            request.setAttribute("branchList", branchList);

            if (request.getParameter("effectiveDate") != null && !"".equals(request.getParameter("effectiveDate"))) {
                ArrayList deviceListDetails = new ArrayList();
                deviceListDetails = opsBP.getDeviceListDetails(opsTO);
                request.setAttribute("deviceListDetails", deviceListDetails);
                request.setAttribute("effectiveDate", request.getParameter("effectiveDate"));
            }

            path = "content/BrattleFoods/viewMappingGpsDevice.jsp";

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Sucessfully inserted ");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleConsignmentDeatilsForAwb(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OpsTO opsTO = new OpsTO();
        String pageTitle = "View Air WayBill Details";
        menuPath = "Operation >>  Air WayBill Details";
        String truckCodeId = "";

        try {

            request.setAttribute("pageTitle", pageTitle);
            int userId = (Integer) session.getAttribute("userId");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            path = "content/BrattleFoods/searchConsignmentsForAwb.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void checkAwbNo(HttpServletRequest request, HttpServletResponse response, OpsCommand command) throws IOException {
        System.out.println("entered checkAwbNo");
        HttpSession session = request.getSession();
        opsCommand = command;
        OpsTO opsTO = new OpsTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList checkAwbNo = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        String awbNo = "";
        String shipmentType = "";
        try {
            response.setContentType("text/html");
            awbNo = request.getParameter("chAwb");
            shipmentType = request.getParameter("shipmentType");
            opsTO.setAwbNo(awbNo);
            opsTO.setShipmentType(shipmentType);

            checkAwbNo = opsBP.getCheckAwbNo(opsTO);

            System.out.println("checkAwbNo.size() = " + checkAwbNo.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = checkAwbNo.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                opsTO = (OpsTO) itr.next();
                jsonObject.put("Id", opsTO.getShipmentType());
                jsonObject.put("AwbTotalPackages", opsTO.getAwbTotalPackages());
                jsonObject.put("AwbReceivedPackages", opsTO.getAwbReceivedPackages());
                jsonObject.put("AwbPendingPackages", opsTO.getAwbPendingPackages());
                jsonObject.put("AwbTotalGrossWeight", opsTO.getAwbTotalGrossWeight());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getAirLineName(HttpServletRequest request, HttpServletResponse response, OpsCommand command) throws IOException {
        HttpSession session = request.getSession();
        opsCommand = command;
        OpsTO opsTO = new OpsTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String cityName = "";
            String lineId = "";
            response.setContentType("text/html");
            lineId = request.getParameter("lineId");
            opsTO.setPreviousCityId(lineId);
            userDetails = opsBP.getAirLineName(opsTO);
            System.out.println("userDetails.size() = " + userDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = userDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                opsTO = (OpsTO) itr.next();
                jsonObject.put("Name", opsTO.getCityName());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView getConsignmentBondedDetailsForEdit(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OperationTO operationTO = new OperationTO();
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  Route Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList routeList = new ArrayList();
        String branchId = (String) session.getAttribute("BranchId");
        try {

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            System.out.println(dateFormat.format(date));
            request.setAttribute("curDate", dateFormat.format(date));
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageType = request.getParameter("pageType");
            String consignmentOrderId = request.getParameter("consignmentOrderId");
            operationTO.setConsignmentOrderId(consignmentOrderId);
            System.out.println("consignmentOrderId" + consignmentOrderId);
            if ( "4".equals(pageType)) {
                System.out.println("pageType=="+pageType);
                path = "content/BrattleFoods/consignmentNoteBondedView1.jsp";
            } else {
                System.out.println("pageType=="+pageType);
                path = "content/BrattleFoods/consignmentNoteBondedEdit1.jsp";
            }
            /*
             String consignmentOrderNo = operationBP.getConsignmentOrderNo(operationTO);
             String temp[] = null;
             int cnote = 0;
             if (consignmentOrderNo != null) {
             temp = consignmentOrderNo.split("/");
             cnote = Integer.parseInt(temp[2]);
             cnote++;
             request.setAttribute("consignmentOrderNo", temp[0] + "/" + temp[1] + "/" + cnote);
             } else {
             request.setAttribute("consignmentOrderNo", "CO/13-14/200301");
             }
             */
            System.out.println("setConsignmentOrderId(consignmentOrderId)=" + consignmentOrderId);
            operationTO.setBranchId(branchId);
            ArrayList todayBooking = new ArrayList();
            todayBooking = operationBP.getTodayBookingDetails(operationTO);
            Iterator itr = todayBooking.iterator();
            OperationTO opsTO = new OperationTO();
            while (itr.hasNext()) {
                opsTO = (OperationTO) itr.next();
                request.setAttribute("totalBooking", opsTO.getTotalBooking());
                request.setAttribute("totalGrossWeight", opsTO.getTotalGrossWeight());

            }

            ArrayList customerTypeList = new ArrayList();
            customerTypeList = operationBP.getCustomerTypeList(operationTO);
            request.setAttribute("customerTypeList", customerTypeList);

            ArrayList vehicleTypeList = new ArrayList();
            vehicleTypeList = operationBP.getVehicleTypeList();
            request.setAttribute("vehicleTypeList", vehicleTypeList);

            ArrayList billingTypeList = new ArrayList();
            billingTypeList = operationBP.getBillingTypeList();
            request.setAttribute("billingTypeList", billingTypeList);
            ArrayList productCategoryList = new ArrayList();
            productCategoryList = operationBP.getProductCategoryList();
            request.setAttribute("productCategoryList", productCategoryList);
            ArrayList getPointList = new ArrayList();
            getPointList = opsBP.getCityCodeList(new OpsTO());
            request.setAttribute("getPointList", getPointList);

            ArrayList getPointList1 = new ArrayList();
            getPointList1 = opsBP.getCityCodeList1(new OpsTO());
            request.setAttribute("getPointList1", getPointList1);

            ArrayList customerList = new ArrayList();
            customerList = opsBP.getCustomerList();
            request.setAttribute("getCustomerList", customerList);

            ArrayList consignmentOrderLists = new ArrayList();
            consignmentOrderLists = opsBP.getConsignmentOrderLists(operationTO);
            request.setAttribute("consignmentOrderLists", consignmentOrderLists);

            ArrayList consignmentArticleList = new ArrayList();
            consignmentArticleList = opsBP.getConsignmentArticleList(operationTO);
            request.setAttribute("consignmentArticleList", consignmentArticleList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView updateBookingOrder(HttpServletRequest request, HttpServletResponse response, OpsCommand command) throws IOException{
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        opsCommand = command;
        String menuPath = "";
        OperationTO operationTO = new OperationTO();
        OpsTO opsTO = new OpsTO();
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  Route Details";
        request.setAttribute("pageTitle", pageTitle);
        int userId = (Integer) session.getAttribute("userId");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList routeList = new ArrayList();
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            System.out.println(dateFormat.format(date));
            request.setAttribute("curDate", dateFormat.format(date));
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            path = "content/contract/contractPointToPointWeight.jsp";
            opsTO.setUserId(userId);
            if (opsCommand.getConsignmentOrderId() != null && !"".equals(opsCommand.getConsignmentOrderId())) {
                opsTO.setConsignmentOrderId(opsCommand.getConsignmentOrderId());
            }
            if (opsCommand.getShipmentType() != null && !"".equals(opsCommand.getShipmentType())) {
                opsTO.setShipmentType(opsCommand.getShipmentType());
            }
            if (opsCommand.getAwbMovementType() != null && !"".equals(opsCommand.getAwbMovementType())) {
                opsTO.setAwbMovementType(opsCommand.getAwbMovementType());
            }
            System.out.println("opsCommand.getAwbMovementType()==" + opsCommand.getAwbMovementType());
            if (opsCommand.getAwbType() != null && !"".equals(opsCommand.getAwbType())) {
                opsTO.setAwbType(opsCommand.getAwbType());
            }
            if (opsCommand.getOrderReferenceAwb() != null && !"".equals(opsCommand.getOrderReferenceAwb())) {
                opsTO.setOrderReferenceAwb(opsCommand.getOrderReferenceAwb());
            }
            if (opsCommand.getOrderReferenceAwbNo() != null && !"".equals(opsCommand.getOrderReferenceAwbNo())) {
                opsTO.setOrderReferenceAwbNo(opsCommand.getOrderReferenceAwbNo());
            }
            if (opsCommand.getAirlineName() != null && !"".equals(opsCommand.getAirlineName())) {
                opsTO.setAirlineName(opsCommand.getAirlineName());
            }
            if (opsCommand.getOrderReferenceEnd() != null && !"".equals(opsCommand.getOrderReferenceEnd())) {
                opsTO.setOrderReferenceEnd(opsCommand.getOrderReferenceEnd());
            }
            if (opsCommand.getAwbOrderDeliveryDate() != null && !"".equals(opsCommand.getAwbOrderDeliveryDate())) {
                opsTO.setAwbOrderDeliveryDate(opsCommand.getAwbOrderDeliveryDate());
            }
            if (opsCommand.getOrderReferenceNo() != null && !"".equals(opsCommand.getOrderReferenceNo())) {
                opsTO.setOrderReferenceNo(opsCommand.getOrderReferenceNo());
            }
            if (opsCommand.getCommodity() != null && !"".equals(opsCommand.getCommodity())) {
                opsTO.setCommodity(opsCommand.getCommodity());
            }
            if (opsCommand.getTotalWeightage() != null && !"".equals(opsCommand.getTotalWeightage())) {
                opsTO.setTotalWeightage(opsCommand.getTotalWeightage());
            }
            if (opsCommand.getTotalChargeableWeights() != null && !"".equals(opsCommand.getTotalChargeableWeights())) {
                opsTO.setTotalChargeableWeights(opsCommand.getTotalChargeableWeights());
            }
            if (opsCommand.getTotalVolume() != null && !"".equals(opsCommand.getTotalVolume())) {
                opsTO.setTotalVolume(opsCommand.getTotalVolume());
            }
            if (opsCommand.getContract() != null && !"".equals(opsCommand.getContract())) {
                opsTO.setContract(opsCommand.getContract());
            }

            if (opsCommand.getNoOfPieces() != null && !"".equals(opsCommand.getNoOfPieces())) {
                opsTO.setNoOfPieces(opsCommand.getNoOfPieces());
            }
            if (opsCommand.getUom() != null && !"".equals(opsCommand.getUom())) {
                opsTO.setUoms(opsCommand.getUom());
            }
            if (opsCommand.getLengths() != null && !"".equals(opsCommand.getLengths())) {
                opsTO.setLengths(opsCommand.getLengths());
            }
            if (opsCommand.getWidths() != null && !"".equals(opsCommand.getWidths())) {
                opsTO.setWidths(opsCommand.getWidths());
            }
            if (opsCommand.getHeights() != null && !"".equals(opsCommand.getHeights())) {
                opsTO.setHeights(opsCommand.getHeights());
            }
            if (opsCommand.getVolumes() != null && !"".equals(opsCommand.getVolumes())) {
                opsTO.setVolumes(opsCommand.getVolumes());
            }
            if (opsCommand.getChargeableWeight() != null && !"".equals(opsCommand.getChargeableWeight())) {
                opsTO.setChargeableWeights(opsCommand.getChargeableWeight());
            }
            if (opsCommand.getChargeableWeightId() != null && !"".equals(opsCommand.getChargeableWeightId())) {
                opsTO.setChargeableWeightId(opsCommand.getChargeableWeightId());
            }
            if (opsCommand.getConsignmentDate() != null && !"".equals(opsCommand.getConsignmentDate())) {
                opsTO.setConsignmentDate(opsCommand.getConsignmentDate());
            }
            if (opsCommand.getRateMode() != null && !"".equals(opsCommand.getRateMode())) {
                opsTO.setRateMode(opsCommand.getRateMode());
            }
            if (opsCommand.getRateMode1() != null && !"".equals(opsCommand.getRateMode1())) {
                opsTO.setRateMode1(opsCommand.getRateMode1());
            }
            String[] contractId1 = request.getParameterValues("contract");
            String[] rateMode1 = request.getParameterValues("rateMode1");
            opsTO.setContractId1(contractId1);
            System.out.println("rateMode1=" + rateMode1[0]);
            String[] rateMode = request.getParameterValues("rateMode");
            if ("1".equals(contractId1[0])) {
                if (request.getParameter("rateMode") != null && !"".equals(request.getParameter("rateMode"))) {
                    if ("1".equals(rateMode[0])) {
                        operationTO.setRateMode(rateMode[0]);
                        operationTO.setPerKgRate(request.getParameter("perKgAutoRate"));
                        operationTO.setRateValue(request.getParameter("rateValue"));
                    } else {
                        operationTO.setRateMode(rateMode[0]);
                        operationTO.setRateValue(request.getParameter("rateValue"));
                    }
                }
            } else if ("2".equals(contractId1[0])) {
                if (request.getParameter("rateMode1") != null && !"".equals(request.getParameter("rateMode1"))) {
                    if ("3".equals(rateMode1[0])) {
                        operationTO.setRateMode1(rateMode1[0]);
                        operationTO.setRateValue1(request.getParameter("rateValue1"));
                    } else {
                        operationTO.setRateMode1(rateMode1[0]);
                        operationTO.setRateValue1(request.getParameter("rateValue1"));
                    }
                }
            }
            int updateconsignmentList = 0;
            updateconsignmentList = opsBP.updateconsignmentList(opsTO, operationTO);
            request.setAttribute("updateconsignmentList", updateconsignmentList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView("consignmentNoteView.do");
    }
    
 public ModelAndView uploadConsignmentContract(HttpServletRequest request, HttpServletResponse response, OpsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        int userId = 0;
        OperationTO operationTO = new OperationTO();
        String pageTitle = "Cnote Vehicle No Creation";
        menuPath = "Operation >> Cnote Vehicle No Creation";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        boolean isMultipart = false;
        System.out.println("isMultipart : " + isMultipart);
        Part partObj = null;
        FilePart fPart = null;
        String customerId = "";
        String origin = "";
        String touchPoint1 = "";
        String destination = "";
        String amount = "";
        String vehicleType = "";
        String km = "";
        String tat = "";

        String vehicleRegDate = "";
        String docket1 = "";
        String docket2 = "";

        try {

            int update = operationBP.updateConsignmentContract(operationTO);
            opsCommand = command;
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();

                        if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                            String[] splitFileName = uploadedFileName.split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath = tempServerFilePath + "\\" + fileSavedAs;
                            actualFilePath = actualServerFilePath + "\\" + uploadedFileName;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
                            String part1 = parts.replace("\\", "");

                            int count = 0;
                            WorkbookSettings ws = new WorkbookSettings();
                            ws.setLocale(new Locale("en", "EN"));
                            Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                            Sheet s = workbook.getSheet(0);
                            System.out.println("rows" + userId + s.getRows());

                            OperationTO opto = new OperationTO();

                            for (int i = 1; i < s.getRows(); i++) {
                                count++;
                                customerId = s.getCell(0, i).getContents();
                                vehicleType = s.getCell(1, i).getContents();
                                origin = s.getCell(2, i).getContents();
                                touchPoint1 = s.getCell(3, i).getContents();
                                destination = s.getCell(4, i).getContents();
                                amount = s.getCell(5, i).getContents();
                                km = s.getCell(6, i).getContents();
                                tat = s.getCell(7, i).getContents();


                                opto.setCustomerName(customerId);
                                opto.setOrigin(origin);
                                opto.setTouchPoint1(touchPoint1);
                                opto.setDestination(destination);
                                opto.setPallets(amount);
                                opto.setVehicleType(vehicleType);
                                opto.setConsignor(km);
                                opto.setConsignee(tat);
                                opto.setUserId(userId);

                                int routeCheck = operationBP.getRouteCheck(opto);
                                opto.setCounts(routeCheck);
                                request.setAttribute("routeCheck", routeCheck);

                                int status = operationBP.insertConsignmentUpload(opto);

                            }
                        }
                    }
                }
            }

            path = "content/BrattleFoods/consignmentUploadDetails.jsp";

            ArrayList consignmentExcelUploadIncorect = new ArrayList();
            consignmentExcelUploadIncorect = operationBP.getConsignmentExcelUploadIncorect(operationTO);
            request.setAttribute("consignmentExcelUploadIncorect", consignmentExcelUploadIncorect);
            request.setAttribute("consignExcelIncorect", consignmentExcelUploadIncorect.size());

            ArrayList consignmentExcelUpload = new ArrayList();
            consignmentExcelUpload = operationBP.getConsignmentExcelUpload(operationTO);
            request.setAttribute("consignmentExcelUpload", consignmentExcelUpload);
            request.setAttribute("consignExcelcorect", consignmentExcelUpload.size());

            System.out.println("consignExcelIncorect size : " + consignmentExcelUploadIncorect.size());
            System.out.println("consignExcelcorect size : " + consignmentExcelUpload.size());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }
}
