package ets.domain.ops.web;

import com.opencsv.CSVReader;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ParseCSV2Bean extends Thread {

    String FILENAME = "";
    String LOADINGSTATUS = "";
    String airWiseBillNo = "";
    String origin = "";
    String destination = "";
    String vehicleNo = "";
    String loadStatus = "";
    String loadTime = "";
    String customerDoc = "";
    String unloadStatus = "";
    String unloadTime = "";
    String podRecieved = "";
    String barCode = "";
    String consignmentOrderId = "";
    int parcelNo = 0;
    int CONSIGNMENTORDERID = 0;

    public ParseCSV2Bean(String fileName, int consignmentOrderId, String loadingStatus) {
        FILENAME = fileName;
        CONSIGNMENTORDERID = consignmentOrderId;
        LOADINGSTATUS = loadingStatus;
    }

    public void run() {
        try {
            // To ignore Processing of 1st row
            CSVReader reader = new CSVReader(new FileReader(FILENAME), ',', '\"', 1);

            ColumnPositionMappingStrategy<ProductDetail> mappingStrategy
                    = new ColumnPositionMappingStrategy<ProductDetail>();
            mappingStrategy.setType(ProductDetail.class);

            // the fields to bind do in your JavaBean
//             AWB,PARCEL,ORIGIN,DESTINATION,BARCODE,LOAD STATUS,LOADED TIME,TRUCK NO,CUSTOMER DOC,UNLOAD STATUS,UNLOAD TIME,POD RECIEVED
            String[] columns = new String[]{"AWB", "PARCEL", "ORIGIN", "DESTINATION", "BARCODE", "LOADSTATUS", "LOADTIME", "TURCKNO", "CUSTOMERDOC", "UNLOADSTATUS", "UNLOADTIME", "PODRECEIVED"};
            mappingStrategy.setColumnMapping(columns);

            CsvToBean<ProductDetail> csv = new CsvToBean<ProductDetail>();
            List<ProductDetail> productList = csv.parse(mappingStrategy, reader);

            for (int i = 0; i < productList.size(); i++) {
                ProductDetail productDetail = productList.get(i);
                // display CSV values
                System.out.println("awbNumber : " + productDetail.getAWB());
                System.out.println("parcel : " + productDetail.getParcel());
                System.out.println("origin: " + productDetail.getOrigin());
                System.out.println("destination: " + productDetail.getDestination());
                System.out.println("barcode: " + productDetail.getBarcode());
                System.out.println("loadStatus: " + productDetail.getLoadStatus());
                System.out.println("loadedTime: " + productDetail.getLOADTIME());
                System.out.println("truckNo: " + productDetail.getTURCKNO());
                System.out.println("customerDocRef: " + productDetail.getCUSTOMERDOC());
                System.out.println("unloadStatus: " + productDetail.getUnloadStatus());
                System.out.println("unloadTime: " + productDetail.getUnloadTime());
                System.out.println("podRecieved: " + productDetail.getPODRECEIVED());
                System.out.println("------------------------------");
                barCode = productDetail.getBarcode();
                airWiseBillNo = productDetail.getAWB();
                origin = productDetail.getOrigin();
                destination = productDetail.getDestination();
                vehicleNo = productDetail.getTURCKNO();
                loadStatus = productDetail.getLoadStatus();
                if (loadStatus == null) {
                    loadStatus = "";
                }
                loadTime = productDetail.getLOADTIME();
                customerDoc = productDetail.getCUSTOMERDOC();
                if (unloadStatus == null) {
                    unloadStatus = "";
                }
                if (unloadTime == null) {
                    unloadTime = "";
                }
                if (podRecieved == null) {
                    podRecieved = "";
                }
                unloadStatus = productDetail.getUnloadStatus();
                unloadTime = productDetail.getUnloadTime();
                podRecieved = productDetail.getPODRECEIVED();
                String fileName = "jdbc_url.properties";
                Properties dbProps = new Properties();
                InputStream is = getClass().getResourceAsStream("/" + fileName);
                dbProps.load(is);//this may throw IOException
                String dbClassName = dbProps.getProperty("jdbc.driverClassName");
                String dbUrl = dbProps.getProperty("jdbc.url");
                String dbUserName = dbProps.getProperty("jdbc.username");
                String dbPassword = dbProps.getProperty("jdbc.password");
                int updateStatus = 0;
                Connection con = null;
                try {
                    Class.forName(dbClassName).newInstance();
                    con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
                    try {
                        if ("1".equals(LOADINGSTATUS)) {
                            PreparedStatement updateAirBillBarcodeDetails = null;
                            PreparedStatement updateConsignmentOrderstatus = null;
                            String sql = "UPDATE \n"
                                    + "TS_CNOTE_AIRBILL_BARCODE SET \n"
                                    + "customer_doc='" + customerDoc + "',load_status='" + loadStatus + "', load_time='" + loadTime + "',truck_no='" + vehicleNo + "' \n"
                                    + "where Consignment_Order_Id=" + CONSIGNMENTORDERID + " and awb_no='" + airWiseBillNo + "' and bar_code='" + barCode + "' \n";
                            System.out.println("sql = " + sql);
                            updateAirBillBarcodeDetails = con.prepareStatement(sql);
                            updateStatus = updateAirBillBarcodeDetails.executeUpdate();
                            System.out.println("updateStatus = " + updateStatus);
                            if (updateStatus != 0) {
                                String sql1 = "UPDATE\n"
                                        + "TS_CONSIGNMENT_NOTE SET awb_download_status = 2, MODIFIED_ON = now()   \n"
                                        + "where Consignment_Order_Id=" + CONSIGNMENTORDERID + "\n";
                                updateConsignmentOrderstatus = con.prepareStatement(sql1);
                                updateStatus = updateConsignmentOrderstatus.executeUpdate();
                            }
                        } else {
                            PreparedStatement updateUnloadAirBillBarcodeDetails = null;
                            PreparedStatement updateUnloadConsignmentOrderstatus = null;
                            String sql = "UPDATE \n"
                                    + "TS_CNOTE_AIRBILL_BARCODE SET \n"
                                    + "customer_doc='" + customerDoc + "',load_status='" + loadStatus + "', load_time='" + loadTime + "',truck_no='" + vehicleNo + "',unload_status='" + unloadStatus + "', unload_time='" + unloadTime + "',pod_recieved='" + podRecieved + "' \n"
                                    + "where Consignment_Order_Id=" + CONSIGNMENTORDERID + " and awb_no='" + airWiseBillNo + "' and bar_code='" + barCode + "' \n";
                            System.out.println("sql = " + sql);
                            updateUnloadAirBillBarcodeDetails = con.prepareStatement(sql);
                            updateStatus = updateUnloadAirBillBarcodeDetails.executeUpdate();
                            System.out.println("updateStatus = " + updateStatus);
                            if (updateStatus != 0) {
                                String sql1 = "UPDATE\n"
                                        + "TS_CONSIGNMENT_NOTE SET awb_download_status =3, MODIFIED_ON = now()   \n"
                                        + "where Consignment_Order_Id=" + CONSIGNMENTORDERID + "\n";
                                updateUnloadConsignmentOrderstatus = con.prepareStatement(sql1);
                                updateStatus = updateUnloadConsignmentOrderstatus.executeUpdate();
                            }
                        }

                    } catch (SQLException e) {
                        e.printStackTrace();
                        System.out.println("Table doesn't exist.");
                    }
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException ex) {
            Logger.getLogger(ParseCSV2Bean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
