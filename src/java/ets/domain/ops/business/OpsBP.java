/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.ops.business;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.ops.data.OpsDAO;
import java.util.ArrayList;
import java.util.Iterator;
import ets.arch.util.FPUtil;
import java.io.IOException;
import java.util.Map;
import java.util.List;
import java.io.File;
import java.io.*;
import jxl.*;
import java.util.*;
import jxl.Workbook;
import jxl.read.biff.*;
import com.ibatis.sqlmap.client.*;
import ets.domain.operation.business.OperationTO;

/**
 *
 * @author vijay
 */
public class OpsBP {

    static FPUtil fpUtil = FPUtil.getInstance();
    static final int poTextSplitSize = Integer.parseInt(fpUtil.getInstance().getProperty("PO_ADDRESS_SPLIT"));
    OpsDAO opsDAO;

    public OpsDAO getOpsDAO() {
        return opsDAO;
    }

    public void setOpsDAO(OpsDAO opsDAO) {
        this.opsDAO = opsDAO;
    }

    public ArrayList getVehicleTypeList() throws FPRuntimeException, FPBusinessException {

        ArrayList MfrList = new ArrayList();
        MfrList = opsDAO.getVehicleTypeList();
        return MfrList;
    }

    public ArrayList getCityList(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList cityList = new ArrayList();
        cityList = opsDAO.getCityList(opsTO);
        return cityList;
    }
    public ArrayList getDestinationCityList(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList cityList = new ArrayList();
        cityList = opsDAO.getDestinationCityList(opsTO);
        return cityList;
    }
    public ArrayList getAirLineName(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList cityList = new ArrayList();
        cityList = opsDAO.getAirLineName(opsTO);
        return cityList;
    }

    public ArrayList getCityCodeList(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList cityList = new ArrayList();
        cityList = opsDAO.getCityCodeList(opsTO);
        return cityList;
    }
    public ArrayList getCityCodeList1(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList cityList = new ArrayList();
        cityList = opsDAO.getCityCodeList1(opsTO);
        return cityList;
    }

    public ArrayList getChargeableWeight(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList chargeableWeight = new ArrayList();
        chargeableWeight = opsDAO.getChargeableWeight(opsTO);
        return chargeableWeight;
    }

    public ArrayList getVehTypeVehicleRegNo(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList chargeableWeight = new ArrayList();
        chargeableWeight = opsDAO.getVehTypeVehicleRegNo(opsTO);
        return chargeableWeight;
    }

    public ArrayList getCustomerContractDetails(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList contractDetails = new ArrayList();
        contractDetails = opsDAO.getCustomerContractDetails(opsTO);
        return contractDetails;
    }

    public ArrayList getContractRouteDetails(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList contractDetails = new ArrayList();
        contractDetails = opsDAO.getContractRouteDetails(opsTO);
        return contractDetails;
    }

    public ArrayList getContractWeightRouteDetails(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList contractDetails = new ArrayList();
        contractDetails = opsDAO.getContractWeightRouteDetails(opsTO);
        return contractDetails;
    }

    public int insertCustomerContractDetails(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        int insertContract = 0;
        int insertContractFullTruck = 0;
        int insertContractWeightBreak = 0;
        SqlMapClient session = opsDAO.getSqlMapClient();
        insertContract = opsDAO.insertCustomerContractDetails(opsTO, session);
        if (insertContract > 0) {
            opsTO.setContractId(String.valueOf(insertContract));
            insertContractFullTruck = opsDAO.insertContractFullTruck(opsTO, session);
            insertContractWeightBreak = opsDAO.insertContractWeightBreak(opsTO, session);
        }
        return insertContract;
    }

    public ArrayList getChargeableRateValue(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList chargableAmount = new ArrayList();
        chargableAmount = opsDAO.getChargeableRateValue(opsTO);
        return chargableAmount;
    }

    public ArrayList getFullTruckRateValue(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList chargableAmount = new ArrayList();
        chargableAmount = opsDAO.getFullTruckRateValue(opsTO);
        return chargableAmount;
    }

    public ArrayList getAvailableWeightAndVolume(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList volumeDetails = new ArrayList();
        volumeDetails = opsDAO.getAvailableWeightAndVolume(opsTO);
        return volumeDetails;
    }

    public ArrayList getAvailableWeightAndVolumeEdit(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList volumeDetails = new ArrayList();
        volumeDetails = opsDAO.getAvailableWeightAndVolumeEdit(opsTO);
        return volumeDetails;
    }

    public ArrayList getTruckCodeList(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList chargeableWeight = new ArrayList();
        //chargeableWeight = opsDAO.getVehTypeVehicleRegNo(opsTO);
        chargeableWeight = opsDAO.getTruckCodeList(opsTO);
        return chargeableWeight;
    }

    public int getAwbStatus(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {

        //chargeableWeight = opsDAO.getVehTypeVehicleRegNo(opsTO);
        int flag = opsDAO.getAwbStatus(opsTO);
        return flag;
    }

    public ArrayList getFleetCode(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList chargeableWeight = new ArrayList();
        //chargeableWeight = opsDAO.getVehTypeVehicleRegNo(opsTO);
        chargeableWeight = opsDAO.getFleetCode(opsTO);
        return chargeableWeight;
    }

    public int updateCustomerContractDetails(String contractIds, OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        int insertContract = 0;
        SqlMapClient session = opsDAO.getSqlMapClient();
        opsTO.setContractId(contractIds);
        insertContract = opsDAO.insertContractFullTruck(opsTO, session);
        insertContract = opsDAO.insertContractWeightBreak(opsTO, session);
        insertContract = opsDAO.insertCustomerContract(opsTO);

        return insertContract;
    }

    public int insertCustomerContract(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        int insertContract = 0;
        insertContract = opsDAO.insertCustomerContract(opsTO);
        return insertContract;
    }

    public int updateContractFullTruck(String[] contractId, String[] rateContractId, String[] routeContractId, String[] rateWithReeferTruck, String[] rateWithOutReeferTruck, String[] activeIndRate) throws FPRuntimeException, FPBusinessException {
        int insertContractFullTruck = 0;
        insertContractFullTruck = opsDAO.updateContractFullTruck(contractId, rateContractId, routeContractId, rateWithReeferTruck, rateWithOutReeferTruck, activeIndRate);
        return insertContractFullTruck;
    }

    public int updateContractWeightBreak(String[] weightContractId, String[] weightRateContractId, String[] weightRouteContractId,String[] fromKgWeight, String[] toKgWeight, String[] rateWithReeferWeight, String[] rateWithOutReeferWeight, String[] activeIndWeight) throws FPRuntimeException, FPBusinessException {
        int insertContractWeightBreak = 0;
        insertContractWeightBreak = opsDAO.updateContractWeightBreak(weightContractId, weightRateContractId, weightRouteContractId, fromKgWeight, toKgWeight, rateWithReeferWeight, rateWithOutReeferWeight, activeIndWeight);
        return insertContractWeightBreak;
    }

    public ArrayList getTsaViewList(String consignmnetIds, String tsaType) throws FPRuntimeException, FPBusinessException {
        ArrayList tsaViewList = new ArrayList();
        tsaViewList = opsDAO.getTsaViewList(consignmnetIds, tsaType);
        return tsaViewList;
    }

    public int insertConsignmentCustomsCheck(OpsTO opsTO, String[] receivedPackages, String[] receivedWeight, String[] routeContractId, String[] igmNo, String[] flightNumber, String[] hawbNo) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = opsDAO.insertConsignmentCustomsCheck(opsTO, receivedPackages, receivedWeight, routeContractId, igmNo, flightNumber, hawbNo);
        return insertStatus;
    }

    public ArrayList getCustomsCheckViewList(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList customsCheckViewList = new ArrayList();
        customsCheckViewList = opsDAO.getCustomsCheckViewList(opsTO);
        return customsCheckViewList;
    }

    public ArrayList getConsignmentCustomsCheck(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consignmentCustomsCheck = new ArrayList();
        consignmentCustomsCheck = opsDAO.getConsignmentCustomsCheck(opsTO);
        return consignmentCustomsCheck;
    }

    public int insertConsignmentManifest(OpsTO opsTO, String[] receivedPackages, String[] receivedWeight, String[] routeContractId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = opsDAO.insertConsignmentManifest(opsTO, receivedPackages, receivedWeight, routeContractId);
        return insertStatus;
    }

    public ArrayList getTsaPrintList(String fromDate, String toDate, String tsaManifestCode, String branchId, String roleId) throws FPRuntimeException, FPBusinessException {
        ArrayList tsaPrintList = new ArrayList();
        tsaPrintList = opsDAO.getTsaPrintList(fromDate, toDate, tsaManifestCode, branchId, roleId);
        return tsaPrintList;
    }

    public ArrayList getManifestPrintList(String fromDate, String toDate, String tsaManifestCode, String branchId, String roleId) throws FPRuntimeException, FPBusinessException {
        ArrayList manifestPrintList = new ArrayList();
        manifestPrintList = opsDAO.getManifestPrintList(fromDate, toDate, tsaManifestCode, branchId, roleId);
        return manifestPrintList;
    }

    public ArrayList getManifestViewList(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList manifestViewList = new ArrayList();
        manifestViewList = opsDAO.getManifestViewList(opsTO);
        return manifestViewList;
    }

    public ArrayList getConsignmentManifest(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consignmentManifest = new ArrayList();
        consignmentManifest = opsDAO.getConsignmentManifest(opsTO);
        return consignmentManifest;
    }

    public String getSrcFilePath() throws FPRuntimeException, FPBusinessException {
        String srcFilePath = "";
        srcFilePath = opsDAO.getSrcFilePath();
        return srcFilePath;
    }

    public ArrayList getTruckNameList() throws FPRuntimeException, FPBusinessException {
        ArrayList truckNameList = new ArrayList();
        truckNameList = opsDAO.getTruckNameList();
        return truckNameList;
    }

    public ArrayList getTruckCapacityList(String truckCodeId) throws FPRuntimeException, FPBusinessException {
        ArrayList truckCapacityList = new ArrayList();
        truckCapacityList = opsDAO.getTruckCapacityList(truckCodeId);
        return truckCapacityList;
    }

    public int removeConsignmentNoteRoute(String consignmentNoteRouteId) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 0;
        updateStatus = opsDAO.removeConsignmentNoteRoute(consignmentNoteRouteId);
        return updateStatus;
    }

    public ArrayList getTsaList(String loadDate, String loadToDate, String branchId, String roleId) throws FPRuntimeException, FPBusinessException {
        ArrayList getTsaList = new ArrayList();
        getTsaList = opsDAO.getTsaList(loadDate, loadToDate, branchId, roleId);
        return getTsaList;
    }

    public ArrayList getFleetCodeGroup(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList fleetCodeGroup = new ArrayList();
        fleetCodeGroup = opsDAO.getFleetCodeGroup(opsTO);
        return fleetCodeGroup;
    }

    public ArrayList getConsignmentOrderRate(String consignmentOrderId) throws FPRuntimeException, FPBusinessException {
        ArrayList list = new ArrayList();
        list = opsDAO.getConsignmentOrderRate(consignmentOrderId);
        return list;
    }

    public int saveEditNonContractOrderRate(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        SqlMapClient session = opsDAO.getSqlMapClient();
        try {
            session.startTransaction();
            status = opsDAO.saveEditNonContractOrderRate(opsTO, session);

            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }

    public int saveOtlSealNo(String consignmentOrderId, String otlNo) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 0;
        updateStatus = opsDAO.saveOtlSealNo(consignmentOrderId, otlNo);
        return updateStatus;
    }

    public int insertDelayTime(OpsTO opsTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = opsDAO.insertDelayTime(opsTO, userId);
        return insertStatus;
    }

    public int saveRoyaltyPayMaster(OpsTO opsTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = opsDAO.saveRoyaltyPayMaster(opsTO, userId);
        return insertStatus;
    }

    public int saveServiceTaxMaster(OpsTO opsTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = opsDAO.saveServiceTaxMaster(opsTO, userId);
        return insertStatus;
    }

    public ArrayList getDelayTimeList(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList delayTimeList = new ArrayList();
        delayTimeList = opsDAO.getDelayTimeList(opsTO);
        return delayTimeList;
    }

    public ArrayList getZoneList(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList zoneList = new ArrayList();
        zoneList = opsDAO.getZoneList(opsTO);
        return zoneList;
    }

    public ArrayList getBranchList(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList branchList = new ArrayList();
        branchList = opsDAO.getBranchList(opsTO);
        return branchList;
    }

    public ArrayList getRoyaltyPayList(String branchId, String roleId) throws FPRuntimeException, FPBusinessException {
        ArrayList royaltyPayList = new ArrayList();
        royaltyPayList = opsDAO.getRoyaltyPayList(branchId, roleId);
        return royaltyPayList;
    }

    public ArrayList getOrderTruckList(String consignmentOrderId, String roleId) throws FPRuntimeException, FPBusinessException {
        ArrayList getOrderTruckList = new ArrayList();
        getOrderTruckList = opsDAO.getOrderTruckList(consignmentOrderId, roleId);
        return getOrderTruckList;
    }

    public ArrayList getServiceTaxList(String branchId, String roleId) throws FPRuntimeException, FPBusinessException {
        ArrayList serviceTaxList = new ArrayList();
        serviceTaxList = opsDAO.getServiceTaxList(branchId, roleId);
        return serviceTaxList;
    }

    public int saveZoneMaster(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = opsDAO.saveZoneMaster(opsTO);
        return insertStatus;
    }

    public int saveZoneList(String[] zoneName) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = opsDAO.saveZoneList(zoneName);
        return insertStatus;
    }

    public int saveCityList(String[] zoneName, String[] cityName, String[] cityCode, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = opsDAO.saveCityList(zoneName, cityName, cityCode, userId);
        return insertStatus;
    }

    public int saveBudgetList(String[] branchId, String[] movementType, String[] january, String[] febuary, String[] march, String[] april, String[] may, String[] june, String[] july, String[] august, String[] september, String[] october, String[] november, String[] december, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        System.out.println("Hi I am here 3....................");
        insertStatus = opsDAO.saveBudgetList(branchId, movementType, january, febuary, march, april, may, june, july, august, september, october, november, december, userId);
        return insertStatus;
    }

    public int saveExpenditureList(String[] branchId, String[] movementType, String[] january, String[] febuary, String[] march, String[] april, String[] may, String[] june, String[] july, String[] august, String[] september, String[] october, String[] november, String[] december, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = opsDAO.saveExpenditureList(branchId, movementType, january, febuary, march, april, may, june, july, august, september, october, november, december, userId);
        return insertStatus;
    }

    public int saveTripList(String[] branchId, String[] movementType, String[] january, String[] febuary, String[] march, String[] april, String[] may, String[] june, String[] july, String[] august, String[] september, String[] october, String[] november, String[] december, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = opsDAO.saveTripList(branchId, movementType, january, febuary, march, april, may, june, july, august, september, october, november, december, userId);
        return insertStatus;
    }

    public int saveRevenueList(String[] branchId, String[] movementType, String[] january, String[] febuary, String[] march, String[] april, String[] may, String[] june, String[] july, String[] august, String[] september, String[] october, String[] november, String[] december, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = opsDAO.saveRevenueList(branchId, movementType, january, febuary, march, april, may, june, july, august, september, october, november, december, userId);
        return insertStatus;
    }

    public String getAwbNo() throws FPRuntimeException, FPBusinessException {
        String awbNo = "";
        awbNo = opsDAO.getAwbNo();
        return awbNo;
    }

    public String getBranchId(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        String branchId = "";
        branchId = opsDAO.getBranchId(opsTO);
        return branchId;
    }

    public ArrayList getDepreciationChargeList(String branchId, String roleId) throws FPRuntimeException, FPBusinessException {
        ArrayList depreciationChargeList = new ArrayList();
        depreciationChargeList = opsDAO.getDepreciationChargeList(branchId, roleId);
        return depreciationChargeList;
    }

    public int saveDepreciationChargeMaster(OpsTO opsTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = opsDAO.saveDepreciationChargeMaster(opsTO, userId);
        return insertStatus;
    }

    public ArrayList getExpenseTypeList(String branchId, String roleId) throws FPRuntimeException, FPBusinessException {
        ArrayList expenseList = new ArrayList();
        expenseList = opsDAO.getExpenseTypeList(branchId, roleId);
        return expenseList;
    }

    public ArrayList getGpsDeviceList(String branchId, String roleId) throws FPRuntimeException, FPBusinessException {
        ArrayList deviceList = new ArrayList();
        deviceList = opsDAO.getGpsDeviceList(branchId, roleId);
        return deviceList;
    }

    public int saveExpenseTypeMaster(OpsTO opsTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = opsDAO.saveExpenseTypeMaster(opsTO, userId);
        return insertStatus;
    }

    public int saveGpsDeviceMaster(OpsTO opsTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = opsDAO.saveGpsDeviceMaster(opsTO, userId);
        return insertStatus;
    }

    public int saveMappingGpsDevice(OpsTO opsTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = opsDAO.saveMappingGpsDevice(opsTO, userId);
        return insertStatus;
    }

    public String getConsignmentDataList(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        String consignmentDataList = "";
        consignmentDataList = opsDAO.getConsignmentDataList(opsTO);
        return consignmentDataList;
    }

    public ArrayList getDeviceListDetails(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList deviceList = new ArrayList();
        deviceList = opsDAO.getDeviceListDetails(opsTO);
        return deviceList;
    }

    public ArrayList getCheckAwbNo(OpsTO opsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList checkAwbNo = new ArrayList();
        checkAwbNo = opsDAO.getCheckAwbNo(opsTO);
        return checkAwbNo;
    }
    
    public ArrayList getPointList() throws FPRuntimeException, FPBusinessException {
        ArrayList getPointList = new ArrayList();
        getPointList = opsDAO.getPointList();
        return getPointList;
    }
    
    public ArrayList getCustomerList() throws FPRuntimeException, FPBusinessException {
        ArrayList getCustomerList = new ArrayList();
        getCustomerList = opsDAO.getCustomerList();
        return getCustomerList;
    }

 
  public int insertConsignmentNote(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        int checkAwbStatus = 0;
        int insertStatusDeatils = 0;
        SqlMapClient session = opsDAO.getSqlMapClient();
        String awbNo = "";
        boolean sts = true;
        boolean transactionCommitStatus = false;
        try {
        session.startTransaction();
//        if(operationTO.getAwbType().equals("1")) {
//        checkAwbStatus = operationDAO.getAwbNoCheckExists(operationTO, userId, session);
//        if(checkAwbStatus == 0 && operationTO.getShipmentType().equals("1")){
//            sts = true;
//        }
//        if(checkAwbStatus >= 0 && operationTO.getShipmentType().equals("2")){
//            sts = true;
//        }
//        }else{
//        awbNo = operationDAO.getAwbNo(session);
//        operationTO.setAirWayBillNo(operationTO.getOrderReferenceAwb()+" "+awbNo.substring(0, 4)+" "+awbNo.substring(4));
//        operationTO.setAwbNo(awbNo);
//        sts = true;
//        }
        if(sts){
            insertStatus = opsDAO.insertConsignmentNote(operationTO, userId, session);
            operationTO.setConsignmentOrderId(String.valueOf(insertStatus));
            if(insertStatus > 0) {
                insertStatus = opsDAO.insertConsignmentArticle(operationTO, userId, session);
                //insertStatus = opsDAO.insertMultiplePoints(operationTO, userId, session);
                System.out.println("InsertStatus Here = " + insertStatus);
//                if(insertStatus == 0){
                    insertStatus = 1;
                   transactionCommitStatus = true;
//                }else{
//                    // Choosen Truck has already used, So that your booking not aloowed 
//                    insertStatus = 3;
//                }
            }
        }else{
            insertStatus = 2;
        }
        if(transactionCommitStatus){
            insertStatus = 1;
            session.commitTransaction();
        }
        } catch (Exception e) {
            e.printStackTrace();
            insertStatus = 0;
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                insertStatus = 0;
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return insertStatus;
    }
  
  public ArrayList getConsignmentOrderLists(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getConsignmentOrderLists = new ArrayList();
        getConsignmentOrderLists = opsDAO.getConsignmentOrderLists(operationTO);
        return getConsignmentOrderLists;
    }
  
  public ArrayList getConsignmentArticleList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getConsignmentArticleList = new ArrayList();
        getConsignmentArticleList = opsDAO.getConsignmentArticleList(operationTO);
        return getConsignmentArticleList;
    }
  
  public int updateconsignmentList(OpsTO opsTO,OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        int updateconsignmentList = 0;
        updateconsignmentList = opsDAO.updateconsignmentList(opsTO,operationTO);
        return updateconsignmentList;
    }
}
