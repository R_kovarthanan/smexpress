
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.ops.business;

import java.util.ArrayList;
import java.io.*;

/**
 * chni
 *
 * @author vijay
 */
public class OpsTO implements Serializable {
    
    private String[] noOfPieces = null;
    private String rateMode1="";
    private String[] chargeableWeightId = null;
    private String[] volumes = null;
    private String[] heights = null;
    private String[] lengths = null;
    private String[] widths = null;
    private String[] contractId1 = null;
    private String contractId2 = "";
    private String rateValue1 = "";
    
    private String consignmentOrderDate = "";
    private String movementTypeId = "";
    private String orderDeliveryTime = "";
    private String orderDeliveryDate = "";
    private String productCategory = "";
    private String pendingWeight = "";
    private String consignmentoriginname = "";
    private String consignmentdestinationname = "";
    private String AWBoriginname = "";
    private String AWBdestinationname = "";
    private String breadth = "";
    private String consignmentArticleIds = "";
    private String awbMovementType = "";
    private String awbType = "";
    private String orderReferenceAwb = "";
    private String orderReferenceAwbNo = "";
    private String orderReferenceEnd = "";
    private String awbOrderDeliveryDate = "";
    private String orderReferenceNo = "";
    private String totalWeightage = "";
    private String totalChargeableWeights = "";
    
    
    private String vehicleTonnage = "";
    private String vehicleCapacity = "";
    private String assignedWeight = "";
    private String ratePerKg = "";
    private String tspInput = "";
    private String payBillTsp = "";
    private String tspOriginPaidTo = "";
    private String tspDestinationPaidTo = "";
    private String tspOriginAmount = "";
    private String tspDestinationAmount = "";

    private String tsrInput = "";
    private String tsrOriginPaidTo = "";
    private String tsrDestinationPaidTo = "";
    private String tsrOriginAmount = "";
    private String tsrDestinationAmount = "";
    private String tsrOriginPercent = "";
    private String tsrDestPercent = "";

    private String screeningInput = "";
    private String payBillScreen = "";
    private String scOriginPaidTo = "";
    private String scDestinationPaidTo = "";
    private String scOriginAmount = "";
    private String scDestinationAmount = "";

    private String totTSPrate = "";
    private String totTSRrate = "";
    private String totSCRrate = "";
    private String totIncentiveAmount = "";
    private String totCustAmount = "";
    private String totTTAamount = "";
    private String totalChargableWeight = "";
    private String originRoyaltyPercent = "";
    private String destinationRoyaltyPercent = "";
    private String[] routeCourseIds = null;
    private String[] chargeableWeights = null;
    private String[] freightCharges = null;
    private String pgRate = "";
    private String totalChargeableWeight = "";
    private String createdOn = "";
    private String totSCrate = "";

//    private String movementType = "";
    private String freightCharge = "";
    private String routeCourseId = "";
    private String tripCode = "";
    private String regNo = "";
    private String routeInfo = "";
    private String grossWeight = "";
    private String chargeableWeight = "";
    private String vehicleCost = "";
    private String awbTotalGrossWeight = "";
    private String awbTotalPackages = "";
    private String awbReceivedPackages = "";
    private String awbPendingPackages = "";
    private String awbNo = "";
    private String deviceId = "";
    private String deviceName = "";
    private String vendorId = "";
    private String truckDeptTime = "";
    private String truckArrTime = "";
    private String truckArrDate = "";
    private String vendorName = "";
    private String expenseType = "";
    private String expenseId = "";
    private String expenseName = "";
    private String depreciationChargeName = "";
    private String percentage = "";
    private String depreciationChargeId = "";
    private String serviceTaxId = "";
    private String validFrom = "";
    private String validTo = "";
    private String serviceTaxAmount = "";
    private String paidByScreenCharge = "";
    private String paidByTsp = "";
    private String paidByTsr = "";
    private String paidId = "";
    private String paidBy = "";
    private String hawbNo = "";
    private String igmNo = "";
    private String consignorName = "";
    private String january = "";
    private String febuary = "";
    private String march = "";
    private String april = "";
    private String may = "";
    private String june = "";
    private String july = "";
    private String august = "";
    private String september = "";
    private String october = "";
    private String november = "";
    private String december = "";
    private String monthName = "";
    private String branchName = "";
    private String screenChargePaidBy = "";
    private String incentiveCharge = "";
    private String screenCharge = "";
    private String receivedStatus = "";
    private String airlineName = "";
    private String tsaGenStatus = "";
    private String manifestGenStatus = "";
    private String generateStatus = "";
    private String receivedPackages = "";
    private String receivedWeight = "";
    private String branchId = "";
    private String zoneId = "";
    private String zoneName = "";
    private String cityCode = "";
    private String payBillTsr = "";
    private String statusName = "";
    private String statusId = "";
    private String delayTimeId = "";
    private String delayTime = "";
    private String fullTruckValue = "";
    private String rateMode = "";
    private String payBill = "";
    private String tspPerkg = "";
    private String tsrPerkg = "";
    private String customerNetAmount = "";
    private int userId = 0;
    private String efoNo = "";
    private String shipType = "";
    private String awbOriginId = "";
    private String shipmentType = "";
    private String consignmentEfoNo = "";
    private String consignmentNoteEmno = "";
    private String consignmentNoteAtdNo = "";
    private String[] totalTsaGoods;
    private String awbDestinationId = "";
    private String routeContractId = "";
    private String rateContractId = "";
    private String chargableWeight = "";
    private String effectiveDate = "";
    private String movementType = "";
    private String weightFromKg = "";
    private String weightToKg = "";
    private String rateWithReefer = "";
    private String rateWithoutReefer = "";
    private String routeContractCode = "";
    private String vehicleTypeName = "";
    private String firstPointName = "";
    private String finalPointName = "";
    private String customerName = "";
    private String customerId = "";
    private String customerCode = "";
    private String billingTypeId = "";
    private String billingTypeName = "";
    private String contractFrom = "";
    private String contractTo = "";
    private String contractNo = "";
    private String contractId = "";
    private String[] originIdFullTruck = null;
    private String[] originNameFullTruck = null;
    private String[] destinationIdFullTruck = null;
    private String[] destinationNameFullTruck = null;
    private String[] routeIdFullTruck = null;
    private String[] travelKmFullTruck = null;
    private String[] travelHourFullTruck = null;
    private String[] travelMinuteFullTruck = null;
    private String[] movementTypeIdFullTruck = null;
    private String[] vehicleTypeIdFullTruck = null;
    private String[] rateWithReeferFullTruck = null;
    private String[] rateWithOutReeferFullTruck = null;
    private String[] originIdWeightBreak = null;
    private String[] originNameWeightBreak = null;
    private String[] destinationIdWeightBreak = null;
    private String[] destinationNameWeightBreak = null;
    private String[] routeIdWeightBreak = null;
    private String[] travelKmWeightBreak = null;
    private String[] travelHourWeightBreak = null;
    private String[] travelMinuteWeightBreak = null;
    private String[] movementTypeIdWeightBreak = null;
    private String[] fromKgWeightBreak = null;
    private String[] toKgWeightBreak = null;
    private String[] rateWithReeferWeightBreak = null;
    private String[] rateWithOutReeferWeightBreak = null;
    private String[] usedVol;
    private String[] usedCapacity;
    private String[] uoms = null;

    private String productRate = "";
    private String contract = "";
    private String rateValue = "";
    private String vehicleRegNo = "";
    private String vehicleId = "";
    private String vehicleTypeId = "";
    private String uom = "";
    private String cityId = "";
    private String previousCityId = "";
    private String cityName = "";
    private String lengthCm = "";
    private String widthCm = "";
    private String heightCm = "";
    private String totalVolume = "";
    private String weightPerVolume = "";
    private String totalWeight = "";
    private String activeInd = "";
    private String cubicWeightId = "";
    private String travelKm = "";
    private String travelHours = "";
    private String travelMinutes = "";
    private String routeId = "";
    private String truckDepDate = "";
    private String consignmentAwbNo = "";
    private String consignmentOrderId = "";
    private String consigneeName = "";
    private String commodity = "";
    private String awbOriginName = "";
    private String awbDestinationName = "";
    private String totalPackages = "";
    private String totalCost = "";
    private String customStatus = "";
    private String customsCheckId = "";
    private String[] consignmentArticleId;
    private String fromAddress = "";
    private String toAddress = "";
    private String fleetDetails = "";
    private String customsId = "";
    private String customsRemark = "";
    private String[] consignmentSbNo;
    private String customsSbNo;
    private String[] consignmentOrderNos;
    private String flightDate = "";
    private String consignmentTpNo = "";
    private String consignmentEgmNo = "";
    private String manifestStatus = "";
    private String operatorName = "";
    private String flightNo = "";
    private String manifestDate = "";
    private String lodingPoint = "";
    private String unLodingPoint = "";
    private String customsDate = "";
    private String customsCode = "";
    private String manifestCode = "";
    private String manifestId = "";
    private String truckCurrentVol = "";
    private String truckCurrentCap = "";
    private String truckVol = "";
    private String fleetTypeId = "";
    private String fleetCapacity = "";
    private String fleetStatus = "";
    private String usedTruckCapcity = "";
    private String truckCapacity = "";
    private String truckCapacityTemp = "";
    private String usedTruckVol = "";
    private String truckId = "";
    private String truckName = "";
    private String fleetVolume = "";
    private String fleetCode = "";
    private String awbOriginRegion = "";
    private String awbno = "";
    private String cNoteNo = "";
    private String origin = "";
    private String destination = "";
    private String loadDate = "";
    private String consignmentOrderStatus = "";
    private String consignmentDate = "";
    private String fleetCodeId = "";
    private String fleetCode1 = "";
    private String otlNo = "";
    private String efo_No = "";
    private String transhipmentNo = "";

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCubicWeightId() {
        return cubicWeightId;
    }

    public void setCubicWeightId(String cubicWeightId) {
        this.cubicWeightId = cubicWeightId;
    }

    public String getHeightCm() {
        return heightCm;
    }

    public void setHeightCm(String heightCm) {
        this.heightCm = heightCm;
    }

    public String getLengthCm() {
        return lengthCm;
    }

    public void setLengthCm(String lengthCm) {
        this.lengthCm = lengthCm;
    }

    public String getPreviousCityId() {
        return previousCityId;
    }

    public void setPreviousCityId(String previousCityId) {
        this.previousCityId = previousCityId;
    }

    public String getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(String totalVolume) {
        this.totalVolume = totalVolume;
    }

    public String getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(String totalWeight) {
        this.totalWeight = totalWeight;
    }

    public String getTravelHours() {
        return travelHours;
    }

    public void setTravelHours(String travelHours) {
        this.travelHours = travelHours;
    }

    public String getTravelKm() {
        return travelKm;
    }

    public void setTravelKm(String travelKm) {
        this.travelKm = travelKm;
    }

    public String getTravelMinutes() {
        return travelMinutes;
    }

    public void setTravelMinutes(String travelMinutes) {
        this.travelMinutes = travelMinutes;
    }

    public String getWeightPerVolume() {
        return weightPerVolume;
    }

    public void setWeightPerVolume(String weightPerVolume) {
        this.weightPerVolume = weightPerVolume;
    }

    public String getWidthCm() {
        return widthCm;
    }

    public void setWidthCm(String widthCm) {
        this.widthCm = widthCm;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getVehicleRegNo() {
        return vehicleRegNo;
    }

    public void setVehicleRegNo(String vehicleRegNo) {
        this.vehicleRegNo = vehicleRegNo;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getProductRate() {
        return productRate;
    }

    public void setProductRate(String productRate) {
        this.productRate = productRate;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getRateValue() {
        return rateValue;
    }

    public void setRateValue(String rateValue) {
        this.rateValue = rateValue;
    }

    public String getContractFrom() {
        return contractFrom;
    }

    public void setContractFrom(String contractFrom) {
        this.contractFrom = contractFrom;
    }

    public String getContractTo() {
        return contractTo;
    }

    public void setContractTo(String contractTo) {
        this.contractTo = contractTo;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String[] getOriginIdFullTruck() {
        return originIdFullTruck;
    }

    public void setOriginIdFullTruck(String[] originIdFullTruck) {
        this.originIdFullTruck = originIdFullTruck;
    }

    public String[] getOriginNameFullTruck() {
        return originNameFullTruck;
    }

    public void setOriginNameFullTruck(String[] originNameFullTruck) {
        this.originNameFullTruck = originNameFullTruck;
    }

    public String[] getDestinationIdFullTruck() {
        return destinationIdFullTruck;
    }

    public void setDestinationIdFullTruck(String[] destinationIdFullTruck) {
        this.destinationIdFullTruck = destinationIdFullTruck;
    }

    public String[] getDestinationNameFullTruck() {
        return destinationNameFullTruck;
    }

    public void setDestinationNameFullTruck(String[] destinationNameFullTruck) {
        this.destinationNameFullTruck = destinationNameFullTruck;
    }

    public String[] getRouteIdFullTruck() {
        return routeIdFullTruck;
    }

    public void setRouteIdFullTruck(String[] routeIdFullTruck) {
        this.routeIdFullTruck = routeIdFullTruck;
    }

    public String[] getTravelKmFullTruck() {
        return travelKmFullTruck;
    }

    public void setTravelKmFullTruck(String[] travelKmFullTruck) {
        this.travelKmFullTruck = travelKmFullTruck;
    }

    public String[] getTravelHourFullTruck() {
        return travelHourFullTruck;
    }

    public void setTravelHourFullTruck(String[] travelHourFullTruck) {
        this.travelHourFullTruck = travelHourFullTruck;
    }

    public String[] getTravelMinuteFullTruck() {
        return travelMinuteFullTruck;
    }

    public void setTravelMinuteFullTruck(String[] travelMinuteFullTruck) {
        this.travelMinuteFullTruck = travelMinuteFullTruck;
    }

    public String[] getMovementTypeIdFullTruck() {
        return movementTypeIdFullTruck;
    }

    public void setMovementTypeIdFullTruck(String[] movementTypeIdFullTruck) {
        this.movementTypeIdFullTruck = movementTypeIdFullTruck;
    }

    public String[] getVehicleTypeIdFullTruck() {
        return vehicleTypeIdFullTruck;
    }

    public void setVehicleTypeIdFullTruck(String[] vehicleTypeIdFullTruck) {
        this.vehicleTypeIdFullTruck = vehicleTypeIdFullTruck;
    }

    public String[] getOriginIdWeightBreak() {
        return originIdWeightBreak;
    }

    public void setOriginIdWeightBreak(String[] originIdWeightBreak) {
        this.originIdWeightBreak = originIdWeightBreak;
    }

    public String[] getOriginNameWeightBreak() {
        return originNameWeightBreak;
    }

    public void setOriginNameWeightBreak(String[] originNameWeightBreak) {
        this.originNameWeightBreak = originNameWeightBreak;
    }

    public String[] getDestinationIdWeightBreak() {
        return destinationIdWeightBreak;
    }

    public void setDestinationIdWeightBreak(String[] destinationIdWeightBreak) {
        this.destinationIdWeightBreak = destinationIdWeightBreak;
    }

    public String[] getDestinationNameWeightBreak() {
        return destinationNameWeightBreak;
    }

    public void setDestinationNameWeightBreak(String[] destinationNameWeightBreak) {
        this.destinationNameWeightBreak = destinationNameWeightBreak;
    }

    public String[] getRouteIdWeightBreak() {
        return routeIdWeightBreak;
    }

    public void setRouteIdWeightBreak(String[] routeIdWeightBreak) {
        this.routeIdWeightBreak = routeIdWeightBreak;
    }

    public String[] getTravelKmWeightBreak() {
        return travelKmWeightBreak;
    }

    public void setTravelKmWeightBreak(String[] travelKmWeightBreak) {
        this.travelKmWeightBreak = travelKmWeightBreak;
    }

    public String[] getTravelHourWeightBreak() {
        return travelHourWeightBreak;
    }

    public void setTravelHourWeightBreak(String[] travelHourWeightBreak) {
        this.travelHourWeightBreak = travelHourWeightBreak;
    }

    public String[] getTravelMinuteWeightBreak() {
        return travelMinuteWeightBreak;
    }

    public void setTravelMinuteWeightBreak(String[] travelMinuteWeightBreak) {
        this.travelMinuteWeightBreak = travelMinuteWeightBreak;
    }

    public String[] getMovementTypeIdWeightBreak() {
        return movementTypeIdWeightBreak;
    }

    public void setMovementTypeIdWeightBreak(String[] movementTypeIdWeightBreak) {
        this.movementTypeIdWeightBreak = movementTypeIdWeightBreak;
    }

    public String[] getFromKgWeightBreak() {
        return fromKgWeightBreak;
    }

    public void setFromKgWeightBreak(String[] fromKgWeightBreak) {
        this.fromKgWeightBreak = fromKgWeightBreak;
    }

    public String[] getToKgWeightBreak() {
        return toKgWeightBreak;
    }

    public void setToKgWeightBreak(String[] toKgWeightBreak) {
        this.toKgWeightBreak = toKgWeightBreak;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getBillingTypeId() {
        return billingTypeId;
    }

    public void setBillingTypeId(String billingTypeId) {
        this.billingTypeId = billingTypeId;
    }

    public String[] getRateWithReeferFullTruck() {
        return rateWithReeferFullTruck;
    }

    public void setRateWithReeferFullTruck(String[] rateWithReeferFullTruck) {
        this.rateWithReeferFullTruck = rateWithReeferFullTruck;
    }

    public String[] getRateWithOutReeferFullTruck() {
        return rateWithOutReeferFullTruck;
    }

    public void setRateWithOutReeferFullTruck(String[] rateWithOutReeferFullTruck) {
        this.rateWithOutReeferFullTruck = rateWithOutReeferFullTruck;
    }

    public String[] getRateWithReeferWeightBreak() {
        return rateWithReeferWeightBreak;
    }

    public void setRateWithReeferWeightBreak(String[] rateWithReeferWeightBreak) {
        this.rateWithReeferWeightBreak = rateWithReeferWeightBreak;
    }

    public String[] getRateWithOutReeferWeightBreak() {
        return rateWithOutReeferWeightBreak;
    }

    public void setRateWithOutReeferWeightBreak(String[] rateWithOutReeferWeightBreak) {
        this.rateWithOutReeferWeightBreak = rateWithOutReeferWeightBreak;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getBillingTypeName() {
        return billingTypeName;
    }

    public void setBillingTypeName(String billingTypeName) {
        this.billingTypeName = billingTypeName;
    }

    public String getRateWithReefer() {
        return rateWithReefer;
    }

    public void setRateWithReefer(String rateWithReefer) {
        this.rateWithReefer = rateWithReefer;
    }

    public String getRateWithoutReefer() {
        return rateWithoutReefer;
    }

    public void setRateWithoutReefer(String rateWithoutReefer) {
        this.rateWithoutReefer = rateWithoutReefer;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public String getFirstPointName() {
        return firstPointName;
    }

    public void setFirstPointName(String firstPointName) {
        this.firstPointName = firstPointName;
    }

    public String getFinalPointName() {
        return finalPointName;
    }

    public void setFinalPointName(String finalPointName) {
        this.finalPointName = finalPointName;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getMovementType() {
        return movementType;
    }

    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    public String getWeightFromKg() {
        return weightFromKg;
    }

    public void setWeightFromKg(String weightFromKg) {
        this.weightFromKg = weightFromKg;
    }

    public String getWeightToKg() {
        return weightToKg;
    }

    public void setWeightToKg(String weightToKg) {
        this.weightToKg = weightToKg;
    }

    public String getRouteContractCode() {
        return routeContractCode;
    }

    public void setRouteContractCode(String routeContractCode) {
        this.routeContractCode = routeContractCode;
    }

    public String getChargableWeight() {
        return chargableWeight;
    }

    public void setChargableWeight(String chargableWeight) {
        this.chargableWeight = chargableWeight;
    }

    public String getRouteContractId() {
        return routeContractId;
    }

    public void setRouteContractId(String routeContractId) {
        this.routeContractId = routeContractId;
    }

    public String getRateContractId() {
        return rateContractId;
    }

    public void setRateContractId(String rateContractId) {
        this.rateContractId = rateContractId;
    }

    public String getAwbOriginId() {
        return awbOriginId;
    }

    public void setAwbOriginId(String awbOriginId) {
        this.awbOriginId = awbOriginId;
    }

    public String getAwbDestinationId() {
        return awbDestinationId;
    }

    public void setAwbDestinationId(String awbDestinationId) {
        this.awbDestinationId = awbDestinationId;
    }

    public String getTruckDepDate() {
        return truckDepDate;
    }

    public void setTruckDepDate(String truckDepDate) {
        this.truckDepDate = truckDepDate;
    }

    public String[] getConsignmentArticleId() {
        return consignmentArticleId;
    }

    public void setConsignmentArticleId(String[] consignmentArticleId) {
        this.consignmentArticleId = consignmentArticleId;
    }

    public String getAwbDestinationName() {
        return awbDestinationName;
    }

    public void setAwbDestinationName(String awbDestinationName) {
        this.awbDestinationName = awbDestinationName;
    }

    public String getAwbOriginName() {
        return awbOriginName;
    }

    public void setAwbOriginName(String awbOriginName) {
        this.awbOriginName = awbOriginName;
    }

    public String getCommodity() {
        return commodity;
    }

    public void setCommodity(String commodity) {
        this.commodity = commodity;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsignmentAwbNo() {
        return consignmentAwbNo;
    }

    public void setConsignmentAwbNo(String consignmentAwbNo) {
        this.consignmentAwbNo = consignmentAwbNo;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public String getTotalPackages() {
        return totalPackages;
    }

    public void setTotalPackages(String totalPackages) {
        this.totalPackages = totalPackages;
    }

    public String getCustomStatus() {
        return customStatus;
    }

    public void setCustomStatus(String customStatus) {
        this.customStatus = customStatus;
    }

    public String[] getConsignmentSbNo() {
        return consignmentSbNo;
    }

    public void setConsignmentSbNo(String[] consignmentSbNo) {
        this.consignmentSbNo = consignmentSbNo;
    }

    public String getCustomsRemark() {
        return customsRemark;
    }

    public void setCustomsRemark(String customsRemark) {
        this.customsRemark = customsRemark;
    }

    public String getFleetDetails() {
        return fleetDetails;
    }

    public void setFleetDetails(String fleetDetails) {
        this.fleetDetails = fleetDetails;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public String getConsignmentOrderId() {
        return consignmentOrderId;
    }

    public void setConsignmentOrderId(String consignmentOrderId) {
        this.consignmentOrderId = consignmentOrderId;
    }

    public String[] getConsignmentOrderNos() {
        return consignmentOrderNos;
    }

    public void setConsignmentOrderNos(String[] consignmentOrderNos) {
        this.consignmentOrderNos = consignmentOrderNos;
    }

    public String getCustomsCheckId() {
        return customsCheckId;
    }

    public void setCustomsCheckId(String customsCheckId) {
        this.customsCheckId = customsCheckId;
    }

    public String getConsignmentEgmNo() {
        return consignmentEgmNo;
    }

    public void setConsignmentEgmNo(String consignmentEgmNo) {
        this.consignmentEgmNo = consignmentEgmNo;
    }

    public String getConsignmentTpNo() {
        return consignmentTpNo;
    }

    public void setConsignmentTpNo(String consignmentTpNo) {
        this.consignmentTpNo = consignmentTpNo;
    }

    public String getFlightDate() {
        return flightDate;
    }

    public void setFlightDate(String flightDate) {
        this.flightDate = flightDate;
    }

    public String getManifestStatus() {
        return manifestStatus;
    }

    public void setManifestStatus(String manifestStatus) {
        this.manifestStatus = manifestStatus;
    }

    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public String getLodingPoint() {
        return lodingPoint;
    }

    public void setLodingPoint(String lodingPoint) {
        this.lodingPoint = lodingPoint;
    }

    public String getManifestDate() {
        return manifestDate;
    }

    public void setManifestDate(String manifestDate) {
        this.manifestDate = manifestDate;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getUnLodingPoint() {
        return unLodingPoint;
    }

    public void setUnLodingPoint(String unLodingPoint) {
        this.unLodingPoint = unLodingPoint;
    }

    public String getCustomsId() {
        return customsId;
    }

    public void setCustomsId(String customsId) {
        this.customsId = customsId;
    }

    public String getCustomsDate() {
        return customsDate;
    }

    public void setCustomsDate(String customsDate) {
        this.customsDate = customsDate;
    }

    public String getManifestId() {
        return manifestId;
    }

    public void setManifestId(String manifestId) {
        this.manifestId = manifestId;
    }

    public String getCustomsCode() {
        return customsCode;
    }

    public void setCustomsCode(String customsCode) {
        this.customsCode = customsCode;
    }

    public String getManifestCode() {
        return manifestCode;
    }

    public void setManifestCode(String manifestCode) {
        this.manifestCode = manifestCode;
    }

    public String getCustomsSbNo() {
        return customsSbNo;
    }

    public void setCustomsSbNo(String customsSbNo) {
        this.customsSbNo = customsSbNo;
    }

    public String[] getTotalTsaGoods() {
        return totalTsaGoods;
    }

    public void setTotalTsaGoods(String[] totalTsaGoods) {
        this.totalTsaGoods = totalTsaGoods;
    }

    public String getConsignmentNoteAtdNo() {
        return consignmentNoteAtdNo;
    }

    public void setConsignmentNoteAtdNo(String consignmentNoteAtdNo) {
        this.consignmentNoteAtdNo = consignmentNoteAtdNo;
    }

    public String getConsignmentNoteEmno() {
        return consignmentNoteEmno;
    }

    public void setConsignmentNoteEmno(String consignmentNoteEmno) {
        this.consignmentNoteEmno = consignmentNoteEmno;
    }

    public String[] getUsedCapacity() {
        return usedCapacity;
    }

    public void setUsedCapacity(String[] usedCapacity) {
        this.usedCapacity = usedCapacity;
    }

    public String[] getUsedVol() {
        return usedVol;
    }

    public void setUsedVol(String[] usedVol) {
        this.usedVol = usedVol;
    }

    public String getTruckCurrentVol() {
        return truckCurrentVol;
    }

    public void setTruckCurrentVol(String truckCurrentVol) {
        this.truckCurrentVol = truckCurrentVol;
    }

    public String getTruckCurrentCap() {
        return truckCurrentCap;
    }

    public void setTruckCurrentCap(String truckCurrentCap) {
        this.truckCurrentCap = truckCurrentCap;
    }

    public String getTruckVol() {
        return truckVol;
    }

    public void setTruckVol(String truckVol) {
        this.truckVol = truckVol;
    }

    public String getFleetTypeId() {
        return fleetTypeId;
    }

    public void setFleetTypeId(String fleetTypeId) {
        this.fleetTypeId = fleetTypeId;
    }

    public String getFleetCapacity() {
        return fleetCapacity;
    }

    public void setFleetCapacity(String fleetCapacity) {
        this.fleetCapacity = fleetCapacity;
    }

    public String getFleetStatus() {
        return fleetStatus;
    }

    public void setFleetStatus(String fleetStatus) {
        this.fleetStatus = fleetStatus;
    }

    public String getUsedTruckVol() {
        return usedTruckVol;
    }

    public void setUsedTruckVol(String usedTruckVol) {
        this.usedTruckVol = usedTruckVol;
    }

    public String getUsedTruckCapcity() {
        return usedTruckCapcity;
    }

    public void setUsedTruckCapcity(String usedTruckCapcity) {
        this.usedTruckCapcity = usedTruckCapcity;
    }

    public String getFleetCode() {
        return fleetCode;
    }

    public void setFleetCode(String fleetCode) {
        this.fleetCode = fleetCode;
    }

    public String getFleetVolume() {
        return fleetVolume;
    }

    public void setFleetVolume(String fleetVolume) {
        this.fleetVolume = fleetVolume;
    }

    public String getTruckId() {
        return truckId;
    }

    public void setTruckId(String truckId) {
        this.truckId = truckId;
    }

    public String getTruckName() {
        return truckName;
    }

    public void setTruckName(String truckName) {
        this.truckName = truckName;
    }

    public String getTruckCapacity() {
        return truckCapacity;
    }

    public void setTruckCapacity(String truckCapacity) {
        this.truckCapacity = truckCapacity;
    }

    public String getAwbOriginRegion() {
        return awbOriginRegion;
    }

    public void setAwbOriginRegion(String awbOriginRegion) {
        this.awbOriginRegion = awbOriginRegion;
    }

    public String getAwbno() {
        return awbno;
    }

    public void setAwbno(String awbno) {
        this.awbno = awbno;
    }

    public String getcNoteNo() {
        return cNoteNo;
    }

    public void setcNoteNo(String cNoteNo) {
        this.cNoteNo = cNoteNo;
    }

    public String getConsignmentOrderStatus() {
        return consignmentOrderStatus;
    }

    public void setConsignmentOrderStatus(String consignmentOrderStatus) {
        this.consignmentOrderStatus = consignmentOrderStatus;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getLoadDate() {
        return loadDate;
    }

    public void setLoadDate(String loadDate) {
        this.loadDate = loadDate;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getConsignmentDate() {
        return consignmentDate;
    }

    public void setConsignmentDate(String consignmentDate) {
        this.consignmentDate = consignmentDate;
    }

    public String getFleetCode1() {
        return fleetCode1;
    }

    public void setFleetCode1(String fleetCode1) {
        this.fleetCode1 = fleetCode1;
    }

    public String getFleetCodeId() {
        return fleetCodeId;
    }

    public void setFleetCodeId(String fleetCodeId) {
        this.fleetCodeId = fleetCodeId;
    }

    public String getOtlNo() {
        return otlNo;
    }

    public void setOtlNo(String otlNo) {
        this.otlNo = otlNo;
    }

    public String getShipmentType() {
        return shipmentType;
    }

    public void setShipmentType(String shipmentType) {
        this.shipmentType = shipmentType;
    }

    public String getConsignmentEfoNo() {
        return consignmentEfoNo;
    }

    public void setConsignmentEfoNo(String consignmentEfoNo) {
        this.consignmentEfoNo = consignmentEfoNo;
    }

    public String getEfoNo() {
        return efoNo;
    }

    public void setEfoNo(String efoNo) {
        this.efoNo = efoNo;
    }

    public String getShipType() {
        return shipType;
    }

    public void setShipType(String shipType) {
        this.shipType = shipType;
    }

    public String getPayBill() {
        return payBill;
    }

    public void setPayBill(String payBill) {
        this.payBill = payBill;
    }

    public String getTspPerkg() {
        return tspPerkg;
    }

    public void setTspPerkg(String tspPerkg) {
        this.tspPerkg = tspPerkg;
    }

    public String getTsrPerkg() {
        return tsrPerkg;
    }

    public void setTsrPerkg(String tsrPerkg) {
        this.tsrPerkg = tsrPerkg;
    }

    public String getCustomerNetAmount() {
        return customerNetAmount;
    }

    public void setCustomerNetAmount(String customerNetAmount) {
        this.customerNetAmount = customerNetAmount;
    }

    public String getRateMode() {
        return rateMode;
    }

    public void setRateMode(String rateMode) {
        this.rateMode = rateMode;
    }

    public String getFullTruckValue() {
        return fullTruckValue;
    }

    public void setFullTruckValue(String fullTruckValue) {
        this.fullTruckValue = fullTruckValue;
    }

    public String getDelayTimeId() {
        return delayTimeId;
    }

    public void setDelayTimeId(String delayTimeId) {
        this.delayTimeId = delayTimeId;
    }

    public String getDelayTime() {
        return delayTime;
    }

    public void setDelayTime(String delayTime) {
        this.delayTime = delayTime;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getPayBillTsr() {
        return payBillTsr;
    }

    public void setPayBillTsr(String payBillTsr) {
        this.payBillTsr = payBillTsr;
    }

    public String getTranshipmentNo() {
        return transhipmentNo;
    }

    public void setTranshipmentNo(String transhipmentNo) {
        this.transhipmentNo = transhipmentNo;
    }

    public String getEfo_No() {
        return efo_No;
    }

    public void setEfo_No(String efo_No) {
        this.efo_No = efo_No;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getReceivedPackages() {
        return receivedPackages;
    }

    public void setReceivedPackages(String receivedPackages) {
        this.receivedPackages = receivedPackages;
    }

    public String getReceivedWeight() {
        return receivedWeight;
    }

    public void setReceivedWeight(String receivedWeight) {
        this.receivedWeight = receivedWeight;
    }

    public String getGenerateStatus() {
        return generateStatus;
    }

    public void setGenerateStatus(String generateStatus) {
        this.generateStatus = generateStatus;
    }

    public String getTsaGenStatus() {
        return tsaGenStatus;
    }

    public void setTsaGenStatus(String tsaGenStatus) {
        this.tsaGenStatus = tsaGenStatus;
    }

    public String getManifestGenStatus() {
        return manifestGenStatus;
    }

    public void setManifestGenStatus(String manifestGenStatus) {
        this.manifestGenStatus = manifestGenStatus;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    public String getReceivedStatus() {
        return receivedStatus;
    }

    public void setReceivedStatus(String receivedStatus) {
        this.receivedStatus = receivedStatus;
    }

    public String getIncentiveCharge() {
        return incentiveCharge;
    }

    public void setIncentiveCharge(String incentiveCharge) {
        this.incentiveCharge = incentiveCharge;
    }

    public String getScreenCharge() {
        return screenCharge;
    }

    public void setScreenCharge(String screenCharge) {
        this.screenCharge = screenCharge;
    }

    public String getScreenChargePaidBy() {
        return screenChargePaidBy;
    }

    public void setScreenChargePaidBy(String screenChargePaidBy) {
        this.screenChargePaidBy = screenChargePaidBy;
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getJanuary() {
        return january;
    }

    public void setJanuary(String january) {
        this.january = january;
    }

    public String getFebuary() {
        return febuary;
    }

    public void setFebuary(String febuary) {
        this.febuary = febuary;
    }

    public String getMarch() {
        return march;
    }

    public void setMarch(String march) {
        this.march = march;
    }

    public String getApril() {
        return april;
    }

    public void setApril(String april) {
        this.april = april;
    }

    public String getMay() {
        return may;
    }

    public void setMay(String may) {
        this.may = may;
    }

    public String getJune() {
        return june;
    }

    public void setJune(String june) {
        this.june = june;
    }

    public String getJuly() {
        return july;
    }

    public void setJuly(String july) {
        this.july = july;
    }

    public String getAugust() {
        return august;
    }

    public void setAugust(String august) {
        this.august = august;
    }

    public String getSeptember() {
        return september;
    }

    public void setSeptember(String september) {
        this.september = september;
    }

    public String getOctober() {
        return october;
    }

    public void setOctober(String october) {
        this.october = october;
    }

    public String getNovember() {
        return november;
    }

    public void setNovember(String november) {
        this.november = november;
    }

    public String getDecember() {
        return december;
    }

    public void setDecember(String december) {
        this.december = december;
    }

    public String getIgmNo() {
        return igmNo;
    }

    public void setIgmNo(String igmNo) {
        this.igmNo = igmNo;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getHawbNo() {
        return hawbNo;
    }

    public void setHawbNo(String hawbNo) {
        this.hawbNo = hawbNo;
    }

    public String getPaidId() {
        return paidId;
    }

    public void setPaidId(String paidId) {
        this.paidId = paidId;
    }

    public String getPaidBy() {
        return paidBy;
    }

    public void setPaidBy(String paidBy) {
        this.paidBy = paidBy;
    }

    public String getPaidByScreenCharge() {
        return paidByScreenCharge;
    }

    public void setPaidByScreenCharge(String paidByScreenCharge) {
        this.paidByScreenCharge = paidByScreenCharge;
    }

    public String getPaidByTsp() {
        return paidByTsp;
    }

    public void setPaidByTsp(String paidByTsp) {
        this.paidByTsp = paidByTsp;
    }

    public String getPaidByTsr() {
        return paidByTsr;
    }

    public void setPaidByTsr(String paidByTsr) {
        this.paidByTsr = paidByTsr;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public String getServiceTaxAmount() {
        return serviceTaxAmount;
    }

    public void setServiceTaxAmount(String serviceTaxAmount) {
        this.serviceTaxAmount = serviceTaxAmount;
    }

    public String getServiceTaxId() {
        return serviceTaxId;
    }

    public void setServiceTaxId(String serviceTaxId) {
        this.serviceTaxId = serviceTaxId;
    }

    public String getDepreciationChargeName() {
        return depreciationChargeName;
    }

    public void setDepreciationChargeName(String depreciationChargeName) {
        this.depreciationChargeName = depreciationChargeName;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getDepreciationChargeId() {
        return depreciationChargeId;
    }

    public void setDepreciationChargeId(String depreciationChargeId) {
        this.depreciationChargeId = depreciationChargeId;
    }

    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }

    public String getExpenseName() {
        return expenseName;
    }

    public void setExpenseName(String expenseName) {
        this.expenseName = expenseName;
    }

    public String getExpenseType() {
        return expenseType;
    }

    public void setExpenseType(String expenseType) {
        this.expenseType = expenseType;
    }

    public String getTruckDeptTime() {
        return truckDeptTime;
    }

    public void setTruckDeptTime(String truckDeptTime) {
        this.truckDeptTime = truckDeptTime;
    }

    public String getTruckArrTime() {
        return truckArrTime;
    }

    public void setTruckArrTime(String truckArrTime) {
        this.truckArrTime = truckArrTime;
    }

    public String getTruckArrDate() {
        return truckArrDate;
    }

    public void setTruckArrDate(String truckArrDate) {
        this.truckArrDate = truckArrDate;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getAwbNo() {
        return awbNo;
    }

    public void setAwbNo(String awbNo) {
        this.awbNo = awbNo;
    }

    public String getAwbTotalPackages() {
        return awbTotalPackages;
    }

    public void setAwbTotalPackages(String awbTotalPackages) {
        this.awbTotalPackages = awbTotalPackages;
    }

    public String getAwbReceivedPackages() {
        return awbReceivedPackages;
    }

    public void setAwbReceivedPackages(String awbReceivedPackages) {
        this.awbReceivedPackages = awbReceivedPackages;
    }

    public String getAwbPendingPackages() {
        return awbPendingPackages;
    }

    public void setAwbPendingPackages(String awbPendingPackages) {
        this.awbPendingPackages = awbPendingPackages;
    }

    public String getAwbTotalGrossWeight() {
        return awbTotalGrossWeight;
    }

    public void setAwbTotalGrossWeight(String awbTotalGrossWeight) {
        this.awbTotalGrossWeight = awbTotalGrossWeight;
    }

    public String getRouteCourseId() {
        return routeCourseId;
    }

    public void setRouteCourseId(String routeCourseId) {
        this.routeCourseId = routeCourseId;
    }

    public String getTripCode() {
        return tripCode;
    }

    public void setTripCode(String tripCode) {
        this.tripCode = tripCode;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getRouteInfo() {
        return routeInfo;
    }

    public void setRouteInfo(String routeInfo) {
        this.routeInfo = routeInfo;
    }

    public String getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(String grossWeight) {
        this.grossWeight = grossWeight;
    }

    public String getChargeableWeight() {
        return chargeableWeight;
    }

    public void setChargeableWeight(String chargeableWeight) {
        this.chargeableWeight = chargeableWeight;
    }

    public String getVehicleCost() {
        return vehicleCost;
    }

    public void setVehicleCost(String vehicleCost) {
        this.vehicleCost = vehicleCost;
    }

    public String getFreightCharge() {
        return freightCharge;
    }

    public void setFreightCharge(String freightCharge) {
        this.freightCharge = freightCharge;
    }

    public String getTspDestinationAmount() {
        return tspDestinationAmount;
    }

    public void setTspDestinationAmount(String tspDestinationAmount) {
        this.tspDestinationAmount = tspDestinationAmount;
    }

    public String getTsrDestinationAmount() {
        return tsrDestinationAmount;
    }

    public void setTsrDestinationAmount(String tsrDestinationAmount) {
        this.tsrDestinationAmount = tsrDestinationAmount;
    }

    public String getScDestinationAmount() {
        return scDestinationAmount;
    }

    public void setScDestinationAmount(String scDestinationAmount) {
        this.scDestinationAmount = scDestinationAmount;
    }

    public String getPgRate() {
        return pgRate;
    }

    public void setPgRate(String pgRate) {
        this.pgRate = pgRate;
    }

    public String getTotalChargeableWeight() {
        return totalChargeableWeight;
    }

    public void setTotalChargeableWeight(String totalChargeableWeight) {
        this.totalChargeableWeight = totalChargeableWeight;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getTotTSPrate() {
        return totTSPrate;
    }

    public void setTotTSPrate(String totTSPrate) {
        this.totTSPrate = totTSPrate;
    }

    public String getTotTSRrate() {
        return totTSRrate;
    }

    public void setTotTSRrate(String totTSRrate) {
        this.totTSRrate = totTSRrate;
    }

    public String getTotSCrate() {
        return totSCrate;
    }

    public void setTotSCrate(String totSCrate) {
        this.totSCrate = totSCrate;
    }

    public String getTotTTAamount() {
        return totTTAamount;
    }

    public void setTotTTAamount(String totTTAamount) {
        this.totTTAamount = totTTAamount;
    }

    public String getTspDestinationPaidTo() {
        return tspDestinationPaidTo;
    }

    public void setTspDestinationPaidTo(String tspDestinationPaidTo) {
        this.tspDestinationPaidTo = tspDestinationPaidTo;
    }

    public String getTsrDestinationPaidTo() {
        return tsrDestinationPaidTo;
    }

    public void setTsrDestinationPaidTo(String tsrDestinationPaidTo) {
        this.tsrDestinationPaidTo = tsrDestinationPaidTo;
    }

    public String getScDestinationPaidTo() {
        return scDestinationPaidTo;
    }

    public void setScDestinationPaidTo(String scDestinationPaidTo) {
        this.scDestinationPaidTo = scDestinationPaidTo;
    }

    public String[] getChargeableWeights() {
        return chargeableWeights;
    }

    public void setChargeableWeights(String[] chargeableWeights) {
        this.chargeableWeights = chargeableWeights;
    }

    public String[] getFreightCharges() {
        return freightCharges;
    }

    public void setFreightCharges(String[] freightCharges) {
        this.freightCharges = freightCharges;
    }

    public String[] getRouteCourseIds() {
        return routeCourseIds;
    }

    public void setRouteCourseIds(String[] routeCourseIds) {
        this.routeCourseIds = routeCourseIds;
    }

    public String getOriginRoyaltyPercent() {
        return originRoyaltyPercent;
    }

    public void setOriginRoyaltyPercent(String originRoyaltyPercent) {
        this.originRoyaltyPercent = originRoyaltyPercent;
    }

    public String getDestinationRoyaltyPercent() {
        return destinationRoyaltyPercent;
    }

    public void setDestinationRoyaltyPercent(String destinationRoyaltyPercent) {
        this.destinationRoyaltyPercent = destinationRoyaltyPercent;
    }

    public String getTspInput() {
        return tspInput;
    }

    public void setTspInput(String tspInput) {
        this.tspInput = tspInput;
    }

    public String getTsrInput() {
        return tsrInput;
    }

    public void setTsrInput(String tsrInput) {
        this.tsrInput = tsrInput;
    }

    public String getScreeningInput() {
        return screeningInput;
    }

    public void setScreeningInput(String screeningInput) {
        this.screeningInput = screeningInput;
    }

    public String getTruckCapacityTemp() {
        return truckCapacityTemp;
    }

    public void setTruckCapacityTemp(String truckCapacityTemp) {
        this.truckCapacityTemp = truckCapacityTemp;
    }

    public String getRatePerKg() {
        return ratePerKg;
    }

    public void setRatePerKg(String ratePerKg) {
        this.ratePerKg = ratePerKg;
    }

    public String getPayBillTsp() {
        return payBillTsp;
    }

    public void setPayBillTsp(String payBillTsp) {
        this.payBillTsp = payBillTsp;
    }

    public String getTspOriginPaidTo() {
        return tspOriginPaidTo;
    }

    public void setTspOriginPaidTo(String tspOriginPaidTo) {
        this.tspOriginPaidTo = tspOriginPaidTo;
    }

    public String getTspOriginAmount() {
        return tspOriginAmount;
    }

    public void setTspOriginAmount(String tspOriginAmount) {
        this.tspOriginAmount = tspOriginAmount;
    }

    public String getTsrOriginPaidTo() {
        return tsrOriginPaidTo;
    }

    public void setTsrOriginPaidTo(String tsrOriginPaidTo) {
        this.tsrOriginPaidTo = tsrOriginPaidTo;
    }

    public String getTsrOriginAmount() {
        return tsrOriginAmount;
    }

    public void setTsrOriginAmount(String tsrOriginAmount) {
        this.tsrOriginAmount = tsrOriginAmount;
    }

    public String getTsrOriginPercent() {
        return tsrOriginPercent;
    }

    public void setTsrOriginPercent(String tsrOriginPercent) {
        this.tsrOriginPercent = tsrOriginPercent;
    }

    public String getTsrDestPercent() {
        return tsrDestPercent;
    }

    public void setTsrDestPercent(String tsrDestPercent) {
        this.tsrDestPercent = tsrDestPercent;
    }

    public String getPayBillScreen() {
        return payBillScreen;
    }

    public void setPayBillScreen(String payBillScreen) {
        this.payBillScreen = payBillScreen;
    }

    public String getScOriginPaidTo() {
        return scOriginPaidTo;
    }

    public void setScOriginPaidTo(String scOriginPaidTo) {
        this.scOriginPaidTo = scOriginPaidTo;
    }

    public String getScOriginAmount() {
        return scOriginAmount;
    }

    public void setScOriginAmount(String scOriginAmount) {
        this.scOriginAmount = scOriginAmount;
    }

    public String getTotSCRrate() {
        return totSCRrate;
    }

    public void setTotSCRrate(String totSCRrate) {
        this.totSCRrate = totSCRrate;
    }

    public String getTotIncentiveAmount() {
        return totIncentiveAmount;
    }

    public void setTotIncentiveAmount(String totIncentiveAmount) {
        this.totIncentiveAmount = totIncentiveAmount;
    }

    public String getTotCustAmount() {
        return totCustAmount;
    }

    public void setTotCustAmount(String totCustAmount) {
        this.totCustAmount = totCustAmount;
    }

    public String getTotalChargableWeight() {
        return totalChargableWeight;
    }

    public void setTotalChargableWeight(String totalChargableWeight) {
        this.totalChargableWeight = totalChargableWeight;
    }

    public String getAssignedWeight() {
        return assignedWeight;
    }

    public void setAssignedWeight(String assignedWeight) {
        this.assignedWeight = assignedWeight;
    }

    public String getVehicleTonnage() {
        return vehicleTonnage;
    }

    public void setVehicleTonnage(String vehicleTonnage) {
        this.vehicleTonnage = vehicleTonnage;
    }

    public String getVehicleCapacity() {
        return vehicleCapacity;
    }

    public void setVehicleCapacity(String vehicleCapacity) {
        this.vehicleCapacity = vehicleCapacity;
    }

    public String getConsignmentOrderDate() {
        return consignmentOrderDate;
    }

    public void setConsignmentOrderDate(String consignmentOrderDate) {
        this.consignmentOrderDate = consignmentOrderDate;
    }

    public String getMovementTypeId() {
        return movementTypeId;
    }

    public void setMovementTypeId(String movementTypeId) {
        this.movementTypeId = movementTypeId;
    }

    

    public String getOrderDeliveryTime() {
        return orderDeliveryTime;
    }

    public void setOrderDeliveryTime(String orderDeliveryTime) {
        this.orderDeliveryTime = orderDeliveryTime;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getPendingWeight() {
        return pendingWeight;
    }

    public void setPendingWeight(String pendingWeight) {
        this.pendingWeight = pendingWeight;
    }

    public String getOrderDeliveryDate() {
        return orderDeliveryDate;
    }

    public void setOrderDeliveryDate(String orderDeliveryDate) {
        this.orderDeliveryDate = orderDeliveryDate;
    }

    public String getConsignmentoriginname() {
        return consignmentoriginname;
    }

    public void setConsignmentoriginname(String consignmentoriginname) {
        this.consignmentoriginname = consignmentoriginname;
    }

    public String getConsignmentdestinationname() {
        return consignmentdestinationname;
    }

    public void setConsignmentdestinationname(String consignmentdestinationname) {
        this.consignmentdestinationname = consignmentdestinationname;
    }

    public String getAWBoriginname() {
        return AWBoriginname;
    }

    public void setAWBoriginname(String AWBoriginname) {
        this.AWBoriginname = AWBoriginname;
    }

    public String getAWBdestinationname() {
        return AWBdestinationname;
    }

    public void setAWBdestinationname(String AWBdestinationname) {
        this.AWBdestinationname = AWBdestinationname;
    }

    public String getBreadth() {
        return breadth;
    }

    public void setBreadth(String breadth) {
        this.breadth = breadth;
    }

    public String getConsignmentArticleIds() {
        return consignmentArticleIds;
    }

    public void setConsignmentArticleIds(String consignmentArticleIds) {
        this.consignmentArticleIds = consignmentArticleIds;
    }

    public String getAwbMovementType() {
        return awbMovementType;
    }

    public void setAwbMovementType(String awbMovementType) {
        this.awbMovementType = awbMovementType;
    }

    public String getAwbType() {
        return awbType;
    }

    public void setAwbType(String awbType) {
        this.awbType = awbType;
    }

    public String getOrderReferenceAwb() {
        return orderReferenceAwb;
    }

    public void setOrderReferenceAwb(String orderReferenceAwb) {
        this.orderReferenceAwb = orderReferenceAwb;
    }

    public String getOrderReferenceAwbNo() {
        return orderReferenceAwbNo;
    }

    public void setOrderReferenceAwbNo(String orderReferenceAwbNo) {
        this.orderReferenceAwbNo = orderReferenceAwbNo;
    }

    public String getOrderReferenceEnd() {
        return orderReferenceEnd;
    }

    public void setOrderReferenceEnd(String orderReferenceEnd) {
        this.orderReferenceEnd = orderReferenceEnd;
    }

    public String getAwbOrderDeliveryDate() {
        return awbOrderDeliveryDate;
    }

    public void setAwbOrderDeliveryDate(String awbOrderDeliveryDate) {
        this.awbOrderDeliveryDate = awbOrderDeliveryDate;
    }

    public String getOrderReferenceNo() {
        return orderReferenceNo;
    }

    public void setOrderReferenceNo(String orderReferenceNo) {
        this.orderReferenceNo = orderReferenceNo;
    }

    public String getTotalWeightage() {
        return totalWeightage;
    }

    public void setTotalWeightage(String totalWeightage) {
        this.totalWeightage = totalWeightage;
    }

    public String getTotalChargeableWeights() {
        return totalChargeableWeights;
    }

    public void setTotalChargeableWeights(String totalChargeableWeights) {
        this.totalChargeableWeights = totalChargeableWeights;
    }

    public String[] getNoOfPieces() {
        return noOfPieces;
    }

    public void setNoOfPieces(String[] noOfPieces) {
        this.noOfPieces = noOfPieces;
    }

    public String getRateMode1() {
        return rateMode1;
    }

    public void setRateMode1(String rateMode1) {
        this.rateMode1 = rateMode1;
    }

    public String[] getChargeableWeightId() {
        return chargeableWeightId;
    }

    public void setChargeableWeightId(String[] chargeableWeightId) {
        this.chargeableWeightId = chargeableWeightId;
    }

    public String[] getVolumes() {
        return volumes;
    }

    public void setVolumes(String[] volumes) {
        this.volumes = volumes;
    }

    public String[] getHeights() {
        return heights;
    }

    public void setHeights(String[] heights) {
        this.heights = heights;
    }

    public String[] getLengths() {
        return lengths;
    }

    public void setLengths(String[] lengths) {
        this.lengths = lengths;
    }

    public String[] getWidths() {
        return widths;
    }

    public void setWidths(String[] widths) {
        this.widths = widths;
    }

    public String[] getUoms() {
        return uoms;
    }

    public void setUoms(String[] uoms) {
        this.uoms = uoms;
    }

    public String[] getContractId1() {
        return contractId1;
    }

    public void setContractId1(String[] contractId1) {
        this.contractId1 = contractId1;
    }

    public String getRateValue1() {
        return rateValue1;
    }

    public void setRateValue1(String rateValue1) {
        this.rateValue1 = rateValue1;
    }

    public String getContractId2() {
        return contractId2;
    }

    public void setContractId2(String contractId2) {
        this.contractId2 = contractId2;
    }

    

}
