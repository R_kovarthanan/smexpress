
/*---------------------------------------------------------------------------
 * ProcessAttendance.java
 * Jul 28, 2008
 *
 * Copyright (c) ETS.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ETS ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ETS.
 ----------------------------------------------------------------------------*/
/**
 * ****************************************************************************
 *
 * Modification Log:
 * ---------------------------------------------------------------------------
 * Ver Date Author Change
 * ---------------------------------------------------------------------------
 * 1.0 Jul 28, 2008 SriniR	Created
 *
 *************************************************************************
 */
import java.io.*;
import java.util.*;
import java.text.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;

/**
 * ProcessAttendance: The business process class that processes the attendance
 * data.
 *
 * @author Srinivasan.R
 * @version 1.0 28 Jul 2008
 */
public class updateRouteFuelPrice {

    /**
     * retriveJobStatus method used to get the jobs status from the factory and
     * updates in the factory portal.
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static void main(String[] args) {
        System.out.println("Update Records Example...");
        Connection con = null;
        Statement statement = null;
        ResultSet rs = null;
        String url = "jdbc:mysql://localhost:3307/";
        String dbName = "throttlebrattle";
        String driverName = "com.mysql.jdbc.Driver";
        String userName = "root";
        String password = "mysql";
        try {
            Class.forName(driverName);

// Connecting to the database
            con = DriverManager.getConnection(url + dbName, userName, password);
            try {
                PreparedStatement updateFuelPrice = null;
                statement = con.createStatement();
// updating records
                String sql = "UPDATE ts_route_master SET fuel_price=? WHERE route_Id=?";
                System.out.println("Updated successfully");
                updateFuelPrice = con.prepareStatement(sql);
                String sql1 = "SELECT route_Id FROM ts_route_master order by route_Id";
                rs = statement.executeQuery(sql1);
                System.out.println("----------------------");
               
                int updateStatus = 0;
                while (rs.next()) {
                    int routeId = rs.getInt(1);
                    System.out.println("roouteId = " + routeId);
                    
                    updateFuelPrice.setString(1,"53.75");
                    
                    updateFuelPrice.setInt(2,routeId);
                    System.out.println("route ID = " + routeId);
                    updateStatus = updateFuelPrice.executeUpdate();
                    if(updateStatus>0){
                    System.out.println("fuel price updated...:-) " );
                    }
                    
                }

            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Table doesn't exist.");
            }
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
