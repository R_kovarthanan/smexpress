
/*---------------------------------------------------------------------------
 * ProcessAttendance.java
 * Jul 28, 2008
 *
 * Copyright (c) ETS.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ETS ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ETS.
 ----------------------------------------------------------------------------*/
/**
 * ****************************************************************************
 *
 * Modification Log:
 * ---------------------------------------------------------------------------
 * Ver Date Author Change
 * ---------------------------------------------------------------------------
 * 1.0 Jul 28, 2008 SriniR	Created
 *
 *************************************************************************
 */
import java.io.*;
import java.util.*;
import java.text.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;

/**
 * ProcessAttendance: The business process class that processes the attendance
 * data.
 *
 * @author Srinivasan.R
 * @version 1.0 28 Jul 2008
 */
public class updateTripDriverVehicleId {

    /**
     * retriveJobStatus method used to get the jobs status from the factory and
     * updates in the factory portal.
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static void main(String[] args) {
        System.out.println("Update Records Example...");
        Connection con = null;
        Statement statement = null;
        ResultSet rs = null;
        String url = "jdbc:mysql://203.124.105.244:3306/";
//        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "throttlebrattle";
        String driverName = "com.mysql.jdbc.Driver";
        String userName = "root";
        String password = "etsadmin";
//        String password = "admin";
        try {
            Class.forName(driverName);

// Connecting to the database
            con = DriverManager.getConnection(url + dbName, userName, password);
            try {
                PreparedStatement vehicleRegNo = null;
                statement = con.createStatement();

// updating records
                String sql = "UPDATE ts_trip_driver SET vehicle_Id=? WHERE trip_Driver_Id=?";
                System.out.println("Updated successfully");
                vehicleRegNo = con.prepareStatement(sql);
                String sql1 = "select b.vehicle_Id,trip_Driver_Id from ts_trip_driver a, ts_trip_vehicle b where a.trip_Id = b.trip_Id and b.active_Ind = 'Y'";
                rs = statement.executeQuery(sql1);
                System.out.println("----------------------");
                int i=1;
                int updateStatus = 0;
                while (rs.next()) {
                    String vehicleId = rs.getString(1);
                    int tripSettlementId = rs.getInt(2);
                    System.out.println("JobCradId = " + vehicleId);
                    vehicleRegNo.setString(1,vehicleId);
                    vehicleRegNo.setInt(2,tripSettlementId);
                    System.out.println("RegNo = " + vehicleId+"---jobCradId---"+tripSettlementId);
                    updateStatus = vehicleRegNo.executeUpdate();
                    i++;
                }
                System.out.println("totalRecordsUpdate = " + i);

            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Table doesn't exist.");
            }
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
