/*
 * This example was written in answer to the question
 * http://stackoverflow.com/questions/39203479
 */
package com.ets.domain.pdf;

import com.lowagie.text.Document;
import com.lowagie.text.Chunk;
import com.lowagie.text.PageSize;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.Barcode128;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.*;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.view.document.AbstractPdfView;
import ets.domain.report.business.NumberWords;
import ets.domain.trip.business.TripTO;
import ets.domain.util.ThrottleConstants;
import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class PdfInvoicePrintViewTSPinvoice extends AbstractPdfView {

    public static final String DEST = "results/tables/small_table.pdf";

    @Override
    protected void buildPdfDocument(Map model, Document document,
            PdfWriter writer, HttpServletRequest request,
            HttpServletResponse response) throws Exception, DocumentException {
        ArrayList invoiceHeader = new ArrayList();
        ArrayList invoiceTaxDetails = new ArrayList();
        ArrayList otherExpense = new ArrayList();
        String invoiceType = "";
        ServletContext servletContext = this.getServletContext();
        String path = servletContext.getRealPath(File.separator);
        String IMG = path + "/images/truckLogo.jpg";
        int invoiceTaxDetailsSize = 0;

        Map<String, ArrayList> printData = (Map<String, ArrayList>) model.get("printData");

        for (Map.Entry<String, ArrayList> entry : printData.entrySet()) {
            if (entry.getKey().equals("invoiceHeaderTsp")) {
                invoiceHeader = entry.getValue();
            }
            if (entry.getKey().equals("invoiceTaxDetailsTsp")) {
                invoiceTaxDetails = entry.getValue();
            }
        }
         String gstLevy = "";
        String placeofSupply = "";
        String invoiceCode = "";
        String invoiceNoBill = "";
        String invoiceNoRemb = "";
        String totalGrossWeight = "";
        String chargeableWeight = "";
        String customerAddress = "";
        String customerName = "";
        String originCityName = "";
        String destinationCityName = "";
        double grandTotal = 0.00;
        double otherExpenseAmount = 0.00;
        double freightAmount = 0.00;
        double invoiceAmount = 0.00;
        double royalInvoiceAmount = 0.00;
        double totalOtherAmount = 0.00;
        String RUPEES = "";
        String RoyalRupees = "";
        String gstNo = "";
        String panNo = "";
        String compGSTNo = "";
        String compAddress = "";
         String custTinno = "";
        String custStateCode = "";
        String companyState = "";
        String companyTinNo = "";
        String companyStateCode = "";
        String invoiceCustName = "";
        String goodsMovement = "";

        String routeInfo = "";
        String invoiceDate = "";
        String billingState = "";
          String companyPanNo = "";

        System.out.println("invoiceDetails****************" + invoiceHeader.size());
        System.out.println("i m in reimbursement type invoice************");
        Iterator itr = invoiceHeader.iterator();
        TripTO tripTO = null;
        while (itr.hasNext()) {
            tripTO = new TripTO();
            tripTO = (TripTO) itr.next();
            invoiceNoBill = tripTO.getInvoiceNoBill();
             invoiceNoRemb = tripTO.getInvoiceNoRemb();
              invoiceType = tripTO.getInvoiceType();
            gstLevy = tripTO.getGstLevy();
             placeofSupply = tripTO.getPlaceofSupply();
            panNo = tripTO.getPanNo();
            billingState = tripTO.getBillingState();
             companyPanNo = tripTO.getCompPanNo();
            custTinno = tripTO.getCustTinno();
            custStateCode = tripTO.getCustStateCode();
            companyState = tripTO.getCompanyState();
            companyTinNo = tripTO.getCompanyTinNo();
            companyStateCode = tripTO.getCompanyStateCode();
            invoiceCustName = tripTO.getInvoiceCustName();
            gstNo = tripTO.getGstNo();
            compGSTNo = tripTO.getCompGSTNo();
            compAddress = tripTO.getCompAddress();
            invoiceCode = tripTO.getInvoiceCode();
            customerName = tripTO.getCustomerName();
            customerAddress = tripTO.getCustomerAddress();
            invoiceDate = tripTO.getBillDate();
            originCityName = tripTO.getOriginCityName();
            destinationCityName = tripTO.getDestinationCityName();
            totalGrossWeight = tripTO.getTotalWeight();
            chargeableWeight = tripTO.getTotalWeight();
            freightAmount = Double.parseDouble(tripTO.getFreightAmount());
            grandTotal = Double.parseDouble(tripTO.getGrandTotal());
            otherExpenseAmount = Double.parseDouble(tripTO.getOtherExpenseAmount());
            goodsMovement = tripTO.getGoodsMovement();
        }
        if ("".equals(customerAddress) || customerAddress == null) {
            customerAddress = "";
        }
        if ("".equals(gstNo) || gstNo == null) {
            gstNo = "";
        }

        NumberWords numberWords = new NumberWords();
        numberWords.setRoundedValue(String.valueOf(java.lang.Math.ceil(grandTotal)));
        numberWords.setNumberInWords(String.valueOf(java.lang.Math.ceil(grandTotal)));
        RUPEES = numberWords.getNumberInWords();

        String igstName = "";
        String cgstName = "";
        String sgstName = "";
        String igstPercentage = "";
        String cgstPercentage = "";
        String sgstPercentage = "";
        String igstAmount = "";
        String cgstAmount = "";
        String sgstAmount = "";
        String invoiceAmount1 = invoiceAmount + "";

        itr = invoiceTaxDetails.iterator();
        tripTO = null;
        while (itr.hasNext()) {
            tripTO = new TripTO();
            tripTO = (TripTO) itr.next();
            if ("SGST".equals(tripTO.getGstName())) {
                sgstName = "SGST";
                sgstPercentage = tripTO.getGstPercentage();
                sgstAmount = tripTO.getTotalTax();
                System.out.println("sgstAmount:" + sgstAmount);
            }
            if ("CGST".equals(tripTO.getGstName())) {
                cgstName = "CGST";
                cgstPercentage = tripTO.getGstPercentage();
                cgstAmount = tripTO.getTotalTax();
                System.out.println("cgstAmount:" + cgstAmount);
            }
            if ("IGST".equals(tripTO.getGstName())) {
                igstName = "IGST";
                igstPercentage = tripTO.getGstPercentage();
                igstAmount = tripTO.getTotalTax();
                System.out.println("igstAmount:" + igstAmount);
            }
        }
        if ("".equals(sgstName) || sgstName == null) {
            sgstName = "SGST";
            sgstPercentage = "0.00";
            sgstAmount = "0.00";
        }
        if ("".equals(cgstName) || cgstName == null) {
            cgstName = "CGST";
            cgstPercentage = "0.00";
            cgstAmount = "0.00";
        }
        if ("".equals(igstName) || igstName == null) {
            igstName = "IGST";
            igstPercentage = "0.00";
            igstAmount = "0.00";
        }

        System.out.println("invoiceTaxDetails iteration over:");
        Rectangle small = new Rectangle(PageSize.A4);
        document = new Document(small, 20, 554, 35, 20);//paper,paddingleftandright,position,topspace,

        writer = PdfWriter.getInstance(document, response.getOutputStream());
        document.open();

        //famouirst Page//
        PdfPTable table = new PdfPTable(2);

        PdfPTable innertable1 = new PdfPTable(4);

        PdfPTable innertable3 = new PdfPTable(4);
        PdfPTable innertable4 = new PdfPTable(5);
        PdfPTable innertable5 = new PdfPTable(3);
        PdfPTable innertable6 = new PdfPTable(1);
        PdfPTable innertable7 = new PdfPTable(1);

        table.setTotalWidth(new float[]{282, 282});
        //table.setTotalWidth(new float[]{191,191,90,92});
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.setLockedWidth(true);

        innertable1.setTotalWidth(new float[]{191, 191, 90, 92});
        innertable1.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable1.setLockedWidth(true);

        innertable3.setTotalWidth(new float[]{191, 191, 90, 92});
        innertable3.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.setLockedWidth(true);

        innertable4.setTotalWidth(new float[]{20, 230, 120, 82, 112});
        innertable4.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable4.setLockedWidth(true);

        innertable5.setTotalWidth(new float[]{101, 269, 194});
        innertable5.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable5.setLockedWidth(true);

        innertable6.setTotalWidth(new float[]{564});
        innertable6.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable6.setLockedWidth(true);

        innertable7.setTotalWidth(new float[]{564});
        innertable7.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable7.setLockedWidth(true);

       Image img = Image.getInstance("\\images\\TT_Aviation_logo_final1.jpg");
//       Image img = Image.getInstance("images/TT_Aviation_logo_final1.jpg");//linux
        img.scalePercent(50);
        PdfPCell headCell1 = new PdfPCell(new Phrase("TAX INVOICE", FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, GrayColor.BLACK)));
        headCell1.setFixedHeight(40);
        headCell1.setColspan(4);
        headCell1.disableBorderSide(2);
        //headCell.setBackgroundColor(Color.lightGray);
        headCell1.addElement(new Chunk(img, 410, -10));
        headCell1.setHorizontalAlignment(Element.ALIGN_LEFT);


        table.addCell(headCell1);

        // first row
        PdfPCell headCell = new PdfPCell(new Phrase("TSP INVOICE", FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, GrayColor.BLACK)));
        headCell.setFixedHeight(14);
        headCell.setColspan(4);
        headCell.disableBorderSide(2);
        //headCell.setBackgroundColor(Color.lightGray);
        headCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(headCell);

        //next row start//
        PdfPCell headCell2 = new PdfPCell(new Phrase("Some text here"));
        headCell2.setColspan(2);

        headCell2 = new PdfPCell(new Phrase("GOODS OR SERVICES SUPPLIED BY", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
        //headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell2.disableBorderSide(2);
        innertable3.addCell(headCell2);

        headCell2 = new PdfPCell(new Phrase("GOODS OR SERVICES SUPPLIED BY", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
        //headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell2.disableBorderSide(2);
        innertable3.addCell(headCell2);

        headCell2 = new PdfPCell(new Phrase("Invoice No :", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell2.disableBorderSide(2);
        headCell2.disableBorderSide(4);
        innertable3.addCell(headCell2);

        headCell2 = new PdfPCell(new Phrase(invoiceCode, FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell2.disableBorderSide(3);
        headCell2.disableBorderSide(2);
        innertable3.addCell(headCell2);

        headCell2 = new PdfPCell(new Phrase("Full Name & Address : ", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
        headCell2.disableBorderSide(3);
//                        headCell2.disableBorderSide(2);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);

        innertable3.addCell(headCell2);
        headCell2 = new PdfPCell(new Phrase("Full Name & Address : ", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
        headCell2.disableBorderSide(1);
        headCell2.disableBorderSide(2);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.addCell(headCell2);

        headCell2 = new PdfPCell(new Phrase("Date : ", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell2.disableBorderSide(1);
        headCell2.disableBorderSide(2);
        headCell2.disableBorderSide(4);
        innertable3.addCell(headCell2);

        headCell2 = new PdfPCell(new Phrase(invoiceDate, FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell2.disableBorderSide(3);
        headCell2.disableBorderSide(1);
        headCell2.disableBorderSide(2);
        innertable3.addCell(headCell2);

        headCell2 = new PdfPCell(new Phrase(compAddress, FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(60);
        headCell2.disableBorderSide(2);
        headCell2.disableBorderSide(1);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.addCell(headCell2);

        headCell2 = new PdfPCell(new Phrase(invoiceCustName + System.lineSeparator() + customerAddress, FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(60);
        headCell2.disableBorderSide(1);
        headCell2.disableBorderSide(2);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.addCell(headCell2);

        headCell2 = new PdfPCell(new Phrase(" "+ System.lineSeparator() +" ", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
        headCell2.disableBorderSide(1);
        headCell2.disableBorderSide(2);
        headCell2.disableBorderSide(4);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.addCell(headCell2);

        headCell2 = new PdfPCell(new Phrase(invoiceType + System.lineSeparator() + goodsMovement, FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
        headCell2.disableBorderSide(3);
        headCell2.disableBorderSide(1);
        headCell2.disableBorderSide(2);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.addCell(headCell2);


        headCell2 = new PdfPCell(new Phrase("GST TIN:" + compGSTNo, FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
//        headCell2.disableBorderSide(2);
//        headCell2.disableBorderSide(1);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.addCell(headCell2);

        headCell2 = new PdfPCell(new Phrase("GST TIN: " + gstNo, FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
//        headCell2.disableBorderSide(2);
//        headCell2.disableBorderSide(1);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.addCell(headCell2);

        headCell2 = new PdfPCell(new Phrase("GST Levy:", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
//        headCell2.disableBorderSide(1);
//        headCell2.disableBorderSide(2);
//        headCell2.disableBorderSide(4);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.addCell(headCell2);

        headCell2 = new PdfPCell(new Phrase(gstLevy, FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
//        headCell2.disableBorderSide(1);
//        headCell2.disableBorderSide(2);
//        headCell2.disableBorderSide(3);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.addCell(headCell2);

       headCell2 = new PdfPCell(new Phrase("PAN: "+companyPanNo, FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
//                        headCell2.disableBorderSide(2);
//        headCell2.disableBorderSide(1);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.addCell(headCell2);

         headCell2 = new PdfPCell(new Phrase("Pan No:" + panNo, FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
//                        headCell2.disableBorderSide(2);
//        headCell2.disableBorderSide(1);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.addCell(headCell2);

        headCell2 = new PdfPCell(new Phrase("Currency:", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
//                        headCell2.disableBorderSide(1);
//                        headCell2.disableBorderSide(2);
//        headCell2.disableBorderSide(4);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.addCell(headCell2);

        headCell2 = new PdfPCell(new Phrase("INR", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
//        headCell2.disableBorderSide(1);
//        headCell2.disableBorderSide(2);
//        headCell2.disableBorderSide(3);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.addCell(headCell2);


        headCell2 = new PdfPCell(new Phrase("Supplier State Code:"+companyStateCode, FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
//                        headCell2.disableBorderSide(2);
        headCell2.disableBorderSide(1);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.addCell(headCell2);

        headCell2 = new PdfPCell(new Phrase("Recipient State Code:" + custStateCode, FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
//                        headCell2.disableBorderSide(2);
        headCell2.disableBorderSide(1);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.addCell(headCell2);

        headCell2 = new PdfPCell(new Phrase("Place of "  , FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
//                        headCell2.disableBorderSide(1);
//                        headCell2.disableBorderSide(2);
        headCell2.disableBorderSide(3);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.addCell(headCell2);

        headCell2 = new PdfPCell(new Phrase(placeofSupply, FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
//        headCell2.disableBorderSide(1);
        headCell2.disableBorderSide(2);
//        headCell2.disableBorderSide(3);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.addCell(headCell2);

         headCell2 = new PdfPCell(new Phrase("Supplier State TIN:"+companyTinNo, FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
//                        headCell2.disableBorderSide(2);
        headCell2.disableBorderSide(1);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.addCell(headCell2);

        headCell2 = new PdfPCell(new Phrase("Recipient State TIN:" + custTinno, FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
//                        headCell2.disableBorderSide(2);
        headCell2.disableBorderSide(1);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.addCell(headCell2);

        headCell2 = new PdfPCell(new Phrase("Supply", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
//                        headCell2.disableBorderSide(1);
//                        headCell2.disableBorderSide(2);
        headCell2.disableBorderSide(3);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.addCell(headCell2);

        headCell2 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell2.setFixedHeight(13);
        headCell2.disableBorderSide(1);
        headCell2.disableBorderSide(2);
        headCell2.disableBorderSide(3);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.addCell(headCell2);




        PdfPCell headCell3 = new PdfPCell(new Phrase("Some text here"));
        headCell3.setColspan(5);

        headCell3 = new PdfPCell(new Phrase("S.No", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase("Description of Goods or Service", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase("HSN/SAC Code", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase("Rate %", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase("Amount", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable4.addCell(headCell3);

      
        
         System.out.println("iteraion start:");
                        Iterator itrExp = otherExpense.iterator();
                           TripTO tripExpTO = null;
                           int i = 1;
                       while (itrExp.hasNext()) {
                           tripExpTO=(TripTO)itrExp.next();
                      //  for(int i=0;i<otherExpesne.size();i++){
                           System.out.println("i m in for loop:"+tripExpTO.getExpenseName());
                           totalOtherAmount=+Double.parseDouble(tripExpTO.getExpenseValue());
                            System.out.println("totalOtherAmount========="+totalOtherAmount);
                        headCell3 = new PdfPCell(new Phrase(i+"", FontFactory.getFont(FontFactory.HELVETICA, 9)));
		        headCell3.setFixedHeight(13);
		        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
		        innertable4.addCell(headCell3);
                        
                        headCell3 = new PdfPCell(new Phrase(tripExpTO.getExpenseName(), FontFactory.getFont(FontFactory.HELVETICA, 9)));
		        headCell3.setFixedHeight(13);
		        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
		        innertable4.addCell(headCell3);
                        
                        
                        headCell3 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
		        headCell3.setFixedHeight(13);
		        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
		        innertable4.addCell(headCell3);
                        

                        headCell3 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
		        headCell3.setFixedHeight(13);
		        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
		        innertable4.addCell(headCell3);
                         
                        headCell3 = new PdfPCell(new Phrase(tripExpTO.getExpenseValue()+"", FontFactory.getFont(FontFactory.HELVETICA, 9)));
		        headCell3.setFixedHeight(13);
		        headCell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        headCell3.disableBorderSide(2);
		        innertable4.addCell(headCell3);
                        i = i + 1;
                        }
if(invoiceNoBill != null || invoiceNoBill != ""){
        headCell3 = new PdfPCell(new Phrase("A", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell3.disableBorderSide(3);
        innertable4.addCell(headCell3);


//        headCell3 = new PdfPCell(new Phrase("A", FontFactory.getFont(FontFactory.HELVETICA, 9)));
//                headCell3.setFixedHeight(25);
//                headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
////                        headCell3.disableBorderSide(2);
//                innertable4.addCell(headCell3);
//
            headCell3 = new PdfPCell(new Phrase("Road Transportation Charges are separately invoiced in Invoice number: "+invoiceNoBill, FontFactory.getFont(FontFactory.HELVETICA, 9)));
            headCell3.setFixedHeight(25);
            headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
            innertable4.addCell(headCell3);


            headCell3 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
            headCell3.setFixedHeight(25);
            headCell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        headCell3.disableBorderSide(2);
            innertable4.addCell(headCell3);


            headCell3 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
            headCell3.setFixedHeight(25);
            headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
            innertable4.addCell(headCell3);

            headCell3 = new PdfPCell(new Phrase( "", FontFactory.getFont(FontFactory.HELVETICA, 9)));
            headCell3.setFixedHeight(25);
            headCell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        headCell3.disableBorderSide(2);
            innertable4.addCell(headCell3);
}else{
    
}
//
            if(invoiceNoRemb != null || invoiceNoRemb != ""){
              headCell3 = new PdfPCell(new Phrase("B", FontFactory.getFont(FontFactory.HELVETICA, 9)));
                headCell3.setFixedHeight(25);
                headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
                innertable4.addCell(headCell3);

            headCell3 = new PdfPCell(new Phrase("Billable Expenses are separately invoiced in Invoice number: "+invoiceNoRemb, FontFactory.getFont(FontFactory.HELVETICA, 9)));
            headCell3.setFixedHeight(25);
            headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
            innertable4.addCell(headCell3);


            headCell3 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
            headCell3.setFixedHeight(25);
            headCell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        headCell3.disableBorderSide(2);
            innertable4.addCell(headCell3);


            headCell3 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
            headCell3.setFixedHeight(25);
            headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
            innertable4.addCell(headCell3);

            headCell3 = new PdfPCell(new Phrase( "", FontFactory.getFont(FontFactory.HELVETICA, 9)));
            headCell3.setFixedHeight(25);
            headCell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        headCell3.disableBorderSide(2);
            innertable4.addCell(headCell3);
            }else{
                
            }

        headCell3 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell3.disableBorderSide(3);
        innertable4.addCell(headCell3);


        headCell3 = new PdfPCell(new Phrase("Total Value", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell3.disableBorderSide(2);
        headCell3.setColspan(2);
        headCell3.disableBorderSide(4);
        innertable4.addCell(headCell3);

        /*headCell3 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
		        headCell3.setFixedHeight(13);
		        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell3.disableBorderSide(2);
                        headCell3.disableBorderSide(4);
                        headCell3.disableBorderSide(3);
                        innertable4.addCell(headCell3); */
        headCell3 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell3.disableBorderSide(2);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase(freightAmount + "", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setColspan(2);
        headCell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
        headCell3.disableBorderSide(2);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell3.disableBorderSide(1);
        headCell3.disableBorderSide(2);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase("Taxable Value", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell3.setColspan(2);
        headCell3.disableBorderSide(1);
        headCell3.disableBorderSide(4);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell3.disableBorderSide(1);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase(freightAmount + "", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
        headCell3.disableBorderSide(1);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell3.disableBorderSide(1);
        headCell3.disableBorderSide(2);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase("Add:" + sgstName, FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell3.setColspan(2);
        headCell3.disableBorderSide(4);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase(sgstPercentage + "%", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        headCell3.disableBorderSide(2);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase(sgstAmount, FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        headCell3.disableBorderSide(2);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell3.disableBorderSide(1);
        headCell3.disableBorderSide(2);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase("Add:" + cgstName, FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell3.setColspan(2);
        headCell3.disableBorderSide(4);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase(cgstPercentage + "%", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
        headCell3.disableBorderSide(2);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase(cgstAmount, FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
        headCell3.disableBorderSide(2);
        innertable4.addCell(headCell3);


        headCell3 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell3.disableBorderSide(1);
        headCell3.disableBorderSide(2);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase("Add:UGST", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell3.setColspan(2);
        headCell3.disableBorderSide(4);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase("0.00%", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        headCell3.disableBorderSide(2);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase("0.00", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        headCell3.disableBorderSide(2);
        innertable4.addCell(headCell3);

         headCell3 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell3.disableBorderSide(1);
        headCell3.disableBorderSide(2);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase("Add:" + igstName, FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell3.setColspan(2);
        headCell3.disableBorderSide(4);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase(igstPercentage + "%", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        headCell3.disableBorderSide(2);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase(igstAmount, FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        headCell3.disableBorderSide(2);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell3.disableBorderSide(1);
        headCell3.disableBorderSide(2);
        innertable4.addCell(headCell3);
       

        headCell3 = new PdfPCell(new Phrase("Total Amount", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell3.setColspan(3);
        headCell3.disableBorderSide(3);
        innertable4.addCell(headCell3);

        headCell3 = new PdfPCell(new Phrase(grandTotal + "", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell3.setFixedHeight(13);
        headCell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        headCell3.disableBorderSide(2);
        innertable4.addCell(headCell3);

//        headCell3 = new PdfPCell(new Phrase("Amount in Words", FontFactory.getFont(FontFactory.HELVETICA, 9)));
//        headCell3.setFixedHeight(13);
//        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
////                        headCell3.disableBorderSide();
//        headCell3.setColspan(2);
//        innertable4.addCell(headCell3);
//
//        headCell3 = new PdfPCell(new Phrase(RUPEES + " Rupees Only", FontFactory.getFont(FontFactory.HELVETICA, 9)));
//
//        headCell3.setFixedHeight(13);
//        headCell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
////                        headCell3.disableBorderSide(2);
//        headCell3.setColspan(3);
//        innertable4.addCell(headCell3);

        PdfPCell headCell4 = new PdfPCell(new Phrase("Some text here"));
        headCell4.setColspan(2);
        
        headCell4 = new PdfPCell(new Phrase("Amount in Words", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide();
//        headCell4.setColspan(2);
        innertable5.addCell(headCell4);
        
        headCell4 = new PdfPCell(new Phrase(RUPEES + " Rupees Only", FontFactory.getFont(FontFactory.HELVETICA, 9)));

        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                        headCell3.disableBorderSide(2);
        headCell4.setColspan(3);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase("Payment to be remitted in favour of:", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.setColspan(2);
//                        headCell2.disableBorderSide(1);
        headCell4.disableBorderSide(2);
        headCell4.disableBorderSide(3);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase("Name:", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_RIGHT);
        headCell4.disableBorderSide(1);
        headCell4.disableBorderSide(2);
        headCell4.disableBorderSide(3);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase("TT Aviation Handling Service Pvt Ltd", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell4.disableBorderSide(1);
        headCell4.disableBorderSide(2);
        headCell4.disableBorderSide(3);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell4.disableBorderSide(1);
        headCell4.disableBorderSide(2);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase("Bank:", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_RIGHT);
        headCell4.disableBorderSide(1);
        headCell4.disableBorderSide(2);
        headCell4.disableBorderSide(3);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase("City Union Bank Limited", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell4.disableBorderSide(1);
        headCell4.disableBorderSide(2);
        headCell4.disableBorderSide(3);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell4.disableBorderSide(1);
        headCell4.disableBorderSide(2);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase("Branch:", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_RIGHT);
        headCell4.disableBorderSide(1);
        headCell4.disableBorderSide(2);
        headCell4.disableBorderSide(3);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase("Mount Road,Chennai", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell4.disableBorderSide(1);
        headCell4.disableBorderSide(2);
        headCell4.disableBorderSide(4);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell4.disableBorderSide(1);
        headCell4.disableBorderSide(2);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase("Bank Address:", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_RIGHT);
        headCell4.disableBorderSide(1);
        headCell4.disableBorderSide(2);
        headCell4.disableBorderSide(3);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase("706,PB NO 475,Mount Road Chennai - 600 006,Tamil Nadu", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell4.disableBorderSide(1);
        headCell4.disableBorderSide(2);
        headCell4.disableBorderSide(3);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell4.disableBorderSide(1);
        headCell4.disableBorderSide(2);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase("Account Type:", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_RIGHT);
        headCell4.disableBorderSide(1);
        headCell4.disableBorderSide(2);
        headCell4.disableBorderSide(3);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase("Current Account", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell4.disableBorderSide(1);
        headCell4.disableBorderSide(2);
        headCell4.disableBorderSide(3);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase(" ", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell4.disableBorderSide(1);
        headCell4.disableBorderSide(2);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase("Account Number:", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_RIGHT);
        headCell4.disableBorderSide(1);
        headCell4.disableBorderSide(2);
        headCell4.disableBorderSide(3);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase("512120020007568", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell4.disableBorderSide(1);
        headCell4.disableBorderSide(2);
        headCell4.disableBorderSide(3);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase("Authorised Signatory", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_CENTER);
        headCell4.disableBorderSide(1);
        headCell4.disableBorderSide(2);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase("IFSC Code:", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_RIGHT);
        headCell4.disableBorderSide(1);
        headCell4.disableBorderSide(2);
        headCell4.disableBorderSide(3);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase("CIUB0000049", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell4.disableBorderSide(1);
        headCell4.disableBorderSide(2);
        headCell4.disableBorderSide(3);
        innertable5.addCell(headCell4);

        headCell4 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell4.setFixedHeight(13);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell4.disableBorderSide(1);
        innertable5.addCell(headCell4);

        PdfPCell headCell5 = new PdfPCell(new Phrase("Some text here"));
        headCell5.setColspan(1);

        headCell5 = new PdfPCell(new Phrase("Note: We are registered under GTA. GST Liability under RCM vested with the Service Recipient.", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell5.setFixedHeight(13);
        headCell5.setHorizontalAlignment(Element.ALIGN_CENTER);
//                        headCell3.disableBorderSide(2);
        innertable6.addCell(headCell5);

        PdfPCell headCell6 = new PdfPCell(new Phrase("Some text here"));
        headCell6.setColspan(1);

        headCell6 = new PdfPCell(new Phrase("Supplier's Registered Office: #7, Cathedral Road, Gopalapuram, Chennai - 600 086", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell6.setFixedHeight(13);
        headCell6.setHorizontalAlignment(Element.ALIGN_CENTER);
        headCell6.disableBorderSide(2);
        innertable7.addCell(headCell6);

        headCell6 = new PdfPCell(new Phrase("Phone: +91 44 2811 5516 / 1610 ; Fax: +91 44 2811 3004 ; Email: corp.fin@ttgroupglobal.com", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell6.setFixedHeight(13);
        headCell6.setHorizontalAlignment(Element.ALIGN_CENTER);
        headCell6.disableBorderSide(1);
        headCell6.disableBorderSide(2);
        innertable7.addCell(headCell6);

        headCell6 = new PdfPCell(new Phrase("CIN: U63033TN2010PTC077089", FontFactory.getFont(FontFactory.HELVETICA, 9)));
        headCell6.setFixedHeight(13);
        headCell6.setHorizontalAlignment(Element.ALIGN_CENTER);
        headCell6.disableBorderSide(1);
        innertable7.addCell(headCell6);

        //table.addCell(new PdfPCell(innertable3));
        //next row end//
        table.addCell(new PdfPCell(innertable1));

        //table.addCell(new PdfPCell(innertable2));
        document.add(table);
        document.add(innertable3);
        document.add(innertable4);
        document.add(innertable5);
        document.add(innertable6);
        document.add(innertable7);
        document.close();
    }

    public static PdfPTable createTable5(String dest) throws DocumentException {
        Document document;
        Rectangle small = new Rectangle(PageSize.A4);
        document = new Document(small, 20, 554, 35, 20);//paper,paddingleftandright,position,topspace,

        PdfPTable table = new PdfPTable(3);
        try {

            table.setTotalWidth(new float[]{144, 72, 72});
            table.setLockedWidth(true);
            PdfPCell cell;
            cell = new PdfPCell(new Phrase("Table 5"));
            cell.setColspan(3);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("Cell with rowspan 2"));
            //cell.setRowspan(2);
            table.addCell(cell);
            table.addCell("row 1; cell 1");
            table.addCell("row 1; cell 2");
            table.addCell("row 2; cell 1");
            table.addCell("row 2; cell 2");

        } catch (Exception e) {
        }
        return table;
    }
}
