
/*---------------------------------------------------------------------------
 * ProcessAttendance.java
 * Jul 28, 2008
 *
 * Copyright (c) ETS.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ETS ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ETS.
 ----------------------------------------------------------------------------*/
/**
 * ****************************************************************************
 *
 * Modification Log:
 * ---------------------------------------------------------------------------
 * Ver Date Author Change
 * ---------------------------------------------------------------------------
 * 1.0 Jul 28, 2008 SriniR	Created
 *
 *************************************************************************
 */
import java.io.*;
import java.util.*;
import java.text.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;

/**
 * ProcessAttendance: The business process class that processes the attendance
 * data.
 *
 * @author Srinivasan.R
 * @version 1.0 28 Jul 2008
 */
public class updateCustomerCode {

    /**
     * retriveJobStatus method used to get the jobs status from the factory and
     * updates in the factory portal.
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static void main(String[] args) {
        System.out.println("Update Records Example...");
        Connection con = null;
        Statement statement = null;
        ResultSet rs = null;
        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "throttlebrattle";
        String driverName = "com.mysql.jdbc.Driver";
        String userName = "root";
        String password = "admin";
        try {
            Class.forName(driverName);

// Connecting to the database
            con = DriverManager.getConnection(url + dbName, userName, password);
            try {
                PreparedStatement updateEmpCode = null;
                statement = con.createStatement();

// updating records
                String sql = "UPDATE papl_customer_master SET cust_code=? WHERE cust_Id=?";
                System.out.println("Updated successfully");
                updateEmpCode = con.prepareStatement(sql);
                String sql1 = "SELECT cust_Id FROM papl_customer_master order by cust_Id";
                rs = statement.executeQuery(sql1);
                System.out.println("----------------------");
                int i=1;
                int updateStatus = 0;
                while (rs.next()) {
                    int roll = rs.getInt(1);
                    System.out.println("roll = " + roll);
                    if(i<=9){
                    updateEmpCode.setString(1,"CC-000"+i);
                    }else if(i>=9 && i<100){
                    updateEmpCode.setString(1,"CC-00"+i);
                    }else{
                    updateEmpCode.setString(1,"CC-0"+i);
                    }
                    updateEmpCode.setInt(2,roll);
                    System.out.println("Customer Id = " + roll+"---CustomerCode---"+i);
                    updateStatus = updateEmpCode.executeUpdate();
                    i++;
                }

            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Table doesn't exist.");
            }
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
