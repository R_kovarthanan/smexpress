<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.customer.business.CustomerTO" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    </head>
    <script language="javascript">
        function submitPage(value) {
            if (value == "add") {
                document.manufacturer.action = '/throttle/handleViewSecondaryCustomer.do';
                document.manufacturer.submit();
            } else if (value == 'alter') {
                document.manufacturer.action = '/throttle/handleViewAlter.do';
                document.manufacturer.submit();
            }
        }
    </script>
     <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ScheduleTrip" text="ScheduleTrip"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.SecondaryOperations" text="SecondaryOperations"/></a></li>
		                    <li class=""><spring:message code="hrms.label.ScheduleTrip" text="ScheduleTrip"/></li>
		
		                </ol>
		            </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
    <body>
        <form name="manufacturer" method="post" >
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <table class="table table-info mb30 table-hover" id="report" >
                <thead><tr><th colspan="4">View Secondary Trip Details</th.</tr></thead>
                                    <tr>
                                        <td height="30"><font color="red">*</font>Customer Code</td>
                                        <td><input name="custId" id="custId" type="text" class="form-control" style="width:250px;height:40px"  onclick="ressetDate(this);"></td>
                                        <td height="30"><font color="red">*</font>Customer Name</td>
                                        <td><input name="custId" id="custId" type="text" class="form-control" style="width:250px;height:40px" onClick="ressetDate(this);"></td>

                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center">
                                            <input type="button"   value="Search" class="btn btn-success" name="search" onClick="submitWindow('reqFor')">&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </table>
            <br>
            <br>
            <c:if test = "${secondaryTripCustomerList != null}" >
                <table class="table table-info mb30 table-hover sortable" id="table" style="width:100%">
                    <thead>
                        <tr height="40">
                            <th>S.No</th>
                            <th>Customer Code</th>
                            <th>Customer Name</th>
                            <th>Status</th>
                            <th>Secondary<br>Schedule</th>
                        </tr>
                    </thead>
                     <tbody>
                        <% int index = 0,sno = 1;%>
                        <c:forEach items="${secondaryTripCustomerList}" var="customer">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <tr height="30">
                                <td align="left" class="text2"><%=sno++%></td>
                                <td align="left" class="text2"><c:out value="${customer.customerCode}"/> </td>
                                <td align="left" class="text2"><c:out value="${customer.custName}"/> </td>
                                <td align="left" class="text2">
                                    <c:if test="${(customer.custStatus=='n') || (customer.custStatus=='N')}" >
                                    InActive
                                    </c:if>
                                    <c:if test="${(customer.custStatus=='y') || (customer.custStatus=='Y')}" >
                                    Active
                                    </c:if>
                                </td>
                                <td align="left" class="text2"> &nbsp;
                                        <c:if test="${customer.secondaryTripscheduleSatus =='0'}" >
                                        <a href="/throttle/secondaryTripSchedule.do?customerId=<c:out value="${customer.customerId}"/>&customerName=<c:out value="${customer.custName}"/>&customerCode=<c:out value="${customer.customerCode}"/>">Schedule</a>
                                        </c:if>
                                        <c:if test="${customer.secondaryTripscheduleSatus =='1'}" >
                                        <a href="/throttle/handleViewSecondaryTripSchedule.do?customerId=<c:out value="${customer.customerId}"/>&customerName=<c:out value="${customer.custName}"/>&customerCode=<c:out value="${customer.customerCode}"/>&billingTypeId=<c:out value="${customer.billingTypeId}"/>">View</a>
                                        &nbsp;&nbsp;
                                        <a href="/throttle/editSecondaryTripSchedule.do?customerId=<c:out value="${customer.customerId}"/>&customerName=<c:out value="${customer.custName}"/>&customerCode=<c:out value="${customer.customerCode}"/>">Edit</a>
                                        </c:if>
                                </td>
                            </tr>  
                        </c:forEach>
                     </tbody>      
                </table>
            </c:if>
            
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>

        </form>
    </body>

</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>
