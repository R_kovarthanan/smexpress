<%-- 
    Document   : index
    Created on : Aug 17, 2012, 4:45:43 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
    </head>
    <body>        
        <a href="LocationDownload.jsp">Location Download</a><br>
        <a href="CardNoDownload.jsp">Card No Download</a><br>
        <a href="LoadRoutes.jsp?LocationCode=502">Route Download</a><br>
        <a href="TripEligibleDriverStatus.jsp?IdentityNo=1234567895634563456346?9&RouteCode=1002">Trip Eligible Status and Driver Name</a><br>
        
        <a href="TripOpen.jsp?IdentityNo=1234567895634563456346?9&DeviceId=2&RouteCode=1002&OutKM=150&DriverId=1240&CleanerStatus=Y&LocationCode=502&CustomerId=1000&TripType=0&Datetime=07-08-2012$16:58:17">Trip Open</a><br>
        <!--IdentityNo,DeviceId,RouteCode,OutKM,DriverName,CleanerStatus,DateTime-->

        <a href="Advance.jsp?IdentityNo=1234567895634563456346?9&DeviceId=5&TripId=1001&IssuerNameCode=CS01&Amount=4500&Datetime=26-08-2012$16:58:17">Advance</a><br>
        <!--Identity No,Device Id,Trip Id,Issuer Name Code,Amount,datetime-->

        <a href="FuelDetails.jsp?IdentityNo=1234567895634563456346?9&DeviceId=1&TripId=1001&FuelStationCode=P01&&Liters=175&Datetime=07-08-2012$16:58:17">Fuel Insert</a><br>
        <!--IdentityNo,DeviceId,TripId,FuelStationCode,Liters,Datetime-->

        <a href="VehicleInOutEntry.jsp?IdentityNo=1234567895634563456346?9&DeviceId=2&TripId=1001&InOutIndication=IN&LocationCode=502&Datetime=07-08-2012$16:58:17">Vehicle IN/OUT</a><br>
        <!--TripId,IdentityNo,DeviceId,InOutIndication,LocationCode,InOutDateTime-->

        <a href="GetTripId.jsp?IdentityNo=1234567895634563456346?9">get Trip Id</a><br>
        
        <a href="TripClose.jsp?IdentityNo=1234567895634563456346?9&DeviceId=2&TripId=1001&InKM=320&LoadedTonnage=40&DeliveredTonnage=38&LocationCode=502&Datetime=07-08-2012$16:58:17">Trip Close</a><br>
        <!--IdentityNo,DeviceId,TripId,InKM,Tonnage,LocationCode,Datetime-->
    </body>
</html>