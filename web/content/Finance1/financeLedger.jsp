<%-- 
    Document   : financeLedger
    Created on : 24 Oct, 2012, 7:51:06 PM
    Author     : ASHOK
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.finance.business.FinanceTO" %>
        <%@ page import="java.util.*" %>

        <title> Manage Group</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
    </head>
    <body>
        <form method="post" action="/throttle/addLedgerPage.do">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>

 
            <div style="height:360px;overflow:scroll;overflow-y:scroll;overflow-x:hidden;">
                <table align="center" width="900" border="0" cellspacing="0" cellpadding="0" class="border">
                    <c:if test = "${ledgerLists != null}" >

                        <tr height="30">
                            <td  align="left" class="contenthead" scope="col"><b>S.No</b></td>
                            <td  align="left" class="contenthead" scope="col"><b>Ledger Code</b></td>
                            <td  align="left" class="contenthead" scope="col"><b>Ledger Name</b></td>
                            <td  align="left" class="contenthead" scope="col"><b>Group Name</b></td>
                            <td  align="left" class="contenthead" scope="col"><b>Opening Balance</b></td>
                            <td  align="left" class="contenthead" scope="col"><b>Active Status</b></td>
<!--                            <td  align="left" class="contenthead" scope="col"><b>Edit</b></td>-->
                        </tr>
                        <% int index = 0;%>
                        <c:forEach items="${ledgerLists}" var="LL">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text1";
                                        } else {
                                            classText = "text2";
                                        }
                            %>
                            <tr height="30">
                                <td class="<%=classText%>"  align="left"> <%= index + 1%> </td>
                                <td class="<%=classText%>" align="left"> <c:out value="${LL.ledgerCode}" /></td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${LL.ledgerName}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${LL.groupname}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${LL.openingBalance}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${LL.active_ind}"/> </td>
<!--                                <td class="<%=classText%>" align="left"> <a href="/throttle/alterLedgerDetail.do?ledgerID=<c:out value='${LL.ledgerID}' />" > Edit </a> </td>-->
                            </tr>
                            <% index++;%>
                        </c:forEach>
                    </c:if>
                </table>
            </div>
            <br>
            <br>
            <center><input type="submit" class="button" value="Add" /></center>
        </form>
    </body>
</html>
