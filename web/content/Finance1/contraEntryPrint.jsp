<%-- 
    Document   : contraEntryPrint
    Created on : Oct 22, 2013, 6:12:08 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <link rel="stylesheet" href="/throttle/css/rupees.css"  type="text/css" />

        <%@ page import="ets.domain.renderservice.business.RenderServiceTO" %>
        <%@ page import="ets.domain.renderservice.business.JobCardItemTO" %>
        <%@ page import="ets.domain.problemActivities.business.ProblemActivitiesTO" %>
        <%@ page import="ets.domain.operation.business.OperationTO" %>


        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.*" %>
        <%@ page import="java.lang.Double" %>

        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>\
        <link rel="stylesheet" href="style.css" />
        <%@ page import="ets.domain.contract.business.ContractTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <title>Cash Payment Voucher</title>
    </head>

    <script>



        function print()
        {
            var DocumentContainer = document.getElementById("printDiv");
            var WindowObject = window.open('', "TrackHistoryData",
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }

        function back()
        {
            window.history.back()
        }

        function printContraEntry(val){
            document.suggestedPS.action = '/throttle/printContraEntry.do?voucherCode='+val;
            document.suggestedPS.submit();
        }
    </script>
    <%--

    --%>

    <body>
        <form name="suggestedPS" method="post">

            <div id="printDiv" style="border : 2px solid">

                <table width="860" cellpadding="0" cellspacing="0" align="center" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; ">
                    <tr>
                        <td height="30" style="text-align:center; text-transform:uppercase;" colspan="4">Chettinad Logistics Private Limited - Karikkali<br>
                            <center><u> PLH-Contra Voucher<u></center>
                                        </td>

                                        </tr>
                                        <tr>
                                            <td height="30" colspan="2" height="30" style="text-align:left; ">Voucher No:<c:out value="${voucherCode}"/></td>
                                            <%java.text.DateFormat df = new java.text.SimpleDateFormat("dd-MM-yyyy");%>
                                            <td height="30" colspan="2" height="30" style="text-align:left; ">Date:<%= df.format(new java.util.Date())%> </td>
                                        </tr>
                                        <tr>
                                            <td height="30" style="text-align:left; border-bottom:1px dashed;"><b>Account Head</b></td>
                                            <td height="30" style="text-align:left; border-bottom:1px dashed;"><b>Narration</b></td>
                                            <td height="30" style="text-align:left; border-bottom:1px dashed;"><b>Debits</b></td>
                                            <td height="30" style="text-align:left; border-bottom:1px dashed;"><b>Credits</b></td>
                                        </tr>
                                        <c:if test="${contraEntry != null}">
                                            <c:set var="creditTotal" value="0"/>
                                            <c:set var="debitTotal" value="0"/>
                                            <c:forEach items="${contraEntry}" var="CE">
                                                <tr>
                                                    <c:if test="${CE.accountsType == 'CREDIT'}">
                                                        <c:set var="creditTotal" value="${CE.creditAmmount + creditTotal}"/>
                                                    </c:if>
                                                    <c:if test="${CE.accountsType == 'DEBIT'}">
                                                        <c:set var="debitTotal" value="${CE.debitAmmount + debitTotal}"/>
                                                    </c:if>
                                                    <td height="30"><c:out value="${CE.ledgerName}"/></td>
                                                    <td height="30"><c:out value="${CE.remarks}"/></td>
                                                    <td height="30"><c:out value="${CE.debitAmmount}"/></td>
                                                    <td height="30"><c:out value="${CE.creditAmmount}"/></td>
                                                </tr>
                                            </c:forEach>
                                        </c:if>
                                        <tr>
                                            <td height="30" colspan="2"></td>
                                            <td height="30" colspan="2" style="text-align:left; border-bottom:1px dashed;"></td>
                                        </tr>
                                        <tr>
                                            <td height="30" colspan="2" style="text-align:right;">Total :</td>
                                            <td height="30" style="text-align:left; border-bottom:1px dashed;"><c:out value="${debitTotal}"/></td>
                                            <td height="30" style="text-align:left; border-bottom:1px dashed;"><c:out value="${creditTotal}"/></td>
                                        </tr>
                                        <tr>
                                            <td height="90" colspan="2">Prepared By</td>
                                            <td height="90" >Checked By</td>
                                            <td height="90">Passed By</td>
                                        </tr>
                                        <tr align="center">
                                            <td colspan="4">
                                                <input align="center" type="button" onclick="print();" value = " Print "   />
                                            </td>
                                        </tr>
                                        </table>

                                        </div>
                                        <br>
                                        <c:if test="${contraCodeList != null}">
                                            <br>
                                            <table align="center" width="100%" border="0" id="table" class="sortable">
                                                <thead>
                                                    <tr height="30">
                                                        <th class="nosort"><h3>S.No</h3></th>
                                                        <th><h3>Entry Date</h3></th>
                                                        <th><h3>Credit Voucher</h3></th>
                                                        <th><h3>Credit Ledger</h3></th>
                                                        <th><h3>Credit Amount</h3></th>
                                                        <th><h3>Debit Ledger</h3></th>
                                                        <th><h3>Debit Amount</h3></th>
                                                        <th class="nosort"><h3>Print</h3></th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <% int index = 0;%>
                                                    <c:forEach items="${contraCodeList}" var="code">
                                                        <%
                                                                    String classText = "";
                                                                    int oddEven = index % 2;
                                                                    if (oddEven > 0) {
                                                                        classText = "text1";
                                                                    } else {
                                                                        classText = "text2";
                                                                    }
                                                        %>
                                                        <tr height="30">
                                                            <td class="<%=classText%>"  align="left"> <%= index + 1%> </td>
                                                            <td class="<%=classText%>"  align="left"> <c:out value="${code.accountEntryDate}"/> </td>
                                                            <td class="<%=classText%>" align="left"> <c:out value="${code.voucherCode}" /></td>
                                                            <c:set var="totalCreditAmount" value="0"/>
                                                            <c:set var="creditLedger" value=""/>
                                                            <c:set var="totalDebitAmount" value="0"/>
                                                            <c:set var="debitLedger" value=""/>
                                                            <c:if test = "${contraEntryCreditList != null}" >
                                                                <c:forEach items="${contraEntryCreditList}" var="CL">
                                                                    <c:if test="${CL.creditVoucherCode == code.voucherCode}">
                                                                        <c:set var="totalCreditAmount" value="${totalCreditAmount + CL.creditAmount}"/>
                                                                        <c:set var="creditLedger" value="${CL.creditLedgerName}"/>
                                                                    </c:if>
                                                                </c:forEach>
                                                                <td class="<%=classText%>" align="left"> <c:out value="${creditLedger}" /></td>
                                                                <td class="<%=classText%>" align="left"> <c:out value="${totalCreditAmount}" /></td>
                                                            </c:if>
                                                            <c:if test="${contraEntryDebitList != null}">
                                                                <c:forEach items="${contraEntryDebitList}" var="DL">
                                                                    <c:if test="${DL.debitVoucherCode == code.voucherCode}">
                                                                        <c:set var="totalDebitAmount" value="${totalDebitAmount + DL.debitAmount}"/>
                                                                        <c:set var="debitLedger" value="${DL.debitLedgerName}"/>
                                                                    </c:if>
                                                                </c:forEach>
                                                                <td class="<%=classText%>" align="left"> <c:out value="${debitLedger}" /></td>
                                                                <td class="<%=classText%>" align="left"> <c:out value="${totalDebitAmount}" /></td>
                                                            </c:if>
                                                            <td class="<%=classText%>"  align="left"><a href="#" onclick="printContraEntry('<c:out value="${code.voucherCode}" />')">Print</a></td>
                                                        </tr>
                                                        <% index++;%>
                                                    </c:forEach>
                                                </c:if>
                                            </tbody>
                                        </table>
                                        <br>
                                        <br>

                                        <div id="controls">
                                            <div id="perpage">
                                                <select onchange="sorter.size(this.value)">
                                                    <option value="5" selected="selected">5</option>
                                                    <option value="10">10</option>
                                                    <option value="20">20</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                </select>
                                                <span>Entries Per Page</span>
                                            </div>
                                            <div id="navigation">
                                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
                                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
                                            </div>
                                            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                                        </div>

                                        <script type="text/javascript">
                                            var sorter = new TINY.table.sorter("sorter");
                                            sorter.head = "head";
                                            sorter.asc = "asc";
                                            sorter.desc = "desc";
                                            sorter.even = "evenrow";
                                            sorter.odd = "oddrow";
                                            sorter.evensel = "evenselected";
                                            sorter.oddsel = "oddselected";
                                            sorter.paginate = true;
                                            sorter.currentid = "currentpage";
                                            sorter.limitid = "pagelimit";
                                            sorter.init("table",1);
                                        </script>
                                        </form>
                                        </body>
                                        </html>

