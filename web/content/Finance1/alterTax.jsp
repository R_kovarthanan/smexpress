<%-- 
    Document   : alterTax
    Created on : 7 Nov, 2012, 5:35:54 PM
    Author     : ASHOK
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title> CLPL </title>
<link href="/throttle/css/parveen.css" rel="stylesheet"/>

<script language="javascript" src="/throttle/js/validate.js"></script>
</head>
<script>
  function submitPage()
    {

        if(textValidation(document.alter.taxCode,'taxCode')){
            return;
        }
        if(textValidation(document.alter.taxName,'taxName')){
            return;
        }
        if(textValidation(document.alter.description,'description')){
            return;
        }

        document.alter.action='/throttle/saveAlterTax.do';
        document.alter.submit();
}


</script>
<body>
<form name="alter" method="post">
<%@ include file="/content/common/path.jsp" %>

<%@ include file="/content/common/message.jsp" %>
<c:if test="${taxAlterLists != null}">
    <c:forEach items="${taxAlterLists}" var="TL">
<table align="center" width="500" border="0" cellspacing="0" cellpadding="0" class="border">
 <tr height="30">
  <Td colspan="2" class="contenthead">Edit Tax Master</Td>
 </tr>
  <tr height="30">
      <td class="text2"><font color="red">*</font>Tax Name</td>
      <td class="text2"><input name="taxName" type="text" class="textbox" value="<c:out value="${TL.taxName}"/>" size="20">
          <input type="hidden" name="taxId" value="<c:out value="${TL.taxId}"/>"> </td>
  </tr>
  <tr height="30">
    <td class="text1"><font color="red">*</font>Tax Code</td>
    <td class="text2"><input name="taxCode" type="text" class="textbox" value="<c:out value="${TL.taxCode}"/>" size="20"></td>
  </tr>
  <tr height="30">
    <td class="text2"><font color="red">*</font>Description</td>
    <td class="text2"><input name="description" type="text" class="textbox" value="<c:out value="${TL.description}"/>" size="20"></td>
  </tr>
</table>
</c:forEach>
</c:if>

<br>
<br>
<center><input type="button" class="button" value="Save" onclick="submitPage();" /></center>
</form>
</body>
</html>

