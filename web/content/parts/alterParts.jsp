<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html >
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Parveen Auto Care</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script>
function computeMrp(){
    var spriceVal = document.update.sprice.value;
    var w = document.update.vatId.selectedIndex;
    var taxPercentage = document.update.vatId.options[w].text;
    document.update.mrp.value = parseFloat(spriceVal) + (parseFloat(spriceVal)* parseFloat(taxPercentage)/100);
}
            function submitPage(value)
            
            {
                
            if(value == "alter"){
                
            
       if(isSelect(document.update.categoryId,"categoryName")){
           return;
       }else       
       if(isSelect(document.update.modelId,"modelName")){
           return;
       }
              
                document.update.action='/throttle/alterPartsdid.do';
                document.update.submit();
            }
            
            }
            
   var httpReq;
var temp = "";
function ajaxData()
{
var url = "/throttle/getModels1.do?mfrId="+document.update.mfrId.value;    
if (window.ActiveXObject)
{
httpReq = new ActiveXObject("Microsoft.XMLHTTP");
}
else if (window.XMLHttpRequest)
{
httpReq = new XMLHttpRequest();
}
httpReq.open("GET", url, true);
httpReq.onreadystatechange = function() { processAjax(); } ;
httpReq.send(null);
}


function processAjax()
{
if (httpReq.readyState == 4)
{
	if(httpReq.status == 200)
	{
	temp = httpReq.responseText.valueOf();
         
        setOptions(temp,document.update.modelId);
        document.update.modelId.value=document.update.model.value;        
	}
	else
	{
	alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
	}
}
}

    
    function ajaxRackData()
{
var url = "/throttle/getSubRack.do?rackId="+document.update.rackId.value;    
if (window.ActiveXObject)
{
httpReq = new ActiveXObject("Microsoft.XMLHTTP");
}
else if (window.XMLHttpRequest)
{
httpReq = new XMLHttpRequest();
}
httpReq.open("GET", url, true);
httpReq.onreadystatechange = function() { processRackAjax(); } ;
httpReq.send(null);
}


function processRackAjax()
{
if (httpReq.readyState == 4)
{
	if(httpReq.status == 200)
	{
	temp = httpReq.responseText.valueOf();
        
        setOptions(temp,document.update.subRackId);
	}
	else
	{
	alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
	}
}
}
        </script>
    </head>
    
    <body onLoad="ajaxData();">
        <form  name="update" method="post">
             
<%@ include file="/content/common/path.jsp" %>
       
<%@ include file="/content/common/message.jsp" %>
  
           
            <c:if test="${GetPartsDetail!=null}">
                <c:forEach items="${GetPartsDetail}" var="comp" >
                    <table align="center" width="500" border="0" cellspacing="0" cellpadding="0" class="border">
                        <Tr height="30">
                            <Td colspan="4" class="contenthead"> View Parts</Td>
                        </Tr>
                        <input type="hidden" class="textbox" name="sectionId" value="0" >
                        <input type="hidden" class="textbox" name="model" value='<c:out value="${comp.modelId}" />'  >
                        <tr>
                                                <td class="text1" height="30"><b>Category</b></td>
                                                <td class="text1"  ><select class="textbox" style="width:125px; " name="categoryId" >
                                                <option value="0">---Select---</option>
                                                <c:if test = "${CategoryList != null}" >
                                                <c:forEach items="${CategoryList}" var="Cat"> 
                                                <c:choose>
                                                <c:when test="${comp.categoryId == Cat.categoryId}" >
                                                    <option selected value='<c:out value="${Cat.categoryId}" />'><c:out value="${Cat.categoryName}" /></option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value='<c:out value="${Cat.categoryId}" />'><c:out value="${Cat.categoryName}" /></option>
                                                </c:otherwise>
                                            </c:choose> 

                                        </c:forEach >
                                          </c:if>   	
                                         </select></td>
                             
                            <td class="text1"><b>Manufacturer</b></td>
                            <td class="text1"  ><select class="textbox" name="mfrId" style="width:125px; "   onchange="ajaxData();" >
                                    <option value="0">---Select---</option>
                                    <c:if test = "${MfrList != null}" >
                                        <c:forEach items="${MfrList}" var="Mfr"> 
                                            <c:choose>
                                                <c:when test="${comp.mfrId == Mfr.mfrId}" >                                                    
                                            <option selected value='<c:out value="${Mfr.mfrId}" />'><c:out value="${Mfr.mfrName}" /></option>                                                    
                                                </c:when>
                                                <c:otherwise>
                                            <option value='<c:out value="${Mfr.mfrId}" />'><c:out value="${Mfr.mfrName}" /></option>                                                    
                                                </c:otherwise>
                                            </c:choose> 
                                        </c:forEach >
                                    </c:if>  	
                                </select>
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="text2"><b>Model</b></td>
                            <td class="text2"  ><select class="textbox" style="width:125px; " name="modelId" >
                                    <option value="0">---Select---</option>
                                    <c:if test = "${ModelList != null}" >
                                        <c:forEach items="${ModelList}" var="Md"> 
                                            <c:choose>
                                                <c:when test="${comp.modelId == Md.modelId}" >                                                    
                                            <option selected value='<c:out value="${Md.modelId}" />'><c:out value="${Md.modelName}" /></option>                                            
                                                </c:when>
                                                <c:otherwise>                                            
                                            <option selected   value='<c:out value="${Md.modelId}" />'><c:out value="${Md.modelName}" /></option>
                                                </c:otherwise>
                                            </c:choose> 
                                        </c:forEach >
                                    </c:if>  	
                            </select></td>  
                        
                            <td class="text2" height="30"><b>Mfr Code</b></td>
                            <td class="text2" height="30"><input name="mfrCode" type="text" style="width:125px; " class="textbox" value='<c:out value="${comp.mfrCode}" />' ></td>
                        </tr>
                        
                        <tr height="30">
                            <td class="text1"><b>PaplCode</b></td>
                            <td class="text1"><input name="paplCode" style="width:125px; " type="text" class="textbox" value=<c:out value="${comp.paplCode}" />></td>
                        
                            <td class="text1" height="30"><b>Item Name</b><input type="hidden" name="itemId" value='<c:out value="${comp.itemId}" />' ></td>
                            <td class="text1" height="30"><input name="itemName" type="text" class="textbox" value='<c:out value="${comp.itemName}" />' ></td>
                        </tr>
                        <tr>
                            <td class="text2" height="30"><b>Uom</b> </td>
                            <td class="text2"  ><select class="textbox" style="width:125px; " readonly="yes" name="uomId" >
                                    <option value="0">---Select---</option>
                                    <c:if test = "${UomList != null}" >
                                        <c:forEach items="${UomList}" var="UM"> 
                                            <c:choose>
                                                <c:when test="${comp.uomId == UM.uomId}" >                                                                                                
                                            <option selected value='<c:out value="${UM.uomId}" />'><c:out value="${UM.uomName}" /></option>
                                                </c:when>
                                                <c:otherwise>                                                                                        
                                            <option value='<c:out value="${UM.uomId}" />'><c:out value="${UM.uomName}" /></option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach >
                                    </c:if>  	
                            </select></td>   
                        
                            <td class="text2" height="30"><b>Group Name</b></td>
                            <td class="text2"  ><select class="textbox" style="width:125px; " readonly="yes" name="groupId" >
                                    <option value="0">---Select---</option>
                                    <c:forEach items="${groupList}" var="group">
                                        <c:choose>
                                        <c:when test="${group.groupId==comp.groupId}">
                                            <option value='<c:out value="${group.groupId}"/>' selected><c:out value="${group.groupName}"/></option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value='<c:out value="${group.groupId}"/>' ><c:out value="${group.groupName}"/></option>
                                        </c:otherwise>
                                            </c:choose>
                                    </c:forEach>

                                </select></td>
                        </tr>
                        <tr>
                            <td class="text1" height="30"><b>Tax Percentage</b></td>
                            <td class="text1"  ><select class="textbox" style="width:125px; " readonly="yes" name="vatId" >
                                    <option value="0">---Select---</option>
                                    <c:forEach items="${vatList}" var="vat">
                                        <c:choose>
                                        <c:when test="${vat.vatId==comp.vatId}">
                                            <option value='<c:out value="${vat.vatId}"/>' selected><c:out value="${vat.vat}"/></option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value='<c:out value="${vat.vatId}"/>' ><c:out value="${vat.vat}"/></option>
                                        </c:otherwise>
                                            </c:choose>
                                    </c:forEach>

                                </select></td>
                        
                            <td class="text1" height="30"><b>Item Specification</b></td>
                            <td class="text1" height="30"><textarea class="textbox" style="width:125px; " name="specification"><c:out value="${comp.specification}" /></textarea></td>

                        </tr>
                        <tr>
                            <td class="text2" height="30"><b>Is Reconditionable</b></td>
                            <td class="text2" height="30">
                                <select name="reConditionable" style="width:125px; " class="textbox" value=<c:out value="${comp.reConditionable}" /> >                                
                                <c:if test="${comp.reConditionable=='Y' || comp.reConditionable=='y' }" >
                                    <option  value='Y' selected>Yes </option>
                                    <option  value='N' >No </option>
                                </c:if>
                                <c:if test="${comp.reConditionable=='N' || comp.reConditionable=='n' }" >
                                    <option value='Y' >Yes </option>
                                    <option value='N' selected >No </option>
                                </c:if>
                            </select>
                            </td>
                        
                            <td class="text2" height="30"><b>Scrap Uom </b></td>
                            <td class="text2"  ><select class="textbox" style="width:125px; " name="scrapUomId" >
                                    <option value="0">---Select---</option>
                                    <c:if test = "${UomList != null}" >
                                        <c:forEach items="${UomList}" var="SOUM"> 
                                            <c:choose>
                                                <c:when test="${comp.scrapUomId == SOUM.uomId}" >                                                                                                
                                            <option selected value='<c:out value="${SOUM.uomId}" />'><c:out value="${SOUM.uomName}" /></option>
                                                </c:when>
                                                <c:otherwise>                                                                                        
                                            <option value='<c:out value="${SOUM.uomId}" />'><c:out value="${SOUM.uomName}" /></option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach >
                                    </c:if>  	
                            </select></td>   
                        </tr>
                        <tr>
                            <td class="text1" height="30"><b>Maximum Qty</b></td>
                            <td class="text1" height="30"><input name="maxQuandity" style="width:125px; " type="text" class="textbox" value=<c:out value="${comp.maxQuandity}" />></td>
                        
                            <td class="text1" height="30"><b>Ro Level</b></td>
                            <td class="text1" height="30"><input name="roLevel" style="width:125px; " type="text" class="textbox" value=<c:out value="${comp.roLevel}" />></td>
                        </tr>
                        <tr height="30">
                            <td class="text2"><b>Rack</b></td>
                            <td class="text2"  ><select class="textbox" style="width:125px; " name="rackId" onChange="ajaxRackData();" >
                                    <option value="0">---Select---</option>
                                    <c:if test = "${getRackList != null}" >
                                        <c:forEach items="${getRackList}" var="RK"> 
                                            <c:choose>
                                                <c:when test="${comp.rackId == RK.rackId}" >                                                    
                                            <option selected value='<c:out value="${RK.rackId}" />'><c:out value="${RK.rackName}" /></option>                                                    
                                                </c:when>
                                                <c:otherwise>
                                            <option value='<c:out value="${RK.rackId}" />'><c:out value="${RK.rackName}" /></option>                                                    
                                                </c:otherwise>
                                            </c:choose> 
                                        </c:forEach >
                                    </c:if>  	
                                </select>
                            </td>
                        
                            <td class="text2" height="30"><b>Sub Rack</b></td>
                            <td class="text2"  ><select class="textbox" style="width:125px; " name="subRackId" >
                                    <option value="0">---Select---</option>
                                    <c:if test = "${SubRackList != null}" >
                                        <c:forEach items="${SubRackList}" var="SRK"> 
                                            <c:choose>
                                                <c:when test="${comp.subRackId == SRK.subRackId}" >                                                    
                                            <option selected value='<c:out value="${SRK.subRackId}" />'><c:out value="${SRK.subRackName}" /></option>                                                    
                                                </c:when>
                                                <c:otherwise>
                                            <option value='<c:out value="${SRK.subRackId}" />'><c:out value="${SRK.subRackName}" /></option>                                                    
                                                </c:otherwise>
                                            </c:choose> 
                                        </c:forEach >
                                    </c:if>  	
                            </select></td>   
                        </tr>
                        <tr>
                        <td class="text1" height="30"><font color="red">*</font><b>Selling Price</b></td>
                        <td class="text1" height="30"><input name="sprice" style="width:125px; " type="text" class="textbox" value=<c:out value="${comp.sellingPrice}" /> onchange="computeMrp();"></td>

                        <td class="text1" height="30"><font color="red">*</font><b>MRP (with Tax)</b></td>
                        <td class="text1" height="30"><input name="mrp" style="width:125px; " readonly type="text" class="textbox" value=<c:out value="${comp.mrp}" />></td>
                        </tr>
                    </table>
                    
                    
                </c:forEach>    
            </c:if>    
            
            <br><br>
            <center><input type="button" class="button" value="Alter" name="alter" onClick="submitPage(this.name);" /></center>
            <input type="hidden" value="" name="temp" >
                <input type="hidden" name=" " value="itemId">
        </form>
    </body>
</html>
