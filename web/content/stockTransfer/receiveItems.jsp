
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ page import="ets.domain.stockTransfer.business.StockTransferTO" %> 
        
        <title>ItemList</title>
    </head>
    
    
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script language="javascript">
        
        function submitPage()
        {       
            //selectedItemValidation();
            var reqType = document.stockTransfer.reqType.value;
            alert(reqType);
            if(reqType == "New" || reqType == "NEW" ){
                document.stockTransfer.action='/throttle/receivedStockUpdate.do';
            }else if(reqType == "RC"){
                document.stockTransfer.action='/throttle/receivedStockUpdate.do';
            }
            alert(document.stockTransfer.action);
            document.stockTransfer.submit();                
        }    
        function submitRcItem(){           
            data="gdId="+document.stockTransfer.gdId.value +"&servicePtId="+document.stockTransfer.servicePtId.value +"&companyName="+document.stockTransfer.companyName.value;
            window.open('/throttle/RcItems.do?'+data , 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
           // document.stockTransfer.action='/throttle/RcItems.do';
           // document.stockTransfer.submit();
            }
        function setSelectbox(i)
        {
            var selected=document.getElementsByName("selectedIndex") ;
            selected[i].checked=1;
        }
        
        function selectedItemValidation(){
            var index = document.getElementsByName("selectedIndex");
            var acceptedQty = document.getElementsByName("acceptedQtys");       
            var issuedQty = document.getElementsByName("issuedQtys");       
            var rcQty = document.getElementsByName("rcQtys");       
            var remarks = document.getElementsByName("remarks");              
            for(var i=0;(i<index.length && index.length!=0);i++){                       
               if(index[i].checked){                   
                if( (parseInt(acceptedQty[i].value) + parseInt(rcQty[i].value)) > issuedQty[i].value){
                    alert("Accepted Quantity Should not be greater than Approved Quantity");
                    return;
            }  
        }
        }
             if(textValidation(document.stockTransfer.remarks,'Remarks')){       
                return;
            } 
            document.stockTransfer.action='/throttle/receivedStockUpdate.do';
            document.stockTransfer.submit();     
        }
       
    </script>
    <body >
        <form method="post"  name="stockTransfer">                    
<%@ include file="/content/common/path.jsp" %>                        
        
          
<%@ include file="/content/common/message.jsp" %>    
<br>

<input type="hidden" name="reqType" value="<%=request.getAttribute("reqType")%>" >
          <c:if test = "${receivedItems != null}">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="90%" id="bg" class="border">
                <tr>
                    <td colspan="9" height="80" align="center" class="contenthead" ><div class="contenthead">Receive Item</div></td>                    
                </tr>
                
                
                
                <tr>
                    <td class="text1" height="30">FROM SERVICE POINT</td>
                    <td class="text1" height="30"><%=request.getAttribute("FromServicePoint")%></td>
                    <input type="hidden" name="companyName" value="<%=request.getAttribute("FromServicePoint")%>">
                    <input type="hidden" name="servicePtId" value="<%=request.getAttribute("servicePtId")%>">
                </tr> 
                <% int index = 0;%> 
                <c:if test = "${receivedStockListAll != null}" >
                    <c:forEach items="${receivedStockListAll}" var="toService"> 
                        <% if (index == 0) {%>    
                        <tr>
                            <td class="text2" height="30">TO SERVICE POINT</td>
                            <td class="text2" height="30"> <c:out value="${toService.companyName}"/> </td>
                            <input type="hidden" name="toServicePt" value="<c:out value="${toService.companyName}"/> ">
                            <input type="hidden" name="gdId" value="<%=request.getAttribute("gdId")%>">
                        </tr>
                        <% index++;
            }%> 
                    </c:forEach>
                </c:if>
                        <tr>
                            <td class="text1" height="30">REQUEST ID</td>
                            <td class="text1" height="30"> <c:out value="${requestId}"/> </td>
                            </tr>
                
            </table>
            <br>
            <br>
            <table  width="90%" align="center" border="0" cellpadding="0" cellspacing="0"  id="bg" class="border">
                
                    <tr>
                        <td colspan="12" align="left" class="text2" height="30"><strong>Receive Items</strong> </td>
                    </tr>  
                    <tr>
                        <td class="contenthead" height="30"><div class="contenthead">Mfr Code </div></td>
                        <td class="contenthead" height="30"><div class="contenthead">Papl Code</div> </td>                       
                        <td class="contenthead" height="30"><div class="contenthead">Item Name</div> </td>
                        <td class="contenthead" height="30"><div class="contenthead">Uom </div></td>                        
                        <td class="contenthead" height="30"><div class="contenthead">Req Qty</div> </td>
                        <td class="contenthead" height="30"><div class="contenthead">App Qty</div> </td>                                            
                         <td class="contenthead" height="30"><div class="contenthead">TyreNo</div> </td>
                         <td class="contenthead" height="30"><div class="contenthead">Vat(%) </div> </td>
                        <td class="contenthead" height="30"><div class="contenthead">Price</div> </td>
                        <td class="contenthead" height="30"><div class="contenthead">Type </div></td>                       
                        <td class="contenthead" height="30"><div class="contenthead">Item Type </div></td>
                        <td class="contenthead" height="30"><div class="contenthead">Select</div></td>                       
                    </tr>  
                    <%  index = 0;%>
                    <c:forEach items="${receivedItems}" var="item"> 
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr>
                            <td class="<%=classText %>" height="30"><c:out value="${item.mfrCode}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.paplCode}"/></td>                       
                            <td class="<%=classText %>" height="30"><input type="hidden" name=itemIds value="<c:out value="${item.itemId}"/>"><c:out value="${item.itemName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.uomName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.requestedQty}"/></td>
                            <td class="<%=classText %>" height="30"><input type="hidden" name="issuedQtys" value="<c:out value="${item.issuedQty}"/>"><c:out value="${item.issuedQty}"/></td>
                            <input type="hidden" id="acceptedQtys<%= index %>" maxlength="10" name="acceptedQtys" size="5" class="textbox" value=<c:out value="${item.issuedQty}"/>  >

                            

                            <td class="<%=classText %>" height="30"><input type="hidden" id="tyreId<%= index %>" maxlength="10" name="tyreId" size="5" class="textbox" value='<c:out value="${item.tyreId}"/>'  > <c:out value="${item.tyreNo}"/> </td>
                            <input type="hidden" id="tyreNo<%= index %>" maxlength="10" name="tyreNo" size="5" class="textbox" value='<c:out value="${item.tyreNo}"/>'  >
                           <td class="<%=classText %>"><input type="hidden" name="taxs" size="5" class="textbox" value="<c:out value="${item.tax}"/>" ><c:out value="${item.tax}"/></td>                                
                           <td class="<%=classText %>"><input type="hidden" name="priceIds" size="5" class="textbox" value="<c:out value="${item.price}"/>" ><c:out value="${item.price}"/></td>                                
                           <td class="<%=classText %>"><input type="hidden" name="priceTypes" size="5" class="textbox" value="<c:out value="${item.pricetype}"/>" ><c:out value="${item.pricetype}"/></td>                                

                           <td class="<%=classText %>" height="30"><input type="text" size='5' readonly name="itemType" value='<c:out value="${item.itemType}"/>' >  </td> 
                          <td width="77" height="30" class="<%=classText %>"><input type="checkbox" readonly  checked name="selectedIndex" value='<%= index %>'></td>
                        </tr>
                        <%
                        index++;
                        %>
                    </c:forEach>
                                
            </table> 
            
            
            <br>
            <div align="center">Remarks &nbsp;&nbsp;:&nbsp;&nbsp;<textarea class="textbox" name="remarks" cols="40" rows="5" ></textarea> </div>
            <br>
            <center>   
                <input type="button" class="button" name="Accepted" value="Accept" onClick="submitPage();" > 
                
            </center>
          </c:if>    
        </form>
    </body>
</html>

