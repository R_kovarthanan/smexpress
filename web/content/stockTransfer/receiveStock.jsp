
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
    
    
    <%@ page import="ets.domain.stockTransfer.business.StockTransferTO" %> 
    <script language="javascript" src="/throttle/js/validate.js"></script>    
    <title>MRSList</title>
</head>

<script language="javascript">
    
    function submitPage()
    {     
      if(document.stockTransfer.companyId.value==0){
            alert("Please Select Service Point Name");
             document.stockTransfer.companyId.focus();
             return;
             }        
     var temp= document.stockTransfer.companyId.value.split("-");
      document.stockTransfer.companyName.value=temp[1];
      document.stockTransfer.servicePtId.value=temp[0];      
        document.stockTransfer.action='/throttle/receiveStockList.do';
        document.stockTransfer.submit();
    }
   function submitReceive(indx, type){
      var checValidate = selectedItemValidation(); 
       var gdId=document.getElementsByName("gdIds");
       var requestId=document.getElementsByName("requestIds");
        document.stockTransfer.gdId.value=gdId[indx].value;
        document.stockTransfer.requestId.value=requestId[indx].value;
        document.stockTransfer.reqType.value=type;
        document.stockTransfer.action='/throttle/receivedItems.do';
        document.stockTransfer.submit();
       }         
    function setSelectbox(i)
    {
        var selected=document.getElementsByName("selectedIndex") ;
        selected[i];
    }
    
    function selectedItemValidation(){
        var index = document.getElementsByName("selectedIndex");
        var issuedQty = document.getElementsByName("issuedQtys");       
        var remarks = document.getElementsByName("remarks");               
        for( var i=0;(i<index.length && index.length!=0);i++ ){                       
            if( numberValidation(issuedQty[i],'Issued Quantity') ) {   
                return;
            }                        
        }
    }        
    function setValues() {       
        document.stockTransfer.companyId.focus();                     
    }    
    
    
    </script>
    
    <body onLoad=" setValues()" >
     <form method="post"  name="stockTransfer">                    
     
<%@ include file="/content/common/path.jsp" %>               
<%@ include file="/content/common/message.jsp" %>    

  
    <table align="center" width="400" border="0" cellspacing="0" cellpadding="0" class="border">
        <tr>
            <td class="text1" height="30">From Service Point</td>  
            <td class="text1"> 
                <select class="textbox" name="companyId" style="width:125px">
                    <option value='0' >-Select-</option>  
                    <c:if test = "${servicePointList != null}" >
                        <c:forEach items="${servicePointList}" var="company"> 
                            <option value='<c:out value="${company.companyId}"/>-<c:out value="${company.companyName}"/>'><c:out value="${company.companyName}"/></option>                           
                      </c:forEach>  
                    </c:if>
                </select>
            </td>
            <input type="hidden" name="companyName" value="<%=request.getAttribute("FromServicePoint")%>" >
            <input type="hidden" name="servicePtId" value="<%=request.getAttribute("servicePtId")%>" >
                <input type="hidden" name="gdId" value="" >
                <input type="hidden" name="requestId" value="" >
                <input type="hidden" name="reqType" value="" >
            
            <td class="text1">
            <input type="button" align="center" class="button" name="search" value="Search" onclick="submitPage();"></td>
        </tr>
    </table>
    <% int index = 0;%>    
    <br>
    <c:if test = "${receivedStockListAll != null}" >
        <table width="500" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
            <tr>
                    <td colspan="4" height="20" align="center" class="text2" ><strong>Received StockList</strong></td>                    
                </tr>
            <tr align="center">
                <td align="left" width="35" height="30" class="contentsub"><div class="contentsub">S.No</div></td>
                <td align="left" width="35" height="30" class="contentsub"><div class="contentsub">Request No</div></td>
                <td align="left" width="45" height="30" class="contentsub"><div class="contentsub">GD No</div></td>
                <td align="left" width="105" height="30" class="contentsub"><div class="contentsub">To ServicePoint</div></td>
                <td align="left" width="55" height="30" class="contentsub"><div class="contentsub">Receive</div></td>                
            </tr>                   
            
            <c:forEach items="${receivedStockListAll}" var="list"> 
                
                <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                %>         
                <tr> 
                    <td class="<%=classText %>" align="left" height="30"><%=index + 1%></td>
                    <td class="<%=classText %>" align="left" height="30"><input type="hidden" name="requestIds" value="<c:out value="${list.requestId}"/>"><c:out value="${list.requestId}"/></td>
                    <td class="<%=classText %>" align="left" height="30"><input type="hidden" name="gdIds" value="<c:out value="${list.gdId}"/>"><c:out value="${list.gdId}"/></td>
                    <td class="<%=classText %>" align="left" height="30"><input type="hidden" name="servicePtName" value="<c:out value="${list.companyName}"/>"><c:out value="${list.companyName}"/></td>                           
                    <td class="<%=classText %>" align="left"> <a  href="#" onclick="submitReceive(<%=index%>,'<c:out value="${list.reqType}"/>');" >Receive</a> </td>
                    <input type="hidden" name="index" value="<%=index%>">
                </tr>
                <%
            index++;
                %>
            </c:forEach >
            
        </table>
    </c:if>                 
    </form>
</body>
</html>

