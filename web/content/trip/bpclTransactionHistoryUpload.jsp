<%@ include file="../common/NewDesign/header.jsp" %>
	<%@ include file="../common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<script type="text/javascript" src="/throttle/js/suest"></script>
<script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    function submitPage(value) {
        if (value == 'Proceed') {
            document.upload.action = '/throttle/saveBpclTransactionHistory.do';
            document.upload.submit();
        } else {
            document.upload.action = '/throttle/bpclTransactionHistoryUpload.do';
            document.upload.submit();
        }
    }
</script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head> 
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.BPCLTransactionHistory" text="BPCLTransactionHistory"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
		                    <li class=""><spring:message code="hrms.label.BPCLTransactionHistory" text="BPCLTransactionHistory"/></li>
		
		                </ol>
		            </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
    <body>
        <form name="upload" method="post" enctype="multipart/form-data">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
            
            
            

            <%
            if(request.getAttribute("errorMessage")!=null){
            String errorMessage=(String)request.getAttribute("errorMessage");                
            %>
            <center><b><font color="red" size="1"><%=errorMessage%></font></b></center>
                        <%}%>
          <table class="table table-info mb30 table-hover"  >
                <thead>
                <tr>
                    <th  colspan="4">BPCL Transaction History Upload</th>
                </tr>
                </thead>
                <tr>
                    <td >Select file</td>
                    <td ><input type="file" name="importBpclTransaction" id="importBpclTransaction" class="importBpclTransaction"></td>                             
                    <td >Last Transaction Date</td>
                    <td ><c:out value="${lastBpclTxDate}"/></td>                             
                </tr>
                <tr>
                    <td colspan="4"  align="center" ><input type="button" value="Submit" name="Submit"  class="btn btn-success" vonclick="submitPage(this.value)" >
                </tr>
            </table>
            
            
            
            <% int i = 1 ;%>
            <% int index = 0;%> 
            <%int oddEven = 0;%>
            <%  String classText = "";%>    
            <c:if test="${bpclTransactionList != null}">
                <table>
                    <th  colspan="15">File Uploaded Details&nbsp;:<input type="hidden" name="transactionHistoryId" value="<c:out value="${transactionHistoryId}"/>"/> <c:out value="${transactionHistoryId}"/></th>
                    <tr>
                        <td >S No</td>
                        <td >Status</td>
                        <td >Trip Code</td>
                        <td >Vehicle No</td>
                        <td >Account Id</td>
                        <td >Dealer Name</td>
                        <td >Dealer City</td>
                        <td >Transaction Date</td>
                        <td >Accounting Date</td>
                        <td >Transaction Type</td>
                        <td >Currency</td>
                        <td >Transaction Amount</td>
                        <td >Volume/Doc No</td>
                        <td >Amount Balance</td>
                        <td >Petromiles Earned</td>
                        <td >Odometer Reading</td>
                    </tr>
                    <c:forEach items="${bpclTransactionList}" var="bpcl">
                        <%
                      oddEven = index % 2;
                      if (oddEven > 0) {
                          classText = "text2";
                      } else {
                          classText = "text1";
                        }
                        %>
                        <tr>
                            <c:if test="${bpcl.status == 'N'}">
                                <td  ><font color="green"><%=i++%></font></td>
                                <td  ><font color="green"><input type="hidden" name="status" id="status" value="<c:out value="${bpcl.status}"/>" />OK</font></td>
                                <td  ><font color="green"><input type="hidden" name="vehicleNo" id="vehicleNo" value="<c:out value="${bpcl.vehicleNo}"/>" /><c:out value="${bpcl.vehicleNo}"/></font></td>
                                <td  ><font color="green"><input type="hidden" name="accountId" id="accountId" value="<c:out value="${bpcl.accountId}"/>" /><c:out value="${bpcl.accountId}"/></font></td>
                                <td  ><font color="green"><input type="hidden" name="dealerName" id="dealerName" value="<c:out value="${bpcl.dealerName}"/>" /><c:out value="${bpcl.dealerName}"/></font></td>
                                <td  ><font color="green"><input type="hidden" name="dealerCity" id="dealerCity" value="<c:out value="${bpcl.dealerCity}"/>" /><c:out value="${bpcl.dealerCity}"/></font></td>
                                <td  ><font color="green"><input type="hidden" name="transactionDate" id="transactionDate" value="<c:out value="${bpcl.transactionDate}"/>" /><c:out value="${bpcl.transactionDate}"/></font></td>
                                <td  ><font color="green"><input type="hidden" name="accountingDate" id="accountingDate" value="<c:out value="${bpcl.accountingDate}"/>" /><c:out value="${bpcl.accountingDate}"/></font></td>
                                <td  ><font color="green"><input type="hidden" name="transactionType" id="transactionType" value="<c:out value="${bpcl.transactionType}"/>" /><c:out value="${bpcl.transactionType}"/></font></td>
                                <td  ><font color="green"><input type="hidden" name="currency" id="currency" value="<c:out value="${bpcl.currency}"/>" /><c:out value="${bpcl.currency}"/></font></td>
                                <td  align="right"><font color="green"><input type="hidden" name="amount" id="amount" value="<c:out value="${bpcl.amount}"/>" /><c:out value="${bpcl.amount}"/></font></td>
                                <td  ><font color="green"><input type="hidden" name="volumeDocNo" id="volumeDocNo" value="<c:out value="${bpcl.volumeDocNo}"/>" /><c:out value="${bpcl.volumeDocNo}"/></font></td>
                                <td  align="right"><font color="green"><input type="hidden" name="amoutBalance" id="amoutBalance" value="<c:out value="${bpcl.amoutBalance}"/>" /><c:out value="${bpcl.amoutBalance}"/></font></td>
                                <td  align="right"><font color="green"><input type="hidden" name="petromilesEarned" id="petromilesEarned" value="<c:out value="${bpcl.petromilesEarned}"/>" /><c:out value="${bpcl.petromilesEarned}"/></font></td>
                                <td  align="right"><font color="green"><input type="hidden" name="odometerReading" id="odometerReading" value="<c:out value="${bpcl.odometerReading}"/>" /><c:out value="${bpcl.odometerReading}"/></font></td>   
                                    </c:if>
                                    <c:if test="${bpcl.status == 'Y'}">
                                <td  ><font color="red"><%=i++%></font></td>
                                <td  ><font color="red"><input type="hidden" name="status" id="status" value="<c:out value="${bpcl.status}"/>" />NOT OK</font></td>
                                <td  ><font color="red"><input type="hidden" name="vehicleNo" id="vehicleNo" value="<c:out value="${bpcl.vehicleNo}"/>" /><c:out value="${bpcl.vehicleNo}"/></font></td>
                                <td  ><font color="red"><input type="hidden" name="accountId" id="accountId" value="<c:out value="${bpcl.accountId}"/>" /><c:out value="${bpcl.accountId}"/></font></td>
                                <td  ><font color="red"><input type="hidden" name="dealerName" id="dealerName" value="<c:out value="${bpcl.dealerName}"/>" /><c:out value="${bpcl.dealerName}"/></font></td>
                                <td  ><font color="red"><input type="hidden" name="dealerCity" id="dealerCity" value="<c:out value="${bpcl.dealerCity}"/>" /><c:out value="${bpcl.dealerCity}"/></font></td>
                                <td  ><font color="red"><input type="hidden" name="transactionDate" id="transactionDate" value="<c:out value="${bpcl.transactionDate}"/>" /><c:out value="${bpcl.transactionDate}"/></font></td>
                                <td  ><font color="red"><input type="hidden" name="accountingDate" id="accountingDate" value="<c:out value="${bpcl.accountingDate}"/>" /><c:out value="${bpcl.accountingDate}"/></font></td>
                                <td  ><font color="red"><input type="hidden" name="transactionType" id="transactionType" value="<c:out value="${bpcl.transactionType}"/>" /><c:out value="${bpcl.transactionType}"/></font></td>
                                <td  ><font color="red"><input type="hidden" name="currency" id="currency" value="<c:out value="${bpcl.currency}"/>" /><c:out value="${bpcl.currency}"/></font></td>
                                <td  align="right"><font color="red"><input type="hidden" name="amount" id="amount" value="<c:out value="${bpcl.amount}"/>" /><c:out value="${bpcl.amount}"/></font></td>
                                <td  ><font color="red"><input type="hidden" name="volumeDocNo" id="volumeDocNo" value="<c:out value="${bpcl.volumeDocNo}"/>" /><c:out value="${bpcl.volumeDocNo}"/></font></td>
                                <td  align="right"><font color="red"><input type="hidden" name="amoutBalance" id="amoutBalance" value="<c:out value="${bpcl.amoutBalance}"/>" /><c:out value="${bpcl.amoutBalance}"/></font></td>
                                <td  align="right"><font color="red"><input type="hidden" name="petromilesEarned" id="petromilesEarned" value="<c:out value="${bpcl.petromilesEarned}"/>" /><c:out value="${bpcl.petromilesEarned}"/></font></td>
                                <td  align="right"><font color="red"><input type="hidden" name="odometerReading" id="odometerReading" value="<c:out value="${bpcl.odometerReading}"/>" /><c:out value="${bpcl.odometerReading}"/></font></td>   
                                    </c:if>

                        </tr>
                        <%index++;%>
                    </c:forEach>
                </table>
                
                
                
                <center>
                    <input type="button" class="btn btn-success"  value="Proceed" onclick="submitPage(this.value)"/>
                </center>

            </c:if>
        </form>
    </body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>