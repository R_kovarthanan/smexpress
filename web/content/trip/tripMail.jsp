<%-- 
    Document   : tripMail
    Created on : Feb 24, 2014, 7:34:29 PM
    Author     : Throttle
--%>
<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery.tokeninput.js"></script>

    <link rel="stylesheet" href="/throttle/css/token-input.css" type="text/css" />
    <link rel="stylesheet" href="/throttle/css/token-input-facebook.css" type="text/css" />

    <script>
        function submitPage() {
//            alert(document.getElementById('mailTO').value);
            if (document.getElementById('mailTO').value == '') {
                alert("Please select to mail Id");
            } else {
                document.mailConfig.action = '/throttle/sendTripDetailsMail.do';
                document.mailConfig.submit();
            }
        }
    </script>    
</head>

<body>
    <form name="mailConfig">
        <c:if test="${consignmentPaymentDetails != null}">
            <c:forEach items="${consignmentPaymentDetails}" var="val">
                <table align="center" width="800px" class="table">
                    <tr>
                        <!--<h2 id="prevent-duplicates">Trip Mail</h2>-->
                        <td class="text1">TO</td>
                        <td class="text1">
                            <div>
                                <input type="text" id="mailTO" name="mailTO" />
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        $("#mailTO").tokenInput("/throttle/getTOEmailList.do", {
                                            preventDuplicates: true,
                                            allowCustomEntry: true
                                        });
                                    });
                                </script>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="text1">CC</td>
                        <td class="text1">
                            <div>
                                <input type="text" id="mailCC" name="mailCC" />
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        $("#mailCC").tokenInput("/throttle/getCCEmailList.do", {
                                            preventDuplicates: true
                                        });
                                    });
                                </script>
                            </div>
                        </td>    
                    </tr>
                    <tr>
                        <td class="text2">Subject</td>
                        <td class="text2"><textarea id="subject" name="subject" style="width: auto"></textarea></td>
                        <td class="text2" colspan="2"><img src="images/mailSendingImage.jpg" alt="Send Mail"   title="Sen Mail" style="width: 30px;height: 30px" onclick="submitPage()"/>
                            <br>
                            Send Mail</td>
                    </tr>
                    <tr>
                        <td class="text1">Consignment Date</td>
                        <td class="text1"><input type="hidden" id="consignmentDate" name="consignmentDate" value="<c:out value="${val.consignmentDate}"/>" /><c:out value="${val.consignmentDate}"/></td>
                        <td class="text1">Consignment No</td>
                        <td class="text1"><input type="hidden" id="consignmentOrderNos" name="consignmentOrderNos" value="<c:out value="${val.consignmentOrderNos}"/>" /><c:out value="${val.consignmentOrderNos}"/></td>
                    </tr>
                    <tr>
                        <td class="text1">Reefer Required</td>
                        <td class="text1"><input type="hidden" id="reeferRequired" name="reeferRequired" value="<c:out value="${val.reeferRequired}"/>" /><c:out value="${val.consignmentDate}"/></td>
                        <td class="text1">Freight Amount</td>
                        <td class="text1"><input type="hidden" id="freightAmount" name="freightAmount" value="<c:out value="${val.freightAmount}"/>" /><c:out value="${val.consignmentOrderNos}"/></td>
                    </tr>

                    <tr>
                        <td class="text2">Customer Name</td>
                        <td class="text2"><input type="hidden" id="customerName" name="customerName" value="<c:out value="${val.customerName}"/>" /><c:out value="${val.customerName}"/></td>
                        <td class="text2">Vehicle Required Date &amp; Time</td>
                        <td class="text2"><input type="hidden" id="vehicleRequiredDateTime" name="vehicleRequiredDateTime" value="<c:out value="${val.vehicleRequiredDateTime}"/>" /><c:out value="${val.vehicleRequiredDateTime}"/></td>
                    </tr>

                    <tr>
                        <td class="text1">Consignor Name</td>
                        <td class="text1"><input type="hidden" id="consignorName" name="consignorName" value="<c:out value="${val.consignorName}"/>" /><c:out value="${val.consignorName}"/></td>
                        <td class="text1">Consignee Name</td>
                        <td class="text1"><input type="hidden" id="consigneeName" name="consigneeName" value="<c:out value="${val.consigneeName}"/>" /><c:out value="${val.consigneeName}"/></td>
                    </tr>
                    <tr>
                        <td class="text2">Total Packages</td>
                        <td class="text2"><input type="hidden" id="totalPackages" name="totalPackages" value="<c:out value="${val.totalPackages}"/>" /><c:out value="${val.totalPackages}"/></td>
                        <td class="text2">Total Weight</td>
                        <td class="text2"><input type="hidden" id="totalWeight" name="totalWeight" value="<c:out value="${val.totalWeight}"/>" /><c:out value="${val.totalWeight}"/></td>
                    </tr>

                    <tr>
                        <td class="text1">Total Hours</td>
                        <td class="text1"><input type="hidden" id="totalKM" name="totalKM" value="<c:out value="${val.totalKM}"/>" /><c:out value="${val.totalKM}"/></td>
                        <td class="text1">Total Mins</td>
                        <td class="text1"><input type="hidden" id="totalHrs" name="totalHrs" value="<c:out value="${val.totalHrs}"/>" /><c:out value="${val.totalHrs}"/></td>
                    </tr>
                    <tr>
                        <td class="text2">Freight Amount</td>
                        <td class="text2"><input type="hidden" id="totalMins" name="totalMins" value="<c:out value="${val.totalMins}"/>" /><c:out value="${val.totalMins}"/></td>
                        <td class="text2">Payment Type</td>
                        <td class="text2"><input type="hidden" id="paymentType" name="paymentType" value="<c:out value="${val.paymentType}"/>" /><c:out value="${val.paymentType}"/></td>
                    </tr>

                    <tr>
                        <td class="text1">Trip Code</td>
                        <td class="text1"><input type="hidden" id="tripCode" name="tripCode" value="<c:out value="${val.tripCode}"/>" /><c:out value="${val.tripCode}"/></td>
                        <td class="text1">Route Info</td>
                        <td class="text1"><input type="hidden" id="routeInfo" name="routeInfo" value="<c:out value="${val.routeInfo}"/>" /><c:out value="${val.routeInfo}"/></td>
                    </tr>

                    <tr>
                        <td class="text2">Estimated Expense</td>
                        <td class="text2"><input type="hidden" id="estimatedExpense" name="estimatedExpense" value="<c:out value="${val.estimatedExpense}"/>" /><c:out value="${val.estimatedExpense}"/></td>
                        <td class="text2">Advance Amount</td>
                        <td class="text2"><input type="hidden" id="advanceAmount" name="advanceAmount" value="<c:out value="${val.advanceAmount}"/>" /><c:out value="${val.advanceAmount}"/></td>
                    </tr>

                    <tr>
                        <td class="text1">Billing Type</td>
                        <td class="text1"><input type="hidden" id="billingType" name="billingType" value="<c:out value="${val.billingType}"/>" /><c:out value="${val.billingType}"/></td>
                        <td class="text1">Product Info</td>
                        <td class="text1"><input type="hidden" id="productInfo" name="productInfo" value="<c:out value="${val.productInfo}"/>" /><c:out value="${val.productInfo}"/></td>
                    </tr>

                    <tr>
                        <td class="text2">Vehicle Type</td>
                        <td class="text2"><input type="hidden" id="vehicleType" name="vehicleType" value="<c:out value="${val.vehicleType}"/>" /><c:out value="${val.vehicleType}"/></td>
                        <td class="text2">Paid Amount</td>
                        <td class="text2"><input type="hidden" id="paidAmount" name="paidAmount" value="<c:out value="${val.paidAmount}"/>" /><c:out value="${val.paidAmount}"/></td>
                    </tr>

                    <tr>
                        <td class="text1">Vehicle No</td>
                        <td class="text1"><input type="hidden" id="regNo" name="regNo" value="<c:out value="${val.regNo}"/>" /><c:out value="${val.regNo}"/></td>
                        <td class="text1">Driver Name</td>
                        <td class="text1"><input type="hidden" id="driverName" name="driverName" value="<c:out value="${val.driverName}"/>" /><c:out value="${val.driverName}"/></td>
                    </tr>

                    <tr>
                        <td class="text2">Driver Mobile</td>
                        <td class="text2"><input type="hidden" id="driverMobile" name="driverMobile" value="<c:out value="${val.driverMobile}"/>" /><c:out value="${val.driverMobile}"/></td>
                        <td class="text2">Vehicle Current Location</td>
                        <td class="text2"><input type="hidden" id="currentLocation" name="currentLocation" value="<c:out value="${val.currentLocation}"/>" /><c:out value="${val.currentLocation}"/></td>
                    </tr>

                </table>
            </c:forEach>
        </c:if>
        <c:if test="${consignmentPaymentDetails == null}">
            <h2 id="prevent-duplicates">No Records Found</h2>
            <script>
                window.close();
            </script>    
        </c:if>
    </form>
</body>
</html>