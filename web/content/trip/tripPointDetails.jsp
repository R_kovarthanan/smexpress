<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
<!--                <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                     yearRange: '1900:' + new Date().getFullYear(),
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>


        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>

         
       <script type="text/javascript" language="javascript">
           
            function submitPage(){
                 var errorMsg1="";
                if(isEmpty(document.getElementById("startDate").value)){
                    alert('please enter plan start date');
                    document.getElementById("startDate").focus();
                }
                if(isEmpty(document.getElementById("startOdometerReading").value)){
                    alert('please enter start odometer reading');
                    document.getElementById("startOdometerReading").focus();
                    return;
                }
                if(isEmpty(document.getElementById("vehicleactreportdate").value)){
                    alert('please enter the vehicle reporting date');
                    document.getElementById("vehicleactreportdate").focus();
                }
                if(isEmpty(document.getElementById("vehicleloadtemperature").value)){
                    alert('please enter the loading temperature');
                    document.getElementById("vehicleloadtemperature").focus();
                }
                if(isEmpty(document.getElementById("startHM").value)){
                    alert('please enter start HM');
                    document.getElementById("startHM").focus();
                }
                if(parseFloat(document.getElementById("vehicleloadtemperature").value) == 0){
                    errorMsg1 += "The value of vehicle temperature is 0\n";
                }
                if(parseFloat(document.getElementById("startOdometerReading").value) == 0 ){
                    errorMsg1 += "The value of start odometer meter reading is 0\n";
                }
                if(parseFloat(document.getElementById("startHM").value) == 0){
                    errorMsg1 += "The value of start hour meter  reading is 0";
                }
                  if(errorMsg1 != ""){
                var r=confirm(errorMsg1);
                if (r==true)
              {
                    document.trip.action = '/throttle/updateStartTripSheet.do';
                    document.trip.submit();
                }
            }else{
                    document.trip.action = '/throttle/updateStartTripSheet.do';
                    document.trip.submit();
                
            }
            }
        </script>



<div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.StartTripSheet" text="Start Trip Sheet "/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.SecondaryOperations" text="SecondaryOperations"/></a></li>
                <li class=""><spring:message code="hrms.label.TripSheet" text="Start Trip Sheet "/></li>
            </ol>
            <div class="contentpanel">
                <div class="panel panel-default">
                    <div class="panel-body">


    <body onload="addRow1();">

        <form name="trip" method="post">
             <%
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String startDate = sdf.format(today);
        %>
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <br>

          
            <br>
            <br>
            <table class="table table-info mb30 table-hover">
                <% int loopCntr = 0;%>
                <c:if test = "${tripDetails != null}" >
                    <c:forEach items="${tripDetails}" var="trip">
                        <% if(loopCntr == 0) {%>
                        <thead> <tr>
                            <th  >Vehicle: <c:out value="${trip.vehicleNo}" /></th>
                            <th  >Trip Code: <c:out value="${trip.tripCode}"/></th>
                            <th  >Customer Name:&nbsp;<c:out value="${trip.customerName}"/></th>
                            <th  >Route: &nbsp;<c:out value="${trip.routeInfo}"/></th>
                            <th  >Status: <c:out value="${trip.status}"/></th>
                            <input type="hidden" name="tripCodeEmail" value='<c:out value="${trip.tripCode}"/>' />
                            <input type="hidden" name="customerNameEmail" value='<c:out value="${trip.customerName}"/>' />
                            <input type="hidden" name="routeInfoEmail" value='<c:out value="${trip.routeInfo}"/>' />
                            <input type="hidden" name="tripType" value='<c:out value="${tripType}"/>' />
                            <input type="hidden" name="statusId" value='<c:out value="${statusId}"/>' />
            </tr></thaed>
                        <% }%>
                        <% loopCntr++;%>
                    </c:forEach>
                </c:if>
            </table>
        
                    <c:if test = "${tripPointDetails != null}" >
                    <table class="table table-info mb30 table-hover" >
                        <thead><tr   >
                            <th colspan="8">Trip Point Details</th>
                        </tr>
                        <tr >
                                <th  height="30" >S No</th>
                                <th  height="30" >Point Name</th>
                                <th  height="30" >Type</th>
                                <th  height="30" >Route Order</th>
                                <th  height="30" >Address</th>
                                <th  height="30" >Planned Date</th>
                                <th  height="30" >Planned Time</th>
                                <th  height="30" >Select</th>
                        </tr></thead>
                        <% int index2 = 1; %>
                        <c:forEach items="${tripPointDetails}" var="tripPoint">
                         <%
                                        String classText1 = "";
                                        int oddEven = index2 % 2;
                                        if (oddEven > 0) {
                                            classText1 = "text1";
                                        } else {
                                            classText1 = "text2";
                                        }
                            %>
                        <tr >
                                 <td  height="30" ><%=index2++%></td>
                                <td  height="30" ><c:out value="${tripPoint.pointName}" /></td>
                                <td  height="30" ><c:out value="${tripPoint.pointType}" /></td>
                                <td  height="30" ><c:out value="${tripPoint.pointSequence}" /></td>
                                <td  height="30" ><c:out value="${tripPoint.pointAddress}" /></td>
                                <td  height="30" ><c:out value="${tripPoint.pointPlanDate}" /></td>
                                <td  height="30" ><c:out value="${tripPoint.pointPlanTime}" /></td>
                                <td  height="30" >
                                     <c:if test = "${tripPoint.pointSequence != '1' && tripPoint.pointType eq 'Start Point' || tripPoint.pointType eq 'Pick Up'}" >
                                      <a href="viewTripPointDetails.do?tripRouteCourseId=<c:out value="${tripPoint.tripRouteCourseId}"/>&pointType=<c:out value="${tripPoint.pointType}"/>&tripId=<c:out value="${tripPoint.tripId}"/>&pointName=<c:out value="${tripPoint.pointName}"/>">Enter Loading Details</a>  
                                    <input type="hidden" name="tripRouteCourseId" id="tripRouteCourseId" value="<c:out value="${tripPoint.tripRouteCourseId}"/>"/>
                                     </c:if>
                                     <c:if test = "${tripPoint.pointSequence > tripPointDetailsSize && tripPoint.pointType eq 'End Point' || tripPoint.pointType eq 'Drop' }" >
                                      <a href="viewTripPointDetails.do?tripRouteCourseId=<c:out value="${tripPoint.tripRouteCourseId}"/>&pointType=<c:out value="${tripPoint.pointType}"/> &pointName=<c:out value="${tripPoint.pointName}"/>&tripId=<c:out value="${tripPoint.tripId}"/>">Enter UnLoading Details</a>
                                    <input type="hidden" name="tripRouteCourseId" id="tripRouteCourseId" value="<c:out value="${tripPoint.tripRouteCourseId}"/>"/>
                                     </c:if>
                                     </td>
                      </tr>
                        </c:forEach >
                    </table>
                    <br/>
                    </c:if>
                 <br/>
                 <br/>
                 <br/>
                    <c:if test = "${tripPointDetails != null}" >
                    <table class="table table-info mb30 table-hover" >
                        <thead><tr   >
                            <th colspan="8">Reporting Details</th>
                        </tr>
                        <tr >
                                <th  height="30" >S No</th>
                                <th  height="30" >Point Name</th>
                                <th  height="30" >Point Type</th>
                                <th  height="30" >Reporting Date</th>
                                <th  height="30" >Reporting Time</th>
                                <th  height="30" >Loading & Unloading Date</th>
                                <th  height="30" >Loading & Unloading Time</th>
                                <th  height="30" >Temperature</th>
                        </tr></thead>
                        <% int index2 = 1; %>
                        <c:forEach items="${tripPointDetails}" var="tripPoint">
                         <%
                                        String classText1 = "";
                                        int oddEven = index2 % 2;
                                        if (oddEven > 0) {
                                            classText1 = "text1";
                                        } else {
                                            classText1 = "text2";
                                        }
                            %>
                        <tr >
                                 <td  height="30" ><%=index2++%></td>
                                <td  height="30" ><c:out value="${tripPoint.pointName}" /></td>
                                <td  height="30" ><c:out value="${tripPoint.pointType}" /></td>
                                <td  height="30" ><c:out value="${tripPoint.startReportingDate}" /></td>
                                <td  height="30" ><c:out value="${tripPoint.startReportingTime}" /></td>
                                <td  height="30" ><c:out value="${tripPoint.loadingDate}" /></td>
                                <td  height="30" ><c:out value="${tripPoint.loadingTime}" /></td>
                                <td  height="30" ><c:out value="${tripPoint.loadingTemperature}" /></td>
                      </tr>
                        </c:forEach >
                    </table>
                    <br/>
                    </c:if>

        </form>
    </body>
   </div>
  </div>
   </div>
<%@ include file="../common/NewDesign/settings.jsp" %>