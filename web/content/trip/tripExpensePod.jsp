<%-- 
    Document   : tripExpensePod
    Created on : Dec 6, 2013, 4:37:26 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">




<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
          <title>Upload Bill Copy</title>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true
                });
            });
        </script>
         <script type="text/javascript">
            function submitPage(){
            document.tripExpensePod.action = '/throttle/saveExpenseTripPodDetails.do';
            document.tripExpensePod.submit();
            window.close();
            }
        </script>
    </head>
    <body>

        <form name="tripExpensePod" method="post" enctype="multipart/form-data">
            <br>
            <h3>
                Upload Bill Copy
            </h3>
            <br/>
            <br/>
            
            <div id="podDetail">
                        <c:if test="${tripExpenseDocumentFileDetails == null}">
                            <center>
                                <font color="red">No Files Uploaded</font>
                            </center>
                        </c:if>
                      <c:if test="${tripExpenseDocumentFileDetails != null}">
                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                <tr>
                                    <th width="50" class="contenthead">S No&nbsp;</th>
                                    <th class="contenthead">Expense Name</th>
                                    <th class="contenthead">Bill Copy</th>
                                    <th class="contenthead">Expense Remarks</th>
                                    <th class="contenthead">Expense Value</th>
<!--                                    <th class="contenthead">Action</th>-->
                                    
                                </tr>
                                <% int index2 = 1;%>
                                <c:forEach items="${tripExpenseDocumentFileDetails}" var="viewPODDetails">
                                     <%
                                        String classText3 = "";
                                        int oddEven = index2 % 2;
                                        if (oddEven > 0) {
                                            classText3 = "text1";
                                        } else {
                                            classText3 = "text2";
                                        }
                            %>
                                    <tr>
                                        <td class="<%=classText3%>" ><%=index2++%></td>
                                        <td class="<%=classText3%>" ><c:out value="${viewPODDetails.expenseName}"/></td>
                                        <td class="<%=classText3%>" >
                                          <a onclick="viewPODFiles('<c:out value="${viewPODDetails.expenseId}"/>')" href="#"><c:out value="${viewPODDetails.billCopyName}"/></a>
                                      </td>
                                        <td class="<%=classText3%>" ><c:out value="${viewPODDetails.expenseRemarks}"/></td>
                                        <td class="<%=classText3%>" ><c:out value="${viewPODDetails.expenseValue}"/></td>
<!--                                        <td class="<%=classText3%>" > <a onclick="deletePODFiles('<c:out value="${viewPODDetails.expenseId}"/>')" href="#">Delete</a></td>-->
                                    </tr>
                                </c:forEach>

                            </table>
                        </c:if>
                     <br/>
                     <script>
                          function viewPODFiles(expenseId) {
                            window.open('/throttle/content/trip/displayExpenseBlobData.jsp?expenseId='+expenseId, 'PopupPage', 'height = 500, width = 500, scrollbars = yes, resizable = yes');
                        }
                        
               function deletePODFiles(expenseId){
            document.tripExpensePod.action = '/throttle/deleteExpensePodFiles.do?expenseId='+expenseId;
            document.tripExpensePod.submit();
           // window.close();
            }
                     </script>
                 
                     

             <div id="tripStart">
                 
                 <c:if test="${tripExpenseDocumentRequiredDetails == null}">
                      <center>
                                <font color="red">No Documents For Upload</font>
                    </center>
                 </c:if>
                  <c:if test="${tripExpenseDocumentRequiredDetails != null}">
                    <table class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="POD1">
                        <tr>
                            <td class="contenthead"  height="30" >Sno</td>
                            <td class="contenthead" >Expense Name </td>
                            <td class="contenthead"  >Expense value</td>
                            <td class="contenthead"  >Expense Remarks</td>
                            <td class="contenthead"  >File Attachment</td>
                        </tr>
                         <% int index2 = 1;%>
                        <c:forEach items="${tripExpenseDocumentRequiredDetails}" var="expense">
                             <%
                                        String classText3 = "";
                                        int oddEven = index2 % 2;
                                        if (oddEven > 0) {
                                            classText3 = "text1";
                                        } else {
                                            classText3 = "text2";
                                        }
                            %>
                         <tr>

                             <td class="<%=classText3%>" ><%=index2++%></td>
                            <input type="hidden" name="tripExpenseId" id="tripExpenseId" value="<c:out value="${expense.expenseId}"/>" />
                             <td   class="<%=classText3%>"><c:out value="${expense.expenseName}"/></td>
                             <td   class="<%=classText3%>"><c:out value="${expense.expenseValue}"/></td>
                             <td   class="<%=classText3%>"><c:out value="${expense.expenseRemarks}"/></td>
                             
                            <td class="<%=classText3%>"><input type='file' name='podFile' id="podFile" class='textbox' >
                        </tr>
                        </c:forEach>
                        <tr>
                            <td colspan="4" align="center">
                                <input type="hidden" name="tripSheetId" value="<c:out value="${tripSheetId}"/>" />
                            <input type="button" class="button" name="Save" value="Save" onclick="submitPage();"/>
                            </td>
                        </tr>

                    </table>
                  </c:if>
                    <br>
                    <br>
             </div>
        </form>
    </body>
</html>