<%--
    Document   : printTripsheet
    Created on : Oct 7, 2018, 11:46:12 AM
    Author     : Arun
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@ page import="java.util.* "%>
<%@ page import="ets.domain.util.ThrottleConstants"%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="/throttle/js/code39.js"></script>
        <title>PrintTripSheet</title>
    </head>
    <body>
        <STYLE type="text/css">

            body {margin-top: 0px;margin-left: 0px;}

            #page_1 {position:relative; overflow: hidden;margin: 75px 0px 0px 89px;padding: 0px;border: none;width: 1284px;}

            #page_1 #p1dimg1 {position:absolute;top:0px;left:0px;z-index:-1;width:919px;height:638px;}
            #page_1 #p1dimg1 #p1img1 {width:919px;height:638px;}

            #page_2 {position:relative; overflow: hidden;margin: 92px 0px 168px 89px;padding: 0px;border: none;width: 1033px;height: 534px;}

            #page_2 #p2dimg1 {position:absolute;top:1px;left:0px;z-index:-1;width:919px;height:533px;}
            #page_2 #p2dimg1 #p2img1 {width:919px;height:533px;}

            #page_3 {position:relative; overflow: hidden;margin: 91px 0px 166px 89px;padding: 0px;border: none;width: 1033px;height: 537px;}

            #page_3 #p3dimg1 {position:absolute;top:2px;left:0px;z-index:-1;width:919px;height:535px;}
            #page_3 #p3dimg1 #p3img1 {width:919px;height:535px;}

            #page_4 {position:relative; overflow: hidden;margin: 92px 1px 168px 89px;padding: 0px;border: #000000;width: 1180px;height: 670px;}

            #page_4 #p4dimg1 {position:absolute;top:1px;left:0px;z-index:-1;width:180px;height:70px;}
            #page_4 #p4dimg1 #p4img1 {width:180px;height:70px;}

            #page_5 {position:relative; overflow: hidden;margin: 92px 0px 170px 89px;padding: 0px;border: none;width: 1033px;height: 532px;}

            #page_5 #p5dimg1 {position:absolute;top:1px;left:0px;z-index:-1;width:919px;height:531px;}
            #page_5 #p5dimg1 #p5img1 {width:919px;height:531px;}

            #page_6 {position:relative; overflow: hidden;margin: 92px 0px 165px 89px;padding: 0px;border: none;width: 1033px;height: 537px;}

            #page_6 #p6dimg1 {position:absolute;top:1px;left:0px;z-index:-1;width:919px;height:536px;}
            #page_6 #p6dimg1 #p6img1 {width:919px;height:536px;}

            .dclr {clear:both;float:none;height:1px;margin:0px;padding:0px;overflow:hidden;}

            .ft0{font: 1px 'Arial';line-height: 1px;}
            .ft1{font: bold 13px 'Arial';line-height: 16px;}
            .ft2{font: bold 12px 'Arial';line-height: 15px;}
            .ft3{font: bold 12px 'Arial';color: #4f81bc;line-height: 15px;}
            .ft4{font: 1px 'Arial';line-height: 15px;}
            .ft5{font: bold 13px 'Arial';line-height: 15px;}
            .ft6{font: 1px 'Arial';line-height: 3px;}
            .ft7{font: 1px 'Arial';line-height: 9px;}
            .ft8{font: 1px 'Arial';line-height: 10px;}
            .ft9{font: 1px 'Arial';line-height: 14px;}
            .ft10{font: 1px 'Arial';line-height: 8px;}
            .ft11{font: italic 13px 'Arial';line-height: 16px;}
            .ft12{font: italic 13px 'Arial';line-height: 15px;}
            .ft13{font: bold 13px 'Arial';line-height: 14px;}
            .ft14{font: 1px 'Arial';line-height: 4px;}
            .ft15{font: bold 11px 'Arial';line-height: 14px;}
            .ft16{font: 7px 'Arial';line-height: 17px;}
            .ft17{font: bold 13px 'Arial';line-height: 0px;}
            .ft18{font: bold 11px 'Arial';line-height: 0px;}
            .ft19{font: 1px 'Arial';line-height: 0px;}
            .ft20{font: bold 11px 'Arial';line-height: 13px;}
            .ft21{font: 1px 'Arial';line-height: 13px;}
            .ft22{font: 11px 'Arial';line-height: 14px;}
            .ft23{font: 1px 'Arial';line-height: 0px;}
            .ft24{font: 11px 'Arial';line-height: 5px;}
            .ft25{font: bold 13px 'Arial';color: #4f81bc;line-height: 16px;}
            .ft26{font: bold 13px 'Arial';line-height: 13px;}
            .ft27{font: 3px 'Arial';line-height: 16px;}
            .ft28{font: 3px 'Arial';line-height: 17px;}
            .ft29{font: 1px 'Arial';line-height: 5px;}
            .ft30{font: 1px 'Arial';line-height: 11px;}
            .ft31{font: 1px 'Arial';line-height: 12px;}
            .ft32{font: bold 11px 'Arial';line-height: 12px;}

            .p0{text-align: left;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p1{text-align: left;padding-left: 60px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p2{text-align: center;padding-right: 3px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p3{text-align: center;padding-right: 81px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p4{text-align: left;padding-left: 2px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p5{text-align: left;padding-left: 4px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p6{text-align: left;padding-left: 56px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p7{text-align: left;padding-left: 1px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p8{text-align: center;padding-right: 80px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p9{text-align: right;padding-right: 73px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p10{text-align: left;padding-left: 5px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p11{text-align: right;padding-right: 8px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p12{text-align: left;padding-left: 20px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p13{text-align: right;padding-right: 163px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p14{text-align: right;padding-right: 193px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p15{text-align: left;margin-top: 0px;margin-bottom: 0px;-webkit-transform: rotate(270deg);-moz-transform: rotate(270deg);filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);direction: rtl;block-progression: lr;width:38px;height:58px;}
            .p16{text-align: left;margin-top: 0px;margin-bottom: 0px;-webkit-transform: rotate(270deg);-moz-transform: rotate(270deg);filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);direction: rtl;block-progression: lr;width:38px;height:57px;}
            .p17{text-align: left;padding-left: 71px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p18{text-align: left;padding-left: 4px;padding-right: 1368px;margin-top: 0px;margin-bottom: 0px;}
            .p19{text-align: left;padding-left: 4px;margin-top: 0px;margin-bottom: 0px;}
            .p20{text-align: center;padding-right: 1px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p21{text-align: center;padding-right: 2px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p22{text-align: left;padding-left: 57px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p23{text-align: left;padding-left: 76px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p24{text-align: left;padding-left: 13px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p25{text-align: left;margin-top: 0px;margin-bottom: 0px;-webkit-transform: rotate(270deg);-moz-transform: rotate(270deg);filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);direction: rtl;block-progression: lr;width:17px;height:64px;}
            .p26{text-align: left;margin-top: 0px;margin-bottom: 0px;-webkit-transform: rotate(270deg);-moz-transform: rotate(270deg);filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);direction: rtl;block-progression: lr;width:17px;height:56px;}
            .p27{text-align: left;padding-left: 4px;padding-right: 126px;margin-top: 13px;margin-bottom: 0px;}
            .p28{text-align: left;padding-left: 4px;margin-top: 12px;margin-bottom: 0px;}
            .p29{text-align: left;padding-left: 61px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p30{text-align: center;padding-right: 74px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p31{text-align: left;padding-left: 4px;padding-right: 126px;margin-top: 12px;margin-bottom: 0px;}

            .td0{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 72px;vertical-align: bottom;}
            .td1{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 120px;vertical-align: bottom;}
            .td2{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 20px;vertical-align: bottom;}
            .td3{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 344px;vertical-align: bottom;}
            .td4{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 79px;vertical-align: bottom;}
            .td5{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 282px;vertical-align: bottom;}
            .td6{padding: 0px;margin: 0px;width: 72px;vertical-align: bottom;}
            .td7{padding: 0px;margin: 0px;width: 120px;vertical-align: bottom;}
            .td8{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 20px;vertical-align: bottom;}
            .td9{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 344px;vertical-align: bottom;}
            .td10{padding: 0px;margin: 0px;width: 79px;vertical-align: bottom;}
            .td11{padding: 0px;margin: 0px;width: 282px;vertical-align: bottom;}
            .td12{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 207px;vertical-align: bottom;}
            .td13{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 69px;vertical-align: bottom;}
            .td14{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 68px;vertical-align: bottom;}
            .td15{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 66px;vertical-align: bottom;}
            .td16{padding: 0px;margin: 0px;width: 140px;vertical-align: bottom;}
            .td17{padding: 0px;margin: 0px;width: 69px;vertical-align: bottom;}
            .td18{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 68px;vertical-align: bottom;}
            .td19{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 72px;vertical-align: bottom;}
            .td20{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 120px;vertical-align: bottom;}
            .td21{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 20px;vertical-align: bottom;}
            .td22{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 66px;vertical-align: bottom;}
            .td23{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 140px;vertical-align: bottom;}
            .td24{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 79px;vertical-align: bottom;}
            .td25{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 282px;vertical-align: bottom;}
            .td26{padding: 0px;margin: 0px;width: 192px;vertical-align: bottom;}
            .td27{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 206px;vertical-align: bottom;}
            .td28{padding: 0px;margin: 0px;width: 138px;vertical-align: bottom;}
            .td29{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 192px;vertical-align: bottom;}
            .td30{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 206px;vertical-align: bottom;}
            .td31{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 67px;vertical-align: bottom;}
            .td32{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 139px;vertical-align: bottom;}
            .td33{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 138px;vertical-align: bottom;}
            .td34{padding: 0px;margin: 0px;width: 21px;vertical-align: bottom;}
            .td35{padding: 0px;margin: 0px;width: 67px;vertical-align: bottom;}
            .td36{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 139px;vertical-align: bottom;}
            .td37{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 419px;vertical-align: bottom;}
            .td38{padding: 0px;margin: 0px;width: 499px;vertical-align: bottom;}
            .td39{padding: 0px;margin: 0px;width: 217px;vertical-align: bottom;}
            .td40{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 21px;vertical-align: bottom;}
            .td41{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 228px;vertical-align: bottom;}
            .td42{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 71px;vertical-align: bottom;}
            .td43{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 227px;vertical-align: bottom;}
            .td44{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 71px;vertical-align: bottom;}
            .td45{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 78px;vertical-align: bottom;}
            .td46{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 78px;vertical-align: bottom;}
            .td47{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 71px;vertical-align: bottom;background: #000000;}
            .td48{padding: 0px;margin: 0px;width: 120px;vertical-align: bottom;background: #000000;}
            .td49{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 20px;vertical-align: bottom;background: #000000;}
            .td50{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 66px;vertical-align: bottom;background: #000000;}
            .td51{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 139px;vertical-align: bottom;background: #000000;}
            .td52{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 78px;vertical-align: bottom;background: #000000;}
            .td53{padding: 0px;margin: 0px;width: 282px;vertical-align: bottom;background: #000000;}
            .td54{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 19px;vertical-align: bottom;}
            .td55{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 345px;vertical-align: bottom;}
            .td56{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 19px;vertical-align: bottom;}
            .td57{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 345px;vertical-align: bottom;}
            .td58{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 208px;vertical-align: bottom;}
            .td59{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 19px;vertical-align: bottom;}
            .td60{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 67px;vertical-align: bottom;}
            .td61{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 207px;vertical-align: bottom;}
            .td62{padding: 0px;margin: 0px;width: 20px;vertical-align: bottom;}
            .td63{padding: 0px;margin: 0px;width: 68px;vertical-align: bottom;}
            .td64{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 20px;vertical-align: bottom;}
            .td65{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 68px;vertical-align: bottom;}
            .td66{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 227px;vertical-align: bottom;}
            .td67{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 73px;vertical-align: bottom;}
            .td68{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 6px;vertical-align: bottom;}
            .td69{padding: 0px;margin: 0px;width: 73px;vertical-align: bottom;}
            .td70{padding: 0px;margin: 0px;width: 6px;vertical-align: bottom;}
            .td71{padding: 0px;margin: 0px;width: 288px;vertical-align: bottom;}
            .td72{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 73px;vertical-align: bottom;}
            .td73{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 6px;vertical-align: bottom;}
            .td74{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 288px;vertical-align: bottom;}
            .td75{padding: 0px;margin: 0px;width: 211px;vertical-align: bottom;}
            .td76{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 5px;vertical-align: bottom;}
            .td77{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 5px;vertical-align: bottom;}

            .tr0{height: 19px;}
            .tr1{height: 17px;}
            .tr2{height: 24px;}
            .tr3{height: 23px;}
            .tr4{height: 15px;}
            .tr5{height: 3px;}
            .tr6{height: 21px;}
            .tr7{height: 9px;}
            .tr8{height: 10px;}
            .tr9{height: 22px;}
            .tr10{height: 14px;}
            .tr11{height: 8px;}
            .tr12{height: 18px;}
            .tr13{height: 16px;}
            .tr14{height: 34px;}
            .tr15{height: 4px;}
            .tr16{height: 38px;}
            .tr17{height: 280px;}
            .tr18{height: 0px;}
            .tr19{height: -259px;}
            .tr20{height: -260px;}
            .tr21{height: 259px;}
            .tr22{height: -241px;}
            .tr23{height: 13px;}
            .tr24{height: 33px;}
            .tr25{height: 5px;}
            .tr26{height: 11px;}
            .tr27{height: 12px;}
            .tr28{height: 20px;}

            .t0{width: 1284px;margin-top: 1px;font: bold 13px 'Arial';}
            .t1{width: 919px;font: bold 13px 'Arial';}

        </STYLE>

        <style type="text/css" media="print">
            @media print
            {                             
                #printbtn {
                    display :  none;
                }


            </style>

            <style>
                @page {
                    size: A4;
                    margin: 1cm;
                }

                .print {
                    display: none;
                }

                @media print {
                    printContent {
                        page-break-inside: avoid;
                    }

                }

                .print:last-child {
                    page-break-after: auto;
                }
            </style>

            <style type="text/css">
                #barcode {font-weight: normal; font-style: normal; line-height:normal; font-size: pt}
            </style>

            <%
            Date today = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String startDate = sdf.format(today);
            String billingPartyId = (String)request.getAttribute("billingPartyId");
            
            String header = "Consignor ,Consignee ,Transporter ,Bank ,Accounts ,Book ";
            String[] title = header.split(",");
            
            %>
            <form name="enter" method="post">
                <div id="printContent">
                    <%  
                    List titleLable = new ArrayList(title.length);
                            for (int i = 0; i < title.length; i++) {
                    %> 
                    <%if(i+1==3){%>
                    <br>
                    <% }%>
                    <%if(i+1==5){%>
                    <br>
                    <% }%>
                    <DIV id="page_4">
                        <DIV id="p4dimg1"><IMG src="images/throttle-login.png" id="p4img1"></DIV>
                        <TABLE  cellpadding=0 cellspacing=0 border="1" class="t1" >
                            <TR>
                                <TD>
                                    <TABLE  cellpadding=0 cellspacing=0 class="t1" >
                                        <TR>
                                            <TD class="tr1 td0">&nbsp;</TD>
                                            <TD class="tr1 td1">&nbsp;</TD>
                                            <TD class="tr1 td54">&nbsp;</TD>
                                            <TD colspan=4 class="tr1 td55">KerryIndev Logistics Private Limited,</TD>
                                            <TD class="tr1 td4">&nbsp;</TD>
                                            <TD class="tr1 td5">&nbsp;</TD>
                                        </TR>
                                        <TR>
                                            <TD class="tr13 td6">&nbsp;</TD>
                                            <TD class="tr13 td7">&nbsp;</TD>
                                            <TD class="tr13 td56">&nbsp;</TD>
                                            <TD colspan=4 class="tr13 td57">B7&B8, Sipcot Industrial Park Irungattukottai,</TD>
                                            <TD class="tr13 td10">&nbsp;</TD>
                                            <TD class="tr13 td11">&nbsp;</TD>
                                        </TR>
                                        <TR>
                                            <TD class="tr1 td6">&nbsp;</TD>
                                            <TD class="tr1 td7">&nbsp;</TD>
                                            <TD class="tr1 td56">&nbsp;</TD>
                                            <TD colspan=4 class="tr1 td57">Sriperumbudur 602 117, TamilNadu, India</TD>
                                            <TD class="tr1 td10">&nbsp;</TD>
                                            <TD class="tr1 td11">LR / <%  out.println(title[i]);   %>  Copy-Orginal</TD>
                                        </TR>
                                        <TR>
                                            <TD class="tr28 td6">&nbsp;</TD>
                                            <TD class="tr28 td7">&nbsp;</TD>
                                            <TD class="tr28 td56">&nbsp;</TD>
                                            <TD colspan=2 class="tr0 td58">&nbsp;</TD>
                                            <TD class="tr0 td13">&nbsp;</TD>
                                            <TD class="tr0 td14">&nbsp;</TD>
                                            <TD class="tr28 td10">&nbsp;</TD>
                                            <TD class="tr28 td11">&nbsp;</TD>
                                        </TR>
                                        <TR>
                                            <TD class="tr13 td19">&nbsp;</TD>
                                            <TD class="tr13 td20">&nbsp;</TD>
                                            <TD class="tr13 td59">&nbsp;</TD>
                                            <TD class="tr13 td60">&nbsp;GST No.</TD>
                                            <TD class="tr13 td23">33AABCC1756D1Z7</TD>
                                            <TD class="tr13 td13">&nbsp;</TD>
                                            <TD class="tr13 td14">&nbsp;</TD>
                                            <TD class="tr13 td24">&nbsp;</TD>
                                            <TD class="tr13 td25">&nbsp;</TD>
                                        </TR>
                                        <TR>
                                            <TD colspan=2 class="tr10 td29">&nbsp;Truck No :</TD>
                                            <TD class="tr10 td59">&nbsp;</TD>
                                            <TD colspan=2 class="tr10 td61"><c:out value='${vehicleNo}'/></TD>
                                            <TD colspan=2 class="tr4 td28">Delivery Address</TD>
                                            <TD class="tr4 td10">&nbsp;</TD>
                                            <TD class="tr4 td11"></TD>
                                        </TR>
                                        <TR>
                                            <TD colspan=2 class="tr10 td29">&nbsp;Lr No :</TD>
                                            <TD class="tr10 td59">&nbsp;</TD>
                                            <TD colspan=2 class="tr10 td61"><c:out value="${lrNumber}"/></TD>
                                            <TD class="tr4 td10">&nbsp;Invoice No.</TD>
                                            <TD class="tr4 td10">&nbsp;&nbsp;<c:out value="${invoiceNumber}"/></TD>
                                            <TD class="tr4 td10">&nbsp;</TD>
                                            <TD class="tr4 td10"> DATED  <c:out value="${invoiceDate}"/></TD>
                                        </TR>
                                        <TR>
                                            <TD colspan=2 class="tr1 td29">&nbsp;Date of Booking :</TD>
                                            <TD class="tr1 td59">&nbsp;</TD>
                                            <TD colspan=2 class="tr1 td61"><c:out value="${consignmentDate}"/></TD>
                                            <TD colspan=2 class="tr1 td33">&nbsp;Declared Value of the</TD>
                                            <TD class="tr1 td24">Rs</TD>
                                            <TD class="tr1 td25"><c:out value="${invoiceValue}"/></TD>
                                        </TR>
                                        <TR>
                                            <TD colspan=2 class="tr23 td26">From</TD>
                                            <TD class="tr23 td62">&nbsp;</TD>
                                            <TD class="tr23 td63">&nbsp;</TD>
                                            <TD class="tr23 td36">&nbsp;</TD>
                                            <TD class="tr23 td17">To</TD>
                                            <TD class="tr23 td17">&nbsp;</TD>
                                            <TD class="tr23 td10">&nbsp;</TD>
                                            <TD class="tr23 td11">&nbsp;</TD>
                                        </TR>
                                        <TR>
                                            <TD colspan=5 class="tr4 td37"><c:out value="${consignorContactPerson}"/><br><c:out value="${consignorName}"/></TD>
                                            <TD colspan=4 class="tr4 td38"><c:out value="${consigneeContactPerson}"/><br><c:out value="${consigneeName}"/></TD>
                                        </TR>
                                        <TR>
                                            <TD colspan=5 class="tr4 td37"><c:out value="${consignorAddress}"/></TD>
                                            <TD colspan=2 class="tr4 td28"><c:out value="${consigneeAddress}"/></TD>
                                            <TD class="tr4 td10">&nbsp;</TD>
                                            <TD class="tr4 td11">&nbsp;</TD>
                                        </TR>
                                        <TR>
                                            <TD colspan=5 class="tr4 td37"><c:out value="${consignorMobileNo}"/></TD>
                                            <TD colspan=4 class="tr4 td38"><c:out value="${consigneeMobileNo}"/></TD>
                                        </TR>
                                        <TR>
                                            <TD colspan=5 class="tr13 td37"</TD>
                                            <TD colspan=3 class="tr13 td39"></TD>
                                            <TD class="tr13 td11">&nbsp;</TD>
                                        </TR>
                                        <TR>
                                            <TD colspan=5 class="tr4 td37"></TD>
                                            <TD colspan=2 class="tr4 td28"></TD>
                                            <TD class="tr4 td10">&nbsp;</TD>
                                            <TD class="tr4 td11">&nbsp;</TD>
                                        </TR>
                                        <TR>
                                            <TD colspan=2 class="tr13 td26"></TD>
                                            <TD class="tr13 td62">&nbsp;</TD>
                                            <TD class="tr13 td63">&nbsp;</TD>
                                            <TD class="tr13 td36">&nbsp;</TD>
                                            <TD colspan=2 class="tr13 td28"></TD>
                                            <TD class="tr13 td10">&nbsp;</TD>
                                            <TD class="tr13 td11">&nbsp;</TD>
                                        </TR>
                                        <TR>
                                            <TD colspan=2 class="tr4 td26"></TD>
                                            <TD class="tr4 td62">&nbsp;</TD>
                                            <TD class="tr4 td63">&nbsp;</TD>
                                            <TD class="tr4 td36">&nbsp;</TD>
                                            <TD class="tr4 td17">&nbsp;</TD>
                                            <TD class="tr4 td17">&nbsp;</TD>
                                            <TD class="tr4 td10">&nbsp;</TD>
                                            <TD class="tr4 td11">&nbsp;</TD>
                                        </TR>
                                        <TR>
                                            <TD class="tr14 td19">&nbsp;</TD>
                                            <TD class="tr14 td20">&nbsp;</TD>
                                            <TD class="tr14 td64">&nbsp;</TD>
                                            <TD class="tr14 td65">&nbsp;</TD>
                                            <TD class="tr14 td32">&nbsp;</TD>
                                            <TD class="tr14 td13">&nbsp;</TD>
                                            <TD class="tr14 td13">&nbsp;</TD>
                                            <TD class="tr14 td24">&nbsp;</TD>
                                            <TD class="tr14 td25">&nbsp;</TD>
                                        </TR>
                                        <TR>
                                            <TD colspan=2 class="tr13 td29">&nbsp;E-WAY BILL:</TD>
                                            <TD class="tr13 td64">&nbsp;<c:out value="${eWayBillNo}"/></TD>
                                            <TD class="tr13 td65">&nbsp;</TD>
                                            <TD class="tr4 td14">&nbsp;</TD>
                                            
                                            <TD class="tr13 td23"><c:out value="${sealType}"/>:</TD>
                                            <TD class="tr13 td13">&nbsp;<c:out value="${sealNo}"/></TD>
                                            <TD class="tr13 td24">&nbsp;</TD>
                                            <TD class="tr13 td25">&nbsp;</TD>
                                        </TR>
                                        <TR>
                                            <TD class="tr4 td44">GST </TD>
                                            <TD class="tr4 td20">&nbsp;</TD>
                                            <TD colspan=3 class="tr4 td66"><c:out value="${consignorGST}"/></TD>
                                            <TD class="tr4 td14">GST</TD>
                                            <TD class="tr4 td13">&nbsp;</TD>
                                            <TD class="tr4 td24">&nbsp;</TD>
                                            <TD class="tr4 td25"><c:out value="${consigneeGST}"/></TD>
                                        </TR>
                                        <TR>
                                            <TD colspan=2 class="tr13 td29">Description (said to contain)</TD>
                                            <TD class="tr13 td64">&nbsp;</TD>
                                            <TD class="tr13 td65">&nbsp;</TD>
                                            <TD class="tr13 td32">&nbsp;</TD>
                                            <TD class="tr13 td14">Qty</TD>
                                            <TD class="tr13 td14">Weight</TD>
                                            <TD class="tr13 td45">Freight</TD>
                                            <TD class="tr13 td25">TO BE BILLED</TD>
                                        </TR>
                                        <TR>
                                            <TD colspan=5 rowspan=2 class="tr9 td37"> <c:out value="${vehicleType}"/> GOODS (SAID TO CONTENT)</TD>
                                            <TD class="tr1 td18"><c:out value="${totalPackages}"/> Pallets</TD>
                                            <TD class="tr1 td18"><c:out value="${packageWeight}"/> KG</TD>
                                            <TD class="tr13 td45">AOC %</TD>
                                            <TD class="tr13 td25">&nbsp;</TD>
                                        </TR>
                                        <TR>
                                            <TD class="tr25 td18"><P class="p0 ft29">&nbsp;</P></TD>
                                            <TD class="tr25 td18"><P class="p0 ft29">&nbsp;</P></TD>
                                            <TD rowspan=2 class="tr4 td45"><P class="p7 ft15">Cover</P></TD>
                                            <TD class="tr25 td11"><P class="p0 ft29">&nbsp;</P></TD>
                                        </TR>
                                        <TR>
                                            <TD class="tr8 td19"><P class="p0 ft8">&nbsp;</P></TD>
                                            <TD class="tr8 td20"><P class="p0 ft8">&nbsp;</P></TD>
                                            <TD class="tr8 td64"><P class="p0 ft8">&nbsp;</P></TD>
                                            <TD class="tr8 td65"><P class="p0 ft8">&nbsp;</P></TD>
                                            <TD class="tr8 td32"><P class="p0 ft8">&nbsp;</P></TD>
                                            <TD class="tr26 td18"><P class="p0 ft30">&nbsp;</P></TD>
                                            <TD class="tr26 td18"><P class="p0 ft30">&nbsp;</P></TD>
                                            <TD class="tr8 td25"><P class="p0 ft8">&nbsp;</P></TD>
                                        </TR>
                                        <TR>
                                            <TD colspan=2 rowspan=2 class="tr6 td26"><P class="p5 ft15">FULL TRUCK LOAD</P></TD>
                                            <TD class="tr1 td62"><P class="p0 ft0">&nbsp;</P></TD>
                                            <TD class="tr1 td63"><P class="p0 ft0">&nbsp;</P></TD>
                                            <TD class="tr1 td36"><P class="p0 ft0">&nbsp;</P></TD>
                                            <TD class="tr1 td18"><P class="p0 ft0">&nbsp;</P></TD>
                                            <TD class="tr1 td18"><P class="p0 ft0">&nbsp;</P></TD>
                                            <TD class="tr13 td45"><P class="p7 ft15">Floor dry</P></TD>
                                            <TD class="tr13 td25"><P class="p0 ft0">&nbsp;</P></TD>
                                        </TR>
                                        <TR>
                                            <TD class="tr15 td62"><P class="p0 ft14">&nbsp;</P></TD>
                                            <TD class="tr15 td63"><P class="p0 ft14">&nbsp;</P></TD>
                                            <TD class="tr15 td36"><P class="p0 ft14">&nbsp;</P></TD>
                                            <TD class="tr15 td18"><P class="p0 ft14">&nbsp;</P></TD>
                                            <TD class="tr15 td18"><P class="p0 ft14">&nbsp;</P></TD>
                                            <TD rowspan=2 class="tr13 td45"><P class="p7 ft15">Mis Charges</P></TD>
                                            <TD class="tr15 td11"><P class="p0 ft14">&nbsp;</P></TD>
                                        </TR>
                                        <TR>
                                            <TD class="tr27 td19"><P class="p0 ft31">&nbsp;</P></TD>
                                            <TD class="tr27 td20"><P class="p0 ft31">&nbsp;</P></TD>
                                            <TD class="tr27 td64"><P class="p0 ft31">&nbsp;</P></TD>
                                            <TD class="tr27 td65"><P class="p0 ft31">&nbsp;</P></TD>
                                            <TD class="tr27 td32"><P class="p0 ft31">&nbsp;</P></TD>
                                            <TD class="tr27 td14"><P class="p0 ft31">&nbsp;</P></TD>
                                            <TD class="tr27 td14"><P class="p0 ft31">&nbsp;</P></TD>
                                            <TD class="tr27 td25"><P class="p0 ft31">&nbsp;</P></TD>
                                        </TR>
                                        <TR>
                                            <TD class="tr4 td19"></TD>
                                            <TD class="tr4 td20"></TD>
                                            <TD class="tr4 td64"></TD>
                                            <TD class="tr4 td65" align="right">Total</TD>
                                            <TD class="tr4 td32">&nbsp;</TD>
                                            <TD class="tr4 td14"><c:out value="${totalPackages}"/></TD>
                                            <TD class="tr4 td14"><c:out value="${packageWeight}"/></TD>
                                            <TD class="tr4 td24"></TD>
                                            <TD class="tr4 td25"></TD>
                                        </TR>
                                        <TR>
                                            <TD colspan=2 class="tr23 td26"><P class="p5 ft20">Comments</P></TD>
                                            <TD class="tr23 td62"><P class="p0 ft21">&nbsp;</P></TD>
                                            <TD class="tr23 td63"><P class="p0 ft21">&nbsp;</P></TD>
                                            <TD class="tr23 td16"><P class="p0 ft21">&nbsp;</P></TD>
                                            <TD class="tr23 td17"><P class="p0 ft21">&nbsp;</P></TD>
                                            <TD class="tr23 td17"><P class="p0 ft21">&nbsp;</P></TD>
                                            <TD class="tr23 td10"><P class="p0 ft21">&nbsp;</P></TD>
                                            <TD class="tr23 td11"><P class="p0 ft21">&nbsp;</P></TD>
                                        </TR>
                                        <TR>
                                            <TD colspan=9 class="p5 ft22">At Owners' Risk<br/>Insurance is by the Consignor</TD>                            
                                        </TR>
                                        <TR>
                                            <TD colspan=9 class="p5 ft22"><hr color="#000000"></TD>                            
                                        </TR>
                                        <TR>
                                            <TD colspan=9 class="p27 ft22"><b>Notice:</b> The consignment covered by this set of special Lorry Receipt from shall be stored at the destination under the control of the Transport Operator and shall be delivered to or to order of the consignee Bank whose name is mentioned in the Lorry Receipt.
                                                It will under no circumstances be delivered to anyone without the written authority from the consignee Bank or its Order, endorsed on The Consignee copy or on a separate letter of authority.</TD>                            
                                        </TR>
                                        <TR>
                                            <TD colspan=9 class="p5 ft22"><hr color="#000000"></TD>                            
                                        </TR>
                                        <TR>
                                            <TD colspan=9 class="p28 ft22">This is computer generated LR/Consignment copy and does not require signature.</TD>                            
                                        </TR>
                                    </TABLE> 
                                </td></tr></table>                            



                        <%if(i+1==title.length){%>
                        <br>
                        <center>
                            <input type="button" id="printbtn" class="button" value="Back"  onClick="viewTripDetails();" />
                            <!--<input type="button"  class="button" value="Print" onClick="print('printContent');" />-->
                        </center>

                        <% }%>


                    </div>              

                    <% }%>
                </div>
            </form>
            <script type="text/javascript">
                function viewTripDetails() {
                    document.enter.action = '/throttle/viewTripSheets.do?&statusId=10&tripType=1&admin=No';
                    document.enter.submit();
                }
                function print(val)
                {
                    var DocumentContainer = document.getElementById(val);
                    var WindowObject = window.open('', "TrackHistoryData",
                            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                    WindowObject.document.writeln(DocumentContainer.innerHTML);
                    WindowObject.document.close();
                    WindowObject.focus();
                    WindowObject.print();
                    WindowObject.close();
                }
            </script>
            <script type="text/javascript">
                /* <![CDATA[ */
                function get_object(id) {
                    var object = null;
                    if (document.layers) {
                        object = document.layers[id];
                    } else if (document.all) {
                        object = document.all[id];
                    } else if (document.getElementById) {
                        object = document.getElementById(id);
                    }
                    return object;
                }
                //get_object("inputdata").innerHTML = DrawCode39Barcode(get_object("inputdata").innerHTML, 0);
                get_object("inputdata").innerHTML = DrawCode39Barcode('<c:out value="${tripCode}"/>', 0);
                /* ]]> */
            </script>
        </body>
    </html>