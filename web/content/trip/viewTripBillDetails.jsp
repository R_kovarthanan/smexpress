<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="java.text.SimpleDateFormat" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
    <%@ page import="java.util.* "%>
    <%@ page import=" javax. servlet. http. HttpServletRequest" %>
    <%@ page import="java.text.DecimalFormat" %>
    <%@ page import="java.text.NumberFormat" %>

    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>


    <style type="text/css" title="currentStyle">
        @import "/throttle/css/layout-styles.css";
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->
    <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script type="text/javascript">

        function onKeyPressBlockCharacters(e)
        {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            reg = /[a-zA-Z]+$/;

            return !reg.test(keychar);

        }

        function checkInvoiceNo() {
            var invoiceNo = document.trip.invoiceNo.value;
            if (invoiceNo == '' || invoiceNo == '0') {
                //                    var x=document.ElementById("invoiceNo").value=invoiceNo;
                //                     invoiceNo=invoiceNo.replace(/\//g," ");
                alert('please enter invoice No');
                document.trip.invoiceNo.focus();
                return false;
            } else {
                return true;
            }
        }
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

        function setCompanyId(value) {
            var temp = value.split("~");
            document.getElementById("companyIdNew").value = temp[0];
            document.getElementById("compBillingState").value = temp[1];
            $("#companyState").show();
            $("#companyGSTNo").show();
            $('#GstStateTTA').text(temp[2]);
            $('#GstNoTTA').text(temp[3]);

            if (document.getElementById("compBillingState").value != '') {
                calculateGST();
            } else {
                alert("Please configure GST Percentage For the state");
                $("#gstTaxRemb").text("");
                $("#gstTaxRembAmount").text('');
                $("#gstTaxExp").text('');
                $("#gstTaxExpAmount").text('');
                $("#generate").hide();
            }
        }

        function setBillingId(value) {
            var temp = value.split("~");
            document.getElementById("billingId").value = temp[0];
            document.getElementById("billingState").value = temp[1];
            $("#customerState").show();
            $("#custpmerGSTNo").show();
            $('#GstStateCust').text(temp[2]);
            $('#GstNoCust').text(temp[3]);
            document.getElementById("compGSTNo").value = temp[3];
            var billingState = document.getElementById("billingState").value;
            if (billingState != "" && document.getElementById("compBillingState").value != "") {
                $("#generate").show();
                $.ajax({
                    url: "/throttle/getCustomerGSTPercentage.do",
                    dataType: "json",
                    data: {
                        billingState: billingState
                    },
                    success: function(data) {
                        //alert(data);
                        if (data != '') {
                            $('#cgst').val("0");
                            $('#sgst').val("0");
                            $('#igst').val("0");
                            $.each(data, function(i, data) {
                                var temp1 = data.Name;
                                var tmp = temp1.split("-");
                                document.getElementById("cgst").value = tmp[0];
                                document.getElementById("sgst").value = tmp[1];
                                document.getElementById("igst").value = tmp[2];
                                calculateGST();
                            });
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            } else {
                alert("Please configure GST Percentage For the state");
                $("#gstTaxRemb").text("");
                $("#gstTaxRembAmount").text('');
                $("#gstTaxExp").text('');
                $("#gstTaxExpAmount").text('');
                $("#generate").hide();
            }
        }

        function calculateGST() {

            var companyIdNew = document.getElementById("companyIdNew").value;
            var compBillingState = document.getElementById("compBillingState").value;
            var billingId = document.getElementById("billingId").value;
            var billingState = document.getElementById("billingState").value;
            var totalExpInvoiceAmt = parseFloat(document.getElementById("totalExpInvoiceAmt").value);
           
            var grandTotal = parseFloat(document.getElementById("grandTotal").value);

            $("#totalExpenseInvoiceAmtSpan").text(parseFloat(totalExpInvoiceAmt));

            if (companyIdNew != '' && compBillingState != '' && billingId != '' && billingState != '') {

                if (document.getElementById("igst").value == 0 && document.getElementById("sgst").value == 0 && document.getElementById("cgst").value == 0) {
                    alert("Please configure GST Percentage For the state");
                    $("#generate").hide();
                } else {
                    $("#generate").show();
                }

                var igst = document.getElementById("igst").value;
                var cgst = document.getElementById("cgst").value;
                var sgst = document.getElementById("sgst").value;
              

                var taxIGST = 0.00, taxCGST = 0.00, taxSGST = 0.00;

                if (billingState == compBillingState) {   //calculate cgst & sgst
                    // for taxInvoice start
                    taxIGST = parseFloat(0.00);
                    if (grandTotal != null && grandTotal != '0.00' && grandTotal != "") {
                        taxCGST = ((grandTotal) * cgst) / 100;
                        taxSGST = ((grandTotal) * sgst) / 100;
                    } else {
                        taxCGST = parseFloat(0.00);
                        taxSGST = parseFloat(0.00);

                    } // for taxInvoice End
                    var totTax =parseFloat(taxCGST) + parseFloat(taxSGST);
                    $("#gstTaxExp").text("(CGST + SGST : "+cgst+" + "+sgst+" % )");
                    $("#gstTaxExpAmount").text(totTax.toFixed(2));
                    $("#gstTaxExpAmountCGST").val(parseFloat(taxCGST));
                    $("#gstTaxExpAmountSGST").val(parseFloat(taxSGST));
                    $("#gstType").val("CGSTSGST");

                } else {  // calacualte igst
                    taxCGST = parseFloat(0.00);
                    taxSGST = parseFloat(0.00);
                    if (grandTotal != null && grandTotal != '0.00' && grandTotal != "") {
                        taxIGST = ((grandTotal) * igst) / 100;
                        alert("igst"+taxIGST)

                    } else {
                        taxIGST = parseFloat(0.00);
                    } // for taxInvoice End
                    
                    $("#gstTaxExp").text("(IGST : "+igst+" % )");
                    $("#gstTaxExpAmount").text(parseFloat(taxIGST).toFixed(2));
                    $("#gstTaxExpAmountIGST").val(parseFloat(taxIGST));
                    $("#gstType").val("IGST");

                }
                // assign values

                document.getElementById("totalExpIGSTAmt").value = taxIGST;
                document.getElementById("totalExpCGSTAmt").value = taxCGST;
                document.getElementById("totalExpSGSTAmt").value = taxSGST;
                
                document.getElementById("totalExpTaxAmt").value = parseFloat(taxSGST) + parseFloat(taxCGST) + parseFloat(taxIGST);
                document.getElementById("totalExpenseInvoiceAmt").value = parseFloat(taxSGST) + parseFloat(taxCGST) + parseFloat(taxIGST) + grandTotal;
                var finalTotal = parseFloat(taxSGST) + parseFloat(taxCGST) + parseFloat(taxIGST) + grandTotal;
                alert(grandTotal)
                alert(finalTotal);
                $("#totalTaxInvoiceAmtSpan").text(finalTotal.toFixed(2));

            }

        }
    </script>
    <script>
        function invoiceTable() {

            var estimatedRevenue = document.getElementById("estimatedRevenue").value;
            if (estimatedRevenue != null && estimatedRevenue != '') {
                $("#infor").hide();
                $("#genButton").show();
            } else {
                $("#infor").show();
                $("#genButton").hide();
            }
        }
    </script>

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <script type="text/javascript" language="javascript">
        $(document).ready(function() {
            $("#tabs").tabs();
        });

        function submitPage(tripSheetId) {

            var canFlag = document.getElementById("InvoiceFlag").value;
            var errStr = "";
            
            if (document.getElementById("companyId").value == "") {
                errStr = "Please enter Kerry Billing Address.\n";
                alert(errStr);
                document.getElementById("companyId").focus();
                return false;
            } else if (document.getElementById("customerIdNew").value == "") {
                errStr = "Please enter Customer Address.\n";
                alert(errStr);
                document.getElementById("customerIdNew").focus();
                return false;
            }
            else if (document.getElementById("invoiceDate").value == "" && canFlag != 1) {
                errStr = "Please enter Invoice Date.\n";
                alert(errStr);
                document.getElementById("invoiceDate").focus();
                return false;
            }

            document.trip.action = '/throttle/saveTripBill.do?tripSheetId=' + tripSheetId;
            document.trip.submit();

        }

        function editEstimateFeright() {
//            alert("Test");
            var errStr = "";
            if (document.getElementById("estimatedRevenue").value == "") {
                errStr = "Please enter Estimated Revenue.\n";
                alert(errStr);
                document.getElementById("estimatedRevenue").focus();
                return false;
            } else {
                document.trip.action = '/throttle/updateEstimateFeright.do';
                document.trip.submit();
            }
        }


        function Replace()
        {
            var Key = document.getElementById('invoiceNo').value;
            Key = Key.replace("/", " ");
            document.getElementById('invoiceNo').value = Key;
        }


        function fuelExpense(val) {
            var fuelEscalation = val;
            //var totalKm = document.getElementById('totalKm').value;
            var totalKm = document.getElementById('totalTripKm').value;
            var fuelEstimation = (fuelEscalation * totalKm);
            $('#fuelEstCharge').val(fuelEstimation);
        }
    </script>




</head>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.BillingDetails" text="Billing Details"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Billing" text="Billing"/></a></li>
            <li class=""><spring:message code="hrms.label.BillingDetails" text="Billing Details"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body onload="calculateGST();
                    invoiceTable()" >

                <form name="trip" method="post">                

                    <input type="text" name="igst"  id="igst" value='<c:out value="${igst}"/>' />
                    <input type="hidden" name="cgst"  id="cgst" value='<c:out value="${cgst}"/>' />
                    <input type="hidden" name="sgst"  id="sgst" value='<c:out value="${sgst}"/>' />

                    <%int sno = 0;%>

                    <table class="table table-info mb30 table-hover" id="bg" style="width:100%">
                        <thead>
                            <tr>
                                <th colspan="2" height="30" >Billing Details</th>
                            </tr>
                        </thead>

                        <c:set var="totalRevenue" value="0" />
                        <c:set var="totalExpToBeBilled" value="0" />
                    </table>


                    <table class="table table-info mb30 table-hover" id="bg"  style="width:100%">
                        <thead>
                            <tr>
                                <th  >Sno</th>
                                <th  >Trip Code</th>
                                <th  >C Note</th>
                                <th  >Customer</th>
                                <th  >Type</th>
                                <th  >Start</th>
                                <th  >End</th>                                
                                <th  >Estimated Revenue</th>                                
                                <th  >Expenses to be Billed</th>
                            </tr>
                        </thead>

                        <c:if test = "${tripsToBeBilledDetails != null}" >
                            <c:set var="totalTripKm" value="0" />
                            <c:set var="prevTripId" value="0" />
                            <c:forEach items="${tripsToBeBilledDetails}" var="trip">
                                <%sno++;%>
                                <tr>
                                <input type="hidden" id="totalKm" name="totalKm" value="<c:out value="${trip.totalKm}"/>">
                                <td  ><%=sno%></td>
                                <td  ><c:out value="${trip.tripCode}"/></td>
                                <td  ><c:out value="${trip.cNotes}"/></td>
                                <td  ><c:out value="${trip.customerName}"/></td>
                                <td  ><c:out value="${trip.customerType}"/> / <c:out value="${trip.billingType}"/></td>
                                <td  ><c:out value="${trip.startDate}"/> / <c:out value="${trip.startTime}"/></td>
                                <td  ><c:out value="${trip.endDate}"/> / <c:out value="${trip.endTime}"/></td>                                
                                <td ><input type="hidden" name="estimatedRevenue" id="estimatedRevenue" value='<c:out value="${trip.estimatedRevenue}"/>' />

                                    <fmt:formatNumber pattern="##0.00" value="${trip.estimatedRevenue}"/>
                                </td>

                                <td ><c:out value="${trip.expenseToBeBilledToCustomer}"/></td>
                                <c:set var="totalRevenue" value="${trip.estimatedRevenue + totalRevenue}" />
                                <c:set var="totalExpToBeBilled" value="${totalExpToBeBilled + trip.expenseToBeBilledToCustomer}" />

                                <c:if test = "${trip.tripId != prevTripId}" >
                                    <c:set var="totalTripKm" value="${totalTripKm + trip.totalKm}" />
                                </c:if>


                                <c:set var="prevTripId" value='<c:out value="${trip.tripId}"/>' />
                                <input type="hidden" name="tripId" value='<c:out value="${trip.tripId}"/>' />
                                <input type="hidden" name="consignmentOrderId" value='<c:out value="${trip.consignmentId}"/>' />
                                <input type="hidden" name="consignmentId" id="consignmentId" value='<c:out value="${trip.consignmentId}"/>' />
                                <input type="hidden" name="customerId" value='<c:out value="${trip.customerId}"/>' />
                                <input type="hidden" name="totalDays" value='<c:out value="${trip.totalDays}"/>' />
                                <input type="hidden" name="startDate" value='<c:out value="${trip.startDate}"/>' />
                                <input type="hidden" name="endDate" value='<c:out value="${trip.endDate}"/>' />
                                <input type="hidden" name="startTime" value='<c:out value="${trip.tripStartTime}"/>' />
                                <input type="hidden" name="tripTransitHours" value='<c:out value="${trip.tripTransitHours}"/>' />
                                <input type="hidden" name="reeferRequired" value='<c:out value="${trip.reeferRequired}"/>' />
                                <c:set var="tripTransitHours" value="${trip.tripTransitHours}"></c:set>
                                <c:set var="reeferRequired" value="${trip.reeferRequired}"></c:set>
                                <c:set var="totalDays" value="${trip.totalDays}"></c:set>
                                <c:set var="cancelStatus" value="${trip.statusId}"></c:set>
                                <input type="hidden" name="cancelInvoiceFlag"  id="cancelInvoiceFlag" value='<c:out value="${cancelStatus}"/>' />
                                <input type="hidden" name="InvoiceFlag"  id="InvoiceFlag" value='<c:out value="${InvoiceFlag}"/>' />
                                <input type="hidden" name="billingState"  id="billingState<%=sno%>" value='<c:out value="${trip.billingState}"/>' />

                            </c:forEach>
                        </c:if>
                    </table>

                    <center>
                        <table width="1000" cellpadding="0" cellspacing="0"  border="0"  style="margin-top:0px;display: none">
                            <tr>
                                <td  align="center"><input type="button" class="btn btn-success" id="estimateFeright" value="Update Feright" name="estimateFeright" onclick="editEstimateFeright();" />
                            </tr>
                        </table>
                    </center>
                    <br>

                    <% int cntr = 1;%>
                    <table>
                        <tr>
                            <td colspan="8">Other Expenses to be Billed:</td>
                        </tr>
                    </table>

                    <table class="table table-info mb30 table-hover" id="bg" style="width:100%">
                        <thead>
                            <tr>
                                <th  >Sno</th>
                                <th  >Trip Code</th>
                                <th  >Expense Description</th>
                                <th  >Expense Type</th>
                                <th  >Remarks</th>
                                <th  >Expense Value</th>
                            </tr>
                        </thead>
                        <c:set var="rembType" value="0" />
                        <c:set var="billToCustomerType" value="0" />
                        <c:set var="billWithFreight" value="1" />
                        <c:set var="totalExpInvoiceAmt" value="0" />
                        <c:set var="totalFreightInvoiceAmt" value="0" />
                        <c:set var="totalReimbursInvoiceAmt" value="0" />
                        <c:set var="totalOtherExpesneForFreightAmt" value="0" />
                        <c:if test = "${tripsOtherExpenseDetails != null}" >

                            <c:forEach items="${tripsOtherExpenseDetails}" var="trip">
                                <tr>
                                    <td  ><%=cntr%></td>
                                    <td  >
                                        <c:if test="${cancelStatus == '4'}">
                                            0
                                        </c:if>
                                        <c:if test="${cancelStatus != '4'}">
                                            <c:out value="${trip.tripCode}"/>
                                        </c:if>
                                    </td>    

                                    <td  ><c:out value="${trip.expenseName}"/></td>
                                    <c:if test = "${trip.expenseType == '1'}" >
                                        <td  >Bill To Customer </td>
                                    </c:if>

                                    <c:if test = "${trip.expenseType == '2'}" >
                                        <td  >Do Not Bill To Customer </td>
                                    </c:if>

                                    <td  ><c:out value="${trip.expenseRemarks}"/></td>
                                    <td   ><c:out value="${trip.expenseValue}"/></td>

                                    <c:if test="${trip.expenseType == '1'}">
                                        <c:set var="billToCustomerType" value="1" />
                                        <c:set var="totalExpInvoiceAmt" value="${totalExpInvoiceAmt + trip.expenseValue}" />
                                    </c:if>

                                    <%cntr++;%>
                                </c:forEach>
                            </c:if>
                            <% if (cntr == 1) {%>
                        <tr>
                            <td colspan="8">no expenses to be billed</td>
                        </tr>
                        <% }%>
                    </table>



                    <table class="table table-info mb30 table-hover" id="bg" style="width:100%">
                        <thead>
                            <tr>
                                <th colspan="2" align="left">Summary:</th>
                            </tr>
                        </thead>

                        <tr>
                            <td align="left">Freight Charges (trucking charges)</td>
                            <td align="right">
                                <fmt:formatNumber pattern="##0.00" value="${totalRevenue}"/>
                            </td>
                        </tr>


                        <tr>
                            <td align="left">Total Expense to be billed&emsp;
                                &emsp;</td>
                            <td align="right"><span id="totalExpenseInvoiceAmtSpan"></span></td>
                        </tr>

                        <tr>
                            <td align="left"> Grant Total&emsp;</td>
                            <td align="right"><fmt:formatNumber pattern="##0.00" value="${totalRevenue + totalExpToBeBilled}"/></td>
                        </tr>

                        <tr>
                            <td align="left">Total Tax to be billed&emsp;<span id="gstTaxExp"></span></td>
                            <td align="right">&emsp; <span id="gstTaxExpAmount"></span>
                                <input type="hidden" name="gstTaxExpAmountIGST" id="gstTaxExpAmountIGST" value="0">
                                <input type="hidden" name="gstTaxExpAmountCGST" id="gstTaxExpAmountCGST" value="0">
                                <input type="hidden" name="gstTaxExpAmountSGST" id="gstTaxExpAmountSGST" value="0">
                                <input type="hidden" name="totalExpInvoiceAmt" id="totalExpInvoiceAmt" value="<fmt:formatNumber pattern="##0.00" value="${totalExpInvoiceAmt}"/>">
                                <input type="hidden" name="totalExpenseInvoiceAmt" id="totalExpenseInvoiceAmt" value="">
                                <input type="hidden" name="totalExpIGSTAmt" id="totalExpIGSTAmt" value="">
                                <input type="hidden" name="totalExpCGSTAmt" id="totalExpCGSTAmt" value="">
                                <input type="hidden" name="totalExpSGSTAmt" id="totalExpSGSTAmt" value="">
                                <input type="hidden" name="totalExpTaxAmt" id="totalExpTaxAmt" value="">
                            </td>
                        </tr>

                        <tr >
                            <td align="left"> Over All Total&emsp;</td>
                            <td align="right">&emsp; <span id="totalTaxInvoiceAmtSpan"></span></td>
                        </tr>
                    </table>

                    <input type="hidden" name="totalRevenue" value='<c:out value="${totalRevenue}"/>' />
                    <input type="hidden" name="totalExpToBeBilled" value='<c:out value="${totalExpToBeBilled}"/>' />
                    <input type="hidden" id="grandTotal" name="grandTotal" value='<c:out value="${totalRevenue + totalExpToBeBilled}"/>' />

                    <input type="hidden" name="billToCustomerType" id="billToCustomerType" value='<c:out value="${billToCustomerType}"/>' />
                    <input type="hidden" name="generateBill" value='1' />
                    <input type="hidden" name="noOfTrips" value='<%=sno%>' />
                    <input type="hidden" id="gstType" name="gstType" value='' />

                    <br/>

                    <table  class="table table-info mb30 table-hover" id="bg"  style="margin-top:0px;width:100%">
                        <tr>
                        <thead><th colspan="4">
                            Billing Details
                        </th></thead>
                        </tr>

                        <tr>
                            <td> Kerry Billing Location: &nbsp;</td>
                            <td><select name="companyId" id="companyId" class="form-control" onchange="setCompanyId(this.value);" style="width:250px;height:40px" >
                                    <c:if test="${companyList != null}">
                                        <option value="" selected>--Select--</option>
                                        <c:forEach items="${companyList}" var="companyList">
                                            <option value='<c:out value="${companyList.companyId}"/>~<c:out value="${companyList.billingState}"/>~<c:out value="${companyList.state}"/>~<c:out value="${companyList.gstNo}"/>'><c:out value="${companyList.companyName}"/></option>
                                        </c:forEach>
                                    </c:if></select>
                                <input type="hidden" name="companyIdNew" id="companyIdNew" value='' />
                                <input type="hidden" name="compBillingState" id="compBillingState" value='' />
                            </td>
                            <td>  Customer Billing Location &nbsp; </td>
                            <td>  <select name="customerIdNew" id="customerIdNew" class="form-control" onchange="setBillingId(this.value);" style="width:250px;height:40px" > <c:if test="${customerList != null}">
                                        <option value="" selected>--Select--</option>
                                        <c:forEach items="${customerList}" var="customerList">
                                            <option value='<c:out value="${customerList.billingId}"/>~<c:out value="${customerList.billingState}"/>~<c:out value="${customerList.state}"/>~<c:out value="${customerList.gstNo}"/>'><c:out value="${customerList.billingAddress}"/></option>
                                        </c:forEach>
                                    </c:if></select>
                                <input type="hidden" name="billingId" id="billingId" value='' />
                                <input type="hidden" name="billingState" id="billingState" value='' />
                            </td>
                        </tr>
                        <tr>
                            <td id="companyState" style="display:none">Company State</td>
                            <td><label  id="GstStateTTA" ></label></td>
                            <td id="customerState" style="display:none">Customer  State</td>
                            <td><label id="GstStateCust" ></label></td>
                        </tr>

                        <tr>
                            <td id="companyGSTNo" style="display:none">Company GST No</td>
                            <td><label id="GstNoTTA" ></label></td>
                            <td id="custpmerGSTNo" style="display:none">Customer  GST No</td>
                            <td><label id="GstNoCust" ></label></td>
                        <input type="hidden" name="compGSTNo" id="compGSTNo" value=""/>
                        </tr>

                    </table>

                    <table  style="display:none">
                        <tr>
                        <thead>
                        <th colspan="4"> Freight Invoice Details</th>
                        </thead>
                        </tr>
                        <tr>
                            <td>Freight Invoice Type&nbsp; </td>
                            <td>  <select name="invoiceType" id="invoiceType" class="form-control" style="width:250px;height:40px" >
                                    <option value="CREDIT" selected>CREDIT</option>                                    
                                </select>
                            </td>
                            <td> </td>
                            <td> 
                                <select name="gstLevy" id="gstLevy" class="form-control" style="display:none" >
                                    <option value="REGULAR" selected>REGULAR</option>
                                </select>
                            </td>
                        </tr>

                        <%
                             Date today = new Date();
                             SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                             String startDate = sdf.format(today);                             
                        %>

                        <tr>
                            <td>Freight Place Of Supply  &nbsp; </td>
                            <td>
                                <select name="placeofSupply" id="placeofSupply" class="form-control" style="width:250px;height:40px" >
                                    <c:if test="${financeStateList != null}">
                                        <c:forEach items="${financeStateList}" var="financeStateList">
                                            <option value='<c:out value="${financeStateList.stateName}"/>' selected><c:out value="${financeStateList.stateName}"/></option>
                                        </c:forEach>
                                    </c:if>

                                </select>
                            </td>
                            <td>Invoice Date</td>
                            <td><input type="text" id="invoiceDate" name="invoiceDate" value="<%=startDate%>" class="form-control datepicker" style="width:250px;height:40px" ></td>
                        </tr>

                    </table>

                    <br/>
                    <center>
                        <table width="1000" cellpadding="0" cellspacing="0"  border="0"  style="margin-top:0px;">
                            <c:if test = "${tripsToBeBilledDetails != null}" >                           
                                <tr id="genButton">
                                    <td  align="center"><input type="button" class="btn btn-success" id="generate" value="Generate bill" name="generate" onclick="submitPage(<c:out value="${tripSheetId}"/>);" />
                                </tr>
                            </c:if>
                            <c:if test = "${tripsToBeBilledDetails == null}" >
                                <tr id="infor">
                                    <td  align="center" ><font color="red">Please Update the fuel Price for this Trip Month</font></td>
                                </tr> 
                            </c:if>
                        </table>
                    </center>

                    <script>
                        $(".nexttab").click(function() {
                            var selected = $("#tabs").tabs("option", "selected");
                            $("#tabs").tabs("option", "selected", selected + 1);
                        });
                    </script>

        </div>
        </form>
        </body>
    </div>
</div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>