<%@ include file="../common/NewDesign/header.jsp" %>
	<%@ include file="../common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });

    function submitTicket(){
        var attachment = document.getElementsByName("attachment");
        if(attachment.length == 0){
        document.ticket.enctype="";
        document.ticket.action=" /throttle/saveTicket.do";
        document.ticket.submit();
        }else{
        document.ticket.enctype="multipart/form-data";
        document.ticket.action=" /throttle/saveTicketFile.do";
        document.ticket.submit();
        }
    }
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>


        <form name="ticket"  method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            
            <%@ include file="/content/common/message.jsp" %>
            
            
            
                 <table class="table table-info mb30 table-hover" style="width:95%" >
		    <thead>
		<tr>
		    <th colspan="4" height="30" >Create Ticket</th>
		</tr>
               </thead>

                <tr>
                    <td>Ticket Priority</td>
                    <td>
                        <select name="priority" class="form-control" style="width:240px;height:40px" id="priority">
                                <c:if test="${ticketingStatusList != null}">
                            <c:forEach items="${ticketingStatusList}" var="sts">
                                <c:if test="${sts.type == 'Priority'}">
                                <option value="<c:out value="${sts.statusId}"/>"><c:out value="${sts.statusName}"/></option>
                                </c:if>
                            </c:forEach>
                                </c:if>
                        </select>
                    </td>
                    <td>Ticket Status</td>
                    <td>
                        <select class="form-control" style="width:240px;height:40px" name="status" >
                            <c:if test="${ticketingStatusList != null}">
                            <c:forEach items="${ticketingStatusList}" var="sts">
                                <c:if test="${sts.type == 'status' && sts.statusId == '5'}">
                                <option value="<c:out value="${sts.statusId}"/>"><c:out value="${sts.statusName}"/></option>
                                </c:if>
                            </c:forEach>
                            </c:if>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Ticket Type</td>
                    <td>
                        <select class="form-control" style="width:240px;height:40px" name="type" id="type">
                            <c:if test="${ticketingStatusList != null}">
                            <c:forEach items="${ticketingStatusList}" var="sts">
                                <c:if test="${sts.type == 'type'}">
                                <option value="<c:out value="${sts.statusId}"/>"><c:out value="${sts.statusName}"/></option>
                                </c:if>
                            </c:forEach>
                            </c:if>
                        </select>
                    </td>
                    <td>From</td>
                    <td><input type="text" name="from" value="<%=session.getAttribute("emailId")%>" readonly class="form-control" style="width:240px;height:40px"></td>
                </tr>
                <tr>
                    <td>To</td>
                    <td colspan="3"><textarea class="form-control" name="to" cols="90" rows="1" readonly>naved.ahmad@brattlefoods.com,nileshkumar@entitlesolutions.com,Throttle@entitlesolutions.com</textarea></td>
<!--                    <td colspan="3"><textarea name="to" cols="90" rows="1" readonly>nileshkumar@entitlesolutions.com,Throttle@entitlesolutions.com</textarea></td>-->
                </tr>
                <tr>
                    <td>CC</td>
                    <td colspan="3"><textarea class="form-control" name="cc" cols="90" rows="1" readonly>subhrakanta.mishra@brattlefoods.com,srini@entitlesolutions.com</textarea></td>
<!--                    <td colspan="3"><textarea name="cc" cols="90" rows="1" readonly></textarea></td>-->
                </tr>
                <tr>
                    <td>Subject/Title</td>
                    <td colspan="3"><textarea class="form-control" name="title" cols="90" rows="1" ></textarea></td>
                </tr>
                <tr>
                    <td>Message</td>
                    <td colspan="3"><textarea class="form-control" name="message" cols="90" rows="10"></textarea> </td>
                </tr>
            </table>

            <table class="border" width="815" border="0" cellpadding="0" align="center"  id="suppExpenseTBL" >
                            <tr>
                                <td align="center" colspan="4">
                                <input type="button" class="btn btn-success" name="add" value="File Attachment" onclick="addRow()">
                                <input type="button" class="btn btn-success" name="Save" value="Create Ticket" onclick="submitTicket()">
                                </td>
                            </tr>
            </table>

            <script>


                        var rowCount = 1;
                        var sno = 0;
                        var rowCount1 = 1;
                        var sno1 = 0;
                        var httpRequest;
                        var httpReq;
                        var styl = "";
                        function addRow() {
                            if (parseInt(rowCount1) % 2 == 0)
                            {
                                styl = "text2";
                            } else {
                                styl = "text1";
                            }

                            sno1++;
                            var tab = document.getElementById("suppExpenseTBL");
                            //find current no of rows
                            var rowCountNew = document.getElementById('suppExpenseTBL').rows.length;
                            rowCountNew--;
                            var newrow = tab.insertRow(rowCountNew);
                            cell = newrow.insertCell(0);
                            cell0 = "<td class='text1' height='25'><input type='file' name='attachment'/></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell0;
                            cell = newrow.insertCell(1);
                            cell0 = "<td class='text1' height='25'><textarea name='fileRemarks' cols='40' rows='2'></textarea></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell0;
                            cell = newrow.insertCell(2);
                            var status = 0;
                            cell0 = "<td class='text1' height='25'><input type='button' class='button' name='delete' value='Remove' onclick='removeRow("+sno+","+status+")'/></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell0;
                            rowCount1++;
                            row++;
                        }

                        function removeRow(sno,status){
                            document.getElementById('suppExpenseTBL').deleteRow(sno);
                            rowCount1--;
                            row--;
                        }

            </script>
        </form>
    </body>
<%@ include file="../common/NewDesign/settings.jsp" %>