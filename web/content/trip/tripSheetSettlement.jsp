<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true
                });
            });
        </script>

        <script>
            function driverSettlement(val) {
                if (document.getElementById('settlementRemarks').value == '') {
                    alert("please enter the remarks");
                    document.getElementById('settlementRemarks').focus();
                } else if (val == 'save') {
                    document.settlement.action = "/throttle/saveDriverSettlement.do?print=no";
                    document.settlement.submit();
                } else if (val == 'saveprint') {
                    document.settlement.action = "/throttle/saveDriverSettlement.do?print=yes";
                    document.settlement.submit();
                }
            }
        </script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.TripSettlement" text="TripSettlement"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.TripSettlement" text=" TripSettlement"/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">    <body onload="calcExpenses();">
        <form name="settlement" method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <br>
             <table width="300" cellpadding="0" cellspacing="0" align="right" border="0" id="report" style="margin-top:0px;">

                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:300;">

                            <div id="first">
                                <c:if test = "${tripDetails != null}" >
                                    <c:forEach items="${tripDetails}" var="trip">
                                <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Expected Revenue:</b></font></td>
                                       <c:out value="${trip.orderRevenue}" /></td>
                                        <!--<td>  <td> <c:out value="${trip.editFreightAmount}" /></td>-->

                                     </tr>
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Actual Expense:</b></font></td>
                                        <td> <c:out value="${trip.orderExpense}" /></td>
                                        <input type="hidden" name="estimatedAdvancePerDay" value='<c:out value="${trip.estimatedAdvancePerDay}" />'>
                                        <input type="hidden" name="estimatedTransitDays" value='<c:out value="${trip.estimatedTransitDays}" />'>
                                        <input type="hidden" name="estimatedTransitHours" value='<c:out value="${trip.estimatedTransitHours}" />'>

                                     </tr>
                                    <c:set var="profitMargin" value="" />
                                    <c:set var="orderRevenue" value="${trip.orderRevenue}" />
                                     <c:set var="editFreightAmount" value="${trip.editFreightAmount}" />
                                     <c:set var="orderExpense" value="${trip.orderExpense}" />
                                     <c:set var="profitMargin" value="${orderRevenue - orderExpense}" />
                                     <%
                                     //double profitVal = (Double)pageContext.getAttribute("profitMargin");
                                     String profitMarginStr = "" + (Double)pageContext.getAttribute("profitMargin");
                                     String revenueStr = "" + (String)pageContext.getAttribute("orderRevenue");
//                                     String revenueStr = "" + (String)pageContext.getAttribute("editFreightAmount");
                                     float profitPercentage = 0.00F;
                                     if(!"".equals(revenueStr) && !"".equals(profitMarginStr)){
                                         profitPercentage = Float.parseFloat(profitMarginStr)*100/Float.parseFloat(revenueStr);
                                     }


                                     %>
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Profit Margin:</b></font></td>
                                        <td>  <%=new DecimalFormat("#0.00").format(profitPercentage)%>(%)
                                            <input type="hidden" name="profitMargin" value='<c:out value="${profitMargin}" />'>
                                        <td>

                                        <td>
                                     </tr>
                                </table>
                                </c:forEach>
                             </c:if>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <table class="table table-info mb30 table-hover">
                <thead><tr>
                    <th  >Trip Settlement</th>
                    <th  >Customer:&nbsp;<c:out value="${customerName}"/></th>
                    <th  >Route: &nbsp;<c:out value="${routeName}"/></th>
                    <th  >Trip Schedule: &nbsp;<c:out value="${tripStartDate}"/>&nbsp; :&nbsp; <c:out value="${tripStartTime}"/></th>
                    <th  >Trip Status: Trip Settlement</th>
                    </tr></thead>
            </table>
            <div id="tabs" >
                <ul>
                    <li><a href="#settlement"><span>Settlement Details</span></a></li>
<!--                    <li><a href="#tripSettlement"><span>Trip Settlement</span></a></li>-->
                    <li><a href="#advance"><span>Advance</span></a></li>
<!--                    <li><a href="#fuel"><span>Fuel</span></a></li>-->
                    <li><a href="#expense"><span>Expense</span></a></li>

                    <li><a href="#tripDetail"><span>Trip Details</span></a></li>
                    <li><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
<!--                    <li><a href="#preStart"><span>Trip Pre Start Details </span></a></li>-->
                    <li><a href="#startDetail"><span>Trip Start Details</span></a></li>
                    <li><a href="#endDetail"><span>Edit Trip End Details</span></a></li>
                    <li><a href="#statusDetail"><span>Status History</span></a></li>
                    <li><a href="#bpclDetail"><span>BPCL Transaction History</span></a></li>
                </ul>

                <div id="settlement">

                    <table  class="table table-info mb30 table-hover" id="bg">
                        <thead><tr>
                            <th  colspan="4" >Trip Settlement Details
                                <input type="hidden" name="tripSheetId" id="tripSheetId" value="<c:out value="${tripSheetId}"/>"  />
                                <input type="hidden" name="tollAmount" id="tollAmount" value="<c:out value="${tollAmount}"/>"  />
                                <input type="hidden" name="battaAmount" id="battaAmount" value="<c:out value="${driverBatta}"/>"  />
                                <input type="hidden" name="incentiveAmount" id="incentiveAmount" value="<c:out value="${driverIncentive}"/>"  />
                                <input type="hidden" name="fuelPrice" id="fuelPrice" value="<c:out value="${fuelPrice}"/>"  />
                                <input type="hidden" name="milleage" id="milleage" value="<c:out value="${milleage}"/>"  />
                                <input type="hidden" name="reeferConsumption" id="reeferConsumption" value="<c:out value="${reeferConsumption}"/>"  />
                                <input type="hidden" name="vehicleTypeId" id="vehicleTypeId" value="<c:out value="${vehicleTypeId}"/>"  />
                                <input type="hidden" name="gpsKm" id="gpsKm" value="<c:out value="${gpsKm}"/>"  />
                                <input type="hidden" name="totalRunKm" id="totalRunKm" value="<c:out value="${runKm}"/>"  />
                                <input type="hidden" name="totalDays" id="totalDays" value="<c:out value="${totaldays}"/>"  />
                                <input type="hidden" name="totalRefeerHours" id="totalRefeerHours" value="<c:out value="${runHm}"/>"  />
                                <input type="hidden" name="estimatedExpense" id="estimatedExpense" value="<c:out value="${estimatedExpense}"/>"  />
                                <input type="hidden" name="miscValue" id="miscValue" value="<c:out value="${miscValue}"/>"  />
                                <input type="hidden" name="driverCount" id="driverCount" value="<c:out value="${driverCount}"/>"  />
                                <input type="hidden" name="extraExpenseValue" id="extraExpenseValue" value="<c:out value="${extraExpenseValue}"/>"  />
                                <input type="hidden" name="extraExpenseValue" id="extraExpenseValue" value="<c:out value="${tripAdvance}"/>"  />
                                <input type="hidden" name="bpclAmount" id="bpclAmount" value="<c:out value="${bpclAmount}"/>"  />
                                <input type="hidden" name="vehicleId" id="vehicleId" value="<c:out value="${vehicleId}"/>"  />
                            </th>
                            </tr></thead>
                        <script>
                            function calcExpenses() {
                                var runKM = document.getElementById("totalRunKm").value;
                                var tollAmount = document.getElementById("tollAmount").value;
                                var driverBatta = document.getElementById("battaAmount").value;
                                var driverIncentive = document.getElementById("incentiveAmount").value;
                                var transitDays = document.getElementById("totalDays").value;
                                var mileage = document.getElementById("milleage").value;
                                var fuelPrice = document.getElementById("fuelPrice").value;
                                var reeferMilleage = document.getElementById("reeferConsumption").value;
                                var driverCount = document.getElementById("driverCount").value;
                                var extraExpenseValue = document.getElementById("extraExpenseValue").value;
                                
                                //Calc Bhatta
                                var bhatta = (transitDays * 100)*driverCount;
                                $("#bhatta").text(bhatta);
                                $("#totalTripBhatta").val(bhatta);
                                //Calc Bhatta
                                
                                
                                //Calculate Misc
                                var reeferHour = document.getElementById("totalRefeerHours").value;
                                var miscValue = document.getElementById("miscValue").value;
                                var misc  = miscValue * runKM ;
                                var totalMisc = ((parseFloat(misc)+parseFloat(extraExpenseValue)+parseFloat(bhatta))/ 100) * 5;
                                totalMisc = totalMisc.toFixed(2);
                                $("#totalMisc").text(totalMisc);
                                $("#totalTripMisc").val(totalMisc);
                                //Calculate Misc
                                
                                
                                
                                //Calc Diesel Consume
                                var reeferConsume = (reeferHour * reeferMilleage).toFixed(2);
                                var vehicleConsume = (runKM / mileage).toFixed(2);
                                var totalConsume = parseFloat(vehicleConsume) + parseFloat(reeferConsume);

                                var ratePerKm = parseFloat(document.getElementById("estimatedExpense").value) /
                                                parseFloat(document.getElementById("totalRunKm").value);
                                ratePerKm = ratePerKm.toFixed(2);

                                $("#ratePerKm").text(ratePerKm);
                                $("#vehicleDiesel").text(vehicleConsume);
                                $("#reeferDiesel").text(reeferConsume);
                                $("#totalConsume").text(totalConsume.toFixed(2));
                                $("#tripDieselConsume").val(totalConsume.toFixed(2));
                                $("#vehicleDieselConsume").val(vehicleConsume);
                                $("#reeferDieselConsume").val(reeferConsume);
                                //Calc Diesel Consume
                                
                                //Calc RCm Allocation
                                //var rcmAllocation = totalConsume  * fuelPrice;
                                //alert(document.getElementById("estimatedExpense").value)
                                var rcmAllocation = parseFloat(document.getElementById("estimatedExpense").value);
                                //alert(rcmAllocation);
                                $("#rcmAllocation").text(rcmAllocation.toFixed(2));
                                //Calc RCm Allocation
                                
                                //Calc Total Expense
                                var extraExpense = $("#extraExpense").text();


                                var nettExtraExpense = (parseFloat(bhatta) + parseFloat(totalMisc) + parseFloat(extraExpense)).toFixed(2);
                                //alert(nettExtraExpense);
                                $("#extraExpense").text(nettExtraExpense);
                                $("#tripExtraExpense").val(nettExtraExpense)
                                extraExpense = nettExtraExpense;
                                //var totalExpense = parseFloat(rcmAllocation) + parseFloat(extraExpense)
                                var totalExpense = parseFloat(rcmAllocation);
                                $("#totalExpense").text(totalExpense.toFixed(2));
                                $("#totalTripExpense").val(totalExpense.toFixed(2));
                                //Calc Total Expense
                                
                                //Calc Balance
                                var bpclAllocation = $("#bpclAllocation").text();
                                var balance = (parseFloat(totalExpense)-parseFloat(bpclAllocation)).toFixed(2);
                                $("#balance").text(balance);
                                $("#tripBalance").val(balance);
                                //Calc Balance
                                
                                //Calc End Balance
                                var startBalance = $("#startBalance").text();
                                var endBalance = parseFloat(balance);
                                $("#endBalance").text(endBalance.toFixed(2));
                                $("#tripEndBalance").val(endBalance.toFixed(2));
                                $("#tripStartingBalance").val(endBalance.toFixed(2));
                                //Calc End Balance
                                
                                
                            }
                        </script>
                        <tr>
                            <td class="text2" colspan="2">&nbsp;</td>
                            <td class="text2">Trip Start Date</td>
                            <td class="text2"><label><c:out value="${startDate}"/></label></td>
                        </tr>
                        <tr>
                            <td class="text1">C Note No</td>
                            <td class="text1"><label id="cNoteNo"><c:out value="${cNotes}"/></label></td>
                            <td class="text1">Trip Start Time in IST</td>
                            <td class="text1"><label><c:out value="${startTime}"/></label></td>
                        </tr>
                        <tr>
                            <td class="text2" colspan="2">&nbsp;</td>
                            <td class="text2">Trip End Date</td>
                            <td class="text2"><label><c:out value="${tripEndDate}"/></label></td>
                        </tr>
                        <tr>
                            <td class="text1" colspan="2">&nbsp;</td>
                            <td class="text1">Trip End Time in IST</td>
                            <td class="text1"><label><c:out value="${tripEndTime}"/></label></td>
                        </tr>

                        <tr>
                            <td class="text2">Truck Number</td>
                            <td class="text2"><label><c:out value="${vehicleNo}"/></label></td>
                            <td class="text2">Trip No</td>
                            <td class="text2"><label><c:out value="${tripCode}"/></label></td>

                        </tr>
                        <tr>
                            <td class="text1">Driver 1</td>
                            <td class="text1"><label><c:out value="${driverName1}"/></label></td>
                            <td class="text1">Rate/Km For Trip</td>
                            <td class="text1"><label id="ratePerKm"></label></td>
                        </tr>
                        <tr>
                            <td class="text2">Driver 2</td>
                            <td class="text2"><label><c:out value="${driverName2}"/></label></td>
                            <td class="text2">Diesel Reference Rate</td>
                            <td class="text2"><label><c:out value="${fuelPrice}"/></label></td>
                        </tr>
                        <tr>
                            <td class="text1">Stop No 1</td>
                            <td class="text1">Pickup</td>
                            <td class="text1"><label><c:out value="${origin}"/></label></td>
                            <td class="text1">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="text2">Stop No 2</td>
                            <td class="text2">Drop</td>
                            <td class="text2"><label><c:out value="${destination}"/></label></td>
                            <td class="text2">&nbsp;</td>
                        </tr>
                        
                        <tr>
                            <td class="text1" colspan="2">&nbsp;</td>
                                <input type="hidden" name="tripStartingBalance" id="tripStartingBalance" value="0" />
                            <td class="text1" colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="text2">Odometer Starting</td>
                            <td class="text2"><label><c:out value="${startKm}"/></label></td>
                            <td class="text2">Reefer Hours Starting</td>
                            <td class="text2"><label><c:out value="${startHm}"/></label></td>
                        </tr>
                        <tr>
                            <td class="text1">Odometer Ending</td>
                            <td class="text1"><label><c:out value="${endKm}"/></label></td>
                            <td class="text1">Reefer Hours Ending</td>
                            <td class="text1"><label><c:out value="${endHm}"/></label></td>
                        </tr>
                        <tr>
                            <td class="text2">Odometer Km</td>
                            <td class="text2"><label><c:out value="${runKm}"/></label></td>
                            <td class="text2">Reefer Hours</td>
                            <td class="text2"><label><c:out value="${runHm}"/></label></td>
                        </tr>
                        <tr>
                            <td class="text1">GPS  Km</td>
                            <td class="text1"><label><c:out value="${gpsKm}"/></label></td>
                            <td class="text1">Diesel Used</td>
                            <td class="text1"><label id="totalConsume"></label>
                             <input type="hidden" name="tripDieselConsume" id="tripDieselConsume" value="" /></td>
                        </tr>
                        <tr>
                            <td class="text2">Km Run</td>
                            <td class="text2"><label>
                                    <c:if test="${gpsKm != 0}">
                                    <c:out value="${gpsKm}"/>
                                    </c:if>
                                    <c:if test="${gpsKm == 0}">
                                    <c:out value="${runKm}"/>
                                    </c:if>
                                </label></td>
                            <td class="text2">Diesel (Vehicle / Reefer)</td>
                            <td class="text2">
                                <label id="vehicleDiesel"></label> / <label id="reeferDiesel"></label>
                                <input type="hidden" name="vehicleDieselConsume" id="vehicleDieselConsume" value=""/>
                                <input type="hidden" name="reeferDieselConsume" id="reeferDieselConsume" value=""/>
                            </td>
                        </tr>
                        <tr>
                            <td class="text1">RCM</td>
                            <td class="text1"><c:out value="${rcmExpense}"/>
                            <input type="hidden" name="rcmExpense" id="rcmExpense" value="<c:out value="${rcmExpense}"/>" />
                            </td>
                            <td class="text1">Extra Expenses</td>
                            <td class="text1"><label id="extraExpense"><c:out value="${tripExpense}"/></label>
                            <input type="hidden" name="tripExtraExpense" id="tripExtraExpense" value="" />
                            </td>
                        </tr>
                        <tr>
                            
                            <td class="text2">Actual Expense</td>
                            <td class="text2"><label id="rcmAllocation"></label>
                                <input type="hidden" name="tripRcmAllocation" id="tripRcmAllocation" value="" />
                            </td>
                            <td class="text2">Break Up Of Extra Expenses</td>
                            <td >&nbsp;</td>

                        </tr>
                        <tr>
                            <td class="text1">Advance Paid</td>
                            <td class="text1"><label id="bpclAllocation"><c:out value="${tripAdvance}"/></label>
                                <input type="hidden" name="totalTripAdvance" id="totalTripAdvance" value="<c:out value="${tripAdvance}"/>" />
                            </td>
                            <td class="text1" >MIS (@kmrun*<c:out value="${miscValue}"/>*5%)</td>
                            <td class="text1"><label id="totalMisc"></label>
                                <input type="hidden" name="totalTripMisc" id="totalTripMisc" value="" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text2">Total Expenses</td>
                            <td class="text2"><label id="totalExpense"></label>
                                <input type="hidden" name="totalTripExpense" id="totalTripExpense" value="" />
                            </td>
                            <td class="text2">Bhatta</td>
                            <td class="text2"><label id="bhatta"></label>
                                <input type="hidden" name="totalTripBhatta" id="totalTripBhatta" value="" />
                            </td>
                            
                        </tr>
                        <tr>
                            <td class="text1">Balance</td>
                            <td class="text1"><label id="balance"></label>
                                <input type="hidden" name="tripBalance" id="tripBalance" value="" />
                            </td>
                            <td class="text1">BPCL Transaction</td>
                            <td class="text1"><c:out value="${bpclAmount}"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="text2">Ending Balance</td>
                            <td class="text2"><label id="endBalance"></label>
                                <input type="hidden" name="tripEndBalance" id="tripEndBalance" value="" />
                            </td>
                            <td class="text2">Uncleared Balance</td>
                            <td class="text2"><c:out value="${tripUnclearedBalance}"/></td>
                        </tr>
                        <tr>
                            <td class="text1"><font color='red'>*</font>Remarks for Extra Expenses</td>
                            <td class="text1" ><textarea name="settlementRemarks" id="settlementRemarks" cols="40" rows=""></textarea></td>
                            <td class="text1">Pay Mode</td>
                                <td class="text1">
                                    <select name="paymentMode" id="paymentMode"> 
                                        <option value="Carry To Salary">Carry Forward to Salary</option>
                                        <option value="Cash">Cash</option>
                                        <option value="Account Deposit">Deposit to Account</option>
                                    </select>
                                </td>
                        </tr>
                        <tr>
                            <c:if test = "${tripStartDetails != null}" >
                                        <c:forEach items="${tripStartDetails}" var="startDetails">
                                            <%--<c:set var="lrNumber" value="${startDetails.lrNumber}" />--%>
                                    <td ><font color="red">*</font>Vehicle Log Sheet Number</td>
                                        <td ><input type="text" name="lrNumber" id="lrNumber"  value="<c:out value="${startDetails.lrNumber}"/>" class="form-control" style="width:250px;height:40px"></td>
                                    <!--<input type="button" value="Close Trip" class="btn btn-success" onclick="closeTrip();" />-->
                                </c:forEach>
                                        </c:if>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                    <center>
                            <input type="button" class="btn btn-success" value="Save" name="save" onclick="driverSettlement(this.name)" />
                            <input type="button" class="btn btn-success" value="Save &amp; Print Settlement Voucher" name="saveprint" onclick="driverSettlement(this.name)"/>
                        </center>

                </div>


                    <c:if test="${tripAdvanceDetails != null}">
                         <div id="advance">
                      <c:if test="${tripAdvanceDetails != null}">
                        <table  class="table table-info mb30 table-hover" id="bg">
                            <thead> <tr>
                                <th  width="30">Sno</th>
                                <th  width="90">Advance Date</th>
                                <th  width="90">Trip Day</th>
                                <th  width="120">Estimated Advance</th>
                                <th  width="120">Requested Advance</th>
                                <th  width="90"> Type</th>
                                <th  width="120">Requested By</th>
                                <th  width="120">Requested Remarks</th>
                                <th  width="120">Approved By</th>
                                <th  width="120">Approved Remarks</th>
                                <th  width="120">Paid Advance</th>
                                </tr></thead>
                            <%int index7=1;%>
                            <c:forEach items="${tripAdvanceDetails}" var="tripAdvance">
                                <c:set var="totalAdvancePaid" value="${ totalAdvancePaid + tripAdvance.paidAdvance + totalpaidAdvance}"></c:set>
                                 <%
                                        String classText7 = "";
                                        int oddEven7 = index7 % 2;
                                        if (oddEven7 > 0) {
                                            classText7 = "text1";
                                        } else {
                                            classText7 = "text2";
                                        }
                            %>
                                <tr>
                                    <td class="<%=classText7%>"><%=index7++%></td>
                                    <td class="<%=classText7%>"><c:out value="${tripAdvance.advanceDate}"/></td>
                                    <td class="<%=classText7%>">DAY&nbsp;<c:out value="${tripAdvance.tripDay}"/></td>
                                    <td class="<%=classText7%>"><c:out value="${tripAdvance.estimatedAdance}"/></td>
                                    <td class="<%=classText7%>"><c:out value="${tripAdvance.requestedAdvance}"/></td>
                                      <c:if test = "${tripAdvance.requestType == 'A'}" >
                                         <td class="<%=classText7%>">Adhoc</td>
                                      </c:if>
                                      <c:if test = "${tripAdvance.requestType == 'B'}" >
                                         <td class="<%=classText7%>">Batch</td>
                                      </c:if>
                                      <c:if test = "${tripAdvance.requestType == 'M'}" >
                                         <td class="<%=classText7%>">Manual</td>
                                      </c:if>
                                     <td class="<%=classText7%>"><c:out value="${tripAdvance.approvalRequestBy}"/></td>
                                     <td class="<%=classText7%>"><c:out value="${tripAdvance.approvalRequestRemarks}"/></td>
                                     <td class="<%=classText7%>"><c:out value="${tripAdvance.approvedBy}"/></td>
                                     <td class="<%=classText7%>"><c:out value="${tripAdvance.approvalRemarks}"/></td>
                                    <td class="<%=classText7%>"><c:out value="${tripAdvance.paidAdvance + totalpaidAdvance}"/></td>
                                </tr>
                            </c:forEach>

                                <tr></tr>
                                    <tr>

                                    <td class="text1">&nbsp;</td>
                                    <td class="text1">&nbsp;</td>
                                    <td class="text1">&nbsp;</td>
                                    <td class="text1">&nbsp;</td>
                                    <td class="text1">&nbsp;</td>
                                         <td class="text1">&nbsp;</td>
                                     <td class="text1"></td>
                                     <td class="text1"></td>
                                     <td class="text1" colspan="2">Total Advance Paid</td>
                                    <td class="text1"><c:out value="${totalAdvancePaid}"/></td>
                                </tr>
                        </table>
                        <br/>
                        <c:if test="${tripAdvanceDetailsStatus != null}">
                         <table  class="table table-info mb30 table-hover" id="bg">
                             <thead><tr>
                            <th  colspan="13" > Advance Approval Status Details</th>
                        </tr>
                             <tr>
                                <th  width="30">Sno</th>
                                <th  width="90">Request Date</th>
                                <th  width="90">Trip Day</th>
                                <th  width="120">Estimated Advance</th>
                                <th  width="120">Requested Advance</th>
                                <th  width="90"> Type</th>
                                <th  width="120">Requested By</th>
                                <th  width="120">Requested Remarks</th>
                                <th  width="120">Approval Status</th>
                                <th  width="120">Paid Status</th>
                             </tr></thead>
                            <%int index13=1;%>
                            <c:forEach items="${tripAdvanceDetailsStatus}" var="tripAdvanceStatus">
                                 <%
                                        String classText13 = "";
                                        int oddEven11 = index13 % 2;
                                        if (oddEven11 > 0) {
                                            classText13 = "text1";
                                        } else {
                                            classText13 = "text2";
                                        }
                            %>
                                <tr>

                                    <td class="<%=classText13%>"><%=index13++%></td>
                                    <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.advanceDate}"/></td>
                                    <td class="<%=classText13%>">DAY&nbsp;<c:out value="${tripAdvanceStatus.tripDay}"/></td>
                                    <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.estimatedAdance}"/></td>
                                    <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.requestedAdvance}"/></td>
                                      <c:if test = "${tripAdvanceStatus.requestType == 'A'}" >
                                         <td class="<%=classText13%>">Adhoc</td>
                                      </c:if>
                                      <c:if test = "${tripAdvanceStatus.requestType == 'B'}" >
                                         <td class="<%=classText13%>">Batch</td>
                                      </c:if>
                                      <c:if test = "${tripAdvanceStatus.requestType == 'M'}" >
                                         <td class="<%=classText13%>">Manual</td>
                                      </c:if>

                                     <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.approvalRequestBy}"/></td>
                                     <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.approvalRequestRemarks}"/></td>
                                     <td class="<%=classText13%>">
                                    <c:if test = "${tripAdvanceStatus.approvalStatus== ''}" >
                                      &nbsp
                                    </c:if>
                                    <c:if test = "${tripAdvanceStatus.approvalStatus== '1' }" >
                                        Request Approved
                                    </c:if>
                                    <c:if test = "${tripAdvanceStatus.approvalStatus== '2' }" >
                                        Request Rejected
                                    </c:if>
                                    <c:if test = "${tripAdvanceStatus.approvalStatus== '0'}" >
                                        Approval in  Pending
                                    </c:if>
                                    &nbsp;</td>
                                         <td class="<%=classText13%>">
                                    <c:if test = "${ tripAdvanceStatus.approvalStatus== '1' || tripAdvanceStatus.approvalStatus== 'N'}" >
                                        Yet To Pay
                                    </c:if>
                                        &nbsp;
                                         </td>
                                </tr>
                            </c:forEach>
                         </table>
                                <br/>
                                <br/>
                                <c:if test="${vehicleChangeAdvanceDetailsSize != '0'}">
                                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                         <tr>
                                                <td  colspan="13" > Old Vehicle Advance Details</td>
                                            </tr>
                                        <tr>
                                            <td  width="30">Sno</td>
                                            <td  width="90">Advance Date</td>
                                            <td  width="90">Trip Day</td>
                                            <td  width="120">Estimated Advance</td>
                                            <td  width="120">Requested Advance</td>
                                            <td  width="90"> Type</td>
                                            <td  width="120">Requested By</td>
                                            <td  width="120">Requested Remarks</td>
                                            <td  width="120">Approved By</td>
                                            <td  width="120">Approved Remarks</td>
                                            <td  width="120">Paid Advance</td>
                                        </tr>
                                        <%int index17=1;%>
                                        <c:forEach items="${vehicleChangeAdvanceDetails}" var="vehicleChangeAdvance">
                                            <c:set var="totalVehicleChangeAdvancePaid" value="${ totalVehicleChangeAdvancePaid + vehicleChangeAdvance.paidAdvance + totalpaidAdvance}"></c:set>
                                            <%
                                                   String classText17 = "";
                                                   int oddEven17 = index17 % 2;
                                                   if (oddEven17 > 0) {
                                                       classText17 = "text1";
                                                   } else {
                                                       classText17 = "text2";
                                                   }
                                            %>
                                            <tr>
                                                <td class="<%=classText17%>"><%=index7++%></td>
                                                <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.advanceDate}"/></td>
                                                <td class="<%=classText17%>">DAY&nbsp;<c:out value="${vehicleChangeAdvance.tripDay}"/></td>
                                                <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.estimatedAdance}"/></td>
                                                <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.requestedAdvance}"/></td>
                                                <c:if test = "${vehicleChangeAdvance.requestType == 'A'}" >
                                                    <td class="<%=classText17%>">Adhoc</td>
                                                </c:if>
                                                <c:if test = "${vehicleChangeAdvance.requestType == 'B'}" >
                                                    <td class="<%=classText17%>">Batch</td>
                                                </c:if>
                                                <c:if test = "${vehicleChangeAdvance.requestType == 'M'}" >
                                                    <td class="<%=classText17%>">Manual</td>
                                                </c:if>
                                                <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvalRequestBy}"/></td>
                                                <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvalRequestRemarks}"/></td>
                                                <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvedBy}"/></td>
                                                <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvalRemarks}"/></td>
                                                <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.paidAdvance + totalpaidAdvance}"/></td>
                                            </tr>
                                        </c:forEach>

                                        <tr></tr>
                                        <tr>

                                            <td class="text1">&nbsp;</td>
                                            <td class="text1">&nbsp;</td>
                                            <td class="text1">&nbsp;</td>
                                            <td class="text1">&nbsp;</td>
                                            <td class="text1">&nbsp;</td>
                                            <td class="text1">&nbsp;</td>
                                            <td class="text1"></td>
                                            <td class="text1"></td>
                                            <td class="text1" colspan="2">Total Advance Paid</td>
                                            <td class="text1"><c:out value="${totalVehicleChangeAdvancePaid}"/></td>
                                        </tr>
                                    </table>
                                         </c:if>
                         </c:if>
                        <br/>
                        <center>
                            <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Save" /></a>
                        </center>
                    </c:if>
                </div>
                    </c:if>

<!--                <div id="fuel">
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">

                        <c:if test="${tripFuelDetails != null}">
                            <tr>
                                <td  colspan="6" >Fuel Expense Details</td>
                            </tr>
                            <tr>
                                <td class="text2">Fuel Liters</td>
                                <td class="text2">Fuel Price Per Liter</td>
                                <td class="text2">Total Fuel Value</td>
                                <td class="text2">Fuel Location</td>
                                <td class="text2">Fuel Remarks</td>
                            </tr>
                            <c:forEach items="${tripFuelDetails}" var="tripFuel">
                                <tr>
                                    <td class="text1"><c:out value="${tripFuel.fuelLitres}"/></td>
                                    <td class="text1"><c:out value="${tripFuel.fuelPricePerLitre}"/></td>
                                    <td class="text1"><c:out value="${tripFuel.totalFuelValue}"/></td>
                                    <td class="text1"><c:out value="${tripFuel.fuelLocation}"/></td>
                                    <td class="text1"><c:out value="${tripFuel.fuelRemarks}"/></td>
                                </tr>
                            </c:forEach>
                        </c:if>
                    </table>
                    <br/>
                    <br/>

                    <br/>
                    <center>
                        <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Save" /></a>
                    </center>
                </div>-->


                <div id="expense">
                    <table  class="table table-info mb30 table-hover" id="bg">
                        <c:if test="${tripExpenseDetails != null}">
                            <thead> <tr>
                                <th  colspan="8" >Other Expense Details</th>
                            </tr>
                            <tr>
                                 <th class="text2">S.No</th>
                                <th class="text2">Expense Name</th>
                                <th class="text2">Expense By</th>
                                <th class="text2">Expense Date</th>
                                <th class="text2">Expense Type</th>
<!--                                <th class="text2">Bill Mode</th>-->
<!--                                <th class="text2">Margin Value</th>-->
<!--                                <th class="text2">TAX</th>-->
                                <th class="text2">Expense Value</th>
                                <th class="text2">Total Expense</th>
                                <th class="text2">Remarks</th>
                            </tr></thead>
                            <% int index9 = 1; %>
                            <c:forEach items="${tripExpenseDetails}" var="tripExp">
                                <%
                                            String classText9 = "";
                                            int oddEven = index9 % 2;
                                            if (oddEven > 0) {
                                                classText9 = "text1";
                                            } else {
                                                classText9 = "text2";
                                            }
                                %>
                                <tr>
                                    <td class="<%=classText9%>"><%=index9++%></td>
                                    <td class="<%=classText9%>"><c:out value="${tripExp.expenseName}"/></td>
                                    <td class="<%=classText9%>"><c:out value="${tripExp.empName}"/></td>
                                    <td class="<%=classText9%>"><c:out value="${tripExp.expenseDate}"/></td>
                                    <td class="<%=classText9%>"><c:out value="${tripExp.expenseType}"/></td>
<!--                                    <td class="<%=classText9%>"><c:out value="${tripExp.passThroughStatus}"/></td>
                                    <td class="<%=classText9%>"><c:out value="${tripExp.marginValue}"/></td>
                                    <td class="<%=classText9%>"><c:out value="${tripExp.applicableTaxPercentage}"/></td>-->
                                    <td class="<%=classText9%>"><c:out value="${tripExp.expenseValue}"/></td>
                                    <td class="<%=classText9%>"><c:out value="${tripExp.totalExpenseValue}"/></td>
                                    <td class="<%=classText9%>"><c:out value="${tripExp.expenseRemarks}"/></td>
                                </tr>
                            </c:forEach>
                        </c:if>
                    </table>
                    <br/>
                    <br/>

                    <br/>
                    <center>
                        <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Save" /></a>
                    </center>
                </div>

               
                   <div id="tripDetail">
                    <table  class="table table-info mb30 table-hover" id="bg">
                        <thead><tr>
                            <th  colspan="6" >Trip Details</th>
                            </tr></thead>

                        <c:if test = "${tripDetails != null}" >
                            <c:forEach items="${tripDetails}" var="trip">


                                <tr>
                                    <!--                            <td class="text1"><font color="red">*</font>Trip Sheet Date</td>
                                                                <td class="text1"><input type="text" name="tripDate" class="datepicker" value=""></td>-->
                                    <td class="text1">CNote No(s)</td>
                                    <td class="text1">
                                        <c:out value="${trip.cNotes}" />
                                    </td>
                                    <td class="text1">Billing Type</td>
                                    <td class="text1">
                                        <c:out value="${trip.billingType}" />
                                    </td>
                                </tr>
                                <tr>
                                    <!--                            <td class="text2">Customer Code</td>
                                                                <td class="text2">BF00001</td>-->
                                    <td class="text2">Customer Name</td>
                                    <td class="text2">
                                        <c:out value="${trip.customerName}" />
                                        <input type="hidden" name="customerName" Id="customerName" class="form-control" style="width:250px;height:40px" value='<c:out value="${trip.customerName}" />'>
                                        <input type="hidden" name="customerNameEmail" value='<c:out value="${trip.customerName}"/>' />
                                        <input type="hidden" name="tripCodeEmail" value='<c:out value="${trip.tripCode}"/>' />
                                    <input type="hidden" name="routeInfoEmail" value='<c:out value="${trip.routeInfo}"/>' />
                                    <input type="hidden" name="cNotesEmail" value='<c:out value="${trip.cNotes}"/>' />
                                    <input type="hidden" name="vehicleNoEmail" value='<c:out value="${trip.vehicleNo}"/>' />
                                        
                                    </td>
                                    <td class="text2">Customer Type</td>
                                    <td class="text2" colspan="3" >
                                        <c:out value="${trip.customerType}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text1">Route Name</td>
                                    <td class="text1">
                                        <c:out value="${trip.routeInfo}" />
                                    </td>
                                    <!--                            <td class="text1">Route Code</td>
                                                                <td class="text1" >DL001</td>-->
                                    <td class="text1">Reefer Required</td>
                                    <td class="text1" >
                                        <c:out value="${trip.reeferRequired}" />
                                    </td>
                                    <td class="text1">Order Est Weight (MT)</td>
                                    <td class="text1" >
                                        <c:out value="${trip.totalWeight}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text2">Vehicle Type</td>
                                    <td class="text2">
                                        <c:out value="${trip.vehicleTypeName}" />
                                    </td>
                                    <td class="text2">Vehicle No</td>
                                    <td class="text2">
                                        <c:out value="${trip.vehicleNo}" />

                                    </td>
                                    <td class="text2">Vehicle Capacity (MT)</td>
                                    <td class="text2">
                                        <c:out value="${trip.vehicleTonnage}" />

                                    </td>
                                </tr>

                                <tr>
                                    <td class="text1">Veh. Cap [Util%]</td>
                                    <td class="text1">
                                        <c:out value="${trip.vehicleCapUtil}" />
                                    </td>
                                    <td class="text1">Special Instruction</td>
                                    <td class="text1">-</td>
                                    <td class="text1">Trip Schedule</td>
                                    <td class="text1"><c:out value="${trip.tripScheduleDate}" />  <c:out value="${trip.tripScheduleTime}" /> </td>
                                </tr>


                                <tr>
                                    <td class="text2">Driver </td>
                                    <td class="text2" colspan="5" >
                                        <c:out value="${trip.driverName}" />
                                    </td>

                                </tr>
                                <tr>
                                    <td class="text1">Product Info </td>
                                    <td class="text1" colspan="5" >
                                        <c:out value="${trip.productInfo}" />
                                    </td>

                                </tr>
                            </c:forEach>
                        </c:if>
                    </table>
                    <br/>
                    <br/>

                 <c:if test = "${expiryDateDetails != null}" >
                    <table  class="table table-info mb30 table-hover" id="bg">
                        <thead><tr>
                            <th  colspan="4" >Vehicle Compliance Check</th>
                            </tr></thead>
                          <c:forEach items="${expiryDateDetails}" var="expiryDate">
                        <tr>
                            <td class="text2">Vehicle FC Valid UpTo</td>
                            <td class="text2"><label><font color="green"><c:out value="${expiryDate.fcExpiryDate}" /></font></label></td>
                        </tr>
                        <tr>
                            <td class="text1">Vehicle Insurance Valid UpTo</td>
                            <td class="text1"><label><font color="green"><c:out value="${expiryDate.insuranceExpiryDate}" /></font></label></td>
                        </tr>
                        <tr>
                            <td class="text2">Vehicle Permit Valid UpTo</td>
                            <td class="text2"><label><font color="green"><c:out value="${expiryDate.permitExpiryDate}" /></font></label></td>
                        </tr>
                        <tr>
                            <td class="text2">Road Tax Valid UpTo</td>
                            <td class="text2"><label><font color="green"><c:out value="${expiryDate.roadTaxExpiryDate}" /></font></label></td>
                        </tr>
                        </c:forEach>
                    </table>
                    <br/>
                    <br/>
                    <center>
                        <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Next" /></a>
                    </center>
                       </c:if>

                </div>

                    <br/>
                      <div id="routeDetail">

                    <c:if test = "${tripPointDetails != null}" >
                        <table class="table table-info mb30 table-hover" >
                            <thead> <tr >
                                <th  height="30" >S No</th>
                                <th  height="30" >Point Name</th>
                                <th  height="30" >Type</th>
                                <th  height="30" >Route Order</th>
                                <th  height="30" >Address</th>
                                <th  height="30" >Planned Date</th>
                                <th  height="30" >Planned Time</th>
                                </tr></thead>
                            <% int index2 = 1;%>
                            <c:forEach items="${tripPointDetails}" var="tripPoint">
                                <%
                                            String classText1 = "";
                                            int oddEven8 = index2 % 2;
                                            if (oddEven8 > 0) {
                                                classText1 = "text1";
                                            } else {
                                                classText1 = "text2";
                                            }
                                %>
                                <tr >
                                    <td class="<%=classText1%>" height="30" ><%=index2++%></td>
                                    <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointName}" /></td>
                                    <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointType}" /></td>
                                    <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointSequence}" /></td>
                                    <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointAddress}" /></td>
                                    <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointPlanDate}" /></td>
                                    <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointPlanTime}" /></td>
                                </tr>
                            </c:forEach >
                        </table>
                        <br/>

                        <table border="0" class="border" align="centver" width="100%" cellpadding="0" cellspacing="0" >
                            <c:if test = "${tripDetails != null}" >
                                <c:forEach items="${tripDetails}" var="trip">
                                    <tr>
                                        <td class="text1" width="150"> Estimated KM</td>
                                        <td class="text1" width="120" > <c:out value="${trip.estimatedKM}" />&nbsp;</td>
                                        <td class="text1" colspan="4">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="text2" width="150"> Estimated Reefer Hour</td>
                                        <td class="text2" width="120"> <c:out value="${trip.estimatedTransitHours * 60 / 100}" />&nbsp;</td>
                                        <td class="text2" colspan="4">&nbsp;</td>
                                    </tr>

                                </c:forEach>
                            </c:if>
                        </table>
                        <br/>
                        <br/>
                         <center>
                            <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Save" /></a>
                        </center>
                    </c:if>
                    <br>
                    <br>

                </div>
<!--                <div id="preStart">
                    <c:if test = "${tripPreStartDetails != null}" >
                        <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                            <c:forEach items="${tripPreStartDetails}" var="preStartDetails">
                                <tr>
                                    <td  colspan="4" >Pre Start Details</td>
                                </tr>
                                <tr >
                                    <td class="text1" height="30" >Pre Start Date</td>
                                    <td class="text1" height="30" ><c:out value="${preStartDetails.preStartDate}" /></td>
                                    <td class="text1" height="30" >Pre Start Time</td>
                                    <td class="text1" height="30" ><c:out value="${preStartDetails.preStartTime}" /></td>
                                </tr>
                                <tr>
                                    <td class="text2" height="30" >Pre Odometer Reading</td>
                                    <td class="text2" height="30" ><c:out value="${preStartDetails.preOdometerReading}" /></td>
                                    <c:if test = "${tripDetails != null}" >
                                        <td class="text2">Pre Start Location / Distance</td>
                                        <td class="text2"> <c:out value="${trip.preStartLocation}" /> / <c:out value="${trip.preStartLocationDistance}" />KM</td>
                                    </c:if>
                                </tr>
                                <tr>
                                    <td class="text1" height="30" >Pre Start Remarks</td>
                                    <td class="text1" height="30" ><c:out value="${preStartDetails.preTripRemarks}" /></td>
                                </tr>
                            </c:forEach >
                        </table>
                        <br/>
                        <br/>
                        <br/>
                         <center>
                            <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Save" /></a>
                        </center>
                    </c:if>
                    <br>
                    <br>
                </div>-->
                    <div id="startDetail">
                    <c:if test = "${tripStartDetails != null}" >
                        <table class="table table-info mb30 table-hover" >
                            <c:forEach items="${tripStartDetails}" var="startDetails">
                                <thead><tr>
                            <th  colspan="6" > Trip Start Details</th>
                                    </tr></thead>
                         <tr>
                                <td class="text1" height="30" >Trip Planned Start Date</td>
                                    <td class="text1" height="30" ><c:out value="${startDetails.planStartDate}" />&nbsp;</td>
                                <td class="text1" height="30" >Trip Planned Start Time</td>
                                    <td class="text1" height="30" ><c:out value="${startDetails.planStartTime}" />&nbsp;</td>
                                <td class="text1" height="30" >Trip Start Reporting Date</td>
                                    <td class="text1" height="30" ><c:out value="${startDetails.startReportingDate}" />&nbsp;</td>

                        </tr>
                            <tr>
                                    <td class="text2" height="30" >Trip Start Reporting Time</td>
                                    <td class="text2" height="30" ><c:out value="${startDetails.startReportingTime}" />&nbsp;</td>
                                <td class="text2" height="30" >Trip Loading date</td>
                                    <td class="text2" height="30" ><c:out value="${startDetails.loadingDate}" />&nbsp;</td>
                                <td class="text2" height="30" >Trip Loading Time</td>
                                    <td class="text2" height="30" ><c:out value="${startDetails.loadingTime}" />&nbsp;</td>
                            </tr>
                                <tr >
                                <td class="text1" height="30" >Trip Loading Temperature</td>
                                    <td class="text1" height="30" ><c:out value="${startDetails.loadingTemperature}" />&nbsp;</td>
                                <td class="text1" height="30" >Trip Actual Start Date</td>
                                    <td class="text1" height="30" ><c:out value="${startDetails.startDate}" />&nbsp;</td>
                                <td class="text1" height="30" >Trip Actual Start Time</td>
                                    <td class="text1" height="30" ><c:out value="${startDetails.startTime}" />&nbsp;</td>
                            </tr>
                            <tr>
                                    <td class="text2" height="30" >Trip Start Odometer Reading(KM)</td>
                                    <td class="text2" height="30" ><c:out value="${startDetails.startOdometerReading}" />&nbsp;</td>
                                <td class="text2" height="30" >Trip Start Reefer Reading(HM)</td>
                                    <td class="text2" height="30" ><c:out value="${startDetails.startHM}" />&nbsp;</td>
                                    <td class="text2" height="30" colspan="2" ></td>
                            </tr>
                            </c:forEach >
                        </table>

                         <c:if test = "${tripUnPackDetails != null}" >
                    <table class="table table-info mb30 table-hover" id="addTyres1">
                        <thead><tr>
                                        <th width="20"  align="center" height="30" >Sno</th>
                                        <th  height="30" >Product/Article Code</th>
                                        <th  height="30" >Product/Article Name </th>
                                        <th  height="30" >Batch </th>
                                        <th  height="30" ><font color='red'>*</font>No of Packages</th>
                                        <th  height="30" ><font color='red'>*</font>Uom</th>
                                        <th  height="30" ><font color='red'>*</font>Total Weight (in Kg)</th>
                                        <th  height="30" ><font color='red'>*</font>Loaded Package Nos</th>
                                        <th  height="30" ><font color='red'>*</font>UnLoaded Package Nos</th>
                                        <th  height="30" ><font color='red'>*</font>Shortage</th>
                            </tr></thead>


                                         <%int i1=1;%>
                                         <c:forEach items="${tripUnPackDetails}" var="tripunpack">
                                            <tr>
                                                <td><%=i1%></td>
                                                <td><input type="text"  name="productCodes" id="productCodes" value="<c:out value="${tripunpack.articleCode}"/>" readonly/></td>
                                                <td><input type="text" name="productNames" id="productNames" value="<c:out value="${tripunpack.articleName}"/>" readonly/></td>
                                                <td><input type="text" name="productbatch" id="productbatch" value="<c:out value="${tripunpack.batch}"/>" readonly/></td>
                                                <td><input type="text" name="packagesNos" id="packagesNos" value="<c:out value="${tripunpack.packageNos}"/>" readonly/></td>
                                                <td><input type="text" name="productuom" id="productuom" value="<c:out value="${tripunpack.uom}"/>" readonly/></td>
                                                <td><input type="text" name="weights" id="weights" value="<c:out value="${tripunpack.packageWeight}"/> " readonly/>
                                                <td><input type="text" name="loadedpackages" id="loadedpackages<%=i1%>" value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly/>
                                                    <input type="hidden" name="consignmentId" value="<c:out value="${tripunpack.consignmentId}"/>"/>
                                                    <input type="hidden" name="tripArticleId" value="<c:out value="${tripunpack.tripArticleid}"/>"/>
                                                </td>
                                                <td><input type="text" name="unloadedpackages" id="unloadedpackages<%=i1%>" onblur="computeShortage(<%=i1%>);"  value="0"  onKeyPress="return onKeyPressBlockCharacters(event);"    /></td>
                                                <td><input type="text" name="shortage" id="shortage<%=i1%>"  value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly /></td>
                                            </tr>
                                            <%i1++;%>
                                         </c:forEach>

                    <br/>
                    <br/>
                    <br/>
                      </table>
                                         <br/>
                                         <br/>
                                         <br/>
                                         <br/>
                                         <br/>
                         <center>
                            <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Save" /></a>
                        </center>
                      </c:if>
                    <br>
                    <br>
                    </c:if>
                </div>
                      <div id="endDetail">
                    <c:if test = "${tripEndDetails != null}" >
                       <c:if test = "${tripEndDetails != null}" >
                         <table class="table table-info mb30 table-hover"" >
                            <c:forEach items="${tripEndDetails}" var="endDetails">
                                <thead>  <tr>
                            <th  colspan="6" > Trip End Details</th>
                                    </tr></thead>
                                <tr>
                                <td class="text1" height="30" >Trip Planned End Date</td>
                                    <td class="text1" height="30" ><c:out value="${endDetails.planEndDate}" /></td>
                                <td class="text1" height="30" >Trip Planned End Time</td>
                                    <td class="text1" height="30" ><c:out value="${endDetails.planEndTime}" /></td>
                                <td class="text1" height="30" >Trip Actual Reporting Date</td>
                                    <td class="text1" height="30" ><c:out value="${endDetails.endReportingDate}" /></td>
                            </tr>
                            <tr>
                                <td class="text2" height="30" >Trip Actual Reporting Time</td>
                                    <td class="text2" height="30" ><c:out value="${endDetails.endReportingTime}" /></td>
                                    <td class="text2" height="30" >Trip Unloading Date</td>
                                    <td class="text2" height="30" ><c:out value="${endDetails.unLoadingDate}" /></td>
                                <td class="text2" height="30" >Trip Unloading Time</td>
                                    <td class="text2" height="30" ><c:out value="${endDetails.unLoadingTime}" /></td>
                            </tr>
                            <tr>
                                <td class="text1" height="30" >Trip Unloading Temperature</td>
                                    <td class="text1" height="30" ><c:out value="${endDetails.unLoadingTemperature}" /></td>
                                <td class="text1" height="30" >Trip Actual End Date</td>
                                    <td class="text1" height="30" ><c:out value="${endDetails.endDate}" /></td>
                                <td class="text1" height="30" >Trip Actual End Time</td>
                                    <td class="text1" height="30" ><c:out value="${endDetails.endTime}" /></td>
                            </tr>
                            <tr>
                                    <td class="text2" height="30" >Trip End Odometer Reading(KM)</td>
                                    <td class="text2" height="30" ><c:out value="${endDetails.endOdometerReading}" /></td>
                                <td class="text2" height="30" >Trip End Reefer Reading(HM)</td>
                                    <td class="text2" height="30" ><c:out value="${endDetails.endHM}" /></td>
                                <td class="text2" height="30" >Total Odometer Reading(KM)</td>
                                    <td class="text2" height="30" ><c:out value="${endDetails.totalKM}" /></td>
                            </tr>
                            <tr>
                                <td class="text1" height="30" >Total Reefer Reading(HM)</td>
                                    <td class="text1" height="30" ><c:out value="${endDetails.totalHrs}" /></td>
                                <td class="text1" height="30" >Total Duration Hours</td>
                                    <td class="text1" height="30" ><c:out value="${endDetails.durationHours}" /></td>
                                <td class="text1" height="30" >Total Days</td>
                                    <td class="text1" height="30" ><c:out value="${endDetails.totalDays}" />

                            </tr>

                            </c:forEach >
                        </table>
                       </c:if>
                         <c:if test = "${tripUnPackDetails != null}" >
                    <table class="table table-info mb30 table-hover" id="addTyres1">
                        <thead><tr>
                                        <th colspan="10"  align="center" height="30" >Consignment Unloading Details</th>
                                    </tr>
                                    <tr>
                                        <th width="20"  align="center" height="30" >Sno</th>
                                        <th  height="30" >Product/Article Code</th>
                                        <th  height="30" >Product/Article Name </th>
                                        <th  height="30" >Batch </th>
                                        <th  height="30" ><font color='red'>*</font>No of Packages</th>
                                        <th  height="30" ><font color='red'>*</font>Uom</th>
                                        <th  height="30" ><font color='red'>*</font>Total Weight (in Kg)</th>
                                        <th  height="30" ><font color='red'>*</font>Loaded Package Nos</th>
                                        <th  height="30" ><font color='red'>*</font>UnLoaded Package Nos</th>
                                        <th  height="30" ><font color='red'>*</font>Shortage</th>
                                    </tr></thead>


                                         <%int i2=1;%>
                                         <c:forEach items="${tripUnPackDetails}" var="tripunpack">
                                            <tr>
                                                <td><%=i2%></td>
                                                <td><input type="text"  name="productCodes" id="productCodes" value="<c:out value="${tripunpack.articleCode}"/>" readonly/></td>
                                                <td><input type="text" name="productNames" id="productNames" value="<c:out value="${tripunpack.articleName}"/>" readonly/></td>
                                                <td><input type="text" name="productbatch" id="productbatch" value="<c:out value="${tripunpack.batch}"/>" readonly/></td>
                                                <td><input type="text" name="packagesNos" id="packagesNos" value="<c:out value="${tripunpack.packageNos}"/>" readonly/></td>
                                                <td><input type="text" name="productuom" id="productuom" value="<c:out value="${tripunpack.uom}"/>" readonly/></td>
                                                <td><input type="text" name="weights" id="weights" value="<c:out value="${tripunpack.packageWeight}"/> " readonly/>
                                                <td><input type="text" name="loadedpackages" id="loadedpackages<%=i2%>" value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly/>
                                                    <input type="hidden" name="consignmentId" value="<c:out value="${tripunpack.consignmentId}"/>"/>
                                                    <input type="hidden" name="tripArticleId" value="<c:out value="${tripunpack.tripArticleid}"/>"/>
                                                </td>
                                                <td><input type="text" name="unloadedpackages" id="unloadedpackages<%=i2%>" onblur="computeShortage(<%=i2%>);"  value="0"  onKeyPress="return onKeyPressBlockCharacters(event);"    /></td>
                                                <td><input type="text" name="shortage" id="shortage<%=i2%>"  value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly /></td>
                                            </tr>
                                            <%i2++;%>
                                         </c:forEach>

                    <br/>
                      </table>
                                         <br/>
                                         <br/>
                      </c:if>
                        <br/>
                        <br/>
                        <br/>
                        <br/>

                        <center>
                            <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Save" /></a>
                        </center>
                    </c:if>
                    <br>
                    <br>
                </div>
                <div id="statusDetail">
                    <% int index1 = 1;%>

                    <c:if test = "${statusDetails != null}" >
                        <table class="table table-info mb30 table-hover" >
                        <thead>   <tr >
                                <th height="30" >S No</th>
                                <th  height="30" >Status Name</th>
                                <th  height="30" >Remarks</th>
                                <th  height="30" >Created User Name</th>
                                <th  height="30" >Created Date</th>
                            </tr></thead>
                            <c:forEach items="${statusDetails}" var="statusDetails">
                                <%
                                            String classText = "";
                                            int oddEven1 = index1 % 2;
                                            if (oddEven1 > 0) {
                                                classText = "text1";
                                            } else {
                                                classText = "text2";
                                            }
                                %>
                                <tr >
                                    <td class="<%=classText%>" height="30" ><%=index1++%></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.statusName}" /></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.tripRemarks}" /></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.userName}" /></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.tripDate}" /></td>
                                </tr>
                            </c:forEach >
                        </table>
                        <br/>
                        <br/>
                        <br/>
                    </c:if>
                    <br>
                    <br>
                </div>
                
                <script>
				$(".nexttab").click(function() {
				    var selected = $("#tabs").tabs("option", "selected");
				    $("#tabs").tabs("option", "selected", selected + 1);
				});
			    </script>
			    
			    <div id="bpclDetail">
				<% int index10 = 1;%>
		
				<c:if test = "${bpclTransactionHistory == null}" >
				    <center><font color="red">No Records Found</font></center>
				</c:if>
				<c:if test = "${bpclTransactionHistory != null}" >
                                    <c:set var="totalAmount" value="0"/>
				    <table class="table table-info mb30 table-hover" >
                                        <thead><tr>
					    <th  height="30" >S No</th>
					    <th  height="30" >Trip Code</th>
					    <th  height="30" >Vehicle No</th>
					    <th  height="30" >Transaction History Id</th>
					    <th  height="30" >BPCL Transaction Id</th>
					    <th  height="30" >BPCL Account Id</th>
					    <th  height="30" >Dealer Name</th>
					    <th  height="30" >Dealer City</th>
					    <th  height="30" >Transaction Date</th>
					    <th  height="30" >Accounting Date</th>
					    <th  height="30" >Transaction Type</th>
					    <th  height="30" >Currency</th>
					    <th  height="30" >Transaction Amount</th>
					    <th  height="30" >Volume Document No</th>
					    <th  height="30" >Amount Balance</th>
					    <th  height="30" >Petromiles Earned</th>
					    <th  height="30" >Odometer Reading</th>
                                    </tr></thead>
					<c:forEach items="${bpclTransactionHistory}" var="bpclDetail">
					    <%
							String classText10 = "";
							int oddEven10 = index10 % 2;
							if (oddEven10 > 0) {
							    classText10 = "text1";
							} else {
							    classText10 = "text2";
							}
					    %>
					    <tr>
						<td class="<%=classText10%>" height="30" ><%=index10++%></td>
						<td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.tripCode}" /></td>
						<td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.vehicleNo}" /></td>
						<td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.transactionHistoryId}" /></td>
						<td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.bpclTransactionId}" /></td>
						<td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.accountId}" /></td>
						<td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.dealerName}" /></td>
						<td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.dealerCity}" /></td>
						<td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.transactionDate}" /></td>
						<td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.accountingDate}" /></td>
						<td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.transactionType}" /></td>
						<td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.currency}" /></td>
						<td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.amount}" /></td>
                                                <c:set var="totalAmount" value="${bpclDetail.amount + totalAmount}"  />
						<td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.volumeDocNo}" /></td>
						<td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.amoutBalance}" /></td>
						<td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.petromilesEarned}" /></td>
						<td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.odometerReading}" /></td>
					    </tr>
					</c:forEach >


                                        <tr>
                                        <td  height="30" style="text-align: right" colspan="12">Total Amount</td>
                                        <td  height="30" style="text-align: right"  ><c:out value="${totalAmount}"/></td>
                                        <td  height="30" style="text-align: right"  colspan="4">&nbsp;</td>
                                        </tr>
				    </table>
				    <br/>
				    <br/>
				    <br/>
				</c:if>
				<br>
				<br>
	</div>

<!--                <div id="tripSettlement">
                    <div style="border: #ffffff solid">
                        <h4>Trip Settlement</h4>
                        <table  border="0" class="border" align="center" width="500" cellpadding="0" cellspacing="0" id="bg">
                            <tr>
                                <td class="text2">Total KM Run</td>
                                <td class="text1"><c:out value="${totalRunKm}"/></td>
                            </tr>
                            <tr>
                                <td class="text2">Transit Days</td>
                                <td class="text1"><c:out value="${transitDays}"/></td>
                            </tr>
                            <tr>
                                <td class="text2">Trip End Status</td>
                                <td class="text1"><c:out value="${tripRemarks}"/></td>
                            </tr>
                            <tr>
                                <td class="text2">No Of Drivers</td>
                                <td class="text1"><c:out value="${driverCount}"/></td>
                            </tr>
                            <tr>
                                <td class="text2">Total Advance Paid (Rs.)</td>
                                <td class="text1">
                                    <input type="hidden" name="tripSheetId" id="tripSheetId" value="<c:out value="${tripSheetId}"/>" />
                                    <input type="hidden" name="totalTripAdvance" id="totalTripAdvance" value="<c:out value="${tripAdvance}"/>" />
                                    <c:out value="${tripAdvance}"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="text2">Total Expenses (Rs.)</td>
                                <td class="text1">
                                    <input type="hidden" name="totalTripExpense" id="totalTripExpense" value="<c:out value="${tripExpense}"/>" />    
                                    <c:out value="${tripExpense}"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="text2">Nett Value (Rs.)</td>
                                <td class="text1">
                                    <c:if test="${tripAdvance lt tripExpense}">
                                        <font color="red"><c:out value="${amountDifference}"/></font>
                                    </c:if>
                                    <c:if test="${tripExpense gt tripAdvance && tripExpense eq tripAdvance}">
                                        <font color="green"><c:out value="${amountDifference}"/></font>
                                    </c:if>
                                </td>
                            </tr>
                            <tr>
                                <td class="text2">Settlement Value (Rs.)</td>
                                <td class="text1">
                                    <c:if test="${tripAdvance lt tripExpense}">
                                        <input type="hidden" name="driverSettleAmount" id="driverSettleAmount" value="<c:out value="${amountDifference}"/>" />
                                        <font color="red"><c:out value="${amountDifference}"/></font>
                                    </c:if>
                                    <c:if test="${tripExpense gt tripAdvance && tripExpense eq tripAdvance}">
                                        <input type="hidden" name="driverReturnAmount" id="driverReturnAmount" value="<c:out value="${amountDifference}"/>" />
                                        <font color="green"><c:out value="${amountDifference}"/></font>
                                    </c:if>
                                </td>
                            </tr>
                            <tr>
                                <td class="text2">Pay Mode</td>
                                <td class="text1">
                                    <select name="paymentMode" id="paymentMode"> 
                                        <option value="Cash">cash</option>
                                        <option value="Account Deposit">Deposit to Account</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="text2">Remarks</td>
                                <td class="text1"><textarea rows="3" cols="20" name="settlementRemarks" id="settlementRemarks"></textarea></td>
                            </tr>
                        </table>
                        <br>
                        <br>
                        <center>
                            <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Save" name="save" onclick="driverSettlement(this.name)" /></a>
                            <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Save &amp; Print Settlement Voucher" name="saveprint" onclick="driverSettlement(this.name)"/></a>
                        </center>
                    </div>
                </div>-->

            </div>

         <script>
                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                </script>
    </form>
</body>
</div>
</div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>