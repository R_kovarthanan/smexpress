<%@page import="java.text.SimpleDateFormat" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
<!--                <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                     yearRange: '1900:' + new Date().getFullYear(),
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>


        <script>

    var httpRequest;
    function checkEmployeeInTrip(driverId) {
        if (vehicleId != '') {
            var url = '/throttle/checkEmployeeInTrip.do?driverId=' + driverId;
            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest();
            };
            httpRequest.send(null);
        }
    }
    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                //alert(val.trim());
                if (val.trim() !== "") {
                    var temp = val;
                    //alert('am here 0');
                    if(val !==""){
                            $("#tripStatus").text('this is already assigned to a trip');
                            $("#Save").hide();
                    }
                } else {
                   
                    $("#tripStatus").text("");
                    $("#Save").show();
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }
    </script>

 <script type="text/javascript">
            //auto com

        
  $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user

                $('#primaryDriver').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getPrimaryDriverName.do",
                            dataType: "json",
                            data: {
                                driverName: request.term
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                var primaryDriver = $('#primaryDriver').val();
                                if(items == '' && primaryDriver != ''){
                                    alert("Invalid Primary Driver Name");
                                    $('#primaryDriver').val('');
                                    $('#primaryDriverId').val('');
                                    $('#primaryDriver').focus();
                                }else{
                                }
                                response(items);
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var id = ui.item.Id;
                        $('#primaryDriver').val(value);
                        $('#primaryDriverId').val(id);
                        $('#secondaryDriverOne').focus();
                         checkEmployeeInTrip(id);
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    itemVal = '<font color="green">' + itemVal + '</font>';
                    return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
                };
                });



        </script>



        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>

          <script type="text/javascript">

               function calTotalKM(){
                var startKM = document.getElementById("startKM").value;
                var endKM = document.getElementById("endOdometerReading").value;
                document.getElementById("totalKM").value = parseInt(endKM) - parseInt(startKM) ;
            }
               function calTotalHM(){
                var startHM = document.getElementById("startHM").value;
                var endHM = document.getElementById("endHM").value;
                document.getElementById("totalHM").value = parseInt(endHM) - parseInt(startHM) ;
            }
               function calculateAmount(){
                var advancePaid = document.getElementById("advancePaid").value;
                var returnAmount = document.getElementById("returnAmount").value;
                var totalAmount =parseFloat(advancePaid)- parseFloat(returnAmount);
                document.getElementById("balanceAmount").value = parseFloat(totalAmount).toFixed(2) ;
            }

             function calculateDays() {
                var diff = 0;
             var one_day = 1000*60*60*24;
             var fromDate = document.getElementById("startDate").value;
             var toDate = document.getElementById("endDate").value;
             //alert(fromDate);
             //alert(toDate);
             var earDate = fromDate.split("-");
             var nexDate = toDate.split("-");
             var fD = parseFloat(earDate[0]).toFixed(0);
            var fM = parseFloat(earDate[1]).toFixed(0);
            var fY = parseFloat(earDate[2]).toFixed(0);

            var tD = parseFloat(nexDate[0]).toFixed(0);
            var tM = parseFloat(nexDate[1]).toFixed(0);
            var tY = parseFloat(nexDate[2]).toFixed(0);
            var date2 = tD + tM + tY;
             var d1 = new Date(fY,fM,fD);
            var d2 = new Date(tY,tM,tD);
            diff = (d2.getTime() - d1.getTime())/one_day;
            document.getElementById("totalDays").value = diff+1;
            //alert(document.getElementById("totalDays").value);
                }

      
        </script>

       <script type="text/javascript" language="javascript">
           
            function submitPage(){
            if(isEmpty(document.getElementById("endDate").value)){
            alert('please enter end date');
            document.getElementById("endDate").focus();
            }
            else if(isEmpty(document.getElementById("endOdometerReading").value)){
            alert('please enter end odometer reading');
            document.getElementById("endOdometerReading").focus();
            }
            else if(isEmpty(document.getElementById("endHM").value)){
            alert('please enter the end reefer reading');
            document.getElementById("endHM").focus();
            }
            else if(isEmpty(document.getElementById("primaryDriverId").value)){
            alert('please enter primary driver');
            document.getElementById("primaryDriverId").focus();
            }
            else{
                    document.trip.action = '/throttle/updateEmployeeTrip.do';
                    document.trip.submit();
                
            }
            }
        </script>





    </head>


    <body onload="calculateDays();">

        <form name="trip" method="post">
             <%
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String startDate = sdf.format(today);
        %>
            <%@ include file="/content/common/path.jsp" %>
               <%@include file="/content/common/message.jsp" %>
            <br>

       

            <br>
            <br>
            <br>
            <br>
            
            <div id="tabs" >
                 <c:if test = "${employeeTripDetails != null}" >
                <ul>
                    <li><a href="#tripDetails"><span>Trip Details </span></a></li>
                    <li><a href="#tripEmployeeDetail"><span>Emplyee Details</span></a></li>
                </ul>
                 </c:if>

               <div id="tripDetails">
                        <c:if test = "${employeeTripDetails != null}" >
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" colspan="6" >Trip Details</td>
                        </tr>

                            <c:forEach items="${employeeTripDetails}" var="trip">


                       
                        <tr>
                            <td class="text2">Trip Code</td>
                            <td class="text2">
                                <c:out value="${trip.tripCode}" />
                            </td>
                            <td class="text2">Vehicle No</td>
                            <td class="text2">
                                <c:out value="${trip.regNo}" />
                               

                            </td>
                            <td class="text2">Inactive Driver Name</td>
                            <td class="text2">
                                <c:out value="${trip.employeeName}" />

                            </td>
                            <td class="text2" colspan="2"></td>
                        </tr>

                        <tr>
                            <td class="text1">Route Name</td>
                            <td class="text1">
                                <c:out value="${trip.routeInfo}" />
                            </td>
                            <td class="text1">Vehicle Type</td>
                            <td class="text1"> <c:out value="${trip.vehicleType}" /></td>
                            <td class="text1">Driver Count</td>
                            <td class="text1"> 
                                <c:out value="${trip.driverCount}" />
                                <input type="text" name="driverCount" id="driverCount" value="<c:out value="${trip.driverCount}" />" />
                                <input type="text" name="driverInTrip" id="driverInTrip" value="<c:out value="${trip.driverInTrip}" />" />
                             </td>
                        </tr>


                        <tr>
                            <td class="text2">Customer Name </td>
                            <td class="text2" >
                                 <c:out value="${trip.customerName}" />
                            </td>
                            
                            
                             <td class="text2" colspan="4"></td>
                        </tr>
                       
                            </c:forEach>
                      
                    </table>
                    <br/>
                    
                    <br/>
                    <br/>
                    <br/>
                    <center>
                        <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Next" /></a>
                    </center>
 </c:if>
                </div>
               
                <div id="tripEmployeeDetail">
                      <c:if test = "${employeeTripDetails != null}" >
                            <c:forEach items="${employeeTripDetails}" var="trip">
                                <input type="hidden"  name="activeInd" id="activeInd" class="textbox" value="<c:out value="${activeInd}" />" >
                                <input type="hidden"  name="remarks" id="remarks" class="textbox" value="<c:out value="${remarks}" />" >
                                <input type="hidden"  name="employeeId" id="employeeId" class="textbox" value="<c:out value="${trip.employeeId}" />" >
                                <input type="hidden"  name="tripSheetId" id="tripSheetId" class="textbox" value="<c:out value="${trip.tripId}" />" >
                                <input type="hidden"  name="vehicleId" id="vehicleId" class="textbox" value="<c:out value="${trip.vehicleId}" />" >
                      <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" colspan="6" >Employee Trip Details</td>
                        </tr>
                        <tr>
                             <td class="text2">Trip Start Date</td>
                             <td class="text2"> <input type="hidden" name="startDate" id="startDate" value="<c:out value="${trip.startDate}" />" /><c:out value="${trip.startDate}" /></td>
                                <td class="text2" height="30" colspan="2"></td> 
                                  
                           
                        </tr>
                        <td class="text1"><font color="red">*</font>Driver Change Date</td>
                               <td class="text1"><input type="text" name="endDate" id="endDate"  class="datepicker" value="<%=startDate%>" onchange="calculateDays();">
                          <td class="text1"><font color="red">*</font>Driver Change Time</td>
                             <td class="text1" height="25" >
                                HH:<select name="driverChangeHour" id="driverChangeHour" class="textbox" ><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                MI:<select name="driverChangeMinute" id="driverChangeMinute" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>

                               <tr>
                            
                        </tr>
                         <tr>
                            <td class="text2"> Trip Start Odometer Reading(KM)</td>
                            <td class="text2"><input type="hidden" name="startKM" id="startKM" class="textbox" value="<c:out value="${trip.startOdometerReading}" />" ><c:out value="${trip.startOdometerReading}" /></td>
                            <td class="text2"> Trip End Odometer Reading(KM)</td>
                            <td class="text2"><input type="text" name="endOdometerReading" id="endOdometerReading" class="textbox" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" onchange="calTotalKM();"></td>

                        <tr>
                             <td class="text1"> Total Odometer Reading(KM)</td>
                             <td class="text1"><input type="text" readonly name="totalKM" id="totalKM" class="textbox" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"></td>
                        <td class="text1"> Total Days</td>
                             <td class="text1"><input type="text" name="totalDays" readonly id="totalDays" class="textbox" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"></td>
                        </tr>
                        <tr>
                            <td class="text2"> Trip Start Refer Reading(HM)</td>
                            <td class="text2"><input type="hidden" name="startHM" id="startHM" class="textbox" value="<c:out value="${trip.startHM}" />" ><c:out value="${trip.startHM}" /></td>
                            <td class="text2"> Trip End Refer Reading(HM)</td>
                            <td class="text2"><input type="text" name="endHM" id="endHM" class="textbox" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" onchange="calTotalHM();"></td>

                        </tr>
                        <tr>
                           <td class="text1"> Total Refer Reading(KM)</td>
                             <td class="text1"><input type="text" readonly name="totalHM" id="totalHM" class="textbox" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"></td>
                             <td class="text1">Driver Change City</td>
                             <td  width="80"> <select name="cityId" id="cityId" class="textbox" style="height:20px; width:122px;"" >
                               <c:if test="${cityList != null}">
                                <option value="" selected>--Select--</option>
                                <c:forEach items="${cityList}" var="cityList">
                                    <option value='<c:out value="${cityList.cityId}"/>'><c:out value="${cityList.cityName}"/></option>
                                </c:forEach>
                               </c:if>
                               </select>
                           </td>
                        </tr>
                        <tr>
                             <td class="text2"> Advance Paid</td>
                             <td class="text2"><input type="hidden"  name="advancePaid" id="advancePaid" class="textbox" value="<c:out value="${trip.actualAdvancePaid}" />" ><c:out value="${trip.actualAdvancePaid}" /></td>
                              <td class="text1" >Remarks</td>
                             <td class="text1" >
                                <textarea rows="3" cols="30" class="textbox" name="driverRemarks" id="driverRemarks"   style="width:142px"></textarea>
                             </td>
                        </tr>
                        <tr>
                             <td class="text2"> Return Amount</td>
                             <td class="text2"><input type="hidden"  name="returnAmount" id="returnAmount" class="textbox" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" onchange="calculateAmount();"></td>
                             <td class="text1"> Balance Amount</td>
                             <td class="text1"><input type="hidden" readonly name="balanceAmount" id="balanceAmount" class="textbox" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"></td>
                        </tr>
                          <tr>
                
                        <tr>
                            <td class="text2"> New Driver Mapped To this Trip</td>
                            <td class="text2" height="30">
                                <input type="hidden" class="textbox" id="primaryDriver"  name="primaryDriver" autocomplete="off" value=""/>
                                <c:if test="${primaryDriverDetails != null}">
                                    <select name="primaryDriverId" id="primaryDriverId" class="textbox">
                                        <option value="0">--Select--</option>
                                    <c:forEach items="${primaryDriverDetails}" var="primary">
                                        <option value="<c:out value="${primary.driverId}"/>"><c:out value="${primary.driverName}"/></option>
                                    </c:forEach>
                                </select>
                                </c:if>
                            </td>
                            
                            <td class="text2" height="30" colspan="2"></td>
                        </tr>
                      </table>
                    <br/>
                    <br/>
                  
                    <center>
                        <label id="tripStatus" style="color: red"></label>
                        <input type="button" class="button" id="Save" name="Save" value="Save" onclick="submitPage();" />
                    </center>
                    <br/>
                    <br/>
                            </c:forEach></c:if>
                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>

                </div>



                <script>
                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                </script>
            </div>
        </form>
    </body>
</html>