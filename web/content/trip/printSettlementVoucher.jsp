<%-- 
    Document   : PrintVoucher
    Created on : Oct 22, 2013, 10:36:08 PM
    Author     : Throttle
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <link rel="stylesheet" href="/throttle/css/rupees.css"  type="text/css" />

        <%@ page import="ets.domain.renderservice.business.RenderServiceTO" %>
        <%@ page import="ets.domain.renderservice.business.JobCardItemTO" %>
        <%@ page import="ets.domain.problemActivities.business.ProblemActivitiesTO" %>
        <%@ page import="ets.domain.operation.business.OperationTO" %>


        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.*" %>
        <%@ page import="java.lang.Double" %>

        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>\
        <link rel="stylesheet" href="style.css" />
        <%@ page import="ets.domain.contract.business.ContractTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <title>Cash Payment Voucher</title>
    </head>

    <script>



        function print()
        {
            var DocumentContainer = document.getElementById("printDiv");
            var WindowObject = window.open('', "TrackHistoryData",
                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }

        function back()
        {
            window.history.back()
        }

    </script>
    <%
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        String voucherDate = dateFormat.format(date);;
    %>



    <body onload="calcExpenses();">
        <div id="printDiv">

            <table width="860" border="1" cellpadding="0" cellspacing="0" align="center" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; ">

                <tr>
                    <td class="contenthead" colspan="4" >Trip Settlement Details
                        <input type="hidden" name="tripSheetId" id="tripSheetId" value="<c:out value="${tripSheetId}"/>"  />
                        <input type="hidden" name="tollAmount" id="tollAmount" value="<c:out value="${tollAmount}"/>"  />
                        <input type="hidden" name="battaAmount" id="battaAmount" value="<c:out value="${driverBatta}"/>"  />
                        <input type="hidden" name="incentiveAmount" id="incentiveAmount" value="<c:out value="${driverIncentive}"/>"  />
                        <input type="hidden" name="fuelPrice" id="fuelPrice" value="<c:out value="${fuelPrice}"/>"  />
                        <input type="hidden" name="milleage" id="milleage" value="<c:out value="${milleage}"/>"  />
                        <input type="hidden" name="reeferConsumption" id="reeferConsumption" value="<c:out value="${reeferConsumption}"/>"  />
                        <input type="hidden" name="vehicleTypeId" id="vehicleTypeId" value="<c:out value="${vehicleTypeId}"/>"  />
                        <input type="hidden" name="gpsKm" id="gpsKm" value="<c:out value="${gpsKm}"/>"  />
                        <input type="hidden" name="totalRunKm" id="totalRunKm" value="<c:out value="${runKm}"/>"  />
                        <input type="hidden" name="totalDays" id="totalDays" value="<c:out value="${totaldays}"/>"  />
                        <input type="hidden" name="totalRefeerHours" id="totalRefeerHours" value="<c:out value="${runHm}"/>"  />
                        <input type="hidden" name="estimatedExpense" id="estimatedExpense" value="<c:out value="${estimatedExpense}"/>"  />
                        <input type="hidden" name="miscValue" id="miscValue" value="<c:out value="${miscValue}"/>"  />
                        <input type="hidden" name="driverCount" id="driverCount" value="<c:out value="${driverCount}"/>"  />
                        <input type="hidden" name="extraExpenseValue" id="extraExpenseValue" value="<c:out value="${extraExpenseValue}"/>"  />
                        <input type="hidden" name="extraExpenseValue" id="extraExpenseValue" value="<c:out value="${tripAdvance}"/>"  />
                        <input type="hidden" name="bpclAmount" id="bpclAmount" value="<c:out value="${bpclAmount}"/>"  />
                    </td>
                </tr>
                <script>
                    function calcExpenses() {
                        var runKM = document.getElementById("totalRunKm").value;
                        var tollAmount = document.getElementById("tollAmount").value;
                        var driverBatta = document.getElementById("battaAmount").value;
                        var driverIncentive = document.getElementById("incentiveAmount").value;
                        var transitDays = document.getElementById("totalDays").value;
                        var mileage = document.getElementById("milleage").value;
                        var fuelPrice = document.getElementById("fuelPrice").value;
                        var reeferMilleage = document.getElementById("reeferConsumption").value;
                        var driverCount = document.getElementById("driverCount").value;
                        var extraExpenseValue = document.getElementById("extraExpenseValue").value;


                        //Calc Bhatta
                        var bhatta = (transitDays * 100) * driverCount;
                        $("#bhatta").text(bhatta);
                        $("#totalTripBhatta").val(bhatta);
                        //Calc Bhatta


                        //Calculate Misc
                        var reeferHour = document.getElementById("totalRefeerHours").value;
                        var miscValue = document.getElementById("miscValue").value;
                        var misc = miscValue * runKM;
//                                var totalMisc = (misc / 100) * 5;
                        var totalMisc = ((parseFloat(misc) + parseFloat(extraExpenseValue) + parseFloat(bhatta)) / 100) * 5;
                        totalMisc = totalMisc.toFixed(2);
                        $("#totalMisc").text(totalMisc);
                        $("#totalTripMisc").val(totalMisc);
                        //Calculate Misc



                        //Calc Diesel Consume
                        var reeferConsume = (reeferHour * reeferMilleage).toFixed(2);
                        var vehicleConsume = (runKM * mileage).toFixed(2);
                        var totalConsume = parseInt(vehicleConsume) + parseInt(reeferConsume);
                        var ratePerKm = parseFloat(document.getElementById("estimatedExpense").value) / parseFloat(document.getElementById("totalRunKm").value);
                        ratePerKm = ratePerKm.toFixed(2);
                        $("#ratePerKm").text(ratePerKm);
                        $("#vehicleDiesel").text(vehicleConsume);
                        $("#reeferDiesel").text(reeferConsume);
                        $("#totalConsume").text(totalConsume.toFixed(2));
                        $("#tripDieselConsume").val(totalConsume.toFixed(2));
                        //Calc Diesel Consume

                        //Calc RCm Allocation
                        //var rcmAllocation = totalConsume  * fuelPrice;
                        //alert(document.getElementById("estimatedExpense").value)
                        var rcmAllocation = parseFloat(document.getElementById("estimatedExpense").value);
                        $("#rcmAllocation").text(rcmAllocation.toFixed(2));
                        //Calc RCm Allocation

                        //Calc Total Expense
                        var extraExpense = $("#extraExpense").text();


                        var nettExtraExpense = (parseFloat(bhatta) + parseFloat(totalMisc) + parseFloat(extraExpense)).toFixed(2);
                        //alert(nettExtraExpense);
                        $("#extraExpense").text(nettExtraExpense);
                        $("#tripExtraExpense").val(nettExtraExpense)
                        extraExpense = nettExtraExpense;
                        var totalExpense = parseFloat(rcmAllocation);
                        $("#totalExpense").text(totalExpense.toFixed(2));
                        $("#totalTripExpense").val(totalExpense.toFixed(2));
                        //Calc Total Expense

                        //Calc Balance
                        var bpclAllocation = $("#bpclAllocation").text();
                        var balance = (parseFloat(totalExpense) - parseFloat(bpclAllocation)).toFixed(2);
                        $("#balance").text(balance);
                        $("#tripBalance").val(balance);
                        //Calc Balance

                        //Calc End Balance
                        var startBalance = $("#startBalance").text();
                        var endBalance = parseFloat(balance);
                        $("#endBalance").text(endBalance.toFixed(2));
                        $("#tripEndBalance").val(endBalance.toFixed(2));
                        $("#tripStartingBalance").val(endBalance.toFixed(2));
                        //Calc End Balance
                    }
                </script>
                <tr>
                    <td height="30" style="text-align:center; text-transform:uppercase;" colspan="4">TTA<br>
                        <center><u>Driver Settlement Voucher</u></center>
                    </td>
                </tr>

                <tr>
                    <td  height="30" height="30" style="text-align:left;" colspan="2">&nbsp;</td>
                    <td  height="30" height="30" style="text-align:left;">Trip Start Date</td>
                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${startDate}"/></label></td>
                </tr>
                <tr>
                    <td  height="30" height="30" style="text-align:left;">C Note No</td>
                    <td  height="30" height="30" style="text-align:left;"><label id="cNoteNo"><c:out value="${cNotes}"/></label></td>
                    <td  height="30" height="30" style="text-align:left;">Trip Start Time in IST</td>
                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${startTime}"/></label></td>
                </tr>

                <tr>
                    <td  height="30" height="30" style="text-align:left;" colspan="2">&nbsp</td>
                    <td  height="30" height="30" style="text-align:left;">Trip End Date</td>
                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${tripEndDate}"/></label></td>
                </tr>
                <tr>
                    <td  height="30" height="30" style="text-align:left;" colspan="2">&nbsp</td>
                    <td  height="30" height="30" style="text-align:left;">Trip End Time in IST</td>
                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${tripEndTime}"/></label></td>
                </tr>

                <tr>
                    <td  height="30" height="30" style="text-align:left;">Truck Number</td>
                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${vehicleNo}"/></label></td>
                    <td  height="30" height="30" style="text-align:left;">Trip No</td>
                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${tripCode}"/></label></td>

                </tr>
                <tr>
                    <td  height="30" height="30" style="text-align:left;">Driver 1</td>
                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${driverName1}"/></label></td>
                    <td  height="30" height="30" style="text-align:left;">Rate/Km For Trip</td>
                    <td  height="30" height="30" style="text-align:left;"><label id="ratePerKm"></label></td>
                </tr>
                <tr>
                    <td  height="30" height="30" style="text-align:left;">Driver 2</td>
                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${driverName2}"/></label></td>
                    <td  height="30" height="30" style="text-align:left;">Diesel Reference Rate</td>
                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${fuelPrice}"/></label></td>
                </tr>
                <tr>
                    <td  height="30" height="30" style="text-align:left;">Stop No 1</td>
                    <td  height="30" height="30" style="text-align:left;">Pickup</td>
                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${origin}"/></label></td>
                    <td  height="30" height="30" style="text-align:left;">&nbsp;</td>
                </tr>
                <tr>
                    <td  height="30" height="30" style="text-align:left;">Stop No 2</td>
                    <td  height="30" height="30" style="text-align:left;">Drop</td>
                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${destination}"/></label></td>
                    <td  height="30" height="30" style="text-align:left;">&nbsp;</td>
                </tr>
                <tr>
                    <td  height="30" height="30" style="text-align:left;"></td>
                    <td  height="30" height="30" style="text-align:left;">
                        <input type="hidden" name="tripStartingBalance" id="tripStartingBalance" value="0" />
                    </td>
                    <td  height="30" height="30" style="text-align:left;" colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td  height="30" height="30" style="text-align:left;">Odometer Starting</td>
                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${startKm}"/></label></td>
                    <td  height="30" height="30" style="text-align:left;">Reefer Hours Starting</td>
                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${startHm}"/></label></td>
                </tr>
                <tr>
                    <td  height="30" height="30" style="text-align:left;">Odometer Ending</td>
                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${endKm}"/></label></td>
                    <td  height="30" height="30" style="text-align:left;">Reefer Hours Ending</td>
                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${endHm}"/></label></td>
                </tr>
                <tr>
                    <td  height="30" height="30" style="text-align:left;">Odometer Km</td>
                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${runKm}"/></label></td>                    
                    <td  height="30" height="30" style="text-align:left;">Reefer Hours</td>
                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${runHm}"/></label></td>
                </tr>
                <tr>
                    <td  height="30" height="30" style="text-align:left;">GPS  Km</td>
                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${gpsKm}"/></label>
                    </td>
                    <td  height="30" height="30" style="text-align:left;">Diesel Used</td>
                    <td  height="30" height="30" style="text-align:left;"><c:out value="${tripDieselConsume}"/>
                        
                    </td>
                </tr>
                <tr>
                    <td  height="30" height="30" style="text-align:left;">Km Run</td>
                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${gpsKm}"/></label></td>
                    <td height="30" height="30" style="text-align:left;">Diesel (Vehicle / Reefer)</td>
                    <td height="30" height="30" style="text-align:left;"><c:out value="${vehicleDieselConsume}"/> / <c:out value="${reeferDieselConsume}"/>
                    </td>

                </tr>
                <tr>
                    <td height="30" height="30" style="text-align:left;">RCM</td>
                    <td height="30" height="30" style="text-align:left;"><c:out value="${rcmExpense}"/></td>
                    <td  height="30" height="30" style="text-align:left;">Extra Expenses</td>
                    <td  height="30" height="30" style="text-align:left;"><label id="extraExpense"><c:out value="${tripExpense}"/></label>
                    <input type="hidden" name="tripExtraExpense" id="tripExtraExpense" value="" /></td>

                </tr>
                
                <tr>

                    <td  height="30" height="30" style="text-align:left;">Actual Expense</td>
                    <td  height="30" height="30" style="text-align:left;"><label id="rcmAllocation"></label>
                        <input type="hidden" name="tripRcmAllocation" id="tripRcmAllocation" value="" />
                    </td>
                    <td  height="30" height="30" style="text-align:left;">Break Up Of Extra Expenses</td>
                    <td height="30" height="30" style="text-align:left;">&nbsp;</td>

                </tr>
                <tr>
                    <td  height="30" height="30" style="text-align:left;">Advance Paid</td>
                    <td  height="30" height="30" style="text-align:left;"><label id="bpclAllocation"><c:out value="${tripAdvance}"/></label>
                        <input type="hidden" name="totalTripAdvance" id="totalTripAdvance" value="<c:out value="${tripAdvance}"/>" />
                    </td>

                    <td  height="30" height="30" style="text-align:left;">Mis(Total Km*<c:out value="${miscValue}"/>*5%)</td>
                    <td  height="30" height="30" style="text-align:left;"><label id="totalMisc"></label>
                        <input type="hidden" name="totalTripMisc" id="totalTripMisc" value="" />
                    </td>
                </tr>
                <tr>
                    <td  height="30" height="30" style="text-align:left;">Total Expenses</td>
                    <td  height="30" height="30" style="text-align:left;"><label id="totalExpense"></label>
                        <input type="hidden" name="totalTripExpense" id="totalTripExpense" value="" />
                    </td>
                    <td  height="30" height="30" style="text-align:left;">Bhatta</td>
                    <td  height="30" height="30" style="text-align:left;"><label id="bhatta"></label>
                        <input type="hidden" name="totalTripBhatta" id="totalTripBhatta" value="" />
                    </td>

                </tr>
                <tr>
                    <td  height="30" height="30" style="text-align:left;">Balance</td>
                    <td  height="30" height="30" style="text-align:left;"><label id="balance"></label>
                        <input type="hidden" name="tripBalance" id="tripBalance" value="" />
                    </td>
                    <td height="30" height="30" style="text-align:left;">BPCL Transaction</td>
                    <td height="30" height="30" style="text-align:left;"><c:out value="${bpclAmount}"/>
                    </td>
                </tr>
                <tr>
                    <td  height="30" height="30" style="text-align:left;">Ending Balance</td>
                    <td  height="30" height="30" style="text-align:left;"><label id="endBalance"></label>
                        <input type="hidden" name="tripEndBalance" id="tripEndBalance" value="" />
                    </td>
                    <td colspan="2" height="30" height="30" style="text-align:left;">&nbsp;</td>
                </tr>
                <tr>
                    <td  height="30" height="30" style="text-align:left;">Remarks for Extra Expenses</td>
                    <td  height="30" height="30" style="text-align:left;" ><c:out value="${settlementRemarks}"/></td>
                    <td  height="30" height="30" style="text-align:left;">Pay Mode</td>
                    <td  height="30" height="30" style="text-align:left;"><c:out value="${paymentMode}"/></td>
                </tr>

                <tr>
                    <% String total = "";
                        if (request.getAttribute("amount") != null) {
                            total = (String) request.getAttribute("amount");
                        } else {
                            total = "00";
                        }
                    %>
                    <% if(total.contains("-")) {
                        total = total.replaceAll("-","");
                    %>
                        <td colspan="5" height="35" align="left">
                        <b>Return amount in rupees:</b><%float spareTotal = 0.00F;
                            spareTotal = Float.parseFloat(total);
                        %>
                        <jsp:useBean id="spareTotalRound"   class="ets.domain.report.business.NumberWords" >
                            <% spareTotalRound.setRoundedValue(String.valueOf(spareTotal));%>
                            <%-- <b><jsp:getProperty name="spareTotalRound" property="roundedValue" /> </b> --%>
                            <% spareTotalRound.setNumberInWords(spareTotalRound.getRoundedValue());%>
                            <b><jsp:getProperty name="spareTotalRound" property="numberInWords" />Only</b>
                        </jsp:useBean>
                        </p> &nbsp;&nbsp;&nbsp;<c:out value="${remarks}"/>
                    </td>
                    <%}else{ %>
                     <td colspan="5" height="35" align="left">
                        <b>Balance amount in rupees:</b><%float spareTotal = 0.00F;
                            spareTotal = Float.parseFloat(total);
                        %>
                        <jsp:useBean id="spareTotalRound1"   class="ets.domain.report.business.NumberWords" >
                            <% spareTotalRound1.setRoundedValue(String.valueOf(spareTotal));%>
                            <%-- <b><jsp:getProperty name="spareTotalRound" property="roundedValue" /> </b> --%>
                            <% spareTotalRound1.setNumberInWords(spareTotalRound1.getRoundedValue());%>
                            <b><jsp:getProperty name="spareTotalRound1" property="numberInWords" />Only</b>
                        </jsp:useBean>
                        </p> &nbsp;&nbsp;&nbsp;<c:out value="${remarks}"/>
                    </td>   
                    <%}%>
                    
                </tr>
                <tr>
                    <td height="90" colspan="2" valign="bottom">Prepared By</td>
                    <td height="90" colspan="2" valign="bottom">Passed By</td>
                </tr>
                <tr align="center">
                    <td colspan="4">
                        <input align="center" type="button" onclick="print();" value = "Print"   />
                    </td>
                </tr>
            </table>

        </div>
    </body>
</html>
