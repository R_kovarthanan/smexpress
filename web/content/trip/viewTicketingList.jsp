
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });

    function createTicket(){
        document.ticketing.action=" /throttle/createTicket.do";
        document.ticketing.method="post";
        document.ticketing.submit();
    }
</script>
<!--<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>-->
    
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ViewTicketing" text="ViewTicketing"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
		                    <li class=""><spring:message code="hrms.label.ViewTicketing" text="ViewTicketing"/></li>
		
		                </ol>
		            </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
                    
    <body>


        <form name="ticketing"  method="post" >
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            
            <%@ include file="/content/common/message.jsp" %>
            
            
          <table class="table table-info mb30 table-hover" id="bg" >
		    <thead>
		<tr>
		    <th colspan="4" height="30" >View Ticketing</th>
		</tr>
               </thead>	
                <tr>
                    <td>Ticket From</td>
                    <td><input type="text" name="fromDate" id="fromDate" class="datepicker  form-control" style="width:240px;height:40px"/></td>
                    <td>Ticket To</td>
                    <td><input type="text" name="toDate" id="toDate" class="datepicker form-control" style="width:240px;height:40px" style="width:200px;height:40px"/></td>
                </tr>
                <tr>
                    <td>Ticket Priority</td>
                    <td>
                        <select class="form-control" style="width:240px;height:40px" name="priority" id="priority" >
                                <c:if test="${ticketingStatusList != null}">
                            <c:forEach items="${ticketingStatusList}" var="sts">
                                <c:if test="${sts.type == 'Priority'}">
                                <option value="<c:out value="${sts.statusId}"/>"><c:out value="${sts.statusName}"/></option>
                                </c:if>
                            </c:forEach>
                                </c:if>
                        </select>
                    </td>
                    <td>Ticket Status</td>
                    <td>
                        <select class="form-control" style="width:240px;height:40px" name="status1" id="status1">
                            <c:if test="${ticketingStatusList != null}">
                            <c:forEach items="${ticketingStatusList}" var="sts">
                                <c:if test="${sts.type == 'status'}">
                                <option value="<c:out value="${sts.statusId}"/>"><c:out value="${sts.statusName}"/></option>
                                </c:if>
                            </c:forEach>
                            </c:if>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Ticket Type</td>
                    <td>
                        <select class="form-control" style="width:240px;height:40px" name="type" id="type">
                            <c:if test="${ticketingStatusList != null}">
                            <c:forEach items="${ticketingStatusList}" var="sts">
                                <c:if test="${sts.type == 'type'}">
                                <option value="<c:out value="${sts.statusId}"/>"><c:out value="${sts.statusName}"/></option>
                                </c:if>
                            </c:forEach>
                            </c:if>
                        </select>
                    </td>
                    </tr>
                     </table>
                    <tr>
                    <center>
                    <td><input type="button" class="btn btn-success" name="Search" value="Search"></td>
                    <td><input type="button" class="btn btn-success" name="Create Ticket" value="Create Ticket" onclick="createTicket()"></td>
               </center>
                    </tr>
           
            <br>
            <br>
            
                <table class="table table-info mb30 table-hover" id="table" >	
			<thead>

           

                    <tr >
                        <th>S.No</th>
                        <th>Ticket No</th>
                        <th>Title</th>
                        <th>Message</th>
                        <th>Mail From</th>
                        <th>Mail To</th>
                        <th>Mail Cc</th>
                        <th>Status</th>
                        <th>Type</th>
                        <th>Priority</th>
                        <th>Raised By</th>
                        <th>Raised On</th>
                        <th>Details</th>
                    </tr>
                </thead>
                <tbody>


                    <% int sno = 0;%>
                    <c:if test = "${ticketingList != null}">
                        <c:forEach items="${ticketingList}" var="ticketList">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td   align="left"> <c:out value="${ticketList.serialNumber}" /> </td>
                                <td   align="left"> <c:out value="${ticketList.ticketNo}" /></td>
                                <td   align="left"> <c:out value="${ticketList.title}" /></td>
                                <td   align="left"> <c:out value="${ticketList.message}"/></td>
                                <td   align="left"> <c:out value="${ticketList.from}"/></td>
                                <td   align="left"> <c:out value="${ticketList.to}"/></td>
                                <td   align="left"> <c:out value="${ticketList.cc}"/></td>
                                <td   align="left"> <c:out value="${ticketList.status}"/></td>
                                <td   align="left"> <c:out value="${ticketList.type}"/></td>
                                <td   align="left"> <c:out value="${ticketList.priority}"/></td>
                                <td   align="left"> <c:out value="${ticketList.raisedBy}"/></td>
                                <td   align="left"> <c:out value="${ticketList.raisedOn}"/></td>
                                <td   align="left">
                                <a href="viewTicketDetails.do?ticketId=<c:out value="${ticketList.ticketId}"/>">Follow Up</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>


            <input type="hidden" name="count" id="count" value="<%=sno%>" />

            
            
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 4);
            </script>
        </form>
    </body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>