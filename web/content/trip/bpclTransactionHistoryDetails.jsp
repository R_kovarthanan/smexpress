<%-- 
    Document   : bpclTransactionHistoryUpload
    Created on : Jan 04, 2014, 12:38:56 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<script type="text/javascript" src="/throttle/js/suest"></script>
<script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    function submitPage(value) {
        if (value == 'Proceed') {
            document.upload.action = '/throttle/saveBpclTransactionHistory.do';
            document.upload.submit();
        } else {
            document.upload.action = '/throttle/bpclTransactionHistoryUpload.do';
            document.upload.submit();
        }
    }
</script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>    
    <body>
        <form name="upload" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <br>

            <%
            if(request.getAttribute("errorMessage")!=null){
            String errorMessage=(String)request.getAttribute("errorMessage");                
            %>
            <center><b><font color="red" size="1"><%=errorMessage%></font></b></center>
                        <%}%>
            <table border="0" cellpadding="0" cellspacing="0" width="780" align="center">
                <tr>
                    <td class="contenthead" colspan="4">BPCL Transaction History Upload</td>
                </tr>
                <tr>
                    <td class="text2">Select file</td>
                    <td class="text2"><input type="file" name="importBpclTransaction" id="importBpclTransaction" class="importBpclTransaction"></td>                             
                </tr>
                <tr>
                    <td colspan="4" class="text1" align="center" ><input type="button" value="Submit" name="Submit" onclick="submitPage(this.value)" >
                </tr>
            </table>
            <br>
            <br>
            <br>
            <% int i = 1 ;%>
            <% int index = 0;%> 
            <%int oddEven = 0;%>
            <%  String classText = "";%>    
            <c:if test="${bpclTransactionList != null}">
                <table>
                    <th class="contenthead" colspan="15">File Uploaded Details&nbsp;:<input type="hidden" name="transactionHistoryId" value="<c:out value="${transactionHistoryId}"/>"/> <c:out value="${transactionHistoryId}"/></th>
                    <tr>
                        <td class="contenthead">S No</td>
                        <td class="contenthead">Status</td>
                        <td class="contenthead">Trip Code</td>
                        <td class="contenthead">Vehicle No</td>
                        <td class="contenthead">Account Id</td>
                        <td class="contenthead">Dealer Name</td>
                        <td class="contenthead">Dealer City</td>
                        <td class="contenthead">Transaction Date</td>
                        <td class="contenthead">Accounting Date</td>
                        <td class="contenthead">Transaction Type</td>
                        <td class="contenthead">Currency</td>
                        <td class="contenthead">Transaction Amount</td>
                        <td class="contenthead">Volume/Doc No</td>
                        <td class="contenthead">Amount Balance</td>
                        <td class="contenthead">Petromiles Earned</td>
                        <td class="contenthead">Odometer Reading</td>
                    </tr>
                    <c:forEach items="${bpclTransactionList}" var="bpcl">
                        <%
                      oddEven = index % 2;
                      if (oddEven > 0) {
                          classText = "text2";
                      } else {
                          classText = "text1";
                        }
                        %>
                        <tr>
                            <c:if test="${bpcl.status == 'N'}">
                                <td class="<%=classText%>" ><font color="green"><%=i++%></font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="status" id="status" value="<c:out value="${bpcl.status}"/>" />OK</font></td>
                                <td class="<%=classText%>" ><font color="green">
                                        <input type="hidden" name="tripId" id="tripId" value="<c:out value="${bpcl.tripId}"/>" />
                                        <input type="hidden" name="vehicleId" id="vehicleId" value="<c:out value="${bpcl.vehicleId}"/>" />
                                        <c:out value="${bpcl.tripCode}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="vehicleNo" id="vehicleNo" value="<c:out value="${bpcl.vehicleNo}"/>" /><c:out value="${bpcl.vehicleNo}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="accountId" id="accountId" value="<c:out value="${bpcl.accountId}"/>" /><c:out value="${bpcl.accountId}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="dealerName" id="dealerName" value="<c:out value="${bpcl.dealerName}"/>" /><c:out value="${bpcl.dealerName}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="dealerCity" id="dealerCity" value="<c:out value="${bpcl.dealerCity}"/>" /><c:out value="${bpcl.dealerCity}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="transactionDate" id="transactionDate" value="<c:out value="${bpcl.transactionDate}"/>" /><c:out value="${bpcl.transactionDate}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="accountingDate" id="accountingDate" value="<c:out value="${bpcl.accountingDate}"/>" /><c:out value="${bpcl.accountingDate}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="transactionType" id="transactionType" value="<c:out value="${bpcl.transactionType}"/>" /><c:out value="${bpcl.transactionType}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="currency" id="currency" value="<c:out value="${bpcl.currency}"/>" /><c:out value="${bpcl.currency}"/></font></td>
                                <td class="<%=classText%>" align="right"><font color="green"><input type="hidden" name="amount" id="amount" value="<c:out value="${bpcl.amount}"/>" /><c:out value="${bpcl.amount}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="volumeDocNo" id="volumeDocNo" value="<c:out value="${bpcl.volumeDocNo}"/>" /><c:out value="${bpcl.volumeDocNo}"/></font></td>
                                <td class="<%=classText%>" align="right"><font color="green"><input type="hidden" name="amoutBalance" id="amoutBalance" value="<c:out value="${bpcl.amoutBalance}"/>" /><c:out value="${bpcl.amoutBalance}"/></font></td>
                                <td class="<%=classText%>" align="right"><font color="green"><input type="hidden" name="petromilesEarned" id="petromilesEarned" value="<c:out value="${bpcl.petromilesEarned}"/>" /><c:out value="${bpcl.petromilesEarned}"/></font></td>
                                <td class="<%=classText%>" align="right"><font color="green"><input type="hidden" name="odometerReading" id="odometerReading" value="<c:out value="${bpcl.odometerReading}"/>" /><c:out value="${bpcl.odometerReading}"/></font></td>   
                                    </c:if>
                                    <c:if test="${bpcl.status == 'Y'}">
                                <td class="<%=classText%>" ><font color="red"><%=i++%></font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="status" id="status" value="<c:out value="${bpcl.status}"/>" />NOT OK</font></td>
                                <td class="<%=classText%>" ><font color="red">
                                        <input type="hidden" name="tripId" id="tripId" value="<c:out value="${bpcl.tripId}"/>" />
                                        <input type="hidden" name="vehicleId" id="vehicleId" value="<c:out value="${bpcl.vehicleId}"/>" />
                                        <c:out value="${bpcl.tripCode}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="vehicleNo" id="vehicleNo" value="<c:out value="${bpcl.vehicleNo}"/>" /><c:out value="${bpcl.vehicleNo}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="accountId" id="accountId" value="<c:out value="${bpcl.accountId}"/>" /><c:out value="${bpcl.accountId}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="dealerName" id="dealerName" value="<c:out value="${bpcl.dealerName}"/>" /><c:out value="${bpcl.dealerName}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="dealerCity" id="dealerCity" value="<c:out value="${bpcl.dealerCity}"/>" /><c:out value="${bpcl.dealerCity}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="transactionDate" id="transactionDate" value="<c:out value="${bpcl.transactionDate}"/>" /><c:out value="${bpcl.transactionDate}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="accountingDate" id="accountingDate" value="<c:out value="${bpcl.accountingDate}"/>" /><c:out value="${bpcl.accountingDate}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="transactionType" id="transactionType" value="<c:out value="${bpcl.transactionType}"/>" /><c:out value="${bpcl.transactionType}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="currency" id="currency" value="<c:out value="${bpcl.currency}"/>" /><c:out value="${bpcl.currency}"/></font></td>
                                <td class="<%=classText%>" align="right"><font color="red"><input type="hidden" name="amount" id="amount" value="<c:out value="${bpcl.amount}"/>" /><c:out value="${bpcl.amount}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="volumeDocNo" id="volumeDocNo" value="<c:out value="${bpcl.volumeDocNo}"/>" /><c:out value="${bpcl.volumeDocNo}"/></font></td>
                                <td class="<%=classText%>" align="right"><font color="red"><input type="hidden" name="amoutBalance" id="amoutBalance" value="<c:out value="${bpcl.amoutBalance}"/>" /><c:out value="${bpcl.amoutBalance}"/></font></td>
                                <td class="<%=classText%>" align="right"><font color="red"><input type="hidden" name="petromilesEarned" id="petromilesEarned" value="<c:out value="${bpcl.petromilesEarned}"/>" /><c:out value="${bpcl.petromilesEarned}"/></font></td>
                                <td class="<%=classText%>" align="right"><font color="red"><input type="hidden" name="odometerReading" id="odometerReading" value="<c:out value="${bpcl.odometerReading}"/>" /><c:out value="${bpcl.odometerReading}"/></font></td>   
                                    </c:if>

                        </tr>
                        <%index++;%>
                    </c:forEach>
                </table>
                <br>
                <br>
                <br>
                <center>
                    <input type="button" class="button" value="Proceed" onclick="submitPage(this.value)"/>
                </center>
            </c:if>
            <c:if test="${tripWrongDataList != null}">
                <table align="center"> 
                    <tr>
                        <td class="contenthead">S No</td>
                        <td class="contenthead">Trip Code</td>
                        <td class="contenthead">Vehicle No</td>
                        <td class="contenthead">Start Date</td>
                        <td class="contenthead">End Date</td>
                        <td class="contenthead">Started By</td>
                        <td class="contenthead">Ended By</td>
                    </tr>
                     <c:forEach items="${tripWrongDataList}" var="trip">
                        <%
                      oddEven = index % 2;
                      if (oddEven > 0) {
                          classText = "text2";
                      } else {
                          classText = "text1";
                        }
                        %>
                        <tr>
                        <td class="<%=classText%>" ><font color="green"><%=i++%></font></td>
                        <td class="<%=classText%>" ><font color="red"><c:out value="${trip.tripCode}"/></font></td>
                        <td class="<%=classText%>" ><font color="red"><c:out value="${trip.vehicleNo}"/></font></td>
                        <td class="<%=classText%>" ><font color="red"><c:out value="${trip.startDate}"/></font></td>
                        <td class="<%=classText%>" ><font color="red"><c:out value="${trip.endDate}"/></font></td>
                        <td class="<%=classText%>" ><font color="red"><c:out value="${trip.startedBy}"/></font></td>
                        <td class="<%=classText%>" ><font color="red"><c:out value="${trip.endedBy}"/></font></td>
                        </tr>   
                        
                </c:forEach>               
                        <tr>
                            <td colspan="7">Please Check and update the trip start and end date of respective trips.</td>
                        </tr>
            </c:if>   
        </form>
    </body>
</html>