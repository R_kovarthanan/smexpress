
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BUS</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    </head>
    <script language="javascript">
        function submitPage(value)
        {
            if (value=='add')
                {
                    document.desigDetail.action ='/throttle/addDesigna.do';
                }else if(value == 'modify'){
                
                document.desigDetail.action ='/throttle/alterDesig.do';
            }
            document.desigDetail.submit();
        }
    </script>
   
                    
                    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Manage Designation" text="Manage Designation"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="HRMS"/></a></li>
		                    <li class=""><spring:message code="hrms.label.Manage Designation" text="Manage Designation"/></li>
		
		                </ol>
		            </div>
                        </div>
                                    
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
    <body>
        
        <form method="post" name="desigDetail">
            <%@ include file="/content/common/message.jsp" %>

             
 <%
            int index = 0;
            ArrayList DesignaList = (ArrayList) session.getAttribute("DesignaList");
%>    
            <br>
            <c:if test = "${DesignaList != null}" >
                <table class="table table-info mb30 table-hover" id="table" >
                    <thead>
                    <tr >
                        <th  >S.No</div></th>
                        <th  >Designation Name</th>
                        <th  >Description</th>
                        <th  >Status</th>
                        <th  >&nbsp;</th>
                    </tr>
                    </thead>
                    <%

                    %>
                    
                    <c:forEach items="${DesignaList}" var="list"> 	
                        <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr  width="208" height="40" align="center"> 
                            <td ><%=index + 1%></td>
                            <td ><input type="hidden" name="designationId" value='<c:out value="${list.desigId}"/>'> <c:out value="${list.desigName}"/></td>
                            <td ><c:out value="${list.description}"/></td>
                            <td >                                
                                <c:if test = "${list.activeInd == 'Y'}" >
                                    Active
                                </c:if>
                                <c:if test = "${list.activeInd == 'N'}" >
                                    InActive
                                </c:if>
                            </td>
                            <td ><a href="/throttle/viewGrade.do?designationId=<c:out value="${list.desigId}"/>&desigName=<c:out value="${list.desigName}"/>" class="href">Grade</a></td>
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach >
                    
                </table>
            </c:if> 
            <center>
                <br>
                <input type="button" name="add" value="Add" onClick="submitPage(this.name)" class="btn btn-success">
                <c:if test = "${DesignaList != null}" >
                    <input type="button" value="Alter" name="modify" onClick="submitPage(this.name)" class="btn btn-success">
                </c:if>
                <input type="hidden" name="reqfor" value="">
            </center>
            <br>
                 <script language="javascript" type="text/javascript">
             setFilterGrid("table");
         </script>
         <div id="controls">
             <div id="perpage">
                 <select onchange="sorter.size(this.value)">
                     <option value="5"  selected="selected">5</option>
                     <option value="10">10</option>
                     <option value="20">20</option>
                     <option value="50">50</option>
                     <option value="100">100</option>
                 </select>
                 <span>Entries Per Page</span>
             </div>
             <div id="navigation">
                 <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                 <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                 <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                 <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
             </div>
             <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
         </div>
         <script type="text/javascript">
             var sorter = new TINY.table.sorter("sorter");
             sorter.head = "head";
             sorter.asc = "asc";
             sorter.even = "evenrow";
             sorter.odd = "oddrow";
             sorter.evensel = "evenselected";
             sorter.oddsel = "oddselected";
             sorter.paginate = true;
             sorter.currentid = "currentpage";
             sorter.limitid = "pagelimit";
             sorter.init("table", 7);
        </script>
        </form>
    </body>
             </div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>