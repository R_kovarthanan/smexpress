<%-- 
Document   : modifyDesig
Created on : Nov 03, 2008, 6:14:28 PM
Author     : Vijay
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>  

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
 
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="ets.domain.designation.business.DesignationTO" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    

<head>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>PAPL</title>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript">

function submitpage(value)
{
 

var checValidate = selectedItemValidation();
var splt = checValidate.split("-");
if(splt[0]=='SubmitForm' && splt[1]!=0 ){
    

document.modify.action='/throttle/modifyDesig.do';
document.modify.submit();
}
}

function setSelectbox(i)
{
var selected=document.getElementsByName("selectedIndex") ;
selected[i].checked = 1;
}

function selectedItemValidation(){
var index = document.getElementsByName("selectedIndex");
var desigName = document.getElementsByName("desigNames");
var chec=0;
var mess = "SubmitForm";
for(var i=0;(i<index.length && index.length!=0);i++){
if(index[i].checked){
chec++;
if(isEmpty(desigName[i].value)){
alert ("Enter The Designation Name");
desigName[i].focus();
mess = "";
break;
}else if(isChar(desigName[i].value)){
alert (" Designation Name must be in Character");
desigName[i].focus();
mess = "";
break;
}
}
}
if(chec == 0){
alert("Please Select Any One And Then Proceed");
desigName[0].focus();
//break;
}
return mess+"-"+chec;
}

function isChar(s){
if(!(/^-?\d+$/.test(s))){
return false;
}
return true;
}
</script>

</head>


<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->

                    
                    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.AlterDesignation" text="AlterDesignation"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="HRMS"/></a></li>
		                    <li class=""><spring:message code="hrms.label.AlterDesignation" text="AlterDesignation"/></li>
		
		                </ol>
		            </div>
        </div>
                                    
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
                    
               
<body onload="document.modify.desigNames[0].focus();">
<form method="post" name="modify"> 
<%--@ include file="/content/common/path.jsp" %>
<%@ include file="/content/common/message.jsp" --%>
<%@ include file="/content/common/message.jsp" %>
<br>
 <%
    int index = 0;
ArrayList DesignaList = (ArrayList) request.getAttribute("DesignaList");
System.out.println("size in jsp="+DesignaList.size() );
%>        

<table class="table table-info mb30 table-hover" id="table" >
    
<c:if test = "${DesignaList != null}" >

 
<thead>

<tr id="tableDesingTH ">
<th  height="30">Designation Id</th> 
<th  height="30">Designation Name </th> 
<th  height="30">Desription</th> 
<th  height="30">Status</th>
<th  height="30">CheckBox</th>
</tr>
</thead>
</c:if>

<c:if test = "${DesignaList != null}" >
<c:forEach items="${DesignaList}" var="Desig"> 		
<%

String classText = "";
int oddEven = index % 2;
if (oddEven > 0) {
classText = "text2";
} else {
classText = "text1";
}
%>
<tr>
<td  height="30"><input type="hidden" name="desigIds" value='<c:out value="${Desig.desigId}"/>'> <div align="center"><c:out value="${Desig.desigId}"/></div> </td>                                           
<td  height="30"><input type="text" class="form-control" style="width:250px;height:40px" name="desigNames" value="<c:out value="${Desig.desigName}"/>" onchange="setSelectbox(<%= index %>)"></td>
<td  height="30"><input type="text" class="form-control" style="width:250px;height:40px" name="descriptions" value="<c:out value="${Desig.description}"/>" onchange="setSelectbox(<%= index %>)"></td>
<td height="30" > <div align="center"><select name="activeInds" class="form-control" style="width:250px;height:40px" onchange="setSelectbox(<%= index %>)" style="width:125px">
<c:choose>
<c:when test="${Desig.activeInd == 'Y'}">
<option value="Y" selected>Active</option>
<option value="N">InActive</option>
</c:when>
<c:otherwise>
<option value="Y">Active</option>
<option value="N" selected>InActive</option>
</c:otherwise>
</c:choose>
</select>
</div> 
</td>
<td width="77" height="30" ><input type="checkbox" name="selectedIndex" value='<%= index %>'></td>
</tr>
<%
index++;
%>
</c:forEach >
</c:if> 
</table>
<center>

<input type="button" name="save" value="Save" onClick="submitpage(this.name)" class="btn btn-success" />
<input type="hidden" name="reqfor" value="designa" />
</center>
<script language="javascript" type="text/javascript">
             setFilterGrid("table");
         </script>
         <div id="controls">
             <div id="perpage">
                 <select onchange="sorter.size(this.value)">
                     <option value="5"  selected="selected">5</option>
                     <option value="10">10</option>
                     <option value="20">20</option>
                     <option value="50">50</option>
                     <option value="100">100</option>
                 </select>
                 <span>Entries Per Page</span>
             </div>
             <div id="navigation">
                 <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                 <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                 <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                 <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
             </div>
             <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
         </div>
         <script type="text/javascript">
             var sorter = new TINY.table.sorter("sorter");
             sorter.head = "head";
             sorter.asc = "asc";
             sorter.even = "evenrow";
             sorter.odd = "oddrow";
             sorter.evensel = "evenselected";
             sorter.oddsel = "oddselected";
             sorter.paginate = true;
             sorter.currentid = "currentpage";
             sorter.limitid = "pagelimit";
             sorter.init("table", 7);
        </script>
        
</form>
</body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>

