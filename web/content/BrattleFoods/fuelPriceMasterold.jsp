<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<script language="javascript" src="/throttle/js/validate.js"></script>
<!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>




<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        $(".datepicker").datepicker({
            changeMonth: true, changeYear: true
        });
    });
</script>

<script>
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#cityName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCityNameList.do",
                    dataType: "json",
                    data: {
                        cityName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#cityName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var Name = ui.item.Name;
                var Id = ui.item.Id;
                $itemrow.find('#cityId').val(Id);
                $itemrow.find('#cityName').val(Name);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var itemId = item.Id;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });
    
    
    function resetCityId(){
        if(document.getElementById('cityName').value == ''){
           document.getElementById('cityId').value = '' ;
        }
    }
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.FuelPriceMaster" text="FuelPriceMaster"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.Fuel" text="Fuel"/></a></li>
                    <li class=""><spring:message code="hrms.label.FuelPriceMaster" text="FuelPriceMaster"/></li>		
                </ol>
            </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
    <body >
        <% String menuPath = "Fuel  >>  Manage Fuel Price";
       request.setAttribute("menuPath", menuPath);
        %>
        
        <form name="fuleprice"  method="post" >
            
            <table class="table table-info mb30 table-hover" id="bg" >
                <thead>
                <tr align="center">
                    <th colspan="4" align="center"  height="30"><div >New Fuel Price</div></th>
                </tr>
                </thead>
                <tr>
                    <td  height="30"><font color="red">*</font>Effective Date</td>
                    <td > <input type="text" class="datepicker" style="width:250px;height:40px" id="effectiveDate" name="effectiveDate" value="" >
                
                    <td  height="30"><font color="red">*</font>Fuel Price INR:</td>
                    <td  height="30"><input type="text" id="fuelPrice" name="fuelPrice" class="form-control" style="width:250px;height:40px"></td>
                </tr>
                <tr>
                    <td  height="30"><font color="red">*</font>Fuel Type:</td>
                    <td><select name="fuelType" id="fuelType" class="form-control" style="width:250px;height:40px">
                            <option value="0">--Select--</option>
                            <c:if test="${fuleTypeList != null}">
                                <c:forEach items="${fuleTypeList}" var="ful">
                                    <option value='<c:out value="${ful.fuelTypeId}"/>'><c:out value="${ful.fuelTypeName}"/></option>
                                </c:forEach>
                            </c:if>
                        </select>
                   </td>
                   
                    <td  height="30"><font color="red">*</font>Fuel Unit:</td>
                    <td  height="30">
                        
                        <select name="fuelUnite" id="fuelUnite" class="form-control" style="width:250px;height:40px">
                            <option value="litre">Litres</option>
                            <option value="kg">Kg</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td  height="30"><font color="red">*</font>City</td>
                    <td><input type="hidden" name="cityId" id="cityId"  value=""/><input type="text" class="form-control" style="width:250px;height:40px" name="cityName" id="cityName"  onchange="resetCityId();"/></td>

               
                </tr>
                <tr>
                      <td colspan="4" align="center">
			<input type="button" class="btn btn-success"  value="Save"  onClick="submitPage();">
			<input type="button" class="btn btn-success"  value="Search"  onClick="searchPage();">
                      </td>
                  </tr>    
                
            </table>

            
            
            <table align="center">
                <tr >
                    <td><B>Current Average Fuel Cost&nbsp;=&nbsp;</B></td>
                <br>
                
                    <td><c:out value="${avgCurrentFuelCost}"/></td>
                </tr>
            </table>
                
                <br>
           <table class="table table-info mb30 table-hover" id="table" >
                <thead>
                <tr height="30">
                <th>S.No</th>
                <th>City Name</th>
                <th>Effective Date From</th>
                <th>Fuel Price (INR)</th>
                <th>Change in Price (INR)</th>

                </tr>
                </thead>
                <tbody>
                    <% int sno = 0;%>
                    <c:if test = "${viewfuelPriceMaster != null}" >
                        <%
                                            sno++;
                                           String className = "text1";
                                           if ((sno % 1) == 0) {
                                               className = "text1";
                                           } else {
                                               className = "text2";
                                           }
                        %>
                        <c:forEach items="${viewfuelPriceMaster}" var="fuelPriceMaster">
                            <tr height="30">
                                <td   height="30"> <%= sno++%></td>
                                <td  align="left" ><c:out value="${fuelPriceMaster.cityName}"/></td>
                                <td  align="left" ><c:out value="${fuelPriceMaster.effectiveDate}"/></td>
                                <td  align="left" ><c:out value="${fuelPriceMaster.fuelPrice}"/></td>
                                <td  align="left" ><c:out value="${fuelPriceMaster.priceDiff}"/></td>
                            </tr>
                        </c:forEach>
                    </c:if>

                </tbody>
            </table>
            <script language="javascript" type="text/javascript">
	                        setFilterGrid("table");
	                    </script>
	                    <div id="controls">
	                        <div id="perpage">
	                            <select onchange="sorter.size(this.value)">
	                                <option value="5" selected="selected">5</option>
	                                <option value="10">10</option>
	                                <option value="20">20</option>
	                                <option value="50">50</option>
	                                <option value="100">100</option>
	                            </select>
	                            <span>Entries Per Page</span>
	                        </div>
	                        <div id="navigation">
	                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
	                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
	                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
	                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
	                        </div>
	                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
	                    </div>
	                    <script type="text/javascript">
	                        var sorter = new TINY.table.sorter("sorter");
	                        sorter.head = "head";
	                        sorter.asc = "asc";
	                        sorter.desc = "desc";
	                        sorter.even = "evenrow";
	                        sorter.odd = "oddrow";
	                        sorter.evensel = "evenselected";
	                        sorter.oddsel = "oddselected";
	                        sorter.paginate = true;
	                        sorter.currentid = "currentpage";
	                        sorter.limitid = "pagelimit";
	                        sorter.init("table", 1);
                    </script>

            <script type="text/javascript">
                function submitPage() {
                    if (document.getElementById('effectiveDate').value == '') {
                        alert("Please select date");
                        document.getElementById('effectiveDate').focus();
                    } else if (document.getElementById('fuelPrice').value == '') {
                        alert("Please enter fuel price");
                        document.getElementById('fuelPrice').focus();
                    } else if (document.getElementById('cityId').value == '') {
                        alert("Please select city");
                        document.getElementById('cityId').focus();
                    } else {
                        document.fuleprice.action = '/throttle/saveFuelPriceMaster.do';
                        document.fuleprice.submit();
                    }
                }
            </script>

        </form>
    </body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>

