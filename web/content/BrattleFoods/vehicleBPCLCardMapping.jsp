<%@ include file="../common/NewDesign/header.jsp" %>
	<%@ include file="../common/NewDesign/sidemenu.jsp" %> 
        
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>
<script>



    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#vehicleNo').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getVehicleNoBPCLCardNo.do",
                    dataType: "json",
                    data: {
                        vehicleNo: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if(items == ''){
                        alert("Invalid Vehicle No");
                        $('#vehicleNo').val('');
                        $('#vehicleId').val('');    
                        $('#vehicleNo').fous();
                        }else{
                        response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.vehicleNo;
                var id = ui.item.vehicleId;
                var bpclCardNo = ui.item.bpclTransactionId;
                $('#vehicleNo').val(value);
                $('#vehicleId').val(id);
                 $('#bpclTransactionId').val(bpclCardNo);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.vehicleNo;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });




    //Update Vehicle Planning
    function setValues(vehicleId, vehicleNo, bpclTransactionId,sno) {
        var count = parseInt(document.getElementById("count").value);
        //document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("vehicleId").value = vehicleId;
        document.getElementById("vehicleNo").value = vehicleNo;
        document.getElementById("vehicleNo").readOnly = true;
        document.getElementById("bpclTransactionId").value = bpclTransactionId;
    }

    //savefunction
    function submitPage() {
        if (document.getElementById("vehicleNo").value == '' || document.getElementById("vehicleId").value == '') {
            alert("Please Select Valid Vehicle No");
            document.getElementById("vehicleNo").focus();
        } else if (document.getElementById("bpclTransactionId").value == '') {
            alert("Please Enter BPCL Card No");
            document.getElementById("bpclTransactionId").focus();
        } else {
            $("#save").hide();
            document.vehicleBPCLCardMapping.action = '/throttle/saveVehicleBPCLCardMapping.do';
            document.vehicleBPCLCardMapping.submit();
        }
    }
</script>
<!--<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>-->
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.VehicleBPCLMapping" text="VehicleBPCLMapping"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
		                    <li class=""><spring:message code="hrms.label.VehicleBPCLMapping" text="VehicleBPCLMapping"/></li>
		
		                </ol>
		            </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
    
    <body onload="document.vehicleBPCLCardMapping.vehicleNo.focus()">
        <form name="vehicleBPCLCardMapping"  method="post" >
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <br>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>

            <table class="table table-info mb30 table-hover" id="bg" >	
			
                <input type="hidden" name="stChargeId" id="stChargeId" value=""  />
                <thead>
                <tr>
                    <th  colspan="4" >Vehicle BPCL Card Mapping</th>
                </tr>
                </thead>
                <tr>
                    <td >&nbsp;&nbsp;<font color="red">*</font>Vehicle No</td>
                    <td ><input type="hidden" name="vehicleId" id="vehicleId" class="form-control" style="width:250px;height:40px"  ><input type="text" name="vehicleNo" id="vehicleNo" class="form-control" style="width:250px;height:40px" ></td>
                    <td >&nbsp;&nbsp;<font color="red">*</font>BPCL Card No</td>
                    <td ><input type="text" name="bpclTransactionId" id="bpclTransactionId" class="form-control" style="width:250px;height:40px"></td>
                </tr>
                <tr>
                    <td  colspan="4" align="center"><input type="button" class="btn btn-success"  value="Save" id="save" onClick="submitPage();"   /></td>
                </tr>
            </table>
            <br>
            
           <table class="table table-info mb30 table-hover " id="table" >	
			<thead>
                
                    <tr >
                        <th>S.No</th>
                        <th>Vehicle No </th>
                        <th>BPCL Card No</th>
                        <th>Select</th>
                    </tr>
                </thead>
                <tbody>
                    <% int sno = 0;%>
                    <c:if test = "${vehicleBPCLCardMapping != null}">
                        <c:forEach items="${vehicleBPCLCardMapping}" var="vehicleCard">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td   align="left"> <%= sno + 1%> </td>
                                <td   align="left"><input type="hidden" name="vehicleIds" id="vehicleIds" value="<c:out value="${vehicleCard.vehicleId}" />"/> <c:out value="${vehicleCard.regNo}" /></td>
                                <td   align="left"><input type="hidden" name="bpclCardIds" id="bpclCardIds" value="<c:out value="${vehicleCard.bpclTransactionId}" />"/>  <c:out value="${vehicleCard.bpclTransactionId}" /></td>
                                <td > <input type="checkbox" id="edit<%=sno%>" onclick="setValues('<c:out value="${vehicleCard.vehicleId}" />', '<c:out value="${vehicleCard.regNo}" />', '<c:out value="${vehicleCard.bpclTransactionId}" />','<%=sno%>');" /></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>
            <input type="hidden" name="count" id="count" value="<%=sno%>" />
            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");</script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        </form>
    </body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>