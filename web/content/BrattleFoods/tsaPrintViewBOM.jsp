<%-- 
    Document   : tsaPrintView
    Created on : Mar 5, 2015, 11:38:59 AM
    Author     : srinientitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
    <head>
          <script language="javascript" src="/throttle/js/validate.js"></script>
          <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
          <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
          <%@ page import="java.util.* "%>
          <%@ page import=" javax. servlet. http. HttpServletRequest" %>
          <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
          <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
          <script language="javascript" src="/throttle/js/validate.js"></script>
          <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
          <style type="text/css" title="currentStyle">
          @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
          </style>
          <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
          <!-- jQuery libs -->
          <script  type="text/javascript" src="/throttle/js/jquery-1.7.1.js"></script>
          <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
          <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
          <!-- Our jQuery Script to make everything work -->
         <script  type="text/javascript" src="js/jq-ac-script.js"></script>
           <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
          <script src="/throttle/js/jquery.ui.core.js"></script>
          <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    </head>
    <body>
        <form name="tsaPrint" method="post">
            <br>
           
            <div id="printDiv">
            <table align="center" width="100%">
                <tr><td><font size="2"><center><br/><u>TT AVIATION HANDLING SERVICES PVT LTD MUMBAI.</u><br>
                                </font></td></tr>
            </table>
            <br>
            <table align="center" width="100%">
                <tr>
                    <td>
                       <table align="center" width="100%">
                            <tr><td><b>From</b></td></tr>
                            <tr><td>Superintendent Of Customs <br/>Incharge Batch<br/>Air Cargo Complex, Sahar,<br/> <c:out value="${fromAddress}"/> - 400 099</td></tr>
                        </table> 
                    </td>
                    <td><table align="left" width="100%">
                    <tr><td><b>To</b></td></tr>
                    <tr><td>I.F.O Of Customs <br/><b><c:out value="${toAddress}"/></b></td></tr>
                    </table></td>
                    <td>&nbsp;
                    <table align="center"  width="100%" style="text-align: center" >
                        <tr><td colspan="2">&nbsp;</td></tr>
<!--                        <tr><td align="left"><font size="2">Flight No:<b>&nbsp;&nbsp;<c:out value="${flightNo}"/></b></font></td>    </tr>
                            <tr><td align="left"><font size="2">Date:<b>&nbsp;&nbsp;<c:out value="${flightDate}"/></b></font></td>    </tr>
                            <tr><td align="left"><font size="2">
                                        <c:choose>
                                            <c:when test="${shipType ==1}">
                                                IGM No :<b><c:out value="${consignmentEgmNo}"/></b>
                                            </c:when>
                                            <c:otherwise>
                                                EGM No :<b><c:out value="${consignmentEgmNo}"/></b>
                                            </c:otherwise>
                                        </c:choose></font></td>    </tr>
                            <tr><td align="left"><font size="2">TP No:<b><c:out value="${consignmentTpNo}"/></b></font></td>    </tr>-->
                    </table>
                    </td>
                </tr>
            </table>
                            <br>
                            
                            <table align="center" width="100%">
                                <tr>
                                    <td>
                                      Sr. No: _____</td>
                                    <td>Dated : <c:out value="${flightDate}"/></td>
                                    <td>T/P Application in terms of PN No.33/2012 Dated <c:out value="${flightDate}"/>(4 Copies)<br/>(IGM wise for One Destination)</td>
                                </tr>
                               

                            </table>

            
         <c:set var="totalPackages" value="0" />
        <c:if test="${tsiViewList != null}">
       
         <table align="center" border="2" width="100%" height="">
                <thead>
                    <tr height="10">
                        <th align="center">S.No</th>
                        <th align="center">MAWB</th>
                        <th align="center">HAWB</th>
                        <th align="center">Flight No.& Date</th>
                        <th align="center">IGM No.& Date</th>
                        <th align="center">Name & Address of shipper</th>
                        <th align="center">Name & Address of Importer</th>
                        <th align="center">Country of Origin/Despatch</th>
                        <th align="center">Description Of Goods</th>
                        <th align="center">No Of Pkgs.</th>
                        <th align="center">Gross Wt</th>
                        <th align="center">The Destination</th>
                        <th align="center">Registration No.Of Vehicle</th>
                    </tr>
                </thead>
                <% int index = 1;%>
               
                <c:forEach items="${tsiViewList}" var="tsiViewList">
                    <tr height="9">
                        <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                        %>
                        <td class="<%=classText%>"  ><%=index%></td>
                        <td class="<%=classText%>"   ><c:out value="${tsiViewList.consignmentAwbNo}"/></td>
                        <td class="<%=classText%>"   ><c:out value="${tsiViewList.hawbNo}"/></td>
                        <td class="<%=classText%>"   ><c:out value="${tsiViewList.flightNo}"/>&nbsp;</td>
                        <td class="<%=classText%>"   ><c:out value="${tsiViewList.igmNo}"/>&nbsp;</td>
                        <td class="<%=classText%>"   ><c:out value="${tsiViewList.consignorName}"/></td>
                        <td class="<%=classText%>"   ><c:out value="${tsiViewList.consigneeName}"/></td>
                        <td class="<%=classText%>"   ><c:out value="${tsiViewList.awbOriginName}"/></td>
                         <td class="<%=classText%>"   ><c:out value="${tsiViewList.commodity}"/></td> 
                        <td class="<%=classText%>"   >
                          <c:if test="${tsiViewList.shipmentType == '1' }">
                            <c:out value="${tsiViewList.totalPackages}"/>
                             <c:set var="totalPackages" value="${totalPackages + tsiViewList.totalPackages}"/>
                            </c:if>
                            <c:if test="${tsiViewList.shipmentType == '2' }">
                            <c:out value="${tsiViewList.receivedPackages}"/>/<c:out value="${tsiViewList.totalPackages}"/>
                             <c:set var="totalPackages" value="${totalPackages + tsiViewList.receivedPackages}"/>
                            </c:if>
                        </td>
                            <c:set var="dest" value="${tsiViewList.awbDestinationName}"/>
                            
                            <input type="hidden" id="totalPackages" value="0"/>
                         <td class="<%=classText%>"   >
                          <c:if test="${tsiViewList.shipmentType == '1' }">
                            <c:out value="${tsiViewList.totalWeight}"/>
                             <c:set var="totalWeight" value="${totalWeight + tsiViewList.totalWeight}"/>
                            </c:if>
                            <c:if test="${tsiViewList.shipmentType == '2' }">
                                 <c:set var="totalWeight" value="${totalWeight + tsiViewList.receivedWeight}"/>
                            <c:out value="${tsiViewList.receivedWeight}"/>/<c:out value="${tsiViewList.receivedWeight}"/>
                            </c:if>
                        </td>
                        <td class="<%=classText%>"   ><c:out value="${tsiViewList.awbDestinationName}"/>  <c:set var="totalCost" value="${totalCost + tsiViewList.totalCost}"/></td>
                        <td class="<%=classText%>"   >&nbsp;</td>                    

                        <%++index;%>
                    </tr>
                </c:forEach>
                    <input type="hidden" name="rowNo" id="rowNo" value="<%=index%>"/>
            </table>
            <br><br>
            <table  width="100%" >
                <tr>
                    <td>Total weight : <b><c:out value="${totalWeight}"/> Kgs.</b></td>
                    <td>&nbsp;</td>
                    <td align="right">TT Aviation Handling Services Pvt Ltd</td>
                </tr>
                <tr>
                    <td>Amount<b> Rs. <c:out value="${totalCost}"/> </b>to be debited on the running Bond</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
            </table>
            <table align="center" width="100%" cellspacing="10">
                <tr>
                    <td>Supervised the loading of  <b><c:out value="${totalPackages}"/>Pkgs</b> 
                        <script>
                            document.getElementById("noOfPackage").innerHTML=document.getElementById("totalPackages").value;
                        </script>
                    </td>
                </tr>
                <tr>
                    <td>Under Transhipment Application No.<c:out value="${consignmentTpNo}"/> </td><td align="right">Authorised signature.</td>
                </tr>
                <tr>
                    <td>Destination To <c:out value="${transitCode}"/> </td>
                </tr>
                <tr>
                    <td>On the Vehicle No :<b><c:out value="${fleetCode}"/></b></td></td>
                </tr>
                <c:set var="count" value="1" scope="page" />
                <c:forEach items="${tsiViewList}" var="tsiViewList">
                    <c:if test="${count == 1}">
                <tr>
                    <td colspan="2">The Container on the vehcile has been sealed with Custom Bottle Seal No: ACC MUMBAI<label id="otlNoDisplay" style="display: none">0</label>
                        <input type="text" name="otlNo" id="otlNo"  style="width: 120px;" value="<c:out value="${tsiViewList.otlNo}"/>"  />
                        <input type="button" name="EditOtlNo" value="EditOtlNo"  id="EditOtlNo" onclick="showOtlEditOption()" /></td>
                        
                </tr><input type="hidden" name="consignmentOrderId" id="consignmentOrderId" value="<c:out value="${tsiViewList.consignmentOrderId}"/>"/>
               <c:set var="count" value="${count + 1}" scope="page" />
                    </c:if>
                </c:forEach>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td align="center"><b>Name and Signature of Customs Officer (Mumbai)</b></td>
                </tr>
                <tr>
                    <td>
                        Checked Custom Entitle Bottle Seal No <u>&emsp;&emsp;</u>on the vehicle No &emsp;&emsp;
                    </td>
                    <td align="center"><b>	&emsp;&emsp;&emsp;</b></td>
                </tr>
                <tr>
                    <td>
                        Packages covered by these application have found in sound condition
                    </td>
                    <td align="right">&nbsp;</td>
                </tr>
                <tr>
                    <td>
                       &nbsp;
                    </td>
                    <td align="center"><b>	Name and Signature of Customs Officer (<c:out value="${toAddress}"/>)</b></td>
                </tr>
            </table>
            </div>
            <br>
            <center>
               <input type="button" class="button" name="print" id="print" value="Print" onclick="printPage()"/>&nbsp;&nbsp;&nbsp;<input type="button" class="button" id="close" name="Close"  value="Close" onclick="closeWindow()"/>
        </center>

        </c:if>
       
                        <script type="text/javascript">
                              function printPage()
                        {       
                         document.getElementById("print").style.display='none';
						                                 document.getElementById("close").style.display='none';
			                                 document.getElementById("EditOtlNo").style.display='none';
                            var DocumentContainer = document.getElementById('printDiv');
                            var WindowObject = window.open('', "TSA Print View", 
                                "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                            WindowObject.document.writeln(DocumentContainer.innerHTML);
                            WindowObject.document.close();
                            WindowObject.focus();
                            WindowObject.print();
                            //WindowObject.close();   
                        }            
            
            
                            
                            
                            function closeWindow(){
                             window.close();
                            }
                            function showOtlEditOption(){
                                var EditOtlNo=document.getElementById("EditOtlNo").value;
                                if(EditOtlNo=="EditOtlNo"){
                                document.getElementById('otlNo').readOnly = false;
                                document.getElementById("EditOtlNo").value="InsertOltNo";
                                }
                                if(EditOtlNo=="InsertOltNo"){
                                    var consignmentOrderId = document.getElementById("consignmentOrderId").value;
                                    var otlNo = document.getElementById("otlNo").value;
                                     $.ajax({
                                            url: '/throttle/saveOtlSealNo.do',
                                            data: {otlNo: otlNo, consignmentOrderId: consignmentOrderId},
                                            dataType: 'json',
                                            success: function(data) {
                                                if (data == '' || data == null) {
                                                    alert('');
                                                } else {
                                                    $.each(data, function(i, data) {
                                                        document.getElementById("otlNo").value = data.Name;
                                                    });



                                                }
                                            }
                                        });
                                        document.getElementById("EditOtlNo").style.display='none';
                                        document.getElementById('otlNo').style.display='none';
                                        $("#otlNoDisplay").text(document.getElementById('otlNo').value);
                                         $('#otlNoDisplay').show();
                                }
                                


                            }

                        </script>
                                <input type="hidden" name="otlHidden" id="otlHidden" value="0"/>
                                 </form>
    </body>

</html>


