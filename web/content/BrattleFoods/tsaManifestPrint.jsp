<%-- 
    Document   : tsaManifestPrint
    Created on : Mar 5, 2015, 10:07:11 AM
    Author     : srinientitle
--%>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
 <%@ include file="/content/common/NewDesign/header.jsp" %>
	<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <style type="text/css" title="currentStyle">
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->

        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
        <script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

        <script src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript">
        $(document).ready(function() {
          $("#datepicker").datepicker({
              showOn: "button",
              buttonImage: "calendar.gif",
              buttonImageOnly: true

          });
        });

        $(function() {
          //alert("cv");
          $(".datepicker").datepicker({
              /*altField: "#alternate",
               altFormat: "DD, d MM, yy"*/
              changeMonth: true, changeYear: true
          });
        });

</script> 
        <script type="text/javascript">    

    

            function searchPage(){
                document.tsaprintSearch.action = "/throttle/tsaManifestPrint.do";
                document.tsaprintSearch.submit();
            }
        </script>
    </head>
     <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Company" text="Company"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.TSA/Manifest Print" text="TSA/Manifest Print"/></a></li>
		                    <li class=""><spring:message code="hrms.label.CBT Booking" text="CBT Booking"/></li>
		
		                </ol>
		            </div>
       			</div>
        
             <div class="contentpanel">
             <div class="panel panel-default">
             <div class="panel-body">
    <body>
        <form name="tsaprintSearch" method="post" >
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <br>
            <br>
            <table class="table table-info mb30 table-hover" id="report" > 
            <!--<table width="90%" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">-->
<!--                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Export" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>-->
                    <thead>
		<tr>
		    <th colspan="4" height="30" >TSA/Manifest Print</th>
		</tr>
                    </thead>
                    
                <tr id="exp_table" >
                    <td colspan="6"  align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <div id="first">
                                <table class="table table-info mb30 table-hover"  >
                                <!--<table width="100%" cellpadding="0" cellspacing="2" border="0" align="center" class="table4">-->
                                    <tr>
                                        <td>Select Printing Type</td>
                                        <td>
                                            <select name="printType" id="printType" class="form-control" style="width:240px;height:40px"  >
                                                <option value="1" >TSA PRINT</option>
                                                <option value="2" >MANIFEST PRINT</option>
                                            </select>
                                    <script>
                                        var printType='<c:out value="${printType}"/>';
                                        if(printType != null & printType != ""){                                      
                                            document.getElementById("printType").value=printType;
                                        }
                                    </script>
                                        </td>

                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker form-control" style="width:240px;height:40px"   onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>

                                    </tr>
                                    <tr>
                                        <td >Customs No</td>
                                        <td>
                                            <input type="text" class="form-control" style="width:240px;height:40px"  name="tsmanifestCode"   id="tsmanifestCode" style="width: 110px" value="<c:out value="${tsaManifestCode}"/> "/>
                                        </td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker form-control" style="width:240px;height:40px"  onclick="ressetDate(this);" value="<c:out value="${toDate}"/>"></td>

                                    </tr>
                                    <tr align="center">
                                    <center>
                                        <td colspan="3"><input type="button" class="btn btn-success"   value="Search" onclick="searchPage()"></td>
                                    </center>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>
            <c:if test = "${tsaPrintList != null}" >
                <br><br>
                <table width="100%" align="center" border="0" id="table" class="sortable"  style="width:1150px;" >
                    <thead>
                        <tr height="40">
                            <th><h3>Sn No</h3></th>
                            <th><h3>Customs No</h3></th>
                        <th><h3>From Address </h3></th>
                        <th><h3>To Address </h3></th>
                        <th><h3>Fleet Details</h3></th>
                        <th><h3>Entry Date</h3></th>
                        <th><h3>TP No</h3></th>
                        <th><h3>EGM/IGM  No</h3></th>
                        <th><h3>Remark</h3></th>
                        </tr>                        
                    </thead>
                    <% int sno = 1;%>
                    <tbody>
                        <c:forEach items="${tsaPrintList}" var="tsaPrintList">
                            <tr height="30">
                        <td align="left" ><%=sno%></td>
                        <td align="left" ><a href="#" onclick="viewTsaPrintPage('<c:out value="${tsaPrintList.customsId}"/>');"><c:out value="${tsaPrintList.customsCode}"/></a></td>
                        <td align="left" ><c:out value="${tsaPrintList.fromAddress}"/></td>
                        <td align="left" ><c:out value="${tsaPrintList.toAddress}"/></td>
                        <td align="left" ><c:out value="${tsaPrintList.fleetDetails}"/></td>
                        <td align="left" ><c:out value="${tsaPrintList.customsDate}"/></td>
                        <td align="left" ><c:out value="${tsaPrintList.consignmentTpNo}"/></td>
                        <td align="left" ><c:out value="${tsaPrintList.consignmentEgmNo}"/></td>
                        <td align="left" ><c:out value="${tsaPrintList.customsRemark}"/></td>
                        <%++sno;%>
                        </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>
            <c:if test = "${manifestPrintList != null}" >
                <br><br><br>
                <table width="100%" align="center" border="0" id="table" class="sortable"  style="width:1150px;" >
                    <thead>
                        <tr height="40">
                        <th><h3>Sno</h3></th>
                        <th><h3>Manifest No</h3></th>
                        <th><h3>Manifest Truck</h3> </th>
                        <th><h3>Flight No </h3></th>
                        <th><h3>Manifest Date</h3></th>
                        <th><h3>Loading Point</h3></th>
                        <th><h3>UnLoading Point</h3></th>
                        <th><h3>Consignment EGM/IGM No</h3></th>
                        <th><h3>Consignment ATD No</h3></th>
                        </tr>
                        
                    </thead>
                    <% int sno1 = 1;%>
                    <tbody>
                        <c:forEach items="${manifestPrintList}" var="manifestPrintList">
                            <tr height="30">
                        <td align="left" ><%=sno1%></td>
                        <td align="left" ><a href="#" onclick="viewManifestPrintPage('<c:out value="${manifestPrintList.manifestId}"/>');"><c:out value="${manifestPrintList.manifestCode}"/></a></td>
                        <td align="left" ><c:out value="${manifestPrintList.operatorName}"/></td>
                        <td align="left" ><c:out value="${manifestPrintList.flightNo}"/></td>
                        <td align="left" ><c:out value="${manifestPrintList.manifestDate}"/></td>
                        <td align="left" ><c:out value="${manifestPrintList.lodingPoint}"/></td>
                        <td align="left" ><c:out value="${manifestPrintList.unLodingPoint}"/></td>
                        <td align="left" ><c:out value="${manifestPrintList.consignmentNoteEmno}"/></td>
                        <td align="left" ><c:out value="${manifestPrintList.consignmentNoteAtdNo}"/></td>
                        </tr>
                        <%++sno1;%>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>
        </form>
    </body>
    <script type="text/javascript">
            function viewManifestPrintPage(manifestId){
            window.open('/throttle/getManifestPrintDetails.do?manifestId='+manifestId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes, menubar = yes');
            }
            function viewTsaPrintPage(customsId){            
                window.open('/throttle/getTsaPrintDetails.do?customsId='+customsId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes, menubar = yes');
            }
    </script>
</div>
	</div>
        </div>
        <%@ include file="/content/common/NewDesign/settings.jsp" %>  

