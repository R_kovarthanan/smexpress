<!-- 
    Document   : tsageneration
    Created on : 12 Jun, 2015, 7:04:33 PM
    Author     : gopi
-->

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<%@page import="java.text.SimpleDateFormat"%>
<html>
    <head>
        <%@page import="javax.servlet.http.HttpServletRequest"%>
        <%@page import="java.util.*"%>
        <%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
        <script language="javascript" type="text/javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.7.1.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
        <!-- Our jQuery Script to make everything work -->
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script>
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true
                });
            });

            function submitPage() {
                var loadDate = document.getElementById("loadDate").value;
                if (loadDate.length > 0 && loadDate != "") {
                    document.searchtsa.action = "/throttle/handleTsaGenView.do";
                    document.searchtsa.submit();
                } else {
                    alert("Select Load Date First For Search");
                }
            }

            function viewConsignmentDetails(orderId) {
                window.open('/throttle/getConsignmentDetails.do?consignmentOrderId=' + orderId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
            }

        </script>


    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.TSA Generation" text="TSA Generation"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.CBT Booking" text="CBT Booking"/></a></li>
                <li class=""><spring:message code="hrms.label.TSA Generation" text="TSA Generation"/></li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body>
                    <form name="searchtsa" method="post">
                        <%
                            String menupath = "Operation >> Booking-O/P >> TSA Generation";
                            request.setAttribute("menupath", menupath);
                        %>
                        <!--<br>-->
                        <%@include file="/content/common/message.jsp" %>
                        <%
                                    Date today = new Date();
                                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                                    String startDate = sdf.format(today);
                        %>



                        <table  align="center" class="table table-info mb30 table-hover">
                            <thead>
                                <tr>
                                    <th colspan="4" height="30" >TSA Generation</th>
                                </tr>
                            </thead>
                            <tr>
                                <td ><font color="red">*</font>Expected Load From Date</td>
                                <td><input type="text" class="datepicker" name="loadDate" id="loadDate"  value="<c:out value="${loadDate}"/>" class="datepicker form-control" style="width:240px;height:40px"></td>
                                <td ><font color="red">*</font>To Date</td>
                                <td><input type="text" class="datepicker" name="loadToDate" id="loadToDate"  value="<c:out value="${loadToDate}"/>" class="datepicker form-control" style="width:240px;height:40px"></td>

                            </tr>
                        </table>
                        <center>
                            <input type="button" value="Search" class="btn btn-success" name="search" id="search" onClick="submitPage();">&nbsp;&nbsp;
                        </center>
                        <br>

                        <script language="javascript" type="text/javascript">
                            function checkSelectedItem(sno) {
                            }
                        </script>

                        <c:if test = "${tsaManifestList != null}" >
                            <table  align="center"  id="table1" class="table table-info mb30 table-hover sortable">
                                <thead>
                                    <tr height="40">
                                        <th>S.No</th>
                                        <th>AWB No</th>
                                        <th>Consignment Note</th>
                                        <th>Consignment Date</th>
                                        <th>Fleet Code</th>
                                        <th>Origin</th>
                                        <th>Destination</th>
                                        <th>Status</th>
                                        <th>Select</th>
                                    </tr>
                                </thead>
                                <tbody><%int flagFortsa=0;%>
                                <% int index = 0, sno =1,cntr=1;%><script>var cntr = 1;</script>
                                <c:forEach items="${tsaManifestList}" var="tsaList">
                                    <%
                                                String classText = "";
                                                int oddEven = index % 2;
                                                if (oddEven > 0) {
                                                    classText = "text2";
                                                } else {
                                                    classText = "text1";
                                                }
                                    %>
                                    <c:set var="forcheckBox" value="${tsaList.fleetStatus}"/>
                                    <c:set var="manifest" value="${tsaList.manifestStatus}"/>
                                    <input type="hidden" name="forfleet" id="forfleet<%=sno%>" value="<c:out value="${tsaList.fleetCode}"/> ">
                                    <input type="hidden" name="forfleetStatus" id="forfleetStatus<%=sno%>" value="<c:out value="${tsaList.fleetStatus}"/> ">
                                    <tr height="30">
                                        <td  ><%=sno%></td>
                                        <td  ><c:out value="${tsaList.awbno}"/></td>
                                        <td  ><a href="#" onclick="viewConsignmentDetails('<c:out value="${tsaList.consignmentOrderId}"/>');"><c:out value="${tsaList.cNoteNo}"/></a></td>
                                        <td  ><c:out value="${tsaList.consignmentDate}"/></td>
                                        <td   id="fleetCodeNo<%=sno%>"></td>

                                        <td  ><c:out value="${tsaList.origin}"/></td>
                                        <td  ><c:out value="${tsaList.destination}"/></td>
                                        <td  >
                                            <% int check = 0; %>
                                            <c:if test="${tsaList.customStatus==1 && manifest==1 && tsaList.receivedStatus==0 && tsaList.receivedStatus==2}">
                                                <% 
                                                 Object startTime1 = (Object) pageContext.getAttribute("forcheckBox");
                                     
                                                 String splitValue1 = (String) startTime1;
                                                 String[] str1=splitValue1.split(",");
                                                  int count1 = 0;
                                                  for(int i=0;i<str1.length;i++){
                                                    int ch1=Integer.parseInt(str1[i]);
                                        
                                                      if(ch1==1 ){
                                                          check = 1;
                                       
                                                      }

                                                  }
                                                %>
                                            </c:if>
                                            <c:if test="${tsaList.customStatus==1 && manifest==1 && tsaList.tsaGenStatus==1 && tsaList.manifestGenStatus==1 && tsaList.receivedStatus == 3}">
                                                <c:out value="TSA & Cargo Manifest already generated"/>
                                            </c:if>
                                            <c:if test="${ tsaList.customStatus==null || manifest==null || tsaList.tsaGenStatus==0 || tsaList.manifestGenStatus==0 || tsaList.receivedStatus==0}">
                                                <% 
                                         Object startTime = (Object) pageContext.getAttribute("forcheckBox");
                                     
                                         String splitValue = (String) startTime;
                                         String[] str=splitValue.split(",");
                                          int count = 0;
                                          for(int i=0;i<str.length;i++){
                                            int ch=Integer.parseInt(str[i]);

                                              if(ch==0 && count == 0){
                                                  flagFortsa=1;
                                                  count = 1;
                                       
                                              }
                                          }
                                          if(flagFortsa==0 && check == 0){         %>
                                                <c:if test="${tsaList.customStatus==null && manifest==null || tsaList.customStatus==1 && manifest==1  && tsaList.tsaGenStatus==0 && tsaList.manifestGenStatus==0}">
                                                    <c:out value="Ready for Tsa generation & Cargo Manifest"/>
                                                </c:if>
                                                <c:if test="${tsaList.customStatus==1 && manifest==null && tsaList.tsaGenStatus==1 }">
                                                    <c:out value="Tsa generated, Ready for Cargo Manifest"/>
                                                </c:if>
                                                <c:if test="${tsaList.customStatus==null && manifest==1 && tsaList.manifestGenStatus==1}">
                                                    <c:out value="Cargo Manifest done, ready for Tsa generation"/>
                                                </c:if>
                                                <c:if test="${tsaList.customStatus==1 && manifest==1 && tsaList.manifestGenStatus==1 && tsaList.tsaGenStatus==1 && tsaList.receivedStatus == 0}">
                                                    <c:out value="Tsa & Manifest generated, Not Ready for next partshipment TSA generation & Cargo Manifest"/>
                                                </c:if>

                                                <%}else{%>
                                                <c:out value="Not ready for TSA generation & Cargo Manifest"/>
                                                <%}%>
                                            </c:if>
                                        </td>
                                        <td  >
                                            <c:choose>
                                                <c:when test="${tsaList.customStatus==1 && manifest==1 && tsaList.tsaGenStatus==1 && tsaList.manifestGenStatus==1 }">
                                                    <c:out value="-"/>
                                                </c:when>
                                            </c:choose>
                                            <c:if test="${tsaList.consignmentOrderStatus >=8 && tsaList.customStatus==null || manifest==null || tsaList.tsaGenStatus==0 || tsaList.manifestGenStatus==0}">
                                                <% if(flagFortsa==0 && check == 0 ){%>
                                                <input type="checkbox" name="checkConsignment"  id="checkConsignment<%=sno%>" value="<c:out value="${tsaList.consignmentOrderId}"/>-<%=sno%>" onclick="checkSelectedItem('<%=sno%>')"/>
                                                <%}else{%>
                                                <c:out value="-"/>
                                                <%}%>
                                            </c:if>
                                            <input type="hidden" name="consignmentOrderId" id="consignmentOrderId<%=sno%>" value="<c:out value="${tsaList.consignmentOrderId}"/>"/>
                                            <c:if test="${tsaList.tsaGenStatus == 0 && tsaList.customStatus == 1 || tsaList.customStatus != 1}">
                                                <input type="hidden" name="customStatus" id="customStatus<%=sno%>" value="0"/>
                                            </c:if>
                                            <c:if test="${tsaList.tsaGenStatus == 1 && tsaList.customStatus == 1}">
                                                <input type="hidden" name="customStatus" id="customStatus<%=sno%>" value="<c:out value="${tsaList.customStatus}"/>"/>
                                            </c:if>
                                            <c:if test="${tsaList.manifestGenStatus == 0 && tsaList.manifestStatus == 1 || tsaList.manifestStatus != 1}">
                                                <input type="hidden" name="manifestStatus" id="manifestStatus<%=sno%>" value="0"/>
                                            </c:if>
                                            <c:if test="${tsaList.manifestGenStatus == 0 && tsaList.manifestStatus != 1}">
                                                <input type="hidden" name="manifestStatus" id="manifestStatus<%=sno%>" value="<c:out value="${tsaList.manifestStatus}"/>"/>
                                            </c:if>
                                        </td>
                                    </tr>
                                    <script>

                                        var fleetstr = document.getElementById("forfleet" + cntr).value.split(",");
                                        var forfleetStatus = document.getElementById("forfleetStatus" + cntr).value.split(",");
                                        //                                      alert(fleetstr+" hhhh "+forfleetStatus);
                                        var buildFleetCode = "";
                                        var sepratn = "";
                                        for (var k = 0; k < forfleetStatus.length; k++) {
                                            if (parseInt(forfleetStatus[k]) == 0) {
                                                if (k > 0) {
                                                    sepratn = "/";
                                                }
                                                buildFleetCode = buildFleetCode + sepratn + "<font color='red'>" + fleetstr[k] + "</font>"
                                            }
                                            if (parseInt(forfleetStatus[k]) == 1) {
                                                if (k > 0) {
                                                    sepratn = "/";
                                                }
                                                buildFleetCode = buildFleetCode + sepratn + "<font color='green'>" + fleetstr[k] + "</font>"
                                            }
                                        }

                                        document.getElementById("fleetCodeNo" + cntr).innerHTML = buildFleetCode;
                                        cntr++;

                                    </script>


                                    <%++sno;cntr++;%>
                                    <%flagFortsa=0;%>
                                </c:forEach>
                                </tbody>
                            </table>
                            <br>
                            <br>
                            <center>
                                <input type="button" class="btn btn-success" name="dhl"  value="Create TSA" onclick="submitPageForTsa()"/>&nbsp;&nbsp;&nbsp;
                                <input type="button" class="btn btn-success" name="dhl"  value="CARGOMANIFEST" onclick="cargoManifest()"/>&nbsp;&nbsp;&nbsp;
                            </center>
                            <br>
                            <br>
                            <br>

                            <script language="javascript" type="text/javascript">
                                setFilterGrid("table1");
                            </script>
                            <script>
                                function submitPageForTsa() {
                                    var checkedStatus = 0;
                                    var consignmentIds = "";
                                    var flag = 0;
                                    var consignmentSplit;
                                    var consignment = document.getElementsByName("checkConsignment");
                                    var tsaFlag = 0;

                                    for (var i = 0; i < consignment.length; i++) {
                                        if (consignment[i].checked == true) {

                                            consignmentSplit = consignment[i].value.split("-");
                                            tsaFlag = document.getElementById("customStatus" + consignmentSplit[1]).value;
                                            flag = 1;
                                            if (parseInt(tsaFlag) == 1) {
                                                alert('TSA Already Generated');
                                                consignment[i].checked = false;
                                                return;
                                            }
                                            if (consignmentIds == "") {
                                                consignmentIds = consignmentSplit[0];
                                            } else {
                                                var newconsignmentId = ",".concat(consignmentSplit[0]);
                                                consignmentIds = consignmentIds.concat(newconsignmentId);
                                            }
                                        }
                                    }//for
                                    if (parseInt(flag) == 1) {
                                        window.open('/throttle/exportConsignmentTsa.do?consignmentIds=' + consignmentIds, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
                                    } else {
                                        alert("Please Check Any Record");
                                        return;
                                    }
                                }
                                function cargoManifest() {
                                    var checkedStatus = 0;
                                    var consignmentIds = "";
                                    var flag = 0;
                                    var consignmentSplit;
                                    var consignment = document.getElementsByName("checkConsignment");
                                    var tsaFlag = 0;
                                    for (var i = 0; i < consignment.length; i++) {
                                        if (consignment[i].checked == true) {
                                            consignmentSplit = consignment[i].value.split("-");
                                            tsaFlag = document.getElementById("manifestStatus" + consignmentSplit[1]).value;
                                            flag = 1;
                                            if (parseInt(tsaFlag) == 1) {
                                                alert('Manifest Already Generated');
                                                consignment[i].checked = false;
                                                return;
                                            }
                                            if (consignmentIds == "") {
                                                consignmentIds = consignmentSplit[0];
                                            } else {
                                                var newconsignmentId = ",".concat(consignmentSplit[0]);
                                                consignmentIds = consignmentIds.concat(newconsignmentId);
                                            }
                                        }
                                    }//for
                                    if (parseInt(flag) == 1) {
                                        window.open('/throttle/consignmentNoteManifest.do?consignmentIds=' + consignmentIds, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
                                    } else {
                                        alert("Please Check Any Record");
                                        return;
                                    }
                                }
                                function cargoManifest1() {
                                    var checkedStatus = 0;
                                    var consignmentIds = "";
                                    var ele = document.CNoteSearch.elements;

                                    if (ele.length > 0) {
                                        for (var i = 0; i < ele.length; i++) {
                                            if (ele[i].name == "checkConsignment" & ele[i].checked == true) {
                                                //                                                   if(parseInt(document.getElementById("customStatus"+ele[i].value).value)== parseInt(1)){
                                                //                                                   ele[i].checked==false;
                                                //                                                   alert("Already Tsa Created For Consignment");
                                                //                                                   }else{
                                                checkedStatus = 1;
                                                if (consignmentIds == "") {
                                                    consignmentIds = ele[i].value;
                                                } else {
                                                    var newconsignmentId = ",".concat(ele[i].value);
                                                    consignmentIds = consignmentIds.concat(newconsignmentId);
                                                    //}
                                                }
                                            }
                                        }
                                    }

                                    if (parseInt(checkedStatus) == parseInt(0)) {
                                        alert("Please Check Any Record");
                                        return;
                                    }
                                    window.open('/throttle/consignmentNoteManifest.do?consignmentIds=' + consignmentIds, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
                                }

                                function submitPageForTsa1() {
                                    //                            alert(ele);
                                    var checkedStatus = 0;
                                    var consignmentIds = "";
                                    var ele = document.searchtsa.elements;
                                    var customStatus = document.getElementsByName("customStatus");
                                    if (ele.length > 0) {
                                        for (var i = 0; i < ele.length; i++) {
                                            if (ele[i].name == "checkConsignment" & ele[i].checked == true) {
                                                if (parseInt(document.getElementById("customStatus" + i).value) == parseInt(1)) {
                                                    ele[i].checked == false;
                                                    alert("Already Tsa Created For Consignment");
                                                } else {
                                                    checkedStatus = 1;
                                                    if (consignmentIds == "") {
                                                        consignmentIds = ele[i].value;
                                                    } else {
                                                        var newconsignmentId = ",".concat(ele[i].value);
                                                        consignmentIds = consignmentIds.concat(newconsignmentId);
                                                        //                                        alert(consignmentIds);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (parseInt(checkedStatus) == parseInt(0)) {
                                        alert("Please Check Any Record");
                                        return;
                                    }
                                    window.open('/throttle/exportConsignmentTsa.do?consignmentIds=' + consignmentIds, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');

                                }



                            </script>
                            <div id="controls">
                                <div id="perpage">
                                    <select onchange="sorter.size((this.value))">
                                        <option value="5" selected="selected">5</option>
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span>Entries Per Page</span>
                                </div>
                                <div id="navigation">
                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                </div>
                                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                            </div>
                            <script type="text/javascript">
                                var sorter = new TINY.table.sorter("sorter");
                                sorter.head = "head";
                                sorter.asc = "asc";
                                sorter.desc = "desc";
                                sorter.even = "evenrow";
                                sorter.odd = "oddrow";
                                sorter.evensel = "evenselected";
                                sorter.oddsel = "oddselected";
                                sorter.paginate = true;
                                sorter.currentid = "currentpage";
                                sorter.limitid = "pagelimit";
                                sorter.init("table", 1);
                            </script>
                            <input type="hidden" class="textbox" name="consignmentOrder" id="consignmentOrder" style="width: 110px" value=""/>
                        </c:if>
                        <c:if test = "${tsaManifestList == null}" >
                            <B><font color="Red"><center>No Record Found :</center></font></B>
                                </c:if>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %> 
