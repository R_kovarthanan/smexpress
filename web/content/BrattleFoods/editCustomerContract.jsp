<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    function showAddOption() {
        $("#addNewRouteButton").hide();
        $("#addNewRoute").show();
        $("#addMoreRouteValue").val(1);
    }

    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });
    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });</script>
<script>
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#accountManager').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getAccountManager.do",
                    dataType: "json",
                    data: {
                        accountManagerName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#accountManagerId').val('');
                            $('#accountManager').val('');
                        } else {
                            response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#accountManager").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#accountManagerId').val(tmp[0]);
                $itemrow.find('#accountManager').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });</script>



<c:if test="${billingTypeId == 1}">
    <script>
        $(document).ready(function() {
            // Get the table object to use for adding a row at the end of the table
            var $itemsTable = $('#itemsTable');
            // Create an Array to for the table row. ** Just to make things a bit easier to read.

            var rowTemp = [
                '<tr class="item-row">',
                '<td><a id="deleteRow"><img src="/throttle/images/icon-minus.png" alt="Remove Item" title="Remove Item"></a><input type="hidden" name="ptpRouteContractCode" id="ptpRouteContractCode" value="RC" class="form-control"  style="width: 60px;"/></td>',
                '<td><c:if test="${vehicleTypeList != null}"><select name="ptpVehicleTypeId" id="ptpVehicleTypeId"  class="form-control" style="width:250px;height:40px" style="width:235px" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></td>',
                
                '<td><input name="ptpPickupPoint" value="" class="form-control" id="ptpPickupPoint"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="ptpPickupPointId" value="" class="form-control" id="ptpPickupPointId"  style="width: 120px;"/></td>',
                '<td><input name="interimPoint1" value="" class="form-control" id="interimPoint1"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId1" value="" class="form-control" id="interimPointId1"  style="width: 120px;"/><input type="hidden" name="interimPoint1Km" value="" class="form-control" id="interimPoint1Km"  style="width: 120px;"/><input type="hidden" name="interimPoint1Hrs" value="" class="form-control" id="interimPoint1Hrs"  style="width: 120px;"/><input type="hidden" name="interimPoint1Minutes" value="" class="form-control" id="interimPoint1Minutes"  style="width: 120px;"/><input type="hidden" name="interimPoint1RouteId" value="" class="form-control" id="interimPoint1RouteId"  style="width: 120px;"/></td>',
                '<td><input name="interimPoint2" value="" class="form-control" id="interimPoint2"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId2" value="" class="form-control" id="interimPointId2"  style="width: 120px;"/><input type="hidden"  name="interimPoint2Km" value="" class="form-control" id="interimPoint2Km"  style="width: 120px;"/><input type="hidden"  name="interimPoint2Hrs" value="" class="form-control" id="interimPoint2Hrs"  style="width: 120px;"/><input type="hidden" name="interimPoint2Minutes" value="" class="form-control" id="interimPoint2Minutes"  style="width: 120px;"/><input type="hidden"  name="interimPoint2RouteId" value="" class="form-control" id="interimPoint2RouteId"  style="width: 120px;"/></td>',
                '<td><input name="interimPoint3" value="" class="form-control" id="interimPoint3"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId3" value="" class="form-control" id="interimPointId3"  style="width: 120px;"/><input type="hidden" name="interimPoint3Km" value="" class="form-control" id="interimPoint3Km"  style="width: 120px;"/><input type="hidden" name="interimPoint3Hrs" value="" class="form-control" id="interimPoint3Hrs"  style="width: 120px;"/><input type="hidden" name="interimPoint3Minutes" value="" class="form-control" id="interimPoint3Minutes"  style="width: 120px;"/><input type="hidden" name="interimPoint3RouteId" value="" class="form-control" id="interimPoint3RouteId"  style="width: 120px;"/></td>',
                '<td><input name="interimPoint4" value="" class="form-control" id="interimPoint4"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId4" value="" class="form-control" id="interimPointId4"  style="width: 120px;"/><input type="hidden"  name="interimPoint4Km" value="" class="form-control" id="interimPoint4Km"  style="width: 120px;"/><input type="hidden"  name="interimPoint4Hrs" value="" class="form-control" id="interimPoint4Hrs"  style="width: 120px;"/><input type="hidden" name="interimPoint4Minutes" value="" class="form-control" id="interimPoint4Minutes"  style="width: 120px;"/><input type="hidden"  name="interimPoint4RouteId" value="" class="form-control" id="interimPoint4RouteId"  style="width: 120px;"/></td>',
                '<td><input name="ptpDropPoint" value="" class="form-control" id="ptpDropPoint"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="ptpDropPointId" value="" class="form-control" id="ptpDropPointId"  style="width: 120px;"/><input type="hidden"  name="ptpDropPointKm" value="" class="form-control" id="ptpDropPointKm"  style="width: 120px;"/><input type="hidden"  name="ptpDropPointHrs" value="" class="form-control" id="ptpDropPointHrs"  style="width: 120px;"/><input type="hidden" name="ptpDropPointMinutes" value="" class="form-control" id="ptpDropPointMinutes"  style="width: 120px;"/><input type="hidden"  name="ptpDropPointRouteId" value="" class="form-control" id="ptpDropPointRouteId"  style="width: 120px;"/></td>',
                '<td><input name="ptpTotalKm" value="" class="form-control" id="ptpTotalKm"  style="width: 120px;" readonly/><input type="hidden" name="ptpTotalHours" value="" class="form-control" id="ptpTotalHours"  style="width: 120px;"/><input type="hidden" name="ptpTotalMinutes" value="" class="form-control" id="ptpTotalMinutes"  style="width: 120px;"/></td>',
                '<td><input name="ptpRateWithReefer" value="" class="form-control" id="ptpRateWithReefer"  style="width: 120px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '<td><input name="ptpRateWithoutReefer" value="" class="form-control" id="ptpRateWithoutReefer"  style="width: 120px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '<td><input name="tatTime" value="" class="form-control" id="tatTime"  style="width: 120px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '</tr>'
            ].join('');
            var count = 1;
            // Add row to list and allow user to use autocomplete to find items.


            $("#addRow").bind('click', function() {
                count++;
                var $row = $(rowTemp);
                // save reference to inputs within row
                var $ptpRouteContractCode = $row.find('#ptpRouteContractCode');
                var $ptpVehicleTypeId = $row.find('#ptpVehicleTypeId');
                var $ptpPickupPoint = $row.find('#ptpPickupPoint');
                var $ptpPickupPointId = $row.find('#ptpPickupPointId');
                var $interimPoint1 = $row.find('#interimPoint1');
                var $interimPointId1 = $row.find('#interimPointId1');
                var $interimPoint1Km = $row.find('#interimPoint1Km');
                var $interimPoint1Hrs = $row.find('#interimPoint1Hrs');
                var $interimPoint1Minutes = $row.find('#interimPoint1Minutes');
                var $interimPoint1RouteId = $row.find('#interimPoint1RouteId');
                var $interimPoint2 = $row.find('#interimPoint2');
                var $interimPointId2 = $row.find('#interimPointId2');
                var $interimPoint2Km = $row.find('#interimPoint2Km');
                var $interimPoint2Hrs = $row.find('#interimPoint2Hrs');
                var $interimPoint2Minutes = $row.find('#interimPoint2Minutes');
                var $interimPoint2RouteId = $row.find('#interimPoint2RouteId');
                var $interimPoint3 = $row.find('#interimPoint3');
                var $interimPointId3 = $row.find('#interimPointId3');
                var $interimPoint3Km = $row.find('#interimPoint3Km');
                var $interimPoint3Hrs = $row.find('#interimPoint3Hrs');
                var $interimPoint3Minutes = $row.find('#interimPoint3Minutes');
                var $interimPoint3RouteId = $row.find('#interimPoint3RouteId');
                var $interimPoint4 = $row.find('#interimPoint4');
                var $interimPointId4 = $row.find('#interimPointId4');
                var $interimPoint4Km = $row.find('#interimPoint4Km');
                var $interimPoint4Hrs = $row.find('#interimPoint4Hrs');
                var $interimPoint4Minutes = $row.find('#interimPoint4Minutes');
                var $interimPoint4RouteId = $row.find('#interimPoint4RouteId');
                var $ptpDropPoint = $row.find('#ptpDropPoint');
                var $ptpDropPointId = $row.find('#ptpDropPointId');
                var $ptpDropPointKm = $row.find('#ptpDropPointKm');
                var $ptpDropPointHrs = $row.find('#ptpDropPointHrs');
                var $ptpDropPointMinutes = $row.find('#ptpDropPointMinutes');
                var $ptpDropPointRouteId = $row.find('#ptpDropPointRouteId');
                var $ptpTotalKm = $row.find('#ptpTotalKm');
                var $ptpTotalHours = $row.find('#ptpTotalHours');
                var $ptpTotalMinuites = $row.find('#ptpTotalMinutes');
                var $ptpRateWithReefer = $row.find('#ptpRateWithReefer');
                var $ptpRateWithoutReefer = $row.find('#ptpRateWithoutReefer');
                if ($('#ptpRouteContractCode:last').val() !== '') {
                    // apply autocomplete widget to newly created row
                    //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
                    $row.find('#ptpPickupPoint').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
                                        alert("Invalid Route Point");
                                        $row.find('#ptpPickupPoint').val('');
                                        $row.find('#ptpPickupPointId').val('');
                                        $row.find("#interimPoint1").val('');
                                        $row.find("#interimPointId1").val('');
                                        $row.find("#interimPoint2").val('');
                                        $row.find("#interimPointId2").val('');
                                        $row.find("#interimPoint3").val('');
                                        $row.find("#interimPointId3").val('');
                                        $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val('');
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint1Km").val('');
                                        $row.find("#interimPoint2Km").val('');
                                        $row.find("#interimPoint3Km").val('');
                                        $row.find("#interimPoint4Km").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint1Hrs").val('');
                                        $row.find("#interimPoint2Hrs").val('');
                                        $row.find("#interimPoint3Hrs").val('');
                                        $row.find("#interimPoint4Hrs").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint1Minutes").val('');
                                        $row.find("#interimPoint2Minutes").val('');
                                        $row.find("#interimPoint3Minutes").val('');
                                        $row.find("#interimPoint4Minutes").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint1RouteId").val('');
                                        $row.find("#interimPoint2RouteId").val('');
                                        $row.find("#interimPoint3RouteId").val('');
                                        $row.find("#interimPoint4RouteId").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find('#ptpPickupPoint').focus();
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 1,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            // Populate the input fields from the returned values
                            var value = ui.item.Name;
                            //                                alert(value);
                            var tmp = value.split('-');
                            $itemrow.find('#ptpPickupPoint').val(tmp[0]);
                            $itemrow.find('#ptpPickupPointId').val(tmp[1]);
                            $itemrow.find('#interimPoint1').focus();
                            $row.find("#interimPoint1").val('');
                            $row.find("#interimPointId1").val('');
                            $row.find("#interimPoint2").val('');
                            $row.find("#interimPointId2").val('');
                            $row.find("#interimPoint3").val('');
                            $row.find("#interimPointId3").val('');
                            $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val('');
                            $row.find("#ptpDropPoint").val('');
                            $row.find("#ptpDropPointId").val('');
                            $row.find("#interimPoint1Km").val('');
                            $row.find("#interimPoint2Km").val('');
                            $row.find("#interimPoint3Km").val('');
                            $row.find("#interimPoint4Km").val('');
                            $row.find("#ptpDropPointKm").val('');
                            $row.find("#interimPoint1Hrs").val('');
                            $row.find("#interimPoint2Hrs").val('');
                            $row.find("#interimPoint3Hrs").val('');
                            $row.find("#interimPoint4Hrs").val('');
                            $row.find("#ptpDropPointHrs").val('');
                            $row.find("#interimPoint1Minutes").val('');
                            $row.find("#interimPoint2Minutes").val('');
                            $row.find("#interimPoint3Minutes").val('');
                            $row.find("#interimPoint4Minutes").val('');
                            $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#interimPoint1RouteId").val('');
                            $row.find("#interimPoint2RouteId").val('');
                            $row.find("#interimPoint3RouteId").val('');
                            $row.find("#interimPoint4RouteId").val('');
                            $row.find("#ptpDropPointRouteId").val('');
                            $row.find("#ptpTotalKm").val('');
                            $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val('');
                            $row.find("#ptpRateWithReefer").val('');
                            $row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                    $row.find('#interimPoint1').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
                                        alert("Invalid Route Point");
                                        $row.find("#interimPoint1").val('');
                                        $row.find("#interimPointId1").val('');
                                        $row.find("#interimPoint2").val('');
                                        $row.find("#interimPointId2").val('');
                                        $row.find("#interimPoint3").val('');
                                        $row.find("#interimPointId3").val('');
                                        $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val('');
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint1Km").val('');
                                        $row.find("#interimPoint2Km").val('');
                                        $row.find("#interimPoint3Km").val('');
                                        $row.find("#interimPoint4Km").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint1Hrs").val('');
                                        $row.find("#interimPoint2Hrs").val('');
                                        $row.find("#interimPoint3Hrs").val('');
                                        $row.find("#interimPoint4Hrs").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint1Minutes").val('');
                                        $row.find("#interimPoint2Minutes").val('');
                                        $row.find("#interimPoint3Minutes").val('');
                                        $row.find("#interimPoint4Minutes").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint1RouteId").val('');
                                        $row.find("#interimPoint2RouteId").val('');
                                        $row.find("#interimPoint3RouteId").val('');
                                        $row.find("#interimPoint4RouteId").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find('#interimPoint1').focus();
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint1').val(tmp[0]);
                            $itemrow.find('#interimPointId1').val(tmp[1]);
                            $itemrow.find('#interimPoint1Km').val(tmp[2]);
                            $itemrow.find('#interimPoint1Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint1Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint1RouteId').val(tmp[5]);
                            $itemrow.find('#interimPoint2').focus();
                            $row.find("#interimPoint2").val('');
                            $row.find("#interimPointId2").val('');
                            $row.find("#interimPoint3").val('');
                            $row.find("#interimPointId3").val('');
                            $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val('');
                            $row.find("#ptpDropPoint").val('');
                            $row.find("#ptpDropPointId").val('');
                            $row.find("#interimPoint2Km").val('');
                            $row.find("#interimPoint3Km").val('');
                            $row.find("#interimPoint4Km").val('');
                            $row.find("#ptpDropPointKm").val('');
                            $row.find("#interimPoint2Hrs").val('');
                            $row.find("#interimPoint3Hrs").val('');
                            $row.find("#interimPoint4Hrs").val('');
                            $row.find("#ptpDropPointHrs").val('');
                            $row.find("#interimPoint2Minutes").val('');
                            $row.find("#interimPoint3Minutes").val('');
                            $row.find("#interimPoint4Minutes").val('');
                            $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#interimPoint2RouteId").val('');
                            $row.find("#interimPoint3RouteId").val('');
                            $row.find("#interimPoint4RouteId").val('');
                            $row.find("#ptpDropPointRouteId").val('');
                            $row.find("#ptpTotalKm").val('');
                            $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val('');
                            $row.find("#ptpRateWithReefer").val('');
                            $row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                    $row.find('#interimPoint2').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
                                        alert("Invalid Route Point");
                                        $row.find("#interimPoint2").val('');
                                        $row.find("#interimPointId2").val('');
                                        $row.find("#interimPoint3").val('');
                                        $row.find("#interimPointId3").val('');
                                        $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val('');
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint2Km").val('');
                                        $row.find("#interimPoint3Km").val('');
                                        $row.find("#interimPoint4Km").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint2Hrs").val('');
                                        $row.find("#interimPoint3Hrs").val('');
                                        $row.find("#interimPoint4Hrs").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint2Minutes").val('');
                                        $row.find("#interimPoint3Minutes").val('');
                                        $row.find("#interimPoint4Minutes").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint2RouteId").val('');
                                        $row.find("#interimPoint3RouteId").val('');
                                        $row.find("#interimPoint4RouteId").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find('#interimPoint2').focus();
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint2').val(tmp[0]);
                            $itemrow.find('#interimPointId2').val(tmp[1]);
                            $itemrow.find('#interimPoint2Km').val(tmp[2]);
                            $itemrow.find('#interimPoint2Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint2Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint2RouteId').val(tmp[5]);
                            $itemrow.find('#interimPoint3').focus();
                            $row.find("#interimPoint3").val('');
                            $row.find("#interimPointId3").val('');
                            $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val('');
                            $row.find("#ptpDropPoint").val('');
                            $row.find("#ptpDropPointId").val('');
                            $row.find("#interimPoint3Km").val('');
                            $row.find("#interimPoint4Km").val('');
                            $row.find("#ptpDropPointKm").val('');
                            $row.find("#interimPoint3Hrs").val('');
                            $row.find("#interimPoint4Hrs").val('');
                            $row.find("#ptpDropPointHrs").val('');
                            $row.find("#interimPoint3Minutes").val('');
                            $row.find("#interimPoint4Minutes").val('');
                            $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#interimPoint3RouteId").val('');
                            $row.find("#interimPoint4RouteId").val('');
                            $row.find("#ptpDropPointRouteId").val('');
                            $row.find("#ptpTotalKm").val('');
                            $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val('');
                            $row.find("#ptpRateWithReefer").val('');
                            $row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                    $row.find('#interimPoint3').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
                                        alert("Invalid Route Point");
                                        $row.find("#interimPoint3").val('');
                                        $row.find("#interimPointId3").val('');
                                        $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val('');
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint3Km").val('');
                                        $row.find("#interimPoint4Km").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint3Hrs").val('');
                                        $row.find("#interimPoint4Hrs").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint3Minutes").val('');
                                        $row.find("#interimPoint4Minutes").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint3RouteId").val('');
                                        $row.find("#interimPoint4RouteId").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find('#interimPoint3').focus();
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint3').val(tmp[0]);
                            $itemrow.find('#interimPointId3').val(tmp[1]);
                            $itemrow.find('#interimPoint3Km').val(tmp[2]);
                            $itemrow.find('#interimPoint3Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint3Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint3RouteId').val(tmp[5]);
                            $itemrow.find('#interimPoint4').focus();
                            $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val('');
                            $row.find("#ptpDropPoint").val('');
                            $row.find("#ptpDropPointId").val('');
                            $row.find("#interimPoint4Km").val('');
                            $row.find("#ptpDropPointKm").val('');
                            $row.find("#interimPoint4Hrs").val('');
                            $row.find("#ptpDropPointHrs").val('');
                            $row.find("#interimPoint4Minutes").val('');
                            $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#interimPoint4RouteId").val('');
                            $row.find("#ptpDropPointRouteId").val('');
                            $row.find("#ptpTotalKm").val('');
                            $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val('');
                            $row.find("#ptpRateWithReefer").val('');
                            $row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                    $row.find('#interimPoint4').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
                                        alert("Invalid Route Point");
                                        $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val('');
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint4Km").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint4Hrs").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint4Minutes").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint4RouteId").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find('#interimPoint4').focus();
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint4').val(tmp[0]);
                            $itemrow.find('#interimPointId4').val(tmp[1]);
                            $itemrow.find('#interimPoint4Km').val(tmp[2]);
                            $itemrow.find('#interimPoint4Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint4Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint4RouteId').val(tmp[5]);
                            $itemrow.find('#ptpDropPoint').focus();
                            $row.find("#ptpDropPoint").val('');
                            $row.find("#ptpDropPointId").val('');
                            $row.find("#ptpDropPointKm").val('');
                            $row.find("#ptpDropPointHrs").val('');
                            $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#ptpDropPointRouteId").val('');
                            $row.find("#ptpTotalKm").val('');
                            $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val('');
                            $row.find("#ptpRateWithReefer").val('');
                            $row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                    $row.find('#ptpDropPoint').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
                                        alert("Invalid Route Point");
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                        $row.find('#ptpDropPoint').focus();
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#ptpDropPoint').val(tmp[0]);
                            $itemrow.find('#ptpDropPointId').val(tmp[1]);
                            $itemrow.find('#ptpDropPointKm').val(tmp[2]);
                            $itemrow.find('#ptpDropPointRouteId').val(tmp[5]);
                            $row.find("#ptpRateWithReefer").val('');
                            $row.find("#ptpRateWithoutReefer").val('');
                            if ($row.find('#interimPoint1Km').val() !== '') {
                                var interimPoint1Km = $row.find('#interimPoint1Km').val();
                            } else {
                                var interimPoint1Km = 0;
                            }
                            if ($row.find('#interimPoint2Km').val() !== '') {
                                var interimPoint2Km = $row.find('#interimPoint2Km').val();
                            } else {
                                var interimPoint2Km = 0;
                            }
                            if ($row.find('#interimPoint3Km').val() !== '') {
                                var interimPoint3Km = $row.find('#interimPoint3Km').val();
                            } else {
                                var interimPoint3Km = 0;
                            }
                            if ($row.find('#interimPoint4Km').val() !== '') {
                                var interimPoint4Km = $row.find('#interimPoint4Km').val();
                            } else {
                                var interimPoint4Km = 0;
                            }


                            if ($row.find('#interimPoint1Hrs').val() !== '') {
                                var interimPoint1Hrs = $row.find('#interimPoint1Hrs').val();
                            } else {
                                var interimPoint1Hrs = 0;
                            }
                            if ($row.find('#interimPoint2Hrs').val() !== '') {
                                var interimPoint2Hrs = $row.find('#interimPoint2Hrs').val();
                            } else {
                                var interimPoint2Hrs = 0;
                            }
                            if ($row.find('#interimPoint3Hrs').val() !== '') {
                                var interimPoint3Hrs = $row.find('#interimPoint3Hrs').val();
                            } else {
                                var interimPoint3Hrs = 0;
                            }
                            if ($row.find('#interimPoint4Hrs').val() !== '') {
                                var interimPoint4Hrs = $row.find('#interimPoint4Hrs').val();
                            } else {
                                var interimPoint4Hrs = 0;
                            }

                            if ($row.find('#interimPoint1Minutes').val() !== '') {
                                var interimPoint1Minutes = $row.find('#interimPoint1Minutes').val();
                            } else {
                                var interimPoint1Minutes = 0;
                            }
                            if ($row.find('#interimPoint2Minutes').val() !== '') {
                                var interimPoint2Minutes = $row.find('#interimPoint2Minutes').val();
                            } else {
                                var interimPoint2Minutes = 0;
                            }
                            if ($row.find('#interimPoint3Minutes').val() !== '') {
                                var interimPoint3Minutes = $row.find('#interimPoint3Minutes').val();
                            } else {
                                var interimPoint3Minutes = 0;
                            }
                            if ($row.find('#interimPoint4Minutes').val() !== '') {
                                var interimPoint4Minutes = $row.find('#interimPoint4Minutes').val();
                            } else {
                                var interimPoint4Minutes = 0;
                            }


                            var totalKm = parseFloat(tmp[2]) + parseFloat(interimPoint1Km) + parseFloat(interimPoint2Km) + parseFloat(interimPoint3Km) + parseFloat(interimPoint4Km);
                            $row.find('#ptpTotalKm').val(totalKm);
                            var totalHrs = parseInt(tmp[3]) + parseInt(interimPoint1Hrs) + parseInt(interimPoint2Hrs) + parseInt(interimPoint3Hrs) + parseInt(interimPoint4Hrs);
                            $row.find('#ptpTotalHours').val(totalHrs);
                            var totalMinutes = parseInt(tmp[4]) + parseInt(interimPoint1Minutes) + parseInt(interimPoint2Minutes) + parseInt(interimPoint3Minutes) + parseInt(interimPoint4Minutes);
                            $row.find('#ptpTotalMinutes').val(totalMinutes);
                            $itemrow.find('#ptpRateWithReefer').focus();
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                    // Add row after the first row in table
                    $('.item-row:last', $itemsTable).after($row);
                    $($ptpRouteContractCode).focus();
                } // End if last itemCode input is empty
                return false;
            });
        }); // End DOM

        // Remove row when clicked
        $("#deleteRow").live('click', function() {
            $(this).parents('.item-row').remove();
            // Hide delete Icon if we only have one row in the list.
            if ($(".item-row").length < 2)
                $("#deleteRow").hide();
        });
        $(document).ready(function() {
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#ptpPickupPoint').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint3: document.getElementById("interimPointId3").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                alert("Invalid Route Point");
                                $('#ptpPickupPoint').val('');
                                $('#ptpPickupPointId').val('');
                                $("#interimPoint1").val('');
                                $("#interimPointId1").val('');
                                $("#interimPoint2").val('');
                                $("#interimPointId2").val('');
                                $("#interimPoint3").val('');
                                $("#interimPointId3").val('');
                                $("#interimPoint4").val('');
                                $("#interimPointId4").val('');
                                $("#ptpDropPoint").val('');
                                $("#ptpDropPointId").val('');
                                $("#interimPoint1Km").val('');
                                $("#interimPoint2Km").val('');
                                $("#interimPoint3Km").val('');
                                $("#interimPoint4Km").val('');
                                $("#ptpDropPointKm").val('');
                                $("#interimPoint1Hrs").val('');
                                $("#interimPoint2Hrs").val('');
                                $("#interimPoint3Hrs").val('');
                                $("#interimPoint4Hrs").val('');
                                $("#ptpDropPointHrs").val('');
                                $("#interimPoint1Minutes").val('');
                                $("#interimPoint2Minutes").val('');
                                $("#interimPoint3Minutes").val('');
                                $("#interimPoint4Minutes").val('');
                                $("#ptpDropPointMinutes").val('');
                                $("#interimPoint1RouteId").val('');
                                $("#interimPoint2RouteId").val('');
                                $("#interimPoint3RouteId").val('');
                                $("#interimPoint4RouteId").val('');
                                $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val('');
                                $("#ptpTotalHours").val('');
                                $("#ptpTotalMinutes").val('');
                                $('#ptpPickupPoint').focus();
                                $("#ptpRateWithReefer").val('');
                                $("#ptpRateWithoutReefer").val('');
                            } else {
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    $("#ptpPickupPoint").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#ptpPickupPoint').val(tmp[0]);
                    $itemrow.find('#ptpPickupPointId').val(tmp[1]);
                    $itemrow.find('#interimPoint1').focus();
                    $("#interimPoint1").val('');
                    $("#interimPointId1").val('');
                    $("#interimPoint2").val('');
                    $("#interimPointId2").val('');
                    $("#interimPoint3").val('');
                    $("#interimPointId3").val('');
                    $("#interimPoint4").val('');
                    $("#interimPointId4").val('');
                    $("#ptpDropPoint").val('');
                    $("#ptpDropPointId").val('');
                    $("#interimPoint1Km").val('');
                    $("#interimPoint2Km").val('');
                    $("#interimPoint3Km").val('');
                    $("#interimPoint4Km").val('');
                    $("#ptpDropPointKm").val('');
                    $("#interimPoint1Hrs").val('');
                    $("#interimPoint2Hrs").val('');
                    $("#interimPoint3Hrs").val('');
                    $("#interimPoint4Hrs").val('');
                    $("#ptpDropPointHrs").val('');
                    $("#interimPoint1Minutes").val('');
                    $("#interimPoint2Minutes").val('');
                    $("#interimPoint3Minutes").val('');
                    $("#interimPoint4Minutes").val('');
                    $("#ptpDropPointMinutes").val('');
                    $("#interimPoint1RouteId").val('');
                    $("#interimPoint2RouteId").val('');
                    $("#interimPoint3RouteId").val('');
                    $("#interimPoint4RouteId").val('');
                    $("#ptpDropPointRouteId").val('');
                    $("#ptpTotalKm").val('');
                    $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val('');
                    $("#ptpRateWithReefer").val('');
                    $("#ptpRateWithoutReefer").val('');
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#interimPoint1').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint3: document.getElementById("interimPointId3").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                alert("Invalid Route Point");
                                $("#interimPoint1").val('');
                                $("#interimPointId1").val('');
                                $("#interimPoint2").val('');
                                $("#interimPointId2").val('');
                                $("#interimPoint3").val('');
                                $("#interimPointId3").val('');
                                $("#interimPoint4").val('');
                                $("#interimPointId4").val('');
                                $("#ptpDropPoint").val('');
                                $("#ptpDropPointId").val('');
                                $("#interimPoint1Km").val('');
                                $("#interimPoint2Km").val('');
                                $("#interimPoint3Km").val('');
                                $("#interimPoint4Km").val('');
                                $("#ptpDropPointKm").val('');
                                $("#interimPoint1Hrs").val('');
                                $("#interimPoint2Hrs").val('');
                                $("#interimPoint3Hrs").val('');
                                $("#interimPoint4Hrs").val('');
                                $("#ptpDropPointHrs").val('');
                                $("#interimPoint1Minutes").val('');
                                $("#interimPoint2Minutes").val('');
                                $("#interimPoint3Minutes").val('');
                                $("#interimPoint4Minutes").val('');
                                $("#ptpDropPointMinutes").val('');
                                $("#interimPoint1RouteId").val('');
                                $("#interimPoint2RouteId").val('');
                                $("#interimPoint3RouteId").val('');
                                $("#interimPoint4RouteId").val('');
                                $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val('');
                                $("#ptpTotalHours").val('');
                                $("#ptpTotalMinutes").val('');
                                $('#interimPoint1').focus();
                                $("#ptpRateWithReefer").val('');
                                $("#ptpRateWithoutReefer").val('');
                            } else {
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#interimPoint1").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#interimPoint1').val(tmp[0]);
                    $itemrow.find('#interimPointId1').val(tmp[1]);
                    $itemrow.find('#interimPoint1Km').val(tmp[2]);
                    $itemrow.find('#interimPoint1Hrs').val(tmp[3]);
                    $itemrow.find('#interimPoint1Minutes').val(tmp[4]);
                    $itemrow.find('#interimPoint1RouteId').val(tmp[5]);
                    $itemrow.find('#interimPoint2').focus();
                    $("#interimPoint2").val('');
                    $("#interimPointId2").val('');
                    $("#interimPoint3").val('');
                    $("#interimPointId3").val('');
                    $("#interimPoint4").val('');
                    $("#interimPointId4").val('');
                    $("#ptpDropPoint").val('');
                    $("#ptpDropPointId").val('');
                    $("#interimPoint2Km").val('');
                    $("#interimPoint3Km").val('');
                    $("#interimPoint4Km").val('');
                    $("#ptpDropPointKm").val('');
                    $("#interimPoint2Hrs").val('');
                    $("#interimPoint3Hrs").val('');
                    $("#interimPoint4Hrs").val('');
                    $("#ptpDropPointHrs").val('');
                    $("#interimPoint2Minutes").val('');
                    $("#interimPoint3Minutes").val('');
                    $("#interimPoint4Minutes").val('');
                    $("#ptpDropPointMinutes").val('');
                    $("#interimPoint2RouteId").val('');
                    $("#interimPoint3RouteId").val('');
                    $("#interimPoint4RouteId").val('');
                    $("#ptpDropPointRouteId").val('');
                    $("#ptpTotalKm").val('');
                    $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val('');
                    $("#ptpRateWithReefer").val('');
                    $("#ptpRateWithoutReefer").val('');
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#interimPoint2').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint3: document.getElementById("interimPointId3").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                alert("Invalid Route Point");
                                $("#interimPoint2").val('');
                                $("#interimPointId2").val('');
                                $("#interimPoint3").val('');
                                $("#interimPointId3").val('');
                                $("#interimPoint4").val('');
                                $("#interimPointId4").val('');
                                $("#ptpDropPoint").val('');
                                $("#ptpDropPointId").val('');
                                $("#interimPoint2Km").val('');
                                $("#interimPoint3Km").val('');
                                $("#interimPoint4Km").val('');
                                $("#ptpDropPointKm").val('');
                                $("#interimPoint2Hrs").val('');
                                $("#interimPoint3Hrs").val('');
                                $("#interimPoint4Hrs").val('');
                                $("#ptpDropPointHrs").val('');
                                $("#interimPoint2Minutes").val('');
                                $("#interimPoint3Minutes").val('');
                                $("#interimPoint4Minutes").val('');
                                $("#ptpDropPointMinutes").val('');
                                $("#interimPoint2RouteId").val('');
                                $("#interimPoint3RouteId").val('');
                                $("#interimPoint4RouteId").val('');
                                $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val('');
                                $("#ptpTotalHours").val('');
                                $("#ptpTotalMinutes").val('');
                                $('#interimPoint2').focus();
                                $("#ptpRateWithReefer").val('');
                                $("#ptpRateWithoutReefer").val('');
                            } else {
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#interimPoint2").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#interimPoint2').val(tmp[0]);
                    $itemrow.find('#interimPointId2').val(tmp[1]);
                    $itemrow.find('#interimPoint2Km').val(tmp[2]);
                    $itemrow.find('#interimPoint2Hrs').val(tmp[3]);
                    $itemrow.find('#interimPoint2Minutes').val(tmp[4]);
                    $itemrow.find('#interimPoint2RouteId').val(tmp[5]);
                    $itemrow.find('#interimPoint3').focus();
                    $("#interimPoint3").val('');
                    $("#interimPointId3").val('');
                    $("#interimPoint4").val('');
                    $("#interimPointId4").val('');
                    $("#ptpDropPoint").val('');
                    $("#ptpDropPointId").val('');
                    $("#interimPoint3Km").val('');
                    $("#interimPoint4Km").val('');
                    $("#ptpDropPointKm").val('');
                    $("#interimPoint3Hrs").val('');
                    $("#interimPoint4Hrs").val('');
                    $("#ptpDropPointHrs").val('');
                    $("#interimPoint3Minutes").val('');
                    $("#interimPoint4Minutes").val('');
                    $("#ptpDropPointMinutes").val('');
                    $("#interimPoint3RouteId").val('');
                    $("#interimPoint4RouteId").val('');
                    $("#ptpDropPointRouteId").val('');
                    $("#ptpTotalKm").val('');
                    $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val('');
                    $("#ptpRateWithReefer").val('');
                    $("#ptpRateWithoutReefer").val('');
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#interimPoint3').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                alert("Invalid Route Point");
                                $("#interimPoint3").val('');
                                $("#interimPointId3").val('');
                                $("#interimPoint4").val('');
                                $("#interimPointId4").val('');
                                $("#ptpDropPoint").val('');
                                $("#ptpDropPointId").val('');
                                $("#interimPoint3Km").val('');
                                $("#interimPoint4Km").val('');
                                $("#ptpDropPointKm").val('');
                                $("#interimPoint3Hrs").val('');
                                $("#interimPoint4Hrs").val('');
                                $("#ptpDropPointHrs").val('');
                                $("#interimPoint3Minutes").val('');
                                $("#interimPoint4Minutes").val('');
                                $("#ptpDropPointMinutes").val('');
                                $("#interimPoint3RouteId").val('');
                                $("#interimPoint4RouteId").val('');
                                $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val('');
                                $("#ptpTotalHours").val('');
                                $("#ptpRateWithReefer").val('');
                                $("#ptpRateWithoutReefer").val('');
                                $("#ptpTotalMinutes").val('');
                                $('#interimPoint3').focus();
                            } else {
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#interimPoint3").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#interimPoint3').val(tmp[0]);
                    $itemrow.find('#interimPointId3').val(tmp[1]);
                    $itemrow.find('#interimPoint3Km').val(tmp[2]);
                    $itemrow.find('#interimPoint3Hrs').val(tmp[3]);
                    $itemrow.find('#interimPoint3Minutes').val(tmp[4]);
                    $itemrow.find('#interimPoint3RouteId').val(tmp[5]);
                    $itemrow.find('#interimPoint4').focus();
                    $("#interimPoint4").val('');
                    $("#interimPointId4").val('');
                    $("#ptpDropPoint").val('');
                    $("#ptpDropPointId").val('');
                    $("#interimPoint4Km").val('');
                    $("#ptpDropPointKm").val('');
                    $("#interimPoint4Hrs").val('');
                    $("#ptpDropPointHrs").val('');
                    $("#interimPoint4Minutes").val('');
                    $("#ptpDropPointMinutes").val('');
                    $("#interimPoint4RouteId").val('');
                    $("#ptpDropPointRouteId").val('');
                    $("#ptpTotalKm").val('');
                    $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val('');
                    $("#ptpRateWithReefer").val('');
                    $("#ptpRateWithoutReefer").val('');
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#interimPoint4').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint3: document.getElementById("interimPointId3").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                alert("Invalid Route Point");
                                $("#interimPoint4").val('');
                                $("#interimPointId4").val('');
                                $("#ptpDropPoint").val('');
                                $("#ptpDropPointId").val('');
                                $("#interimPoint4Km").val('');
                                $("#ptpDropPointKm").val('');
                                $("#interimPoint4Hrs").val('');
                                $("#ptpDropPointHrs").val('');
                                $("#interimPoint4Minutes").val('');
                                $("#ptpDropPointMinutes").val('');
                                $("#interimPoint4RouteId").val('');
                                $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val('');
                                $("#ptpTotalHours").val('');
                                $("#ptpRateWithReefer").val('');
                                $("#ptpRateWithoutReefer").val('');
                                $("#ptpTotalMinutes").val('');
                                $('#interimPoint4').focus();
                            } else {
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#interimPoint4").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#interimPoint4').val(tmp[0]);
                    $itemrow.find('#interimPointId4').val(tmp[1]);
                    $itemrow.find('#interimPoint4Km').val(tmp[2]);
                    $itemrow.find('#interimPoint4Hrs').val(tmp[3]);
                    $itemrow.find('#interimPoint4Minutes').val(tmp[4]);
                    $itemrow.find('#interimPoint4RouteId').val(tmp[5]);
                    $itemrow.find('#ptpDropPoint').focus();
                    $("#ptpDropPoint").val('');
                    $("#ptpDropPointId").val('');
                    $("#ptpDropPointKm").val('');
                    $("#ptpDropPointHrs").val('');
                    $("#ptpDropPointMinutes").val('');
                    $("#ptpDropPointRouteId").val('');
                    $("#ptpTotalKm").val('');
                    $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val('');
                    $("#ptpRateWithReefer").val('');
                    $("#ptpRateWithoutReefer").val('');
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#ptpDropPoint').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint3: document.getElementById("interimPointId3").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                alert("Invalid Route Point");
                                $("#ptpDropPoint").val('');
                                $("#ptpDropPointId").val('');
                                $("#ptpDropPointKm").val('');
                                $("#ptpDropPointHrs").val('');
                                $("#ptpDropPointMinutes").val('');
                                $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val('');
                                $("#ptpTotalHours").val('');
                                $("#ptpTotalMinutes").val('');
                                $("#ptpRateWithReefer").val('');
                                $("#ptpRateWithoutReefer").val('');
                                $('#ptpDropPoint').focus();
                            } else {
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#ptpDropPoint").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#ptpDropPoint').val(tmp[0]);
                    $itemrow.find('#ptpDropPointId').val(tmp[1]);
                    $itemrow.find('#ptpDropPointKm').val(tmp[2]);
                    $itemrow.find('#ptpDropPointHrs').val(tmp[3]);
                    $itemrow.find('#ptpDropPointMinutes').val(tmp[4]);
                    $itemrow.find('#ptpDropPointRouteId').val(tmp[5]);
                    $("#ptpRateWithReefer").val('');
                    $("#ptpRateWithoutReefer").val('');
                    if (document.getElementById("interimPoint1Km").value != '') {
                        var interimPoint1Km = document.getElementById("interimPoint1Km").value;
                    } else {
                        var interimPoint1Km = 0;
                    }
                    if (document.getElementById("interimPoint2Km").value != '') {
                        var interimPoint2Km = document.getElementById("interimPoint2Km").value;
                    } else {
                        var interimPoint2Km = 0;
                    }
                    if (document.getElementById("interimPoint3Km").value != '') {
                        var interimPoint3Km = document.getElementById("interimPoint3Km").value;
                    } else {
                        var interimPoint3Km = 0;
                    }
                    if (document.getElementById("interimPoint4Km").value != '') {
                        var interimPoint4Km = document.getElementById("interimPoint4Km").value;
                    } else {
                        var interimPoint4Km = 0;
                    }

                    if (document.getElementById("interimPoint1Hrs").value != '') {
                        var interimPoint1Hrs = document.getElementById("interimPoint1Hrs").value;
                    } else {
                        var interimPoint1Hrs = 0;
                    }
                    if (document.getElementById("interimPoint2Hrs").value != '') {
                        var interimPoint2Hrs = document.getElementById("interimPoint2Hrs").value;
                    } else {
                        var interimPoint2Hrs = 0;
                    }
                    if (document.getElementById("interimPoint3Hrs").value != '') {
                        var interimPoint3Hrs = document.getElementById("interimPoint3Hrs").value;
                    } else {
                        var interimPoint3Hrs = 0;
                    }
                    if (document.getElementById("interimPoint4Hrs").value != '') {
                        var interimPoint4Hrs = document.getElementById("interimPoint4Hrs").value;
                    } else {
                        var interimPoint4Hrs = 0;
                    }

                    if (document.getElementById("interimPoint1Minutes").value != '') {
                        var interimPoint1Minutes = document.getElementById("interimPoint1Minutes").value;
                    } else {
                        var interimPoint1Minutes = 0;
                    }
                    if (document.getElementById("interimPoint2Minutes").value != '') {
                        var interimPoint2Minutes = document.getElementById("interimPoint2Minutes").value;
                    } else {
                        var interimPoint2Minutes = 0;
                    }
                    if (document.getElementById("interimPoint3Minutes").value != '') {
                        var interimPoint3Minutes = document.getElementById("interimPoint3Minutes").value;
                    } else {
                        var interimPoint3Minutes = 0;
                    }
                    if (document.getElementById("interimPoint4Minutes").value != '') {
                        var interimPoint4Minutes = document.getElementById("interimPoint4Minutes").value;
                    } else {
                        var interimPoint4Minutes = 0;
                    }

                    var totalKm = parseFloat(tmp[2]) + parseFloat(interimPoint1Km) + parseFloat(interimPoint2Km) + parseFloat(interimPoint3Km) + parseFloat(interimPoint4Km);
                    $itemrow.find('#ptpTotalKm').val(totalKm);
                    var totalHrs = parseInt(tmp[3]) + parseInt(interimPoint1Hrs) + parseInt(interimPoint2Hrs) + parseInt(interimPoint3Hrs) + parseInt(interimPoint4Hrs);
                    $itemrow.find('#ptpTotalHours').val(totalHrs);
                    var totalMinutes = parseInt(tmp[4]) + parseInt(interimPoint1Minutes) + parseInt(interimPoint2Minutes) + parseInt(interimPoint3Minutes) + parseInt(interimPoint4Minutes);
                    $itemrow.find('#ptpTotalMinutes').val(totalMinutes);
                    $itemrow.find('#ptpRateWithReefer').focus();
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
        });</script>


</c:if>
<c:if test="${billingTypeId == 4}">
    <script>
        $(document).ready(function() {
            // Get the table object to use for adding a row at the end of the table
            var $itemsTable = $('#itemsTable');
            // Create an Array to for the table row. ** Just to make things a bit easier to read.

            var rowTemp = [
                '<tr class="item-row">',
                '<td><a id="deleteRow"><img src="/throttle/images/icon-minus.png" alt="Remove Item" title="Remove Item"></a></td>',
                '<td><input name="ptpRouteContractCode" id="ptpRouteContractCode" value="" class="form-control"  style="width: 60px;"/></td>',
                '<td><c:if test="${vehicleTypeList != null}"><select name="ptpVehicleTypeId" id="ptpVehicleTypeId"  class="form-control" style="width:250px;height:40px" style="width:235px" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></td>',
                '<td><input name="ptpPickupPoint" value="" class="form-control" id="ptpPickupPoint"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="ptpPickupPointId" value="" class="form-control" id="ptpPickupPointId"  style="width: 90px;"/></td>',
                '<td><input name="interimPoint1" value="" class="form-control" id="interimPoint1"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId1" value="" class="form-control" id="interimPointId1"  style="width: 90px;"/><input type="hidden" name="interimPoint1Km" value="" class="form-control" id="interimPoint1Km"  style="width: 90px;"/><input type="hidden" name="interimPoint1Hrs" value="" class="form-control" id="interimPoint1Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint1Minutes" value="" class="form-control" id="interimPoint1Minutes"  style="width: 90px;"/><input type="hidden" name="interimPoint1RouteId" value="" class="form-control" id="interimPoint1RouteId"  style="width: 90px;"/></td>',
                '<td><input name="interimPoint2" value="" class="form-control" id="interimPoint2"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId2" value="" class="form-control" id="interimPointId2"  style="width: 90px;"/><input type="hidden"  name="interimPoint2Km" value="" class="form-control" id="interimPoint2Km"  style="width: 90px;"/><input type="hidden"  name="interimPoint2Hrs" value="" class="form-control" id="interimPoint2Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint2Minutes" value="" class="form-control" id="interimPoint2Minutes"  style="width: 90px;"/><input type="hidden"  name="interimPoint2RouteId" value="" class="form-control" id="interimPoint2RouteId"  style="width: 90px;"/></td>',
                '<td><input name="interimPoint3" value="" class="form-control" id="interimPoint3"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId3" value="" class="form-control" id="interimPointId3"  style="width: 90px;"/><input type="hidden" name="interimPoint3Km" value="" class="form-control" id="interimPoint3Km"  style="width: 90px;"/><input type="hidden" name="interimPoint3Hrs" value="" class="form-control" id="interimPoint3Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint3Minutes" value="" class="form-control" id="interimPoint3Minutes"  style="width: 90px;"/><input type="hidden" name="interimPoint3RouteId" value="" class="form-control" id="interimPoint3RouteId"  style="width: 90px;"/></td>',
                '<td><input name="interimPoint4" value="" class="form-control" id="interimPoint4"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId4" value="" class="form-control" id="interimPointId4"  style="width: 90px;"/><input type="hidden"  name="interimPoint4Km" value="" class="form-control" id="interimPoint4Km"  style="width: 90px;"/><input type="hidden"  name="interimPoint4Hrs" value="" class="form-control" id="interimPoint4Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint4Minutes" value="" class="form-control" id="interimPoint4Minutes"  style="width: 90px;"/><input type="hidden"  name="interimPoint4RouteId" value="" class="form-control" id="interimPoint4RouteId"  style="width: 90px;"/></td>',
                '<td><input name="ptpDropPoint" value="" class="form-control" id="ptpDropPoint"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="ptpDropPointId" value="" class="form-control" id="ptpDropPointId"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointKm" value="" class="form-control" id="ptpDropPointKm"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointHrs" value="" class="form-control" id="ptpDropPointHrs"  style="width: 90px;"/><input type="hidden" name="ptpDropPointMinutes" value="" class="form-control" id="ptpDropPointMinutes"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointRouteId" value="" class="form-control" id="ptpDropPointRouteId"  style="width: 90px;"/></td>',
                '<td><input name="ptpTotalKm" value="" class="form-control" id="ptpTotalKm"  style="width: 90px;" readonly/><input type="hidden" name="ptpTotalHours" value="" class="form-control" id="ptpTotalHours"  style="width: 90px;"/><input type="hidden" name="ptpTotalMinutes" value="" class="form-control" id="ptpTotalMinutes"  style="width: 90px;"/><input type="hidden" name="ptpRateWithReefer" value="0" class="form-control" id="ptpRateWithReefer"  style="width: 90px;" onKeyPress="return onKeyPressBlockCharacters(event);"/><input type="hidden" name="ptpRateWithoutReefer" value="0" class="form-control" id="ptpRateWithoutReefer"  style="width: 90px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '</tr>'
            ].join('');
            var count = 1;
            // Add row to list and allow user to use autocomplete to find items.


            $("#addRow").bind('click', function() {
                count++;
                var $row = $(rowTemp);
                // save reference to inputs within row
                var $ptpRouteContractCode = $row.find('#ptpRouteContractCode');
                var $ptpVehicleTypeId = $row.find('#ptpVehicleTypeId');
                var $ptpPickupPoint = $row.find('#ptpPickupPoint');
                var $ptpPickupPointId = $row.find('#ptpPickupPointId');
                var $interimPoint1 = $row.find('#interimPoint1');
                var $interimPointId1 = $row.find('#interimPointId1');
                var $interimPoint1Km = $row.find('#interimPoint1Km');
                var $interimPoint1Hrs = $row.find('#interimPoint1Hrs');
                var $interimPoint1Minutes = $row.find('#interimPoint1Minutes');
                var $interimPoint1RouteId = $row.find('#interimPoint1RouteId');
                var $interimPoint2 = $row.find('#interimPoint2');
                var $interimPointId2 = $row.find('#interimPointId2');
                var $interimPoint2Km = $row.find('#interimPoint2Km');
                var $interimPoint2Hrs = $row.find('#interimPoint2Hrs');
                var $interimPoint2Minutes = $row.find('#interimPoint2Minutes');
                var $interimPoint2RouteId = $row.find('#interimPoint2RouteId');
                var $interimPoint3 = $row.find('#interimPoint3');
                var $interimPointId3 = $row.find('#interimPointId3');
                var $interimPoint3Km = $row.find('#interimPoint3Km');
                var $interimPoint3Hrs = $row.find('#interimPoint3Hrs');
                var $interimPoint3Minutes = $row.find('#interimPoint3Minutes');
                var $interimPoint3RouteId = $row.find('#interimPoint3RouteId');
                var $interimPoint4 = $row.find('#interimPoint4');
                var $interimPointId4 = $row.find('#interimPointId4');
                var $interimPoint4Km = $row.find('#interimPoint4Km');
                var $interimPoint4Hrs = $row.find('#interimPoint4Hrs');
                var $interimPoint4Minutes = $row.find('#interimPoint4Minutes');
                var $interimPoint4RouteId = $row.find('#interimPoint4RouteId');
                var $ptpDropPoint = $row.find('#ptpDropPoint');
                var $ptpDropPointId = $row.find('#ptpDropPointId');
                var $ptpDropPointKm = $row.find('#ptpDropPointKm');
                var $ptpDropPointHrs = $row.find('#ptpDropPointHrs');
                var $ptpDropPointMinutes = $row.find('#ptpDropPointMinutes');
                var $ptpDropPointRouteId = $row.find('#ptpDropPointRouteId');
                var $ptpTotalKm = $row.find('#ptpTotalKm');
                var $ptpTotalHours = $row.find('#ptpTotalHours');
                var $ptpTotalMinuites = $row.find('#ptpTotalMinutes');
                var $ptpRateWithReefer = $row.find('#ptpRateWithReefer');
                var $ptpRateWithoutReefer = $row.find('#ptpRateWithoutReefer');
                if ($('#ptpRouteContractCode:last').val() !== '') {
                    // apply autocomplete widget to newly created row
                    //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
                    $row.find('#ptpPickupPoint').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
                                        alert("Invalid Route Point");
                                        $row.find('#ptpPickupPoint').val('');
                                        $row.find('#ptpPickupPointId').val('');
                                        $row.find("#interimPoint1").val('');
                                        $row.find("#interimPointId1").val('');
                                        $row.find("#interimPoint2").val('');
                                        $row.find("#interimPointId2").val('');
                                        $row.find("#interimPoint3").val('');
                                        $row.find("#interimPointId3").val('');
                                        $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val('');
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint1Km").val('');
                                        $row.find("#interimPoint2Km").val('');
                                        $row.find("#interimPoint3Km").val('');
                                        $row.find("#interimPoint4Km").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint1Hrs").val('');
                                        $row.find("#interimPoint2Hrs").val('');
                                        $row.find("#interimPoint3Hrs").val('');
                                        $row.find("#interimPoint4Hrs").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint1Minutes").val('');
                                        $row.find("#interimPoint2Minutes").val('');
                                        $row.find("#interimPoint3Minutes").val('');
                                        $row.find("#interimPoint4Minutes").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint1RouteId").val('');
                                        $row.find("#interimPoint2RouteId").val('');
                                        $row.find("#interimPoint3RouteId").val('');
                                        $row.find("#interimPoint4RouteId").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find('#ptpPickupPoint').focus();
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 1,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            // Populate the input fields from the returned values
                            var value = ui.item.Name;
                            //                                alert(value);
                            var tmp = value.split('-');
                            $itemrow.find('#ptpPickupPoint').val(tmp[0]);
                            $itemrow.find('#ptpPickupPointId').val(tmp[1]);
                            $itemrow.find('#interimPoint1').focus();
                            $row.find("#interimPoint1").val('');
                            $row.find("#interimPointId1").val('');
                            $row.find("#interimPoint2").val('');
                            $row.find("#interimPointId2").val('');
                            $row.find("#interimPoint3").val('');
                            $row.find("#interimPointId3").val('');
                            $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val('');
                            $row.find("#ptpDropPoint").val('');
                            $row.find("#ptpDropPointId").val('');
                            $row.find("#interimPoint1Km").val('');
                            $row.find("#interimPoint2Km").val('');
                            $row.find("#interimPoint3Km").val('');
                            $row.find("#interimPoint4Km").val('');
                            $row.find("#ptpDropPointKm").val('');
                            $row.find("#interimPoint1Hrs").val('');
                            $row.find("#interimPoint2Hrs").val('');
                            $row.find("#interimPoint3Hrs").val('');
                            $row.find("#interimPoint4Hrs").val('');
                            $row.find("#ptpDropPointHrs").val('');
                            $row.find("#interimPoint1Minutes").val('');
                            $row.find("#interimPoint2Minutes").val('');
                            $row.find("#interimPoint3Minutes").val('');
                            $row.find("#interimPoint4Minutes").val('');
                            $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#interimPoint1RouteId").val('');
                            $row.find("#interimPoint2RouteId").val('');
                            $row.find("#interimPoint3RouteId").val('');
                            $row.find("#interimPoint4RouteId").val('');
                            $row.find("#ptpDropPointRouteId").val('');
                            $row.find("#ptpTotalKm").val('');
                            $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val('');
                            $row.find("#ptpRateWithReefer").val('');
                            $row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                    $row.find('#interimPoint1').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
                                        alert("Invalid Route Point");
                                        $row.find("#interimPoint1").val('');
                                        $row.find("#interimPointId1").val('');
                                        $row.find("#interimPoint2").val('');
                                        $row.find("#interimPointId2").val('');
                                        $row.find("#interimPoint3").val('');
                                        $row.find("#interimPointId3").val('');
                                        $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val('');
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint1Km").val('');
                                        $row.find("#interimPoint2Km").val('');
                                        $row.find("#interimPoint3Km").val('');
                                        $row.find("#interimPoint4Km").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint1Hrs").val('');
                                        $row.find("#interimPoint2Hrs").val('');
                                        $row.find("#interimPoint3Hrs").val('');
                                        $row.find("#interimPoint4Hrs").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint1Minutes").val('');
                                        $row.find("#interimPoint2Minutes").val('');
                                        $row.find("#interimPoint3Minutes").val('');
                                        $row.find("#interimPoint4Minutes").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint1RouteId").val('');
                                        $row.find("#interimPoint2RouteId").val('');
                                        $row.find("#interimPoint3RouteId").val('');
                                        $row.find("#interimPoint4RouteId").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find('#interimPoint1').focus();
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint1').val(tmp[0]);
                            $itemrow.find('#interimPointId1').val(tmp[1]);
                            $itemrow.find('#interimPoint1Km').val(tmp[2]);
                            $itemrow.find('#interimPoint1Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint1Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint1RouteId').val(tmp[5]);
                            $itemrow.find('#interimPoint2').focus();
                            $row.find("#interimPoint2").val('');
                            $row.find("#interimPointId2").val('');
                            $row.find("#interimPoint3").val('');
                            $row.find("#interimPointId3").val('');
                            $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val('');
                            $row.find("#ptpDropPoint").val('');
                            $row.find("#ptpDropPointId").val('');
                            $row.find("#interimPoint2Km").val('');
                            $row.find("#interimPoint3Km").val('');
                            $row.find("#interimPoint4Km").val('');
                            $row.find("#ptpDropPointKm").val('');
                            $row.find("#interimPoint2Hrs").val('');
                            $row.find("#interimPoint3Hrs").val('');
                            $row.find("#interimPoint4Hrs").val('');
                            $row.find("#ptpDropPointHrs").val('');
                            $row.find("#interimPoint2Minutes").val('');
                            $row.find("#interimPoint3Minutes").val('');
                            $row.find("#interimPoint4Minutes").val('');
                            $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#interimPoint2RouteId").val('');
                            $row.find("#interimPoint3RouteId").val('');
                            $row.find("#interimPoint4RouteId").val('');
                            $row.find("#ptpDropPointRouteId").val('');
                            $row.find("#ptpTotalKm").val('');
                            $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val('');
                            $row.find("#ptpRateWithReefer").val('');
                            $row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                    $row.find('#interimPoint2').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
                                        alert("Invalid Route Point");
                                        $row.find("#interimPoint2").val('');
                                        $row.find("#interimPointId2").val('');
                                        $row.find("#interimPoint3").val('');
                                        $row.find("#interimPointId3").val('');
                                        $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val('');
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint2Km").val('');
                                        $row.find("#interimPoint3Km").val('');
                                        $row.find("#interimPoint4Km").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint2Hrs").val('');
                                        $row.find("#interimPoint3Hrs").val('');
                                        $row.find("#interimPoint4Hrs").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint2Minutes").val('');
                                        $row.find("#interimPoint3Minutes").val('');
                                        $row.find("#interimPoint4Minutes").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint2RouteId").val('');
                                        $row.find("#interimPoint3RouteId").val('');
                                        $row.find("#interimPoint4RouteId").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find('#interimPoint2').focus();
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint2').val(tmp[0]);
                            $itemrow.find('#interimPointId2').val(tmp[1]);
                            $itemrow.find('#interimPoint2Km').val(tmp[2]);
                            $itemrow.find('#interimPoint2Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint2Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint2RouteId').val(tmp[5]);
                            $itemrow.find('#interimPoint3').focus();
                            $row.find("#interimPoint3").val('');
                            $row.find("#interimPointId3").val('');
                            $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val('');
                            $row.find("#ptpDropPoint").val('');
                            $row.find("#ptpDropPointId").val('');
                            $row.find("#interimPoint3Km").val('');
                            $row.find("#interimPoint4Km").val('');
                            $row.find("#ptpDropPointKm").val('');
                            $row.find("#interimPoint3Hrs").val('');
                            $row.find("#interimPoint4Hrs").val('');
                            $row.find("#ptpDropPointHrs").val('');
                            $row.find("#interimPoint3Minutes").val('');
                            $row.find("#interimPoint4Minutes").val('');
                            $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#interimPoint3RouteId").val('');
                            $row.find("#interimPoint4RouteId").val('');
                            $row.find("#ptpDropPointRouteId").val('');
                            $row.find("#ptpTotalKm").val('');
                            $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val('');
                            $row.find("#ptpRateWithReefer").val('');
                            $row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                    $row.find('#interimPoint3').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
                                        alert("Invalid Route Point");
                                        $row.find("#interimPoint3").val('');
                                        $row.find("#interimPointId3").val('');
                                        $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val('');
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint3Km").val('');
                                        $row.find("#interimPoint4Km").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint3Hrs").val('');
                                        $row.find("#interimPoint4Hrs").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint3Minutes").val('');
                                        $row.find("#interimPoint4Minutes").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint3RouteId").val('');
                                        $row.find("#interimPoint4RouteId").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find('#interimPoint3').focus();
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint3').val(tmp[0]);
                            $itemrow.find('#interimPointId3').val(tmp[1]);
                            $itemrow.find('#interimPoint3Km').val(tmp[2]);
                            $itemrow.find('#interimPoint3Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint3Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint3RouteId').val(tmp[5]);
                            $itemrow.find('#interimPoint4').focus();
                            $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val('');
                            $row.find("#ptpDropPoint").val('');
                            $row.find("#ptpDropPointId").val('');
                            $row.find("#interimPoint4Km").val('');
                            $row.find("#ptpDropPointKm").val('');
                            $row.find("#interimPoint4Hrs").val('');
                            $row.find("#ptpDropPointHrs").val('');
                            $row.find("#interimPoint4Minutes").val('');
                            $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#interimPoint4RouteId").val('');
                            $row.find("#ptpDropPointRouteId").val('');
                            $row.find("#ptpTotalKm").val('');
                            $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val('');
                            $row.find("#ptpRateWithReefer").val('');
                            $row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                    $row.find('#interimPoint4').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
                                        alert("Invalid Route Point");
                                        $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val('');
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint4Km").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint4Hrs").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint4Minutes").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint4RouteId").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find('#interimPoint4').focus();
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint4').val(tmp[0]);
                            $itemrow.find('#interimPointId4').val(tmp[1]);
                            $itemrow.find('#interimPoint4Km').val(tmp[2]);
                            $itemrow.find('#interimPoint4Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint4Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint4RouteId').val(tmp[5]);
                            $itemrow.find('#ptpDropPoint').focus();
                            $row.find("#ptpDropPoint").val('');
                            $row.find("#ptpDropPointId").val('');
                            $row.find("#ptpDropPointKm").val('');
                            $row.find("#ptpDropPointHrs").val('');
                            $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#ptpDropPointRouteId").val('');
                            $row.find("#ptpTotalKm").val('');
                            $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val('');
                            $row.find("#ptpRateWithReefer").val('');
                            $row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                    $row.find('#ptpDropPoint').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
                                        alert("Invalid Route Point");
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                        $row.find('#ptpDropPoint').focus();
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#ptpDropPoint').val(tmp[0]);
                            $itemrow.find('#ptpDropPointId').val(tmp[1]);
                            $itemrow.find('#ptpDropPointKm').val(tmp[2]);
                            $itemrow.find('#ptpDropPointRouteId').val(tmp[5]);
                            $row.find("#ptpRateWithReefer").val('');
                            $row.find("#ptpRateWithoutReefer").val('');
                            if ($row.find('#interimPoint1Km').val() !== '') {
                                var interimPoint1Km = $row.find('#interimPoint1Km').val();
                            } else {
                                var interimPoint1Km = 0;
                            }
                            if ($row.find('#interimPoint2Km').val() !== '') {
                                var interimPoint2Km = $row.find('#interimPoint2Km').val();
                            } else {
                                var interimPoint2Km = 0;
                            }
                            if ($row.find('#interimPoint3Km').val() !== '') {
                                var interimPoint3Km = $row.find('#interimPoint3Km').val();
                            } else {
                                var interimPoint3Km = 0;
                            }
                            if ($row.find('#interimPoint4Km').val() !== '') {
                                var interimPoint4Km = $row.find('#interimPoint4Km').val();
                            } else {
                                var interimPoint4Km = 0;
                            }


                            if ($row.find('#interimPoint1Hrs').val() !== '') {
                                var interimPoint1Hrs = $row.find('#interimPoint1Hrs').val();
                            } else {
                                var interimPoint1Hrs = 0;
                            }
                            if ($row.find('#interimPoint2Hrs').val() !== '') {
                                var interimPoint2Hrs = $row.find('#interimPoint2Hrs').val();
                            } else {
                                var interimPoint2Hrs = 0;
                            }
                            if ($row.find('#interimPoint3Hrs').val() !== '') {
                                var interimPoint3Hrs = $row.find('#interimPoint3Hrs').val();
                            } else {
                                var interimPoint3Hrs = 0;
                            }
                            if ($row.find('#interimPoint4Hrs').val() !== '') {
                                var interimPoint4Hrs = $row.find('#interimPoint4Hrs').val();
                            } else {
                                var interimPoint4Hrs = 0;
                            }

                            if ($row.find('#interimPoint1Minutes').val() !== '') {
                                var interimPoint1Minutes = $row.find('#interimPoint1Minutes').val();
                            } else {
                                var interimPoint1Minutes = 0;
                            }
                            if ($row.find('#interimPoint2Minutes').val() !== '') {
                                var interimPoint2Minutes = $row.find('#interimPoint2Minutes').val();
                            } else {
                                var interimPoint2Minutes = 0;
                            }
                            if ($row.find('#interimPoint3Minutes').val() !== '') {
                                var interimPoint3Minutes = $row.find('#interimPoint3Minutes').val();
                            } else {
                                var interimPoint3Minutes = 0;
                            }
                            if ($row.find('#interimPoint4Minutes').val() !== '') {
                                var interimPoint4Minutes = $row.find('#interimPoint4Minutes').val();
                            } else {
                                var interimPoint4Minutes = 0;
                            }


                            var totalKm = parseFloat(tmp[2]) + parseFloat(interimPoint1Km) + parseFloat(interimPoint2Km) + parseFloat(interimPoint3Km) + parseFloat(interimPoint4Km);
                            $row.find('#ptpTotalKm').val(totalKm);
                            var totalHrs = parseInt(tmp[3]) + parseInt(interimPoint1Hrs) + parseInt(interimPoint2Hrs) + parseInt(interimPoint3Hrs) + parseInt(interimPoint4Hrs);
                            $row.find('#ptpTotalHours').val(totalHrs);
                            var totalMinutes = parseInt(tmp[4]) + parseInt(interimPoint1Minutes) + parseInt(interimPoint2Minutes) + parseInt(interimPoint3Minutes) + parseInt(interimPoint4Minutes);
                            $row.find('#ptpTotalMinutes').val(totalMinutes);
                            $itemrow.find('#ptpRateWithReefer').focus();
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                    // Add row after the first row in table
                    $('.item-row:last', $itemsTable).after($row);
                    $($ptpRouteContractCode).focus();
                } // End if last itemCode input is empty
                return false;
            });
        }); // End DOM

        // Remove row when clicked
        $("#deleteRow").live('click', function() {
            $(this).parents('.item-row').remove();
            // Hide delete Icon if we only have one row in the list.
            if ($(".item-row").length < 2)
                $("#deleteRow").hide();
        });
        $(document).ready(function() {
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#ptpPickupPoint').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint3: document.getElementById("interimPointId3").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                alert("Invalid Route Point");
                                $('#ptpPickupPoint').val('');
                                $('#ptpPickupPointId').val('');
                                $("#interimPoint1").val('');
                                $("#interimPointId1").val('');
                                $("#interimPoint2").val('');
                                $("#interimPointId2").val('');
                                $("#interimPoint3").val('');
                                $("#interimPointId3").val('');
                                $("#interimPoint4").val('');
                                $("#interimPointId4").val('');
                                $("#ptpDropPoint").val('');
                                $("#ptpDropPointId").val('');
                                $("#interimPoint1Km").val('');
                                $("#interimPoint2Km").val('');
                                $("#interimPoint3Km").val('');
                                $("#interimPoint4Km").val('');
                                $("#ptpDropPointKm").val('');
                                $("#interimPoint1Hrs").val('');
                                $("#interimPoint2Hrs").val('');
                                $("#interimPoint3Hrs").val('');
                                $("#interimPoint4Hrs").val('');
                                $("#ptpDropPointHrs").val('');
                                $("#interimPoint1Minutes").val('');
                                $("#interimPoint2Minutes").val('');
                                $("#interimPoint3Minutes").val('');
                                $("#interimPoint4Minutes").val('');
                                $("#ptpDropPointMinutes").val('');
                                $("#interimPoint1RouteId").val('');
                                $("#interimPoint2RouteId").val('');
                                $("#interimPoint3RouteId").val('');
                                $("#interimPoint4RouteId").val('');
                                $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val('');
                                $("#ptpTotalHours").val('');
                                $("#ptpTotalMinutes").val('');
                                $('#ptpPickupPoint').focus();
                                $("#ptpRateWithReefer").val('');
                                $("#ptpRateWithoutReefer").val('');
                            } else {
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    $("#ptpPickupPoint").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#ptpPickupPoint').val(tmp[0]);
                    $itemrow.find('#ptpPickupPointId').val(tmp[1]);
                    $itemrow.find('#interimPoint1').focus();
                    $("#interimPoint1").val('');
                    $("#interimPointId1").val('');
                    $("#interimPoint2").val('');
                    $("#interimPointId2").val('');
                    $("#interimPoint3").val('');
                    $("#interimPointId3").val('');
                    $("#interimPoint4").val('');
                    $("#interimPointId4").val('');
                    $("#ptpDropPoint").val('');
                    $("#ptpDropPointId").val('');
                    $("#interimPoint1Km").val('');
                    $("#interimPoint2Km").val('');
                    $("#interimPoint3Km").val('');
                    $("#interimPoint4Km").val('');
                    $("#ptpDropPointKm").val('');
                    $("#interimPoint1Hrs").val('');
                    $("#interimPoint2Hrs").val('');
                    $("#interimPoint3Hrs").val('');
                    $("#interimPoint4Hrs").val('');
                    $("#ptpDropPointHrs").val('');
                    $("#interimPoint1Minutes").val('');
                    $("#interimPoint2Minutes").val('');
                    $("#interimPoint3Minutes").val('');
                    $("#interimPoint4Minutes").val('');
                    $("#ptpDropPointMinutes").val('');
                    $("#interimPoint1RouteId").val('');
                    $("#interimPoint2RouteId").val('');
                    $("#interimPoint3RouteId").val('');
                    $("#interimPoint4RouteId").val('');
                    $("#ptpDropPointRouteId").val('');
                    $("#ptpTotalKm").val('');
                    $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val('');
                    $("#ptpRateWithReefer").val('');
                    $("#ptpRateWithoutReefer").val('');
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#interimPoint1').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint3: document.getElementById("interimPointId3").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                alert("Invalid Route Point");
                                $("#interimPoint1").val('');
                                $("#interimPointId1").val('');
                                $("#interimPoint2").val('');
                                $("#interimPointId2").val('');
                                $("#interimPoint3").val('');
                                $("#interimPointId3").val('');
                                $("#interimPoint4").val('');
                                $("#interimPointId4").val('');
                                $("#ptpDropPoint").val('');
                                $("#ptpDropPointId").val('');
                                $("#interimPoint1Km").val('');
                                $("#interimPoint2Km").val('');
                                $("#interimPoint3Km").val('');
                                $("#interimPoint4Km").val('');
                                $("#ptpDropPointKm").val('');
                                $("#interimPoint1Hrs").val('');
                                $("#interimPoint2Hrs").val('');
                                $("#interimPoint3Hrs").val('');
                                $("#interimPoint4Hrs").val('');
                                $("#ptpDropPointHrs").val('');
                                $("#interimPoint1Minutes").val('');
                                $("#interimPoint2Minutes").val('');
                                $("#interimPoint3Minutes").val('');
                                $("#interimPoint4Minutes").val('');
                                $("#ptpDropPointMinutes").val('');
                                $("#interimPoint1RouteId").val('');
                                $("#interimPoint2RouteId").val('');
                                $("#interimPoint3RouteId").val('');
                                $("#interimPoint4RouteId").val('');
                                $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val('');
                                $("#ptpTotalHours").val('');
                                $("#ptpTotalMinutes").val('');
                                $('#interimPoint1').focus();
                                $("#ptpRateWithReefer").val('');
                                $("#ptpRateWithoutReefer").val('');
                            } else {
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#interimPoint1").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#interimPoint1').val(tmp[0]);
                    $itemrow.find('#interimPointId1').val(tmp[1]);
                    $itemrow.find('#interimPoint1Km').val(tmp[2]);
                    $itemrow.find('#interimPoint1Hrs').val(tmp[3]);
                    $itemrow.find('#interimPoint1Minutes').val(tmp[4]);
                    $itemrow.find('#interimPoint1RouteId').val(tmp[5]);
                    $itemrow.find('#interimPoint2').focus();
                    $("#interimPoint2").val('');
                    $("#interimPointId2").val('');
                    $("#interimPoint3").val('');
                    $("#interimPointId3").val('');
                    $("#interimPoint4").val('');
                    $("#interimPointId4").val('');
                    $("#ptpDropPoint").val('');
                    $("#ptpDropPointId").val('');
                    $("#interimPoint2Km").val('');
                    $("#interimPoint3Km").val('');
                    $("#interimPoint4Km").val('');
                    $("#ptpDropPointKm").val('');
                    $("#interimPoint2Hrs").val('');
                    $("#interimPoint3Hrs").val('');
                    $("#interimPoint4Hrs").val('');
                    $("#ptpDropPointHrs").val('');
                    $("#interimPoint2Minutes").val('');
                    $("#interimPoint3Minutes").val('');
                    $("#interimPoint4Minutes").val('');
                    $("#ptpDropPointMinutes").val('');
                    $("#interimPoint2RouteId").val('');
                    $("#interimPoint3RouteId").val('');
                    $("#interimPoint4RouteId").val('');
                    $("#ptpDropPointRouteId").val('');
                    $("#ptpTotalKm").val('');
                    $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val('');
                    $("#ptpRateWithReefer").val('');
                    $("#ptpRateWithoutReefer").val('');
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#interimPoint2').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint3: document.getElementById("interimPointId3").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                alert("Invalid Route Point");
                                $("#interimPoint2").val('');
                                $("#interimPointId2").val('');
                                $("#interimPoint3").val('');
                                $("#interimPointId3").val('');
                                $("#interimPoint4").val('');
                                $("#interimPointId4").val('');
                                $("#ptpDropPoint").val('');
                                $("#ptpDropPointId").val('');
                                $("#interimPoint2Km").val('');
                                $("#interimPoint3Km").val('');
                                $("#interimPoint4Km").val('');
                                $("#ptpDropPointKm").val('');
                                $("#interimPoint2Hrs").val('');
                                $("#interimPoint3Hrs").val('');
                                $("#interimPoint4Hrs").val('');
                                $("#ptpDropPointHrs").val('');
                                $("#interimPoint2Minutes").val('');
                                $("#interimPoint3Minutes").val('');
                                $("#interimPoint4Minutes").val('');
                                $("#ptpDropPointMinutes").val('');
                                $("#interimPoint2RouteId").val('');
                                $("#interimPoint3RouteId").val('');
                                $("#interimPoint4RouteId").val('');
                                $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val('');
                                $("#ptpTotalHours").val('');
                                $("#ptpTotalMinutes").val('');
                                $('#interimPoint2').focus();
                                $("#ptpRateWithReefer").val('');
                                $("#ptpRateWithoutReefer").val('');
                            } else {
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#interimPoint2").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#interimPoint2').val(tmp[0]);
                    $itemrow.find('#interimPointId2').val(tmp[1]);
                    $itemrow.find('#interimPoint2Km').val(tmp[2]);
                    $itemrow.find('#interimPoint2Hrs').val(tmp[3]);
                    $itemrow.find('#interimPoint2Minutes').val(tmp[4]);
                    $itemrow.find('#interimPoint2RouteId').val(tmp[5]);
                    $itemrow.find('#interimPoint3').focus();
                    $("#interimPoint3").val('');
                    $("#interimPointId3").val('');
                    $("#interimPoint4").val('');
                    $("#interimPointId4").val('');
                    $("#ptpDropPoint").val('');
                    $("#ptpDropPointId").val('');
                    $("#interimPoint3Km").val('');
                    $("#interimPoint4Km").val('');
                    $("#ptpDropPointKm").val('');
                    $("#interimPoint3Hrs").val('');
                    $("#interimPoint4Hrs").val('');
                    $("#ptpDropPointHrs").val('');
                    $("#interimPoint3Minutes").val('');
                    $("#interimPoint4Minutes").val('');
                    $("#ptpDropPointMinutes").val('');
                    $("#interimPoint3RouteId").val('');
                    $("#interimPoint4RouteId").val('');
                    $("#ptpDropPointRouteId").val('');
                    $("#ptpTotalKm").val('');
                    $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val('');
                    $("#ptpRateWithReefer").val('');
                    $("#ptpRateWithoutReefer").val('');
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#interimPoint3').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                alert("Invalid Route Point");
                                $("#interimPoint3").val('');
                                $("#interimPointId3").val('');
                                $("#interimPoint4").val('');
                                $("#interimPointId4").val('');
                                $("#ptpDropPoint").val('');
                                $("#ptpDropPointId").val('');
                                $("#interimPoint3Km").val('');
                                $("#interimPoint4Km").val('');
                                $("#ptpDropPointKm").val('');
                                $("#interimPoint3Hrs").val('');
                                $("#interimPoint4Hrs").val('');
                                $("#ptpDropPointHrs").val('');
                                $("#interimPoint3Minutes").val('');
                                $("#interimPoint4Minutes").val('');
                                $("#ptpDropPointMinutes").val('');
                                $("#interimPoint3RouteId").val('');
                                $("#interimPoint4RouteId").val('');
                                $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val('');
                                $("#ptpTotalHours").val('');
                                $("#ptpRateWithReefer").val('');
                                $("#ptpRateWithoutReefer").val('');
                                $("#ptpTotalMinutes").val('');
                                $('#interimPoint3').focus();
                            } else {
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#interimPoint3").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#interimPoint3').val(tmp[0]);
                    $itemrow.find('#interimPointId3').val(tmp[1]);
                    $itemrow.find('#interimPoint3Km').val(tmp[2]);
                    $itemrow.find('#interimPoint3Hrs').val(tmp[3]);
                    $itemrow.find('#interimPoint3Minutes').val(tmp[4]);
                    $itemrow.find('#interimPoint3RouteId').val(tmp[5]);
                    $itemrow.find('#interimPoint4').focus();
                    $("#interimPoint4").val('');
                    $("#interimPointId4").val('');
                    $("#ptpDropPoint").val('');
                    $("#ptpDropPointId").val('');
                    $("#interimPoint4Km").val('');
                    $("#ptpDropPointKm").val('');
                    $("#interimPoint4Hrs").val('');
                    $("#ptpDropPointHrs").val('');
                    $("#interimPoint4Minutes").val('');
                    $("#ptpDropPointMinutes").val('');
                    $("#interimPoint4RouteId").val('');
                    $("#ptpDropPointRouteId").val('');
                    $("#ptpTotalKm").val('');
                    $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val('');
                    $("#ptpRateWithReefer").val('');
                    $("#ptpRateWithoutReefer").val('');
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#interimPoint4').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint3: document.getElementById("interimPointId3").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                alert("Invalid Route Point");
                                $("#interimPoint4").val('');
                                $("#interimPointId4").val('');
                                $("#ptpDropPoint").val('');
                                $("#ptpDropPointId").val('');
                                $("#interimPoint4Km").val('');
                                $("#ptpDropPointKm").val('');
                                $("#interimPoint4Hrs").val('');
                                $("#ptpDropPointHrs").val('');
                                $("#interimPoint4Minutes").val('');
                                $("#ptpDropPointMinutes").val('');
                                $("#interimPoint4RouteId").val('');
                                $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val('');
                                $("#ptpTotalHours").val('');
                                $("#ptpRateWithReefer").val('');
                                $("#ptpRateWithoutReefer").val('');
                                $("#ptpTotalMinutes").val('');
                                $('#interimPoint4').focus();
                            } else {
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#interimPoint4").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#interimPoint4').val(tmp[0]);
                    $itemrow.find('#interimPointId4').val(tmp[1]);
                    $itemrow.find('#interimPoint4Km').val(tmp[2]);
                    $itemrow.find('#interimPoint4Hrs').val(tmp[3]);
                    $itemrow.find('#interimPoint4Minutes').val(tmp[4]);
                    $itemrow.find('#interimPoint4RouteId').val(tmp[5]);
                    $itemrow.find('#ptpDropPoint').focus();
                    $("#ptpDropPoint").val('');
                    $("#ptpDropPointId").val('');
                    $("#ptpDropPointKm").val('');
                    $("#ptpDropPointHrs").val('');
                    $("#ptpDropPointMinutes").val('');
                    $("#ptpDropPointRouteId").val('');
                    $("#ptpTotalKm").val('');
                    $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val('');
                    $("#ptpRateWithReefer").val('');
                    $("#ptpRateWithoutReefer").val('');
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#ptpDropPoint').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint3: document.getElementById("interimPointId3").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                alert("Invalid Route Point");
                                $("#ptpDropPoint").val('');
                                $("#ptpDropPointId").val('');
                                $("#ptpDropPointKm").val('');
                                $("#ptpDropPointHrs").val('');
                                $("#ptpDropPointMinutes").val('');
                                $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val('');
                                $("#ptpTotalHours").val('');
                                $("#ptpTotalMinutes").val('');
                                $("#ptpRateWithReefer").val('');
                                $("#ptpRateWithoutReefer").val('');
                                $('#ptpDropPoint').focus();
                            } else {
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#ptpDropPoint").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#ptpDropPoint').val(tmp[0]);
                    $itemrow.find('#ptpDropPointId').val(tmp[1]);
                    $itemrow.find('#ptpDropPointKm').val(tmp[2]);
                    $itemrow.find('#ptpDropPointHrs').val(tmp[3]);
                    $itemrow.find('#ptpDropPointMinutes').val(tmp[4]);
                    $itemrow.find('#ptpDropPointRouteId').val(tmp[5]);
                    $("#ptpRateWithReefer").val('');
                    $("#ptpRateWithoutReefer").val('');
                    if (document.getElementById("interimPoint1Km").value != '') {
                        var interimPoint1Km = document.getElementById("interimPoint1Km").value;
                    } else {
                        var interimPoint1Km = 0;
                    }
                    if (document.getElementById("interimPoint2Km").value != '') {
                        var interimPoint2Km = document.getElementById("interimPoint2Km").value;
                    } else {
                        var interimPoint2Km = 0;
                    }
                    if (document.getElementById("interimPoint3Km").value != '') {
                        var interimPoint3Km = document.getElementById("interimPoint3Km").value;
                    } else {
                        var interimPoint3Km = 0;
                    }
                    if (document.getElementById("interimPoint4Km").value != '') {
                        var interimPoint4Km = document.getElementById("interimPoint4Km").value;
                    } else {
                        var interimPoint4Km = 0;
                    }

                    if (document.getElementById("interimPoint1Hrs").value != '') {
                        var interimPoint1Hrs = document.getElementById("interimPoint1Hrs").value;
                    } else {
                        var interimPoint1Hrs = 0;
                    }
                    if (document.getElementById("interimPoint2Hrs").value != '') {
                        var interimPoint2Hrs = document.getElementById("interimPoint2Hrs").value;
                    } else {
                        var interimPoint2Hrs = 0;
                    }
                    if (document.getElementById("interimPoint3Hrs").value != '') {
                        var interimPoint3Hrs = document.getElementById("interimPoint3Hrs").value;
                    } else {
                        var interimPoint3Hrs = 0;
                    }
                    if (document.getElementById("interimPoint4Hrs").value != '') {
                        var interimPoint4Hrs = document.getElementById("interimPoint4Hrs").value;
                    } else {
                        var interimPoint4Hrs = 0;
                    }

                    if (document.getElementById("interimPoint1Minutes").value != '') {
                        var interimPoint1Minutes = document.getElementById("interimPoint1Minutes").value;
                    } else {
                        var interimPoint1Minutes = 0;
                    }
                    if (document.getElementById("interimPoint2Minutes").value != '') {
                        var interimPoint2Minutes = document.getElementById("interimPoint2Minutes").value;
                    } else {
                        var interimPoint2Minutes = 0;
                    }
                    if (document.getElementById("interimPoint3Minutes").value != '') {
                        var interimPoint3Minutes = document.getElementById("interimPoint3Minutes").value;
                    } else {
                        var interimPoint3Minutes = 0;
                    }
                    if (document.getElementById("interimPoint4Minutes").value != '') {
                        var interimPoint4Minutes = document.getElementById("interimPoint4Minutes").value;
                    } else {
                        var interimPoint4Minutes = 0;
                    }

                    var totalKm = parseFloat(tmp[2]) + parseFloat(interimPoint1Km) + parseFloat(interimPoint2Km) + parseFloat(interimPoint3Km) + parseFloat(interimPoint4Km);
                    $itemrow.find('#ptpTotalKm').val(totalKm);
                    var totalHrs = parseInt(tmp[3]) + parseInt(interimPoint1Hrs) + parseInt(interimPoint2Hrs) + parseInt(interimPoint3Hrs) + parseInt(interimPoint4Hrs);
                    $itemrow.find('#ptpTotalHours').val(totalHrs);
                    var totalMinutes = parseInt(tmp[4]) + parseInt(interimPoint1Minutes) + parseInt(interimPoint2Minutes) + parseInt(interimPoint3Minutes) + parseInt(interimPoint4Minutes);
                    $itemrow.find('#ptpTotalMinutes').val(totalMinutes);
                    $itemrow.find('#ptpRateWithReefer').focus();
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
        });</script>


</c:if>
<c:if test="${billingTypeId == 2}">
    <script>
        $(document).ready(function() {
            // Get the table object to use for adding a row at the end of the table
            var $itemsTable = $('#itemsTable1');
            // Create an Array to for the table row. ** Just to make things a bit easier to read.
            var count = 0;
            var rowTemp = [
                '<tr class="item-row">',
                '<td><a id="deleteRow"><img src="/throttle/images/icon-minus.png" alt="Remove Item" title="Remove Item"></a><input type="hidden" name="ptpwRouteContractCode" id="ptpwRouteContractCode" value="RC" class="form-control" style="width:120px"/></td>',
                '<td><c:if test="${vehicleTypeList != null}"><select name="ptpwVehicleTypeId" id="ptpwVehicleTypeId"  class="form-control" style="width:250px;height:40px" style="width:235px"><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></td>',

                '<td><input type="hidden" name="ptpwPickupPointId" id="ptpwPickupPointId" value="" class="form-control"  style="width: 120px;"/><input name="ptpwPickupPoint" id="ptpwPickupPoint" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);" /></td>',
                '<td><input name="ptpwDropPoint" id="ptpwDropPoint" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="ptpwDropPointId" id="ptpwDropPointId" value="" class="form-control"  style="width: 120px;"/><input type="hidden" name="ptpwPointRouteId" id="ptpwPointRouteId" value="" class="form-control"  style="width: 120px;"/></td>',
                '<td><input name="minWgtBase" id="minWgtBase" value="0" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>',
                '<td><input name="ptpwTotalKm" id="ptpwTotalKm" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);" readonly/><input type="hidden" name="ptpwTotalHrs" id="ptpwTotalHrs" value="" class="form-control"  style="width: 120px;"/><input type="hidden" name="ptpwTotalMinutes" id="ptpwTotalMinutes" value="" class="form-control"  style="width: 120px;"/></td>',
                '<td><input name="ptpwRateWithReefer" id="ptpwRateWithReefer" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>',
                '<td><input name="ptpwRateWithoutReefer" id="ptpwRateWithoutReefer" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '</tr>'
            ].join('');
            // Add row to list and allow user to use autocomplete to find items.
            $("#addRow1").bind('click', function() {
                var $row = $(rowTemp);
                count++;
                // save reference to inputs within row
                var $ptpwRouteContractCode = $row.find('#ptpwRouteContractCode');
                var $ptpwVehicleTypeId = $row.find('#ptpwVehicleTypeId');
                var $ptpwPickupPointId = $row.find('#ptpwPickupPointId');
                var $ptpwPickupPoint = $row.find('#ptpwPickupPoint');
                var $ptpwDropPoint = $row.find('#ptpwDropPoint');
                var $ptpwDropPointId = $row.find('#ptpwDropPointId');
                var $ptpwPointRouteId = $row.find('#ptpwPointRouteId');
                var $ptpwTotalKm = $row.find('#ptpwTotalKm');
                var $ptpwTotalHrs = $row.find('#ptpwTotalHrs');
                var $ptpwTotalMinutes = $row.find('#ptpwTotalMinutes');
                var $ptpwRateWithReefer = $row.find('#ptpwRateWithReefer');
                var $ptpwRateWithoutReefer = $row.find('#ptpwRateWithoutReefer');
                if ($('#ptpwRouteContractCode:last').val() !== '') {
                    // apply autocomplete widget to newly created row
                    $row.find('#ptpwPickupPoint').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointWeightRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpwDropPoint: $row.find('#ptpwPointRouteId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
                                        alert("Invalid Route Point");
                                        $row.find("#ptpwPickupPoint").val('');
                                        $row.find("#ptpwPickupPointId").val('');
                                        $row.find('#ptpwDropPoint').val('');
                                        $row.find('#ptpwDropPointId').val('');
                                        $row.find('#ptpwTotalKm').val('');
                                        $row.find('#ptpwTotalHrs').val('');
                                        $row.find('#ptpwTotalMinutes').val('');
                                        $row.find('#ptpwPointRouteId').val('');
                                        $row.find('#ptpwRateWithReefer').val('');
                                        $row.find('#ptpwRateWithoutReefer').val('');
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 1,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            // Populate the input fields from the returned values
                            var value = ui.item.Name;
                            //                                alert(value);
                            var tmp = value.split('-');
                            $itemrow.find('#ptpwPickupPoint').val(tmp[0]);
                            $itemrow.find('#ptpwPickupPointId').val(tmp[1]);
                            $itemrow.find('#ptpwDropPoint').val('');
                            $itemrow.find('#ptpwDropPointId').val('');
                            $itemrow.find('#ptpwTotalKm').val('');
                            $itemrow.find('#ptpwTotalHrs').val('');
                            $itemrow.find('#ptpwTotalMinutes').val('');
                            $itemrow.find('#ptpwPointRouteId').val('');
                            $itemrow.find('#ptpwRateWithReefer').val('');
                            $itemrow.find('#ptpwRateWithoutReefer').val('');
                            $itemrow.find('#ptpwDropPoint').focus();
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                    $row.find('#ptpwDropPoint').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointWeightRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpwPickupPoint: $row.find('#ptpwPickupPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
                                        alert("Invalid Route Point");
                                        $row.find("#ptpwDropPoint").val('');
                                        $row.find("#ptpwDropPointId").val('');
                                        $row.find("#ptpwPointRouteId").val('');
                                        $row.find("#ptpwTotalKm").val('');
                                        $row.find("#ptpwTotalHrs").val('');
                                        $row.find("#ptpwTotalMinutes").val('');
                                        $row.find("#ptpwRateWithReefer").val('');
                                        $row.find("#ptpwRateWithoutReefer").val('');
                                        $row.find('#ptpwDropPoint').focus();
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            // Populate the input fields from the returned values
                            var value = ui.item.Name;
                            //                                alert(value);
                            var tmp = value.split('-');
                            $itemrow.find('#ptpwDropPoint').val(tmp[0]);
                            $itemrow.find('#ptpwDropPointId').val(tmp[1]);
                            $itemrow.find('#ptpwTotalKm').val(tmp[2]);
                            $itemrow.find('#ptpwTotalHrs').val(tmp[3]);
                            $itemrow.find('#ptpwTotalMinutes').val(tmp[4]);
                            $itemrow.find('#ptpwPointRouteId').val(tmp[5]);
                            $itemrow.find('#minWgtBase').focus();
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                    // Add row after the first row in table
                    $('.item-row:last', $itemsTable).after($row);
                    $($ptpwRouteContractCode).focus();
                } // End if last itemCode input is empty
                return false;
            });
        }); // End DOM

        // Remove row when clicked
        $("#deleteRow").live('click', function() {
            $(this).parents('.item-row').remove();
            // Hide delete Icon if we only have one row in the list.
            if ($(".item-row").length < 2)
                $("#deleteRow").hide();
        });
        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#ptpwPickupPoint').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointWeightRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpwDropPoint: document.getElementById("ptpwDropPoint").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            //alert(items);
                            if (items == '') {
                                alert("Invalid Route Point");
                                $("#ptpwPickupPoint").val('');
                                $("#ptpwPickupPointId").val('');
                                $('#ptpwDropPoint').val('');
                                $('#ptpwDropPointId').val('');
                                $('#ptpwTotalKm').val('');
                                $('#ptpwTotalHrs').val('');
                                $('#ptpwTotalMinutes').val('');
                                $('#ptpwPointRouteId').val('');
                                $('#ptpwRateWithReefer').val('');
                                $('#ptpwRateWithoutReefer').val('');
                            } else {
                                //alert(items);
                                response(items);
                            }
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    $("#ptpwPickupPoint").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#ptpwPickupPoint').val(tmp[0]);
                    $itemrow.find('#ptpwPickupPointId').val(tmp[1]);
                    if ($itemrow.find('#ptpwDropPoint').val() != '') {
                        $itemrow.find('#ptpwDropPoint').val('');
                        $itemrow.find('#ptpwDropPointId').val('');
                        $itemrow.find('#ptpwTotalKm').val('');
                        $itemrow.find('#ptpwTotalHrs').val('');
                        $itemrow.find('#ptpwTotalMinutes').val('');
                        $itemrow.find('#ptpwPointRouteId').val('');
                        $itemrow.find('#ptpwRateWithReefer').val('');
                        $itemrow.find('#ptpwRateWithoutReefer').val('');
                    }
                    $itemrow.find('#ptpwDropPoint').focus();
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
            $('#ptpwDropPoint').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointWeightRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpwPickupPoint: document.getElementById("ptpwPickupPointId").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                alert("Invalid Route Point");
                                $("#ptpwDropPoint").val('');
                                $("#ptpwDropPointId").val('');
                                $("#ptpwPointRouteId").val('');
                                $("#ptpwTotalKm").val('');
                                $("#ptpwTotalHrs").val('');
                                $("#ptpwTotalMinutes").val('');
                                $("#ptpwRateWithReefer").val('');
                                $("#ptpwRateWithoutReefer").val('');
                                $('#ptpwDropPoint').focus();
                            } else {
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#ptpwDropPoint").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#ptpwDropPoint').val(tmp[0]);
                    $itemrow.find('#ptpwDropPointId').val(tmp[1]);
                    $itemrow.find('#ptpwTotalKm').val(tmp[2]);
                    $itemrow.find('#ptpwTotalHrs').val(tmp[3]);
                    $itemrow.find('#ptpwTotalMinutes').val(tmp[4]);
                    $itemrow.find('#ptpwPointRouteId').val(tmp[5]);
                    $itemrow.find('#ptpwRateWithReefer').focus();
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
        });</script>

</c:if>
<script>
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#customerName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerDetails.do",
                    dataType: "json",
                    data: {
                        customerName: request.term,
                        customerCode: document.getElementById('customerCode').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#customerName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#customerId').val(tmp[0]);
                $itemrow.find('#customerName').val(tmp[1]);
                $itemrow.find('#customerCode').val(tmp[2]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
        $('#customerCode').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerDetails.do",
                    dataType: "json",
                    data: {
                        customerCode: request.term,
                        customerName: document.getElementById('customerName').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#customerCode").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#customerId').val(tmp[0]);
                $itemrow.find('#customerName').val(tmp[1]);
                $itemrow.find('#customerCode').val(tmp[2]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[2] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });</script>


<script type="text/javascript">


    function backPage() {
        document.customerContract.action = '/throttle/handleViewCustomer.do';
        document.customerContract.submit();
    }

    function updatePage(value) {
        document.customerContract.action = "/throttle/saveEditCustomerContract.do";
        document.customerContract.submit();
    }


    function submitPage(value) {

        document.getElementById('billingTypeId').value = value;
        var validate = "";
        //  var validate = customerValidation();
        //  var  customerValidation();
        var a = 0;
        var addMoreRouteValue = $("#addMoreRouteValue").val();
        var ptpRouteContractCode = document.getElementsByName('ptpRouteContractCode');
        var ptpwRouteContractCode = document.getElementsByName('ptpwRouteContractCode');
        if (document.getElementById('creditDays').value == '') {
            alert("please enter the credit days")
            $("#creditDays").focus();
        } else if (document.getElementById('custContactPerson').value == '') {
            alert("please enter the contact person");
            $("#custContactPerson").focus();
        } else if (document.getElementById('custPhone').value == '') {
            alert("please enter the phone no")
            $("#custPhone").focus();
        } else if (document.getElementById('custMobile').value == '') {
            alert("please enter the mobile no");
            $("#custMobile").focus();
        } else if (document.getElementById('custEmail').value == '') {
            alert("please enter the email");
            $("#custEmail").focus();
        } else if (document.getElementById('accountManagerId').value == '' && document.getElementById('accountManager').value != '') {
            alert("please select the valid accountMcanager name");
            $("#accountMcanager").focus();
        } else if (document.getElementById('creditLimit').value == '') {
            alert("please enter the credit limit");
            $("#creditLimit").focus();
        } else if (document.getElementById('contractTo').value == "") {
            alert("Please Select Contract End Date");
            document.getElementById('contractTo').focus();
        } else if (document.getElementById('billingTypeId').value == "1") {
            var routeContractId = document.getElementsByName('routeContractId1');
            var ptpRateWithReefer1 = document.getElementsByName('ptpRateWithReefer1');
            var ptpRateWithoutReefer1 = document.getElementsByName('ptpRateWithoutReefer1');
            var tatTime = document.getElementsByName('tatTime');
            for (var k = 0; k < routeContractId.length; k++) {

                a = k + 1;
                if (ptpRateWithReefer1[k].value == '') {
                    alert("please enter the reefer with reefer amount for row " + a);
                    ptpRateWithReefer1[k].focus();
                    return;
                } else if (ptpRateWithoutReefer1[k].value == '') {
                    alert("please enter the agreedfuel amount for row " + a);
                    ptpRateWithoutReefer1[k].focus();
                    return;
                }
            }
            if (ptpRouteContractCode.length > 0) {
                validate = validatePtpContract(a);
            } else {
                validate = 'true';
            }
            if (validate == 'true') {
                document.customerContract.action = "/throttle/saveEditCustomerContract.do";
                document.customerContract.submit();
            }
        } else if (document.getElementById('billingTypeId').value == "2") {
            var routeContractId = document.getElementsByName('routeContractId2');
            var ptpwRateWithReefer1 = document.getElementsByName('ptpwRateWithReefer1');
            var ptpwRateWithoutReefer1 = document.getElementsByName('ptpwRateWithoutReefer1');
//            alert(routeContractId.length);
            for (var k = 0; k < routeContractId.length; k++) {
                a = k + 1;
                if (ptpwRateWithReefer1[k].value == '') {
                    alert("please enter the reefer with reefer amount for row " + a);
                    ptpwRateWithReefer1[k].focus();
                    return;
                } else if (ptpwRateWithoutReefer1[k].value == '') {
                    alert("please enter the reefer without reefer amount for row " + a);
                    ptpwRateWithoutReefer1[k].focus();
                    return;
                }
            }
            if (ptpwRouteContractCode.length > 0 && addMoreRouteValue == 1) {
                validate = validatePtpwContract(a);
            } else {
                validate = 'true';
            }
            if (validate == 'true') {
//                alert("Edit");
                document.customerContract.action = "/throttle/saveEditCustomerContract.do";
                document.customerContract.submit();
            }
        } else if (document.getElementById('billingTypeId').value == "4") {
            var routeContractId = document.getElementsByName('routeContractId');
            a = routeContractId.length;
            //alert(a);
            if (addMoreRouteValue >= 1) {
                validate = validateFixedContract(a);
            } else {
                validate = 'true';
            }
            if (validate == 'true') {
                document.customerContract.action = "/throttle/saveEditCustomerContract.do";
                document.customerContract.submit();
            }
        } else if (document.getElementById('billingTypeId').value == "3") {
            var contractRateId = document.getElementsByName('contractRateId');
            var vehicleRatePerKm = document.getElementsByName('vehicleRatePerKm');
            var reeferRatePerHour = document.getElementsByName('reeferRatePerHour');
            for (var k = 0; k < contractRateId.length; k++) {
                a = k + 1;
                if (vehicleRatePerKm[k].value == '') {
                    alert("please enter the reefer per km for row " + a);
                    vehicleRatePerKm[k].focus();
                    return;
                } else if (reeferRatePerHour[k].value == '') {
                    alert("please enter the rate per reefer hour fro row " + a);
                    reeferRatePerHour[k].focus();
                    return;
                }
            }
            document.customerContract.action = "/throttle/saveEditCustomerContract.do";
            document.customerContract.submit();
        }

        else if (validate == 'true') {
            document.customerContract.action = "/throttle/saveEditCustomerContract.do";
            document.customerContract.submit();
        }
    }

    function validatePtpContract(a) {
        var returnvalue = "";
        var ptpRouteContractCode = document.getElementsByName('ptpRouteContractCode');
        var ptpVehicleTypeId = document.getElementsByName('ptpVehicleTypeId');
        //Point Name
        var ptpPickupPoint = document.getElementsByName('ptpPickupPoint');
        var interimPoint1 = document.getElementsByName('interimPoint1');
        var interimPoint2 = document.getElementsByName('interimPoint2');
        var interimPoint4 = document.getElementsByName('interimPoint4');
        var interimPoint3 = document.getElementsByName('interimPoint3');
        var ptpDropPoint = document.getElementsByName('ptpDropPoint');
        //Point Id
        var ptpPickupPointId = document.getElementsByName('ptpPickupPointId');
        var interimPointId1 = document.getElementsByName('interimPointId1');
        var interimPointId2 = document.getElementsByName('interimPointId2');
        var interimPointId3 = document.getElementsByName('interimPointId3');
        var interimPointId4 = document.getElementsByName('interimPointId4');
        var ptpDropPointId = document.getElementsByName('ptpDropPointId');
        //Point Km
        var interimPoint1Km = document.getElementsByName('interimPoint1Km');
        var interimPoint2Km = document.getElementsByName('interimPoint2Km');
        var interimPoint3Km = document.getElementsByName('interimPoint3Km');
        var interimPoint4Km = document.getElementsByName('interimPoint4Km');
        var ptpDropPointKm = document.getElementsByName('ptpDropPointKm');
        //Point Hours
        var interimPoint1Hrs = document.getElementsByName('interimPoint1Hrs');
        var interimPoint2Hrs = document.getElementsByName('interimPoint2Hrs');
        var interimPoint3Hrs = document.getElementsByName('interimPoint3Hrs');
        var interimPoint4Hrs = document.getElementsByName('interimPoint4Hrs');
        var ptpDropPointHrs = document.getElementsByName('ptpDropPointHrs');
        //Point Minutes
        var interimPoint1Minutes = document.getElementsByName('interimPoint1Minutes');
        var interimPoint2Minutes = document.getElementsByName('interimPoint2Minutes');
        var interimPoint3Minutes = document.getElementsByName('interimPoint3Minutes');
        var interimPoint4Minutes = document.getElementsByName('interimPoint4Minutes');
        var ptpDropPointMinutes = document.getElementsByName('ptpDropPointMinutes');
        //Point Route Id
        var interimPoint1RouteId = document.getElementsByName('interimPoint1RouteId');
        var interimPoint2RouteId = document.getElementsByName('interimPoint2RouteId');
        var interimPoint3RouteId = document.getElementsByName('interimPoint3RouteId');
        var interimPoint4RouteId = document.getElementsByName('interimPoint4RouteId');
        var ptpDropPointRouteId = document.getElementsByName('ptpDropPointRouteId');
        //Total Km, Total Hours, Total Minutes
        var ptpTotalKm = document.getElementsByName('ptpTotalKm');
        var ptpTotalHours = document.getElementsByName('ptpTotalHours');
        var ptpTotalMinutes = document.getElementsByName('ptpTotalMinutes');
        var tatTime = document.getElementsByName('tatTime');
        //Rate Details
        var ptpRateWithReefer = document.getElementsByName('ptpRateWithReefer');
        var ptpRateWithoutReefer = document.getElementsByName('ptpRateWithoutReefer');
        for (var i = 0; i < ptpRouteContractCode.length; i++) {
            var j = i + 1 + parseInt(a);
            if (ptpRouteContractCode[i].value == '') {
                alert("Please Enter The Valid Route Code for row " + j);
                ptpRouteContractCode[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpVehicleTypeId[i].value == '0') {
                alert("Please Select The vehicle Type for row " + j);
                ptpVehicleTypeId[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpPickupPoint[i].value == '') {
                alert("please select the pickup point for row " + j);
                ptpPickupPoint[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value == '') {
                alert("please select the valid point name for pickup point at row " + j);
                ptpPickupPoint[i].focus();
                returnvalue = 'false';
                ptpPickupPoint[i].value = '';
                interimPoint1[i].value = '';
                interimPoint2[i].value = '';
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                return returnvalue;
            } else if (interimPoint1[i].value != '' && interimPointId1[i].value == '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != '') {
                alert("please select the valid point name for interim point 1 at row " + j);
                interimPoint1[i].focus();
                interimPoint1[i].value = '';
                interimPoint2[i].value = '';
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (interimPoint2[i].value != '' && interimPointId2[i].value == '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != '') {
                alert("please select valid point name for interim point 2 at row " + j);
                interimPoint2[i].focus();
                interimPoint2[i].value = '';
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (interimPoint3[i].value != '' && interimPointId3[i].value == '' && interimPoint2[i].value != '' && interimPointId2[i].value != '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != '') {
                alert("please select valid point name for interim point 3 at row " + j);
                interimPoint3[i].focus();
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (interimPoint4[i].value != '' && interimPointId4[i].value == '' && interimPoint3[i].value != '' && interimPointId3[i].value != '' && interimPoint2[i].value != '' && interimPointId2[i].value != '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != '') {
                alert("please select valid point name for interim point 4 at row " + j);
                interimPoint4[i].focus();
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpDropPoint[i].value != '' && ptpDropPointId[i].value == '' && interimPoint4[i].value != '' && interimPointId4[i].value != '' && interimPoint3[i].value != '' && interimPointId3[i].value != '' && interimPoint2[i].value != '' && interimPointId2[i].value != '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != '') {
                alert("please select valid point name for Drop point at row " + j);
                ptpDropPoint[i].focus();
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpTotalKm[i].value == '') {
                alert("please validate the route points at row " + j);
                ptpDropPoint[i].value = '';
                ptpDropPoint[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpRateWithReefer[i].value == '') {
                alert("Please enter the freight rate with reefer at row " + j);
                ptpRateWithReefer[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpRateWithoutReefer[i].value == '') {
                alert("please enter the freight rate without reefer at row " + j);
                ptpRateWithoutReefer[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (tatTime[i].value == '') {
                alert("please enter the TAT Days at row " + j);
                tatTime[i].focus();
                returnvalue = 'false';
                return returnvalue;
            }

            else {
                returnvalue = 'true';
            }
        }
        return returnvalue;
    }

    function validateFixedContract(a) {
        var returnvalue = "";
        var ptpRouteContractCode = document.getElementsByName('ptpRouteContractCode');
        var ptpVehicleTypeId = document.getElementsByName('ptpVehicleTypeId');
        //Point Name
        var ptpPickupPoint = document.getElementsByName('ptpPickupPoint');
        var interimPoint1 = document.getElementsByName('interimPoint1');
        var interimPoint2 = document.getElementsByName('interimPoint2');
        var interimPoint4 = document.getElementsByName('interimPoint4');
        var interimPoint3 = document.getElementsByName('interimPoint3');
        var ptpDropPoint = document.getElementsByName('ptpDropPoint');
        //Point Id
        var ptpPickupPointId = document.getElementsByName('ptpPickupPointId');
        var interimPointId1 = document.getElementsByName('interimPointId1');
        var interimPointId2 = document.getElementsByName('interimPointId2');
        var interimPointId3 = document.getElementsByName('interimPointId3');
        var interimPointId4 = document.getElementsByName('interimPointId4');
        var ptpDropPointId = document.getElementsByName('ptpDropPointId');
        //Point Km
        var interimPoint1Km = document.getElementsByName('interimPoint1Km');
        var interimPoint2Km = document.getElementsByName('interimPoint2Km');
        var interimPoint3Km = document.getElementsByName('interimPoint3Km');
        var interimPoint4Km = document.getElementsByName('interimPoint4Km');
        var ptpDropPointKm = document.getElementsByName('ptpDropPointKm');
        //Point Hours
        var interimPoint1Hrs = document.getElementsByName('interimPoint1Hrs');
        var interimPoint2Hrs = document.getElementsByName('interimPoint2Hrs');
        var interimPoint3Hrs = document.getElementsByName('interimPoint3Hrs');
        var interimPoint4Hrs = document.getElementsByName('interimPoint4Hrs');
        var ptpDropPointHrs = document.getElementsByName('ptpDropPointHrs');
        //Point Minutes
        var interimPoint1Minutes = document.getElementsByName('interimPoint1Minutes');
        var interimPoint2Minutes = document.getElementsByName('interimPoint2Minutes');
        var interimPoint3Minutes = document.getElementsByName('interimPoint3Minutes');
        var interimPoint4Minutes = document.getElementsByName('interimPoint4Minutes');
        var ptpDropPointMinutes = document.getElementsByName('ptpDropPointMinutes');
        //Point Route Id
        var interimPoint1RouteId = document.getElementsByName('interimPoint1RouteId');
        var interimPoint2RouteId = document.getElementsByName('interimPoint2RouteId');
        var interimPoint3RouteId = document.getElementsByName('interimPoint3RouteId');
        var interimPoint4RouteId = document.getElementsByName('interimPoint4RouteId');
        var ptpDropPointRouteId = document.getElementsByName('ptpDropPointRouteId');
        //Total Km, Total Hours, Total Minutes
        var ptpTotalKm = document.getElementsByName('ptpTotalKm');
        var ptpTotalHours = document.getElementsByName('ptpTotalHours');
        var ptpTotalMinutes = document.getElementsByName('ptpTotalMinutes');
        //Rate Details

        for (var i = 0; i < ptpRouteContractCode.length; i++) {
            var j = i + 1 + parseInt(a);
            if (ptpRouteContractCode[i].value == '') {
                alert("Please Enter The Valid Route Code for row " + j);
                ptpRouteContractCode[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpVehicleTypeId[i].value == '0') {
                alert("Please Select The vehicle Type for row " + j);
                ptpVehicleTypeId[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpPickupPoint[i].value == '') {
                alert("please select the pickup point for row " + j);
                ptpPickupPoint[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value == '') {
                alert("please select the valid point name for pickup point at row " + j);
                ptpPickupPoint[i].focus();
                returnvalue = 'false';
                ptpPickupPoint[i].value = '';
                interimPoint1[i].value = '';
                interimPoint2[i].value = '';
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                return returnvalue;
            } else if (interimPoint1[i].value != '' && interimPointId1[i].value == '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != '') {
                alert("please select the valid point name for interim point 1 at row " + j);
                interimPoint1[i].focus();
                interimPoint1[i].value = '';
                interimPoint2[i].value = '';
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (interimPoint2[i].value != '' && interimPointId2[i].value == '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != '') {
                alert("please select valid point name for interim point 2 at row " + j);
                interimPoint2[i].focus();
                interimPoint2[i].value = '';
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (interimPoint3[i].value != '' && interimPointId3[i].value == '' && interimPoint2[i].value != '' && interimPointId2[i].value != '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != '') {
                alert("please select valid point name for interim point 3 at row " + j);
                interimPoint3[i].focus();
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (interimPoint4[i].value != '' && interimPointId4[i].value == '' && interimPoint3[i].value != '' && interimPointId3[i].value != '' && interimPoint2[i].value != '' && interimPointId2[i].value != '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != '') {
                alert("please select valid point name for interim point 4 at row " + j);
                interimPoint4[i].focus();
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpDropPoint[i].value != '' && ptpDropPointId[i].value == '' && interimPoint4[i].value != '' && interimPointId4[i].value != '' && interimPoint3[i].value != '' && interimPointId3[i].value != '' && interimPoint2[i].value != '' && interimPointId2[i].value != '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != '') {
                alert("please select valid point name for Drop point at row " + j);
                ptpDropPoint[i].focus();
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpTotalKm[i].value == '') {
                alert("please validate the route points at row " + j);
                ptpDropPoint[i].value = '';
                ptpDropPoint[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else {
                returnvalue = 'true';
            }
        }
        return returnvalue;
    }

    function validatePtpwContract(a) {
        var returnvalue = "";
        var ptpwRouteContractCode = document.getElementsByName('ptpwRouteContractCode');
        var ptpwVehicleTypeId = document.getElementsByName('ptpwVehicleTypeId');
        var ptpwPickupPoint = document.getElementsByName('ptpwPickupPoint');
        var ptpwPickupPointId = document.getElementsByName('ptpwPickupPointId');
        var ptpwDropPoint = document.getElementsByName('ptpwDropPoint');
        var ptpwDropPointId = document.getElementsByName('ptpwDropPointId');
        var ptpwTotalKm = document.getElementsByName('ptpwTotalKm');
        var ptpwRateWithReefer = document.getElementsByName('ptpwRateWithReefer');
        var ptpwRateWithoutReefer = document.getElementsByName('ptpwRateWithoutReefer');
        //alert(ptpwRouteContractCode.length);
        for (var i = 0; i < ptpwRouteContractCode.length; i++) {
            //alert("i=="+i)
            var j = i + 1 + parseInt(a);
            if (ptpwRouteContractCode[i].value == '') {
                alert("Please Enter Contract Route Code For Row " + j);
                ptpwRouteContractCode[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpwVehicleTypeId[i].value == '0') {
                alert("Please Select Vehicle Type For Row " + j);
                ptpwVehicleTypeId[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpwPickupPoint[i].value == '') {
                alert("Please Select The Pickup Point For Row " + j);
                ptpwPickupPoint[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpwPickupPoint[i].value != '' && ptpwPickupPointId[i].value == '') {
                alert("Please Select The Pickup Point For Row " + j);
                ptpwPickupPoint[i].focus();
                ptpwPickupPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpwDropPointId[i].value == '') {
                alert("Please Select The Drop Point For Row " + j);
                ptpwDropPoint[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpwDropPoint[i].value != '' && ptpwDropPointId[i] == '') {
                alert("Please Select The Drop Point For Row " + j);
                ptpwDropPoint[i].focus();
                ptpwDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpwTotalKm[i].value == '') {
                alert("Validate The Route Point Details and check Total Km For Row " + j);
                ptpwTotalKm[i].focus();
                c
            } else if (ptpwRateWithReefer[i].value == '') {
                alert("Please enter the amount for rate with reefer for row " + j);
                ptpwRateWithReefer[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpwRateWithoutReefer[i].value == '') {
                alert("Please enter the amount for rate without reefer for row " + j);
                ptpwRateWithoutReefer[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else {
                returnvalue = 'true';
            }
        }
        return returnvalue;
    }
    //        function customerValidation(){
    //        var custContactPerson = document.getElementsByName("custContactPerson");
    //        var custPhone =  document.getElementsByName("custPhone");
    //        var custMobile =  document.getElementsByName("custMobile");
    //        var custEmail = document.getElementsByName("custEmail");
    //        var accountManagerId = document.getElementsByName("accountManagerId");
    //        var accountManager = document.getElementsByName("accountManager");
    //        var creditDays = document.getElementsByName("creditDays");
    //        var creditLimit = document.getElementsByName("creditLimit");
    //        var a = 0;
    //        var validate='true';
    //
    //            if (document.getElementById('custContactPerson').value == '') {
    //                alert("please enter the contact person");
    //                $("#custContactPerson").focus();
    //                validate='false';
    //                return validate;
    //            }else if (document.getElementById('custPhone').value == '') {
    //                alert("please enter the phone no");
    //                $("#custPhone").focus();
    //                validate='false';
    //                return validate;
    //            }else if(document.getElementById('custMobile').value == ''){
    //                alert("please enter the mobile no");
    //                $("#custMobile").focus();
    //                validate='false';
    //                return validate;
    //            }else if (document.getElementById('custEmail').value == '') {
    //                alert("please enter the email");
    //                $("#custEmail").focus();
    //                validate='false';
    //                return validate;
    //            }else if (document.getElementById('accountManagerId').value == '' && document.getElementById('accountManager').value != '') {
    //                alert("please select the valid accountMcanager name");
    //                $("#accountMcanager").focus();
    //                validate=1;
    //                return validate;
    //            }else if (document.getElementById('creditLimit').value == '') {
    //                alert("please enter the credit limit");
    //                $("#creditLimit").focus();
    //                validate='false';
    //                return validate;
    //            }else if (document.getElementById('creditDays').value == '') {
    //                alert("please enter the creditDays");
    //                $("#creditDays").focus();
    //                validate='false';
    //                return validate;
    //            }
    //        return validate;
    //    }
    function customerValidation()
    {
        //    var vehMileage = document.getElementsByName("vehMileage");
        //        var reefMileage =  document.getElementsByName("reefMileage");
        //        var fuelCostPerKms =  document.getElementsByName("fuelCostPerKms");
        //        var fuelCostPerHrs = document.getElementsByName("fuelCostPerHrs");
        //        var tollAmounts = document.getElementsByName("tollAmounts");
        //        var miscCostKm = document.getElementsByName("miscCostKm");
        //        var driverIncenKm = document.getElementsByName("driverIncenKm");
        //        var factor = document.getElementsByName("factor");
        var returnvalue = "";
        if (document.getElementById('custContactPerson').value == '') {
            alert("please enter the contact person")
            $("#custContactPerson").focus();
            returnvalue = 'false';
            return returnvalue;
        } else if (document.getElementById('custPhone').value == '') {
            alert("please enter the phone no")
            $("#custPhone").focus();
            returnvalue = 'false';
            return returnvalue;
        } else if (document.getElementById('custMobile').value == '') {
            alert("please enter the mobile no")
            $("#custMobile").focus();
            returnvalue = 'false';
            return returnvalue;
        } else if (document.getElementById('custEmail').value == '') {
            alert("please enter the email")
            $("#custEmail").focus();
            returnvalue = 'false';
            return returnvalue;
        } else if (document.getElementById('accountManagerId').value == '' && document.getElementById('accountManager').value != '') {
            alert("please select the valid accountant name")
            $("#accountManager").focus();
            returnvalue = 'false';
            return returnvalue;
        } else if (document.getElementById('creditLimit').value == '') {
            alert("please enter the credit limit")
            $("#creditLimit").focus();
            returnvalue = 'false';
            return returnvalue;
        }
        else {
            returnvalue = 'true';
        }
    }

    function  validateFixedKmRateDetails() {
        var sts = true;
        var fixedKmvehicleTypeId = document.getElementsByName("fixedKmvehicleTypeId");
        var vehicleNos = document.getElementsByName("vehicleNos");
        var fixedTotalKm = document.getElementsByName("fixedTotalKm");
        var fixedTotalHm = document.getElementsByName("fixedTotalHm");
        var fixedRateWithReefer = document.getElementsByName("fixedRateWithReefer");
        var extraKmRateWithReefer = document.getElementsByName("extraKmRateWithReefer");
        var extraHmRateWithReefer = document.getElementsByName("extraHmRateWithReefer");
        var fixedRateWithoutReefer = document.getElementsByName("fixedRateWithoutReefer");
        var extraKmRateWithoutReefer = document.getElementsByName("extraKmRateWithoutReefer");
        for (var i = 0; i < fixedKmvehicleTypeId.length; i++) {
            if (fixedKmvehicleTypeId[i].value == '0') {
                alert("Please select vehicle type");
                fixedKmvehicleTypeId[i].focus();
                sts = false;
            } else if (vehicleNos[i].value == '') {
                alert("Please enter vehicle nos");
                vehicleNos[i].focus();
                sts = false;
            } else if (fixedTotalKm[i].value == '') {
                alert("Please enter total km");
                fixedTotalKm[i].focus();
                sts = false;
            } else if (fixedTotalHm[i].value == '') {
                alert("Please enter total hm");
                fixedTotalHm[i].focus();
                sts = false;
            } else if (fixedRateWithReefer[i].value == '') {
                alert("Please enter rate with reefer");
                fixedRateWithReefer[i].focus();
                sts = false;
            } else if (extraKmRateWithReefer[i].value == '') {
                alert("Please enter extra km rate with reefer");
                extraKmRateWithReefer[i].focus();
                sts = false;
            } else if (extraHmRateWithReefer[i].value == '') {
                alert("Please enter extra hm rate with reefer");
                extraHmRateWithReefer[i].focus();
                sts = false;
            } else if (fixedRateWithoutReefer[i].value == '') {
                alert("Please enter rate without reefer");
                fixedRateWithoutReefer[i].focus();
                sts = false;
            } else if (extraKmRateWithoutReefer[i].value == '') {
                alert("Please enter extra km rate without reefer");
                extraKmRateWithoutReefer[i].focus();
                sts = false;
            } else {
                sts = true;
            }
        }
        return sts;
    }

</script>

<script type="text/javascript">
    function checked() {
        if (document.getElementById("editcustomer").checked == true) {
            $("#displayCustomerDetails").show();
        } else {
            $("#displayCustomerDetails").hide();
        }
    }

</script>
<html>
    <!--    <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>JSP Page</title>
        </head>-->
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.EditContract" text="Edit Contract"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
                <li class=""><spring:message code="hrms.label.EditContract" text="Edit Contract"/></li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">

                <body onload="">
                    <form name="customerContract"  method="post" onsubmit="customerValidation();">
                        <%--<%@ include file="/content/common/path.jsp" %>--%>
                        <%@include file="/content/common/message.jsp" %>
                        <br>
                        <input type="hidden" name="custId" id="custId" value="<c:out value="${custId}"/>"/>
                        <input type="hidden" name="contractId" id="contractId" value="<c:out value="${contractId}"/>"/>
                        <input type="hidden" name="billingTypeId" Id="billingTypeId" value="<c:out value="${billingTypeId}"/>"/>

                        <table class="table table-info mb30 table-hover" id="bg" >
                            <thead>

                                <tr>
                                    <th  colspan="4" >Customer Contract Details</th>
                                </tr>
                            </thead>
                            <tr>
                                <td >Customer Name</td>
                                <td >
                                    <c:out value="${customerName}"/>
                                </td>
                                <td >Customer Code</td>
                                <td >
                                    <c:out value="${customerCode}"/>
                                </td>
                            </tr>
                            <tr>
                                <td >Contract From</td>
                                <td ><c:out value="${contractFrom}"/></td>
                                <td >Contract To</td>
                                <td >
                                    <input type="hidden" class="datepicker" style="width:250px;height:40px" name="contractToOld" id="contractToOld" value="<c:out value="${contractTo}"/>"/>
                                    <input type="text" class="datepicker" style="width:250px;height:40px" name="contractTo" id="contractTo" value="<c:out value="${contractTo}"/>"/>
                                </td>
                            </tr>
                            <tr>
                                <td >Billing Type</td>
                                <td >
                                    <c:out value="${billingTypeName}"/>
                                </td>

                                <td >Payment Type </td>
                                <td  > <select name="paymentType" id="paymentType" class="form-control" style="width:250px;height:40px" style="width:125px;" >

                                        <c:if test="${paymentType == '0'}">
                                            <option value="0" selected>--Select--</option>
                                            <option value="1" >Credit</option>
                                            <option value="2" >Advance</option>
                                            <option value="3" >To Pay and Advance</option>
                                            <option value="4" >To Pay</option>
                                        </c:if>
                                        <c:if test="${paymentType == 1}">
                                            <option value="1" selected>Credit</option>
                                            <option value="2" >Advance</option>
                                            <option value="3" >To Pay and Advance</option>
                                            <option value="4" >To Pay</option>
                                        </c:if>
                                        <c:if test="${paymentType == 2}">
                                            <option value="1" >Credit</option>
                                            <option value="2" selected>Advance</option>
                                            <option value="3" >To Pay and Advance</option>
                                            <option value="4" >To Pay</option>
                                        </c:if>
                                        <c:if test="${paymentType == 3}">
                                            <option value="1" >Credit</option>
                                            <option value="2" >Advance</option>
                                            <option value="3" selected >To Pay and Advance</option>
                                            <option value="4" >To Pay</option>
                                        </c:if>
                                        <c:if test="${paymentType == 4}">
                                            <option value="1" >Credit</option>
                                            <option value="2" >Advance</option>
                                            <option value="3" >To Pay and Advance</option>
                                            <option value="4" selected>To Pay</option>
                                        </c:if>
                                    </select></td>
                            </tr>
                            <tr>
                                <td style="display:none" >Contract No</td>
                                <td style="display:none" >
                                    <c:out value="${contractNo}"/>
                                </td>
                                <td style="display:none"  height="30"><font color="red">*</font>Contact person</td>
                                <td style="display:none" height="30"><input name="custContactPerson" id="custContactPerson" type="text" class="form-control" style="width:250px;height:40px"  onKeyPress="return onKeyPressBlockNumbers(event);" value="<c:out value="${custContactPerson}"/>"></td>
                            </tr>
                            <tr style="display:none" >
                                <td  height="30"><font color="red">*</font>Phone No</td>
                                <td  height="30"><input name="custPhone" id="custPhone" type="text" class="form-control" style="width:250px;height:40px" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" value="<c:out value="${custPhone}"/>"></td>
                                <td  height="30"><font color="red">*</font>Mobile No</td>
                                <td  height="30"><input name="custMobile" id="custMobile" type="text" class="form-control" style="width:250px;height:40px"  onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="12" value="<c:out value="${custMobile}"/>"></td>

                            </tr>
                            <tr>
                                <td  height="30">&nbsp;&nbsp;Email</td>
                                <td  height="30"><input name="custEmail" id="custEmail" type="text" class="form-control" style="width:250px;height:40px" value="<c:out value="${custEmail}"/>" maxlength="20" onKeyPress="return isEmail()"></td>
                                <td  height="30"><font color="red">*</font>Account Manager</td>
                                <td  height="30"><input name="accountManagerId" id="accountManagerId" type="hidden" class="form-control" style="width:250px;height:40px" value="<c:out value="${accountManagerId}"/>"><input name="accountManager" id="accountManager" type="text" class="form-control" style="width:250px;height:40px" value="<c:out value="${accountManager}"/>" onKeyPress="return onKeyPressBlockNumbers(event);" ></td>
                            </tr>
                            <tr>
                                <td  height="30"><font color="red">*</font>Credit Limit</td>
                                <td  height="30"><input name="creditLimit" id="creditLimit" type="text" class="form-control" style="width:250px;height:40px" value="<c:out value="${creditLimit}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10"></td>
                                <td  height="30">&nbsp;&nbsp;Credit Days</td>
                                <td  height="30"><input name="creditDays" id="creditDays" type="text" class="form-control" style="width:250px;height:40px" value="<c:out value="${creditDays}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10"></td>
                            </tr>


                        </table>
                        <br>

                        <div id="tabs" >
                            <ul>
                                <!--                    <li><a href="#standard"><span>Standard Charges</span></a></li>-->
                                <c:if test="${billingTypeId ==1 }">
                                    <li id="pp" style="display: block"><a href="#ptp"><span>Point to Point - Based </span></a></li>
                                    </c:if>
                                    <c:if test="${billingTypeId == 1}">
                                    <!--<li data-toggle="tab" id="pc" style="display: block"><a href="#pcm"><span> Penality charges </span></a></li>-->
                                </c:if>
                                <c:if test="${billingTypeId == 1}">
                                    <!--<li data-toggle="tab" id="dc" style="display: block"><a href="#dcm"><span> Detention charges </span></a></li>-->
                                </c:if>    
                                <c:if test="${billingTypeId == 2 }">
                                    <li id="pw" style="display: block"><a href="#ptpw"><span>Point to Point Weight - Based </span></a></li>
                                    </c:if>
                                    <c:if test="${billingTypeId == 3}">
                                    <li id="akm" style="display: block"><a href="#mfr"><span>Actual KM - Based </span></a></li>
                                    </c:if>
                                    <c:if test="${billingTypeId == 4}">
                                    <li><a href="#fkmRate"><span>Fixed KM - Based - Rate Details</span></a></li>
                                    <li id="fkmRouteDetails" style="display: none"><a href="#fkmRoute"><span>Fixed KM - Based - Route Details </span></a></li>
                                    </c:if>
                            </ul>

                            <!--
                                        //Show existing contracts
                                        //user can change rates
                                        //user can make active to incactive
                                        //show only active routes
                            -->

                            <c:if test="${billingTypeId == 4}">
                                <div id="fkmRate" style="overflow: auto">
                                    <c:if test = "${contractFixedRateList != null}">
                                        <table id="suppExpenseTBL" class="sortable">
                                            <thead>
                                                <% int sno1 = 1;%>
                                                <tr height="110">
                                                    <th><h3>S.No</h3></th>
                                            <th><h3>Vehicle Type Name</h3></th>
                                            <th><h3>Vehicle Count</h3></th>
                                            <th><h3>Total Km</h3></th>
                                            <th><h3>Total Reefer Hm</h3></th>
                                            <th><h3>Rate with Reefer</h3></th>
                                            <th><h3>Extra Km Rate/Km with Reefer</h3></th>
                                            <th><h3>Extra Reefer Hm Rate/Hm with Reefer</h3></th>
                                            <th><h3>Rate without Reefer</h3></th>
                                            <th><h3>Extra Km Rate/Km without Reefer</h3></th>
                                            <th><h3>Status</h3></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${contractFixedRateList}" var="ptpl">
                                                    <tr>
                                                        <td><%=sno1%></td>
                                                        <td><input type='text' name='editId' id="editId<%=sno1-1%>" value='<c:out value="${ptpl.contractFixedRateId}" />'/>
                                                            <input type="hidden" name="fixedKmvehicleTypeId" id="fixedKmvehicleTypeId<%=sno1-1%>" value="<c:out value="${ptpl.vehicleTypeId}" />" class="form-control"  style="width: 60px;"/>
                                                            <c:out value="${ptpl.vehicleTypeName}" />
                                                        </td>
                                                        <td><input type="text" name="vehicleNos" id="vehicleNos<%=sno1-1%>" value="<c:out value="${ptpl.contractVehicleNos}" />" class="form-control"  style="width: 90px;"/></td>
                                                        <td><input type="text" name="fixedTotalKm" id="fixedTotalKm<%=sno1-1%>" value="<c:out value="${ptpl.totalKm}" />" class="form-control"  style="width: 90px;"/></td>
                                                        <td><input type="text" name="fixedTotalHm" id="fixedTotalHm<%=sno1-1%>" value="<c:out value="${ptpl.totalHm}" />" class="form-control" style="width: 90px;"/></td>
                                                        <td><input type="text" name="fixedRateWithReefer" id="fixedRateWithReefer<%=sno1-1%>" value="<c:out value="${ptpl.rateWithReefer}" />" class="form-control" style="width: 60px;" /></td>
                                                        <td><input type="text" name="extraKmRateWithReefer" id="extraKmRateWithReefer<%=sno1-1%>" value="<c:out value="${ptpl.extraKmRatePerKmRateWithReefer}" />" class="form-control" style="width: 60px;" /></td>
                                                        <td><input type="text" name="extraHmRateWithReefer" id="extraHmRateWithReefer<%=sno1-1%>" value="<c:out value="${ptpl.extraHmRatePerHmRateWithReefer}" />" class="form-control" style="width: 60px;" /></td>
                                                        <td><input type="text" name="fixedRateWithoutReefer" id="fixedRateWithoutReefer<%=sno1-1%>" value="<c:out value="${ptpl.rateWithoutReefer}" />" class="form-control" style="width: 60px;" /></td>
                                                        <td><input type="text" name="extraKmRateWithoutReefer" id="extraKmRateWithoutReefer<%=sno1-1%>" value="<c:out value="${ptpl.extraKmRatePerKmRateWithoutReefer}" />" class="form-control" style="width: 60px;" /></td>
                                                        <td><select name="status" id="status<%=sno1-1%>">
                                                                <c:if test="${ptpl.activeInd == 'Y'}" >
                                                                    <option value="Y" selected>Active</option>
                                                                    <option value="N">In-Active</option>
                                                                </c:if>
                                                                <c:if test="${ptpl.activeInd == 'N'}" >
                                                                    <option value="N" selected>In-Active</option>
                                                                    <option value="Y" >Active</option>
                                                                </c:if>
                                                            </select></td>
                                                    </tr>
                                                    <%sno1++;%>
                                                </c:forEach >
                                                <tr>
                                                    <td colspan="11"  align="center">
                                                        <input class="btn btn-success" type="button" value="Add Row" id="addFixedkmRow" onClick="addRow(2);"/>&nbsp;&nbsp;&nbsp;
                                                        <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Next" /></a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </c:if>
                                </div>
                                <script>

                                    var rowCount = 1;
                                    var sno = 0;
                                    var rowCount1 = 1;
                                    var sno1 = 0;
                                    var httpRequest;
                                    var httpReq;
                                    var styl = "";
                                    function addRow(val) {
                                        if (parseInt(rowCount1) % 2 == 0)
                                        {
                                            styl = "form-control";
                                        } else {
                                            styl = "form-control";
                                        }

                                        var sn = sno - 1;
                                        var tab = document.getElementById("suppExpenseTBL");
                                        //find current no of rows
                                        var rowCountNew = document.getElementById('suppExpenseTBL').rows.length;
                                        sno1 = 1 + parseInt(rowCountNew - 2);
                                        rowCountNew--;
                                        var newrow = tab.insertRow(rowCountNew);
                                        cell = newrow.insertCell(0);
                                        var cell0 = "<td class='text1' height='25' style='width:10px;'> <input type='hidden' name='editId' id='editId" + sno + "' value='0'/>" + sno1 + "</td>";
                                        cell.setAttribute("className", styl);
                                        cell.innerHTML = cell0;
                                        cell = newrow.insertCell(1);
                                        cell0 = "<td class='text1' height='25'><select class='textbox' id='fixedKmvehicleTypeId" + sno + "' style='width:125px'  name='fixedKmvehicleTypeId'  ><option selected value=0>---Select---</option> <c:if test="${vehicleTypeList != null}" ><c:forEach items="${vehicleTypeList}" var="vehType"><option  value='<c:out value="${vehType.vehicleTypeId}" />'><c:out value="${vehType.vehicleTypeName}" /> </c:forEach > </c:if> </select></td>";
                                        cell.setAttribute("className", styl);
                                        cell.innerHTML = cell0;
                                        cell = newrow.insertCell(2);
                                        cell0 = "<td class='text1' height='25'><input type='text' class='textbox' id='vehicleNos" + sno + "' style='width:125px'  name='vehicleNos' onKeyPress='return onKeyPressBlockCharacters(event);'/></td>";
                                        cell.setAttribute("className", styl);
                                        cell.innerHTML = cell0;
                                        cell = newrow.insertCell(3);
                                        cell0 = "<td class='text1' height='25' ><input type='text' name='fixedTotalKm' id='fixedTotalKm" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);'   value='' ></td>";
                                        cell.setAttribute("className", styl);
                                        cell.innerHTML = cell0;
                                        cell = newrow.insertCell(4);
                                        var cell0 = "<td class='text1' height='25' ><input type='text' name='fixedTotalHm' id='fixedTotalHm" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);'   value='' ></td>";
                                        cell.setAttribute("className", styl);
                                        cell.innerHTML = cell0;
                                        cell = newrow.insertCell(5);
                                        var cell0 = "<td class='text1' height='25' ><input type='text' name='fixedRateWithReefer' id='fixedRateWithReefer" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);'   value='' ></td>";
                                        cell.setAttribute("className", styl);
                                        cell.innerHTML = cell0;
                                        cell = newrow.insertCell(6);
                                        var cell0 = "<td class='text1' height='25' ><input type='text' name='extraKmRateWithReefer' id='extraKmRateWithReefer" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);'   value='' ></td>";
                                        cell.setAttribute("className", styl);
                                        cell.innerHTML = cell0;
                                        cell = newrow.insertCell(7);
                                        var cell0 = "<td class='text1' height='25' ><input type='text' name='extraHmRateWithReefer' id='extraHmRateWithReefer" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);'   value='' ></td>";
                                        cell.setAttribute("className", styl);
                                        cell.innerHTML = cell0;
                                        cell = newrow.insertCell(8);
                                        var cell0 = "<td class='text1' height='25' ><input type='text' name='fixedRateWithoutReefer' id='fixedRateWithoutReefer" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);'   value='' ></td>";
                                        cell.setAttribute("className", styl);
                                        cell.innerHTML = cell0;
                                        cell = newrow.insertCell(9);
                                        var cell0 = "<td class='text1' height='25' ><input type='text' name='extraKmRateWithoutReefer' id='extraKmRateWithoutReefer" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);'   value='' ></td>";
                                        cell.setAttribute("className", styl);
                                        cell.innerHTML = cell0;
                                        cell = newrow.insertCell(10);
                                        var cell0 = "<td class='text1' height='25' ><select name='status' id='status" + sno + "' width='120px' ><option value='Y'>Active</option></select></td>";
                                        cell.setAttribute("className", styl);
                                        cell.innerHTML = cell0;
                                        rowCount1++;
                                        sno++;
                                    }
                                        </script>
                                        <div id="fkmRoute" style="overflow: auto; display: none">
                                    <c:if test = "${fixedKmBillingList != null}" >
                                        <table width="100%" align="center" border="0" id="border" style="width:1250px;" class="general" >

                                            <thead>
                                                <tr height="40">
                                                    <td  height="30" >Sno</td>
                                                    <td  height="30" >VehicleType</td>
                                                    <td  height="30" >Origin</td>
                                                    <td  height="30" >Touch Point1 </td>
                                                    <td  height="30" >Touch Point2 </td>
                                                    <td  height="30" >Touch Point3 </td>
                                                    <td  height="30" >Touch Point4 </td>
                                                    <td  height="30" >Destination </td>
                                                    <td  height="30" >Status </td>
                                                </tr>
                                            </thead>
                                            <% int index = 0;
                                               int sno = 1;
                                            %>
                                            <tbody>
                                                <c:forEach items="${fixedKmBillingList}" var="contractList">

                                                    <tr height="30">
                                                        <td align="left"  ><%=sno%>
                                                            <%sno++;%>
                                                        </td>
                                                        <td align="left" >
                                                            <input type="hidden" name="routeContractId" value="<c:out value="${contractList.routeContractId}"/>"/>
                                                            <input type="hidden" name="contractRateId" value="<c:out value="${contractList.contractRateId}"/>"/>
                                                            <c:out value="${contractList.vehicleTypeName}"/>
                                                        </td>
                                                        <td align="left"   ><c:out value="${contractList.firstPickupName}"/></td>
                                                        <td align="left"   ><c:out value="${contractList.point1Name}"/></td>
                                                        <td align="left"   ><c:out value="${contractList.point2Name}"/></td>
                                                        <td align="left"   ><c:out value="${contractList.point3Name}"/></td>
                                                        <td align="left"   ><c:out value="${contractList.point4Name}"/></td>
                                                        <td align="left"   ><c:out value="${contractList.finalPointName}"/></td>
                                                        <td align="left"   >
                                                            <!--                                                            <select name="validStatus">
                                                                                                                            <option value="1" selected >Active</option>
                                                                                                                            <option value="0"  >In Active</option>
                                                                                                                        </select>-->
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                        </table>

                                        <center>
                                            <a><input type="button" class="btn btn-success btnPrevious" value="Update" name="update" style="width:70px;height:30px;font-weight: bold;padding:1px;" onclick="updatePage('4')" /> </a>
                                            <!--<input type="button" value="CANCEL" class="btn btn-success" onClick="backPage();">-->
                                        </center>
                                    </c:if>
                                </div>
                            </c:if>


                            <c:if test="${billingTypeId == 3}">
                                <!--actual kms-->
                                <c:if test = "${actualKmBillingList != null}" >
                                    <table width="100%" align="center" border="0" id="table" style="width:700px;" >
                                        <thead>
                                            <tr height="40">
                                                <th><h3>Sno</h3></th>
                                        <th><h3>Vehicle Type</h3></th>
                                        <th><h3>Rate Per Km</h3> </th>
                                        <th><h3>Rate Per Reefer Hr</h3> </th>
                                        <th><h3>Status</h3> </th>
                                        </tr>
                                        </thead>
                                        <%
                                                    int sno2 = 1;
                                        %>
                                        <tbody>
                                            <c:forEach items="${actualKmBillingList}" var="contractList">

                                                <tr height="30">
                                                    <td align="left" ><%=sno2%></td>
                                                    <%sno2++;%>
                                                    <td align="left" >
                                                        <input type="hidden" name="routeContractId" value="0"/>
                                                        <input type="hidden" name="contractRateId" value="<c:out value="${contractList.contractRateId}"/>"/>
                                                        <c:out value="${contractList.vehicleTypeName}"/>
                                                    </td>
                                                    <td align="left" >
                                                        <input type="text" name="vehicleRatePerKm" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${contractList.vehicleRatePerKm}"/>"/>
                                                    </td>
                                                    <td align="left" >
                                                        <input type="text" name="reeferRatePerHour" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${contractList.reeferRatePerHour}"/>"/>
                                                    </td>
                                                    <td align="left" >
                                                        <!--                                                        <select name="validStatus">
                                                                                                                    <option value="1" selected >Active</option>
                                                                                                                    <option value="0"  >In Active</option>
                                                                                                                </select>-->
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                    </table>
                                </c:if>

                            </c:if>
                            <br>
                            <br>
                            <c:if test="${billingTypeId != 3 && billingTypeId != 4}">
                                <div id="addNewRouteButton" style="display:block" >
                                    <!--<center><input type="button" class="btn btn-success" name="showAdd" value="Add More" onclick="showAddOption();" /></center>-->
                                </div>
                            </c:if>


                            <div id="addNewRoute" style="display:block" >
                                <%--<c:if test="${billingTypeId == 1}">--%>
                                <div id="ptp" style="overflow: auto">
                                    <div class="inpad">


                                        <%--<c:if test="${billingTypeId == 1}">--%>
                                        <!--                        point to point-->

                                        <c:if test = "${ptpBillingList != null}" >

                                            <table class="table table-info mb30 table-hover" id="table" >

                                                <thead>
                                                    <tr height="40">
                                                        <th  height="30" >Sno</th>
                                                        <th  height="30" >VehicleType</th>
                                                        <th height="30">Movement Type</th>
                                                        <th  height="30" >Origin</th>
                                                        <th  height="30" >Touch Point1 </th>
                                                        <th  height="30" >Touch Point2 </th>
                                                        <th  height="30" >Touch Point3 </th>
                                                        <th  height="30" >Touch Point4 </th>
                                                        <th  height="30" >Destination</th>
                                                        <th  height="30" >Agreed Fuel Price </th>
                                                        <th  height="30" >Rate </th>
                                                        <th  height="30" >TAT </th>
                                                        <th  height="30" >Status</th>
                                                    </tr>
                                                </thead>
                                                <% int index = 0;
                                                            int sno = 1;
                                                %>
                                                <tbody>
                                                    <c:forEach items="${ptpBillingList}" var="contractList">

                                                        <tr height="30">
                                                            <td align="left"  ><%=sno%>
                                                                <%sno++;%>
                                                            </td>
                                                            <td align="left" >
                                                                <input type="hidden" name="routeContractId1" value="<c:out value="${contractList.routeContractId}"/>"/>
                                                                <input type="hidden" name="contractRateId1" value="<c:out value="${contractList.contractRateId}"/>"/>
                                                                <c:out value="${contractList.vehicleTypeName}"/>
                                                            </td>
                                                             <td align="left"><c:out value="${contractList.movementTypeId}"/></td>
                                                            <td align="left"><c:out value="${contractList.firstPickupName}"/></td>
                                                            <td align="left"   ><c:out value="${contractList.point1Name}"/></td>
                                                            <td align="left"   ><c:out value="${contractList.point2Name}"/></td>
                                                            <td align="left"   ><c:out value="${contractList.point3Name}"/></td>
                                                            <td align="left"   ><c:out value="${contractList.point4Name}"/></td>
                                                            <td align="left"   ><c:out value="${contractList.finalPointName}"/></td>
                                                            <td align="left"   >
                                                                <input type="text" name="ptpRateWithReefer1" class="form-control" style="width:100px;height:40px" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${contractList.rateWithReefer}"/>"/>
                                                            </td>
                                                            <td align="left" >
                                                                <input type="text" name="ptpRateWithoutReefer1" class="form-control" style="width:100px;height:40px" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${contractList.rateWithoutReefer}"/>"/>
                                                            </td>
                                                            <td align="left" >
                                                                <input type="text" name="tatTime1" class="form-control" style="width:100px;height:40px" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${contractList.tatTime}"/>"/>
                                                            </td>
                                                            <td><select name="validStatus" id="form-control" style="width:100px;height:40px">
                                                                    <c:if test="${contractList.activeInd == 'Y'}" >
                                                                        <option value="Y" selected>Active</option>
                                                                        <option value="N">In-Active</option>
                                                                    </c:if>
                                                                    <c:if test="${contractList.activeInd == 'N'}" >
                                                                        <option value="N" selected>In-Active</option>
                                                                        <option value="Y" >Active</option>
                                                                    </c:if>
                                                                </select></td>


                                                        </tr>
                                                    </c:forEach>
                                            </table>
                                            <center>
                                                <a><input type="button" class="btn btn-success btnPrevious" value="Update" name="update" style="width:120px;" onclick="updatePage('4')" /> </a>
                                                <!--<input type="button" value="CANCEL" class="btn btn-success" onClick="backPage();">-->
                                            </center><br>
                                        </c:if>

                                        <%--</c:if>--%>

                                        <c:if test="${billingTypeId != 3 && billingTypeId != 4 && billingTypeId != 2}">

                                            <table class="table table-info mb30 table-hover" id="itemsTable" >
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <!--<th  height="80" style="width: 10px;"><font color="red">*</font>Route Contract Code</th>-->
                                                        <th  height="30" style="width: 10px;"><font color="red">*</font>Vehicle Type</th>
                                                        <th  height="30" style="width: 10px;"><font color="red">*</font>Movement Type</th>
                                                        <th  height="30" style="width: 10px;"><font color="red">*</font>First Pick UP</th>
                                                        <th  height="30" >Interim Point 1</th>
                                                        <th  height="30" >Interim Point 2</th>
                                                        <th  height="30" >Interim Point 3</th>
                                                        <th  height="30" >Interim Point 4</th>
                                                        <th  height="30" ><font color="red">*</font>Final Drop Point</th>
                                                        <th  height="30" style="width: 90px;" ><font color="red">*</font>Total Km</th>
                                                        <th  height="30" style="width: 90px;" ><font color="red">*</font>Agreed Fuel Price</th>
                                                        <th  height="30" style="width: 90px;" ><font color="red">*</font>Rate Without Reefer</th>
                                                        <th  height="30" style="width: 90px;" ><font color="red">*</font>TAT</th>
                                                    </tr>
                                                <tbody>
                                                    <tr class="item-row">
                                                        <td><input type="hidden" name="ptpRouteContractCode" id="ptpRouteContractCode" value="RC" class="form-control"  style="width: 60px;"/></td>
                                                        <td><c:if test="${vehicleTypeList != null}"><select name="ptpVehicleTypeId" id="ptpVehicleTypeId"  class="form-control" style="width:250px;height:40px" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value='<c:out value="${vehType.vehicleTypeId}"/>'><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></select></c:if></td>
                                                          <td><c:if test="${movementTypeList != null}"><select name="movementTypeId" id="movementTypeId"  class="form-control" style="width:250px;height:40px" style="width:235px"><option value="0">-Select-</option><c:forEach items="${movementTypeList}" var="vehType"><option value="<c:out value="${vehType.movementTypeId}"/>"><c:out value="${vehType.movementTypeName}"/></option></c:forEach></c:if></td>
                                                                <td><input name="ptpPickupPoint" value="" class="form-control" id="ptpPickupPoint"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);" onchange="checkPointNames(this.name)"/><input type="hidden" name="ptpPickupPointId" value="" class="form-control" id="ptpPickupPointId"  style="width: 90px;"/></td>
                                                                <td><input name="interimPoint1" value="" class="form-control" id="interimPoint1"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="interimPointId1" value="" class="form-control" id="interimPointId1"  style="width: 90px;"/><input type="hidden" name="interimPoint1Km" value="" class="form-control" id="interimPoint1Km"  style="width: 90px;"/><input type="hidden" name="interimPoint1Hrs" value="" class="form-control" id="interimPoint1Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint1Minutes" value="" class="form-control" id="interimPoint1Minutes"  style="width: 90px;"/><input type="hidden" name="interimPoint1RouteId" value="" class="form-control" id="interimPoint1RouteId"  style="width: 90px;"/></td>
                                                                <td><input name="interimPoint2" value="" class="form-control" id="interimPoint2"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="interimPointId2" value="" class="form-control" id="interimPointId2"  style="width: 90px;"/><input type="hidden"  name="interimPoint2Km" value="" class="form-control" id="interimPoint2Km"  style="width: 90px;"/><input type="hidden"  name="interimPoint2Hrs" value="" class="form-control" id="interimPoint2Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint2Minutes" value="" class="form-control" id="interimPoint2Minutes"  style="width: 90px;"/><input type="hidden"  name="interimPoint2RouteId" value="" class="form-control" id="interimPoint2RouteId"  style="width: 90px;"/></td>
                                                                <td><input name="interimPoint3" value="" class="form-control" id="interimPoint3"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="interimPointId3" value="" class="form-control" id="interimPointId3"  style="width: 90px;"/><input type="hidden" name="interimPoint3Km" value="" class="form-control" id="interimPoint3Km"  style="width: 90px;"/><input type="hidden" name="interimPoint3Hrs" value="" class="form-control" id="interimPoint3Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint3Minutes" value="" class="form-control" id="interimPoint3Minutes"  style="width: 90px;"/><input type="hidden" name="interimPoint3RouteId" value="" class="form-control" id="interimPoint3RouteId"  style="width: 90px;"/></td>
                                                                <td><input name="interimPoint4" value="" class="form-control" id="interimPoint4"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="interimPointId4" value="" class="form-control" id="interimPointId4"  style="width: 90px;"/><input type="hidden"  name="interimPoint4Km" value="" class="form-control" id="interimPoint4Km"  style="width: 90px;"/><input type="hidden"  name="interimPoint4Hrs" value="" class="form-control" id="interimPoint4Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint4Minutes" value="" class="form-control" id="interimPoint4Minutes"  style="width: 90px;"/><input type="hidden"  name="interimPoint4RouteId" value="" class="form-control" id="interimPoint4RouteId"  style="width: 90px;"/></td>
                                                                <td><input name="ptpDropPoint" value="" class="form-control" id="ptpDropPoint"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="ptpDropPointId" value="" class="form-control" id="ptpDropPointId"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointKm" value="" class="form-control" id="ptpDropPointKm"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointHrs" value="" class="form-control" id="ptpDropPointHrs"  style="width: 90px;"/><input type="hidden" name="ptpDropPointMinutes" value="" class="form-control" id="ptpDropPointMinutes"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointRouteId" value="" class="form-control" id="ptpDropPointRouteId"  style="width: 90px;"/></td>
                                                                <td><input name="ptpTotalKm" value="" class="form-control" id="ptpTotalKm"  style="width: 120px;" readonly/><input type="hidden" name="ptpTotalHours" value="" class="form-control" id="ptpTotalHours"  style="width: 90px;"/><input type="hidden" name="ptpTotalMinutes" value="" class="form-control" id="ptpTotalMinutes"  style="width: 90px;"/></td>
                                                                <td><input name="ptpRateWithReefer" value="" class="form-control" id="ptpRateWithReefer"  style="width: 120px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                                <td><input name="ptpRateWithoutReefer" value="" class="form-control" id="ptpRateWithoutReefer"  style="width: 120px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                                <td><input name="tatTime" value="" class="form-control" id="tatTime"  style="width: 120px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <!--<a href="#" id="addRow" class="button-clean large"><span> <img src="/throttle/images/icon-plus.png" alt="Add" title="Add Row" /> Add Route </span></a>-->
                                                    <br>
                                                    <br>
                                                </div>
                                                <center>
                                                    <input type="button" class="btn btn-success" value="Save" style="width: 120px" onclick="submitPage('1')"/>
                                                    <input type="button" value="CANCEL" class="btn btn-success" onClick="backPage();">
                                                </center>
                                    </c:if>

                                </div>
                                <div id="pcm" style="display:none;"> 
                                    <div class="inpad">
                                        <table class="table table-info mb30 table-hover" width="90%" id="penalitytable">
                                            <thead>
                                                <tr id="rowId0" >
                                                    <td    id="tableDesingTD">S No</td>
                                                    <td    id="tableDesingTD">Charges names</td>
                                                    <td    id="tableDesingTD">Unit</td>
                                                    <td    id="tableDesingTD">Amount</td>
                                                    <td    id="tableDesingTD">Remarks</td>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <% int sno = 0;%>
                                                <c:forEach items="${viewpenalitycharge}" var="vpc">
                                                    <%
                                                                sno++;
                                                    %>
                                                    <tr>
                                                        <td   align="left" > <%= sno%>  </td>
                                                        <td   align="left"> <c:out value="${vpc.penality}" /></td>
                                                        <td   align="left"> <c:out value="${vpc.pcmunit}" /></td>
                                                        <td align="left"   >
                                                            <input type="text" name="penalitycharge" class="form-control" style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${vpc.chargeamount}" />"/>
                                                        </td>
                                                        <td   align="left"> <c:out value="${vpc.pcmremarks}" /></td>
                                                        <td align="left"   ><input type="hidden"  name="penid" id="penid" value="<c:out value="${vpc.id}"/>" /></td>
                                                    </tr>
                                                </c:forEach>

                                                <tr>
                                            <input type="hidden" name="selectedRowCount" id="sno" value="1"/>
                                            <input type="hidden" name="tripSheetId" id="tripSheetId" value="<%=request.getParameter("tripSheetId")%>" />
                                            </tbody>
                                        </table>
                                        <br>
                                        <center>
                                            <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRowPenality()" />
                                            <!--<INPUT type="button" class="button" value="Delete Row" onclick="deleteRowPenality()" />-->
                                            <!--&nbsp;&nbsp;-->
                                            <!--<input type="button" class="btn btn-success" value="Save" onclick="Savepencharge(this.value)"/>-->
                                        </center>
                                        <SCRIPT language="javascript">
                                            var poItems = 1;
                                            var rowCount = 2;
                                            var sno = 1;
                                            var snumber = 1;
                                            function addRowPenality()
                                            {
                                                //                                           alert(rowCount);
                                                if (sno < 20) {
                                                    sno++;
                                                    var tab = document.getElementById("penalitytable");
                                                    rowCount = tab.rows.length;
                                                    snumber = (rowCount) - 1;
                                                    if (snumber == 1) {
                                                        snumber = parseInt(rowCount);
                                                    } else {
                                                        snumber++;
                                                    }
                                                    snumber = snumber - 1;
                                                    var newrow = tab.insertRow((rowCount));
                                                    newrow.height = "30px";
                                                    // var temp = sno1-1;
                                                    var cell = newrow.insertCell(0);
                                                    var cell0 = "<td class='text1' align='center'> " + snumber + "</td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;
                                                    cell = newrow.insertCell(1);
                                                    cell0 = "<td class='text1'><input type='text' name='penality' id='penality" + snumber + "' class='form-control' /></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;
                                                    cell = newrow.insertCell(2);
                                                    cell0 = "<td class='text1'><input type='text' name='pcmunit' id='pcmunit" + snumber + "' class='form-control' /></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;
                                                    cell = newrow.insertCell(3);
                                                    cell0 = "<td class='text1'><input type='text' name='chargeamount' id='chargeamount" + snumber + "' class='form-control' onKeyPress='return onKeyPressBlockCharacters(event);' /></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;
                                                    cell = newrow.insertCell(4);
                                                    cell0 = "<td class='text1'><input type='text' name='pcmremarks' id='pcmremarks" + snumber + "' class='form-control' /></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;
                                                    rowCount++;
                                                }
                                            }

                                        </SCRIPT>
                                    </div>
                                    &nbsp;&nbsp;
                                    <center>
                                        <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                        <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                    </center>
                                    <br>
                                </div>
                                <div id="dcm" style="display:none;">
                                    <div class="inpad">
                                        <table class="table table-info mb30 table-hover" width="90%" id="detentiontable">
                                            <thead>
                                                <tr id="rowId1" >
                                                    <td id="tableDesingTD" >S No</td>
                                                    <td id="tableDesingTD"  >Vehicle type</td>
                                                    <td id="tableDesingTD"  >Time Slot From(Days)</td>
                                                    <td id="tableDesingTD"  >Time Slot To(Days)</td>
                                                    <td id="tableDesingTD"  >Amount</td>
                                                    <td id="tableDesingTD"  >Remarks</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <% int Sno = 0;%>
                                                <c:forEach items="${viewdetentioncharge}" var="vdc">
                                                    <%
                                                                Sno++;
                                                    %>
                                                    <tr>
                                                        <td   align="left"> <%= Sno%>  </td>
                                                        <td   align="left"> <c:out value="${vdc.vehicleTypeName}" /></td>
                                                        <td   align="left"> <c:out value="${vdc.timeSlot}" /></td>
                                                        <td   align="left"> <c:out value="${vdc.timeSlotTo}" /></td>

                                                        <td align="left"   >
                                                            <input type="text" readonly name="detentioncharge" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${vdc.chargeamt}" />"  style="width:150px;height:44px;"/>
                                                        </td>
                                                        <td   align="left"> <c:out value="${vdc.dcmremarks}" /></td>
                                                        <td align="left"   ><input type="hidden"  name="denid" id="denid" value="<c:out value="${vdc.id}"/>" /></td>

                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                        <br>
                                        <center>
                                            <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRowdetention()" />
                                            <!--<INPUT type="button" class="button" value="Delete Row" onclick="deleteRowdetention()" />-->
                                            <br>
                                            <!--<input type="button" class="btn btn-success" value="Save" onclick="Savedencharge(this.value)"/>-->
                                        </center>
                                        <SCRIPT language="javascript">
                                            var poItems1 = 1;
                                            var rowCount1 = 2;
                                            var sno1 = 1;
                                            var snumber1 = 1;
                                            function addRowdetention()
                                            {
                                                //                                           alert(rowCount);
                                                if (sno1 < 20) {
                                                    sno1++;
                                                    var tab = document.getElementById("detentiontable");
                                                    rowCount1 = tab.rows.length;
                                                    snumber1 = (rowCount1) - 1;
                                                    if (snumber1 == 1) {
                                                        snumber1 = parseInt(rowCount1);
                                                    } else {
                                                        snumber1++;
                                                    }
                                                    //                                            snumber1 = snumber1-1;
                                                    //alert( (rowCount)-1) ;
                                                    //alert(rowCount);
                                                    var newrow = tab.insertRow((rowCount1));
                                                    newrow.height = "30px";
                                                    // var temp = sno1-1;
                                                    var cell = newrow.insertCell(0);
                                                    var cell0 = "<td class='text1' align='center'> " + snumber1 + "</td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;
                                                    cell = newrow.insertCell(1);
                                                    cell0 = "<td class='text1' ><select class='form-control' name='detention' id='detention" + snumber1 + "'  style='width:200px;height:44px;'><option value='0'>-Select-</option><c:if test = "${vehicleTypeList!= null}" ><c:forEach  items="${vehicleTypeList}" var="vehType"><option value='<c:out value="${vehType.vehicleTypeId}" />'><c:out value="${vehType.vehicleTypeName}" /></option></c:forEach ></c:if></select></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;
                                                    cell = newrow.insertCell(2);
                                                    cell0 = "<td class='text1' ><select   name='dcmunit' id='dcmunit" + snumber1 + "' class='form-control' style='width:200px;height:44px;'/><option value='0'>-Select-</option><c:if test = "${detaintionTimeSlot!= null}" ><c:forEach  items="${detaintionTimeSlot}" var="detain"><option value='<c:out value="${detain.id}" />'><c:out value="${detain.timeSlot}" /></option></c:forEach ></c:if></select></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;
                                                    cell = newrow.insertCell(3);
                                                    cell0 = "<td class='text1' ><select   name='dcmToUnit' id='dcmToUnit" + snumber1 + "' class='form-control' style='width:200px;height:44px;'/><option value='0'>-Select-</option><c:if test = "${detaintionTimeSlot!= null}" ><c:forEach  items="${detaintionTimeSlot}" var="detain"><option value='<c:out value="${detain.id}" />'><c:out value="${detain.timeSlot}" /></option></c:forEach ></c:if></select></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;
                                                    cell = newrow.insertCell(4);
                                                    cell0 = "<td class='text1' ><input type='text' name='chargeamt' id='chargeamt" + snumber1 + "' class='form-control' style='width:200px;height:44px;' onKeyPress='return onKeyPressBlockCharacters(event);' /></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;
                                                    cell = newrow.insertCell(5);
                                                    cell0 = "<td class='text1'><input type='text' name='dcmremarks' id='dcmremarks" + snumber1 + "' class='form-control' style='width:200px;height:44px;'/></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;
                                                    rowCount1++;
                                                }
                                            }

                                                </SCRIPT>

                                            </div>
                                            <br>
                                            <center>
                                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                            </center>
                                            <br>
                                        </div>

                                <%--</c:if>--%>



                                <c:if test="${billingTypeId == 4}">
                                    <div id="fkmRate" style="overflow: auto">

                                    </div>
                                    <div id="fkmRoute" style="overflow: auto">
                                        <div class="inpad">
                                            <table id="itemsTable" class="general">
                                                <tr>
                                                    <td></td>
                                                    <td  height="80" style="width: 10px;"><font color="red">*</font>Vehicle Route Contract Code</td>
                                                    <td  height="30" style="width: 10px;"><font color="red">*</font>Vehicle Type</td>
                                                    <td  height="30" style="width: 10px;"><font color="red">*</font>First Pick UP</td>
                                                    <td  height="30" >Interim Point 1</td>
                                                    <td  height="30" >Interim Point 2</td>
                                                    <td  height="30" >Interim Point 3</td>
                                                    <td  height="30" >Interim Point 4</td>
                                                    <td  height="30" ><font color="red">*</font>Final Drop Point</td>
                                                    <td  height="30" style="width: 90px;" ><font color="red">*</font>Total Km</td>
                                                </tr>
                                                <tbody>
                                                    <tr class="item-row">
                                                        <td></td>
                                                        <td><input name="ptpRouteContractCode" id="ptpRouteContractCode" value="" class="form-control"  style="width: 60px;"/></td>
                                                        <td><c:if test="${vehicleTypeList != null}"><select name="ptpVehicleTypeId" id="ptpVehicleTypeId"  class="form-control" style="width:250px;height:40px" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value='<c:out value="${vehType.vehicleTypeId}"/>'><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></select></c:if></td>
                                                                <td><input name="ptpPickupPoint" value="" class="form-control" id="ptpPickupPoint"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" onchange="checkPointNames(this.name)"/><input type="hidden" name="ptpPickupPointId" value="" class="form-control" id="ptpPickupPointId"  style="width: 90px;"/></td>
                                                                <td><input name="interimPoint1" value="" class="form-control" id="interimPoint1"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="interimPointId1" value="" class="form-control" id="interimPointId1"  style="width: 90px;"/><input type="hidden" name="interimPoint1Km" value="" class="form-control" id="interimPoint1Km"  style="width: 90px;"/><input type="hidden" name="interimPoint1Hrs" value="" class="form-control" id="interimPoint1Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint1Minutes" value="" class="form-control" id="interimPoint1Minutes"  style="width: 90px;"/><input type="hidden" name="interimPoint1RouteId" value="" class="form-control" id="interimPoint1RouteId"  style="width: 90px;"/></td>
                                                                <td><input name="interimPoint2" value="" class="form-control" id="interimPoint2"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="interimPointId2" value="" class="form-control" id="interimPointId2"  style="width: 90px;"/><input type="hidden"  name="interimPoint2Km" value="" class="form-control" id="interimPoint2Km"  style="width: 90px;"/><input type="hidden"  name="interimPoint2Hrs" value="" class="form-control" id="interimPoint2Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint2Minutes" value="" class="form-control" id="interimPoint2Minutes"  style="width: 90px;"/><input type="hidden"  name="interimPoint2RouteId" value="" class="form-control" id="interimPoint2RouteId"  style="width: 90px;"/></td>
                                                                <td><input name="interimPoint3" value="" class="form-control" id="interimPoint3"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="interimPointId3" value="" class="form-control" id="interimPointId3"  style="width: 90px;"/><input type="hidden" name="interimPoint3Km" value="" class="form-control" id="interimPoint3Km"  style="width: 90px;"/><input type="hidden" name="interimPoint3Hrs" value="" class="form-control" id="interimPoint3Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint3Minutes" value="" class="form-control" id="interimPoint3Minutes"  style="width: 90px;"/><input type="hidden" name="interimPoint3RouteId" value="" class="form-control" id="interimPoint3RouteId"  style="width: 90px;"/></td>
                                                                <td><input name="interimPoint4" value="" class="form-control" id="interimPoint4"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="interimPointId4" value="" class="form-control" id="interimPointId4"  style="width: 90px;"/><input type="hidden"  name="interimPoint4Km" value="" class="form-control" id="interimPoint4Km"  style="width: 90px;"/><input type="hidden"  name="interimPoint4Hrs" value="" class="form-control" id="interimPoint4Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint4Minutes" value="" class="form-control" id="interimPoint4Minutes"  style="width: 90px;"/><input type="hidden"  name="interimPoint4RouteId" value="" class="form-control" id="interimPoint4RouteId"  style="width: 90px;"/></td>
                                                                <td><input name="ptpDropPoint" value="" class="form-control" id="ptpDropPoint"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="ptpDropPointId" value="" class="form-control" id="ptpDropPointId"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointKm" value="" class="form-control" id="ptpDropPointKm"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointHrs" value="" class="form-control" id="ptpDropPointHrs"  style="width: 90px;"/><input type="hidden" name="ptpDropPointMinutes" value="" class="form-control" id="ptpDropPointMinutes"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointRouteId" value="" class="form-control" id="ptpDropPointRouteId"  style="width: 90px;"/></td>
                                                                <td><input name="ptpTotalKm" value="" class="form-control" id="ptpTotalKm"  style="width: 90px;" readonly/><input type="hidden" name="ptpTotalHours" value="" class="form-control" id="ptpTotalHours"  style="width: 90px;"/><input type="hidden" name="ptpTotalMinutes" value="" class="form-control" id="ptpTotalMinutes"  style="width: 90px;"/><input type="hidden" name="ptpRateWithReefer" value="0" class="form-control" id="ptpRateWithReefer"  style="width: 90px;" onKeyPress="return onKeyPressBlockCharacters(event);"/><input type="hidden" name="ptpRateWithoutReefer" value="0" class="form-control" id="ptpRateWithoutReefer"  style="width: 90px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <a href="#" id="addRow" class="button-clean large"><span> <img src="/throttle/images/icon-plus.png" alt="Add" title="Add Row" /> Add Route </span></a>
                                                    <br>
                                                    <br>
                                                    <center>
                                                        <input type="button" class="btn btn-success" value="Save" style="width: 120px" onclick="submitPage('4')"/>
                                                        <input type="button" value="CANCEL" class="btn btn-success" onClick="backPage();">
                                                    </center>
                                                </div>
                                            </div>

                                </c:if>
                                <%--<c:if test="${billingTypeId == 2}">--%>
                                <div id="ptpw" style="overflow: auto">

                                    <div class="inpad">
                                        <c:if test="${billingTypeId == 2}">
                                            <!--                        point to point weight-->
                                            <c:if test = "${ptpwBillingList != null}" >

                                                <table class="table table-info mb30 table-hover" id="bg" style="width:850px;" >

                                                    <thead>
                                                        <tr height="40">
                                                            <th  height="30" >Sno</th>
                                                            <th  height="30" >VehicleType</th>
                                                            <th  height="30" >Origin</th>
                                                            <th  height="30" >Destination</th>
                                                            <th  height="30" >Minimum Base Weight(KG)</th>
                                                            <th  height="30" >Agreed Fuel Price Per Kg</th>
                                                            <th  height="30" >Rate Per Kg</th>
                                                            <th  height="30" >Status</th>
                                                        </tr>
                                                    </thead>
                                                    <%
                                                                int sno1 = 1;
                                                    %>
                                                    <tbody>
                                                        <c:forEach items="${ptpwBillingList}" var="contractList">

                                                            <tr height="30">
                                                                <td align="left"   ><%=sno1%></td>
                                                                <%sno1++;%>
                                                                <td align="left"   >
                                                                    <input type="hidden" name="routeContractId2" value="<c:out value="${contractList.routeContractId}"/>"/>
                                                                    <input type="hidden" name="contractRateId2" value="<c:out value="${contractList.contractRateId}"/>"/>
                                                                    <c:out value="${contractList.vehicleTypeName}"/>
                                                                </td>
                                                                <td align="left"   ><c:out value="${contractList.firstPickupName}"/></td>
                                                                <td align="left"   ><c:out value="${contractList.finalPointName}"/></td>
                                                                <td align="left"   >
                                                                    <input type="text" name="minWgtBase1" id="minWgtBase1" onKeyPress="return onKeyPressBlockCharacters(event);"   value="<c:out value="${contractList.minimumBaseWgt}"/>" />
                                                                </td>
                                                                <td align="left"   >
                                                                    <input type="text" name="ptpwRateWithReefer1" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${contractList.rateWithReeferPerKg}"/>"/>
                                                                </td>
                                                                <td align="left"   >
                                                                    <input type="text" name="ptpwRateWithoutReefer1" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${contractList.rateWithoutReeferPerKg}"/>"/>
                                                                </td>
                                                                <td align="left"   >
                                                                    <select name="validStatus">
                                                                        <option value="1" selected >Active</option>
                                                                        <option value="0"  >In Active</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        </c:forEach>
                                                </table>
                                            </c:if>
                                        </c:if>

                                        <c:if test="${billingTypeId == 2}">
                                            <table class="table table-info mb30 table-hover" id="itemsTable1" style="width:1050px;" >
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <!--<th ><font color="red">*</font>Vehicle Route Contract Code</th>-->
                                                        <th><font color="red">*</font>Vehicle Type</th>
                                                        <th><font color="red">*</font>First Pick UP</th>
                                                        <th><font color="red">*</font>Final Drop Point</th>
                                                        <th><font color="red">*</font>Minimum Base Weight(KG)</th>
                                                        <th><font color="red">*</font>Total Km</th>
                                                        <th><font color="red">*</font>Kg Agreed Fuel Price</th>
                                                        <th><font color="red">*</font>Kg Rate Without Reefer</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="item-row">
                                                        <td><input type="hidden" name="ptpwRouteContractCode" id="ptpwRouteContractCode" value="RC" class="form-control" style="width:120px"/></td>
                                                        <td><c:if test="${vehicleTypeList != null}"><select name="ptpwVehicleTypeId" id="ptpwVehicleTypeId"  class="form-control" style="width:250px;height:40px" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value='<c:out value="${vehType.vehicleTypeId}"/>'><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></select></c:if></td>
                                                                <td><input type="hidden" name="ptpwPickupPointId" id="ptpwPickupPointId" value="" class="form-control"  style="width: 120px;"/><input name="ptpwPickupPoint" id="ptpwPickupPoint" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);"/></td>

                                                                <td><input name="ptpwDropPoint" id="ptpwDropPoint" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="ptpwDropPointId" id="ptpwDropPointId" value="" class="form-control"  style="width: 120px;"/><input type="hidden" name="ptpwPointRouteId" id="ptpwPointRouteId" value="" class="form-control"  style="width: 120px;"/></td>
                                                                <td><input type="text" name="minWgtBase" id="minWgtBase"  onKeyPress="return onKeyPressBlockCharacters(event);" value="" class="form-control"  style="width: 120px;" /></td>
                                                                <td><input name="ptpwTotalKm" id="ptpwTotalKm" value="" class="form-control"  style="width: 120px;" readonly/><input type="hidden" name="ptpwTotalHrs" id="ptpwTotalHrs" value="" class="form-control"  style="width: 120px;"/><input type="hidden" name="ptpwTotalMinutes" id="ptpwTotalMinutes" value="" class="form-control"  style="width: 120px;"/></td>
                                                                <td><input name="ptpwRateWithReefer" id="ptpwRateWithReefer" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                                <td><input name="ptpwRateWithoutReefer" id="ptpwRateWithoutReefer" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                    <a href="#" id="addRow1" class="button-clean large"><span> <img src="/throttle/images/icon-plus.png" alt="Add" title="Add Row" /> Add Route </span></a>
                                                    <br>
                                                    <br>


                                                </div>


                                                <center>
                                                    <input type="button" class="btn btn-success" value="Save" style="width: 120px" onclick="submitPage('2')"/>
                                                    <input type="button" value="CANCEL" class="btn btn-success" onClick="backPage();">
                                                </center>
                                    </c:if>
                                </div>
                                <%--</c:if>--%>

                            </div>
                            <br>
                            <c:if test="${billingTypeId != 4}">
                                <center>
                                    <input type="hidden" class="form-control" style="width:250px;height:40px" value="0"  id="addMoreRouteValue"/>
                                </center>
                            </c:if>
                            <c:if test="${billingTypeId == 4}">
                                <script>
                                    $(".nexttab").click(function() {
                                        var chek = validateFixedKmRateDetails();
                                        //                        alert(chek);
                                        if (chek == true) {
                                            $("#addFixedkmRow").hide();
                                            $("#fkmRouteDetails").show();
                                            $("#fkmRoute").show();
                                            var selected = $("#tabs").tabs("option", "selected");
                                            $("#tabs").tabs("option", "selected", selected + 1);
                                        }
                                    });
                                </script>
                            </c:if>


                        </div>

                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5"  selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span>Entries Per Page</span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 1);
                        </script>

                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="../common/NewDesign/settings.jsp" %>
