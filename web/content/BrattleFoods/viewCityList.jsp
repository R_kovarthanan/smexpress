<%-- 
    Document   : bpclTransactionHistoryUpload
    Created on : Jan 04, 2014, 12:38:56 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<script type="text/javascript" src="/throttle/js/suest"></script>
<script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    function submitPage(value) {
        if (value == 'Proceed') {
            document.upload.action = '/throttle/saveUploadCity.do';
            document.upload.submit();
        } else {
            document.upload.action = '/throttle/handleUploadEmployee.do';
            document.upload.submit();
        }
    }
</script>

<html>
    <body>
        <form name="upload" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <br>

            <%
            if(request.getAttribute("errorMessage")!=null){
            String errorMessage=(String)request.getAttribute("errorMessage");                
            %>
            <center><b><font color="red" size="1"><%=errorMessage%></font></b></center>
                        <%}%>
            <!--            <table border="0" cellpadding="0" cellspacing="0" width="780" align="center">
                            <tr>
                                <td class="contenthead" colspan="4">Employee  History Upload</td>
                            </tr>
                            <tr>
                                <td class="text2">Select file</td>
                                <td class="text2"><input type="file" name="importBpclTransaction" id="importBpclTransaction" class="importBpclTransaction"></td>                             
                            </tr>
                            <tr>
                                <td colspan="4" class="text1" align="center" ><input type="button" value="Submit" name="Submit" onclick="submitPage(this.value)" >
                            </tr>
                        </table>-->
            <% int i = 1 ;%>
            <% int index = 0;%> 
            <%int oddEven = 0;%>
            <%  String classText = "";%>    
            <c:if test="${cityList != null}">
                <table>
                    <th class="contenthead" colspan="42">File Uploaded Details&nbsp;:</th>
                    <tr height="45">
                        <th class="contentsub"><h3>Sno</h3></th>
                        <th class="contentsub"><h3>City Name</h3></th>
                        <th class="contentsub"><h3>Zone Name</h3></th>
                        <th class="contentsub"><h3>City Code</h3></th>
                    </tr>
                    <c:forEach items="${cityList}" var="city">
                        <%
                      oddEven = index % 2;
                      if (oddEven > 0) {
                          classText = "text2";
                      } else {
                          classText = "text1";
                        }
                        %>
                        <tr>
                                <td class="<%=classText%>" ><font color="green"><%=i++%></font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="cityName" id="cityName<%=i%>" value="<c:out value="${city.cityName}"/>" /><c:out value="${city.cityName}"/> </font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="zoneName" id="zoneName<%=i%>" value="<c:out value="${city.zoneName}"/>" /><c:out value="${city.zoneName}"/> </font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="cityCode" id="cityCode<%=i%>" value="<c:out value="${city.cityCode}"/>" /><c:out value="${city.cityCode}"/> OK</font></td>

                        </tr>
                        <%index++;%>
                    </c:forEach>
                </table>
                <br>
                <br>
                <br>
                <center>
                    <input type="button" class="button" value="Proceed" onclick="submitPage(this.value)"/>
                </center>

            </c:if>
        </form>
    </body>
</html>