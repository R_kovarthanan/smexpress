<%-- 
    Document   : bpclTransactionHistoryUpload
    Created on : Jan 04, 2014, 12:38:56 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<script type="text/javascript" src="/throttle/js/suest"></script>
<script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    function submitPage(value) {
        if (value == 'Proceed') {
            document.upload.action = '/throttle/saveUploadOldData.do';
            document.upload.submit();
        } else {
            document.upload.action = '/throttle/handleUploadOldData.do';
            document.upload.submit();
        }
    }
</script>

<html>
    <body>
        <form name="upload" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <br>
            <br>
            <br>
            <br>

            <% int i = 1 ;%>
            <% int index = 0;%> 
            <%int oddEven = 0;%>
            <%  String classText = "";%>    
            <c:if test="${consignmentOrderList != null}">
                <table>
                    <th class="contenthead" colspan="42">File Uploaded Details&nbsp;:</th>
                    <tr height="45">
                        <th class="contentsub"><h3>Sno</h3></th>
                        <th class="contentsub"><h3>Truck No</h3></th>
                        <th class="contentsub"><h3>Trip Date</h3></th>
                        <th class="contentsub"><h3>Origin</h3></th>
                        <th class="contentsub"><h3>Destination</h3></th>
                        <th class="contentsub"><h3>Movement Type</h3></th>
                        <th class="contentsub"><h3>Customer Name</h3></th>
                        <th class="contentsub"><h3>Customer Type</h3></th>
                        <th class="contentsub"><h3>AWB Destination</h3></th>
                        <th class="contentsub"><h3>Vehicle Type</h3></th>
                        <th class="contentsub"><h3>AWB No</h3></th>
                        <th class="contentsub"><h3>Total Packages</h3></th>
                        <th class="contentsub"><h3>Total Weight</h3></th>
                        <th class="contentsub"><h3>Total Chargeable Weight</h3></th>
                        <th class="contentsub"><h3>Total Volume</h3></th>
                        <th class="contentsub"><h3>Total Cost</h3></th>
                        <th class="contentsub"><h3>Vendor Name</h3></th>
                        <th class="contentsub"><h3>Truck Dept Date</h3></th>
                        <th class="contentsub"><h3>Truck Dept Time</h3></th>
                        <th class="contentsub"><h3>Truck Arrival Date</h3></th>
                        <th class="contentsub"><h3>Truck Arrival Time</h3></th>
                    </tr>
                    <c:forEach items="${consignmentOrderList}" var="consignment">
                        <%
                      oddEven = index % 2;
                      if (oddEven > 0) {
                          classText = "text2";
                      } else {
                          classText = "text1";
                        }
                        %>
                        <tr>
                                <td class="<%=classText%>" ><font color="green"><%=i++%></font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="vehicleRegNo" id="vehicleRegNo<%=i%>" value="<c:out value="${consignment.vehicleRegNo}"/>" /><c:out value="${consignment.vehicleRegNo}"/> </font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="consignmentDate" id="consignmentDate<%=i%>" value="<c:out value="${consignment.consignmentDate}"/>" /><c:out value="${consignment.consignmentDate}"/> </font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="origin" id="origin<%=i%>" value="<c:out value="${consignment.origin}"/>" /><c:out value="${consignment.origin}"/> </font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="destination" id="destination<%=i%>" value="<c:out value="${consignment.destination}"/>" /><c:out value="${consignment.destination}"/> </font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="movementType" id="movementType<%=i%>" value="<c:out value="${consignment.movementType}"/>" /><c:out value="${consignment.movementType}"/> </font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="customerName" id="customerName<%=i%>" value="<c:out value="${consignment.customerName}"/>" /><c:out value="${consignment.customerName}"/> </font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="customerCode" id="customerCode<%=i%>" value="<c:out value="${consignment.customerCode}"/>" /><c:out value="${consignment.customerCode}"/> </font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="awbDestinationId" id="awbDestinationId<%=i%>" value="<c:out value="${consignment.awbDestinationId}"/>" /><c:out value="${consignment.awbDestinationId}"/> </font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="vehicleTypeName" id="vehicleTypeName<%=i%>" value="<c:out value="${consignment.vehicleTypeName}"/>" /><c:out value="${consignment.vehicleTypeName}"/> </font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="awbNo" id="awbNo<%=i%>" value="<c:out value="${consignment.awbno}"/>" /><c:out value="${consignment.awbno}"/> </font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="totalPackages" id="totalPackages<%=i%>" value="<c:out value="${consignment.totalPackages}"/>" /><c:out value="${consignment.totalPackages}"/> </font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="totalWeight" id="totalWeight<%=i%>" value="<c:out value="${consignment.totalWeight}"/>" /><c:out value="${consignment.totalWeight}"/> </font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="chargeableWeight" id="chargeableWeight<%=i%>" value="<c:out value="${consignment.chargableWeight}"/>" /><c:out value="${consignment.chargableWeight}"/> </font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="totalVolume" id="totalVolume<%=i%>" value="<c:out value="${consignment.totalVolume}"/>" /><c:out value="${consignment.totalVolume}"/> </font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="totalCost" id="totalCost<%=i%>" value="<c:out value="${consignment.totalCost}"/>" /><c:out value="${consignment.totalCost}"/> </font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="vendorName" id="vendorName<%=i%>" value="<c:out value="${consignment.vendorName}"/>" /><c:out value="${consignment.vendorName}"/> </font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="truckDepDate" id="truckDepDate<%=i%>" value="<c:out value="${consignment.truckDepDate}"/>" /><c:out value="${consignment.truckDepDate}"/> </font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="truckDeptTime" id="truckDeptTime<%=i%>" value="<c:out value="${consignment.truckDeptTime}"/>" /><c:out value="${consignment.truckDeptTime}"/> </font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="truckArrDate" id="truckArrDate<%=i%>" value="<c:out value="${consignment.truckArrDate}"/>" /><c:out value="${consignment.truckArrDate}"/> </font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="truckArrTime" id="truckArrTime<%=i%>" value="<c:out value="${consignment.truckArrTime}"/>" /><c:out value="${consignment.truckArrTime}"/> </font></td>

                        </tr>
                        <%index++;%>
                    </c:forEach>
                </table>
                <br>
                <br>
                <br>
                <center>
                    <input type="button" class="button" value="Proceed" onclick="submitPage(this.value)"/>
                </center>

            </c:if>
        </form>
    </body>
</html>