<%-- 
    Document   : consignmentNoteWalkin
    Created on : 31 Mar, 2021, 5:42:50 PM
    Author     : mahendiran
--%>

<!DOCTYPE html>
<%@page import="java.text.SimpleDateFormat" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<head>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ page import="java.util.* "%>
    <%@ page import=" javax. servlet. http. HttpServletRequest" %>    
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>
    <link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
    <script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
    <!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
    <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
    <!--<script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>-->

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>
    <script type="text/javascript" src="/throttle/js/sweetalert.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/sweetalert.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//select2.github.io/select2/select2-3.3.2/select2.js"></script>
    <link rel="stylesheet" type="text/css" href="//select2.github.io/select2/select2-3.3.2/select2.css"/>
    <!--<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">-->
    <link rel="stylesheet" href="/throttle/css/bootstrap_combined.css"  type="text/css" />
    <link rel="stylesheet" type="text/css" href="/throttle/css/select2-bootstrap.css"/>


    <script>

        function setProductCategoryValues() {
            var temp = document.cNote.productCategoryIdTemp.value;
            //alert(temp);
            if (temp != 0) {
                var tempSplit = temp.split('~');
                document.getElementById("temperatureInfo").innerHTML = 'Reefer Temp (deg Celcius): Min ' + tempSplit[1] + ' Max ' + tempSplit[2];
                document.cNote.productCategoryId.value = tempSplit[0];
                var reeferRequired = tempSplit[3];
                if (reeferRequired == 'Y') {
                    reeferRequired = 'Yes';
                } else {
                    reeferRequired = 'No';
                }
                document.cNote.reeferRequired.value = reeferRequired;
            } else {
                document.getElementById("temperatureInfo").innerHTML = '';
                document.cNote.productCategoryId.value = 0;
                document.cNote.reeferRequired.value = '';
            }
        }



        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#consignorName').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getConsignorName.do",
                        dataType: "json",
                        data: {
                            consignorName: request.term,
                            customerId: document.getElementById('customerId').value,
                            textBox: 1
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    var id = ui.item.Id;
                    var mobile = ui.item.Mobile;
                    var address = ui.item.Address;
                    //                        alert(id + " : " + value);
                    $('#consignorId').val(id);
                    $('#consignorName').val(value);
                    $('#consignorAddress').val(address);
                    $('#consignorPhoneNo').val(mobile);
                    document.getElementById("consignorName").value = value;
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + itemVal + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
        });



        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#consigneeName').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getConsigneeName.do",
                        dataType: "json",
                        data: {
                            consigneeName: request.term,
                            customerId: document.getElementById('customerId').value,
                            textBox: 1
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    var id = ui.item.Id;
                    var mobile = ui.item.Mobile;
                    var address = ui.item.Address;
                    //                        alert(id + " : " + value);
                    $('#consigneeId').val(id);
                    $('#consigneeNames').val(value);
                    $('#consigneeAddress').val(address);
                    $('#consigneePhoneNo').val(mobile)
                    document.getElementById("consigneeName").value = value;
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + itemVal + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
        });


        function custValidation() {
            var custIds = document.getElementById('customerSearch').value;
            if (custIds == "") {
                alert("Please select Customer Name");
            }
        }








        //    function callConsignorAjax(val) {
        //        alert(val);
        //        $('#consignorNames').autocomplete({
        //            source: function(request, response) {
        //                $.ajax({
        //                    url: '/throttle/getConsignorName.do',
        //                    dataType: "json",
        //                    data: {
        //                        consignorName: request.term,
        //                        customerId: document.getElementById('customerId').value,
        //                        textBox: 1
        //                    },
        //                    success: function(data, textStatus, jqXHR) {
        //
        //                        var items = data;
        //                        response(items);
        //                    },
        //                    error: function(data, type) {
        //                        //console.log(type);
        //                    }
        //                });
        //            },
        //            minLength: 1,
        //            select: function(event, ui) {
        //                var value = ui.item.Name;
        //                var id = ui.item.Id;
        //                var mobile = ui.item.Mobile;
        //                var address = ui.item.Address;
        //                alert(id + " : " + value);
        //                $('#consignorIds').val(value);
        //                $('#consignorNames').val(id);
        //
        //                document.getElementById("consignorName").value = value;
        //
        //                return false;
        //            }
        //
        //            // Format the list menu output of the autocomplete
        //        }).data("ui-autocomplete")._renderItem = function(ul, item) {
        //            //alert(item);
        //            var itemVal = item.Name;
        //            itemVal = '<font color="green">' + itemVal + '</font>';
        //            return $("<li></li>")
        //                .data("item.autocomplete", item)
        //                .append("<a>" + itemVal + "</a>")
        //                .appendTo(ul);
        //        };
        //    }
        //        

        //    function callConsigneeAjax(val) {
        //
        //            $('#consigneeNames').autocomplete({
        //            source: function(request, response) {
        //               $.ajax({
        //                   url: '/throttle/getConsigneeName.do',
        //                   dataType: "json",
        //                   data: {
        //                       consigneeName: request.term,
        //                       customerId: document.getElementById('customerId').value,
        //                       textBox: 1
        //                   },
        //                   success: function(data, textStatus, jqXHR) {
        //
        //                       var items = data;
        //                       response(items);
        //                   },
        //                   error: function(data, type) {
        //
        //                       //console.log(type);
        //                   }
        //               });
        //            },
        //            minLength: 1,
        //            select: function(event, ui) {
        //               var value = ui.item.Name;
        //               var id = ui.item.Id;
        //               var mobile = ui.item.Mobile;
        //               var address = ui.item.Address;
        //            //                                        alert(id + " : " + value);
        //               $('#consigneeIds').val(value);
        //               $('#consigneeNames').val(id);
        //
        //
        //
        //               document.getElementById("consigneeName").value = value;
        //
        //               return false;
        //            }
        //
        //            // Format the list menu output of the autocomplete
        //            }).data("ui-autocomplete")._renderItem = function(ul, item) {
        //            //alert(item);
        //            var itemVal = item.Name;
        //            itemVal = '<font color="green">' + itemVal + '</font>';
        //            return $("<li></li>")
        //                   .data("item.autocomplete", item)
        //                   .append("<a>" + itemVal + "</a>")
        //                   .appendTo(ul);
        //            };
        //
        //            }        
        //        
    </script>

    <script type="text/javascript">

        var source, destination;
        var directionsDisplay;
        var options = {
            types: ['(cities)'],
            componentRestrictions: {country: "india"}
        };
        var directionsService = new google.maps.DirectionsService();
        google.maps.event.addDomListener(window, 'load', function() {
            new google.maps.places.Autocomplete(document.getElementById('gloriginName', options));
            directionsDisplay = new google.maps.DirectionsRenderer({'draggable': false});
        }
        );

        function setCityName1() {

            var tempdestination = document.getElementById("gloriginName").value;
            var tempdestination1 = tempdestination.split(",");
            //alert(tempdestination1[0]);            
            document.getElementById("gloriginName").value = tempdestination1[0];
            getLatLng1();
        }

        function getLatLng1() {

            var address = document.getElementById("gloriginName").value;
            var geocoder = new google.maps.Geocoder();

            geocoder.geocode({'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var longaddress = results[0].address_components[0].long_name;
                    var latitude = results[0].geometry.location.lat();
                    // var latitude = results[0].geometry.location.lat();
                    // alert(latitude);
                    var longitude = results[0].geometry.location.lng();
                    // alert(longitude);
                    //  latitude.value = latitute;
                    //  longitute.value = longitude;
                    document.getElementById("originlatitude").value = latitude.toFixed(5);
                    document.getElementById("originlongitude").value = longitude.toFixed(5);
                    //initialize(results[0].geometry.location.lat(), results[0].geometry.location.lng(), longaddress);

                } else {
                    alert('Geocode error: ' + status);
                }
            });
        }

        var source, destination;
        var directionsDisplay;
        var options = {
            types: ['(cities)'],
            componentRestrictions: {country: "india"}
        };
        var directionsService = new google.maps.DirectionsService();
        google.maps.event.addDomListener(window, 'load', function() {
            new google.maps.places.Autocomplete(document.getElementById('gldestinationName', options));
            directionsDisplay = new google.maps.DirectionsRenderer({'draggable': false});
        }
        );

        function setCityName2() {

            var tempdestination = document.getElementById("gldestinationName").value;
            var tempdestination1 = tempdestination.split(",");
            //  alert(tempdestination1[0]);            
            document.getElementById("gldestinationName").value = tempdestination1[0];
            getLatLng2();
        }



        function getLatLng2() {

            var address = document.getElementById("gldestinationName").value;
            var geocoder = new google.maps.Geocoder();

            geocoder.geocode({'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var longaddress = results[0].address_components[0].long_name;
                    var latitude = results[0].geometry.location.lat();
                    //  var latitude = results[0].geometry.location.lat();
                    //alert(latitude);
                    var longitude = results[0].geometry.location.lng();
                    // alert(longitude);
                    //  latitude.value = latitute;
                    //  longitute.value = longitude;
                    document.getElementById("destinationlatitude").value = latitude.toFixed(5);
                    document.getElementById("destinationlongitude").value = longitude.toFixed(5);
                    //initialize(results[0].geometry.location.lat(), results[0].geometry.location.lng(), longaddress);
                } else {
                    alert('Geocode error: ' + status);
                }
            });
        }



        $(document).ready(function() {

            $('#walkinCustomerName').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getWalkinCustomerDetails.do",
                        dataType: "json",
                        data: {
                            customerName: request.term,
                            customerCode: document.getElementById('customerCode').value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    $("#walkinCustomerName").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('~');
                    $('#walkinCustomerId').val(tmp[0]);
                    $itemrow.find('#walkinCustomerName').val(tmp[1]);
                    $itemrow.find('#walkinCustomerCode').val(tmp[2]);
                    $('#walkinCustomerAddress').val(tmp[3]);
                    $('#walkinPincode').val(tmp[4]);
                    $('#walkinCustomerPhoneNo').val(tmp[5]);
                    $('#walkinCustomerMobileNo').val(tmp[6]);
                    $('#walkinMailId').val(tmp[7]);
                    $('#state').val(tmp[8]);
                    $('#gstNo').val(tmp[9]);
                    document.getElementById('walkinCustomerCode').readOnly = true;
                    document.getElementById('walkinCustomerAddress').readOnly = true;
                    document.getElementById('walkinPincode').readOnly = true;
                    document.getElementById('walkinCustomerMobileNo').readOnly = true;
                    document.getElementById('walkinCustomerPhoneNo').readOnly = true;
                    document.getElementById('walkinMailId').readOnly = true;
                    document.getElementById('state').disabled = true;
                    document.getElementById('gstNo').readOnly = true;
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('~');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
        });
        //end ajax for vehicle Nos


        function autoRateWeight() {
            var typeId = document.getElementById("customerTypeId").value;
            var totalPackage = document.getElementById("totalPackage").value;

            if (typeId == 1) {

                var awbDestinationId = document.getElementById("awbDestinationId").value;

                if (awbDestinationId == 0) {

                    document.getElementById("totFreightAmount").value = "";
                    document.getElementById('totFreightAmount').readOnly = false;

                } else {

                    document.getElementById("totalChargeableWeights").value = (parseFloat(totalPackage) * 50) / 1000;

                    var awbOriginId = document.getElementById("awbOriginId").value;
                    var customerId = document.getElementById("customerId").value;
                    var chargableWeight = document.getElementById("totalChargeableWeights").value;
                    var productRate = document.getElementById("productRate").value;
                    var totalFreightValue = 0;

                    if (chargableWeight != 0 || chargableWeight != "") {

                        $.ajax({
                            url: '/throttle/getChargeableRateValue.do',
                            data: {customerId: customerId, chargeableWeight: chargableWeight, awbOriginId: awbOriginId, awbDestinationId: awbDestinationId, productRate: productRate},
                            dataType: 'json',
                            async: false,
                            success: function(data) {
                                if (data == '' || data == null) {
                                    $("#createOrder").hide();
                                    alert('chosen contract route does not exists. please check.');
                                } else {
                                    $.each(data, function(i, data) {
                                        document.getElementById("perKgAutoRate").value = data.perKgAutoRate;
                                        document.getElementById("rateValue").value = data.Name;

                                        //  alert(document.getElementById("perKgAutoRate").value);
                                        // alert(document.getElementById("rateValue").value);
                                        //  alert(chargableWeight);

                                        //totalFreightValue = parseFloat(chargableWeight) * parseFloat(data.Name);
                                        document.getElementById("totFreightAmount").value = document.getElementById("rateValue").value;
                                    });
                                    $("#createOrder").show();
                                }
                            }
                        });

                    } else {
                        document.getElementById("totFreightAmount").value = "";
                    }
                }

            } else {
                document.getElementById("totFreightAmount").value = "";
            }
        }
    </script>

    <script type="text/javascript">

        function setCustomerDetails(value) {
            var tmp = value.split('~');
            $('#customerId').val(tmp[0]);
            $('#customerName').val(tmp[1]);
            $('#customerCode').val(tmp[2]);
            $('#customerAddress').text(tmp[3]);
            $('#pincode').val(tmp[4]);
            $('#customerMobileNo').val(tmp[6]);
            $('#customerPhoneNo').val(tmp[5]);
            $('#mailId').val(tmp[7]);
            $('#billingTypeName').text(tmp[9]);
            $('#billTypeName').text(tmp[9]);
            $('#contractNo').text(tmp[10]);
            $('#billingTypeId').val(tmp[8]);

            document.getElementById('totalChargeableWeights').value = "";
            document.getElementById('bookingReferenceRemarks1').value = "";
            document.getElementById('totFreightAmount').value = "";

            document.getElementById('consignorName').value = "";
            document.getElementById('consignorId').value = "0";
            document.getElementById('consignorAddress').value = "0";
            document.getElementById('consignorPhoneNo').value = "0";
            document.getElementById('consigneeName').value = "";
            document.getElementById('consigneeId').value = "0";
            document.getElementById('consigneeAddress').value = "0";
            document.getElementById('consigneePhoneNo').value = "0";

            getContractRoutesOrigin(tmp[12]);

            var endDates = document.getElementById("startDate").value;
            var tempDate1 = endDates.split("-");
            var stDates = tmp[11];
            var tempDate2 = stDates.split("-");
            var prevTime = new Date(tempDate2[2], tempDate2[1], tempDate2[0]); // Feb 1, 2011
            var thisTime = new Date(tempDate1[2], tempDate1[1], tempDate1[0]);

            if (prevTime.getTime() > thisTime.getTime()) {
                $('#contractExpDate').text(tmp[11]);
            } else {
                $('#contractExpDate1').text(tmp[11]);
                alert("Order Cannot Processed Customer Contract Expired");
            }

            $('#contractId').val(tmp[12]);
            $('#paymentType').val(tmp[13]);

//            $('#consignorName').val(tmp[1]);
//            $('#consignorPhoneNo').val(tmp[6]);
//            $('#consignorAddress').val(tmp[3]);
//            $('#consigneeName').val(tmp[1]);
//            $('#consigneePhoneNo').val(tmp[6]);
//            $('#consigneeAddress').val(tmp[3]);
//            $('#creditLimitTemp').text(tmp[14]);
//            $('#creditDaysTemp').text(tmp[15]);
//            $('#outStandingTemp').text(tmp[16]);
//            $('#customerRankTemp').text(tmp[17]);
//            if (tmp[18] == "0") {
//                $('#approvalStatusTemp').text("No");
//            } else {
//                $('#approvalStatusTemp').text("Yes");
//            }
//            $('#creditLimitTable').show();
//            $('#outStandingDateTemp').text(tmp[19]);            
//            $('#creditLimit').val(tmp[14]);
//            $('#creditDays').val(tmp[15]);
//            $('#outStanding').val(tmp[16]);
//            $('#customerRank').val(tmp[17]);
//            $('#approvalStatus').val(tmp[18]);
//            $('#outStandingDate').val(tmp[19]);
//            $('#ahref').attr('href', '/throttle/viewCustomerContract.do?custId=' + tmp[0]);            

//            if (tmp[8] == '3') {//actual kms contract
//                setVehicleTypeForActualKM(tmp[12]);
//                $('#vehicleTypeDiv').show();
//                $('#vehicleTypeContractDiv').hide();
//                $('#contractRouteDiv1').hide();
//                $('#contractRouteDiv2').show();
//            } else {
//                $('#vehicleTypeDiv').hide();
//                $('#vehicleTypeContractDiv').show();
//                $('#contractRouteDiv1').show();
//                $('#contractRouteDiv2').hide();
//            }

            $("#contractDetails").show();
            document.getElementById('customerCode').readOnly = true;
            $('#origin').empty();
            $('#origin').append(
                    $('<option style="width:200px"></option>').val(0).html('--Select--')
                    )
            $("#productCategoryId").focus();
        }


        function checkNewCity() {

            var checkBox = document.getElementById("newroute");
            if (checkBox.checked == true) {
                $('#contractRouteDiv2').hide();
                $('#contractRouteDiv3').show();
                $('#contractRouteDiv4').show();
                $('#contractRouteDiv5').show();
                $('#contractRouteDiv6').show();
                $('#newRouteFlag').val("1");

            } else if (checkBox.checked == false) {
                $('#contractRouteDiv2').show();
                $('#contractRouteDiv3').hide();
                $('#contractRouteDiv4').show();
                $('#contractRouteDiv5').hide();
                $('#contractRouteDiv6').hide();
                $('#newRouteFlag').val("0");
            }

        }

        function showVehicle(val) {
            var check = document.getElementById("onwardReturn").value;

            if (val == "2") {
                $('#hideVehicleNo').show();
                //
                setVehicle();
            } else {
                $('#hideVehicleNo').hide();
            }

        }


        function setRouteDetails() {
            var temp = document.getElementById('destinationTemp').value;
            var destinationSelect = document.getElementById('destinationTemp');
            var destinationName = destinationSelect.options[destinationSelect.selectedIndex].text;
            document.cNote.destinationName.value = destinationName;
            var tempVal = temp.split('-');
            document.cNote.destination.value = tempVal[1];
        }

        function setOriginList(val) {
            $.ajax({
                url: '/throttle/getContractRouteList.do',
                data: {contractId: val, billingTypeId: $('#billingTypeId').val(), customerTypeId: 0},
                dataType: 'json',
                success: function(data) {
                    $.each(data, function(i, data) {
                        $('#originTemp').append(
                                $('<option style="width:200px"></option>').val(data.Id).html(data.Name)
                                )
                    });
                }
            });
        }

        var httpReq;
        var temp = "";
        function setDestination()
        {
            document.cNote.origin.value = document.cNote.originTemp.value;
            var billingTypeId = document.getElementById('billingTypeId').value;
            var originTemp = document.getElementById('originTemp');
            var originName = originTemp.options[originTemp.selectedIndex].text;
            document.cNote.originName.value = originName;
            var url = "/throttle/getDestinationList.do?originId=" + document.cNote.originTemp.value + "&billingTypeId=" + document.getElementById('billingTypeId').value + "&customerTypeId=" + document.getElementById('customerTypeId').value + "&contractId=" + document.getElementById('contractId').value;
            if (window.ActiveXObject)
            {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest)
            {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() {
                processAjax();
            };
            httpReq.send(null);
        }

        function processAjax()
        {
            if (httpReq.readyState == 4)
            {
                if (httpReq.status == 200)
                {
                    temp = httpReq.responseText.valueOf();
                    setDestinationOptions(temp, document.cNote.destinationTemp);
                } else
                {
                    alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                }
            }
        }

        function setDestinationOptions(text, variab) {
            variab.options.length = 0;
            option0 = new Option("--select--", '0');
            variab.options[0] = option0;
            if (text != "") {
                var splt = text.split('~');
                var temp1;
                var id;
                var name;
                for (var i = 0; i < splt.length; i++) {
                    temp1 = splt[i].split('#');
                    //alert(temp1[0]);
                    //alert(temp1[1]);
                    id = temp1[0];
                    name = temp1[1];
                    option1 = new Option(name, id)
                    variab.options[i + 1] = option1;
                }
            }
        }



        function setContractOriginDestination(value) {

            $("#awbDestinationId").val('');
            $("#awbDestination").val('');
            $("#truckDestinationId1").val('0');
            $("#truckDestinationName1").val('');
            setClassError('destSpan', 1, '0');
            $("#awbOriginId").val(value);
            $("#truckOriginId1").val(value);
            $("#truckOriginName1").val($("#originId option:selected").text());
            $("#awbOrigin").val($("#originId option:selected").text());
            $.ajax({
                url: '/throttle/getContractRoutesOriginDestination.do',
                data: {contractId: $('#contractId').val(), contractRouteOrigin: $('#originId').val(), customerTypeId: 0},
                dataType: 'json',
                success: function(data) {
                    $('#destinationId1').empty();
                    $('#destinationId1').append(
                            $('<option ></option>').val(0).html('-select-')
                            )
                    $.each(data, function(i, data) {
                        $('#destinationId1').append(
                                $('<option ></option>').val(data.Id).html(data.Name)
                                )
                    });
                }
            });
        }

        function getContractRoutesOrigin(val) {
            $.ajax({
                url: '/throttle/getContractRoutesOrigin.do',
                data: {contractId: val, billingTypeId: $('#billingTypeId').val(), customerTypeId: 0},
                dataType: 'json',
                success: function(data) {
                    $('#originId').empty();
                    $('#originId').append(
                            $('<option></option>').val(0).html('-select-')
                            )
                    $.each(data, function(i, data) {
                        $('#originId').append(
                                $('<option ></option>').val(data.Id).html(data.Name)
                                )
                    });
                }
            });
        }


        function setContractDestination(value) {
            $("#awbDestinationId").val(value);
            $("#truckDestinationId1").val(value);
            $("#truckDestinationName1").val($("#destinationId1 option:selected").text());
            $("#awbDestination").val($("#destinationId1 option:selected").text());
        }


        function createOrderSubmit(value) {

            var validateStatus = 0;
            var customerTypeId = document.getElementById("customerTypeId").value;
            var totalChargeableWeights = document.getElementById("totalChargeableWeights").value;
            var totFreightAmount = document.getElementById("totFreightAmount").value;
            var onwardReturn = document.getElementById("onwardReturn").value;

            if (customerTypeId == 1) {

                var customerId = document.getElementById("customerId").value;
                var originId = document.getElementById("originId").value;

                if (customerId == "") {
                    alert("Customer name is empty");
                    document.getElementById("customerName").focus();
                    validateStatus = 1;
                }

                if (onwardReturn == "2") {

                    var vehicleNo = document.getElementById("vehicleNo").value;
                    var onwardTrip = document.getElementById("onwardTrip").value;


                    if (vehicleNo == "") {
                        alert("vehicleNo is empty");
                        document.getElementById("vehicleNo").focus();
                        validateStatus = 1;
                    }

                    if (onwardTrip == "") {
                        alert("onwardTrip is empty");
                        document.getElementById("onwardTrip").focus();
                        validateStatus = 1;
                    }

                }

                if (originId == "0" || originId == "") {
                    alert("Origin is empty");
                    document.getElementById("Origin").focus();
                    validateStatus = 1;
                }

//                else if (totalChargeableWeights == "") {
//                    alert("Tonnage is empty");
//                    document.getElementById("totalChargeableWeights").focus();
//                    validateStatus = 1;
//                }

                else if (totFreightAmount == "") {
                    alert("Freight Amount is empty");
                    document.getElementById("totFreightAmount").focus();
                    validateStatus = 1;
                }

                else if (validateStatus == 0 && value == 'Create Order') {
                    $("#createOrder").hide();
                    document.cNote.action = "/throttle/saveBookingOrder.do";
                    document.cNote.submit();
                }


            } else {

                var newRouteFlag = document.getElementById("newRouteFlag").value;
                var walkinCustomerName = document.getElementById("walkinCustomerName").value;
                var origin = "";

                if (newRouteFlag == 1) {
                    var tempdestination = document.getElementById("gloriginName").value;
                    var tempdestination1 = tempdestination.split(",");
                    document.getElementById("originCity").value = tempdestination1[0];

                    var tempdestination3 = document.getElementById("gldestinationName").value;
                    var tempdestination2 = tempdestination3.split(",");
                    document.getElementById("destinationCity").value = tempdestination2[0];
                }

                if (newRouteFlag == 1) {
                    origin = document.getElementById("originCity").value;
                } else {
                    origin = document.getElementById("origin").value;
                }

                if (onwardReturn == "2") {

                    var vehicleNo = document.getElementById("vehicleNo").value;
                    var onwardTrip = document.getElementById("onwardTrip").value;

                    if (vehicleNo == "") {
                        alert("vehicleNo is empty");
                        document.getElementById("vehicleNo").focus();
                        validateStatus = 1;
                    }

                    if (onwardTrip == "") {
                        alert("onwardTrip is empty");
                        document.getElementById("onwardTrip").focus();
                        validateStatus = 1;
                    }

                }

                if (walkinCustomerName == "") {
                    alert("Customer name is empty");
                    document.getElementById("walkinCustomerName").focus();
                    validateStatus = 1;
                }
                else if (origin == "0" || origin == "") {
                    alert("Origin is empty");
                    validateStatus = 1;
                }
//                else if (totalChargeableWeights == "" || totalChargeableWeights == 0) {
//                    alert("Tonnage is empty");
//                    document.getElementById("totalChargeableWeights").focus();
//                    validateStatus = 1;
//                }

                else if (totFreightAmount == "") {
                    alert("Freight Amount is empty");
                    document.getElementById("totFreightAmount").focus();
                    validateStatus = 1;
                }

                else if (validateStatus == 0 && value == 'Create Order') {
                    $("#createOrder").hide();
                    document.cNote.action = "/throttle/saveWalkinConsignmentOrder.do";
                    document.cNote.submit();
                }

            }

        }

        function createEmpty(value) {
            var onwardTrip = document.getElementById("onwardTrip").value;
            var validateStatus = 0;
            if (onwardTrip == "" || onwardTrip == "0") {
                alert("Please select Trip code.");
                document.getElementById("onwardTrip").focus();
                validateStatus = 1;
            }

            if (validateStatus == 0) {
                $("#emptyTrip").hide();                
                document.cNote.action = "/throttle/saveReturnEmptyTrip.do";
                document.cNote.submit();
            }
        }

    </script>

    <script type="text/javascript">

        function showVehicleOnwardTrip(val) {

            $.ajax({
                url: "/throttle/getVehicleOnwardTrip.do",
                dataType: "json",
                data: {
                    vehicleId: val
                },
                success: function(data) {
                    // alert(data);
                    if (data != '') {
                        $('#onwardTrip').empty();
                        $('#onwardTrip').append(
                                $('<option></option>').val(0).html('--select--'))
                        $.each(data, function(i, data) {
                            $('#onwardTrip').append(
                                    $('<option style="width:90px"></option>').attr("value", data.Id).text(data.Name)
                                    )
                        });
                    } else {
                        $('#onwardTrip').empty();
                        $('#onwardTrip').append(
                                $('<option></option>').val(0).html('--select--'))
                    }
                }
            });

        }



        function setVehicle() {

            $.ajax({
                url: "/throttle/getReturnVendorVehicle.do",
                dataType: "json",
                data: {
                    ownerShip: 1
                },
                success: function(data) {
                    // alert(data);
                    if (data != '') {
                        $('#vehicleNo').empty();
                        $('#vehicleNo').append(
                                $('<option></option>').val(0).html('--select--'))
                        $.each(data, function(i, data) {
                            $('#vehicleNo').append(
                                    $('<option style="width:90px"></option>').attr("value", data.Id).text(data.Name)
                                    )
                        });
                    } else {
                        $('#vehicleNo').empty();
                        $('#vehicleNo').append(
                                $('<option></option>').val(0).html('--select--'))
                    }
                }
            });

        }

        function enableContract(val) {

            if (val == 1) {
                $("#contractDetails").show();
                $("#walkinCustomerTable").hide();
                $("#contractFreightTable").show();
                $("#walkinFreightTable").hide();
                $("#contractRouteDiv1").show();
                $("#contractRouteDiv2").hide();
                $("#contractRouteDiv3").hide();
                $("#contractRouteDiv4").hide();
                $("#contractRouteDiv5").hide();
                $("#contractRouteDiv6").hide();
                $("#contractFreightTable1").show();
                document.getElementById("custtype").innerHTML = 'Contract';
                document.getElementById('contract').value = "1";
                showVehicle("1");
                $("#custRefField").show();
                $("#createOrder").show();
                $("#emptyTrip").hide();                
                document.getElementById("billType").disabled = false;
                document.getElementById('onwardReturn').value = "1";
                document.getElementById("onwardReturn").disabled = false;
                // document.getElementById('totFreightAmount').readOnly = true;
                //document.getElementById('customerAddress').readOnly = true;
                //document.getElementById('pincode').readOnly = true;
                //document.getElementById('customerMobileNo').readOnly = true;
                //document.getElementById('mailId').readOnly = true;
                //document.getElementById('customerPhoneNo').readOnly = true;
                document.getElementById('walkinCustomerName').value = "";
                document.getElementById('walkinCustomerCode').value = "";
                document.getElementById('walkinCustomerAddress').value = "";
                document.getElementById('walkinPincode').value = "";
                document.getElementById('walkinCustomerMobileNo').value = "";
                document.getElementById('walkinMailId').value = "";
                document.getElementById('walkinCustomerPhoneNo').value = "";
                document.getElementById("totalChargeableWeights").value = 0;
                document.getElementById("totalChargeableWeights").readOnly = true;
            }
            else if (val == 3) {
                $("#contractDetails").hide();
                $("#walkinCustomerTable").hide();
                $("#contractFreightTable").hide();
                $("#walkinFreightTable").hide();
                $("#contractRouteDiv1").hide();
                $("#contractRouteDiv2").hide();
                $("#contractRouteDiv3").hide();
                $("#contractRouteDiv4").hide();
                $("#contractRouteDiv5").hide();
                $("#contractRouteDiv6").hide();
                $("#contractFreightTable1").hide();
                showVehicle("2");
                document.getElementById('onwardReturn').value = "2";
                document.getElementById("onwardReturn").disabled = true;
                document.getElementById("billType").disabled = true;
                $("#custRefField").hide();
                $("#createOrder").hide();
                $("#emptyTrip").show();
                document.getElementById("custtype").innerHTML = 'Empty';
                document.getElementById('contract').value = "3";
                document.getElementById('walkinCustomerName').value = "";
                document.getElementById('walkinCustomerCode').value = "";
                document.getElementById('walkinCustomerAddress').value = "";
                document.getElementById('walkinPincode').value = "";
                document.getElementById('walkinCustomerMobileNo').value = "";
                document.getElementById('walkinMailId').value = "";
                document.getElementById('walkinCustomerPhoneNo').value = "";
                document.getElementById("totalChargeableWeights").value = 0;
                document.getElementById("totalChargeableWeights").readOnly = true;
            }

            else {
                $('#contractId').val(0);
                $("#contractDetails").hide();
                $("#walkinCustomerTable").show();
                $("#contractRouteDiv1").hide();
                $("#contractRouteDiv2").show();
                $("#contractRouteDiv3").hide();
                $("#contractRouteDiv4").show();
                $("#contractRouteDiv5").hide();
                $("#contractRouteDiv6").hide();
                $("#contractFreightTable1").show();
                $("#custRefField").show();
                showVehicle("1");
                document.getElementById("billType").disabled = false;
                document.getElementById("onwardReturn").disabled = false;
                $("#createOrder").show();
                $("#emptyTrip").hide();
                document.getElementById("custtype").innerHTML = 'Walk-in';
                //  document.getElementById('totFreightAmount').readOnly = false;
                document.getElementById('contract').value = "2";
                document.getElementById("totalChargeableWeights").value = 0;
                document.getElementById("totalChargeableWeights").readOnly = false;
                setOriginList(0);
            }
        }

    </script>
</head>

<body onload="enableContract(2);">
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Booking" text="Booking"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Booking" text="Booking"/></a></li>
                <li class=""><spring:message code="hrms.label.Booking" text="Booking"/></li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <% String menuPath = "Operations >>  Create New Booking";
                            request.setAttribute("menuPath", menuPath);
                %>
                <form name="cNote" method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>                    
                    <%@include file="/content/common/message.jsp" %>
                    <%
                                Date today = new Date();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                                String startDate = sdf.format(today);
                    %>
                    <input type="hidden" name="startDate" id="startDate" value='<%=startDate%>' />

                    <div id="customerDetail" >                        

                        <table class="table table-info mb30 table-hover" >
                            <thead>
                                <tr>
                                    <th  colspan="4" >Consignment Note</th>
                                </tr>
                            </thead> 
                            <tr>
                                <td ><font color="red">*</font>Customer Type</td>
                                <td >
                                    <c:if test="${customerTypeList != null}">
                                        <select class="form-control" style="width:200px;height:40px" name="customerTypeId" id="customerTypeId" onchange="enableContract(this.value);" >
                                            <c:forEach items="${customerTypeList}" var="cusType">
                                                <c:if test="${cusType.customerTypeId == 2}">
                                                <option value="<c:out value="${cusType.customerTypeId}"/>"><c:out value="${cusType.customerTypeName}"/></option>
                                             </c:if>
                                            </c:forEach>
                                        </select>
                                    </c:if>
                                </td>

                                <td><font color="red">*</font>Customer Bill Type</td>
                                <td> 
                                    <select class="form-control" style="width:200px;height:40px" name="billType" id="billType"  >
                                        <option value="1" selected>To Be Bill</option>
                                        <option value="2" >To Pay</option>
                                    </select>
                                </td>
                            </tr>
                            <tr id="custRefField">

                                <td ><font color="red">*</font>Customer Reference No</td>
                                <td ><input type="text" name="orderReferenceNo" id="orderReferenceNo" value="" class="form-control" style="width:200px;height:40px" /></td>
                                <td >Customer Reference  Remarks</td>
                                <td ><textarea name="orderReferenceRemarks" id="orderReferenceRemarks" class="form-control" style="width:200px;height:40px"></textarea></td>

                            </tr>
                        </table>
                        <div id="walkinCustomerTable">
                            <table class="table table-info mb30 table-hover" >
                                <thead>
                                    <tr>
                                        <th  colspan="4" >Walkin Customer Details</th>
                                    </tr>
                                </thead>

                                <tr>
                                    <td ><font color="red">*</font> Customer Name</td>
                                    <td >
                                        <input type="text" style="width:200px;height:40px" name="walkinCustomerName" id="walkinCustomerName" class="form-control"  />
                                        <input type="hidden" name="walkinCustomerId" id="walkinCustomerId"  class="form-control" value="0"  />
                                    </td>
                                    <td ><font color="red">*</font> Customer Code</td>
                                    <td ><input type="text" style="width:150px;height:40px" name="walkinCustomerCode" id="walkinCustomerCode" class="form-control" readonly /></td>
                                </tr>


                                <tr>
                                    <td ><font color="red">*</font>Address</td>
                                    <td ><textarea rows="3" cols="30" id="walkinCustomerAddress" name="walkinCustomerAddress" class="form-control" style="width:200px;height:40px"></textarea></td>
                                    <td ><font color="red">*</font>Mobile No</td>
                                    <td ><input type="text"    onKeyPress='return onKeyPressBlockCharacters(event);'   name="walkinCustomerMobileNo" id="walkinCustomerMobileNo" class="form-control" style="width:200px;height:40px"  /></td>
                                </tr>

                                <tr>
                                    <td><font color="red"></font>PAN No</td>
                                    <td><input type="text"   name="walkinPanNo" id="walkinPanNo" class="form-control" style="width:200px;height:40px"  onBlur="validatePanNo()" placeholder="Type pan no..."  required/></td>
                                    <td><font color="red"></font>GST No</td>
                                    <td><input type="text"   name="walkinGSTNo" id="walkinGSTNo" class="form-control" style="width:200px;height:40px"  /></td>
                                </tr>

                                <tr>
                                    <td ><font color="red">*</font>Pincode</td>
                                    <td >
                                        <input type="text" onKeyPress='return onKeyPressBlockCharacters(event);'   name="walkinPincode" id="walkinPincode" class="form-control" style="width:200px;height:40px" />                                        
                                    </td>    
                                    <td></td>
                                    <td></td>
                                </tr> 
                                <tr>
                                    <td><font color="red">*</font>Consignor Name</td>
                                    <td><input type="text"   name="walkinConsoignor" id="walkinConsoignor" class="form-control" style="width:200px;height:40px"  /></td>
                                    <td><font color="red">*</font>Consignee Name</td>
                                    <td><input type="text"   name="walkinConsignee" id="walkinConsignee" class="form-control" style="width:200px;height:40px"  /></td>
                                </tr>

                                <input type="hidden" name="walkInBillingTypeId" id="walkInBillingTypeId" value="2" class="form-control" style="width:200px;height:40px" >                                
                                <input type="hidden" name="walkinMailId" id="walkinMailId" value="NA" class="form-control" style="width:200px;height:40px"  />
                                <input type="hidden" name="walkinCustomerPhoneNo" id="walkinCustomerPhoneNo" value="0"  class="form-control" style="width:200px;height:40px" maxlength="10"   />
                                <input type="hidden" name="walkinRateWithoutReeferPerKg" id="walkinRateWithoutReeferPerKg" class="form-control" value="0"/>
                                <input type="hidden" name="walkinFreightWithoutReefer" id="walkinFreightWithoutReefer" class="form-control" value="0"/></td>

                            </table>
                        </div>

                        <div id="contractDetails">
                            <table class="table table-info mb30 table-hover"  >
                                <thead>
                                    <tr>
                                        <th  colspan="4" >Contract Customer Details</th>
                                    </tr>
                                </thead>     

                                <tr >
                                    <td><font color="red">*</font>Customer Name </td>
                                    <td><select style="width:200px;height:40px" id="customerSearch" class="form-control" onchange="setClassError('customerSpan', 2, this.value);
                                            setCustomerDetails(this.value);">
                                            <option></option>    
                                            <c:if test="${getCustomerList != null}">
                                                <c:forEach items="${getCustomerList}" var="cust">
                                                    <option value="<c:out value="${cust.customerId}"/>"><c:out value="${cust.customerName}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select>
                                        <script>
                                            $('#customerSearch').select2({placeholder: 'Fill Customer Name'});</script>
                                    </td>

                                <input type="hidden" name="customerId" id="customerId" value="" id="customerId" class="form-control" />
                                <input type="hidden" name="customerName" id="customerName" class="form-control" style="width:200px;height:40px" />

                                <td ><font color="red">*</font>Customer Code</td>
                                <td ><input style="width:200px;height:40px" class="form-control" type="text" name="customerCode" id="customerCode"  readonly /></td>
                                </tr>

                                <tr>
                                    <td ><font color="red">*</font> Consignor Name</td>
                                    <td >
                                        <input type="text" style="width:200px;height:40px" name="consignorName" id="consignorName" value="" class="form-control" onclick="custValidation();" />
                                        <input type="hidden" name="consignorId" id="consignorId"  class="form-control" value="0"  />
                                        <input type="hidden" name="consignorAddress" id="consignorAddress"  class="form-control" value="0"  />
                                        <input type="hidden" name="consignorPhoneNo" id="consignorPhoneNo"  class="form-control" value="0"  />
                                    </td>
                                    <td ><font color="red">*</font> Consignee Name</td>
                                    <td ><input type="text" style="width:200px;height:40px" name="consigneeName" id="consigneeName" class="form-control"  onclick=" custValidation();" />
                                        <input type="hidden" name="consigneeId" id="consigneeId"  class="form-control" value="0"  />
                                        <input type="hidden" name="consigneeAddress" id="consigneeAddress"  class="form-control" value="0"  />
                                        <input type="hidden" name="consigneePhoneNo" id="consigneePhoneNo"  class="form-control" value="0"  />
                                    </td>
                                </tr>
                                <!--                                <tr>
                                                                    <td>Customer Reference No.</td>
                                                                    <td colspan="3"><input type="text" style="width:200px;height:40px" name="customerReference" id="customerReference" value="" class="form-control" /></td>
                                                                </tr>-->

                            </table>
                        </div>

                        <table class="table table-info mb30 table-hover" >
                            <thead>
                                <tr>
                                    <th  colspan="4" >Route Details</th>
                                </tr>
                            </thead>
                            <tr>
                                <td>Onward</td>
                                <td> 
                                    <select name="onwardReturn" id="onwardReturn" onchange="showVehicle(this.value);"  class="form-control" style="width:200px;height:40px" >
                                        <option value="1" selected>Onward</option>
                                        <option value="2" >Return</option>
                                    </select>

                                    <input type="hidden" name="shipmentType" id="shipmentType" value="1" class="form-control" style="width:200px;height:40px" >
                                </td>
                                <td>Booking Date </td>
                                <td ><input type="text" name="consignmentDate" id="consignmentDate" value="<%=startDate%>"  class="datepicker form-control" style="width:200px;height:40px"/></td>
                            </tr>

                            <tr id="hideVehicleNo" style="display:none;">
                                <td >Vehicle No</td>
                                <td>                                        
                                    <select name="vehicleNo" id="vehicleNo" onchange="showVehicleOnwardTrip(this.value);"  class="form-control" style="width:200px;height:40px"  >
                                    </select>
                                    <script>
                                        $('#vehicleNo').select2({placeholder: 'Fill vehicleNo'});
                                    </script>
                                </td>

                                <td>Onward Trip</td>
                                <td>
                                    <select id="onwardTrip" name="onwardTrip" class="form-control" style="width:200px;height:40px">

                                    </select>
                                    <script>
                                        $('#onwardTrip').select2({placeholder: 'Fill Onward Trip'});
                                    </script>
                                </td>                                   
                            </tr>                            

                            <tr  id="contractRouteDiv1">
                                <td><font color="red">*</font>Origin</td>
                                <td>
                                    <input type="hidden" name="awbOriginId" id="awbOriginId" value="" class="form-control" style="width:200px;height:40px"/>
                                    <input type="hidden" name="awbOrigin" id="awbOrigin" value="" class="form-control" style="width:200px;height:40px"/>
                                    <input type="hidden" name="truckOriginId" id="truckOriginId1" value="0" class="form-control" style="width:200px;height:40px">
                                    <input type="hidden" name="truckOriginName" id="truckOriginName1" value="" class="form-control" style="width:200px;height:40px"  onCopy="return false" onDrag="return false" onDrop="return false" onPaste="return false" onkeyup="return checkKey(this, event, 'truckOriginId1')">

                                    <select id="originId" class="form-control" style="width:200px;height:40px"  onchange="setClassError('originSpan', 1, this.value);
                                            setContractOriginDestination(this.value);" >
                                        <option value="0">---select--</option>
                                        <c:if test="${getPointList1 != null}">
                                            <c:forEach items="${getPointList1}" var="point">
                                                <option value="<c:out value="${point.cityId}"/>"><c:out value="${point.cityName}"/></option>
                                            </c:forEach>
                                        </c:if>
                                    </select>

                                </td>
                                <td><font color="red">*</font>Destination</td>
                                <td>
                                    <input type="hidden" name="awbDestinationId" id="awbDestinationId" value="0" class="form-control" style="width:200px;height:40px"/>
                                    <input type="hidden" name="awbDestination" id="awbDestination" value="0" class="form-control" style="width:200px;height:40px"/>
                                    <input type="hidden" name="truckDestinationId" id="truckDestinationId1" value="0">
                                    <input type="hidden" name="truckDestinationName" id="truckDestinationName1" value="0" class="form-control" style="width:200px;height:40px"  onCopy="return false" onDrag="return false" onDrop="return false" onPaste="return false" onkeyup="return checkKey(this, event, 'truckDestinationId1')">
                                    <select id="destinationId1" name="destinationId1" 
                                            onchange=" setContractDestination(this.value);
                                                    autoRateWeight(0);
                                                    setClassError('destSpan', 1, this.value);" class="form-control" style="width:200px;height:40px">
                                        <option value="0">---select--</option>
                                    </select>
                                </td>
                            </tr>                            

                            <tr  id="contractRouteDiv2" style="display:none;">
                                <td><font color="red">*</font>Origin</td>
                                <td >
                                    <select id="originTemp" name="originTemp" onchange='setDestination();' class="form-control" style="width:200px;height:40px">
                                        <option value="0">-Select-</option>
                                    </select>
                                </td>
                                <td ><font color="red">*</font>Destination</td>
                                <td ><select id="destinationTemp" name="destinationTemp" class="form-control" onchange="setRouteDetails();"  style="width:200px;height:40px">
                                        <option value="0">-Select-</option>
                                    </select></td>
                            <input type="hidden" name="origin" id="origin" value="" />
                            <input type="hidden" name="destination" id="destination" value="" />
                            <input type="hidden" name="originName" id="originName" value="" />
                            <input type="hidden" name="destinationName" id="destinationName" value="" />                                
                            </tr>  

                            <tr id="contractRouteDiv4">
                                <td><font color="red">*</font>New Route</td>                                
                                <td><input type="checkbox" name="newroute" id="newroute" onClick="checkNewCity();"/></td>                                
                                <td></td>                                
                                <td></td>                                
                            </tr>
                            <input type="hidden" name="newRouteFlag" id="newRouteFlag" value="0"/>

                            <tr id="contractRouteDiv3">
                                <td><font color="red">*</font>Origin</td>
                                <td>
                                    <input type="text" name="gloriginName" id="gloriginName" value="" class="form-control" style="width:200px;height:40px" value="<c:out value="${originName}"/>" onkeypress="return onKeyPressBlockNumbers(event);" onkeyup="setCityName1();"  autocomplete="off" maxlength="50" placeholder="Enter a location"/>
                                </td>
                                <td ><font color="red">*</font>Destination</td>
                                <td>
                                    <input type="text" name="gldestinationName" id="gldestinationName" value="" class="form-control" style="width:200px;height:40px" value="<c:out value="${destinationName}"/>" onkeypress="return onKeyPressBlockNumbers(event);" onkeyup="setCityName2();"  autocomplete="off" maxlength="50" placeholder="Enter a location"/>
                                </td>        
                            <input type="hidden" name="originCity" id="originCity" value="" />
                            <input type="hidden" name="destinationCity" id="destinationCity" value="" />   
                            </tr>


                            <tr id="contractRouteDiv5">
                                <td >&nbsp;Origin Latitude</td>
                                <td ><input type="text" name="originlatitude" id="originlatitude" class="form-control" style="width:200px;height:40px"   value="<c:out value="${originlatitude}"/>"  readOnly maxlength="50"></td>
                                <td >&nbsp;Origin Longitude</td>
                                <td ><input type="text" name="originlongitude" id="originlongitude"   value="<c:out value="${originlatitude}"/>" readOnly class="form-control" style="width:200px;height:40px" ></td>                            
                            </tr>
                            <tr id="contractRouteDiv6">
                                <td >&nbsp;Destination Latitude</td>
                                <td ><input type="text" name="destinationlatitude" id="destinationlatitude" class="form-control" style="width:200px;height:40px"   value="<c:out value="${destinationlatitude}"/>"  readOnly maxlength="50"></td>
                                <td >&nbsp;Destination Longitude</td>
                                <td ><input type="text" name="destinationlongitude" id="destinationlongitude"   value="<c:out value="${destinationlongitude}"/>" readOnly class="form-control" style="width:200px;height:40px" ></td>
                            </tr>

                        </table>

                        <table  class="table table-info mb30 table-hover"  id="contractFreightTable1" >
                            <thead>
                                <tr>                                    
                                    <th height="30"  style="width:200px;height:40px">Freight Details</th>
                                    <th height="30"  style="width:200px;height:40px">Vehicle Type</th>
                                    <th height="30"  style="width:200px;height:40px">Total No of Pallets</th>
                                    <th height="30"  style="width:200px;height:40px">Pallet Description</th>
                                    <th height="30"  style="width:200px;height:40px">Freight Amount</th>
                                    <th height="30"  style="width:200px;height:40px">Special Instruction</th>
                                </tr>
                            </thead>
                            <tr>
                                <td><div id="custtype"></div></td>
                                
                                    <td> <select name="vehTypeIdTemp" id="vehTypeIdTemp" class="form-control" style="width:200px;height:40px;" >
                                            <c:if test="${vehicleTypeList != null}">
                                                <option value="0" selected>--Select--</option>
                                                <c:forEach items="${vehicleTypeList}" var="vehicleType">
                                                    <option value="<c:out value="${vehicleType.vehicleTypeId}"/>"><c:out value="${vehicleType.vehicleTypeName}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select></td>
                              


                                </td>
                                <td><input type="text" id="totalPackage" name="totalPackage" value="0" style="width:200px;height:40px" onchange="autoRateWeight(this.value);"/></td>
                                <td><input type="text" id="totalChargeableWeights" name="totalChargeableWeights" value="" onKeyPress='return onKeyPressBlockCharacters(event);' style="width:200px;height:40px" onchange="autoRateWeight(this.value);"/></td>
                                <td><input type="text" id="totFreightAmount" name="totFreightAmount" value="" style="width:200px;height:40px" onKeyPress='return onKeyPressBlockCharacters(event);'/></td>
                                <td>
                                    <textarea name="bookingReferenceRemarks" id="bookingReferenceRemarks1" style="width:200px;height:40px">-</textarea>
                                </td>
                            <input type="hidden" name="rateMode" id="rateMode" value="1"/>
                            <input type="hidden" name="rateMode1" id="rateMode1" value="1"/>
                            <input type="hidden" name="rateValue" id="rateValue" value="0"  class="form-control" style="width:200px;height:40px"  readonly/>
                            <input type="hidden"  name="perKgAutoRate" id="perKgAutoRate" value="0"  class="form-control" style="width:200px;height:40px"/>
                            </tr>

                        </table>

                        <table border="0"  align="center" width="97.5%" cellpadding="0" cellspacing="0" >
                            <tr>
                                <td height="30" >
                            <center>
                                <input type="button" class="btn btn-success" name="Save" value="Create Order" id="createOrder" onclick="createOrderSubmit(this.value);" >
                                <input type="button" class="btn btn-success" name="emptyTrip" value="Empty Trip" id="emptyTrip" onclick="createEmpty(this.value);" >
                            </center>
                            </td>
                            </tr>
                        </table>
                        <br>
                        <input type="hidden" name="noOfPieces" id="noOfPieces1" value="0" >
                        <input type="hidden" name="uom" id="uom1" value="2" >
                        <input type="hidden" name="lengths" id="lengths1" value="0"  >
                        <input type="hidden" name="widths" id="widths1"   value="0"  >
                        <input type="hidden" name="heights" id="heights1" value="0" >
                        <input type="hidden" name="chargeableWeight" id="chargeableWeight1" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"  class="form-control" value="" style="width : 80px" readonly >
                        <input type="hidden" name="chargeableWeightId" id="chargeableWeightId1" value="0">
                        <input type="hidden" name="volumes" id="volumes1"  onKeyPress="return onKeyPressBlockCharacters(event);"  class="form-control" value="0">

                        <input type="hidden" name="contract" id="contract" value="0" />
                        <input type="hidden" id="totalVolumeActual" value="0" name="totalVolumeActual" />
                        <input type="hidden" id="totalVolume" name="totalVolume" value="0"/>
                        <input type="hidden" name="productRate" id="productRate" class="form-control"   value="2" />
                        <input type="hidden" name="approvalStatus" id="approvalStatus" value="1" />
                        <input type="hidden" name="orderDeliveryHour" id="orderDeliveryHour" value="00"/>
                        <input type="hidden" name="orderDeliveryMinute" id="orderDeliveryMinute" value="00"/>
                        <!--<input type='hidden' name='productCategoryId' id='productCategoryId' value='55'/>-->
                        <input type="hidden" name="commodity" id="commodity" class="form-control"  value="Cement" />

                        <input type="hidden" name="awbOrderDeliveryDate" id="awbOrderDeliveryDate" value="<%=startDate%>"/>
                        <input type="hidden" id="awbOriginRegionId" name="awbOriginRegionId" value="0"/>
                        <input type="hidden" id="awbMovementType" name="awbMovementType" value="0"/>
                        <input type="hidden" name="awbOriginRegion" id="awbOriginRegion" value="0" on class="form-control" style="width: 120px;"  onkeyup="return checkKey(this, event, 'awbOriginRegionId')"/>
                        <input type="hidden" id="awbDestinationRegionId" name="awbOriginRegionId" value="0"/>
                        <input type="hidden" name="awbDestinationRegion" id="awbDestinationRegion" value="0" class="form-control" style="width: 120px;"  onkeyup="return checkKey(this, event, 'awbDestinationRegionId')"/>
                        <input type="hidden" name="currencyType" id="currencyType" value="1">
                        <input type="hidden" name="customerRank" id="customerRank" value="0" />
                        <input type="hidden" name="outStanding" id="outStanding" value="0" />
                        <input type="hidden" name="creditLimit" id="creditLimit" value="0" />                        

                        <input type="hidden" name="customerAddress" id="customerAddress" class="form-control" />
                        <input type="hidden" name="pincode" id="pincode" class="form-control" />
                        <input type="hidden" name="customerMobileNo" id="customerMobileNo" class="form-control" />
                        <input type="hidden" name="customerPhoneNo" id="customerPhoneNo" class="form-control" />
                        <input type="hidden" name="mailId" id="mailId" class="form-control" />

                        <input type="hidden" name="billingTypeName" id="billingTypeName" class="form-control" />
                        <input type="hidden" name="billTypeName" id="billTypeName" class="form-control" />
                        <input type="hidden" name="contractNo" id="contractNo" class="form-control" />
                        <input type="hidden" name="billingTypeId" id="billingTypeId" class="form-control" />
                        <input type="hidden" name="contractExpDate" id="contractExpDate" class="form-control" />

                        <input type="hidden" class="form-control" name="destinationId" id="destinationId" >
                        <input type="hidden" class="form-control" name="routeContractId" id="routeContractId" >
                        <input type="hidden" class="form-control" name="contractId" id="contractId" value="0" >
                        <input type="hidden" class="form-control" name="routeId" id="routeId" >
                        <input type="hidden" class="form-control" name="routeBased" id="routeBased" >
                        <input type="hidden" class="form-control" name="contractRateId" id="contractRateId" >
                        <input type="hidden" class="form-control" name="totalKm" id="totalKm" >
                        <input type="hidden" class="form-control" name="totalHours" id="totalHours" >
                        <input type="hidden" class="form-control" name="totalMinutes" id="totalMinutes" >
                        <input type="hidden" class="form-control" name="totalPoints" id="totalPoints" >
                        <input type="hidden" class="form-control" name="vehicleTypeId" id="vehicleTypeId" >
                        <input type="hidden" class="form-control" name="rateWithReefer" id="rateWithReefer" >
                        <input type="hidden" class="form-control" name="rateWithoutReefer" id="rateWithoutReefer" >
                        <input type="hidden" readonly class="datepicker" name="vehicleRequiredDate" id="vehicleRequiredDate" >

                        <input type="hidden" name="wareHouseName" id="wareHouseName" value="MAA" class="form-control" style="width: 80px"/>
                        <input type="hidden" name="wareHouseLocation" id="wareHouseLocation" value="MAA" class="form-control" style="width: 80px"/>
                        <input type="hidden" name="freightReceipt" id="freightReceipt" value="YES" class="form-control" style="width: 80px"/>
                        <input type="hidden" name="shipmentAccpHour" id="shipmentAccpHour" value="00" class="form-control" style="width: 80px"/>
                        <input type="hidden" name="shipmentAccpMinute" id="shipmentAccpMinute" value="00" class="form-control" style="width: 80px"/>
                        <input type="hidden" name="shipmentAcceptanceDate" id="shipmentAcceptanceDate" value="" class="datepicker" style="width: 80px" data-date-format='dd-mm-yyyy'/>

                        <input type="hidden" readonly  name="vehicleRequiredHour" id="vehicleRequiredHour" value='00'>
                        <input type="hidden" readonly  name="vehicleRequiredMinute" id="vehicleRequiredMinute"  value='00'>
                        <input type="hidden" name="entryType" value="1" checked="">
                        <input type="hidden" id="paymentType" name="paymentType" value="0">
                        <input type="hidden" class="form-control" name="multiPickup" id="multiPickup" onclick="multiPickupShow()" value="N">
                        <input type="hidden" class="form-control" name="multiDelivery" id="multiDelivery" onclick="multiDeliveryShow()" value="N">
                        <input type="hidden" class="form-control" name="consignmentOrderInstruction" id="consignmentOrderInstruction">
                        <input type="hidden" readonly  name="reeferRequired" id="reeferRequired"  class="form-control"  value="No" />


                        <script type="text/javascript">

                            function checkEmpty(obj) {
                                if (obj.value == '') {
                                    obj.value = 0;
                                } else {
                                    var myString = obj.value;
                                    var result = (myString[0] == '0') ? myString.substr(1) : myString;
                                    obj.value = result;
                                }
                            }


                            function setClassError(id, type, value) {
                                if (type == 1) {
                                    if (value == 0) {
                                        // ErrorClass
                                        $('#' + id).removeClass('success');
                                        $('#' + id).addClass('error');
                                    } else {
                                        // NoErrorClass
                                        $('#' + id).removeClass('error');
                                        $('#' + id).addClass('success');
                                    }
                                } else {
                                    if (value == '') {
                                        // ErrorClass
                                        $('#' + id).removeClass('success');
                                        $('#' + id).addClass('error');
                                    } else {
                                        // NoErrorClass
                                        $('#' + id).removeClass('error');
                                        $('#' + id).addClass('success');
                                    }
                                }
                            }
                        </script>

                        <script type="text/javascript">
                            $(document).ready(function() {
                                $("#datepicker").datepicker({
                                    showOn: "button",
                                    buttonImage: "calendar.gif",
                                    buttonImageOnly: true

                                });
                            });
                            $(function() {
                                //alert("cv");
                                $(".datepicker").datepicker({
                                    /*altField: "#alternate",
                                     altFormat: "DD, d MM, yy"*/
                                    changeMonth: true, changeYear: true
                                });
                            });</script> 
                        <style>
                            .errorClass { 
                                border:  1px solid red; 
                            }
                            .noErrorClass { 
                                border:  1px solid black; 
                            }
                        </style>
                        <script type="text/javascript">
                            $(function() {
                                //alert("cv");
                                $(".datepicker").datepicker({
                                    //                    /*altField: "#alternate",
                                    dateFormat: "dd-mm-yy",
                                    changeMonth: true, changeYear: true
                                });
                            });</script>
                        <script type="text/javascript">
                            $(document).ready(function() {
                                //Disable full page
                                $('body').bind('cut copy paste', function(e) {
                                    e.preventDefault();
                                });
                                //Disable part of page
                                $('#id').bind('cut copy paste', function(e) {
                                    e.preventDefault();
                                    return false;
                                });
                            });</script>
                        <script type="text/javascript">

                            function onKeyPressBlockCharacters(e, value) {
                                var sts = false;
                                var key = window.event ? e.keyCode : e.which;
                                var keychar;
                                keychar = String.fromCharCode(key);
                                reg = /[a-zA-Z-`~!@#$%^&*() _|+\-=?;:'",<>\{\}\[\]\\\/]/gi;
                                sts = !reg.test(keychar);
                                if (value.indexOf(".") > -1 && (value.split('.')[1].length > 1) && key != 46 && key != 8) {
                                    sts = false;
                                }
                                if (value.split('.').length > 1 && key == 46) {
                                    sts = false;
                                }
                                //            if(e.button==2){
                                //                 sts = false;
                                //            }

                                return sts;
                            }
                            function onKeyPressBlockCharacters(e) {
                                var sts = false;
                                var key = window.event ? e.keyCode : e.which;
                                var keychar = String.fromCharCode(key);
                                var reg = /[a-zA-Z-`~!@#$%^&*() _|+\-=?;:.'",<>\{\}\[\]\\\/]/gi;
                                sts = !reg.test(keychar);
                                return sts;
                            }
                            function onKeyPressBlockCharacters(e) {
                                var sts = false;
                                var key = window.event ? e.keyCode : e.which;
                                keychar = String.fromCharCode(key);
                                reg = /[a-zA-Z-`~!@#$%^&*()_|+\-=?;:'",<>\{\}\[\]\\\/]/gi;
                                sts = !reg.test(keychar);
                                //             var reg = /[a-zA-Z-`~!@#$%^&*() _|+\-=?;:.'",<>\{\}\[\]\\\/]/gi;
                                //            sts = !reg.test(keychar);
                                return sts;
                            }

                            function setClass(id, type) {

                                if (type == 1) {
                                    // ErrorClass
                                    $('#' + id).removeClass('success');
                                    $('#' + id).addClass('error');
                                } else {
                                    // NoErrorClass
                                    $('#' + id).removeClass('error');
                                    $('#' + id).addClass('success');
                                }
                            }

                            function checkField(field, value, fieldName, checkVal, alertName) {
                                alert(field + " - " + checkVal + " - " + fieldName);
                                checkField(customerId, value, "customerName", '', "Customer name is empty");
                                if (field == checkVal && value == '') {
                                    setClass(fieldName, 1);
                                    validateStatus = 1;
                                } else if (field == checkVal && value == 'Create Order') {
                                    setClass(fieldName, 1);
                                    validateStatus = 1;
                                    alertCount++;
                                    if (alertCount <= 8) {
                                        alertNames = alertNames + '\n' + alertName + ",";
                                    }
                                } else {
                                    setClass(fieldName, 2);
                                    validateStatus = 0;
                                }

                                alert(validateStatus);

                                if (validateStatus == 1) {
                                    $("#createOrder").show();
                                }

                                return validateStatus;
                            }

                        </script>

                    </div>
                    </body>
            </div>
            </form>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
