<%-- 
Document   : manifestPrintView
Created on : Mar 5, 2015, 12:42:22 PM
Author     : srinientitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
    <head>

    </head>
    <body>
        <form name="tsaPrint" method="post">
            <br><br>
            <input type="hidden" id="totalWeigthHidden" value="0">
            <input type="hidden" id="totalPackagehHidden" value="0">
            <div id="printDiv">
                <table align="center">
                    <tr><td><font size="4"><center>CARGO MANIFEST.<br></center></font></td></tr>
                </table>
                <br>
                <table align="center" border="0" width="80%">
                    <tr><td><font size="4">Operator/Truck :&nbsp;</font><b><c:out value="${operatorName}"/></b></td><td><font size="4">Flight No :&nbsp;</font><b><c:out value="${flightNo}"/></b></td></tr>
                    <tr><td><font size="4">Manifest Date :&nbsp;&nbsp;&nbsp;</font><b><c:out value="${manifestDate}"/></b></td>
                        <td><font size="4"><c:choose>
                                    <c:when test="${shipType ==1}">
                                        IGM No :<b><c:out value="${consignmentNoteEmno}"/></b>
                                    </c:when>
                                    <c:otherwise>
                                        EGM No :<b><c:out value="${consignmentNoteEmno}"/></b>
                                    </c:otherwise>
                                </c:choose></font><b></b></td></tr>
                    <tr><td><font size="4">Loading Point :&nbsp;&nbsp;&nbsp;</font><b><c:out value="${lodingPoint}"/></b></td><td><font size="4">UnLoading Point :&nbsp;&nbsp;</font><b><c:out value="${unLodingPoint}"/></b></td></tr>
                    <tr><td><font size="4">ATD No :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><b><c:out value="${consignmentNoteAtdNo}"/></b></td><td>&nbsp;</td></tr>
                </table>
                <br>
                <c:if test="${manifestViewList != null}">

                    <table align="center" border="0" id="table" class="sortable" style="width:1200px;font-size: 17px;" >
                        <thead>
                            <tr>
                                <th align="center">S.No</th>
                                <th align="center">MAWB No</th>
                                <th align="center">No Of Pcs</th>
                                <th align="center">Nature Of Goods</th>
                                <th align="center">Weight</th>
                                <th align="center">Origin</th>
                                <th align="center">Destination</th>
                                <th align="center">For Office Use</th>
                            </tr>
                        </thead>
                        <% int index = 1;%>
                        <c:forEach items="${manifestViewList}" var="tsiViewList">
                            <tr>
                                <%
                                            String classText = "";
                                            int oddEven = index % 2;
                                            if (oddEven > 0) {
                                                classText = "text2";
                                            } else {
                                                classText = "text1";
                                            }
                                %>
                                <td align="center" class="<%=classText%>"  ><%=index%></td>
                                <td align="center" class="<%=classText%>"   ><c:out value="${tsiViewList.consignmentAwbNo}"/>
                                    <input type="hidden" name="consignmentOrderNos"  id="consignmentOrderId<%=index%>" class="textbox"  value="<c:out value="${tsiViewList.consignmentOrderId}"/>"/>
                                </td>
                                <td class="<%=classText%>"   align="center">
                                    <c:if test="${tsiViewList.shipmentType == '1' }">
                                        <c:out value="${tsiViewList.totalPackages}"/>
                                        <c:set var="totalPackages" value="${totalPackages + tsiViewList.totalPackages}"/>
                                    </c:if>
                                    <c:if test="${tsiViewList.shipmentType == '2' }">
                                        <c:out value="${tsiViewList.totalPackages}"/>/<c:out value="${tsiViewList.awbTotalPackages}"/>
                                        <c:set var="totalPackages" value="${totalPackages + tsiViewList.receivedPackages}"/>
                                    </c:if>
                                </td>
                                <td align="center" class="<%=classText%>"><c:out value="${tsiViewList.commodity}"/></td>
                                <td class="<%=classText%>" align="center">
                                    <c:if test="${tsiViewList.shipmentType == '1' }">
                                        <c:out value="${tsiViewList.totalWeight}"/>
                                        <c:set var="totalWeight" value="${totalWeight + tsiViewList.totalWeight}"/>
                                    </c:if>
                                    <c:if test="${tsiViewList.shipmentType == '2' }">
                                        <c:set var="totalWeight" value="${totalWeight + tsiViewList.receivedWeight}"/>
                                        <c:out value="${tsiViewList.totalWeight}"/>/<c:out value="${tsiViewList.awbTotalGrossWeight}"/>
                                    </c:if>
                                </td>
                                <td align="center" class="<%=classText%>"   ><c:out value="${tsiViewList.awbOriginName}"/></td>
                                <td align="center" class="<%=classText%>"   ><c:out value="${tsiViewList.awbDestinationName}"/>

                                    <!--                            <script>
                                                                var totalPackage='<c:out value="${tsiViewList.totalWeight}"/>';
                                                                document.getElementById("totalWeigthHidden").value=parseInt(totalPackage)+parseInt(document.getElementById("totalWeigthHidden").value);
                                                                var totalWeight='<c:out value="${tsiViewList.totalPackages}"/>';
                                                                document.getElementById("totalPackagehHidden").value=parseInt(totalWeight)+parseInt(document.getElementById("totalPackagehHidden").value);
                                                            </script>-->
                                </td>

                                <td class="<%=classText%>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>                    
                                <%++index;%>
                            </tr>
                        </c:forEach>
                        <tr class="text1" height="25"><td colspan="8">&nbsp;</td></tr>
                        <tr>
                            <td align="center" class="text2"  colspan="2" ><b>Total</b></td>
                            <td align="center" class="text2"><c:out value="${totalPackages}"/>
                            </td><td class="text2">&nbsp;</td>
                            <td align="center" class="text2"><c:out value="${totalWeight}"/></td>
                            <td class="text2">&nbsp;</td>
                            <td class="text2">&nbsp;</td>
                            <td class="text2">&nbsp;</td>
                        </tr>
                        <tr height="25"><td colspan="8">&nbsp;</td></tr>
                        <tr>
                            <td colspan="5">&nbsp;</td>
                            <td colspan="3" align="left" >EFO/IFO Customs</td>
                        </tr>
                        <tr height="25"><td colspan="8">&nbsp;</td></tr>
                        <tr>
                            <td colspan="8"  align="right" >Page 01 of 01</td>
                        </tr>
                        <!--                    <script>
                                                
                                                document.getElementById("totalPackage").innerHTML=document.getElementById("totalPackagehHidden").value;
                                                document.getElementById("totalWeight").innerHTML=document.getElementById("totalWeigthHidden").value;
                                            </script>-->


                    </table>
                </div>
                <br><br>
                <center>
                    <input type="button" class="button" name="Print"  value="Print" onclick="printPage()"/>&nbsp;&nbsp;&nbsp;<input type="button" class="button" name="Close"  value="Close" onclick="closeWindow()"/>
                </center>

            </c:if>
        </form>
        <script type="text/javascript">
            function printPage()
            {
                var DocumentContainer = document.getElementById('printDiv');
                var WindowObject = window.open('', "Manifest Print View",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                //WindowObject.close();   
            }

//                            function printPage(){
//                                window.print();
//                                window.close();
//                            }
            function closeWindow() {
                window.close();
            }
        </script>

    </body>
</html>