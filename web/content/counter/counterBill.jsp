

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


    <%@ page import="ets.domain.renderservice.business.RenderServiceTO" %>
    <%@ page import="ets.domain.mrs.business.MrsTO" %>
    <%@ page import="ets.domain.problemActivities.business.ProblemActivitiesTO" %>


    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    <%@ page import="java.util.*" %>

    <%@ page import="java.text.DecimalFormat" %>
    <%@ page import="java.text.NumberFormat" %>
    <title>Job Card Bill</title>

</head>







<script>



    function print(val)
    {
        var DocumentContainer = document.getElementById(val);
        var WindowObject = window.open('', "TrackHistoryData",
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();
    }


</script>


<body>

<%

int itemNameLength = 35,uomLength=10;
MrsTO itemTO=null;
ProblemActivitiesTO activityTO = null;
int pageSize = 24;
String[] activity = null;
//ArrayList billRecord = (ArrayList) request.getAttribute("billRecord");
ArrayList billItems = new ArrayList();
billItems = (ArrayList)request.getAttribute("billItems");
int totalRecords = billItems.size();
int itemSize = billItems.size();
//int laborSize = (Integer) request.getAttribute("laborSize") ;
//int taxSize = (Integer) request.getAttribute("taxSize") ;
float laborTotal = 0;
float spareTotal = 0;
float taxTotal = 0;
double total = 0;
int index = 0;
int loopCntr = 0;



for(int i=0;i< itemSize; i=i+pageSize){
loopCntr = i;
%>

<div id="print<%= i %>" >

<style>


.text1 {
font-family:Tahoma;
font-size:14px;
color:#333333;
background-color:#E9FBFF;
padding-left:10px;
line-height:15px;
}
.text2 {
font-family:Tahoma;
font-size:12px;
color:#333333;
background-color:#FFFFFF;
padding-left:10px;
line-height:15px;
}


</style>


    <c:if test= "${billHeaderInfo != null}">
        <c:forEach items="${billHeaderInfo}" var="bill" >
    <table width="725" border="1" cellspacing="0" cellpadding="0"  style="border:1px; border-color:#E8E8E8; border-style:solid;" >
        <tr>
            <td height="27" colspan="4" class="text2"  align="center"  scope="row" style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
               <b> CREDIT INVOICE </b>
            </td>
        </tr>
        <c:set var="counterNo" value="${bill.counterId}" />
        <c:set var="createdDate" value="${bill.date}" />
        <c:set var="spareRetAmount" value="${bill.spareRetAmount}" />
        <c:set var="spareNettAmount" value="${bill.spareAmount}" />
        <c:set var="servicePtName" value="${bill.mrsCompanyName}" />
        <c:set var="remarks" value="${bill.remarks}" />
        <c:set var="nettAmount" value="${bill.nettAmount}" />
        <c:set var="discount" value="${bill.discount}" />
        <c:set var="tax" value="${bill.tax}"/>
        <c:set var="spareTaxAmount" value="${bill.spareTaxAmount}"/>
        <c:set var="discountAmount" value="${bill.discountAmount}"/>
        <% //float grandTotal = (Float)pageContext.getAttribute("nettAmount");
//System.out.println("Page Context Variable out-->"+grandTotal);
%>
        <tr>
            <td class="text2" colspan="2" width="427"  align="left" rowspan="2" scope="col" style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; " >
                <p> <b> Your Company. </b><br>
                   address1,<br>
                    address 2,<br>
                    Pin Code xxx xxx<br>
            E-mail : abc@xyz.com</p></td>
            <td  class="text2"  align="left"  width="150" height="53" scope="col"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; " >
                <p>Invoice No<br>
                    <c:if test= "${bill.invoiceNo != ''}"  >
                <b> <c:out value="${bill.invoiceNo}" /> </b>
                </c:if>
                <c:if test= "${bill.invoiceNo == ''}"  >
                <b> <c:out value="${bill.billNo}" /> </b>
                </c:if>
                </p>
            </td>
            <td  class="text2"  align="left"  width="148" scope="col"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  " >
                <p>Dated<br>
                <b> <c:out value="${bill.date}" /> </b> </p>
            </td>
        </tr>
        <tr>
            <td class="text2"  align="left" scope="col"  style=" padding-bottom:3px; padding-top:3px; border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; ">
                <p>Vehicle No.<br>
                <b> - </b> </p>
            </td>
            <td  class="text2"  align="left" scope="col"  style="padding-bottom:3px; padding-top:3px; border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
                <p>Other Reference<br>
                <b><c:out value="${bill.name}" /></b>
                </p>
            </td>
        </tr>
        <tr>
            <td colspan="2" rowspan="4" align="left" valign="top" class="text2" scope="col"  style="border:1px; border-right-color:#CCCCCC; border-right-style:solid; ">
                <p>Buyer<br>
                <b><c:out value="${bill.name}" /></b> <br>
                 <c:out value="${bill.address}" /> <br>
                 <c:out value="${bill.mobileNo}" /> <br>
                     <p>
            </td>
            <td class="text2"  align="left"  style="border:1px; border-right-color:#CCCCCC; border-right-style:solid;border-bottom-color:#CCCCCC; border-bottom-style:solid; ">
                <p>Buyer's Order No.<br>
                &nbsp;</p>
            </td>
            <td class="text2"  align="left"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                <p>Dated<br>
                &nbsp;</p>
            </td>
        </tr>
        <tr>
            <td class="text2"  align="left"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; ">
                <p>Despatch Document  No.<br>
                &nbsp;</p>
            </td>
            <td class="text2"  align="left"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; ">
                <p>Dated<br>
                &nbsp;</p>
            </td>
        </tr>
        <tr>
            <td class="text2"  align="left"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; ">
                <p>Despatched through <br>
                &nbsp;</p>
            </td>
            <td class="text2"  align="left"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; ">
                <p>Destination<br>
                &nbsp;</p>
            </td>
        </tr>
        <tr>
            <td  class="text2"  colspan="2" align="left"  style="border:1px;">
                <p>Terms of Delivery <br>
                <p> &nbsp;
            </td>
        </tr>
    </table>
    </c:forEach>
</c:if>








    <table width="725" height="99"  border="0" cellpadding="0"  style="border:1px; border-left-color:#E8E8E8; border-left-style:solid; border-right-color:#E8E8E8; border-right-style:solid;"  cellspacing="0">
        <tr>
	    <th width="30" class="text2"  height="30" style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid; " scope="col" >Sno</th>
            <th class="text2" width="407"  height="30"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; " scope="col" >Description of Goods </th>
            <th width="65"  class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; "  scope="col">Price</th>
            <th width="50" class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; "  scope="col">VAT %</th>
            <th width="50"  class="text2" style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; "  scope="col">Qty.</th>
            <th width="50"  class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; " scope="col">UOM</th>
            <th width="65"  class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; " scope="col">Amount</th>
        </tr>

<% if(loopCntr <= itemSize ){ %>
<%

        for(int j=loopCntr; ( j < (itemSize) ) && (j < i + pageSize  ) ; j++){

       ++loopCntr;
       itemTO = new MrsTO();
       itemTO = (MrsTO) billItems.get(j);
String itemName = "";
String uom = "";
       itemName = itemTO.getItemName();
       uom = itemTO.getUomName();
       if(itemName.length() > itemNameLength){
           itemName = itemName.substring(0,itemNameLength-1);
       }
       if(uom.length() > uomLength){
           uom = uom.substring(0,uomLength-1);
       }

%>



        <tr>
            <td  class="text2"  align="left"  style="border:1px; border-color:#FFFFFF; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                <% index++; %> <%= index %>
            </td>
            <td  class="text2"   align="left"   style="border:1px; border-color:#FFFFFF; border-left-color:#CCCCCC; border-left-style:solid; "  scope="row" >
                <%= itemName %>
            </td>
            <td  class="text2"   style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid; "  align="right" >
                <%= itemTO.getPrice() %>
            </td>
            <td  class="text2"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;"   align="center" >
                <%= itemTO.getTax() %>
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-left-color:#CCCCCC; border-left-style:solid; "  align="center" >
                <%= itemTO.getQuantityRI() %>
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;border-left-color:#CCCCCC; border-left-style:solid; "  align="center" >
                <%= uom %>
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; "  align="right" >
                <%= itemTO.getNettAmount() %>
            </td>
            <% spareTotal =(float) spareTotal + (itemTO.getNettAmount()); %>
        </tr>
        
    <% } %>
    


    <% if( (loopCntr < i+pageSize ) && (loopCntr == itemSize) ) { %>

        <tr>
            <td  class="text2"  align="right"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
               &nbsp;
            </td>
            <td  class="text2"  align="right"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                 &nbsp;
            </td>
            <td  class="text2" align="right"  style="border:1px; border-color:#FFFFFF; border-left-color:#CCCCCC; border-left-style:solid; "  scope="row" >
                &nbsp;
            </td>
            <td  class="text2"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid; " align="right" >
                &nbsp;
            </td>
            <td  class="text2"   style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" align="right" >
                &nbsp;
            </td>
            <td  class="text2"   style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" align="right" >
                &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;border-left-color:#CCCCCC; border-left-style:solid; "   align="right" >
                &nbsp;
            </td>
        </tr>

        <% } %>


<% } %>










<% total = (double) (spareTotal); %>

            <% for(int m=loopCntr; ( (m%pageSize) != 0); m++){ %>

        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;"  scope="row" >
                &nbsp;
            </td>
            <td class="text2" colspan="1"  align="left" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
               &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                &nbsp;
            </td>
        </tr>
            <% } %>

<% if(loopCntr != (totalRecords) ) { %>

        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                &nbsp;
            </td>
            <td class="text2" colspan="0"  align="left"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
               &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                &nbsp;
            </td>
        </tr>

                <tr>
	            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
	                &nbsp;
	            </td>
	            <td class="text2" colspan="0"  align="left"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
	               &nbsp;
	            </td>
	            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
	                &nbsp;
	            </td>
        </tr>

        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                &nbsp;
            </td>
            <td class="text2" colspan="0"  align="left" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
               &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid; border-bottom-color:#CCCCCC; border-bottom-style:solid;" scope="row" >
               &nbsp;
            </td>
            <td class="text2" colspan="0"  align="left" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid; border-bottom-color:#CCCCCC; border-bottom-style:solid;" scope="row" >
               &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-bottom-color:#CCCCCC; border-bottom-style:solid; "  align="right" >
                &nbsp;
            </td>
        </tr>


<% } if(loopCntr == (totalRecords) ) { %>


        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
				&nbsp;
            </td>
            <td class="text2" colspan="5" align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                Spare Net Amount
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                  <c:out value="${spareNettAmount}"/>
            </td>
        </tr>
        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
				&nbsp;
            </td>
            <td class="text2" colspan="5" align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                Tax &nbsp;<c:out value="${tax}"/>%
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                  <c:out value="${spareTaxAmount}"/>
            </td>
        </tr>
        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
				&nbsp;
            </td>
            <td class="text2" colspan="5" align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                Discount <c:out value="${discount}"/>%
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                <fmt:formatNumber pattern="##.00" value="${discountAmount}"/>
                  
            </td>
        </tr>
        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
				&nbsp;
            </td>
            <td class="text2" colspan="5" align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                Spare Return Amount
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                  <c:out value="${spareRetAmount}" />
            </td>
        </tr>






        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;border-bottom-style:solid; border-bottom-color:#CCCCCC; " scope="row" >
				&nbsp;
            </td>
            <td class="text2" colspan="5"  align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;border-bottom-style:solid; border-bottom-color:#CCCCCC;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
               <b> Grand Total </b>
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >


 			<%
float grandTotal1 = (Float)pageContext.getAttribute("nettAmount");
System.out.println("Page Context Variable out-->"+grandTotal1);
 %>
 <b> <%= java.lang.Math.ceil(grandTotal1) %></b>


                    
            </td>
            </tr>
                    <%--<jsp:useBean id="numberWords2"   class="ets.domain.report.business.NumberWords" >
 			<% numberWords2.setRoundedValue(String.valueOf(total));
                        NumberFormat formatter = new DecimalFormat("#0.00");
                        %>
                        </tr> <jsp:getProperty name="numberWords2" property="roundedValue" />
                        <b><%=formatter.format(java.lang.Math.ceil(total)) %> </b>
                    </jsp:useBean>--%>




<% } %>

<% if(loopCntr != (totalRecords) ) { %>

        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;  border-bottom-color:#CCCCCC; border-bottom-style:solid; " scope="row" >
                &nbsp;
            </td>
            <td class="text2" colspan="5"  align="center" style="border:1px; border-color:#FFFFFF;    border-bottom-color:#CCCCCC; border-bottom-style:solid; " scope="row" >
                <b>Contnd..</b>
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF;  border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid;"  align="right" >
               &nbsp;
            </td>
        </tr>






<% } %>

    </table>




<table width="725" height="79" border="0" style="border:1px; border-color:#E8E8E8; border-style:solid;"  cellpadding="0" cellspacing="0">
<tr>
<td width="720">
<div class="text2" >
 <% if(loopCntr == (totalRecords) ) { %>
 Amount Chargeable (in words)<br>
  <jsp:useBean id="numberWords1"   class="ets.domain.report.business.NumberWords" >
 			<%
float grandTotal = (Float)pageContext.getAttribute("nettAmount");
System.out.println("Page Context Variable out-->"+grandTotal);
numberWords1.setRoundedValue(String.valueOf(java.lang.Math.ceil(grandTotal))); %>

 			<% numberWords1.setNumberInWords(numberWords1.getRoundedValue()); %>

                    <b>  Rupees <jsp:getProperty name="numberWords1" property="numberInWords" /> Only</b>
                    </jsp:useBean>
<% } %>
 <br>
<br>

Remarks:
<% if(loopCntr == (totalRecords) ) { %>
<% if(request.getAttribute("vendorNames") != null ){ %>
<b> <%= request.getAttribute("vendorNames") %> </b>
<% } %>
<% } %>
<br>
<table height="10" align="left" cellpadding="0" cellspacing="0" border="0">



    <tr>
        <td width="200" class="text2">
            <b> AS PER <c:out value="${servicePtName}" />&nbsp;Counter No</b>
        </td>
        <td class="text2">
            :&nbsp;<b><c:out value="${counterNo}" /></b>
        </td>
    </tr>
    <tr>
        <td width="200" class="text2">
            <b> Spare Return:</b>
        </td>
        <td class="text2">
            :&nbsp;<b><c:out value="${remarks}" /></b>
        </td>
    </tr>
    <tr>
        <td width="200" class="text2">
        <b> Company's VAT TIN</b>
        </td>
        <td class="text2">
            :&nbsp;<b>33241085632</b>
        </td>
    </tr>
    <tr>
        <td width="200" class="text2">
        <b> Company's CST No</b>
        </td>
        <td class="text2">
            :&nbsp;<b>999473 - 28.05.08</b>
        </td>
    </tr>
    <tr>
        <td width="200" class="text2">
            <b>Buyer's VAT TIN/Sales Tax No</b>
        </td>
        <td class="text2">
            :&nbsp;
        </td>
    </tr>
<%--</table>
</div>

<table width="720" border="0" cellpadding="0" cellspacing="0">
--%>    <tr>
    <td class="text2" width="380" align="left"><b><br>Declaration</b> </td>
    <td class="text2" width="315" align="right"><b> for Your Company Name. </b> </td>
    </tr>
    <tr>
    <td class="text2" width="380" align="left">We declare that this invoice shows the actual price of the</td>
    <td class="text2" width="325" align="right"> &nbsp;</td>
    </tr>
    <tr>
    <td class="text2" width="380" align="left">goods described and that all particulars are true and correct</td>
    <td class="text2" width="325" align="right">&nbsp; </td>
    </tr>
    <tr>
    <td class="text2" width="380" align="left">&nbsp; </td>
    <td class="text2" width="315" align="right"><b> Authorized Signatory </b></td>
    </tr>

</table>

</div>

</td>
</tr>
</table>



<br>
<div ALIGN="CENTER" class="text2">
    SUBJECT TO CHENNAI JURISDICTION &nbsp; This is a Computer Generated Invoice
</div>
</div>
<br>
    <center>
        <input type="button" class="button" name="Print" value="Print" onClick="print('print<%= i %>');" > &nbsp;
    </center>
<br>
<br>
<% } %>


</body>
</html>
