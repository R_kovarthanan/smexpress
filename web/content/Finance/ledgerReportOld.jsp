<%-- 
    Document   : journalEntry
    Created on : Dec 11, 2012, 5:39:26 PM
    Author     : DineshKumar.s
--%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.contract.business.ContractTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });
        </script>

    </head>
    <script language="javascript">



        function submitPage()
        {
            //alert('am here..');
            var ledgerId = document.ledgerReport.ledgerId.value;
            if(ledgerId != '0')
            {
                document.ledgerReport.action = '/throttle/ledgerReportOld.do';
                document.ledgerReport.submit();
            }else{
                alert('please select ledger');
                document.lederReport.ledgerReport.focus();
            }
        }
        function setValues(){
            var ledgerId = <%=request.getAttribute("ledgerId")%>;
            if(ledgerId != null){
                document.ledgerReport.ledgerId.value = ledgerId;
            }
        }

        
    </script>
    <body onload="setValues();">
        <form name="ledgerReport" method="post" >
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                <tr>
                    <td >
                        <%@ include file="/content/common/path.jsp" %>
                    </td></tr></table>
            <!-- pointer table -->
            <!-- message table -->
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
                <tr>
                    <td >
                        <%@ include file="/content/common/message.jsp"%>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="850" id="bg" class="border">
                <tr>
                    <td class="contenthead" height="30"><div class="contenthead" align="center">Voucher</div></td>
                    <td class="contenthead" height="30">
                        <div class="contenthead" align="center">
                            <select class="textbox" name="ledgerId">
                                <option selected   value='0'>-select-</option>
                                <c:if test = "${journalLedgerList != null}" >
                                    <c:forEach items="${journalLedgerList}" var="jlist">
                                    <option  value='<c:out value="${jlist.ledgerID}"/>'><c:out value="${jlist.ledgerName}" /></option>
                                </c:forEach >
                                </c:if>
                                </select>

                        </div>
                    </td>
                    <td class="contenthead" height="30">
                        <input type="button" value="fetch data" class="button" onClick="submitPage();">
                    </td>
                </tr>

            </table>
            <p>&nbsp;</p>
            <div align="center">

                <c:if test = "${ledgerTransactionList != null}" >
                <table width="850" class="border" border="1">
                    <tr class="contentsub">
                        <td class="contentsub" width="10%" ><div class="contentsub">Date</div></td>
                        <td class="contentsub" width="70%"><div class="contentsub">Particulars</div></td>
                        <td class="contentsub" width="10%"><div class="contentsub">Debit</div></td>
                        <td class="contentsub" width="10%"><div class="contentsub">Credit</div></td>
                    </tr>
                    
                    
                    <c:forEach items="${ledgerTransactionList}" var="txnlist">
                        <tr>
                            <td class="text1" ><c:out value="${txnlist.accountEntryDate}" /></td>
                            <c:if test = "${txnlist.accountType == 'DEBIT'}" >
                                <td class="text1" ><c:out value="${txnlist.ledgerName}" />&nbsp; A/C &nbsp;&nbsp;&nbsp; Dr.</td>
                                <td class="text1" align ="right"><c:out value="${txnlist.accountAmount}" /></td>
                                <td class="text1" align ="right">&nbsp;</td>
                            </c:if>
                            <c:if test = "${txnlist.accountType == 'CREDIT'}" >
                                <td class="text1" >&nbsp;&nbsp;&nbsp;To &nbsp;<c:out value="${txnlist.ledgerName}" />&nbsp; A/C <br>
                                                    &nbsp;(Being <c:out value="${txnlist.accountNarration}" /> for Trip <c:out value="${txnlist.tripId}" />)</td>
                                <td class="text1" align ="right">&nbsp;</td>
                                <td class="text1" align ="right"><c:out value="${txnlist.accountAmount}" /></td>
                            </c:if>
                        </tr>
                    </c:forEach>
                    
                </table>
                </c:if>
            </div>
            
        </form>
    </body>
</html>