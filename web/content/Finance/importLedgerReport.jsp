<%-- 
    Document   : importLedgerReport
    Created on : May 14, 2013, 7:37:59 PM
    Author     : Entitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@page import="java.text.SimpleDateFormat"%>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }

        </style>
    </head>
    <body>
        <form name="viewTrips" action=""  method="post">
            <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "LedgerReport.xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>
            <br/>


            <c:if test = "${ledgerTransactionList != null}" >
                <%
                            int index = 1;
                            ArrayList ledgerTransactionList = (ArrayList) request.getAttribute("ledgerTransactionList");
                            if (ledgerTransactionList.size() != 0) {
                %>
                <table  border="1" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" id="bg">
                    <tr>
                        <td class="contentsub" height="30">S.No</td>
                        <td class="contentsub" height="30">Date</td>
                        <td class="contentsub" height="30">Particulars</td>
                        <td class="contentsub" height="30">Credit</td>
                        <td class="contentsub" height="30">Debit</td>
                    </tr>


                    <c:forEach items="${ledgerTransactionList}" var="txnlist">
                        <%
                                                        String classText = "";
                                                        int oddEven = index % 2;
                                                        if (oddEven > 0) {
                                                            classText = "text2";
                                                        } else {
                                                            classText = "text1";
                                                        }
                        %>
                        <tr>
                            <td class="<%=classText%>"  height="30" align="left"><%=index%></td>
                            <td class="text1" ><c:out value="${txnlist.accountEntryDate}" /></td>
                            <c:if test = "${txnlist.accountType == 'DEBIT'}" >
                                <c:set var="creditTotal" value="${creditTotal + txnlist.accountAmount}"/>
                                <td class="text1" ><c:out value="${txnlist.ledgerName}" />&nbsp; A/C &nbsp;&nbsp;&nbsp; Cr. <br>
                                    &nbsp;(Being <c:out value="${txnlist.accountNarration}" /> for Trip <c:out value="${txnlist.tripId}" />)</td>
                                <td class="text1" align ="right"><c:out value="${txnlist.accountAmount}" /></td>
                                <td class="text1" align ="right">&nbsp;</td>
                            </c:if>
                            <c:if test = "${txnlist.accountType == 'CREDIT'}" >
                                <c:set var="debitTotal" value="${debitTotal + txnlist.accountAmount}"/>
                                <td class="text1" >&nbsp;&nbsp;&nbsp;To &nbsp;<c:out value="${txnlist.ledgerName}" />&nbsp; A/C &nbsp;&nbsp;&nbsp; Dr. <br>
                                    &nbsp;(Being <c:out value="${txnlist.accountNarration}" /> for Trip <c:out value="${txnlist.tripId}" />)</td>
                                <td class="text1" align ="right">&nbsp;</td>
                                <td class="text1" align ="right"><c:out value="${txnlist.accountAmount}" /></td>
                            </c:if>
                        </tr>
                        <%index++;%>
                    </c:forEach>
                    <tr>
                        <td colspan="3" align="right" class="text2"  height="30"><b>Total</b></td>
                        <td  class="text2"  height="30"><b><c:out value="${creditTotal}" /></b></td>
                        <td  class="text2"  height="30"><b><c:out value="${debitTotal}" /></b></td>
                    </tr>
                </table>
                <%
                           }
                %>
            </c:if>
        </form>
    </body>
</html>
