<%-- 
    Document   : vendorPaymentView
    Created on : 03 Dec, 2018, 5:34:26 PM
    Author     : arun
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<!--    <script type="text/javascript">
        $(document).ready(function () {
            var date2 = new Date();
            $(".datepicker").datepicker({
                dateFormat: 'dd-mm-yy'
            }).datepicker('setDate', date2)

        });
    </script>-->
<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {

        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            dateFormat: 'dd-mm-yy',
            changeMonth: true, changeYear: true
        });

    });
</script>
<script type="text/javascript">

    function setValueHire() {

        // document.getElementById("totalNoOfTripsHire").innerHTML = '';
        var temp = 0;
        var temp1 = 0;
        var cntr = 0;
        var totAmtt = document.getElementsByName('totAmttHire');
        //var noOfTripss = document.getElementsByName('noOfTripssHire');
        var activeInd = document.getElementsByName('activeIndHire');
        for (var i = 0; i < totAmtt.length; i++) {
            if (activeInd[i].value == 'Y') {
                if (cntr == 0) {
                    temp = totAmtt[i].value;
                    // temp1 = noOfTripss[i].value;
                    //   alert(temp);
                } else {
                    temp = +temp + +totAmtt[i].value;
                    //  temp1 = +temp1 + +noOfTripss[i].value;
                    // alert(temp);
                }
                cntr++;


            }

        }
        var igstTax = document.getElementById("igst").value;
        var sgstTax = document.getElementById("sgst").value;
        var cgstTax = document.getElementById("cgst").value;
        var sgstHire = 0, cgstHire = 0, igstHire = 0;
        sgstHire = parseFloat((parseFloat(temp).toFixed(2) * sgstTax) / 100).toFixed(2);
        cgstHire = parseFloat((parseFloat(temp).toFixed(2) * cgstTax) / 100).toFixed(2);
        igstHire = parseFloat((parseFloat(temp).toFixed(2) * igstTax) / 100).toFixed(2);

        var billingState = document.getElementById("billingState").value;



        document.getElementById("totalAmtHire").value = parseFloat(temp).toFixed(2);
        document.getElementById("totalAmountHire").innerHTML = '';
        document.getElementById("totalAmountHire").innerHTML = parseFloat(temp).toFixed(2);
//        var tdsHire = 0;
//        var serviceTaxHire = 0;
//        tdsHire = document.getElementById("tdsHire").value;
//        serviceTaxHire = document.getElementById("serviceTaxHire").value;



        if (billingState == '37') {
            document.getElementById("sgstHire").value = sgstHire;
            document.getElementById("cgstHire").value = cgstHire;
            document.getElementById("sgstHireSpan").innerHTML = sgstHire;
            document.getElementById("cgstHireSpan").innerHTML = cgstHire;

            document.getElementById("totalInvoiceAmountHire").innerHTML = parseFloat(parseFloat(temp) + parseFloat(cgstHire) + parseFloat(sgstHire)).toFixed(2);
            document.getElementById("totalVendiorAmountHire").innerHTML = parseFloat(parseFloat(temp)).toFixed(2);
            document.getElementById("actualInvoiceAmountHire").value = parseFloat(parseFloat(temp) + parseFloat(cgstHire) + parseFloat(sgstHire)).toFixed(2);
        } else {
            document.getElementById("igstHire").value = igstHire;
//            alert(" document.getElementById:"+ document.getElementById("igstHire").value);
            document.getElementById("igstHireSpan").innerHTML = igstHire;
            document.getElementById("totalInvoiceAmountHire").innerHTML = parseFloat(parseFloat(temp) + parseFloat(igstHire)).toFixed(2);
            document.getElementById("totalVendiorAmountHire").innerHTML = parseFloat(parseFloat(temp)).toFixed(2);
            document.getElementById("actualInvoiceAmountHire").value = parseFloat(parseFloat(temp) + parseFloat(igstHire)).toFixed(2);

        }

        // document.getElementById("totalNoOfTripsHire").innerHTML = temp1;
    }


    function getContractDetails()
    {
        var vendorId = document.getElementById("vendorId").value;
        var vendorName = document.getElementById("vendorName").value;
        var startDate = document.getElementById("startDate").value;
        var endDate = document.getElementById("endDate").value;
        var contractTypeId = document.getElementById("contractTypeId").value;
        if (textValidation(document.vendor.vendorId, 'vendorName')) {
            return;
        }
        else if (textValidation(document.vendor.startDate, 'FromDate')) {
            return;
        }
        else if (textValidation(document.vendor.endDate, 'ToDate')) {
            return;
        }
        else if (textValidation(document.vendor.contractTypeId, 'ContractType')) {
            return;
        }


        document.vendor.action = "/throttle/getContractVendorPaymentDetails.do?vendorId=" + vendorId + "&contractTypeId=" + contractTypeId + "&startDate=" + startDate + "&endDate=" + endDate + "&vendorName=" + vendorName;
        document.vendor.submit();

    }
    function submitPage(Proceed) {
        var contractTypeId = document.getElementById("contractTypeId").value;
        if (contractTypeId == 2) {
            var billStatus = document.getElementsByName("billStatus");
            var totalAmount = document.getElementById("totalAmtHire").value;
        } else if (contractTypeId == 1) {
            var totalAmount = document.getElementById("totalAmtDedicate").value;
            var billStatus = document.getElementsByName("billStatusDedicate");
        }
        var count = 0;
        for (var i = 0; i < billStatus.length; i++) {
            if (billStatus[i].value == 0) {
                count++;
            }
        }
        var invoiceAmount = document.getElementById("invoiceAmount").value;
        if (parseFloat(invoiceAmount).toFixed(2) != parseFloat(totalAmount).toFixed(2)) {
            alert("Amount doesn't match");
        } else if (count > 0) {
            alert("Some Trips are not Ready to Bill");
        } else {
            document.vendor.action = "/throttle/updateVendorInvoiceTrips.do";
            document.vendor.submit();
        }
    }


    function setActiveIndHire(sno) {
        if (document.getElementById("selectedIndexHire" + sno).checked) {
            document.getElementById("activeIndHire" + sno).value = 'Y';
            document.getElementById("totAmttHire" + sno).value = document.getElementById("totAmtHire" + sno).value;
            //document.getElementById("noOfTripssHire" + sno).value = document.getElementById("noOfTripsHire" + sno).value;

        } else {
//                alert("hai");
            document.getElementById("activeIndHire" + sno).value = 'N';
            document.getElementById("totAmttHire" + sno).value = '';
        }
        setValueHire();
    }

    function setActiveIndDedicate(sno) {
        if (document.getElementById("selectedIndexDedicate" + sno).checked) {
            document.getElementById("activeIndDedicate" + sno).value = 'Y';
            document.getElementById("totAmttDedicate" + sno).value = document.getElementById("totAmtDedicate" + sno).value;
            //document.getElementById("noOfTripssDedicate" + sno).value = document.getElementById("noOfTripsDedicate" + sno).value;

        } else {
//                alert("hai");
            document.getElementById("activeIndDedicate" + sno).value = 'N';
            document.getElementById("totAmttDedicate" + sno).value = '';
        }
        setValueDedicate();
    }
    function setValueDedicate() {

        // document.getElementById("totalNoOfTripsDedicate").innerHTML = '';
        var temp = 0;
        var temp1 = 0;
        var cntr = 0;
        var totAmtt = document.getElementsByName('totAmttDedicate');
        //var noOfTripss = document.getElementsByName('noOfTripssDedicate');
        var activeInd = document.getElementsByName('activeIndDedicate');
        for (var i = 0; i < totAmtt.length; i++) {
            if (activeInd[i].value == 'Y') {
                if (cntr == 0) {
                    temp = totAmtt[i].value;
                    // temp1 = noOfTripss[i].value;
                    //   alert(temp);
                } else {
                    temp = +temp + +totAmtt[i].value;
                    //  temp1 = +temp1 + +noOfTripss[i].value;
                    // alert(temp);
                }
                cntr++;


            }

        }

        var igstTax = document.getElementById("igst").value;
        var sgstTax = document.getElementById("sgst").value;
        var cgstTax = document.getElementById("cgst").value;
        var sgstDedicated = 0, cgstDedicated = 0, igstDedicated = 0;
        sgstDedicated = parseFloat((parseFloat(temp).toFixed(2) * sgstTax) / 100).toFixed(2);
        cgstDedicated = parseFloat((parseFloat(temp).toFixed(2) * cgstTax) / 100).toFixed(2);
        igstDedicated = parseFloat((parseFloat(temp).toFixed(2) * igstTax) / 100).toFixed(2);

        var billingState = document.getElementById("billingState").value;
        document.getElementById("totalAmtDedicate").value = parseFloat(temp).toFixed(2);
        document.getElementById("totalAmountDedicate").innerHTML = '';
        document.getElementById("totalAmountDedicate").innerHTML = parseFloat(temp).toFixed(2);

        if (billingState == '9') {

            document.getElementById("sgstDedicated").value = sgstDedicated;
            document.getElementById("cgstDedicated").value = cgstDedicated;
            document.getElementById("sgstDedicatedSpan").innerHTML = sgstDedicated;
            document.getElementById("cgstDedicatedSpan").innerHTML = igstDedicated;
            document.getElementById("totalInvoiceAmountDedicate").innerHTML = parseFloat(parseFloat(temp) + parseFloat(sgstDedicated) + parseFloat(cgstDedicated)).toFixed(2);
            document.getElementById("totalVendiorAmountDedicate").innerHTML = parseFloat(parseFloat(temp)).toFixed(2);
            document.getElementById("actualInvoiceAmountDedicate").value = parseFloat(parseFloat(temp) + parseFloat(sgstDedicated) + parseFloat(cgstDedicated)).toFixed(2);
        } else {
            document.getElementById("igstDedicated").value = igstDedicated;
            document.getElementById("igstDedicatedSpan").innerHTML = igstDedicated;
            document.getElementById("totalInvoiceAmountDedicate").innerHTML = parseFloat(parseFloat(temp) + parseFloat(igstDedicated)).toFixed(2);
            document.getElementById("totalVendiorAmountDedicate").innerHTML = parseFloat(parseFloat(temp));
            document.getElementById("actualInvoiceAmountDedicate").value = parseFloat(parseFloat(temp) + parseFloat(igstDedicated)).toFixed(2);
        }



        // document.getElementById("totalNoOfTripsHire").innerHTML = temp1;
    }


    function loadTripDetailsPage(tripIds) {
        var url = '/throttle/viewInvoiceTripDetails.do?tripIds=' + tripIds;
        $('#slabRateListSet').html('');
        $.ajax({
            url: url,
            type: "get",
            success: function(data)
            {
                $('#slabRateListSet').html(data);
            }
        });
    }
</script>

<style>
    #index td {
        color:white;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i>Invoice Receipt Process</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active">Invoice Receipt Process</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%               
               
                

            %>
            <body >
                <%
                            String vendorName = (String) request.getAttribute("vendorName");
                            String vendorId = (String) request.getAttribute("vendorId");
                            String vendorType = (String) request.getAttribute("vendorType");
                            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                            Date date = new Date();
                            String startDate = "";
                            String endDate = "";
                            String accountEntryDate =dateFormat.format(date);
                            if (request.getAttribute("startDate") != null) {
                            startDate = (String) request.getAttribute("startDate");
                            } 
                            if (request.getAttribute("endDate") != null) {
                            endDate = (String) request.getAttribute("endDate");
                            } 
                %>
                <form name="vendor">
                    <%@ include file="/content/common/message.jsp" %>
                    <input name="accountEntryDate" id="accountEntryDate" value="<%=accountEntryDate%>" type="hidden" class="datepicker" >
                    <br>
                    <font color="green" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; ">

                        <div align="center" id="Status">&nbsp;&nbsp;
                        </div>
                    </font>
                    <div>
                        <table class="table table-info mb30 table-hover">
                            <thead>
                                <tr height="30" id="index">
                                    <th colspan="10" ><b>Vendor Payment Info</b></th>
                                </tr>
                            </thead>
                            <tr height="40">
                                <td>Vendor Name</td>
                                <td>
                                    <input type="hidden" name="billingState" id="billingState" value="<c:out value="${billingState}"/>" >
                                    <input type="hidden" name="igst" id="igst" value="<c:out value="${IGST}"/>" >
                                    <input type="hidden" name="cgst" id="cgst" value="<c:out value="${CGST}"/>" >
                                    <input type="hidden" name="sgst" id="sgst" value="<c:out value="${SGST}"/>" >
                                    <input type="hidden" name="vendorType" id="vendorType" value="<c:out value="${vendorType}"/>" >
                                    <input type="hidden" name="vendorId" id="vendorId" value="<c:out value="${vendorId}"/>" >
                                    <input type="hidden" name="vendorName" id="vendorName" value="<c:out value="${vendorName}"/>" >
                                    <input type="hidden" name="virCode" id="virCode" value="<c:out value="${virCode}"/>" >
                                    <c:out value="${vendorName}"/></td>
                                <td>Invoice No</td>
                                <td><input type="hidden" name="invoiceNo" id="invoiceNo" value="<c:out value="${invoiceNo}"/>">
                                    <c:out value="${invoiceNo}"/>
                                </td>
                                <td > Invoice Date</td>
                                <td><input type="hidden" name="invoiceDate" id="invoiceDate" value="<c:out value="${invoiceDate}"/>">
                                    <c:out value="${invoiceDate}"/>
                                </td>

                                <td >Invoice Amount</td>
                                <td><input type="hidden" name="invoiceAmount" id="invoiceAmount" value="<c:out value="${invoiceAmount}"/>">
                                    <c:out value="${invoiceAmount}"/>
                                </td>
                                <td >Contract Type</td>
                                <td><input type="hidden" name="contractTypeId" id="contractTypeId" value="<c:out value="${contractTypeId}"/>">
                                    <c:if test = "${contractTypeId == 2}" >Hire</c:if>
                                    <c:if test = "${contractTypeId == 1}" >Dedicate</c:if>
                                    </td>

                                </tr>

                            </table>
                        </div>
                        <br>
                        <br>
                    <c:if test = "${contractTypeId == 1}" >
                        <c:if test="${VIRdetailList != null}">
                            <c:forEach items="${VIRdetailList}" var="pd">
                                <c:set var="invoiceYear" value="${pd.invoiceYear}"/>
                                <c:set var="currentMonth" value="${pd.currentMonth}"/>
                                <c:set var="previousMonth" value="${pd.previousMonth}"/>
                                <c:set var="previousYear" value="${pd.previousYear}"/>
                            </c:forEach>
                        </c:if>
                        <c:if test = "${vendorInvoiceTripDetailsList != null}" >
                            <label style="color:green"><b> <c:out value="${previousMonth}"/>&nbsp;<c:out value="${previousYear}"/>&nbsp;-<c:out value="${currentMonth}"/>&nbsp;<c:out value="${invoiceYear}"/></b></label>
                            <br>
                            <div>
                                <table class="table table-info mb30 table-hover" border="1">
                                    <thead>
                                        <tr align="center" id="index" height="30">
                                            <th height="30">S.No</th>
                                            <th height="30">Vehicle Type</th>
                                            <th height="30">Vehicle No</th>
                                            <th height="30">Contract Category</th>
                                            <th height="30">No Of Trips</th>
                                            <th height="30">Total Run Km</th>
                                            <th height="30" colspan="5"><center style="color:black">Fixed Contract</center>
                                                <table><tr><th height="30">Allowed Km</th>
                                                        <th height="30">Extra Run Km</th>
                                                        <th height="30">Fixed Cost Per Month</th>
                                                        <th height="30">Rate/km for Extra km</th>
                                                        <th height="30">Extra run km cost</th></tr></table>
                                            </th>
                                            <th height="30" colspan="1"><center style="color:black">Actual Contract</center>
                                                <table><tr>
                                                        <th height="30">Rate / km</th>
                                                    </tr></table>
                                            </th>
                                            <th height="30">Amount</th>
                                            <th height="30">Bill Status</th>
                                            <th height="30" >Select</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <% int sno = 0;
                            int index = 1;%>
                                        <c:set var="totalAmtDedicate" value="${0}"/>
                                    <input type="hidden" name="totalAmtDedicate" id="totalAmtDedicate" value="0" />
                                    <c:forEach items="${vendorInvoiceTripDetailsList}" var="list"> 
                                        <c:set var="totalAmtDedicate" value="${totalAmtDedicate + list.expense}"/>
                                        <%

                                            String classText = "";
                                            int oddEven = index % 2;
                                            if (oddEven > 0) {
                                                classText = "text2";
                                            } else {
                                                classText = "text1";
                                            }
                                        %>

                                        <tr  width="208" height="40" > 
                                            <td height="20"><%=index%></td>
                                            <td height="20">
                                                <input type="text" name="virId" id="virId<%=index%>" value="<c:out value="${list.virId}"/>"/>
                                                <input type="hidden" name="tripIdDedicate" id="tripIdDedicate<%=index%>" value="<c:out value="${list.tripId}"/>"/>
                                                <input type="hidden" name="vehicleIdDedicate"  value="<c:out value="${list.vehicleId}"/>"/>
                                                <c:out value="${list.vehicleNo}"/>
                                            </td>
                                            <td height="20">
                                                <input type="hidden" name="vehicleTypeIdDedicate"  value="<c:out value="${list.vehicleTypeId}"/>"/>
                                                <c:out value="${list.vehicleTypeName}"/>
                                            </td>
                                            <td height="20" align="right">
                                                <input type="hidden" name="contractCategory" id="contractCategory<%=index%>" value="<c:out value="${list.contractCategory}"/>"/>
                                                <c:if test = "${list.contractCategory == 2}" >Actual</c:if>
                                                <c:if test = "${list.contractCategory == 1}" >Fixed</c:if>

                                                </td>
                                                <td height="20" align="right">
                                                    <input type="hidden" name="noOfTripsDedicate" id="noOfTripsDedicate<%=index%>" value="<c:out value="${list.noOfTrips}"/>"/>
                                                <a href=""  data-toggle="modal" data-target="#myModal"  onclick="loadTripDetailsPage('<c:out value='${list.tripId}' />');"><c:out value="${list.noOfTrips}"/></a>
                                            </td>
                                            <td height="20">
                                                <input type="hidden" name="totRunKM"  value="<c:out value="${list.totRunKM}"/>"/>
                                                <c:out value="${list.totRunKM}"/>
                                            </td>
                                            <td height="20" align="right">
                                                <input type="hidden" name="allowedKM"  value="<c:out value="${list.allowedKM}"/>"/>
                                                <c:out value="${list.allowedKM}"/>
                                            </td>
                                            <td height="20" align="right">
                                                <input type="hidden" name="totExtraRunKm" id="totExtraRunKm<%=index%>" value="<c:out value="${list.totExtraRunKm}"/>"/>
                                                <c:out value="${list.totExtraRunKm}"/>
                                            </td>
                                            <td height="20" align="right">
                                                <input type="hidden" name="fixedCost" id="fixedCost<%=index%>" value="<c:out value="${list.fixedCost}"/>"/>
                                                <c:out value="${list.fixedCost}"/>
                                            </td>
                                            <td height="20" align="right">
                                                <input type="hidden" name="ratePerExtraKm" id="ratePerExtraKm<%=index%>" value="<c:out value="${list.ratePerExtraKm}"/>"/>
                                                <c:out value="${list.ratePerExtraKm}"/>
                                            </td>
                                            <td height="20" align="right">
                                                <input type="hidden" name="totExtraKMRate" id="totExtraKMRate<%=index%>" value="<c:out value="${list.totExtraKMRate}"/>"/>
                                                <c:out value="${list.totExtraKMRate}"/>
                                            </td>
                                            <td height="20" align="right">
                                                <input type="hidden" name="actualRatePerKM" id="actualRatePerKM<%=index%>" value="<c:out value="${list.actualRatePerKM}"/>"/>
                                                <c:out value="${list.actualRatePerKM}"/>
                                            </td>
                                            <td height="20" align="right">
                                                <input type="hidden" name="totAmtDedicate" id="totAmtDedicate<%=index%>" value="<c:out value="${list.expense}"/>"/>
                                                <c:out value="${list.expense}"/>
                                            </td>
                                            <td height="20" align="left">
                                                <c:if test = "${list.statusName == 0}" ><label style="color:red">Not Ready to Bill</label></c:if>
                                                <c:if test = "${list.statusName == 1}" ><label style="color:green">Ready to Bill</label></c:if>

                                                </td>
                                                <td height="20" align="center">
                                                    <input type="checkbox" name="selectedIndexDedicate" id="selectedIndexDedicate<%=index%>" onclick="setActiveIndDedicate('<%=index%>');" checked/>
                                                <input type="hidden" name ="activeIndDedicate" id="activeIndDedicate<%=index%>" value="N"/>
                                                <input type="hidden" name="totAmttDedicate" id="totAmttDedicate<%=index%>" value=""/>

                                                <input type="hidden" name="billStatusDedicate" id="billStatusDedicate<%=index%>" value="<c:out value="${list.statusName}"/>"/>
                                            </td>
                                        </tr>
                                        <script>setActiveIndDedicate(<%=index%>);</script>
                                        <%
                                            index++;
                                        %>
                                    </c:forEach>
                                    <tr>
                                        <td colspan="13" align="right" ><span id="totalAmountDedicate" style="color:black">
                                                <fmt:formatNumber pattern="##0.00" value="${totalAmtDedicate}"/>
                                            </span> 
                                        </td>
                                        <td colspan="2" align="right">&emsp;
                                        </td>

                                    </tr>
                                    <%
                                 if("37".equalsIgnoreCase((String)request.getAttribute("billingState")) )
                                 {
                                    System.out.println(" am in if...!!!!:");
                                    %>
                                    <tr>
                                        <td colspan="12" align="right" style="color:black">
                                            <b>CGST</b>
                                        </td>
                                        <td align="right"><span id="cgstDedicatedSpan"></span>
                                            <b><input type="hidden" name="cgstDedicated" id="cgstDedicated" value="0" style="width:90px;color:black"  onKeyPress="return onKeyPressBlockCharacters(event);"/></b>
                                        </td>
                                        <td colspan="1" align="right">&emsp;
                                        </td>

                                    </tr>
                                    <tr>
                                        <td colspan="12" align="right" style="color:black">
                                            <b>SGST</b>
                                        </td>
                                        <td  align="right" > <span id="sgstDedicatedSpan"></span>
                                            <b><input type="hidden" name="sgstDedicated" id="sgstDedicated" value="0" style="width:90px;color:black"  onKeyPress="return onKeyPressBlockCharacters(event);" /></b>
                                        </td>
                                        <td colspan="1" align="right">&emsp;
                                        </td>

                                    </tr>
                                    <%} else if(!"37".equalsIgnoreCase((String)request.getAttribute("billingState")) )
                                { System.out.println(" am in else if...!!!!:");%>
                                    <tr>
                                        <td colspan="12" align="right" style="color:black">
                                            <b>IGST</b>
                                        </td>
                                        <td  align="right" >
                                            <b><input type="hidden" name="igstDedicated" id="igstDedicated" value="0" style="width:90px;color:black"  onKeyPress="return onKeyPressBlockCharacters(event);" /></b>
                                            <span id="igstDedicatedSpan"></span>
                                        </td>
                                        <td colspan="1" align="right">&emsp;
                                        </td>

                                    </tr>
                                    <%}%>
                                    <tr>
                                        <td colspan="12" align="right" style="color:black">
                                            <b>Vendor Amount</b>
                                        </td>
                                        <td  align="right" style="color:black">
                                            <span id="totalVendiorAmountDedicate">
                                                <fmt:formatNumber pattern="##0.00" value="${totalAmtDedicate}"/>
                                            </span> 

                                        </td>
                                        <td colspan="2" align="right">&emsp;
                                        </td>

                                    </tr>
                                    <tr>
                                        <td colspan="12" align="right" style="color:black">
                                            <b>Total Amount</b>
                                        </td>
                                        <td  align="right" style="color:black">
                                            <span id="totalInvoiceAmountDedicate">
                                                <fmt:formatNumber pattern="##0.00" value="${totalAmtDedicate}"/>
                                            </span> 
                                            <input type="hidden" name="actualInvoiceAmountDedicate" id="actualInvoiceAmountDedicate" value="0" />

                                        </td>
                                        <td colspan="2" align="right">&emsp;
                                        </td>

                                    </tr>
                                    <tr>
                                        <td colspan="12" align="right" style="color:black">
                                            <b>TDS percentage</b>
                                        </td>
                                        <td  align="right" style="color:black">
                                            <!--                                            <span id="totalInvoiceAmountDedicate">
                                            <fmt:formatNumber pattern="##0.00" value="${totalAmtDedicate}"/>
                                        </span> -->
                                            <input type="text" name="tdsDedicate" id="tdsDedicate" value="0" onchange="calculateDedicatedTDS(this.value);"/>

                                        </td>
                                        <td colspan="2" align="right">&emsp;
                                        </td>

                                    </tr>
                                    <tr>
                                        <td colspan="12" align="right" style="color:black">
                                            <b>TDS Amount</b>
                                        </td>
                                        <td  align="right" style="color:black">
                                            <span id="tdsAmtDedicated">

                                            </span> 
                                            <input type="hidden" name="tdsDedicatedAmount" id="tdsDedicatedAmount" value="0" />

                                        </td>
                                        <td colspan="1" align="right">&emsp;
                                        </td>

                                    </tr>
                                    <tr>
                                        <td colspan="12" align="right" style="color:black">
                                            <b>Total Amount to Pay</b>
                                        </td>
                                        <td  align="right" style="color:black">
                                            <span id="totalDedicatedInvoiceAmt">

                                            </span> 
                                            <input type="hidden" name="totalDedicatedInvAmount" id="totalDedicatedInvAmount" value="0" />

                                        </td>
                                        <td colspan="2" align="right">&emsp;
                                        </td>

                                    </tr>
                                    <tr>
                                        <td colspan="12" align="right"  style="color:black"> Remarks:</td>
                                        <td colspan="3"><textArea id="descriptionDedicate" name="descriptionDedicate"><c:out value="${description}"/></textArea></td>
                            </tr>
                        </tbody>
                    </table>
                    <input type="hidden" name="count" id="count" value="<%=sno%>" />
                </div><script>setValueDedicate();
                                function calculateDedicatedTDS(val) {
                                    var totalInvoiceAmt = document.getElementById("actualInvoiceAmountDedicate").value;
                                    var tdsAmt = 0;
                                    tdsAmt = ((parseFloat(totalInvoiceAmt) * val) / 100).toFixed(2);
                                    // alert("tdsAmt:"+tdsAmt);
                                    document.getElementById("totalDedicatedInvAmount").value = (totalInvoiceAmt - tdsAmt).toFixed(2);
                                    document.getElementById("tdsDedicatedAmount").value = tdsAmt;
                                    document.getElementById("totalDedicatedInvoiceAmt").innerHTML = (totalInvoiceAmt - tdsAmt).toFixed(2);
                                    document.getElementById("tdsAmtDedicated").innerHTML = tdsAmt;

                                }
                                                                                                    </script>
                        </c:if>
                    </c:if>
                    <c:if test = "${contractTypeId == 2}" >
                        <c:if test = "${vendorInvoiceTripDetailsList != null}" >
                <div>
                    <table class="table table-info mb30 table-hover" border="1" >
                        <thead>
                            <tr align="center" id="index" height="30">
                                <th height="30">S.No</th>
                                <th height="30">Vehicle No / Trip No</th>
                                <th height="30">Vehicle Type</th>
                                <th height="30">Route</th>
                                <th height="30">Amount</th>
                                <th height="30">Bill Status</th>
                            </tr>
                        </thead>
                        <tbody>
                                        <% int sno = 0;
                            int index = 1;%>
                                        <c:set var="totalAmtHire" value="${0}"/>
                                             <input type="hidden" name="totalAmtHire" id="totalAmtHire" value="0" />
                                    <c:forEach items="${vendorInvoiceTripDetailsList}" var="list"> 
                                        <c:set var="totalAmtHire" value="${totalAmtHire + list.invoiceAmount}"/>
                                        <%

                                            String classText = "";
                                            int oddEven = index % 2;
                                            if (oddEven > 0) {
                                                classText = "text2";
                                            } else {
                                                classText = "text1";
                                            }
                                        %>

                                <tr  width="208" height="40" > 
                                    <td height="20"><%=index%></td>
                                    
                                           <input type="hidden" name="tripIdHire" id="tripIdHire<%=index%>" value="<c:out value="${list.tripId}"/>"/>
                                           <input type="hidden" name="virId" id="virId<%=index%>" value="<c:out value="${list.virId}"/>"/>
                                    
                                    
                                    <td height="20">
                                                <c:out value="${list.vehicleNo}"/>
                                    </td>
                                    <td height="20">
                                                <c:out value="${list.vehicleTypeName}"/>
                                    </td>
                                 
                                    <td height="20">
                                                <c:out value="${list.routeIdFullTruck}"/>
                                    </td>
                                    <td height="20" align="right">
                                           <input type="hidden" name="totAmtHire" id="totAmtHire<%=index%>" value="<c:out value="${list.invoiceAmount}"/>"/>
                                                <c:out value="${list.invoiceAmount}"/>
                                    </td>
                                    
                                    <td height="20" align="center">
                                                <c:if test = "${list.statusName == 0}" ><label style="color:red">Not Ready to Bill</label></c:if>
                                                <c:if test = "${list.statusName == 1}" ><label style="color:green">Ready to Bill</label></c:if>
                                          <input type="checkbox" name="selectedIndexHire" id="selectedIndexHire<%=index%>" onclick="setActiveIndHire('<%=index%>');" checked/>
                                        <input type="hidden" name ="activeIndHire" id="activeIndHire<%=index%>" value="N"/>
                                        <input type="hidden" name="totAmttHire" id="totAmttHire<%=index%>" value=""/>
                                        <input type="hidden" name="billStatus" id="billStatus<%=index%>" value="<c:out value="${list.statusName}"/>"/>
                                        <!--<input type="hidden" name="noOfTripssHire" id="noOfTripssHire<%=index%>" value=""/>-->
                               
                                    </td>
                                </tr>
                                  <script>setActiveIndHire(<%=index%>);</script>
                                        <%
                                            index++;
                                        %>
                                    </c:forEach>
                            <tr>
                                <td colspan="6" align="right" ><span id="totalAmountHire" style="color:black">
                                                <fmt:formatNumber pattern="##0.00" value="${totalAmtHire}"/>
                                    </span> 
                                </td>
                                <td colspan="1" align="right">&emsp;
                                </td>
                                
                            </tr>
                                    <%
                                       if("37".equalsIgnoreCase((String)request.getAttribute("billingState")) )
                                        {
                                        System.out.println(" am in if...!!!!:");
                                    %>
                            <tr>
                                <td colspan="5" align="right" style="color:black">
                                        <b>CGST</b>
                                </td>
                                <td align="right"><span id="cgstHireSpan"></span>
                                    <b><input type="hidden" name="cgstHire" id="cgstHire" value="0" style="width:90px;color:black" onkeyup="setValueHire()" onKeyPress="return onKeyPressBlockCharacters(event);"/></b>
                                </td>
                                <td colspan="1" align="right">&emsp;
                                </td>
                                
                            </tr>
                            <tr>
                                <td colspan="5" align="right" style="color:black">
                                        <b>SGST</b>
                                </td>
                                <td  align="right" > <span id="sgstHireSpan"></span>
                                    <b><input type="hidden" name="sgstHire" id="sgstHire" value="0" style="width:90px;color:black" onkeyup="setValueHire()" onKeyPress="return onKeyPressBlockCharacters(event);" /></b>
                                </td>
                                <td colspan="1" align="right">&emsp;
                                </td>
                                
                            </tr>
                                    <%} 
                                       else if(!"37".equalsIgnoreCase((String)request.getAttribute("billingState")) )
                                       { System.out.println(" am in else if...!!!!:");%>
                            <tr>
                                <td colspan="5" align="right" style="color:black">
                                        <b>IGST</b>
                                </td>
                                <td  align="right" >
                                    <b><input type="hidden" name="igstHire" id="igstHire" value="0" style="width:90px;color:black"  onKeyPress="return onKeyPressBlockCharacters(event);" /></b>
                                    <span id="igstHireSpan"></span>
                                </td>
                                <td colspan="1" align="right">&emsp;
                                </td>

                            </tr>
                                    <%}%>
                            <tr>
                                <td colspan="5" align="right" style="color:black">
                                        <b>Vendor Amount</b>
                                </td>
                                <td  align="right" style="color:black">
                                        <span id="totalVendiorAmountHire">
                                                <fmt:formatNumber pattern="##0.00" value="${totalAmtHire}"/>
                                    </span> 
                                                  
                                </td>
                                <td colspan="1" align="right">&emsp;
                                </td>
                                
                            </tr>
                            <tr>
                                <td colspan="5" align="right" style="color:black">
                                        <b>Total Amount</b>
                                </td>
                                <td  align="right" style="color:black">
                                        <span id="totalInvoiceAmountHire">
                                                <fmt:formatNumber pattern="##0.00" value="${totalAmtHire}"/>
                                    </span> 
                                               <input type="hidden" name="actualInvoiceAmountHire" id="actualInvoiceAmountHire" value="0" />
                                                  
                                </td>
                                <td colspan="1" align="right">&emsp;
                                </td>
                                
                            </tr>
                            <tr style="display:none">
                                        <td colspan="5" align="right" style="color:black">
                                            <b>TDS percentage</b>
                                        </td>
                                        <td  align="right" style="color:black">
<!--                                            <span id="totalInvoiceAmountDedicate">
                                            <fmt:formatNumber pattern="##0.00" value="${totalAmtDedicate}"/>
                                            </span> -->
                                            <input type="text" name="tdsHire" id="tdsHire" value="0" onchange="calculateHireTDS(this.value);"/>

                                        </td>
                                        <td colspan="1" align="right">&emsp;
                                        </td>

                            </tr>
                            <tr style="display:none">
                                        <td colspan="5" align="right" style="color:black">
                                            <b>TDS Amount</b>
                                        </td>
                                        <td  align="right" style="color:black">
                                            <span id="tdsAmtHire">
                                               
                                            </span> 
                                            <input type="hidden" name="tdsHireAmount" id="tdsHireAmount" value="0" />

                                        </td>
                                        <td colspan="1" align="right">&emsp;
                                        </td>

                                    </tr>
                                <tr style="display:none">
                                <td colspan="5" align="right" style="color:black">
                                        <b>Total Amount to Pay</b>
                                </td>
                                <td  align="right" style="color:black">
                                        <span id="totalInvoiceAmt">
                                                
                                    </span> 
                                               <input type="text" name="totalHireInvAmount" id="totalHireInvAmount" value="0" />
                                                  
                                </td>
                                <td colspan="1" align="right">&emsp;
                                </td>
                                
                            </tr>
                             <tr>
                               <td colspan="5" align="right"  style="color:black"> Remarks:</td>
                                <td colspan="2"><textArea id="description" name="description"><c:out value="${description}"/></textArea></td>
                            </tr>
                        </tbody>
                    </table>
                    <input type="hidden" name="count" id="count" value="<%=sno%>" />
                </div> <script> setValueHire();</script>
                        </c:if>
                    </c:if>
           
             <center>
                            <input type="button"  class="btn btn-success" name="Proceed"  id="Proceed" onclick="submitPage(this.name);" value="Save">
                        </center>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            
<div class="modal fade" id="myModal" role="dialog" style="width: 100%;height: 100%">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" onclick="resetTheSlabDetails()">Close</button>
                            <h4 class="modal-title">Trip Details</h4>
                        </div>
                        <div class="modal-body" id="slabRateListSet" style="width: 100%;height: 100%">

                        </div>
                        <div class="modal-footer">


                        </div>
                    </div>
                </div>
            </div>
            <br>
            <script>
                        function resetTheSlabDetails() {
                            $('#slabRateListSet').html('');
                        }



                        function calculateHireTDS(val) {
                            var totalInvoiceAmt = document.getElementById("actualInvoiceAmountHire").value;
                            var tdsAmt = 0;
                            tdsAmt = ((parseFloat(totalInvoiceAmt) * val) / 100).toFixed(2);
                            // alert("tdsAmt:"+tdsAmt);
                            document.getElementById("totalHireInvAmount").value = (totalInvoiceAmt - tdsAmt).toFixed(2);
                            document.getElementById("tdsHireAmount").value = tdsAmt;
                            document.getElementById("totalInvoiceAmt").innerHTML = (totalInvoiceAmt - tdsAmt).toFixed(2);
                            document.getElementById("tdsAmtHire").innerHTML = tdsAmt;

                        }
                                                                            </script>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>



