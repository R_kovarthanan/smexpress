
<%--
    Document   : finBank
    Created on : Oct 19, 2012, 12:58:13 PM
    Author     : ASHOK
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.finance.business.FinanceTO" %>
        <%@ page import="java.util.*" %>

        <title> Manage Financial Year</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
    </head>
    <body>
        <form method="post" action="/throttle/addBankPage.do">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>

            <c:if test = "${financeYearList != null}" >
                <table align="center" width="800" border="0" cellspacing="0" cellpadding="0" class="border">

                    <tr height="30">
                        <td  align="left" class="contenthead" scope="col"><b>S.No</b></td>
                        <td  align="left" class="contenthead" scope="col"><b>Financial Year</b></td>
                        <td  align="left" class="contenthead" scope="col"><b>Description</b></td>
                        <td  align="left" class="contenthead" scope="col"><b>Status</b></td>
                        <td  align="left" class="contenthead" scope="col"><b>Created By</b></td>
                        <td  align="left" class="contenthead" scope="col"><b>Created On</b></td>
                    </tr>
                    <% int index = 0;%>
                    <c:forEach items="${financeYearList}" var="year">
                        <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text1";
                            } else {
                                classText = "text2";
                            }
                        %>
                        <tr height="30">
                            <td class="<%=classText %>"  align="left"> <%= index + 1 %> </td>
                            <td class="<%=classText %>" align="left"> <c:out value="${year.accountYear}" /></td>
                            <td class="<%=classText %>"  align="left"> <c:out value="${year.discription}"/> </td>
                            <td class="<%=classText %>"  align="left"> <c:out value="${year.statusName}"/> </td>
                            <td class="<%=classText %>"  align="left"> <c:out value="${year.createdBy}"/> </td>
                            <td class="<%=classText %>"  align="left"> <c:out value="${year.createdOn}"/> </td>

                        </tr>
                        <% index++;%>
                    </c:forEach>
                </c:if>

            </table>
            <br>
            <br>
        </form>
    </body>
</html>
