
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BUS</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>

         <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

            
    </head>
    <script language="javascript">

function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

        function submitPage(value)
        {
        if(value == 'search' || value == 'Prev' || value == 'Next' || value == 'GoTo' || value =='First' || value =='Last'){
                        if(value=='GoTo'){
                            var temp=document.jobCardCloseView.GoTo.value;       
                            document.jobCardCloseView.pageNo.value=temp;
                            document.jobCardCloseView.button.value=value;
                            document.jobCardCloseView.action = '/throttle/ViewJobCardsBilling.do';   
                            document.jobCardCloseView.submit();
                        }else if(value == "First"){
                        temp ="1";
                        document.jobCardCloseView.pageNo.value = temp; 
                        value='GoTo';
                    }else if(value == "Last"){
                    temp =document.jobCardCloseView.last.value;
                    document.jobCardCloseView.pageNo.value = temp; 
                    value='GoTo';
                }
                /*
                 if(document.jobCardCloseView.jobcardId.value != ""){
                 **/
                document.jobCardCloseView.button.value=value;
                document.jobCardCloseView.action = '/throttle/ViewJobCardsBilling.do';   
                document.jobCardCloseView.submit();
                /*
                 } else {
                 alert("Job Dard No is not Filled");
                }
                 **/
            }
        }
        
        function setValues(){
            if( '<%= request.getAttribute("regNo") %>' != 'null' ){
                document.jobCardCloseView.regNo.value = '<%= request.getAttribute("regNo") %>';
            }    
            if( '<%= request.getAttribute("jcId") %>' != 'null' ){
                document.jobCardCloseView.jobcardId.value = '<%= request.getAttribute("jcId") %>';
            }    
            if( '<%= request.getAttribute("compId") %>' != 'null' ){
                document.jobCardCloseView.serviceLocation.value = '<%= request.getAttribute("compId") %>';
            }    
            
         }          
function getVehicleNos(){
    //onkeypress='getList(sno,this.id)'         
    var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
}         
        
    </script>
    
        <c:if test="${jcList != null}">
        <body onLoad="setValues();getVehicleNos();">
        </c:if>
        <c:if test="${jcList == null && sessionCount == null}">
        <input type="hidden"  name="sessionCount" value="1"/>
        <body onLoad="submitPage('search');setValues();getVehicleNos();">
        </c:if>
        <form method="post" name="jobCardCloseView">
            <!-- copy there from end -->
            <div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
                <div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
                    <!-- pointer table -->
                    <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                        <tr>
                            <td >
                                <%@ include file="/content/common/path.jsp" %>
                    </td></tr></table>
                    <!-- pointer table -->
               
                </div>
            </div>
            <br>
            
            <!-- message table -->
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
                <tr>
                    <td >
                        <%@ include file="/content/common/message.jsp" %>
                    </td>
                </tr>
            </table>
            <!-- message table -->
<!-- copy there  end -->
<%          
            int index = 0;      
            int pag = (Integer)request.getAttribute("pageNo");
            index = ((pag -1) *10) +1 ;
%>  

    <table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">View Billed Job Cards </li>
    </ul>
    <div id="first">
    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
    <tr>
         <td align="left" height="30"><font color="red">*</font>Reg.No</td>
            <td height="30"><input type="text" class="textbox" id="regno" name="regNo" value="" /></td>
            <td align="left" height="30">Job Card No</td>
            <td><input type="text" class="textbox" name="jobcardId" value="" /> </td>
            <td>Service Point</td>
            <td> <select class="textbox" name="serviceLocation"  style="width:125px;">
                <option selected   value=0>---Select---</option>
                <c:if test = "${servicePoints != null}" >
                <c:forEach items="${servicePoints}" var="mfr">
                <option  value='<c:out value="${mfr.compId}" />'>
                <c:out value="${mfr.compName}" />
                </c:forEach >
                </c:if>
                </select>
            </td>
        <td><input type="button" class="button" name="search" value="search" onClick="submitPage(this.name);"  />   </td>
    </tr>
    </table>
    </div></div>
    </td>
    </tr>
    </table>

    
            <c:if test = "${jcList != null}" >
		<table align="center" width="80%" border="0" cellspacing="0" cellpadding="0" id="table"  class="border">
		
		<tr height="30">
		<th class="contenthead" align="left">SNo</th>
		<th class="contenthead" align="left">Job Card No</th>
		<th class="contenthead" align="left">Vehicle No</th>
		<th class="contenthead" align="left">Service Point</th>
		<th class="contenthead" align="left">Delivery Time</th>
		<th class="contenthead" align="left">Closed By</th>
		<th class="contenthead" align="left">Closed On</th>
		<th class="contenthead" align="left">Status</th>
		<th class="contenthead" align="left">&nbsp;</th>
		</tr>
                    <%
 
                    %>
                    
                    <c:forEach items="${jcList}" var="list"> 	
                        <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                %>
                <tr height="30">
                    <td class="<%=classText %>" height="30"><%=index %></td>
                    <td class="<%=classText %>" height="30"><c:out value="${list.jcMYFormatNo}"/></td>
                    <td class="<%=classText %>" height="30"><c:out value="${list.vehicleNo}"/></td>
                    <td class="<%=classText %>" height="30"><c:out value="${list.companyName}"/></td>
                    <td class="<%=classText %>" height="30"><c:out value="${list.jobCardPCD}"/></td>
                    <td class="<%=classText %>" height="30"><c:out value="${list.closedBy}"/></td>
                    <td class="<%=classText %>" height="30"><c:out value="${list.closedOn}"/></td>
                    <td class="<%=classText %>" height="30"><c:out value="${list.status}"/></td>
                    <td class="<%=classText %>" height="30">
                    <c:if test = "${list.serviceVendor == 0}" >
                    <!-- <a href='/throttle/closedJobCardBillDetails.do?jobCardId=<c:out value="${list.jobCardId}"/>&vehicleId=<c:out value="${list.vehicleId}"/>' target="frame">Bill</a></td>-->
                    <a href='/throttle/viewJobCardBillDetails.do?jobCardId=<c:out value="${list.jobCardId}"/>&vehicleId=<c:out value="${list.vehicleId}"/>' target="frame">View</a></td>
                    </c:if>
                    <c:if test = "${list.serviceVendor != 0}" >
                    <a href='/throttle/viewJobCardBillDetails.do?jobCardId=<c:out value="${list.jobCardId}"/>&vehicleId=<c:out value="${list.vehicleId}"/>' target="frame">View</a></td>
                    </c:if>
                </tr>
                <%
                index++;
                %>
                </c:forEach>
                </table>
            </c:if> 
            
            <br>
             <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
     <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" >5</option>
                    <option value="10">10</option>
                    <option value="20" selected="selected">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>


            <table align="center" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <%@ include file="/content/common/pagination.jsp"%>  
                    </td>                    
                </tr>
            </table>             
                <input type="hidden" name="reqfor" value="">
            <br>
        </form>
    </body>
</html>
