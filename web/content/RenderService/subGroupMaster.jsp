<%--
    Document   : standardChargesMaster
    Created on : Oct 29, 2013, 11:32:08 AM
    Author     : srinivasan
--%>

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
    function submitPage()
    {var errStr = "";
        var nameCheckStatus = $("#SubGroupNameStatus").text();
        if(document.getElementById("billSubGroupName").value == "") {
            errStr = "Please enter standard Charge Name.\n";
            alert(errStr);
            document.getElementById("billSubGroupName").focus();
        }
        else if(nameCheckStatus != "") {
            errStr ="StandardCharge Name Already Exists.\n";
            alert(errStr);
            document.getElementById("billSubGroupName").focus();
        }
        else if(document.getElementById("billSubGroupDesc").value == "") {
            errStr ="Please enter standard  Description.\n";
            alert(errStr);
            document.getElementById("billSubGroupDesc").focus();
        }
        else if(document.getElementById("billGroupId").value == "0") {
            errStr ="Please select group name.\n";
            alert(errStr);
            document.getElementById("billGroupId").focus();
        }
        if(errStr == "") {
            document.subGroup.action=" /throttle/saveSubGroup.do";
            document.subGroup.method="post";
            document.subGroup.submit();
        }



    }
    function setValues(sno,billSubGroupName,billSubGroupDesc,billGroupId,billSubGroupId,activeInd){
        var count = parseInt(document.getElementById("count").value);
        document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if(i != sno) {
                document.getElementById("edit"+i).checked = false;
            } else {
                document.getElementById("edit"+i).checked = true;
            }
        }
        document.getElementById("billSubGroupId").value = billSubGroupId;
        document.getElementById("billSubGroupName").value = billSubGroupName;
        document.getElementById("billSubGroupDesc").value = billSubGroupDesc;
        document.getElementById("billGroupId").value = billGroupId;
        document.getElementById("activeInd").value = activeInd;
    }






    function onKeyPressBlockNumbers(e)
	{

		var key = window.event ? e.keyCode : e.which;
		var keychar = String.fromCharCode(key);
		reg = /\d/;
		return !reg.test(keychar);

	}




var httpRequest;
    function checkstChargeName() {

        var billSubGroupName = document.getElementById('billSubGroupName').value;
        var nameCheckStatus = $("#productCategoryNameStatus").text();
            var url = '/throttle/checkStandardCharge.do?billSubGroupName=' + billSubGroupName ;
            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest();
            };
            httpRequest.send(null);
       
    }


    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.activeInd == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                     $("#nameStatus").show();
                    $("#SubGroupNameStatus").text('Please Check Standard Charge Name  :' + val+' is Already Exists');
                } else {
                    $("#nameStatus").hide();
                    $("#SubGroupNameStatus").text('');
                }
            } else {
                alert("Error loading page\n" + httpRequest.activeInd + ":" + httpRequest.statusText);
            }
        }
    }

</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body onload="document.subGroup.billSubGroupName.focus();">


        <form name="subGroup"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <table  border="1" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                <input type="hidden" name="billSubGroupId" id="billSubGroupId" value=""  />
                <tr>
                <table  border="0" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">

                <tr>
                    <td class="contenthead" colspan="4" >Bill Sub Group Master</td>
                </tr>
                <tr>
                    <td class="text1" colspan="4" align="center" style="display: none" id="nameStatus"><label id="SubGroupNameStatus" style="color: red"></label></td>
                </tr>
                <tr>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>SubGroup Name</td>
                    <td class="text1"><input type="text" name="billSubGroupName" id="billSubGroupName" class="textbox" maxlength="100" onkeypress="return onKeyPressBlockNumbers(event);"  onchange="checkstChargeName();" autocomplete="off"/></td>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Sub Group Description</td>
                    <td class="text1"><input type="text" name="billSubGroupDesc" id="billSubGroupDesc" class="textbox"
onkeyup="maxlength(this.form.billSubGroupDesc,500)"></td>
                </tr>
                <tr>
                    <td class="text2">&nbsp;&nbsp;<font color="red">*</font>Bill Group Name</td>
                    <td class="text2">
                        <select  align="center" class="textbox" name="billGroupId" id="billGroupId" >
                            <option value='0'>--select--</option>
                            <c:if test="${groupList != null}">
                                <c:forEach items="${groupList}" var="gl">
                                    <option value='<c:out value="${gl.billGroupId}"/>'><c:out value="${gl.billGroupName}"/></option>
                                </c:forEach>
                            </c:if>
                        </select>
                    </td>
                    <td class="text2">&nbsp;&nbsp;&nbsp;&nbsp;Status</td>
                    <td class="text2">
                        <select  align="center" class="textbox" name="activeInd" id="activeInd" >
                            <option value='Y'>Active</option>
                            <option value='N' id="inActive" style="display: none">In-Active</option>
                        </select>
                    </td>
                </tr>
                </table>
                </tr>
                <tr>
                    <td>
                        <br>
                        <center>
                            <input type="button" class="button" value="Save" name="Submit" onClick="submitPage()">
                        </center>
                    </td>
                </tr>
          </table>
            <br>


            <h2 align="center">Bill SubGroup List</h2>


            <table width="815" align="center" border="0" id="table" class="sortable">

                <thead>

                    <tr height="30">
                        <th><h3>S.No</h3></th>
                        <th><h3>Bill SubGroup</h3></th>
                        <th><h3>Description</h3></th>
                        <th><h3>Bill Group</h3></th>
                        <th><h3>Select</h3></th>
                    </tr>
                </thead>
                <tbody>


                    <% int sno = 0;%>
                    <c:if test = "${subGroupList != null}">
                        <c:forEach items="${subGroupList}" var="sgl">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td class="<%=className%>"  align="left"> <%= sno + 1%> </td>
                                <td class="<%=className%>"  align="left"> <c:out value="${sgl.billSubGroupName}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${sgl.billSubGroupDesc}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${sgl.billGroupName}"/></td>
                                <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues( <%= sno%>,'<c:out value="${sgl.billSubGroupName}" />','<c:out value="${sgl.billSubGroupDesc}" />','<c:out value="${sgl.billGroupId}" />','<c:out value="${sgl.billSubGroupId}" />','<c:out value="${sgl.activeInd}" />');" /></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>


            <input type="hidden" name="count" id="count" value="<%=sno%>" />

            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        </form>
    </body>
</html>