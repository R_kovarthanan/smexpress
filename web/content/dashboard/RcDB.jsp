<%--
    Document   : StoresDB
    Created on : Feb 3, 2012, 1:01:43 PM
    Author     : Administrator
--%>

<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.util.*,java.io.*,java.text.*" errorPage="" %>
<html lang="en">
<head>
        <script language="JavaScript" src="FusionCharts.js"></script>
        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="js/prettify.js"></script>
        <script type="text/javascript" src="js/json2.js"></script>
        <script type="text/javascript">
            $(document).ready ( function () {
                $("a.view-chart-data").click( function () {
                    var chartDATA = '';
                    if ($(this).children("span").html() == "View XML" ) {
                        chartDATA = FusionCharts('ChartId').getChartData('xml').replace(/\</gi, "&lt;").replace(/\>/gi, "&gt;");
                    } else if ($(this).children("span").html() == "View JSON") {
                        chartDATA = JSON.stringify( FusionCharts('ChartId').getChartData('json') ,null, 2);
                    }
                    $('pre.prettyprint').html( chartDATA );
                    $('.show-code-block').css('height', ($(document).height() - 56) ).show();
                    prettyPrint();
                })

                $('.show-code-close-btn a').click(function() {
                    $('.show-code-block').hide();
                });
            })
        </script>
	<meta charset="utf-8">
	<title></title>
	<link rel="stylesheet" href="css/jquery.ui.theme.css">
	<script src="js/jquery-1.4.4.js"></script>
	<script src="js/jquery.ui.core.js"></script>
	<script src="js/jquery.ui.widget.js"></script>
	<script src="js/jquery.ui.mouse.js"></script>
	<script src="js/jquery.ui.sortable.js"></script>
<style type="text/css">
.link {
	font: normal 12px Arial;
	text-transform:uppercase;
	padding-left:10px;
	font-weight:bold;
}

.link a  {
	color:#7f8ba5;
	text-decoration:none;
}

.link a:hover {
		color:#7f8ba5;
	text-decoration:underline;

}

</style>
	<style type="text/css">
            #expand {
                width:100%;
}
	.column { width: 250px; float: left; }
	.portlet { margin: 0 1em 1em 0; }
	.portlet-header { margin: 0.3em; padding-bottom: 4px; padding-left: 0.2em; cursor:move; }
	.portlet-header .ui-icon { float: right; }
	.portlet-content { padding: 0.4em; }
	.ui-sortable-placeholder { border: 1px dotted black; visibility: visible !important; height: 50px !important; }
	.ui-sortable-placeholder * { visibility: hidden; }
	</style>
	<script>
	$(function() {
		$( ".column" ).sortable({
			connectWith: ".column"
		});

		$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
			.find( ".portlet-header" )
				.addClass( "ui-widget-header ui-corner-all" )
				.prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
				.end()
			.find( ".portlet-content" );

		$( ".portlet-header .ui-icon" ).click(function() {
			$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
			$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
		});

		$( ".column" ).disableSelection();
	});
	</script>
</head>
<body>

<%
    String stockWorth = "";
    Connection conn = null;
    int count = 0;
    try{

        String fileName = "jdbc_url.properties";
        Properties dbProps = new Properties();
    //The forward slash "/" in front of in_filename will ensure that
    //no package names are prepended to the filename when the Classloader
    //search for the file in the classpath

    InputStream is = getClass().getResourceAsStream("/"+fileName);
    dbProps.load(is);//this may throw IOException
    String dbClassName = dbProps.getProperty("jdbc.driverClassName");

    String dbUrl = dbProps.getProperty("jdbc.url");
    String dbUserName = dbProps.getProperty("jdbc.username");
    String dbPassword = dbProps.getProperty("jdbc.password");


    String itemCode = request.getParameter("itemCode");


    Class.forName(dbClassName).newInstance();
    conn = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);


    double newItemWorth = 0, rcItemWorth = 0;
    String rcSaving = "SELECT  ip.price,sum((rcw.quantity * ip.price)) as worth, sum(total_amount) as rcworth"
            + " FROM papl_stock_worth_rc rcw, papl_item_price_master ip WHERE rcw.item_id = ip.item_id AND"
            + " ip.active_ind = 'Y' ";

    String rcStockBalance = "SELECT cm.name as name,sum(quantity) as tot FROM papl_rc_stock_balance sb, papl_company_master cm WHERE"
                     + " service_point_id = cm.comp_id AND cm.active_ind = 'Y' GROUP BY service_point_id";



    PreparedStatement pstmRcSaving = conn.prepareStatement(rcSaving);
    PreparedStatement pstmRcBalance = conn.prepareStatement(rcStockBalance);
    ResultSet res = pstmRcSaving.executeQuery();
    while(res.next()) {

        newItemWorth = res.getDouble("worth");
        rcItemWorth = res.getDouble("rcworth");
    }

    res = pstmRcBalance.executeQuery();
    while(res.next()) {        
        stockWorth = stockWorth+"<set value='"+res.getString("tot")+"' label='"+res.getString("name")+"' alpha='60'/>";
    }




// used dash board codes

/*
String stockWorth = "<chart caption='RC Stock Worth' bgColor='FFFFFF,CCCCCC' showPercentageValues='0' plotBorderColor='FFFFFF' numberPrefix='Rs' isSmartLineSlanted='0' showValues='1' showLabels='0' showLegend='1'>"+
                          "<set value='208700' label='Location 1'  color='3EA99F' alpha='60'/>"+
                          "<set value='30315' label='Location 2'  color='99CC00' alpha='60'/>"+
                          "<set value='40355' label='Location 3'  color='357EC7' alpha='60'/>"+
                          "</chart>";

*/
stockWorth = "<chart caption='RC Stock Worth' bgColor='FFFFFF,CCCCCC' showPercentageValues='0' plotBorderColor='FFFFFF' numberPrefix='Rs' isSmartLineSlanted='0' showValues='1' showLabels='0' showLegend='1'>"+stockWorth+"</chart>";

/*
//SELECT
((pm.price * rc.Quantity)) as amount, sum(total_amount) as rcamount FROM papl_item_price_master pm, papl_stock_worth_rc rc
WHERE pm.item_id = rc.item_id;

*/




String savings = "<chart yAxisName='Values' caption='RC Savings' numberPrefix='Rs.' useRoundEdges='1' bgColor='FFFFFF,FFFFFF' showBorder='0'> "+
	" <set label='New Price' value='"+newItemWorth+"' color='357EC7' />  "+
        "<set label='RC Price' value='"+rcItemWorth+"' color='F87431' /> "+
        "<set label='Savings' value='"+(newItemWorth - rcItemWorth)+"' color='3EA99F' /> "+
        "</chart> ";


%>

<div id="expand">
<div class="column" style="width:430px;">
	
	
        
        <div class="portlet" >
            <div class="portlet-header" style="width:405px;">&nbsp;&nbsp;RC Stock Worth</div>
                <div class="portlet-content">
                <table align="center"   cellspacing="0px" >
                <tr>
                    <td>
                        <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                            <jsp:param name="chartSWF" value="/throttle/swf/Pie2D.swf" />
                            <jsp:param name="strURL" value="" />
                            <jsp:param name="strXML" value="<%=stockWorth %>" />
                            <jsp:param name="chartId" value="productSales" />
                            <jsp:param name="chartWidth" value="400" />
                            <jsp:param name="chartHeight" value="250" />
                            <jsp:param name="debugMode" value="false" />
                            <jsp:param name="registerWithJS" value="false" />
                        </jsp:include>
                    </td>
                </tr>
                </table>
                </div>
	</div>
    

</div>
<div class="column" style="width:430px;">
	
    <div class="portlet" >
                    <div class="portlet-header" style="width:405px;">&nbsp;&nbsp;RC Savings</div>
                        <div class="portlet-content">
                        <table align="center"   cellspacing="0px" >
                        <tr>
                            <td>
                                <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                    <jsp:param name="chartSWF" value="/throttle/swf/Column2D.swf" />
                                    <jsp:param name="strURL" value="" />
                                    <jsp:param name="strXML" value="<%=savings %>" />
                                    <jsp:param name="chartId" value="productSales" />
                                    <jsp:param name="chartWidth" value="400" />
                                    <jsp:param name="chartHeight" value="250" />
                                    <jsp:param name="debugMode" value="false" />
                                    <jsp:param name="registerWithJS" value="false" />
                                </jsp:include>
                            </td>
                        </tr>
                        </table>
                        </div>
	</div>
    

</div>

   
    


</div><!-- End demo -->



<%

if(res != null) {
        res.close();
}

}catch (FileNotFoundException fne){
    System.out.println("File Not found "+fne.getMessage());
} catch (SQLException se){
    System.out.println("SQL Exception "+se.getMessage());
}finally{
    if(conn == null) {
        conn.close();
    }
}

%>


</body>
</html>
