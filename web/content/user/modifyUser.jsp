<%@ include file="/content/common/NewDesign/header.jsp" %>
   <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--<%@ page contentType="text/html" pageEncoding="UTF-8" language="java" import="java.sql.*" errorPage="" %>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import = "ets.domain.users.business.LoginTO" %>
<%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">


<title>Parveen</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">

<script>


</script>

<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->
<div class="pageheader">
      <h2><i class="fa fa-edit"></i><spring:message code="settings.label.Change Password" text="Change Password"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="header.label.YouAreHere" text="default text"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="header.label.Home" text="default text"/></a></li>
          <li><a href="general-forms.html"><spring:message code="header.label.Setting" text="default text"/></a></li>
          <li class=""><spring:message code="settings.label.Change Password" text="Change Password"/></li>
                  </ol>
      </div>
      </div>
<div class="contentpanel">
            <div class="panel panel-default">
             <div class="panel-body">
<body onLoad="document.modifyuser.OldPassword.focus()">
<form method="post" name="modifyuser">
<!-- copy there from end -->
<!-- pointer table -->
<!-- message table -->
<!-- copy there  end -->
 <!--<table class="table table-bordered">-->
<%@ include file="/content/common/message.jsp" %>
  <table  class="table table-info mb30 table-hover">
      <thead>
<tr>
    <th colspan="6" >Change Password</th>
</tr>
</thead>

<tr>
<input type="hidden" name="userId" value='<%= session.getAttribute("userId")%>'/> 
<td ><spring:message code="settings.label.Username" text="default text"/>   </td>
<td ><input type="hidden" name="userName" value="<%=session.getAttribute("userName") %>">  <%=session.getAttribute("userName") %></td>   

<td > <spring:message code="settings.label.OldPassword" text="default text"/> </td>
<td > <input type="password" name="OldPassword" style="width:240px;height:40px;"  class="form-control" /></td>
</tr>
<tr>
<td  ><spring:message code="settings.label.NewPassword" text="default text"/>   </td>
<td  > <input type="password" name="newPassword" style="width:240px;height:40px;"  class="form-control"/></td>

<td  ><spring:message code="settings.label.ConfirmPassword" text="default text"/>  </td>
<td  ><input type="password" name="confirm_pwd" style="width:240px;height:40px;"  class="form-control" /></td>
</tr>

</td>
</table>
<!--</table>-->

<center>

<input type="button" class="btn btn-success" value="<spring:message code="settings.label.SAVE" text="default text"/>" name="save" onClick="submitPage(this.name)" class="button"/>                       
<input type="hidden" name="reqfor" value="modifyUser"/>
<input type="hidden" name="userNams" value='<%=session.getAttribute("userName") %>'/>
<input type="hidden" name="oldPswd" value='<%= session.getAttribute("password")%>'/>
</center>


<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

<script language="javascript">

function submitPage(value){
if(value == 'save'){
    if(document.modifyuser.OldPassword.value == ""){
    alert("Old Password field is blank"); 
    document.modifyuser.OldPassword.focus();
    }else if(document.modifyuser.newPassword.value == "") {
    alert("New Password field is blank"); 
    document.modifyuser.newPassword.focus();  
    }else if(document.modifyuser.confirm_pwd.value == "") {
    alert("Confirm Password field is blank"); 
    document.modifyuser.confirm_pwd.focus();  
    }else if(document.modifyuser.newPassword.value ==document.modifyuser.oldPswd.value ) {
    alert("Old and New password are same"); 
    document.modifyuser.newPassword.focus(); 
    document.modifyuser.newPassword.value="";
    document.modifyuser.confirm_pwd.value="";
    }else if((document.modifyuser.oldPswd.value == document.modifyuser.OldPassword.value) && document.modifyuser.newPassword.value != "") {
    if((document.modifyuser.newPassword.value == document.modifyuser.confirm_pwd.value)) 
    {

    document.modifyuser.action ='/throttle/changePass.do';
    document.modifyuser.reqfor.value='changePass'
    document.modifyuser.submit();
    } 
    else
    {
    alert("New Password and Confirm Password Mismatch");
    document.modifyuser.newPassword.focus();
    }  
    }else
    {
    alert("Existing Password Incorrect. Try again");
    document.modifyuser.OldPassword.focus();
    document.modifyuser.OldPassword.value="";
    document.modifyuser.newPassword.value="";
    document.modifyuser.confirm_pwd.value="";
    }
}else {
    if(document.modifyuser.userName.value == ""){
    alert("Enter the User Name "); 
    document.modifyuser.userName.focus();
    } else if(value == 'recover') {
    document.modifyuser.action='/throttle/recoverPass.do';
    document.modifyuser.reqfor.value='recoverPass';
    document.modifyuser.submit();
    }    
    }
}
</script>
</body>
</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>