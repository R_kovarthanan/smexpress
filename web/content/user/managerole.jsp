<%-- 
Document   : managerole
Created on : Jul 19, 2008, 4:23:46 PM
Author     : vidya
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
   <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
   <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import = "ets.domain.users.business.LoginTO" %>
<%@ page import="java.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript">
function submitPage(value){
if(value=='add') {
document.managerole.action='/throttle/addrole.do';
document.managerole.reqfor.value='addrole';
document.managerole.submit();
}else if(value=='moddel') {
document.managerole.action='/throttle/alterrole.do';
document.managerole.reqfor.value='alterrole';
document.managerole.submit(); 
}
}
</script>



<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->
<div class="pageheader">
      <h2><i class="fa fa-edit"></i><spring:message code="settings.label.ManageRole" text="default text"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="header.label.YouAreHere" text="default text"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="header.label.Home" text="default text"/></a></li>
          <li><a href="general-forms.html"><spring:message code="header.label.Setting" text="default text"/></a></li>
          <li class=""><spring:message code="settings.label.ManageRole" text="default text"/> </li>
        
                  </ol>
      </div>
      </div>
<div class="contentpanel">
            <div class="panel panel-default">
             <div class="panel-body">
<body >

<form method="post" name="managerole">

<!-- copy there from end -->
<!--<div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
<div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
 pointer table 
<table class="table table-info mb30 table-hover" >
<tr>
<td >

</td></tr></table>
 pointer table 

</div>
</div>-->

<!-- message table -->
<table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
<tr>
<td >
<%@ include file="/content/common/message.jsp" %>
</td></tr></table>
<!-- message table -->
<!-- copy there  end -->

<!--<table class="table table-bordered">-->

  <table  class="table table-info mb30 table-hover" id="table">

    <thead>
<tr >
<th  ><spring:message code="settings.label.RoleId" text="default text"/></th>
<th  ><spring:message code="settings.label.Roletype" text="default text"/></th>
<th  ><spring:message code="settings.label.Roledescription" text="default text"/></th>
<th  ><spring:message code="settings.label.Status" text="default text"/></th>
<th  ><spring:message code="settings.label.CreatedBy" text="default text"/></th>
<th  ><spring:message code="settings.label.CreatedDate" text="default text"/></th>
</tr>
    </thead>

<%
int index = 0;
ArrayList roletypes = (ArrayList) request.getAttribute("roleRecord");
if(roletypes.size()!=0) {


%>
<c:if test = "${roleRecord != null}">
<c:forEach items="${roleRecord}" var="user">             
<%
String classText = "";
int oddEven = index % 2;
if (oddEven > 0) {
classText = "text2";
} else {
classText = "text1";
}
%>
<tr>

<td  >
<input type="hidden" name="roleId" value='<c:out value="${user.roleId}" />' /> <div align="left"><c:out value="${user.roleId}" /></div> </td>
<td  >
<input type="hidden" name="roletype" value='<c:out value="${user.roleId}" />' /><c:out value="${user.roleName}" /></td>
<td width="150"  >
<c:out value="${user.description}" /></td>
<td  >
<input type="hidden" name="actind" value='<c:out value="${user.status}"/>'><c:out value="${user.status}"/></td>
<td  >
<input type="hidden" name="createdby" value='<c:out value="${user.createdBy}"/>'><div align="left"><c:out value="${user.createdBy}"/></div> </td>
<td  >
<input type="hidden" name="createddate" value='<c:out value="${user.createdon}"/>'><div align="left"><c:out value="${user.createdon}"/></div> </td>


</tr>
<%
index++;

%>

</c:forEach >
</c:if> 

<%
}
%>
</td>
</table>
<center>
<input type="button" class="btn btn-success" name="add" value="<spring:message code="settings.label.Add" text="default text"/>" onClick="submitPage(this.name)" class="button">
<input type="button" class="btn btn-success" name="moddel" value="<spring:message code="settings.label.Alter" text="default text"/>" onClick="submitPage(this.name)" class="button">
<input type="hidden" value="" name="reqfor">
</center>
<br>
<script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 1);
                    </script>

<%@ include file="/content/common/NewDesign/commonParameters.jsp" %>
</form>
</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
