<%-- 
Document   : userroles
Created on : Jul 22, 2008, 3:35:04 PM
Author     : vidya
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
   <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import = "ets.domain.users.business.LoginTO" %>
<%@ page import="java.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"></head>
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script language="javascript">
/*window.onload = nameSearch;
function nameSearch() {
var oTextbox = new AutoSuggestControl(document.getElementById("userName"),
    new ListSuggestions("userName","/throttle/getUserNameList.do?"));
}
*/

window.onload = nameSearch;
function nameSearch(){
    //alert("value");
    var oTextbox = new AutoSuggestControl(document.getElementById("userName"),new ListSuggestions("userName","/throttle/getUserNameList.do?"));
} 

function submitPage(value)    {
if(value=='search'){
if(document.userroles.userName.value==""){
alert("Required field cannot be left blank");
document.userroles.userName.focus();
}else{
document.userroles.action='/throttle/searchUserRoles.do';
document.userroles.reqfor.value='searchUserRoles';
document.userroles.submit();
}
}else if(value=='save'){
var length = document.userroles.roleId.length;
var counter = document.userroles.assignedRole.length;


for(var j = 0; j < counter; j++){			
//alert(document.userroles.assignedRole.options[j].value);
document.userroles.assignedRole.options[j].selected = true;                   
}             
document.userroles.action='/throttle/saveUserRoles.do';
document.userroles.reqfor.value='saveUserRoles';
document.userroles.submit();
}
}

function copyAddress(roleId)
{
var avilableFunction = document.getElementById("availableFunc");
var index = 0;
var selectedLength = 0;
if(avilableFunction.length != 0){
for (var i=0; i<avilableFunction.options.length ; i++) {    
    if(avilableFunction.options[i].selected == true){
            selectedLength++;
    }    
  }
for (var j=0; j<selectedLength ; j++) {
        for (var i=0; i<avilableFunction.options.length ; i++) {
            if(avilableFunction.options[i].selected == true){
                
                    var optionCounter = document.userroles.assignedfunc.length;
                    document.userroles.assignedfunc.length = document.userroles.assignedfunc.length +1;
                    document.userroles.assignedfunc.options[optionCounter].text = avilableFunction.options[i].text ;
                    document.userroles.assignedfunc.options[optionCounter].value =  avilableFunction.options[i].value;
                    avilableFunction.options[i] = null;
                    break;
            }
          }
}  
}
else {
alert("Please Select any Value");
}
}



function copyAddress1(assRole)
{
var selectedValue = document.getElementById('assignedfunc');
var index = 0;
var selectedLength = 0;

if(selectedValue.length != 0){
for (var i=0; i<selectedValue.options.length ; i++) {    
    if(selectedValue.options[i].selected == true){
            selectedLength++;
    }    
  }  
for (var j=0; j<selectedLength ; j++) {
        for (var i=0; i<selectedValue.options.length ; i++) {
            if(selectedValue.options[i].selected == true){
                
                    var optionCounter = document.userroles.availableFunc.length;
                    document.userroles.availableFunc.length = document.userroles.availableFunc.length +1;
                    document.userroles.availableFunc.options[optionCounter].text = selectedValue.options[i].text ;
                    document.userroles.availableFunc.options[optionCounter].value =  selectedValue.options[i].value;
                    selectedValue.options[i] = null;
                   break;
            }
          }
}  
}
else {
alert("Please Select any Value");
}
}


function setValue(userName) {



if(userName != 'null'){
//    alert(userName);
document.userroles.userName.value="";
document.userroles.userName.value= userName; 
}
document.userroles.userName.focus();
}
window.onload = setValue;
</script>

<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->
<div class="pageheader">
      <h2><i class="fa fa-edit"></i><spring:message code="settings.label.ManageUserRole" text="default text"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="header.label.YouAreHere" text="default text"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="header.label.Home" text="default text"/></a></li>
          <li><a href="general-forms.html"><spring:message code="header.label.Setting" text="default text"/></a></li>
          <li class=""><spring:message code="settings.label.ManageUserRole" text="default text"/></li>


                  </ol>
      </div>
      </div>
<div class="contentpanel">
            <div class="panel panel-default">
             <div class="panel-body">

<body onLoad="nameSearch();setValue('<%= request.getAttribute("userName") %>');">
<form name="userroles" method="post">
<!-- copy there from end -->
<!-- pointer table -->



<!-- pointer table -->


<%@ include file="/content/common/message.jsp" %>


<!--<table class="table table-bordered">-->

  <table  class="table table-info mb30 table-hover">
   <thead>
    
    <th colspan="4"  ><spring:message code="settings.label.Manage User Role" text="User Role"/> </th>
</thead>

<tr>              
    <td height="30" ><spring:message code="settings.label.EntertheUserName" text="default text"/> </td><td><input type="text" class="form-control"  id="userName" style="width:260px;height:40px;"  name="userName" value="" onKeyPress="nameSearch();"></td>
<td  height="30" ><input type="button"  class="btn btn-success" name="search"   value="<spring:message code="settings.label.GO" text="default text"/>" onClick="submitPage(this.name)" ></td>
</tr>
</td>
</table>

<% int index = 0;
ArrayList empTotalDays = (ArrayList) request.getAttribute("Rolelist");
if(request.getAttribute("Rolelist") != null){

%> 

<table class="table table-info mb30 table-hover" >
    <thead>
        <tr>
            <th  valign="center" height="35" ><spring:message code="settings.label.AvailableRoles" text="default text"/> </th>
            <th  height="35"></th>
            <th  valign="center" height="35"><spring:message code="settings.label.AssignedRoles" text="default text"/> </th>
        </tr>
    </thead>
    <tr>
        <td valign="top">
            <table width="150" height="150" cellpadding="0" cellspacing="0" align="center" id="bg">
                <tr>  
                    <td width="150"><select style="width:200px;"  id="availableFunc" multiple size="10" name="roleId">
                            <c:if test = "${Rolelist != null}" >
                                <c:forEach items="${Rolelist}" var="roles"> 
                                    <option value='<c:out value="${roles.roleId}" />'><c:out value="${roles.roleName}" /></option>
                                </c:forEach>
                            </c:if>
                        </select></td>
                </tr>
            </table>                       

        </td>   

        <td height="100" valign="center">
            <table width="20" height="70" cellpadding="2" cellspacing="2" align="center" id="bg">
                <tr>
                    <td height="15" align="center" bgcolor="#F4F4F4" style=" border:1px; border-bottom-style:solid; "><input type="button" class="textbox" value=">" onClick="copyAddress(roleId.value)"></td></tr>
                <tr >
                    <td height="15" align="center" bgcolor="#F4F4F4" style=" border:1px; "><input type="button" class="textbox" value="<" onClick="copyAddress1(assignedRole.value)"></td></tr>


            </table>
        </td>
        <td valign="top">
            <table  width="150" height="150" cellpadding="0" cellspacing="0" align="center" id="bg" >
                <tr>
                    <td width="150"><select style="width:200px;" id="assignedfunc" multiple size="10" name="assignedRole" >
                            <c:if test = "${userRoles != null}" >
                                <c:forEach items="${userRoles}" var="role"> 
                                    <option value='<c:out value="${role.roleId}" />'><c:out value="${role.roleName}" /></option>
                                </c:forEach >
                            </c:if>
                        </select></td>
                    <td>
                </tr>
            </table>
        </td>

    </tr> 
    <!--<td></td>-->
</table>
<center>  
<input type="button" class="btn btn-success" name="save" value="<spring:message code="settings.label.SAVE" text="default text"/>" onClick="submitPage(this.name)" class="button">
</center>
<% } %>
<input type="hidden" value="" name="assignRoleId">
<input type="hidden" value="" name="reqfor">

<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
