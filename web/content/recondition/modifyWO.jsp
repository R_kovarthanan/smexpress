

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.purchase.business.PurchaseTO" %>
        <%@ page import="java.util.*" %>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%--<script language="javascript" src="/throttle/js/validate.js"></script>--%>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions1.js"></script>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>
        <title>Modify Purchase Order</title>




    </head>
    <body>


        <script>


            var httpRequest1;


            function getOthers(sno)
            {

                var temp=0;
                var mfrCode = document.getElementsByName("mfrCode");
                var itemCode = document.getElementsByName("itemCode");
                var itemName = document.getElementsByName("itemName");
                if(mfrCode[sno].value !=""){
                    temp = mfrCode[sno].value;
                }else if(itemCode[sno].value !=""){
                    temp = itemCode[sno].value;
                }else if(itemName[sno].value !=""){
                    temp = itemName[sno].value ;
                }

                var url='/throttle/getQuandity.do?mfrCode='+mfrCode[sno].value+'&itemCode='+itemCode[sno].value+'&itemName='+itemName[sno].value;
                if (window.ActiveXObject)
                {
                    httpRequest1 = new ActiveXObject("Microsoft.XMLHTTP");
                }
                else if (window.XMLHttpRequest)
                {
                    httpRequest1 = new XMLHttpRequest();
                }
                httpRequest1.open("POST", url, true);
                httpRequest1.onreadystatechange = function() {
                    go(sno,temp);
                } ;
                httpRequest1.send(null);
            }


            function go(sno,temp) {
                if (httpRequest1.readyState == 4) {
                    if (httpRequest1.status == 200) {
                        var response = httpRequest1.responseText;                        
                        var uom= document.getElementsByName("uom");
                        var itemId= document.getElementsByName("itemId");
                        var mfrCode= document.getElementsByName("mfrCode");
                        var itemCode= document.getElementsByName("itemCode");
                        var itemName= document.getElementsByName("itemName");
                        var qty= document.getElementsByName("qty");
                        var rcActiveInd="";
                        if(response!=""){
                            var details=response.split('~');
                            rcActiveInd=details[7];                            
                            if(rcActiveInd=='N')
                            {
                                alert("Please Enter Rc Items");
                                temp.value="";                                

                                                    }
                                                    else{
                                                        itemName[sno].value=details[5];
                                                        itemCode[sno].value=details[4];
                                                        mfrCode[sno].value=details[3];
                                                        uom[sno].value=details[2];
                                                        itemId[sno].value=details[0];
                                                        qty[sno].focus();
                                                    }
                                                }else
                                                {
                                                    alert("Invalid value");
                                                    temp.value="";
                                                    temp.focus();
                                                }
                                            }
                                        }
                                    }


                                    function validateItems()
                                    {
                                        var itemId = document.getElementsByName("itemId");
                                        var itemName = document.getElementsByName("itemName");
                                        var qty = document.getElementsByName("qty");
                                        for(var i=0; i <itemId.length; i++){
                                            if(itemId[i].value != '' ){
                                                if( qty[i].value == '' ){
                                                    alert("Please enter item quantity");
                                                    qty[i].focus();
                                                    return 'false';
                                                }
                                                if( isFloat(qty[i].value) ){
                                                    alert("Please enter valid item quantity");
                                                    qty[i].focus();
                                                    qty[i].select();
                                                    return 'false';
                                                }
                                            }
                                        }
                                        return 'true';
                                    }



                                    function validateItems1()
                                    {
                                        var itemId = document.getElementsByName("itemId");
                                        var deleteInd = document.getElementsByName("deleteInd");
                                        var cntr = 0;
                                        for(var i=0; i < deleteInd.length; i++){
                                            if(deleteInd[i].checked == 1){
                                                cntr++;
                                            }
                                        }
                                        if( parseInt(cntr) >= itemId.length  ){
                                            alert("All items cannot be deleted");
                                            return 'false';
                                        }
                                        return 'true';
                                    }




                                    function clearFieldValues(val)
                                    {
                                        getItemNames(val);
                                        val = val-1;
                                        var uom = document.getElementsByName("uom");
                                        var itemId = document.getElementsByName("itemId");
                                        var mfrCode = document.getElementsByName("mfrCode");
                                        var itemCode = document.getElementsByName("itemCode");
                                        var itemName = document.getElementsByName("itemName");
                                        var qty = document.getElementsByName("qty");
                                        var selectedInd = document.getElementsByName("selectedInd");
                                        if(selectedInd[val].checked == true){
                                            itemName[val].value="";
                                            itemCode[val].value="";
                                            mfrCode[val].value="";
                                            uom[val].value="";
                                            itemId[val].value="";
                                            qty[val].value="";
                                        }
                                    }


                                    var poItems = 0;
                                    var rowCount=2;
                                    var sno=0;
                                    var httpRequest;
                                    var httpReq;
                                    function addRow()
                                    { 

                                        sno++;
                                        var tab = document.getElementById("items");
                                        var snumber = parseInt(rowCount) + parseInt(poItems)-1;
                                        var newrow = tab.insertRow( parseInt(rowCount) + parseInt(poItems) ) ;
                                        var temp = sno-1;
                                        var cell = newrow.insertCell(0);
                                        var cell0 = "<td class='text1' height='30' > <input type='hidden'  name='itemId' > "+snumber+"</td>";
                                        cell.setAttribute("className","text2");
                                        cell.innerHTML = cell0;



                                        var cell = newrow.insertCell(1);
                                        var cell0 = "<td class='text1' height='30' ><input name='mfrCode' class='textbox'   onchange='getOthers("+snumber+")'  type='text'></td>";
                                        cell.setAttribute("className","text2");
                                        cell.innerHTML = cell0;

                                        cell = newrow.insertCell(2);
                                        var cell1 = "<td class='text1' height='30'><input name='itemCode' class='textbox'  onchange='getOthers("+snumber+")'  type='text'></td>";
                                        cell.setAttribute("className","text2");
                                        cell.innerHTML = cell1;

                                        cell = newrow.insertCell(3);
                                        var cell2 = "<td class='text1' height='30'><input id='itemNames"+snumber+"' name='itemName' value='' class='textbox'  onFocusOut='getOthers("+(snumber-1)+")'  type='text'></td>";
                                        cell.setAttribute("className","text2");
                                        cell.innerHTML = cell2;

                                        cell = newrow.insertCell(4);
                                        var cell3 = " <td class='text1' height='30'><input name='uom' size='5' readonly class='textbox'  type='text'></td>";
                                        cell.setAttribute("className","text2");
                                        cell.innerHTML = cell3;

                                        cell = newrow.insertCell(5);
                                        var cell4 = " <td class='text1' height='30'> <input name='qty' size='5' class='textbox'   type='text'></td>";
                                        cell.setAttribute("className","text2");
                                        cell.innerHTML = cell4;

                                        cell = newrow.insertCell(6);
                                        var cell1 = "<td class='text1' height='30' ><input type='checkbox' name='selectedInd' onClick='clearFieldValues("+snumber+");' > </td>";
                                        cell.setAttribute("className","text2");
                                        cell.innerHTML = cell1;

                                        cell = newrow.insertCell(7);
                                        var cell1 = "<td class='text1' height='30' ><input type='checkbox' name='deleteInd' value='"+snumber+"'   > </td>";
                                        cell.setAttribute("className","text2");
                                        cell.innerHTML = cell1;

                                        cell = newrow.insertCell(8);
                                        var cell1 = "<td class='text1' height='30' ><input type='hidden' name='rcItemIds' value='0'   > </td>";
                                        cell.setAttribute("className","text2");
                                        cell.innerHTML = cell1;


                                        rowCount++;
                                        getItemNames(snumber);
                                    }


                                    function setRowCount(val)
                                    {
                                        poItems = val;
                                        
                                    }

                                    function getItemNames(val){
                                        var itemTextBox = 'itemNames'+ val;
                                        var oTextbox = new AutoSuggestControl(document.getElementById(itemTextBox),new ListSuggestions(itemTextBox,"/throttle/handleItemSuggestions.do?"));
                                        //getVehicleDetails(document.getElementById("regno"));
                                    }

                                    function submitPag()
                                    {
                                        if(validateItems() == 'true' ){                                            
                                            if(confirm("Are you sure to modify Work order")){
                                                document.getElementById("buttonStyle").style.visibility="hidden" ;
                                                document.alterWO.action = "/throttle/handleModifyWO.do";
                                                document.alterWO.submit();
                                            }
                                        }
                                    }

                                    function submitPag1()
                                    {
                                        if(validateItems1() == 'true' ){
                                            if(confirm("Are you sure to delete work order items")){
                                                document.getElementById("buttonStyle").style.visibility="hidden" ;
                                                document.alterWO.action = "/throttle/handleDeleteWoItems.do";
                                                document.alterWO.submit();
                                            }
                                        }
                                    }



        </script>

        <form name="alterWO"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <!-- pointer table -->
            <!-- message table -->
            <%@ include file="/content/common/message.jsp" %>
            <% int cntr = 0;%>
            <c:if test="${woItems != null}" >
                <c:forEach items ="${woItems}" var="wo" >
                    <%if (cntr == 0) {%>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="650" class="border" >

                        <tr>
                            <td class="contentsub" height="30">WO No.</td>
                            <td class="contentsub" height="30">Vendor Name</td>
                            <td class="contentsub" height="30">WO Date</td>
                            <td class="contentsub" height="30">Remarks</td>
                        </tr>
                        <tr>
                            <c:set var="mrsId" value="${wo.mrsId}"/>
                            <td class="text2" height="30"><input type="hidden" name="rcWorkId" value='<c:out value="${wo.rcWorkId}" />'>

                                <input type="hidden" name="mrsId" value='<c:out value="${wo.mrsId}" />'><c:out value="${wo.rcWorkId}" /> </td>
                            <td class="text2" height="30">
                                <select name="vendorId" class="textbox" >
                                    <c:if test = "${vendorList != null}" >
                                        <c:forEach items="${vendorList}" var="vendor">
                                            <c:if test="${wo.vendorId == vendor.vendorId}" >
                                                <option selected value="<c:out value="${vendor.vendorId}"/>"><c:out value="${vendor.vendorName}"/></option>
                                            </c:if>
                                            <c:if test="${wo.vendorId != vendor.vendorId}" >
                                                <option value="<c:out value="${vendor.vendorId}"/>"><c:out value="${vendor.vendorName}"/></option>
                                            </c:if>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                            <td class="text2" height="30"> <input name="createdDate" readonly  class="textbox"  type="text" value="<c:out value="${wo.createdDate}" />" size="18">
                                <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.alterWO.createdDate,'dd-mm-yyyy',this)"/>  </td>
                            <td class="text2" height="30" >
                                <textarea name="remarks" class="textbox" ><c:out value="${wo.remarks}" /></textarea>
                            </td>
                        </tr>

                    </table>
                    <% cntr++;
                                }%>
                </c:forEach>
            </c:if>
            <br>
            <br>

            <table id="items" align="center" border="0" cellpadding="0" cellspacing="0" width="650" class="border" >
                <tr>
                    <td colspan="8" align="center" class="text2" height="30"><strong>Material Details</strong></td>
                </tr>
                <tr>
                    <td class="contentsub" height="30">SNo</td>
                    <td class="contentsub" height="30">MFR Item code</td>
                    <td class="contentsub" height="30">Item Code</td>
                    <td class="contentsub" height="30">Item Name</td>
                    <td class="contentsub" height="30">UOM</td>
                    <td class="contentsub" height="30">Quantity</td>
                    <td class="contentsub" height="30">Clear</td>
                    <td class="contentsub" height="30">Delete</td>

                </tr>

                <% int index = 0;%>
                <c:if test="${woItems != null}" >
                    <c:forEach items ="${woItems}" var="wo" >

                        <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text1";
                                    } else {
                                        classText = "text2";
                                    }
                        %>

                        <c:set var="rcItemId" value="${wo.rcItemId}" />
                        <c:set var="rcQueueId" value="${wo.rcQueueId}" />
                        <tr>
                        <input type="hidden" name="itemId" value="<c:out value="${wo.itemId}" />" >
                            <td class="<%= classText%>"  height="30"> <%= index + 1%> </td>
                            <td class="<%= classText%>"  height="30"> <input type="text" readonly class="textbox" name="mfrCode" value='<c:out value="${wo.mfrCode}" />' > </td>
                            <td class="<%= classText%>"  height="30"> <input type="text" readonly class="textbox"  name="itemCode" value='<c:out value="${wo.paplCode}" />' > </td>
                            <td class="<%= classText%>"  height="30"> <input type="text" readonly  class="textbox" name="itemName" value='<c:out value="${wo.itemName}" />'> </td>
                            <td class="<%= classText%>"  height="30"> <input name='uom' size='5' readonly class='textbox'  type='text' value='<c:out value="${wo.uomName}" />' >  </td>
                            <td class="<%= classText%>"  height="30"> <input type="text"  class="textbox" name="qty" size="5" readonly value='1' > </td>
                            <td class="<%= classText%>"  height="30"> <input type="checkbox"  class="textbox" name="selectedInd" value="<%= index%>"> </td>
                            <td class="<%= classText%>"  height="30"> <input type="checkbox"  class="textbox" name="deleteInd" value="<%= index%>"> </td>                            
                        <input type="hidden" name="rcQueueIds" value="<c:out value="${wo.rcQueueId}" />" >
                        <input type="hidden" name="rcItemIds" value="<c:out value="${wo.rcItemId}" />" >
                        </tr>

                        <% index++;%>
                    </c:forEach>
                </c:if>
            </table>



            <div id="buttonStyle" style="visibility:visible;" align="center" >
                <c:if test="${mrsId==0}">
                    <input type="button" class="button" name="addrw" value="Add Row" onClick="setRowCount('<%= index%>');addRow();" >
                </c:if>
                <input type="button" class="button" name="Alter" value="Save" onClick="submitPag();" >
                <input type="button" class="button" name="Delete" value="Delete" onClick="submitPag1();" >
            </div>
            <br>
            <br>
            <br>


        </form>
    </body>
</html>
