<%-- 
    Document   : TyresPurchaseReport
    Created on : Mar 27, 2012, 12:22:23 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
       
       
       


    </head>
    <script language="javascript">
function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}



    </script>
    <body>
        <form name="purchaseReport" method="post">


<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
<tr>


<td height="30" align="left" style="padding-right:5px; "><span class="pathText" >Tyres >>  Purchase Report</span> </td>


<Td width="75%">
<table id="imgTable" width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="119">&nbsp;</td>
<Td width="70" align="center"><img src="/throttle/images/Add1.png" align="middle" border="0" alt="Add"></Td>
<Td width="70" align="center"><img src="/throttle/images/edit1.png" align="middle" border="0" alt="Edit"></Td>
<Td width="70" align="center"><img src="/throttle/images/Search1.png" align="middle" border="0" alt="Search"></Td>
<Td width="70" align="center"><img src="/throttle/images/Export1.png" align="middle" border="0" alt="Export"></Td>
<Td width="70" align="center"><img src="/throttle/images/Import1.png" align="middle" border="0" alt="Import"></Td>
<Td width="70" align="center"><img src="/throttle/images/Print1.png" align="middle" border="0" alt="Print"></Td>
<td width="329">&nbsp;</td>
</tr>
<tr>
<td width="104">&nbsp;</td>
<Td width="70" align="center" style="font:Arial; font-size:12px; color:#999999; " >Add</Td>
<Td width="70" align="center" style="font:Arial; font-size:12px; color:#999999; " >Edit</Td>
<Td width="70" align="center" style="font:Arial; font-size:12px; color:#999999; " >Search</Td>
<Td width="70" align="center" style="font:Arial; font-size:12px; color:#999999; " >Export</Td>
<Td width="70" align="center" style="font:Arial; font-size:12px; color:#999999; ">Import</Td>
<Td width="70" align="center" style="font:Arial; font-size:12px; color:#999999; ">Print</Td>
<td width="314" >&nbsp;

</td>
</tr>
</table>
</Td>
</tr>
</table>


<table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="/throttle/images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="/throttle/images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Tyres Purchase Report</li>
    </ul>
    <div id="first">
    <table width="900" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
    <tr>
                        <td height="30"><font color="red">*</font>Location</td>
            <td height="30"><select  class="textbox" name="companyId" style="width:125px">

                                <option selected value="1022">Red Hills SP</option>

                </select></td>

            <td height="30">&nbsp;&nbsp;MFR</td>
            <td height="30"><select name="mfrId"  class="textbox">
                    <option value="0">-select-</option>                            
                            <option value="1022">Apollo</option>
                            <option value="1028">Bridgestone</option>
                            <option value="1001">MRF</option>

                </select></td>
            <td height="30">&nbsp;&nbsp;Model</td>
            <td height="30"><select name="modelId" class="textbox" style="width:125px">
                    <option value="0">-select-</option>
                            <option value="1025">ALL MODEL</option>
                            <option value="1026">Mark I</option>
                            <option value="1024">VOLVO</option>
                            <option value="1021">ALL MODEL</option>
                            <option value="1082">MARK</option>

                </select></td>

        </tr>
        <tr>
            <td height="30">Purchase Type</td>
            <td height="30"><select name="purType" class="textbox" style="width:130px">
                    <option value="0">-select-</option>
                    <option value="N">Direct</option>
                    <option value="Y">Vendor</option>
                </select></td>
            <td width="148">&nbsp;&nbsp;Vendor</td>
            <td width="148"><select name="vendorId" class="textbox" style="width:125px">
                    <option value="0">-select-</option>
                            <option value="1104">A.L.TRADER</option>
                            <option value="1357">BAGHYAA ENTERPRISES</option>
                </select></td>
            <td width="162" height="30">&nbsp;&nbsp;PO/ LPO No</td>
            <td width="151" height="30" class="text1"> <input name="poId" type="text" class="textbox"  size="20"></td>
        </tr>
        <tr>
            <td width="114" height="30">&nbsp;&nbsp;Item Name</td>
            <td width="172" height="30"><input name="itemName" id="itemName" type="text"  class="textbox" value=""></td>
            <td width="111" height="30">&nbsp;&nbsp;Mfr Item Code</td>
            <td width="151" height="30"><input name="mfrCode" type="text" class="textbox" value="" size="20"></td>
            <td>&nbsp;&nbsp;PAPL Code</td>
            <td height="30"><input name="paplCode" type="text" class="textbox" value="" size="20"></td>
        </tr>
        <tr>
        
            <td width="80" height="30"><font color="red">*</font>From Date</td>
            <td width="182"><input name="fromDate" type="text" class="textbox" value="" size="20">
                <span><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.purchaseReport.fromDate,'dd-mm-yyyy',this)"/></span></td>
            <td width="148"><font color="red">*</font>To Date</td>
            <td width="172" height="30"><input name="toDate" type="text" class="textbox" value="" size="20">
                <span><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.purchaseReport.toDate,'dd-mm-yyyy',this)"/></span></td>

                <td height="30">&nbsp;</td>
            <td height="30"><input type="button"  value="Fetch Data" class="button" name="Fetch Data" onClick="submitPage()">
                <input type="hidden" value="" name="reqfor"></td>
        </tr>


        <tr><td colspan="6" align="center"> 
                </td></tr>
    </table>
    </div></div>
    </td>
    </tr>
    </table>
            <br>

                <table width="1266" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
                    <tr class="contenthead">
                        <td width="71" class="contentsub">Date</td>
                        <td width="102" height="30" class="contentsub">Manufacturer</td>
                        <td width="82" class="contentsub">Model</td>
                        <td width="104" height="30" class="contentsub">Mfr Item Code </td>
                        <td width="82" class="contentsub">PAPL Code </td>
                        <td width="142" height="30" class="contentsub">Item Name</td>
                        <td width="78" height="30" class="contentsub">Category</td>
                        <td width="71" height="30" class="contentsub">Vendor</td>
                        <td width="71" height="30" class="contentsub">Purchase Type</td>
                        <td width="76" height="30" class="contentsub">PO / LPO No </td>
                    <blockquote>&nbsp;</blockquote>
                    <td width="80" height="30" class="contentsub">Accepted Qty</td>
                    <td width="80" height="30" class="contentsub">Unused Qty</td>
                    <td width="80" height="30" class="contentsub">Unit Price</td>
                    <td width="99" height="30" class="contentsub">Purchase Value (INR) </td>
                    </tr>


                    <tr>
                        <td class="text1">09-05-2011</td>
                        <td class="text1" height="30">Apollo</td>
                        <td class="text1">ALL MODEL</td>
                        <td class="text1" height="30"></td>
                        <td class="text1">28011</td>
                        <td class="text1" height="30">APOLLO TYRE 10X20</td>
                        <td class="text1" height="30">TYRES</td>
                        <td class="text1" height="30">INTERNATIONAL TYRE SERVICE </td>
                        <td class="text1" height="30">Vendor Purchase</td>
                        <td class="text1" height="30">1003</td>
                        <td class="text1" height="30">2.000</td>
                        <td class="text1" height="30">0.000</td>
                        <td class="text1" height="30">12639.11</td>
                        <td class="text1" height="30">28438.00</td>
                    </tr>


                        <tr>
                            <td class="text2">09-05-2011</td>
                            <td class="text2" height="30">Apollo</td>
                            <td class="text2">ALL MODEL</td>
                            <td class="text2" height="30"></td>
                            <td class="text2">28011</td>
                            <td class="text2" height="30">APOLLO TYRE 10X20</td>
                            <td class="text2" height="30">TYRES</td>
                            <td class="text2" height="30">INTERNATIONAL TYRE SERVICE </td>
                            <td class="text2" height="30">Vendor Purchase</td>
                            <td class="text2" height="30">1000</td>
                            <td class="text2" height="30">2.000</td>
                            <td class="text2" height="30">0.000</td>
                            <td class="text2" height="30">12639.11</td>
                            <td class="text2" height="30">28438.00</td>
                        </tr>

                        <tr>
                            <td class="text1">09-05-2011</td>
                            <td class="text1" height="30">Apollo</td>
                            <td class="text1">ALL MODEL</td>
                            <td class="text1" height="30"></td>
                            <td class="text1">28011</td>
                            <td class="text1" height="30">APOLLO TYRE 10X20</td>
                            <td class="text1" height="30">TYRES</td>
                            <td class="text1" height="30">INTERNATIONAL TYRE SERVICE </td>
                            <td class="text1" height="30">Vendor Purchase</td>
                            <td class="text1" height="30">1035</td>
                            <td class="text1" height="30">2.000</td>
                            <td class="text1" height="30">0.000</td>
                            <td class="text1" height="30">12506.67</td>
                            <td class="text1" height="30">28140.00</td>

                        </tr>




                        <tr>
                            <td class="text2">09-05-2011</td>
                            <td class="text2" height="30">Apollo</td>
                            <td class="text2">ALL MODEL</td>
                            <td class="text2" height="30"></td>
                            <td class="text2">28014</td>
                            <td class="text2" height="30">APOLLO TYRE 8.25X20</td>
                            <td class="text2" height="30">TYRES</td>
                            <td class="text2" height="30">INTERNATIONAL TYRE SERVICE </td>
                            <td class="text2" height="30">Vendor Purchase</td>
                            <td class="text2" height="30">1085</td>
                            <td class="text2" height="30">4.000</td>
                            <td class="text2" height="30">0.000</td>
                            <td class="text2" height="30">10515.56</td>
                            <td class="text2" height="30">47320.04</td>
                        </tr>


                        <tr>
                            <td class="text1">09-05-2011</td>
                            <td class="text1" height="30">Apollo</td>
                            <td class="text1">ALL MODEL</td>
                            <td class="text1" height="30"></td>
                            <td class="text1">28014</td>
                            <td class="text1" height="30">APOLLO TYRE 8.25X20</td>
                            <td class="text1" height="30">TYRES</td>
                            <td class="text1" height="30">INTERNATIONAL TYRE SERVICE </td>
                            <td class="text1" height="30">Vendor Purchase</td>
                            <td class="text1" height="30">1034</td>
                            <td class="text1" height="30">2.000</td>
                            <td class="text1" height="30">0.000</td>
                            <td class="text1" height="30">9582.22</td>
                            <td class="text1" height="30">21560.00</td>
                        </tr>


                        <tr>
                            <td class="text2">09-05-2011</td>
                            <td class="text2" height="30">BRIDGESTONE</td>
                            <td class="text2">ALL MODEL</td>
                            <td class="text2" height="30"></td>
                            <td class="text2">28026</td>
                            <td class="text2" height="30">BRIDGESTONE TYRE TUBELESS 295/80R22.5</td>
                            <td class="text2" height="30">TYRES</td>
                            <td class="text2" height="30">INTERNATIONAL TYRE SERVICE </td>
                            <td class="text2" height="30">Vendor Purchase</td>
                            <td class="text2" height="30">1086</td>
                            <td class="text2" height="30">2.000</td>
                            <td class="text2" height="30">0.000</td>
                            <td class="text2" height="30">22222.22</td>
                            <td class="text2" height="30">50000.00</td>
                        </tr>                    
                </table>

<%

String purchase = "<chart caption='Tyres Purchase Details' bgColor='FFFFFF,CCCCCC' showPercentageValues='0' plotBorderColor='FFFFFF' numberPrefix='' isSmartLineSlanted='0' showValues='1' showLabels='0' showLegend='1'>"+
                          "<set value='158896.04' label='Vendor'  color='99CC00' alpha='60'/>"+
                          "<set value='0' label='Direct'  color='F535AA' alpha='60'/>"+
                          "<set value='0' label='Unused'  color='357EC7' alpha='60'/>"+
                          "</chart>";
String purchaseMakewise = "<chart caption='Tyres Purchase Makewise' bgColor='FFFFFF,CCCCCC' showPercentageValues='0' plotBorderColor='FFFFFF' numberPrefix='' isSmartLineSlanted='0' showValues='1' showLabels='0' showLegend='1'>"+
                          "<set value='5' label='Apollo'  color='99CC00' alpha='60'/>"+
                          "<set value='1' label='Bridgestone'  color='F535AA' alpha='60'/>"+
                          "<set value='0' label='JK Tyres'  color='357EC7' alpha='60'/>"+
                          "<set value='0' label='Mrf'  color='357EC7' alpha='60'/>"+
                          "<set value='0' label='Michelim'  color='357EC7' alpha='60'/>"+
                          "</chart>";
%>


            <table class="table2" width="100%">
                <tr>
                    <td valign="top">
                        <table width="100%">
                            <tr><td class="contentsub" colspan="2">Summary</td></tr>
                            <tr>
                                <td><b>Vendor Purchase Amount</b></td><td><b>Rs: 158896.04</b></td>
                            </tr>
                            <tr>
                                <td><b>Direct Purchase Amount</b></td><td><b>Rs: 0.00</b></td>
                            </tr>
                            <tr>
                                <td><b>Unused Stock Value</b></td><td><b>Rs: 0.00</b></td>
                            </tr>
                        </table>
                    </td>
                    
                    <td>
                        <table align="center"   cellspacing="0px" >
                        <tr>
                            <td>
                                <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                    <jsp:param name="chartSWF" value="/throttle/swf/Pie2D.swf" />
                                    <jsp:param name="strURL" value="" />
                                    <jsp:param name="strXML" value="<%=purchase %>" />
                                    <jsp:param name="chartId" value="productSales" />
                                    <jsp:param name="chartWidth" value="350" />
                                    <jsp:param name="chartHeight" value="250" />
                                    <jsp:param name="debugMode" value="false" />
                                    <jsp:param name="registerWithJS" value="false" />
                                </jsp:include>
                            </td>
                        </tr>
                        </table>
                    </td>
                    <td>
                        <table align="center"   cellspacing="0px" >
                        <tr>
                            <td>
                                <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                    <jsp:param name="chartSWF" value="/throttle/swf/Pie2D.swf" />
                                    <jsp:param name="strURL" value="" />
                                    <jsp:param name="strXML" value="<%=purchaseMakewise %>" />
                                    <jsp:param name="chartId" value="productSales" />
                                    <jsp:param name="chartWidth" value="350" />
                                    <jsp:param name="chartHeight" value="250" />
                                    <jsp:param name="debugMode" value="false" />
                                    <jsp:param name="registerWithJS" value="false" />
                                </jsp:include>
                            </td>
                        </tr>
                        </table>
                    </td>
                </tr>
            </table>

        </form>
    </body>
</html>


