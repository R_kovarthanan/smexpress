<%-- 
    Document   : TyresRotationList
    Created on : Mar 19, 2012, 3:55:54 PM
    Author     : Senthil
--%>

<%--
    Document   : ViewTyreRotationList
    Created on : 15 Mar, 2012, 5:59:24 PM
    Author     : kannan
--%>

<%@page contentType="text/html" import="java.sql.*,java.text.DecimalFormat" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <head>
        <title>ViewTyreRotationDetails</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript">
            function show_src() {
                document.getElementById('exp_table').style.display='none';
            }
            function show_exp() {
                document.getElementById('exp_table').style.display='block';
            }
            function show_close() {
                document.getElementById('exp_table').style.display='none';
            }
            function getVehicleNos(){
                //onkeypress='getList(sno,this.id)'
                var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
            }
        </script>
    </head>
    <body onLoad="getVehicleNos();">
        <form name="viewTyresRotationList">
            <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="/throttle/images/icon_report.png" alt="Export" onclick="show_exp();" class="arrow" />&nbsp;<img src="/throttle/images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Tyres Rotation List</li>
                            </ul>
                            <div id="first">
                                <table width="900" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                    <tr>
                                        <td>Vehicle No</td><td>
                                            <input type="text" class="textbox" id="regno" name="regNo" value="" />
                                        </td>
                                        <td>Make</td><td><select  name="mfrId" id="mfrId" class="textbox">
                                                <option value="">---Select---</option>
                                                <c:if test = "${mfrList != null}" >
                                                    <c:forEach items="${mfrList}" var="mfr">
                                                        <option  value='<c:out value="${mfr.mfrId}" />'>
                                                            <c:out value="${mfr.mfrName}" />
                                                        </c:forEach >
                                                    </c:if>
                                            </select>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td><font color="red">*</font>From Date</td><td><input name="fromDate" id="fromDate" readonly  class="textbox"  type="text" value="" size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.viewTyresRotationList.fromDate,'dd-mm-yyyy',this)"/></td>
                                        <td><font color="red">*</font>To Date</td><td><input name="toDate" id="toDate" readonly  class="textbox"  type="text" value="" size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.viewTyresRotationList.toDate,'dd-mm-yyyy',this)"/></td>
                                        <td colspan="1" rowpan="2"><input type="submit" name="search" value="View List" class="button" /></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>
            <script language="javascript">
        var vehicleNo = "<%=request.getAttribute("regNo")%>";
        if(vehicleNo != null && vehicleNo != "null") {
            document.viewTyresRotationList.regno.value = vehicleNo;
        }
        var mfrId = "<%=request.getAttribute("mfrId")%>";
        if(mfrId != null && mfrId != "null") {
            document.viewTyresRotationList.mfrId.value = mfrId;
        }
        var fromDate = "<%=request.getAttribute("fromDate")%>";
        if(fromDate != null && fromDate != "null") {
            document.viewTyresRotationList.fromDate.value = fromDate;
        }
        var toDate = "<%=request.getAttribute("toDate")%>";
        if(toDate != null && toDate != "null") {
            document.viewTyresRotationList.toDate.value = toDate;
        }
            </script>
            <br>
            <table width="900" align="center" cellspacing="0" cellpadding="2">
                <tr>
                    <td  class="contenthead">S.No</td>
                    <td  class="contenthead">Vehicle No</td>
                    <td  class="contenthead">Make</td>
                    <td  class="contenthead">Model</td>
                </tr>
                <c:set var="sno" value="0"></c:set>
                <c:if test = "${tyresRotationList != null}" >
                    <c:forEach items="${tyresRotationList}" var="trl">
                        <c:set var="sno" value="${sno+1}"></c:set>
                        <tr>
                            <td class="text2"><c:out value="${sno}" /></td>
                            <td class="text2"><a href="/throttle/tyresRotation.do?vehicleId=<c:out value='${trl.vehicleId}' />">
                                    <c:out value="${trl.vehicleRegNo}" /></a></td>
                            <td class="text2"><c:out value="${trl.mfrName}" /></td>
                            <td class="text2"><c:out value="${trl.modelName}" /></td>
                        </tr>
                    </c:forEach >
                </c:if>
            </table>
        </form>
    </body>
</html>
