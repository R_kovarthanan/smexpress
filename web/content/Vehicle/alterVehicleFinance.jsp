 <%@ include file="/content/common/NewDesign/header.jsp" %>
	<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page  import="java.sql.*,java.text.DecimalFormat" pageEncoding="UTF-8"%>

    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <head>
        <title>Vehicle Finance Update</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });
        </script>

        <script language="javascript">

            function show_src() {
                document.getElementById('exp_table').style.display='none';
            }
            function show_exp() {
                document.getElementById('exp_table').style.display='block';
            }
            function show_close() {
                document.getElementById('exp_table').style.display='none';
            }
            function validate(){
                var errMsg = "";
                if(document.VehicleFinance.regNo.value){
                    errMsg = errMsg+"Vehicle Registration No is not filled\n";
                }
                if(document.VehicleFinance.insuranceCompanyName.value){
                    errMsg = errMsg+"Insurance Company is not filled\n";
                }
                if(document.VehicleFinance.premiumNo.value){
                    errMsg = errMsg+"Premium No is not filled\n";
                }
                if(document.VehicleFinance.premiumPaidAmount.value){
                    errMsg = errMsg+"Premium paid Amount is not filled\n";
                }
                if(document.VehicleFinance.premiumPaidDate.value){
                    errMsg = errMsg+"Premium paid Date is not filled\n";
                }
                if(document.VehicleFinance.vehicleValue.value){
                    errMsg = errMsg+"Vehicle Value is not filled\n";
                }
                if(document.VehicleFinance.premiumExpiryDate.value){
                    errMsg = errMsg+"Premium expiry Date is not filled\n";
                }
                if(errMsg != "") {
                    alert(errMsg);
                    return false;
                }else {
                    return true;
                }
            }
            /*var httpRequest;
            function getVehicleDetails(regNo)
            {

                if(regNo != "") {
                    var url = "/throttle/getVehicleDetailsInsurance.do?regNo1="+ regNo;
                    url = url+"&sino="+Math.random();
                    if (window.ActiveXObject)  {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)  {
                        httpRequest = new XMLHttpRequest();
                    }
                    httpRequest.open("GET", url, true);
                    httpRequest.onreadystatechange = function() { processRequest(); } ;
                    httpRequest.send(null);
                }
            }


            function processRequest()
            {
                if (httpRequest.readyState == 4)  {

                    if(httpRequest.status == 200) {
                        if(httpRequest.responseText.valueOf()!=""){
                            var detail = httpRequest.responseText.valueOf();
                            var vehicleValues = detail.split("~");
                            document.VehicleInsurance.vehicleId.value = vehicleValues[0]
                            document.VehicleInsurance.chassisNo.value = vehicleValues[2]
                            document.VehicleInsurance.engineNo.value = vehicleValues[2]
                            document.VehicleInsurance.vehicleMake.value = vehicleValues[4]
                            document.VehicleInsurance.vehicleModel.value = vehicleValues[5]

                        }
                    }
                    else
                    {
                        alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
                    }
                }
            }*/
        </script>

    </head>
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Alter Vehicle Finance" text="Alter Vehicle Finance"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Vehicles" text="Vehicles"/></a></li>
		                    <li class=""><spring:message code="hrms.label.Alter Vehicle Finance" text="Alter Vehicle Finance"/></li>
		
		                </ol>
		            </div>
       			</div>
        
             <div class="contentpanel">
             <div class="panel panel-default">
             <div class="panel-body">

    <body onLoad="">
        <form name="VehicleFinance" action="/throttle/updateVehicleFinance.do">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
            <!--<h2 align="center">Vehicle Insurance Details</h2>-->
            <table class="table table-info mb30 table-hover" id="bg" >
                <thead>
                    
              

                <tr>
                    <th  colspan="4">Vehicle Details</th>
                </tr>
                  </thead>
                <c:if test="${vehicleDetail != null}">
                    <c:forEach items="${vehicleDetail}" var="VDet" >
                        <tr>
                            <td >Vehicle No</td>
                            <td ><input type="text" name="regNo" id="regNo" class="form-control" style="width:240px;height:40px" value='<c:out value="${VDet.regno}" />' ><input type="hidden" name="vehicleId" id="vehicleId" value='<c:out value="${VDet.vehicleId}" />' /></td>
                            <td >Make</td>
                            <td ><input type="text" name="vehicleMake" id="vehicleMake" class="form-control" style="width:240px;height:40px" value='<c:out value="${VDet.mfrName}" />'></td>
                        </tr>
                        <tr>
                            <td >Model</td>
                            <td ><input type="text" name="vehicleModel" id="vehicleModel" class="form-control" style="width:240px;height:40px" value='<c:out value="${VDet.modelName}" />' ></td>
                            <td >Usage</td>
                            <td ><input type="text" name="vehicleUsage" id="vehicleUsage" class="form-control" style="width:240px;height:40px"  ></td>
                        </tr>
                        <tr>
                            <td >Engine No</td>
                            <td ><input type="text" name="engineNo" id="engineNo" class="form-control" style="width:240px;height:40px" value='<c:out value="${VDet.engineNo}" />' ></td>
                            <td >Chassis No</td>
                            <td ><input type="text" name="chassisNo" id="chassisNo" class="form-control" style="width:240px;height:40px" value='<c:out value="${VDet.chassisNo}" />' ></td>
                        </tr>
                    </c:forEach>
                </c:if>
                        <thead>
                            
                        
                <tr>
                    <th  colspan="4">Finance Details</th>
                </tr>
                </thead>
                <c:if test="${vehicleFinance != null}">
                    <c:forEach items="${vehicleFinance}" var="vFin" >
                        <tr>
                            <td >Banker Name</td>
                            <td ><input type="text" name="bankerName" id="bankerName" class="form-control" style="width:240px;height:40px" value="<c:out value="${vFin.bankerName}" />" /><input type="hidden" name="amcId" id="amcId" value='<c:out value="${vFin.financeId}" />'></td>
                            <td >Banker Address</td>
                            <td ><textarea name="bankerAddress" class="form-control" style="width:240px;height:40px" value="<c:out value="${vFin.bankerAddress}" />"></textarea></td>
                        </tr>
                        <tr>
                            <td >Finance Amount</td>
                            <td ><input type="text" name="financeAmount" id="financeAmount" class="form-control" style="width:240px;height:40px" value="<c:out value="${vFin.financeAmount}" />" /></td>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>
                        </tr>
                        <tr>
                            <td >Rate of Interest</td>
                            <td ><input type="text" name="roi" id="roi" class="form-control" style="width:240px;height:40px" value="<c:out value="${vFin.roi}" />" /></td>
                            <td >Interest Type</td>
                            <td >
                                <select name="interestType" class="form-control" style="width:240px;height:40px">
                                    <option value="Simple Interest">Simple Interest</option>
                                    <option value="Compound Interest">Compound Interest</option>
                                    <option value="Diminishing Balance Interest">Diminishing Balance Interest</option>
                                </select>
                                <script type="text/javascript">
                                    document.getElementById("interestType").value = '<c:out value="${vFin.interestType}"/>';
                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td >EMI Total Months</td>
                            <td ><input type="text" name="emiMonths" id="emiMonths" class="form-control" style="width:240px;height:40px" value="<c:out value="${vFin.emiMonths}" />" /></td>
                            <td >EMI Amount</td>
                            <td ><input type="text" name="emiAmount" id="emiAmount" class="form-control" style="width:240px;height:40px" value="<c:out value="${vFin.emiAmount}" />" /></td>
                        </tr>
                        <tr>
                            <td >EMI Start Date</td>
                            <td ><input type="text" name="emiStartDate" id="emiStartDate" class="datepicker form-control" style="width:240px;height:40px" value="<c:out value="${vFin.emiStartDate}" />" /></td>
                            <td >EMI End Date</td>
                            <td ><input type="text" name="emiEndDate" id="emiEndDate" class="datepicker form-control" style="width:240px;height:40px" value="<c:out value="${vFin.emiEndDate}" />" /></td>
                        </tr>
                        <tr>
                            <td >EMI Monthly Pay By Date</td>
                            <td >
                                <select name="emiPayDay" class="form-control" style="width:240px;height:40px">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="10">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                </select>
                                <script type="text/javascript">
                                    document.getElementById("emiPayDay").value = '<c:out value="${vFin.emiPayDay}"/>';
                                </script>
                            </td>
                            <td >Pay Mode</td>
                            <td >
                                <select name="payMode" class="form-control" style="width:240px;height:40px">
                                    <option value="ECS">ECS</option>
                                    <option value="PDC">PDC</option>
                                    <option value="PDC">RTGS</option>
                                </select>
                                <script type="text/javascript">
                                    document.getElementById("payMode").value = '<c:out value="${vFin.payMode}"/>';
                                </script>
                            </td>
                        </tr>
                    </c:forEach>
                </c:if>
<!--                <tr>
                    <td align="center"  colspan="4"></td>
                </tr>-->
            </table>
            <br>
            <center>                
                <input type="submit" class="btn btn-success" style="width:100px;height:40px" value=" Update " />
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>
	</div>
        </div>
        <%@ include file="/content/common/NewDesign/settings.jsp" %>

