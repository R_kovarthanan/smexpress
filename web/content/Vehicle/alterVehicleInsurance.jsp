<%-- 
    Document   : alterVehicleInsurance
    Created on : Sep 20, 2012, 11:26:54 AM
    Author     : ASHOK
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--<%@page contentType="text/html" import="java.sql.*,java.text.DecimalFormat" pageEncoding="UTF-8"%>--%>
<!DOCTYPE html>
<!--<html>-->
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <head>
        <title>Vehicle Insurance Update</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        
        <script type="text/javascript">  

      function alphanumeric_only(e) {

            var keycode;
            if (window.event) keycode = window.event.keyCode;
            else if (event) keycode = event.keyCode;
            else if (e) keycode = e.which;

            else return true;
           if ((keycode >= 47 && keycode <= 57) || (keycode >= 65 && keycode <= 90) || (keycode >= 95 && keycode <= 122)) {

                return true;
            }

            else {
                alert("Please do not use special characters")
                return false;
            }

            return true;
        }

</script>




        
        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>

        <script language="javascript">

            function show_src() {
                document.getElementById('exp_table').style.display='none';
            }
            function show_exp() {
                document.getElementById('exp_table').style.display='block';
            }
            function show_close() {
                document.getElementById('exp_table').style.display='none';
            }



            function validate(){
                var errMsg = "";
                if(document.VehicleInsurance.regNo.value){
                    errMsg = errMsg+"Vehicle Registration No is not filled\n";
                }
                if(document.VehicleInsurance.insuranceCompanyName.value){
                    errMsg = errMsg+"Insurance Company is not filled\n";
                }
                if(document.VehicleInsurance.premiumNo.value){
                    errMsg = errMsg+"Premium No is not filled\n";
                }
                if(document.VehicleInsurance.premiumPaidAmount.value){
                    errMsg = errMsg+"Premium paid Amount is not filled\n";
                }
                if(document.VehicleInsurance.premiumPaidDate.value){
                    errMsg = errMsg+"Premium paid Date is not filled\n";
                }
                if(document.VehicleInsurance.vehicleValue.value){
                    errMsg = errMsg+"Vehicle Value is not filled\n";
                }
                if(document.VehicleInsurance.premiumExpiryDate.value){
                    errMsg = errMsg+"Premium expiry Date is not filled\n";
                }
                if(errMsg != "") {
                    alert(errMsg);
                    return false;
                }else {
                    return true;
                }
            }


            var httpRequest;
            function getVehicleDetails(regNo)
            {

                if(regNo != "") {
                    var url = "/throttle/getVehicleDetailsInsurance.do?regNo1="+ regNo;
                    url = url+"&sino="+Math.random();
                    if (window.ActiveXObject)  {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)  {
                        httpRequest = new XMLHttpRequest();
                    }
                    httpRequest.open("GET", url, true);
                    httpRequest.onreadystatechange = function() { processRequest(); } ;
                    httpRequest.send(null);
                }
            }


            function processRequest()
            {
                if (httpRequest.readyState == 4)  {

                    if(httpRequest.status == 200) {
                        if(httpRequest.responseText.valueOf()!=""){
                            var detail = httpRequest.responseText.valueOf();
                            var vehicleValues = detail.split("~");
                            document.VehicleInsurance.vehicleId.value = vehicleValues[0]
                            document.VehicleInsurance.chassisNo.value = vehicleValues[2]
                            document.VehicleInsurance.engineNo.value = vehicleValues[2]
                            document.VehicleInsurance.vehicleMake.value = vehicleValues[4]
                            document.VehicleInsurance.vehicleModel.value = vehicleValues[5]

                        }
                    }
                    else
                    {
                        alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
                    }
                }
            }
        </script>

    </head>
    <div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.Alter Vehicle Insurance"  text="Alter Vehicle Insurance"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></a></li>
            <li class="active"><spring:message code="stores.label.Alter Vehicle Insurance"  text="Alter Vehicle Insurance"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">


    <body onLoad="setImages(1,0,0,0,0,0);">
        <form name="VehicleInsurance" action="/throttle/updateVehicleInsurance.do">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
<!--            <h2 align="center">Vehicle Insurance Details</h2>
            <table width="800" align="center" class="table2" cellpadding="0" cellspacing="0">-->
<table class="table table-info mb30 table-hover" id="report">
        <thead>
                <tr>
                    <th  colspan="4">Vehicle Details</th>
                </tr>
                </thead>
                <c:if test="${vehicleDetail != null}">
                    <c:forEach items="${vehicleDetail}" var="VDet" >
                        <tr>
                            <td >Vehicle No</td>
                            <td ><input type="text" name="regNo" id="regNo" onkeypress="return alphanumeric_only(this);" maxlength="12" class="form-control" value='<c:out value="${VDet.regno}" />' ><input type="hidden" name="vehicleId" id="vehicleId" value='<c:out value="${VDet.vehicleId}" />' /></td>
                            <td >Make</td>
                            <td ><input type="text" name="vehicleMake" id="vehicleMake" class="form-control" value='<c:out value="${VDet.mfrName}" />'></td>
                        </tr>
                        <tr>
                            <td >Model</td>
                            <td ><input type="text" name="vehicleModel" id="vehicleModel" class="form-control" value='<c:out value="${VDet.modelName}" />' ></td>
                            <td >Usage</td>
                            <td ><input type="text" name="vehicleUsage" id="vehicleUsage" class="form-control"  ></td>
                        </tr>
                        <tr>
                            <td >Engine No</td>
                            <td ><input type="text" name="engineNo" id="engineNo" class="form-control" value='<c:out value="${VDet.engineNo}" />' ></td>
                            <td >Chassis No</td>
                            <td ><input type="text" name="chassisNo" id="chassisNo" class="form-control" value='<c:out value="${VDet.chassisNo}" />' ></td>
                        </tr>
                    </c:forEach>
                </c:if>
                        <thead>
                <tr>
                    <th  colspan="4">Insurance Details</th>
                </tr>
                </thead>
                <c:if test="${vehicleInsurance != null}">
                    <c:forEach items="${vehicleInsurance}" var="VIns" >
                        <tr>
                            <td >Insurance Company</td>
                            <td ><input type="text" name="insuranceCompanyName" id="insuranceCompanyName" class="form-control" value='<c:out value="${VIns.companyname}" />' ><input type="hidden" name="insuranceid" id="insuranceid" value='<c:out value="${VIns.insuranceid}" />' ></td>
                            <td >Premium No</td>
                            <td ><input type="text" name="premiumNo" id="premiumNo" class="form-control" value='<c:out value="${VIns.premiumno}" />' ></td>
                        </tr>
                        <tr>
                            <td >Premium Amount</td>
                            <td ><input type="text" name="premiumPaidAmount" id="premiumPaidAmount" class="form-control"  value='<c:out value="${VIns.premiumamount}" />' ></td>
                            <td >Premium Paid Date</td>
                            <td ><input type="text" name="premiumPaidDate" id="premiumPaidDate" class="datepicker form-control"    value='<c:out value="${VIns.premiumpaiddate}" />' ></td>
                        </tr>
                        <tr>
                            <td >Vehicle Value</td>
                            <td ><input type="text" name="vehicleValue" id="vehicleValue" class="form-control"  value='<c:out value="${VIns.vehiclevalue}" />' ></td>
                            <td >Expiry Date</td>
                            <td ><input type="text" name="premiumExpiryDate" id="premiumExpiryDate" class="datepicker form-control"    value='<c:out value="${VIns.expirydate}" />' ></td>
                        </tr>
                        <tr>
                            <td >NCB</td>
                            <td ><input type="text" name="insuarnceNCB" id="insuarnceNCB" class="form-control"  value='<c:out value="${VIns.ncb}" />' ></td>
                            <td >Remarks</td>
                            <td ><input type="text" name="insuarnceRemarks" id="insuarnceRemarks" class="form-control"  value='<c:out value="${VIns.remarks}" />' ></td>
                        </tr>
                    </c:forEach>
                </c:if>
                <tr>
                    <td align="center"  colspan="4"></td>
                </tr>
            </table>
           <br>
            <center>
                Upload Copy Of Bills&emsp;<br><br><input type="file" />
                <br>
                <br>
                <input type="submit" class="btn btn-success" value=" Update " />
            </center>
    </body>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</div>
    </div>
    </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
