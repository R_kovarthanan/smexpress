   <%@ include file="/content/common/NewDesign/header.jsp" %>
	<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 


<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

    <head>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>

        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        
       <script type="text/javascript">  

      function alphanumeric_only(e) {

            var keycode;
            if (window.event) keycode = window.event.keyCode;
            else if (event) keycode = event.keyCode;
            else if (e) keycode = e.which;

            else return true;
           if ((keycode >= 47 && keycode <= 57) || (keycode >= 65 && keycode <= 90) || (keycode >= 95 && keycode <= 122)) {

                return true;
            }

            else {
                alert("Please do not use special characters")
                return false;
            }

            return true;
        }

</script>
        
        
        <script type="text/javascript">

            function show_src() {
                document.getElementById('exp_table').style.display='none';
            }

            function show_exp() {
                document.getElementById('exp_table').style.display='block';
            }

            function show_close() {
                document.getElementById('exp_table').style.display='none';
            }
        </script>

    </head>
   <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Vehicle AMC" text="Vehicle AMC"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Vehicle" text="Vehicle"/></a></li>
		                    <li class=""><spring:message code="hrms.label.Vehicle AMC" text="Vehicle AMC"/></li>
		
		                </ol>
		            </div>
       			</div>
        
             <div class="contentpanel">
             <div class="panel panel-default">
             <div class="panel-body">
    <body onLoad="getVehicleNos();setImages(1,0,0,0,0,0);setDefaultVals('<%= request.getAttribute("regNo")%>','<%= request.getAttribute("typeId")%>','<%= request.getAttribute("mfrId")%>','<%= request.getAttribute("usageId")%>','<%= request.getAttribute("groupId")%>');">
        <form name="viewVehicleDetails"  method="post" >
            <%--<%@ include file="/content/common/path.jsp" %>--%>


            <%@ include file="/content/common/message.jsp" %>


<!--            <table width="700" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Export" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:700px;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">View Vehicle AMC Search</li>
                            </ul>-->
                                <table class="table table-info mb30 table-hover" id="bg"   >
		    <thead>
		<tr>
		    <th colspan="4" align="center" > Vehicle AMC Search</th>
		</tr>
                    </thead>
           
                           
                                    <tr>
                                        <td>Vehicle Number</td>
                                        <td><input type="text" id="regno" name="regNo" onkeypress="return alphanumeric_only(this);" maxlength="15" value="" class="form-control" autocomplete="off" style="width:240px;height:40px" /></td>

                                    

                           <td><input type="button" class="btn btn-success" onclick="submitPage(this.name);" name="search" value="Search"></td>
                          <td> <input type="button" name="add" value="Add" onClick="addPage(this.name)" class="btn btn-success"></td>
                    
                
                                    </tr>
                                </table>
            
            <br>
            
<%
            int index = 1;
           
%>


            <c:if test="${vehicleAmcList == null }" >
                <br>
                <center><font color="red" size="2"> no records found </font></center>
            </c:if>
            <c:if test="${vehicleAmcList != null }" >
            
            <table class="table table-info mb30 table-hover" id="table">
                <thead>
                    <tr>

                        <th >Sno</div></td>
                        <th >Vehicle Number</th>
                        <th >AMC Company Name</th>
                        <th >AMC Amount</th>
                        <th >AMC From Date</th>
                        <th >AMC To Date</th>
                        <th >Action</th>
                    </tr>
                    </thead>
                    <%
            String style = "text1";%>
                    <c:forEach items="${vehicleAmcList}" var="veh" >
                        <%
            if ((index % 2) == 0) {
                style = "text1";
            } else {
                style = "text2";
            }%>
                        <tr>
                            <td > <%= index %> </td>
                            <td > <c:out value="${veh.regNo}" /></td>
                            <td ><c:out value="${veh.amcCompanyName}" /></td>
                            <td ><c:out value="${veh.amcAmount}" /></td>
                            <td ><c:out value="${veh.amcFromDate}" /></td>
                            <td ><c:out value="${veh.amcToDate}" /></td>
                            <td ><a href='/throttle/vehicleAMCDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&amcid=<c:out value="${veh.amcId}" />'>Alter </a> </td>
                        </tr>
                        <% index++; %>
                    </c:forEach>
                </table>
            </c:if>
            <br>

            <script language="javascript" type="text/javascript">
             setFilterGrid("table");
         </script>
         <div id="controls">
             <div id="perpage">
                 <select onchange="sorter.size(this.value)">
                     <option value="5"  selected="selected">5</option>
                     <option value="10">10</option>
                     <option value="20">20</option>
                     <option value="50">50</option>
                     <option value="100">100</option>
                 </select>
                 <span>Entries Per Page</span>
             </div>
             <div id="navigation">
                 <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                 <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                 <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                 <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
             </div>
             <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
         </div>
         <script type="text/javascript">
             var sorter = new TINY.table.sorter("sorter");
             sorter.head = "head";
             sorter.asc = "asc";
             sorter.even = "evenrow";
             sorter.odd = "oddrow";
             sorter.evensel = "evenselected";
             sorter.oddsel = "oddselected";
             sorter.paginate = true;
             sorter.currentid = "currentpage";
             sorter.limitid = "pagelimit";
             sorter.init("table", 1);
        </script>
        
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    <script type="text/javascript">
        function submitPage(value){
            //alert(value);
            if(value == 'search' || value == 'Prev' || value == 'Next' || value == 'GoTo' || value =='First' || value =='Last'){
                if(value=='GoTo'){
                    var temp=document.viewVehicleDetails.GoTo.value;
                    document.viewVehicleDetails.pageNo.value=temp;
                    document.viewVehicleDetails.button.value=value;
                    document.viewVehicleDetails.action = '/throttle/vehicleAMCList.do';
                    document.viewVehicleDetails.submit();
                }else if(value == "First"){
                    temp ="1";
                    document.viewVehicleDetails.pageNo.value = temp;
                    value='GoTo';
                }else if(value == "Last"){
                    temp =document.viewVehicleDetails.last.value;
                    document.viewVehicleDetails.pageNo.value = temp;
                    value='GoTo';
                }
                //document.viewVehicleDetails.button.value=value;
                document.viewVehicleDetails.action = '/throttle/vehicleAMCList.do';
                document.viewVehicleDetails.submit();
              
            }else if(value == 'add'){
                document.viewVehicleDetails.action = '/throttle/handleVehicleAMC.do';
                document.viewVehicleDetails.submit();
            }else{
                document.viewVehicleDetails.action='/throttle/vehicleAMCList.do'
                document.viewVehicleDetails.submit();
            }
        }


        function setDefaultVals(regNo,typeId,mfrId,usageId,groupId){

            if( regNo!='null'){
                document.viewVehicleDetails.regNo.value=regNo;
            }
            if( typeId!='null'){
                document.viewVehicleDetails.typeId.value=typeId;
            }
            if( mfrId!='null'){
                document.viewVehicleDetails.mfrId.value=mfrId;
            }
            if( usageId!='null'){
                document.viewVehicleDetails.usageId.value=usageId;
            }
            if( groupId!='null'){
                document.viewVehicleDetails.groupId.value=groupId;
            }
        }


        function getVehicleNos(){
            //onkeypress='getList(sno,this.id)'
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
        }

    </script>
    <script language="javascript">
        function addPage(value)
        {
//            if (value=='add')
//                {
                 document.viewVehicleDetails.action ='/throttle/handleVehicleAMC.do';
//                }else if(value == 'modify'){
//                
//                document.desigDetail.action ='/throttle/handleModelAlterPage.do';
//            }
            document.viewVehicleDetails.submit();
        }
    </script>
    
</div>
	</div>
        </div>
        <%@ include file="/content/common/NewDesign/settings.jsp" %>  
        
