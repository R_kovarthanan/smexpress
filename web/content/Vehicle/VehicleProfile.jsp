<%-- 
    Document   : VehicleProfile
    Created on : 13 Mar, 2012, 1:41:50 PM
    Author     : kannan
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="JavaScript" src="/throttle/FusionCharts.js"></script>
        <script type="text/javascript" src="/throttle/ui/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="/throttle/prettify/prettify.js"></script>
        <script type="text/javascript" src="/throttle/ui/js/json2.js"></script>
         <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            function viewProfile(vehicleId) {        
                if(vehicleId != "") {
                    document.vehicleProfile.action = "/throttle/viewVehicleProfile.do";
                    document.vehicleProfile.method = "post";
                    document.vehicleProfile.submit();
                }
            }
        </script>
        <style type="text/css">
            .link {
                font: normal 12px Arial;
                text-transform:uppercase;
                padding-left:10px;
                font-weight:bold;
            }
            .link a  {
                color:#7f8ba5;
                text-decoration:none;
            }
            .link a:hover {
                color:#7f8ba5;
                text-decoration:underline;
            }
        </style>
        <style type="text/css">
            #expand { width:100%; }
            .column { width: 250px; float: left; }
            .portlet {width: 410px; margin: 0 1em 1em 0; border:1px solid #CCCCCC;}
            .portlet-header { margin: 0.3em; padding-bottom: 4px; padding-left: 0.2em; cursor:move;font-weight: bold;color:#FFFFFF; background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;height:25px;font-size: 14px; vertical-align: middle; }
            .portlet-header .ui-icon { float: right; }
            .portlet-content { padding: 0.4em; }
            .ui-sortable-placeholder { border: 1px dotted black; visibility: visible !important; height: 50px !important; }
            .ui-sortable-placeholder * { visibility: hidden; }
        </style>
    </head>
   


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                            altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
    <div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Vehicle Profile" text="Vehicle Profile"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Vehicle" text="Vehicle"/></a></li>
            <li class=""><spring:message code="hrms.label.Vehicle Profile" text="Vehicle Profile"/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <!--setImages(1,0,0,0,0,0);-->
    <body>
        <form name="vehicleProfile">
            <table  class="table table-info mb30 table-hover">
                <tr >
            <thead >
                <th colspan="4" >Vehicle Profile</th>
            </thead>
            </tr>
            <!--<table width="90%" class="table2" cellspacing="0" align="center">-->
                <tr>
                    <td >Vehicle Number</td><td >
                        <select class="form-control" style="width:240px;height:40px" name="vehicleId" onchange="viewProfile(this.value);">
                            <option value="">-select-</option>
                            <c:if test = "${vehicleRegNos != null}" >
                                <c:forEach items="${vehicleRegNos}" var="reg">
                                    <option value='<c:out value="${reg.vehicleId}" />'> <c:out value="${reg.regNo}" /></option>
                                </c:forEach>
                            </c:if>
                        </select>
                    </td>

                </tr>
            </table>
            <script type="text/javascript">

                if("<%=request.getAttribute("vehicleId")%>" != "null" && "<%=request.getAttribute("vehicleId")%>" != null) {
                    document.vehicleProfile.vehicleId.value = "<%=request.getAttribute("vehicleId")%>";
                }

            </script>
                <table class="table table-info mb30 table-hover" id="bg" >
                <thead>
                <tr><th colspan="4" >Vehicle Details</td></tr>
                </table>
                <c:if test = "${vehicleProfileList != null}" >
                    <c:forEach items="${vehicleProfileList}" var="vp">
                    <table class="table table-info mb30 table-hover" id="bg" >
                        
                        <tr>
                            <td >Vehicle Number</td><td ><c:out value="${vp.regNo}" /></td>
                            <td >Registration Date</td><td ><c:out value="${vp.registrationDate}" /></td>
                        </tr>
                        <!--<tr>
                            <td >Usage</td><td >Cargo</td>
                            <td >Warranty Period</td><td ><c:out value="${vp.war_period}" /></td>
                        </tr>-->
                        <tr>
                            <td >MFRs</td><td ><c:out value="${vp.mfrName}" /></td>
                            <td >Model</td><td ><c:out value="${vp.modelName}" /></td>                            
                        </tr>                        
                        <tr>
                            <td >Engine No</td><td ><c:out value="${vp.engineNo}" /></td>
                            <td >Chassis No</td><td ><c:out value="${vp.chassisNo}" /></td>
                        </tr>                        
                        <tr>
                            <td >KM Reading</td><td ><c:out value="${vp.kmReading}" /></td>
                            <td >HM Reading</td><td ><c:out value="${vp.hmReading}" /></td>
                        </tr>                        
                        <tr>
                            <td >Daily Running KM</td><td ><c:out value="${vp.dailyKm}" /></td>
                            <td >Daily Running HM</td><td ><c:out value="${vp.dailyHm}" /></td>                            
                        </tr>
                        <tr>
                            <td >Customer</td><td ><c:out value="${vp.custName}" /></td>
                            <td >GPS Tracking System</td><td ><c:out value="${vp.gpsSystem}" /></td>
                        </tr>
                    <thead>
                        <tr><th colspan="4" >Insurance Details</th></tr>
                        </thead>
                        <tr>
                            <td >Insurance Company</td><td ><c:out value="${vp.companyName}" /></td>
                            <td >Premium No</td><td ><c:out value="${vp.premiumNo}" /></td>
                        </tr>
                        <tr>
                            <td >Insurance Expiry Date</td><td ><c:out value="${vp.insExpiryDate}" /></td>
                            <td >Insurance Premium Amount</td><td ><c:out value="${vp.insPremiumAmount}" /></td>
                        </tr>
                        <thead>
                        <tr><th colspan="4" >Permit Details</th></tr>
                        </thead>
                        <tr>
                            <td >Permit Type</td><td ><c:out value="${vp.permitType}" /></td>
                            <td >Permit No</td><td ><c:out value="${vp.permitNo}" /></td>
                        </tr>
                        <tr>
                            <td >Permit Expiry Date</td><td ><c:out value="${vp.permitExpiryDate}" /></td>
                            <td >Permit Amount</td><td ><c:out value="${vp.permitAmount}" /></td>
                        </tr>
                        <thead>
                        <tr><th colspan="4" >AMC Details</th></tr>
                        </thead>
                        <tr>
                            <td >AMC Company Name</td><td ><c:out value="${vp.amcCompanyName}" /></td>
                            <td >AMC Amount</td><td ><c:out value="${vp.amcAmount}" /></td>
                        </tr>
                        <tr>
                            <td >AMC From Date</td><td ><c:out value="${vp.amcFromDate}" /></td>
                            <td >AMC To Date</td><td ><c:out value="${vp.amcToDate}" /></td>
                        </tr>
                        <tr>
                            <td >AMC Duration</td><td ><c:out value="${vp.amcDuration}" /></td>
                            <td >Vehicle Group</td><td ><c:out value="${vp.groupName}" /></td>
                        </tr>
                        <thead>
                        <tr><th colspan="4" >FC Details</th></tr>
                        </thead>
                        <tr>
                            <td >Next FC Date</td><td ><c:out value="${vp.nextFCDate}" /></td>
                            <td >RTO Detail</td><td ><c:out value="${vp.rtoDetail}" /></td>
                        </tr>
                        <tr>
                            <td >Receipt No</td><td ><c:out value="${vp.receiptNo}" /></td>
                            <td >AMC Amount</td><td ><c:out value="${vp.fcAmount}" /></td>
                        </tr>
                        <tr>
                            <td >Remark</td><td ><c:out value="${vp.remarks}" /></td>
                            <td >&nbsp;</td><td >&nbsp;</td>
                        </tr>
                        <thead>
                        <tr><th colspan="4" >Road Tax Details</th></tr>
                        </thead>
                        <tr>
                            <td >Road Tax From Date</td><td ><c:out value="${vp.roadTaxFromDate}" /></td>
                            <td >Road Tax To Date</td><td ><c:out value="${vp.roadTaxToDate}" /></td>
                        </tr>
                        <tr>
                            <td >Road Tax Period</td><td ><c:out value="${vp.roadTaxPeriod}" /></td>
                            <td >Road Tax Amount</td><td ><c:out value="${vp.roadTaxAmount}" /></td>
                        </tr>
                        <tr>
                            <td >Road Tax Paid Location</td><td ><c:out value="${vp.roadTaxPaidLocation}" /></td>
                            <td >Road Tax Receipt No</td><td ><c:out value="${vp.roadTaxReceiptNo}" /></td>
                        </tr>
                        <tr>
                            <td >Road Tax Receipt Date</td><td ><c:out value="${vp.roadTaxReceiptDate}" /></td>
                            <td >&nbsp;</td><td >&nbsp;</td>
                        </tr>
<!--                    </table>-->
            <br>
            <table class="table table-info mb30 table-hover" id="bg" >
                <thead>
                <tr><th colspan="4"  align="right">OEM Details</th></tr>
                </thead>
                <thead>
                <tr>
                    <th>S.No</th><th >Item</th>
                    <th >Position</th><th>OEM Number</th>
                </tr>
                </thead>
                <%
                            int sno = 0;
                %>
                <c:if test = "${tyreList != null}" >
                    <c:forEach items="${tyreList}" var="tyre">
                        <%
                                    sno++;
                        %>
                        <tr>
                            <td ><%=sno%></td><td ><c:out value="${tyre.itemName}" /> </td>
                            <td ><c:out value="${tyre.description}" /></td>
                            <td ><c:out value="${tyre.tyreNo}" /></td>
                        </tr>
                    </c:forEach></c:if>

                </table>
            <br>
            <table class="table table-info mb30 table-hover" id="bg" >
                <thead>
                <tr><th colspan="4" >Cost Details</th></tr>
                </thead>
                <tr>
                    
                    
                    <td >Buying Price</td><td ><c:out value="${vp.vehicleCost}" /></td>
                <td >Depreciation</td><td ><c:out value="${(vp.vehicleCost * vp.vehicleDepreciation)/100}" /></td>
            </tr>
            <tr>
                <td >Current Price</td><td ><c:out value="${vp.vehicleCost- ((vp.vehicleCost * vp.vehicleDepreciation)/100)}" /></td>
                <td >&nbsp;</td><td >&nbsp;</td>
            </tr>

            <tr><td colspan="4" >Government Compliance</td></tr>
            <tr>
                <td >IC</td><td ><c:out value="${vp.premiumPaidAmount}" /></td>
                <td >FC Cost</td><td ><c:out value="${vp.fcAmount}" /></td>
            </tr>
            <tr>
                <td >Road Tax</td><td ><c:out value="${vp.roadTaxPaidAmount}" /></td>
                <td >&nbsp;</td><td >&nbsp;</td>
            </tr>

            <tr><td colspan="4" >Fuel Cost</td></tr>
            <tr>
                <td >KM Run</td><td ><c:out value="${vp.tripRunKm}" /></td>
                <td >Fuel Consumed</td><td ><c:out value="${vp.fuelLiters}" /></td>
            </tr>
            <tr>
                <td >Total Fuel Cost</td><td ><c:out value="${vp.fuelAmount}" /></td>
                <td >&nbsp;</td><td >&nbsp;</td>
            </tr>


            <tr><td colspan="4" >Driver Bata</td></tr>
            <tr>
                <td >KM Run</td><td >&nbsp;</td>
                <td >Cost</td><td >&nbsp;</td>
            </tr>
            <tr><td colspan="4" >Service Cost</td></tr>
            <tr>
                <td  >Total</td><td ><c:out value="${vp.serviceCost}" /></td>
                <td >Cost/month</td><td ><c:out value="${vp.serviceCost}" /></td>
            </tr>

        </table>
            <!--  <table width="90%" height="50" class="table2" cellspacing="0" align="center">
                  <tr>
                      <td>
                          <div style="overflow:auto;width:100%;height:150px; margin: 0px;">
                  <table width="100%" align="center"cellspacing="0" cellpadding="0" class="table2">
              <tr>
                  <td  >S.No</td>
                  <td  >Tyre No</td>
                  <td  >Make</td>
                  <td  >Position</td>
                  <td  >Description</td>
                  <td  >From Date</td>
                  <td  >To Date</td>
                  <td  >Type</td>
                  <td  >Km</td>
                  <td  >Cost</td>
              </tr>


          <tr>
              <td  >1</td>
              <td  >T100014</td>
              <td  >BRIDGESTONE</td>
              <td  >RRO</td>
              <td  >RRO</td>
              <td  >26-08-2009</td>
              <td  >26-02-2011</td>
              <td  >New</td>
              <td  >40,000</td>
              <td  >25,000</td>

          </tr>


          <tr>
              <td  >2</td>
              <td  >T100011</td>
              <td  >BRIDGESTONE</td>
              <td  >RRI</td>
              <td  >Rear Right Inner</td>
              <td  >26-08-2009</td>
              <td  >26-02-2011</td>
              <td  >New</td>
              <td  >40,000</td>
              <td  >25,000</td>

          </tr>
            


          <tr>
              <td  >3</td>
              <td  >T100012</td>
              <td  >BRIDGESTONE</td>
              <td  >RLO</td>
              <td  >Right Outer</td>
              <td  >26-08-2009</td>
              <td  >07-06-2011</td>
              <td  >New</td>
              <td  >40,000</td>
              <td  >25,000</td>
          </tr>


          <tr>
              <td  >4</td>
              <td  >T100013</td>
              <td  >BRIDGESTONE</td>
              <td  >RLI</td>
              <td  >RLI</td>
              <td  >26-08-2009</td>
              <td  >07-06-2011</td>
              <td  >New</td>
              <td  >40,000</td>
              <td  >25,000</td>
          </tr>


          <tr>
              <td  >5</td>
              <td  >MOJ352851</td>
              <td  >BRIDGESTONE</td>
              <td  >FR</td>
              <td  >Front Right</td>
              <td  >29-06-2011</td>
              <td  >Till Date</td>
              <td  >RC</td>
              <td  >20,000</td>
              <td  >10,000</td>
          </tr>


          <tr>
              <td  >6</td>
              <td  >MOJ336551</td>
              <td  >BRIDGESTONE</td>
              <td  >FL</td>
              <td  >Front Left</td>
              <td  >29-06-2011</td>
              <td  >Till Date</td>
              <td  >RC</td>
              <td  >20,000</td>
              <td  >10,000</td>
          </tr>

          </table>
          </td>
              </tr>

      </table>-->
    <br>
    <%

                String serviceCost = "<chart caption='Service Cost' bgColor='FFFFFF,CCCCCC' showPercentageValues='0' plotBorderColor='FFFFFF' numberPrefix='' isSmartLineSlanted='0' showValues='1' showLabels='0' showLegend='1'>"
                        + "<set value='30000' label='Engine'  color='3EA99F' alpha='60'/>"
                        + "<set value='30000' label='Body Works'  color='99CC00' alpha='60'/>"
                        + "<set value='10000' label='Transmission'  color='357EC7' alpha='60'/>"
                        + "<set value='60000' label='Tyres'  color='F535AA' alpha='60'/>"
                        + "<set value='5000' label='Others'  color='FFEA00' alpha='60'/>"
                        + "</chart>";


    %>
    <%--<div id="expand">
        <center>
            <div class="portlet" >
                <div class="portlet-header" style="width:405px;">&nbsp;&nbsp;Service Cost</div>
                <div class="portlet-content">
                    <table align="center"   cellspacing="0px" >
                        <tr>
                            <td>
                                <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                    <jsp:param name="chartSWF" value="/throttle/swf/Pie2D.swf" />
                                    <jsp:param name="strURL" value="" />
                                    <jsp:param name="strXML" value="<%=serviceCost%>" />
                                    <jsp:param name="chartId" value="productSales" />
                                    <jsp:param name="chartWidth" value="400" />
                                    <jsp:param name="chartHeight" value="250" />
                                    <jsp:param name="debugMode" value="false" />
                                    <jsp:param name="registerWithJS" value="false" />
                                </jsp:include>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </center>
    </div>--%>
                    </c:forEach></table>
                </c:if>
    
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
<!--</html>--></div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>


