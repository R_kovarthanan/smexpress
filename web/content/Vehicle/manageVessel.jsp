<%-- 
    Document   : manageVessel
    Created on : Feb 16, 2016, 3:56:03 PM
    Author     : Jp
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <!--<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>-->
<!--        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>-->
  </head>
    <script language="javascript">
        
        
        function submitPage() {
                           
                            var vesselName = document.getElementById("vesselName").value;
                            var vesselCode = document.getElementById("vesselCode").value;
                            var vesselType = document.getElementById("vesselType").value;
                            var linerCode = document.getElementById("linerCode").value;
                            var updateStatus=0;
                            if (document.vessel.vesselName.value == '') {
                                alert('Please Enter Vessel Name ');
                                $("#vesselName").focus();
                                return;
                            } else if (document.vessel.vesselCode.value == '') {
                                alert('Please Enter Vessel Code');
                                $("#vesselCode").focus();
                                return;
                            } else if (document.vessel.vesselType.value == '') {
                                alert('Please Enter Vessel Type');
                                $("#vesselType").focus();
                                return;
                            } else if (document.vessel.linerCode.value == '') {
                                alert('Please Enter liner Code');
                                $("#linerCode").focus();
                                return;
                            }
                            var url = '';
                            url = './saveVessel.do';
                            $.ajax({
                                url: url,
                                data: {vesselName: vesselName, vesselCode: vesselCode, vesselType: vesselType, linerCode: linerCode
                                   
                                },
                                type: "GET",
                                success: function (response) {
                                    updateStatus = response.toString().trim();
                                    if (updateStatus == 0) {
                                        $("#Status").text("Vessel added failed ");
                                    } else {
                                        insertStatus = updateStatus;
                                        $("#Status").text("Vessel added sucessfully ");
                                    }
                                },
                                error: function (xhr, status, error) {
                                }
                            });
                            document.getElementById("save").style.visibility = 'hidden';
                        }
	        
	        
    </script>

    


    <body onload="">
        <form name="vessel"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <font color="red" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; ">

                                    <div align="center" id="Status">&nbsp;&nbsp;
                                    </div>
                                </font>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" id="bg" class="border">
                    <tr align="center">
                        <td colspan="4" align="center" class="contenthead" height="30"><div class="contenthead">Add Vessel</div></td>
                    </tr>
                    
                    <tr>
                        <td class="text2" height="30"><font color="red">*</font>Vessel Name
                        </td>
                        <td class="text2" height="30"><input name="vesselName" id="vesselName" type="text" class="form-control" value="" maxlength="50"></td>
                        <td class="text1" height="30"><font color="red">*</font>Vessel Code</td>
                        <td class="text2" height="30"><input name="vesselCode" id="vesselCode" type="text" class="form-control" value="" maxlength="50"></td>
                        
                    </tr>
                    <tr>
                        <td class="text2" height="30"><font color="red">*</font>Vessel Type
                        </td>
                        <td class="text2" height="30"><input name="vesselType" id="vesselType" type="text" class="form-control" value="" maxlength="50"></td>
                        <td class="text1" height="30"><font color="red">*</font>Liner Code</td>
                        <td class="text2" height="30"><input name="linerCode" id="linerCode" type="text" class="form-control" value="" maxlength="50"></td>
                    </tr>
                    

                    
            </table>
            <br>
            <center>
                <input type="button" value="Save" id="save" class="button" onClick="submitPage();">
                
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    
    </body>
</html>

