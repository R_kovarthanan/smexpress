<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<head>
    <title>Vehicle AMC Update</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
    <script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
    <script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>     

    <script type="text/javascript" src="/throttle/js/suest"></script>
    <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
    <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
    <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
    <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
    
    
    
  <script type="text/javascript">  

      function alphanumeric_only(e) {

            var keycode;
            if (window.event) keycode = window.event.keyCode;
            else if (event) keycode = event.keyCode;
            else if (e) keycode = e.which;

            else return true;
           if ((keycode >= 47 && keycode <= 57) || (keycode >= 65 && keycode <= 90) || (keycode >= 95 && keycode <= 122)) {

                return true;
            }

            else {
                alert("Please do not use special characters")
                return false;
            }

            return true;
        }

</script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

    </script> 
     <script type="text/javascript">
            //auto com

            $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#regno').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getRegistrationNo.do",
                            dataType: "json",
                            data: {
                                regno: request.term
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#vehicleId').val(tmp[0]);
                        $('#regno').val(tmp[1]);
                        getVehicleDetails(tmp[1]);
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };
            });


        </script>


    <script type="text/javascript">

        function show_src() {
            document.getElementById('exp_table').style.display = 'none';
        }
        function show_exp() {
            document.getElementById('exp_table').style.display = 'block';
        }
        function show_close() {
            document.getElementById('exp_table').style.display = 'none';
        }



        function validate() {
            var errMsg = "";
            if (document.VehicleAMC.regNo.value) {
                errMsg = errMsg + "Vehicle Registration No is not filled\n";
            }
            if (document.VehicleAMC.insuranceCompanyName.value) {
                errMsg = errMsg + "Insurance Company is not filled\n";
            }
            if (document.VehicleAMC.premiumNo.value) {
                errMsg = errMsg + "Premium No is not filled\n";
            }
            if (document.VehicleAMC.premiumPaidAmount.value) {
                errMsg = errMsg + "Premium paid Amount is not filled\n";
            }
            if (document.VehicleAMC.premiumPaidDate.value) {
                errMsg = errMsg + "Premium paid Date is not filled\n";
            }
            if (document.VehicleAMC.vehicleValue.value) {
                errMsg = errMsg + "Vehicle Value is not filled\n";
            }
            if (document.VehicleAMC.premiumExpiryDate.value) {
                errMsg = errMsg + "Premium expiry Date is not filled\n";
            }
            if (errMsg != "") {
                alert(errMsg);
                return false;
            } else {
                return true;
            }
        }


        var httpRequest;
        function getVehicleDetails(regNo)
        {

            //alert("ajax");
            if (regNo != "") {
                var url = "/throttle/getVehicleDetailsInsurance.do?regNo1=" + regNo;
                url = url + "&sino=" + Math.random();
                if (window.ActiveXObject) {
                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                }
                else if (window.XMLHttpRequest) {
                    httpRequest = new XMLHttpRequest();
                }
                httpRequest.open("GET", url, true);
                httpRequest.onreadystatechange = function() {
                    processRequest();
                };
                httpRequest.send(null);

            }

        }


        function processRequest()
        {
            if (httpRequest.readyState == 4) {

                if (httpRequest.status == 200) {
                    if (httpRequest.responseText.valueOf() != "") {
                        var detail = httpRequest.responseText.valueOf();
                        //                            alert(detail);
                        if (detail != "null") {
                            var vehicleValues = detail.split("~");
                            document.VehicleAMC.vehicleId.value = vehicleValues[0];
                            document.VehicleAMC.chassisNo.value = vehicleValues[1];
                            document.VehicleAMC.engineNo.value = vehicleValues[2];
                            document.VehicleAMC.vehicleMake.value = vehicleValues[4];
                            document.VehicleAMC.vehicleModel.value = vehicleValues[5];
                            if (parseInt(vehicleValues[8]) > 0) {
                                document.getElementById('exMsg').innerHTML = "Vehicle AMC Entry is Already Existing";
                                document.getElementById('detail').style.display = "none";
                            }
                        } else {
                            document.VehicleAMC.vehicleId.value = "";
                            document.VehicleAMC.chassisNo.value = "";
                            document.VehicleAMC.engineNo.value = "";
                            document.VehicleAMC.vehicleMake.value = "";
                            document.VehicleAMC.vehicleModel.value = "";
                            document.getElementById('exMsg').innerHTML = "";
                        }
                    }

                }

                else
                {
                    alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                }
            }

        }


        function submitPage() {
            if (textValidation(document.VehicleAMC.regno, 'Vehicle No'))
                ;
            else {
                document.VehicleAMC.action = '/throttle/saveVehicleAMC.do';
                document.VehicleAMC.submit();
            }
        }


        function getEvents(e, val) {
            var key;
            if (window.event) {
                key = window.event.keyCode;
            } else {
                key = e.which;
            }
            if (key == 0) {
                getVehicleDetails(val);
            } else {
                getVehicleDetails(val);
            }
        }

    </script>

</head>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Add Vehicle AMC" text="Add Vehicle AMC"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Vehicle" text="Vehicle"/></a></li>
            <li class=""><spring:message code="hrms.label.Add Vehicle AMC" text="Add Vehicle AMC"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body onLoad="getVehicleNos();
                    document.VehicleAMC.regno.focus();">
                <form name="VehicleAMC" method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>


                    <%@ include file="/content/common/message.jsp" %>
                    <table class="table table-info mb30 table-hover"   >
                        <thead>
                            <tr>
                                <th colspan="4" height="30" > Vehicle Details</th>
                            </tr>
                        </thead>
                        <tr>
                            <td >Vehicle No</td>
                            <td c>
                                <input type="text" name="regNo" id="regno" onkeypress="return alphanumeric_only(this);" maxlength="13"
 class="form-control" onkeypress="getEvents(event, this.value);" onblur="getVehicleDetails(this.value);" autocomplete="off" style="width:240px;height:40px" />
                                <!--                            <input type="text" name="regNo" id="regno" class="form-control"  onchange="getVehicleDetails(this.value);" onkeypress="getVehicleDetails(this.value);" autocomplete="off" />-->
                                <input type="hidden" name="vehicleId" id="vehicleId" /></td>
                            <td >Make</td>
                            <td c><input type="text" name="vehicleMake" id="vehicleMake" class="form-control" readonly style="width:240px;height:40px" /></td>
                        </tr>
                        <tr>
                            <td >Model</td>
                            <td ><input type="text" name="vehicleModel" id="vehicleModel" class="form-control" readonly style="width:240px;height:40px" /></td>
                            <td >Usage</td>
                            <td ><input type="text" name="vehicleUsage" id="vehicleUsage" class="form-control" readonly style="width:240px;height:40px" /></td>
                        </tr>
                        <tr>
                            <td >Engine No</td>
                            <td c><input type="text" name="engineNo" id="engineNo" class="form-control" readonly style="width:240px;height:40px" /></td>
                            <td >Chassis No</td>
                            <td c><input type="text" name="chassisNo" id="chassisNo" class="form-control" readonly style="width:240px;height:40px" /></td>
                        </tr>
                        <tr>
                            <td  colspan="4">
                        <center><label id="exMsg" style="color: green; text-align: center; font-weight: bold; font-size: medium;"></label></center>
                        </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <div id="detail" style="display: block;">
                                    <table class="table table-info mb30 table-hover">
                                        <thead>
                                            <tr>
                                                <th colspan="4">AMC Details</th>
                                            </tr>
                                        </thead>
                                        <tr>
                                            <td >AMC Company</td>
                                            <td c><input type="text" name="amcCompanyName" id="amcCompanyName" class="form-control" onclick="getVehicleDetails(regno.value);"  style="width:240px;height:40px"/></td>
                                            <td >AMC Amount</td>
                                            <td c><input type="text" name="amcAmount" id="amcAmount" class="form-control" style="width:240px;height:40px" /></td>
                                        </tr>
                                        <tr>
                                            <td >AMC Duration</td>
                                            <td ><input type="text" name="amcDuration" id="amcDuration" class="form-control" style="width:240px;height:40px" /></td>
                                            <td >AMC Cheque Date</td>
                                            <td ><input type="text" name="amcChequeDate" id="amcChequeDate" class="datepicker form-control"  style="width:240px;height:40px"/></td>
                                        </tr>
                                        <tr>
                                            <td >AMC Cheque No</td>
                                            <td c><input type="text" name="amcChequeNo" id="amcChequeNo" class="form-control" style="width:240px;height:40px" /></td>
                                            <td >AMC From Date</td>
                                            <td c><input type="text" name="amcFromDate" id="amcFromDate" class="datepicker form-control" style="width:240px;height:40px" /></td>
                                        </tr>
                                        <tr>
                                            <td >AMC To Date</td>
                                            <td ><input type="text" name="amcToDate" id="amcToDate" class="datepicker form-control" style="width:240px;height:40px" /></td>
                                            <td >Remarks</td>
                                            <td ><input type="text" name="amcRemarks" id="amcRemarks" class="form-control" style="width:240px;height:40px"/></td>
                                        </tr>

                                        <tr>
                                            <td align="center"  colspan="4"></td>
                                        </tr>

                                    </table>
                                    <br>
                                    <center>
                                        <input type="button" value="Update" name="generate" id="generate" class="btn btn-success" onclick="submitPage();">
                                        <input type="reset" value="Clear" class="btn btn-success">
                                    </center>
                                </div>
                            </td>
                        </tr>
                    </table>

                    <%--<%@ include file="/content/common/NewDesign/commonParameters.jsp" %>--%>
                </form>
            </body>
<!--            <script type="text/javascript">
                function getVehicleNos() {
                    var oTextbox = new AutoSuggestControl(document.getElementById("regno"), new ListSuggestions("regno", "/throttle/getVehicleNos.do?"));
                    //getVehicleDetails(document.getElementById("regno").value);        
                }
            </script>-->

        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>  
