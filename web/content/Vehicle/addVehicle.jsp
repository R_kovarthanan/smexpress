<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>


<script type="text/javascript">

    function alphanumeric_only(e) {

        var keycode;
        if (window.event)
            keycode = window.event.keyCode;
        else if (event)
            keycode = event.keyCode;
        else if (e)
            keycode = e.which;

        else
            return true;
        if ((keycode >= 47 && keycode <= 57) || (keycode >= 65 && keycode <= 90) || (keycode >= 95 && keycode <= 122)) {

            return true;
        }

        else {
            alert("Please do not use special characters")
            return false;
        }

        return true;
    }

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $(function() {
            $("#fromDate1").datepicker({
                defaultDate: "-d",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: "dd-mm-yy",
//                                        maxDate:"-d",
                onClose: function(selectedDate) {
                    var d = selectedDate.split("-");
                    var da = d[0];
                    var mo = d[1];
                    var yr = parseInt(d[2]) + 1;
                    var nd = da + "-" + mo + "-" + yr;
                    $("#toDate1").datepicker("setDate", nd);
                }
            });
            $("#toDate1").datepicker({
                changeMonth: true,
                dateFormat: "dd-mm-yy",
            });
        });
    });
</script>

<script>
    $(document).ready(function() {
        $(function() {
            $("#fcDate").datepicker({
                defaultDate: "-d",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: "dd-mm-yy",
//                                        maxDate:"-d",
                onClose: function(selectedDate) {
                    var d = selectedDate.split("-");
                    var da = d[0];
                    var mo = d[1];
                    var yr = parseInt(d[2]) + 1;
                    var nd = da + "-" + mo + "-" + yr;
                    $("#fcExpiryDate").datepicker("setDate", nd);
                }
            });
            $("#fcExpiryDate").datepicker({
                changeMonth: true,
                dateFormat: "dd-mm-yy",
            });
        });
    });
</script>

<script>
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });

    });

    $(function() {
        $(".datepicker").datepicker({
            changeMonth: true, changeYear: true,
            dateFormat: 'dd-mm-yy'
        });

    });


</script>

<script language="javascript">

    function CalWarrantyDate(selDate) {

        var seleDate = selDate.split('-');
        var dd = seleDate[0];
        var mm = seleDate[1];
        var yyyy = seleDate[2];

        var today = new Date();
        var dd1 = today.getDate();
        var mm1 = today.getMonth() + 1; //January is 0!
        var yyyy1 = today.getFullYear();

        //                if(dd<10){dd='0'+dd}if(mm<10){mm='0'+mm}today = mm+'-'+dd+'-'+yyyy;alert("today"+today);alert("value"+value);

        var selecedDate = new Date(yyyy, mm, dd);
        var currentDate = new Date(yyyy1, mm1, dd1);
        var Days = Math.floor((selecedDate.getTime() - currentDate.getTime()) / (1000 * 60 * 60 * 24));
        //alert("DAYS--"+Days);
        document.addVehicle.war_period.value = Days;

    }



    function submitPage2(value) {
        if (value == "Save") {
            document.addVehicle.action = '/throttle/addVehicle.do';
            document.addVehicle.submit();
        }

    }
    function submitPage(value) {
        if (value == "save") {
            document.addVehicle.action = '/throttle/addVehicle.do';
            document.addVehicle.submit();
        }
    }
    function submitPageDedicate(value) {

        var owner = document.getElementById("asset").value;
        document.getElementById("asset").value = owner;

        if (validate() == 'fail') {
            return;
        } else if (document.addVehicle.vendorId.value == '') {
            alert('Please Enter Vendor name');
            document.addVehicle.vendorId.focus();
            return;
        } else if (document.addVehicle.regNo.value == '') {
            alert('Please Enter Vehicle Registration Number');
            document.addVehicle.regNo.focus();
            return;
        } else if (document.addVehicle.regNoCheck.value == 'exists') {
            alert('Vehicle RegNo already Exists');
            return;
            //                }else if(isSelect(document.addVehicle.usageId,'Usage Type')){
            //                    return;

        } else if (isSelect(document.addVehicle.mfrId, 'Manufacturer')) {
            return;
        } else if (isSelect(document.addVehicle.modelId, 'Vehicle Model')) {
            return;
        } else if (isSelect(document.addVehicle.typeId, 'Vehicle type')) {
            return;
        }
        if (value == "save") {

            document.addVehicle.action = '/throttle/addVehicle.do';
            document.addVehicle.submit();
        }
    }
    function submitPage1(value) {

        var owner = document.getElementById("asset").value;
        document.getElementById("asset").value = owner;

        if (validate() == 'fail') {
            return;
        } else if (document.addVehicle.vendorId.value == '') {
            alert('Please Enter Vendor name');
            document.addVehicle.vendorId.focus();
            return;
        } else if (document.addVehicle.regNo.value == '') {
            alert('Please Enter Vehicle Registration Number');
            document.addVehicle.regNo.focus();
            return;
        } else if (document.addVehicle.regNoCheck.value == 'exists') {
            alert('Vehicle RegNo already Exists');
            return;
            //                }else if(isSelect(document.addVehicle.usageId,'Usage Type')){
            //                    return;

        } else if (isSelect(document.addVehicle.mfrId, 'Manufacturer')) {
            return;
        } else if (isSelect(document.addVehicle.modelId, 'Vehicle Model')) {
            return;
        } else if (isSelect(document.addVehicle.typeId, 'Vehicle type')) {
            return;
        }

        if (value == "save1") {
            // alert("cgsd77f");
            //                    document.addVehicle.nextFCDate.value = dateFormat(document.addVehicle.nextFCDate);
            //     document.addVehicle.dateOfSale.value = dateFormat(document.addVehicle.dateOfSale);
            document.addVehicle.action = '/throttle/addVehicle.do';
            document.addVehicle.submit();
        }
    }


    var httpRequest;
    function getVehicleDetails() {
        var vehicleId = $("#vehicleId").val();
        if (document.addVehicle.regNo.value != '' && vehicleId == '') {
            var url = '/throttle/checkVehicleExists.do?regno=' + document.addVehicle.regNo.value;

            if (window.ActiveXObject)
            {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest)
            {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("POST", url, true);
            httpRequest.onreadystatechange = function() {
                go1();
            };
            httpRequest.send(null);
        }
    }


    function go1() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var response = httpRequest.responseText;
                var temp = response.split('-');
                if (response != "") {
                    alert('Vehicle Already Exists');
                    document.getElementById("StatusMsg").innerHTML = httpRequest.responseText.valueOf() + " Already Exists";
                    document.addVehicle.regNo.focus();
                    document.addVehicle.regNo.select();
                    document.addVehicle.regNoCheck.value = 'exists';
                } else
                {
                    document.addVehicle.regNoCheck.value = 'Notexists';
                    document.getElementById("StatusMsg").innerHTML = "";
                }
            }
        }
    }

</script>

<script>

    function deleteRows(sno) {
        if (sno != 1) {
            rowCount--;
            document.getElementById("addTyres").deleteRow(sno);
        } else {
            alert("Can't Delete The Row");
        }
    }

    var httpReq;
    var temp = "";
    function getVehicleType(mfrId) { //alert(str);
        var fleetTypeId = $("#fleetTypeId").val();
        $.ajax({
            url: "/throttle/getMfrVehicleType.do",
            dataType: "json",
            data: {
                mfrId: mfrId,
                fleetTypeId: fleetTypeId
            },
            success: function(temp) {
                // alert(data);
                if (temp != '') {
                    $('#vehicleTypeId').empty();
                    $('#vehicleTypeId').append(
                            $('<option style="width:150px"></option>').val(0).html('--Select--')
                            )
                    $.each(temp, function(i, data) {
                        $('#vehicleTypeId').append(
                                $('<option style="width:150px"></option>').val(data.Id + "~" + data.AxleTypeId).html(data.Name)
                                )
                    });
                } else {
                    $('#vehicleTypeId').empty();
                }
            }
        });

    }



    function getModelAndTonnage(str) {
        var fleetTypeId = $("#fleetTypeId").val();
        var vehicleTypeId = str.split("~")[0];
        document.getElementById("typeId").value = vehicleTypeId;
        var fleetTypeId = $("#fleetTypeId").val();
        $.ajax({
            url: "/throttle/getModels1.do",
            dataType: "text",
            data: {
                typeId: vehicleTypeId,
                mfrId: document.addVehicle.mfrId.value,
                fleetTypeId: fleetTypeId
            },
            success: function(temp) {
                // alert(data);
                if (temp != '') {
                    setOptions1(temp, document.addVehicle.modelId);
                    document.getElementById("modelId").value = '<c:out value="${modelId}"/>';
                    getVehAxleName(str);
                } else {
                    setOptions1(temp, document.addVehicle.modelId);
                    alert('There is no model based on Vehicle Type Please add first');
                }
            }
        });
        // for tonnage now
        $.ajax({
            url: "/throttle/getTonnage.do",
            dataType: "text",
            data: {
                typeId: vehicleTypeId,
                fleetTypeId: fleetTypeId
            },
            success: function(temp) {
                if (temp != '') {
                    setOptions2(temp, document.addVehicle.seatCapacity);
                } else {

                    alert('Tonnage is not there please set first in vehicle type');
                }
            }
        });
    }


    function setOptions1(text, variab) {
//                                                                alert("setOptions on page")
        //
//                                                                                      alert("variab = "+variab)
        variab.options.length = 0;
        //                                alert("1")
        var option0 = new Option("--select--", '0');
        //                                alert("2")
        variab.options[0] = option0;
        //                                alert("3")


        if (text != "") {
//                                                                        alert("inside the condition")
            var splt = text.split('~');
            var temp1;
            variab.options[0] = option0;
            for (var i = 0; i < splt.length; i++) {
//                                                                            alert("splt.length ="+splt.length)
//                                                                            alert("for loop ="+splt[i])
                temp1 = splt[i].split('-');
                option0 = new Option(temp1[1], temp1[0])
//                                        alert("option0 ="+option0)
                variab.options[i + 1] = option0;
            }
        }
    }

    function setOptions2(text) {
        //                                alert("setOptions on page")
        //                               alert("text = "+text)
        if (text != "") {
            //                                        alert(text);
            document.getElementById('seatCapacity').value = text;

        }
    }


    function getInsCompanyName(val) {

        var issueCmpId = document.getElementById("IssuingCmnyName").value;
        //alert(issueCmpId);
        $.ajax({
            url: '/throttle/getInsCompanyName.do?issueCmpId=' + issueCmpId,
            // alert(url);
            data: {issueCmpId: issueCmpId},
            dataType: 'json',
            success: function(data) {

                if (data !== '') {
                    $.each(data, function(i, data) {
                        var temp = data.Name.split('~');
                        document.getElementById("ins_AgentName").value = temp[0];
                        document.getElementById("ins_AgentCode").value = temp[1];
                        document.getElementById("ins_AgentMobile").value = temp[2];
                    });
                }
            }
        });
    }





    function validate()
    {
        var tyreIds = document.getElementsByName("tyreIds");
        var tyreDate = document.getElementsByName("tyreDate");
        var positionIds = document.getElementsByName("positionIds");
        var itemIds = document.getElementsByName("itemIds");
        var tyreExists = document.getElementsByName("tyreExists");

        var cntr = 0;
        for (var i = 0; i < tyreIds.length; i++) {
            for (var j = 0; j < tyreIds.length; j++) {
                if ((tyreIds[i].value == tyreIds[j].value) && (tyreIds[i].value == tyreIds[j].value) && (i != j) && (tyreIds[i].value != '')) {
                    cntr++;
                }
            }
            if (parseInt(cntr) > 0) {
                alert("Same Tyre Number should not exists twice");
                return "fail";
                break;
            }
        }

        for (var i = 0; i < positionIds.length; i++) {
            for (var j = 0; j < positionIds.length; j++) {
                if ((positionIds[i].value == positionIds[j].value) && (positionIds[i].value == positionIds[j].value) && (i != j) && (positionIds[i].value != '0')) {
                    cntr++;
                }
            }
            if (parseInt(cntr) > 0) {
                alert("Tyre Positions should not be repeated");
                return "fail";
                break;
            }
        }

        for (var i = 0; i < tyreIds.length; i++) {

            if (itemIds[i].value != '0') {
                if (positionIds[i].value == '0') {
                    alert('Please Select Position');
                    positionIds[i].focus();
                    return "fail";
                } else if (tyreIds[i].value == '') {
                    alert('Please Enter Tyre No');
                    tyreIds[i].focus();
                    return "fail";
                } else if (tyreExists[i].value == 'exists') {
                    alert('Tyre number ' + tyreIds[i].value + ' is already fitted to another vehicle');
                    tyreIds[i].focus();
                    tyreIds[i].select();
                    return "fail";
                }
            }
        }

        return "pass";
    }




    var httpRequest;
    function checkTyreId(val)
    {
        val = val - 1;
        var tyreNo = document.getElementsByName("tyreIds");
        var tyreExists = document.getElementsByName("tyreExists");
        if (tyreNo[val].value != '') {
            var url = '/throttle/checkVehicleTyreNo.do?tyreNo=' + tyreNo[val].value;
            if (window.ActiveXObject)
            {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest)
            {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest(tyreNo[val].value, val);
            };
            httpRequest.send(null);
        }
    }


    function processRequest(tyreNo, index)
    {
        if (httpRequest.readyState == 4)
        {
            if (httpRequest.status == 200)
            {
                var tyreExists = document.getElementsByName("tyreExists");
                if (httpRequest.responseText.valueOf() != "") {
                    document.getElementById("userNameStatus").innerHTML = "Tyre No " + tyreNo + " Already Exists in " + httpRequest.responseText.valueOf();
                    tyreExists[index].value = 'exists';
                } else {
                    document.getElementById("userNameStatus").innerHTML = "";
                    tyreExists[index].value = 'notExists';
                }
            } else
            {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }

    function openAttachedInfo() {
        document.getElementById("asset").value = "2";
        document.getElementById("attachInfo").style.display = "block";
    }
    function closeAttachedInfo() {
        document.getElementById("asset").value = "1";
        document.getElementById("attachInfo").style.display = "none";
    }

    function blockNonNumbers(obj, e, allowDecimal, allowNegative)
    {
        var key;
        var isCtrl = false;
        var keychar;
        var reg;

        if (window.event) {
            key = e.keyCode;
            isCtrl = window.event.ctrlKey
        } else if (e.which) {
            key = e.which;
            isCtrl = e.ctrlKey;
        }

        if (isNaN(key))
            return true;

        keychar = String.fromCharCode(key);

        // check for backspace or delete, or if Ctrl was pressed
        if (key == 8 || isCtrl)
        {
            return true;
        }

        reg = /\d/;
        var isFirstN = allowNegative ? keychar == '-' && obj.value.indexOf('-') == -1 : false;
        var isFirstD = allowDecimal ? keychar == '.' && obj.value.indexOf('.') == -1 : false;

        return isFirstN || isFirstD || reg.test(keychar);
    }


</script>

<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();

        $("#insurance").focus();
        $("#vehicleInsurane").focus();
    });
    function selectTabssf(listId) {
        if (listId == '1') {
//                                        alert(listId)
            $("#insurance").focus();

        }
    }
    function selectTabs(listId) {
        var selectedId = getHash(this.getAttribute('href'));

// Highlight the selected tab, and dim all others.
// Also show the selected content div, and hide all others.
        for (var id in contentDivs) {
            if (id == selectedId) {
                tabLinks[id].className = 'selected';
                contentDivs[id].className = 'tabContent';
            } else {
                tabLinks[id].className = '';
                contentDivs[id].className = 'tabContent hide';
            }
        }

// Stop the browser following the link
        return false;
    }

    function vehicleValidation() {//pavi
//                            $("#vehicleDetailUpdateButton").hide();
        var owner = document.getElementById("ownerShips").value;
        document.getElementById("asset").value = owner;
// alert("owner:"+owner);
        if (owner == 1 || owner == 5) {
            if (document.addVehicle.regNo.value == '') {
                alert('Please Enter Vehicle Registration Number');
                document.addVehicle.regNo.focus();
                return false;
            } else if (document.addVehicle.regNoCheck.value == 'exists') {
                alert('Vehicle RegNo already Exists');
                document.addVehicle.regNoCheck.focus();
                return false;
            } else if (document.addVehicle.mfrId.value == 0) {
                alert('Please select Manufacturer');
                document.addVehicle.mfrId.focus();
                return false;
            } else if (document.addVehicle.typeId.value == 0) {
                alert('Please select Vehicle Type');
                document.addVehicle.typeId.focus();
                return false;
            } else if (document.addVehicle.modelId.value == 0) {
                alert('Please select Vehicle Model');
                document.addVehicle.modelId.focus();
                return false;
            } else if (document.addVehicle.dateOfSale.value == '') {
                alert("Please select Registration Date");
                document.addVehicle.dateOfSale.focus();
                return false;
            } else if (document.addVehicle.engineNo.value == '') {
                alert("Please enter Engine Number");
                document.addVehicle.engineNo.focus();
                return false;
            } else if (document.addVehicle.chassisNo.value == '') {
                alert("Please enter Chassis Number");
                document.addVehicle.chassisNo.focus();
                return false;
            } else if (document.addVehicle.seatCapacity.value == '' || document.addVehicle.seatCapacity.value == 0) {
                alert("Please enter Vehicle Tonnage");
                document.addVehicle.seatCapacity.focus();
                return false;
            } else if (document.addVehicle.warrantyDate.value == '') {
                alert("Please select Warranty Date");
                document.addVehicle.warrantyDate.focus();
                return false;
            } else if (document.addVehicle.war_period.value == '' || document.addVehicle.war_period.value == 0) {
                alert("Please select Warranty Period");
                document.addVehicle.war_period.focus();
                return false;
            } else if (document.addVehicle.vehicleCost.value == '') {
                alert("Please select Vehicle Cost");
                document.addVehicle.vehicleCost.focus();
                return false;
            } else if (document.addVehicle.vehicleDepreciation.value == '') {
                alert("Please select Vehicle Depreciation");
                document.addVehicle.vehicleDepreciation.focus();
                return false;
                //                }else if(document.addVehicle.classId == 0){
                //                    alert("Please select Vehicle Class");
                //                    document.addVehicle.classId.focus();
                //                    return false;
            } else if (document.addVehicle.kmReading.value == '') {
                alert("Please select Km Reading");
                document.addVehicle.kmReading.focus();
                return false;
            } else if (document.addVehicle.dailyKm.value == '') {
                alert("Please select Daily Km Reading");
                document.addVehicle.dailyKm.focus();
                return false;
            } else if (document.addVehicle.dailyHm.value == '') {
                alert('Please select Daily Hm reading');
                document.addVehicle.dailyHm.focus();
                return false;
            } else if (document.addVehicle.vehicleColor.value == '') {
                alert('Please fill vehicle color');
                document.addVehicle.vehicleColor.focus();
                return false;
            } else {
                //alert("am here :2");
                saveVehicleDetails();
                return true;

            }
        } else {
            if (document.addVehicle.regNo.value == '') {
                alert('Please Enter Vehicle Registration Number');
                document.addVehicle.regNo.focus();
                return false;
            } else if (document.addVehicle.regNoCheck.value == 'exists') {
                alert('Vehicle RegNo already Exists');
                document.addVehicle.regNoCheck.focus();
                return false;
            } else {
                saveVehicleDetails();
                return true;

            }
        }


    }
    function saveVehicleDetails() {
//alert("a m here 3");
//$("#vehicleDetailSaveButton").hide();
        var usageId = document.addVehicle.usageId.value;
        var warPeriod = document.addVehicle.war_period.value;
        var mfrId = document.addVehicle.mfrId.value;
        var classId = document.addVehicle.classId.value;
        var modelId = document.addVehicle.modelId.value;
        var dateOfSale = document.addVehicle.dateOfSale.value;
        var regNo = document.addVehicle.regNo.value;
        var description = document.addVehicle.description.value;
        var engineNo = document.addVehicle.engineNo.value;
        var chassisNo = document.addVehicle.chassisNo.value;
        var opId = document.addVehicle.opId.value;
        var seatCapacity = document.addVehicle.seatCapacity.value;
        var kmReading = document.addVehicle.kmReading.value;
        var vehicleCost = document.addVehicle.vehicleCost.value;
        var vehicleColor = document.addVehicle.vehicleColor.value;
        var groupId = document.addVehicle.groupId.value;
        var gpsSystem = document.addVehicle.gpsSystem.value;
        var warrantyDate = document.addVehicle.warrantyDate.value;
        var asset = document.addVehicle.asset.value;
        var axles = document.addVehicle.axles.value;
        var vehicleDepreciation = document.addVehicle.vehicleDepreciation.value;
        var dailyKm = document.addVehicle.dailyKm.value;
        var dailyHm = document.addVehicle.dailyHm.value;
        var gpsSystemId = document.addVehicle.gpsSystemId.value;
        var ownerShips = document.addVehicle.ownerShips.value;
        var typeId = document.addVehicle.typeId.value;
        var vendorId = document.addVehicle.vendorId.value;
        var axleTypeId = document.addVehicle.axleTypeId.value;
        var vehicleId = document.addVehicle.vehicleId.value;
        var activeInd = document.addVehicle.activeInd.value;
// alert("csh")
        var insertStatus = 0;
        if (vendorId == '') {
            vendorId = 0;
        }
//                alert("typeId == "+typeId);
        var vehicleId = $("#vehicleId").val();
        if (vehicleId == '') {
            vehicleId = 0;
        }
        var statusName = "";
        var url = "";
        if (vehicleId == 0) {
            //alert("added")
            url = "./saveVehicleDetails.do";
            statusName = "added";
        } else {
//                            alert("updated")
            url = "./updateVehicleDetails.do";
            statusName = "updated";
        }
//  alert("test url");
        $.ajax({
            url: url,
            data: {usageId: usageId,
                war_period: warPeriod, mfrId: mfrId, classId: classId, modelId: modelId,
                dateOfSale: dateOfSale, regNo: regNo, description: description, regNo: regNo,
                        engineNo: engineNo, chassisNo: chassisNo, opId: opId, seatCapacity: seatCapacity,
                kmReading: kmReading, vehicleColor: vehicleColor, groupId: groupId,
                gpsSystem: gpsSystem, warrantyDate: warrantyDate, asset: asset, axles: axles, vehicleCost: vehicleCost,
                vehicleDepreciation: vehicleDepreciation, dailyKm: dailyKm, dailyHm: dailyHm,
                gpsSystemId: gpsSystemId, ownerShips: ownerShips, typeId: typeId, vendorId: vendorId,
                axleTypeId: axleTypeId, vehicleId: vehicleId, activeInd: activeInd
            },
            type: "GET",
            success: function(response) {

                var vehicleDetails = response.toString().trim();
                var temp = vehicleDetails.split("~");
                vehicleId = parseInt(temp[0]);
                // alert(vehicleId);
                if (vehicleId == 0) {
                    insertStatus = 0;
                    $("#StatusMsg").text("Vehicle " + regNo + " " + statusName + " failed ").css("color", "red");
                } else {
                    insertStatus = vehicleId;
                    $("#StatusMsg").text("Vehicle " + regNo + " " + statusName + " sucessfully. Door No is " + temp[1]).css("color", "green");
                    $("#vehicleId").val(vehicleId);
                    $("#vehicleDetailSaveButton").hide();
                    // $("#tyreSave").show();
                    $("#depreciationSave").show();
                    $("#uploadBtn").show();
                    $("#oEmNext").show();
                    alert("Vehicle " + regNo + " " + statusName + " sucessfully. Door No is " + temp[1]);
                }
            },
            error: function(xhr, status, error) {
            }
        });
        return insertStatus;
    }
    function checkaddRow()
    {
        if ('<%=request.getAttribute("vehicleUploadsDetails")%>' != 'null') {
        } else {
            addRow();
        }

    }
//                   function testPage(){
//                                        var str = new String("Hello world");
//         alert(str.css("color", "green"));
//                                    }
</script>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.Vehicle"  text="Vehicle"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></a></li>
            <li class="active"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">

            <!--    <body onLoad="addRow();setImages(0,0,0,0,0,0);hideAdds(1),hideAdds1(1);advanceFuelTabHide()">-->
            <body onLoad="checkaddRow();
                    addOem(0, 1, '', '', '', 0, 0, 0);
                    vehicleStepneyDetails(1);
                  ">
<!--                        <body onLoad="checkaddRow();checkPaymentMode();getBankBranch('<c:out value="${bankId}"/>');getBankBranchRoadTax('<c:out value="${bankIdRT}"/>');
                   getBankBranchFC('<c:out value="${bankIdFC}"/>');getBankBranchPermit('<c:out value="${bankIdPermit}"/>');selectTabs('<c:out value="${listId}"/>');
                   addOem(0, 1, '', '', '', 0, 0, 0);
                   vehicleOEMRow();vehicleStepneyDetails(1);
                   addRow1();
                   setImages(0, 0, 0, 0, 0, 0);
                   hideAdds(1);hideAdds1(1);">-->

                <style>
                    body {
                        font:13px verdana;
                        font-weight:normal;
                    }
                </style>
                <form name="addVehicle" method="post" enctype="multipart/form-data" >

                    <%@ include file="/content/common/message.jsp" %>
                    <input type="hidden" name="fleetTypeId" id="fleetTypeId" value="<c:out value="${fleetTypeId}"/>" />
                    <font  style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;">
                    <div align="center" style="height:30px;" id="StatusMsg">&nbsp;&nbsp;
                    </div>
                    </font>

                    <input type="hidden" name="fleetTypeId" id="fleetTypeId" value="1" />
                    <div id="tabs">
                        <ul class="nav nav-tabs">
                            <li class="active" data-toggle="tab"><a href="#vehicleInfo"><span><spring:message code="trucks.label.Vehicle"  text="default text"/></span></a></li>
<!--                            <li  data-toggle="tab"><a href="#vehicleOem"><span><spring:message code="trucks.label.OEM"  text="default text"/></span></a></li>
                            <li  data-toggle="tab"><a href="#vehicleDepreciationTab"><span><spring:message code="trucks.label.Depreciation"  text="default text"/></span></a></li>-->
                            <li  data-toggle="tab"><a href="#vehicleFileAttachments"><span><spring:message code="trucks.label.FileAttachments"  text="default text"/></span></a></li>
                            <!--                                        <li id="insurance"><a href="#vehicleInsurane"><span>Insurance</span></a></li>
                                                                    <li><a href="#vehicleRoadTax"><span>Road Tax</span></a></li>
                                                                    <li><a href="#vehicleFitnessCertificate"><span>Fitness Certificate</span></a></li>
                                                                    <li><a href="#vehiclePermit"><span>Permit</span></a></li>-->
                            <!--<li  class="disable"><a href="#vehicleDetailsConfirm"><span>Confirm</span></a></li>-->
                        </ul>
                        <div id="vehicleInfo">
                            <input type="hidden" name="regNoCheck" id="regNoCheck" value='' >
                            <table border="0" class="border" align="center" width="1000" cellpadding="0" cellspacing="0" id="bg">
                                <tr>
                                    <td colspan="6">
                                        <table class="table table-info mb30 table-hover" >
                                            <thead>
                                            <th colspan="6"  height="30">
                                            <div  align="center"><spring:message code="trucks.label.Vehicle"  text="default text"/> </div>
                                            </th>
                                            </thead>
                                            <!--<td colspan="4">-->
                                            <tr width="100%">
                                            <div id="groupInfo" style="display: none;">

                                                <td height="30" width="20%"><font color="red" size="4">*</font><font style="bold"><spring:message code="trucks.label.VehicleOwnerShip"  text="default text"/></font></td>
                                                <td width="20%"> <select class="form-control" name="ownerShips" id="ownerShips" onchange="ChangeDropdowns(this.value);" style="width:260px;height:40px;">
                                                        <option value="0">---<spring:message code="trucks.label.select"  text="default text"/>---</option>
                                                        <option value="1" ><spring:message code="trucks.label.Own"  text="default text"/></option>
                                                        <option value="2" ><spring:message code="trucks.label.Dedicate"  text="default text"/></option>
                                                        <option value="3" ><spring:message code="trucks.label.Hire"  text="default text"/> </option>
                                                        <option value="4" ><spring:message code="trucks.label.Replacement"  text="default text"/></option>
                                                        <input type="hidden" name="groupNo" id="groupNo" class="form-control" value="1">
                                                    </select>
                                                    <script>
                                                        document.getElementById("ownerShips").value = '<c:out value="${ownership}"/>';
                                                    </script>
                                                    <input type="hidden" name="vehicleId" id="vehicleId" value="<c:out value="${vehicleId}"/>" /></td>
                                            </div>

                                            <input type="hidden" name="asset" id="asset" class="form-control" value="1">
                                            <td  colspan="4">
                                                <div id="attachInfo" style="display: none">
                                                    <table width="100%" cellpadding="0" cellspacing="2"  id="vandornameId" >
                                                        <tr>
                                                            <td height="30%" width="20%"><font color="red">*</font><spring:message code="trucks.label.VendorName"  text="VendorName"/></td>

                                                            <td width="10%" align="center">
                                                                <c:if test = "${vendorNameList != null}" >
                                                                    <c:forEach items="${vendorNameList}" var="vnl">
                                                                        <input type="text" id ="vendorId" value='<c:out value="${vnl.vendorId}"/>' style="width:260px;height:40px;"/>
                                                                        <input type="text" id ="vendorName" value='<c:out value="${vnl.vendorName}"/>' style="width:260px;height:40px;"/>
                                                                    </c:forEach >
                                                                </c:if>
                                                                <c:if test = "${leasingCustList != null}" >
                                                                    <select class="form-control" name="vendorId" id="vId" style="width:260px;height:40px;" >
                                                                        <option value="0" selected>--<spring:message code="trucks.label.Select"  text="default text"/>--</option>
                                                                        <c:forEach items="${leasingCustList}" var="LCList">
                                                                            <option value='<c:out value="${LCList.vendorId}" />'><c:out value="${LCList.vendorName}" /></option>
                                                                        </c:forEach >
                                                                    </select>
                                                                </c:if>
                                                                <script>
                                                                    document.getElementById("vId").value = '<c:out value="${vendorId}"/>';
                                                                </script>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                </tr>
                                <tr>
                                    <td height="30"><font color="red">*</font><spring:message code="trucks.label.VehicleNumber"  text="default text"/></td>
                                    <td height="30"><input onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9]/g, '')" maxlength="10"  name="regNo" type="text" class="form-control" value="<c:out value="${regNo}"/>" onChange="getVehicleDetails();" style="width:260px;height:40px;"></td>


                                    <td height="30" width="20%"><font color="red">*</font><spring:message code="trucks.label.Make"  text="default text"/></td>
                                    <td width="20%"><select class="form-control" name="mfrId" id="mfrId"   onChange="getVehicleType(this.value);" style="width:260px;height:40px;" >
                                            <option value="0">---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
                                            <c:if test = "${MfrList != null}" >
                                                <c:forEach items="${MfrList}" var="Dept">
                                                    <option value='<c:out value="${Dept.mfrId}" />'><c:out value="${Dept.mfrName}" /></option>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                        <script>
                                            document.getElementById("mfrId").value = '<c:out value="${mfrId}"/>';
                                        </script>
                                    </td>


                                </tr>
                                <tr>
                                    <td height="30" width="20%"><font color="red">*</font><spring:message code="trucks.label.VehicleType"  text="default text"/></td>
                                    <td  width="20%">
                                        <input type="hidden" name="typeId" id="typeId" value="<c:out value="${typeId}"/>" />
                                        <input type="hidden" name="axleTypeId" id="axleTypeId" value="<c:out value="${axleTypeId}"/>" />
                                        <select class="form-control" name="vehicleTypeId" id="vehicleTypeId" onChange="getModelAndTonnage(this.value);" style="width:260px;height:40px;" >
                                            <option value="0">---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
                                            <c:if test = "${TypeList != null}" >
                                                <c:forEach items="${TypeList}" var="Type">
                                                    <option value='<c:out value="${Type.typeId}" />~<c:out value="${Type.axleTypeId}" />'><c:out value="${Type.typeName}" /></option>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                        <c:if test="${vehicleId != null}">
                                            <script>
                                                document.getElementById("vehicleTypeId").value = '<c:out value="${typeId}"/>~<c:out value="${axleTypeId}"/>';
                                                    getModelAndTonnage('<c:out value="${typeId}"/>~<c:out value="${axleTypeId}"/>');
                                            </script>
                                        </c:if>
                                    </td>

                                    <td height="30" width="20%"><font color="red">*</font><spring:message code="trucks.label.Model"  text="default text"/> </td>
                                    <td height="30">
                                        <select class="form-control" name="modelId" id="modelId" style="width:260px;height:40px;" >
                                            <option value="0">---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
                                        </select>
                                        <script>
                                            document.getElementById("modelId").value = '<c:out value="${modelId}"/>'
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="30"><font color="red">*</font><spring:message code="trucks.label.Branches"  text="Branches"/></td>
                                    <td   >
                                        <select class="form-control" name="opId" id="opId"  style="width:260px;height:40px;">
                                            <option value="0" selected>-select-</option>
                                            <c:if test = "${OperationPointList != null}" >
                                                <c:forEach items="${OperationPointList}" var="Dept">
                                                    <option value='<c:out value="${Dept.opId}" />'> <c:out value="${Dept.opName}" /> </option>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                        <script>
//                                                                                    alert('<c:out value="${opId}"/>')
//                                                                                    document.getElementById("opId").value = '<c:out value="${opId}"/>'
                                        </script>
                                    </td>
                                    <td height="30"><font color="red">*</font><spring:message code="trucks.label.ColdStorage"  text="default text"/></td>
                                    <td   ><select class="form-control" name="classId" id="classId" style="width:260px;height:40px;">
                                            <c:if test = "${ClassList != null}" >
                                                <c:forEach items="${ClassList}" var="Dept">
                                                    <c:if test = "${Dept.classId == '1012'}" >
                                                    <option value="<c:out value="${Dept.classId}" />"><c:out value="${Dept.className}" /></option>
                                                    </c:if>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                        <script>
                                            document.getElementById("classId").value = "1012";
                                        </script>
                                    </td>
                                </tr>
                            </table>
                            </td></tr>

                            <tr>
                                <td colspan="6">


                                    <div id="hiredGroup" >
                                        <table class="table table-info mb30 table-hover">
                                            <tr  width="20%">
                                                <td height="30" width="20%"><font color="red">*</font><spring:message code="trucks.label.RegistrationDate"  text="default text"/></td>

                                                <td  height="30" width="20%"><input name="dateOfSale" type="text" class="datepicker" value="<c:out value="${dateOfSale}" />" style="width:260px;height:40px;" >

                                                </td>
                                                <td height="30"><font color="red">*</font><spring:message code="trucks.label.Tonnage(MT)"  text="default text"/></td>
                                                <td height="30"><input name="seatCapacity" id="seatCapacity" type="text" class="form-control" value="<c:out value="${seatCapacity}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="15" value="0" style="width:260px;height:40px;"></td>

                                                <!--                    <td></td>-->


                                            </tr>

                                            <tr width="20%">
                                                <td height="30" ><font color="red">*</font><spring:message code="trucks.label.EngineNo"  text="default text"/></td>
                                                <td  height="30"><input maxlength='20' name="engineNo" type="text" class="form-control" value="<c:out value="${engineNo}"/>" style="width:260px;height:40px;"></td>
                                                <td height="30" width="20%"><font color="red">*</font><spring:message code="trucks.label.ChassisNo"  text="default text"/></td>
                                                <td  height="30" width="20%"><input maxlength='20'  type="text" name="chassisNo"  class="form-control" value="<c:out value="${chassisNo}"/>" style="width:260px;height:40px;"> </td>
                                            </tr>
                                            <tr style="display: none">
                                                <td height="30" style="display: none"><font color="red">*</font><spring:message code="trucks.label.NoOfAxles"  text="default text"/></td>
                                                <td height="30" style="display: none"><select name="axles" class="form-control" style="width:260px;height:40px;">
                                                        <option value="2"><spring:message code="trucks.label.DoubleAxle"  text="default text"/></option>
                                                        <option value="3"><spring:message code="trucks.label.TripleAxle"  text="default text"/></option>
                                                        <option value="3"><spring:message code="trucks.label.FourAxle"  text="default text"/></option>
                                                        <option value="3"><spring:message code="trucks.label.FiveAxle"  text="default text"/></option>
                                                        <option value="3"><spring:message code="trucks.label.SixAxle"  text="default text"/></option>
                                                        <option value="3"><spring:message code="trucks.label.MoreThan6Axle"  text="default text"/></option>
                                                    </select></td>
                                                <td  colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td height="30"><font color="red">*</font><spring:message code="trucks.label.WarrantyDate"  text="default text"/></td>
                                                <td height="30"><input name="warrantyDate" id="warrantyDate" type="text" class="datepicker" value="<c:out value="${warrantyDate}"/>"  onchange="CalWarrantyDate(this.value)" style="width:260px;height:40px;" ></td>
                                                <td height="30"><spring:message code="trucks.label.Warranty(days)"  text="default text"/></td>
                                                <td height="30"><input name="war_period" id="war_period" type="text"  class="form-control" value="<c:out value="${warPeriod}"/>"   onKeyPress="return onKeyPressBlockCharacters(event);" style="width:260px;height:40px;"></td>
                                            </tr>
                                            <tr style="display:none;" >
                                                <td height="30"><spring:message code="trucks.label.VehicleCostAsOnDate"  text="default text"/></td>
                                                <td   ><input type="text" name="vehicleCost" class="form-control" maxlength="15" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" onkeyup="setVehicleCost(this.value);" style="width:260px;height:40px;"/></td>
                                                <td height="30"><spring:message code="trucks.label.Depreciation"  text="default text"/>(%)</td>
                                                <td   ><input type="text" name="vehicleDepreciation" id="vehicleDepreciation" class="form-control" maxlength="15" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="width:260px;height:40px;"/></td>
                                            </tr>
                                            <script>
                                                function setVehicleCost(value) {
                                                    $("#vehicleDepreciationCost").val(value);
                                                }
                                            </script>
                                            <tr>

                                                <td height="30"> <font color="red">*</font><spring:message code="trucks.label.KMReading"  text="default text"/></td>
                                                <td   ><input maxlength='20'  name="kmReading" type="text" class="form-control" value="<c:out value="${kmReading}"/>"  onKeyPress="return onKeyPressBlockCharacters(event);" style="width:260px;height:40px;"></td>
                                                <td  colspan="2">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td height="30"><font color="red">*</font><spring:message code="trucks.label.DailyRunKM"  text="default text"/></td>
                                                <td height="30"> <input  type="text"  id="dailyKm"  name="dailyKm" class="form-control" maxlength="10" value="<c:out value="${dailyKm}"/>"   onKeyPress="return onKeyPressBlockCharacters(event);" style="width:260px;height:40px;"> </td>
                                                <td height="30"> <font color="red">*</font><spring:message code="trucks.label.DailyRunHM"  text="default text"/></td>
                                                <td   > <input type="text" id="dailyHm" name="dailyHm"  class="form-control" value="<c:out value="${dailyHm}"/>" maxlength="10"  onKeyPress="return onKeyPressBlockCharacters(event);" style="width:260px;height:40px;"></td>
                                            </tr>
                                            <tr>
                                                <td height="30"> <font color="red">*</font><spring:message code="trucks.label.VehicleColor"  text="default text"/></td>
                                                <td ><input type="text"  name="vehicleColor" id="vehicleColor" class="form-control" maxlength="20" value="<c:out value="${vehicleColor}"/>" onKeyPress="return onKeyPressBlockNumbers(event);" style="width:260px;height:40px;"></td>
                                                    <%--
                                                    <td><font color="red">*</font>Vehicle Usage</td>
                                                    <td   ><select class="form-control" name="usageId"  >
                                                            <option value="1" >Short Trip</option>
                                                            <option value="2" selected>Long Trip</option>
                                                        </select></td>
                                                    --%>
                                            <input type="hidden" name="usageId" value="2"/>
                                            <td height="30"> <font color="red">*</font><spring:message code="trucks.label.GPSTrackingSystem"  text="default text"/></td>
                                            <td   >
                                                <select class="form-control" name="gpsSystem" id="gpsSystem" onchange="openGpsInfo(this.value)"style="width:260px;height:40px;" >
                                                    <option value='Yes' ><spring:message code="trucks.label.Yes"  text="default text"/></option>
                                                    <option value='No' selected><spring:message code="trucks.label.No"  text="default text"/></option>
                                                </select>
                                                <script type="text/javascript">
                                                    function openGpsInfo(val) {
                                                        if (val == 'Yes') {
                                                            document.getElementById("trckingId").style.display = "none";
                                                        } else {
                                                            document.getElementById("trckingId").style.display = "none";
                                                        }
                                                    }
                                                </script>
                                            </td>
                                            </tr>
                                            <tr>
                                                <td > &nbsp;<input type="hidden" name="groupId" value="0"/><spring:message code="trucks.label.Status"  text="default text"/></td>
                                                <td >
                                                    <select name="activeInd" id="activeInd" style="width:260px;height:40px;" class="form-control">
                                                        <option value="Y"><spring:message code="trucks.label.Active"  text="default text"/></option>
                                                        <option value="N"><spring:message code="trucks.label.InActive"  text="default text"/></option>
                                                    </select>
                                                </td>


                                                <td > <spring:message code="trucks.label.Remarks"  text="default text"/></td>
                                                <td  ><textArea id="description" name="description" cols="5" rows="5" style="width:260px;"  class="form-control"><c:out value="${description}"/></textArea></td>
                                                                        </tr>



                                <script type="text/javascript">


                                    function ChangeDropdowns(value) {
                                        //alert(value);
                                        if (value == "1") {
                                            document.getElementById("attachInfo").style.display = "none";
                                        }

                                        if (value == "2" || value == "3" || value == "4") {

                                            document.getElementById("attachInfo").style.display = "block";
                                        }
                                        //
                                        if (value == "1") {
                                            document.getElementById("hiredGroup").style.display = "block";
                                            //                         document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "2" || value == "3" || value == "4") {
                                            document.getElementById("hiredGroup").style.display = "none";
                                        }
                                        if (value == "1") {
                                            document.getElementById("hiredGroup").style.display = "block";
                                            //                         document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "3" || value == "4" || value == "2") {
                                            document.getElementById("hiredGroup").style.display = "none";
                                        }
                                        if (value == "2" || value == "3" || value == "4") {

                                            document.getElementById("attachInfo").style.display = "block";
                                        }

                                        //    function hideAdd(value){

                                        if (value == "3" || value == "4" || value == "2") {
                                            document.getElementById("add").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "1") {
                                            document.getElementById("add").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }

                                        //    function hideAdds(value){


                                        if (value == "1" || value == "2") {
                                            document.getElementById("adds").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "3" || value == "4") {
                                            document.getElementById("adds").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }

                                        //       function hideAdds1(value){


                                        if (value == "1" || value == "3" || value == "4") {
                                            document.getElementById("adds1").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "2") {
                                            document.getElementById("adds1").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }


                                        if (value == "2") {
                                            document.getElementById("groupNo").style.display = "block";

                                        }
                                        if (value == "1") {
                                            document.getElementById("groupNo").style.display = "block";
                                        }

                                        if (value == "4") {
                                            ocument.getElementById("groupNo").style.display = "block";
                                        }

                                        if (value == "3") {
                                            document.getElementById("groupNo").style.display = "block";
                                        }

                                        $('#vandornameId').show();

                                        if (value != "1") {
                                            document.getElementById("bankPayment1").style.display = "block";
                                            document.getElementById("bankPayment2").style.display = "block";
                                            document.getElementById("bankPayment3").style.display = "block";
                                            document.getElementById("bankPayment4").style.display = "block";
                                        }
                                        else {
                                            document.getElementById("bankPayment1").style.display = "none";
                                            document.getElementById("bankPayment2").style.display = "none";
                                            document.getElementById("bankPayment3").style.display = "none";
                                            document.getElementById("bankPayment4").style.display = "none";
                                        }

                                    }

                                    function hideAdd(value) {

                                        if (value == "3" || value == "4" || value == "2") {
                                            document.getElementById("add").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "1") {
                                            document.getElementById("add").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                    }
                                    function hideAdds(value) {


                                        if (value == "1" || value == "2") {
                                            document.getElementById("adds").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "3" || value == "4") {
                                            document.getElementById("adds").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                    }
                                    function hideAdds1(value) {


                                        if (value == "1" || value == "3" || value == "4") {
                                            document.getElementById("adds1").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "2") {
                                            document.getElementById("adds1").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                    }


                                                                                           </script>
</table></div>
</td>
</tr>
<tr>


                                <td colspan="6">
                                    <div id="trckingId" style="display: none;">
                                        <table width="50%" cellpadding="0" cellspacing="2">
                                            <tr>
                                                <td height="30"><font color="red">*</font><spring:message code="trucks.label.GPSSystemId"  text="default text"/></td>
                                                <td  align="center">
                                                    <select class="form-control" name="gpsSystemId"  >
                                                        <option value="1" selected>0001</option>
                                                        <option value="2" >0002</option>
                                                        <option value="3" >0003</option>
                                                    </select>
                                                </td>
                                                <td height="30">&nbsp;</td>
                                                <td >&nbsp;</td>

                                            </tr>
                                        </table>
                        </div> </td>
</tr>
</table>
                    <br>
                    <table  border="0" class="border" align="center" width="300" cellpadding="0" cellspacing="0" id="bg">
                        <tr id="buttonHide">
                            <td>
                                <input type="button" class="btn btn-success" value="<spring:message code="trucks.label.update"  text="default text"/>" id="vehicleDetailUpdateButton" onclick="vehicleValidation()" style="display: none;width:100px;height:40px;"/>&nbsp;                            
                                <input type="button" class="btn btn-success" value="<spring:message code="trucks.label.Save"  text="default text"/>" id="vehicleDetailSaveButton" onclick="buttonHide();vehicleValidation()" style="display: none;width:100px;height:40px;"/>&nbsp;
                            </td>
                            <td>
                                <input type="button" class="btn btn-success btnNext" value="<spring:message code="hrms.label.NEXT" text="default text"/>" name="Next" style="width:100px;height:40px;"/>                                
                            </td>
                        </tr>
                    </table>

<script type="text/javascript">

    function buttonHide() {
//        alert("Helo")
        $("#buttonHide").hide();
    }
</script>
                
            </div>
            <div id="vehicleOem" style="display: none">
<!--                <div align="center" style="font-family:Arial, Helvetica, sans-serif; color:#f5533d; font-size:12px; font-weight:bold;" id="userNameStatus">&nbsp;&nbsp;</div>-->
                <table class="table table-info mb30 table-hover" id="positionViewTable">
                <thead>
                    <tr>
                        <th><spring:message code="trucks.label.AxleType"  text="default text"/></th>
                        <th><spring:message code="trucks.label.Left"  text="default text"/></th>
                        <th><spring:message code="trucks.label.Right"  text="default text"/></th>
                        <input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>
                    </tr>
                    </thead>
                </table>
                  <script type="text/javascript">
                      function getVehAxleName(str) {
                          var axleTypeId = str.split("~")[1];
                          var typeId = axleTypeId;
                          $("#axleTypeId").val(axleTypeId);
                          //                        var typeId=document.getElementById("typeId").value;

                          var axleTypeName = "";
                          var leftSideTyreCount = 0;
                          var rightSideTyreCount = 0;
                          var axleDetailId = 0;
                          var count = 1;

                          $.ajax({
                              url: '/throttle/getVehAxleName.do',
                              // alert(url);
                              data: {typeId: typeId},
                              dataType: 'json',
                              success: function(data) {
                                  if (data !== '') {
                                      $.each(data, function(i, data) {
                                          axleTypeName = data.AxleTypeName;
                                          leftSideTyreCount = data.LeftSideTyreCount;
                                          rightSideTyreCount = data.RightSideTyreCount;
                                          axleDetailId = data.AxleDetailId;
                                          positionAddRow(axleTypeName, leftSideTyreCount, rightSideTyreCount, count, axleDetailId);
                                          count++;
                                      });
                                  }
                              }
                          });
                      }


                      function positionAddRow(axleTypeName, leftSideTyreCount, rightSideTyreCount, rowCount, axleDetailId) {
                          var rowCount = "";
                          var style = "";

                          rowCount = document.getElementById('selectedRowCount').value;
                          rowCount++;
                          var tab = document.getElementById("positionViewTable");
                          var newrow = tab.insertRow(rowCount);
                          newrow.id = 'rowId' + rowCount;

                          cell = newrow.insertCell(0);
                          var cell2 = "<td  height='30'><input type='hidden' name='vehicleAxleDetailId' id='vehicleAxleDetailId" + rowCount + "' value='" + axleDetailId + "'  class='form-control' /><input type='hidden' name='vehicleFrontAxle' id='vehicleFrontAxle" + rowCount + "' value='" + axleTypeName + "'  class='form-control' /><label name='vehicleFronts' id='vehicleFronts" + rowCount + "'>" + axleTypeName + "</label></td>";
                          //                        var cell2 = "<td  height='30'><input type='text' name='vehicleFrontAxle' id='vehicleFrontAxle" + rowCount + "' value='"+ axleTypeName +"'  class='form-control' /><label name='vehicleFronts' id='vehicleFronts" + rowCount + "'>" + axleTypeName + "</label></td>";
                          cell.setAttribute("className", style);
                          cell.innerHTML = cell2;

                          cell = newrow.insertCell(1);
                          var cell3 = "<td height='30'  name='leftTyreIds'  id='leftTyreIds2' colspan=" + leftSideTyreCount + "><b>";
                          for (var i = 0; i < leftSideTyreCount; i++) {
                              cell3 += parseInt(i + 1) + "</b>:Tyre No:<input type='text'  style='width:175px;' id='tyreNoLeft" + axleDetailId + parseInt(i + 1) + "'/>Depth<input type='text' maxlength='3'  name='treadDepthLeft' id='treadDepthLeft" + axleDetailId + parseInt(i + 1) + "' style='width:55px;' onkeypress='return blockNonNumbers(this, event, true, false);'/>mm <select name='leftItemIds' class='form-control' id='tyreMfrLeft" + axleDetailId + parseInt(i + 1) + "'  onchange='updateVehicleTyreDetails(" + axleDetailId + ",0," + i + ",this.value,1);'><option value='0'>--Tyre Make & Spec--</option><c:if test = "${tyreItemList != null}" ><c:forEach items="${tyreItemList}" var="Type"><option value='<c:out value="${Type.itemId}" />'><c:out value="${Type.itemName}" /></option></c:forEach > </c:if></select><br>";
                          }
                          cell.setAttribute("className", style);
                          cell.innerHTML = cell3 + "</td>";
                          for (var i = 0; i < leftSideTyreCount; i++) {
                              fillVehicleTyreDetails(axleDetailId, 0, i);
                          }

                          cell = newrow.insertCell(2);
                          var cell4 = "<td  height='30' name='rightTyreIds' id='rightTyreIds2' colspan=" + rightSideTyreCount + "> <b>";
                          for (var j = 0; j < rightSideTyreCount; j++) {
                              cell4 += parseInt(j + 1) + "</b>:Tyre No:<input type='text'  style='width:175px;' id='tyreNoRight" + axleDetailId + parseInt(j + 1) + "' />Depth<input type='text' maxlength='3' name='treadDepthRight' id='treadDepthRight" + axleDetailId + parseInt(j + 1) + "'   style='width:55px;' onkeypress='return blockNonNumbers(this, event, true, false);'/>mm<select name='rightItemIds' id='tyreMfrRight" + axleDetailId + parseInt(j + 1) + "' class='form-control' onchange='updateVehicleTyreDetails(" + axleDetailId + ",1," + j + ",this.value,1);'><option value='0'>--Tyre Make & Spec--</option><c:if test = "${tyreItemList != null}" ><c:forEach items="${tyreItemList}" var="Type"><option value='<c:out value="${Type.itemId}" />'><c:out value="${Type.itemName}" /></option></c:forEach > </c:if></select><br>";
                          }
                          cell.setAttribute("className", style);
                          cell.innerHTML = cell4 + "</td>";
                          for (var j = 0; j < rightSideTyreCount; j++) {
                              fillVehicleTyreDetails(axleDetailId, 1, j);
                          }


                          cell = newrow.insertCell(3);
                          var cell5 = "<td  height='30'><input type='hidden' name='position' id='position'  value='" + parseInt(i) + "' ></td>";
                          cell.setAttribute("className", style);
                          cell.innerHTML = cell5;

                          document.getElementById('selectedRowCount').value = rowCount;


                      }

                      function fillVehicleTyreDetails(axleDetailId, positionName, positionNo) {
                          var vehicleId = $("#vehicleId").val();
                          var axleTypeId = $("#axleTypeId").val();
                          if (vehicleId != 0) {
                              positionNo = parseInt(positionNo + 1);
                              if (positionName == '0') {
                                  positionName = 'Left';
                              } else {
                                  positionName = 'Right';
                              }
                              $.ajax({
                                  url: '/throttle/getVehAxleTyreNo.do',
                                  // alert(url);
                                  data: {axleDetailId: axleDetailId, positionName: positionName,
                                      positionNo: positionNo, vehicleId: vehicleId, axleTypeId: axleTypeId},
                                  dataType: 'json',
                                  success: function(data) {
                                      if (data !== '') {
                                          $.each(data, function(i, data) {
                                              //                                                axleTypeName = data.AxleTypeName;
                                              document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value = data.TyreNo;
                                              document.getElementById("treadDepth" + positionName + axleDetailId + positionNo).value = data.TyreDepth;
                                              document.getElementById("tyreMfr" + positionName + axleDetailId + positionNo).value = data.TyreMfr;
                                          });
                                      }
                                  }
                              });
                          }
                          //                            alert(document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value);
                          //                            document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value = 100;
                      }
                      function updateVehicleTyreDetails(axleDetailId, positionName, positionNo, val, updateType) {
                          var vehicleId = $("#vehicleId").val();
                          var kmReading = document.addVehicle.kmReading.value;
                          positionNo = parseInt(positionNo + 1);
                          var url = '';
                          if (positionName == '0') {
                              positionName = 'Left';
                          } else {
                              positionName = 'Right';
                          }
                          if (updateType == 1) {

                          }
                          var treadDepth = document.getElementById("treadDepth" + positionName + axleDetailId + positionNo).value;
                          var tyreNo = document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value;
                          if (treadDepth == '') {
                              alert("Please fill tread depth for " + positionName + " " + positionNo);
                              document.getElementById("treadDepth" + positionName + axleDetailId + positionNo).focus();
                              document.getElementById("tyreMfr" + positionName + axleDetailId + positionNo).value = 0;
                              return;
                          } else if (tyreNo == '') {
                              alert("Please fill tyre no for " + positionName + " " + positionNo);
                              document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).focus();
                              document.getElementById("tyreMfr" + positionName + axleDetailId + positionNo).value = 0;
                              return;
                          } else {
                              url = './updateVehicleTyreNo.do';
                              $.ajax({
                                  url: url,
                                  data: {
                                      axleDetailId: axleDetailId, positionName: positionName, positionNo: positionNo,
                                      updateValue: val, depthVal: treadDepth, updateType: updateType, vehicleId: vehicleId,
                                      tyreNo: tyreNo, kmReading: kmReading
                                  },
                                  type: "GET",
                                  success: function(response) {
                                      if (response.toString().trim() == 0) {
                                          document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value = '';
                                          document.getElementById("treadDepth" + positionName + axleDetailId + positionNo).value = '';
                                          document.getElementById("tyreMfr" + positionName + axleDetailId + positionNo).value = 0;
                                          alert("Tyre No Already Mapped Please Enter Valid Tyre No");
                                          $("#StatusMsg").text("Vehicle TyreNo failed to add").css("color", "red");
                                          document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).focus();
                                      } else {
                                          alert("Vehicle TyreNo Added Successfully");
                                          $("#StatusMsg").text("Vehicle TyreNo added sucessfully").css("color", "green");
                                      }
                                  },
                                  error: function(xhr, status, error) {
                                  }
                              });
                          }

                      }

                      //jp End Ajex insCompny list
                                                                                      </script>


                   <table class="table table-info mb30 table-hover"  id="oEMtable">
                    <thead>
                    <tr>
                        <th  align="center" height="30" ><spring:message code="trucks.label.SNo"  text="default text"/></th>
                        <th   align="center" height="30"><spring:message code="trucks.label.Stepney/Battery"  text="default text"/></th>
                        <th   align="center" height="30"><spring:message code="trucks.label.Details"  text="default text"/></th>
                    </tr>
                    </thead>

                 <input type="hidden" name="oemIdCount" id="oemIdCount" value="<c:out value="${oemListSize}"/>" />
                                  <td><input type="button" name="AddRow" id="AddRow" value="<spring:message code="trucks.label.AddRow"  text="default text"/>" onclick="addOem(0, 1, '', '', '', 0, 0, 0);" class="btn btn-success" style="width:100px;height:35px;"/></td>
                <input type="hidden" name="rowCounted" id="rowCounted" value=""/>
               <script>
                   function vehicleStepneyDetails(val, sno) {

                       if (val == 1) {

                           $("#oemMfr" + sno).show();
                           $("#oemDepth" + sno).show();
                           $("#oemTyreNo" + sno).show();
                           $("#oemBattery" + sno).hide();
                           //
                       } else if (val == 2) {
                           $("#oemBattery" + sno).show();
                           $("#oemMfr" + sno).hide();
                           $("#oemDepth" + sno).hide();
                           $("#oemTyreNo" + sno).hide();

                       }
                   }

                                                                                                 </script>
               <script>

                   function insertVehicleAxle() {

                       var vehicleId = $("#vehicleId").val();
                       var url = '';
                       var oemId = [];
                       var oemDetailsId = [];
                       var oemMfr = [];
                       var oemDepth = [];
                       var oemTyreNo = [];
                       var oemBattery = [];
                       var oemDetailsIds = document.getElementsByName("oemDetailsId");
                       var oemMfrs = document.getElementsByName("oemMfr");
                       var oemDepths = document.getElementsByName("oemDepth");
                       var oemTyreNos = document.getElementsByName("oemTyreNo");
                       var oemBatterys = document.getElementsByName("oemBattery");
                       var oemIds = document.getElementsByName("oemId");
                       for (var i = 0; i < oemDetailsIds.length; i++) {
                           oemDetailsId.push(oemDetailsIds[i].value);
                           oemMfr.push(oemMfrs[i].value);
                           oemTyreNo.push(oemTyreNos[i].value);
                           oemDepth.push(oemDepths[i].value);
                           oemBattery.push(oemBatterys[i].value);
                           oemId.push(oemIds[i].value);
                           if (oemDetailsIds[i].value == 1) {
                               if (oemMfrs[i].value == '') {
                                   alert('Please select MFR ');
                                   return;
                               } else if (oemTyreNos[i].value == '') {
                                   alert('Please Enter The TyreNo ');
                                   return;
                               } else if (oemDepths[i].value == '') {
                                   alert('Please Enter The Depth ');
                                   return;
                               }
                           } else if (oemDetailsIds[i].value == 2) {
                               if (oemBattery[i].value == '') {
                                   alert('Please select battty ');
                                   return;
                               }
                           }

                       }
                       var insertStatus = 0;
                       var oemIdResponse = "";
                       url = './insertOemDetails.do';
                       $("#oEmNext").hide();
                       $.ajax({
                           url: url,
                           data: {oemDetailsIdVal: oemDetailsId, vehicleId: vehicleId, oemMfrVal: oemMfr, oemTyreNoVal: oemTyreNo, oemDepthVal: oemDepth,
                               oemBatteryVal: oemBattery, oemIdVal: oemId
                           },
                           type: "GET",
                           dataType: 'json',
                           success: function(data) {
                               var r = 0;
                               $.each(data, function(i, data) {
                                   //                                alert(data.OemId);
                                   oemIds[r].value = data.OemId;
                                   r++;
                               });
                               $("#StatusMsg").text("Vehicle VehicleAxle added sucessfully").css("color", "green");
                               alert("Vehicle VehicleAxle added sucessfully");
                               $("#oEmNext").show();
                           },
                           error: function(xhr, status, error) {
                           }
                       });
                       //                        $("#oEmNext").hide();
                   }

                   function addOem(sno, oemDetailsId, batteryNo, tyreNo, depth, mfrId, oemId, addType) {
                       var rowCount = sno;
                       var style = "";
                       rowCount = document.getElementById('rowCounted').value;
                       rowCount++;


                       var tab = document.getElementById("oEMtable");
                       var newrow = tab.insertRow(rowCount);
                       newrow.id = 'rowId' + rowCount;

                       cell = newrow.insertCell(0);
                       var cell0 = "<td  height='25' ><input type='hidden' name='serNO' id='serNO" + rowCount + "' value='" + rowCount + "'  class='form-control' />" + rowCount + "</td>";
                       cell.setAttribute("className", style);
                       cell.innerHTML = cell0;

                       cell = newrow.insertCell(1);
                       var cell1 = "";
                       var oemDetailsName = "";
                       if (oemDetailsId == 1) {
                           oemDetailsName = "Stepney";
                       } else {
                           oemDetailsName = "Battery";
                       }
                       if (addType == 1) {
                           cell1 = "<td  height='30'><input type='hidden' id='oemDetailsId" + rowCount + "' style='width:125px'  name='oemDetailsId' value='" + oemDetailsId + "'>'" + oemDetailsName + "'</td>";
                       } else {
                           cell1 = "<td  height='30'><select class='form-control' id='oemDetailsId" + rowCount + "' style='width:125px'  name='oemDetailsId' value='" + oemDetailsId + "' onchange='batteryTextHideShow(this.value," + rowCount + ");'><option value='1'>Stepney</option><option value='2'>battery</option></select></td>";
                       }
                       cell.setAttribute("className", style);
                       cell.innerHTML = cell1;

                       cell = newrow.insertCell(2);
                       var cell2 = "<td  height='30'  ><span id='batterySpan" + rowCount + "' style='display:none'>Bat:<input type='text'  name='oemBattery' id='oemBattery" + rowCount + "' maxlength='13' placeholder='BatteryNo'  size='20' class='form-control' value='" + batteryNo + "'  /></span><span id='tyreSpan" + rowCount + "'>Tyre Make & Spec:<select name='oemMfr' id='oemMfr" + rowCount + "' class='form-control' style='width:124px'  ><option value=0>--Select--</option><c:if test = "${tyreItemList != null}" ><c:forEach items="${tyreItemList}" var="Type"><option value='<c:out value="${Type.itemId}" />'><c:out value="${Type.itemName}" /></option></c:forEach > </c:if></select>\n\
                                                                                                                       TyreNo:<input type='text'  name='oemTyreNo' id='oemTyreNo" + rowCount + "' placeholder='TyreNo'   size='20' class='form-control' value='" + tyreNo + "'  />Depth:<input type='text'  name='oemDepth' id='oemDepth" + rowCount + "' maxlength='13'   size='20' class='form-control' placeholder='TyreDepth' value='" + depth + "' />mm</span></td>";
                       cell.setAttribute("className", style);
                       cell.innerHTML = cell2;
                       $("#oemMfr" + rowCount).val(mfrId);
                       if (addType == 1) {
                           if (oemDetailsId == 2) {
                               $("#batterySpan" + rowCount).show();
                               $("#tyreSpan" + rowCount).hide();
                           }
                       }
                       cell = newrow.insertCell(3);
                       var cell3 = "<td  height='25' ><input type='hidden' name='oemId' id='oemId" + rowCount + "' value='" + oemId + "'  class='form-control' /></td>";
                       cell.setAttribute("className", style);
                       cell.innerHTML = cell3;
                       document.getElementById('rowCounted').value = rowCount;
                   }

                   function batteryTextHideShow(value, rowCount) {
                       if (value == 1) {
                           $("#batterySpan" + rowCount).hide();
                           $("#tyreSpan" + rowCount).show();
                       } else {
                           $("#batterySpan" + rowCount).show();
                           $("#tyreSpan" + rowCount).hide();
                       }
                   }

                                     </script>
                                <%int addRowCount = 1;%>
                                <c:if test="${oemList != null}">
                                    <c:forEach items="${oemList}" var="item">
                        <script>
                            addOem('<%=addRowCount%>', '<c:out value="${item.oemDetailsId}"/>', '<c:out value="${item.oemBattery}"/>', '<c:out value="${item.oemTyreNo}"/>', '<c:out value="${item.oemDepth}"/>', '<c:out value="${item.oemMfr}"/>', '<c:out value="${item.oemId}"/>', 1);
                        </script>
                                    </c:forEach>
                                </c:if>

                </table>


                <br>
                <center>
                    <input type="button" class="btn btn-success" value="<spring:message code="trucks.label.Save"  text="default text"/>" name="Save" id="oEmNext"  onClick="insertVehicleAxle(this.name)" style="display: none;width:100px;height:40px;" />
                    &emsp;
                    <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="hrms.label.NEXT" text="default text"/>" name="Next" style="width:100px;height:40px;"/></a>
                    <a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="hrms.label.Prev" text="default text"/>" name="Prev" style="width:100px;height:40px;"/></a>
                </center>


            </div>



                <div id="vehicleDepreciationTab" style="display: none">
                <table class="table table-info mb30 table-hover" >
                    <tr>
                        <td ><spring:message code="trucks.label.VehicleCost"  text="default text"/></td>
                        <td ><input type="text" name="vehicleDepreciationCost" id="vehicleDepreciationCost" class="form-control" value='<c:out value="${vehicleCost}"/>' style="width:250px;"></td>

                        <td ><spring:message code="trucks.label.DepreciationYear"  text="default text"/></td>
                        <td >

                            <select name="depreciationType" id="depreciationType" class="form-control"  style="width:250px;" onChange="removeRowDetails();">
                                <option value="0"><spring:message code="trucks.label.Select"  text="default text"/></option>
                                <option value="1">1<spring:message code="trucks.label.Year"  text="default text"/></option>
                                <option value="2">2<spring:message code="trucks.label.Year"  text="default text"/></option>
                                <option value="3">3<spring:message code="trucks.label.Year"  text="default text"/></option>
                                <option value="4">4<spring:message code="trucks.label.Year"  text="default text"/></option>
                                <option value="5">5<spring:message code="trucks.label.Year"  text="default text"/></option>
                            </select>
                        </td>
                        <script>
                            document.getElementById("depreciationType").value = '<c:out value="${depreciationSize}"/>';
                        </script>
                    </tr>

                </table>
              <table class="table table-info" id="vehicleCostTable">
                     <thead>
                        <th  align="center" height="30" ><spring:message code="trucks.label.SNo"  text="default text"/></th>
                        <th   align="center" height="30"><spring:message code="trucks.label.Year"  text="default text"/></th>
                        <th  height="30" ><spring:message code="trucks.label.Depreciation"  text="default text"/>(%)</th>
                        <th  height="30" ><spring:message code="trucks.label.AnnualDepreciationCost"  text="default text"/></th>
                        <th  height="30" ><spring:message code="trucks.label.MonthlyDepreciationCost"  text="default text"/></th>
                        </thead>
                                <%int rcount = 1;%>
                                <c:if test="${depreciationList != null}">
                                    <c:forEach items="${depreciationList}" var="dep">
                        <tr>
                            <td  height='25' ><input type='hidden' name='serNO' id='serNO<%=rcount%>' value='<%=rcount%>'  class='form-control' /><%=rcount%></td>
                            <td  height='30'>
                                <input type='hidden' name='vehicleYear' id='vehicleYear<%=rcount%>' value='<%=rcount%>'  class='form-control' />
                                <label name='vehicleYears' id='vehicleYears<%=rcount%>'><%=rcount%>-<spring:message code="trucks.label.Year"  text="default text"/></label>
                            </td>
                            <td height='30' >
                                <input type='text' value='<c:out value="${dep.depreciation}"/>' name='depreciation' id='depreciation<%=rcount%>' maxlength='13'   size='20' class='form-control'  onchange="vehicleCostCalculation('<%=rcount%>');" />
                            </td>
                            <td  height='30'><input type='hidden' value='<c:out value="${dep.yearCost}"/>' name='yearCost' id='yearCost<%=rcount%>' maxlength='13'   size='20' class='form-control'    /><span id='yearCostSpan<%=rcount%>'><c:out value="${dep.yearCost}"/></span></td>
                            <td height='30' ><input type='hidden' value='<c:out value="${dep.perMonth}"/>' name='perMonth' id='perMonth<%=rcount%>' maxlength='13'   size='20' class='form-control' onchange='perMonthCalculation(<%=rcount%>);'  /><span id='perMonthSpan<%=rcount%>'><c:out value="${dep.perMonth}"/></span></td>
                        </tr>
                                        <%rcount++;%>
                                    </c:forEach>
                                </c:if>


                </table>
                <br>
                <br>
                <center>
                    <input type="button" class="btn btn-success" name="Save" id="depreciationSave" value="<spring:message code="trucks.label.Save"  text="default text"/>" onclick="saveVehicleDepreciation(this.name);" style="display: none;width:100px;height:40px;"/>&nbsp;
                    <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="hrms.label.NEXT" text="default text"/>" name="Next" style="width:100px;height:40px;"/></a>
                    <a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="hrms.label.Prev" text="default text"/>" name="Prev" style="width:100px;height:40px;"/></a>
                </center>
                <script type="text/javascript">
                    function saveVehicleDepreciation() {
                        var depreciationType = document.getElementById("depreciationType").value;
                        var vehicleDepreciationCost = document.getElementById("vehicleDepreciationCost").value;
                        var vehicleYear = document.getElementsByName("vehicleYear");
                        var depreciation = document.getElementsByName("depreciation");
                        var yearCost = document.getElementsByName("yearCost");
                        var perMonth = document.getElementsByName("perMonth");
                        var status = 0;
                        for (var i = 1; i <= depreciationType; i++) {
                            if ($("#depreciation" + i).val() == '') {
                                var vehicleYear = $("#vehicleYear" + i).val();
                                alert("Please enter the depreciation for Year " + vehicleYear);
                                $("#depreciation" + i).focus();
                                return;
                            } else {
                                if (i == 1) {
                                    vehicleYear = $("#vehicleYear" + i).val();
                                    depreciation = $("#depreciation" + i).val();
                                    yearCost = $("#yearCost" + i).val();
                                    perMonth = $("#perMonth" + i).val();
                                } else {
                                    vehicleYear = vehicleYear + "~" + $("#vehicleYear" + i).val();
                                    depreciation = depreciation + "~" + $("#depreciation" + i).val();
                                    yearCost = yearCost + "~" + $("#yearCost" + i).val();
                                    perMonth = perMonth + "~" + $("#perMonth" + i).val();
                                }
                                status = 1;
                            }
                            //alert("i == " + i);
                        }
                        if (status == 1) {
                            var responseStatus = 0;
                            var vehicleId = $("#vehicleId").val();
                            var url = '';
                            url = './saveVehicleDepreciation.do';
                            $.ajax({
                                url: url,
                                data: {vehicleYear: vehicleYear, depreciation: depreciation, yearCost: yearCost, perMonth: perMonth, vehicleId: vehicleId, vehicleDepreciationCost: vehicleDepreciationCost, depreciationType: depreciationType
                                },
                                type: "GET",
                                success: function(response) {
                                    responseStatus = response.toString().trim();
                                    if (responseStatus == 0) {
                                        $("#StatusMsg").text("Vehicle Depreciation added failed ").css("color", "red");
                                    } else {
                                        $("#StatusMsg").text("Vehicle Depreciation added sucessfully ").css("color", "green");
                                        alert("Vehicle Depreciation added sucessfully");
                                        $("#depreciationSave").hide();
                                    }

                                },
                                error: function(xhr, status, error) {
                                }
                            });
                        }

                    }
                    function removeRowDetails() {
                        var tab = document.getElementById("vehicleCostTable");
                        //find current no of rows
                        var rowCountLength = document.getElementById('vehicleCostTable').rows.length;
                        rowCountLength = parseInt(rowCountLength);
                        for (var s = rowCountLength; s > 1; s--) {
                            $('#vehicleCostTable tr:last-child').remove();
                        }
                        vehicleCostAddRow();
                    }

                    function vehicleCostAddRow() {
                        var rowCount = "";
                        var style = "text2";
                        var axleCount = document.getElementById('depreciationType').value;
                        for (var i = 0; i < parseInt(axleCount); i++) {

                            var tab = document.getElementById("vehicleCostTable");
                            var rowCount = document.getElementById("vehicleCostTable").rows.length;
                            var newrow = tab.insertRow(rowCount);
                            newrow.id = 'rowId' + rowCount;

                            var cell = newrow.insertCell(0);
                            var cell1 = "<td  height='25' ><input type='hidden' name='serNO' id='serNO" + rowCount + "' value='" + rowCount + "'  class='form-control' />" + rowCount + "</td>";
                            cell.setAttribute("className", style);
                            cell.innerHTML = cell1;

                            cell = newrow.insertCell(1);
                            var cell2 = "<td  height='30'><input type='hidden' name='vehicleYear' id='vehicleYear" + rowCount + "' value='" + rowCount + "'  class='form-control' /><label name='vehicleYears' id='vehicleYears" + rowCount + "'>" + (rowCount) + "-Year</label></td>";
                            cell.setAttribute("className", style);
                            cell.innerHTML = cell2;

                            cell = newrow.insertCell(2);
                            var cell3 = "";
                            if (parseInt(axleCount - 2) == parseInt(i)) {
                                cell3 = "<td height='30' ><input type='text' value = '' name='depreciation' id='depreciation" + rowCount + "' maxlength='13'   size='20' class='form-control'  onchange='vehicleCostCalculation(" + rowCount + ");lastRowPercent(" + rowCount + ")' /></td>";
                            } else if (parseInt(axleCount - 1) == parseInt(i)) {
                                cell3 = "<td height='30' ><input type='text' value = '' name='depreciation' id='depreciation" + rowCount + "' maxlength='13'   size='20' class='form-control'  onchange='vehicleCostCalculation(" + rowCount + ");' readonly /></td>";
                            } else {
                                cell3 = "<td height='30' ><input type='text' value = '' name='depreciation' id='depreciation" + rowCount + "' maxlength='13'   size='20' class='form-control'  onchange='vehicleCostCalculation(" + rowCount + ");' /></td>";
                            }
                            cell.setAttribute("className", style);
                            cell.innerHTML = cell3;

                            cell = newrow.insertCell(3);
                            var cell4 = "<td  height='30'><input type='hidden' value = '' name='yearCost' id='yearCost" + rowCount + "' maxlength='13'   size='20' class='form-control'    /><span id='yearCostSpan" + rowCount + "'>0.00</span></td>";
                            cell.setAttribute("className", style);
                            cell.innerHTML = cell4;

                            cell = newrow.insertCell(4);
                            var cell5 = "";
                            if (parseInt(axleCount - 1) == parseInt(i)) {
                                cell5 = "<td height='30' ><input type='hidden' value = '' name='perMonth' id='perMonth" + rowCount + "' maxlength='13'   size='20' class='form-control'  onchange='perMonthCalculation(" + rowCount + ");'  /><span id='perMonthSpan" + rowCount + "'>0.00</span></td>";
                            } else if (parseInt(axleCount) == parseInt(i) + 1) {
                                cell5 = "<td height='30' ><input type='hidden' value = '' name='perMonth' id='perMonth" + rowCount + "' maxlength='13'   size='20' class='form-control'  onchange='perMonthCalculation(" + rowCount + ");'  /><span id='perMonthSpan" + rowCount + "'>0.00</span></td>";
                            } else {
                                cell5 = "<td height='30' ><input type='hidden' value = '' name='perMonth' id='perMonth" + rowCount + "' maxlength='13'   size='20' class='form-control' onchange='perMonthCalculation(" + rowCount + ");'  /><span id='perMonthSpan" + rowCount + "'>0.00</span></td>";
                            }
                            cell.setAttribute("className", style);
                            cell.innerHTML = cell5;

                        }

                    }


                    function lastRowPercent(rowCount) {
                        var depreciation = document.getElementsByName("depreciation");
                        var yearCost = document.getElementsByName("yearCost");
                        var perMonth = document.getElementsByName("perMonth");
                        var depreciationLength = parseInt(depreciation.length - 1);
                        var percentSum = 0;
                        var row = parseInt(rowCount + 1);
                        for (var i = 0; i < depreciationLength; i++) {
                            percentSum += parseInt(depreciation[i].value);
                        }
                        var balancePercent = parseInt(100 - percentSum);
                        if (balancePercent <= 0) {
                            alert("Depreciation planning is incorrect please plan again");
                            for (var i = 1; i <= depreciationLength; i++) {
                                $("#depreciation" + i).val('');
                                $("#yearCost" + i).val('');
                                $("#yearCostSpan" + i).text('0.00');
                                $("#perMonth" + i).val('');
                                $("#perMonthSpan" + i).text('0.00');
                            }
                            $("#depreciation" + row).val('');
                            $("#yearCost" + row).val('');
                            $("#yearCostSpan" + row).text('0.00');
                            $("#perMonth" + row).val('');
                            $("#perMonthSpan" + row).text('0.00');
                        } else {
                            var row = parseInt(rowCount + 1);
                            $("#depreciation" + row).val(balancePercent);
                            vehicleCostCalculation(row);
                            $("#depreciation" + 1).focus();
                        }
                    }
                    function vehicleCostCalculation(rowCount) {
                        for (var i = 0; i < parseInt(rowCount); i++) {
                            var depCost = document.getElementById('depreciation' + rowCount).value;
                            var vehicleCost = document.getElementById('vehicleDepreciationCost').value;
                            var depCostPercent = parseInt(depCost) / 100;
                            var depCostPerYear = parseFloat(depCostPercent * parseInt(vehicleCost)).toFixed(2);
                            document.getElementById('yearCost' + rowCount).value = depCostPerYear
                            $('#yearCostSpan' + rowCount).text(depCostPerYear);
                            var perMothCost = parseFloat(depCostPerYear / 12);
                            document.getElementById('perMonth' + rowCount).value = perMothCost.toFixed(2);
                            $('#perMonthSpan' + rowCount).text(perMothCost.toFixed(2));
                        }
                    }



                                                                                                                                        </script>
            </div>

            <div id="vehicleFileAttachments">

                   <script type="text/javascript">

                       $(document).ready(function()
                       {
                           $("#uploadBtn").click(function()
                           {
                               var uploadRemarks = "";
                               var count = 0;
                               $('input[name="file"]').each(function(index, value)
                               {
                                   var file = value.files[0];
                                   if (file)
                                   {
                                       var vehicleId = document.addVehicle.vehicleId.value;
                                       uploadRemarks = $("#narration" + count).val();
                                       var formData = new FormData();
                                       formData.append('file', file);
                                       $.ajax({
                                           url: './uploadVehicleDocuments.do?narrationVal=' + uploadRemarks + '&vehicleId=' + vehicleId,
                                           type: 'POST',
                                           data: formData,
                                           cache: false,
                                           contentType: false,
                                           processData: false,
                                           success: function(data, textStatus, jqXHR) {
                                               var message = jqXHR.responseText;
                                               // $("#messages").append("<li>" + message + "</li>");
                                               $("#StatusMsg").text("Files Uploaded sucessfully ").css("color", "green");
                                               alert("Files Uploaded sucessfully");
                                           },
                                           error: function(jqXHR, textStatus, errorThrown) {
                                               // $("#messages").append("<li style='color: red;'>" + textStatus + "</li>");
                                           }
                                       });
                                   }
                                   count++;
                               });
                           });
                       });
                                                                                                                                  </script>



                            <c:if test="${vehicleUploadsDetails != null}">
                                <%int img = 0;%>
                            <table class="table table-info"   style="width: 950px;" align="center">
                                <thead>
                                <th><spring:message code="trucks.label.SNo"  text="default text"/></th>
                                <th>&emsp;&emsp;&emsp;<spring:message code="trucks.label.File"  text="default text"/></th>
                                <th>&emsp;&emsp;&emsp;&emsp;<spring:message code="trucks.label.FileName"  text="default text"/></th>
                                <th>&nbsp;<spring:message code="trucks.label.Remarks"  text="default text"/></th>
                                </thead>
                                    <c:forEach items="${vehicleUploadsDetails}" var="cust">
                                    <tr>
                                        <td>&nbsp;<%=img+1%></td>
                                        <td style="width: 100px;">
                                            <img src="/throttle/displayVehicleLogoBlobData.do?uploadId=<c:out value ="${cust.uploadId}"/>"  style="width: 90px;height: 40px" data-toggle="modal" data-target="#myModal<%=img%>" title="<c:out value ="${cust.remarks}"/>"/>
                                        </td>
                                        <td>&emsp;&emsp;<c:out value ="${cust.fileName}"/></td>
                                        <td><c:out value ="${cust.remarks}"/></td>
                                    </tr>
                                    <div class="modal fade" id="myModal<%=img%>" role="dialog" style="width: 100%;height: 100%">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" onclick="resetTheSlabDetails()">&<spring:message code="trucks.label.times"  text="default text"/>;</button>
                                                    <h4 class="modal-title"><c:out value ="${cust.fileName}"/></h4>
                                                </div>
                                                <div class="modal-body" id="slabRateListSet" style="width: 100%;height: 80%">
                                                    <img src="/throttle/displayVehicleLogoBlobData.do?uploadId=<c:out value ="${cust.uploadId}"/>" style="width: 95%;height: 75%" class="img-responsive" title="<c:out value ="${cust.remarks}"/>">
                                                </div>
                                                <div class="modal-footer">


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input  type="hidden" id="uploadId" name="uploadId" value="<c:out value ="${cust.uploadId}"/>"/>
                                        <%img++;%>
                                    </c:forEach>
                            </table>
                            </c:if>
                            <c:if test="${vehicleUploadsDetails == null}">

        <table class="table table-info" id="fileAddRow" width="700">
                       <thead>
                         <th><spring:message code="trucks.label.SNo"  text="default text"/></th>
                         <th>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<spring:message code="trucks.label.File"  text="default text"/></th>
                         <th>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<spring:message code="trucks.label.Remarks"  text="default text"/></th>
                         <th>&nbsp;<spring:message code="trucks.label.Delete"  text="default text"/></th>
                </thead>

            </table>
        <p><br>
            </p>

                            </c:if>
            <table align="center">

                <tr >
                     <td>&nbsp;</td>
                    <td align="right"><input type="button" id="AddRow" value="<spring:message code="trucks.label.AddRow"  text="default text"/>" class="btn btn-success" onClick="addRow();" style="width:100px;height:40px;"></td>
                    <td align="left"> &emsp;&emsp;<input type="button" id="DeleteRow1" value="<spring:message code="trucks.label.DeleteRow"  text="default text"/>" class="btn btn-success" onClick="DeleteRow();" style="width:100px;height:40px;">
                    </td>
                </tr>
               <br>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
<!--               <tr>&ensp;<br></tr>
               <tr>&ensp;<br></tr>-->
                <tr><br>
                <br>
                    <td align="center" colspan="4">&nbsp;<input type="button" class="btn btn-success btnPrevious" value="<spring:message code="hrms.label.Prev" text="default text"/>" name="Prev" style="width:100px;height:40px;"/></td>
                 </tr>
                <tr >
                    <td align="center" colspan="4"> <input id="uploadBtn" type="button" value="<spring:message code="trucks.label.UploadFiles"  text="default text"/>" class="btn btn-success" style="display: none;width:100px;height:40px;"/></td>
                </tr>
                <tr>
                      <ul id="messages" style="display: none;"></ul>
                </tr>

            </table>

                <script>
                    var rowCount = 1;
                    var sno = 0;
                    var style = "text2";
                    var align = "center";
                    function addRow()
                    {
                        // alert("swdbgf");
                        if (rowCount % 2 == 0) {
                            style = "text2";
                        } else {
                            style = "text1";
                        }

                        var serialNo = parseInt(sno) + 1;
                        var tab = document.getElementById("fileAddRow");
                        var newrow = tab.insertRow(rowCount);

                        var cell = newrow.insertCell(0);
                        var cell1 = "<td  height='25' >" + serialNo + "</td>";
                        cell.setAttribute("align", align);
                        cell.innerHTML = cell1;

                        cell = newrow.insertCell(1);
                        var cell2 = "<td  height='30'><input type='file' name='file' size='110'  multiple /></td>";
                        cell.setAttribute("align", align);
                        cell.innerHTML = cell2;

                        cell = newrow.insertCell(2);
                        var cell3 = "<td  height='30'><textarea name='narration' class='form-control'  id='narration" + sno + "'  style='width:300px;'/></textarea></td>";
                        cell.setAttribute("align", align);
                        cell.innerHTML = cell3;
                        cell = newrow.insertCell(3);
                        var cell4 = "<td align='left' ><input type='checkbox' class='checkbox form-control' id='selectedIndex" + sno + "' name='selectedIndex' value='" + sno + "'style='width:15px;'/></td>";
                        cell.setAttribute("align", align);
                        cell.innerHTML = cell4;

                        var temp = sno - 1;

                        rowCount++;
                        sno++;

                    }
                    function DeleteRow() {
                        document.getElementById('AddRow').style.display = 'block';
                        try {
                            var table = document.getElementById("fileAddRow");
                            rowCount = table.rows.length;
                            // alert(rowCount);
                            for (var i = 1; i < rowCount; i++) {
                                var row = table.rows[i];
                                var checkbox = row.cells[3].childNodes[0];
                                if (null != checkbox && true == checkbox.checked) {
                                    if (rowCount <= 1) {
                                        alert("Cannot delete all the rows");
                                        break;
                                    }
                                    table.deleteRow(i);
                                    rowCount--;
                                    i--;
                                    sno--;
                                    // snumber--;
                                }
                            }
                        } catch (e) {
                            alert(e);
                        }
                    }
                            </script>

            </div>

        </div>

                     <script>
                         //                    $(".nexttab").click(function() {
                         //                        var selected = $("#tabs").tabs("option", "selected");
                         //                        $("#tabs").tabs("option", "selected", selected + 1);
                         //                    });
                         $('.btnNext').click(function() {
                             $('.nav-tabs > .active').next('li').find('a').trigger('click');
                         });
                         $('.btnPrevious').click(function() {
                             $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                         });




                         function saveVehicleTyre() {
                             var itemIds = document.getElementsByName("itemIds");
                             var positionIds = document.getElementsByName("typeId");
                             var tyreIds = document.getElementsByName("tyreIds");
                             var tyreNos = document.getElementsByName("tyreNos");
                             var tyreExists = document.getElementsByName("tyreExists");
                             //                var tyreDate = document.getElementsByName("tyreDate");
                             //                var description = document.getElementById("description").value;
                             var tyreId = "";
                             var item = "";
                             var pos = "";
                             var tyreNo = "";
                             var date = "";
                             var vehicleId = "";
                             for (var i = 0; i < itemIds.length; i++) {
                                 //                    if(itemIds[i+1].value == 0){
                                 //                        alert("Please select item for row "+i)
                                 //                        document.getElementById("itemIds"+i).focus();
                                 //                        return;
                                 //                    }else if(positionIds[i+1].value == 0){
                                 //                        alert("Please select position for row "+i)
                                 //                        document.getElementById("positionIds"+i).focus();
                                 //                        return;
                                 //                    }else if(tyreIds[i+1].value == 0){
                                 //                        alert("Please enter tyre no for row "+i)
                                 //                        document.getElementById("tyreIds"+i).focus();
                                 //                        return;
                                 //                    }else if(tyreDate[i+1].value == 0){
                                 //                        alert("Please select tyre date for row "+i)
                                 //                        document.getElementById("tyreDate"+i).focus();
                                 //                        return;
                                 //                    }else{
                                 tyreId = document.getElementById("tyreIds" + i).value;
                                 item = document.getElementById("itemIds" + i).value;
                                 pos = document.getElementById("positionIds" + i).value;
                                 tyreNo = document.getElementById("tyreNos" + i).value;
                                 date = document.getElementById("tyreDate" + i).value;
                                 vehicleId = document.getElementById("vehicleId").value;
                                 var urlInfo = "";
                                 if (tyreId == 0) {
                                     urlInfo = "./saveVehicleTyre.do";
                                 } else {
                                     urlInfo = "./updateVehicleTyre.do";
                                 }
                                 $.ajax({
                                     url: urlInfo,
                                     data: {itemId: item, pos: pos,
                                         tyreId: tyreId,
                                         tyreNo: tyreNo,
                                         date: date, vehicleId: vehicleId, description: description
                                     },
                                     type: "GET",
                                     success: function(response) {
                                         $("#tyreId" + i).val(response.toString().trim());
                                     },
                                     error: function(xhr, status, error) {
                                     }
                                 });
                                 //                    }
                             }


                         }

                                                                                                                               </script>

        <script type="text/javascript">


            function advanceFuelTabHide() {

                //alert(<%=request.getAttribute("vehicle_id")%>);
                if ('<%=request.getAttribute("vehicle_id")%>' == 'null') {//Mk and topay


                    $('#vehicleInsurane').hide();
                } else if ('<%=request.getAttribute("vehicle_id")%>' != 'null') {


                    $('#vehicleInsurane').show();
                }

            }
                    </script>
                    <c:if test="${vehicleId > '0' && editStatus == '1'}">
        <script>
            $("#vehicleDetailUpdateButton").show();
            $("#vehicleDetailSaveButton").hide();
            // $("#tyreSave").show();
            $("#depreciationSave").show();
            $("#oEmNext").show();
            //            $("#insuranceNext").show();
            //            $("#roadTaxNext").show();
            //            $("#fcNext").show();
            //            $("#permitNext").show();
                                                                                                                                                     </script>

                    </c:if>
                    <c:if test="${vehicleId > '0' && editStatus == '0'}">
        <script>
            $("#vehicleDetailUpdateButton").hide();
            $("#vehicleDetailSaveButton").hide();
            // $("#tyreSave").hide();
            $("#depreciationSave").hide();
            $("#oEmNext").hide();
            //            $("#insuranceNext").hide();
            //            $("#roadTaxNext").hide();
            //            $("#fcNext").hide();
            //            $("#permitNext").hide();
                        </script>

                    </c:if>




    </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>

</div>


    </div>
    </div>





<%@ include file="/content/common/NewDesign/settings.jsp" %>

<!--        <p>
            Select file 1:
        </p>
        <p>
            Select file 2: <input type="file" name="file" size="45"  multiple />
        </p>
        <p>
            Select file 3: <input type="file" name="file" size="45"  multiple />
        </p>
         -->
<!--                <table align="center" border="0" cellpadding="0" cellspacing="0" width="70%" id="POD1">
                    <tr id="rowId0">
                        <td  align="center" height="30" >Sno</td>
                        <td   align="center" height="30">UploadType  </td>
                        <td  height="30" >Attachment</td>
                        <td  height="30" >Delete</td>
                    </tr>
<% int index = 1;%>
                    <tr>
                        <td>      <input type="hidden" name="tripSheetId1" id="tripSheetId1" value="<%=request.getParameter("tripSheetId1")%>" />

                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <input type="button" class="button" name="add" value="ADD File" onclick="addRow1();"/>
                            <a  class="nexttab" ><input type="button" class="button" value="Confirm" name="Next" /></a>
                        </td>
                    </tr>


                    <script>
                    var podRowCount1 = 1;
                    var podSno1 = 1;
                    function addRow1() {
                        var tab = document.getElementById("POD1");
                        var newrow = tab.insertRow(podSno1);

                        newrow.id = 'rowId' + podSno1;
                        var cell = newrow.insertCell(0);
                        var cell0 = "<td  height='25' >" + podSno1 + "</td>";
                        cell.innerHTML = cell0;

                        cell = newrow.insertCell(1);
                        cell0 = "<td  height='25'><select class='form-control' id='vehicleDetails" + podSno1 + "' style='width:125px'  name='vehicleDetails' ><option selected value='0'>Policy Receipt</option><option selected value='1'>Vehicle Registration Copy</option><option selected value='2'>FC (Fitness Certificate)</option><option selected value='3'>Tax Details</option><option selected value='4'>others</option></select></td>";
                        cell.innerHTML = cell0;

                        cell = newrow.insertCell(2);
                        var cell0 = "<td  height='25' ><input type='file'   id='podFile" + podSno1 + "' name='podFile' class='textbox typeahead validate[required]' value='' onchange='checkName(" + podSno1 + ");' ><br/><font size='2' color='blue'> Allowed file type:pdf & image</td>";
                        cell.innerHTML = cell0;

                        var cell = newrow.insertCell(3);
                        var cell0 = "";
                        if (podSno1 == 1) {
                            cell0 = "";
                        } else {
                            cell0 = "<td  height='30' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src='baxiimages/min1.jpg' onclick='deleteFileRow(" + podSno1 + ")'/>";
                        }
                        cell.setAttribute("className", styl);
                        cell.innerHTML = cell0;
                        podSno1++;
                    }

                    function deleteFileRow(sno) {
                        if (sno != 1) {
                            podSno1--;
                            document.getElementById("POD1").deleteRow(sno);
                        } else {
                            alert("Can't Delete The Row");
                        }
                    }


                </script>
<%index++;%>
                </table>-->

<!--            <div id="vehicleRoadTax">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" id="bg" class="border">
                    <tr>
                        <td >Receipt No</td>
                        <td ><input type="text" name="roadTaxReceiptNo" id="roadTaxReceiptNo" maxlength="15" class="form-control"  value='<c:out value="${roadTaxReceiptNo}"/>' ></td>
                        <td >Receipt Date</td>
                        <td ><input type="text" name="roadTaxReceiptDate" id="roadTaxReceiptDate" class="datepicker"  value='<c:out value="${roadTaxReceiptDate}"/>' ></td>
                    </tr>
                    <tr>
                        <td >Location</td>
                        <td ><input type="text" name="roadTaxPaidLocation" id="roadTaxPaidLocation" class="form-control"  value='<c:out value="${roadTaxPaidLocation}"/>' maxlength="15" onkeypress="return onKeyPressBlockNumbers(event);"></td>
                        <td >Amount</td>
                        <td ><input type="text" name="roadTaxPaidAmount" id="roadTaxPaidAmount"  maxlength="7" class="form-control"  value='<c:out value="${roadTaxAmount}"/>' onkeypress="return blockNonNumbers(this, event, true, false);"></td>
                    </tr>
                    <tr>
                        <td >Period</td>
                        <td ><select name="roadTaxPeriod" id="roadTaxPeriod"  class="form-control"  >
                                <option value="0">Quarterly</option>
                                <option value="1">Half Yearly</option>
                                <option value="2">Yearly</option>
                            </select>
                        <script>
                            document.getElementById("roadTaxPeriod").value = '<c:out value="${roadTaxPeriod}"/>'
                        </script>
                        </td>
                        <td >Road Tax From Date</td>
                        <td ><input type="text" name="roadTaxFromDate" id="roadTaxFromDate" class="datepicker"  value='<c:out value="${roadTaxFromDate}"/>' ></td>
                    </tr>
                    <tr>
                        <td >Road Tax To Date</td>
                        <td ><input type="text" name="nextRoadTaxDate" id="nextRoadTaxDate" class="datepicker"  value='<c:out value="${nextRoadTaxDate}"/>' ></td>
                        <td >Remarks</td>
                        <td ><input type="text" name="roadTaxRemarks" id="roadTaxRemarks" class="form-control"  value='<c:out value="${roadTaxRemarks}"/>' ></td>
                    </tr>
                    <tr>
                        <tr>
                      <td id="vendorRT" style="visibility: hidden;">Vendor Name</td>
                        <td id="vendorRT1" style="visibility: hidden;"><select name="vendorIdRT" id="vendorIdRT" class="form-control" style="width:129px;height:20px;">
<c:if test="${vendorListCompliance != null}">
                                <option value="0" selected>--Select--</option>
    <c:forEach items="${vendorListCompliance}" var="vendor">
                                    <option value='<c:out value="${vendor.vendorId}"/>~<c:out value="${vendor.ledgerId}"/>~<c:out value="${vendor.ledgerCode}"/>'><c:out value="${vendor.vendorName}"/></option>
    </c:forEach>
</c:if>
                            </select>
                        <script>
//                             alert('<c:out value="${bankBranchIdRT}"/>')
                            document.getElementById("vendorIdRT").value = '<c:out value="${bankBranchIdRT}"/>'
                        </script>
                            </td>

                      <td id="bankPaymentModeRT" style="visibility: hidden;" height="30">Payment Mode:</td>
                        <td id="bankPaymentModeRT1" style="visibility: hidden;">
                    <select class="form-control" name="paymentTypeRT" id="paymentTypeRT"  onchange="getRoadTaxBankPayment(this.value)" >
                                <option value="0">----select----</option>
                                <option value="1">Cash Payment</option>
                                <option value="2">Bank Payment</option>
                        <input type="hidden" id="roadTaxId" name="roadTaxId" value='<c:out value="${roadTaxId}"/>'>
                        <input type="hidden" id="clearanceIdRT" name="clearanceIdRT" value='<c:out value="${clearanceIdRT}"/>'>
                            </select>
                        </td>
                    <script>
                        document.getElementById("paymentTypeRT").value = '<c:out value="${paymentTypeRT}"/>'
                    </script>
                    </tr>
                    <tr>
                         <td >Payment Date</td>
                        <td ><input type="text" name="paymentDateRT" id="paymentDateRT" class="datepicker"  value='<c:out value="${paymentDateRT}"/>' ></td>
                    </tr>
                    <tr>
<%--<c:if test="${clearanceIdRT == '' || clearanceIdRT == null }">--%>
                    <tr id="bankPayment5" style="visibility: hidden;">
                     <td colspan="6"  height="30">
                        <div   align="center">Bank Payment Details</div>
                    </td>
                      </tr>
                  <tr id="bankPayment6" style="visibility: hidden;">

                        <td>Bank Head</td>
                        <td><select name="bankIdRT" id="bankIdRT" class="form-control"  onchange="getBankBranchRoadTax(this.value)" style="width:129px;height:20px;">
<c:if test="${primarybankList != null}">
                                <option value="0" selected>--Select--</option>
    <c:forEach items="${primarybankList}" var="bankVar">
                                    <option value='<c:out value="${bankVar.primaryBankId}"/>'><c:out value="${bankVar.primaryBankName}"/></option>
    </c:forEach>
</c:if>
                            </select>
                        <script>
                            document.getElementById("bankIdRT").value = '<c:out value="${bankIdRT}"/>'
                        </script>
                        </td>
                        <td>Bank branch :</td>
                        <td> <select name="bankBranchIdRT" id="bankBranchIdRT" class="form-control" style="width:129px;height:20px;">
<c:if test="${bankBranchList != null}">
                                <option value="0" selected>--Select--</option>
    <c:forEach items="${bankBranchList}" var="branch">
                                    <option value='<c:out value="${branch.branchId}"/>'><c:out value="${branch.bankname}"/></option>
    </c:forEach>
</c:if>
                            </select>
                         <script>
                            document.getElementById("bankBranchIdRT").value ='<c:out value="${vendorIdRT}"/>'
                        </script>
                        </td>
                    </tr>
                  <tr id="bankPayment7" style="visibility: hidden;">
                        <td><font color="red">*</font>Cheque Date</td>

                        <td><input name="chequeDateRT" id="chequeDateRT" type="text" class="datepicker" style="width:129px;" value='<c:out value="${chequeDateRT}"/>' ></td>
                        <td><font color="red">*</font>Cheque No</td>
                        <td><input name="chequeNoRT" id="chequeNoRT" style="width:129px;" maxlength="45" type="text" class="form-control" value='<c:out value="${chequeNoRT}"/>'></td>
                    </tr>
                    <tr id="bankPayment8" style="visibility: hidden;">
                        <td>Cheque Amount</td>
                        <td><input name="chequeAmountRT" id="chequeAmountRT" style="width:129px;" maxlength="7" type="text" class="form-control" value='<c:out value="${chequeAmountRT}"/>' onkeypress="return blockNonNumbers(this, event, true, false);"></td>

                    <td>Clearance Date</td>
                        <td height="30"><input name="clearanceDateRT" id="clearanceDateRT" type="text" class="datepicker" value='<c:out value="${clearanceDateRT}"/>' ></td>
                    </tr>
                   </table>
                    </tr>
<%--</c:if>--%>
                </table>
                <br>
                <br>
                <center>

                    <input type="button" class="button" value="Save" id="roadTaxNext" name="Save"  onClick="updateRoadTaxDetails();" style="display: none"/>
                        <a  class="nexttab" ><input type="button" class="button" value="Next" name="Next" /></a>

                </center>
                    <script>
                     function getBankBranchRoadTax(bankIdRT) {
                              var temp = "";
                                  $.ajax({
                                      url: '/throttle/getBankBranchDetails.do',
                                      data: {bankId: bankIdRT},
                                      dataType: 'json',
                                       success: function (temp) {
//                                             alert(url);
                                  if (temp != '') {
                                      $('#bankBranchIdRT').empty();
                                      $('#bankBranchIdRT').append(
                                              $('<option style="width:150px"></option>').val(0+"~"+0).html('---Select----')
                                              )
                                      $.each(temp, function (i, data) {
                                          $('#bankBranchIdRT').append(
                                         $('<option value="'+data.branchId+"~"+data.bankLedgerId+'" style="width:150px"></option>').val(data.branchId+"~"+data.bankLedgerId).html(data.bankName)
                                         )
                                         document.getElementById("bankBranchIdRT").value ='<c:out value="${vendorIdRT}"/>'
                                      });
                                  } else {
                                      $('#bankBranchIdRT').empty();
                                  }
                              }
                                  });
                              }

                        function getRoadTaxBankPayment(val){
                           if(val == 2){
                             document.getElementById("bankPayment5").style.visibility = 'visible';
                             document.getElementById("bankPayment6").style.visibility = 'visible';
                             document.getElementById("bankPayment7").style.visibility = 'visible';
                             document.getElementById("bankPayment8").style.visibility = 'visible';
                           }
                           else if(val == 1){
                             document.getElementById("bankPayment5").style.visibility = 'hidden';
                             document.getElementById("bankPayment6").style.visibility = 'hidden';
                             document.getElementById("bankPayment7").style.visibility = 'hidden';
                             document.getElementById("bankPayment8").style.visibility = 'hidden';
                           }
                       }


                    </script>

               <script type="text/javascript">
                   function updateRoadTaxDetails() {
                       var vehicleId = $("#vehicleId").val();
                       var roadTaxReceiptNo = document.getElementById("roadTaxReceiptNo").value;
                       var roadTaxReceiptDate = document.getElementById("roadTaxReceiptDate").value;
                       var roadTaxPaidLocation = document.getElementById("roadTaxPaidLocation").value;
                       var roadTaxPaidAmount = document.getElementById("roadTaxPaidAmount").value;
                       var roadTaxPeriod = document.getElementById("roadTaxPeriod").value;
                       var roadTaxFromDate = document.getElementById("roadTaxFromDate").value;
                       var nextRoadTaxDate = document.getElementById("nextRoadTaxDate").value;
                       var roadTaxRemarks = document.getElementById("roadTaxRemarks").value;
                        var paymentTypeRT = document.getElementById("paymentTypeRT").value;
                        var roadTaxId = document.getElementById("roadTaxId").value;
                        var ledgerId = document.getElementById("vendorIdRT").value;
                        var cashOnHandLedgerId = document.getElementById("cashOnHandLedgerId").value;
                        var cashOnHandLedgerCode = document.getElementById("cashOnHandLedgerCode").value;
                        var paymentDateRT = document.getElementById("paymentDateRT").value;

                         var bankIdRT = "0";
                             if(document.getElementById("bankIdRT").value == null){
                                bankIdRT = "0";
                            }
                            else{
                                bankIdRT = document.getElementById("bankIdRT").value;
                            }
                            var clearanceIdRT = "0";
                            if(document.getElementById("clearanceIdRT").value == null){
                                clearanceIdRT = "0";
                            }
                            else{
                               clearanceIdRT =document.getElementById("clearanceIdRT").value;
                            }

                            var bankBranchIdRT = "0";
                             if(document.getElementById("bankBranchIdRT").value == null){
                                bankBranchIdRT = "0";
                            }
                            else{
                                bankBranchIdRT = document.getElementById("bankBranchIdRT").value;
                            }
                            var chequeDateRT = "0";
                            if(document.getElementById("chequeDateRT").value == null){
                              chequeDateRT = "";
                            }else{
                              chequeDateRT = document.getElementById("chequeDateRT").value;
                            }
                            var chequeNoRT = "0";
                             if(document.getElementById("chequeNoRT").value == null){
                              chequeNoRT = "";
                            }else{
                              chequeNoRT = document.getElementById("chequeNoRT").value;
                            }
                            var chequeAmountRT ="0";
                             if(document.getElementById("chequeAmountRT").value == null){
                              chequeAmountRT = "0";
                            }else{
                              chequeAmountRT = document.getElementById("chequeAmountRT").value;
                            }

                       if (document.addVehicle.roadTaxReceiptNo.value == '') {
                           alert('Please Enter Vehicle ReceiptNo');
                           $("#roadTaxReceiptNo").focus();
                           return;
                       } else if (document.addVehicle.roadTaxReceiptDate.value == '') {
                           alert('Please Enter Vehicle ReceiptDate');
                           $("#roadTaxReceiptDate").focus();
                           return;
                       } else if (document.addVehicle.roadTaxPaidLocation.value == '') {
                           alert('Please Enter Vehicle PaidLocation');
                           $("#roadTaxPaidLocation").focus();
                           return;
                       } else if (document.addVehicle.roadTaxPaidAmount.value == '') {
                           alert('Please Enter Vehicle PaidAmount');
                           $("#roadTaxPaidAmount").focus();
                           return;
                       } else if (document.addVehicle.roadTaxPeriod.value == '') {
                           alert('Please Enter Vehicle Period');
                           $("#roadTaxPeriod").focus();
                           return;
                       } else if (document.addVehicle.roadTaxFromDate.value == '') {
                           alert('Please Enter Vehicle Tax Valid FromDate');
                           $("#roadTaxFromDate").focus();
                           return;
                       } else if (document.addVehicle.nextRoadTaxDate.value == '') {
                           alert('Please Enter Vehicle Tax Valid Todate');
                           $("#nextRoadTaxDate").focus();
                           return;
                       }
                       var url = '';
                       url = './insertRoadTaxDetails.do';
                       $.ajax({
                           url: url,
                           data: {roadTaxReceiptNo: roadTaxReceiptNo, roadTaxReceiptDate: roadTaxReceiptDate, roadTaxPaidLocation: roadTaxPaidLocation, roadTaxPaidAmount: roadTaxPaidAmount, roadTaxPeriod: roadTaxPeriod, vehicleId: vehicleId,
                               roadTaxFromDate: roadTaxFromDate, nextRoadTaxDate: nextRoadTaxDate, roadTaxRemarks: roadTaxRemarks,
                               paymentTypeRT:paymentTypeRT,bankIdRT:bankIdRT,bankBranchIdRT:bankBranchIdRT,chequeDateRT:chequeDateRT,
                               chequeNoRT:chequeNoRT,chequeAmountRT:chequeAmountRT,clearanceIdRT:clearanceIdRT,
                               roadTaxId:roadTaxId,ledgerId:ledgerId,cashOnHandLedgerId:cashOnHandLedgerId,cashOnHandLedgerCode:cashOnHandLedgerCode,
                               paymentDateRT:paymentDateRT
                           },
                           type: "GET",
                           success: function (response) {
                               alert(response);
                               vehicleId = response.toString().trim();
                               if (vehicleId == 0) {
                                   $("#StatusMsg").text("Vehicle Roadtax added failed ");
                               } else {
                                   $("#StatusMsg").text("Vehicle RoadTax added sucessfully ");
                               }

                           },
                           error: function (xhr, status, error) {
                           }
                       });
                       document.getElementById("roadTaxNext").style.visibility = 'hidden';
                   }
                                                                                                                                                                                  </script>


            </div>-->
            <div style="display:none;">
            <div id="vehicleFitnessCertificate">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" id="bg" class="border">
                    <tr>
                        <td ><spring:message code="trucks.label.RTODetails"  text="default text"/></td>
                        <td ><input type="text" name="rtoDetail" id="rtoDetail" maxlength="25" class="form-control" style="width:129px;" value='' ></td>
                        <td ><spring:message code="trucks.label.FcDate"  text="default text"/></td>
                     <td ><input name="fcDate" id="fcDate" type="text" class="form-control"  date-format="dd-mm-yyyy"  value='<c:out value="${fcExpiryDate}"/>' ></td>
                    </tr>
                    <tr>
                        <td ><spring:message code="trucks.label.ReceiptNo"  text="default text"/></td>
                        <td ><input type="text" name="fcReceiptNo" maxlength="25" id="fcReceiptNo" class="form-control" style="width:129px;" value='' ></td>
                        <td ><spring:message code="trucks.label.FCAmount"  text="default text"/></td>
                        <td ><input type="text" name="fcAmount" maxlength="7" id="fcAmount" class="form-control" style="width:129px;" value='' onkeypress="return onKeyPressBlockCharacters(event);" onchange="fcAmountSet(this.value);"></td>
                    </tr>
                    <tr>

                        <td ><spring:message code="trucks.label.FCExpiryDate"  text="default text"/></td>
                        <td ><input name="fcExpiryDate" id="fcExpiryDate" type="text" class="form-control" id="toDate1"  date-format="dd-mm-yyyy"  value='' ></td>
                        <td ><spring:message code="trucks.label.Remarks"  text="default text"/></td>
                        <td ><input type="text" name="fcRemarks" id="fcRemarks" class="form-control" style="width:129px;" value='' ></td>

                    </tr>


                     <tr>
                       <td  id="vendorFC" style="visibility: hidden;"><spring:message code="trucks.label.InsuranceCompanyName"  text="default text"/></td>
                        <td  id="vendorFC1" style="visibility: hidden;"><select name="vendorIdFC" id="vendorIdFC" style="width:129px;" class="form-control"  style="width:129px;height:20px;">
                        <c:if test="${vendorListCompliance != null}">
                                <option value="0" selected>--<spring:message code="trucks.label.SNo"  text="default text"/>Select--</option>
                            <c:forEach items="${vendorListCompliance}" var="vendor">
                                    <option value='<c:out value="${vendor.vendorId}"/>~<c:out value="${vendor.ledgerId}"/>~<c:out value="${vendor.ledgerCode}"/>'><c:out value="${vendor.vendorName}"/></option>
                            </c:forEach>
                        </c:if>
                            </select>
<!--                        <script>
                            document.getElementById("vendorIdFC").value = '<c:out value="${bankBranchIdFC}"/>'
                        </script>-->
                            </td>
                      <td   id="bankPaymentModeFC" style="visibility: hidden;"  style="width:129px;" height="30"><spring:message code="trucks.label.PaymentMode"  text="default text"/>:</td>
                        <td   id="bankPaymentModeFC1" style="visibility: hidden;">
                    <select class="form-control" name="paymentTypeFC" id="paymentTypeFC"  onchange="getFCBankPayment(this.value)" >
                                <option value="0">----<spring:message code="trucks.label.select"  text="default text"/>----</option>
                                <!--<option value="1">Cash Payment</option>-->
                                <option value="2"><spring:message code="trucks.label.BankPayment"  text="default text"/></option>
                            </select>
                        </td>
<!--                    <script>
                        document.getElementById("paymentTypeFC").value = '<c:out value="${paymentTypeFC}"/>'
                    </script>-->
                    </tr>
                    <tr>
                         <td id="bankPaymentDateFC" style="visibility: hidden;" ><spring:message code="trucks.label.PaymentDate"  text="default text"/></td>
                        <td id="bankPaymentDateFC1" style="visibility: hidden;" ><input type="text" name="paymentDateFC" id="paymentDateFC" class="datepicker" style="width:129px;" value='' ></td>
                    </tr>
            <%--<c:if test="${clearanceIdFC == '' || clearanceIdFC == null }">--%>
                    <tr>
                    <tr id="bankPayment9" style="visibility: block;">
                     <td colspan="6"  height="30">
                        <div   align="center"><spring:message code="trucks.label.BankPaymentDetails"  text="default text"/></div>
                    </td>
                      </tr>
                  <tr id="bankPayment10" style="visibility: block;">
                        <td><spring:message code="trucks.label.BankHead"  text="default text"/></td>
                        <td  ><select name="bankIdFC" id="bankIdFC" class="form-control"  onchange="getBankBranchFC(this.value)" style="width:129px;height:20px;">
                        <c:if test="${primarybankList != null}">
                                <option value="0" selected>--<spring:message code="trucks.label.Select"  text="default text"/>--</option>
                            <c:forEach items="${primarybankList}" var="bankVar">
                                    <option value='<c:out value="${bankVar.primaryBankId}"/>'><c:out value="${bankVar.primaryBankName}"/></option>
                            </c:forEach>
                        </c:if>
                            </select>
<!--                        <script>
                            document.getElementById("bankIdFC").value = '<c:out value="${bankIdFC}"/>'
                        </script>-->
                        </td>
                        <td><spring:message code="trucks.label.Bankbranch"  text="default text"/>:</td>
                        <td  > <select name="bankBranchIdFC" id="bankBranchIdFC" class="form-control" style="width:129px;height:20px;">
                        <c:if test="${bankBranchList != null}">
                                <option value="0" selected>--<spring:message code="trucks.label.Select"  text="default text"/>--</option>
                            <c:forEach items="${bankBranchList}" var="branch">
                                    <option value='<c:out value="${branch.branchId}"/>'><c:out value="${branch.bankname}"/></option>
                            </c:forEach>
                        </c:if>
                            </select>
<!--                         <script>
                            document.getElementById("bankBranchIdFC").value ='<c:out value="${vendorIdFC}"/>'
                        </script>            -->
                        </td>

                    </tr>
                  <tr id="bankPayment11" style="visibility: block;">
                        <td  ><font color="red">*</font><spring:message code="trucks.label.ChequeDate"  text="default text"/></td>
<!--                        <input type="hidden" id="fcid" name="fcid" value='<c:out value="${fcid}"/>'>
                        <input type="hidden" id="clearanceIdFC" name="clearanceIdFC" value='<c:out value="${clearanceIdFC}"/>'>-->
                        <input type="hidden" id="fcid" name="fcid" value=''>
                        <input type="hidden" id="clearanceIdFC" name="clearanceIdFC" value=''>
                        <td ><input name="chequeDateFC" id="chequeDateFC" type="text" class="datepicker" style="width:129px;" value='' ></td>
                        <td   ><font color="red">*</font><spring:message code="trucks.label.ChequeNo"  text="default text"/></td>
                        <td ><input name="chequeNoFC" id="chequeNoFC" style="width:129px;" maxlength="45" type="text" class="form-control" value=''></td>
                    </tr>
                    <tr id="bankPayment12" style="visibility: block;">
                        <td><spring:message code="trucks.label.ChequeAmount"  text="default text"/></td>
                        <td><input name="chequeAmountFC" id="chequeAmountFC" style="width:129px;" maxlength="7" type="text" class="form-control" value='' onkeypress="return blockNonNumbers(this, event, true, false);" readonly></td>

                    </tr>

                   <!--</table>-->
                    </tr>
            <%--</c:if>--%>
                </table>
                <br>
                <br>
                <center>

                    <input type="button" class="button" value="<spring:message code="trucks.label.Save"  text="default text"/>" id="fcNext" name="Save"  onClick="updateFCDetails();" style="display: none"/>
                        <a  class="nexttab" ><input type="button" class="button" value="<spring:message code="trucks.label.Next"  text="default text"/>" name="Next" /></a>

                </center>
                <script>
                    function fcAmountSet(val) {
                        document.getElementById("chequeAmountFC").value = val;
                    }
                    function getBankBranchFC(bankIdFC) {
                        var temp = "";
                        $.ajax({
                            url: '/throttle/getBankBranchDetails.do',
                            data: {bankId: bankIdFC},
                            dataType: 'json',
                            success: function(temp) {
//                                             alert(url);
                                if (temp != '') {
                                    $('#bankBranchIdFC').empty();
                                    $('#bankBranchIdFC').append(
                                            $('<option style="width:150px"></option>').val(0 + "~" + 0).html('---Select----')
                                            )
                                    $.each(temp, function(i, data) {
                                        $('#bankBranchIdFC').append(
                                                $('<option value="' + data.branchId + "~" + data.bankLedgerId + '" style="width:150px"></option>').val(data.branchId + "~" + data.bankLedgerId).html(data.bankName)
                                                )
                                        document.getElementById("bankBranchIdFC").value = '<c:out value="${vendorIdFC}"/>'
                                    });
                                } else {
                                    $('#bankBranchIdFC').empty();
                                }
                            }
                        });
                    }

                    function getFCBankPayment(val) {
//                           var ownerShips=document.getElementById("ownerShips").value;
                        if (val == 2) {
                            document.getElementById("bankPayment9").style.visibility = 'visible';
                            document.getElementById("bankPayment10").style.visibility = 'visible';
                            document.getElementById("bankPayment11").style.visibility = 'visible';
                            document.getElementById("bankPayment12").style.visibility = 'visible';
                        }
                        else if (val == 1) {
                            document.getElementById("bankPayment9").style.visibility = 'hidden';
                            document.getElementById("bankPayment10").style.visibility = 'hidden';
                            document.getElementById("bankPayment11").style.visibility = 'hidden';
                            document.getElementById("bankPayment12").style.visibility = 'hidden';
                        }
                    }


            </script>
               <script type="text/javascript">

                   function updateFCDetails() {
                       var vehicleId = $("#vehicleId").val();
                       var rtoDetail = document.getElementById("rtoDetail").value;
                       var fcDate = document.getElementById("fcDate").value;
                       var fcReceiptNo = document.getElementById("fcReceiptNo").value;
                       var fcAmount = document.getElementById("fcAmount").value;
                       var fcExpiryDate = document.getElementById("fcExpiryDate").value;
                       var fcRemarks = document.getElementById("fcRemarks").value;
                       var paymentTypeFC = document.getElementById("paymentTypeFC").value;

                       var fcid = document.getElementById("fcid").value;
                       var ledgerId = document.getElementById("vendorIdFC").value;
                       var cashOnHandLedgerId = document.getElementById("cashOnHandLedgerId").value;
                       var cashOnHandLedgerCode = document.getElementById("cashOnHandLedgerCode").value;
                       var paymentDateFC = document.getElementById("paymentDateFC").value;

                       var bankIdFC = "0";
                       if (document.getElementById("bankIdFC").value == null) {
                           bankIdFC = "0";
                       }
                       else {
                           bankIdFC = document.getElementById("bankIdFC").value;
                       }
                       var clearanceIdFC = "0";
                       if (document.getElementById("clearanceIdFC").value == null) {
                           clearanceIdFC = "0";
                       }
                       else {
                           clearanceIdFC = document.getElementById("clearanceIdFC").value;
                       }

                       var bankBranchIdFC = "0";
                       if (document.getElementById("bankBranchIdFC").value == null) {
                           bankBranchIdFC = "0";
                       }
                       else {
                           bankBranchIdFC = document.getElementById("bankBranchIdFC").value;
                       }
                       var chequeDateFC = "0";
                       if (document.getElementById("chequeDateFC").value == null) {
                           chequeDateFC = "";
                       } else {
                           chequeDateFC = document.getElementById("chequeDateFC").value;
                       }
                       var chequeNoFC = "0";
                       if (document.getElementById("chequeNoFC").value == null) {
                           chequeNoFC = "";
                       } else {
                           chequeNoFC = document.getElementById("chequeNoFC").value;
                       }
                       var chequeAmountFC = "0";
                       if (document.getElementById("chequeAmountFC").value == null) {
                           chequeAmountFC = "0";
                       } else {
                           chequeAmountFC = document.getElementById("chequeAmountFC").value;
                       }

                       if (document.addVehicle.rtoDetail.value == '') {
                           alert('Please Enter Vehicle RtoDetail ');
                           $("#rtoDetail").focus();
                           return;
                       } else if (document.addVehicle.fcDate.value == '') {
                           alert('Please Enter Vehicle FcDate');
                           $("#fcDate").focus();
                           return;
                       } else if (document.addVehicle.fcReceiptNo.value == '') {
                           alert('Please Enter Vehicle FcReceiptNo');
                           $("#fcReceiptNo").focus();
                           return;
                       } else if (document.addVehicle.fcAmount.value == '') {
                           alert('Please Enter Vehicle FcAmount');
                           $("#fcAmount").focus();
                           return;
                       } else if (document.addVehicle.fcExpiryDate.value == '') {
                           alert('Please Enter Vehicle FcExpiryDate');
                           $("#fcExpiryDate").focus();
                           return;
                       }
                       var url = '';
                       url = './insertFCDetails.do';
                       $.ajax({
                           url: url,
                           data: {rtoDetail: rtoDetail, fcDate: fcDate, fcReceiptNo: fcReceiptNo, fcAmount: fcAmount, fcExpiryDate: fcExpiryDate, vehicleId: vehicleId,
                               fcRemarks: fcRemarks, paymentTypeFC: paymentTypeFC, bankIdFC: bankIdFC, bankBranchIdFC: bankBranchIdFC,
                               chequeDateFC: chequeDateFC, chequeNoFC: chequeNoFC, chequeAmountFC: chequeAmountFC,
                               clearanceIdFC: clearanceIdFC, fcid: fcid, ledgerId: ledgerId, cashOnHandLedgerId: cashOnHandLedgerId,
                               cashOnHandLedgerCode: cashOnHandLedgerCode, paymentDateFC: paymentDateFC, fleetTypeId: 1
                           },
                           type: "GET",
                           success: function(response) {
                               vehicleId = response.toString().trim();
                               if (vehicleId == 0) {
                                   $("#StatusMsg").text("Vehicle FcDetails added failed ");
                               } else {
                                   $("#StatusMsg").text("Vehicle FcDetails added sucessfully ");
                               }

                           },
                           error: function(xhr, status, error) {
                           }
                       });
                       document.getElementById("fcNext").style.visibility = 'hidden';

                   }
                                                                                                                                                                                                 </script>

            </div>
            </div>

<!--            <div id="vehiclePermit">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" id="bg" class="border">
                    <tr>
                        <td >Permit Type</td>
                        <td >

                            <select name="permitType" id="permitType" class="form-control" style="width:129px;">
                                <option value="State permit" selected>State permit</option>
                                <option value="National permit">National permit</option>
                            </select>
                            <script>
                            document.getElementById("permitType").value = '<c:out value="${permitType}"/>'
                        </script>
                        </td>
                        <td >Permit No</td>
                        <td ><input type="text" name="permitNo" id="permitNo" maxlength="15" class="form-control" style="width:129px;" value='<c:out value="${permitNo}"/>' ></td>
                    </tr>
                    <tr>
                        <td >Permit Paid Date</td>
                        <td ><input type="text" name="permitPaidDate" id="permitPaidDate" class="datepicker" style="width:129px;" value='<c:out value="${permitPaidDate}"/>' ></td>
                        <td >Permit Amount</td>
                        <td ><input type="text" name="permitAmount" id="permitAmount" maxlength="7" class="form-control" style="width:129px;" value='<c:out value="${permitAmount}"/>' onkeypress="return onKeyPressBlockCharacters(event)" ></td>
                    </tr>
                    <tr>
                        <td >Permit Expiry Date</td>
                        <td ><input type="text" name="permitExpiryDate" id="permitExpiryDate" class="datepicker" style="width:129px;" value='<c:out value="${permitExpiryDate}"/>' ></td>
                        <td >remarks</td>
                        <td ><input type="text" name="permitRemarks" id="permitRemarks"  class="form-control" style="width:129px;" value='<c:out value="${remarks}"/>' ></td>
                    </tr>

                    <tr>
                        <td  id="vendorPermits" style="visibility: hidden;">Vendor Name</td>
                        <td  id="vendorPermit1" style="visibility: hidden;"><select name="vendorPermit" id="vendorPermit" class="form-control" style="width:129px;height:20px;">
<c:if test="${vendorListCompliance != null}">
                                <option value="0" selected>--Select--</option>
    <c:forEach items="${vendorListCompliance}" var="vendor">
                                    <option value='<c:out value="${vendor.vendorId}"/>~<c:out value="${vendor.ledgerId}"/>~<c:out value="${vendor.ledgerCode}"/>'><c:out value="${vendor.vendorName}"/></option>
    </c:forEach>
</c:if>
                            </select>
                        <script>
                            document.getElementById("vendorPermit").value = '<c:out value="${bankBranchIdPermit}"/>'
                        </script>
                            </td>
                      <td id="bankPaymentModePermit" style="visibility: hidden;">Payment Mode:</td>
                        <td id="bankPaymentModePermit1" style="visibility: hidden;">
                    <select class="form-control" name="paymentTypePermit" id="paymentTypePermit"  onchange="getPermitBankPayment(this.value)" >
                                <option value="0">----select----</option>
                                <option value="1">Cash Payment</option>
                                <option value="2">Bank Payment</option>
                            </select>
                        <input type="hidden" id="permitid" name="permitid" value='<c:out value="${permitid}"/>'>
                        <input type="hidden" id="clearanceIdPermit" name="clearanceIdPermit" value='<c:out value="${clearanceIdPermit}"/>'>
                        </td>
                    <script>
                        document.getElementById("paymentTypePermit").value = '<c:out value="${paymentTypePermit}"/>'
                    </script>
                    </tr>
                    <tr>
                         <td >Payment Date</td>
                        <td ><input type="text" name="paymentDatePermit" id="paymentDatePermit" class="datepicker" value='<c:out value="${paymentDatePermit}"/>' ></td>
                    </tr>
<%--<c:if test="${clearanceIdPermit == '' || clearanceIdPermit == null }">--%>
                    <tr>
                    <tr id="bankPayment13" style="visibility: hidden;">
                     <td colspan="6"  height="30">
                        <div   align="center">Bank Payment Details</div>
                    </td>
                      </tr>
                  <tr id="bankPayment14" style="visibility: hidden;">


                        <td>Bank Head</td>
                        <td><select name="bankIdPermit" id="bankIdPermit" class="form-control"  onchange="getBankBranchPermit(this.value)" style="width:129px;height:20px;">
<c:if test="${primarybankList != null}">
                                <option value="0" selected>--Select--</option>
    <c:forEach items="${primarybankList}" var="bankVar">
                                    <option value='<c:out value="${bankVar.primaryBankId}"/>'><c:out value="${bankVar.primaryBankName}"/></option>
    </c:forEach>
</c:if>
                            </select>
                        <script>
                            document.getElementById("bankIdPermit").value = '<c:out value="${bankIdPermit}"/>'
                        </script>
                        </td>
                        <td>Bank branch :</td>
                        <td> <select name="bankBranchIdPermit" id="bankBranchIdPermit" class="form-control" style="width:129px;height:20px;">
<c:if test="${bankBranchList != null}">
                                <option value="0" selected>--Select--</option>
    <c:forEach items="${bankBranchList}" var="branch">
                                    <option value='<c:out value="${branch.branchId}"/>'><c:out value="${branch.bankname}"/></option>
    </c:forEach>
</c:if>
                            </select>

                        </td>

                    </tr>
                  <tr id="bankPayment15" style="visibility: hidden;">
                        <td><font color="red">*</font>Cheque Date</td>

                        <td><input name="chequeDatePermit" id="chequeDatePermit" type="text" class="datepicker" style="width:129px;" value='<c:out value="${chequeDatePermit}"/>' ></td>
                        <td><font color="red">*</font>Cheque No</td>
                        <td><input name="chequeNoPermit" id="chequeNoPermit" style="width:129px;" maxlength="45" type="text" class="form-control" value='<c:out value="${chequeNoPermit}"/>'></td>
                    </tr>
                    <tr id="bankPayment16" style="visibility: hidden;">
                        <td>Cheque Amount</td>
                        <td><input name="chequeAmountPermit" id="chequeAmountPermit" style="width:129px;" maxlength="7" type="text" class="form-control" value='<c:out value="${chequeAmountPermit}"/>' onkeypress="return blockNonNumbers(this, event, true, false);"></td>

                    <td>Clearance Date</td>
                        <td height="30"><input name="clearanceDatePermit" id="clearanceDatePermit" type="text" class="datepicker" value='<c:out value="${clearanceDatePermit}"/>' ></td>
                    </tr>
                   </table>
                    </tr>
<%--</c:if>--%>
                </table>
                <br>
                <br>
                <center>

                    <input type="button" class="button" value="Save" id="permitNext" name="Save"  onClick="updatePermitDetails();" style="display: none"/>

                        <a  class="nexttab" ><input type="button" class="button" value="Next" name="Next" /></a>
                </center>

                <script>
                     function getBankBranchPermit(bankIdPermit) {
                              var temp = "";
                                  $.ajax({
                                      url: '/throttle/getBankBranchDetails.do',
                                      data: {bankId: bankIdPermit},
                                      dataType: 'json',
                                       success: function (temp) {
//                                             alert(url);
                                  if (temp != '') {
                                      $('#bankBranchIdPermit').empty();
                                      $('#bankBranchIdPermit').append(
                                              $('<option style="width:150px"></option>').val(0+"~"+0).html('---Select----')
                                              )
                                      $.each(temp, function (i, data) {
                                          $('#bankBranchIdPermit').append(
                                         $('<option value="'+data.branchId+"~"+data.bankLedgerId+'" style="width:150px"></option>').val(data.branchId+"~"+data.bankLedgerId).html(data.bankName)
                                         )
                                      document.getElementById("bankBranchIdPermit").value = '<c:out value="${vendorIdPermit}"/>'
                                      });
                                  } else {
                                      $('#bankBranchIdPermit').empty();
                                  }
                              }
                                  });
                              }

                        function getPermitBankPayment(val){
                           var ownerShips=document.getElementById("ownerShips").value;
                           if(val == 2){
                             document.getElementById("bankPayment13").style.visibility = 'visible';
                             document.getElementById("bankPayment14").style.visibility = 'visible';
                             document.getElementById("bankPayment15").style.visibility = 'visible';
                             document.getElementById("bankPayment16").style.visibility = 'visible';
                           }
                           else if(val == 1){
                             document.getElementById("bankPayment13").style.visibility = 'hidden';
                             document.getElementById("bankPayment14").style.visibility = 'hidden';
                             document.getElementById("bankPayment15").style.visibility = 'hidden';
                             document.getElementById("bankPayment16").style.visibility = 'hidden';
                           }
                       }


                </script>

               <script type="text/javascript">

                   function updatePermitDetails() {
                       var vehicleId = $("#vehicleId").val();
                       var permitType = document.getElementById("permitType").value;
                       var permitNo = document.getElementById("permitNo").value;
                       var permitPaidDate = document.getElementById("permitPaidDate").value;
                       var permitAmount = document.getElementById("permitAmount").value;
                       var permitExpiryDate = document.getElementById("permitExpiryDate").value;
                       var permitRemarks = document.getElementById("permitRemarks").value;
                        var paymentTypePermit = document.getElementById("paymentTypePermit").value;
                        var permitid = document.getElementById("permitid").value;
                         var ledgerId = document.getElementById("vendorPermit").value;
                        var cashOnHandLedgerId = document.getElementById("cashOnHandLedgerId").value;
                        var cashOnHandLedgerCode = document.getElementById("cashOnHandLedgerCode").value;

                          var bankIdPermit = "0";
                             if(document.getElementById("bankIdPermit").value == null){
                                bankIdPermit = "0";
                            }
                            else{
                                bankIdPermit = document.getElementById("bankIdPermit").value;
                            }
                            var clearanceIdPermit = "0";
                            if(document.getElementById("clearanceIdPermit").value == null){
                                clearanceIdPermit = "0";
                            }
                            else{
                               clearanceIdPermit =document.getElementById("clearanceIdPermit").value;
                            }

                            var bankBranchIdPermit = "0";
                             if(document.getElementById("bankBranchIdPermit").value == null){
                                bankBranchIdPermit = "0";
                            }
                            else{
                                bankBranchIdPermit = document.getElementById("bankBranchIdPermit").value;
                            }
                            var chequeDatePermit = "0";
                            if(document.getElementById("chequeDatePermit").value == null){
                              chequeDatePermit = "";
                            }else{
                              chequeDatePermit = document.getElementById("chequeDatePermit").value;
                            }
                            var chequeNoPermit = "0";
                             if(document.getElementById("chequeNoPermit").value == null){
                              chequeNoPermit = "";
                            }else{
                              chequeNoPermit = document.getElementById("chequeNoPermit").value;
                            }
                            var chequeAmountPermit ="0";
                             if(document.getElementById("chequeAmountPermit").value == null){
                              chequeAmountPermit = "0";
                            }else{
                              chequeAmountPermit = document.getElementById("chequeAmountPermit").value;
                            }

                       if (document.addVehicle.permitNo.value == '') {
                           alert('Please Enter Vehicle PermitNo');
                           $("#permitNo").focus();
                           return;
                       } else if (document.addVehicle.permitPaidDate.value == '') {
                           alert('Please Enter Vehicle PermitPaidDate');
                           $("#permitPaidDate").focus();
                           return;
                       } else if (document.addVehicle.permitAmount.value == '') {
                           alert('Please Enter Vehicle PermitAmount');
                           $("#permitAmount").focus();
                           return;
                       } else if (document.addVehicle.permitExpiryDate.value == '') {
                           alert('Please Enter Vehicle PermitExpiryDate');
                           $("#permitExpiryDate").focus();
                           return;
                       }
                       var url = '';
                       url = './insertPermitDetails.do';
                       $.ajax({
                           url: url,
                           data: {permitType: permitType, permitNo: permitNo, permitPaidDate: permitPaidDate, permitAmount: permitAmount, permitExpiryDate: permitExpiryDate, vehicleId: vehicleId,
                                permitRemarks: permitRemarks,paymentTypePermit:paymentTypePermit,bankIdPermit:bankIdPermit,bankBranchIdPermit:bankBranchIdPermit,chequeDatePermit:chequeDatePermit,
                               chequeNoPermit:chequeNoPermit,chequeAmountPermit:chequeAmountPermit,clearanceIdPermit:clearanceIdPermit,
                               permitid:permitid,ledgerId:ledgerId,cashOnHandLedgerId:cashOnHandLedgerId,cashOnHandLedgerCode:cashOnHandLedgerCode
                           },
                           type: "GET",
                           success: function (response) {
                               vehicleId = response.toString().trim();
                               if (vehicleId == 0) {
                                   $("#StatusMsg").text("Vehicle Premit added failed ");
                               } else {
                                   $("#StatusMsg").text("Vehicle Permit added sucessfully ");
                               }

                           },
                           error: function (xhr, status, error) {
                           }
                       });

                       document.getElementById('permitNext').style.visibility = 'hidden';
                   }

                                                                                                                                                                                                        </script>
            </div>-->
<!--                    <div style="display:none;">
                   <div id="vehicleInsurane" class="tabContent" >
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="70%" id="bg" class="border">
                    <tr>
                        <td colspan="6"  height="30">
                            <div  align="center">Add Insurance</div>
                        </td>
                    </tr>
                    <tr>
                        <td>Policy</td>
                        <td>
                            <select class="form-control" name="insPolicy" id="insPolicy"  onchange="dispalyPackageType(this.value);" >
                                <option value="0">New</option>
                                <option value="1">Renewal</option>
                            </select>
                        </td>
                        <script>
                            document.getElementById("insPolicy").value = '<c:out value="${insPolicy}"/>'
                        </script>

                        <td style ="display:none" id ="labelPrevPolicy">Previous Policy No</td>
                        <td  id ="fieldPrevPolicy"   >
                            <input name="prevPolicy" id ="prevPolicy" style ="display:none" maxlength="25" type="text"  value="<c:out value="${prevPolicy}"/>" onkeypress="return blockNonNumbers(this, event, true, false);">
                        </td>
                        <script>
                            document.getElementById("prevPolicy").value = '<c:out value="${prevPolicy}"/>'
                        </script>
                    </tr>
                    <tr>
                        <td>Insurance  Type :</td>
                        <td>
                            <select class="form-control" name="packageType" id="packageType"  >
                                <option value="1">Comprehensive Policy</option>
                                <option value="2">Third Party Claim</option>
                            </select>
                        </td>
                        <script>
                            document.getElementById("packageType").value = '<c:out value="${packageType}"/>'
                        </script>
                    </tr>
                    <td colspan="6"  height="30">
                        <div  align="center">Owner Details</div>
                    </td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>
                            <input type="text" name="insName" id="insName" class="form-control"  value=''></td>
                        <td>Address</td>
                    <td  colspan="3"><textArea id="insAddress" name="insAddress"  cols="20" rows="2"></textArea></td>
                    </tr>
                    <tr>
                        <td>Mobile No</td>
                        <td>
                        <input type="text" name="insMobileNo" id="insMobileNo"  class="form-control" value='' onkeypress="return blockNonNumbers(this, event, true, false);"></td>
                    </tr>
                    <td colspan="6"  height="30">
                        <div  align="center">Insurance Details</div>
                    </td>
                    </tr>
                    <tr>
                        <td>Policy No</td>
                        <td><input name="insPolicyNo" id="insPolicyNo"  maxlength="45" type="text" class="form-control" value=''></td>
                        <td>IDV Value :</td>
                        <td><input name="insIdvValue" id="insIdvValue"  maxlength="7"  type="text" class="form-control" value='' onkeypress="return blockNonNumbers(this, event, true, false);"></td>

                    </tr>

                    <tr>
                        <td><font color="red">*</font>Valid From Date</td>

                       <td ><input name="fromDate1" id="fromDate1" type="text" class="form-control" id="fromDate1"  date-format="dd-mm-yyyy"  value='<c:out value="${toDate1}"/>' ></td>
                        <td><font color="red">*</font>Valid To Date</td>
                   <td ><input name="toDate1" id="toDate1" type="text" class="form-control" id="toDate1"  date-format="dd-mm-yyyy"  value=''   ></td>
                    </tr>

                    <tr >
                        <input type="hidden" id="insuranceid" name="insuranceid" value='<c:out value="${insuranceid}"/>'>
                        <input type="hidden" id="clearanceId" name="clearanceId" value='<c:out value="${clearanceId}"/>'>
                        <input type="hidden" id="insuranceid" name="insuranceid" value=''>
                        <input type="hidden" id="clearanceId" name="clearanceId" value=''>
                        <input type="hidden" id="cashOnHandLedgerId" name="cashOnHandLedgerId" value='<%=ThrottleConstants.CashOnHandLedgerId%>'>
                        <input type="hidden" id="cashOnHandLedgerCode" name="cashOnHandLedgerCode" value='<%=ThrottleConstants.CashOnHandLedgerCode%>'>
                        <input text="hidden" id="vendorTypeId" name="vendorTypeId" value=''>
                        <td>Premium Amount</td>
                        <td><input name="premiunAmount" id="premiunAmount"  maxlength="7" type="text" class="form-control" value='' onkeypress="return blockNonNumbers(this, event, true, false);" onchange="premiumAmountSet(this.value);" ></td>
                    <td id="paymentDateIns" style="visibility: hidden;"><font color="red">*</font>Payment Date</td>
                        <td id="paymentDateIns1" style="visibility: hidden;" height="30"><input name="paymentDate" id="paymentDate" type="text" class="datepicker"  value='' ></td>
                   </tr>
                    <tr>
                      <td id="vendorIns" style="visibility: hidden;">Vendor Name</td>
                        <td  id="vendorIns1" style="visibility: hidden;"><select name="vendorId1" id="vendorId1" class="form-control" height:20px;">
<c:if test="${vendorList != null}">
                                <option value="0" selected>--Select--</option>
    <c:forEach items="${vendorList}" var="vendor">
                                    <option value='<c:out value="${vendor.vendorId}"/>~<c:out value="${vendor.ledgerId}"/>~<c:out value="${vendor.ledgerCode}"/>'><c:out value="${vendor.vendorName}"/></option>
    </c:forEach>
</c:if>
                            </select>
                        <script>
//                            alert('<c:out value="${bankBranchIdIns}"/>')
                            document.getElementById("vendorId1").value = '<c:out value="${bankBranchIdIns}"/>'
                        </script>
                            </td>

                        <td id="bankPaymentModeIns" style="visibility: hidden;">Payment Mode:</td>
                        <td id="bankPaymentModeIns1" style="visibility: hidden;">
                    <select class="form-control" name="paymentType" id="paymentType"  onchange="getBankPayment(this.value)" >
                                <option value="0">----select----</option>
                                <option value="1">Cash Payment</option>
                                <option value="2">Bank Payment</option>
                            </select>
                        </td>
                    <script>
                        document.getElementById("paymentType").value = '<c:out value="${paymentTypeIns}"/>'
                    </script>
                    </tr>
                    <tr >
                   <table align="center" border="0" cellpadding="0" cellspacing="0" width="70%" id="bg" class="border">
<%--<c:if test="${clearanceId == '' || clearanceId == null }">--%>
                   <tr id="bankPayment1" style="visibility: block;">
                     <td colspan="6"  height="30">
                        <div   align="center">Bank Payment Details</div>
                    </td>
                      </tr>
                   <tr id="bankPayment2" style="visibility: block;">


                        <td>Bank Head</td>
                        <td><select name="bankId" id="bankId" class="form-control"  onchange="getBankBranch(this.value)" height:20px;">
<c:if test="${primarybankList != null}">
                                <option value="0" selected>--Select--</option>
    <c:forEach items="${primarybankList}" var="bankVar">
                                    <option value='<c:out value="${bankVar.primaryBankId}"/>'><c:out value="${bankVar.primaryBankName}"/></option>

    </c:forEach>
</c:if>
                            </select>
                        <script>
                            document.getElementById("bankId").value = '<c:out value="${bankId}"/>'
                        </script>
                        </td>
                        <td>Bank branch :</td>
                        <td> <select name="bankBranchId" id="bankBranchId" class="form-control" height:20px;">
<c:if test="${bankBranchList != null}">
                                <option value="0" selected>--Select--</option>
    <c:forEach items="${bankBranchList}" var="branch">
                                    <option value='<c:out value="${branch.branchId}"/>'><c:out value="${branch.bankname}"/></option>
    </c:forEach>
</c:if>
                            </select>
                        <script>
                            document.getElementById("bankBranchId").value ='<c:out value="${bankBranchId}"/>'
                        </script>
                        </td>

                    </tr>

                    <tr id="bankPayment3" style="visibility: block;">
                        <td><font color="red">*</font>Cheque Date</td>
                        <td><input name="chequeDate" id="chequeDate" type="text" class="datepicker"  value='' ></td>
                        <td ><font color="red">*</font>Cheque No</td>
                        <td height="30"><input name="chequeNo" id="chequeNo"  maxlength="45" type="text" class="form-control" value=''></td>
                    </tr>
                    <tr id="bankPayment4" style="visibility: block;">
                        <td>Cheque Amount</td>
                        <td><input name="chequeAmount" id="chequeAmount"  maxlength="7" type="text" class="form-control" value=''  readonly></td>

                    <td>Clearance Date</td>
                        <td height="30"><input name="clearanceDate" id="clearanceDate" type="text" class="datepicker" value='<c:out value="${clearanceDate}"/>' ></td>
                    </tr>
                   </table>
                    </tr>

<%--</c:if>--%>
                <tr>
              <br>

<%
            int index1 = 1;

%>

<c:if test="${listId == '1' }" >

    <c:if test="${VehicleInsList == null }" >
                <br>
                <center><font color="red" size="2"> no records found </font></center>
    </c:if>
    <c:if test="${VehicleInsList != null }" >
                <table align="center" width="70%" cellpadding="0" cellspacing="0"  class="border">

                    <tr>

                        <td class="contentsub" height="30"><div class="contentsub">Sno</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">Vehicle Number</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">Company Name</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">Premium No</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">Premiumpaid Date</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">&nbsp;</div></td>
                    </tr>
        <%
            String style = "text1";%>
        <c:forEach items="${VehicleInsList}" var="veh" >
            <%
if ((index1 % 2) == 0) {
    style = "text1";
} else {
    style = "text2";
}%>
                        <tr>
                            <td class="<%= style %>" height="30" style="padding-left:30px; "> <%= index1 %> </td>
                            <td class="<%= style %>" height="30" style="padding-left:30px; "> <c:out value="${veh.regNo}" /></td>
                            <td class="<%= style %>" height="30" style="padding-left:30px; "><c:out value="${veh.companyname}" /></td>
                            <td class="<%= style %>" height="30" style="padding-left:30px; "><c:out value="${veh.premiumno}" /></td>
                            <td class="<%= style %>" height="30" style="padding-left:30px; "><c:out value="${veh.premiumpaiddate}" /></td>
                            <td class="<%= style %>" height="30" style="padding-left:30px; "><a href='/throttle/vehicleInsuranceDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />'  >alter </a> </td>
                            <td class="<%= style %>" height="30" style="padding-left:30px; "><a href='/throttle/vehicleInsuranceDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&insuranceid=<c:out value="${veh.insuranceid}" />'>alter </a> </td>
                        </tr>
            <% index1++; %>
        </c:forEach>
                </table>
    </c:if>
</c:if>
            <br>
                     </tr>
                         <script>
                             function premiumAmountSet(val){
                               document.getElementById("chequeAmount").value =val;
                              }
                   function getBankBranch(bankId) {
                              var temp = "";
                                  $.ajax({
                                      url: '/throttle/getBankBranchDetails.do',
                                      data: {bankId: bankId},
                                      dataType: 'json',
                                       success: function (temp) {
//                                             alert(url);
                                  if (temp != '') {
                                      $('#bankBranchId').empty();
                                      $('#bankBranchId').append(
                                              $('<option style="width:150px"></option>').val(0+"~"+0+"~"+0).html('---Select----')
                                              )
                                      $.each(temp, function (i, data) {
                                          $('#bankBranchId').append(
                                         $('<option value="'+data.branchId+"~"+data.bankLedgerId+"~"+data.bankLedgerCode+'" style="width:150px"></option>').val(data.branchId+"~"+data.bankLedgerId+"~"+data.bankLedgerCode).html(data.bankName)
                                         )
                                          document.getElementById("bankBranchId").value ='<c:out value="${vendorId1}"/>'
                                      });
                                  } else {
                                      $('#bankBranchId').empty();
                                  }
                              }
                                  });
                              }

                       function getBankPayment(val){
                           var ownerShips=document.getElementById("ownerShips").value;
                           if(val == 2){
                             document.getElementById("bankPayment1").style.visibility = 'visible';
                             document.getElementById("bankPayment2").style.visibility = 'visible';
                             document.getElementById("bankPayment3").style.visibility = 'visible';
                             document.getElementById("bankPayment4").style.visibility = 'visible';
                           }
                           else if(val == 1 ){
                             document.getElementById("bankPayment1").style.visibility = 'hidden';
                             document.getElementById("bankPayment2").style.visibility = 'hidden';
                             document.getElementById("bankPayment3").style.visibility = 'hidden';
                             document.getElementById("bankPayment4").style.visibility = 'hidden';
                           }
                       }

                       function checkPaymentMode() {
                           var ownerShips=document.getElementById("ownerShips").value;
                           if(ownerShips == "1"){

                               document.getElementById("paymentDateIns").style.visibility = 'visible';
                               document.getElementById("paymentDateIns1").style.visibility = 'visible';
                               document.getElementById("vendorIns").style.visibility = 'visible';
                               document.getElementById("vendorIns1").style.visibility = 'visible';
//                               document.getElementById("vendorRT").style.visibility = 'visible';
//                               document.getElementById("vendorRT1").style.visibility = 'visible';
                               document.getElementById("vendorFC").style.visibility = 'visible';
                               document.getElementById("vendorFC1").style.visibility = 'visible';
//                               document.getElementById("vendorPermits").style.visibility = 'visible';
//                               document.getElementById("vendorPermit1").style.visibility = 'visible';
                               document.getElementById("bankPaymentModeIns").style.visibility = 'visible';
                               document.getElementById("bankPaymentModeIns1").style.visibility = 'visible';
//                               document.getElementById("bankPaymentModeRT").style.visibility = 'visible';
//                               document.getElementById("bankPaymentModeRT1").style.visibility = 'visible';
                               document.getElementById("bankPaymentModeFC").style.visibility = 'visible';
                               document.getElementById("bankPaymentModeFC1").style.visibility = 'visible';
                               document.getElementById("bankPaymentDateFC").style.visibility = 'visible';
                               document.getElementById("bankPaymentDateFC1").style.visibility = 'visible';
//                               document.getElementById("bankPaymentModePermit").style.visibility = 'visible';
//                               document.getElementById("bankPaymentModePermit1").style.visibility = 'visible';
                               document.getElementById("bankPayment1").style.visibility = 'visible';
                             document.getElementById("bankPayment2").style.visibility = 'visible';
                             document.getElementById("bankPayment3").style.visibility = 'visible';
                             document.getElementById("bankPayment4").style.visibility = 'visible';

                              document.getElementById("bankPayment9").style.visibility = 'visible';
                             document.getElementById("bankPayment10").style.visibility = 'visible';
                             document.getElementById("bankPayment11").style.visibility = 'visible';
                             document.getElementById("bankPayment12").style.visibility = 'visible';

                           }else{
                               document.getElementById("paymentDateIns").style.visibility = 'hidden';
                               document.getElementById("paymentDateIns1").style.visibility = 'hidden';
                               document.getElementById("vendorIns").style.visibility = 'hidden';
                               document.getElementById("vendorIns1").style.visibility = 'hidden';
//                               document.getElementById("vendorRT").style.visibility = 'hidden';
//                               document.getElementById("vendorRT1").style.visibility = 'hidden';
                               document.getElementById("vendorFC").style.visibility = 'hidden';
                               document.getElementById("vendorFC1").style.visibility = 'hidden';
//                               document.getElementById("vendorPermits").style.visibility = 'hidden';
//                               document.getElementById("vendorPermit1").style.visibility = 'hidden';
                               document.getElementById("bankPaymentModeIns").style.visibility = 'hidden';
                               document.getElementById("bankPaymentModeIns1").style.visibility = 'hidden';
//                               document.getElementById("bankPaymentModeRT").style.visibility = 'hidden';
//                               document.getElementById("bankPaymentModeRT1").style.visibility = 'hidden';
                               document.getElementById("bankPaymentModeFC").style.visibility = 'hidden';
                               document.getElementById("bankPaymentModeFC1").style.visibility = 'hidden';
                               document.getElementById("bankPaymentDateFC1").style.visibility = 'hidden';
                               document.getElementById("bankPaymentDateFC").style.visibility = 'hidden';
//                               document.getElementById("bankPaymentModePermit").style.visibility = 'hidden';
//                               document.getElementById("bankPaymentModePermit1").style.visibility = 'hidden';
                             document.getElementById("bankPayment1").style.visibility = 'hidden';
                             document.getElementById("bankPayment2").style.visibility = 'hidden';
                             document.getElementById("bankPayment3").style.visibility = 'hidden';
                             document.getElementById("bankPayment4").style.visibility = 'hidden';

                              document.getElementById("bankPayment9").style.visibility = 'hidden';
                             document.getElementById("bankPayment10").style.visibility = 'hidden';
                             document.getElementById("bankPayment11").style.visibility = 'hidden';
                             document.getElementById("bankPayment12").style.visibility = 'hidden';
                           }



                     }
                    </script>

                    <script type="text/javascript">

                        var ar_ext = ['pdf', 'txt', 'gif', 'jpeg', 'jpg', 'png', 'Gif', 'GIF', 'Png', 'PNG', 'JPG', 'Jpg'];        // array with allowed extensions

                        function checkName(sno) {
                            var name = document.getElementById("podFile" + sno).value;
                            var ar_name = name.split('.');

                            var ar_nm = ar_name[0].split('\\');
                            for (var i = 0; i < ar_nm.length; i++)
                                var nm = ar_nm[i];

                            var re = 0;
                            for (var i = 0; i < ar_ext.length; i++) {
                                if (ar_ext[i] == ar_name[1]) {
                                    re = 1;
                                    break;
                                }
                            }

                            if (re == 1) {
                            } else {
                                alert('".' + ar_name[1] + '" is not an file type allowed for upload');
                                document.getElementById("podFile" + sno).value = '';
                            }
                        }


                        function dispalyPackageType(val) {
                            //                            var vendorTypeId = document.addVehicle.ins_policy.value
                            //        alert(vendorTypeId);
                            if (val == 1) {
                                document.getElementById("labelPrevPolicy").style.display = "block";
                                document.getElementById("prevPolicy").style.display = "block";
                            } else {
                                document.getElementById("labelPrevPolicy").style.display = "none";
                                document.getElementById("prevPolicy").style.display = "none";
                            }
                        }

                        function updateInsuranceDetails() {

                            var insuranceid = document.getElementById("insuranceid").value;
                            var vehicleId = $("#vehicleId").val();
                            var insPolicy = document.getElementById("insPolicy").value;
                            var prevPolicy = document.getElementById("prevPolicy").value;
                            var packageType = document.getElementById("packageType").value;
                            var insName = document.getElementById("insName").value;
                            var insAddress = document.getElementById("insAddress").value;
                            var insMobileNo = document.getElementById("insMobileNo").value;
                            var insPolicyNo = document.getElementById("insPolicyNo").value;
                            var insIdvValue = document.getElementById("insIdvValue").value;
                            var fromDate1 = document.getElementById("fromDate1").value;
                            var toDate1 = document.getElementById("toDate1").value;
                            var premiunAmount = document.getElementById("premiunAmount").value;
                            var paymentType = document.getElementById("paymentType").value;
                            var paymentDate = document.getElementById("paymentDate").value;
                            var ledgerId = document.getElementById("vendorId1").value;

                            if (document.addVehicle.insPolicy.value == '') {
                                alert('Please Enter Insurance Policy ');
                                $("#insPolicy").focus();
                                return;
                            } else if (document.addVehicle.insName.value == '') {
                                alert('Please Enter Vehicle Insurance Name');
                                $("#insName").focus();
                                return;
                            } else if (document.addVehicle.insAddress.value == '') {
                                alert('Please Enter Vehicle Insurance Address');
                                $("#insAddress").focus();
                                return;
                            } else if (document.addVehicle.insMobileNo.value == '') {
                                alert('Please Enter Vehicle Insurance MobileNo');
                                $("#insMobileNo").focus();
                                return;
                            } else if (document.addVehicle.insPolicyNo.value == '') {
                                alert('Please Enter Vehicle Insurance PolicyNo');
                                $("#insPolicyNo").focus();
                                return;
                            } else if (document.addVehicle.insIdvValue.value == '') {
                                alert('Please Enter Vehicle Insurance IdvValue');
                                $("#insIdvValue").focus();
                                return;
                            } else if (document.addVehicle.fromDate1.value == '') {
                                alert('Please Enter Vehicle Insurance Valid FromDate');
                                $("#fromDate1").focus();
                                return;
                            } else if (document.addVehicle.toDate1.value == '') {
                                alert('Please Enter Vehicle Insurance Valid Todate');
                                $("#toDate1").focus();
                                return;
                            } else if (document.addVehicle.premiunAmount.value == '') {
                                alert('Please Enter Vehicle Insurance Premium Amount');
                                $("#premiunAmount").focus();
                                return;
                            }


                            var clearanceId = "0";
                            if(document.getElementById("clearanceId").value == null){
                                clearanceId = "0";
                            }
                            else{
                               clearanceId =document.getElementById("clearanceId").value;
                            }

                            var bankId = "0";
                             if(document.getElementById("bankId").value == null){
                                bankId = "0";
                            }
                            else{
                                bankId = document.getElementById("bankId").value;
                            }
                            var bankBranchId = document.getElementById("bankBranchId").value;
                             if(document.getElementById("bankBranchId").value == null){
                                bankBranchId = "0";
                            }
                            else{
                                bankBranchId = document.getElementById("bankBranchId").value;
                            }

                            var chequeDate = "0";
                            if(document.getElementById("chequeDate").value == null){
                              chequeDate = "";
                            }else{
                              chequeDate = document.getElementById("chequeDate").value;
                            }
                            var chequeNo = "0";
                             if(document.getElementById("chequeNo").value == null){
                              chequeNo = "";
                            }else{
                              chequeNo = document.getElementById("chequeNo").value;
                            }
                            var chequeAmount ="0";
                             if(document.getElementById("chequeAmount").value == null){
                              chequeAmount = "0";
                            }else{
                              chequeAmount = document.getElementById("chequeAmount").value;
                            }
                            var cashOnHandLedgerId = document.getElementById("cashOnHandLedgerId").value;
                            var cashOnHandLedgerCode = document.getElementById("cashOnHandLedgerCode").value;
//                             if (document.addVehicle.bankId.value == '') {
//                                alert('Please Enter Bank Name ');
//                                $("#bankId").focus();
//                                return;
//                            } else if (document.addVehicle.bankBranchId.value == '') {
//                                alert('Please Enter bankBranch Name');
//                                $("#bankBranchId").focus();
//                                return;
//                            } else if (document.addVehicle.chequeDate.value == '') {
//                                alert('Please Enter Insurance chequeDate');
//                                $("#chequeDate").focus();
//                                return;
//                            }else if (document.addVehicle.chequeNo.value == '') {
//                                alert('Please Enter cheque No');
//                                $("#chequeNo").focus();
//                                return;
//                            }else if (document.addVehicle.chequeAmount.value == '') {
//                                alert('Please Enter cheque Amount');
//                                $("#chequeAmount").focus();
//                                return;
//
//                            }

                            var url = '';
                            url = './updateInsuranceDetails.do';
                            $.ajax({
                                url: url,
                                data: {insPolicy: insPolicy, prevPolicy: prevPolicy, packageType: packageType, insName: insName, insAddress: insAddress, vehicleId: vehicleId,
                                    insMobileNo: insMobileNo, insPolicyNo: insPolicyNo, insIdvValue: insIdvValue, fromDate1: fromDate1, toDate1: toDate1, premiunAmount: premiunAmount,
                                    paymentType:paymentType,bankId:bankId,bankBranchId:bankBranchId,chequeDate:chequeDate,chequeNo:chequeNo,chequeAmount:chequeAmount,
                                    clearanceId:clearanceId,insuranceid:insuranceid,ledgerId:ledgerId,cashOnHandLedgerId:cashOnHandLedgerId,cashOnHandLedgerCode:cashOnHandLedgerCode,paymentDate:paymentDate,
                                   fleetTypeId:1
                                },
                                type: "GET",
                                success: function (response) {
                                    vehicleId = response.toString().trim();
                                    if (vehicleId == 0) {
                                        $("#StatusMsg").text("Vehicle Insurance added failed ");
                                    } else {
                                        insertStatus = vehicleId;
                                        $("#StatusMsg").text("Vehicle Insurance added sucessfully ");
                                    }
                                },
                                error: function (xhr, status, error) {
                                }
                            });
                            document.getElementById("insuranceNext").style.visibility = 'hidden';
                        }
                     </script>




                </table>
                                      <br>
                                      <br>
                                      <br>
                <center>

                    <input type="button" class="button" value="Save" name="Save" id="insuranceNext" onClick="updateInsuranceDetails();" style="display: none"/>
                        <a  class="nexttab" ><input type="button" class="button" value="Next" name="Next" /></a>

                </center>

            </div>
             </div>-->
                                      <!--            <div id="vehicleDetailsConfirm">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" id="bg" class="border">
                    <tr>
                        <td >Permit Type</td>
                        <td >

                            <select name="permitType" id="permitType" class="form-control" style="width:100px;">
                                <option value="State permit" selected>State permit</option>
                                <option value="National permit">National permit</option>
                            </select>
                        </td>
                        <td >Permit No</td>
                        <td ><input type="text" name="permitNo" id="permitNo" class="form-control" value='' ></td>
                    </tr>
                    <tr>
                        <td >Permit Paid Date</td>
                        <td ><input type="text" name="permitPaidDate" id="permitPaidDate" class="datepicker"  value='' ></td>
                        <td >Permit Amount</td>
                        <td ><input type="text" name="permitAmount" id="permitAmount"  class="form-control"  value='' ></td>
                    </tr>
                    <tr>
                        <td >Permit Expiry Date</td>
                        <td ><input type="text" name="permitExpiryDate" id="permitExpiryDate" class="datepicker"  value='' ></td>
                        <td >remarks</td>
                        <td ><input type="text" name="remarks" id="remarks"  class="form-control"  value='' ></td>
                    </tr>
                </table>
                <br>
                <br>
                <center>
                    <input type="button" class="button" name="Save" value="Save" onclick="submitPage2(this.name);"/>
                </center>
            </div>-->
