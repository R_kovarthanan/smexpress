<%--
    Document   : vehicleReplacement
    Created on : Oct 29, 2013, 11:32:08 AM
    Author     : srinivasan
--%>

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        $(".datepicker").datepicker({
            changeMonth: true, changeYear: true, dateFormat: 'dd-mm-yy'
        });
    });
</script>

<script>


    function ajaxData() {
        var vName = document.getElementById("vName").value;
        var vendorId = vName.split("~");
        $.ajax({
            url: "/throttle/getRegNoAjax.do",
            dataType: "json",
            data: {
                vName: parseInt(vendorId[0])
            },
            success: function(data) {
                if (data != '') {
                    $('#eRegNo').empty();
                    $('#eRegNo').append(
                            $('<option></option>').val(0).html('--select--'))
                    $.each(data, function(i, data) {
                        $('#eRegNo').append(
                                $('<option style="width:90px"></option>').attr("value", data.Name).text(data.Name)
                                )
                    });
                } else {
                    $('#eRegNo').empty();
                    $('#eRegNo').append(
                            $('<option></option>').val(0).html('--select--'))
                }
            }
        });
    }
    function ajaxData1() {


        var vName = document.getElementById("vName").value;
        $.ajax({
            url: "/throttle/getRegNos.do",
            dataType: "json",
            data: {
                vName: vName
            },
            success: function(data) {
                // alert(data);
                if (data != '') {
                    $('#rRegNo').empty();
                    $('#rRegNo').append(
                            $('<option></option>').val(0).html('--select--'))
                    $.each(data, function(i, data) {
                     //   alert(data.Id);
                        $('#rRegNo').append(
                                $('<option style="width:90px"></option>').attr("value", data.Name).text(data.Name)
                                )
                    });
                } else {
                    $('#rRegNo').empty();
                    $('#rRegNo').append(
                            $('<option></option>').val(0).html('--select--'))
                }
            }
        });
    }

</script>

<script type="text/javascript">
    function submitPage()
    {

        document.cubicWeight.action = "/throttle/insertVehicleReplacementPage.do";
        document.cubicWeight.submit();
    }
    function setValues(sno, vName, cType, eRegNo, rRegNo, rDate) {
        alert("the");
        var count = parseInt(document.getElementById("count").value);
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("vName").value = vName;
        document.getElementById("cType").value = cType;
        document.getElementById("eRegNo").value = eRegNo;
        document.getElementById("rRegNo").value = rRegNo;
        document.getElementById("rDate").value = rDate;
        document.cubicWeight.action = "/throttle/updateVehicleReplacementPage.do";
        document.cubicWeight.submit();

    }
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body onload="document.cubicWeight.lengthInCm.focus();">


        <form name="cubicWeight"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>



            <table  border="0" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                <tr>
                    <td><font color="red"></font>Vendor Name</td>
                    <td>
                        <select class="form-control" name="vName" id="vName"  onchange="ajaxData(), ajaxData1();" style="width:125px;">
                            <c:if test = "${vendorList != null}" >
                                <c:forEach items="${vendorList}" var="vendor">
                                    <option value='<c:out value="${vendor.vendorId}"/>~<c:out value="${vendor.vendorName}"/>'> <c:out value="${vendor.vendorName}"/> </option>
                                </c:forEach>
                            </c:if>
                        </select>
                    </td>
                    <td class="text1">Contract Type</td>
                    <td>
                        <select name="cType" id="cType" class="form-control" maxlength="5"   onchange="showOrderType();" >
                             <option value="Dedicate">Dedicate</option>
                              <option value="Demand">Demand</option>
                             <option value="3">--Select---</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="text2">Existing Vehicle Reg No</td>
                    <td>
                        <select class="form-control" name="eRegNo" id="eRegNo"  style="width:125px;"></select>
                    </td>
                    <td class="text2">Replacement Vehicle Reg No</td>

                    <td height="30">
                        <select class="form-control" name="rRegNo" id="rRegNo" style="width:125px;">
                        </select>
                    </td>

                </tr>
                <tr>
                    <td class="text3">Replacement Date</td>
                    <td class="text3"><input type="text" name="rDate" id="rDate" class="datepicker"/></td>
                </tr>
                <table  border="0" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                    <tr>
                        <td>
                            <br>
                            <center>
                                <input type="button" class="button" name="save" value="Save" onClick="submitPage();"/>
                            </center>
                        </td>
                    </tr>
                </table>
            </table>
            <br>


            <h2 align="center">Vehicle Replacement List</h2>


            <table width="815" align="center" border="0" id="table" class="sortable">

                <thead>

                    <tr height="30">
                        <th><h3>S.No</h3></th>
                        <th><h3>Vendor Name</h3></th>
                        <th><h3>Contract Type</h3></th>
                        <th><h3>Existing Vehicle RegNo</h3></th>
                        <th><h3>Replaced Vehicle RegNo</h3></th>
                        <th><h3>Replaced Date</h3></th>


                    </tr>
                </thead>
                <tbody>


                    <% int sno = 0;%>
                    <% int sno1 = 1;%>
                    <c:if  test = "${vehicleReplacementList != null}">
                        <c:forEach items="${vehicleReplacementList}" var="vehicleReplacement">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td class="<%=className%>"  align="left"> <%= sno1%> </td>
                                <td class="<%=className%>"  align="left"> <c:out value="${vehicleReplacement.vName}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${vehicleReplacement.cType}" /></td>
                                <td class="<%=className%>"  align="left"><c:out value="${vehicleReplacement.eRegNo}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${vehicleReplacement.rRegNo}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${vehicleReplacement.rDate}" /></td>

                            </tr>
                            <%sno1++;%>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>

            <input type="hidden" name="count" id="count" value="<%=sno%>" />

            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>