<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>

<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<style>
    #index td {
        color:white;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ViewCompany" text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="default text"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="default text"/></a></li>
            <li class=""><spring:message code="hrms.label.ViewCompany" text="default text"/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <c:if test="${jcList != null}">
                <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
                        setValues();
                        getVehicleNos();">
                </c:if>

            <body>
                <form action="/throttle/viewAlterScreen.do" method="post">

                    <c:if test="${companyDetail!=null}">
                        <c:forEach items="${companyDetail}" var="comp" >
                            <table class="table table-info mb30 table-hover">
                                <thead>
                                                                <tr >
                                                                    <th colspan="4"> <spring:message code="hrms.label.Viewcompany" text="View Company"/></th>
                                                                </tr>

                                <tr >
                                    <td ><spring:message code="hrms.label.CompanyType" text="default text"/></td>
                                    <td ><c:out value="${comp.companyType}" /></td>
<!--                                </tr>
                                <tr >-->
                                    <td ><spring:message code="hrms.label.Name" text="default text"/></td>
                                    <td ><c:out value="${comp.name}"></c:out> </td>
                                <input type="hidden" name="cmpId" value=<c:out value="${comp.cmpId}"/>  >
                                </tr>
                                <tr >
                                    <td ><spring:message code="hrms.label.Address" text="default text"/></td>
                                    <td ><c:out value="${comp.address}"></c:out></td>
<!--                                    </tr>

                                    <tr >-->
                                        <td ><spring:message code="hrms.label.City" text="default text"/></td>
                                    <td ><c:out value="${comp.city}"></c:out></td>
                                    </tr>
                                    <tr >
                                        <td ><spring:message code="hrms.label.District" text="default text"/></td>
                                    <td ><c:out value="${comp.district}"/></td>
<!--                                </tr>
                                <tr >-->
                                    <td ><spring:message code="hrms.label.State" text="default text"/></td>
                                    <td ><c:out value="${comp.state}"/></td>
                                </tr>
                                <tr >
                                    
                                    <td ><spring:message code="hrms.label.Phone" text="default text"/>1</td>
                                    <td ><c:out value="${comp.phone1}"/></td>
<!--                                </tr>
                                <tr >-->
                                    <td ><spring:message code="hrms.label.Pincode" text="default text"/></td>
                                    <td ><c:out value="${comp.pinCode}"/></td>
                                </tr>
                                <tr >
                                    <td ><spring:message code="hrms.label.Phone" text="default text"/>2</td>
                                    <td ><c:out value="${comp.phone2}"/></td>
<!--                                </tr>
                                <tr >-->
                                    <td ><spring:message code="hrms.label.Fax" text="default text"/></td>
                                    <td ><c:out value="${comp.fax}"/></td>
                                </tr>
                                <tr >
                                    <td ><spring:message code="hrms.label.Email" text="default text"/></td>
                                    <td ><c:out value="${comp.email}"/></td>
<!--                                </tr>
                                <tr >-->
                                    <td ><spring:message code="hrms.label.Web" text="default text"/></td>
                                    <td ><c:out value="${comp.web}"/></td>
                                </tr>
                                <tr >
                                    <td ><spring:message code="hrms.label.Status" text="default text"/></td>
                                    <td ><c:out value="${comp.status}"/></td>
                                </tr><tr>
                                <td class="text2"><spring:message code="hrms.label.Billing State" text="Billing State"/></td>
                                       <td class="text2">
                                           <c:out value="${comp.stateName}"/>
                                    </td>
   <td class="text1"><spring:message code="hrms.label.Organization Type" text="Organization Type"/></td>
                                    <td class="text1"><c:out value="${comp.organizationName}"/></td>
                                </tr>
                                <tr>
   <td class="text1"><spring:message code="hrms.label.GST NO" text="GST No"/></td>
                                    <td class="text1" ><c:out value="${comp.gstNo}"/></td>
   <td class="text1"><spring:message code="hrms.label.Pan No" text="Pan No"/></td>
                                    <td class="text1" ><c:out value="${comp.panNo}"/></td>
                                </tr>
                                </thead>
                            </table>
                        </c:forEach>    
                    </c:if>    
                    <br>
                    <center><input type="submit" class="btn btn-success" value="<spring:message code="hrms.label.Alter" text="Alter"/>" /></center>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
