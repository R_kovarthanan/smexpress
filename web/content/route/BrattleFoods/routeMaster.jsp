<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCVpQCXyHbal5NLsAdEUP5OY8myKMaXefg&signed_in=true&libraries=places"></script>-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>
<script  type="text/javascript" src="js/jq-ac-script.js"></script>

<script>
    function calculateDistance1() {
        var fromCity = document.getElementById("cityFromId").value;
        var toCity = document.getElementById("cityToId").value;
//        alert("fromCity="+fromCity)
//        alert("toCity="+toCity)
        if (fromCity != '' && toCity != '') {
            var distanceValue = distance(document.getElementById("cityFromLat").value, document.getElementById("cityFromLon").value,
                    document.getElementById("cityToLat").value, document.getElementById("cityToLon").value, "K");
//                    alert("A="+document.getElementById("cityFromLat").value)
//                    alert("B="+document.getElementById("cityFromLon").value)
//                    alert("C="+document.getElementById("cityToLat").value)
//                    alert("D="+document.getElementById("cityToLat").value)

            alert(distanceValue);
            document.getElementById("distance").value = Math.ceil(distanceValue);
            calcFuleCostPerKm();

        }
//        else {
//            alert("data not adequate");
//        }
    }
    function distance(lat1, lon1, lat2, lon2, unit) {
//        alert(lat1);
//        alert(lon1);
//        alert(lat2);
//        alert(lon2);
//        var radlat1 = Math.PI * lat1 / 180
//        var radlat2 = Math.PI * lat2 / 180
//        var radlon1 = Math.PI * lon1 / 180
//        var radlon2 = Math.PI * lon2 / 180
//        var theta = lon1 - lon2
//        //alert(theta);
//        var radtheta = Math.PI * theta / 180
//        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
//        dist = Math.acos(dist)
//        dist = dist * 180 / Math.PI
//        dist = dist * 60 * 1.1515
//        if (unit == "K") {
//            dist = dist * 1.609344
//        }
//        if (unit == "N") {
//            dist = dist * 0.8684
//        }
//        return dist

//        New
        var R = 6371; // Radius of the earth in km
        var dLat = deg2rad(lat2 - lat1);  // deg2rad below
        var dLon = deg2rad(lon2 - lon1);
        var angle =
                Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                Math.sin(dLon / 2) * Math.sin(dLon / 2);

        var c = 2 * Math.atan2(Math.sqrt(angle), Math.sqrt(1 - angle));
        var dist = R * c; // Distance in km
        return dist
    }
    function deg2rad(deg) {
        return deg * (Math.PI / 180)
    }


    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#cityFrom').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCityFromName.do",
                    dataType: "json",
                    data: {
                        cityFrom: request.term,
                        cityToId: document.getElementById('cityToId').value

                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#cityFromId').val('');
                            $('#cityFrom').val('');
                            $('#cityToId').val('');
                            $('#cityTo').val('');
                        } else {
                            response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#cityFromId').val(tmp[0]);
                $('#cityFrom').val(tmp[1]);
                $('#cityFromLat').val(tmp[2]);
                $('#cityFromLon').val(tmp[3]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

        $('#cityTo').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCityToName.do",
                    dataType: "json",
                    data: {
                        cityTo: request.term,
                        cityFromId: document.getElementById('cityFromId').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#cityToId').val('');
                            $('#cityTo').val('');
//                            $('#cityFromId').val('');
//                            $('#cityFrom').val('');
                        } else {
                            response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#cityToId').val(tmp[0]);
                $('#cityTo').val(tmp[1]);

                $('#cityToLat').val(tmp[2]);

                $('#cityToLon').val(tmp[3]);

                checkRouteCode();
                initMap();
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        }

    });


    var httpRequest;
    function checkRouteCode() {
        var cityFromId = document.getElementById('cityFromId').value;
        var cityToId = document.getElementById('cityToId').value;
//        alert(cityFromId)
//        alert(cityToId)
        if (cityFromId != '' && cityToId != '') {
            var url = '/throttle/checkRoute.do?cityFromId=' + cityFromId + '&cityToId=' + cityToId;
            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest();
            };
            httpRequest.send(null);
        }
    }



    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
//                alert(val);
                if (val != "" && val != 'null') {
                    $("#routeStatus").text('Route Exists Code is :' + val);
                    $("#save").hide();
                } else {
                    $("#routeStatus").text('');
                    $("#save").show();
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }

//    function processRequest() {
//        if (httpRequest.readyState == 4) {
//            if (httpRequest.status == 200) {
//                var val = httpRequest.responseText.valueOf();
//                if (val != "" && val != 'null') {
//                    $("#routeStatus").text('Route Exists Code is :' + val);
//                } else {
//                    $("#routeStatus").text('');
//                }
//            } else {
//                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
//            }
//        }
//    }

    //savefunction
    function submitPage(value) {
        var count = validateRouteDetails();
        var errStr = "";
        //        alert(count);
        var nameCheckStatus = $("#routeStatus").text();
        if (document.getElementById('cityFromId').value == '' || document.getElementById('cityFrom').value == '') {
            alert("choose the from location");
            document.getElementById('cityFrom').focus();
        } else if (document.getElementById('cityToId').value == '' || document.getElementById('cityTo').value == '') {
            alert("choose the to location");
            document.getElementById('cityTo').focus();
        } else if (nameCheckStatus != "") {
            errStr = " Route Already Exists.\n";
            alert(errStr);
            document.getElementById("routeStatus").focus();
        } else if (document.getElementById('distance').value == '') {
            alert("enter the travel distance");
            document.getElementById('distance').focus();
        } else if (document.getElementById('fuelCost').value == '') {
            alert("please check the current fuel cost");
        } else if (count == 0) {
            $("#save").hide();
            document.route.action = '/throttle/saveRoute.do';
            document.route.submit();
        }
    }

    function validateRouteDetails() {
        var vehMileage = document.getElementsByName("vehMileage");
        var reefMileage = document.getElementsByName("reefMileage");
        var fuelCostPerKms = document.getElementsByName("fuelCostPerKms");
        var fuelCostPerHrs = document.getElementsByName("fuelCostPerHrs");
        var tollAmounts = document.getElementsByName("tollAmounts");
        var miscCostKm = document.getElementsByName("miscCostKm");
        var driverIncenKm = document.getElementsByName("driverIncenKm");
        var factor = document.getElementsByName("factor");
        var vehExpense = document.getElementsByName("vehExpense");
        var reeferExpense = document.getElementsByName("reeferExpense");
        var totExpense = document.getElementsByName("totExpense");

        var a = 0;
        var count = 0;
        for (var i = 0; i < vehMileage.length; i++) {
            a = i + 1;
            if (fuelCostPerKms[i].value == '') {
                alert("please fill fuel cost per km for row " + a);
                fuelCostPerKms[i].focus();
                count = 1;
                return count;
            } else if (fuelCostPerHrs[i].value == '') {
                alert("please fill fuel cost per hrs for row " + a);
                fuelCostPerHrs[i].focus();
                count = 1;
                return count;
            } else if (tollAmounts[i].value == '') {
                alert("please fill toll rate per km for row " + a);
                tollAmounts[i].focus();
                count = 1;
                return count;
            } else if (miscCostKm[i].value == '') {
                alert("please fill miscellaneous cost per km for row " + a);
                miscCostKm[i].focus();
                count = 1;
                return count;
            } else if (driverIncenKm[i].value == '') {
                alert("please fill driver incentive per km for row " + a);
                driverIncenKm[i].focus();
                count = 1;
                return count;
            } else if (factor[i].value == '') {
                alert("please fill factor for row " + a);
                factor[i].focus();
                count = 1;
                return count;
            } else if (vehExpense[i].value == '') {
                alert("please fill vehExpense for row " + a);
                vehExpense[i].focus();
                count = 1;
                return count;
            } else if (reeferExpense[i].value == '') {
                alert("please fill reeferExpense for row " + a);
                reeferExpense[i].focus();
                count = 1;
                return count;
            } else if (totExpense[i].value == '') {
                alert("please fill totExpense for row " + a);
                totExpense[i].focus();
                count = 1;
                return count;
            }

        }
        return count;
    }

//function numbersOnly(oToCheckField, oKeyEvent) {
//    return oKeyEvent.charCode === 0 || /\d/.test(String.fromCharCode(oKeyEvent.charCode));
//  }
    function onKeyPressBlockNumbers(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /\d/;
        return !reg.test(keychar);
    }
//    function FilterInput (event) {
//            var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
//
//                // returns false if a numeric character has been entered
//            return (chCode < 48 /* '0' */ || chCode > 57 /* '9' */);
//        }



</script>

<!--<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>-->
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.AddRoute" text="AddRoute"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.AddRoute" text="AddRoute"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">


            <body onload="sorter.size(10);
                    setTollCost(1)">
                <form name="route"  method="post">

                    <table class="table table-info mb30 table-hover" id="bg" >
                        <center><span align="center" id="routeStatus" style="color:red;"/></center>
                        <thead>
                            <tr>
                                <th  colspan="4" >Route  Creation</th>
                            </tr>
                        </thead>
                        <tr>
                            <td >Route Code</td>
                            <td ><input type="hidden" name="routeCode" id='routeCode' class="textbox" value="<c:out value="${nextRouteCode}"/>" readonly><c:out value="${nextRouteCode}"/></td>
                            <td style="display:none">Toll Amount Type</td>
                            <td style="display:none">
                                <input type="hidden" name="tollAmountType" id="tollAmountType" onclick="setTollCost(1);" value="1" checked/>Average
                                <!--
                                <!--                        <input type="radio" name="tollAmountType" id="tollAmountType" onclick="setTollCost(1);" value="1" checked/>Average
                                                        <input type="radio" name="tollAmountType" id="tollAmountType" value="2"  onclick="setTollCost(2);"  />Fixed-->
                            </td>
                        </tr>
                        <tr>
                            <td >From Location</td>
                            <td >
                                <input type="hidden" name="cityFromId" id="cityFromId" value="<c:out value="${cityId}"/>" class="form-control" style="width:250px;height:40px">
                                <input type="hidden" name="cityFromLat" id="cityFromLat" value="" class="form-control">
                                <input type="hidden" name="cityFromLon" id="cityFromLon" value="" class="form-control">
                                <input type="text" name="cityFrom" id="cityFrom" value="<c:out value="${cityName}"/>"  class="form-control" onblur="calculateDistance();" style="width:250px;height:40px" onkeypress="return onKeyPressBlockNumbers(event);"></td>
                            <td >To Location</td>
                            <td >
                                <input type="hidden" name="cityToId" id="cityToId" value="<c:out value="${cityId}"/>" class="form-control" style="width:250px;height:40px">
                                <input type="hidden" name="cityToLat" id="cityToLat" value="" class="form-control">
                                <input type="hidden" name="cityToLon" id="cityToLon" value="" class="form-control">
                                <input type="text" name="cityTo" id="cityTo" value="<c:out value="${cityName}"/>"  class="form-control" onblur="calculateDistance();" style="width:250px;height:40px" ></td>
                        </tr>
                        <tr>
                            <td >KM</td>
                            <td ><input type="text" name="distance" id="distance" class="form-control" style="width:250px;height:40px" onkeyup="calcFuleCostPerKm()"  onkeypress="return onKeyPressBlockCharacters(event)" onpaste="return false;"/></td>
                            <td >Travel hours(Hrs)</td>
                            <td >HR:<input type="text"  style="width:38px" name="travelHour" id="travelHour" class="textbox" onkeyup="calcReferHours()" readonly/>MI:<input type="text" name="travelMinute" style="width:38px" id="travelMinute" class="textbox" onkeyup="calcReferHours()" readonly/></td>
                        </tr>
                        <tr style="display:none">
                            <td >Reefer Running Hours</td>
                            <td >HR:<input type="text"  style="width:38px" name="reeferHour" id="reeferHour" class="textbox"  readonly/>MI:<input type="text" name="reeferMinute" style="width:38px" id="reeferMinute" class="textbox" readonly/></td>
                            <td >Road Type</td>
                            <td ><select class="form-control" style="width:250px;height:40px" name="roadType" id="roadType">
                                    <option value="NH">National Highway</option>
                                    <option value="SH" selected>State Highway</option>
                                </select>
                            </td>
                        </tr>
                        <tr>

                            <td >Current Fuel Cost/Ltr (PAN INDIA)</td>
                            <td ><input type="hidden" name="fuelCost" id="fuelCost"  value="<%=request.getAttribute("currentFuelPrice")%>" readonly/><label><%=request.getAttribute("currentFuelPrice")%></label></td>
                            <td style="display:none" >Current CNG Cost/KG (PAN INDIA)</td>
                            <td style="display:none" ><input type="hidden" name="cngCost" id="cngCost"  value="<%=request.getAttribute("currentCNGPrice")%>" readonly/><label><%=request.getAttribute("currentCNGPrice")%></label></td>
                        </tr>
                        <tr>
                            <td><input type="hidden" id="avgTollAmount" name="avgTollAmount" value="<c:out value="${avgTollAmount}"/>" ></td>
                            <td><input type="hidden" name="avgMisCost" id="avgMisCost"  value="<c:out value="${avgMisCost}"/>"/></td>
                            <td><input type="hidden" name="avgDriverIncentive" id="avgDriverIncentive"  value="<c:out value="${avgDriverIncentive}"/>"/></td>
                            <td><input type="hidden" name="avgFactor" id="avgFactor" value="<c:out value="${avgFactor}"/>"/></td>
                            <td><input type="hidden" id="avgTollAmount3118" name="avgTollAmount3118" value="<c:out value="${avgTollAmount3118}"/>" ></td>
                            <td><input type="hidden" name="avgMisCost3118" id="avgMisCost3118"  value="<c:out value="${avgMisCost3118}"/>"/></td>
                            <td><input type="hidden" name="avgDriverIncentive3118" id="avgDriverIncentive3118"  value="<c:out value="${avgDriverIncentive3118}"/>"/></td>
                            <td><input type="hidden" name="avgFactor3118" id="avgFactor3118" value="<c:out value="${avgFactor3118}"/>"/></td>
                        </tr>
                        <tr>
                            <td colspan="4" >

                                <center>
                                    <input type="button" class="btn btn-success"  value="Save"  name='Save' id="save" onclick="submitPage(this.value)"/>
                                </center>
                            </td>
                        </tr>
                    </table>
                    <script type="text/javascript">
                        //concard value get from the addrout configDetails
                        function setTollCost(type) {
                            if (type == 1) {
                                var km = document.getElementById("distance").value;
                                var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                                var tollAmounts = document.getElementsByName("tollAmounts");
                                var misCost = document.getElementsByName("miscCostKm");
                                var driverIncentive = document.getElementsByName("driverIncenKm");
                                var reeferExpense = document.getElementsByName('reeferExpense');
                                var factor = document.getElementsByName('factor');
                                var vehExpense = document.getElementsByName('vehExpense');
                                var fuelTypeId = document.getElementsByName('fuelTypeId');
                                var vehicleTonnage = document.getElementsByName('vehicleTonnage');
                                for (var i = 0; i < tollAmounts.length; i++) {
                                    if (vehicleTonnage[i].value == "13.50" && fuelTypeId[i].value == "1002") {
                                        tollAmounts[i].value = document.getElementById('avgTollAmount').value;
                                        misCost[i].value = document.getElementById('avgMisCost').value;
                                        driverIncentive[i].value = document.getElementById('avgDriverIncentive').value;
                                        factor[i].value = document.getElementById('avgFactor').value;
                                        driverIncentive[i].readOnly = true;
                                        misCost[i].readOnly = true;
                                        tollAmounts[i].readOnly = true;
                                        factor[i].readOnly = true;
                                    } else if (vehicleTonnage[i].value == "18.00" && fuelTypeId[i].value == "1002") {
                                        tollAmounts[i].value = document.getElementById('avgTollAmount3118').value;
                                        misCost[i].value = document.getElementById('avgMisCost3118').value;
                                        driverIncentive[i].value = document.getElementById('avgDriverIncentive3118').value;
                                        factor[i].value = document.getElementById('avgFactor3118').value;
                                        driverIncentive[i].readOnly = true;
                                        misCost[i].readOnly = true;
                                        tollAmounts[i].readOnly = true;
                                        factor[i].readOnly = true;
                                    } else if (vehicleTonnage[i].value == "0.00" && fuelTypeId[i].value == "1003") {
                                        tollAmounts[i].value = 0;
                                        misCost[i].value = 0;
                                        driverIncentive[i].value = 0;
                                        factor[i].value = 0;
                                        driverIncentive[i].readOnly = false;
                                        misCost[i].readOnly = false;
                                        tollAmounts[i].readOnly = false;
                                        factor[i].readOnly = false;
                                    } else if (vehicleTonnage[i].value == "0.00" && fuelTypeId[i].value == "1002") {
                                        tollAmounts[i].value = 0;
                                        misCost[i].value = 0;
                                        driverIncentive[i].value = 0;
                                        factor[i].value = 0;
                                        driverIncentive[i].readOnly = false;
                                        misCost[i].readOnly = false;
                                        tollAmounts[i].readOnly = false;
                                        factor[i].readOnly = false;
                                    }
                                    if (fuelCostPerKms[i].value != '') {
                                        var fuelAmnt = fuelCostPerKms[i].value * km;
                                    } else {
                                        var fuelAmnt = 0 * km;
                                    }
                                    if (tollAmounts[i].value != '') {
                                        var tollAmnt = tollAmounts[i].value * km;
                                    } else {
                                        var tollAmnt = 0 * km;
                                    }
                                    if (misCost[i].value != '') {
                                        var misCostPerKm = misCost[i].value * km;
                                    } else {
                                        var misCostPerKm = 0 * km;
                                    }
                                    if (driverIncentive[i].value != '') {
                                        var driverIncentivePerKm = driverIncentive[i].value * km;
                                    } else {
                                        var driverIncentivePerKm = 0 * km;
                                    }
                                    if (fuelCostPerKms[i].value != '') {
                                        var total = parseFloat(tollAmnt) + parseFloat(misCostPerKm) + parseFloat(driverIncentivePerKm) + parseFloat(fuelAmnt);
                                        vehExpense[i].value = total.toFixed(2);
                                    } else {
                                        var driverIncentivePerKm = 0 * km;
                                    }

                                    calcTotExp();
                                }
                            } else if (type == 2) {
                                var km = document.getElementById("distance").value;
                                var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                                var vehExpense = document.getElementsByName('vehExpense');
                                var tollAmounts = document.getElementsByName("tollAmounts");
                                var misCost = document.getElementsByName("miscCostKm");
                                var driverIncentive = document.getElementsByName("driverIncenKm");
                                var factor = document.getElementsByName("factor");
                                for (var i = 0; i < fuelCostPerKms.length; i++) {
                                    if (fuelCostPerKms[i].value != '') {
                                        var total = fuelCostPerKms[i].value * km
                                        vehExpense[i].value = total.toFixed(2);
                                    } else {
                                        vehExpense[i].value = 0.00;
                                    }
                                    tollAmounts[i].value = 0;
                                    misCost[i].value = 0;
                                    driverIncentive[i].value = 0;
                                    factor[i].value = 0;
                                    tollAmounts[i].readOnly = false;
                                    driverIncentive[i].readOnly = false;
                                    misCost[i].readOnly = false;
                                    factor[i].readOnly = false;
                                    calcTotExp();
                                }
                            }
                        }

                        //calculate fuelCostPerKm
                        function calcFuleCostPerKm() {
                            var km = document.getElementById("distance").value;
                            var fuelCostPerKm = 0;
                            var fuelCost = document.getElementById('fuelCost').value;
                            var cngCost = document.getElementById('cngCost').value;
                            var fuelTypeId = document.getElementsByName('fuelTypeId');
                            var vehMileage = document.getElementsByName('vehMileage');
                            var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                            var factor = document.getElementsByName('factor');
                            if (km != '') {
                                for (var i = 0; i < vehMileage.length; i++) {
                                    var totFuelCost = parseFloat(km) / parseFloat(vehMileage[i].value) * parseFloat(fuelCost);
                                    //                            fuelCostPerKm = parseFloat(totFuelCost) / parseFloat(km);
                                    if (fuelTypeId[i].value == 1002) {
                                        fuelCostPerKm = parseFloat(fuelCost) / parseFloat(vehMileage[i].value);
                                        fuelCostPerKms[i].value = fuelCostPerKm.toFixed(2);
                                    } else if (fuelTypeId[i].value == 1003) {
                                        fuelCostPerKm = parseFloat(cngCost) / parseFloat(vehMileage[i].value);
                                        fuelCostPerKms[i].value = fuelCostPerKm.toFixed(2);
                                    }
                                }
                            } else {
                                for (var i = 0; i < vehMileage.length; i++) {
                                    fuelCostPerKms[i].value = '';
                                }
                            }
                            calcVehExp();
                            //New
                            var avgKmPerHr = 32; // Assumbtion
                            var travelKm = km;
                            var hours = travelKm / avgKmPerHr;
                            //
                            //Calc Vehicle Running Hours
                            //                            var perDayTravelKm = 450;
                            //                            var travelKm = km / perDayTravelKm;
                            //                            var hours = travelKm.toFixed(2) * 24;
                            var travelHrs = hours.toFixed(2);
                            //alert(travelHrs);
                            var temp = travelHrs.split(".");
                            travelHrs = temp[0];
                            var travelMin = temp[1];
                            var travelMinute = travelMin;
                            var hour = 0;
                            var minute = 0;
                            if (temp[1] > 59) {
                                for (var travelHour = 0; travelMinute > 59; travelHrs++) {
                                    hour = travelHour + 1;
                                    travelMinute = parseInt(travelMinute) - 60;
                                    minute = travelMinute;
                                }
                            } else {
                                hour = 0;
                                minute = travelMinute;
                            }
                            var tothour = hour + parseInt(temp[0]);
                            if (tothour < 10) {
                                tothour = '0' + tothour;
                            }
                            if (minute < 10) {
                                minute = '0' + minute;
                            }
                            //                    alert(minute.indexOf("0") != -1);
                            document.getElementById("travelHour").value = tothour;
                            document.getElementById("travelMinute").value = minute;
                            calcReferHours();
                        }
                        //calculate Vehicle Expenses cost
                        function calcVehExp() {
                            var km = document.route.distance.value;
                            var tollAmounts = document.getElementsByName('tollAmounts');
                            var miscCostKm = document.getElementsByName('miscCostKm');
                            var driverIncenKm = document.getElementsByName('driverIncenKm');
                            var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                            var factorPerKm = document.getElementsByName('factor');
                            var vehExpense = document.getElementsByName('vehExpense');
                            var fuelCostPerHrs = document.getElementsByName('fuelCostPerHrs');
                            var reeferExpense = document.getElementsByName('reeferExpense');
                            var totExpense = document.getElementsByName('totExpense');
                            if (km != '') {
                                for (var i = 0; i < fuelCostPerKms.length; i++) {
                                    var expCost = parseInt(km) * fuelCostPerKms[i].value;
                                    if (tollAmounts[i].value != '') {
                                        var tollAmnt = tollAmounts[i].value * km;
                                    } else {
                                        var tollAmnt = 0 * km;
                                    }
                                    if (miscCostKm[i].value != '') {
                                        var misCost = miscCostKm[i].value * km;
                                    } else {
                                        var misCost = 0 * km;
                                    }
                                    if (driverIncenKm[i].value != '') {
                                        var driverIncentive = driverIncenKm[i].value * km;
                                    } else {
                                        var driverIncentive = 0 * km;
                                    }
                                    //                            if (factorPerKm[i].value != '') {
                                    //                                var factor = factorPerKm[i].value * km;
                                    //                            } else {
                                    //                                var factor = 0 * km;
                                    //                            }
                                    //                            var total = parseFloat(expCost) + parseFloat(tollAmnt) + parseFloat(misCost) + parseFloat(driverIncentive) + parseFloat(factor);
                                    //                            alert("parseFloat(expCost)"+parseFloat(expCost));
                                    //                            alert("parseFloat(tollAmnt)"+parseFloat(tollAmnt));
                                    //                            alert("parseFloat(misCost)"+parseFloat(misCost));
                                    //                            alert("parseFloat(driverIncentive)"+parseFloat(driverIncentive.toFixed(2)));
                                    var total = parseFloat(expCost.toFixed(2)) + parseFloat(tollAmnt.toFixed(2)) + parseFloat(misCost.toFixed(2)) + parseFloat(driverIncentive.toFixed(2));
                                    vehExpense[i].value = total.toFixed(2);
                                }
                            } else {
                                for (var i = 0; i < fuelCostPerKms.length; i++) {
                                    vehExpense[i].value = '';
                                    fuelCostPerHrs[i].value = '';
                                    reeferExpense[i].value = '';
                                    totExpense[i].value = '';
                                }
                            }
                            calcTotExp();
                        }

                        //calculate reefer hours
                        function calcReferHours() {
                            var km = document.getElementById("distance").value;
                            //Calc reefer Running Hours
                            var perDayTravelKm1 = 600;
                            var travelKm1 = km / perDayTravelKm1;
                            var hours = travelKm1.toFixed(2) * 24;
                            var travelHrs = hours.toFixed(2);
                            var travelHour2 = travelHrs / 100;
                            var travelHour3 = (travelHour2 * 66).toFixed(2);
                            var refMinute = travelHour3;
                            var reeferHrs = travelHour3;
                            var temp = reeferHrs.split(".");
                            var reeferHrs1 = temp[0];
                            var reeferMinute = temp[1];
                            var hour = 0;
                            var minute = 0;

                            hour = reeferHrs1;
                            minute = ((reeferMinute / 100) * 60).toFixed(0);
                            /*
                             if (reeferMinute > 59) {
                             for (var reeferHour = 0; reeferMinute > 59; reeferHrs1++) {
                             var hour1 = reeferHour + 1;
                             reeferMinute = parseInt(reeferMinute) - 60;
                             minute = reeferMinute;
                             }
                             hour=reeferHrs1 + hour
                             } else {
                             hour = reeferHrs1;
                             minute = reeferMinute;
                             }
                             */
                            if (hour < 10) {
                                hour = '0' + hour;
                            }

                            if (minute < 10) {
                                minute = '0' + minute;
                            }
                            if (minute == '') {
                                minute == '0';
                            }
                            document.getElementById("reeferHour").value = parseFloat(hour);
                            document.getElementById("reeferMinute").value = parseFloat(minute);


                            if (refMinute != 00) {
                                //                    alert(refMinute);
                                calcFuleCostPerHr(refMinute);
                            }
                        }

                        //calculate fuelCostPerHr
                        function calcFuleCostPerHr(refMinute) {
                            //  
                            var fuelCost = document.getElementById('fuelCost').value;
                            var reefMileage = document.getElementsByName('reefMileage');
                            var fuelCostPerHrs = document.getElementsByName('fuelCostPerHrs');
                            var reeferExpense = document.getElementsByName('reeferExpense');
                            for (var i = 0; i < reefMileage.length; i++) {
                                //alert("refMinute==="+refMinute.toFixed(2));
                                /*     var totFuleCostPerMinute = parseInt(refMinute) / parseInt(reefMileage[i].value) * parseInt(fuelCost);
                                 var fuleCostPerMinute = parseInt(totFuleCostPerMinute) / parseInt(refMinute);
                                 var fuelCostPerHour = fuleCostPerMinute * 60;
                                 fuelCostPerHrs[i].value = fuelCostPerHour.toFixed(2);
                                 var totalReeferCastPerMinute = parseInt(refMinute) * parseInt(fuelCostPerHrs[i].value);
                                 var refExpCal = totalReeferCastPerMinute / 60;
                                 reeferExpense[i].value = refExpCal.toFixed(2);
                                 */
                                //alert(reefMileage[i].value);

                                var referExp = reefMileage[i].value * fuelCost;
                                fuelCostPerHrs[i].value = referExp.toFixed(2);
                                //alert(fuelCostPerHrs[i].value);
                                //alert(refMinute.toFixed(2));
                                var totalReefExp = fuelCostPerHrs[i].value * refMinute;
                                //alert(totalReefExp.toFixed(2));
                                reeferExpense[i].value = totalReefExp.toFixed(2);


                            }
                            calcTotExp();
                        }


                        function calcTotExp() {
                            var km = document.route.distance.value;
                            //                    var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                            //                    var fuelCostPerHrs = document.getElementsByName('fuelCostPerHrs');
                            var reeferExpense = document.getElementsByName('reeferExpense');
                            var vehExpense = document.getElementsByName('vehExpense');
                            var totExpense = document.getElementsByName('totExpense');
                            var fuelTypeId = document.getElementsByName("fuelTypeId");
                            for (var i = 0; i < vehExpense.length; i++) {
                                if (vehExpense[i].value != '') {
                                    var vehExp = vehExpense[i].value;
                                } else {
                                    var vehExp = 0;
                                }
                                if (reeferExpense[i].value != '') {
                                    var refExp = reeferExpense[i].value;
                                } else {
                                    var refExp = 0;
                                }
                                var totCost = parseFloat(vehExp) + parseFloat(refExp);
                                totExpense[i].value = totCost.toFixed(2);
                            }
                        }

                        function calcTollExp(index) {
                            var tollAmounts = document.getElementById('tollAmounts' + index).value;
                            var km = document.getElementById('distance').value;
                            var vehExp = document.getElementById('vehExpense' + index).value;
                            var fuelTypeId = document.getElementById('fuelTypeId' + index).value;
                            var miscCostKm = document.getElementById('miscCostKm' + index).value;
                            if (miscCostKm == "") {
                                miscCostKm = 0;
                            }
                            var driverIncenKm = document.getElementById('driverIncenKm' + index).value;
                            if (driverIncenKm == "") {
                                driverIncenKm = 0;
                            }
                            var factorPerKm = document.getElementById('factor' + index).value;
                            if (factorPerKm == "") {
                                factorPerKm = 0;
                            }
                            //var reeferExp = document.getElementById('reeferExpense'+index).value;
                            var totExp = "";
                            if (tollAmounts != '' && fuelTypeId == 1002) {
                                totExp = parseFloat(tollAmounts) * km + parseFloat(vehExp);
                                document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                            } else if (tollAmounts != '' && fuelTypeId == 1003 && miscCostKm != "") {
                                totExp = parseFloat(tollAmounts) + parseFloat(vehExp) + parseFloat(miscCostKm) + parseFloat(driverIncenKm) + parseFloat(factorPerKm);
                                document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                            }
                        }
                        function calcMiscExp(index) {
                            var tollAmounts = document.getElementById('tollAmounts' + index).value;
                            var miscCostKm = document.getElementById('miscCostKm' + index).value;
                            var km = document.getElementById('distance').value;
                            var vehExp = document.getElementById('vehExpense' + index).value;
                            var fuelTypeId = document.getElementById('fuelTypeId' + index).value;
                            var driverIncenKm = document.getElementById('driverIncenKm' + index).value;
                            if (tollAmounts == "") {
                                tollAmounts = 0;
                            }
                            if (driverIncenKm == "") {
                                driverIncenKm = 0;
                            }
                            var factorPerKm = document.getElementById('factor' + index).value;
                            if (factorPerKm == "") {
                                factorPerKm = 0;
                            }
                            //var reeferExp = document.getElementById('reeferExpense'+index).value;
                            var totExp = "";
                            if (miscCostKm != '' && fuelTypeId == 1002) {
                                totExp = parseFloat(miscCostKm) * km + parseFloat(vehExp);
                                document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                            } else if (miscCostKm != '' && fuelTypeId == 1003) {
                                totExp = parseFloat(miscCostKm) + parseFloat(vehExp) + parseFloat(tollAmounts) + parseFloat(driverIncenKm) + parseFloat(factorPerKm);
                                document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                            }

                        }
                        function calcDriExp(index) {
                            var driverIncenKm = document.getElementById('driverIncenKm' + index).value;
                            var km = document.getElementById('distance').value;
                            var vehExp = document.getElementById('vehExpense' + index).value;
                            //var reeferExp = document.getElementById('reeferExpense'+index).value;
                            var fuelTypeId = document.getElementById('fuelTypeId' + index).value;
                            if (driverIncenKm == "") {
                                driverIncenKm = 0;
                            }
                            var tollAmounts = document.getElementById('tollAmounts' + index).value;
                            if (tollAmounts == "") {
                                tollAmounts = 0;
                            }
                            var factorPerKm = document.getElementById('factor' + index).value;
                            if (factorPerKm == "") {
                                factorPerKm = 0;
                            }
                            var miscCostKm = document.getElementById('miscCostKm' + index).value;
                            if (miscCostKm == "") {
                                miscCostKm = 0;
                            }
                            var totExp = "";
                            if (driverIncenKm != '' && fuelTypeId == 1002) {
                                totExp = parseFloat(driverIncenKm) * km + parseFloat(vehExp);
                                document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                            } else if (driverIncenKm != '' && fuelTypeId == 1003) {
                                totExp = parseFloat(driverIncenKm) + parseFloat(vehExp) + parseFloat(tollAmounts) + parseFloat(miscCostKm) + parseFloat(factorPerKm);
                                document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                            }

                        }
                        function calcFactorExp(index) {
                            var factorPerKm = document.getElementById('factor' + index).value;
                            var km = document.getElementById('distance').value;
                            var vehExp = document.getElementById('vehExpense' + index).value;
                            var fuelTypeId = document.getElementById('fuelTypeId' + index).value;
                            //var reeferExp = document.getElementById('reeferExpense'+index).value;
                            var miscCostKm = document.getElementById('miscCostKm' + index).value;
                            if (miscCostKm == "") {
                                miscCostKm = 0;
                            }
                            var driverIncenKm = document.getElementById('driverIncenKm' + index).value;
                            if (driverIncenKm == "") {
                                driverIncenKm = 0;
                            }
                            var tollAmounts = document.getElementById('tollAmounts' + index).value;
                            if (tollAmounts == "") {
                                tollAmounts = 0;
                            }
                            var totExp = "";
                            if (factorPerKm != '' && fuelTypeId == 1002) {
                                totExp = parseFloat(factorPerKm) * km + parseFloat(vehExp);
                                document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                            } else if (factorPerKm != '' && fuelTypeId == 1003) {
                                totExp = parseFloat(factorPerKm) + parseFloat(vehExp) + parseFloat(driverIncenKm) + parseFloat(tollAmounts) + parseFloat(miscCostKm);
                                document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                            }

                        }

                    </script>
                    <br>
                    <br>
                    <br>
                    <br>
                    <% int count = 0;%>
                    <c:if test="${mileageConfigList != null}">
                        <table class="table table-info mb30 table-hover sortable" id="table" style="display:none" >	
                            <thead>

                                <tr >
                                    <th>S.No</th>
                                    <th>Vehicle Type </th>

                                    <th>Fuel Cost Per Km's</th>
                                    <th>Fuel Cost Per Hr's </th>
                                    <th>Toll Cost Per KM </th>
                                    <th>Misc Cost Per KM</th>
                                    <th>Driver Incentive Per KM</th>
                                    <th>Factor</th>
                                    <!--<th>Variable Expense Cost</th>-->
                                    <th>Vehicle Expense Cost</th>
                                    <th>Reefer Expense Cost</th>
                                    <th>Total Expense Cost</th>
                                </tr>
                            </thead>
                            <% int index = 0;
                                int sno = 1;
                            %>
                            <tbody>
                                <c:forEach items="${mileageConfigList}" var="mcl">
                                    <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                                    %>

                                    <tr height="30">
                                        <td align="left" ><%=sno%></td>
                                        <td align="left"  style="width: 200px"><input type="hidden" name="vehTypeId" id="vehTypeId" value="<c:out value="${mcl.vehicleTypeId}"/>" readonly/><c:out value="${mcl.vehicleTypeName}"/>
                                            <input type="hidden" name="vehMileage" id="vehMileage" value="<c:out value="${mcl.vehicleMileage}"/>"/>
                                            <input type="hidden" name="reefMileage" id="reefMileage" value="<c:out value="${mcl.reeferMileage}"/>"/>
                                            <input type="hidden" name="fuelTypeId" id="fuelTypeId<%=index%>" value="<c:out value="${mcl.fuelTypeId}"/>"/>
                                            <input type="hidden" name="vehicleTonnage" id="vehicleTonnage<%=index%>" value="<c:out value="${mcl.vehicleTonnage}"/>"/>
                                        </td>
                                        <td align="left" ><input type="text" name="fuelCostPerKms" id="fuelCostPerKms<%=index%>"   class="form-control" style="width:150px;height:40px" readonly/></td>
                                        <td align="left" ><input type="text" name="fuelCostPerHrs" id="fuelCostPerHrs<%=index%>"  class="form-control" style="width:150px;height:40px" readonly/></td>
                                        <td align="left" ><input type="text" name="tollAmounts" id="tollAmounts<%=index%>"  class="form-control" style="width:150px;height:40px" value='0' onkeyup="calcTollExp('<%=index%>')" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                                        <td align="left" ><input type="text" name="miscCostKm" id="miscCostKm<%=index%>"   class="form-control" style="width:150px;height:40px" value='0'onkeyup="calcMiscExp('<%=index%>')" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                                        <td align="left" ><input type="text" name="driverIncenKm" id="driverIncenKm<%=index%>"  class="form-control" style="width:150px;height:40px" value='0'onkeyup="calcDriExp('<%=index%>')" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                                        <td align="left" ><input type="text" name="factor" id="factor<%=index%>"  class="form-control" style="width:150px;height:40px" value='0' onkeyup="calcFactorExp('<%=index%>')"  onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                                        <td align="left" ><input type="text" name="vehExpense" id="vehExpense<%=index%>"class="form-control" style="width:150px;height:40px" readonly/>
                                            <input type="hidden" name="varExpense" id="varExpense" value="0"/>
                                        </td>
                                        <td align="left" ><input type="text" name="reeferExpense" id="reeferExpense<%=index%>" readonly class="form-control" style="width:150px;height:40px" /></td>
                                        <td align="left" ><input type="text" name="totExpense" id="totExpense<%=index%>"  readonly class="form-control" style="width:150px;height:40px"/></td>
                                    </tr>
                                    <%sno++;%>
                                    <%index++;%>
                                </c:forEach>

                            </tbody>
                        </table>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");</script>
                        <div id="controls" style="display:none">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="200">200</option>
                                </select>
                                <span>Entries Per Page</span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 1);
                        </script>
                    </c:if>
                    <br>
                </form>
            </body>
            <div id="mapTab" style="width: 1200px; height: 500px; margin-top:20px;display:none">
                <div id="map" style="display:none"></div>
                <div id="right-panel" style="display:none">

                </div>
                <script>
                    function initMap() {
                        var fromLat = document.getElementById("cityFromLat").value;
                        var fromLon = document.getElementById("cityFromLon").value;
                        var toLat = document.getElementById("cityToLat").value;
                        var toLon = document.getElementById("cityToLon").value;
                        var latlng = new google.maps.LatLng(fromLat, fromLon);
                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 5,
                            center: latlng
                        });

                        var directionsService = new google.maps.DirectionsService;
                        var directionsDisplay = new google.maps.DirectionsRenderer({
                            draggable: true,
                            map: map,
                            panel: document.getElementById('right-panel')
                        });

                        directionsDisplay.addListener('directions_changed', function() {
                            computeTotalDistance(directionsDisplay.getDirections());
                        });

                        displayRoute(fromLat, fromLon, toLat, toLon, directionsService,
                                directionsDisplay);
                    }

                    function displayRoute(fromLat, fromLon, toLat, toLon, service, display) {
                        var request = '';

                        request = {
                            origin: new google.maps.LatLng(fromLat, fromLon),
                            destination: new google.maps.LatLng(toLat, toLon),
                            travelMode: google.maps.DirectionsTravelMode.DRIVING

                        };
                        service.route(request, function(response, status) {
                            if (status === google.maps.DirectionsStatus.OK) {
                                display.setDirections(response);
                            } else {
                            }
                        });
                    }
                    function computeTotalDistance(result) {
                        var total = 0;
                        var myroute = result.routes[0];
                        for (var i = 0; i < myroute.legs.length; i++) {
                            total += myroute.legs[i].distance.value;
                        }
                        //                                    alert("total="+total)
                        total = (total / 1000).toFixed(2);
                        //document.getElementById('total').innerHTML = total + ' km';
                        document.getElementById("distance").value = total;
                    }
                </script>
            </div>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
