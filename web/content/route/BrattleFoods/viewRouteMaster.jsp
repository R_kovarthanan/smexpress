<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<!--<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>-->
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ViewRoute" text="ViewRoute"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
		                    <li class=""><spring:message code="hrms.label.ViewRoute" text="ViewRoute"/></li>
		
		                </ol>
		            </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
                    
    <body>
        <script type="text/javascript">

           $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#cityFrom').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCityFromName.do",
                    dataType: "json",
                    data: {
                        cityFrom: request.term,
                        cityToId: document.getElementById('cityToId').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#cityFromId').val(tmp[0]);
                $('#cityFrom').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

        $('#cityTo').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCityToName.do",
                    dataType: "json",
                    data: {
                        cityTo: request.term,
                        cityFromId: document.getElementById('cityFromId').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#cityToId').val(tmp[0]);
                $('#cityTo').val(tmp[1]);
                checkRouteCode();
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        }

    });


    var httpRequest;
    function checkRouteCode() {
        var cityFromId = document.getElementById('cityFromId').value;
        var cityToId = document.getElementById('cityToId').value;
        if (cityFromId != '' && cityToId != '') {
            var url = '/throttle/checkRoute.do?cityFromId=' + cityFromId + '&cityToId=' + cityToId;
            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest();
            };
            httpRequest.send(null);
        }
    }


    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                    $("#routeCode").val(val);
                } else {
                    $("#routeCode").val('');
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }

    $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#routeCode').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getRouteCode.do",
                            dataType: "json",
                            data: {
                                term: request.term
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                //console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        //                        $( "#itemCode" ).val( ui.item.Name);
                        var $itemrow = $(this).closest('tr');
                        // Populate the input fields from the returned values
                        var value = ui.item.Name;
//                        alert(value);
                        var tmp = value.split('-');
                        $itemrow.find('#routeCode').val(tmp[0]);
                        $itemrow.find('#routeId').val(tmp[1]);
                        $('#routeNameFrom').attr('readonly', true);
                        $('#routeNameTo').attr('readonly', true);
                        // Give focus to the next input field to recieve input from user

                        //$itemrow.find('#itemQty').focus();
                        return false;
                    }
                    // Format the list menu output of the autocomplete
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    //alert(item);
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[0] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            //.append( "<a>"+ item.Name + "</a>" )
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };

            });


            function checkValue(value,id){
                if(value == '' && id=='cityFrom'){
                   $('#cityTo').attr('readonly', true);
                   $('#routeCode').attr('readonly', true);
                   document.getElementById('cityFromId').value = '';
                }
                if(value == '' && id=='cityTo'){
                   $('#cityFrom').attr('readonly', true);
                   $('#routeCode').attr('readonly', true);
                   document.getElementById('cityToId').value = '';
                }
                if(value == '' && id=='routeCode'){
                   $('#cityFrom').attr('readonly', true);
                   $('#cityTo').attr('readonly', true);
                   document.getElementById('routeId').value = '';
                }
            }
        function submitPage(val) {
                if (val == 'addRoute') {
                    document.routeMaster.action = '/throttle/addRouteMaster.do';
                    document.routeMaster.submit();
                } else if (val == 'Search') {
                    var errStr = "";
                    if(document.getElementById("routeCode").value == "")
                    {
                        errStr = "Please enter routeCode.\n";
                        alert(errStr);
                        document.getElementById("routeCode").focus();
                    }
                    if(errStr == "") {
                    document.routeMaster.action = '/throttle/viewRouteDetails.do?routeCode.value';
                    document.routeMaster.submit();
                    }
                }
            }

            function editPage(routeId){
                    document.routeMaster.action = '/throttle/editRouteDetails.do?editRouteId='+routeId;
                    document.routeMaster.submit();
            }
            function viewPage(routeId){
                    document.routeMaster.action = '/throttle/viewRouteDetail.do?editRouteId='+routeId;
                    document.routeMaster.submit();
            }


        </script>
        <form name="routeMaster"  method="post">
            
             <%@ include file="/content/common/message.jsp" %>
            
            
            
           <table class="table table-info mb30 table-hover" id="bg" >
		    <thead>
		<tr>
		    <th colspan="2" height="30" >View Route Details</th>
		</tr>
               </thead>
               
               
                                 <table class="table table-info mb30 table-hover" id="bg" >
                                    <tr>
                                        <td height="30">&nbsp;&nbsp;<font color="red">*</font>Origin</td>
                                        <td><input type="hidden" name="cityFromId" id="cityFromId" value="<c:out value="${cityId}"/>" class='form-control' style='width:250px;height:40px'><input type="text" name="cityFrom" id="cityFrom" value="<c:out value="${cityName}"/>" class='form-control' style='width:250px;height:40px' onchange="checkValue(this.value,this.id)"></td>
                                        <td height="30">&nbsp;&nbsp;<font color="red">*</font>Destination</td>
                                        <td><input type="hidden" name="cityToId" id="cityToId" value="<c:out value="${cityId}"/>" class='form-control' style='width:250px;height:40px'><input type="text" name="cityTo" id="cityTo" value="<c:out value="${cityName}"/>" class='form-control' style='width:250px;height:40px' onchange="checkValue(this.value,this.id)"></td>

                                    </tr>
                                    <tr>
                                        <td height="30">&nbsp;&nbsp;<font color="red">*</font>Route Code</td>
                                        <td  height="25"><input name="routeCode" value="<c:out value="${routeCode}"/>" class="tInput" id="routeCode" style="width:250px;height:40px" onchange="checkValue(this.value,this.id)" />
                                             <input name="routeId" value="<c:out value="${routeId}"/>" class="tInput" id="routeId" type="hidden"/></td>
                                         </table>
                                         <tr>
                                         <center>
                                        
                                        
                                        <td>
                                            <input type="button"   value="Search" class="btn btn-success"  name="search" onClick="submitPage(this.value)" style="width: 120px;">
                                        </td>
                                        <td height="25"><input type="button"   value="Add Route" class="btn btn-success"  name="addRoute" onClick="submitPage(this.name)" style="width: 120px;"/>
                                        </td>
                                        </center>
                                    </tr>
                               
                            </div>
                        </div>
                    
            
            
            <c:if test="${routeList != null}">
               <table class="table table-info mb30 table-hover" id="table" >	
			<thead>
                        <tr >
                            <th>S.No</th>
                            <th>Route Code</th>
                            <th>From Location</th>
                            <th>To Location</th>
                            <th>Travel Time(Hrs)</th>
                            <th>Distance(Km)</th>
                            <!--<th>Reefer Running Hours</th>-->
                            <!--<th>Highway Type</th>-->
                            <th>view/edit</th>
                        </tr>
                    </thead>
                    <% int index = 0;
                        int sno = 1;
                    %>
                    <tbody>
                        <c:forEach items="${routeList}" var="rl">
                            <%
                                String classText = "";
                                int oddEven = index % 2;
                                if (oddEven > 0) {
                                    classText = "text2";
                                } else {
                                    classText = "text1";
                                }
                            %>
                            <tr height="30">
                                <td align="left" ><%=sno%></td>
                                <td align="left" ><c:out value="${rl.routeCode}"/></td>
                                <td align="left" ><c:out value="${rl.cityFromName}"/></td>
                                <td align="left" ><c:out value="${rl.cityToName}"/></td>
                                <td align="center" ><c:out value="${rl.travelHour}"/>:<c:out value="${rl.travelMinute}"/></td>
                                <td align="center" ><c:out value="${rl.distance}"/></td>
                                <%--<c:out value="${rl.reeferHour}"/>--%>
                                <%--<c:out value="${rl.reeferMinute}"/>--%>                                
                                <%--<c:out value="${rl.roadType}"/>--%>                                
                                <td align="left" >
                                    <a href="#" onclick="viewPage('<c:out value="${rl.routeId}"/>')">view</a> &nbsp;&nbsp;
                                    
                                    <%
                                    int desigId = (Integer)session.getAttribute("DesigId");
                                    if(desigId != 1046){                                    
                                    %>
                                    <a href="#" onclick="editPage('<c:out value="${rl.routeId}"/>')">edit</a>
                                    <%}%>
                                </td>
                            </tr>
                            <%sno++;%>
                        </c:forEach>
                    </tbody>
                </table>
                <script language="javascript" type="text/javascript">
                    setFilterGrid("table");
                </script>
                <div id="controls">
                    <div id="perpage">
                        <select onchange="sorter.size(this.value)">
                            <option value="5" selected="selected">5</option>
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <span>Entries Per Page</span>
                    </div>
                    <div id="navigation">
                        <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                        <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                        <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                        <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                    </div>
                    <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                </div>
                <script type="text/javascript">
                    var sorter = new TINY.table.sorter("sorter");
                    sorter.head = "head";
                    sorter.asc = "asc";
                    sorter.desc = "desc";
                    sorter.even = "evenrow";
                    sorter.odd = "oddrow";
                    sorter.evensel = "evenselected";
                    sorter.oddsel = "oddselected";
                    sorter.paginate = true;
                    sorter.currentid = "currentpage";
                    sorter.limitid = "pagelimit";
                    sorter.init("table", 1);
                </script>
            </c:if>
            
        </form>
    </body>
</div>
            </div>
        </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
