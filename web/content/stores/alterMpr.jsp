
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ page import="ets.domain.mrs.business.MrsTO" %> 
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <title>MRSList</title>
    </head>
    <body>
        
        
        <script>
            function submitPage(val){
                
                if(val=='approve'){
                    if(selectedItemValidation(val)=='pass' ){
                    document.mpr.status.value="APPROVED"
                    document.mpr.action="/throttle/approveMpr.do";                    
                    document.mpr.submit();
                    }
                }else if(val=='reject'){
                    if(selectedItemValidation(val)=='pass' ){  
                    document.mpr.status.value="REJECTED"                
                    document.mpr.action="/throttle/approveMpr.do" ;                    
                    document.mpr.submit();
                    }
                }
            }   
            
   function selectedItemValidation(value){
        var issueQty = document.getElementsByName("approvedQtys");
        var chec=0;        
        if(value != 'reject'){
        for(var i=0;(i<issueQty.length && issueQty.length!=0);i++){
            chec++;
            if(floatValidation(issueQty[i],'Approve Quantity')){       
                return 'fail';
            }   
        }
        }
        
        if(value == 'reject'){
            for(var i=0;(i<issueQty.length && issueQty.length!=0);i++){
                issueQty[i].value='0';
            }
        }
        
        if(textValidation(document.mpr.desc,'Remarks')){   
            return 'fail';
        }
        return 'pass';
    }            
            
            
        </script>
        
        <form name="mpr" method="post" >                    
            <%@ include file="/content/common/path.jsp" %>            
            <%@ include file="/content/common/message.jsp" %>    
            <br>
<% int index = 0; %>                
                <c:if test = "${mprList != null}" >   
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="550" id="bg">
                <tr>
                    <td colspan="9" height="80" align="center" class="contenthead" >Approve MPR </td>
                </tr>

                    <c:forEach items="${mprList}" var="mpr">  
<% if(index==0){ %>          
                <input type="hidden" name="mprId" value=<c:out value="${mpr.mprId}"/> >    
                <tr>
                    <td class="text1" height="30">MPR NO</td>
                    <td class="text1" height="30"> <c:out value="${mpr.mprId}"/> </td>
                </tr>  
                <tr>
                    <td class="text2" height="30">Vendor Name</td>
                    <td class="text2" height="30"> <c:out value="${mpr.vendorName}"/> </td>
                </tr>
                <tr>
                    <td class="text1" height="30">Raised Date</td>
                    <td class="text1" height="30"> <c:out value="${mpr.mprDate}"/></td>                    
                </tr>            
<% index++; } %>                
                    </c:forEach>
   
            </table>
            <br>
            <br>    
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="500" id="bg">              
                <%index = 0;
            String classText = "";
            int oddEven = 0;
                %>
                <c:if test = "${mprList != null}" >
                <tr>
                    <td colspan="6" align="center" class="contenthead" height="30">MPR Items </td>
                </tr>  
                        <tr>
                            <td class="text2" height="30"><b>Mfr Code</b></td>
                            <td class="text2" height="30"><b>Papl Code</b></td>                       
                            <td class="text2" height="30"><b>Item Name</b></td>
                            <td class="text2" height="30"><b>Uom</b></td>
                            <td class="text2" height="30"><b>Requested Qty</b></td>                            
                            <td class="text2" height="30"><b></b> </td>                                                                        
                        </tr>                
                    <c:forEach items="${mprList}" var="mpr"> 
                        <%

            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr>
                            <td class="<%=classText %>" height="30"><c:out value="${mpr.mfrCode}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${mpr.paplCode}"/></td>                       
                            <td class="<%=classText %>" height="30"><c:out value="${mpr.itemName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${mpr.uomName}"/></td>
                            <td class="<%=classText %>" height="30"><input type="text" name="reqQtys" size="5" class="textbox" value='<c:out value="${mpr.reqQty}"/>' > </td>                             
                            <input type="hidden" name="itemIds" value=<c:out value="${mpr.itemId}"/> > 
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach>
                </c:if>                  
            </table> 
           
                <input type="hidden" name="status" value="" >             
            <br>
            <center><input type="text" name="desc" value="" > </center>
            <br>
             <center>   
            <input type="button" class="button" name="approve" value="Approve" onClick="submitPage(this.name);" > &nbsp;
            <input type="button" class="button" name="reject" value="Reject" onClick="submitPage(this.name);" >
            </center>
                </c:if>                  
        </form>
    </body>
</html>
