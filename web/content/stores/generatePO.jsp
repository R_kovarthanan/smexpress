

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ page import="ets.domain.purchase.business.PurchaseTO" %>
    <%@ page import="java.util.*" %>
    <%@ page import="java.text.DecimalFormat" %>
    <%@ page import="java.text.NumberFormat" %>
    
    <title>Purchase Order</title>

</head>
<body>


<script>
    function submitPage(val){
        if(val=='approve'){
            document.mpr.status.value="APPROVED"
            document.mpr.action="/throttle/approveMpr.do"
            document.mpr.submit();
        }else if(val=='reject'){
        document.mpr.status.value="REJECTED"
        document.mpr.action="/throttle/approveMpr.do"
        document.mpr.submit();
    }
}


    function print(ind)
    {
        var DocumentContainer = document.getElementById(ind);
        var WindowObject = window.open('', "TrackHistoryData",
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        //WindowObject.close();
    }


</script>

<form name="mpr"  method="post" >
    <br>


<%
int index=0;
ArrayList poDetail = new ArrayList();
poDetail = (ArrayList) request.getAttribute("poDetail");
PurchaseTO purch = new PurchaseTO();
int listSize=10;
int itemNameLimit = 30;
int mfrCodeLimit = 10;

PurchaseTO headContent = new PurchaseTO();
System.out.println("poDetail size in jsp="+poDetail.size());
headContent = (PurchaseTO)poDetail.get(0);
String[] address = headContent.getAddress();
String itemName = "";
String mfrCode = "";


for(int i=0; i<poDetail.size(); i=i+listSize) {
%>



<div id="print<%= i %>" >
<style type="text/css">
.header {font-family:Arial;
font-size:15px;
color:#000000;
text-align:center;
 padding-top:10px;
 font-weight:bold;
}
.border {border:1px;
border-color:#000000;
border-style:solid;
}
.text1 {
font-family:Arial, Helvetica, sans-serif;
font-size:14px;
color:#000000;
}
.text2 {
font-family:Arial, Helvetica, sans-serif;
font-size:16px;
color:#000000;
}
.text3 {
font-family:Arial, Helvetica, sans-serif;
font-size:18px;
color:#000000;
}

</style>


<table width="700" align="center" border="0" cellpadding="0" cellspacing="0" >
<tr>
<td valign="top" colspan="2" height="30" align="center" class="border"><strong>PURCHASE ORDER</strong></td>
</tr>
<tr>
<td height="125" valign="top">
<table width="350" height="125" border="0" cellpadding="0" cellspacing="0" class="border">
<tr>
<Td height="80" width="400" class="text3" align="left">
    <img src="/throttle/images/Brattle_Logo.png" style="width:120px">    
</Td>
</tr>
<tr>
<Td align="left" class="text2" style="padding-left:10px; ">
    <p>
        PRR House, #280, 'N' Block,Anna Nagar East,<br>
        (Near Lotus Colony E.B.Office),<br>
        Chennai - 600 102.<br>
        Phone +91 44 - 4217 2266.<br>
        E-mail : maintenance@prrtravels.com
    </p>
</Td>
</tr>
</table>
</td>
<td height="125"  valign="top" >

<table width="350" height="125" border="0" cellpadding="0" cellspacing="0">
<tr>
<Td height="125" width="350" valign="top">
<table height="136" width="350" align="left" border="0" cellpadding="0" cellspacing="0" class="border" >
<tr>
<td colspan="2" class="text2" style="padding-left:10px;" ><strong>To</strong></td>
</tr>


<tr>
<td colspan="2" class="text2" style="line-height:20px;"  height="112" style="padding-left:10px;">

                                    <p><strong>M/S&nbsp;&nbsp; <%= headContent.getVendorName() %>  </strong> <br>
                                      <%= headContent.getVendorAddress() %> <br>

                                      Phno &nbsp;:&nbsp; <%= headContent.getVendorPhone() %> <br>
                                      Tin No &nbsp;:&nbsp; <%= headContent.getTinno() %> 
                                      
                                      </p>
    </td>
</tr>
<tr>
<td class="text3" width="137" style="padding-left:10px;"> PO.No.:<strong><span class="text3" >  <%= headContent.getPoId() %></span></strong>
<strong>Date</strong>:<%= headContent.getCreatedDate() %> </td>
<td class="text3" width="161"  style="padding-left:10px;">&nbsp; </td>
</tr>

</table>
</Td>
</tr>
</table>
</td>
</tr>


<tr>
<Td valign="top" colspan="2">
<table width="700" height="30" align="right" border="0" cellpadding="0" cellspacing="0" class="border">

<tr>
<%--bala--%>
<%
if(headContent.getVehicleNo()  != null &&  headContent.getUsageType()!=null)
{%>
<Td width="150" style="padding-left:10px;" class="text1"> Service Point : &nbsp <strong><span class="text3" >  <%= headContent.getCompanyName() %></span></strong> </Td>
<Td width="200" style="padding-left:10px;" class="text1"> &nbsp; </Td>
<Td width="150" style="padding-left:10px;" class="text1"> Vehicle No : &nbsp <strong><span class="text3" >  <%= headContent.getVehicleNo() %></span></strong> </Td>
<Td width="150" style="padding-left:10px;" class="text1"> &nbsp; </Td>
<Td width="150" style="padding-left:10px;" class="text1"> Usage Type : &nbsp <strong><span class="text3" >  <%= headContent.getUsageType() %></span></strong> </Td>
<Td width="150" style="padding-left:10px;" class="text1"> &nbsp; </Td>
<%}
 else
     {%>
    <Td width="450" style="padding-left:10px;" class="text1"> Service Point : &nbsp <strong><span class="text3" >  <%= headContent.getCompanyName() %></span></strong> </Td>
    <Td width="200" style="padding-left:10px;" class="text1"> &nbsp; </Td>
    <%}%>
    <%--bala ends--%>
</tr>
</table>
</Td>
</tr>

<tr>
<Td valign="top" colspan="2">

<table width="700" align="center" border="0" cellpadding="0" cellspacing="0" class="border" style="margin-bottom:25px; ">


        <tr>
        <Td class="text2"  height="28" valign="top" class="border" style="padding-left:2px; border:1px; border-color:#000000; border-style:solid;"> <strong>S.NO</strong></Td>
        <Td class="text2" valign="top" width="130" style="padding-left:2px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>PART NO</strong></Td>
        <Td class="text2"  valign="top" width="220" style="padding-left:2px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>PART NAME</strong></Td>
        <Td  class="text2" valign="top" width="30" style="padding-left:2px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>UOM</strong></Td>
<!--        <Td class="text2"  valign="top" width="60" style="padding-left:10px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>STR QTY</strong></Td>-->
        <Td class="text2"  valign="top" width="70" style="padding-left:2px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>UNIT PRICE</strong></Td>
        <Td class="text2"  valign="top" width="70" style="padding-left:2px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>QTY</strong></Td>
        <Td class="text2"  valign="top" width="70" style="padding-left:2px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>DIS %</strong></Td>
        <Td class="text2"  valign="top" width="70" style="padding-left:2px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>Tax %</strong></Td>
        <Td class="text2"  valign="top" width="70" style="padding-left:2px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>Tax Value</strong></Td>
        <Td class="text2"  valign="top" width="70" style="padding-left:2px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>TOTAL</strong></Td>
        </tr>












<%	index = 0;
        float totalPrice = 0.00f;
        float totalTax = 0.00f;
        float nettPrice = 0.00f;

        String mask = "0.00";
         String totalPrice1 = "";
        DecimalFormat df = new DecimalFormat(mask);
	for(int j=i; ( j < (listSize+i) ) && (j<poDetail.size() ) ; j++){
		purch = new PurchaseTO();
		purch = (PurchaseTO)poDetail.get(j);
                mfrCode = purch.getMfrCode();
                itemName  = purch.getItemName();
                if(purch.getMfrCode().length() > mfrCodeLimit ){
                    mfrCode = mfrCode.substring(0,mfrCodeLimit-1);
                }if(purch.getItemName().length() > itemNameLimit ){
                    itemName = itemName.substring(0,itemNameLimit-1);
                }
                System.out.println("j=="+j);
%>

                <tr>
                    <Td valign="top" HEIGHT="20" class="text1"   style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"   > <%= j+1 %> </td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px;  border-right-style:solid; border-left-style:solid;" align="center"> <%= mfrCode %> &nbsp; </td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-right-style:solid; border-left-style:solid;" align="left"> <%= itemName %> </td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px;  border-right-style:solid; border-left-style:solid;" align="center"> <%= purch.getUomName() %> </td>
<!--                    <Td valign="top" HEIGHT="20"  class="text1" width="60"  style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"> <%= purch.getReqQty() %> </td>-->
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"> <%= purch.getUnitPrice() %> </td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"> <%= purch.getApprovedQty() %> </td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"> <%= purch.getDiscount() %> </td><!--
-->                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"> <%= purch.getVat() %> </td>
                    <%  //compute total price
                        totalPrice = Float.parseFloat(purch.getApprovedQty()) * Float.parseFloat(purch.getBuyPrice());
                        //deduct discount
                        //totalPrice = totalPrice - (totalPrice*Float.parseFloat(purch.getDiscount())/100);
                        //add tax
                        totalTax = totalPrice*Float.parseFloat(purch.getVat())/100;
                        totalPrice = totalPrice + totalTax;

                        nettPrice = nettPrice + totalPrice;
                        totalPrice1 = df.format(totalPrice);
                     %>

                    <td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right"> <%=df.format(totalTax)%> </td>
                    <td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right"> <%=df.format(totalPrice)%> </td>
                </tr>

                <%  index++; }  %>

            <% while(index <= listSize ){ %>

	        <tr>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;"  > &nbsp; </td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="center">   &nbsp;</td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="left"> &nbsp;</td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="center"> &nbsp;</td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right"> &nbsp; </td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right"> &nbsp; </td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right"> &nbsp; </td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right"> &nbsp; </td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right"> &nbsp; </td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right"> &nbsp; </td>
                </tr>


            <% index++; } %>
	        <tr>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;"  > &nbsp; </td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="center">   &nbsp;</td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="left"> &nbsp;</td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="center"> &nbsp;</td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right"> &nbsp; </td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right"> &nbsp; </td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right"> &nbsp; </td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right"> &nbsp; </td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right"> <b>Total Rs.</b> </td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right"> <%=df.format(nettPrice)%> </td>
                </tr>

    </table>
</td>
</tr>

<Tr>
<Td valign="top" colspan="3">
<table width="700" height="30" align="center" border="0" cellpadding="0" cellspacing="0" class="border">

<Tr>
    <Td width="690" class="text1" height="30" style="padding-left:5px; " ><b>Remarks:</b><%= headContent.getRemarks() %></Td>
</Tr>
</table>
</Td>
</Tr>

<Tr>
<Td valign="top" colspan="3">
<table width="700" height="30" align="center" border="0" cellpadding="0" cellspacing="0" class="border">

<Tr>
<Td width="110" class="text1" height="30" > &nbsp; </Td>
<Td width="336" class="text1" height="30" >&nbsp;</Td>
<Td width="154" class="text1" height="30" >&nbsp;</Td>
</Tr>

<Tr>
<Td width="110" class="text1" height="30" > &nbsp; </Td>
<Td width="336" class="text1" height="30" >&nbsp;</Td>
<Td width="154" class="text1" height="30" >&nbsp;</Td>
</Tr>

<Tr>
<Td width="110" class="text1"><strong>Prepared by</strong></Td>
<Td width="336" class="text1">&nbsp;</Td>
<Td width="154" class="text1"><strong>Authorised Signatory</strong></Td>
</Tr>
</table>
</Td>
</Tr>
</table>
</div>
    <center>
        <input type="button" class="button" name="Print" value="Print" onClick="print('print<%= i %>');" > &nbsp;
    </center>
<br>
<br>
<br>
<% } %>

</form>
</body>
</html>
