<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/parveencss/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@ page import="ets.domain.mrs.business.MrsTO" %>
</head><title>Store</title>

<script>

function addRow(val){
    if(document.getElementById(val).innerHTML == ""){
    document.getElementById(val).innerHTML = "<input type='text' size='7' class='textbox' >";
    }else{
    document.getElementById(val).innerHTML = "";
    }
}
function newWO(val){
        window.open('/throttle/OtherServiceStockAvailability.do?itemId='+val, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }
    function back(){
        document.spareIssue.action='/throttle/MRSList.do';
        document.spareIssue.submit();
        }
</script>
<body>
<form name="spareIssue"  method="post" >
 <%@ include file="/content/common/path.jsp" %>


<%@ include file="/content/common/message.jsp" %>
<br>
    <c:if test = "${vehicleDetails != null}" >
<table align="center" border="0" cellpadding="0" cellspacing="0" width="400" id="bg" class="border">
<tr>
<td class="contenthead" colspan="4" align="center" height="30"><div class="contenthead">Vehicle Details</div></td>
</tr>

      <c:forEach items="${vehicleDetails}" var="vehicle">

<tr>
<td class="text2" height="30">Vehicle Number</td>
<td class="text2" height="30"><c:out value="${vehicle.mrsVehicleNumber}"/></td>
<td class="text2" height="30">KM</td>
<td class="text2" height="30"><c:out value="${vehicle.mrsVehicleKm}"/></td>
</tr>
<tr>
<td class="text1" height="30">Vehicle Type</td>
<td class="text1" height="30"><c:out value="${vehicle.mrsVehicleType}"/></td>
<td class="text1" height="30">MFR</td>
<td class="text1" height="30"><c:out value="${vehicle.mrsVehicleMfr}"/></td>
</tr>

<tr>
<td class="text2" height="30">Use Type</td>
<td class="text2" height="30"><c:out value="${vehicle.mrsVehicleUsageType}"/></td>
<td class="text2" height="30">Model</td>
<td class="text2" height="30"><c:out value="${vehicle.mrsVehicleModel}"/></td>
</tr>

<tr>
<td class="text1" height="30">Engine No</td>
<td class="text1" height="30"><c:out value="${vehicle.mrsVehicleNumber}"/></td>
<td class="text1" height="30">Chasis No</td>
<td class="text1" height="30"><c:out value="${vehicle.mrsVehicleChassisNumber}"/></td>
</tr>

<tr>
<td class="text2" height="30">Technician</td>
<td class="text2" height="30"><c:out value="${vehicle.mrsTechnician}"/></td>
<td class="text2" height="30">Job Card No.</td>
<td class="text2" height="30"><c:out value="${vehicle.mrsJobCardNumber}"/></td>
</tr>

 </c:forEach>
 </c:if>
</table>
<br>



    <c:if test = "${rcMrsDetails != null}" >
<table align="center" border="0" cellpadding="0" cellspacing="0" width="400" id="bg" class="border">
<tr>
<td class="contenthead" colspan="4" align="center" height="30"><div class="contenthead">RC Details</div></td>
</tr>

      <c:forEach items="${rcMrsDetails}" var="rc">

<tr>
<td class="text2" height="30">Work Order No</td>
<td class="text2" height="30"><c:out value="${rc.rcWorkId}"/></td>
<td class="text2" height="30">Requested Date</td>
<td class="text2" height="30"><c:out value="${rc.issuedOn}"/></td>
</tr>
<tr>
<td class="text1" height="30">Vendor Name</td>
<td class="text1" height="30"><c:out value="${rc.vendorName}"/></td>
<td class="text1" height="30">Technician</td>
<td class="text1" height="30"><c:out value="${rc.mrsTechnician}"/></td>
</tr>

 </c:forEach>
 </c:if>
</table>
    <c:if test = "${counterMrsDetails != null}" >
<table align="center" border="0" cellpadding="0" cellspacing="0" width="400" id="bg" class="border">
<tr>
<td class="contenthead" colspan="4" align="center" height="30"><div class="contenthead">Counter Sale Details</div></td>
</tr>

      <c:forEach items="${counterMrsDetails}" var="counter">

<tr>
<td class="text1" height="30">Counter Sale No</td>
<td class="text1" height="30"><c:out value="${counter.counterId}"/></td>
<td class="text1" height="30">Customer Name</td>
<td class="text1" height="30"><c:out value="${counter.name}"/></td>
</tr>
<tr>
<td class="text2" height="30">Customer Address</td>
<td class="text2" height="30"><c:out value="${counter.address}"/></td>
<td class="text2" height="30">Mobile Number</td>
<td class="text2" height="30"><c:out value="${counter.mobileNo}"/></td>
</tr>
<tr>
<td class="text1" height="30">Requested Date</td>
<td class="text1" height="30"><c:out value="${counter.issuedOn}"/></td>
<td class="text1" height="30">&nbsp;</td>
<td class="text1" height="30">&nbsp;</td>
</tr>

 </c:forEach>
 </c:if>
</table>


<br>
 <c:if test = "${mrsDetails != null}" >
<table align="center" border="0" cellpadding="0" cellspacing="0" width="710" class="border" >
  <tr>
    <td colspan="9" align="center" class="text2" height="30"><strong>MRS  Details</strong> </td>
  </tr>
  <tr>
    <td  class="contentsub"><div class="contentsub">Sno</div></td>
    <td  class="contentsub"><div class="contentsub">Item Code</div> </td>
    <td  height="15" class="contentsub"><div class="contentsub" align="left">PAPL Code</div></td>
    <td  height="15" class="contentsub"><div class="contentsub">Item Name</div></td>
    <td height="15" colspan="2" class="contentsub"><div class="contentsub">Available Stock</div></td>
    <td  height="15" class="contentsub"><div class="contentsub">Stock At other Service Point</div></td>
    <td  height="15" class="contentsub"><div class="contentsub">Required Qty</div></td>
    <td  height="15" class="contentsub"><div class="contentsub">PO Qty</div></td>

  </tr>

  <tr>
    <td class="contentsub">&nbsp;</td>
    <td class="contentsub">&nbsp;</td>
    <td height="30" class="contentsub"><div align="left"></div></td>
    <td height="30" class="contentsub">&nbsp;</td>
    <td width="52" height="30" class="contentsub">New</td>
    <td width="57" class="contentsub">RC</td>
    <td height="30" class="contentsub">&nbsp;</td>
    <td height="30" class="contentsub">&nbsp;</td>
    <td height="30" class="contentsub">&nbsp;</td>
  </tr>
   <% int index = 0; %>
      <c:forEach items="${mrsDetails}" var="mrs">
<%
   String classText = "";
    int oddEven = index % 2;
    if (oddEven > 0) {
    classText = "text2";
    } else {
    classText = "text1";
    }
    %>
  <tr>
    <td class="<%=classText %>"><%=index+1%></td>
    <td class="<%=classText %>"><c:out value="${mrs.mrsItemMfrCode}"/></td>
    <td class="<%=classText %>" height="30"><div align="left"><c:out value="${mrs.mrsPaplCode}"/></div></td>
    <td class="<%=classText %>" height="30"><c:out value="${mrs.mrsItemName}"/></td>
    <td height="30" class="<%=classText %>"><div align="center"><c:out value="${mrs.mrsLocalServiceItemSum}"/></div></td>
    <td height="30" class="<%=classText %>"><div align="center"><c:out value="${mrs.qty}"/></div></td>
    <td class="<%=classText %>" height="30"><div align="center"><a href="#" onClick="newWO(<c:out value="${mrs.mrsItemId}"/>)"><c:out value="${mrs.mrsOtherServiceItemSum}"/></a></div></td>
    <td class="<%=classText %>" height="30"><div align="center"><c:out value="${mrs.mrsRequestedItemNumber}"/></div></td>
    <td class="<%=classText %>" height="30"><div align="center"><c:out value="${mrs.poQuantity}"/></div></td>
  </tr>
  <% index++; %>
  </c:forEach>
   </c:if>
</table>
<br>
<center>
<input type="button" value="Back" class="button" onclick="back();">
</center>

<br>
<br>

</form>
</body>
</html>
