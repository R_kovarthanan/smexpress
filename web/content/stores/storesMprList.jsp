
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>          
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script> 
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"> </script> 
        <title>MPRList</title>
    </head>
    <body onLoad="setDates();">
        
        
        <script>
            function submitPag(){
                document.mpr.dat1.value = dateFormat(document.mpr.dat1);
                document.mpr.dat2.value = dateFormat(document.mpr.dat2);
                alert(document.mpr.dat1.value);
                document.mpr.action="/throttle/mprApprovalList.do";
                document.mpr.submit();
            }            
            
            function viewPO(ind)
            {
                var poId = document.getElementsByName('poIds');
                document.mpr.action="/throttle/handlePoDetail.do?poId="+poId[ind].value;
                document.mpr.submit();                
            }    
            
            function cancelPO(ind)
            {
                var poId = document.getElementsByName('poIds');
                if(confirm("Please Ensure to Cancel Purchase Order = "+poId[ind].value,'parveen' )){
                document.mpr.action="/throttle/handleCancelPo.do?poId="+poId[ind].value;
                document.mpr.submit();                
                }
            }    
            
            function setDates()
            {
             if('<%= request.getAttribute("fromDate") %>' != 'null' ){
                 document.mpr.fromDate.value = '<%= request.getAttribute("fromDate") %>' ;
             }
             if('<%= request.getAttribute("toDate") %>' != 'null' ){
                 document.mpr.toDate.value = '<%= request.getAttribute("toDate") %>' ;
             }                
            }
            
            function getData(){
                if(document.mpr.toDate.value==''){
                    alert("Please Enter From Date");
                }else if(document.mpr.fromDate.value==''){
                    alert("Please Enter to Date");
                }                 
                document.mpr.action = "/throttle/storesMprList.do";
                document.mpr.submit();                
            }    
            
            
        </script>
        
        <form name="mpr"  method="post" >                    
            <%@ include file="/content/common/path.jsp" %>            
            <!-- pointer table -->
          
            <!-- message table -->           
            <%@ include file="/content/common/message.jsp" %>    


            <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%" id="bg"  >
               <tr> <td colspan="4" class="contentsub" align="center" >MPR LIST</td></tr>
                <tr>
                    <td class="text2" width="150" height="30">From Date</td>
                    <td width="196" height="30" class="text2"><input readonly name="fromDate" class="textbox" type="text" value="" size="20">
                    <span class="text2"><img src="/throttle/images/cal.gif" name="Calendar"  readonly  onclick="displayCalendar(document.mpr.fromDate,'dd-mm-yyyy',this)"/></span></td>

                    <td class="text2" width="150" height="30">To Date</td>
                    <td width="196" height="30" class="text1"><input name="toDate" class="textbox"  type="text" value="" size="20">
                    <span class="text2"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.mpr.toDate,'dd-mm-yyyy',this)"/></span></td>
                </tr>         
                <tr> <td colspan="4" align="center" > &nbsp; </td></tr>
                <tr>
                    <td colspan="4" align="center" > <input type="button" class="button" value="Search" name="search" onClick="getData();" </td>
                </tr>    
            </table>
             <br>
    
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%" id="bg" class="border">
               
                <tr>
                    <td class="contentsub" height="30"><div class="contentsub">Sno</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">MPR/LPR NO</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">PO No</div></td>
                    <td class="contentsub" width="100" height="30"><div class="contentsub">Vendor Name</div></td>
                    <td class="contentsub" width="120" height="30"><div class="contentsub">Requested Date</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Status</div></td>
                    <td class="contentsub" width="100" height="30"><div class="contentsub">Approved By</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Remarks</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Generate PO</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">View</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Cancel</div></td>
                </tr>
                <% int index = 0;
            String classText = "";
            int oddEven = 0;
                %>
                <c:if test = "${mprList != null}" >
                    <c:forEach items="${mprList}" var="mpr"> 
                        <%

            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr>
                            <td class="<%=classText %>" height="30"> <%= index+1 %></td>
                            <td class="<%=classText %>" height="30"><c:out value="${mpr.mprId}"/></td>
                            <td class="<%=classText %>" height="30">
                            <c:if test="${mpr.poId!='0'}" >
                            <c:out value="${mpr.poId}"/>
                            </c:if>
                            <c:if test="${mpr.poId=='0'}" >
                            Not Available
                            </c:if>
                            </td>
                            <td class="<%=classText %>" height="30"><c:out value="${mpr.vendorName}"/></td>                       
                            <td class="<%=classText %>" height="30"><c:out value="${mpr.mprDate}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${mpr.status}"/></td>                                                                        
                            <td class="<%=classText %>" height="30"><c:out value="${mpr.approvedBy}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${mpr.desc}"/></td> 
                            <c:if test="${mpr.status=='APPROVED' && mpr.poId!='0'}" >
                            <td class="<%=classText %>" height="30"> <a href="#" disabled > GeneratePO  </a> </td>                                                                        
                            </c:if>                            
                            <c:if test="${mpr.status=='APPROVED' && mpr.poId=='0'}" >
                            <td class="<%=classText %>" height="30"><a href="/throttle/generatePo.do?mprId=<c:out value='${mpr.mprId}'/>" > GeneratePO </a> </td>                                                                        
                            </c:if>
                            <c:if test="${mpr.status=='REJECTED'}" >
                            <td class="<%=classText %>" height="30"><a href="#" disabled > GeneratePO  </a> </td>                                                                                                                                    
                            </c:if>
                            <c:if test="${mpr.status=='PENDING'}" >
                            <td class="<%=classText %>" height="30"><a href="#" disabled > GeneratePO  </a> </td>                                                                                                        
                            </c:if>
                            
                            
                            <c:if test="${mpr.poId=='0'}" >
                                <td class="<%=classText %>" height="30"> <a href="#" disabled > View </a> </td> 
                                <td class="<%=classText %>" height="30"> <a href="#" disabled onClick="cancelPO('<%= index %>')" > Cancel </a> </td> 
                                <input type="hidden" name="poIds" value='<c:out value="${mpr.poId}"/>' > 
                            </c:if>
                            <c:if test="${mpr.poId!='0'}" >
                                <td class="<%=classText %>" height="30"> <a href="#" onClick="viewPO('<%= index %>')" > View </a> </td>
                                <td class="<%=classText %>" height="30"> <a href="#" onClick="cancelPO('<%= index %>')" > Cancel </a> </td> 
                                <input type="hidden" name="poIds" value='<c:out value="${mpr.poId}"/>' > 
                            </c:if>
                                                                                     
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach>
                </c:if>                  
            </table>                        
        </form>
    </body>
</html>
