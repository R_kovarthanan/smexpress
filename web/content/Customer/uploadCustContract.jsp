<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

    var rowCount = 1;
    var sno = 0;
    var style = "text2";
    var align = "left";

    function addRow() {
//                alert("guu1");
        if (rowCount % 2 == 0) {
            style = "text2";
        } else {
            style = "text1";
        }

        var serialNo = parseInt(sno) + 1;
        var tab = document.getElementById("fileAddRow");
        var newrow = tab.insertRow(rowCount);

        var cell = newrow.insertCell(0);
        var cell1 = "<td  height='30' >" + serialNo + "</td>";
        cell.setAttribute("align", "center");
        cell.innerHTML = cell1;

        cell = newrow.insertCell(2);
        var cell3 = "<td  height='30'><textarea class='textbox' style='width:250px;height:40px' type='text' name='desc' id='desc' size='110' /></textarea></td>";
        cell.setAttribute("align", align);
        cell.innerHTML = cell3;

        cell = newrow.insertCell(2);
        var cell3 = "<td  height='30'><input class='textbox'  type='file' name='fileName' id='fileName' size='110' /></td>";
        cell.setAttribute("align", align);
        cell.innerHTML = cell3;

        var temp = sno - 1;

        rowCount++;
        sno++;

    }

    function delRow() {

        var table = document.getElementById("fileAddRow");
        rowCount = table.rows.length;
        var i = parseInt(rowCount) - 1;
        if (i > 0) {
            table.deleteRow(i);
            rowCount--;
            i--;
        }
    }

    function addRowNew() {
        // alert("guu");
        if (rowCount % 2 == 0) {
            style = "text2";
        } else {
            style = "text1";
        }

        var serialNo = parseInt(sno) + 1;
        var tab = document.getElementById("fileAddRow");
        var newrow = tab.insertRow(rowCount);

        var cell = newrow.insertCell(0);
        var cell1 = "<td  height='30' >" + serialNo + "</td>";
        cell.setAttribute("align", align);
        cell.innerHTML = cell1;

        cell = newrow.insertCell(1);
        var cell2 = "<td  height='30'><textarea class='textbox' style='width:200px;height:30px' type='text' name='desc' id='desc' size='110' /></textarea></td>";
        cell.setAttribute("align", align);
        cell.innerHTML = cell2;

        cell = newrow.insertCell(2);
        var cell3 = "<td  height='30'><input class='textbox'  type='file' name='fileName' id='fileName' size='110' /></td>";
        cell.setAttribute("align", align);
        cell.innerHTML = cell3;

        var temp = sno - 1;

        rowCount++;
        sno++;
    }

    function submitPage() {
        
        if (document.getElementById('importCnote').value == '' || document.getElementById('importCnote').value == '0') {
            alert("Please Upload Excel")
            return;
        }
        document.lhc.action = '/throttle/handleSaveUploadedFile.do';
        document.lhc.submit();
        $("#Edit").hide();
    }
</script>
<body>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.LHC DETAILS" text="Customer Upload"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Master" text="Master"/></a></li>
                <li class=""><spring:message code="hrms.label.LHC" text="Customer Upload"/></li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">

                <form name="lhc"  method="post" enctype="multipart/form-data"  >                    

                    <%@ include file="/content/common/message.jsp" %>

                    <table class="table table-info mb30 table-hover" id="bg" >

                        <thead><tr>
                                <th colspan="5" > Customer Contract Upload</th>
                            </tr>
                        </thead>

                        <%
                        String customerId = request.getParameter("custId");
                        String custName = request.getParameter("custName");
                        %>

                        <tr>
                            <td  height="30">Customer Name</td>
                            <td  height="30">

                                <c:out value="${custName}"/>

                                <input class="textbox" type="hidden" name="customerId" id="customerId" value="<c:out value="${customerId}"/>" style="width:150px;height:25px;"/>
                                <input class="textbox" type="hidden" name="custName" id="custName" readonly value="<%=custName%>" style="width:150px;height:25px;"/>
                            </td>
                        </tr> 

                    </table>

                    <table class="table table-info mb30 table-hover">

                        <tr>
                            <td >Select Excel     </td>
                            <td ><input type="file" name="importCnote" id="importCnote"  class="importCnote"></td>                             
                        </tr>
                        <tr>
                            <td >  </td>
                            <td ><a href="uploadedxls/KerryUpload.xls">Template XLS</a></td>                             
                        </tr>
                        <tr>
                            <td colspan="2" align="center" >
                                <input type="button" class="btn btn-success" id="Edit" value="Submit" name="Submit" onclick="submitPage()">                                
                            </td>
                        </tr>
                    </table>

                    <%--
                        <table class="table table-info mb30 table-hover" align="center" >
                
                    <thead>
                        <tr>
                        <th  height="25" align="left">Sno</th>
                        <th  height="25" align="left">Description</th>
                        <th  height="25" align="left">View File</th>
                    </tr>
                    </thead>
                    <tbody>
                        <% int sno = 0;%>
                        <% int sno1 = 1;%>
                            <c:forEach items="${CustContractFiless}" var="lhc">
                                <%
                                    sno++;
                                    String className = "text1";
                                    if ((sno % 1) == 0) {
                                        className = "text1";
                                    } else {
                                        className = "text2";
                                    }
                                %>
                                <tr>
                                    <td><%=sno%></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${lhc.description}" /></td>
                                    
                                    <c:if test = "${lhc.path != null}">
                                    <td class="<%=className%>"  align="left"> 
                                            <a href="uploadFiles/customer_contract/<c:out value="${lhc.path}" />" target="_blank" ><c:out value="${lhc.path}" /></a>
                                    </td>
                                    </c:if>
                                    <c:if test = "${lhc.path == null}">
                                        <td> - </td>
                                    </c:if>
                                </tr>
                                <%sno1++;%>
                            </c:forEach>
                        </tbody>
                </table>

                    <table class="table table-info mb30 table-hover" id="fileAddRow" align="center" style="display:none">
                        <thead>
                            <tr>
                                <th  height="25" align="left">Sno</th>
                                <th  height="25" align="left">Description</th>
                                <th  height="25" align="left">Upload</th>
                            </tr>
                        </thead>

                        <br/>
                        <center>
                            <br/>
                            <input type="hidden" class="btn btn-info" id="addRow" name="addRow" value="Add Row" style="width:90px;height:30px;font-weight: bold;padding:1px;" onclick="addRowNew();"/>
                            <input type="hidden" class="btn btn-info" id="Edit" name="Edit" value="Save" style="width:90px;height:30px;font-weight: bold;padding:1px;" onclick="submitPage();"/>
                        </center>
                    </table>--%>

                    <br> 

                </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="../common/NewDesign/settings.jsp" %>