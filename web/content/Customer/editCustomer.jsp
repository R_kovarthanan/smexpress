<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<head>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ page import="java.util.* "%>
    <%@ page import=" javax. servlet. http. HttpServletRequest" %>


    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

    <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>




    <style type="text/css" title="currentStyle">
        @import "/throttle/css/layout-styles.css";
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
</head>
<script language="javascript">
    function submitPage() {
//        alert("test")
                           if (document.getElementById('customerName').value == '') {
        	                alert("please enter the customer name")
        	                $("#customerName").focus();
//        	            } else if (document.getElementById('billingTypeId').value == '0' && document.getElementById('secondaryBillingTypeId').value == '0' ) {
//        	                alert("please select the billing type")
//        	                $("#billingTypeId").focus();
        	            }
                            else if (document.getElementById('ttaCustType').value == '') {
        	                alert("please select the TTA Customer Type")
        	                $("#ttaCustType").focus();
        	            }
                            else if (document.getElementById('paymentType').value == '0') {
        	                alert("please select the payment type")
        	                $("#paymentType").focus();
        	            } else if (document.getElementById('custContactPerson').value == '') {
        	                alert("please enter the contact person")
        	                $("#custContactPerson").focus();
        	            } else if (document.getElementById('custAddress').value == '') {
        	                alert("please enter the contact address")
        	                $("#custAddress").focus();
        	            } else if (document.getElementById('custCity').value == '') {
        	                alert("please enter the city")
        	                $("#custCity").focus();
        	            } else if (document.getElementById('custState').value == '') {
        	                alert("please enter the state")
        	                $("#custState").focus();
        	            } else if (document.getElementById('custPhone').value == '') {
        	                alert("please enter the phone no")
        	                $("#custPhone").focus();
        	            } else if (document.getElementById('custMobile').value == '') {
        	                alert("please enter the mobile no")
        	                $("#custMobile").focus();
        	            } else if (document.getElementById('custEmail').value == '') {
        	                alert("please enter the email")
        	                $("#custEmail").focus();
        	            } else if (document.getElementById('accountManagerId').value == '' && document.getElementById('accountManager').value != '') {
        	                alert("please select the valid accountant name")
        	                $("#accountManager").focus();
        	            } else if (document.getElementById('creditLimit').value == '') {
        	                alert("please enter the credit limit")
        	                $("#creditLimit").focus();
        	            } else if (document.getElementById('creditDays').value == '') {
        	                alert("please enter the credit days")
        	                $("#creditDays").focus();
        	            }
                            else if (document.getElementById('custContactPerson').value == '') {
        	                alert("please enter the contact person")
        	                $("#custContactPerson").focus();
        	            }
                            else {
        document.editcustomer.action = '/throttle/addCustomer.do';
        document.editcustomer.submit();
        	            }
    }
    
    function backPage(){
         document.editcustomer.action = '/throttle/handleViewCustomer.do';
        document.editcustomer.submit();
    }

    function validatePanNo() {
        var nPANNo = document.getElementById("panNo").value;
        if (nPANNo != "") {
            document.getElementById("panNo").value = nPANNo.toUpperCase();
            var ObjVal = nPANNo;
            var pancardPattern = /^([ABCFGHLJPT]{1})([A-Z]{2})([PCFHABTGL]{1})([A-Z]{1})(\d{4})([A-Z]{1})$/;
            var patternArray = ObjVal.match(pancardPattern);
            if (patternArray == null) {
                alert("PAN Card No Invalid ");
                return false;
            } else {
                return true;
            }
        } else {
            alert("Please enter pan no");
            return false;
        }
    }

    function setFocus() {
        document.editcustomer.custCode.focus();
    }
</script>

<script>
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#accountManager').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getAccountManager.do",
                    dataType: "json",
                    data: {
                        accountManagerName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#accountManagerId').val('');
                            $('#accountManager').val('');
                        } else {
                            response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#accountManager").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#accountManagerId').val(tmp[0]);
                $itemrow.find('#accountManager').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + itemVal + "</a>")
            .appendTo(ul);
        };

    });
</script>


<script>
    function setAddValue() {
        var address = document.getElementById("custAddress").value;
        var city = document.getElementById("custCity").value;
        var State = document.getElementById("custState").value;
        var temp = State.split('-');
        var billingAddressIds = document.getElementsByName("billingAddressIds");
        var billingNameAddress = document.getElementsByName("billingNameAddress");
        var stateId = document.getElementsByName("stateId");

        for (var i = 0; i < billingAddressIds.length; i++) {
            billingNameAddress[0].value = address + ", " + city;
            stateId[0].value = temp[1];
            billingNameAddress[0].readOnly = true;
            stateId[0].readOnly = true;
        }
    }
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.EditCustomer" text="Edit Customer"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.EditCustomer" text="Edit Customer"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body onload="setFocus();">
                <form name="editcustomer"  method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>

                    <c:if test="${getCustomerDetails != null}">
                        <c:forEach items="${getCustomerDetails}" var="cudl">
                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr align="center">
                                        <th colspan="4" align="center"  height="30"><div >EDIT Customer</div></th>
                                    </tr>
                                </thead>
                                <tr>
                                    <!--<td  height="30"><input name="custCode" id="custCode" type="text" class="form-control" style="width:250px;height:40px" value="" maxlength="8"></td>-->
                                    <td  height="30"><font color="red">*</font>Customer Name (For Record)</td>
                                    <td  height="30"><input name="customerId" id="customerId" type="hidden" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.customerId}"/>" onClick="ressetDate(this);">
                                        <input name="customerName" id="customerName" type="text" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.custName}"/>" ></td>

                            <td><font color="red">*</font>eFS Id</td>
                            <td>
                                <input type="text"  name="ttaCustType" id="ttaCustType"  value="<c:out value="${cudl.ttaCustType}"/>" class="form-control" style="width:250px;height:40px;">
                            </td>
                                    <td  height="30" style="display:none"><font color="red">*</font>Customer Type</td>
                                    <td  height="30" style="display:none">
                                        <select class="form-control" style="width:250px;height:40px" name="custType" id="custType" style="width:125px;">
                                            <c:if test="${cudl.custType == '1' }">
                                                <option value="1" >Contracted</option>
<!--                                                <option value="3" >Broker</option>-->

                                            </c:if>
                                            <c:if test="${cudl.custType == '2'}">
<!--                                                <option value="" >--Select--</option>-->
<!--                                                <option value="1" selected>Contracted</option>-->
                                                <option value="2" >Walk In</option>

                                            </c:if>

                                        </select>
                                    </td>

                                </tr>

                                <tr>
                                    <c:if test="${cudl.custType == '1'}">
                                    <td  height="30" ><font color="red">*</font>PrimaryContractType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td  height="30" >
                                        <select class="form-control" style="width:250px;height:40px" name="billingTypeId" id="billingTypeId" style="width:125px;">
                                            <c:if test="${billingTypeList != null}">
                                                <c:forEach items="${billingTypeList}" var="btl">
                                                    <c:choose>
                                                        <c:when test="${btl.billingTypeId==cudl.billingTypeId}">
                                                            <option selected value='<c:out value="${btl.billingTypeId}"/>'><c:out value="${btl.billingTypeName}"/>
                                                            </c:when>
                                                            <c:otherwise>
                                                            <option value="<c:out value="${btl.billingTypeId}"/>" ><c:out value="${btl.billingTypeName}"/></option>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                    </td>
                                    </c:if>
                                    <td  height="30" >Customer Name (For Invoice)</td>
                                    <td  height="30" ><input name="displayCustName" id="displayCustName" type="text" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.displayCustName}"/>" maxlength="50"></td>


                                <input name="secondaryBillingTypeId" id="secondaryBillingTypeId" type="hidden" class="form-control" style="width:250px;height:40px" value="1" maxlength="50">
                                </tr>
                                <tr>
                                    <td  width="80" >Payment Type </td>
                                    <td  width="80"> <select name="paymentType" id="paymentType" class="form-control" style="width:250px;height:40px" style="width:125px;" >
                                            <c:if test="${cudl.paymentType == '' || cudl.paymentType == null }">
                                                <option value="0" selected>--Select--</option>
                                                <option value="1" >Credit</option>
                                                <option value="2" >Advance</option>
                                                <option value="3" >To Pay and Advance</option>
                                                <option value="4" >To Pay</option>
                                            </c:if>
                                            <c:if test="${cudl.paymentType == '1'}">
                                                <option value="" >--Select--</option>
                                                <option value="1" selected>Credit</option>
                                                <option value="2" >Advance</option>
                                                <option value="3" >To Pay and Advance</option>
                                                <option value="4" >To Pay</option>
                                            </c:if>
                                            <c:if test="${cudl.paymentType == '2'}">
                                                <option value="" >--Select--</option>
                                                <option value="1" >Credit</option>
                                                <option value="2" selected>Advance</option>
                                                <option value="3" >To Pay and Advance</option>
                                                <option value="4" >To Pay</option>
                                            </c:if>
                                            <c:if test="${cudl.paymentType == '3'}">
                                                <option value="" >--Select--</option>
                                                <option value="1" >Credit</option>
                                                <option value="2" >Advance</option>
                                                <option value="3" selected>To Pay and Advance</option>
                                                <option value="4" >To Pay</option>
                                            </c:if>
                                            <c:if test="${cudl.paymentType == '4'}">
                                                <option value="" >--Select--</option>
                                                <option value="1" >Credit</option>
                                                <option value="2" >Advance</option>
                                                <option value="3" >To Pay and Advance</option>
                                                <option value="4" selected>To Pay</option>
                                            </c:if>
                                            <c:if test="${cudl.paymentType == '0'}">
                                                <option value="" selected>--Select--</option>
                                                <option value="1" >Credit</option>
                                                <option value="2" >Advance</option>
                                                <option value="3" >To Pay and Advance</option>
                                                <option value="4" >To Pay</option>
                                            </c:if>
                                        </select></td>
                                    <td  height="30"><font color="red">*</font>Contactperson</td>
                                    <td  height="30"><input name="custContactPerson" id="custContactPerson" type="text" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.custContactPerson}"/>" onKeyPress="return onKeyPressBlockNumbers(event);"></td>
                                </tr>

                                <tr>
                                    <td  height="30"><font color="red">*</font>Customer Address</td>
                                    <td  height="30"><textarea  type="text" class="form-control" style="width:250px;height:40px" name="custAddress" id="custAddress"  cols='100' rows='4'  ><c:out value="${cudl.custAddress}"/></textarea></td>
                                    <td  height="30"><font color="red">*</font> City</td>
                                    <td  height="30"><input name="custCity" id="custCity" type="text" class="form-control" style="width:250px;height:40px"  value="<c:out value="${cudl.custCity}"/>" onKeyPress="return onKeyPressBlockNumbers(event);"></td>
                                </tr>
                                <tr>
                                    <td  height="30"><font color="red">*</font> State</td>
                                    <td  height="30">
                                        <select name="custState" id="custState" class="form-control" required data-placeholder="Choose One" style="width:250px;height:40px" onchange="setAddValue()">
                                            <option value="0">Choose One</option>
                                            <c:if test = "${stateList != null}" >
                                                <c:forEach items="${stateList}" var="Type">
                                                    <option value='<c:out value="${Type.stateName}" />-<c:out value="${Type.stateId}" />'><c:out value="${Type.stateName}" /></option>
                                                </c:forEach >
                                            </c:if>
                                        </select>
<!--                                            <input name="custState" id="custState" type="text" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.custState}"/>" onKeyPress="return onKeyPressBlockNumbers(event);">-->
                                    </td>
                                <script>

                                    document.getElementById("custState").value = '<c:out value="${cudl.custState}"/>-<c:out value="${cudl.stateIdAlt}"/>';
                                </script>
                                <td  height="30"><font color="red">*</font>Phone No</td>
                                <td  height="30"><input name="custPhone" id="custPhone" type="text" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.custPhone}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10"></td>
                                </tr>
                                <tr>
                                    <td  height="30"><font color="red">*</font>Mobile No</td>
                                    <td  height="30"><input name="custMobile" id="custMobile" type="text" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.custMobile}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="12"></td>
                                    <td  height="30">&nbsp;&nbsp;Email</td>
                                    <td  height="30"><input name="custEmail" id="custEmail" type="text" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.custEmail}"/>" maxlength="20"></td>
                                </tr>
                                <tr>
                                    <td  height="30"><font color="red">*</font>Credit Limit</td>
                                    <td  height="30"><input name="creditLimit" id="creditLimit" type="text" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.creditLimit}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10"></td>
                                    <td  height="30">&nbsp;&nbsp;Credit Days</td>
                                    <td  height="30"><input name="creditDays" id="creditDays" type="text" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.creditDays}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10"></td>

                                </tr>
                                <tr>
                                    <td  height="30"><font color="red">*</font>Detention Amount</td>
                                    <td  height="30"><input name="detentionAmount" id="detentionAmount" type="text" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.detentionAmount}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10"></td>
                                    <td  height="30">&nbsp;&nbsp;Detention Cut Off Period</td>
                                    <td  height="30"><input name="detentionPeriod" id="detentionPeriod" type="text" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.detentionPeriod}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10"></td>

                                </tr>

                                <tr>
                                    <td  height="30"><font color="red">*</font>CustomerGroup&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td  height="30">
                                        <select class="form-control" style="width:250px;height:40px" name="customerGroupId" id="customerGroupId" style="width:125px;">
                                            <c:if test="${companyLists != null}">
                                                <c:forEach items="${companyLists}" var="cgl">
                                                    <c:choose>
                                                        <c:when test="${cgl.companyId==cudl.customerGroupId}">
                                                            <option selected value="<c:out value="${cgl.companyId}"/>" ><c:out value="${cgl.companyName}"/>
                                                            </c:when>
                                                            <c:otherwise>
                                                            <option value="<c:out value="${cgl.companyId}"/>" ><c:out value="${cgl.companyName}"/></option>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                    </td>
                                    <td  height="30"><font color="red">*</font>Account Manager</td>
                                    <td  height="30"><input name="accountManagerId" id="accountManagerId" type="hidden" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.accountManagerId}"/>"><input name="accountManager" id="accountManager" type="text" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.empName}"/>" onKeyPress="return onKeyPressBlockNumbers(event);"></td>
                                </tr>
                                <tr>
                                    <td  height="30"><font color="red">*</font>Status</td>
                                    <td  height="30">
                                        <select name="custStatus" id="custStatus" class="form-control" style="width:250px;height:40px" style="width:125px;">
                                            <c:if test="${cudl.custStatus == 'Y'}">
                                                <option value="Y" selected >Active</option>
                                                <option value="N"  >In Active</option>
                                            </c:if>
                                            <c:if test="${cudl.custStatus == 'N'}">
                                                <option value="Y" >Active</option>
                                                <option value="N" selected>In Active</option>
                                            </c:if>
                                        </select>
                                    </td>
                                    <td><font color="red">*</font>Organisation Type</td>
                                    <td>
                                        <input name="organizationIdTemp" id="organizationIdTemp"  type="hidden" class="form-control" >
                                        <select class="form-control" name="organizationId" id="organizationId" style="width:250px;height:40px;"required data-placeholder="Choose One">
                                            <option value="0" selected>Choose One</option>
                                            <!--<option value="<c:out value="${cudl.orgId}"/>" selected ><c:out value="${cudl.orgId}"/></option>-->
                                            <c:if test="${organizationList != null}">
                                                <c:forEach items="${organizationList}" var="org">
                                                    <option value="<c:out value="${org.orgId}"/>" ><c:out value="${org.orgName}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select>
                                    </td>
                                <script>
                                    document.getElementById("organizationId").value = '<c:out value="${cudl.orgId}"/>';
                                </script>
                                <tr>
                                    <td><font color="red">*</font>PAN No</td>
                                    <td colspan="3"><input name="panNo" id="panNo" type="text" class="form-control" value="<c:out value="${cudl.panNo}"/>" maxlength="10" style="width:250px;height:40px" onBlur="validatePanNo()" placeholder="Type pan no..."  required></td>
                                </tr>
                            </table>
                        </c:forEach >
                    </c:if>

                    <c:if test="${getCustomerAddressDetails == null}">
                        <font color="red"> No Address Details Found</font>
                    </c:if>
                    <c:if test="${getCustomerAddressDetails != null}">
                        <h4 align="center">Address Details</h4>
                        <table class="table table-info mb30 table-hover" id="items" >
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Branch Name</th>
                                    <th>Branch Address</th>
                                    <th>Branch State</th>
                                    <th>GST No</th>
                                    <th>Status</th>
                                    
                            </tr>
                            </thead>
                            <input type="hidden" name="selectedIndex" value="">
                            <%int rIndex = 0;
                                        int index = 1;
                            %>
                            <c:forEach items="${getCustomerAddressDetails}" var="list">
                                <%


                                            String classText = "";

                                            int oddEven = index % 2;
                                            if (oddEven > 0) {
                                                classText = "text2";
                                            } else {
                                                classText = "text1";
                                            }
                                %>
                                <tr >
                                    <td><%=index%></td>
                                    <td><c:out value="${list.branchName}"/><input type='hidden'  name='branchId' id="branchId" value='<c:out value="${list.branchId}"/>' ></td>
                                    <td><c:out value="${list.billingNameAddress}"/></td>
                                    <td><c:out value="${list.stateName}"/></td>
                                    <td><c:out value="${list.gstNo}"/></td>
                                    <td>
                                        <c:if test="${list.activeInd == '0' }">
                                            Active
                                        </c:if></td>
                                        <c:if test="${list.activeInd == '1' }">
                                            In Active
                                        </c:if></td>

                                </tr>
                                <%
                                            index++;
                                            rIndex++;
                                %>
                            </c:forEach >
                            <%
                                        System.out.println("rIndex:" + rIndex);
                                        rIndex--;
                            %>
                            <input type="hidden" name="rIndex" value=<%=rIndex%> />
                        </table>
                    </c:if>
                    <center>
                        <input value="Add Row" class="btn btn-success" type="button" onClick="addRow();" >
                        <input type="button" value="CANCEL" class="btn btn-success" onClick="backPage();">
                        <input type="button" value="SAVE" class="btn btn-success" onClick="submitPage();">
                    </center>


                    <script>
                        var rowCount = 1;
                        var sno = 0;
                        var httpRequest;
                        var httpReq;
                        var rowIndex = 0;
                        var styl = '';
                        function addRow()
                        {

                            if (parseInt(rowCount) % 2 == 0)
                            {
                                styl = "text2";
                            } else {
                                styl = "text1";
                            }




                            sno++;
                            document.editcustomer.selectedIndex.value = sno;
                            var tab = document.getElementById("items");
                            var iRowCount = tab.getElementsByTagName('tr').length;
                            //                            alert("len:" + iRowCount);
                            rowCount = iRowCount;
                            var newrow = tab.insertRow(rowCount);
                            sno = rowCount;
                            rowIndex = rowCount;
                            //alert("rowIndex:"+rowIndex);
                            var cell = newrow.insertCell(0);
                            var cell0 = "<td class='text1' height='25' > <input type='hidden'  name='billingAddressIds' value='' > " + sno + "</td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell0;
                            
                            var cell = newrow.insertCell(1);
                            var cell0 = "<td class='text1' height='25' ><input name='branchName' id='branchName" + rowIndex + "' class='form-control'   type='text' style='width:250px;height:40px'></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell0;

                            var cell = newrow.insertCell(2);
                            var cell0 = "<td class='text1' height='25' ><textarea name='billingNameAddress' id='billingNameAddress" + rowIndex + "' cols='100' rows='4' style='width:250px;height:40px'></textarea></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell0;

                            cell = newrow.insertCell(3);
                            var cell1 = "<td class='text1' height='25'><select class='form-control' name='stateId' id='stateId" + rowIndex + "' required data-placeholder='Choose One' style='width:250px;height:40px'><option value='0'>--Select--</option><c:forEach items='${stateList}' var='Type'><option value='<c:out value='${Type.stateId}'/>'><c:out value='${Type.stateName}'/></option></c:forEach></select></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell1;

                            cell = newrow.insertCell(4);
                            var cell2 = "<td class='text1' height='25'><input name='gstNo' id='gstNo" + rowIndex + "' class='form-control'   type='text' style='width:250px;height:40px'></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell2;

                            cell = newrow.insertCell(5);
                            cell2 = "<td class='text1' height='25'><select class='form-control' name='activeInd' id='activeInd" + rowIndex + "' style='width:250px;height:40px'><option value='0'>--Select--</option><option value='0'>ACTIVE</option> <option value='1'>INACTIVE</option></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell2;
                            
                            if (rowCount > 1) {
                            cell = newrow.insertCell(6);
                            var cell2 = "<td class='text1' height='25' ><input type='button' class='btn btn-info'   name='delete'  id='delete" + rowIndex + "'   value='Delete Row' onclick='deleteRow(this);' ></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell2;
                            }

                            //        getItemNames(rowIndex);
                            //        var itemCode = document.getElementsByName("itemCode");
                            //        itemCode[rowCount - 1].focus();

                            rowIndex++;
                            rowCount++;

                        }
                    </script>
                    <script>
                    function deleteRow(src) {
                        rowIndex--;
                        var oRow = src.parentNode.parentNode;
                        var dRow = document.getElementById('items');
                        dRow.deleteRow(oRow.rowIndex);
                        document.getElementById("selectedRowCount").value--;
                    }

                </script>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>
