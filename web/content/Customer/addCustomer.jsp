<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<head>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ page import="java.util.* "%>
    <%@ page import=" javax. servlet. http. HttpServletRequest" %>


    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

    <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>




    <style type="text/css" title="currentStyle">
        @import "/throttle/css/layout-styles.css";
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
</head>
<script language="javascript">
    function submitPage() {
        if (document.getElementById('custName').value == '') {
            alert("please enter the customer name")
            $("#custName").focus();
        } else if (document.getElementById('ttaCustType').value == '') {
            alert("please select the TTA Customer Type")
            $("#ttaCustType").focus();
        } else if (document.getElementById('paymentType').value == '0') {
            alert("please select the payment type")
            $("#paymentType").focus();
        } else if (document.getElementById('custContactPerson').value == '') {
            alert("please enter the contact person")
            $("#custContactPerson").focus();
        } else if (document.getElementById('custAddress').value == '') {
            alert("please enter the contact address")
            $("#custAddress").focus();
        } else if (document.getElementById('custCity').value == '') {
            alert("please enter the city")
            $("#custCity").focus();
        } else if (document.getElementById('custState').value == '') {
            alert("please enter the state")
            $("#custState").focus();
        }else if (document.getElementById('custPhone').value == '') {
            alert("please enter the phone no")
            $("#custPhone").focus();
        } else if (document.getElementById('custMobile').value == '') {
            alert("please enter the mobile no")
            $("#custMobile").focus();
        } else if (document.getElementById('custEmail').value == '') {
            alert("please enter the email")
            $("#custEmail").focus();
        } else if (document.getElementById('customerGroupId').value == '0') {
            alert("please enter the Customer Group")
            $("#customerGroupId").focus();
        } else if (document.getElementById('accountManagerId').value == '') {
            alert("please enter the accountManager")
            $("#accountManager").focus();
        } else if (document.getElementById('accountManagerId').value == '' && document.getElementById('accountManager').value != '') {
            alert("In valid name")
            $("#accountManager").focus();
        } else if (document.getElementById('creditLimit').value == '') {
            alert("please enter the credit limit")
            $("#creditLimit").focus();
        } else if (document.getElementById('creditDays').value == '') {
            alert("please enter the credit days")
            $("#creditDays").focus();
        } else if (document.getElementById('organizationId').value == '') {
            alert("please enter the Organization Name")
            $("#organizationId").focus();
        } else if (document.getElementById('panNo').value == '') {
            alert("please enter the PAN No")
            $("#panNo").focus();
        }
        else {
            $("#add").hide();
            document.manufacturer.action = '/throttle/addCustomer.do';
            document.manufacturer.submit();
        }
    }
    function setFocus() {
        document.manufacturer.custCode.focus();
    }

    function validatePanNo() {
        var nPANNo = document.getElementById("panNo").value;
        if (nPANNo != "") {
            document.getElementById("panNo").value = nPANNo.toUpperCase();
            var ObjVal = nPANNo;
            var pancardPattern = /^([ABCFGHLJPT]{1})([A-Z]{2})([PCFHABTGL]{1})([A-Z]{1})(\d{4})([A-Z]{1})$/;
            var patternArray = ObjVal.match(pancardPattern);
            if (patternArray == null) {
                alert("PAN Card No Invalid ");
                return false;
            } else {
                return true;
            }
        } else {
            alert("Please enter pan no");
            return false;
        }
    }
</script>

<script>
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#accountManager').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getAccountManager.do",
                    dataType: "json",
                    data: {
                        accountManagerName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#accountManagerId').val('');
                            $('#accountManager').val('');
                        } else {
                            response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#accountManager").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#accountManagerId').val(tmp[0]);
                $itemrow.find('#accountManager').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });




    var rowCount = 1;
    var sno = 0;
    var httpRequest;
    var httpReq;
    var rowIndex = 0;
    var styl = '';
    function addRow()
    {

        if (parseInt(rowCount) % 2 == 0)
        {
            styl = "text2";
        } else {
            styl = "text1";
        }




        sno++;

        var tab = document.getElementById("items");
        var iRowCount = tab.getElementsByTagName('tr').length;
//        alert("len:" + iRowCount);
        rowCount = iRowCount;
        var newrow = tab.insertRow(rowCount);
        sno = rowCount;
        rowIndex = rowCount;
        //alert("rowIndex:"+rowIndex);
        var cell = newrow.insertCell(0);
                            var cell0 = "<td class='text1' height='25' > <input type='hidden'  name='billingAddressIds' value='' > " + sno + "</td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell0;
                            
                            var cell = newrow.insertCell(1);
                            var cell0 = "<td class='text1' height='25' ><input name='branchName' id='branchName" + rowIndex + "' class='form-control'   type='text' style='width:250px;height:40px'></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell0;

                            var cell = newrow.insertCell(2);
                            var cell0 = "<td class='text1' height='25' ><textarea name='billingNameAddress' id='billingNameAddress" + rowIndex + "' cols='100' rows='4' style='width:250px;height:40px'></textarea></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell0;

                            cell = newrow.insertCell(3);
                            var cell1 = "<td class='text1' height='25'><select class='form-control' name='stateId' id='stateId" + rowIndex + "' required data-placeholder='Choose One' style='width:250px;height:40px'><option value='0'>--Select--</option><c:forEach items='${stateList}' var='Type'><option value='<c:out value='${Type.stateId}'/>'><c:out value='${Type.stateName}'/></option></c:forEach></select></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell1;

                            cell = newrow.insertCell(4);
                            var cell2 = "<td class='text1' height='25'><input name='gstNo' id='gstNo" + rowIndex + "' class='form-control'   type='text' style='width:250px;height:40px'></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell2;

                            cell = newrow.insertCell(5);
                            cell2 = "<td class='text1' height='25'><select class='form-control' name='activeInd' id='activeInd" + rowIndex + "' style='width:250px;height:40px'><option value='0'>--Select--</option><option value='0'>ACTIVE</option> <option value='1'>INACTIVE</option></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell2;
                            
                            if (rowCount > 1) {
                            cell = newrow.insertCell(6);
                            var cell2 = "<td class='text1' height='25' ><input type='button' class='btn btn-info'   name='delete'  id='delete" + rowIndex + "'   value='Delete Row' onclick='deleteRow(this);' ></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell2;
                            }
        rowIndex++;
        rowCount++;

    }
    </script>
<script>
    function deleteRow(src) {
        rowIndex--;
        sno--;
        var oRow = src.parentNode.parentNode;
        var dRow = document.getElementById('items');
        dRow.deleteRow(oRow.rowIndex);
        document.getElementById("selectedRowCount").value--;
    }

</script>

<input type="hidden" name="selectedRowCount" id="selectedRowCount" value="1"/>

    <script>
        function setAddValue() {
            var address = document.getElementById("custAddress").value;
            var city = document.getElementById("custCity").value;
            var State = document.getElementById("custState").value;
            var temp = State.split('-');
            var billingAddressIds = document.getElementsByName("billingAddressIds");
            var billingNameAddress = document.getElementsByName("billingNameAddress");
            var stateId = document.getElementsByName("stateId");

            for (var i = 0; i < billingAddressIds.length; i++) {
                billingNameAddress[0].value = address + "," + city;
                stateId[0].value = temp[1];
//                billingNameAddress[0].readOnly = true;
                stateId[0].readOnly = true;
            }
        }
    </script>
    

    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.AddCustomer" text="Add Customer"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.AddCustomer" text="Add Customer"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body onload="setFocus();
                    addRow()">
                <form name="manufacturer"  method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>

                    <table class="table table-info mb30 table-hover" id="bg" >
                        <thead>
                            <tr align="center">
                                <th colspan="4" align="center"  height="30"><div >Add Customer</div></th>
                        </tr>
                        </thead>
                        <tr>
                            <td  height="30"><font color="red">*</font>Customer Code</td>
                            <td  height="30"><input name="custCode" id="custCode" type="text" class="form-control" style="width:250px;height:40px" value="" maxlength="8" readonly></td>
                            <td  height="30"><font color="red">*</font>Customer Name (For Record)</td>
                            <td  height="30"><input name="custName" id="custName" type="text" class="form-control" style="width:250px;height:40px" value="" maxlength="50"></td>
                        </tr>
                        <tr>
                            <td  height="30"><font color="red">*</font>Customer Type</td>
                            <td  height="30">
                                <select class="form-control" style="width:250px;height:40px" name="custType" id="custType" style="width:125px;">
                                    <option value="1">Contracted</option>
                                    <!--                                    <option value="3">Broker</option>-->

                                </select>
                            </td>

                            <td><font color="red">*</font>eFS-Id</td>
                            <td><input type="text" class="form-control" style="width:250px;height:40px" name="ttaCustType" value="" id="ttaCustType"  maxlength="15" >
<!--                                <select class="form-control" style="width:250px;height:40px" name="ttaCustType" id="ttaCustType" style="width:125px;">
                                    <option value="0">--Select--</option>
                                    <option value="1">CBT</option>
                                    <option value="2">NBT</option>
                                    </select>-->
                            </td>
                            
                        </tr>
                        <tr>
                            <td  height="30" ><font color="red">*</font>PrimaryContractType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td  height="30" >
                                <select class="form-control" style="width:250px;height:40px" name="billingTypeId" id="billingTypeId" style="width:125px;">
                                    <c:if test="${billingTypeList != null}">
                                        
                                        <c:forEach items="${billingTypeList}" var="btl">
                                            <c:if test="${btl.billingTypeId == 1}">
                                            <option value="<c:out value="${btl.billingTypeId}"/>" ><c:out value="${btl.billingTypeName}"/></option>
                                            </c:if>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                            <td  height="30" ><font color="red">*</font>Customer Name (For Invoice)</td>
                            <td  height="30" ><input name="displayCustName" id="displayCustName" type="text" class="form-control" style="width:250px;height:40px" value="" maxlength="50"></td>
                            <td  height="30" style="display:none">Secondary Contract Type</td>
                            <td  height="30" style="display:none">
                                <select class="form-control" style="width:250px;height:40px" name="secondaryBillingTypeId" id="secondaryBillingTypeId" style="width:125px;">
                                    <c:if test="${billingTypeList != null}">
                                        <option value="0">--select--</option>
                                        <option value="1">Fixed Km Based</option>
                                        <option value="2">Actual Km Based</option>
                                        <option value="3">Drop Based</option>
                                    </c:if>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td  width="80" >Payment Type </td>
                            <td  width="80"> <select name="paymentType" id="paymentType" class="form-control" style="width:250px;height:40px" style="width:125px;" >
                                    <option value="" selected>--select--</option>
                                    <option value="1" >Credit</option>
                                    <option value="2" >Advance</option>
                                    <option value="3" >To Pay and Advance</option>
                                    <option value="4" >To Pay</option>
                                </select></td>
                            <td  height="30"><font color="red">*</font>Contactperson</td>
                            <td  height="30"><input name="custContactPerson" id="custContactPerson" type="text" class="form-control" style="width:250px;height:40px" value="" onKeyPress="return onKeyPressBlockNumbers(event);"></td>
                        </tr>

                        <tr>
                            <td  height="30"><font color="red">*</font>Customer Address</td>
                            <td  height="30"><textarea class="form-control" style="width:250px;height:40px" name="custAddress" id="custAddress" ></textarea></td>
                            <td  height="30"><font color="red">*</font> City</td>
                            <td  height="30"><input name="custCity" id="custCity" type="text" class="form-control" style="width:250px;height:40px" value=""  onKeyPress="return onKeyPressBlockNumbers(event);" ></td>
                        </tr>

                        <tr>
                            <td  height="30"><font color="red">*</font> State</td>

                            <td  height="30">  <select name="custState" id="custState" class="form-control" required data-placeholder="Choose One" style="width:250px;height:40px" onchange="setAddValue()">
                                    <option value="">Choose One</option>
                                    <c:if test = "${stateList != null}" >
                                        <c:forEach items="${stateList}" var="Type">
                                            <option value='<c:out value="${Type.stateName}" />-<c:out value="${Type.stateId}" />'><c:out value="${Type.stateName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select>
                            </td>
                            <td  height="30"><font color="red">*</font>Phone No</td>
                            <td  height="30"><input name="custPhone" id="custPhone" type="text" class="form-control" style="width:250px;height:40px" value="" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10"></td>
                        </tr>
                        <tr>
                            <td  height="30"><font color="red">*</font>Mobile No</td>
                            <td  height="30"><input name="custMobile" id="custMobile" type="text" class="form-control" style="width:250px;height:40px" value="" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="12"></td>
                            <td  height="30"><font color="red">*</font>Email</td>
                            <td  height="30"><input name="custEmail" id="custEmail" type="text" class="form-control" style="width:250px;height:40px" value="" maxlength="100"></td>
                        </tr>
                        <tr>
                            <td  height="30" ><font color="red">*</font>CustomerBranch&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td  height="30" >
                                <select class="form-control" style="width:250px;height:40px" name="customerGroupId" id="customerGroupId" style="width:125px;">
                                    <c:if test="${companyLists != null}">
                                        <option value="0">--Select--</option>
                                        <c:forEach items="${companyLists}" var="cgl">
                                            <option value="<c:out value="${cgl.companyId}"/>" ><c:out value="${cgl.companyName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                    
                                    <%-- <c:if test="${customerGroup != null}">
                                        <option value="0">--Select--</option>
                                        <c:forEach items="${customerGroup}" var="cgl">
                                            <option value="<c:out value="${cgl.groupId}"/>" ><c:out value="${cgl.groupName}"/></option>
                                        </c:forEach>
                                    </c:if>--%>
                                        
                                </select>
                            </td>
                            <td  height="30"><font color="red">*</font>Account Manager</td>
                            <td  height="30"><input name="accountManagerId" id="accountManagerId" type="hidden" class="form-control" style="width:250px;height:40px" value="2559">
                                <input name="accountManager" id="accountManager"  type="text" class="form-control" style="width:250px;height:40px" value="" onKeyPress="return onKeyPressBlockNumbers(event);" onchange="validateManager(this.value);"></td>

                        </tr>
                        <script>
                            function validateManager(value) {
                                if (value == "") {
                                    $("#accountManagerId").val('');
                                }
                            }
                        </script>
                        <tr>
                            <td  height="30"><font color="red">*</font>Credit Limit</td>
                            <td  height="30"><input name="creditLimit" id="creditLimit" type="text" class="form-control" style="width:250px;height:40px" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10"></td>
                            <td  height="30">&nbsp;&nbsp;Credit Days</td>
                            <td  height="30"><input name="creditDays" id="creditDays" type="text" class="form-control" style="width:250px;height:40px" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10"></td>

                        </tr>
                        <tr>
                            <td  height="30"><font color="red">*</font>Detention Amount</td>
                            <td  height="30"><input name="detentionAmount" id="detentionAmount" type="text" class="form-control" style="width:250px;height:40px" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10"></td>
                            <td  height="30">&nbsp;&nbsp;Detention Cut Off Period</td>
                            <td  height="30"><input name="detentionPeriod" id="detentionPeriod" type="text" class="form-control" style="width:250px;height:40px" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10"></td>

                        </tr>
                        <tr>
                            <td><font color="red">*</font>PAN No</td>
                            <td ><input name="panNo" id="panNo" type="text" class="form-control" value="" maxlength="10" style="width:250px;height:40px" onBlur="validatePanNo()" placeholder="Type pan no..."  required></td>
                        <td >Organization Type</td>
                            <td>
                                <select class="form-control" style="width:250px;height:40px" name="organizationId" id="organizationId" style="width:125px;">

                                    <c:if test="${organizationList != null}">
                                        <option value="0">--Select--</option>
                                        <c:forEach items="${organizationList}" var="org">
                                            <option value="<c:out value="${org.orgId}"/>" ><c:out value="${org.orgName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                        </tr>
                        <!--<tr>-->
                        <!--                            <td  height="30"><font color="red">*</font>Billing Name and Address</td>
                                                    <td  height="30" colspan="3"><textarea name="billingNameAddress" id="billingNameAddress" cols="100" rows="4" style="width: 300px;"></textarea></td>-->

                        <!--                            <td >Organization Type</td>
                                                    <td>
                                                        <select class="form-control" style="width:250px;height:40px" name="organizationId" id="organizationId" style="width:125px;">
                        
                        <c:if test="${organizationList != null}">
                            <option value="">--Select--</option>
                            <c:forEach items="${organizationList}" var="org">
                                <option value="<c:out value="${org.orgId}"/>" ><c:out value="${org.orgName}"/></option>
                            </c:forEach>
                        </c:if>
                    </select>
                </td>-->

                        <!--                        <tr>
                                                    <td><font color="red">*</font>Billing State</td>
                                                    <td>
                                                        <input name="stateIdTemp" id="stateIdTemp"  type="hidden" class="form-control" >
                                                        <select name="stateId" id="stateId" class="form-control" required data-placeholder="Choose One">
                                                            <option value="">Choose One</option>
                        <c:if test = "${stateList != null}" >
                            <c:forEach items="${stateList}" var="Type">
                                <option value='<c:out value="${Type.stateId}" />'><c:out value="${Type.stateName}" /></option>
                            </c:forEach >
                        </c:if>
                    </select>
                </td>
                <td><font color="red">*</font>GST No</td>
                <td>
                    <input name="gstNo" id="gstNo"  type="text" class="form-control" >

                </td>
            </tr>-->
                    </table>

                    <h4 align="center">Address Details</h4>
                    <table class="table table-info mb30 table-hover" id="items" >
                        <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Branch Name</th>
                                    <th>Branch Address</th>
                                    <th>Branch State</th>
                                    <th>GST No</th>
                                    <th>Status</th>                                    
                            </tr>
                            </thead>
                    </table>
                    <center>
                        <input value="Add Row" class="btn btn-success" type="button" onClick="addRow();" >
                    </center>
                    &nbsp;
                    &nbsp;
                    &nbsp;
                    <center>
                        <input type="button" value="Save" id="add" class="btn btn-success" onClick="submitPage();">
                        <!--&emsp;<input type="reset" class="btn btn-success" value="Clear">-->
                    </center>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>
