<%-- 
    Document   : manageBranch
    Created on : Aug 11, 2017, 2:54:55 PM
    Author     : RAM
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });


    function setValuesBranch(sno,branchId,customerId, branchName, contactPerson,phoneNo,email, cityId, stateId, stateName, remarks, activeInd, pinCode, gstNo, customerName) {

        var count = parseInt(document.getElementById("count").value);

        for (var i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById('branchId').value = branchId;
        document.getElementById('customerId').value = customerId;
        document.getElementById('custName').value = customerName;
        document.getElementById('branchName').value = branchName;
        document.getElementById('contactPerson').value = contactPerson;
        document.getElementById('phoneNo').value = phoneNo;
       // alert(document.getElementById('phoneNo').value);
       // alert(document.getElementById('email').value);
        document.getElementById('email').value = email;
        document.getElementById('cityId').value = cityId;
        document.getElementById('stateName').value = stateId;
        document.getElementById('remarks').value = remarks;
        document.getElementById('activeInd').value = activeInd;
        document.getElementById('pinCode').value = pinCode;
        document.getElementById('gstNo').value = gstNo;
        document.getElementById('address1').value = document.getElementById('addr1'+sno).value;
        //alert(document.getElementById('addr2'+sno).value);
        document.getElementById('address2').value = document.getElementById('addr2'+sno).value;
        document.getElementById('custName').readOnly = true;
//        $('#stateName').css('pointer-events', 'none');
//        $('#cityId').css('pointer-events', 'none');
//        document.getElementById('pinCode').readOnly = true;
//        document.getElementById('gstNo').readOnly = true;

        var State = document.getElementById("stateName").value;
        var temp = State.split('-');
        document.getElementById("stateId").value = temp[1];
    }


    function submitPageBranch(value)
    {
        var errStr = "";
        if (document.getElementById("custName").value == "") {
            errStr = "Please enter Customer Name.\n";
            alert(errStr);
            document.getElementById("custName").focus();
        } else if (document.getElementById("branchName").value == "") {
            errStr = "Please select valid Branch Name.\n";
            alert(errStr);
            document.getElementById("branchName").focus();
        } else if (document.getElementById("contactPerson").value == "") {
            errStr = "Please select valid Contact Person.\n";
            alert(errStr);
            document.getElementById("contactPerson").focus();
        } else if (document.getElementById("phoneNo").value == "") {
            errStr = "Please enter Contact No.\n";
            alert(errStr);
            document.getElementById("phoneNo").focus();
        } else if (document.getElementById("address1").value == "") {
            errStr = "Please enter Address.\n";
            alert(errStr);
            document.getElementById("address1").focus();

        } else if (document.getElementById("cityId").value == 0) {
            errStr = "Please select valid City.\n";
            alert(errStr);
            document.getElementById("cityId").focus();
        } else if (document.getElementById("stateId").value == 0) {
            errStr = "Please select valid State.\n";
            alert(errStr);
            document.getElementById("stateId").focus();
        } else if (document.getElementById("pinCode").value == "") {
            errStr = "Please enter PinCode.\n";
            alert(errStr);
            document.getElementById("pinCode").focus();
        }
        else if (document.getElementById("gstNo").value == "") {
            errStr = "Please enter GST No.\n";
            alert(errStr);
            document.getElementById("gstNo").focus();
        }

        var gstNoSize = $("#gstNo").val().length;
        if (gstNoSize < 15) {
            errStr = "Please enter Valid GST No.\n";
            alert(errStr);
            document.getElementById("gstNo").focus();
        }

        if (errStr == "") {
            document.Branch.action = "/throttle/handleBranchInsert.do";
            document.Branch.method = "post";
            document.Branch.submit();
        }
    }


</script>

<script type="text/javascript">


    function contactPersonAllowAlphabet() {
        var field = document.Branch.contactPerson.value;
        if (!field.match(/^[a-zA-Z-\s]+$/) && field != "")
        {
            alert("Please Enter numbers in text");
            document.Branch.contactPerson.value = "";
            document.Branch.contactPerson.focus();
        }
    }
    function phoneNoAllowAlphabet() {
        var field = document.Branch.phoneNo.value;
        if (!field.match(/^[0-9]+$/) && field != "")
        {
            alert("Please Enter numbers in text");
            document.Branch.phoneNo.value = "";
            document.Branch.phoneNo.focus();
        }
    }




    var httpRequest;
    function getStateNames(cityId) {
//        alert(cityId)
        var url = '/throttle/handleStateName.do?cityId=' + cityId;
        if (window.ActiveXObject)
        {
            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        } else if (window.XMLHttpRequest)
        {
            httpRequest = new XMLHttpRequest();
        }

        httpRequest.open("GET", url, true);

        httpRequest.onreadystatechange = function() {
            processRequest();
        };

        httpRequest.send(null);
    }

    function processRequest() {
        if (httpRequest.readyState == 4)
        {
            if (httpRequest.status == 200)
            {
                if (httpRequest.responseText.valueOf() != "") {
                    var val = httpRequest.responseText.valueOf();
                    var state = val.split('~');
                    //                            alert(state[0]);
                    //                            alert(state[1]);

                    document.getElementById("stateId").value = state[0];
                    document.getElementById("stateName").value = state[1];
                } else
                {
                    alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                }
            }
        }
    }


    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#custName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerNameDetails.do",
                    dataType: "json",
                    data: {
                        custName: request.term,
                        customerId: document.getElementById('customerId').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#custName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#customerId').val(tmp[0]);
                $('#custName').val(tmp[1]);
//                                alert(val(tmp[1]))
                $('#panNo').val(tmp[3]);
//                                $('#gstNo').val(tmp[3]);

//                                document.getElementById('custName').readOnly = true;
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.ui-autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    })
</script>
<script>
    function setAddValue() {

        var errStr = "";
        if (document.getElementById("custName").value == "") {
            errStr = "Please enter Customer Name.\n";
            alert(errStr);
            document.getElementById("custName").focus();
            document.getElementById("stateName").value = "";
        } else {
            var State = document.getElementById("stateName").value;
            var temp = State.split('-');
            document.getElementById("stateTinNo").value = temp[0];
            document.getElementById("stateId").value = temp[1];
            document.getElementById("gstNo").value = document.getElementById("stateTinNo").value + "" + document.getElementById("panNo").value
            $('#gstValidation').show();
        }
    }
</script>
<!--<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>-->
<body>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Branch" text="Branch"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Master" text="Master"/></a></li>
                <li class=""><spring:message code="hrms.label.Branch" text="Branch"/></li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">


                <form name="Branch"  method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>


                    <table  border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" id="bg">
                        <!--<input type="hidden" name="id" id="id" value=""  />-->
                        <!--<tr>-->
                        <table class="table table-info mb30 table-hover" id="bg" >
                            <!--<div align="center" id="StatusNew" height="20" >&nbsp;&nbsp;</div>-->
                            <thead><tr>
                                    <th colspan="5" > Branch Details</th>
                                </tr>
                            </thead>

                            <tr>
                                <td  height="30"> <font color="red"> *</font>Customer Name</td>
                                <td  height="30"><input type="hidden" name="customerId" id="customerId" class="form-control" value="<c:out value='${customerId}'/>" />
                                   <input type="text" autocomplete="OFF"  name="custName" id="custName" class="form-control"  style="width:250px;height:40px;"  value="<c:out value='${customerName}'/>"  ></td>
                                <td ><font color="red"> *</font>Branch Name</td>
                                <td ><input type="hidden" name="branchId" id="branchId" value="<c:out value='${branchId}'/>"/>
                                    <input type="text" autocomplete="OFF" class="form-control" style="width:240px;height:40px" value="<c:out value='${branchName}'/>"  name="branchName" id="branchName"  value="" maxlength="200" onKeyPress="return onKeyPressBlockNumbers(event);"  /></td>
                            </tr>

                            <tr >
                                <td ><font color="red"> *</font>Contact Person </td>
                                <td ><input type="text" autocomplete="OFF" class="form-control" style="width:240px;height:40px" value="<c:out value='${contactPerson}'/>"  name="contactPerson" id="contactPerson" maxlength="25" value="" onKeyPress="return onKeyPressBlockNumbers(event);" /></td>
                                <td ><font color="red"> *</font>Contact No</td>
                                <td ><input type="text" autocomplete="OFF" class="form-control" style="width:240px;height:40px" value="<c:out value='${phoneNo}'/>"  name="phoneNo" id="phoneNo" maxlength="10" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                            </tr>
                            <tr >
                                <td >&nbsp;Email</td>
                                <td ><input type="text" autocomplete="OFF" class="form-control" style="width:240px;height:40px" value="<c:out value='${email}'/>"  name="email" id="email" value="" maxlength="50" onchange="ValidateEmail();" /></td>
                                <td ><font color="red"> *</font>Address</td>
                                <td >
                                    <textarea cols="15" autocomplete="OFF" rows="1" name="address1" id="address1" style="width:240px;height:40px"  ><c:out value='${address1}'/> </textarea>
                                </td>
                            </tr>
                            <tr >
                                <td><font color="red"> *</font>City</td>
                                <td ><select name="cityId"  class="form-control" style="width:240px;height:40px"   id="cityId" >
                                        <option value="0">-select-</option>
                                        <c:if test = "${CityList!= null}" >
                                            <c:forEach items="${CityList}" var="cList">
                                                <option  value="<c:out value='${cList.cityId}'/>"><c:out value="${cList.cityName}"/></option>
                                            </c:forEach>
                                        </c:if>
                                    </select>
                                    <script>
                                        document.getElementById("cityId").value = "<c:out value='${cityId}'/>"
                                    </script>
                                </td>

                                <td ><font color="red"> *</font>State</td>
                                <td ><input type="hidden"  class="form-control"  style="width:240px;height:40px"   name="panNo" id="panNo" value=""/>
                                    <input type="hidden"  class="form-control" style="width:240px;height:40px"   name="stateTinNo" id="stateTinNo" value=""/>
                                    <input type="hidden"  class="form-control" style="width:240px;height:40px"   name="stateId" id="stateId" value=""/>
                                    <select name="stateName" id="stateName" class="form-control" required data-placeholder="Choose One" style="width:250px;height:40px" onchange="setAddValue()">
                                        <option value="">Choose One</option>
                                        <c:if test = "${stateList != null}" >
                                            <c:forEach items="${stateList}" var="Type">
                                                <option value='<c:out value="${Type.stateIdAlt}" />-<c:out value="${Type.stateId}" />'><c:out value="${Type.stateName}" /></option>
                                            </c:forEach >
                                        </c:if>
                                    </select>
                                </td>
                            </tr>
                            <tr >
                                <td ><font color="red"> *</font>Pincode</td>
                                <td ><input type="text" autocomplete="OFF" class="form-control" style="width:240px;height:40px"   name="pinCode" id="pinCode" value="<c:out value='${pinCode}'/>" maxlength="6" onkeypress="return onKeyPressBlockCharacters(event);" /></td>

                                <td ><font color="red"> *</font>GST No</td>
                                <td ><input type="text" autocomplete="OFF" class="form-control" style="width:240px;height:40px;text-transform:uppercase"  name="gstNo" maxlength="15" id="gstNo" value="<c:out value='${gstNo}'/>" />&nbsp;&nbsp;&nbsp;<div id="gstValidation" style="display:none"><font color="red">(Please Enter Last 3 Digits)</font></div></td><br>
                            </tr>
                            <tr>
                                <td >Status</td>
                                <td >
                                    <select  class="form-control" style="width:240px;height:40px"   name="activeInd" id="activeInd">
                                        <option value="0" selected>Active</option>
                                        <option value="1">In-Active</option>
                                    </select>
                                    <script>
                                        if(<c:out value='${activeInd}'/>==''){
                                        document.getElementById("activeInd").value = "<c:out value='${activeInd}'/>";
                                        }
                                    </script>
                                </td>
                                <td>Remarks</td>
                                <td><textarea cols="15" rows="1" name="address2" id="address2" style="display:none;"  > NA</textarea>
                                    <textarea cols="15" rows="1" name="remarks" id="remarks" style="width:240px;height:40px"  ><c:out value='${remarks}'/> </textarea> </td>
                            </tr>

                            <br>
                            <br>
                            <tr >
                                <td colspan="4" ><center><input type="button" name="Save" value="Save" class="btn btn-success" onclick="submitPageBranch(this.value)"/></center></td>
                            </tr>

                        </table>
                        <br>
                        <br>
                        <table class="table table-info mb30 table-hover" id="table" >	
                            <!--<table align="center" border="0" id="tableNew" class="sortable" style="width:650px">-->
                            <thead>
                                <tr height="30">
                                    <th >Customer Name</th>
                                    <th>Branch Name</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int sno = 0;%>
                                <% int sno1 = 1;%>
                                <c:if test = "${branchList != null}">
                                    <c:forEach items="${branchList}" var="br">
                                        <%
                                                    sno++;
                                                    String className = "text1";
                                                    if ((sno % 1) == 0) {
                                                        className = "text1";
                                                    } else {
                                                        className = "text2";
                                                    }
                                        %>

                                        <tr>
                                            <td class="<%=className%>"  align="left"> <c:out value="${br.customerName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${br.branchName}" /></td>

                                            <td class="<%=className%>"  align="left"> <c:out value="${br.cityName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${br.stateName}" /></td>

                                            <c:set var="customerId" value="${br.customerId}" scope="page" />
                                            <td class="<%=className%>" align="center"> 
                                               <input type="hidden" name="addr1" id="addr1<%=sno%>" value="<c:out value="${br.address1}" />"/>
                                               <input type="hidden" name="addr2" id="addr2<%=sno%>" value="<c:out value="${br.address2}" />"/>
                                                <input type="checkbox" align="center" id="edit<%=sno%>" onclick="setValuesBranch('<%= sno%>', '<c:out value="${br.branchId}"/>', '<c:out value="${br.customerId}"/>', '<c:out value="${br.branchName}"/>', '<c:out value="${br.contactPerson}"/>', '<c:out value="${br.phoneNo}"/>', '<c:out value="${br.email}"/>', '<c:out value="${br.cityId}"/>', '<c:out value="${br.stateIdAlt}"/>-<c:out value="${br.stateId}"/>', '<c:out value="${br.stateName}"/>', '<c:out value="${br.remarks}"/>', '<c:out value="${br.activeInd}"/>', '<c:out value="${br.pinCode}"/>', '<c:out value="${br.gstNo}"/>', '<c:out value="${br.customerName}"/>');" />
                                            </td>
                                        </tr>
                                        <%sno1++;%>
                                    </c:forEach>
                                </tbody>
                            </c:if>
                        </table>

                        <input type="hidden" name="count" id="count" value="<%=sno%>" />
                        <br>
                        <br>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span>Entries Per Page</span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 1);
                        </script>
                </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="../common/NewDesign/settings.jsp" %>