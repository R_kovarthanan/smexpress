<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });

    function setConsignorDetails(sno, consignorId, customerId, consignorName, contactPerson, phoneNo, address1, address2, email, cityId, stateId, stateName, remarks, activeInd, pinCode, eFScode, customerName,gstNo) {
        
        var count = parseInt(document.getElementById("count").value);
        for (var i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById('consignorId').value = consignorId;
        document.getElementById('consignorName').value = consignorName;
        document.getElementById('custName').value = customerName;
        document.getElementById('customerId').value = customerId;
        document.getElementById('contactPerson').value = contactPerson;
        document.getElementById('phoneNo').value = phoneNo;
        document.getElementById('address1').value = address1;
        document.getElementById('address2').value = address2;
        document.getElementById('email').value = email;
        document.getElementById('cityId').value = cityId;
        document.getElementById('stateId').value = stateId;
        document.getElementById('remarks').value = remarks;
        document.getElementById('activeInd').value = activeInd;
        document.getElementById('pinCodeAdd').value = pinCode;
        document.getElementById('gstNo').value = gstNo;
        document.getElementById('consignorCode').value = eFScode;

    }


    function submitPageConsignor()
    {        
        var custId = document.getElementById("customerId").value;
        var custName = document.getElementById("custName").value;
        var gstNoSize = $("#gstNo").val().length;
        
        var errStr = "";
        if (document.getElementById("custName").value == "") {
            errStr = "Please enter Customer Name.\n";
            alert(errStr);
            document.getElementById("custName").focus();
        } 
        else if (document.getElementById("consignorCode").value == "") {
            errStr = "Please enter eFS Code.\n";
            alert(errStr);
            document.getElementById("consignorCode").focus();
        } 
        
        else if (document.getElementById("consignorName").value == "") {
            errStr = "Please select valid Consignor Name.\n";
            alert(errStr);
            document.getElementById("consignorName").focus();
        } else if (document.getElementById("contactPerson").value == "") {
            errStr = "Please select valid Contact Person.\n";
            alert(errStr);
            document.getElementById("contactPerson").focus();
        } else if (document.getElementById("phoneNo").value == "") {
            errStr = "Please enter Contact No.\n";
            alert(errStr);
            document.getElementById("phoneNo").focus();
        } else if (document.getElementById("address1").value == "") {
            errStr = "Please enter Address.\n";
            alert(errStr);
            document.getElementById("address1").focus();

        } else if (document.getElementById("cityId").value == 0) {
            errStr = "Please select valid City.\n";
            alert(errStr);
            document.getElementById("cityId").focus();
        } else if (document.getElementById("stateId").value == 0) {
            errStr = "Please select valid State.\n";
            alert(errStr);
            document.getElementById("stateId").focus();
        } else if (document.getElementById("pinCodeAdd").value == "") {
            errStr = "Please enter PinCode.\n";
            alert(errStr);
            document.getElementById("pinCodeAdd").focus();
        }
//        else if (document.getElementById("gstNo").value == "") {
//            errStr = "Please enter GST No.\n";
//            alert(errStr);
//            document.getElementById("gstNo").focus();
//        }       
//        
//        else if (gstNoSize < 15) {
//            errStr = "Please enter Valid GST No.\n";
//            alert(errStr);
//            document.getElementById("gstNo").focus();
//        }

        if(errStr == "") {
            document.consignor.action = "/throttle/handleConsignorInsert.do?custId=" + custId + "&custName=" + custName;
            document.consignor.method = "post";
            document.consignor.submit();
        }
    }

</script>

<script type="text/javascript">

    function consignorNameAllowAlphabet() {
        var field = document.consignor.consignorName.value;
        if (!field.match(/^[a-zA-Z-\s]+$/) && field != "")
        {
            alert("Please Enter numbers in text");
            document.consignor.consignorName.value = "";
            document.consignor.consignorName.focus();
        }
    }
    function contactPersonAllowAlphabet() {
        var field = document.consignor.contactPerson.value;
        if (!field.match(/^[a-zA-Z. -\s]+$/) && field != "")
        {
            alert("Please Enter numbers in text");
            document.consignor.contactPerson.value = "";
            document.consignor.contactPerson.focus();
        }
    }
    function phoneNoAllowAlphabet() {
        var field = document.consignor.phoneNo.value;
        if (!field.match(/^[0-9]+$/) && field != "")
        {
            alert("Please Enter numbers in text");
            document.consignor.phoneNo.value = "";
            document.consignor.phoneNo.focus();
        }
    }
    function consignorNameAltAllowAlphabet() {
        var field = document.consignor.consignorNameAlt.value;
        if (!field.match(/^[a-zA-Z. -\s]+$/) && field != "")
        {
            alert("Please Enter numbers in text");
            document.consignor.consignorNameAlt.value = "";
            document.consignor.consignorNameAlt.focus();
        }
    }
    function contactPersonAltAllowAlphabet() {
        var field = document.consignor.contactPersonAlt.value;
        if (!field.match(/^[a-zA-Z-\s]+$/) && field != "")
        {
            alert("Please Enter numbers in text");
            document.consignor.contactPersonAlt.value = "";
            document.consignor.contactPersonAlt.focus();
        }
    }
    function phoneNoAltAllowAlphabet() {
        var field = document.consignor.phoneNoAlt.value;
        if (!field.match(/^[0-9]+$/) && field != "")
        {
            alert("Please Enter numbers in text");
            document.consignor.phoneNoAlt.value = "";
            document.consignor.phoneNoAlt.focus();
        }
    }


    var httpRequest;
    function getStateName(cityId) {
        var url = '/throttle/handleStateName.do?cityId=' + cityId;
        if (window.ActiveXObject)
        {
            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        } else if (window.XMLHttpRequest)
        {
            httpRequest = new XMLHttpRequest();
        }

        httpRequest.open("GET", url, true);

        httpRequest.onreadystatechange = function() {
            processRequest();
        };

        httpRequest.send(null);
    }

    function processRequest() {
        if (httpRequest.readyState == 4)
        {
            if (httpRequest.status == 200)
            {
                if (httpRequest.responseText.valueOf() != "") {
                    var val = httpRequest.responseText.valueOf();
                    var state = val.split('~');
                    //                            alert(state[0]);
                    //                            alert(state[1]);

                    //                    document.getElementById("stateId").value = state[0];
                    //                    document.getElementById("stateName").value = state[1];
                } else
                {
                    alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                }
            }
        }
    }

    function checkConsignorName(value) {
        var customerId = document.getElementById('custId').value;
        var url = '/throttle/checkConsignorName.do';
        $.ajax({
            url: url,
            data: {customerId: customerId, consignName: value},
            type: "POST",
            success: function(data)
            {
                if (data > 0) {
                    //                                alert("already Exits");
                    var consignorName = $('#consignorName').val();
                    $("#StatusNew").text("ConsingnorName " + consignorName + " Already Exits");
                    $("#StatusNew").css('color', 'red');
                    $('#consignorName').val("");
                    $('#consignorName').focus();
                } else {
                    $("#StatusNew").text("");
                }
                //                            $('#contractSet').html(data);
            }
        });
    }

    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#custName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerNameDetails.do",
                    dataType: "json",
                    data: {
                        custName: request.term,
                        customerId: document.getElementById('customerId').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#custName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#customerId').val(tmp[0]);
                $('#custName').val(tmp[1]);

                //                                document.getElementById('custName').readOnly = true;
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.ui-autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    })
</script>
<body onload="document.consignor.consignorName.focus();" >
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Consignor" text="Consignor"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Master" text="Master"/></a></li>
                <li class=""><spring:message code="hrms.label.Consignor" text="Consignor"/></li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">


                <form name="consignor"  method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>


                    <table class="table table-info mb30 table-hover" id="bg" >

                        <thead><tr>
                                <th colspan="5" > Consignor Details</th>
                            </tr>
                        </thead>

                        <tr>
                            <td  height="30"><font color="red"> *</font>Customer Name</td>
                            <td  height="30"><input type="hidden" name="customerId" id="customerId" class="form-control"  value="<c:out value="${custId}"/>"/>
                                <input readonly type="textbox" name="custName" id="custName" class="form-control" style="width:250px;height:40px"  value="<c:out value="${custName}"/>"  ></td>
                            
                            <td><font color="red"> *</font>eFS Code</td>                            
                            <td><input type="text" class="form-control" style="width:240px;height:40px" name="consignorCode" id="consignorCode" maxlength="25" value="" /></td>
                        </tr>

                        <tr >
                            <td ><font color="red"> *</font>Consignor Name</td>
                            <td ><input type="hidden" name="consignorId" id="consignorId" value=""/>
                                <!--<input class="form-control"  style="width:240px;height:40px" type="text" name="consignorName" id="consignorName" maxlength="200" value="" onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9]/g, '')"  />-->
                                <input class="form-control"  style="width:240px;height:40px" type="text" name="consignorName" id="consignorName" maxlength="200" value=""   />
                            </td>
                            <td ><font color="red"> *</font>Contact Person</td>
                            <!--<td ><input type="text" class="form-control" style="width:240px;height:40px" name="contactPerson" id="contactPerson" maxlength="25" value="" onKeyPress="return onKeyPressBlockNumbers(event);" /></td>-->
                            <td ><input type="text" class="form-control" style="width:240px;height:40px" name="contactPerson" id="contactPerson" maxlength="25" value="" /></td>
                        </tr>
                        <tr >
                            <td ><font color="red"> *</font>Contact No</td>
                            <td ><input type="text" class="form-control" style="width:240px;height:40px" name="phoneNo" id="phoneNo" maxlength="10" value="" onkeyup="phoneNoAltAllowAlphabet()" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                            <td >&nbsp;Email</td>
                            <td ><input type="text" class="form-control" style="width:240px;height:40px" name="email" id="email" value="" maxlength="50"  /></td>
                        </tr>
                        <tr >
                            <td ><font color="red"> *</font>Address 1</td>
                            <td ><textarea cols="15" rows="1" name="address1" id="address1" style="width:240px;height:40px" > </textarea></td>
                            <td >&nbsp;Address 2</td>
                            <td ><textarea cols="15" rows="1" name="address2" id="address2" style="width:240px;height:40px" > </textarea></td>
                        </tr>
                        <tr >
                            <td ><font color="red"> *</font>City</td>
                            <td ><input type="text" class="form-control" style="width:240px;height:40px" name="cityId" id="cityId" value="" />
                                <!--                                <select name="cityId" class="form-control" style="width:240px;height:40px" id="cityId" onchange="getStateName(this.value)">
                                                                    <option value="0">-select-</option>
                                <c:if test = "${CityList!= null}" >
                                    <c:forEach items="${CityList}" var="cList">
                                        <option  value="<c:out value='${cList.cityId}'/>"><c:out value="${cList.cityName}"/></option>
                                    </c:forEach>
                                </c:if>
                            </select>-->
                            </td>
                            <td ><font color="red"> *</font>State</td>
                            <td >
                                <select name="stateId" class="form-control" style="width:240px;height:40px" id="stateId" onchange="getStateName(this.value)">
                                    <option value="0">-select-</option>
                                    <c:if test = "${stateList!= null}" >
                                        <c:forEach items="${stateList}" var="sList">
                                            <option  value="<c:out value='${sList.stateId}'/>"><c:out value="${sList.stateName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                                <!--                                <input type="hidden" class="form-control" style="width:240px;height:40px" name="stateId" id="stateId" value="" />
                                                                <input type="text" class="form-control" style="width:240px;height:40px" name="stateName" id="stateName" value="" readonly/></td>-->
                        </tr>
                        <tr >

                            <td ><font color="red"> *</font>Pin Code</td>
                            <td ><input type="text" class="form-control" style="width:240px;height:40px" name="pinCodeAdd" id="pinCodeAdd" value="" maxlength="6" onkeypress="return onKeyPressBlockCharacters(event);"  /></td>

                            <td >Status</td>
                            <td >
                                <select class="form-control" style="width:240px;height:40px" name="activeInd" id="activeInd">
                                    <option value="0">Active</option>
                                    <option value="1">In-Active</option>
                                </select>
                        </tr>
                        <tr >
                            <td>GST No</td>
                            <td><input type="text"  class="form-control" style="width:240px;height:40px;text-transform:uppercase" id="gstNo" name="gstNo" maxlength="15" id="gstNo" value="<c:out value='${gstNo}'/>" /></td>
                            <td >Remarks</td>
                            <td ><textarea cols="15" rows="1" name="remarks" id="remarks" style="width:240px;height:40px"> </textarea> </td>
                        </tr>

                        <tr >
                            <td colspan="4" >
                                <center>
                                    <input type="button" name="Edit" value="Save" class="btn btn-success" onclick="submitPageConsignor()"/>
                                </center>
                            </td>
                        </tr>
                    </table>
                    <br>            
                    <br> 
                    <table class="table table-info mb30 table-hover" id="table" >	
                        <!--<table align="center" border="0" id="tableNew" class="sortable" style="width:650px">-->
                        <thead>
                            <tr height="30">
                                <th >Customer Name</th>
                                <!--                                <th >Customer Site Location</th>-->
                                <th>Consignor Name</th>
                                <th >City</th>
                                <th>State</th>
                                <th>View</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% int sno = 0;%>
                            <% int sno1 = 1;%>
                            <c:if test = "${ConsignorList != null}">
                                <c:forEach items="${ConsignorList}" var="consignor">
                                    <%
                                                sno++;
                                                String className = "text1";
                                                if ((sno % 1) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>
<!--                                        <td class="<%=className%>"  align="left"> <c:out value="${consignores.customerSiteLocation}" /></td>-->
                                    <tr>
                                        <td class="<%=className%>"  align="left"> <c:out value="${consignor.customerName}" /></td>

                                        <td class="<%=className%>"  align="left"> <c:out value="${consignor.consignorName}" /></td>
                                        <td class="<%=className%>"  align="left"> <c:out value="${consignor.cityId}" /></td>
                                        <td class="<%=className%>"  align="left"> <c:out value="${consignor.stateName}" /></td>
                                        <td  >
                                            <a href="handleViewConsignor.do?customerId=<c:out value="${consignor.customerId}"/>">View</a>
                                        </td>
                                        <td class="<%=className%>" align="center"> <input type="checkbox" align="center" id="edit<%=sno%>" onclick="setConsignorDetails('<%=sno%>', '<c:out value="${consignor.consignorId}"/>', '<c:out value="${consignor.customerId}"/>', '<c:out value="${consignor.consignorName}"/>', '<c:out value="${consignor.contactPerson}"/>', '<c:out value="${consignor.phoneNo}"/>', '<c:out value="${consignor.address1}"/>', '<c:out value="${consignor.address2}"/>', '<c:out value="${consignor.email}"/>', '<c:out value="${consignor.cityId}"/>', '<c:out value="${consignor.stateId}"/>', '<c:out value="${consignor.stateName}"/>', '<c:out value="${consignor.remarks}"/>', '<c:out value="${consignor.activeInd}"/>', '<c:out value="${consignor.pinCode}"/>', '<c:out value="${consignor.consignorCode}"/>', '<c:out value="${consignor.customerName}"/>','<c:out value="${consignor.gstNo}"/>');" /></td>
                                    </tr>
                                        <c:set var="customerId" value="${consignor.customerId}" scope="page" />
                                    <%sno1++;%>
                                </c:forEach>
                            </tbody>
                        </c:if>
                    </table>
                    <input type="hidden" name="count" id="count" value="<%=sno%>" />
                    <br>
                    <br>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 1);
                    </script>
                </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="../common/NewDesign/settings.jsp" %>