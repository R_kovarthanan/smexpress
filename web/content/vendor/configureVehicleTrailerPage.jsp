<%-- 
    Document   : configureVehicleTrailerPage
    Created on : Apr 27, 2015, 10:27:19 AM
    Author     : Administrator
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });
    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });</script>

<script>
    function savePage(val) {
//        alert("Val"+val);
// document.getElementById("trailerNo"+val).value="";
        document.vehicleVendorContract.action = "/throttle/saveVehicleTrailerContract.do";
        document.vehicleVendorContract.submit();

    }

</script>
<script>
    function onKeyPressBlockNumbers(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /\d/;
        return !reg.test(keychar);
    }


    function extractNumber(obj, decimalPlaces, allowNegative)
    {
        var temp = obj.value;

        // avoid changing things if already formatted correctly
        var reg0Str = '[0-9]*';
        if (decimalPlaces > 0) {
            reg0Str += '\\.?[0-9]{0,' + decimalPlaces + '}';
        } else if (decimalPlaces < 0) {
            reg0Str += '\\.?[0-9]*';
        }
        reg0Str = allowNegative ? '^-?' + reg0Str : '^' + reg0Str;
        reg0Str = reg0Str + '$';
        var reg0 = new RegExp(reg0Str);
        if (reg0.test(temp))
            return true;

        // first replace all non numbers
        var reg1Str = '[^0-9' + (decimalPlaces != 0 ? '.' : '') + (allowNegative ? '-' : '') + ']';
        var reg1 = new RegExp(reg1Str, 'g');
        temp = temp.replace(reg1, '');

        if (allowNegative) {
            // replace extra negative
            var hasNegative = temp.length > 0 && temp.charAt(0) == '-';
            var reg2 = /-/g;
            temp = temp.replace(reg2, '');
            if (hasNegative)
                temp = '-' + temp;
        }

        if (decimalPlaces != 0) {
            var reg3 = /\./g;
            var reg3Array = reg3.exec(temp);
            if (reg3Array != null) {
                // keep only first occurrence of .
                //  and the number of places specified by decimalPlaces or the entire string if decimalPlaces < 0
                var reg3Right = temp.substring(reg3Array.index + reg3Array[0].length);
                reg3Right = reg3Right.replace(reg3, '');
                reg3Right = decimalPlaces > 0 ? reg3Right.substring(0, decimalPlaces) : reg3Right;
                temp = temp.substring(0, reg3Array.index) + '.' + reg3Right;
            }
        }

        obj.value = temp;
    }
    function blockNonNumbers(obj, e, allowDecimal, allowNegative)
    {
        var key;
        var isCtrl = false;
        var keychar;
        var reg;

        if (window.event) {
            key = e.keyCode;
            isCtrl = window.event.ctrlKey
        }
        else if (e.which) {
            key = e.which;
            isCtrl = e.ctrlKey;
        }

        if (isNaN(key))
            return true;

        keychar = String.fromCharCode(key);

        // check for backspace or delete, or if Ctrl was pressed
        if (key == 8 || isCtrl)
        {
            return true;
        }

        reg = /\d/;
        var isFirstN = allowNegative ? keychar == '-' && obj.value.indexOf('-') == -1 : false;
        var isFirstD = allowDecimal ? keychar == '.' && obj.value.indexOf('.') == -1 : false;

        return isFirstN || isFirstD || reg.test(keychar);
    }


</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<script>
    function checkVehicleTrailerAvalible() {
        var vehicleUnits =<c:out value="${vehicleUnitsDedicate}"/>;
        var trailorUnits =<c:out value="${trailorUnitsDedicate}"/>;
//        alert("vehicleUnits ==== " + vehicleUnits);
//        alert("trailorUnits ==== " + trailorUnits);
        if (vehicleUnits == "" && vehicleUnits == "0") {
            $("#vehicles1").hide();
            $("#vehicles").hide();
            $("#tabs").tabs("select", 1);
            $("ul#myTab li:nth-child(2)").addClass("active");
            $("#trailers").addClass("active");
////            $("#trailers").attr('class').indexOf("active");
////            $("#trailers1").attr('class').indexOf("active");
////            $("#trailers").addClass("active");
////            $('#tabs').tabs({selected: '2'}); 
////            $( ".selector" ).tabs( "option", "active" );
//            $( ".selector" ).tabs( '2', "active" );
        }
        if (trailorUnits == "" && trailorUnits == "0") {
            $("#trailers1").hide();
            $("#trailers").hide();
            $("#tabs").tabs("select", 2);
            $("ul#myTab li:nth-child(1)").addClass("active");
            $("#vehicles").addClass("active");
        }
    }
    function checkTrailerNos(val) {
//        alert("Sno"+val);
        var sno = val - 1;
        var trailerNoOld = document.getElementById("trailerNo" + sno).value;
        var trailerNoNew = document.getElementById("trailerNo" + val).value;
//        alert("trailerNoOld"+trailerNoOld);
//        alert("trailerNoNew"+trailerNoNew);
        if (trailerNoNew == trailerNoOld) {
            document.getElementById("checkTrailerNo").value = "1";
            document.getElementById("trailerNo" + val).value = "";
            document.getElementById("trailerNo" + val).focus();
            alert("Trailer No's already Exit's");
        } else {
            document.getElementById("checkTrailerNo").value = "0";
        }
    }

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Vendor</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Vendor</a></li>
            <li class="active">Configure Vehicle / Trailer</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="checkVehicleTrailerAvalible();">

                <style>
                    body {
                        font:13px verdana;
                        font-weight:normal;
                    }
                </style>



                <form name="vehicleVendorContract" method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
                    <br>

                    <input type="hidden" name="vendorId" id="vendorId" value="<c:out value="${vendorId}"/>" >
                    <input type="hidden" name="regNoCheck" value='exists' >
                    <input type="hidden" name="Status" id="Status" value='' >
                    <input type="hidden" name="contractDedicateId" id="contractDedicateId" value='<c:out value="${contractDedicateId}"/>' >
                    <input type="hidden" name="contractTypeId" id="contractTypeId" value='1' >
                    <input type="hidden" name="trailerStatus" id="trailerStatus" value='' >
                    <input type="hidden" name="checkTrailerNo" id="checkTrailerNo" value='' >
                    <input type="hidden" name="checkVehicleRegNo" id="checkVehicleRegNo" value='' >
                    <div id="tabs">
                        <ul class="nav nav-tabs">
                            <li id="vehicles1" data-toggle="tab"><a href="#vehicles"><span>VEHICLES</span></a></li>
                            <!--<li id="trailers1" data-toggle="tab"><a href="#trailers"><span>TRAILERS</span></a></li>-->

                        </ul>
                        <input type="hidden" name="trailerTypeId" id="trailerTypeId" value="<c:out value="${trailorTypeIdDedicate}"/>" >
                        <div id="trailers">

                            <script>
                                var container = "";
                                $(document).ready(function() {
                                    var iCnt = 1;
                                    var rowCnt = 1;
                                    // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                    container = $($("#routeFullTruck")).css({
                                        padding: '5px', margin: '20px', width: '170px', border: '0px dashed',
                                        borderTopColor: '#999', borderBottomColor: '#999',
                                        borderLeftColor: '#999', borderRightColor: '#999'
                                    });
                                    $(container).last().after('<table   id="routeDetails' + iCnt + '"  border="1">\n\
                                    <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px;"><td>TrailerType</td><td>Trailer Units</td>\n\
                                    </tr>\n\
                                    <tr><td><c:out value="${trailorTypeDedicate}"/></td>\n\
                                    <td><c:out value="${trailorUnitsDedicate}"/></td>\n\
                                    </tr></td>\n\
                                  </tr></tr>\n\
                                 </table>\n\
                                    <table class="contentsub" id="routeInfoDetailsFullTrucks' + iCnt + '" border="1"  width="73%">\n\
                                    <tr>\n\
                                    <td>Sno</td>\n\
                                    <td>TrailerNo</td>\n\
                                    <td>Remarks</td>\n\
                                    </tr>\n\
                                    <tr><td>' + rowCnt + '</td>\n\
                                     <td><input type="hidden" name="trailerType" id="trailerType' + iCnt + '" value="<c:out value="${trailorTypeIdDedicate}"/>"/><input type="text" name="trailerNo" id="trailerNo' + iCnt + '" value="" onchange="getTrailerDetails(' + iCnt + ');checkTrailerNos(' + iCnt + ');"/></td>\n\
                                    <td><input type="text" name="trailerRemarks" id="trailerRemarks' + iCnt + '" value="" /></td>\n\
                                   <table><tr>\n\
                                    <td><input class="button" type="button" name="addRouteDetailsFullTrucks" id="addRouteDetailsFullTrucks' + iCnt + rowCnt + '" value="Add" onclick="addRows(' + (parseFloat(iCnt) + parseFloat(rowCnt)) + ',' + iCnt + ',' +<c:out value="${trailorUnitsDedicate}"/> + ')" />\n\
                                    <input class="button" type="button" name="removeRouteDetailsFullTrucks" id="removeRouteDetailsFullTrucks' + iCnt + '" value="Remove"  onclick="deleteRows(' + iCnt + ')" /></td>\n\
                                    </tr></table></td></tr></table><br><br>');

                                    $('#btAdd').click(function() {
        //                                if(<c:out value="${trailorUnitsDedicate}"/> = iCnt){

                                        iCnt = iCnt + 1;
                                        $(container).last().after('<table id="mainTableFullTruck" ><tr><td>\
                                      </tr></table></td></tr></table><br><br>');

                                        $('#mainFullTruck').after(container);
        //                                }else{
        //                                    alert("test 421525");
        //                                }
                                    });
                                    $('#btRemove').click(function() {
        //                                alert($('#mainTableFullTruck tr').size());
                                        if ($(container).size() > 1) {
                                            $(container).last().remove();
                                            iCnt = iCnt - 1;
                                        }
                                    });
                                });

                                // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                                var divValue, values = '';
                                function GetTextValue() {
                                    $(divValue).empty();
                                    $(divValue).remove();
                                    values = '';
                                    $('.input').each(function() {
                                        divValue = $(document.createElement('div')).css({
                                            padding: '5px', width: '200px'
                                        });
                                        values += this.value + '<br />'
                                    });
                                    $(divValue).append('<p><b>Your selected values</b></p>' + values);
                                    $('body').append(divValue);
                                }

                                function addRows(val, valu, trailerUnit) {
        //                          alert("trailerUnit" + trailerUnit);
        //                          alert(val);
                                    var loadCnt = val;
                                    var count12 = <c:out value="${trailorUnitsDedicate}"/>;

                                    //  alert(loadCnt);
                                    var routeInfoSize = $('#routeInfoDetailsFullTrucks' + valu + ' tr').size();
                                    //  alert(routeInfoSize);
                                    if (parseInt(count12) >= parseInt(routeInfoSize)) {
                                        var addRouteDetails = "addRouteDetailsFullTrucks" + valu;
                                        var routeInfoDetails = "routeInfoDetailsFullTrucks" + valu;
                                        $('#routeInfoDetailsFullTrucks' + valu + ' tr').last().after('<tr><td>' + routeInfoSize + '</td><td><input type="hidden" name="trailerType" id="trailerType' + loadCnt + '" value="<c:out value="${trailorTypeIdDedicate}"/>"</td>\n\
                                    <td><input type="text" name="trailerNo" id="trailerNo' + loadCnt + '" value=""  onchange="getTrailerDetails(' + loadCnt + ');checkTrailerNos(' + loadCnt + ');"/></td>\n\
                                    <td><input type="text" name="trailerRemarks" id="trailerRemarks' + loadCnt + '" value="" /></td></tr>');
                                        loadCnt++;
                                    }
                                }
                                function deleteRows(val) {
                                    var loadCnt = val;
                                    // alert(loadCnt);
                                    var addRouteDetails = "addRouteDetailsFullTrucks" + loadCnt;
                                    var routeInfoDetails = "routeInfoDetailsFullTrucks" + loadCnt;
                                    if ($('#routeInfoDetailsFullTrucks' + loadCnt + ' tr').size() > 2) {
                                        $('#routeInfoDetailsFullTrucks' + loadCnt + ' tr').last().remove();
                                        loadCnt = loadCnt - 1;
                                    } else {
                                        alert('One row should be present in table');
                                    }
                                }


                                var httpRequest;
                                function getTrailerDetails(val) {
        //                alert("entered= "+val);
        //                alert(document.vehicleVendorContract.vehicleRegNo.value);
                                    var trailerNo = document.getElementById("trailerNo" + val).value;
                                    if (trailerNo != '') {
                                        var url = '/throttle/checkTrailerExists.do?trailerNo=' + trailerNo;
                                        //alert("url----"+url);
                                        if (window.ActiveXObject)
                                        {
                                            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                                        }
                                        else if (window.XMLHttpRequest)
                                        {
                                            httpRequest = new XMLHttpRequest();
                                        }
                                        httpRequest.open("POST", url, true);
                                        httpRequest.onreadystatechange = function() {
                                            go2(val);
        //                        alert("url----"+url);
                                        };
                                        httpRequest.send(null);
                                    }
                                }


                                function go2(val) {
        //                        alert("hi");
                                    if (httpRequest.readyState == 4) {
                                        if (httpRequest.status == 200) {
                                            var response = httpRequest.responseText;
                                            var temp = response.split('-');
        //                         alert("hi111");
                                            if (response != "") {
                                                alert('Trailer Already Exists');
                                                document.getElementById("trailerStatus").innerHTML = httpRequest.responseText.valueOf() + " Already Exists";
                                                document.getElementById("trailerNo" + val).value = "";
                                                document.getElementById("trailerNo" + val).focus();
                                                document.getElementById("trailerNo" + val).select();
                                                document.vehicleVendorContract.regNoCheck.value = 'exists';
                                            } else
                                            {
                                                document.vehicleVendorContract.regNoCheck.value = 'Notexists';
                                                document.getElementById("Status").innerHTML = "";
                                                document.getElementById("trailerStatus").innerHTML = "";
                                            }
                                        }
                                    }
                                }
                            </script>


                            <div id="routeFullTruck">

                            </div>
                            <center>
                                <!--<a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" /></a>-->
                            </center>
                            <!--                            <a  href="#vehicles"><input type="button" class="button" value="Save" onclick="saveVendorContract();" /></a>-->

                        </div>

                        <!--<script>
                            function saveVendorContract(){
                                document.customerContract.action = "/throttle/saveVehicleVendorContract.do";
                                document.customerContract.submit();
                            }
                         </script>-->
                        <div id="vehicles">

                            <script>
                                var contain = "";
                                $(document).ready(function() {
                                    var iCnt = 1;

                                    var rowCnt = 1;
                                    // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                    contain = $($("#routedeD")).css({
                                        padding: '5px', margin: '20px', width: '100%', border: '0px dashed',
                                        borderTopColor: '#999', borderBottomColor: '#999',
                                        borderLeftColor: '#999', borderRightColor: '#999'
                                    });
                                    $(contain).last().after('<table   id="routeDetails' + iCnt + '" class="tableborder table-bordered mb30"  border="1">\n\
                                      <tr height="30" id="tableDesingTD"><td>VehicleType</td><td>Vehicle Units</td>   \n\
                                      </tr>\n\
                                      <tr><td><c:out value="${vehicleTypeDedicate}"/></td>\n\
                                      <td><input type="hidden" name="vehicleTypeId" id="vehicleTypeId" value="<c:out value="${vehicleTypeId}"/>" ><c:out value="${vehicleUnitsDedicate}"/></td>\n\
                                      </tr></td>\n\
                                      </tr></tr>\n\
                                      </table>\n\
                                      <table class="tableborder table-bordered mb30" id="routeInfoDetailsFullTruck' + (parseFloat(iCnt) + parseFloat(rowCnt)) + '" border="1"  >\n\
                                      <tr height="30" id="tableDesingTD">\n\
                                    <td><center>Sno</center></td><td><center>VehicleRegNo</center></td><td><center>Agreed Date</center></td><td><center>MFR</center></td><td><center>Model</center></td><td><center>Remarks</center></td></tr>\n\
                                      <tr  height="30">\n\
                                      <td>' + rowCnt + '</td>\n\
                                      <td><input type="hidden" name="vehicleTypeIdcheck" id="vehicleTypeIdcheck' + iCnt + '" value="<c:out value="${vehicleTypeId}"/>" ><input type="hidden" name="contractId" id="contractId' + iCnt + '" value="" />\n\
                                      <input type="text" name="vehicleRegNo" id="vehicleRegNo' + iCnt + '" value=""  onChange="getVehicleDetails(' + iCnt + ');checkVehicleRegNos(' + iCnt + ');" style="height:30;width:160px;"/></td>\n\
                                      <td><input type="text" name="agreedDate" id="agreedDate' + iCnt + '" class="datepicker textbox" style="height:30;width:160px;"/></td>\n\
                                       <td><select type="text" name="mfr" id="mfr' + iCnt + '" onChange="ajaxData(' + iCnt + ',this.value);" style="height:30;width:160px;"><option value="0">--Select--</option><c:if test="${MfrList != null}"><c:forEach items="${MfrList}" var="Dept"><option value="<c:out value="${Dept.mfrId}"/>"><c:out value="${Dept.mfrName}"/></option></c:forEach></c:if></select></td>\n\
                                      <td><select type="text" name="model" id="model' + iCnt + '" style="height:30;width:160px;"><option value="0">---Select---</option></select></td>\n\
                                      <td><input type="text" name="remarks" id="remarks' + iCnt + '" value="" style="height:30;width:160px;"/></td>\n\
                                      </tr></table>\n\
                                      <table><tr>\n\
                                      <td><input class="btn btn-success" type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Add" onclick="addRow(' + (parseFloat(iCnt) + parseFloat(rowCnt)) + ',' + iCnt + rowCnt + ' )" />\n\
                                      <input class="btn btn-success" type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Remove"  onclick="deleteRow(' + (parseFloat(iCnt) + parseFloat(rowCnt)) + ',' + iCnt + rowCnt + ' )" /></td>\n\
                                      </tr></table></td></tr></table><br><br>');
                                    $('#btAdd').click(function() {

                                        iCnt = iCnt + 1;
                                        $(contain).last().after('<table id="mainTabledeD" ><tr><td>\
                                      <table  class="contenthead" id="routeDetailsdeD' + iCnt + '" border="1" >\n\
                                     </tr></table></td></tr></table><br><br>');
                                        callOriginAjaxdeD(iCnt);
                                        callDestinationAjaxdeD(iCnt);
                                        $('#maindeD').after(contain);

                                    });
                                    $('#btRemove').click(function() {
                                        //                                        alert($('#routeInfoDetailsFullTruck tr').size());
                                        if ($(contain).size() > 1) {
                                            $(contain).last().remove();
                                            iCnt = iCnt - 1;
                                        }
                                    });

                                });

                                $(document).ready(function() {
                                    //  alert("eeeee");
                                    $("#datepicker").datepicker({
                                        showOn: "button",
                                        buttonImage: "calendar.gif",
                                        buttonImageOnly: true

                                    });



                                });

                                $(function() {
                                    //	alert("cv");
                                    $(".datepicker").datepicker({
                                        /*altField: "#alternate",
                                         altFormat: "DD, d MM, yy"*/
                                        changeMonth: true, changeYear: true
                                    });

                                });

                                // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                                var divValue, values = '';
                                function GetTextValue() {
                                    $(divValue).empty();
                                    $(divValue).remove();
                                    values = '';
                                    $('.input').each(function() {
                                        divValue = $(document.createElement('div')).css({
                                            padding: '5px', width: '200px'
                                        });
                                        values += this.value + '<br />'
                                    });
                                    $(divValue).append('<p><b>Your selected values</b></p>' + values);
                                    $('body').append(divValue);
                                }
                                var temp;
                                function addRow(val, v1) {
                                    //     alert("val" + val);
                                    //                                      alert("v1"+v1);
                                    var count = <c:out value="${vehicleUnitsDedicate}"/>;
                                    var loadCnt = val;
                                    //                                    alert("loadCnt"+loadCnt);
                                    var routeInfoSize = $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size();
                                    //                                    alert("routeInfoSize"+routeInfoSize);
                                    var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                                    var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                                    if (parseInt(count) >= parseInt(routeInfoSize)) {
                                        $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().after('<tr  height="30"><td>' + routeInfoSize + '</td>\n\
                                  <td><input type="hidden" name="vehicleTypeIdcheck" id="vehicleTypeIdcheck' + routeInfoSize + '" value="<c:out value="${vehicleTypeIdDedicate}"/>" >\n\
                                    <input type="text" name="vehicleRegNo" id="vehicleRegNo' + routeInfoSize + '" onChange="getVehicleDetails(' + routeInfoSize + ');checkVehicleRegNos(' + routeInfoSize + ');" style="height:30;width:160px;"></td>\n\
                                  <td><input type="text" name="agreedDate" id="agreedDate' + routeInfoSize + '" class="datepicker textbox" style="height:30;width:160px;"/></td>\n\
                                    <td><select type="text" name="mfr" id="mfr' + routeInfoSize + '" onChange="ajaxData(' + routeInfoSize + ',this.value);" style="height:30;width:160px;">\n\
                                        <option value="0">--Select--</option><c:if test="${MfrList != null}"><c:forEach items="${MfrList}" var="Dept"><option value="<c:out value="${Dept.mfrId}"/>"><c:out value="${Dept.mfrName}"/></option></c:forEach></c:if></select></td>\n\
                                   <td><select type="text" name="model" id="model' + routeInfoSize + '" style="height:30;width:160px;"><option value="0">---Select---</option></select></td>\n\
                                 <td><input type="text" name="remarks" id="remarks' + routeInfoSize + '" value="" style="height:30;width:160px;"/></td></tr>');
                                        loadCnt++;
                                    }
                                    datepicker();
                                    function datepicker() {
                                        $(".datepicker").datepicker({
                                            dateFormat: 'dd-mm-yy',
                                            changeMonth: true, changeYear: true
                                        })

                                    }
                                }


                                var httpReq;
                                var temp = "";
                                function ajaxData(val, paramVal)
                                {
                                    var vehicleTypeId = document.getElementById("vehicleTypeId").value;
                                    var url = "/throttle/getModel.do?mfr=" + paramVal+"&vehicleTypeId=" +vehicleTypeId;

                                    if (window.ActiveXObject)
                                    {
                                        httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                                    }
                                    else if (window.XMLHttpRequest)
                                    {
                                        httpReq = new XMLHttpRequest();
                                    }
                                    httpReq.open("GET", url, true);
                                    httpReq.onreadystatechange = function() {
                                        processAjax(val);
                                    };
                                    httpReq.send(null);
                                }


                                function processAjax(val)
                                {
                                    if (httpReq.readyState == 4)
                                    {
                                        if (httpReq.status == 200)
                                        {
                                            temp = httpReq.responseText.valueOf();

                                            //                                          setOptions(temp, document.vehicleVendorContract.model);
                                            setOptions1(temp, document.getElementById("model" + val));

                                        }
                                        else
                                        {
                                            alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                                        }
                                    }
                                }
                                function deleteRow(val, v1) {
                                    var loadCnt = val;
                                    var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                                    var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                                    if ($('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size() > 2) {
                                        $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().remove();
                                        loadCnt = loadCnt - 1;
                                    } else {
                                        alert('One row should be present in table');
                                    }
                                }

                                function setOptions1(text, variab) {
                                    //                                alert("setOptions on page")
                                    //                                alert("text = "+text+ "variab = "+variab)
                                    variab.options.length = 0;
                                    //                                alert("1")
                                    var option0 = new Option("--select--", '0');
                                    //                                alert("2")
                                    variab.options[0] = option0;
                                    //                                alert("3")


                                    if (text != "") {
                                        //                                    alert("inside the condition")
                                        var splt = text.split('~');
                                        var temp1;
                                        variab.options[0] = option0;
                                        for (var i = 0; i < splt.length; i++) {
                                            //                                    alert("splt.length ="+splt.length)
                                            //                                    alert("for loop ="+splt[i])
                                            temp1 = splt[i].split('-');
                                            option0 = new Option(temp1[1], temp1[0])
                                            //alert("option0 ="+option0)
                                            variab.options[i + 1] = option0;
                                        }
                                    }
                                }
                                function checkVehicleRegNos(val) {
//                                            alert("Sno"+val);
                                    var sno = val - 1;
                                    var vehicleRegNoOld = document.getElementById("vehicleRegNo" + sno).value;
                                    var vehicleRegNoNew = document.getElementById("vehicleRegNo" + val).value;
                                    //        alert("vehicleRegNoOld"+vehicleRegNoOld);
                                    //        alert("vehicleRegNoNew"+vehicleRegNoNew);
                                    if (vehicleRegNoNew == vehicleRegNoOld) {
                                        document.getElementById("checkVehicleRegNo").value = "1";
                                        document.getElementById("vehicleRegNo" + val).value = "";
                                        document.getElementById("vehicleRegNo" + val).focus();
                                        alert("vehicle RegNo's already Exit's");
                                    } else {
                                        document.getElementById("checkVehicleRegNo").value = "0";
                                    }
                                }
                                var httpRequest;
                                function getVehicleDetails(val) {
                                    //   alert("entered= " + val);
                                    //                alert(document.vehicleVendorContract.vehicleRegNo.value);
                                    var vehicleRegNo = document.getElementById("vehicleRegNo" + val).value;
                                    //alert("vehicleRegNo" + vehicleRegNo);
                                    if (vehicleRegNo != '') {
                                        var url = '/throttle/checkVehicleRegNoExists.do?vehicleRegNo=' + vehicleRegNo;
                                        //     alert("url----" + url);
                                        if (window.ActiveXObject)
                                        {
                                            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                                        }
                                        else if (window.XMLHttpRequest)
                                        {
                                            httpRequest = new XMLHttpRequest();
                                        }
                                        httpRequest.open("POST", url, true);
                                        httpRequest.onreadystatechange = function() {
                                            //     alert("Hi I am Here");
                                            go1(val);
                                        };
                                        httpRequest.send(null);
                                    }
                                }


                                function go1(val) {
                                    //alert("val" + val);
                                    // alert("hi");
                                    if (httpRequest.readyState == 4) {
                                        if (httpRequest.status == 200) {
                                            var response = httpRequest.responseText;
                                            var temp = response.split('-');
                                            //     alert("hi");
                                            if (response != "") {
                                                alert('Vehicle Already Exists');
                                                document.getElementById("Status").innerHTML = httpRequest.responseText.valueOf() + " Already Exists";
                                                document.getElementById("vehicleRegNo" + val).value = "";
                                                document.getElementById("vehicleRegNo" + val).focus();
                                                document.getElementById("vehicleRegNo" + val).select();
                                                document.vehicleVendorContract.regNoCheck.value = 'exists';
                                            } else
                                            {
                                                document.vehicleVendorContract.regNoCheck.value = 'Notexists';
                                                document.getElementById("Status").innerHTML = "";
                                            }
                                        }
                                    }
                                }

                                    </script>

                                    <div id="routedeD"> </div>
                                    <center>
                                        <a  href="#"><input type="button" class="btn btn-success" value="Save" onclick="savePage(this.val);" /></a>
                                    </center>

                                </div>


                                <script>

                                    $('.btnNext').click(function() {
                                        $('.nav-tabs > .active').next('li').find('a').trigger('click');
                                    });
                                    $('.btnPrevious').click(function() {
                                        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                                    });
                                </script>

                            </div>
                            <br>
                            <br>
                            <br>
                            <center>
                                
                            </center>
                            <br>
                            <br>
                            <br>
                            <br>
                        </form>

                    </body>
                </div>
            </div>
        </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>