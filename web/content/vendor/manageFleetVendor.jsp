<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Vendor</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Vendor</a></li>
            <li class="active">Manage Fleet Vendor</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form method="post" name="fleetVendorDetail">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <br>
                    <%@ include file="/content/common/message.jsp" %>

                    <table class="table table-info mb30 table-hover"  >
                        <tr>
                            <td>
                                Select ContractType:&nbsp;&nbsp;
                                <select id="typeId" name="typeId" style="width:250px;height:40px" >
                                    <option value="1">Dedicated</option>
                                    <option value="2">Market Hire</option>
                                </select>
                            </td>
                            <td>
                                <input type="button" name="ExportExcel" value="ExportExcel" id="buttonDesign" style="width:250px;height:40px" onClick="submitPage(this.name)" class="btn btn-info">
                            </td>
                        <tr>
                    </table>
                    <br>
                    <script>
                        function submitPage(name) {
                            if (name == "ExportExcel") {
                                document.fleetVendorDetail.action = '/throttle/manageFleetVendorPage.do?param=' + name;
                                document.fleetVendorDetail.submit();
                            }
                        }
                    </script>

                    <c:if test = "${VendorList != null}" >
                        <table class="table table-info mb30 table-hover" id="table" >
                            <thead >
                                <tr >
                                    <th >S.No</th>
                                    <th >Vendor Name</th>
                                    <th >Vendor Type</th>
                                    <th >Address</th>
                                    <th >Phone No.</th>
                                    <th >Mail Id</th>
                                    <th >Credit Status</th>
                                    <th >Status</th>
                                    <th >Contract</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int sno = 0;
                            int index = 1;%>
                                <c:forEach items="${VendorList}" var="list"> 

                                    <%

                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "form-control";
                                        } else {
                                            classText = "form-control";
                                        }
                                    %>

                                    <tr  width="208" height="40" > 
                                        <td class="<%=classText%>" height="20"><%=index%></td>
                                        <td class="<%=classText%>" height="20"><c:out value="${list.vendorName}"/></td>
                                        <td class="<%=classText%>" height="20"><c:out value="${list.vendorTypeValue}"/></td>

                                        <td class="<%=classText%>" height="20"><c:out value="${list.vendorAddress}"/></td>
                                        <td class="<%=classText%>" height="20"><c:out value="${list.vendorPhoneNo}"/></td>
                                        <td class="<%=classText%>" height="20"><c:out value="${list.vendorMailId}"/></td>
                                        <td class="<%=classText%>" height="20">                                
                                            <c:if test = "${list.settlementType == '1' }" >
                                                No
                                            </c:if>
                                            <c:if test = "${list.settlementType == '2'}" >
                                                Yes
                                            </c:if>
                                        </td>

                                        <td class="<%=classText%>" height="20">
                                            <c:if test = "${list.vendorContractStatus == '0'}" >
                                                <a href="/throttle/createVehicleVendorContract.do?vendorId=<c:out value="${list.vendorId}" />&vendor=<c:out value="${list.vendorName}"/>&vendorTypeValue=<c:out value="${list.vendorTypeValue}"/>"  ><span class="label label-Primary"><spring:message code="trucks.label.Create"  text="Create"/> </span> </a>
                                            </c:if>
                                            <c:if test = "${list.vendorContractStatus == '1'}" >
                                                <a href="/throttle/viewVendorVehicleContractList.do?vendorId=<c:out value="${list.vendorId}" />&vendor=<c:out value="${list.vendorName}"/>&vendorTypeValue=<c:out value="${list.vendorTypeValue}"/>"  ><span class="label label-info"><spring:message code="trucks.label.View"  text="default text"/> </span> </a>
                                                &nbsp;
                                                &nbsp;
                                                <a href="/throttle/editVendorVehicleContract.do?vendorId=<c:out value="${list.vendorId}" />&vendor=<c:out value="${list.vendorName}"/>&vendorTypeValue=<c:out value="${list.vendorTypeValue}"/>"  ><span class="label label-warning"><spring:message code="trucks.label.Edit"  text="default text"/> </span> </a>
                                            </c:if>
                                        </td>

                                        <td align="left" >
                                            <a href="#" onclick='uploadVendorContractDetails("<c:out value="${list.vendorId}"/>", "<c:out value="${list.vendorName}"/>")'>Upload</a>                                            
                                        </td>

                                    </tr>
                                    <%
                                        index++;
                                    %>
                                </c:forEach>
                            </tbody>
                        </table>
                        <script>
                            function uploadVendorContractDetails(venId, vendorName) {
                                document.fleetVendorDetail.action = '/throttle/handleUploadVendorContract.do?vendorId=' + venId + "&vendorName=" + vendorName + "";
                                document.fleetVendorDetail.submit();
                            }
                        </script>


                        <input type="hidden" name="count" id="count" value="<%=sno%>" />
                    </c:if>
                    <br>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 1);
                    </script>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>