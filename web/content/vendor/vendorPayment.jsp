

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script>
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            dateFormat: 'dd-mm-yy',
            changeMonth: true, changeYear: true
        });
    });

    function show_src() {
        document.getElementById('exp_table').style.display = 'none';
    }
    function show_exp() {
        document.getElementById('exp_table').style.display = 'block';
    }
    function show_close() {
        document.getElementById('exp_table').style.display = 'none';
    }



    function submitPage() {
        var chek = validation1();
        if (chek == 'pass') {
            document.receivedStock.action = '/throttle/handleVendorPaymentDetails.do';
            document.receivedStock.submit();
        }
    }
    function addPayment() {
        var chek = validation();
        if (chek == 'pass') {
            if (confirm("Sure to submit", "parveen")) {
                document.receivedStock.action = '/throttle/handleAddVendorPayments.do';
                document.receivedStock.submit();
            }
        }
    }

    function validation1()
    {
        if (document.receivedStock.vendorId.value == '0') {
            alert("Please select vendor name");
            document.receivedStock.vendorId.focus();
            return 'fail';
        } else if (document.receivedStock.fromDate.value == '') {
            alert("Please enter from date");
            return 'fail';
        } else if (document.receivedStock.toDate.value == '') {
            alert("Please enter to date");
            return 'fail';
        }
        return 'pass';
    }

    function validation() {
        var selectedIndex = document.getElementsByName("selectedInd");
        var invoiceAmnt = document.getElementsByName("invoiceAmounts");
        var paidAmnt = document.getElementsByName("paidAmounts");
        var paidDate = document.getElementsByName("payDates");
        var remarks = document.getElementsByName("remarkss");
        var cntr = 0;
        for (var i = 0; i < selectedIndex.length; i++) {
            if (selectedIndex[i].checked == 1) {
                if (paidAmnt[i].value == '') {
                    alert("Please enter paid amount");
                    paidAmnt[i].focus();
                    return 'fail';
                }
                if (isFloat(paidAmnt[i].value)) {
                    alert("Please enter valid paid amount");
                    paidAmnt[i].select();
                    paidAmnt[i].focus();
                    return 'fail';
                }
                if (parseFloat(invoiceAmnt[i].value) < parseFloat(paidAmnt[i].value)) {
                    alert("Pay amount should not exceed invoice amount");
                    paidAmnt[i].focus();
                    paidAmnt[i].select();
                    return 'fail';
                }
                if (trim(remarks[i].value) == '') {
                    alert("Please enter Remarks");
                    remarks[i].focus();
                    return 'fail';
                }
                if (paidDate[i].value == '') {
                    alert("Please enter paid date");
                    return 'fail';
                }
                cntr++;
            }
        }

        if (parseInt(cntr) == 0) {
            alert("Please select any grn and proceed");
            return 'fail';
        }
        return 'pass';
    }





    function newWindow(indx) {
        var supplyId = document.getElementsByName("grnIds");
        window.open('/throttle/viewGRN.do?supplyId=' + supplyId[indx].value, 'PopupPage', 'height=450,width=700,scrollbars=yes,resizable=yes');
    }


    function detail(indx) {
        reqId = document.getElementsByName("grnIds");
        var url = '/throttle/handleRcBillDetail.do?billNo=' + reqId[indx].value;
        window.open(url, 'PopupPage', 'height=450,width=650,scrollbars=yes,resizable=yes');
    }


    function setValues() {
        if ('<%=request.getAttribute("fromDate")%>' != 'null') {
            document.receivedStock.fromDate.value = '<%=request.getAttribute("fromDate")%>';
        }
        if ('<%=request.getAttribute("toDate")%>' != 'null') {
            document.receivedStock.toDate.value = '<%=request.getAttribute("toDate")%>';
        }
        if ('<%=request.getAttribute("vendorId")%>' != 'null') {
            document.receivedStock.vendorId.value = '<%=request.getAttribute("vendorId")%>';
        }

    }


    function selectBill(val) {
        var selectedInd = document.getElementsByName("selectedInd");
        selectedInd[val].checked = 1;
    }

    function paidDate(ind)
    {
        var dat = document.getElementsByName("payDates");
        selectBill(ind);
        return dat[ind];
    }


</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Vendor</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Vendor</a></li>
            <li class="active">Vendor Payments</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="setValues();">
                <form name="receivedStock" method="post">
                    <%@ include file="/content/common/message.jsp" %>
                    <table class="table table-info mb30 table-hover" style="width:100%">
                        <tr height="22" id="tableDesingTD">
                            <th colspan="7">Vendor Payments</th>
                        </tr>
                        <tr height="25">
                            <td><font color="red">*</font>Vendor</td>
                            <td><select name="vendorId" class="textbox" style="width:180px;height:22px;">
                                    <option value="0">-select-</option>
                                    <c:if test = "${vendorList != null}" >
                                        <c:forEach items="${vendorList}" var="vendor">
                                            <option value='<c:out value="${vendor.vendorId}"/>' > <c:out value="${vendor.vendorName}"/> </option>
                                        </c:forEach>
                                    </c:if>
                                </select></td>
                            <td  ><font color="red">*</font>From Date</td>
                            <td  ><input name="fromDate" type="text" class="datepicker" value="" size="20" style="width:100px;height:22px;color:black;">
                            </td>
                            <td  ><font color="red">*</font>To Date</td>
                            <td  >
                                <input name="toDate" type="text" class="datepicker" value="" size="20" style="width:100px;height:22px;color:black">
                            </td>

                            <td><input type="button" name="" value="Search" class="btn btn-info" height="30" onclick="submitPage();" id="buttonDesign"> </td>
                        </tr>
                    </table>
                    <br>

                    <c:set var="Amount" value="0" />
                    <c:if test = "${invoiceList != null}" >
                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" height="30" class="border">
                            <!--DWLayoutTable-->

                            <tr id="tableDesingTD" height="30">
                                <td height="30"   >Sno</td>
                                <td height="30"   >GR No</td>
                                <td height="30"   style="width:20px;">Vehicle No</td>
                                <td height="30"   >Container No</td>
                                <td  height="30"   >Transporter</td>
                                <td  height="30"   >Route</td>
                                <td height="30"   >Invoice Amount</td>
                                <td  height="30"   >Pay Date</td>
                                <td height="30"   >Amount Paid </td>
                                <td colspan="3"   >Select &nbsp;</td>
                            </tr>
                            <% int index = 0;%>
                            <c:forEach items="${invoiceList}" var="vend">
                                <c:set var="vendorId1" value='<c:out value="${vend.vendorId}"/>' />
                                <%
                    String classText = "";
                    int oddEven = index % 2;
                    if (oddEven > 0) {
                        classText = "text2";
                    } else {
                        classText = "text1";
                    }
                                %>	

                                <tr height="30">
                                    <td class="<%=classText%>" height="30" > <%= index + 1%> </td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${vend.grNo}"/></td>
                                    <td class="<%=classText%>" height="30" style="width:20px;">
                                        <%--   <c:choose>
                                               <c:when test="${vend.orderType=='PURCHASE ORDER'}" >
                                               <input type="hidden" name="grnIds" value="<c:out value='${vend.supplyId}'/>"><a href="#" onClick="newWindow(<%=index%>);"><c:out value="${vend.supplyId}"/></a>
                                               </c:when>
                                               <c:otherwise>
                                               <input type="hidden" name="grnIds" value="<c:out value='${vend.supplyId}'/>"><a href="#" onClick="detail(<%=index%>);"><c:out value="${vend.supplyId}"/></a>
                                               </c:otherwise>
                                           </c:choose> --%>
                                        <c:out value="${vend.regNo}"/>
                                    </td>
                                    <td class="<%=classText%>" height="30" > 
                                        <c:out value="${vend.containerNo}"/> </td>
                                    <td class="<%=classText%>" height="30" >
                                        <c:out value="${vend.vendorName}"/> </td>
                                    <td class="<%=classText%>" height="30" >
                                        <c:out value="${vend.routeInfo}"/> </td>

                                    <td class="<%=classText%>" height="30" ><input type="text" class="textbox" id="invoiceAmounts<%=index%>" name="invoiceAmounts" value='<c:out value="${vend.inVoiceAmount}"/>' > </td>
                                    <td class="<%=classText%>" height="30" width="100" >
                                        <input type="text" size="9" name="payDates" id="payDates<%=index%>" readonly class="textbox" value='<c:out value="${vend.payDate}"/>' >
                                        <img src="/throttle/images/cal.gif" name="Calendar" id="Calendar<%=index%>" onClick="displayCalendar(document.receivedStock.payDates<%=index%>, 'dd-mm-yyyy', this)"/>
                                    </td>
                                    <td class="<%=classText%>" height="30" ><input type="text" name="paidAmounts" id="paidAmounts<%=index%>"class="textbox" value="" > </td>
                                    <td class="<%=classText%>" height="30" ><input type="checkbox" name="selectedIndex" id="selectedIndex<%=index%>"class="textbox" value="" onClick="disableCheckBox(this.id)" > </td>

                                </tr>
                                <% index++;%>
                            </c:forEach>

                        </table>
                        <input type="hidden" name="vendorId1" value='<c:out value="vendorId1" />' >
                        <br>
                        <center> <input type="button" class="btn btn-info" name="save" value="save" onClick="addPayment();" style="width:90px;height:35px;font-weight: bold;padding: 2px;" > </center>
                        </c:if>
                    <script>
                        function disableCheckBox(sno) {
                            alert(sno);
                            var selectedIndex = document.getElementsByName("selectedIndex");
                            if (document.getElementById("selectedIndex" + sno).checked == true) {
                                for (var i = 0; i < selectedIndex.length; i++) {
                                    if (i != sno)
                                    {
                                        document.getElementById("selectedIndex" + i).style = "disabled";
                                    }
                                }
                            }
                        }
                    </script>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
