<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<script>
      function setValues() {
            if ('<%=request.getAttribute("billingType")%>' != 'null') {
                document.getElementById('billingTypeId').value = '<%=request.getAttribute("billingType")%>';
            }
            if ('<%=request.getAttribute("contractFrom")%>' != 'null') {
                document.getElementById('startDate').value = '<%=request.getAttribute("contractFrom")%>';
            }
            if ('<%=request.getAttribute("contractTo")%>' != 'null') {
                document.getElementById('endDate').value = '<%=request.getAttribute("contractTo")%>';
            }
            if ('<%=request.getAttribute("billingKmCalculationType")%>' != 'null') {
                document.getElementById('billingKmCalculationId').value = '<%=request.getAttribute("billingKmCalculationType")%>';
            }
            if ('<%=request.getAttribute("maxAllowedKm")%>' != 'null') {
                document.getElementById('maxAllowedKm').value = '<%=request.getAttribute("maxAllowedKm")%>';
            }
            if ('<%=request.getAttribute("driverResponsibility")%>' != 'null') {
                document.getElementById('driverResponsibility').value = '<%=request.getAttribute("driverResponsibility")%>';
            }
            if ('<%=request.getAttribute("fuelResponsibility")%>' != 'null') {
                document.getElementById('fuelResponsibility').value = '<%=request.getAttribute("fuelResponsibility")%>';
            }
            if ('<%=request.getAttribute("extraRunKmCost")%>' != 'null') {
                document.getElementById('extraRunKmCost').value = '<%=request.getAttribute("extraRunKmCost")%>';
            }

        }

    function submitPage() {
        document.vehicleVendorContract.action = '/throttle/saveEditTransportCustomerDailyContract.do';
        document.vehicleVendorContract.submit();
    }

      function calculateTotalTrailerFixedCostE(sno) {
                               //   alert(sno);
            var trailerUnits = document.getElementById("trailerUnitE" + sno).value;
            var fixedCost = document.getElementById("costPerTrailerPerMonthE" + sno).value;
            var totalCost = parseInt(trailerUnits) * parseInt(fixedCost);
            document.getElementById("totalTrailerCostE" + sno).value = totalCost;
        }
        function calculateTotalVehicleFixedCostE(sno) {
      //  alert(sno);
            var vehicleUnits = document.getElementById("vehicleUnitE" + sno).value;
            var fixedCost = document.getElementById("fixedCostPerVehicleE" + sno).value;
            var totalCost = parseInt(vehicleUnits) * parseInt(fixedCost);
            document.getElementById("totlaFixedCostE" + sno).value = totalCost;
        }
</script>

         <div class="pageheader">
      <h2><i class="fa fa-edit"></i>  <spring:message code="sales.label.EditContract"  text="default text"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="default text"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="default text"/></a></li>
          <li><a href="general-forms.html"><spring:message code="sales.label.Sales"  text="default text"/></a></li>
          <li class="active"><spring:message code="sales.label.Contract"  text="default text"/></li>
        </ol>
      </div>
      </div>

<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
    <body onload="setValues();">
        <%--  <%try{%>--%>

        <style>
            body {
                font:13px verdana;
                font-weight:normal;
            }
        </style>



        <form name="vehicleVendorContract" method="post" >

            <br>

           <table class="table table-info mb30 table-hover">
               <thead>
                <tr>
                    <th  colspan="4" ><spring:message code="sales.label.CustomerContractInfo"  text="default text"/></th>
                </tr>
               </thead>
                <input type="hidden" name="contractId" id="contractId" value="<c:out value="${contractId}"/>" class="form-control" style="width:260px;height:40px;"/>
                <tr>
                    <td ><spring:message code="sales.label.CustomerName"  text="default text"/></td>
                    <td ><input type="hidden" name="customerId" id="customerId" value="<c:out value="${customerId}"/>" class="form-control" style="width:260px;height:40px;"><c:out value="${customerName}"/></td>
                    <td ><spring:message code="sales.label.CustomerCode"  text="default text"/></td>
                    <td ><c:out value="${customerCode}"/></td>

                </tr>
                <tr>
                    <td height="30"><spring:message code="sales.label.ContractFrom"  text="default text"/></td>
                    <td><input type="text" style="width:260px;height:40px;"name="startDate" id="startDate" value="<c:out value="${startDate}" />" class="datepicker form-control">
                    </td>

                    <td height="30"><spring:message code="sales.label.ContractTo"  text="default text"/></td>
                    <td><input type="text"  style="width:260px;height:40px;"name="endDate" id="endDate" value="<c:out value="${endDate}" />" class="datepicker form-control"></td>
                </tr>



            </table>
            <br>

            <div id="tabs">
                <ul class="">
                    <li><a href="#veh"><span><spring:message code="sales.label.DailyContractFlat"  text="default text"/></span></a></li>
                    <li><a href="#trailer"><span><spring:message code="sales.label.DailyContract(ActualKM)"  text="default text"/></span></a></li>

                    <!--<li><a href="#weightBreak"><span>Weight Break </span></a></li>-->
                </ul>
               
                <div id="trailer" align="center">
<!--                    <div id="mainFullTruck" class="contenthead2" style="width: 72%">-->
                             <table class="table table-info mb30 table-hover" id="ondemandTable">
                                <thead >
                                <tr >

                                   <th><spring:message code="sales.label.Sno"  text="default text"/></th>
                                   <th><spring:message code="sales.label.VehicleType"  text="default text"/></th>
                                   <th><spring:message code="sales.label.TrailerType"  text="default text"/></th>
                                   <th><spring:message code="sales.label.Rate(perKm)"  text="default text"/></th>
                               </tr>
                                <tbody>
                                    <% int index = 1;%>
                                    <c:forEach items="${contractTrailerDetails}" var="route">
                                        <%
                                            String classText = "";
                                            int oddEven = index % 2;
                                            if (oddEven > 0) {
                                                classText = "text2";
                                            } else {
                                                classText = "text1";
                                            }
                                        %>
                                        <tr>
                                            <td class="<%=classText%>"><%=index++%></td>
                                            <input type="hidden" name="trailerContractId" id="trailerContractId<%=index%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${route.trailerContractId}"/>" />
                                            <td class="<%=classText%>"><c:out value="${route.trailerTypeName}"/></td>
                                            <td class="<%=classText%>"><c:out value="${route.contractTypeName}"/></td>
                                            <td class="<%=classText%>"><c:out value="${route.contractTypeName}"/></td>
                                            
                                         
                                               
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
<!--                    </div>-->

                    <script>
                        var container = "";
                        function AddTrailer(){
                    //    $(document).ready(function() {
                            var iCnt = 1;
                            var rowCnt = 1;
                            // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                            container = $($("#routeFullTruck"))
                            .css({
                                padding: '5px', margin: '20px', width: '170px', border: '0px dashed',
                                borderTopColor: '#999', borderBottomColor: '#999',
                                borderLeftColor: '#999', borderRightColor: '#999'
                            });
                            $(container).last().after('<table id="mainTableFullTruck" width="100%"><tr></td>\
                             <table  class="table table-info mb30 table-hover" id="routeInfoDetailsFullTruck' + iCnt + rowCnt + '" >\n\
                              <thead> <tr><th><spring:message code="sales.label.Sno"  text="default text"/></th><th><spring:message code="sales.label.VehicleType"  text="default text"/></th><th><spring:message code="sales.label.TrailerType"  text="default text"/></th><th><spring:message code="sales.label.Rate(perKm)"  text="default text"/></th></tr> </thead> \n\
                            <td>' + rowCnt + '<input type="hidden" name="iCnt1" id="iCnt1' + iCnt + '" value="' + iCnt + '"/></td>\n\
                           <td><select class="form-control" style="width:260px;height:40px;" type="text" name="actualKmVehicleTypeId" id="actualKmVehicleTypeId' + iCnt + '" onchange="onSelectVal(this.value,' + iCnt + ');"><option value="0">--<spring:message code="sales.label.Select"  text="default text"/>--</option><c:if test="${vehicleTypeList != null}"><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></select></td>\n\
                            <td><select class="form-control" style="width:260px;height:40px;" type="text" name="actualKmTrailerTypeId" id="actualKmTrailerTypeId' + iCnt + '"><option value="0">--<spring:message code="sales.label.Select"  text="default text"/>--</option><c:if test="${trailerTypeList != null}"><c:forEach items="${trailerTypeList}" var="trailer"><option value="<c:out value="${trailer.trailerId}"/>"><c:out value="${trailer.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                           <td><input class="form-control" style="width:260px;height:40px;" type="text" name="actualKmFixedCostPerVehicle" id="actualKmFixedCostPerVehicle' + iCnt + '" value="0"  onchange="calculateTotalVehicleFixedCost(' + iCnt + ');" onKeyPress="return onKeyPressBlockCharacters(event);"/></td> \n\
                              </tr></table>\n\
                            <table border="" width=><tr>\n\
                            <td><input class="btn btn-info" type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Add" onclick="addRow(' + iCnt + rowCnt + ',' + rowCnt + ')" />\n\
                            <input class="btn btn-info" type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Remove"  onclick="deleteRow(' + iCnt + rowCnt + ')" /></td>\n\
                           </tr></table></td></tr></table><br><br>');
                          //  callOriginAjaxFullTruck(iCnt);
                          //  callDestinationAjaxFullTruck(iCnt);
                           $('#btAdd').click(function() {
                                iCnt = iCnt + 1;
                                $(container).last().after('<tr><td>' + rowCnt + '<input type="hidden" name="iCnt1" id="iCnt1' + iCnt + '" value="' + iCnt + '"/></td>\n\
                            <td><select class="form-control" style="width:260px;height:40px;" type="text" name="actualKmVehicleTypeId" id="actualKmVehicleTypeId' + iCnt + '" onchange="onSelectVal(this.value,' + iCnt + ');"><option value="0">--<spring:message code="sales.label.Select"  text="default text"/>--</option><c:if test="${vehicleTypeList != null}"><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></select></td>\n\
                            <td><select class="form-control" style="width:260px;height:40px;" type="text" name="actualKmTrailerTypeId" id="actualKmTrailerTypeId' + iCnt + '"><option value="0">--<spring:message code="sales.label.Select"  text="default text"/>--</option><c:if test="${trailerTypeList != null}"><c:forEach items="${trailerTypeList}" var="trailer"><option value="<c:out value="${trailer.trailerId}"/>"><c:out value="${trailer.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                           <td><input class="form-control" style="width:260px;height:40px;" type="text" name="actualKmFixedCostPerVehicle" id="actualKmFixedCostPerVehicle' + iCnt + '" value="0"  onchange="calculateTotalVehicleFixedCost(' + iCnt + ');" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            </tr></table>\n\
                            <table border="" align="center" width=""><tr>\n\
                            <td><input class="btn btn-info"  type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Add" onclick="addRow(' + iCnt + rowCnt + ','+ iCnt +')" />\n\
                            <input class="btn btn-info"  type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Remove" onclick="deleteRow(' + iCnt + rowCnt + ')"  /></td>\n\
                           </tr></table></td></tr></table><br><br>');
                              //  callOriginAjaxFullTruck(iCnt);
                              //  callDestinationAjaxFullTruck(iCnt);
                                $('#mainFullTruck').after(container);
                            });
                              //  callOriginAjaxFullTruck(iCnt);
                              //  callDestinationAjaxFullTruck(iCnt);
                           
                            $('#btRemove').click(function() {
//                                alert($('#mainTableFullTruck tr').size());
                                if ($(container).size() > 1) {
                                    $(container).last().remove();
                                    iCnt = iCnt - 1;
                                }
                            });
                            }
                      //  });

                        // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                        var divValue, values = '';
                        function GetTextValue() {
                            $(divValue).empty();
                            $(divValue).remove();
                            values = '';
                            $('.input').each(function() {
                                divValue = $(document.createElement('div')).css({
                                    padding: '5px', width: '200px'
                                });
                                values += this.value + '<br />'
                            });
                            $(divValue).append('<p><b>Your selected values</b></p>' + values);
                            $('body').append(divValue);
                        }

                        function addRow(val,v1) {
//                            alert(val);
//                            alert(v1);
                            var loadCnt = val;
                            var loadCnt1 = v1;
                            var routeInfoSize = $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size()-1;
                            //alert(routeInfoSize);
                            var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                            var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                            $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSize + '<input class="form-control" style="width:260px;height:40px;" type="hidden" name="iCnt1" id="iCnt1' + loadCnt + '" value="' + loadCnt1 + '"/></td><td><select class="form-control" style="width:260px;height:40px;" type="text" name="vehicleTypeId" id="vehicleTypeId' + loadCnt + '" onchange="onSelectVal(this.value,' + loadCnt + ');"><option value="0">--<spring:message code="sales.label.Select"  text="default text"/>--</option><c:if test="${vehicleTypeList != null}"><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></select></td><td><select class="form-control" style="width:260px;height:40px;" type="text" name="trailerTypeId" id="trailerTypeId' + loadCnt + '"><option value="0">--<spring:message code="sales.label.Select"  text="default text"/>--</option><c:if test="${trailerTypeList != null}"><c:forEach items="${trailerTypeList}" var="vehType"><option value="<c:out value="${vehType.seatCapacity}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td><td><input class="form-control" style="width:260px;height:40px;" type="text" name="fixedCostPerVehicle" id="fixedCostPerVehicle' + loadCnt + '" value="0"  onchange="calculateTotalVehicleFixedCost(' + loadCnt + ');" onKeyPress="return onKeyPressBlockCharacters(event);"/></td></tr>');
                            loadCnt++;
                        }
                        function deleteRow(val) {
                            var loadCnt = val;

                            var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                            var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                            if ($('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size() > 2) {
                                $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().remove();
                                loadCnt = loadCnt - 1;
                            } else {
                                alert('One row should be present in table');
                            }
                        }
                            </script>



                            <div id="routeFullTruck">

                            </div>

                             <a><input type="button" class="btn btn-info" value="<spring:message code="sales.label.AddNew"  text="default text"/>" id="AddRouteFullTruck" name="AddRouteFullTruck" onclick="AddTrailer();"/></a>
                             <a  class="pretab" href=""><input type="button" class="btn btn-info" value="<spring:message code="sales.label.Previous"  text="default text"/>" name="Previous" /></a> &nbsp;
                             <!--<a  class="nexttab" href=""><input type="button" class="btn btn-info" value="<spring:message code="sales.label.Next"  text="default text"/>" name="Next" /></a>-->
                        </div>

                        <!--<script>
                            function saveVendorContract(){
                                document.customerContract.action = "/throttle/saveVehicleVendorContract.do";
                                document.customerContract.submit();
                            }
                         </script>-->
                        <div id="veh" align="center">
                            <table class="table table-info mb30 table-hover" id="ondemandTable" id="mainTableFullDedicate">
                                <thead >
                                    <tr>
                                <th align="center"><spring:message code="sales.label.Sno"  text="default text"/></th>
                                <th align="center"><spring:message code="sales.label.VehicleType"  text="default text"/></th>
                                <th align="center"><spring:message code="sales.label.TrailerType"  text="default text"/></th>
                               <th   align="center"><spring:message code="sales.label.FixedCostPerVehicle"  text="default text"/></th>
                                    <tr>
                               </thead>
                                <tbody>
                                    <% int index1 = 1;%>
                                    <c:forEach items="${contractFlateRateDetails}" var="weight">
                                        <%
                                            String classText1 = "";
                                            int oddEven1 = index1 % 2;
                                            if (oddEven1 > 0) {
                                                classText1 = "text2";
                                            } else {
                                                classText1 = "text1";
                                            }
                                        %>
                                        <tr>
                                            <td class="<%=classText1%>"  ><%=index1++%></td>
                                            <input type="hidden" name="vehicleContractId" id="vehicleContractId<%=index1%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${weight.vehicleContractId}"/>" />
                                            <td class="<%=classText1%>"   ><c:out value="${weight.vehicleTypeName}"/></td>
                                            <td class="<%=classText1%>"   >
                                                <c:out value="${weight.trailerTypeName}"/>
                                            </td>
                                            <td class="<%=classText1%>"   >

                                                <input class="form-control" style="width:260px;height:40px;" type="text" name="fixedCostPerVehicleE" id="fixedCostPerVehicleE<%=index1%>" onchange="calculateTotalVehicleFixedCostE(<%=index1%>);"onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${weight.costPerVehiclePerMonth}"/>" onchange="calculateTotalFixedCost1(<%=index1%>,this.value);"/>
                                            </td>
                                            <!--<td></td>-->
                                            
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>

                            <script>
                                var contain = "";
                              //  $(document).ready(function() {
                              function AddVehicle(){
                                    var iCnt = 1;
                                    var rowCnt = 1;
                                    // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                    contain = $($("#routedeD")).css({
                                        padding: '5px', margin: '20px', width: '100%', border: '0px dashed',
                                        borderTopColor: '#999', borderBottomColor: '#999',
                                        borderLeftColor: '#999', borderRightColor: '#999'
                                    });
                                    $(contain).last().after('<table id="mainTableFullTruck" width="100%"><tr></td>\n\
                                   <table   class="table table-info mb30 table-hover" id="routeDetails1' + iCnt + '"  >\n\
                                    <thead> <tr>\n\
                                    <th><spring:message code="sales.label.Sno"  text="default text"/></th>\n\
                                    <th><spring:message code="sales.label.VehicleType"  text="default text"/></th>\n\
                                    <th><spring:message code="sales.label.TrailerType"  text="default text"/></th>\n\
                                    <th><spring:message code="sales.label.FixedCostpervehicle"  text="default text"/></th>\n\
                                    <tr></thead>\n\
                                   <td> ' + iCnt + ' </td>\n\
                                    <td><select class="form-control" style="width:260px;height:40px;" type="text" name="vehicleTypeId" id="vehicleTypeId' + iCnt + '" onchange="onSelectVal(this.value,' + iCnt + ');"><option value="0">--<spring:message code="sales.label.Select"  text="default text"/>--</option><c:if test="${vehicleTypeList != null}"><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></select></td>\n\
                                   <td><select class="form-control" style="width:260px;height:40px;" type="text" name="trailerTypeId" id="trailerTypeId' + iCnt + '"><option value="0">--<spring:message code="sales.label.Select"  text="default text"/>--</option><c:if test="${trailerTypeList != null}"><c:forEach items="${trailerTypeList}" var="trailer"><option value="<c:out value="${trailer.trailerId}"/>"><c:out value="${trailer.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                                   <td><input class="form-control" style="width:260px;height:40px;" type="text" name="fixedCostPerVehicle" id="fixedCostPerVehicle' + iCnt + '" value="0"  onchange="calculateTotalVehicleFixedCost(' + iCnt + ');" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                    </tr>\n\
                                    </table>\n\
                                   <table border="" width=""><tr>\n\
                                    <td><input class="btn btn-info" type="button" name="addRouteDetailsFullTruck1" id="addRouteDetailsFullTruck1' + iCnt + rowCnt + '" value="Add" onclick="addRows(' + iCnt + ')" />\n\
                                    <input class="btn btn-info" type="button" name="removeRouteDetailsFullTruck1" id="removeRouteDetailsFullTruck1' + iCnt + rowCnt + '" value="Remove"  onclick="deleteRows(' + iCnt + ')" />\n\
                                    </tr></table></td></tr></table><br><br>');
                                    callOriginAjaxdeD(iCnt);
                                    callDestinationAjaxdeD(iCnt);
                                    $('#btAdd').click(function() {
                                        iCnt = iCnt + 1;

                                        callOriginAjaxdeD(iCnt);
                                        callDestinationAjaxdeD(iCnt);
                                        $('#maindeD').after(contain);
                                    });
                                    $('#btRemove').click(function() {
//                                        alert($('#mainTabledeD tr').size());
                                        if ($(contain).size() > 2) {
                                            $(contain).last().remove();
                                            iCnt = iCnt - 1;
                                        }
                                    });
                                    }
                            //    });

                                // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                                var divValue, values = '';
                                function GetTextValue() {
                                    $(divValue).empty();
                                    $(divValue).remove();
                                    values = '';
                                    $('.input').each(function() {
                                        divValue = $(document.createElement('div')).css({
                                            padding: '5px', width: '200px'
                                        });
                                        values += this.value + '<br />'
                                    });
                                    $(divValue).append('<p><b>Your selected values</b></p>' + values);
                                    $('body').append(divValue);
                                }

                                $(document).ready(function() {
                                    $("#mAllow").keyup(function() {
//                                        alert($(this).val());
                                    });
                                })



                                function addRows(val) {
                                    //alert(val);
                                    var loadCnt = val;
                                    //    alert(loadCnt)
                                    //alert("loadCnt");
                                    //var loadCnt1 = ve;
                                    var routeInfoSize = $('#routeDetails1' + loadCnt + ' tr').size();
                                   //  alert(routeInfoSize);
                                    var routeInfoSizeSub = routeInfoSize ;
                                    var addRouteDetails = "addRouteDetailsFullTruck1" + loadCnt;
                                    var routeInfoDetails = "routeDetails1" + loadCnt;
                                    $('#routeDetails1' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSizeSub + '</td>\n\
                        <%--   <td><input type="text" name="vehicleTypeIddeDTemp" id="vehicleTypeIddeDTemp' + loadCnt + '" /></td>--%>\n\
                                   <td><select class="form-control" style="width:260px;height:40px;" type="text" name="vehicleTypeId" id="vehicleTypeId' + routeInfoSizeSub + '" onchange="onSelectVal(this.value,' + routeInfoSizeSub + ');"><option value="0">--<spring:message code="sales.label.Select"  text="default text"/>--</option><c:if test="${vehicleTypeList != null}"><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></select></td>\n\
                                   <td><input class="form-control" style="width:260px;height:40px;" type="text" name="vehicleUnits" id="vehicleUnits' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                   <td><select class="form-control" style="width:260px;height:40px;" type="text" name="contractType" id="contractType' + routeInfoSizeSub + '"><option value="0">--<spring:message code="sales.label.Select"  text="default text"/>--</option><option value="1">Fixed Km</option><option value="2">Actual Km</option></select></td>\n\
                                   <td><input class="form-control" style="width:260px;height:40px;" type="text" name="fixedCostPerVehicle" id="fixedCostPerVehicle' + routeInfoSizeSub + '" value="0" onchange="calculateTotalVehicleFixedCost(' + iCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                    <td><input class="form-control" style="width:260px;height:40px;" type="text" name="totlaFixedCost" id="totlaFixedCost' + routeInfoSizeSub + '"  value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                                    <td ><input class="form-control" style="width:260px;height:40px;" type="text" name="extraVehicleRunKm" id="extraVehicleRunKm' + routeInfoSizeSub + '"  value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                                     </tr>');
                                    // alert("loadCnt = "+loadCnt)
                                    loadCnt++;
                                    //   alert("loadCnt = +"+loadCnt)
                                }
                                function onSelectVal(val, countVal) {
                                    //                            alert("vehicleTypeIdDedicate"+val);
                                    document.getElementById("vehicleTypeIddeDTemp" + countVal).value = val;
                                }


                                function deleteRows(val) {
                                    var loadCnt = val;
                                    //     alert(loadCnt);
                                    var addRouteDetails = "addRouteDetailsFullTruck1" + loadCnt;
                                    var routeInfoDetails = "routeDetails1" + loadCnt;
                                    // alert(routeInfoDetails);
                                    if ($('#routeDetails1' + loadCnt + ' tr').size() > 2) {
                                        $('#routeDetails1' + loadCnt + ' tr').last().remove();
                                        loadCnt = loadCnt - 1;
                                    } else {
                                        alert('One row should be present in table');
                                    }
                                }

                                function calculateTotalTrailerFixedCost(sno) {
//                                    alert(sno);
                                    var trailerUnits = document.getElementById("trailerTypeUnits" + sno).value;
                                    var fixedCost = document.getElementById("fixedCostPerTrailer" + sno).value;
                                    var totalCost = parseInt(trailerUnits) * parseInt(fixedCost);
                                    document.getElementById("totalTrailerCost" + sno).value = totalCost;
                                }
                                function calculateTotalVehicleFixedCost(sno) {
//                                    alert(sno);
                                    var vehicleUnits = document.getElementById("vehicleUnits" + sno).value;
                                    var fixedCost = document.getElementById("fixedCostPerVehicle" + sno).value;
                                    var totalCost = parseInt(vehicleUnits) * parseInt(fixedCost);
                                    document.getElementById("totlaFixedCost" + sno).value = totalCost;
                                }
                               function setTextBoxEnable(val,count){
                                //    alert("enterd----"+val);
                              //  alert(count);

                                                 if(val==1){
                                                    document.getElementById("rateCost"+count).readOnly=true;
                                                    document.getElementById("rateLimit"+count).readOnly=false;
                                                    document.getElementById("maxAllowableKM"+count).readOnly=false;
                                                }
                                               if(val==2){

                                                document.getElementById("rateCost"+count).readOnly=false;
                                                document.getElementById("rateLimit"+count).readOnly=true;
                                                document.getElementById("maxAllowableKM"+count).readOnly=true;

                                               }
                                                        }

                                                        function showVehicleExtraKmRun(){
                                                            var billingKmType=document.getElementById("billingKmCalculationId").value;
                                                          //  alert("new Fun"+billingKmType);
                                                             var tbl = document.getElementById("routeDetails1");
                                                            if(billingKmType==1){
                                                               for (var i = 0; i < tbl.rows.length; i++) {

                                                                alert("here..."+tbl.rows.length);

                                                                        tbl.rows[i].cells[6].style.display ="none";


                                                                 }

                                                            }else{
                                                              // alert("else");
                                                            }

                                                        }
                            </script>

                            <script>
                                function callOriginAjaxdeD(val) {
                                    // Use the .autocomplete() method to compile the list based on input from user
                                    //alert(val);
                                    var pointNameId = 'originNamedeD' + val;
                                    var pointIdId = 'originIddeD' + val;
                                    var desPointName = 'destinationNamedeD' + val;


                                    //alert(prevPointId);
                                    $('#' + pointNameId).autocomplete({
                                        source: function(request, response) {
                                            $.ajax({
                                                url: "/throttle/getTruckCityList.do",
                                                dataType: "json",
                                                data: {
                                                    cityName: request.term,
                                                    textBox: 1
                                                },
                                                success: function(data, textStatus, jqXHR) {
                                                    var items = data;
                                                    response(items);
                                                },
                                                error: function(data, type) {

                                                    //console.log(type);
                                                }
                                            });
                                        },
                                        minLength: 1,
                                        select: function(event, ui) {
                                            var value = ui.item.Name;
                                            var id = ui.item.Id;
                                            //alert(id+" : "+value);
                                            $('#' + pointIdId).val(id);
                                            $('#' + pointNameId).val(value);
                                            $('#' + desPointName).focus();
                                            //validateRoute(val,value);

                                            return false;
                                        }

                                        // Format the list menu output of the autocomplete
                                    }).data("autocomplete")._renderItem = function(ul, item) {
                                        //alert(item);
                                        var itemVal = item.Name;
                                        itemVal = '<font color="green">' + itemVal + '</font>';
                                        return $("<li></li>")
                                                .data("item.autocomplete", item)
                                                .append("<a>" + itemVal + "</a>")
                                                .appendTo(ul);
                                    };


                                }

                                function callDestinationAjaxdeD(val) {
                                    // Use the .autocomplete() method to compile the list based on input from user
                                    //alert(val);
                                    var pointNameId = 'destinationNamedeD' + val;
                                    var pointIdId = 'destinationIddeD' + val;
                                    var originPointId = 'originIddeD' + val;
                                    var truckRouteId = 'routeIddeD' + val;
                                    var travelKm = 'travelKmdeD' + val;
                                    var travelHour = 'travelHourdeD' + val;
                                    var travelMinute = 'travelMinutedeD' + val;

                                    //alert(prevPointId);
                                    $('#' + pointNameId).autocomplete({
                                        source: function(request, response) {
                                            $.ajax({
                                                url: "/throttle/getTruckCityList.do",
                                                dataType: "json",
                                                data: {
                                                    cityName: request.term,
                                                    originCityId: $("#" + originPointId).val(),
                                                    textBox: 1
                                                },
                                                success: function(data, textStatus, jqXHR) {
                                                    var items = data;
                                                    response(items);
                                                },
                                                error: function(data, type) {

                                                    //console.log(type);
                                                }
                                            });
                                        },
                                        minLength: 1,
                                        select: function(event, ui) {
                                            var value = ui.item.Name;
                                            var id = ui.item.Id;
                                            //alert(id+" : "+value);
                                            $('#' + pointIdId).val(id);
                                            $('#' + pointNameId).val(value);
                                            $('#' + travelKm).val(ui.item.TravelKm);
                                            $('#' + travelHour).val(ui.item.TravelHour);
                                            $('#' + travelMinute).val(ui.item.TravelMinute);
                                            $('#' + truckRouteId).val(ui.item.RouteId);
                                            //validateRoute(val,value);

                                            return false;
                                        }

                                        // Format the list menu output of the autocomplete
                                    }).data("autocomplete")._renderItem = function(ul, item) {
                                        //alert(item);
                                        var itemVal = item.Name;
                                        itemVal = '<font color="green">' + itemVal + '</font>';
                                        return $("<li></li>")
                                                .data("item.autocomplete", item)
                                                .append("<a>" + itemVal + "</a>")
                                                .appendTo(ul);
                                    };


                                }

                            </script>

                            <div id="routedeD">

                            </div>
                            <a><input type="button" class="btn btn-info" value="<spring:message code="sales.label.AddNewContract"  text="default text"/>" id="AddRouteFullTruck1" name="AddRouteFullTruck1" onclick="AddVehicle();"/></a>
                            <a  class="nexttab" href=""><input type="button" class="btn btn-info" value="<spring:message code="sales.label.Next"  text="default text"/>" name="Next" /></a>

                        </div>
                        <script>

                            $(".nexttab").click(function() {
                                var selected = $("#tabs").tabs("option", "selected");
                                $("#tabs").tabs("option", "selected", selected + 1);
                            });
                            $(".pretab").click(function() {
                                var selected = $("#tabs").tabs("option", "selected");
                                $("#tabs").tabs("option", "selected", selected - 1);
                            });

                        </script>

                    </div>

                    <center>
                        <input type="button" class="btn btn-info" value="<spring:message code="sales.label.Save"  text="default text"/>" onclick="submitPage();" />

                    </center>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        <%--   <%}catch(Exception e)
     {
            out.println(e.toString());
        }
   %>--%>
    </body>

  </div>


    </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>

