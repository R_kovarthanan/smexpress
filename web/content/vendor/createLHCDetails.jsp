<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    
    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });   
    
    
    function submitPage() {
        var errStr = "";
//        if (document.getElementById("vehicleNo").value == "") {
//            errStr = "Please enter vehicleNo.\n";
//            alert(errStr);
//            document.getElementById("vehicleNo").focus();
//        }
        if (errStr == "") {
            document.lhc.action = '/throttle/handleAddLHC.do';
            document.lhc.submit();
            $("#Edit").hide();            
        }
    }
</script>
<body>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.LHC DETAILS" text="LHC"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Master" text="Master"/></a></li>
                <li class=""><spring:message code="hrms.label.LHC" text="LHC"/></li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">

                <form name="lhc"  method="post" enctype="multipart/form-data"  >                    

                    <%@ include file="/content/common/message.jsp" %>

                    <table class="table table-info mb30 table-hover" id="bg" >

                        <%
                        String vehicleTypeName = request.getParameter("vehicleTypeName");
                        String vehicleTypeId = request.getParameter("vehicleTypeId");
                        String contractHireId = request.getParameter("contractHireId");
                        String rate = request.getParameter("rate");
                        String vendor = request.getParameter("vendor");
                        %>
                        
                        <thead><tr>
                                <th colspan="5" > Lorry Hire Challan For - <%=vendor%> </th>
                            </tr>
                        </thead>
                        

                        <tr>
                            <td  height="30">Vehicle No</td>
                            <td  height="30"><input class="textbox" type="text" name="vehicleNo" id="vehicleNo" style="width:150px;height:25px;"/></td>
                            <td  height="30">Vehicle Type</td>
                            <td  height="30"><%=vehicleTypeName%></td>                            
                        </tr>
                        
<!--                        <tr>
                            <td  height="30">Driver Name</td>
                            <td  height="30"></td>                        
                            <td  height="30">License No</td>
                        <td  height="30"></td>                          
                        </tr>-->

                        <tr>
                            <td  height="30">Driver Mobile</td>
                            <td  height="30"><input class="textbox" type="text" name="driverMobile" id="driverMobile" style="width:150px;height:25px;"/></td>                      
                                <input class='textbox'  type='hidden' name='licenseNo' id='licenseNo' style="width:150px;height:25px;" />
                                <input class="textbox" type="hidden" name="driverName" id="driverName" style="width:150px;height:25px;"/>
                            <td  height="30">Agreed Rate</td>
                            <td  height="30"><input class="textbox" type="text" name="additionalCost" id="additionalCost" value="<%=rate%>" style="width:150px;height:25px;"/></td>                        
                        </tr>
                    </table>
                    <table class="table table-info mb30 table-hover" id="fileAddRow" >
                    
                        <thead>
                            <tr>
                            <th  height="30">Sno</th>
                            <th  height="30">Document</th>
                            <th  height="30">Expiry Date</th>
                            <th  height="30">Upload</th>
                        </tr>
                        </thead>
                        <tr>
                            <td  height="30">1</td>
                            <td>Insurance</td>
                            <!--<td  height="30"><input class='textbox'  type='text' name='insuranceNo' id='insuranceNo' style="width:150px;height:25px;" /></td>-->
                            <td  height="30"><input type="text" name="insuranceDate" id="insuranceDate" value="" class="datepicker form-control"  style="height:22px;width:150px;color:black;"></td>
                            <td  height="30"><input class='textbox'  type='file' name='insurance' id='insurance' style="width:200px;height:25px;" /></td>
                        </tr>
                        <tr>
                            <td  height="30">2</td>
                            <td>Road Tax</td>
                            <!--<td  height="30"><input class='textbox'  type='text' name='roadTaxNo' id='roadTaxNo' style="width:150px;height:25px;" /></td>-->
                            <td  height="30"><input type="text" name="roadTaxDate" id="roadTaxDate" value="" class="datepicker form-control"  style="height:22px;width:150px;color:black;"></td>
                            <td  height="30"><input class='textbox'  type='file' name='roadTax' id='roadTax' style="width:200px;height:25px;" /></td>
                        </tr>
                        <tr>
                            <td  height="30">3</td>
                            <td>FC</td>
                            <!--<td  height="30"><input class='textbox'  type='text' name='fcNumber' id='fcNumber' style="width:150px;height:25px;" /></td>-->
                            <td  height="30"><input type="text" name="fcDate" id="fcDate" value="" class="datepicker form-control"  style="height:22px;width:150px;color:black;"></td>
                            <td  height="30"><input class='textbox'  type='file' name='fc' id='fc' style="width:200px;height:25px;" /></td>
                        </tr>
                        <tr>
                            <td  height="30">4</td>
                            <td>Permit</td>
                            <!--<td  height="30"><input class='textbox'  type='text' name='permitNumber' id='permitNumber' style="width:150px;height:25px;" /></td>-->
                            <td  height="30"><input type="text" name="permitDate" id="permitDate" value="" class="datepicker form-control"  style="height:22px;width:150px;color:black;"></td>
                            <td  height="30"><input class='textbox'  type='file' name='permit' id='permit' style="width:200px;height:25px;" /></td>
                        </tr>
                        
                    </table>
                        <br/>
                    <center>
                        <br/>
                        <input type="button" class="btn btn-info" id="Edit" name="Edit" value="Save" style="width:90px;height:30px;font-weight: bold;padding:1px;" onclick="submitPage();"/>
                        <input type="hidden" name="contractHireId" id="contractHireId" class="form-control"  value="<%=contractHireId%>"/>
                        <input type="hidden" name="vehicleTypeId" id="vehicleTypeId" class="form-control"  value="<%=vehicleTypeId%>"/>
                    </center>

                    <br> 
                    
                </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="../common/NewDesign/settings.jsp" %>