<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true
        });
    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });

    function pastContractDetails(venId,name) {
        window.open('/throttle/pastContractDetails.do?vendorId='+venId+"&venName="+name+"&type=search", 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Vendor Contract</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Vendor</a></li>
            <li class="active">View Fleet Vendor Contract</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>

                <style>
                    body {
                        font:13px verdana;
                        font-weight:normal;
                    }
                </style>


                <form name="vehicleVendorContract" method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
                    <br>
                    <%--   <c:if test="${vendorVehicleContractLists != null}">
                        <c:forEach items="${vendorVehicleContractLists}" var="vendorContract">--%>
                    <%--    </c:forEach></c:if>--%>
                    <table width="990px;" align="center" border="0"  >
                        <tr id="tableDesingTD" height="30">
                            <td  colspan="4" >View Vendor Contract Info</td>
                        </tr>
                        <tr height="30">
                            <td class="text1">Vendor Name</td>
                            <td class="text1"><input type="hidden" name="vendorId" id="vendorId" value="<c:out value="${vendorId}"/>" class="textbox"><c:out value="${vendorName}"/></td>
                            <td class="text1">Contract Type</td>
                            <td class="text1"><input type="hidden" name="contractTypeId" id="contractTypeId" value="<c:out value="${contractTypeId}"/>" style="height:22px;width:150px;"/>
                                <c:if test="${contractTypeId == '1'}">
                                    Dedicated
                                </c:if>
                                <c:if test="${contractTypeId == '2'}">
                                    Hired
                                </c:if>
                                <c:if test="${contractTypeId == '3'}">
                                    Both
                                </c:if>
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="text2">Contract From</td>
                            <td class="text2"><input type="hidden" name="startDate" id="startDate" value="<c:out value="${startDate}"/>" class="datepicker"><c:out value="${startDate}"/></td>
                            <td class="text2">Contract To</td>
                            <td class="text2"><input type="hidden" name="endDate" id="endDate" value="<c:out value="${endDate}"/>" class="datepicker"><c:out value="${endDate}"/></td>

                        </tr>
                        <tr height="30">
                            <td class="text1">Payment Type </td>
                            <td class="text1"><input type="hidden" name="paymentType" id="paymentType" value="<c:out value="${paymentType}"/>"/>
                                <c:if test="${paymentType =='1'}">
                                    Monthly
                                </c:if>
                                <c:if test="${paymentType == '2'}">
                                    Advance
                                </c:if>
                                <c:if test="${paymentType == '3'}">
                                    FortNight
                                </c:if>
                            </td>
                            <td class="text1" colspan="2">&emsp;
                                <!--<a href="#" onclick="pastContractDetails('<c:out value="${vendorId}"/>','<c:out value="${vendorName}"/>');">Past Contracts</a>-->
                            </td>
                        </tr>

                    </table>
                    <br>
                    <br>
                    <div id="tabs" >
                        <ul class="nav nav-tabs">
                            <%--<c:if test="${contractTypeId == 1}">--%>
                            <!--                            <li class="active" data-toggle="tab"><a href="#deD"><span>DEDICATED</span></a></li>-->
                            <%--     </c:if>--%>
                            <%--  <c:if test="${contractTypeId == 2}">--%>
                            <%--   </c:if>--%>
                            <li data-toggle="tab"><a href="#fullTruck"><span>Market Hire</span></a></li>
<!--                            <li data-toggle="tab" id="pc" style="display: block"><a href="#pcm"><span> Penality charges </span></a></li>
                            <li data-toggle="tab" id="dc" style="display: block"><a href="#dcm"><span> Detention charges </span></a></li>-->
                        </ul>

                        <%--   <c:if test="${contractTypeId == 2}">--%>
                        <div id="fullTruck" class="tab-pane active">
                            <div id="routeFullTruck">
                                <c:if test="${hireList != null}">
                                    <table align="center" border="0" id="table" class="sortable" style="width:100%" >
                                        <tr style="height: 40px;" id="tableDesingTH">
                                            <th align="center">S.No</th>
                                            <th align="center">From Date</th>
                                            <th align="center">To Date</th>
                                            <th align="center">Vehicle Type</th>
                                            <!--<th align="center">Vehicle Units<br><br></th>-->
                                            <th align="center">Route</th>
                                            <!--                                            <th align="center">Touch Point</th>-->
                                            <!--                                            <th align="center">Interim Point2</th>
                                                                                        <th align="center">Interim Point3</th>
                                                                                        <th align="center">Interim Point4</th>-->
                                            <!--<th align="center">Destination</th>-->
                                            <!--                                        <th align="center">Trailer Type</th>
                                                                                    <th align="center">Trailer Units</th>-->
                                            <th align="center">Load Type</th>
                                            <th align="center">Rate</th>
                                            <!--<th align="center">Rate without Reefer</th>-->
                                            <!--                                             <td align="center">Container Type</td>
                                                                                        <td align="center">Container Qty</td>-->
                                            <td align="center">Advance Type</td>
                                            <td align="center">Advance Value</td>
                                            <td align="center">Initial Advance</td>
                                            <td align="center">Remaining Amount</td>

                                            <td align="center">Current Status</td>
                                            <!--<td align="center">Market Rate</td>-->
                                            <!--                                            <th align="center">Travel Kms</th>
                                                                                        <th align="center">Travel Hours</th>
                                                                                        <th align="center">Travel Minutes</th>-->
                                            <th align="center">Active Status</th>
                                        </tr>
                                        <tbody>
                                            <% int index = 1;%>
                                            <c:forEach items="${hireList}" var="route">
                                                <%
                                                    String classText = "";
                                                    int oddEven = index % 2;
                                                    if (oddEven > 0) {
                                                        classText = "text2";
                                                    } else {
                                                        classText = "text1";
                                                    }
                                                %>
                                                <tr height="30">
                                                    <td class="<%=classText%>"><%=index++%></td>
                                                    <td class="<%=classText%>"><c:out value="${route.startDate}"/></td>
                                                    <td class="<%=classText%>"><c:out value="${route.endDate}"/></td>
                                                    <td class="<%=classText%>"><c:out value="${route.vehicleTypeName}"/></td>
                                                    <!--<td class="<%=classText%>"><c:out value="${route.vehicleUnits}"/></td>-->
                                                    <td class="<%=classText%>"><c:out value="${route.originNameFullTruck}"/>-<c:out value="${route.point1Name}"/>-<c:out value="${route.destinationNameFullTruck}"/></td>
                                                    <!--<td class="<%=classText%>"><c:out value="${route.point1Name}"/></td>-->
<!--                                                <td class="<%=classText%>"><c:out value="${route.point2Name}"/></td>
                                                    <td class="<%=classText%>"><c:out value="${route.point3Name}"/></td>
                                                    <td class="<%=classText%>"><c:out value="${route.point4Name}"/></td>-->
                                                    <!--<td class="<%=classText%>"><c:out value="${route.destinationNameFullTruck}"/></td>-->

<!--                                            <td class="<%=classText%>"><c:out value="${route.trailerType}"/></td>
                                                <td class="<%=classText%>"   ><c:out value="${route.trailorTypeUnits}"/></td>-->
                                                    <td class="<%=classText%>"   >
                                                        <c:if test="${route.loadTypeId =='1'}">
                                                            Empty Trip
                                                        </c:if>
                                                        <c:if test="${route.loadTypeId =='2'}">
                                                            Load Trip
                                                        </c:if>
                                                    </td>

<!--<td class="<%=classText%>"   ><c:out value="${route.spotCost}"/></td>-->
                                                    <td class="<%=classText%>"   ><c:out value="${route.additionalCost}"/></td>
        <!--                                            <td class="<%=classText%>"   ><c:out value="${route.travelKmFullTruck}"/></td>
                                                    <td class="<%=classText%>"   ><c:out value="${route.travelHourFullTruck}"/></td>
                                                    <td class="<%=classText%>"   ><c:out value="${route.travelMinuteFullTruck}"/></td>
                                                   <td><c:if test="${containerTypeList != null}">
                                                                    <select name="containerTypeId" id="containerTypeId" style="height:22px;width:130px;" disabled>
                                                                 <option value="0">-Select-</option>
                                                        <c:forEach items="${containerTypeList}" var="conType">
                                                            <option value="<c:out value="${conType.containerId}"/>"><c:out value="${conType.containerName}"/></option>
                                                        </c:forEach>
                                                    </c:if>
                                                    </select>
                                                   </td>
                                                   <td><select name="containerQty" id="containerQty" style="height:22px;width:130px;" disabled>
                                                           <option value="0">-Select-</option>
                                                           <option value="1">1</option>
                                                           <option value="2">2</option>
                                                       </select>
                                                   </td>-->

                                                    <td class="<%=classText%>"><c:out value="${route.advanceTripMode}"/></td>
                                                    <td class="<%=classText%>"><c:out value="${route.modeTripRate}"/></td>
                                                    <td class="<%=classText%>"><c:out value="${route.initialTripAdvance}"/></td>
                                                    <td class="<%=classText%>"><c:out value="${route.endTripAdvance}"/></td>

                                                    <c:if test="${route.approvalStatus == 1}">
                                                        <td class="<%=classText%>"   ><span class="label label-success">approved</span></td>
                                                    </c:if>
                                                    <c:if test="${route.approvalStatus == 2}">
                                                        <td class="<%=classText%>"   ><span class="label label-warning">pending</span></td>
                                                    </c:if>
                                                    <c:if test="${route.approvalStatus == 3}">
                                                        <td class="<%=classText%>"   ><span class="label label-danger">rejected</span></td>
                                                    </c:if>

                                                    <td class="<%=classText%>"   >
                                                        <c:if test="${route.activeInd == 'Y'}">
                                                            Active
                                                        </c:if>
                                                        <c:if test="${route.activeInd == 'N'}">
                                                            In Active
                                                        </c:if>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </c:if>
                            </div>
                            <br>
                            <br>
                            <center>
                                <!--<a><input type="button" class="btn btn-info btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>-->
                            </center>
                            <br>
                            <br>
                        </div>

                        <div id="pcm" class="tab-pane" style="display:none">
                            <div class="inpad">
                                <table class="table table-info mb30 table-hover" width="90%" id="penalitytable">
                                    <thead>
                                        <tr id="rowId0" >
                                            <td    id="tableDesingTD">S No</td>
                                            <td    id="tableDesingTD">Charges names</td>
                                            <td    id="tableDesingTD">Unit</td>
                                            <td    id="tableDesingTD">Amount</td>
                                            <td    id="tableDesingTD">Remarks</td>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <% int sno = 0;%>
                                        <c:forEach items="${viewpenalitycharge}" var="vpc">
                                            <%
                                                        sno++;
                                            %>
                                            <tr>
                                                <td   align="left" > <%= sno%>  </td>
                                                <td   align="left"> <c:out value="${vpc.penality}" /></td>
                                                <td   align="left"> <c:out value="${vpc.pcmunit}" /></td>
                                                <td align="left"   >
                                                    <input type="hidden" name="penalitycharge" class="form-control" style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${vpc.chargeamount}" />"/><c:out value="${vpc.chargeamount}" />
                                                </td>
                                                <td   align="left"> <c:out value="${vpc.pcmremarks}" /></td>
                                                <td align="left"   ><input type="hidden"  name="penid" id="penid" value="<c:out value="${vpc.id}"/>" /></td>
                                            </tr>
                                        </c:forEach>

                                        <tr>
                                    <input type="hidden" name="selectedRowCount" id="sno" value="1"/>
                                    <input type="hidden" name="tripSheetId" id="tripSheetId" value="<%=request.getParameter("tripSheetId")%>" />
                                    </tbody>
                                </table>
                                <br>
                                <SCRIPT language="javascript">
                                    var poItems = 1;
                                    var rowCount = 2;
                                    var sno = 1;
                                    var snumber = 1;

                                    function addRowPenality()
                                    {
                                        //                                           alert(rowCount);
                                        if (sno < 20) {
                                            sno++;
                                            var tab = document.getElementById("penalitytable");
                                            rowCount = tab.rows.length;

                                            snumber = (rowCount) - 1;
                                            if (snumber == 1) {
                                                snumber = parseInt(rowCount);
                                            } else {
                                                snumber++;
                                            }
                                            snumber = snumber - 1;
                                            var newrow = tab.insertRow((rowCount));
                                            newrow.height = "30px";
                                            // var temp = sno1-1;
                                            var cell = newrow.insertCell(0);
                                            var cell0 = "<td class='text1' align='center'> " + snumber + "</td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(1);
                                            cell0 = "<td class='text1'><input type='text' name='penality' id='penality" + snumber + "' class='form-control' /></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(2);
                                            cell0 = "<td class='text1'><input type='text' name='pcmunit' id='pcmunit" + snumber + "' class='form-control' /></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(3);
                                            cell0 = "<td class='text1'><input type='text' name='chargeamount' id='chargeamount" + snumber + "' class='form-control' onKeyPress='return onKeyPressBlockCharacters(event);' /></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(4);
                                            cell0 = "<td class='text1'><input type='text' name='pcmremarks' id='pcmremarks" + snumber + "' class='form-control' /></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            rowCount++;
                                        }
                                    }

                                </SCRIPT>
                            </div>
                            &nbsp;&nbsp;
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                            </center>
                            <br>
                        </div>


                        <div id="dcm" class="tab-pane" style="display:none">
                            <div class="inpad">
                                <table class="table table-info mb30 table-hover" width="90%" id="detentiontable">
                                    <thead>
                                        <tr id="rowId1" >
                                            <td id="tableDesingTD" >S No</td>
                                            <td id="tableDesingTD"  >Vehicle type</td>
                                            <td id="tableDesingTD"  >Time Slot From(Days)</td>
                                            <td id="tableDesingTD"  >Time Slot To(Days)</td>
                                            <td id="tableDesingTD"  >Amount</td>
                                            <td id="tableDesingTD"  >Remarks</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <% int Sno = 0;%>
                                        <c:forEach items="${viewdetentioncharge}" var="vdc">
                                            <%
                                                        Sno++;
                                            %>
                                            <tr>
                                                <td   align="left"> <%= Sno%>  </td>
                                                <td   align="left"> <c:out value="${vdc.vehicleTypeName}" /></td>
                                                <td   align="left"> <c:out value="${vdc.timeSlot}" /></td>
                                                <td   align="left"> <c:out value="${vdc.timeSlotTo}" /></td>

                                                <td align="left"   >
                                                    <input type="hidden" name="detentioncharge" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${vdc.chargeamt}" />"  style="width:150px;height:44px;"/><c:out value="${vdc.chargeamt}" />
                                                </td>
                                                <td   align="left"> <c:out value="${vdc.dcmremarks}" /></td>
                                                <td align="left"   ><input type="hidden"  name="denid" id="denid" value="<c:out value="${vdc.id}"/>" /></td>

                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                                <br>
                                <SCRIPT language="javascript">
                                    var poItems1 = 1;
                                    var rowCount1 = 2;
                                    var sno1 = 1;
                                    var snumber1 = 1;

                                    function addRowdetention()
                                    {
                                        //                                           alert(rowCount);
                                        if (sno1 < 20) {
                                            sno1++;
                                            var tab = document.getElementById("detentiontable");
                                            rowCount1 = tab.rows.length;

                                            snumber1 = (rowCount1) - 1;
                                            if (snumber1 == 1) {
                                                snumber1 = parseInt(rowCount1);
                                            } else {
                                                snumber1++;
                                            }
                                            //                                            snumber1 = snumber1-1;
                                            //alert( (rowCount)-1) ;
                                            //alert(rowCount);
                                            var newrow = tab.insertRow((rowCount1));
                                            newrow.height = "30px";
                                            // var temp = sno1-1;
                                            var cell = newrow.insertCell(0);
                                            var cell0 = "<td class='text1' align='center'> " + snumber1 + "</td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(1);
                                            cell0 = "<td class='text1' ><select class='form-control' name='detention' id='detention" + snumber1 + "'  style='width:200px;height:44px;'><option value='0'>-Select-</option><c:if test = "${vehicleTypeList!= null}" ><c:forEach  items="${vehicleTypeList}" var="vehType"><option value='<c:out value="${vehType.vehicleTypeId}" />'><c:out value="${vehType.vehicleTypeName}" /></option></c:forEach ></c:if></select></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(2);
                                            cell0 = "<td class='text1' ><select   name='dcmunit' id='dcmunit" + snumber1 + "' class='form-control' style='width:200px;height:44px;'/><option value='0'>-Select-</option><c:if test = "${detaintionTimeSlot!= null}" ><c:forEach  items="${detaintionTimeSlot}" var="detain"><option value='<c:out value="${detain.id}" />'><c:out value="${detain.timeSlot}" /></option></c:forEach ></c:if></select></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(3);
                                            cell0 = "<td class='text1' ><select   name='dcmToUnit' id='dcmToUnit" + snumber1 + "' class='form-control' style='width:200px;height:44px;'/><option value='0'>-Select-</option><c:if test = "${detaintionTimeSlot!= null}" ><c:forEach  items="${detaintionTimeSlot}" var="detain"><option value='<c:out value="${detain.id}" />'><c:out value="${detain.timeSlot}" /></option></c:forEach ></c:if></select></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(4);
                                            cell0 = "<td class='text1' ><input type='text' name='chargeamt' id='chargeamt" + snumber1 + "' class='form-control' style='width:200px;height:44px;' onKeyPress='return onKeyPressBlockCharacters(event);' /></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(5);
                                            cell0 = "<td class='text1'><input type='text' name='dcmremarks' id='dcmremarks" + snumber1 + "' class='form-control' style='width:200px;height:44px;'/></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            rowCount1++;
                                        }
                                    }

                                        </SCRIPT>

                                    </div>
                                    <br>
                                    <center>
                                        <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                    </center>
                                    <br>
                                </div>             
                        <%--   </c:if>--%>



                        <%--   <c:if test="${contractTypeId == 1}">--%>
                        <div id="deD"  class="tab-pane active" style="padding-left: 1px; display: none">
                            <div id="routeWeightBreak">
                                <c:if test="${dedicateList != null}">
                                    <table align="center" border="1" id="table" class="sortable" style="width:100%;" >
                                        <tr style="height: 40px;" id="tableDesingTH">
                                            <th align="center">S.No</th>
                                            <th align="center">Vehicle Type</th>
                                            <th align="center">Vehicle Units</th>
                                            <th align="center">Trailer Type</th>
                                            <th align="center">Trailer Units</th>
                                            <th align="center">Contract Category</th>
                                            <th align="center">Fixed Cost Per Vehicle & Month</th>
                                            <th colspan="2" align="center">Fixed Duration Per day<br>&emsp14;
                                        <table   border="0"><tr  id="tableDesingTD"><td width="250px;"><center>Hours</center></td><td width="212px;"><center>Minutes</center></td></tr></table></th>
                                        <th align="center">Total Fixed Cost Per Month</th>
                                        <th align="center">Rate Per KM</th>
                                        <th align="center">Rate Exceeds Limit KM</th>
                                        <th align="center">Max Allowable KM Per Month</th>
                                        <th align="center" colspan="2">Over Time Cost Per HR<br>&emsp14;
                                        <table   border="0"><tr  id="tableDesingTD"><td width="250px;"><center>Work Days</center></td><td width="212px;"><center>Holidays</center></td></tr></table></th>
                                        <th align="center">Additional Cost</th>
                                        <th align="center">Active Status</th>

                                        </tr>
                                        <tbody>
                                            <% int index1 = 1;%>
                                            <c:forEach items="${dedicateList}" var="weight">
                                                <%
                                                    String classText1 = "";
                                                    int oddEven1 = index1 % 2;
                                                    if (oddEven1 > 0) {
                                                        classText1 = "text2";
                                                    } else {
                                                        classText1 = "text1";
                                                    }
                                                %>
                                                <tr height="30">
                                                    <td class="<%=classText1%>"  ><%=index1++%></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.vehicleTypeIdDedicate}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.vehicleUnitsDedicate}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.trailorTypeDedicate}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.trailorUnitsDedicate}"/></td>
                                                    <td class="<%=classText1%>"   >
                                                        <c:if test="${weight.contractCategory == '1'}">
                                                            Fixed </c:if>
                                                        <c:if test="${weight.contractCategory == '2'}">
                                                            Actual
                                                        </c:if></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.fixedCost}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.fixedHrs}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.fixedMin}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.totalFixedCost}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.rateCost}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.rateLimit}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.maxAllowableKM}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.workingDays}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.holidays}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.addCostDedicate}"/></td>
                                                    <td class="<%=classText1%>"   >
                                                        <c:if test="${weight.activeInd == 'Y'}">
                                                            Active
                                                        </c:if>
                                                        <c:if test="${weight.activeInd == 'N'}">
                                                            In Active
                                                        </c:if>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table
                                </c:if>
                            </div>

                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-info btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                            </center>
                            <br>
                            <br>
                        </div>
                        <%--       </c:if>--%>
                        <script>
                            $('.btnNext').click(function() {
                                $('.nav-tabs > .active').next('li').find('a').trigger('click');
                            });
                            $('.btnPrevious').click(function() {
                                $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                            });
                        </script>


                    </div>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
