<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    function validatePanNo() {
        var status = true;
        var nPANNo = document.getElementById("panNo").value;
        if (nPANNo != "") {
            document.getElementById("panNo").value = nPANNo.toUpperCase();
            var ObjVal = nPANNo;
            var pancardPattern = /^([ABCFGHLJPT]{1})([A-Z]{2})([PCFH]{1})([A-Z]{1})(\d{4})([A-Z]{1})$/;
            var patternArray = ObjVal.match(pancardPattern);
            if (patternArray == null) {
                alert("PAN Card No Invalid ");
                status = false;
            } else {
                status = true;
            }
        } else {
            alert("Please enter pan no");
            status = false;
        }
        return status;
    }



</script>
<script type="text/javascript">
    function submitPage() {
        var contactName = document.getElementById("contactName").value;
        var teleNo = document.getElementById("teleNo").value;
        var mobileNo = document.getElementById("mobileNo").value;
        var bankName = document.getElementById("bankName").value;
        var branch = document.getElementById("branch").value;
        var ifscCode = document.getElementById("ifscCode").value;
        var panNo = document.getElementById("panNo").value;
        var stateIdTemp = document.getElementById("stateId").value;
        var settlementType = document.getElementById("settlementType").value;
        var annualTurnOver = document.getElementById("annualTurnOver").value;

        if (settlementType == "") {
            alert("Please enter the Settlement Type");
            document.getElementById("settlementType").focus();
            return false;
        } else if (annualTurnOver == "") {
            alert("Please enter the Annual TurnOver");
            document.getElementById("annualTurnOver").focus();
            return false;
        } else if (contactName == "") {
            alert("Please enter the contact name");
            document.getElementById("contactName").focus();
            return false;
        } else if (teleNo == "") {
            alert("Please enter the Telephone");
            document.getElementById("teleNo").focus();
            return false;
        } else if (mobileNo == "") {
            alert("Please enter the Mobile No");
            document.getElementById("mobileNo").focus();
            return false;
        } else if (bankName == "") {
            alert("Please enter the Bank Name");
            document.getElementById("bankName").focus();
            return false;
        } else if (branch == "") {
            alert("Please enter the Branch");
            document.getElementById("branch").focus();
            return false;
        } else if (ifscCode == "") {
            alert("Please enter the Ifsc Code");
            document.getElementById("ifscCode").focus();
            return false;
        } else if (panNo == "") {
            alert("Please enter the panNo");
            document.getElementById("panNo").focus();
            return false;
        } else if (stateIdTemp == "") {
            alert("Please enter the state");
            document.getElementById("stateId").focus();
            return false;
        } else if (annualTurnOver < 2000000 && document.getElementById("podFile2").value == "" && document.getElementById("test2").value == "View Attachment") {
            alert("please Upload GST Exemption File");
            return false;
        } else {
            document.dept.action = '/throttle/alterVendor.do';
            document.dept.submit();
        }
        return false;
    }
</script>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Vendor</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Vendor</a></li>
            <li class="active">Alter Vendor</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="hideState();">
                <form name="dept" id="alterVendor" method="post" enctype="multipart/form-data"   >
                    <%@ include file="/content/common/message.jsp" %>
                    <table class="table table-info mb30 table-hover" id="bg" >
                        <% int index1 = 0;%>

                        <c:if test="${GetVendorDetailUnique != null}">
                            <c:forEach items="${GetVendorDetailUnique}" var="vnd" >
                                <% if (index1 == 0) {%>
                                <thead><tr height="30">
                                        <th colspan="4" >Alter Vendor</th>
                                    </tr>
                                    <tr>
                                        <td  ><font color="red">*</font>Vendor Name</td>
                                        <td  >
                                            <input type="hidden" name="vendorId" value=<c:out value="${vnd.vendorId}"/>  >
                                            <input name="vendorName" style="width:250px;height:40px" readonly type="text" class="form-control" value="<c:out value="${vnd.vendorName}"/>"  ></td>
                                        <!--<td  >TIN No / Vendor Reg No</td>
                                       <td  ></td>-->
                                <input name="tinNo" style="width:250px;height:40px" type="hidden" class="form-control"  value="<c:out value="${vnd.tinNo}"/>" placeholder="Type vendor TIN No..."  required maxlength="45" >

                                <td>Annual TurnOver
                                </td>
                                <td>
                                    <input name="annualTurnOver"  id="annualTurnOver" style="width:250px;height:40px" type="text"  onkeypress="return onKeyPressBlockCharacters(event)" class="form-control" onchange="setGSTHead(this.value)" value="<c:out value="${vnd.annualTurnOver}"/>" placeholder="Type Annual TurnOver.....">
                                    <input name="gstExemp"  id="gstExemp" style="width:250px;height:40px" type="hidden"  onkeypress="return onKeyPressBlockCharacters(event)" class="form-control"  value="<c:out value="${vnd.gstExemp}"/>">
                                </td>
                                </tr>

                                <tr>
                                    <td  ><font color="red">*</font>Vendor Type </td>
                                    <td   >
                                        <select class="form-control" style="width:250px;height:40px" id="vendorTypeId" name="vendorTypeId"  >
                                            <!--<option value="0">---Select---</option>-->
                                            <c:if test = "${VendorTypeList != null}" >
                                                <c:forEach items="${VendorTypeList}" var="Tp">
                                                    <c:if test = "${Tp.vendorTypeId == 1001}" >
                                                        <option value='<c:out value="${Tp.vendorTypeId}" />'><c:out value="${Tp.vendorTypeValue}" /></option>
                                                    </c:if>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                        <script>
                                            document.getElementById("vendorTypeId").value = '<c:out value="${vnd.vendorTypeId}" />';
                                        </script>
                                    </td>
                                    <td><div id="settlementDiv" ><font color="red">*</font>Settlement Type</div></td>
                                    <td>
                                        <div id="settlementDiv1">
                                            <select class="form-control" style="width:250px;height:40px" name="settlementType"  id="settlementType">
                                                <!--<option value="1">Cash Settlement</option>-->
                                                <option value="2">Credit Settlement</option>
                                            </select>
                                        </div>
                                        <script>
                                            document.getElementById("settlementType").value = '<c:out value="${vnd.settlementType}" />';
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td  >Vendor Address</td>
                                    <td  ><textarea  style="width:250px;height:40px" name="vendorAddress" class="form-control"><c:out value="${vnd.vendorAddress}" /></textarea></td>

                                    <td  >Vendor Phone No/Fax No</td>
                                    <td  ><input style="width:250px;height:40px" name="vendorPhoneNo" class="form-control" type="text" maxlength="45"  onkeypress="return onKeyPressBlockCharacters(event)"  value= <c:out value="${vnd.vendorPhoneNo}"/> ></td>
                                </tr>
                                <tr>
                                    <td  >Vendor Mail Id</td>
                                    <td  ><input style="width:250px;height:40px" name="vendorMailId" class="form-control" type="text"  value=<c:out value="${vnd.vendorMailId}" />></td>
                                    <td><font color="red">*</font>Credit (Days)</td>
                                    <td><input name="creditDays" style="width:250px;height:40px" type="text" class="form-control" class="form-control" value="<c:out value="${vnd.creditDays}" />" onkeypress="return onKeyPressBlockCharacters(event)" maxlength="3" placeholder="Type credit days...."></td>
                                </tr>
                                <tr>
                                    <td ><font color="red">*</font>Status</td>
                                    <td ><select style="width:250px;height:40px" name="activeStatus"   class="form-control" >
                                            <c:choose>
                                                <c:when test="${vnd.activeInd=='N' && vnd.activeInd=='n'}" >
                                                    <option value="Y" >Active</option>
                                                    <option value="N" selected>InActive</option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option selected value="Y" >Active</option>
                                                    <option value="N">InActive</option>

                                                </c:otherwise>
                                            </c:choose>

                                        </select></td>
                                </tr>


                                <tr>
                                    <td>Vendor Payment Type</td>
                                    <td><select id="paymentType" name="paymentType"  class="form-control" style="width:250px;height:40px" onchange="setFCMValue(this.value)">
                                            <option value="1">RCM</option>
                                            <option value="2">FCM</option>
                                        </select></td>
                                    <td id="fcmHeader" style="display: none">FCM (%)</td>
                                    <td id="fcmFooter" style="display: none"><input name="fcmPerc" style="width:250px;height:40px" id="fcmPerc" type="text" class="form-control"  value="<c:out value="${vnd.fcmPerc}" />" onkeypress="return onKeyPressBlockCharacters(event)" maxlength="3" placeholder="Type FCM Percentage"></td>
                                </tr>

                                <script>
                                    document.getElementById("paymentType").value = '<c:out value="${vnd.paymentType}" />';
                                    setFCMValue('<c:out value="${vnd.paymentType}" />');
                                    function setFCMValue(val) {

                                        if (val == 1) {
                                            $("#fcmHeader").hide();
                                            $("#fcmFooter").hide();
                                            $("#fcmPerc").val(0);
                                        } else {
                                            $("#fcmHeader").show();
                                            $("#fcmFooter").show();
                                            $("#fcmPerc").val('<c:out value="${vnd.fcmPerc}" />');
                                        }

                                    }
                                </script>




                                <tr height="30">
                                    <td colspan="4" style="background-color:#5BC0DE;">Contact Details</td>
                                </tr>
                                <tr>
                                    <td  ><font color="red">*</font>Name : </td>
                                    <td  >
                                        <input type="text" id="contactName" style="width:250px;height:40px" name="contactName"   class="form-control"   value= <c:out value="${vnd.contactName}"/>>
                                    </td>
                                    <td  >Designation :</td>
                                    <td  >
                                        <input type="text" id="designation" style="width:250px;height:40px" name="designation"  class="form-control"   value= <c:out value="${vnd.designation}"/>>
                                    </td>
                                </tr>
                                <tr>
                                    <td  >Email Id :</td>
                                    <td  >
                                        <input type="text" id="emailId" style="width:250px;height:40px" name="emailId" class="form-control"  value= <c:out value="${vnd.emailId}"/>>
                                    </td>
                                    <td  >Telephone No :</td>
                                    <td  >
                                        <input type="text" id="teleNo" style="width:250px;height:40px" name="teleNo"  class="form-control"   onkeypress="return onKeyPressBlockCharacters(event)" value= <c:out value="${vnd.teleNo}"/> >
                                    </td>
                                </tr>
                                <tr>
                                    <td  >Mobile No :</td>
                                    <td  >
                                        <input type="text" id="mobileNo" style="width:250px;height:40px" name="mobileNo" class="form-control"  onkeypress="return onKeyPressBlockCharacters(event)" value= <c:out value="${vnd.mobileNo}"/>>
                                    </td>
                                    <td  >Fax No :</td>
                                    <td  >
                                        <input type="text" id="faxNo" style="width:250px;height:40px" name="faxNo"  class="form-control" onkeypress="return onKeyPressBlockCharacters(event)"  value= <c:out value="${vnd.faxNo}"/> >
                                    </td>
                                </tr>
                                <tr height="30">
                                    <td colspan="4" style="background-color:#5BC0DE;">Bank Details</td>
                                </tr>
                                <tr>
                                    <td  ><font color="red">*</font>Bank Name:</td>
                                    <td  >
                                        <input type="text" style="width:250px;height:40px" id="bankName" name="bankName" class="form-control" value= <c:out value="${vnd.bankName}"/>   >
                                    </td>
                                    <td  ><font color="red">*</font>Branch :</td>
                                    <td  >
                                        <input type="text" style="width:250px;height:40px" id="branch" name="branch" class="form-control"  value="<c:out value="${vnd.branch}"/>">
                                    </td>
                                </tr>
                                <tr>
                                    <td  ><font color="red">*</font>Branch Code:</td>
                                    <td  >
                                        <input type="text" style="width:250px;height:40px" id="branchCode" name="branchCode" class="form-control" value= <c:out value="${vnd.branchCode}"/>>
                                    </td>
                                    <td  ><font color="red">*</font>Account No :</td>
                                    <td  >
                                        <input type="text" class="form-control" id="accountNo" style="width:250px;height:40px" name="accountNo" class="form-control" onkeypress="return onKeyPressBlockCharacters(event)"   value= <c:out value="${vnd.accountNo}"/>>
                                    </td>
                                </tr>
                                <tr>
                                    <td  ><font color="red">*</font>IFSC Code :</td> 
                                    <td  >
                                        <input type="text" style="width:250px;height:40px" id="ifscCode" name="ifscCode" class="form-control" value= <c:out value="${vnd.ifscCode}"/>>
                                    </td>
                                    <td  >Micr No :</td>
                                    <td  >
                                        <input type="text" style="width:250px;height:40px" id="micrNo" name="micrNo" class="form-control" value= <c:out value="${vnd.micrNo}"/> >
                                    </td>

                                </tr>
                                <tr height="30">
                                    <td colspan="4" style="background-color:#5BC0DE;">Registration No Details</td>
                                </tr>
                                <tr>
                                    <td  >M.S.M.E :</td>
                                    <td  >
                                        <input type="text" style="width:250px;height:40px" id="msmeId" name="msmeId" class="form-control"  value= <c:out value="${vnd.msmeId}"/>   >
                                    </td>
                                    <!--<td  >GST :</td>-->
                                    <!--<td  >-->
                                <input type="hidden" style="width:250px;height:40px" id="gstId" name="gstId" class="form-control"  value= <c:out value="${vnd.gstId}"/>>
                                <!--</td>-->
                                </tr>
                                <!--                        <tr>
                                                            <td  ><font color="red">*</font>Service Tax No :</td>
                                                            <td  >-->
                                <input type="hidden" style="width:250px;height:40px" id="serViceTax" name="serViceTax"  class="form-control"  value= <c:out value="${vnd.serViceTax}"/>>
                                <!--                            <td  >Excise duty :</td>
                                                            <td  >-->
                                <input type="hidden" style="width:250px;height:40px" id="exciseDuty" name="exciseDuty" class="form-control"  value= <c:out value="${vnd.exciseDuty}"/> >
                                <!--                            </td>
                                                        </tr>-->
                                <tr>
                                    <!--                            <td  >VAT - TIN </td>
                                                                <td  >-->
                                <input type="hidden" style="width:250px;height:40px" id="vatId" name="vatId"  class="form-control"   value= <c:out value="${vnd.vatId}"/> >

                                <td><font color="red">*</font>eFS-Id</td>
                                <td><input type="text" class="form-control" style="width:250px;height:40px" name="eFSId" value="<c:out value="${vnd.eFSId}"/>" id="eFSId" maxlength="15" >
                                </td>

                                <td  ><font color="red">*</font>Pan No *:</td>
                                <td  >
                                    <input type="text" style="width:250px;height:40px" id="panNo" name="panNo" class="form-control" maxlength="10" onBlur="validatePanNo()" value= <c:out value="${vnd.panNo}"/>    >
                                </td>
                                </tr>
                                <tr>
                                    <td><font color="red">*</font>Billing State</td>
                                    <td>
                                        <input name="stateIdTemp" id="stateIdTemp"  type="hidden" class="form-control" >
                                        <select name="stateId" style="width:250px;height:40px" id="stateId" class="form-control" required data-placeholder="Choose One">
                                            <option value="">Choose One</option>
                                            <c:if test = "${stateList != null}" >
                                                <c:forEach items="${stateList}" var="Type">
                                                    <option value='<c:out value="${Type.stateId}" />'><c:out value="${Type.stateName}" /></option>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                    </td>
                                    <td><font color="red">*</font>GST No</td>
                                    <td>
                                        <input name="gstNo" style="width:250px;height:40px" id="gstNo"  type="text" class="form-control" value= <c:out value="${vnd.gstNo}"/> >
                                        <%--<c:out value="${vnd.stateId}"/>--%>
                                    </td>
                                    <!--</tr>-->

                                <script>
                                    //                                document.getElementById("categoryName").value = '<c:out value="${getGstCategoryCode}"/>';
                                    document.getElementById("stateId").value = '<c:out value="${vnd.stateId}"/>';

                                </script>
                                </tr>
                                </thead>
                            </table>
                            <br/>
                            <br/>



                            <table class="table table-info mb30 table-hover" id="POD1">
                                <thead><tr height="30" id="tableDesingTD">
                                        <th  align="center" height="30" ></th>
                                        <th  height="30" >Attachment</th>
                                        <th  height="30" ></th>
                                    </tr> 
                                </thead>
                                <% int index = 1;%>
                                <tr>
                                    <td colspan="4" align="center">
                                        <input type="hidden" name="tripSheetId1" id="tripSheetId1" value="<%=request.getParameter("tripSheetId1")%>" />
                                        <input type="hidden" name="selectedRowCount" id="selectedRowCount" value="1"/>
                                    </td>
                                </tr>
                                <!--                                <tr>
                                                                    <td colspan="4" align="center">
                                                                        <input type="button" class="btn btn-info" name="add" value="add" onclick="addRow();"/>
                                                                    </td>
                                                                </tr>-->
                                <script>
                                    function viewPODFiles(vendorId, sno) {
                                        var sno = parseInt(sno) - 1;
                                        window.open('/throttle/content/vendor/displayBlobData1.jsp?vendorId=' + vendorId + "&sno=" + sno, 'PopupPage', 'height = 500, width = 500, scrollbars = yes, resizable = yes');
                                    }
                                </script>
                                <script>

                                    function setGSTHead(val) {
                                        if (document.getElementById("selectedRowCount").value < 4) {
                                            for (var i = 0; i <= 2; i++) {
                                                addRow();
                                            }
                                        }
                                        $("#header1").text("PAN No");
                                        if (val >= 2000000) {
                                            $('#gstHeader').show();
                                            $('#gstFooter').show();
                                            $("#header2").text("GST No");
                                            $('#gstExemp').val("0");
                                        } else {
                                            $('#gstHeader').hide();
                                            $('#gstFooter').hide();
                                            $('#gstNo').val("0");
                                            $('#gstExemp').val("1");
                                            $("#header2").text("GST Excemption");
                                        }
                                        $("#header3").text("Others");
                                    }

                                    var podRowCount1 = 1;
                                    var podSno1 = '';
                                    function addRow() {

                                        podSno1 = document.getElementById('selectedRowCount').value;
                                        var tab = document.getElementById("POD1");
                                        var newrow = tab.insertRow(podSno1);
                                        var cell = newrow.insertCell(0);
                                        var cell0 = "<td class='text1' height='25' ><label id='header" + podSno1 + "'></label></td>";
                                        cell.innerHTML = cell0;

                                        cell = newrow.insertCell(1);
                                        var cell0 = "<td class='text1' height='25' ><input type='file'   id='podFile" + podSno1 + "' name='podFile' class='textbox'  onchange='checkName(" + podSno1 + ");' value= <c:out value="${vnd.fileName}"/> ><br/><font size='2' color='blue' > Allowed file type:pdf & image</td>";
                                        cell.innerHTML = cell0;

                                        cell = newrow.insertCell(2);
                                        var cell0 = "<td><input type ='button' class='btn btn-info' id ='test" + podSno1 + "' name ='test' style='width:200px;' value='View Attachment' onclick='viewPODFiles(<c:out value="${vnd.vendorId}"/>," + podSno1 + ");' ></td>";
                                        cell.innerHTML = cell0;

//                                        if (podSno1 == 1) {
//                                            cell = newrow.insertCell(3);
//                                            cell0 = "<td class='text1' height='25' ></td>";
//                                            cell.innerHTML = cell0;
//                                        } else {
//                                            cell = newrow.insertCell(3);
//                                            cell0 = "<td class='text1' height='25' align='left'><input type='button' class='btn btn-info'  style='width:90px;height:30px;font-weight: bold;padding: 1px;'  name='delete'  id='delete" + podSno1 + "'   value='Delete Row' onclick='deleteRow(this);' ></td>";
//                                            cell.innerHTML = cell0;
//                                        }


                                        podSno1++;
                                        if (podSno1 > 0) {
                                            document.getElementById('selectedRowCount').value = podSno1;
                                        }

                                    }


                                    function hideState() {
                                        $('#stateId').css('pointer-events', 'none');
                                    }


                                    function deleteRow(src) {
                                        podSno1--;
                                        var oRow = src.parentNode.parentNode;
                                        var dRow = document.getElementById('POD1');
                                        dRow.deleteRow(oRow.rowIndex);
                                        document.getElementById("selectedRowCount").value--;
                                    }

                                    $(document).ready(function () {

                                        var podSno = 1;

                                    <c:if test="${GetVendorDetailUnique != null}">
                                        <c:forEach items="${GetVendorDetailUnique}" var="vnd" >
                                            <c:if test="${vnd.fileName != null}">
                                        addRow();
                                        var filename = "<c:out value ="${vnd.fileName}"/>";
                                        var buttonImage = document.getElementById("test" + podSno + "");
                                        buttonImage.value = filename;
                                        $("#header1").text("PAN No");
                                        podSno++;
                                            </c:if>
                                            <c:if test="${vnd.fileName == null}">
                                        addRow();
                                        $("#header1").text("PAN No");
                                        podSno++;
                                            </c:if>
                                            <c:if test="${vnd.fileName1 != null}">
                                        var filename1 = "<c:out value ="${vnd.fileName1}"/>";
                                        addRow();
                                        var buttonImage = document.getElementById("test" + podSno + "");
                                        buttonImage.value = filename1;
                                                <c:if test="${vnd.gstExemp == 0}">
                                        $("#header2").text("GST No");
                                                </c:if>
                                                <c:if test="${vnd.gstExemp != 0}">
                                        $("#header2").text("GST Exemption");
                                                </c:if>
                                        podSno++;
                                            </c:if>
                                            <c:if test="${vnd.fileName1 == null}">
                                        addRow();
                                                <c:if test="${vnd.gstExemp == 0}">
                                        $("#header2").text("GST No");
                                                </c:if>
                                                <c:if test="${vnd.gstExemp != 0}">
                                        $("#header2").text("GST Exemption");
                                                </c:if>
                                        podSno++;
                                            </c:if>
                                            <c:if test="${vnd.fileName2 != null}">
                                        var filename2 = "<c:out value ="${vnd.fileName2}"/>";
                                        addRow();
                                        var buttonImage = document.getElementById("test" + podSno + "");
                                        buttonImage.value = filename2;
                                        $("#header3").text("Others");
                                        podSno++;
                                            </c:if>
                                            <c:if test="${vnd.fileName2 == null}">
                                        addRow();
                                        $("#header3").text("Others");
                                        podSno++;
                                            </c:if>
                                            <c:if test="${vnd.fileName3 != null}">
                                        var filename3 = "<c:out value ="${vnd.fileName3}"/>";
                                        addRow();
                                        var buttonImage = document.getElementById("test" + podSno + "");
                                        buttonImage.value = filename3;
                                        podSno++;
                                            </c:if>
                                            <c:if test="${vnd.fileName4 != null}">
                                        var filename4 = "<c:out value ="${vnd.fileName4}"/>";
                                        addRow();
                                        var buttonImage = document.getElementById("test" + podSno + "");
                                        buttonImage.value = filename4;
                                        podSno++;
                                            </c:if>
                                        </c:forEach>
                                    </c:if>

                                    });


                                </script>


                                <%index++;%>
                            </table>
                            <% index1++;
                                    }%>
                        </c:forEach>
                    </c:if>

                    <br>
                    <br>
                    <br>

                    <center>
                        <input class="btn btn-info" value="Alter"  onClick="submitPage();">
                    </center>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
<script>
    jQuery(document).ready(function () {
        "use strict";
        // Select2
        jQuery(".select2").select2({
            width: '100%',
            minimumResultsForSearch: -1
        });
    });
</script>
