<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    function validatePanNo() {
        var nPANNo = document.getElementById("panNo").value;
        if (nPANNo != "") {
            document.getElementById("panNo").value = nPANNo.toUpperCase();
            var ObjVal = nPANNo;
            var pancardPattern = /^([ABCFGHLJPT]{1})([A-Z]{2})([PCFH]{1})([A-Z]{1})(\d{4})([A-Z]{1})$/;
            var patternArray = ObjVal.match(pancardPattern);
            if (patternArray == null) {
                alert("PAN Card No Invalid ");
                return false;
            } else {
                return true;
            }
        } else {
            alert("Please enter pan no");
            return false;
        }
    }
</script>
<script>
    $(document).ready(function () {
        $('#vendorName').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getVendorNameDetails.do",
                    dataType: "json",
                    data: {
                        vendorName: request.term
                    },
                    success: function (data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function (data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };



    });


    function checkVendorNameExists() {
        var vendorName = $("#vendorName").val();
        var vendorTypeId = $("#vendorTypeId").val();
        if (vendorName != '' && vendorTypeId != '') {
            $.ajax({
                url: "/throttle/checkVendorNameExists.do",
                dataType: "json",
                data: {
                    vendorName: vendorName,
                    vendorTypeId: vendorTypeId
                },
                success: function (data, textStatus, jqXHR) {
                    var count = data.VendorCount;
                    if (count == 0) {
                        $("#vendorStatus").text("");
                        $("#saveButton").show();
                    } else {
                        $("#vendorStatus").text("Vendor Already Exists");
                        $("#saveButton").hide();
                    }
                },
                error: function (data, type) {
                    console.log(type);
                }
            });
        }

    }


    function setSettlementDiv() {
        var vendorTypeId = document.dept.vendorTypeIdTemp.value;
//        alert(vendorTypeId);
        $("#vendorTypeId").val(vendorTypeId);
        if (vendorTypeId == 1001) {
            document.getElementById("settlementDiv").style.display = "block";
            document.getElementById("settlementDiv1").style.display = "block";
        } else {
            document.getElementById("settlementDiv").style.display = "none";
            document.getElementById("settlementDiv1").style.display = "none";
        }
        checkVendorNameExists();
    }


    function onKeyPressBlockCharacters(e) {
        var sts = false;
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /[a-zA-Z-`~!@#$%^&*() _|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
        sts = !reg.test(keychar);
        return sts;
    }


</script>
<script language="javascript">
    function submitPage() {
//        var errStr = "";
//        var check = validatePanNo();
//        var eFSId = document.getElementById("eFSId").value;
//        var gstNoSize = $("#gstNo").val().length;
//        if(gstNoSize < 15 ){
//        errStr = "Please enter Valid GST No.\n";
//        alert(errStr);
//        document.getElementById("gstNo").focus();
//        }
//        
//        if(eFSId==""){
//            alert("Please Enter the eFS Id");
//            document.getElementById("eFSId").focus();
//            return false;
//        }
//        else if (check == true) {
//        }

        var vendorName = document.getElementById("vendorName").value;
        var contactName = document.getElementById("contactName").value;
        var teleNo = document.getElementById("teleNo").value;
        var mobileNo = document.getElementById("mobileNo").value;
        var bankName = document.getElementById("bankName").value;
        var branch = document.getElementById("branch").value;
        var ifscCode = document.getElementById("ifscCode").value;
        var panNo = document.getElementById("panNo").value;
        var stateIdTemp = document.getElementById("stateName").value;
        var annualTurnOver = document.getElementById("annualTurnOver").value;

        if (vendorName == "") {
            alert("Please enter the vendor name");
            document.getElementById("vendorName").focus();
            return;
        } else if (annualTurnOver == "") {
            alert("Please enter the Annual TurnOver");
            document.getElementById("annualTurnOver").focus();
            return;
        } else if (contactName == "") {
            alert("Please enter the contact name");
            document.getElementById("contactName").focus();
            return;
        } else if (teleNo == "") {
            alert("Please enter the Telephone");
            document.getElementById("teleNo").focus();
            return;
        } else if (mobileNo == "") {
            alert("Please enter the Mobile No");
            document.getElementById("mobileNo").focus();
            return;
        } else if (bankName == "") {
            alert("Please enter the Bank Name");
            document.getElementById("bankName").focus();
            return;
        } else if (branch == "") {
            alert("Please enter the Branch");
            document.getElementById("branch").focus();
            return;
        } else if (ifscCode == "") {
            alert("Please enter the Ifsc Code");
            document.getElementById("ifscCode").focus();
            return;
        } else if (panNo == "") {
            alert("Please enter the panNo");
            document.getElementById("panNo").focus();
            return;
        } else if (stateIdTemp == "") {
            alert("Please enter the state");
            document.getElementById("stateIdTemp").focus();
            return;
        } else if (annualTurnOver < 2000000 && document.getElementById("podFile2").value == "") {
            alert("please Upload GST Exemption File");
            return false;
        } else {
            document.dept.action = '/throttle/addVendor.do';
            document.dept.submit();
        }

    }

    function setAddValue() {


        var State = document.getElementById("stateName").value;
        var temp = State.split('-');
        document.getElementById("stateTinNo").value = temp[0];
        document.getElementById("stateId").value = temp[1];
        document.getElementById("gstNo").value = document.getElementById("stateTinNo").value + "" + document.getElementById("panNo").value
        $('#gstValidation').show();

    }
</script>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Vendor</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Vendor</a></li>
            <li class="active">Add Vendor</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <%@ include file="/content/common/message.jsp" %>
                <form name="dept" id="addVendor"  method="post" enctype="multipart/form-data" >
                    <center><span id="vendorStatus" style="color: red"></span></center>
                    <table class="table table-info mb30 table-hover" id="bg" >
                        <thead><tr>
                                <th colspan="4" >Add Vendor</th>
                            </tr></thead>
                        <tr>
                            <td><font color="red">*</font>Vendor Name </td>

                            <td>
                                <input type="hidden" name="vendorId" value='<c:out value="${list.vendorId}"/>'>
                                <input name="vendorName" style="width:250px;height:40px;" id="vendorName" type="text" class="form-control" value="" placeholder="Type vendor name..."  required maxlength="100" onchange="checkVendorNameExists()">
                            </td>

                            <td style="display:none"><font color="red">*</font>TIN No / Vendor Reg No</td>

                            <td style="display:none">
                                <input name="tinNo" style="width:250px;height:40px" type="text" class="form-control" value="0" placeholder="Type vendor TIN No..."  required maxlength="45"></td>


                            <td>Annual TurnOver
                            </td>
                            <td>
                                <input name="annualTurnOver"  id="annualTurnOver" style="width:250px;height:40px" type="text"  onkeypress="return onKeyPressBlockCharacters(event)" class="form-control" onchange="setGSTHead(this.value)" value="" placeholder="Type Annual TurnOver.....">
                                <input name="gstExemp"  id="gstExemp" style="width:250px;height:40px" type="hidden"  onkeypress="return onKeyPressBlockCharacters(event)" class="form-control"  value="">
                            </td>
                        </tr>

                        <tr>
                            <td><font color="red">*</font>Vendor Type</td>
                            <td>
                                <input name="vendorTypeId" id="vendorTypeId"  type="hidden" value="1001" class="form-control" >
                                <select name="vendorTypeIdTemp" style="width:250px;height:40px" id="vendorTypeIdTemp" onChange="setSettlementDiv();"  class="form-control" required data-placeholder="Choose One">

                                    <c:if test = "${VendorTypeList != null}" >
                                        <c:forEach items="${VendorTypeList}" var="Type">
                                            <c:if test = "${Type.vendorTypeId == 1001}" >
                                                <option value='<c:out value="${Type.vendorTypeId}" />'><c:out value="${Type.vendorTypeValue}" /></option>
                                            </c:if>
                                        </c:forEach >
                                    </c:if>
                                </select>
                            </td>


                            <td><div id="settlementDiv" ><font color="red">*</font>Settlement Type</div></td>
                            <td>
                                <div id="settlementDiv1">
                                    <select class="form-control" style="width:250px;height:40px" name="settlementType"  >
                                        <!--<option value="1">Cash Settlement</option>-->
                                        <option value="2">Credit Settlement</option>
                                    </select>
                                </div>
                            </td>

                        <input type="hidden" name="priceType" value="M"/>
                        </tr>
                        <tr>
                            <td>Vendor Address</td>
                            <td><textarea name="vendorAddress" style="width:250px;height:40px" rows="2" class="form-control" placeholder="Type vendor address....." maxlength="200"></textarea></td>
                            <td>Vendor Phone No/Fax No</td>
                            <td><input name="vendorPhoneNo" style="width:250px;height:40px" maxlength="11" type="text"  onkeypress="return onKeyPressBlockCharacters(event)" class="form-control" value="" placeholder="Type vendor phone no / fax no....."></td>
                        </tr>
                        <tr>
                            <td>Vendor Mail Id</td>
                            <td><input name="vendorMailId" style="width:250px;height:40px" type="text" class="form-control" value="" placeholder="Type vendor mailid......" maxlength="50"></td>
                            <td>Credit (Days)</td>
                            <td><input name="creditDays" style="width:250px;height:40px" type="text" class="form-control" class="form-control" value="" onkeypress="return onKeyPressBlockCharacters(event)" maxlength="3" placeholder="Type credit days...."></td>
                        </tr>

                        <tr>
                            <td>Vendor Payment Type</td>
                            <td><select id="paymentType" name="paymentType"  class="form-control" style="width:250px;height:40px" onchange="setFCMValue(this.value)">
                                    <option value="1">RCM</option>
                                    <option value="2">FCM</option>
                                </select></td>
                            <td id="fcmHeader" style="display:none">FCM (%)</td>
                            <td id="fcmFooter" style="display:none"><input name="fcmPerc" style="width:250px;height:40px" id="fcmPerc" type="text" class="form-control"  value="0" onkeypress="return onKeyPressBlockCharacters(event)" maxlength="3" placeholder="Type FCM Percentage"></td>
                        </tr>

                        <script>
                            function setFCMValue(val) {
                                if (val == 1) {
                                    $("#fcmHeader").hide();
                                    $("#fcmFooter").hide();
                                    $("#fcmPerc").val(0);
                                } else {
                                    $("#fcmHeader").show();
                                    $("#fcmFooter").show();
                                }

                            }
                        </script>

                        <tr>
                            <td colspan="4" style="background-color:#5BC0DE;"><font color="white">Contact Details</font></td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>Name</td>
                            <td>
                                <input type="text" style="width:250px;height:40px" id="contactName" name="contactName" value=""  class="form-control" placeholder="Type contact person name..." required maxlength="45">
                            </td>
                            <td>Designation</td>
                            <td>
                                <input type="text" style="width:250px;height:40px" id="designation" name="designation" value=""  class="form-control" maxlength="45" >
                            </td>
                        </tr>
                        <tr>
                            <td>Email Id</td>
                            <td>
                                <input type="text" style="width:250px;height:40px" id="emailId" name="emailId" value="" class="form-control" maxlength="45">
                            </td>
                            <td><font color="red">*</font>Telephone No</td>
                            <td>
                                <input type="text" style="width:250px;height:40px" id="teleNo" name="teleNo" value="" class="form-control" maxlength="10" onkeypress="return onKeyPressBlockCharacters(event)" placeholder="Type Tel No....." required >
                            </td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>Mobile NO</td>
                            <td>
                                <input type="text" style="width:250px;height:40px" id="mobileNo" name="mobileNo" value="" maxlength="10" onkeypress="return onKeyPressBlockCharacters(event)" class="form-control"  placeholder="Type MobileNo....." required>
                            </td>
                            <td>Fax No </td>
                            <td>
                                <input type="text" style="width:250px;height:40px" id="faxNo" name="faxNo" value="" onkeypress="return onKeyPressBlockCharacters(event)"  class="form-control" maxlength="10">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="background-color:#5BC0DE;"><font color="white">Bank Details</font></td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>Bank Name</td>
                            <td>
                                <input type="text" style="width:250px;height:40px" id="bankName" name="bankName" value=""  class="form-control"  placeholder="Type BankName....." required maxlength="45">
                            </td>
                            <td><font color="red">*</font>Branch</td>
                            <td>
                                <input type="text" style="width:250px;height:40px" id="branch" name="branch" value=""  class="form-control"   placeholder="Type BranchName....." required maxlength="45">
                            </td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>Branch Code</td>
                            <td>
                                <input type="text" style="width:250px;height:40px" id="branchCode" name="branchCode" value=""  class="form-control"   placeholder="Type BankCode....." required maxlength="45">
                            </td>
                            <td><font color="red">*</font>Account No</td>
                            <td>
                                <input type="text" style="width:250px;height:40px" id="accountNo" name="accountNo" value=""  class="form-control" onkeypress="return onKeyPressBlockCharacters(event)"  placeholder="Type AccountNo....." required maxlength="45">
                            </td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>IFSC Code</td>
                            <td>
                                <input type="text" style="width:250px;height:40px" id="ifscCode" name="ifscCode" value=""   class="form-control" maxlength="45">
                            </td>
                            <td>Micr No</td>
                            <td>
                                <input type="text" style="width:250px;height:40px" id="micrNo" name="micrNo" value=""  class="form-control"  onkeypress="return onKeyPressBlockCharacters(event)" maxlength="45">
                            </td>

                        </tr>

                        <tr>                            
                            <td colspan="4" style="background-color:#5BC0DE;"><font color="white">Registration No Details</font></td>
                        </tr>
                        <tr>
                            <td>M.S.M.E</td>
                            <td>
                                <input type="text" style="width:250px;height:40px" id="msmeId" name="msmeId" value=""    onkeypress="return onKeyPressBlockCharacters(event)"  class="form-control" maxlength="45">
                            </td>
                            <!--<td>GST</td>-->
                            <!--<td>-->
                        <input type="hidden" style="width:250px;height:40px" id="gstId" name="gstId" value="0"   onkeypress="return onKeyPressBlockCharacters(event)"  class="form-control" maxlength="45">
                        <!--</td>-->
                        </tr>
                        <!--                        <tr>
                                                    <td><font color="red">*</font>Service Tax No</td>
                                                    <td>-->
                        <input type="hidden" style="width:250px;height:40px" id="serViceTax" name="serViceTax" value="0"  class="form-control" onkeypress="return onKeyPressBlockCharacters(event)"    placeholder="Type ServiceTax No....." required maxlength="45">
                        <!--                            </td>
                                                    <td>Excise duty</td>
                                                    <td>-->
                        <input type="hidden" style="width:250px;height:40px" id="exciseDuty" name="exciseDuty" value="0"  class="form-control"  onkeypress="return onKeyPressBlockCharacters(event)"  maxlength="45">
                        <!--</td>-->
                        <!--</tr>-->
                        <tr>
                            <!--                            <td>VAT - TIN</td>
                                                        <td>-->
                        <input type="hidden" style="width:250px;height:40px" id="vatId" name="vatId" value="0"  class="form-control"  onkeypress="return onKeyPressBlockCharacters(event)"  maxlength="45">
                        <!--</td>-->
                        <td><font color="red">*</font>eFS-Id</td>
                        <td><input type="text" class="form-control" style="width:250px;height:40px" name="eFSId" value="" id="eFSId" maxlength="15" >
                        </td>

                        <td><font color="red">*</font>Pan No</td>
                        <td>
                            <input type="text" style="width:250px;height:40px" id="panNo" name="panNo" value="" maxlength="10"  class="form-control" placeholder="Type Pan No....." required maxlength="45">
                            <!--onBlur="validatePanNo()"-->
                        </td>

                        </tr>
                        <tr>
                            <td><font color="red">*</font>Billing State</td>
                            <td>
                                <input name="stateIdTemp" id="stateIdTemp"  type="hidden" class="form-control" >
                                <input type="hidden"  class="form-control" style="width:240px;height:40px"   name="stateTinNo" id="stateTinNo" value=""/>
                                <input type="hidden"  class="form-control" style="width:240px;height:40px"   name="stateId" id="stateId" value=""/>
                                <select style="width:250px;height:40px" name="stateName" id="stateName" class="form-control"  onchange="setAddValue()" required data-placeholder="Choose One">
                                    <option value="">Choose One</option>
                                    <c:if test = "${stateList != null}" >
                                        <c:forEach items="${stateList}" var="Type">
                                            <option value='<c:out value="${Type.stateIdAlt}" />-<c:out value="${Type.stateId}" />'><c:out value="${Type.stateName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select>
                            </td>
                            <td id="gstHeader"><font color="red">*</font>GST No</td>
                            <td id="gstFooter">
                                <input style="width:250px;height:40px;text-transform:uppercase" maxlength="15" name="gstNo" id="gstNo"  type="text" class="form-control" value="">
                                <!--                                                            &nbsp;&nbsp;&nbsp;<div id="gstValidation" style="display:none"><font color="red">(Please Enter Last 3 Digits)</font></div>-->

                            </td>
                        </tr>
                    </table>

                    <table class="table table-info mb30 table-hover" id="POD1">
                        <tr  id="tableDesingTD">
                            <td align="center"  ></td>
                            <td>Attachment</td>
                        </tr>
                        <% int index = 1;%>
                        <tr>
                            <td colspan="4" align="center">
                                <input type="hidden" name="selectedRowCount" id="selectedRowCount" value="1"/>
                                <input type="hidden" name="tripSheetId1" id="tripSheetId1" value="<%=request.getParameter("tripSheetId1")%>" />
                                <!--<input type="button" name="add"  id="buttonDesign" value="Add File" onclick="addRow();" class="btn btn-info"/>-->
                            </td>
                        </tr>
                        <script>

                            function setGSTHead(val) {
                                if (document.getElementById("selectedRowCount").value < 4) {
                                    for (var i = 0; i <= 2; i++) {
                                        addRow();
                                    }
                                }
                                $("#header1").text("PAN No")
                                if (val >= 2000000) {
                                    $('#gstHeader').show();
                                    $('#gstFooter').show();
                                    $("#header2").text("GST No");
                                    $('#gstExemp').val("0");
                                } else {
                                    $('#gstHeader').hide();
                                    $('#gstFooter').hide();
                                    $('#gstNo').val("0");
                                    $('#gstExemp').val("1");
                                    $("#header2").text("GST Excemption");
                                }
                                $("#header3").text("Others");
                            }

                            var podRowCount1 = 1;
                            var podSno1 = '';
                            function addRow() {
                                podSno1 = document.getElementById('selectedRowCount').value;
                                if (podSno1 < 4) {
                                    var tab = document.getElementById("POD1");
                                    var newrow = tab.insertRow(podSno1);

                                    newrow.id = 'rowId' + podSno1;

                                    var cell = newrow.insertCell(0);
                                    var cell0 = "<td class='text1' height='25' ><label id='header" + podSno1 + "'></label></td>";
                                    cell.innerHTML = cell0;


                                    cell = newrow.insertCell(1);
                                    var cell0 = "<td class='text1' height='25' ><input type='file'   id='podFile" + podSno1 + "' name='podFile' class='form-control' value='' onchange='checkName(" + podSno1 + ");'><br/><font size='2' color='blue'> Allowed file type:pdf & image</td>";
                                    cell.innerHTML = cell0;

//
//                                    if (podSno1 <= 2) {
//                                        cell = newrow.insertCell(2);
//                                        cell0 = "<td class='text1' height='25' ></td>";
//                                        cell.innerHTML = cell0;
//                                    } else {
//                                        cell = newrow.insertCell(2);
//                                        cell0 = "<td class='text1' height='25' align='left'><input type='button' class='btn btn-info'  style='width:90px;height:30px;font-weight: bold;padding: 1px;'  name='delete'  id='delete" + podSno1 + "'   value='Delete Row' onclick='deleteRow(this);' ></td>";
//                                        cell.innerHTML = cell0;
//                                    }
                                    podSno1++;
                                    if (podSno1 > 0) {
                                        document.getElementById('selectedRowCount').value = podSno1;
                                    }

                                } else {
                                    alert("Only 5 Files Can Be added");
                                }
                            }
                            function deleteRow(src) {
                                alert(src)
                                podSno1--;
                                var oRow = src.parentNode.parentNode;
                                var dRow = document.getElementById('POD1');
                                dRow.deleteRow(oRow.rowIndex);
                                document.getElementById("selectedRowCount").value--;
                            }

                        </script>
                        <%index++;%>
                    </table>







                    <script type="text/javascript">

                        var ar_ext = ['pdf', 'txt', 'gif', 'jpeg', 'jpg', 'png', 'Gif', 'GIF', 'Png', 'PNG', 'JPG', 'Jpg'];        // array with allowed extensions

                        function checkName(sno) {
                            var name = document.getElementById("podFile" + sno).value;
                            var ar_name = name.split('.');

                            var ar_nm = ar_name[0].split('\\');
                            for (var i = 0; i < ar_nm.length; i++)
                                var nm = ar_nm[i];

                            var re = 0;
                            for (var i = 0; i < ar_ext.length; i++) {
                                if (ar_ext[i] == ar_name[ar_name.length - 1]) {
                                    re = 1;
                                    break;
                                }
                            }

                            if (re == 1) {
                            } else {
                                alert('".' + ar_name[ar_name.length - 1] + '" is not an file type allowed for upload');
                                document.getElementById("podFile" + sno).value = '';
                            }
                        }
                    </script>
                    </table>
                    <br/>
                    <center>
                        <input type="button" class="btn btn-info" value="Submit" id="saveButton" onclick="submitPage();" >
                        &emsp;<input type="reset" class="btn btn-info" value="Clear" >
                    </center>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
<script>
    jQuery(document).ready(function () {
        "use strict";
        // Select2
        jQuery(".select2").select2({
            width: '100%',
            minimumResultsForSearch: -1
        });
    });
</script>

