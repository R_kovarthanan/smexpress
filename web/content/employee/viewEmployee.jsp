

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<title>PAPL</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>


<script type="text/javascript">
$(document).ready(function() {
//alert('hai');
$("#datepicker").datepicker({
showOn: "button",
buttonImage: "calendar.gif",
buttonImageOnly: true

});



});

$(function() {
//	alert("cv");
$(".datepicker").datepicker({
/*altField: "#alternate",
 altFormat: "DD, d MM, yy"*/
changeMonth: true, changeYear: true
});

});
/*$( ".datepicker" ).datepicker({changeMonth: true,changeYear: true, minDate: 0, maxDate: "+10Y +10D" });
 
}*/
</script>
<script language="javascript">



    function submitPage(value) {
        if (value == "search") {
            if (isEmpty(document.lecture.staffCode.value) && isEmpty(document.lecture.staffName.value)) {
                alert("please enter the staff code or staff name and search");
                document.lecture.staffCode.focus();
            } else if (document.lecture.staffCode.value != "" && document.lecture.staffName.value != "") {
                alert("please enter the any one and serach");
                document.lecture.staffCode.value = "";
                document.lecture.staffName.value = "";
            } else {
                document.lecture.action = '/throttle/searchViewEmp.do';
                document.lecture.submit();
                return;
            }

        } else if (value == 'add') {
            document.lecture.action = '/throttle/addEmployeePage.do';
            document.lecture.submit();
        }
        else if (value == 'ExcelExport') {
            document.lecture.action = "/throttle/handleEmpViewPage.do?param=" + value;
            document.lecture.submit();

        }
        else if (value == 'modify') {

            document.lecture.action = '/throttle/searchAlterEmp.do';
            document.lecture.submit();

        }

    }

    function importExcel() {
        document.lecture.action = '/throttle/employeeImport.do';
        document.lecture.submit();
    }
//    function importExcel() {
//        document.lecture.action = '/throttle/content/employee/employeeImport.jsp';
//        document.lecture.submit();
//    }

    function copyAddress(value)
    {
        if (value == true) {
            var addAddr;
            var addCity;
            var addState;
            var pinCode;

            addAddr = document.lecture.addr.value;
            addCity = document.lecture.city.value;
            addState = document.lecture.state.value;
            pinCode = document.lecture.pincode.value;

            document.lecture.addr1.value = addAddr;
            document.lecture.city1.value = addCity;
            document.lecture.state1.value = addState;
            document.lecture.pincode1.value = pinCode;
        }
        else
        {
            document.lecture.addr1.value = "";
            document.lecture.city1.value = "";
            document.lecture.state1.value = "";
            document.lecture.pincode1.value = "";

        }
    }



    function isChar(s) {
        if (!(/^-?\d+$/.test(s))) {
            return false;
        }
        return true;
    }

    function isEmail(s)
    {

        if (/[^@]+@[^@]+\.(com)|(co.in)$/.test(s))
            return false;
        alert("Email not in valid form!");
        return true;
    }

    function initCs() {
        var desig = document.getElementById('desigId');
        //alert(desig.value);
        desig.onchange = function() {
            if (this.value != "") {
                var list = document.getElementById("gradeId");
                while (list.childNodes[0]) {
                    list.removeChild(list.childNodes[0])
                }
                fillSelect(this.value);
            }
        }

        fillSelect(document.getElementById('desigId').value);
    }

    function go() {

        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var response = httpRequest.responseText;
                var list = document.getElementById("gradeId");
                var grade = response.split(',');
                for (i = 1; i < grade.length; i++) {
                    var gradeDetails = grade[i].split('-');
                    var gradeId = gradeDetails[0];
                    var gradeName = gradeDetails[1];
                    var x = document.createElement('option');
                    var name = document.createTextNode(gradeName);
                    x.appendChild(name);
                    x.setAttribute('value', gradeId)
                    list.appendChild(x);
                }
            }
        }
    }


    function fillSelect(desigId) {
        //alert(desigId +" design Id ");
        var url = '/throttle/getGradeForDesig.do?desigId=' + desigId;

        if (window.ActiveXObject)
        {
            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else if (window.XMLHttpRequest)
        {
            httpRequest = new XMLHttpRequest();
        }

        httpRequest.open("POST", url, true);
        httpRequest.onreadystatechange = function() {
            go();
        };
        httpRequest.send(null);
    }



    function showTab() {
        if (document.lecture.userStatus.checked == true) {
            document.getElementById("userStat").style.visibility = "visible";
            document.getElementById("userStat1").style.visibility = "visible";
        } else {
            document.getElementById("userStat").style.visibility = "hidden";
            document.getElementById("userStat1").style.visibility = "hidden";
        }
    }




    var httpRequest;
    function getProfile(userName)
    {

        var url = '/throttle/checkUserName.do?userName=' + userName;

        if (window.ActiveXObject)
        {
            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else if (window.XMLHttpRequest)
        {
            httpRequest = new XMLHttpRequest();
        }

        httpRequest.open("GET", url, true);

        httpRequest.onreadystatechange = function() {
            processRequest();
        };

        httpRequest.send(null);
    }


    function processRequest()
    {
        if (httpRequest.readyState == 4)
        {
            if (httpRequest.status == 200)
            {
                if (trim(httpRequest.responseText) != "") {

                    document.getElementById("userNameStatus").innerHTML = httpRequest.responseText;
                    document.lecture.password.value = '';
                    document.lecture.userCheck.value = 'exists';
                    document.lecture.password.disabled = true;
                    //document.lecture.password.disabled=true;
                    document.lecture.userName.select();
                } else {
                    document.getElementById("userNameStatus").innerHTML = "";
                    document.lecture.userCheck.value = 'notExists';
                    document.lecture.password.disabled = false;
                    document.lecture.password.focus();
                }
            }
            else
            {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }

    function nameSearch() {
        var oTextbox = new AutoSuggestControl(document.getElementById("staffName"),
                new ListSuggestions("staffName", "/throttle/EmpNameSuggests.do?"));
    }

    function addPage()
    {
        document.lecture.action = '/throttle/addEmployeePage.do';
        document.lecture.submit();
    }


    function viewPage()
    {
        document.lecture.action = '/throttle/EmpViewSearchPage.do';
        document.lecture.submit();
    }


    function codeSearch(mode) {
        if (mode == 0) {
            var oTextbox = new AutoSuggestControl(document.getElementById("staffCode"), new ListSuggestions("staffCode", "/throttle/handleGetEmpCode.do?"));
        } else {
            var oTextbox = new AutoSuggestControl(document.getElementById("staffName"), new ListSuggestions("staffName", "/throttle/EmpNameSuggests.do?"));
        }

    }

    function setValues() {
        var staffCode = '<%=request.getAttribute("staffCode")%>';
        var staffName = '<%=request.getAttribute("staffName")%>';
//                alert(staffCode);
//                alert(staffName);
        if (staffCode != "null" && staffCode != "") {
            document.lecture.staffCode.value = staffCode;
        }
        if (staffCode == "null") {
            document.lecture.staffCode.value = "";
        }
        if (staffName != "null" && staffName != "") {
            document.lecture.staffName.value = staffName;
        }
        if (staffName == "null") {
            document.lecture.staffName.value = "";
        }
    }

</script>



<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
                  setValues();
                  getVehicleNos();">
    </c:if>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ManageEmployee" text="default text"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="default text"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="default text"/></a></li>
                <li class="active"><spring:message code="hrms.label.ManageEmployee" text="default text"/></li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">



                <body onload="setValues();
            document.lecture.staffId.focus();">
                    <form name="lecture" method="post">
                          <%@ include file="/content/common/message.jsp" %>
                        <table class="table table-info mb30 table-hover" >
                            <thead>
                                <tr>
                                    <th colspan="5">Manage Employee</th>
                                </tr>
                                
                            </thead>
                            <tr>
                                <td  height="30"><spring:message code="hrms.label.EmployeeCode" text="default text"/></td>
                                <td  height="30"><input type="text" class="form-control" style="width:220px;height:40px;" id="staffCode" name="staffCode" autocomplete="off" onFocus="codeSearch(0)" /></td>
                                <td  height="30"><spring:message code="hrms.label.EmployeeName" text="default text"/></td>
                                <td  height="30"><input type="text" class="form-control" style="width:220px;height:40px;" id="staffName"  name="staffName" autocomplete="off" onFocus="nameSearch(1)"/></td>
                                <td  height="30"></td>

                            </tr>

                            <tr height="35">
                                <td colspan="5" align="center" > &nbsp;
                                    <input type="button" class="btn btn-success" value="<spring:message code="hrms.label.ADD" text="default text"/>" name="add" onClick="submitPage(this.name)"/>
                                    <input type="button" class="btn btn-success" value="<spring:message code="hrms.label.SEARCH" text="default text"/>" name="search" onClick="submitPage(this.name)"/>
<!--                                    &nbsp;&nbsp;   <input type="button"   value="<spring:message code="hrms.label.GENERATEEXCEL" text="default text"/>" class="btn btn-success" name="ExcelExport" onClick="submitPage(this.name)" style="width:150px">
                                    &nbsp;&nbsp;<input type="button"   value="<spring:message code="hrms.label.UPLOADEMPLOYEEDETAILS" text="default text"/>" class="btn btn-success" name="search" onClick="importExcel()" style="width:200px">-->
                                </td> </tr>
                        </table>

                        <br/>
                        <c:if test = "${employeeDetails != null}" >
                            <% int index1 = 1; %>
                            <table class="table table-info mb30 table-hover" id="table">
                                <thead>
                                    <tr height="45">
                                        <th><spring:message code="hrms.label.SNo" text="default text"/></th>
                                        <th><spring:message code="hrms.label.EmployeeCode" text="default text"/></th>
                                        <th><spring:message code="hrms.label.EmployeeName" text="default text"/></th>
                                            <%--                      <th>Date Of Birth</th>
                                                                  <th>Father Name</th>--%>
                                        <th><spring:message code="hrms.label.MobileNumber" text="default text"/></th>
                                        <th><spring:message code="hrms.label.VehicleNo" text="default text"/></th>
                                            <%--                      <th>Email Id</th>--%>
                                        <th><spring:message code="hrms.label.DepartmentName" text="default text"/></th>
                                        <th><spring:message code="hrms.label.DesignationName" text="default text"/></th>
                                            <%--                      <th>Grade</th>
                                                                  <th>Qualification</th>
                                                                  <th>Date Of Joining</th>
                                                                  <th>License No</th>
                                                                  <th>Refer By</th>
                                                                  <th>Basic Salary</th>--%>
                                        <th><spring:message code="hrms.label.Company" text="Company"/></th>
                                        <th><spring:message code="hrms.label.MovementType" text="MovementType"/></th>
                                        <th><spring:message code="hrms.label.Status" text="default text"/></th>
                                        <th><spring:message code="hrms.label.Action" text="default text"/></th>
                                    </tr>
                                </thead>

                                <c:forEach items="${employeeDetails}" var="employee">
                                    <%
                                           String classText = "";
                                           int oddEven1 = index1 % 2;
                                           if (oddEven1 > 0) {
                                               classText = "text1";
                                           } else {
                                               classText = "text2";
                                           }
                                    %>
                                    <c:if test="${employee.status == 'B'}">
                                        <tr>
                                            <td  ><font color="red"><%=index1++%></font></td>
                                            <td  ><font color="red"><c:out value="${employee.empCode}" />&nbsp;</font></td>
                                            <td  ><font color="red"><c:out value="${employee.name}" />&nbsp;</font></td>
                                                <%--                                    <td  ><font color="red"><c:out value="${employee.dateOfBirth}" />&nbsp;</font></td>
                                                                                    <td  ><font color="red"><c:out value="${employee.fatherName}" />&nbsp;</font></td>--%>
                                            <td  ><font color="red"><c:out value="${employee.mobile}" />&nbsp;</font></td>
                                            <td  ><font color="red"><c:out value="${employee.mappedVehicle}" />&nbsp;</font></td>
                                                <%--                                    <td  ><font color="red"><c:out value="${employee.email}" />&nbsp;</font></td>--%>
                                            <td  ><font color="red"><c:out value="${employee.deptName}" />&nbsp;</font></td>
                                            <td  ><font color="red"><c:out value="${employee.desigName}" />&nbsp;</font></td>
                                                <%--
                                                <c:if test="${action == 1 }">
                                                <c:if test="${employee.gradeName == 'Primary Driver' }">
                                                <td  ><c:out value="${employee.gradeName}" />&nbsp;</td>
                                                </c:if>
                                                 </c:if>
                                                 <c:if test="${action == null }">
                                                <td  ><c:out value="${employee.gradeName}" />&nbsp;</td>
                                                 </c:if>
                                                <td  ><font color="red"><c:out value="${employee.qualification}" />&nbsp;</font></td>
                                                <td  ><font color="red"><c:out value="${employee.DOJ}" />&nbsp;</font></td>
                                                <td  ><font color="red"><c:out value="${employee.drivingLicenseNo}" />&nbsp;</font></td>
                                                <td  ><font color="red"><c:out value="${employee.referenceName}" />&nbsp;</font></td>
                                                <td  ><font color="red"><c:out value="${employee.basicSalary}" />&nbsp;</font></td>
                                                --%>
                                            <td  ><font color="red"><c:out value="${employee.status}" />&nbsp;</font></td>
                                            <td>
                                                <c:if test="${action == null}">
<!--                                                    <a href="searchViewEmp.do?staffCode=<c:out value="${employee.empCode}"/> &staffName=<c:out value="${employee.name}"/>">view</a>
                                                    &nbsp;<a href="getAlterEmpPage.do?staffCode=<c:out value="${employee.empCode}"/> &staffName=<c:out value="${employee.name}"/>">Alter</a>-->
                                                    <a href="searchViewEmp.do?staffCode=<c:out value="${employee.empCode}"/> &staffName=<c:out value="${employee.name}"/>"  ><span class="label label-info"><spring:message code="trucks.label.View"  text="default text"/> </span> </a>
                                                    &nbsp;
                                                    &nbsp;
                                                    <a href="getAlterEmpPage.do?staffCode=<c:out value="${employee.empCode}"/> &staffName=<c:out value="${employee.name}"/>"  ><span class="label label-warning"><spring:message code="trucks.label.Edit"  text="default text"/> </span> </a>

                                                </c:if>
                                                <c:if test="${action == 1}">
<!--                                                    <a href="searchViewEmp.do?staffCode=<c:out value="${employee.empCode}"/> &staffName=<c:out value="${employee.name}"/>">view</a>-->
                                                    <a href="searchViewEmp.do?staffCode=<c:out value="${employee.empCode}"/> &staffName=<c:out value="${employee.name}"/>"  ><span class="label label-info"><spring:message code="trucks.label.View"  text="default text"/> </span> </a>

                                                </c:if>
                                            </td>
                                        </tr>
                                    </c:if>

                                    <c:if test="${employee.status == 'Y' || employee.status == 'N' }">
                                        <tr>
                                            <td  ><%=index1++%></td>
                                            <td  ><c:out value="${employee.empCode}" />&nbsp;</td>
                                            <td  ><c:out value="${employee.name}" />&nbsp;</td>
                                            <%--                                    <td  ><c:out value="${employee.dateOfBirth}" />&nbsp;</td>
                                                                                <td  ><c:out value="${employee.fatherName}" />&nbsp;</td>--%>
                                            <td  ><c:out value="${employee.mobile}" />&nbsp;</td>
                                            <td  ><c:out value="${employee.mappedVehicle}" />&nbsp;</td>
                                            <%--                                    <td  ><c:out value="${employee.email}" />&nbsp;</td>--%>
                                            <td  ><c:out value="${employee.deptName}" />&nbsp;</td>
                                            <td  ><c:out value="${employee.desigName}" />&nbsp;</td>
                                            <%--
                                            <c:if test="${action == 1 }">
                                            <c:if test="${employee.gradeName == 'Primary Driver' }">
                                            <td  ><c:out value="${employee.gradeName}" />&nbsp;</td>
                                            </c:if>
                                             </c:if>
                                             <c:if test="${action == null }">
                                            <td  ><c:out value="${employee.gradeName}" />&nbsp;</td>
                                             </c:if>
                                            <td  ><c:out value="${employee.qualification}" />&nbsp;</td>
                                            <td  ><c:out value="${employee.DOJ}" />&nbsp;</td>
                                            <td  ><c:out value="${employee.drivingLicenseNo}" />&nbsp;</td>
                                            <td  ><c:out value="${employee.referenceName}" />&nbsp;</td>
                                            <td  ><c:out value="${employee.basicSalary}" />&nbsp;</td>
                                            --%>
                                            <td  ><c:out value="${employee.companyName}" />&nbsp;</td>
                                            
                                            <td  >
                                                <c:if test="${employee.userMovementType == '0'}">Both</c:if>
                                                <c:if test="${employee.userMovementType == '1'}">CBT</c:if>
                                                <c:if test="${employee.userMovementType == '2'}">NBT</c:if>
                                            </td>
                                            <td  ><c:out value="${employee.status}" />&nbsp;</td>
                                            <td>
                                                <c:if test="${action == null}">
<!--                                                    <a href="searchViewEmp.do?staffCode=<c:out value="${employee.empCode}"/> &staffName=<c:out value="${employee.name}"/>">view</a>
                                                    &nbsp;<a href="getAlterEmpPage.do?staffCode=<c:out value="${employee.empCode}"/> &staffName=<c:out value="${employee.name}"/>">Alter</a>-->
                                                    <a href="searchViewEmp.do?staffCode=<c:out value="${employee.empCode}"/> &staffName=<c:out value="${employee.name}"/>"  ><span class="label label-info"><spring:message code="trucks.label.View"  text="default text"/> </span> </a>
                                                    &nbsp;
                                                    &nbsp;
                                                    <a href="getAlterEmpPage.do?staffCode=<c:out value="${employee.empCode}"/> &staffName=<c:out value="${employee.name}"/>"  ><span class="label label-warning"><spring:message code="trucks.label.Edit"  text="default text"/> </span> </a>

                                                </c:if>
                                                <c:if test="${action == 1}">
<!--                                                    <a href="searchViewEmp.do?staffCode=<c:out value="${employee.empCode}"/> &staffName=<c:out value="${employee.name}"/>">view</a>-->
                                                    <a href="searchViewEmp.do?staffCode=<c:out value="${employee.empCode}"/> &staffName=<c:out value="${employee.name}"/>"  ><span class="label label-info"><spring:message code="trucks.label.View"  text="default text"/> </span> </a>

                                                </c:if>
                                            </td>
                                        </tr>
                                    </c:if>
                                </c:forEach >
                            </table>
                        </c:if>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span><spring:message code="hrms.label.EntriesPerPage" text="default text"/></span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text"><spring:message code="hrms.label.DisplayingPage" text="default text"/> <span id="currentpage"></span> <spring:message code="hrms.label.of" text="default text"/> <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 1);
                        </script>

                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>

