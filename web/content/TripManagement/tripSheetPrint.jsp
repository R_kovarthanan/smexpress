<%-- 
    Document   : tripSheetPrint
    Created on : Feb 1, 2013, 4:01:29 PM
    Author     : Entitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <link rel="stylesheet" href="/throttle/css/rupees.css"  type="text/css" />

        <%@ page import="ets.domain.renderservice.business.RenderServiceTO" %>
        <%@ page import="ets.domain.renderservice.business.JobCardItemTO" %>
        <%@ page import="ets.domain.problemActivities.business.ProblemActivitiesTO" %>


        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.*" %>

        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <title>Pink Slip</title>
    </head>

    <script>



        function print()
        {
            var DocumentContainer = document.getElementById("printDiv");
            var WindowObject = window.open('', "TrackHistoryData",
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }

        function back()
        {
            window.history.back()
        }

    </script>


    <body>
        <form name="suggestedPS" method="post">
            <div id="printDiv">
                <br>
                <br>
                <br>


                <table align="center" width="725" border="1" cellspacing="0" cellpadding="0"  style="border:1px; border-color:#E8E8E8; border-style:solid;" >
                    <tr>
                        <td>
                            <table align="center" width="725" border="1" cellspacing="0" cellpadding="0"  style="border:0px; border-color:#E8E8E8; border-style:solid;" >
                                <tr>
                                    <td height="27" colspan="6" class="text2"  align="center"  scope="row" style="border:0px;  border-bottom-color:#CCCCCC; border-bottom-style:solid; background:#CCCCCC; color:#000000;">
                                        <b><font size="5"> Trip Sheet </font></b>
                                    </td>
                                </tr>
                                <c:if test="${tripDetails != null}">
                                    <c:forEach items="${tripDetails}" var="td">
                                        <c:set var="routeName" value="${td.routeName}"/>
                                        <c:set var="tripVehicleId" value="${td.tripVehicleId}"/>
                                        <c:set var="tripDate" value="${td.tripDate}"/>
                                        <c:set var="tripSheetId" value="${td.tripSheetId}"/>
                                        <c:set var="custId" value="${td.custId}"/>
                                        <c:set var="orderNumber" value="${td.orderNumber}"/>
                                        <c:set var="productId" value="${td.productId}"/>
                                        <c:set var="bags" value="${td.bags}"/>
                                        <c:set var="totalTonnage" value="${td.totalTonnage}"/>
                                        <c:set var="tripTotalKms" value="${td.tripTotalKms}"/>
                                        <c:set var="GPSKm" value="${td.GPSKm}"/>
                                        <c:set var="tripTotalLitres" value="${td.tripTotalLitres}"/>
                                        <c:set var="tripTotalExpenses" value="${td.tripTotalExpenses}"/>
                                        <c:set var="tripTotalAllowances" value="${td.tripTotalAllowances}"/>
                                        <c:set var="tripFuelAmount" value="${td.tripFuelAmount}"/>
                                        <c:set var="tripBalanceAmount" value="${td.tripBalanceAmount}"/>
                                    </c:forEach>
                                </c:if>
                                <tr>
                                    <td class="text2"  align="left"  width="150" height="53" scope="col"  style="padding-bottom:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        <b>Trip Date and Time</b></td>
                                    <td  class="text2"  align="left" scope="col"  style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
                                        <input name="tripDate" type="text" class="textbox" value="<c:out value="${tripDate}"/>" readonly="true" id="tripDate"  />
                                    </td>
                                    <td class="text2"  align="left"  width="150" height="53" scope="col"  style="padding-bottom:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        <b>Trip Sheet Number</b></td>
                                    <td  class="text2"  align="left" scope="col"  style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
                                        <input name="tripSheetId" type="text" class="textbox" value="<c:out value="${tripSheetId}"/>" readonly="true" id="tripSheetId"  />
                                    </td>
                                </tr>
                                <tr>
                                    <td  class="text2"  align="left"  width="150" height="53" scope="col"  style="padding-bottom:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;" >
                                        <b>Order Number</b>
                                    </td>
                                    <td class="text2"  align="left" scope="col"  style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
                                        <input name="orderNumber" type="text" class="textbox" value="<c:out value="${orderNumber}"/>" readonly="true" id="orderNumber"  />
                                    </td>


                                </tr>
                                <tr>
                                    <td  class="text2"  align="left"  width="150" height="53" scope="col"  style="padding-bottom:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;" >
                                        <b>Route</b>
                                    </td>
                                    <td class="text2"  align="left" scope="col"  style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
                                        <input name="routeName" type="text" class="textbox" value="<c:out value="${routeName}"/>" readonly="true" id="routeName"  />
                                    </td>
                                    <td  class="text2"  align="left"  width="148" scope="col"  style="border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  " >
                                        <b>Vehicle</b>
                                    </td>
                                    <td class="text2"  align="left" scope="col"  style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
                                        <c:if test = "${vehicleRegNos != null}" >
                                            <c:forEach items="${vehicleRegNos}" var="vd">
                                                <c:if test = "${vd.vehicleId == tripVehicleId}">
                                                    <input name="vehicleId" type="text" class="textbox" value="<c:out value="${vd.regNo}"/>" readonly="true" id="vehicleId"  />
                                                </c:if>
                                            </c:forEach >
                                        </c:if>
                                    </td>

                                </tr>
                                <tr>
                                    <td  class="text2"  align="left"  width="148" scope="col"  style="border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  " >
                                        <b>Product Name</b>
                                    </td>
                                    <td class="text2"  align="left" scope="col"  style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
                                        <c:if test = "${productList != null}" >
                                            <c:forEach items="${productList}" var="pl">
                                                <c:if test = "${pl.productId == productId}">
                                                    <input name="productname" type="text" class="textbox" value="<c:out value="${pl.productname}"/>" readonly="true" id="productname"  />
                                                </c:if>
                                            </c:forEach >
                                        </c:if>
                                    </td>
                                    <td  class="text2"  align="left"  width="150" height="53" scope="col"  style="padding-bottom:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;" >
                                        <b>Trip Total Km</b>
                                    </td>
                                    <td class="text2"  align="left" scope="col"  style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
                                        <input name="tripTotalKms" type="text" class="textbox" value="<c:out value="${tripTotalKms}"/>" readonly="true" id="tripTotalKms"  />
                                    </td>
                                </tr>
                                <tr>
                                    <td  class="text2"  align="left"  width="150" height="53" scope="col"  style="padding-bottom:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;" >
                                        <b>Bags</b>
                                    </td>
                                    <td class="text2"  align="left" scope="col"  style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
                                        <input name="bags" type="text" class="textbox" value="<c:out value="${bags}"/>" readonly="true" id="bags"  />
                                    </td>
                                    <td  class="text2"  align="left"  width="150" height="53" scope="col"  style="padding-bottom:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;" >
                                        <b>Tonnage</b>
                                    </td>
                                    <td class="text2"  align="left" scope="col"  style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
                                        <input name="totalTonnage" type="text" class="textbox" value="<c:out value="${totalTonnage}"/>" readonly="true" id="totalTonnage"  />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <table align="center" width="725" border="0" cellspacing="0" cellpadding="0"  >
                                            <tr>
                                                <td height="27" colspan="7" class="text2"  align="center"  scope="row" >
                                                    <b><font size="3"> Payment Detail</font></b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <%int index = 0;%>
                                                <td style="border:1px;  border-bottom-color:#CCCCCC; border-bottom-style:solid; background:#CCCCCC; color:#000000;"><b>S.No</b></td>
                                                <td style="border:1px;  border-bottom-color:#CCCCCC; border-bottom-style:solid; background:#CCCCCC; color:#000000;"><b>Location</b></td>
                                                <td style="border:1px;  border-bottom-color:#CCCCCC; border-bottom-style:solid; background:#CCCCCC; color:#000000;"><b>Date</b></td>
                                                <td style="border:1px;  border-bottom-color:#CCCCCC; border-bottom-style:solid; background:#CCCCCC; color:#000000;"><b>Amount</b></td>
                                                <td style="border:1px;  border-bottom-color:#CCCCCC; border-bottom-style:solid; background:#CCCCCC; color:#000000;"><b>Paid Person</b></td>
                                                <td style="border:1px;  border-bottom-color:#CCCCCC; border-bottom-style:solid; background:#CCCCCC; color:#000000;"><b>Remark</b></td>
                                            </tr>
                                            <tr>
                                                <c:if test="${tripAllowanceDetails != null}">
                                                    <c:forEach items="${tripAllowanceDetails}" var="tad">
                                                        <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;" height="30"><%=index + 1%></td>

                                                        <c:if test="${opLocation != null}">
                                                            <c:forEach items="${opLocation}" var="fd">
                                                                <c:if test="${fd.locationId == tad.tripStageId}">
                                                                    <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;"><c:out value="${fd.locationName}"/></td>
                                                                </c:if>
                                                            </c:forEach>
                                                        </c:if>
                                                        <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;"><c:out value="${tad.tripAllowanceDate}"/></td>
                                                        <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;"><c:out value="${tad.tripAllowanceAmount}"/></td>
                                                        <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;"><c:out value="${tad.tripAllowancePaidBy}"/></td>
                                                        <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;"><c:out value="${tad.tripAllowanceRemarks}"/></td>
                                                    </c:forEach>
                                                </c:if>
                                            </tr>
                                            <%index++;%>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <table align="center" width="725" border="0" cellspacing="0" cellpadding="0"  >
                                            <tr>
                                                <td height="27" colspan="7" class="text2"  align="center"  scope="row" >
                                                    <b><font size="3"> Fuel Detail</font></b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <%int index1 = 0;%>
                                                <td style="border:1px;  border-bottom-color:#CCCCCC; border-bottom-style:solid; background:#CCCCCC; color:#000000;"><b>S.No</b></td>
                                                <td style="border:1px;  border-bottom-color:#CCCCCC; border-bottom-style:solid; background:#CCCCCC; color:#000000;"><b>Bunk Name</b></td>
                                                <td style="border:1px;  border-bottom-color:#CCCCCC; border-bottom-style:solid; background:#CCCCCC; color:#000000;"><b>Place</b></td>
                                                <td style="border:1px;  border-bottom-color:#CCCCCC; border-bottom-style:solid; background:#CCCCCC; color:#000000;"><b>Date</b></td>
                                                <td style="border:1px;  border-bottom-color:#CCCCCC; border-bottom-style:solid; background:#CCCCCC; color:#000000;"><b>Ltrs</b></td>
                                                <td style="border:1px;  border-bottom-color:#CCCCCC; border-bottom-style:solid; background:#CCCCCC; color:#000000;"><b>Amount</b></td>
                                                <td style="border:1px;  border-bottom-color:#CCCCCC; border-bottom-style:solid; background:#CCCCCC; color:#000000;"><b>Remark</b></td>
                                            </tr>
                                            <tr>
                                                <c:if test="${fuelDetails != null}">
                                                    <c:forEach items="${fuelDetails}" var="fd">
                                                        <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;" height="30"><%=index1 + 1%></td>
                                                        <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;"><c:out value="${fd.tripBunkName}"/></td>
                                                        <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;"><c:out value="${fd.tripBunkPlace}"/></td>
                                                        <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;"><c:out value="${fd.tripFuelDate}"/></td>
                                                        <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;"><c:out value="${fd.tripFuelLtrs}"/></td>
                                                        <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;"><c:out value="${fd.tripFuelAmounts}"/></td>
                                                        <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;"><c:out value="${fd.tripFuelRemarks}"/></td>
                                                    </c:forEach>
                                                </c:if>
                                            </tr>
                                            <%index1++;%>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td  class="text2"  align="left"  width="150" height="53" scope="col"  style="padding-bottom:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;" >
                                        <b>   <p>trip Total Allowances<br>  </b>
                                    </td>
                                    <td class="text2"  align="left" scope="col"  style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
                                        <c:if test= "${tripTotalAllowances != ''}"  >
                                            <b>:</b>&nbsp;<c:out value="${tripTotalAllowances}" />
                                        </c:if>
                                    </td>

                                    <td  class="text2"  align="left"  width="148" scope="col"  style="border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  " >
                                        <b> <p>trip Total Litres<br></b>
                                    </td>
                                    <td class="text2"  align="left" scope="col"  style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
                                        <b>:</b>&nbsp;<c:out value="${tripTotalLitres}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td  class="text2"  align="left"  width="150" height="53" scope="col"  style="padding-bottom:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;" >
                                        <b>   <p>Trip Fuel Amount<br>  </b>
                                    </td>
                                    <td class="text2"  align="left" scope="col"  style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
                                        <c:if test= "${tripFuelAmount != ''}"  >
                                            <b>:</b>&nbsp;<c:out value="${tripFuelAmount}" />
                                        </c:if>
                                    </td>

                                    <td  class="text2"  align="left"  width="148" scope="col"  style="border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  " >
                                        <b> <p>Trip Balance Amount<br></b>
                                    </td>
                                    <td class="text2"  align="left" scope="col"  style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
                                        <b>:</b>&nbsp;<c:out value="${tripBalanceAmount}" />
                                    </td>



                                </tr>


                            </table>
                            <table align="center" width="725" border="0" cellspacing="0" cellpadding="0"  style="border:1px; border-color:#E8E8E8; border-style:solid;" >
                                <br>
                                <br>
                                <br>
                                <br>
                                <tr align="center">
                                    <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">

                                        <input align="center" type="button" onclick="print();" value = " Print "   />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </form>
    </body>
</html>
