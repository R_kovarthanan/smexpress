
<%--
    Document   : viewPinkSlip
    Created on : Nov 22, 2012, 3:26:26 PM
    Author     : ASHOK
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
    </head>

    <body onload="setImages(0,0,0,0,0,0);">
        <form name="pinkSlip" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>

            <table width="100%" align="center" border="0"  id="table" class="sortable">
                <thead>
                    <%int index2 = 1;%>
                    <c:if test="${pinkSlipSummaryDetail != null}">
                        <tr>
                            <th><h3>S.No</h3></th>
                            <th><h3>PinkSlip ID</h3></th>
                            <th><h3>LPS ID</h3></th>
                            <th><h3>Route Name</h3></th>
                            <th><h3>Vehicle No</h3></th>
                            <th><h3>Driver Name</h3></th>
                            <th><h3>View</h3></th>
                            <th><h3>Delete</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${pinkSlipSummaryDetail}" var="pinkList">

                            <%--                <c:set var="DriverN1" value="${pinkList.driverID)}" />
                                            <c:set var="DriverN" value="${fn:split(DriverN1, '-')}" />
                            --%>

                            <%
                                        String classText2 = "";
                                        int oddEven2 = index2 % 2;
                                        if (oddEven2 > 0) {
                                            classText2 = "text2";
                                        } else {
                                            classText2 = "text1";
                                        }
                            %>

                            <tr>
                                <td class="<%=classText2%>"><%=index2%></td>
                                <td class="<%=classText2%>"><c:out value="${pinkList.pinkSlipID}"/></td>
                                <td class="<%=classText2%>"><c:out value="${pinkList.lpsID}"/></td>
                                <td class="<%=classText2%>"><c:out value="${pinkList.routeName}"/></td>
                                <td class="<%=classText2%>"><c:out value="${pinkList.vehicleID}"/></td>
                                <td class="<%=classText2%>"><c:out value="${pinkList.driverID}"/></td>
    <!--                                                <td class="<%=classText2%>"><c:out value="${DriverN[0]}"/></td>-->
                                <td class="<%=classText2%>" align="left"> <a href="/throttle/PinkSlipDetail.do?pinkSlipID=<c:out value='${pinkList.pinkSlipID}' />&lpsID=<c:out value="${pinkList.lpsID}"/>"  > View </a> </td>
                                <c:if test="${pinkList.tripId == null || pinkList.tripId == ''}">
                                    
                                </c:if>
                                <td class="<%=classText2%>" align="left"> <a onclick="return confirm('Are you sure you want to delete')" href="/throttle/PinkSlipDelete.do?pinkSlipID=<c:out value='${pinkList.pinkSlipID}' />&lpsID=<c:out value="${pinkList.lpsID}"/>" > Delete </a> </td>
<!--                                <td class="<%=classText2%>" align="left"> <a  href="/throttle/PinkSlipDelete.do?pinkSlipID=<c:out value='${pinkList.pinkSlipID}' />&lpsID=<c:out value="${pinkList.lpsID}"/>"  > Delete </a> </td>-->
                            </tr>
                            <%index2++;%>
                        </c:forEach>
                    </c:if>
                </tbody>
            </table>
        </form>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
                    <script type="text/javascript">
  var sorter = new TINY.table.sorter("sorter");
	sorter.head = "head";
	sorter.asc = "asc";
        sorter.desc = "desc";
	sorter.even = "evenrow";
	sorter.odd = "oddrow";
	sorter.evensel = "evenselected";
	sorter.oddsel = "oddselected";
	sorter.paginate = true;
	sorter.currentid = "currentpage";
	sorter.limitid = "pagelimit";
	sorter.init("table",1);
  </script>
    </body>
</html>
