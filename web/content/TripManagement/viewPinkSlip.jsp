
<%--
    Document   : viewPinkSlip
    Created on : Nov 22, 2012, 3:26:26 PM
    Author     : ASHOK
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
    </head>
    <script type="text/javascript">


           function VehicelStatus()
            {
                var vehicleNo = document.getElementById("regno").value;
                if(vehicleNo != "") {
                    var url = "/throttle/pinkSlipVechicelStatus.do?vehicleNo="+vehicleNo;
                    url = url+"&sino="+Math.random();
                    if (window.ActiveXObject)  {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)  {
                        httpRequest = new XMLHttpRequest();
                    }
                    httpRequest.open("GET", url, true);
                    httpRequest.onreadystatechange = function() { processRequest(); } ;
                    httpRequest.send(null);

                }
            }

            function processRequest()
            {
                if (httpRequest.readyState == 4)
                {
                    if(httpRequest.status == 200)
                    {
                        if(httpRequest.responseText.valueOf()!=""){
                            var detail = httpRequest.responseText.valueOf();
                          // alert(detail);
                           if(detail != "null"){
                               document.getElementById('DuesDetails').innerHTML = "Vehicle Alredy used  :"+detail;
                               document.getElementById('regno').value ='';
                           }
                        }
                    }
                    else
                    {
                        alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
                    }
                }

            }


        function fillData(routeId)
        {
            var Rid = routeId.split("-");
            document.pinkSlip.routeId.value = Rid[1];
        }

        function submitPage() {

            var regno = document.pinkSlip.regno.value;
            if(textValidation(document.pinkSlip.regno,'RegNo')){
                return;
            }
            var driName = document.pinkSlip.driName.value;
            if(textValidation(document.pinkSlip.driName,'DriverName')){
                return;
            }
            var route = document.getElementById('routeId');
            if(textValidation(document.pinkSlip.routeId,'RouteName')){
                return;
            }

            document.pinkSlip.action="/throttle/suggestedPinkSlip.do";
            document.pinkSlip.submit();
        }

    </script>

    <body>
<!--    <body onload="getDriverName()">-->
        <form name="pinkSlip" method="post">
            <font color="red" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; ">
                <div align="center" name="DuesDetails" id="DuesDetails" height="20">&nbsp;&nbsp;</div></font>
            <table width="870" cellpadding="0" cellspacing="2" align="center" border="0" id="report" bgcolor="#97caff" style="margin-top:0px;">
                <tr><td><b>Search</b></td></tr>
                <tr>
                    <td style="padding:15px;" align="right">
                        <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                            <tr>
                                <td><font color="red">*</font>Vehicle No</td>
                                <td>
                                    <input name="vechileId" id="vechileId" type="hidden" class="textbox" size="20" value="">
                                    <input name="regno" id="regno" type="text" class="textbox" size="20" value="" onfocus="getVehicleNosPinkSlip();" onchange="VehicelStatus();"  autocomplete="off" onPaste="return false" onDrag="return false" onDrop="return false" maxlength="5" >
                                </td>
                                <td><font color="red">*</font>Driver Name</td>
                                <td><input name="driName" id="driName" type="text" class="textbox" size="20" value=""  autocomplete="off" onfocus="getPinkSlipDriverName();"></td>
                            </tr>
                            <tr>
                                <td><font color="red">*</font>Preferred Route:</td>
                                <td class="text">
                                    <input name="destination" id="destination" type="text" class="textbox" value="" size="20" onfocus="getDestination();" onchange="fillData(this.value)" autocomplete="off">
                                    <input name="routeId" id="routeId" type="hidden" class="textbox" value="" size="20" autocomplete="off">
                                </td>
<!--                                <td><font color="red">*</font>Preferred Route:</td>
                                <td class="text">
                                    <c:set var="routeData" value="" />
                                    <select name="routeId" id="routeId" class="textbox" style="width:120px;">
                                        <option value=""> -Select- </option>
                                        <c:if test = "${routeList != null}" >
                                            <c:forEach items="${routeList}" var="rl">
                                                <option value='<c:out value="${rl.routeId}" />~<c:out value="${rl.routeName}" />'><c:out value="${rl.toCity}" /></option>
                                                <c:set var="routeData" value="${routeData}~-${rl.routeId}-${rl.routeKm}-${rl.driveBata}-${rl.routeToll}-${rl.routeCode}" />
                                            </c:forEach >
                                        </c:if>
                                    </select>
                                    <input type="hidden" name="routeDetails" id="routeDetails" value='<c:out value="${routeData}" />' />
                                </td>-->
                                    <td><input type="button" class="button"  onclick="submitPage();" value="Search"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br/>
            <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" id="lpsOrderList" class="border">
                <tr>
                    <td valign="top">
                        <div style="height:360px;overflow:scroll;overflow-y:scroll;overflow-x:hidden;">
                            <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" id="lpsOrderList" class="border">
                                <tr><th colspan="6">Order List Based On The LPS</th></tr>
                                <%int index = 0;%>
                                <c:if test="${LPSOrderList != null}">
                                    <tr>
                                        <td class="contenthead">S.No</td>
                                        <td class="contenthead">LPS Number</td>
                                        <td class="contenthead">Tonnage</td>
                                        <td class="contenthead">Bags</td>
                                        <td class="contenthead">Destination</td>
                                        <td class="contenthead">Bill Status</td>
                                        <td class="contenthead">Priority</td>

                                    </tr>
                                    <c:forEach items="${LPSOrderList}" var="LPS">
                                        <%
                                                    String classText = "";
                                                    int oddEven = index % 2;
                                                    if (oddEven > 0) {
                                                        classText = "text2";
                                                    } else {
                                                        classText = "text1";
                                                    }
                                        %>
                                        <tr>
                                            <td class="<%=classText%>" height="30"><%=index + 1%></td>
                                            <td class="<%=classText%>"><c:out value="${LPS.lpsNumber}"/></td>
                                            <td class="<%=classText%>"><c:out value="${LPS.quantity}"/></td>
                                            <td class="<%=classText%>"><c:out value="${LPS.bags}"/></td>
                                            <td class="<%=classText%>"><c:out value="${LPS.destination}"/></td>
                                            <td class="<%=classText%>"><c:out value="${LPS.billStatus}"/></td>
                                            <td class="<%=classText%>"><c:out value="${LPS.clplPriority}"/></td>
                                        </tr>
                                        <%index++;%>
                                    </c:forEach>
                                </c:if>
                            </table>
                        </div>
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td valign="top">
                        <div style="height:360px;overflow:scroll;overflow-y:scroll;overflow-x:hidden;">
                        <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" id="lpsOrderList" class="border">
                            <tr><th colspan="6">High Priority Orders</th></tr>

                            <%int index2 = 0;%>
                            <c:if test="${LPSOrderList != null}">
                                <tr>

                                    <td class="contenthead">S.No</td>
                                    <td class="contenthead">LPS Number</td>
                                    <td class="contenthead">Tonnage</td>
                                    <td class="contenthead">Bags</td>
                                    <td class="contenthead">Destination</td>
                                    <td class="contenthead">Bill Status</td>
                                    <td class="contenthead">Priority</td>

                                </tr>
                                <c:forEach items="${LPSOrderList}" var="LPS">
                                    <%
                                                String classText2 = "";
                                                int oddEven2 = index2 % 2;
                                                if (oddEven2 > 0) {
                                                    classText2 = "text2";
                                                } else {
                                                    classText2 = "text1";
                                                }
                                    %>

                                    <tr>
                                        <c:if test="${LPS.clplPriority == 'High'}">
                                            <td class="<%=classText2%>" height="30"><%=index2 + 1%></td>
                                            <td class="<%=classText2%>"><c:out value="${LPS.lpsNumber}"/></td>
                                            <td class="<%=classText2%>"><c:out value="${LPS.quantity}"/></td>
                                            <td class="<%=classText2%>"><c:out value="${LPS.bags}"/></td>
                                            <td class="<%=classText2%>"><c:out value="${LPS.destination}"/></td>
                                            <td class="<%=classText2%>"><c:out value="${LPS.billStatus}"/></td>
                                            <td class="<%=classText2%>"><c:out value="${LPS.clplPriority}"/></td>
                                        </c:if>
                                    </tr>
                                    <%index2++;%>
                                </c:forEach>
                            </c:if>
                        </table>
                     </div>
                    </td>
                </tr>


            </table>
        </form>
    <script type="text/javaScript">
        function getDriverName(){
            var oTextbox = new AutoSuggestControl(document.getElementById("driName"),new ListSuggestions("driName","/throttle/handleDriverSettlement.do?"));
        }
        function getPinkSlipDriverName(){
            var oTextbox = new AutoSuggestControl(document.getElementById("driName"),new ListSuggestions("driName","/throttle/handlePinkSlipDriver.do?"));
        }
        function getVehicleNosPinkSlip(){
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/handleVehicleNoPinkSlip.do?"));
        }
        function getDestination(){
            var oTextbox = new AutoSuggestControl(document.getElementById("destination"),new ListSuggestions("destination","/throttle/handleToDestination.do?"));
        }
    </script>
    </body>
</html>
