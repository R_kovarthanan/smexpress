
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>


    <script language="javascript">

        function show_src() {
            document.getElementById('exp_table').style.display = 'none';
        }
        function show_exp() {
            document.getElementById('exp_table').style.display = 'block';
        }
        function show_close() {
            document.getElementById('exp_table').style.display = 'none';
        }


        var httpReq;
        var temp = "";
        function ajaxData()
        {

            var url = "/throttle/getModels1.do?mfrId=" + document.mileage.mfrId.value;
            if (window.ActiveXObject)
            {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest)
            {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() {
                processAjax();
            };
            httpReq.send(null);
        }

        function processAjax()
        {
            if (httpReq.readyState == 4)
            {
                if (httpReq.status == 200)
                {
                    temp = httpReq.responseText.valueOf();
                    setOptions(temp, document.mileage.modelId);
                    setTimeout("fun()", 1);
                }
                else
                {
                    alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                }
            }
        }
        function submitGo() {
            document.mileage.action = '/throttle/viewVehicleMilleage.do';
            document.mileage.submit();

        }
        function deleteMilleage(indx) {
            var mfrId = document.getElementsByName("mfrIds");
            var modelId = document.getElementsByName("modelIds");
            var fromYear = document.getElementsByName("fromYears");
            var toYear = document.getElementsByName("toYears");

            document.mileage.action = '/throttle/deleteVehicleMilleage.do?mfrId=' + mfrId[indx].value + '&modelId=' + modelId[indx].value + '&fromYear=' + fromYear[indx].value + '&toYear=' + toYear[indx].value;
            document.mileage.submit();

        }

        function fun() {
            document.mileage.modelId.value = '<%=request.getAttribute("modelId")%>';
        }



        function setValues() {
            if ('<%=request.getAttribute("vehicleTypeId")%>' != 'null') {
                document.mileage.vehicleTypeId.value = '<%=request.getAttribute("vehicleTypeId")%>';
            }
            ajaxData();
        }

        function setSelectbox(i,obj) {
            var selected = document.getElementsByName("selectedIndex");
            if(obj.value != ''){
            selected[i].checked = 1;
            }    
        }
        
        function submitPage() {
            var checValidate = selectedItemValidation();
//                document.mileage.action = '/throttle/saveVehicleMilleage.do';
//                document.mileage.submit();
        }
        function selectedItemValidation(){
            var index = document.getElementsByName("selectedIndex");
            var chec=0;
            for(var i=0;(i<index.length && index.length!=0);i++){
                if(index[i].checked){
                chec++;
                }
            }
            if(chec == 0){
            alert("Please Select Any One And Then Proceed");
            }else{
            document.mileage.action = '/throttle/saveVehicleMilleage.do';
            document.mileage.submit();
            }
        }
    </script>
    
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.MileageConfiguration" text="MileageConfiguration"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Fuel" text="Fuel"/></a></li>
		                    <li class=""><spring:message code="hrms.label.MileageConfiguration" text="MileageConfiguration"/></li>
		
		                </ol>
		            </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
                    
    <body onload="setValues();">
        <form name="mileage" method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>                 
            <%@ include file="/content/common/message.jsp" %>

            
               
              
                   <table class="table table-info mb30 table-hover" id="report" >
		    <thead>
		<tr>
		    <th colspan="3"  >Milleage Configuration Search</th>
		</tr>
                     </thead>
                   
                            
                               
                                    <tr >
                                        <td>Vehicle Type</td>
                                        <td ><select class="form-control" style="width:250px;height:40px" name="vehicleTypeId" id="vehicleTypeId">
                                                <option value="0">---Select---</option>
                                                <c:if test = "${vehicleTypeList != null}" >
                                                    <c:forEach items="${vehicleTypeList}" var="vtList">
                                                        <option value='<c:out value="${vtList.typeId}" />'><c:out value="${vtList.typeName}" /></option>
                                                    </c:forEach >
                                                </c:if>
                                            </select></td>
                                        
                                        <td><input type="button" class="btn btn-success" style="width:100px;height:40px" onclick="submitGo();" value="Go"></td>
                                    </tr>
                                   
                                </table>
                            
                    
            <c:if test = "${milleageList!= null}">

                <table class="table table-info mb30 table-hover" id="report" >
                    <thead>

                    <tr >

                        <th >S no</th>                              
                        <th >Vehicle Type</th>                              
                        <th >Vehicle Mileage</th>
                        <th >Reefer Mileage</th>
                        <th >Select</th>

                    </tr>
                    </thead>
                    <% int index = 0;
                    int sno = 1;%>
                    <c:forEach items="${milleageList}" var="fuel"> 
                        <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                            
                        %>	
                        <tr >

                            <td ><%=sno%></td>                                                         
                            <td ><input type="hidden" name="vehicleTypeIds" value='<c:out value="${fuel.vehicleTypeId}"/>'><c:out value="${fuel.vehicleTypeName}" /></td>                                                         
                            <td ><input type="text" class="form-control" style="width:250px;height:40px" name="vehicleMileage" value="<c:out value="${fuel.milleage}" />" onchange="setSelectbox('<%= index %>',this);"/></td>
                            <td ><input type="text" class="form-control" style="width:250px;height:40px" name="reeferMileage" value="<c:out value="${fuel.reeferMileagePerKm}" />" onchange="setSelectbox('<%= index %>',this);"/></td>
                            <td ><input type="checkbox" name="selectedIndex" value="<%=index%>"/></td>
                        </tr>
                        <%
                        index++;
                        sno++;
                        %>
                    </c:forEach>
                </table>
            </c:if>
            
            <center><input type="button" class="btn btn-success" value="Update Mileage" name="update" onclick="submitPage();" >
            </center>
            
            
        </form>
    </body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>
