<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    </head>
    <script language="javascript">
        function submitPage(){
            var chek=Validation();
            if(chek=='true'){
                document.alterVeh.action='/throttle/alterVehicleCompany.do';
                document.alterVeh.submit();        
            }
        }
        function Validation(){
            
            if(textValidation(document.alterVeh.regNo,'Vehicle No')){       
                return 'false';
            }     
            if(textValidation(document.alterVeh.date,'Purchase Date')){       
                return 'false';
            }     
           
            return 'true';
        } 
        
        function getVehicleNos(){
            
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regNos","/throttle/getVehicleNos.do?"));
        }    
    </script>
    <body onload="getVehicleNos();">
        <form name="alterVeh" method="post">
            <%@ include file="/content/common/path.jsp" %>                 
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <c:if test = "${VehicleCompList!= null}" >
                <table align="center" width="400" class="border" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <Td colspan="2" class="contenthead">Alter Vehicle Company</Td>
                    </tr>
                    
                    <% int index = 0;%>
                    <c:forEach items="${VehicleCompList}" var="veh"> 
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>	
                        <tr height="30">
                            <td class="text1">Vehicle Number</td>
                            <td class="text1"><input name="regNo" readonly id="regno" class="textbox" type="text" value='<c:out value="${veh.regNo}"/>' size="20" ></td>
                            <input type="hidden" name="vehicleId" value='<c:out value="${veh.vehicleId}"/>'>
                        </tr>
                        <tr>
                            <td class="text2">Vehicle Type</td>
                            <td class="text2"><select class="textbox" name="vehTypeId" style="width:125px" >
                                    
                                    <c:if test = "${TypeList != null}" >
                                        <c:forEach items="${TypeList}" var="type"> 
                                            <c:choose>                                
                                                <c:when test="${type.typeName==veh.typeName}">
                                                    <option selected value='<c:out value="${type.typeId}" />'><c:out value="${type.typeName}" /></option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value='<c:out value="${type.typeId}" />'><c:out value="${type.typeName}" /></option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </c:if>  	
                            </select></td>
                        </tr>
                        <tr>
                            
                           <tr>
                    <td height="30" class="text2"><font color="red">*</font>MFR</td>
                    <td height="30" class="text2"><select name="mfrId" onchange="ajaxData();" class="textbox" style="width:125px">
                            
                            <c:if test = "${MfrLists != null}" >
                                <c:forEach items="${MfrLists}" var="mfr"> 
                                <c:if test="${veh.mfr==mfr.mfrName}">
                                    <option selected value="<c:out value="${mfr.mfrId}"/>"><c:out value="${mfr.mfrName}"/></option>
                                </c:if>
                                <option value="<c:out value="${mfr.mfrId}"/>"><c:out value="${mfr.mfrName}"/></option>
                               </c:forEach>
                            </c:if>        
                    </select></td>
                </tr>
                <tr>
                    <td height="30" class="text1"><font color="red">*</font>Model</td>
                    <td height="30" class="text1"><select name="modelId" class="textbox" style="width:125px">
                           
                             <c:if test = "${ModelLists != null}" >
                                <c:forEach items="${ModelLists}" var="model"> 
                                 <c:if test="${veh.model==model.modelName}">
                                    <option selected value="<c:out value="${model.modelId}"/>"><c:out value="${model.modelName}"/></option>
                                </c:if>
                                    <option value="<c:out value="${model.modelId}"/>"><c:out value="${model.modelName}"/></option>
                                </c:forEach>
                            </c:if> 
                    </select></td> 
                </tr>
                <tr>
                    <td height="30" class="text2"><font color="red">*</font>Usage Type</td>
                    <td height="30" class="text2"><select name="usageTypeId" class="textbox" style="width:125px">
                          
                            <c:if test = "${usageList != null}" >
                                <c:forEach items="${usageList}" var="type"> 
                                 <c:if test="${veh.usageType==type.usageName}">
                                    <option selected value="<c:out value="${type.usageId}"/>"><c:out value="${type.usageName}"/></option>
                                </c:if>
                                    <option value="<c:out value="${type.usageId}"/>"><c:out value="${type.usageName}"/></option>
                                </c:forEach>
                            </c:if>       
                    </select></td> 
                </tr>
                <tr height="30">
                    <td class="text1"><font color="red">*</font>Owning Company</td>
                    <td height="30" class="text1"><select name="owningCompId" class="textbox" style="width:125px">
                            
                            <c:if test = "${OwningCompanyList != null}" >
                                <c:forEach items="${OwningCompanyList}" var="owner"> 
                                 <c:if test="${veh.servicePtName==owner.custName}">
                                    <option selected value='<c:out value="${owner.custId}"/>'><c:out value="${owner.custName}"/></option>
                                </c:if>
                                    <option value='<c:out value="${owner.custId}"/>'><c:out value="${owner.custName}"/></option>
                                </c:forEach>
                            </c:if>       
                    </select></td> 
                    
                </tr>
                <tr>
                    <td class="text2"><font color="red">*</font>Using Company</td>
                    <td class="text2"><select class="textbox" name="usingCompId" style="width:125px">
                           
                            <c:if test = "${UsingCompanyList != null}" >
                                <c:forEach items="${UsingCompanyList}" var="comp1"> 
                                    <c:if test = "${comp1.activeInd=='Y'}" >
                                    <c:if test="${veh.companyName==comp1.companyName}">
                                        <option selected value='<c:out value="${comp1.usingCompId}" />'><c:out value="${comp1.companyName}" /><c:out value="${comp.usingCompId}" /></option>
                                    </c:if>
                                        <option value='<c:out value="${comp1.usingCompId}" />'><c:out value="${comp1.companyName}" /><c:out value="${comp.usingCompId}" /></option>
                                    </c:if>
                                </c:forEach>
                            </c:if>  	
                    </select></td>
                </tr>
                <tr height="30">
                    <td class="text1"><font color="red">*</font>Purchase Date</td>
                    <td class="text1"><input name="date" class="textbox"  type="text" value='<c:out value="${veh.date}" />'   size="20">
                    <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.addVeh.date,'dd-mm-yyyy',this)"/></td>
                    
                </tr>
                 <tr>
                <td class="text2"><font color="red">*</font>Status</td>
                    <td class="text2"> <select class="textbox" name="activeInd" style="width:125px">
                            <c:if test = "${veh.activeInd=='Y'}" >
                            <option selected value="Y">Active</option>
                            <option value="N">InActive</option>                            
                            </c:if>
                            <c:if test = "${veh.activeInd=='N'}" >
                            <option  value="Y">Active</option>
                            <option selected value="N">InActive</option>                            
                            </c:if>
                    </select></td>
                </tr>
                        <% index++;%>
                    </c:forEach>
                    
                </table>
            </c:if>
            <br><br>
            <center><input type="button" class="button" value="Save" onclick="submitPage();" /></center>
            <br><br>
            <br><br>
        </form>
    </body>
</html>
