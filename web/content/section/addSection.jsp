<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
 <script language="javascript" src="/throttle/js/validate.js"></script>   

</head>
<script>
    function submitpage()
    {
       if(textValidation(document.dept.sectionName,"SectionName")){
           return;
       }
       if(textValidation(document.dept.sectionCode,"SectionCode")){
           return;
       }
       if(textValidation(document.dept.description,"Description")){
           return;
       }
       if(textValidation(document.dept.description,"Description")){
           return;
       }
       document.dept.action= "/throttle/addSection.do";
       document.dept.submit();
    }
</script>
<body>
    
<form name="dept"  method="post">
<%@ include file="/content/common/path.jsp" %>
       

<%@ include file="/content/common/message.jsp" %>
 
<table align="center" border="0" cellpadding="0" cellspacing="0" width="300" id="bg" class="border">
<tr height="30">
  <td colspan="5" class="contenthead"><div class="contenthead"> Add Section</div></td>
  </tr>
<tr>
<td class="text1" height="30"><font color=red>*</font>Section Name</td>
<td class="text1" height="30"><input name="sectionName" type="text" class="textbox" value=""></td>
</tr>
<tr>
<td class="text2" height="30"><font color=red>*</font>Section Code</td>
<td class="text2" height="30"><input name="sectionCode" type="text" maxlength="3" class="textbox" value=""></td>
</tr>
<tr>
<td class="text1" height="30"><font color=red>*</font> Description</td>
<td class="text1" height="30"><textarea class="textbox" name="description"></textarea></td>
</tr>
<tr>
<td class="text1" height="30"><font color=red>*</font>Service Type</td>
<td class="text1" height="30">
    <c:if test="${serviceTypeList != null}">
    <select name="serviceTypeId" id="serviceTypeId" class="textbox">
            <option value="0">--Select--</option>
        <c:forEach items="${serviceTypeList}" var="service">
            <option value="<c:out value="${service.serviceTypeId}"/>"><c:out value="${service.serviceTypeName}"/></option>
        </c:forEach>
    </select>
    </c:if>
</td>
</tr>
</table>
<br>
<center>
<input type="button" value="Add" class="button" onclick="submitpage();">
&emsp;<input type="reset" class="button" value="Clear">
</center>
</form>
</body>
</html>
