<%-- 
    Document   : addVendorInvoiceReceipt
    Created on : 30 Jan, 2017, 11:55:58 AM
    Author     : pavithra
--%>


<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {

        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            dateFormat: 'dd-mm-yy',
            changeMonth: true, changeYear: true
        });

    });
</script>

<script type="text/javascript">



    function submitPage(value) {

        if (document.getElementById('vendorName').value == '0') {
            alert("Please select Vendor Name");
            document.getElementById('vendorName').focus();
            return false();
        }
        if (value == "ExportExcel") {
            document.payment.action = '/throttle/handleVendorOutstandingReport.do?param=ExportExcel';
            document.payment.submit();
        } else {
            document.payment.action = '/throttle/handleVendorOutstandingReport.do?param=Search';
            document.payment.submit();
        }
    }



</script>
<style>
    #index td {
        color:white;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i>Vendor Outstanding Report</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="Vendor Outstanding Report"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Report"  text="Report"/></a></li>
            <li class="active">Vendor Outstanding Report</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="payment" id="payment" class="form-horizontal form-bordered" method="post">

                    <table class="table table-info mb30 table-hover">
                        <thead>
                            <tr height="30" id="index">
                                <th colspan="6" ><b>Vendor Outstanding Report</b></th>
                            </tr>
                        </thead>
                        <tr height="40">
                            <td  ><font color=red>*</font><spring:message code="finance.label.Vendor"  text="default text"/></td>
                            <td  >
                                <select class="form-control" name="vendorName" id="vendorName" style="width:220px;height:40px;">
                                    <option value='0'>--Select--</option>
                                    <c:if test="${vendorList != null}">
                                        <c:forEach items="${vendorList}" var="vendor">

                                            <option value='<c:out value="${vendor.vendorId}"/>'><c:out value="${vendor.vendorName}"/></option>
                                            <script>
                                                document.getElementById("vendorName").value = '<c:out value="${vendorName}"/>';
                                            </script>
                                        </c:forEach>
                                    </c:if>

                                </select>

                            </td>
                            <td style="display:none"><font color=red >*</font><spring:message code="finance.label.InvoiceNo"  text="default text"/></td>
                            <td style="display:none">
                                <input type="textbox" name="invoiceNo" id="invoiceNo" value="<c:out value="${invoiceNo}"/>" autocomplete="off" class="form-control" style="width:220px;height:40px;"/>
                            </td>
                        </tr>
                        <tr>
                            <td><font color=red>*</font>From Date</td>
                            <td>
                                <input type="textbox" name="fromDate" id="fromDate" value="<c:out value="${fromDate}"/>" autocomplete="off" class="datepicker" style="width:220px;height:40px;"  onchange="searchPage('search')"/>
                            </td>
                            <td><font color=red>*</font>To Date</td>
                            <td>
                                <input type="textbox" name="toDate" id="toDate" value="<c:out value="${toDate}"/>" autocomplete="off" class="datepicker" style="width:220px;height:40px;"  onchange="searchPage('search')"/>
                            </td>
                        </tr>
                    </table>
                    <center>
                    <input type="button" value="Search" class="btn btn-success" id="find" onClick="submitPage(this.value);" style="width:100px;height:35px;">
                    </center>
                    <br>
                    <br>
                    <% int sno1 = 0;
                        int index1 = 1;%>

                    <div>
                        <c:set var="outstandingAmount" value="${0}"/>
                        <c:set var="totalAmtHire" value="${0}"/>
                        <table class="table table-info mb30 table-hover"  >
                            <thead>
                                <tr align="center"  height="30">
                                    <th height="30">S.No</th>
                                    <th height="30">Vendor Name</th>
                                    <th height="30">Expense</th>
                                    <th height="30">Initial Advance </th>
                                    <th height="30">Invoice Amount </th>
                                    <th height="30">Outstanding Amount</th>

                                </tr>
                            </thead>
                            <tbody>

                                <c:forEach items="${vendorOutStandingReport}" var="list">
                                    <c:set var="outstandingAmount" value="${list.totalExpense -(list.paidAdvance-list.invoiceAmount)}"/>
                                    <%

                                        String classText = "";
                                        int oddEven = index1 % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                                    %>


                                    <tr  width="208" height="40" > 
                                        <td height="20"><%=index1%></td>
                                        <td height="20"><c:out value="${list.vendorName}"/></td>
                                        <td height="20"><c:out value="${list.totalExpense}"/></td>
                                        <td height="20"><c:out value="${list.paidAdvance}"/></td>
                                        <td height="20"><c:out value="${list.invoiceAmount}"/></td>
                                        <td height="20"><c:out value="${outstandingAmount}"/></td>


                                    </tr>
                                <script>setActiveIndHire(<%=index1%>);</script>
                                <%
                                    index1++;
                                    sno1++;
                                %>
                            </c:forEach>
                            <input type="hidden" name="count" id="count" value="<%=sno1%>"/>

                            </tbody>
                        </table>
                    </div>
                    <%--</c:if>--%>
                    <br>
                    <br>

                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

