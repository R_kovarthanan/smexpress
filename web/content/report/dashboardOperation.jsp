<%@ include file="../common/NewDesign/header.jsp" %>
<%@ include file="../common/NewDesign/sidemenu.jsp" %>


<!--<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>-->

<!--<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>-->

<!-- Data from www.netmarketshare.com. Select Browsers => Desktop share by version. Download as tsv. -->
<pre id="tsv" style="display:none"></pre>




<div class="pageheader">
    <h2><i class="fa fa-home"></i> Dashboard
        <!--<span>Operations</span>-->
    </h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li>Dashboard</li>
            <!--<li class="active">Operations</li>-->
        </ol>
    </div>
</div>

<div class="contentpanel" style="overflow: auto;height:1600px">

    <div class="row">

        <div class="col-sm-6 col-md-3">
            <div class="panel panel-success panel-stat">
                <div class="panel-heading">

                    <div class="stat">
                        <div class="row">
                            <div class="col-xs-4">

                                <i class="ion ion-android-bus"></i>
                            </div>
                            <div class="col-xs-8">
                                <!--                    <small class="stat-label">Truck Utilisation</small>-->
                                <h4> <spring:message code="dashboard.label.Routes"  text="Routes    "/></h4>

                                <h1><span id="totalTruckNosSpan"></span></h1>
                            </div>
                        </div><!-- row -->

                        <div class="mb15"></div>

                    </div><!-- stat -->

                </div><!-- panel-heading -->
            </div><!-- panel -->
        </div><!-- col-sm-6 -->


        <div class="col-sm-6 col-md-3">
            <div class="panel panel-warning panel-stat">
                <div class="panel-heading">

                    <div class="stat">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="ion ion-happy"></i>
                            </div>
                            <div class="col-xs-8">
                                <h4> <spring:message code="dashboard.label.Total Trips"  text="Total Trips"/></h4>
                                <h1><span id="totalMonthlyLeaseCustomerNosSpan"></span></h1>
                            </div>
                        </div><!-- row -->

                        <div class="mb15"></div>



                    </div><!-- stat -->

                </div><!-- panel-heading -->
            </div><!-- panel -->
        </div><!-- col-sm-6 -->
    </div><!-- row -->
    
    
    <table style="width:100%">
        <tr>
            <td>
                <div class="row" >
                    <div class="col-sm-8 col-md-9">
                        <div class="panel panel-default">

                            <div class="outer1" >
                                <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div><!-- panel-btns -->
                                <h5  class="panel-title" ><font color="white"><b><spring:message code="dashboard.label.RegionWise"  text="Customer Revenue"/></b></font></h5>
                                <div  id="custRevenue" style="min-width: 500px; height: 400px; margin: 0 auto"></div>
                            </div>
                        </div>
                    </div></div>
            </td>
            <td>
                <div class="row" >
                    <div class="col-sm-8 col-md-9">
                        <div class="panel panel-default">

                            <div class="outer2">
                                <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div><!-- panel-btns -->
                                <h5 class="panel-title"><font color="white"><b><spring:message code="dashboard.label.RegionWise"  text="Customer Profit"/></b></font></h5>
                                <div id="custProfit" style="min-width: 500px; height: 400px; margin: 0 auto"></div>
                            </div>

                        </div>
                    </div></div>
            </td>
        </tr>
    </table>   

    <table style="width:100%">
        <tr>
            <td>
                <div class="row" >
                    <div class="col-sm-8 col-md-9">
                        <div class="panel panel-default">

                            <div class="outer3">
                                <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div><!-- panel-btns -->
                                <h5  class="panel-title"> <font color="white"><b><spring:message code="dashboard.label.RegionWise"  text="Region Wise Order"/></b></font></h5>
                                <div  id="regionWise" style="min-width: 500px; height: 400px; margin: 0 auto"></div>
                            </div>                        
                        </div>
                    </div></div>
            </td>
            <td>
                <div class="row" >
                    <div class="col-sm-8 col-md-9">
                        <div class="panel panel-default">

                            <div class="outer4">
                                <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div><!-- panel-btns -->
                                <h5 class="panel-title"><font color="white"><b><spring:message code="dashboard.label.RegionWise"  text="Route Wise Order"/></b></font></h5>
                                <div id="routeWiseTrip" style="min-width: 500px; height: 400px; margin: 0 auto"></div>
                            </div>
                          
                        </div>
                    </div></div>
            </td>
        </tr>
    </table>
    <table style="width:50%">
        <tr>
            <td>
                <div class="row" >
                    <div class="col-sm-8 col-md-9">
                        <div class="panel panel-default">

                            <div class="outer5">
                                <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div><!-- panel-btns -->
                                <h5  class="panel-title"> <font color="white"><b><spring:message code="dashboard.label.StatusWise"  text="Status Wise Trip"/></b></font></h5>
                                <div  id="statusWise" style="min-width: 500px; height: 400px; margin: 0 auto"></div>
                            </div>                        
                        </div>
                    </div></div>
            </td>
            
        </tr>
    </table>
    
    <br>
    <br>
    
       

    <style>
        .outer1 {
            width: 600px;
            color: navy;            
            background-color: #1caf9a;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer2 {
            width: 600px;
            color: navy;
            background-color: #a94442;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer3 {
            width: 600px;
            color: navy;
            background-color: #f0ad4e;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer4 {
            width: 600px;
            color: navy;
            background-color: #ccad4e;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer5 {
            width: 600px;
            color: navy;            
            background-color: #1caf9a;
            border: 2px solid darkblue;
            padding: 5px;
        }
      
        .ben{
            width: 600px;
        }
    </style>
<!--    <style>
        .outer {
            width: 600px;
            color: navy;            
            background-color: #1caf9a;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer1 {
            width: 600px;
            color: navy;
            background-color: #428bca;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer2 {
            width: 600px;
            color: navy;
            background-color: #a94442;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer3 {
            width: 600px;
            color: navy;
            background-color: #f0ad4e;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer4 {
            width: 1210px;
            color: navy;
            background-color: white;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer5 {
            width: 700px;
            color: violet;
            background-color: teal;
            border: 2px solid teal;
            padding: 5px;
        }
        .ben{
            width: 600px;
        }
    </style>-->
    <!-- /.box -->




</div><!-- contentpanel -->

</div><!-- mainpanel -->

<%@ include file="../common/NewDesign/settings.jsp" %>

<script>
    loadTrucknos();
    function loadTrucknos() {
        $.ajax({
            url: '/throttle/getDashBoardOperationNos.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                    if (value.Name == 'Route') {
                        $("#totalTruckNosSpan").text(value.Count);
                    } else if (value.Name == 'Orders') {
                        $("#totalDriversNosSpan").text(value.Count);
                    } else if (value.Name == 'Trips') {
                        $("#totalMonthlyLeaseCustomerNosSpan").text(value.Count);
                    }


                });
//                        vehicleMakeArray.push(value.Make);
//                        vehicleCountArray.push([parseInt(value.Count)]);
            }
        });
    }
</script>


<script>

    regionWise();
    function regionWise() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/getOverAllTripNos.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                    x_values_sub['name'] = value.Name;
                    x_values_sub['y'] = parseInt(value.Count);
                    x_values.push(x_values_sub);
                    x_values_sub = {};
//                    }
                });
//                alert(vehicleMakeArray[1])
                $('#regionWise').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y} ',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Nos',
                            data: x_values
                        }]
                });
            }
        });
    }

  routeWiseTrip();
    function routeWiseTrip() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/getRouteWiseTripNos.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                    x_values_sub['name'] = value.Name;
                    x_values_sub['y'] = parseInt(value.Count);
                    x_values.push(x_values_sub);
                    x_values_sub = {};
//                    }
                });
//                alert(vehicleMakeArray[1])
                $('#routeWiseTrip').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y} ',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Nos',
                            data: x_values
                        }]
                });
            }
        });
    }

    custRevenue();
    function custRevenue() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/getCustRevenu.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                    x_values_sub['name'] = value.CustName;
                    x_values_sub['y'] = parseInt(value.Revenu);
                    x_values.push(x_values_sub);
                    x_values_sub = {};
//                    }
                });
//                alert(vehicleMakeArray[1])
                $('#custRevenue').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y} ',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Nos',
                            data: x_values
                        }]
                });
            }
        });
    }


    custProfit();
    function custProfit() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/getCustProfit.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                    x_values_sub['name'] = value.CustName;
                    x_values_sub['y'] = parseInt(value.Profit);
                    x_values.push(x_values_sub);
                    x_values_sub = {};
//                    }
                });
//                alert(vehicleMakeArray[1])
                $('#custProfit').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y} ',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Nos',
                            data: x_values
                        }]
                });
            }
        });
    }
    
    loadMonthlyTripStatus();
    function loadMonthlyTripStatus() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/getStatusWiseTripNos.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                    x_values_sub['name'] = value.Name;
                    x_values_sub['y'] = parseInt(value.Count);
                    x_values.push(x_values_sub);
                    x_values_sub = {};
                });

                $('#statusWise').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y} ',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Nos',
                            data: x_values
                        }]
                });
            }
        });
    }
</script>

<script>
//    loadMonthlyTripStatus();
//    function loadMonthlyTripStatus() {
//        var vehicleMakeArray = [];
//        var vehicleCountArray = [];
//        $.ajax({
//            url: '/throttle/getStatusWiseTripNos.do',
//            dataType: 'json',
//            success: function (data) {
//                $.each(data, function (key, value) {
//                        vehicleMakeArray.push(value.Name);
//                        vehicleCountArray.push([parseInt(value.Count)]);
//                });
//                $('#truckType').highcharts({
//                    chart: {
//                        type: 'column'
//                    },
//                    title: {
//                        text: 'Status Wise Trip Details'
//                    },
//                    subtitle: {
//                        text: ''
//                    },
//                    xAxis: {
//                        type: 'category',
//                        categories: vehicleMakeArray,
//                        labels: {
//                            rotation: -45,
//                            style: {
//                                fontSize: '13px',
//                                fontFamily: 'Verdana, sans-serif'
//                            }
//                        }
//                    },
//                    yAxis: {
//                        min: 0,
//                        title: {
//                            text: 'No Of Trips'
//                        }
//                    },
//                    legend: {
//                        enabled: false
//                    },
//                    tooltip: {
//                        pointFormat: 'Trip : <b>{point.y:f} Nos</b>'
//                    },
//                    series: [{
//                            name: 'RegionWise',
//                            data: vehicleCountArray,
//                            dataLabels: {
//                                enabled: true,
//                                rotation: -90,
//                                color: '#FFFFFF',
//                                align: 'right',
//                                format: '{point.y:f}', // one decimal
//                                y: 10, // 10 pixels down from the top
//                                style: {
//                                    fontSize: '13px',
//                                    fontFamily: 'Verdana, sans-serif'
//                                }
//                            }
//                        }]
//                });
//            }
//        });
//    }
//    
//    loadMonthlyTripStatus1();
//    function loadMonthlyTripStatus1() {
//        var vehicleMakeArray = [];
//        var vehicleCountArray = [];
//        $.ajax({
//            url: '/throttle/getRouteWiseTripNos.do',
//            dataType: 'json',
//            success: function (data) {
//                $.each(data, function (key, value) {
//                        vehicleMakeArray.push(value.Name);
//                        vehicleCountArray.push([parseInt(value.Count)]);
//                });
//                $('#barchart1').highcharts({
//                    chart: {
//                        type: 'column'
//                    },
//                    title: {
//                        text: 'Route Wise Trips'
//                    },
//                    subtitle: {
//                        text: ''
//                    },
//                    xAxis: {
//                        type: 'category',
//                        categories: vehicleMakeArray,
//                        labels: {
//                            rotation: -45,
//                            style: {
//                                fontSize: '13px',
//                                fontFamily: 'Verdana, sans-serif'
//                            }
//                        }
//                    },
//                    yAxis: {
//                        min: 0,
//                        title: {
//                            text: 'No Of Trips'
//                        }
//                    },
//                    legend: {
//                        enabled: false
//                    },
//                    tooltip: {
//                        pointFormat: 'Trips : <b>{point.y:f} Nos</b>'
//                    },
//                    series: [{
//                            name: 'Trip Nos',
//                            data: vehicleCountArray,
//                            dataLabels: {
//                                enabled: true,
//                                rotation: -90,
//                                color: '#FFFFFF',
//                                align: 'right',
//                                format: '{point.y:f}', // one decimal
//                                y: 10, // 10 pixels down from the top
//                                style: {
//                                    fontSize: '13px',
//                                    fontFamily: 'Verdana, sans-serif'
//                                }
//                            }
//                        }]
//                });
//            }
//        });
//    }
</script>
<script>
  
</script>



<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

