<%-- 
    Document   : displayTripSheetWiseExcel
    Created on : Jun 11, 2013, 10:38:22 PM
    Author     : Entitle
--%>

<%@page import="java.text.SimpleDateFormat"%>
<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
         <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }

        </style>
    </head>
    <body>
        <form name="tripwise" action=""  method="post">
            <%
                        DecimalFormat df = new DecimalFormat("0.00##");
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "DSR Report.xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>
            <br/>
            
             <c:if test="${dsrReport != null}">
                        <table  id="table" class="table table-info mb30 table-hover">

                            <thead>
                                <tr height="50">
                                    <th class="text1" align="center">S.No</th>                                    
                                    <th class="text1" align="center">Docket/LR/AWB</th>                                    
                                    <th class="text1" align="center">Fleet Center</th>
                                    <th class="text1" align="center">Customer Name</th>
                                    <th class="text1" align="center">Invoice</th>
                                    <th class="text1" align="center">Shipping warehouse</th>
                                    <th class="text1" align="center">Destination City</th>
                                    <th class="text1" align="center">No of Pallets</th>
                                    <th class="text1" align="center">Actual weight in kg</th>
                                    <th class="text1" align="center">Vehicle No</th>
                                    <th class="text1" align="center">Vehicle Type</th>
                                    <th class="text1" align="center">Pickup Date</th>
                                    <th class="text1" align="center">Pickup Time</th>
                                    <th class="text1" align="center">EwayBillNo</th>
                                    <th class="text1" align="center">Seal/Tag No</th>
                                    <th class="text1" align="center">EDD</th>
                                    <th class="text1" align="center">Vehicle Arrival Date</th>
                                    <th class="text1" align="center">Actual Delivered Date</th>
                                    <th class="text1" align="center">Delivered Time</th>
                                    <th class="text1" align="center">Status</th>
                                    <th class="text1" align="center">Total Distance (KMS)</th>


                                </tr>
                            </thead>
                            <tbody>
                                <% int index = 1;%>

                                <c:forEach items="${dsrReport}" var="dsr">
                                    <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                                    %>

                                     <tr>
                                        <td class="<%=classText%>"><%=index++%></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.docketNo}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.companyName}"/></td>                                        
                                        <td class="<%=classText%>"><c:out value="${dsr.consignorName}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.invoiceNo}"/></td>

                                        <td class="<%=classText%>"><c:out value="${dsr.origin}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.destination}"/></td>

                                        <td class="<%=classText%>"><c:out value="${dsr.totalPackages}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.invoiceWeight}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.regNo}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.vehicleType}"/></td>

                                        <td class="<%=classText%>"><c:out value="${dsr.tripStartDate}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.tripstarttime}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.eWayBillNo}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.sealno}"/></td>

                                        <td class="<%=classText%>"><c:out value="${dsr.planeDate}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.destinationReportingDateTime}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.actualEndDateTime}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.tripEndTime}"/></td>
                                        <td class="<%=classText%>">
                                            <c:if test="${dsr.status == 8}">
                                                Yet to Start
                                            </c:if>
                                            <c:if test="${dsr.status == 10}">
                                                In-Transit
                                            </c:if>
                                            <c:if test="${dsr.status > 10}">
                                                Delivered
                                            </c:if>
                                        </td>                                        
                                        <td  class="<%=classText%>"  ><c:out value="${dsr.startKM}"/></td>

                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
        </form>
    </body>
</html>
