<%-- 
    Document   : displayDistrictWiseExcelview
    Created on : 10-Jun-2013, 23:18:35
    Author     : Hari@entitlesolutions.com
--%>
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
         <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }

        </style>
    </head>
    <body>
        <form name="districtwise" action=""  method="post">
            <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "Ditrictwise_Sheet_Report-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>
            <br/>
              <%
                             DecimalFormat df = new DecimalFormat("0.00##");
                            String districtName = "",tripId = "",vehicleNo = "",invoiceNo = "",tripDate = "",destination = "",consigneeName = "",productName = "";
                            String departureName = "",gpno = "",totalTonnage = "",distance = "",totalFreightAmount = "";
                            double totalFreight = 0,subTotalFreight = 0;
                            double subTotalTonnage = 0,grandTotalTonnage = 0,finalSubTotalTonnage = 0;
                            String className = "text2",existTripDate = "",existDistrictName = "";
                            int index = 0;
                            int sno = 0;
                            %>
                            <table  border="1" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" id="bg">
                            <c:if test = "${districtSummaryList != null}">
            <%
                            ArrayList districtSummaryList = (ArrayList) request.getAttribute("districtSummaryList");
                           Iterator summaryItr = districtSummaryList.iterator();
                           ReportTO repTO = null;
                            if (districtSummaryList.size() != 0) {
            %>
                 <tr>
                    <td class="contentsub" height="30">S.No</td>
                    <td class="contentsub" height="30">Trip Date</td>
                    <td class="contentsub" height="30">District</td>
                    <td class="contentsub" height="30">Destination</td>
                    <td class="contentsub" height="30">Distance</td>
                    <td class="contentsub" height="30">Total Tonnage</td>
                    <td class="contentsub" height="30">CCC Freight </td>
<!--                    <td class="contentsub" height="140">Crossing</td>-->
                </tr>
                <%
                while (summaryItr.hasNext()) {
                    index++;
                    sno++;

                    repTO = new ReportTO();
                    repTO = (ReportTO) summaryItr.next();

                    tripDate  = repTO.getTripDate();
                    if(tripDate == null){
                        tripDate = "";
                        }
                    districtName  = repTO.getDistrict();
                    if(districtName == null){
                        districtName = "";
                        }
                    destination  = repTO.getDestination();
                    if(destination == null){
                        destination = "";
                        }
                    distance  = repTO.getDistance();
                    if(distance == null){
                        distance = "";
                        }
                    totalTonnage  = repTO.getTotalTonnage();
                    if(totalTonnage == null){
                        totalTonnage = "";
                        }
                    grandTotalTonnage += Double.parseDouble(totalTonnage);
                    totalFreightAmount  = repTO.getTotalFreightAmount();
                    if(totalFreightAmount == null){
                        totalFreightAmount = "";
                        }
                    totalFreight += Double.parseDouble(totalFreightAmount);


                        if(existDistrictName !=""){
                    if(!existDistrictName.equalsIgnoreCase(districtName)){
                        if(className.equalsIgnoreCase("text1")){
                            className = "text2";
                            }else{
                            className = "text1";
                            }
                            index++;
                        %>
                        <tr>
                            <td class="<%=className%>"  height="30" colspan="4" >&nbsp;</td>
                            <td class="<%=className%>" align="right"  height="30"><b>Sub Total</b></td>
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(subTotalTonnage)%></b></td>
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(subTotalFreight)%></b></td>

                        </tr>
                        <%

                        subTotalTonnage = 0;
                        subTotalFreight = 0;

                    }
                    }
                    subTotalTonnage += Double.parseDouble(totalTonnage);
                    subTotalFreight += Double.parseDouble(totalFreightAmount);
                    if(index%2 == 0){
                        className = "text2";

                        }else {
                        className = "text1";
                    }
                    %>
                        <tr>
                          <td class="<%=className%>"  height="30"><%=sno%></td>
                          <td class="<%=className%>"  height="30"><%=tripDate%></td>
                          <td class="<%=className%>"  height="30"><%=districtName%></td>
                          <td class="<%=className%>"  height="30"><%=destination%></td>
                          <td class="<%=className%>" align="right"  height="30"><%=distance%></td>
                          <td class="<%=className%>" align="right"  height="30"><%=totalTonnage%></td>
                          <td class="<%=className%>" align="right"  height="30"><%=totalFreightAmount%></td>
                        </tr>

                <%
                 existDistrictName = districtName;
                    } %>
                         <tr>
                            <td class="<%=className%>"  height="30" colspan="4" >&nbsp;</td>
                            <td class="<%=className%>" align="right"  height="30"><b>Sub Total</b></td>
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(subTotalTonnage)%></b></td>
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(subTotalFreight)%></b></td>
                        </tr>
                        <%
                        subTotalTonnage = 0;
                        subTotalFreight = 0;

%>
                         <tr>
                            <td class="text2"  height="30" colspan="4" >&nbsp;</td>
                            <td class="text2" align="right"  height="30"><b>Grand Total</b></td>
                            <td class="text2" align="right"  height="30"><b><%=df.format(grandTotalTonnage)%></b></td>
                            <td class="text2" align="right"  height="30"><b><%=df.format(totalFreight)%></b></td>
                        </tr>
                        <%


            }
                        
 %>

                   
                            
                </c:if>
                <c:if test = "${districtDetailsList != null}" >
                    <tr>
                    <td class="contentsub" height="30">S.No</td>
                    <td class="contentsub" height="30" width="150">District Name</td>
                    <td class="contentsub" height="30" width="80">Trip Id</td>
                    <td class="contentsub" height="30" width="80" >Trip Date</td>
                    <td class="contentsub" height="30" width="80" >Vehicle No</td>
                    <td class="contentsub" height="30" width="80" >Invoice No</td>
                    <td class="contentsub" height="30" width="150">Destination</td>
                    <td class="contentsub" height="30" width="200">Consignee Name</td>
                    <td class="contentsub" height="30" width="200">Material</td>
<!--                    <td class="contentsub" height="30">Departure Time</td>-->
                    <td class="contentsub" height="30" width="150">GP No</td>
                    <td class="contentsub" height="30" width="150">Total Tonnage</td>
                    <td class="contentsub" height="30" idth="150">CCC Freight </td>
<!--                    <td class="contentsub" height="140">Crossing</td>-->
                </tr>
                <%
                index = 0;
                    ArrayList districtDetailsList = (ArrayList) request.getAttribute("districtDetailsList");
                    Iterator detailsItr = districtDetailsList.iterator();
                    ReportTO reportTO = null;

                   if (districtDetailsList.size() != 0) {
                    while (detailsItr.hasNext()) {
                    sno++;
                    reportTO = new ReportTO();
                    reportTO = (ReportTO) detailsItr.next();
                    districtName  = reportTO.getDistrict();
                    if(districtName == null){
                        districtName = "";
                        }
                    tripId  = reportTO.getTripId();
                    if(tripId == null){
                        tripId = "";
                        }
                    tripDate  = reportTO.getTripDate();
                    if(tripDate == null){
                        tripDate = "";
                        }
                    vehicleNo  = reportTO.getVehicleId();
                    if(vehicleNo == null){
                        vehicleNo = "";
                        }
                    invoiceNo  = reportTO.getInvoiceNo();
                    if(invoiceNo == null){
                        invoiceNo = "";
                        }
                    destination  = reportTO.getDestination();
                    if(destination == null){
                        destination = "";
                        }
                    consigneeName  = reportTO.getConsigneeName();
                    if(consigneeName == null){
                        consigneeName = "";
                        }
                    gpno  = reportTO.getGpno();
                    if(gpno == null){
                        gpno = "";
                        }
                    productName  = reportTO.getProductName();
                    if(productName == null){
                        productName = "";
                        }
                    totalTonnage  = reportTO.getTotalTonnage()  ;
                    if(totalTonnage == null){
                        totalTonnage = "";
                        }

                    grandTotalTonnage += Double.parseDouble(totalTonnage);
                    totalFreightAmount  = reportTO.getTotalFreightAmount();
                    if(totalFreightAmount == null){
                        totalFreightAmount = "";
                        }
                    //subTotalFreight += Double.parseDouble(totalFreightAmount);
                    totalFreight += Double.parseDouble(totalFreightAmount);

                    if(existDistrictName !=""){
                    if(!existDistrictName.equalsIgnoreCase(districtName)){
                        //System.out.println("className is:::"+className);
                        if(className.equalsIgnoreCase("text1")){
                            className = "text2";
                            }else{
                            className = "text1";
                            }
                            index++;

                        %>
                        <tr>
                            <td class="<%=className%>"  height="30" colspan="8" >&nbsp;</td>
                            <td class="<%=className%>"  height="30" colspan="2" align="right" ><b><%=existDistrictName%>&nbsp;Dist Total</b></td>
                            <td class="<%=className%>" align="right"   height="30">&nbsp;<b><%=df.format(subTotalTonnage)%></b></td>
                            <td class="<%=className%>" align="right"   height="30">&nbsp;<b><%=df.format(subTotalFreight)%></b></td>
                        </tr>
                        <%
                        subTotalTonnage = 0;
                        subTotalFreight = 0;

                    }
                    }

                            index++;
                            if(index%2 == 0){
                            className = "text2";

                            }else {
                            className = "text1";
                            }%>
                <tr>
                <td class="<%=className%>"  height="30"><%=sno%></td>
                <td class="<%=className%>"  height="30"><%=districtName%></td>
                <td class="<%=className%>"  height="30"><%=tripId%></td>
                <td class="<%=className%>"  height="30"><%=tripDate%></td>
                <td class="<%=className%>"  height="30"><%=vehicleNo%></td>
                <td class="<%=className%>"  height="30"><%=invoiceNo%></td>
                <td class="<%=className%>"  height="30"><%=destination%></td>
                <td class="<%=className%>"  height="30"><%=consigneeName%></td>
                <td class="<%=className%>"  height="30"><%=productName%></td>
                <td class="<%=className%>"  height="30"><%=gpno%></td>
                <td class="<%=className%>" align="right"  height="30"><%=totalTonnage%></td>
                <td class="<%=className%>" align="right"  height="30"><%=totalFreightAmount%></td>
                </tr>
                <%
                    subTotalTonnage += Double.parseDouble(totalTonnage);
                    subTotalFreight += Double.parseDouble(totalFreightAmount);
                        existDistrictName = districtName;

                       }%>
                        <tr>
                            <td class="<%=className%>"  height="30" colspan="8" >&nbsp;</td>
                            <td class="<%=className%>"  height="30" colspan="2" align="right" ><b><%=existDistrictName%>&nbsp;Dist Total</b></td>
                            <td class="<%=className%>" align="right"  height="30">&nbsp;<b><%=df.format(subTotalTonnage)%></b></td>
                            <td class="<%=className%>" align="right"  height="30">&nbsp;<b><%=df.format(subTotalFreight)%></b></td>
                        </tr>
                     <tr>
                            <td class="text2"  height="30" colspan="8" >&nbsp;</td>
                            <td class="text2"  height="30" colspan="2" align="right" ><b>Grand Total</b></td>
                            <td class="text2" align="right"  height="30"><b>&nbsp;<%=df.format(grandTotalTonnage)%></b></td>
                            <td class="text2" align="right"  height="30"><b>&nbsp;<%=df.format(totalFreight)%></b></td>

                        </tr>
                        <%
                        }%>
                </c:if>
                </table>

        </form>
    </body>
</html>
