<%-- 
    Document   : displyLpsReport
    Created on : Jan 4, 2012, 6:09:09 PM
    Author     : Entitle
--%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->

        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
         <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="ets.domain.security.business.SecurityTO" %>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script src="/throttle/js/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script src="/throttle/js/TableSort.js" language="javascript" type="text/javascript"></script>
        <link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />

        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    </head>
    <script type="text/javascript">

        function submitPage(value)
        {

            if(textValidation(document.saveInvoice.remark,'Remark')){
                return;
            }
            document.saveInvoice.action="/throttle/saveInvoice.do";
            document.saveInvoice.submit();
        }

    </script>

    <%
                String lpsId = (String) request.getAttribute("lpsId");
                String fromDate = (String) request.getAttribute("fromDate");
                String toDate = (String) request.getAttribute("toDate");



    %>

    <body>
        <form name="saveInvoice" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br/>
            <table cellpadding="0" cellspacing="2" align="center" border="0" width="700" id="report" bgcolor="#97caff" style="margin-top:0px;">
                <tr>
                    <td><b>LPS Wise Detail</b></td>
                    <td align="right"><span id="openClose" onclick="displayCollapse();" style="cursor: pointer;">Close</span>&nbsp;</td>
                </tr>
                <tr id="exp_table"  style="display: block;">
                    <td colspan="2" style="padding:15px;" align="right">
                        <div class="tabs" align="center" style="width:900px">
                            <table width="100%" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                <tr>
                                    <td  height="30">From Date :</td>
                                    <td  height="30"><input type="text" name="fromDate" id="fromDate" value="<%=fromDate%>" />  </td>

                                    <td  height="30">To Date : </td>
                                    <td  height="30"><input type="text" name="toDate" id="toDate" value="<%=toDate%>" /></td>
                                </tr>
                                <tr>
                                    <td  height="30">LPS No :   </td>
                                    <td  height="30"><input type="text" name="lpsId" id="lpsId" value="<%=lpsId%>"/></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <table cellpadding="0" cellspacing="2" align="center" border="0" width="700" id="report" bgcolor="#97caff" style="margin-top:0px;">

                <c:if test= "${lpsCount != null}"  >
                    <c:forEach items="${lpsCount}" var="lc" >
                        <c:set var="lpsCount" value="${lc.lpsCount}" />
                    </c:forEach>
                </c:if>
                <c:if test= "${lpsTrippedList != null}"  >
                    <c:forEach items="${lpsTrippedList}" var="ltl" >
                        <c:set var="trippedLPS" value="${ltl.trippedLPS}" />
                    </c:forEach>
                </c:if>
                <c:set var="sum" value="${lpsCount -trippedLPS}" />
                <tr>
                    <td width="25%" height="30">
                        <input type="hidden" id="noOfTrip" name="noOfTrip" value="<c:out value="${noOfTrip}"/>"/>
                        <input type="hidden" id="taxAmount" name="taxAmount" value="<c:out value="${taxAmount}"/>"/>
                        <input type="hidden" id="lpsCount" name="lpsCount" value="<c:out value="${lpsCount}"/>"/>
                        <b>Number Of LPS : </b></td>
                        <td align="left"><b><c:out value="${lpsCount}"/></b></td>
                    <td width="25%">
                        <input type="hidden" id="trippedLPS" name="trippedLPS" value="<c:out value="${trippedLPS}"/>"/>
                        <b>Tripped LPS  : </b></td>
                    <td width="25%" align="left" ><b><c:out value="${trippedLPS}"/></b></td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td width="25%"><b>Balance LPS  : </b></td>
                    <td width="25%" align="left" ><b><c:out value="${sum}"/></b></td>
                    
                </tr>
            </table>
            <br/>
            <c:if test = "${tripList != null}" >
                <%
                            int index = 0;
                            ArrayList tripList = (ArrayList) request.getAttribute("tripList");
                            if (tripList.size() != 0) {
                %>

                <table  border="0" class="border" align="center" width="700" cellpadding="0" cellspacing="0" id="bg">
                    <tr>
                        <td class="contentsub" height="30">Trip Id</td>
                        <td class="contentsub" height="30">LPS Id</td>
                        <td class="contentsub" height="30">Trip Date</td>
                        <td class="contentsub" height="140">Vehicle No</td>
                        <td class="contentsub" height="30">Route Name</td>
                        <td class="contentsub" height="30">Driver Name</td>
                        <td class="contentsub" height="30">Customer Name</td>
                        <td class="contentsub" height="30">Total Amount</td>
                        <td class="contentsub" height="30">Status</td>

                    </tr>
                    <c:forEach items="${tripList}" var="tripDetails">
                        <c:set var="total" value="${total+1}"></c:set>
                        <c:set var="totalAmo" value="${total + tripDetails.totalamount}" />

                        <%
                                                        String classText = "";
                                                        int oddEven = index % 2;
                                                        if (oddEven > 0) {
                                                            classText = "text2";
                                                        } else {
                                                            classText = "text1";
                                                        }
                        %>
                        <tr>
                            <td class="<%=classText%>"  height="30"><c:out value="${tripDetails.tripId}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${tripDetails.orderNo}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${tripDetails.tripDate}"/></td>
                            <td class="<%=classText%>"  height="140"><c:out value="${tripDetails.regno}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${tripDetails.routeName}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${tripDetails.empName}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${tripDetails.custName}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${tripDetails.totalamount}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${tripDetails.status}"/></td>
                        </tr>

                        <%index++;%>
                    </c:forEach>
                </table>
                <%
                            }
                %>
            </c:if>
        </form>

    </body>

</html>