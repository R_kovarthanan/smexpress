<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<title>JobCard</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ page import="ets.domain.operation.business.OperationTO" %> 
        <%@ page import="ets.domain.mrs.business.MrsTO" %> 
        <%@ page import="java.util.*" %> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
</head>


        <script>
            
            
            
            function print(ind)
            {       
                var DocumentContainer = document.getElementById('print'+ind);
                var WindowObject = window.open('', "TrackHistoryData", 
                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                //WindowObject.close();   
            }            
            
            
        </script>

<body>
<form name="mpr"  method="post" >                        
            <br>
            
            
            <%
            int index = 0;
            int sno = 0;

            int service = 0;
            ArrayList cardDetail = new ArrayList();
            cardDetail = (ArrayList) request.getAttribute("DetailsList");
            String[] problemNames,symptoms;
            String techTemp[] = null;
            String techNames = "";
            OperationTO operationTO = new OperationTO();

            int jlistSize = 36;
            int secNameLimit = 30;
            int probLimit = 30;

            System.out.println("jobcard in jsp=" + cardDetail.size());

            String secName = "";
            String probName = "";




            int cardDet = cardDetail.size();
            if (cardDet != 0) {
                for (int i = 0; i < cardDet; i = i + jlistSize) {
            %>
<div id="print<%= i %>">

<style type="text/css">
table {
font:normal 12px arial;
}

h1 {
font-size:18px;
font-weight:normal;
padding:0px;
margin:0px;
}

.dashed {
border:1px dashed #000;
border-bottom-style:dashed;
border-top-style:none;
border-right-style:none;
border-left-style:none;
}
.line {
border:1px solid #000;
border-bottom-style:solid;
border-top-style:none;
border-right-style:none;
border-left-style:none;
}

.rgline {
border:1px solid #000;
border-bottom-style:solid;
border-top-style:none;
border-right-style:solid;
border-left-style:none;
}
</style>
<table width="650" height="785" cellpadding="0" cellspacing="0" border="0" align="center">
<tr>
<td height="85"><!-- header logo print empty sapce --></td>
</tr>
<tr>
<td align="center" height="35">
					<!-- Address Print -->
					address 1,
					address 2. 

</td>
</tr>
		<%
		
		ArrayList details = new ArrayList();
		details = (ArrayList) request.getAttribute("details");
		OperationTO opTO = new OperationTO();
		OperationTO headContent = new OperationTO();
		headContent = (OperationTO) details.get(0);
		%>

<tr>
<td valign="top" height="30">
					<!-- job card &amp; reg no -->
					<table width="630"  cellpadding="0" cellspacing="0" border="0" align="center" style="padding:5px; border:1px solid #000; margin-top:5px; ">
					<tr>
					<td width="224" height="30" align="left"><h1>BAY NO:<%=headContent.getBayNo()%></h1></td>
					<td width="300" height="30" align="center"><h1>JOB CARD:<%=request.getAttribute("jcMYFormatNo")%></h1></td>
					<td width="176" align="right"><h1> <%= headContent.getRegno() %></h1></td>
					</tr>
                                        <tr>

                                        <td width="224" height="30" align="left"><h1><%=headContent.getPriority1()%></h1></td>
					<td width="300" height="30" align="center"><h1><%=headContent.getCompName()%></h1></td>
					<td width="176" align="right"><h1> <%= headContent.getServicetypeName() %></h1></td>

                                        </tr>
					</table>


</td>
</tr>

<tr>
<td valign="top" height="75">
					<!-- Customer Details -->
					<table width="630"  cellpadding="0" cellspacing="0" border="0" align="center" style="padding:5px; border:1px solid #000; margin-top:5px; ">
					<tr>
					<td width="120" height="20" valign="bottom"><b>Customer Name </b>:</td>
					<td width="187" height="20" class="dashed"><%= headContent.getCustName() %></td>
					<td width="100" height="20"><b>Date</b> :</td>
					<td width="147" height="20" class="dashed"> <%=headContent.getCreatedDate()%></td>
					</tr>
					<tr>
					<td width="120" height="20" valign="bottom"><b>Customer Address </b>:</td>
					<td width="187" class="dashed">
                                            <%= headContent.getCustAddress() %>
                                            <%= headContent.getCustCity() %>
                                            <%= headContent.getCustState() %>
                                            Phone:<%= headContent.getCustPhone() %>
                                            Mobile:<%= headContent.getCustMobile() %>
                                            Email<%= headContent.getCustEmail() %>
                                        </td>
					<td width="100" height="20" valign="bottom"><b>In Time </b>:</td>
					<td width="147" class="dashed" valign="bottom"><%= headContent.getIntime() %></td>
					</tr>
					<tr>
					<td width="120" height="30" valign="bottom"><b>Operation Area </b>:</td>
					<td width="187"  valign="bottom"><%= headContent.getUsageName() %> </td>
					<td width="100" height="30" valign="bottom">&nbsp;</td>
                                        <td width="147" >&nbsp;</td>
					</tr>
					</table>
</td>
</tr>
<tr>
<td valign="top" >
					<!-- vehicle details -->
					<table width="630"  cellpadding="0" cellspacing="0" border="0" align="center" style="padding:0px; border:1px solid #000; margin-top:15px; ">
					<tr >
					<td colspan="2" class="rgline" align="left" height="35" style="padding-left:5px; " ><h1>Vehicle Details</h1></td>
					<td colspan="2" class="line"  align="left" style="padding-left:5px; "><h1>Job Card Details</h1></td>
					</tr>
					<tr >
					<td width="110" height="20" class="line" style="padding-left:5px; " >Model No:</td>
					<td width="165" height="20" class="rgline" style="padding-left:5px; "><b><%= headContent.getMfrName() %>-<%= headContent.getModelName() %></b></td>
					<td width="100" height="20" class="line" style="padding-left:5px; ">Current KM :</td>
					<td width="175" height="20" class="line" style="padding-left:5px; "><b><%= request.getAttribute("km") %> KM</b></td>
					</tr>
					<tr >
					<td width="110" height="20" class="line" style="padding-left:5px; " >Registration No:</td>
					<td width="165" height="20" class="rgline" style="padding-left:5px; "><b><%= headContent.getRegno() %></b></td>
					<td width="100" height="20" class="line" style="padding-left:5px; ">Total KM :</td>
					<td width="175" height="20" class="line" style="padding-left:5px; "><b><%= headContent.getTotalKm() %> KM</b></td>
					</tr>
					<tr >
					<td width="110" height="20" class="line" style="padding-left:5px; " >Registration Date:</td>
					<td width="165" height="20" class="rgline" style="padding-left:5px; "><b><%= headContent.getSaleDate() %></b></td>
					<td width="100" height="20" class="line" style="padding-left:5px; ">Service Type:</td>
					<td width="175" height="20" class="line" style="padding-left:5px; "><b><%= headContent.getServicetypeName() %></b></td>
					</tr>
					<tr >
					<td width="110" height="20" class="line" style="padding-left:5px; " >Chassis No:</td>
					<td width="165" height="20" class="rgline" style="padding-left:5px; "><b><%= headContent.getChassNo() %></b></td>
					<td width="100" height="20" class="line" style="padding-left:5px; ">Driver :</td>
					<td width="175" height="20" class="line" style="padding-left:5px; "><b>Mr.<%= headContent.getDriverName() %></b></td>
					</tr>
					<tr >
					<td width="110" height="20" class="line" style="padding-left:5px; " >Engine No:</td>
					<td width="165" height="20" class="rgline" style="padding-left:5px; "><b><%= headContent.getEngineNo() %></b></td>
					<td width="100" height="20" class="line" style="padding-left:5px; ">EST Delivery :</td>
                                        <td width="175" height="20" class="line" style="padding-left:5px; "><b><%= headContent.getScheduledDate() %></b>&nbsp;</td>
					</tr>
					<tr>
                                        <td width="110" height="20" class="line" style="padding-left:5px; " >&nbsp;</td>
					<td width="165" height="20" class="rgline" style="padding-left:5px; ">&nbsp;</td>
					<td width="100" height="20" class="line" style="padding-left:5px; ">Work Area :</td>
                                        <td width="175" height="20" class="line" style="padding-left:5px; "><b><%= headContent.getServiceLocation() %></b>&nbsp;</td>
					</tr>
					<tr>
					<td colspan="4" height="30" valign="top" style="padding:5px; ">
					<b>Job Card Creator:</b> Mr.<%=request.getAttribute("jobCardUser")%>
					<p></p>
					</td>
					</tr>
					</table>
</td>
</tr>
<tr>
<td valign="top">
					<!-- complaints -->
					<table width="630"  cellpadding="0" cellspacing="0" border="0" align="center" style="padding:0px; border:1px solid #000; margin-top:15px; ">
					<tr >
					<td colspan="4" class="line" align="left" height="35" style="padding-left:5px; " ><h1>Complaints</h1></td>
					</tr>
					<tr >
					<td width="40" height="20" class="rgline" style="padding-left:5px; " ><b>S.No</b></td>
					<td width="205" height="20" class="rgline" align="center" style="padding-left:5px; "><b>Complaints</b></td>
					<td width="205" height="20" class="rgline" align="center" style="padding-left:5px; "><b>Descriptions</b></td>
					<td width="205" height="20" class="rgline" align="center" style="padding-left:5px; "><b>Technician</b></td>
					</tr>
					 <%
                                    for (int j = i; (j < cardDetail.size() ) && (j < jlistSize + i) ; j++) {
                                        operationTO = new OperationTO();
                                        operationTO = (OperationTO) cardDetail.get(j);
                                        secName = operationTO.getSecName();
                                        problemNames = operationTO.getProblemNameSplit();
                                        symptoms = operationTO.getSymptomsSplit();
                                        techNames = operationTO.getTechnicianName();
                                        System.out.println("techNames:"+techNames);
                                        if(!"".equals(techNames) && techNames != null) {
                                            techTemp = techNames.split("~");
                                            techNames = techTemp[1];
                                        }else {
                                            techNames = "Not Assigned";
                                        }
                                        if (operationTO.getSecName().length() > secNameLimit) {
                                            secName = secName.substring(0, secNameLimit - 1);
                                        }

                                        if( operationTO.getProbName().equals("WORKORDERCOMPLAINT") ){ %>
                                <tr>
                                    <Td valign="top" colspan="4" width="750" align="left" HEIGHT="20" class="line" style="border:1px; padding-left :6px;"   > <STRONG>IDENTIFIED COMPLAINTS </STRONG> </td>
                                </tr>                                                                                                                                  
                                        
                                        <% }else if( operationTO.getProbName().equals("JOBCARDCOMPLAINT") ){ %>
                                <tr>
                                    <Td  colspan="4" align="left"  width="750"  HEIGHT="20"  class="line" style="border:1px;  padding-left :6px;"  > <STRONG> CUSTOMER COMPLAINTS </STRONG> </td>
                                </tr>                                                                                                                                                                  
                                        <% }else if( operationTO.getProbName().equals("PERIODICSERVICE") ){ %>
                                <tr>
                                    <Td valign="top" colspan="4" align="left"  width="750"  HEIGHT="20" class="line"  style="border:1px;  padding-left :6px;"  ><STRONG>PERIODIC SERVICES </STRONG> </td>
                                </tr>    
                                <% }else if( operationTO.getProbName() != "" ){ %>
					
					
					<tr >
					<td width="40" height="20" class="rgline" style="padding-left:5px; " ><%= sno %></td>
					<td width="205" height="20" class="rgline" align="left" style="padding-left:5px; ">
					<% for(int k=0; k<problemNames.length; k++ ) {%>
                                      <%= problemNames[k] %>  <br>
                    
					
					                <%}%>  
					</td>

					<Td valign="top" width="205"  HEIGHT="20" class="line" style="border:1px;  border-right-style:solid;  padding-left :6px;" align="LEFT">
					
					<% for(int m=0; m<symptoms.length; m++ ){%>
					  <%= symptoms[m] %>  <br>
					<%}%>
					
					</td>                                       
                                    <%--
                                    <%

                                       ArrayList technician = new ArrayList();
                                       int cntr = 0;
                                       technician = (ArrayList) request.getAttribute("technicians");
                                       MrsTO mrstechTO = new MrsTO();
                                       if (operationTO.getEmpId() != 1 && operationTO.getEmpId() != 0) {

                                           for (int k = 0; k < technician.size(); k++) {
                                               mrstechTO = (MrsTO) technician.get(k);
                                               if (mrstechTO.getEmpId() == operationTO.getEmpId() && cntr ==0 ) {
                                    %>              
                                    <Td valign="top" HEIGHT="20" class="line"    border-left-style:solid; padding-left :6px;  " align="left"><%=techNames%>&nbsp;</td>
                                            <%  cntr++;  %>
                                    <%  }
                                        }
                                           cntr=0;
                                    } else if (operationTO.getEmpId() == 1) {
                                    %>     
                                    <Td valign="top" HEIGHT="20"  class="line"    border-left-style:solid; padding-left :6px; " align="left">External Technician</td>
                                    
                                    <%} else if (operationTO.getEmpId() == 0) {
                                    %>      
                                    <Td valign="top" HEIGHT="20"  class="line"   border-left-style:solid; padding-left :6px; " align="left">Not Assigned</td>
                                    
                                    <% }else {
                                    %>
                                    <Td valign="top" HEIGHT="20" class="line"    style="border-left-style:solid; padding-left :6px;" align="left"><%=techNames%>&nbsp;</td>

                                    <%
                                     }
                                     %>--%>
                                    <Td valign="top" HEIGHT="20" class="line"    style="border-left-style:solid; padding-left :6px;" align="left"><%=techNames%>&nbsp;</td>
                                </tr>


<%  }
                            index++;
                            sno++;
                                    }   %>



                  

					<tr>
					<td colspan="3" height="80" valign="top" style="padding:5px; ">
					<b>Remarks:</b>
                                        <%= headContent.getRemarks() %>
					<p></p>
					</td>
					</tr>
					</table>
</td>
</tr>

</table>
</div>
<table align="center">
<tr>
<td><center>   
                <input type="button" class="button" name="Print" value="Print" onClick="print(<%= i %>)" > &nbsp;        
            </center>
            <br>    
            <br>    
            
              
            <%   }}  %> </td>
</tr>
<tr>
<td ><!-- empty space height --></td>
</tr>
</table>

</body>
</html>
