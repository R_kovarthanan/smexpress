<%--
    Document   : serviceTypewiseData
    Created on : Sep 25, 2010, 3:38:32 PM
    Author     : Admin
--%>
<%--This is the chart processor code which is used to customise the chart genearted by ceowlf tags below --%>


<%--From here its Displaying Fluidity Reports-- --%>

     <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="java.util.*" %>
     </head>


     <body onload="setValues();">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>

<form name="servicewise" method="post" >
<table width='500' align="center" bgcolor='white' border='0' cellpadding='0' cellspacing='0' class='repcontain'>
<tr>
    <td class="text1" > From Date : </td>
    <td class="text1" >
       <input type="text" class="textbox" readonly name="fromDate" value="" >
       <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.servicewise.fromDate,'dd-mm-yyyy',this)"/>
    </td>

    <td class="text2" > To Date : </td>
    <td class="text2" >
        <input type="text" class="textbox" readonly name="toDate" value="" >
        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.servicewise.toDate,'dd-mm-yyyy',this)"/>
    </td>
</tr>
<tr> <td colspan="4" align="center">&nbsp;</td> </tr>
<tr>
    <td colspan="4" align="center">
        <input type="button" class="button" readonly name="search" value="search" onClick="searchSubmit();" > </td>
</tr>

</table>

</form>
<table width='70%' align="center" bgcolor='white' border='0' cellpadding='5' cellspacing='0' class='repcontain'>



   <%-- <c:if test = "${serviceTypewiseSummary != null}" >--%>
        <tr>
            <td class="contentsub">S.No</td>
            <td class="contentsub">Service Types</td>
            <td>
                <table>
                    <tr>
                        <td class="contentsub" align="center" colspan="2">Trip</td>
                    </tr>
                    <tr>
                        <td class="contentsub">Sp.Amt</td>
                        <td class="contentsub">Lab.Amt</td>
                    </tr>
               </table>
            </td>
            <td>
                <table>
                    <tr>
                        <td class="contentsub" align="center">Tour</td>
                    </tr>
                    <tr>
                        <td class="contentsub">Sp.Amt</td>
                        <td class="contentsub">Lab.Amt</td>
                    </tr>
               </table>
            </td>
            <td>
                <table>
                    <tr>
                        <td class="contentsub" align="center">Cargo</td>
                    </tr>
                    <tr>
                        <td class="contentsub">Sp.Amt</td>
                        <td class="contentsub">Lab.Amt</td>
                    </tr>
               </table>
            </td>
            <td>
                <table>
                    <tr>
                        <td class="contentsub" align="center">StaffBus</td>
                    </tr>
                    <tr>
                        <td class="contentsub">Sp.Amt</td>
                        <td class="contentsub">Lab.Amt</td>
                    </tr>
               </table>
            </td>
            <td>
                <table>
                    <tr>
                        <td class="contentsub" align="center">Other</td>
                    </tr>
                    <tr>
                    <td class="contentsub">Sp.Amt</td>
                    <td class="contentsub">Lab.Amt</td>
                    </tr>
               </table>
            </td>
        </tr>

       <%-- <%
					int index = 1 ;
                    %>
    <c:forEach items="${serviceTypewiseSummary}" var="serviceTypewiseSummary">
        <%

            String classText = "";

            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
    <tr>
        <td class="<%=classText %>"><%=index %></td>
        <td class="<%=classText %>"><c:out value="${serviceTypewiseSummary.companyName}"/></td>
        <td class="<%=classText %>"><c:out value="${serviceTypewiseSummary.spareAmount}"/></td>
        <td class="<%=classText %>"><c:out value="${serviceTypewiseSummary.labour}"/></td>
        <td class="<%=classText %>"><c:out value="${serviceTypewiseSummary.noOfVehicles}"/></td>
        <td class="<%=classText %>"><c:out value="${serviceTypewiseSummary.totalAmount}"/></td>






</tr>
<%
            index++;
                        %>

   </c:forEach>
</c:if>
--%>
</table>




</body>

<script>
    function searchSubmit()
    {
        document.servicewise.action="/throttle/serviceTypewisereport.do"
        document.servicewise.submit();
    }

function setValues(){
    if('<%= request.getAttribute("fromDate") %>' != 'null'){
        document.servicewise.fromDate.value='<%= request.getAttribute("fromDate") %>';
    }
    if('<%= request.getAttribute("toDate") %>' != 'null'){
        document.servicewise.toDate.value='<%= request.getAttribute("toDate") %>';
    }
}

</script>





