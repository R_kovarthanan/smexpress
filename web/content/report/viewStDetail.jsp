<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    </head>
    <body>
        <form name="grn" method="post">
            <%@ include file="/content/common/path.jsp" %>           
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <% int index = 0;%> 
            <c:if test = "${stkTransferDetail != null}" >        
                <table align="center" width="300" border="0" cellspacing="0" cellpadding="0">
                    <c:forEach items="${stkTransferDetail}" var="stDetail"> 
                        <% if (index == 0) {%>   
                        <tr>
                            <td class="contentsub" colspan="2" align="center" height="30">Goods Delivery No&nbsp;&nbsp;: &nbsp;&nbsp;<c:out value="${stDetail.gdId}"/></td>
                        </tr>
                        <tr>
                            <td class="text1" width="150"  height="30"> From Service Point </td>
                            <td height="30"  class="text1">
                            <c:if test = "${operationPointList != null}" >
                                <c:forEach items="${operationPointList}" var="Dept">                             
                                    <c:if test = "${Dept.spId==stDetail.fromSpId }" >
                                    <c:out value="${Dept.spName}"/>
                                    </c:if>
                                </c:forEach>
                            </c:if>
                            </td>
                        </tr>
                        <tr>
                            <td class="text2" width="150"  height="30"> To Service Point</td>
                            <td height="30"  class="text2">
                            <c:if test = "${operationPointList != null}" >
                                <c:forEach items="${operationPointList}" var="Dept">                             
                                    <c:if test = "${Dept.spId==stDetail.toSpId }" >
                                    <c:out value="${Dept.spName}"/>
                                    </c:if>
                                </c:forEach>
                            </c:if>                                 
                            </td>
                        </tr>
                        <tr>
                            <td class="text1" width="150"  height="30">Requested Date</td>
                            <td height="30"  class="text1"><c:out value="${stDetail.requiredDate}"/></td>
                        </tr>
                        <tr>
                            <td class="text2" width="150"  height="30"> Issued Date</td>
                            <td height="30"  class="text2"><c:out value="${stDetail.createdDate}"/></td>
                        </tr>
                        <tr>
                            <td class="text1" width="150"  height="30"> Remarks </td>
                            <td height="30"  class="text1"><c:out value="${stDetail.remarks}"/></td>
                        </tr>                        

                        <% index++;
            }%>               
                    </c:forEach>
                </table>
                <br>
                
                <br>
                
        
                <table width="650" border="0" align="center" cellpadding="0" cellspacing="0" id="bg">
                    
                    <tr>
                        <td  height="30" class="contentsub">S No</td>
                        <td  height="30" class="contentsub">MFR Code</td>
                        <td  height="30" class="contentsub">PAPL Code</td>
                        <td  height="30" class="contentsub">Item Name</td>
                        <td  height="30" class="contentsub">Requested Qty</td>                        
                        <td  height="30" class="contentsub">Approved Qty</td>                        
                        <td  height="30" class="contentsub">Issued Qty</td>                        
                        <td  height="30" class="contentsub">MRP(Rs)</td>                        
                        <td  height="30" class="contentsub">Amount(Rs)</td>
                    </tr>
                    <%  index = 0;%> 
                    <c:forEach items="${stkTransferDetail}" var="stDetail"> 
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>	
                        <tr>
                            <td class="<%=classText %>" height="30"><%=index + 1%></td>
                            <td class="<%=classText %>" height="30"><c:out value="${stDetail.mfrCode}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${stDetail.paplCode}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${stDetail.itemName}"/></td>                            
                            <td class="<%=classText %>" height="30"><c:out value="${stDetail.requestedQty}"/></td>                            
                            <td  class="<%=classText %>" height="30"><c:out value="${stDetail.approvedQty}"/></td> 
                            <td  class="<%=classText %>" height="30"><c:out value="${stDetail.issuedQty}"/></td> 
                            <td  class="<%=classText %>" height="30"><c:out value="${stDetail.price}"/></td> 
                            <td  class="<%=classText %>" height="30"><c:out value="${stDetail.amount}"/></td> 
                        </tr>
                        <% index++;%>
                    </c:forEach>
                    
                </table>
            </c:if>
            <br>
            
            
        </form>
    </body>
    
    
</html>
