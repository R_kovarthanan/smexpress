<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="java.util.*" %>
        <SCRIPT LANGUAGE="Javascript" SRC="/throttle/js/FusionCharts.js"></SCRIPT>


    </head>

    <script language="javascript">

function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

       function submitPage(){
           var chek=validation();
           if(chek=='true'){
                document.stkWorth.action='/throttle/stockWorthGraph.do';
                document.stkWorth.submit();
           }
       }
       function validation(){
               if(textValidation(document.stkWorth.fromDate,'From Date')){
                   document.stkWorth.fromDate.focus();
                   return 'false';
               }
               else if(textValidation(document.stkWorth.toDate,'To Date')){
                   document.stkWorth.toDate.focus();
                   return 'false';
               }
               return 'true';
          }
       function setValues(){
           var a='<%=request.getAttribute("fromDate")%>';
           if(a!='null'){

                document.stkWorth.fromDate.value='<%=request.getAttribute("fromDate")%>';
                document.stkWorth.toDate.value='<%=request.getAttribute("toDate")%>';
           }
       }
       function getItemNames(){
        var oTextbox = new AutoSuggestControl(document.getElementById("itemName"),new ListSuggestions("itemName","/throttle/handleItemSuggestions.do?"));
            }

    </script>
    <body onload="getItemNames()">
        <form  name="stkWorth" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
        <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
        </h2></td>
        <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
        </tr>
        <tr id="exp_table" >
        <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
            <div class="tabs" align="left" style="width:850;">
        <ul class="tabNavigation">
                <li style="background:#76b3f1">Sales Bill Trend</li>
        </ul>
        <div id="first">
            <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
             <tr>
            <td height="25">Item Category</td>
            <td height="25">
                <select name="category" style="width:125px"><option value="0">All</option>
                <%if(request.getAttribute("CategoryList")!=null){
                    ArrayList categoryList = (ArrayList)request.getAttribute("CategoryList");
                    Iterator itr = categoryList.iterator();
                    ReportTO repTO = new ReportTO();
                    while(itr.hasNext()){
                        repTO = (ReportTO) itr.next();
                    %>
                    <option value = '<%=repTO.getCategoryId()%>'><%=repTO.getCategoryName()%></option>
                    <%
                    }
                }
                %>
             </select>
            </td>
        <td height="25">Item Name</td>
        <td height="25"><input type="text" value="" name="itemName"/></td>
        <td height="25">Item Code</td>
        <td height="25"><input type="text" value="" name="itemCode"/></td>
        </tr>

    <tr>
        <td><font color="red">*</font>From Date</td>
        <td><input name="fromDate" class="textbox" type="text" value="" size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.stkWorth.fromDate,'dd-mm-yyyy',this)"/></td>
       <td height="25"><font color="red">*</font>To Date</td>
       <td><input name="toDate" class="textbox" type="text" value="" size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.stkWorth.toDate,'dd-mm-yyyy',this)"/></td>
       <td><input type="button" class="button" value="Search" onclick="submitPage();" /></td>
    </tr>
</table>
</div></div>
</td>
</tr>
</table>
            <br>

             <%

            ArrayList stockWorthGraph= (ArrayList) request.getAttribute("stockWorthGraph");
            ArrayList monthNameList= (ArrayList) request.getAttribute("monthNameList");
            ArrayList ColorList= (ArrayList) request.getAttribute("ColorList");
            ArrayList companyNameList= (ArrayList) request.getAttribute("companyNameList");

            if(request.getAttribute("stockWorthGraph")!=null)
                {
            if(stockWorthGraph.size()!=0 ){

            int size = stockWorthGraph.size();
            int companyNameSize = companyNameList.size();
            int monthNameSize = monthNameList.size();

            String  strXML = "<graph caption='Stock Worth'  hovercapbg='FFECAA' hovercapborder='F47E00' formatNumberScale='0' decimalPrecision='0' showvalues='0' numdivlines='3' numVdivlines='0'   rotateNames='0'>";
            String categories="<categories >";
            Iterator monthName=monthNameList.iterator();
            ReportTO monthNameTO=new ReportTO();
            while(monthName.hasNext()){
                  monthNameTO=(ReportTO) monthName.next();
                  String temp=monthNameTO.getMonthName();
                  categories+="<category name='"+temp+"' />";
                 }
            categories+="</categories>";

            String[] dataset= new String[companyNameSize];
            String[][] set= new String[companyNameSize][monthNameSize];
            int i=0;

            Iterator companyName=companyNameList.iterator();
            ReportTO companyNameTO=new ReportTO();
            while(companyName.hasNext()){
                companyNameTO=(ReportTO) companyName.next();
                String tempComp=companyNameTO.getName();
                String tempColour=companyNameTO.getColour();
                dataset[i]="<dataset seriesName='"+tempComp+"' color='"+tempColour+"' anchorBorderColor='"+tempColour+"' anchorBgColor='"+tempColour+"'>" ;

                Iterator stockWorth=stockWorthGraph.iterator();
                ReportTO stockWorthTO=new ReportTO();
                int j=0;

                while(stockWorth.hasNext()){
                stockWorthTO=(ReportTO) stockWorth.next();
                Float tempAmount=stockWorthTO.getAmount();
                String tempCompName=stockWorthTO.getName();
                if(tempComp.equals(tempCompName))
                    {
                    set[i][j]="<set value='"+tempAmount+"' />";

                    j=j+1;
                }
                }


                i=i+1;
            }
            String contentSet="";
           for(int x=0;x<dataset.length;x++){
                    contentSet+=dataset[x];
                    for(int y=0;y<set[1].length;y++)
                        contentSet+=set[x][y];
                    contentSet+="</dataset>";
                    }
                    contentSet+="</graph>";
              strXML+=categories+contentSet;

            %>

<table width='70%' align="center" bgcolor='white' border='0' cellpadding='5' cellspacing='0' class='repcontain'>



    <c:if test = "${stockWorthGraph != null}" >
        <tr>
            <td class="contentsub">S.No</td>
            <td class="contentsub">Company Name</td>
            <td class="contentsub">Month Name</td>
            <td class="contentsub">Total Value</td>
        </tr>
        <%
					int index = 1 ;
                    %>
    <c:forEach items="${stockWorthGraph}" var="stock">
        <%

            String classText = "";

            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
    <tr>
        <td class="<%=classText %>"><%=index %></td>
        <td class="<%=classText %>"><c:out value="${stock.name}"/></td>
        <td class="<%=classText %>"><c:out value="${stock.monthName}"/></td>
        <td class="<%=classText %>"><c:out value="${stock.amount}"/></td>


</tr>
<%
            index++;
                        %>

   </c:forEach>
</c:if>

</table>
<p>
            <table align="center" style="margin-left:10px;" >
            <tr>
                <td >
                 <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                <jsp:param name="chartSWF" value="/throttle/swf/FCF_MSLine.swf" />
                <jsp:param name="strURL" value="" />
                <jsp:param name="strXML" value="<%=strXML %>" />
                <jsp:param name="chartId" value="productSales" />
                <jsp:param name="chartWidth" value="800" />
                <jsp:param name="chartHeight" value="550" />
                <jsp:param name="debugMode" value="false" />
                <jsp:param name="registerWithJS" value="false" />
        </jsp:include>
                </td>
            </tr>
        </table>
    </p>


<%}}%>

        </form>
    </body>
</html>
