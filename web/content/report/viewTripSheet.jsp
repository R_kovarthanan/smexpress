<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script type="text/javascript">
        

        function viewTripDetails(tripId) {
            window.open('/throttle/viewTripSheetDetails.do?tripId='+tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        function viewVehicleDetails(vehicleId) {
            window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

    </script>
    <script type="text/javascript">
        function setValues(){
            if('<%=request.getAttribute("vehicleTypeId")%>' != 'null' ){
                document.getElementById('vehicleTypeId').value= '<%=request.getAttribute("vehicleTypeId")%>';
            }
            if('<%=request.getAttribute("fromDate")%>' != 'null' ){
                document.getElementById('startDateFrom').value= '<%=request.getAttribute("fromDate")%>';
            }
            if('<%=request.getAttribute("toDate")%>' != 'null' ){
                document.getElementById('startDateTo').value= '<%=request.getAttribute("toDate")%>';
            }
            if('<%=request.getAttribute("endDateFrom")%>' != 'null' ){
                document.getElementById('endDateFrom').value= '<%=request.getAttribute("endDateFrom")%>';
            }
            if('<%=request.getAttribute("endDateTo")%>' != 'null' ){
                document.getElementById('endDateTo').value= '<%=request.getAttribute("endDateTo")%>';
            }
            if('<%=request.getAttribute("fleetCenterId")%>' != 'null'){
                document.getElementById('fleetCenterId').value='<%=request.getAttribute("fleetCenterId")%>';
            }
            if('<%=request.getAttribute("vehicleId")%>' != 'null'){
                document.getElementById('vehicleId').value='<%=request.getAttribute("vehicleId")%>';
            }
            if('<%=request.getAttribute("customerId")%>' != 'null' ){
                document.getElementById('customerId').value= '<%=request.getAttribute("customerId")%>';
            }
            if('<%=request.getAttribute("tripStatusId")%>' != 'null' ){
                document.getElementById('tripStatusId').value= '<%=request.getAttribute("tripStatusId")%>';
            }
            if('<%=request.getAttribute("tripStatusIdTo")%>' != 'null' ){
                document.getElementById('tripStatusIdTo').value= '<%=request.getAttribute("tripStatusIdTo")%>';
            }
            if('<%=request.getAttribute("not")%>' != 'null' && '<%=request.getAttribute("not")%>' != ''){
                document.getElementById('not').checked= true;
            }
             if('<%=request.getAttribute("page")%>' !='null'){
                var page = '<%=request.getAttribute("page")%>';
                if(page == 1){
                    submitPage('search');
                }
            }
        }

        function submitPage(val) {
            if(val == 'ExportExcel'){
                document.tripSheet.action = '/throttle/handleViewTripSheetReport.do?param=ExportExcel';
                document.tripSheet.submit();
            }else{
                document.tripSheet.action = '/throttle/handleViewTripSheetReport.do?param=search';
                document.tripSheet.submit();
            }
        }
    </script>

    <style type="text/css">





        .container {width: 960px; margin: 0 auto; overflow: hidden;}
        .content {width:800px; margin:0 auto; padding-top:50px;}
        .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

        /* STOP ANIMATION */



        /* Second Loadin Circle */

        .circle1 {
            background-color: rgba(0,0,0,0);
            border:5px solid rgba(100,183,229,0.9);
            opacity:.9;
            border-left:5px solid rgba(0,0,0,0);
            /*	border-right:5px solid rgba(0,0,0,0);*/
            border-radius:50px;
            /*box-shadow: 0 0 15px #2187e7; */
            /*	box-shadow: 0 0 15px blue;*/
            width:40px;
            height:40px;
            margin:0 auto;
            position:relative;
            top:-50px;
            -moz-animation:spinoffPulse 1s infinite linear;
            -webkit-animation:spinoffPulse 1s infinite linear;
            -ms-animation:spinoffPulse 1s infinite linear;
            -o-animation:spinoffPulse 1s infinite linear;
        }

        @-moz-keyframes spinoffPulse {
            0% { -moz-transform:rotate(0deg); }
        100% { -moz-transform:rotate(360deg);  }
        }
        @-webkit-keyframes spinoffPulse {
            0% { -webkit-transform:rotate(0deg); }
        100% { -webkit-transform:rotate(360deg);  }
        }
        @-ms-keyframes spinoffPulse {
            0% { -ms-transform:rotate(0deg); }
        100% { -ms-transform:rotate(360deg);  }
        }
        @-o-keyframes spinoffPulse {
            0% { -o-transform:rotate(0deg); }
        100% { -o-transform:rotate(360deg);  }
        }



    </style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.View Trip Sheet Report" text="View Trip Sheet Report"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Report" text="Reports"/></a></li>
            <li class=""><spring:message code="hrms.label.View Trip Sheet Report" text=" View Trip Sheet Report"/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body"><body onload="setValues();sorter.size(20);">
    <form name="tripSheet" method="post">
        <%--<%@ include file="/content/common/path.jsp" %>--%>
        <%@include file="/content/common/message.jsp" %>
                            <table class="table table-info mb30 table-hover" style="width:60%">
                                    <thead><tr><th colspan="4">View Trip Sheet Report</th></tr></thead>

                                <tr>
                                    <td width="80">Customer</td>
                                    <td  width="80"> <select name="customerId" id="customerId" class="form-control" style="width:250px;height:40px" style="height:20px; width:122px;" >
                                            <c:if test="${customerList != null}">
                                                <option value="" selected>--Select--</option>
                                                <c:forEach items="${customerList}" var="cust">
                                                    <option value='<c:out value="${cust.customerId}"/>'><c:out value="${cust.customerName}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select>
                                    </td>
                                    <td  width="80" >Vehicle No</td>
                                    <td  width="80"> <select name="vehicleId" id="vehicleId" class="form-control" style="width:250px;height:40px" style="height:20px; width:122px;"" >
                                                             <c:if test="${vehicleList != null}">
                                                                 <option value="" selected>--Select--</option>
                                                                 <c:forEach items="${vehicleList}" var="vehicleList">
                                                                     <option value='<c:out value="${vehicleList.vehicleId}"/>'><c:out value="${vehicleList.regNo}"/></option>
                                                                 </c:forEach>
                                                             </c:if>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="80" >Vehicle Type </td>
                                    <td width="80" > <select name="vehicleTypeId" id="vehicleTypeId" class="form-control" style="width:250px;height:40px" style="height:20px; width:122px;" >
                                            <c:if test="${vehicleTypeList != null}">
                                                <option value="" selected>--Select--</option>
                                                <c:forEach items="${vehicleTypeList}" var="vh">
                                                    <option value='<c:out value="${vh.typeName}"/>'><c:out value="${vh.typeName}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select></td>

                                    <td width="80" >From Status </td>
                                    <td width="80" > <select name="tripStatusId" id="tripStatusId" class="form-control" style="width:250px;height:40px" style="height:20px; width:122px;" >
                                            <c:if test="${statusDetails != null}">
                                                <option value="" selected>--Select--</option>
                                                <c:forEach items="${statusDetails}" var="statusDetails">
                                                    <option value='<c:out value="${statusDetails.statusId}"/>'><c:out value="${statusDetails.statusName}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select></td>

                                </tr>
                                <tr>
                                    <td width="80" >Trip Start From</td>
                                    <td  width="80" ><input type="textbox"   value="" class="datepicker , form-control" style="width:250px;height:40px" name="startDateFrom" id="startDateFrom" ></td>
                                    <td width="80" >Trip Start To</td>
                                    <td  width="80" ><input type="textbox"   value="" class="datepicker , form-control" style="width:250px;height:40px" name="startDateTo" id="startDateTo" ></td>
                                </tr>
                                  <tr style="display:none">
                                    <td width="80" >Trip End Date From</td>
                                    <td  width="80" ><input type="textbox"   value="" class="datepicker , form-control" style="width:250px;height:40px" name="endDateFrom" id="endDateFrom" ></td>
                                    <td width="80" >Trip End Date To</td>
                                    <td  width="80" ><input type="textbox"   value="" class="datepicker , form-control" style="width:250px;height:40px" name="endDateTo" id="endDateTo" ></td>
                                </tr>
                                <tr style="display:none">
                                    <td width="80" >Trip Closed Date From</td>
                                    <td  width="80" ><input type="textbox"   value="" class="datepicker , form-control" style="width:250px;height:40px" name="closedDateFrom" id="closedDateFrom" ></td>
                                    <td width="80" >Trip Closed Date To</td>
                                    <td  width="80" ><input type="textbox"   value="" class="datepicker , form-control" style="width:250px;height:40px" name="closedDateTo" id="closedDateTo" ></td>
                                </tr>
                                <tr style="display:none">
                                    <td width="80" >Trip Settled Date From</td>
                                    <td  width="80" ><input type="textbox"   value="" class="datepicker , form-control" style="width:250px;height:40px" name="settledDateFrom" id="settledDateFrom" ></td>
                                    <td width="80" >Trip Settled Date To</td>
                                    <td  width="80" ><input type="textbox"   value="" class="datepicker , form-control" style="width:250px;height:40px" name="settledDateTo" id="settledDateTo" ></td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <input type="button"   value="Search"  class="btn btn-success" name="Search" onclick="submitPage('Search');" >
                                        &nbsp;&nbsp;<input type="button"   value="Export Excel"  class="btn btn-success" name="ExportExcel" onclick="submitPage('ExportExcel');" >
                                    </td>
                                </tr>

                            </table>
                        

        <c:if test = "${tripDetails == null}" >
            <center>
                <font color="blue">Please Wait Your Request is Processing</font>
                <div class="container">
                    <div class="content">
                        <div class="circle"></div>
                        <div class="circle1"></div>
                    </div>
                </div>
            </center>
        </c:if>
        <c:if test="${tripDetails != null}">
            <table class="table table-info mb30 table-hover" id="table"  >
                <thead>
                    <tr height="70">
                        <th><h3>Sno</h3></th>
                        <th><h3>Trip Code</h3></th>
                        <th><h3>Trip Status</h3></th>
                        <th><h3>Consignment No</h3></th>
                        <th><h3>Vehicle No </h3></th>                        
                        <th><h3>Customer Name </h3></th>
                        <th><h3>Route </h3></th>
                        <th><h3>Vehicle Type </h3></th>
                        <th><h3>Trip Scheduled</h3></th>
                        <th><h3>Loading Date Time</h3></th>
                        <th><h3>Start Date Time</h3></th>
                        <th><h3>Unloading Date Time</h3></th>
                        <th><h3>Planned End Date Time</h3></th>
                        <th><h3>End Date Time</h3></th>                        
                    </tr>
                </thead>
                <tbody>
                    <% int index = 1;%>
                    <c:forEach items="${tripDetails}" var="tripDetails">
                        <%
                                    String className = "text1";
                                    if ((index % 2) == 0) {
                                        className = "text1";
                                    } else {
                                        className = "text2";
                                    }
                        %>
                        <tr height="30">
                            <td class="<%=className%>" width="40" align="left"><%=index%></td>
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.tripCode}"/></td>
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.status}"/></td>
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.cNotes}"/></td>
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.vehicleNo}"/></td>                                                       
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.customerName}"/></td>
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.routeInfo}"/></td>                            
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.vehicleTypeName}"/></td>
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.plannedStartDateTime}"/></td>
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.loadingDateTime}"/></td>
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.actualStartDateTime}"/></td>                            
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.unLoadingDate}"/></td>
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.plannedEndDateTime}"/></td>
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.actualEndDateTime}"/></td>                            
                        </tr>

                        <%index++;%>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>
        <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" >5</option>
                    <option value="10">10</option>
                    <option value="20" selected="selected">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>
    </form>
</body>
</div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>