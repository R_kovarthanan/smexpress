<%-- 
    Document   : displayConsigneeWiseExcel
    Created on : Jun 12, 2013, 12:28:21 AM
    Author     : Entitle
--%>

<%@page import="java.text.SimpleDateFormat"%>
<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
         <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }

        </style>
    </head>
    <body>
        <form name="consigneewise" action=""  method="post">
        <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "Consignee_Wise_Report-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>
            <br/>
             <%
                            DecimalFormat df = new DecimalFormat("0.00##");
                            String tripId = "",tripDate = "",destination = "",vehicleNumber = "",productName = "",crossing = "";
                            String invoiceNo = "",gpno = "",totalTonnage = "",distance = "",totalFreightAmount = "",consigneeName = "";
                            double totalFreight = 0,subTotalFreight = 0,totalSettlement = 0;
                            double grandTotalTonnage = 0,subTotalTonnage = 0;
                            String className = "text2",settlementAmount = "",existConsigneeName = "";
                            int index = 0;
                            int sno = 0;
%>
<br>
                        <table  border="1" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" id="bg">
                                <c:if test = "${consigneeWiseDetailsList != null}" >
                                    <%
                                    ArrayList consigneeWiseDetailsList = (ArrayList) request.getAttribute("consigneeWiseDetailsList");
                                        Iterator detailsItr = consigneeWiseDetailsList.iterator();
                                        ReportTO repTO = null;
                                        if (consigneeWiseDetailsList.size() != 0) {
                                            %>
                                <tr>
                                <td class="contentsub" height="30">S.No</td>
                                <td class="contentsub" height="30">Consignee Name</td>
                                <td class="contentsub" height="30">Trip Id</td>
                                <td class="contentsub" height="30">Trip Date</td>
                                <td class="contentsub" height="30">Destination</td>
                                <td class="contentsub" height="30">Vehicle No</td>
                                <td class="contentsub" height="30">GP No</td>
                                <td class="contentsub" height="30">Invoice No</td>
                                <td class="contentsub" height="30">Total Tonnage</td>
                                <td class="contentsub" height="30">Freight Amount</td>
                                                </tr>
                                <%
                                while (detailsItr.hasNext()) {
                                        index++;
                                        sno++;


                                        repTO = new ReportTO();
                                        repTO = (ReportTO) detailsItr.next();
                                        consigneeName  = repTO.getConsigneeName();
                                        if(consigneeName == null){
                                            consigneeName = "";
                                            }
                                        tripId  = repTO.getTripId();
                                        if(tripId == null){
                                            tripId = "";
                                            }
                                        tripDate  = repTO.getTripDate();
                                        if(tripDate == null){
                                            tripDate = "";
                                            }
                                        vehicleNumber  = repTO.getVehicleId();
                                        if(vehicleNumber == null){
                                            vehicleNumber = "";
                                            }
                                        gpno  = repTO.getGpno();
                                        if(gpno == null){
                                            gpno = "";
                                            }

                                        invoiceNo  = repTO.getInvoiceNo();
                                        if(invoiceNo == null){
                                            invoiceNo = "";
                                            }
                                        destination  = repTO.getDestination();
                                        if(destination == null){
                                            destination = "";
                                            }
                                        totalTonnage  = repTO.getTotalTonnage();

                                        if(totalTonnage == null){
                                            totalTonnage = "";
                                            }
                                        grandTotalTonnage += Double.parseDouble(totalTonnage);
                                        totalFreightAmount  = repTO.getTotalFreightAmount();
                                        if(totalFreightAmount == null){
                                            totalFreightAmount = "";
                                            }

                                        totalFreight += Double.parseDouble(totalFreightAmount);

                                    if(existConsigneeName !=""){
                                if(!existConsigneeName.equalsIgnoreCase(consigneeName)){
                                    if(className.equalsIgnoreCase("text1")){
                                        className = "text2";
                                        }else{
                                        className = "text1";
                                        }
                                        index++;

                        %>
                        <tr>
                            <td class="<%=className%>"  height="30" colspan="7" >&nbsp;</td>
                            <td class="<%=className%>" align="right" height="30"><b>Sub Total</b></td>
                            <td class="<%=className%>" align="right" height="30"><b><%=df.format(subTotalTonnage)%></b></td>
                            <td class="<%=className%>" align="right" height="30"><b><%=df.format(subTotalFreight)%></b></td>

                        </tr>
                        <%
                        subTotalTonnage = 0;
                        subTotalFreight = 0;

                    }
                    }
                                        subTotalTonnage += Double.parseDouble(totalTonnage);
                                        subTotalFreight += Double.parseDouble(totalFreightAmount);
                                        if(index%2 == 0){
                                        className = "text2";

                                        }else {
                                        className = "text1";
                                        }
                                         %>
                                         <tr>
                                              <td class="<%=className%>"  height="30"><%=sno%></td>
                                              <td class="<%=className%>"  height="30"><%=consigneeName%></td>
                                              <td class="<%=className%>"  height="30"><%=tripId%></td>
                                              <td class="<%=className%>"  height="30"><%=tripDate%></td>
                                              <td class="<%=className%>"  height="30"><%=destination%></td>
                                              <td class="<%=className%>"  height="30"><%=vehicleNumber%></td>
                                              <td class="<%=className%>"  height="30"><%=gpno%></td>
                                              <td class="<%=className%>" align="right" height="30"><%=invoiceNo%></td>
                                              <td class="<%=className%>" align="right" height="30"><%=totalTonnage%></td>
                                              <td class="<%=className%>" align="right" height="30"><%=totalFreightAmount%></td>
                                          </tr>

                                                <%
                                                existConsigneeName = consigneeName;

                                    } %>
                                           <tr>
                                    <td class="<%=className%>"  height="30" colspan="7" >&nbsp;</td>
                                    <td class="<%=className%>" align="right" height="30"><b>Sub Total</b></td>
                                    <td class="<%=className%>" align="right" height="30"><b><%=df.format(subTotalTonnage)%></b></td>
                                    <td class="<%=className%>" align="right" height="30"><b><%=df.format(subTotalFreight)%></b></td>
                        </tr>
                                    <tr>
                                        <td class="text2"  height="30" colspan="7" >&nbsp;</td>
                                        <td class="text2" align="right" height="30"><b>Grand Total</b></td>
                                        <td class="text2" align="right" height="30"><b><%=df.format(grandTotalTonnage)%></b></td>
                                        <td class="text2" align="right" height="30"><b><%=df.format(totalFreight)%></b></td>
                                    </tr>
                                          <%

                                            }
                                %>
                                </c:if>
                            </table>
        </form>
    </body>
</html>