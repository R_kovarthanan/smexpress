<%-- 
    Document   : displayTripSettlementExcel
    Created on : Jun 12, 2013, 12:27:24 AM
    Author     : Entitle
--%>

<%@page import="java.text.SimpleDateFormat"%>
<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

         <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }

        </style>
    </head>
    <body>
        <form name="tripsettlewise" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "Tripshettlement_Wise_Report-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>
            <br/>
             <%
                            DecimalFormat df = new DecimalFormat("0.00##");
                            String tripId = "",tripDate = "",destination = "",vehicleNumber = "",productName = "",totalExpenses = "";
                            String departureName = "",gpno = "",totalTonnage = "",distance = "",totalFreightAmount = "",driverName = "";
                            double grandTotalFreight = 0,subTotalFreight = 0,totalSettlement = 0,grandTotalSettlementAmount = 0,subTotalSettlementAmount = 0;
                            double grandTotalTonnage = 0,subTotalTonnage = 0,subTotalExpenses = 0,grandTotalExpenses = 0;
                            String className = "text2",settlementAmount = "",settlementDate = "",existTripDate = "";
                            int index = 0;
                            int sno = 0;
%> <br>
                <table  border="1" class="border" align="center" width="800" cellpadding="0" style="margin-left:30px" cellspacing="0" id="bg">
                    <c:if test = "${tripsettlementDetailsList != null}" >
                        <%
                        ArrayList tripsettlementDetailsList = (ArrayList) request.getAttribute("tripsettlementDetailsList");
                        Iterator detailsItr = tripsettlementDetailsList.iterator();
                        ReportTO repTO = null;
                         if (tripsettlementDetailsList.size() != 0) {
                            %>
                            <tr>
                                <td class="contentsub" height="30">S.No</td>
                                <td class="contentsub" height="30">Trip Id</td>
                                <td class="contentsub" height="30">Trip Date</td>
                                <td class="contentsub" height="30">Settlement Date</td>
<!--                                <td class="contentsub" height="30" width="80">Settlement</td>-->
                                <td class="contentsub" height="30" width="100">Vehicle No</td>
                                <td class="contentsub" height="30">Driver Name</td>
                                <td class="contentsub" height="30">Route Name</td>
                                <td class="contentsub" height="30">Tonnage</td>
                                <td class="contentsub" height="30">Material Name</td>
                                <td class="contentsub" height="30">Freight </td>
                                <td class="contentsub" height="30">Total Expenses</td>
                                </tr>
                    <%
                     while (detailsItr.hasNext()) {
                         index++;
                         sno++;

                         repTO = new ReportTO();
                         repTO = (ReportTO) detailsItr.next();
                         tripId  = repTO.getTripId();
                        if(tripId == null){
                            tripId = "";
                            }
                        tripDate  = repTO.getTripDate();
                        if(tripDate == null){
                            tripDate = "";
                            }
                            destination  = repTO.getDestination();
                        if(destination == null){
                            destination = "";
                            }

                         vehicleNumber  = repTO.getVehicleId();
                            if(vehicleNumber == null){
                                vehicleNumber = "";
                                }
                            gpno  = repTO.getGpno();
                            if(gpno == null){
                                gpno = "";
                                }
                            driverName  = repTO.getDriverName();
                            if(driverName == null){
                                driverName = "";
                                }
                            settlementAmount  = repTO.getSettlementAmount();
                           subTotalSettlementAmount += Double.parseDouble(settlementAmount);
                           grandTotalSettlementAmount += Double.parseDouble(settlementAmount);
                            if(settlementAmount == null){
                                settlementAmount = "";
                                }
                            settlementDate  = repTO.getSettlementDate();
                            if(settlementDate == null){
                                settlementDate = "";
                                }
                         productName  = repTO.getProductName();
                        if(productName == null){
                            productName = "";
                            }
                            totalTonnage  = repTO.getTotalTonnage();
                            if(totalTonnage == null){
                                totalTonnage = "";
                                }
                            subTotalTonnage += Double.parseDouble(totalTonnage);
                          grandTotalTonnage += Double.parseDouble(totalTonnage);
                            totalFreightAmount  = repTO.getTotalFreightAmount();
                          subTotalFreight += Double.parseDouble(totalFreightAmount);
                          grandTotalFreight += Double.parseDouble(totalFreightAmount);
                            if(totalFreightAmount == null){
                                totalFreightAmount = "";
                                }
                            totalExpenses  = repTO.getTotalExpenses();
                           subTotalExpenses += Double.parseDouble(totalExpenses);
                           grandTotalExpenses += Double.parseDouble(totalExpenses);
                            if(totalExpenses == null){
                                totalExpenses = "";
                                }

                            if(existTripDate !=""){
                    if(!existTripDate.equalsIgnoreCase(tripDate)){
                        if(className.equalsIgnoreCase("text1")){
                            className = "text2";
                            }else{
                            className = "text1";
                            }
                            index++;
                        %>
                        <tr>
                            <td class="<%=className%>" align="right"  height="30"  colspan="6" >&nbsp;</td>
                            <td class="<%=className%>"  height="30" ><b>Sub Total</b></td>
<!--                            <td class="<%//=className%>"  height="30"><b><%//=df.format(subTotalExpenses)%></b></td>-->
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(subTotalTonnage)%></b></td>
<!--                            <td class="<%//=className%>" align="right"  height="30"  colspan="3" >&nbsp;</td>-->
                            <td class="<%=className%>" align="right"  height="30">&nbsp;</td>
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(subTotalFreight)%></b></td>
<!--                            <td class="<%//=className%>" align="right"  height="30"  colspan="2" >&nbsp;</td>-->
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(subTotalSettlementAmount)%></b></td>
                        </tr>
                        <%

                        subTotalTonnage = 0;
                        subTotalFreight = 0;
                        subTotalExpenses = 0;
                        subTotalSettlementAmount = 0;

                    }
                    }
                             if(index%2 == 0){
                            className = "text2";

                            }else {
                            className = "text1";
                        }
                        %>
                    <tr>
                          <td class="<%=className%>"  height="30"><%=sno%></td>
                          <td class="<%=className%>"  height="30"><%=tripId%></td>
                          <td class="<%=className%>"  height="30"><%=tripDate%>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td class="<%=className%>"  height="30"><%=settlementDate%></td>
<!--                          <td class="<%=className%>"  height="30"><%=totalExpenses%></td>-->
                          <td class="<%=className%>"  height="30"><%=vehicleNumber%></td>
                          <td class="<%=className%>"  height="30"><%=driverName%></td>
                          <td class="<%=className%>"  height="30"><%=destination%></td>
                          <td class="<%=className%>"  align="right"  height="30"><%=totalTonnage%>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td class="<%=className%>"  height="30"><%=productName%></td>
                          <td class="<%=className%>"  align="right"  height="30"><%=totalFreightAmount%>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td class="<%=className%>" align="right"  height="30"><%=settlementAmount%>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                        <%
                        existTripDate = tripDate;
                     }
                     %>
                     <tr>
                            <td class="<%=className%>" align="right"  height="30"  colspan="6" >&nbsp;</td>
                            <td class="<%=className%>"  height="30" ><b>Sub Total</b></td>
<!--                            <td class="<%//=className%>"  height="30"><b><%//=df.format(subTotalExpenses)%></b></td>-->
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(subTotalTonnage)%></b></td>
<!--                            <td class="<%//=className%>" align="right"  height="30"  colspan="3" >&nbsp;</td>-->
                            <td class="<%=className%>" align="right"  height="30">&nbsp;</td>
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(subTotalFreight)%></b></td>
<!--                            <td class="<%//=className%>" align="right"  height="30"  colspan="2" >&nbsp;</td>-->
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(subTotalSettlementAmount)%></b></td>
                        </tr>
                        <%
                        subTotalTonnage = 0;
                        subTotalFreight = 0;
                        subTotalExpenses = 0;
                        subTotalSettlementAmount = 0;

%>
                         <tr>
                            <td class="<%=className%>" align="right"  height="30" colspan="6" >&nbsp;</td>
                            <td class="<%=className%>" height="30"><b>Grand Total</b></td>
<!--                            <td class="<%//=className%>" height="30"><b><%//=df.format(grandTotalExpenses)%></b></td>-->
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(grandTotalTonnage)%></b></td>
                            <td class="<%=className%>" align="right"  height="30" colspan="" >&nbsp;</td>
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(grandTotalFreight)%></b></td>
<!--                            <td class="<%//=className%>" align="right"  height="30" colspan="2" >&nbsp;</td>-->
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(grandTotalSettlementAmount)%></b></td>
                        </tr>
                        <%
                             }

                            
%>
                    </c:if>
                </table>
        </form>
    </body>
</html>
