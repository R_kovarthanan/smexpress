<%@ include file="/content/common/NewDesign/header.jsp" %>
	<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
	

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">



    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js" type="text/javascript"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                // alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        <script type="text/javascript">
            function submitPage(value) {
                //alert("am here...");
                if(value == 'ExportExcel'){
                document.accountReceivable.action = '/throttle/dashboardReport.do?param=ExportExcel';
                document.accountReceivable.submit();
                }else{
                document.accountReceivable.action = '/throttle/dashboardReport.do?param=Search';
                document.accountReceivable.submit();
                }
            }
            
           

        </script>

    </head>
    
     <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Dashboard Report" text="Dashboard Report"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
		                    <li class=""><spring:message code="hrms.label.Dashboard Report" text="Dashboard Report"/></li>
		
		                </ol>
		            </div>
       			</div>
        
             <div class="contentpanel">
             <div class="panel panel-default">
             <div class="panel-body">
    
    <body >
        <form name="accountReceivable" action=""  method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <br>
            <!--<table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">-->
                <table class="table table-info mb30 table-hover" id="report" >
                    <thead>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li>Dashboard Report</li>
                            </ul>
                            <div id="first">
                                <!--<table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >-->
                                    <tr>
                                        
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker form-control" style="width:240px;height:40px" " value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker form-control" style="width:240px;height:40px" " value="<c:out value="${toDate}"/>"></td>
                                    </tr>
                                    </thead>
                                    <tr>
                                        <td colspan="2" align="right"><input type="button" class="btn btn-success" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                                        <td colspan="2"><input type="button" class="btn btn-success" name="Search"   value="Search" onclick="submitPage(this.name);"></td>
                                    </tr>


                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>
                                    

            <br>
            <br>
            <br>
            
       
            <%
                int index = 0,sno = 1;
                String classText = "";
                int oddEven = index % 2;
            %>
            <c:if test = "${salesDashboard != null}" >
                <!--<table style="width: 1100px" align="center" border="0" id="table1" class="sortable">-->
                    <table class="table table-info mb30 table-hover" id="table1" >	
                    <thead>
                        <tr height="80">                            
                            <th><h3>Sales Lead</h3></th>
                            <th><h3>Trips</h3></th>
                            <th><h3>Revenue/Km</h3></th>
                            <th><h3>Margin/Km</h3></th>
                            <th><h3>WFU</h3></th>
                            <th><h3>WFL</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <c:forEach items="${salesDashboard}" var="odList">
                            <%
                                        classText = "";
                                        oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                                    <tr>
                                    <td align="left" class="text2"><c:out value="${odList.empName}"/> </td>
                                    <td align="right" class="text2"><c:out value="${odList.trips}"/> </td>
                                    <td align="right" class="text2"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${odList.sales / odList.km}" /></td>
                                    <td align="right" class="text2"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${odList.margin / odList.km}" /></td>
                                    <td align="right" class="text2"><c:out value="${odList.wfu}"/> </td>
                                    <td align="right" class="text2"><c:out value="${odList.wfl}"/> </td>
                                    </tr>
                                     <%
                                       index++;
                                       sno++;
                                    %>
                                    
                                
                           
                        </c:forEach>

                    </tbody>
                </table>

                <br>
                <br>
                <br>
    <c:if test = "${opsDashboard != null}" >
                <!--<table style="width: 1100px" align="center" border="0" id="table" class="sortable">-->
                    <table class="table table-info mb30 table-hover" id="table" >	
                    <thead>
                        <tr height="80">
                            <th><h3>Ops Lead</h3></th>
                            <th><h3>TotalVehicles</h3></th>
                            <th><h3>TripVehicles</h3></th>
                            <th><h3>Trips</h3></th>
                            <th><h3>Km</h3></th>
                            <th><h3>KM/truck/mnth</h3></th>
                            <th><h3>KM/truck/day</h3></th>
                            <th><h3>VehicleDaysLostDue to Unloading delays</h3></th>
                            <th><h3>VehicleDaysLostDue to Loading delays</h3></th>
                            <th><h3>VehicleDaysLostDue to R&M</h3></th>
                            <th><h3>RCM Deviation</h3></th>
                            <th><h3>Cost/Km</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <%  index = 0; sno = 1;%>
                        <c:forEach items="${opsDashboard}" var="odList">
                            <%
                                        classText = "";
                                        oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                                    <tr>
                                    <td align="left" class="text2"><c:out value="${odList.empName}"/> </td>
                                    <td align="right" class="text2"><c:out value="${odList.noOfVehicles}"/> </td>
                                    <td align="right" class="text2"><c:out value="${odList.vehicles}"/> </td>
                                    <td align="right" class="text2"><c:out value="${odList.trips}"/> </td>
                                    <td align="right" class="text2"><c:out value="${odList.km}"/> </td>
                                    <td align="right" class="text2"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${(odList.km / odList.noOfVehicles) / odList.months}" /></td>
                                    <td align="right" class="text2"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${(odList.km / odList.noOfVehicles) / odList.days}" /></td>

                                    
                                    <td align="right" class="text2"><c:out value="${odList.rmDays}"/> </td>
                                    <td align="right" class="text2"><c:out value="${odList.wfu}"/> </td>
                                    <td align="right" class="text2"><c:out value="${odList.wfl}"/> </td>
                                    <td align="right" class="text2"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${odList.estimatedExpenses - odList.expenses}" /></td>
                                    <td align="right" class="text2"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${odList.expenses / odList.km}" /></td>
                                    </tr>
                                     <%
                                       index++;
                                       sno++;
                                    %>



                        </c:forEach>

                    </tbody>
                </table>
                </c:if>

            
            
        </form>
        </c:if>
    </body>    
</div>
	</div>
        </div>
        <%@ include file="/content/common/NewDesign/settings.jsp" %>