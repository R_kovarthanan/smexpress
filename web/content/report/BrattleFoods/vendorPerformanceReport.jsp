<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>

        <script type="text/javascript">
            //auto com

            $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#vehicleNo').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getRegistrationNo.do",
                            dataType: "json",
                            data: {
                                regno: request.term
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#vehicleId').val(tmp[0]);
                        $('#vehicleNo').val(tmp[1]);
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };
            });


        </script>



        <script type="text/javascript">
            function submitPage(value) {
                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
                } else if (document.getElementById('toDate').value == '') {
                    alert("Please select to Date");
                    document.getElementById('toDate').focus();
                } else if (document.getElementById('expenseType').value == '') {
                    alert("Please select  expense Type");
                    document.getElementById('expenseType').focus();
                }

                else {
                    if (value == 'ExportExcel') {
                        document.accountReceivable.action = '/throttle/handleVendorPerformance.do?param=ExportExcel';
                        document.accountReceivable.submit();
                    } else {
                        document.accountReceivable.action = '/throttle/handleVendorPerformance.do?param=Search';
                        document.accountReceivable.submit();
                    }
                }
            }

            function setValue() {
                if ('<%=request.getAttribute("page")%>' != 'null') {
                    var page = '<%=request.getAttribute("page")%>';
                    if (page == 1) {
                        submitPage('search');
                    }
                }
                if ('<%=request.getAttribute("vehicleTypeId")%>' != 'null') {
                    document.getElementById('vehicleTypeId').value = '<%=request.getAttribute("vehicleTypeId")%>';
                }
                if ('<%=request.getAttribute("vendorId")%>' != 'null') {
                    document.getElementById('vendorId').value = '<%=request.getAttribute("vendorId")%>';
                }
                if ('<%=request.getAttribute("expenseType")%>' != 'null') {
                    document.getElementById('expenseType').value = '<%=request.getAttribute("expenseType")%>';
                }
                
            }


        </script>
    </head>

    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ManageUser" text="Vendor Performance Report"/> </h2>
        <div class="breadcrumb-wrapper">            
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Reports" text="Reports"/></a></li>
                <li class=""><spring:message code="hrms.label.ManageUser" text="Vendor Performance"/></li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">

                <body onload="showLable('<%=request.getAttribute("expenseType")%>');setValue();
                        sorter.size(20);">
                    <form name="accountReceivable"   method="post">
                        <%@ include file="/content/common/message.jsp" %>
                        <table class="table table-info mb10 table-hover" id="bg" >
                            <thead>
                                <tr>
                                    <th colspan="2" height="30" >Vendor Performance Report</th>
                                </tr>
                            </thead>
                        </table>
                        <table class="table table-info mb30 table-hover" id="report" >
                            <tr height="30">
                                <td >Vehicle Type</td>
                                <td height="30">
                                    <select name="vehicleTypeId" id="vehicleTypeId"  class="form-control" style="width:250px;height:40px" style="height:20px; width:122px;" >
                                        <c:if test="${VehicleTypeList != null}">
                                            <option value="" selected>--Select--</option>
                                            <c:forEach items="${VehicleTypeList}" var="type">
                                                <option value='<c:out value="${type.vehicleTypeId}"/>'><c:out value="${type.vehicleTypeName}"/></option>
                                            </c:forEach>
                                        </c:if>
                                    </select>
                                </td>
                                <td>Vendor Name</td>
                                <td height="30"> 
                                    <select name="vendorId" id="vendorId"  class="form-control" style="width:250px;height:40px" style="height:20px; width:122px;" >
                                        <c:if test="${VendorList != null}">
                                            <option value="" selected>--Select--</option>
                                            <c:forEach items="${VendorList}" var="VendorList">
                                                <option value='<c:out value="${VendorList.vendorId}"/>'><c:out value="${VendorList.vendorName}"/></option>
                                            </c:forEach>
                                        </c:if>
                                    </select>
                                </td>
                            </tr>

                            <tr height="30">
                                <td ><font color="red">*</font>From Date</td>
                                <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:250px;height:40px" value="<c:out value="${fromDate}"/>" ></td>
                                <td><font color="red">*</font>To Date</td>
                                <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" style="width:250px;height:40px" value="<c:out value="${toDate}"/>" onchange="calculateDays();">
                                    <input type="hidden" value="" name="totalDays" id="totalDays" /></td>
                            </tr>
                            <tr height="30">
                                <td ><font color="red">*</font>Report Type</td>
                                <td>
                                    <select name="expenseType" id="expenseType" class="form-control"  style="width:250px;height:40px"  >
                                        <option value="" selected>--Select--</option>
                                        <option value="MIN">Minimum Expense</option>
                                        <option value="MAX">Maximum Expense</option>                                        
                                        <option value="VEH">Vehicle Volume</option>                                        
                                    </select></td>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr height="30">
                                <!--<td colspan="3" align="right"><input type="button" class="btn btn-success" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>-->
                                <td colspan="3"><input type="button" class="btn btn-success" name="Search"  id="Search"  value="Search" onclick="submitPage(this.name);"></td>
                            </tr>
                        </table>
                                    
                                    <script>
                                            function showLable(val) {
                                                if (val == 'VEH') {
                                                    $("#table1").hide();                                                    
                                                    $("#table2").show();
                                                } else if(val != '') {
                                                    $("#table1").show();
                                                    $("#table2").hide();                                                    
                                                }else{
                                                    $("#table1").hide();
                                                    $("#table2").hide();                                                    
                                                }
                                            }
                                        </script>
        
        
        <c:if test="${VendorPerformanceList != null}">
            <table class="table table-info mb30 table-hover" id="table1" style='display:none'>
                <thead>
                    <tr>
                        <th align="center">S.No</th>
                        <th align="center">Vendor</th>
                        <th align="center">Vehicle Type</th>
                        <th align="center">From Date</th>
                        <th align="center">To Date</th>
                        <th align="center">Origin</th>
<!--                        <th align="center">Touch Point 1</th>
                        <th align="center">Touch Point 2</th>
                        <th align="center">Touch Point 3</th>
                        <th align="center">Touch Point 4</th>-->
                        <th align="center">Destination</th>
                        <th align="center">Vehicle Expense</th>                       
                    </tr> 
                </thead>
                <tbody>
                    <% int index = 1;%>
                    <c:forEach items="${VendorPerformanceList}" var="utilList">

                        <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                        %>
                        <tr>
                            <td  ><%=index++%></td>                           
                            <td><c:out value="${utilList.vendorName}"/></td>
                            <td><c:out value="${utilList.vehicleTypeName}"/></td>
                            <td><c:out value="${utilList.startDate}"/></td>
                            <td><c:out value="${utilList.endDate}"/></td>
                            <td><c:out value="${utilList.originNameFullTruck}"/></td>
<!--                            <td><c:out value="${utilList.point1Name}"/></td>
                            <td><c:out value="${utilList.point2Name}"/></td>
                            <td><c:out value="${utilList.point3Name}"/></td>
                            <td><c:out value="${utilList.point4Name}"/></td>                            -->
                            <td><c:out value="${utilList.destinationNameFullTruck}"/></td>                            
                            <td><c:out value="${utilList.additionalCost}"/></td>                            
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>
                                    
        <c:if test="${VendorVehicleList != null}">
            <table class="table table-info mb30 table-hover" id="table2" style='display:none'>
                <thead>
                    <tr>
                        <th align="center">S.No</th>
                        <th align="center">Vendor Name</th>
                        <th align="center">No.Of Vehicle </th>                        
                        <th align="center">Vendor Address</th>                       
                    </tr> 
                </thead>
                <tbody>
                    <% int index1 = 1;%>
                    <c:forEach items="${VendorVehicleList}" var="utilList1">

                        <%
                            String classText = "";
                            int oddEven = index1 % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                        %>
                        <tr>
                            <td  ><%=index1++%></td>                           
                            <td><c:out value="${utilList1.vendorName}"/></td>
                            <td><c:out value="${utilList1.vehCount}"/></td>
                            <td><c:out value="${utilList1.address}"/></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>                            

        <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" selected="selected">5</option>
                    <option value="10" selected>10</option>
                    <option value="20" >20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var expenseType = document.getElementById("expenseType").value;
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            if(expenseType=="VEH"){
            sorter.init("table2", 1);
            }else{
            sorter.init("table1", 1);    
            }
        </script>
    </form>
</body>    


</div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>