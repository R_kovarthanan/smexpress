<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

         <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                            altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>


        <script type="text/javascript">
            function submitPage(value){
        
                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
                } else if (document.getElementById('toDate').value == '') {
                    alert("Please select to Date");
                    document.getElementById('toDate').focus();
                }else{
                    if(value == "ExportExcel"){
                        document.BPCLTransaction.action = '/throttle/handleTripExtraExpenseReportExcel.do?param=ExportExcel';
                        document.BPCLTransaction.submit();
                    }
                    else{
                        document.BPCLTransaction.action = '/throttle/handleTripExtraExpenseReportExcel.do?param=Search';
                        document.BPCLTransaction.submit();
                    }
                }
            }
        </script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.BrokerCustomerReport" text="BrokerCustomerReport"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Report" text="Reports"/></a></li>
            <li class=""><spring:message code="hrms.label.BrokerCustomerReport" text=" BrokerCustomerReport"/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body>
        <form name="BPCLTransaction" method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <!-- pointer table -->
            <!-- message table -->
            <%@ include file="/content/common/message.jsp"%>

                                <table class="table table-info mb30 table-hover">
                                    <thead><tr><th colspan="4">Trip Extra Expense Report</th></tr></thead>
                                    <!--                                    <tr>
                                                                            <td><font color="red">*</font>Driver Name</td>
                                                                            <td height="30">
                                                                                <input name="driName" id="driName" type="text" class="textbox" size="20" value="" onKeyPress="getDriverName();" autocomplete="off">
                                                                        </tr>-->
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker , form-control" style="width:250px;height:40px"" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker , form-control" style="width:250px;height:40px"" value="<c:out value="${toDate}"/>"></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><input type="button" class="btn btn-success" name="search" onclick="submitPage(this.name);" value="Search"></td>
                                        <td><input type="button" class="btn btn-success" name="ExportExcel" onclick="submitPage(this.name);" value="ExportExcel"></td>
                                        <td></td>
                                    </tr>

                                </table>

            <c:if test="${TripExtraExpenseDetails != null}">
                <table  id="table" class="table table-info mb30 table-hover" >

                    <thead>
                        <tr height="50">
                            <th align="center"><h3>S.No</h3></th>
                            <th align="center"><h3>Trip Code</h3></th>
                            <th align="center"><h3>Route Name</h3></th>
                            <th align="center"><h3>Customer Name</h3></th>
                            <th align="center"><h3>Start Date</h3></th>
                            <th align="center"><h3>End Date</h3></th>
                            <th align="center"><h3>Driver Name</h3></th>
                            <th align="center"><h3>Expense Date</h3></th>
                            <th align="center"><h3>Total Expense</h3></th>
                            <th align="center"><h3>Expense Name</h3></th>
                            <th align="center"><h3>Expense Remarks</h3></th>
                                                    </tr>
                    </thead>
                    <tbody>
                        <% int index = 1;%>
                        
                        <c:forEach items="${TripExtraExpenseDetails}" var="BPCLTD">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <tr>
                                <td class="<%=classText%>"><%=index++%></td>
                                <td  width="30" class="<%=classText%>"><c:out value="${BPCLTD.tripCode}"/></td>
                                <td  width="30" class="<%=classText%>"><c:out value="${BPCLTD.routeInfo}"/><input type="hidden" value="<c:out value="${BPCLTD.vehicleNo}"/>" name="vehicleNo" id="vehicleNo"/></td>
                                <td width="30" class="<%=classText%>"><c:out value="${BPCLTD.customerName}"/></td>
                                <td width="30" class="<%=classText%>" ><c:out value="${BPCLTD.startDate}"/></td>
                                <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.endDate}"/></td>
                                <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.expenseDriverName}"/></td>
                                <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.expenseDate}"/></td>
                                <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.totalExpense}"/>
                                  </td>
                                <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.expenseName}"/></td>
                                <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.expenseRemarks}"/></td>
                                                            </tr>

                        </c:forEach>
                    </tbody>
                </table>

               
            </c:if>
 <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
     <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" >5</option>
                    <option value="10">10</option>
                    <option value="20" selected="selected">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>


        </form>
    </body>
</div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
