<%-- 
    Document   : Accounts Receivable
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>
       
        <form name="accountReceivable" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "AccountsReceivableHeader-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>

            <c:if test = "${customerWiseProfitList != null}" >
                <table style="width: 1100px" align="center" border="0" >
                    <thead>
                        <tr height="80">
                            <th class="contentsub"><h3>S.No</h3></th>
                            <th class="contentsub"><h3>Customer Name</h3></th>
                            <th class="contentsub"><h3>Billing Type</h3></th>
                            <th class="contentsub"><h3>Revenue</h3></th>
                            <th class="contentsub"><h3>Expense</h3></th>
                            <th class="contentsub"><h3>Profit</h3></th>
                            <th class="contentsub"><h3>Profit %</h3></th>
                            <th class="contentsub"><h3>No Of Trip</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0,sno = 1;%>
                        <c:forEach items="${customerWiseProfitList}" var="csList">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <% if (oddEven > 0) { %>
                            <tr height="40">
                                <td class="text2" align="left" style="width: 10px;"><%=sno%></td>
                                <td class="text2" align="left" height="40"><c:out value="${csList.customerName}"/></td>
                                <td class="text2" align="left"><c:out value="${csList.billingType}"/></td>
                                <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${csList.freightAmount}" /></td>
                                <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${csList.totalExpenses}" /></td>
                                <c:if test="${csList.customerName != 'Empty Trip'}">
                                    <c:if test="${csList.profitValue <= 0}">
                                <td align="right">
                                    <font color="red">
                                    <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${csList.profitValue}" />
                                    </font>
                                </td>
                                </c:if>
                                <c:if test="${csList.profitValue > 0}">
                                <td align="right">
                                    <font color="green">
                                    <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${csList.profitValue}" />
                                    </font>
                                    </td>
                                </c:if>
                                <c:if test="${csList.profitPercent <= 0}">
                                <td align="right">
                                    <font color="red">
                                    <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${csList.profitPercent}" />
                                    </font>
                                </td>
                                </c:if>
                                <c:if test="${csList.profitPercent > 0}">
                                <td align="right">
                                    <font color="green">
                                    <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${csList.profitPercent}" />
                                    </font>
                                    </td>
                                </c:if>
                                </c:if>
                                <c:if test="${csList.customerName == 'Empty Trip'}">
                                    <td align="right"><font color="red">0.00</font></td>
                                    <td align="right"><font color="red">0.00</font></td>
                                </c:if>
                                <td class="text2" align="center"> <a href="#" onclick="viewCustomerProfitDetails('<c:out value="${csList.tripId}"/>');"><c:out value="${csList.tripNos}"/></a></td>
                                <c:set var="totalRevenue" value="${csList.freightAmount + totalRevenue}"/>
                                <c:set var="expenses" value="${csList.totalExpenses + expenses}"/>
                                <c:set var="profit" value="${csList.profitValue + profit}"/>
                                <c:set var="totalTripNos" value="${csList.tripNos + totalTripNos}"/>
                                <c:set var="perc" value="${csList.profitPercent + perc}"/>
                                <c:set var="count" value="${count + 1}"/>
                            </tr>
                            <%}else{%>
                             <tr height="40">
                                <td  align="left" ><%=sno%></td>
                                <td class="text1" align="left"><c:out value="${csList.customerName}"/></td>
                                <td class="text1" align="left"><c:out value="${csList.billingType}"/></td>
                                <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${csList.freightAmount}" /></td>
                                <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${csList.totalExpenses}" /></td>
                                <c:if test="${csList.profitValue <= 0}">
                                <td class="text1" align="right">
                                    <font color="red">
                                    <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${csList.profitValue}" />
                                    </font>
                                </td>
                                </c:if>
                                <c:if test="${csList.profitValue > 0}">
                                <td class="text1" align="right">
                                    <font color="green">
                                    <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${csList.profitValue}" />
                                    </font>
                                    </td>
                                </c:if>
                                <c:if test="${csList.profitPercent <= 0}">
                                <td class="text1" align="right">
                                    <font color="red">
                                    <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${csList.profitPercent}" />
                                    </font>
                                </td>
                                </c:if>
                                <c:if test="${csList.profitPercent > 0}">
                                <td class="text1" align="right">
                                    <font color="green">
                                    <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${csList.profitPercent}" />
                                    </font>
                                    </td>
                                </c:if>
                                <td class="text1" align="center"> <a href="#" onclick="viewCustomerProfitDetails('<c:out value="${csList.tripId}"/>');"><c:out value="${csList.tripNos}"/></a></td>
                                <c:set var="totalRevenue" value="${csList.freightAmount + totalRevenue}"/>
                                <c:set var="expenses" value="${csList.totalExpenses + expenses}"/>
                                <c:set var="profit" value="${csList.profitValue + profit}"/>
                                <c:set var="totalTripNos" value="${csList.tripNos + totalTripNos}"/>
                                <c:set var="perc" value="${csList.profitPercent + perc}"/>
                                <c:set var="count" value="${count + 1}"/>
                            </tr>
                            <%}%>
                            <%
                                       index++;
                                       sno++;
                            %>
                        </c:forEach>

                    </tbody>
                </table>
            </c:if>


            <table>
                <tr>
                    <td></td>
                    <td></td>
                </tr>

            </table>
            <br/>
            <br/>
            <c:if test = "${customerWiseProfitList != null}" >
            <table border="2" style="border: 1px solid #666666;"  align="center"  cellpadding="0" cellspacing="0" >
                <tr height="25" align="right">
                    <td style="background-color: #6374AB; color: #ffffff">Total Trips Carried Out</td>
                    <td width="150" align="right"><fmt:formatNumber type="number"  value="${totalTripNos}" /></td>
                </tr>
                <tr height="25" >
                    <td style="background-color: #6374AB; color: #ffffff">Total Income</td>
                    <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalRevenue}" /></td>
                </tr>
                <tr height="25" >
                    <td style="background-color: #6374AB; color: #ffffff">Total Fixed Expenses</td>
                    <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${expenses}" /></td>
                </tr>
                <tr height="25" >
                    <td style="background-color: #6374AB; color: #ffffff">Total Profit </td>
                    <td width="150" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profit}" /></font></td>
                </tr>
                <tr height="25">
                    <td style="background-color: #6374AB; color: #ffffff">Total Profit % </td>
                    <td width="150" align="right"><font color="green">
                            <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${perc/count}" />
                            </font></td>
                </tr>
            </table>
            </c:if>
        </form>
    </body>    
</html>