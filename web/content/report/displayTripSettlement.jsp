<%-- 
    Document   : displayTripSettlement
    Created on : Jun 11, 2013, 11:52:27 PM
    Author     : Entitle
--%>

<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->

        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
         <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="ets.domain.security.business.SecurityTO" %>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script src="/throttle/js/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script src="/throttle/js/TableSort.js" language="javascript" type="text/javascript"></script>
        <link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {

                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>
        <script>
            function submitPage(value){
               if(value == 'Fetch Data'){
               var param = 'No';

               } else{
               var param = 'ExportExcel';
               }
               //alert('param is::;'+param);
                if(textValidation(document.tripsettlementwise.fromDate,'From Date')){
                    return;
                }else if(textValidation(document.tripsettlementwise.toDate,'To Date')){
                    return;
                }
                //alert(check);
                document.tripsettlementwise.action="/throttle/searchTripsettlementwiseDetails.do?param="+param;
                document.tripsettlementwise.submit();

            }
            function setFocus(){
                var tripId='<%=request.getAttribute("tripId")%>';
                var vehicleNo='<%=request.getAttribute("vehicleNo")%>';
                var ownerShip='<%=request.getAttribute("ownerShip")%>';
                var sdate='<%=request.getAttribute("fromDate")%>';
                var edate='<%=request.getAttribute("toDate")%>';
                if(tripId!='null'){
                document.tripsettlementwise.tripId.value=tripId;
                }
                if(ownerShip!='null'){
                document.tripsettlementwise.ownership.value=ownerShip;
                }
                if(vehicleNo!='null'){
                document.tripsettlementwise.vehicleNo.value=vehicleNo;
                }

                if(sdate!='null' && edate!='null'){
                    document.tripsettlementwise.fromDate.value=sdate;
                    document.tripsettlementwise.toDate.value=edate;
                }
            }
        </script>
    </head>
    <body onload="setFocus()" >
        <form name="tripsettlementwise" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
        <%@ include file="/content/common/message.jsp" %>
         <br/>
            <table cellpadding="0" cellspacing="2" align="center" border="0" width="900" id="report" bgcolor="#97caff" style="margin-top:0px;">
              <tr>
                    <td><b>Trip Settlement Details Report</b></td>
                    <td align="right"><span id="openClose" onclick="displayCollapse();" style="cursor: pointer;">Close</span>&nbsp;</td>
                </tr>
                <tr id="exp_table"  style="display: block;">
                    <td colspan="2" style="padding:15px;" align="right">
                        <div class="tabs" align="center" style="width:800px">
                            <table width="100%" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                <tr>
                                    <td  height="30">Trip Id:   </td>
                                    <td  height="30"><input type="text" name="tripId" id="tripId" class="textbox" size="20" onfocus="getTripIds();" autocomplete="off" onPaste="return false" onDrag="return false" onDrop="return false"></td>
                                    <td  height="30">Vehicle No:   </td>
                                    <td  height="30"><input type="text" name="vehicleNo" class="textbox" id="vehicleNo" size="20" onfocus="getVehicleNumbers();" autocomplete="off" onPaste="return false" onDrag="return false" onDrop="return false"></td>
                                </tr>
                                <tr>
                                    <td  height="30"><font color="red">*</font>From Date :</td>
                                    <td  height="30"><input type="text" name="fromDate" id="fromDate" class="datepicker" value="" />  </td>
                                    <td  height="30"><font color="red">*</font>To Date : </td>
                                    <td  height="30"><input type="text" name="toDate" id="toDate" class="datepicker" value="" /></td>
                                </tr>

                                 <tr>
                                     <td> Ownership : </td>
                                    <td><select class="textbox" style="width:123px; " name="ownership">
                                            <option value="0">All</option>
                                            <option value="1">Own</option>
                                            <option value="2">Attach</option>
                                        </select>
                                    </td>
                                     <td  height="30" colspan="2" align="center" >
                                            <input type="button"  value="Fetch Data"  class="button" name="Fetch Data" onClick="submitPage(this.value)">&nbsp;&nbsp;&nbsp;
                                            <input type="button"  value="Export to Excel"  class="button" name="excelExport" onClick="submitPage(this.value)">
                                    </td>

                                </tr>

                            </table>

                        </div>
                    </td>
                </tr>
         </table>
         <%
                            DecimalFormat df = new DecimalFormat("0.00##");
                            String tripId = "",tripDate = "",destination = "",vehicleNumber = "",productName = "",totalExpenses = "";
                            String departureName = "",gpno = "",totalTonnage = "",distance = "",totalFreightAmount = "",driverName = "";
                            double grandTotalFreight = 0,subTotalFreight = 0,totalSettlement = 0,grandTotalSettlementAmount = 0,subTotalSettlementAmount = 0;
                            double grandTotalTonnage = 0,subTotalTonnage = 0,subTotalExpenses = 0,grandTotalExpenses = 0;
                            String className = "text2",settlementAmount = "",settlementDate = "",existTripDate = "";
                            int index = 0;
                            int sno = 0;
%> <br>
                <table  border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" id="bg">
                    <c:if test = "${tripsettlementDetailsList != null}" >
                        <%
                        ArrayList tripsettlementDetailsList = (ArrayList) request.getAttribute("tripsettlementDetailsList");
                        Iterator detailsItr = tripsettlementDetailsList.iterator();
                        ReportTO repTO = null;
                         if (tripsettlementDetailsList.size() != 0) {
                            %>
                            <tr>
                                <td class="contentsub" height="30">S.No</td>
                                <td class="contentsub" height="30" >Trip Id</td>
                                <td class="contentsub" height="30">Trip Date</td>
                                <td class="contentsub" height="30">Settlement Date</td>
<!--                                <td class="contentsub" height="30" width="80">Settlement</td>-->
                                <td class="contentsub" height="30" width="80">Vehicle No</td>
                                <td class="contentsub" height="30">Driver Name</td>
                                <td class="contentsub" height="30">Route Name</td>
                                <td class="contentsub" height="30">Tonnage</td>
                                <td class="contentsub" height="30">Material Name</td>
                                <td class="contentsub" height="30">Freight </td>
                                
                                <td class="contentsub" height="30">Total Expenses</td>
                                </tr>
                    <%
                     while (detailsItr.hasNext()) {
                         index++;
                         sno++;
                          
                         repTO = new ReportTO();
                         repTO = (ReportTO) detailsItr.next();
                         tripId  = repTO.getTripId();
                        if(tripId == null){
                            tripId = "";
                            }
                        tripDate  = repTO.getTripDate();
                        if(tripDate == null){
                            tripDate = "";
                            }
                            destination  = repTO.getDestination();
                        if(destination == null){
                            destination = "";
                            }

                         vehicleNumber  = repTO.getVehicleId();
                            if(vehicleNumber == null){
                                vehicleNumber = "";
                                }
                            gpno  = repTO.getGpno();
                            if(gpno == null){
                                gpno = "";
                                }
                            driverName  = repTO.getDriverName();
                            if(driverName == null){
                                driverName = "";
                                }
                            settlementAmount  = repTO.getSettlementAmount();
                           subTotalSettlementAmount += Double.parseDouble(settlementAmount);
                           grandTotalSettlementAmount += Double.parseDouble(settlementAmount);
                            if(settlementAmount == null){
                                settlementAmount = "";
                                }
                            settlementDate  = repTO.getSettlementDate();
                            if(settlementDate == null){
                                settlementDate = "";
                                }
                         productName  = repTO.getProductName();
                        if(productName == null){
                            productName = "";
                            }
                            totalTonnage  = repTO.getTotalTonnage();
                            if(totalTonnage == null){
                                totalTonnage = "";
                                }
                            subTotalTonnage += Double.parseDouble(totalTonnage);
                          grandTotalTonnage += Double.parseDouble(totalTonnage);
                            totalFreightAmount  = repTO.getTotalFreightAmount();
                          subTotalFreight += Double.parseDouble(totalFreightAmount);
                          grandTotalFreight += Double.parseDouble(totalFreightAmount);
                            if(totalFreightAmount == null){
                                totalFreightAmount = "";
                                }
                            totalExpenses  = repTO.getTotalExpenses();
                           subTotalExpenses += Double.parseDouble(totalExpenses);
                           grandTotalExpenses += Double.parseDouble(totalExpenses);
                            if(totalExpenses == null){
                                totalExpenses = "";
                                }

                            if(existTripDate !=""){
                    if(!existTripDate.equalsIgnoreCase(tripDate)){
                        if(className.equalsIgnoreCase("text1")){
                            className = "text2";
                            }else{
                            className = "text1";
                            }
                            index++;
                        %>
                        <tr>
                            <td class="<%=className%>" align="right"  height="30"  colspan="6" >&nbsp;</td>
                            <td class="<%=className%>"  height="30" ><b>Sub Total</b></td>
<!--                            <td class="<%//=className%>"  height="30"><b><%//=df.format(subTotalExpenses)%></b></td>-->
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(subTotalTonnage)%></b></td>
<!--                            <td class="<%//=className%>" align="right"  height="30"  colspan="3" >&nbsp;</td>-->
                            <td class="<%=className%>" align="right"  height="30">&nbsp;</td>
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(subTotalFreight)%></b></td>
<!--                            <td class="<%//=className%>" align="right"  height="30"  colspan="2" >&nbsp;</td>-->
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(subTotalSettlementAmount)%></b></td>
                        </tr>
                        <%

                        subTotalTonnage = 0;
                        subTotalFreight = 0;
                        subTotalExpenses = 0;
                        subTotalSettlementAmount = 0;

                    }
                    }
                             if(index%2 == 0){
                            className = "text2";

                            }else {
                            className = "text1";
                        }
                        %>
                    <tr>
                          <td class="<%=className%>"  height="30"><%=sno%></td>
                          <td class="<%=className%>"  height="30"><%=tripId%></td>
                          <td class="<%=className%>"  height="30"><%=tripDate%>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td class="<%=className%>"  height="30"><%=settlementDate%></td>
<!--                          <td class="<%//=className%>"  height="30"><%//=totalExpenses%></td>-->
                          
                          <td class="<%=className%>"  height="30"><%=vehicleNumber%></td>
                          <td class="<%=className%>"  height="30"><%=driverName%></td>
                          <td class="<%=className%>"  height="30"><%=destination%></td>
                          <td class="<%=className%>"  align="right"  height="30"><%=totalTonnage%>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td class="<%=className%>"  height="30"><%=productName%></td>
                          <td class="<%=className%>"  align="right"  height="30"><%=totalFreightAmount%>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          
                          <td class="<%=className%>" align="right"  height="30"><%=settlementAmount%>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                        <%
                        existTripDate = tripDate;
                     }
                     %>
                     <tr>
                            <td class="<%=className%>" align="right"  height="30"  colspan="6" >&nbsp;</td>
                            <td class="<%=className%>"  height="30" ><b>Sub Total</b></td>
<!--                            <td class="<%//=className%>"  height="30"><b><%//=df.format(subTotalExpenses)%></b></td>-->
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(subTotalTonnage)%></b></td>
<!--                            <td class="<%//=className%>" align="right"  height="30"  colspan="3" >&nbsp;</td>-->
                            <td class="<%=className%>" align="right"  height="30">&nbsp;</td>
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(subTotalFreight)%></b></td>
<!--                            <td class="<%//=className%>" align="right"  height="30"  colspan="2" >&nbsp;</td>-->
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(subTotalSettlementAmount)%></b></td>
                        </tr>
                        <%
                        subTotalTonnage = 0;
                        subTotalFreight = 0;
                        subTotalExpenses = 0;
                        subTotalSettlementAmount = 0;

%>
                         <tr>
                            <td class="<%=className%>" align="right"  height="30" colspan="6" >&nbsp;</td>
                            <td class="<%=className%>" height="30"><b>Grand Total</b></td>
<!--                            <td class="<%//=className%>" height="30"><b><%//=df.format(grandTotalExpenses)%></b></td>-->
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(grandTotalTonnage)%></b></td>
                            <td class="<%=className%>" align="right"  height="30" colspan="" >&nbsp;</td>
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(grandTotalFreight)%></b></td>
<!--                            <td class="<%//=className%>" align="right"  height="30" colspan="2" >&nbsp;</td>-->
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(grandTotalSettlementAmount)%></b></td>
                        </tr>
                        <%
                             }
%>
                    </c:if>
                </table>
        </form>
    </body>
</html>
<script type="text/javaScript">
        function getTripIds(){
            var oTextbox = new AutoSuggestControl(document.getElementById("tripId"),new ListSuggestions("tripId","/throttle/tripIdSuggestions.do?"));
        }
        function getVehicleNumbers(){
            var oTextbox = new AutoSuggestControl(document.getElementById("vehicleNo"),new ListSuggestions("vehicleNo","/throttle/vehicleNumberSuggestions.do?"));
        //alert('oTextbox is::'+oTextbox);
        }

    </script>