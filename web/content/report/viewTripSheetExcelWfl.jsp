<%--
    Document   : viewTripSheet
    Created on : Oct 31, 2013, 1:48:05 PM
    Author     : Throttle
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
         <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>
    <form name="tripSheet" method="post">
        <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "TripSheet-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>

        <c:if test="${tripDetails != null}">
            <table class="table table-info mb30 table-hover" id="table"  >
                <thead>
                    <tr height="70">
                        <th><h3>Sno</h3></th>
                        <th><h3>Trip Code</h3></th>
                        <th><h3>Trip Status</h3></th>
                        <th><h3>Consignment No</h3></th>
                        <th><h3>Vehicle No </h3></th>                        
                        <th><h3>Customer Name </h3></th>
                        <th><h3>Route </h3></th>
                        <th><h3>Vehicle Type </h3></th>
                        <th><h3>Trip Scheduled</h3></th>
                        <th><h3>Loading Date Time</h3></th>
                        <th><h3>Start Date Time</h3></th>
                        <th><h3>Unloading Date Time</h3></th>
                        <th><h3>Planned End Date Time</h3></th>
                        <th><h3>End Date Time</h3></th>                        
                    </tr>
                </thead>
                <tbody>
                    <% int index = 1;%>
                    <c:forEach items="${tripDetails}" var="tripDetails">
                        <%
                                    String className = "text1";
                                    if ((index % 2) == 0) {
                                        className = "text1";
                                    } else {
                                        className = "text2";
                                    }
                        %>
                        <tr height="30">
                            <td class="<%=className%>" width="40" align="left"><%=index%></td>
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.tripCode}"/></td>
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.status}"/></td>
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.cNotes}"/></td>
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.vehicleNo}"/></td>                                                       
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.customerName}"/></td>
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.routeInfo}"/></td>                            
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.vehicleTypeName}"/></td>
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.plannedStartDateTime}"/></td>
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.loadingDateTime}"/></td>
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.actualStartDateTime}"/></td>                            
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.unLoadingDate}"/></td>
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.plannedEndDateTime}"/></td>
                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.actualEndDateTime}"/></td>                            
                        </tr>

                        <%index++;%>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>
    </form>
</body>
</html>
