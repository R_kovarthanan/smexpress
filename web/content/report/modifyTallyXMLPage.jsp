<%-- 
    Document   : modifyTallyXMLPage
    Created on : Oct 29, 2010, 7:30:43 PM
    Author     : Admin
--%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="java.util.*" %>
  </head>
  <script>
      function searchSubmit()
      {
        document.modifyTallyXML.action = '/throttle/modifyTallyXMLReport.do'
        document.modifyTallyXML.submit();
      }
  </script>
  <body>
    <form name="modifyTallyXML"  method="post" >
        <%@ include file="/content/common/path.jsp" %>
        <%@ include file="/content/common/message.jsp" %>
        <c:if test="${modifyTallyXMLPage != null}">
      <table align="center" border="0" cellpadding="0" cellspacing="0" width="500" id="bg" class="border">
           <c:forEach items="${modifyTallyXMLPage}" var="modifyTally">
                <tr>
                    <td colspan="4" align="center" class="contenthead" height="30"><div class="contenthead">Modify TallyXML Page</div> </td>
                </tr>
                <tr>
                    <input type="hidden" name="xmlId" value="<c:out value="${modifyTally.xmlId}"/>" >
                   <td class="text1" height="30">From Date</td>
                    <td class="text1" height="30">
                        <input type="text" class="textbox"  name="fromDate" value="<c:out value="${modifyTally.fromDate}"/>" >
                        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.modifyTallyXML.fromDate,'dd-mm-yyyy',this)"/>
                    </td>
                    <td class="text1" height="30">TO Date</td>
                    <td class="text1" height="30">
                        <input type="text" class="textbox"  readonly name="toDate" value="<c:out value="${modifyTally.toDate}"/>" >
                        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.modifyTallyXML.toDate,'dd-mm-yyyy',this)"/>
                    </td>
                </tr>
                <tr>
                    <td class="text1" height="30">XML Required Date</td>
                    <td class="text1" height="30">
                        <input type="text" class="textbox"  readonly name="reqDate" value="<c:out value="${modifyTally.requiredDate}"/>" >
                        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.modifyTallyXML.reqDate,'dd-mm-yyyy',this)"/>
                    </td>
                    <td class="text1" height="30">Type Of Data</td>
                    <td class="text1" height="30">
                        <select class="textbox" name="dataType"  height="30">
                            <option value="0">--Select--</option>
                            
        <c:if test="${modifyTally.dataType==1}">
            <option selected value="1">All Data</option>
            <option value="2">New Data</option>
        </c:if>
        <c:if test="${modifyTally.dataType==2}">
            <option  value="1">All Data</option>
            <option selected value="2">New Data</option>
        </c:if>
              
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" align="center" class="text2" height="30">
                        <input type="button" class="button" readonly name="Generate" value="Modify" onClick="searchSubmit();" >
                    </td>

                </tr>
           </c:forEach>
                </table>
      </c:if>