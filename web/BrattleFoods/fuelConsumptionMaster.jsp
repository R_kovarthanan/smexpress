<%--
    Document   : truckLoadMaster
    Created on : Oct 27, 2013, 5:04:09 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
         <% String menuPath = "Fuel  >>  Manage Fuel Consumption";
        request.setAttribute("menuPath", menuPath);
        %>
        <form name="fulecal"  method="post" >
        <div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
                <div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
                    <!-- pointer table -->
                    <table width="700" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                        <tr>
                            <td >
                                <%@ include file="/content/common/path.jsp" %>
                            </td></tr></table>
                    <!-- pointer table -->

                </div>
            </div>
            <br>
            <br>
            <br>
            <br>
            <br>
             <table align="center" border="0" cellpadding="0" cellspacing="0" width="980" id="bg" class="border">
            <tr>
                        <td colspan="2" width="980" align="center" class="contenthead" height="30"><div class="contenthead">Manage Fuel Consumption</div></td>
    
            </tr>
           
            <table width="815" align="center" border="0" id="table" class="sortable">

                <thead>
               
                    <tr height="30">
                        <th><h3>S.No</h3></th>
                        <th><h3>Mfr</h3></th>
                        <th><h3>Model</h3></th>
                        <th><h3>Vehicle Mileage Km/Ltr</h3></th>
                        <th><h3>Reefer Consumption Hrs/Ltr</h3></th>
                        <th><h3>Select</h3></th>
                    </tr>
                </thead>
                <tbody>
                    
                    <tr height="30">
                        <td align="left" class="text2">1</td>
                        <td align="left" class="text2">Ashok Leyland</td>
                        <td align="left" class="text2">2516</td>
                        <td align="left" class="text2"><input type="text" name="Mileage" class="textbox" value="6"></td>
                        <td align="left" class="text2"><input type="text" name="Mileage" class="textbox" value="4"></td>
                        <td align="left" class="text2"><input type="checkbox" name="edit" id="edit" /></td>
                    </tr>
                    <tr height="30">
                        <td align="left" class="text2">2</td>
                        <td align="left" class="text2">Tata</td>
                        <td align="left" class="text2">3118</td>
                        <td align="left" class="text2"><input type="text" name="Mileage" class="textbox" value="5"></td>
                        <td align="left" class="text2"><input type="text" name="Mileage" class="textbox" value="6"></td>
                        <td align="left" class="text2"><input type="checkbox" name="edit" id="edit" /></td>
                    </tr>
                </tbody>
            </table>
             </table>

            <table width="980" align="center" border="0" id="table" class="sortable">
                 <tr>
                    <td class="text2" height="30" colspan="2" align="center"><input align="center" type="button" class="button" value="Save"></td>
            </tr>

            </table>
           
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table",1);
            </script>
        </form>
    </body>
</html>
