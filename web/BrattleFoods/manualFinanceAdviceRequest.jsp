<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date,java.io.*,java.util.Enumeration"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {

                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });

            });

            $(function() {
                //	alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });

            });


            function replaceSpecialCharacters()
            {
                var content = document.getElementById("requestremarks").value;

                //alert(content.replace(/[^a-zA-Z0-9]/g,'_'));
                // content=content.replace(/[^a-zA-Z0-9]/g,'');
                content = content.replace(/[@&\/\\#,+()$~%'":*?<>{}]/g, ' ');
                document.getElementById("requestremarks").value = content;
                //alert("Repalced");

            }
        </script>


        <script language="javascript">
            function submitPage() {

                if (textValidation(document.approve.advancerequestamt, 'Request Amount')) {
                    return;
                }
                if (textValidation(document.approve.requeststatus, 'Request Status')) {

                    return;
                }
                if (textValidation(document.approve.requeststatus, 'Request Status')) {

                    return;
                }
                if (textValidation(document.approve.requestremarks, 'Request Remarks')) {

                    return;
                }
                document.getElementById("submitButton").style.display = 'none';             
                document.approve.action = '/throttle/manualAdvanceRequest.do';
                document.approve.submit();
                $("#mySubmit").hide();  
            }
            
            function setFocus() {
                document.approve.advancerequestamt.select();
            }
        </script>
    </head>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Finance Advice" text="Finance Advice"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
                <li class=""><spring:message code="hrms.label.Finance Advice" text=" Finance Advice"/></li>

            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">

                <body onload="se
                        tFocus();">
                    <form name="approve"  method="post" >
                        <%
                                    request.setAttribute("menuPath", "Advance >>  Request");
                                    String tripid = request.getParameter("tripid");
                                    String type = "M";
                        %>
                        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
                        <%--<%@ include file="/content/common/path.jsp" %>--%>
                        <%@ include file="/content/common/message.jsp" %>
                        <br>
                        <%
                                    Date today = new Date();
                                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                                    String startDates = sdf.format(today);                                    

                                    DecimalFormat df = new DecimalFormat("#0.00");

                                    String ovrtotalamount = (String) request.getAttribute("ovrtotalamount");
                                    String todayreqamt = (String) request.getAttribute("totalReqamount");
                                    String fcreqamt = (String) request.getAttribute("fcRequestamount");

                                    System.out.println("ovrtotalamount : "+ovrtotalamount);
                                    System.out.println("todayreqamt : "+todayreqamt);
                                    System.out.println("fcreqamt : "+fcreqamt);

                                    // String totalTodayReqamount = (String) request.getAttribute("totalTodayReqamount");
                                    // Double fcreqted = Double.parseDouble(fcreqamt);

                                    double totreq = 0;
                                    double todayallowed = 0;
                                    double totalamt = 0;
                                    double actamount = 0;

                                    if(fcreqamt!=null){
                                        actamount = Double.parseDouble(fcreqamt);
                                    }

                                    if(ovrtotalamount!=null){
                                        totalamt = Double.parseDouble(ovrtotalamount);
                                    }

                                    if(todayreqamt!=null){
                                        totreq = Double.parseDouble(todayreqamt);
                                    }

                                    todayallowed = actamount - totalamt;

                                    if(todayallowed<0){
                                        todayallowed=0;
                                    }

                        %>
                        <input type="hidden" name="consignmentOrderId" value="<c:out value="${consignmentOrderId}"/>"/>
                        <input type="hidden" name="tripCode" value="<c:out value="${tripCode}"/>"/>
                        <input type="hidden" name="totalTodayReqamount" value="0"/>
                        <input type="hidden" name="todayreqamount" value="<%=todayreqamt%>"/>
                        <input type="hidden" name="fcreqamount" value="<%=fcreqamt%>"/>
                        <input type="hidden" name="ovrtotalamount" value="<%=ovrtotalamount%>"/>
                        <input type="hidden" id="todayallow" name="todayallow" value="<%=todayallowed%>"/>
                        <input type="hidden" id="tripType" name="tripType" value="<c:out value="${tripType}"/>"/>
                        <input type="hidden" id="statusId" name="statusId" value="<c:out value="${statusId}"/>"/>
                        <input type="hidden" id="nextTrip" name="nextTrip" value="<c:out value="${nextTrip}"/>"/>

                        <table width="100" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">

                            <tr id="exp_table" >
                                <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                                    <div class="tabs" align="left" style="width:300;">
                                        <div id="first">
                                            <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">
                                                <tr id="exp_table" >
                                                    <td> <font color="white"><b>Actual Advance Paid:</b></font></td>
                                                            <c:if test = "${manualfinanceAdviceDetails != null}" >
                                                                <c:forEach items="${manualfinanceAdviceDetails}" var="FD">
                                                            <td align="right"> <c:out value="${FD.actualadvancepaid}"/></td>
                                                        </c:forEach>
                                                    </c:if>
                                                </tr>
                                                <tr id="exp_table" >
                                                    <td> <font color="white"><b>Already Requested :</b></font></td>
                                                    <td align="right"> <%=df.format(totreq)%> </td>
                                                </tr>
                                                <tr id="exp_table" >
                                                    <td> <font color="white"><b>You can Request upto:</b></font></td>
                                                    <td align="right"><%=df.format(todayallowed)%></td>
                                                </tr>
                                            </table>

                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <br/>

                        <c:if test = "${manualfinanceAdviceDetails != null}" >

                            <table class="table table-info mb30 table-hover" id="bg" >
                                <c:forEach items="${manualfinanceAdviceDetails}" var="FD">
                                    <thead> <tr align="center">
                                            <th colspan="4" >
                                                Advance Request</th>
                                        </tr></thead>
                                    <tr>
                                        <td  height="30">Customer Name</td>
                                        <td  height="30"><c:out value="${customerName}"/>                                
                                        </td>                                    
                                        <td  height="30">Cnote No</td>
                                        <td  height="30"><c:out value="${FD.cnoteName}"/>
                                            <input type="hidden" name="cnote" value='<c:out value="${FD.cnoteName}"/>'/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="30">Vendor Name</td>
                                        <td  height="30"><c:out value="${FD.vendorName}"/>                                
                                        </td>                                    
                                        <td  height="30">Trip Code</td>
                                        <td  height="30"><c:out value="${FD.tripcode}"/>                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="30">Vehicle Type</td>
                                        <td  height="30"><c:out value="${FD.vehicleTypeName}"/>
                                            <input type="hidden" name="vehicletype" value="<c:out value="${FD.vehicleTypeName}"/>"/>
                                        </td>

                                        <td  height="30">Vehicle No</td>
                                        <td  height="30"><c:out value="${FD.regNo}"/>
                                            <input type="hidden" name="vegno" value="<c:out value="${FD.regNo}"/>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="30">Route Name</td>
                                        <td  height="30"><c:out value="${FD.routeName}"/>
                                            <input type="hidden" name="routename" value="<c:out value="${FD.routeName}"/>"/>
                                        </td>
                                        <td  height="30">Driver Name</td>
                                        <td  height="30"><c:out value="${FD.driverName}"/>
                                            <input type="hidden" name="drivername" value="<c:out value="${FD.driverName}"/>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="30">Planned Date</td>
                                        <td  height="30"><c:out value="${FD.planneddate}"/>
                                            <input type="hidden" name="planneddate" value="<c:out value="${FD.planneddate}"/>"/>
                                        </td>
                                        <td  height="30">Actual Advance Paid</td>
                                        <td  height="30"><c:out value="${FD.actualadvancepaid}"/>
                                            <input type="hidden" name="actadvancepaid" value="<c:out value="${FD.actualadvancepaid}"/>"/>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td  height="30">Estimated Expense</td>
                                        <td  height="30"><c:out value="${FD.estimatedexpense}"/>
                                            <input type="hidden" name="estimatedexpense" value="<c:out value="${FD.estimatedexpense}"/>"/>
                                        </td>                                    
                                        <td><font color="red">*</font>Req.Advance Amt.</td>
                                        <td><input id="advancerequestamt" name="advancerequestamt" 
                                         onKeyPress="return onKeyPressBlockCharacters(event);" type="text" class="form-control" 
                                         style="width:250px;height:40px" value="<c:out value="${FD.estimatedadvance}"/>" onchange="checkamt();"></td>
                                    </tr>

                                    <input type="hidden" name="tripday" value="<c:out value="${FD.tripday}"/>"/>
                                    <input type="hidden" name="requeststatus" value="0"/>
                                    <input type="hidden" name="tripid" value="<%=tripid%>"/>
                                    <input type="hidden" name="batchType" value="<%=type%>"/>                                    
                                    <input type="hidden" name="vendoreFSId" value="<c:out value="${FD.vendoreFSId}"/>"/>
                                    <input type="hidden" name="vehicleTypeId" value="<c:out value="${FD.vehicleTypeId}"/>"/>                                    
                                    <input type="hidden" name="estimatedadvance" value="<c:out value="${FD.estimatedadvance}"/>"/>
                                    
                           
                                    <tr>
                                        <td  height="30"><font color="red">*</font>Request On</td>
                                        <td  height="30">
                                            <input name="requeston" type="text" class="datepicker , form-control" style="width:250px;height:40px" value="<%=startDates%>">
                                        </td>
                                  
                                        <td  height="30"><font color="red">*</font>Request Remarks</td>
                                        <td  height="30">
                                            <textarea class="form-control" style="width:300px;height:50px" name="requestremarks" id="requestremarks" 
                                            onKeyUp="return replaceSpecialCharacters();"></textarea></td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </c:if>
                        <br>
                        <div id="submitButton" style="display:block">
                        </div>
                        <center>
                            <input type="button" value="save" id="mySubmit" class="btn btn-success" onClick="submitPage();">
                            &emsp;
                            <!--                <input type="reset" class="button" value="Clear">-->
                        </center>

                        <script type="text/javascript">
                                function validateSpecialCharacters() {
                                var spclChars = "!@#$%^&*()/-:?<>+.~`'_="; // specify special characters
                                var content = document.getElementById("requestremarks").value;
                                    for (var i = 0; i < content.length; i++) {
                                        if (spclChars.indexOf(content.charAt(i)) != -1) {
                                        alert("Special characters are not allowed here.");
                                        document.getElementById("requestremarks").value = "";
                                    return false;
                                }
                            }
                            }
                        </script>
                        <script>
                                function checkamt() {
                                    true;
//                                    if (document.getElementById("nextTrip").value == "0") {
//                                        if (document.getElementById("advancerequestamt").value > parseInt(document.approve.todayallow.value)) {
//                                        alert("Maximum amount payable is Rs. " + document.approve.todayallow.value + "");
//                                        document.approve.advancerequestamt.value = 0;
//                                        document.getElementById("mySubmit").style.display = "none";
//                                    document.approve.advancerequestamt.focus();
//                                        } else {
//                                    document.getElementById("mySubmit").style.display = "block";
//                                    }
//                                } else if (document.getElementById("nextTrip").value == "1") {
//                                    if (document.getElementById("advancerequestamt").value > parseInt(10000)) {
//                                        alert("Maximum amount payable is Rs. 10,000 per day");
//                                        document.approve.advancerequestamt.value = 0;
//                                        document.getElementById("mySubmit").style.display = "none";
//                                        document.approve.advancerequestamt.focus();
//                                    } else {
//                                        document.getElementById("mySubmit").style.display = "block";
//                                    }
                                //                                }

                            }
                            }
                        </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>