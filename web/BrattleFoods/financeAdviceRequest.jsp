<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<html>
    <head>
        <!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

        </script>
    </head>
    <script language="javascript">
        function submitPage() {

                document.customer.action = '/throttle/handlefinanceRequest.do';
                document.customer.submit();
        }
    </script>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.FinanceAdviceRequest" text="FinanceAdviceRequest"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
                <li class=""><spring:message code="hrms.label.FinanceAdviceRequest" text="FinanceAdviceRequest"/></li>

            </ol>
        </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
    <body>
        
        
        <form name="customer" method="post" >
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
            
            <table class="table table-info mb30 table-hover" id="bg" >
		    <thead>
		<tr>
		    <th colspan="2" height="30" >View Finance Request</th>
		</tr>
               </thead>
               
            
                                <table class="table table-info mb30 table-hover"  >
                           <%!
                           public String NullCheck(String inputString)
                                {
                                        try
                                        {
                                                if ((inputString == null) || (inputString.trim().equals("")))
                                                                inputString = "";
                                        }
                                        catch(Exception e)
                                        {
                                                                inputString = "";
                                        }
                                        return inputString.trim();
                                }
                           %>

                           <%

                            String today="";
                            String fromday="";

                            fromday = NullCheck((String) request.getAttribute("fromdate"));
                            today = NullCheck((String) request.getAttribute("todate"));

                            if(today.equals("") && fromday.equals("")){
                            Date dNow = new Date();
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(dNow);
                            cal.add(Calendar.DATE, 0);
                            dNow = cal.getTime();

                            Date dNow1 = new Date();
                            Calendar cal1 = Calendar.getInstance();
                            cal1.setTime(dNow1);
                            cal1.add(Calendar.DATE, -6);
                            dNow1 = cal1.getTime();

                            SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy");
                            today = ft.format(dNow);
                            fromday = ft.format(dNow1);
                            }

            %>
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  style="width:250px;height:40px"  onclick="ressetDate(this);" value="<%=fromday%>"></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker"  style="width:250px;height:40px"  onclick="ressetDate(this);" value="<%=today%>"></td>
                                        <td>&nbsp;</td>
                                        <td><input type="button" class="btn btn-success" name="search" onClick="submitPage();" value="Search"></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            
            

            <c:if test = "${financeRequest != null}" >
               <table class="table table-info mb30 table-hover " id="table" >
                    <thead>
                        <tr height="40">
                            <th>S.No</th>
                            <th>Advice Date</th>
                            <th>Vehicle No</th>
                            <th>Trip Code</th>
                            <th>Cnote</th>
                            <th>Advice Type</th>
                            <th>Trip Day</th>
                            <th>Estimated Amt.</th>
                            <th>Requested Amt.</th>                            
                            <th>View</th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0,sno = 1;%>
                        <c:forEach items="${financeRequest}" var="fd">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <tr height="30">
                                <td align="left" ><%=sno%></td>
                                <td align="left" ><c:out value="${fd.advicedate}"/> </td>
                                <td align="left" ><c:out value="${fd.regno}"/> </td>
                                <td align="left" ><c:out value="${fd.tripCode}"/> </td>
                                <td align="left" ><c:out value="${fd.cnoteName}"/> </td>
                                <td align="left" >
                                <c:if test="${(fd.batchType=='b') || (fd.batchType=='B')}" >
                                    Batch
                                    </c:if>
                                    <c:if test="${(fd.batchType=='a') || (fd.batchType=='A')}" >
<!--                                    Adhoc-->
                                    Auto
                                    </c:if>
                                    <c:if test="${(fd.batchType=='m') || (fd.batchType=='M')}" >
                                    Manual
                                    </c:if>
                                </td>

                                <td align="left" ><c:out value="${fd.tripday}"/></td>

                                <td align="left" >
                                    <%--<c:if test="${(fd.batchType=='m') || (fd.batchType=='M')}" ></c:if>--%>
                                    <%--<c:if test="${(fd.batchType!='m') || (fd.batchType!='M')}" ></c:if>--%>                                    
                                    
                                 <c:out value="${fd.estimatedadvance}"/>
                                </td>
                                
                                <td align="left" ><c:out value="${fd.requestedadvance}"/></td>                                
                                <td align="left" >
                                    <c:if test="${(fd.approvalstatus==0)}" >
                                    <a href="/throttle/viewAdviceApprove.do?tripAdvaceId=<c:out value="${fd.tripAdvaceId}"/>&batchType=<c:out value="${fd.batchType}"/>&tripId=<c:out value="${fd.tripId}"/>">view details</a>
                                    </c:if>
                                    <c:if test="${(fd.approvalstatus==1) }" >
                                        APPROVED
                                    </c:if>
                                    <c:if test="${(fd.approvalstatus==2) }" >
                                        REJECTED
                                    </c:if>
                                </td>

                            </tr>
                        <%
                                   index++;
                                   sno++;
                        %>
                    </c:forEach>

                    </tbody>
                </table>
            </c:if>
            <br/>
            <br/>
            <br/>
            
            <c:if test = "${vehicleAdvanceRequest != null}" >
                     <table class="table table-info mb30 table-hover " id="table1" style="display:none">
                    <thead>
                        <tr height="40">
                            <th>S.No</th>
                            <th>Advice Date</th>
                            <th>Vehicle No</th>
                            <th>Advice Type</th>
                            <th>Requested Amt.</th>                            
                            <th>View</th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0,sno = 1;%>
                        <c:forEach items="${vehicleAdvanceRequest}" var="vehicle">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <c:if test="${(vehicle.approvalStatus != null) }" >
                            <tr height="30">
                                <td align="left" ><%=sno%></td>
                                <td align="left" ><c:out value="${vehicle.advanceDate}"/> </td>
                                <td align="left" ><c:out value="${vehicle.regNo}"/> </td>
                                <td align="left" >
                                Vehicle Driver
                                </td>
                                <td align="left" ><c:out value="${vehicle.requestedAdvance}"/></td>                                
                                <td align="left" >
                                    <c:if test="${(vehicle.paidStatus=='N') && (vehicle.approvalStatus==1)}" >
                                    <a href="/throttle/viewVehicleDriverAdvanceApprove.do?vehicleDriverAdvanceId=<c:out value="${vehicle.vehicleDriverAdvanceId}"/> &vehicleNo=<c:out value="${vehicle.regNo}"/>&vehicleId=<c:out value="${vehicle.vehicleId}"/>&advanceAmount=<c:out value="${vehicle.requestedAdvance}"/>&expenseType=<c:out value="${vehicle.expenseType}"/>&advanceDate=<c:out value="${vehicle.advanceDate}"/>&primaryDriver=<c:out value="${vehicle.primaryDriverName}"/>">view details</a>
                                    </c:if>
                                    <c:if test="${(vehicle.paidStatus=='Y') || (vehicle.approvalStatus==2) }" >
                                        APPROVED
                                    </c:if>
                                </td>
                            </c:if>
                            </tr>
                        <%
                                   index++;
                                   sno++;
                        %>
                    </c:forEach>

                    </tbody>
                </table>
            </c:if>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
                setFilterGrid("table1");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>

        </form>
    </body>


</div>
            </div>
        </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
