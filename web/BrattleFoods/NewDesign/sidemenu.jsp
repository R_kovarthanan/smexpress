
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<!--      <h5 class="sidebartitle">Navigation</h5>-->

<% Object rolId = session.getAttribute("RoleId");%>
<%--<%out.println("rolId==="+rolId);%>--%>
<ul class="nav nav-pills nav-stacked nav-bracket">

    <li class="nav-parent"><a href="#"><i class="fa fa-dashboard"></i> <span><spring:message code="subMenus.label.Dashboard" text="Dashboard"/></span></a>
        <ul class="children" >
            <li><a href="/throttle/dashboardOperation.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="dashboard.subMenus.label.Operations" text="Operations"/></a></li>
            <li><a href="/throttle/dashboardWorkshop.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="dashboard.subMenus.label.Workshop" text="Workshop"/></a></li>
        </ul>
    </li>

    <li class="nav-parent"><a href=""><i class="fa fa-user"></i> <span><spring:message code="subMenus.label.HRMS" text="HRMS"/></span></a>
        <ul class="children">
            <li><a href="/throttle/viewCompany1.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="hrms.subMenus.label.Company" text="Company"/> </a></li>
            <li><a href="/throttle/manageDepartment.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="hrms.subMenus.label.Department" text="Department"/></a></li>
            <li><a href="/throttle/viewDesign.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="hrms.subMenus.label.Designation" text="Designation"/></a></li>
            <li><a href="/throttle/handleEmpViewPage.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="hrms.subMenus.label.Employees" text="Employees"/></a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href=""><i class="fa fa-gears"></i> <span><spring:message code="subMenus.label.Settings" text="Settings"/></span></a>
        <ul class="children">
            <li><a href="/throttle/alterPassword.do?menuClick=1" ><spring:message code="settings.subMenus.label.ChangePassword" text="ChangePassword"/></a></li>
            <li><a href="/throttle/recoverPassword.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManagePassword" text="ManagePassword"/></a></li>
            <li><a href="/throttle/viewuser.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManageUsers" text="ManageUsers"/></a></li>
            <li><a href="/throttle/allroles.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManageRoles" text="ManageRoles"/></a></li>
            <li><a href="/throttle/userroles.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManageUserRoles" text="ManageUserRoles"/></a></li>
            <li><a href="/throttle/activerole.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManageRoleFunctions" text="ManageRoleFunctions"/></a></li>

        </ul>
    </li>
<!--    <li class="nav-parent"><a href="#"><i class="fa fa-bus"></i> <span><spring:message code="subMenus.label.Trucks" text="Trucks"/></span></a>
        <ul class="children">
            <li><a href="/throttle/handleViewAxle.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.AxleMaster" text="default text"/></a></li>
            <li><a href="/throttle/viewType.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.VehicleType" text="default text"/></a></li>
            <li><a href="/throttle/viewMfr.do?fleetType=1&menuClick=1"><i class="fa fa-cog"></i><spring:message code="trucks.subMenus.label.Make" text="default text"/> </a></li>
            <li><a href="/throttle/viewModelPage.do?fleetType=1&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.Model" text="default text"/></a></li>
            <li><a href="/throttle/searchVehiclePage.do?fleetTypeId=1&listId=&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.Vehicles" text="default text"/></a></li>
            <li><a href="/throttle/resetVehicleKM.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="trucks.subMenus.label.ResetVehicleKM" text="default text"/> </a></li>
            <li><a href="/throttle/searchVehiclePage.do?listId=5&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.VehiclePurchase" text="default text"/> </a></li>
            <li><a href="/throttle/searchVehiclePage.do?listId=1&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.VehicleInsurance" text="default text"/></a></li>
            <li><a href="/throttle/searchVehiclePage.do?listId=3&menuClick=1"><i class="fa fa-cog"></i><spring:message code="trucks.subMenus.label.VehicleRegistration" text="default text"/> </a></li>
            <li><a href="/throttle/viewType.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="trucks.subMenus.label.VehicleInspection" text="default text"/> </a></li>
            <li><a href="/throttle/viewType.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.VehicleRoadPermit" text="default text"/></a></li>
        </ul>
    </li>-->
<!--    <li class="nav-parent"><a href="#"><i class="fa fa-truck"></i> <span><spring:message code="subMenus.label.Trailers" text="Trailers"/></span></a>
        <ul class="children">
            <li><a href="/throttle/handleViewTrailerTypePage.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trailers.subMenus.label.TrailerType" text="default text"/></a></li>
            <li><a href="/throttle/viewMfr.do?fleetType=2&menuClick=2"><i class="fa fa-cog"></i> <spring:message code="trailers.subMenus.label.Make" text="default text"/></a></li>
            <li><a href="/throttle/viewModelPage.do?fleetType=2&menuClick=2"><i class="fa fa-cog"></i><spring:message code="trailers.subMenus.label.Model" text="default text"/> </a></li>
            <li><a href="/throttle/searchTrailerPage.do?fleetTypeId=2&listId=&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trailers.subMenus.label.Trailers" text="default text"/></a></li>
            <li><a href="/throttle/searchVehiclePage.do?listId=5&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trailers.subMenus.label.TrailerPurchase" text="default text"/> </a></li>
        </ul>
    </li>-->
<!--    <li class="nav-parent"><a href="#"><i class="fa fa-retweet"></i> <span><spring:message code="subMenus.label.Accidents" text="Accidents"/></span></a>
        <ul class="children">
            <li><a href="/throttle/handleVehicleAccident.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="accidents.subMenus.label.Accidents" text="default text"/> </a></li>
            <li><a href="/throttle/vehicleAccidentDetails.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="accidents.subMenus.label.AccidentDetails" text="default text"/> </a></li>
            <li><a href="/throttle/vehicleAccidentHistory.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="accidents.subMenus.label.AccidentHistory" text="default text"/>  </a></li>
        </ul>
    </li>-->
    <li class="nav-parent"><a href="#"><i class="fa fa-users"></i> <span><spring:message code="subMenus.label.Vendors" text="Vendors"/></span></a>
        <ul class="children">

            <li><a href="/throttle/manageVendorPage.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="vendors.subMenus.label.Vendors" text="Vendors"/> </a></li>
            <li><a href="/throttle/manageVendorItemConfigPage.do?menuClick=1"><i class="fa fa-cog"></i>Config Vendor Items </a></li>
            <li><a href="/throttle/handleVendorPaymentPage.do?menuClick=1"><i class="fa fa-cog"></i>Vendor Payment </a></li>

            <!--<li><a href="/throttle/manageFleetVendorPage.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="vendors.subMenus.label.FleetVendorContract" text="default text"/></a></li>-->
        </ul>
    </li>
    
    <li class="nav-parent"><a href="#"><i class="fa fa-bus"></i> <span><spring:message code="subMenus.label.Vehicles" text="Vehicles"/></span></a>
        <ul class="children">
            <li><a href="/throttle/viewMfr.do" target="frame">&nbsp;MFR</a></li>
             <li><a href="/throttle/viewModelPage.do" target="frame">&nbsp;Model</a></li>
              <li><a href="/throttle/viewType.do" target="frame">&nbsp;Vehicle Type</a></li>
               <li><a href="/throttle/searchVehiclePage.do" target="frame">&nbsp;Manage Vehicle</a></li>
                 <li><a href="/throttle/resetVehicleKM.do" target="frame">&nbsp;Reset Vehicle KM</a></li>
                                        <li><a href="/throttle/handleVehicleFinance.do" target="frame">&nbsp;Vehicle Financing Details</a></li>
                                        <li><a href="/throttle/searchVehicleInsurance.do" target="frame">&nbsp;Vehicle Insurance Update</a></li>
                                        <li><a href="/throttle/searchVehicleAMC.do" target="frame">&nbsp;Vehicle AMC Update</a></li>
                                        <li><a href="/throttle/searchVehicleRoadTax.do" target="frame">&nbsp;Vehicle Road Tax Update</a></li>
                                        <li><a href="/throttle/searchVehicleFc.do" target="frame">&nbsp;Vehicle Fc Update</a></li>
                                        <li><a href="/throttle/handleVehicleAccident.do" target="frame">&nbsp;Vehicle Accident Update</a></li>
                                        <li><a href="/throttle/searchVehiclePermit.do" target="frame">&nbsp;Vehicle Permit Update</a></li>
                                        <li><a href="/throttle/viewVehicleProfile.do" target="frame">&nbsp;Vehicle Profile</a></li>
                                         </ul>
    </li>
    
    
   
    <li class="nav-parent"><a href="#"><i class="fa fa-angle-double-up"></i> <span><spring:message code="subMenus.label.SecondaryOperation" text="SecondaryOperation"/></span></a>
        <ul class="children">
    
                                            <li><a href="/throttle/handleViewCustomer.do?customerType=2" target="frame">&nbsp;Customers </a></li>
                                            <li><a href="/throttle/handleEditViewCustomer.do" target="frame">&nbsp;Routes</a></li>
                                            <li><a href="/throttle/vehicleDriverPlanning.do" target="frame">&nbsp;Vehicle &amp; Driver Mapping</a></li>
                                            <li><a href="/throttle/secondaryTripSheduleView.do" target="frame">&nbsp;Schedule Trip</a></li>
                                            <li><a href="/throttle/emptyTripPlanning.do?tripType=2" target="frame">&nbsp;Empty Trip</a></li>
                                            <li><a href="/throttle/secondaryDriverSettlement.do" target="frame">&nbsp; Secondary Driver Settlement</a></li>
                                            <li><a href="/throttle/viewSecondaryDriverSettlement.do" target="frame">&nbsp; View Secondary Settlement</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=2&admin=No" target="frame">&nbsp;View Trip Sheet</a></li>
                                            <li><a href="/throttle/viewVehicleDriverAdvance.do?usageTypeId=1" target="frame">&nbsp;VehicleDriverAdvance</a></li>
                                            <li><a href="/throttle/handlefinanceAdvice.do?tripType=2" target="frame">&nbsp;Finance Advice (Adv)</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=6&tripType=2&admin=No" target="frame">&nbsp;Trip Assign Vehicle</a></li>
                                            <!--                                            <li><a href="/throttle/viewTripSheets.do?statusId=7&tripType=2" target="frame">&nbsp;Trip Freeze</a></li>-->
                                            <li><a href="/throttle/viewTripSheets.do?statusId=8&tripType=2&admin=No" target="frame">&nbsp;Trip Un Freeze</a></li>
                                            <!--                                            <li><a href="/throttle/viewTripSheets.do?statusId=8&tripType=2" target="frame">&nbsp;Trip Pre Start</a></li>-->
                                            <li><a href="/throttle/viewTripSheets.do?statusId=9&tripType=2&admin=No" target="frame">&nbsp;Trip Start</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=2&admin=No" target="frame">&nbsp;Trip End</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=2&admin=No" target="frame">&nbsp;Trip Closure</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=2&admin=Yes" target="frame">&nbsp;Trip FC Closed</a></li>

                                            <li><a href="/throttle/secondaryCustomerApprovalMailId.do?" target="frame">&nbsp;Secondary Approval Mail</a></li>
                                        </ul>
                                    </li>
    
                                    
                                    
                                     <li class="nav-parent"><a href="#"><i class="fa fa-angle-double-up"></i> <span><spring:message code="subMenus.label.Sales/Ops" text="Sales/Ops"/></span></a>
        <ul class="children">
            <li><a href="/throttle/handleViewCustomer.do?menuClick=1" ><i class="fa fa-cog"></i>&nbsp;Customer Master</a></li>
            <li><a href="/throttle/consignmentNote.do?menuClick=1" ><i class="fa fa-cog"></i>&nbsp;C-Note Create</a></li>
            <li><a href="/throttle/consignmentNoteView.do?menuClick=1" ><i class="fa fa-cog"></i>&nbsp;C-Note View</a></li>

        </ul>
    </li>
    
          <li class="nav-parent"><a href="#"><i class="fa fa-angle-double-up"></i> <span><spring:message code="subMenus.label.Operations" text="Operations"/></span></a>
          <ul class="children">
         <li><a href="/throttle/handleLatestUpdates.do?param=search" target="frame">&nbsp;Latest Updates</a></li>
                                            <li><a href="/throttle/viewTicketingDetails.do" target="frame">&nbsp;Ticketing</a></li>
                                            <li><a href="/throttle/WOstatus.do" target="frame">&nbsp;Work Order</a></li>
                                            <!--<li><a href="/throttle/manageSecurityPage.do" target="frame">&nbsp;Security</a></li>
                                            <li><a href="/throttle/freeServiceDues.do" target="frame">&nbsp;Free Services Dues</a></li>-->
                                            <li><a href="/throttle/periodicServiceDues.do" target="frame">&nbsp;Periodic Service Dues</a></li>
                                            <li><a href="/throttle/userCustomer.do" target="frame">&nbsp;Manage User Customer</a></li>
                                            <li><a href="/throttle/tripEmailSettings.do" target="frame">&nbsp;TripEmailSettings</a></li>
                                            <li><a href="/throttle/viewStandardCharge.do" target="frame">&nbsp;StandardCharge Master</a></li>
                                            <li><a href="/throttle/viewCheckList.do" target="frame">&nbsp;Check List Master</a></li>
                                            <li><a href="/throttle/viewAlerts.do" target="frame">&nbsp;Alerts</a></li>
                                            <li><a href="/throttle/viewProductCategory.do" target="frame">&nbsp;Product Category Master</a></li>
                                            <li><a href="/throttle/viewConfigMaster.do" target="frame">&nbsp;Config Master</a></li>
                                            <li><a href="/throttle/viewCityMaster.do" target="frame">&nbsp;&nbsp;City Master</a></li>
                                            <li><a href="/throttle/viewRouteDetails.do" target="frame">&nbsp;Route Master</a></li>
                                            <li><a href="/throttle/vehicleDriverPlanning.do" target="frame">&nbsp;Vehicle &amp; Driver Mapping</a></li>
                                            <li><a href="/throttle/handleViewPrimaryDriver.do" target="frame">&nbsp;Manage Driver</a></li>
                                            <li><a href="/throttle/vehicleBPCLCardMapping.do" target="frame">&nbsp;Vehicle &amp; BPCL Card Mapping</a></li>
                                            <li><a href="/throttle/viewVehicleAvailability.do?tripType=1" target="frame">&nbsp;&nbsp;Vehicle Availability</a></li>
                                            <li><a href="/throttle/tripPlanning.do?tripType=1" target="frame">&nbsp;Trip Planning</a></li>
                                            <li><a href="/throttle/emptyTripPlanning.do?tripType=1" target="frame">&nbsp;Empty Trip</a></li>
                                            <li><a href="/throttle/viewTripPlannedVehicle.do" target="frame">&nbsp;Trip Creation (XLS Upload)</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=1&admin=No" target="frame">&nbsp;View Trip Sheet</a></li>
                                            <li><a href="/throttle/viewVehicleDriverAdvance.do?usageTypeId=2" target="frame">&nbsp;VehicleDriverAdvance</a></li>
                                            <li><a href="/throttle/handlefinanceAdvice.do?tripType=1" target="frame">&nbsp;Finance Advice (Adv)</a></li>
                                            <li><a href="/throttle/handlefinanceRequest.do" target="frame">&nbsp;Finance Advice Approval</a></li>
                                            <li><a href="/throttle/handleTripClosureRequest.do" target="frame">&nbsp;Trip Closure Approval</a></li>
                                            <li><a href="/throttle/manualEmptyTripApproval.do" target="frame">&nbsp;Empty Trip Approval</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=6&tripType=1&admin=No" target="frame">&nbsp;Trip Assign Vehicle</a></li>
                                            <!--                                            <li><a href="/throttle/viewTripSheets.do?statusId=7&tripType=1" target="frame">&nbsp;Trip Freeze</a></li>-->
                                            <li><a href="/throttle/viewTripSheets.do?statusId=8&tripType=1&admin=No" target="frame">&nbsp;Trip Un Freeze</a></li>
                                            <!--                                            <li><a href="/throttle/viewTripSheets.do?statusId=8&tripType=1" target="frame">&nbsp;Trip Pre Start</a></li>-->
                                            <li><a href="/throttle/viewTripSheets.do?statusId=8&tripType=1&admin=No" target="frame">&nbsp;Trip Start</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=18&tripType=1&admin=No" target="frame">&nbsp;Trip WFU</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=1&admin=No" target="frame">&nbsp;Trip End</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=1&admin=No" target="frame">&nbsp;Trip Ended</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=100&tripType=1&admin=No" target="frame">&nbsp;Trip POD</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=1&admin=No" target="frame">&nbsp;Trip Closure</a></li>
                                            <li><a href="/throttle/viewTripSheetsForChallan.do?tripType=1&admin=No&documentRequired=Y" target="frame">&nbsp;Upload Expense Bill Copy</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=1&admin=Yes" target="frame">&nbsp;Trip Closure Admin</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=13&tripType=1&admin=No" target="frame">&nbsp;Trip Settlement</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=14&tripType=1&admin=No" target="frame">&nbsp;View Settled Trips</a></li>
                                            <li><a href="/throttle/handlePrimaryDriverSettlement.do" target="frame">&nbsp;Driver Settlement</a></li>
                                            <li><a href="/throttle/emptyTripMerging.do" target="frame">&nbsp;Empty Trip Merging</a></li>
                                            <li><a href="/throttle/uploadBPCLData.do" target="frame">&nbsp;Upload BPCL Txn</a></li>
                                            <li><a href="/throttle/mailNotDelivered.do" target="frame">&nbsp;Mail Not Delivered</a></li>
                                            <li><a href="/throttle/mailDelivered.do" target="frame">&nbsp;Mail Delivered</a></li>
                                      </ul>
    </li>       
    
    
    
                                            
    <li class="nav-parent"><a href="#"><i class="fa fa-money"></i> <span><spring:message code="subMenus.label.Finance" text="Finance"/></span></a>
        <ul class="children">
             <li><a href="/throttle/uploadCustomeroutStanding.do" target="frame">&nbsp;Upload Customer Outstanding</a></li>
                      <li><a href="/throttle/viewclosedtrip.do" target="frame">&nbsp;Generate Billing</a></li>
                      <li><a href="/throttle/viewbillpageforsubmit.do" target="frame">&nbsp; Submit Bill </a></li>
                      <li><a href="/throttle/viewbillpage.do" target="frame">&nbsp; View Bill </a></li>
                      <li><a href="/throttle/customerBillPayment.do" target="frame">&nbsp; Customer Bill Payment</a></li>
        </ul>
              </li>
   <li class="nav-parent"><a href="#"><i class="fa fa-money"></i> <span><spring:message code="subMenus.label.SecondaryBilling" text="SecondaryBilling"/></span></a>
        <ul class="children">
                                            <li><a href="/throttle/viewClosedTripForSecondaryBilling.do" target="frame">&nbsp;Generate Secondary Billing</a></li>
                                            <li><a href="/throttle/viewbillpageforsubmit.do" target="frame">&nbsp; Submit Bill </a></li>
                                            <li><a href="/throttle/viewbillpage.do" target="frame">&nbsp; View Bill </a></li>
                                            <li><a href="/throttle/customerBillPayment.do" target="frame">&nbsp; Customer Bill Payment</a></li>
                                        </ul>
                                    </li>
                                    
              <li class="nav-parent"><a href="#"><i class="fa fa-money"></i> <span><spring:message code="subMenus.label.Reports" text="Reports"/></span></a>
        <ul class="children">
<!--                                            <li><a href="/throttle/handleLatestUpdates.do?param=search" target="frame">&nbsp;Latest Updates</a></li>-->
                                            <li><a href="/throttle/handleAccountsReceivableReport.do?param=search" target="frame">&nbsp;Accounts Receivable</a></li>
                                            <li><a href="/throttle/handleVehicleWiseProfitReport.do?param=search" target="frame">&nbsp;Vehicle Wise Profit</a></li>
                                            <li><a href="/throttle/handleCustomerWiseProfitReport.do?param=search" target="frame">&nbsp; Customer wise Profit</a></li>
                                            <li><a href="/throttle/handleVehicleUtilizationReport.do?param=search" target="frame">&nbsp; Vehicle Utilization Report</a></li>
                                            <li><a href="/throttle/handleTripWiseProfitReport.do?param=search" target="frame">&nbsp; Trip Wise Profitability Report(Closure)</a></li>
                                            <li><a href="/throttle/handleTripWiseProfitReportEndStatus.do?param=search" target="frame">&nbsp; Trip Wise Profitability Report(End)</a></li>
                                            <li><a href="/throttle/handleGPSLogReport.do?param=search" target="frame">&nbsp; GPS Log Report</a></li>
                                            <li><a href="/throttle/handleDriverSettlementDetails.do?param=search" target="frame">&nbsp; Driver Settlement Report</a></li>
                                            <li><a href="/throttle/handleBPCLTransactionDetails.do?param=search" target="frame">&nbsp; BPCL Transaction Report</a></li>
                                            <li><a href="/throttle/viewFinanceAdviceDetais.do?param=search" target="frame">&nbsp; Finance  Advice Report</a></li>
                                            <li><a href="/throttle/viewGpsStatusDetais.do?param=search" target="frame">&nbsp; GPS Status Report</a></li>
                                            <li><a href="/throttle/viewWFLWFUDetais.do?param=search" target="frame">&nbsp;WFL & WFU Mail Sent</a></li>
                                            <li><a href="/throttle/dashboardReport.do?param=search" target="frame">&nbsp;Dashboard</a></li>
                                            <li><a href="/throttle/handleViewTripSheet.do?param=search" target="frame">&nbsp;Trip Sheet Report</a></li>
                                            <li><a href="/throttle/handleViewTripSheetWfl.do?param=search" target="frame">&nbsp;Trip Sheet Report With Wfl</a></li>
                                            <li><a href="/throttle/handleFCWiseTripSummary.do?param=search" target="frame">&nbsp;FC Trip Summary</a></li>
                                            <li><a href="/throttle/handleMarginWiseReport.do?param=search" target="frame">&nbsp;Margin Trip Summary</a></li>
                                            <li><a href="/throttle/handleMonthWiseEmptyRunSummary.do?param=search" target="frame">&nbsp;Empty Run Summary</a></li>
                                            <li><a href="/throttle/handleJobcardSumary.do?param=search" target="frame">&nbsp;Jobcard Summary</a></li>
                                            <li><a href="/throttle/handleTripMergingDetails.do?param=search" target="frame">&nbsp; Trip Merging Report</a></li>
                                            <li><a href="/throttle/handleVehicleOdometerReading.do?param=search" target="frame">&nbsp;Vehicle Current Odometer Reading Report</a></li>
                                            <li><a href="/throttle/handleCustomerWiseMergingProfitReport.do?param=search" target="frame">&nbsp; Customer&nbsp;Wise&nbsp;Profit&nbsp;Merging</a></li>
                                            <li><a href="/throttle/handleTripExtraExpenseReport.do?param=search" target="frame">&nbsp;Trip Extra Expense Report</a></li>
                                            <li><a href="/throttle/handleVehicleDriverAdvance.do?param=search" target="frame">&nbsp;Vehicle Driver Advance Report</a></li>
                                            <li><a href="/throttle/handleToPayCustomerTripDetails.do?param=search" target="frame">&nbsp;To pay customer trip Details Report</a></li>
                                            <li><a href="/throttle/handleTripVmrReport.do?param=search" target="frame">&nbsp;Trip VMR Report</a></li>
                                            <li><a href="/throttle/handleTripBudgetReport.do?param=search" target="frame">&nbsp;FC Performance Report</a></li>
                                            <li><a href="/throttle/handleAccountMgrPerformanceReport.do?param=search" target="frame">&nbsp;A/C Mgr Performance Report</a></li>
                                            <li><a href="/throttle/handleRNMExpenseReport.do?param=search" target="frame">&nbsp;R&M Spent Analysis Report</a></li>
                                            <li><a href="/throttle/handleTyerExpenseReport.do?param=search" target="frame">&nbsp;Tyre  Analysis Report</a></li>
                                            <li><a href="/throttle/handleVehileUtilize.do?param=search" target="frame">&nbsp;Vehicle Utilize Report</a></li>
                                            <li><a href="/throttle/handleRNMReport.do?param=search" target="frame">&nbsp;R&M Report New</a></li>
                                            <li><a href="/throttle/handleTyreAgeingeReport.do?param=search" target="frame">&nbsp;Tyre Ageing Report</a></li>
                                            <li><a href="/throttle/handleBrokerCustomerReport.do?param=search" target="frame">&nbsp;Broker Customer Report</a></li>
                                            <!--<li><a href="/throttle/handleTripWiseProfitReport.do" target="frame">&nbsp; Trip wise Profit</a></li>
                                           <li><a href="/throttle/handleTripWiseProfit1.do" target="frame">&nbsp; Trip wise Profit-1</a></li>-->
                                        </ul>
                                    </li>
                                   <li class="nav-parent"><a href="#"><i class="fa fa-money"></i> <span><spring:message code="subMenus.label.MIS" text="MIS"/></span></a>
                                       <ul class="children">
                                            <li><a href="/throttle/handlecustomerrevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Customer Wise Revenue</a></li>
                                            <li><a href="/throttle/customerrevenuebillwise.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Bill Wise Revenue </a></li>
                                            <li><a href="/throttle/revenuefcwise.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp; FC Wise Revenue </a></li>
                                            <li><a href="/throttle/vehiclerevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Wise Revenue </a></li>
                                            <li><a href="/throttle/vehicletyperevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Type Wise Revenue</a></li>
                                            <li><a href="/throttle/accountreceivablerevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Customer Wise A.R</a></li>
                                            <li><a href="/throttle/accountreceivablebillwiserevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Bill Wise A.R</a></li>
                                            <li><a href="/throttle/totaloperationcost.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Total Operation Cost</a></li>
                                            <li><a href="/throttle/totaloperationcostperbill.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Total Operation Cost (Bill)</a></li>
                                            <li><a href="/throttle/fuelcostperbill.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Fuel Cost</a></li>
                                            <li><a href="/throttle/customerwiseprofit.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Customer Wise Profit</a></li>
                                            <li><a href="/throttle/vehiclewiseprofit.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Wise Profit</a></li>
                                            <li><a href="/throttle/fcwiseprofit.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;FC Wise Profit</a></li>
                                            <li><a href="/throttle/consolidatedreportpermonth.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Month Wise Consolidated</a></li>
                                            <li><a href="/throttle/consolidatedreportperbill.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Month Wise Consolidated (Bill)</a></li>
                                        </ul>
                                    </li>                      
                      <li class="nav-parent"><a href="#"><i class="fa fa-beer"></i> <span><spring:message code="subMenus.label.Fuel" text="Fuel"/></span></a>
        <ul class="children">

            <li><a href="/throttle/viewVehicleMilleage.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="fuel.subMenus.label.MilleageConfiguration" text="MilleageConfiguration"/>   </a></li>
            <li><a href="/throttle/viewFuelPriceMaster.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="fuel.subMenus.label.FuelPrice" text="FuelPrice"/>   </a></li>
            <!--<li><a href="/throttle/vehicleTypeOperationgCostMaster.do?menuClick=1" ><i class="fa fa-cog"></i>VehicleTypeOperationgCost </a></li>-->
        </ul>
    </li> 
                                    
    <!--
    <li class="nav-parent"><a href="#"><i class="fa fa-motorcycle"></i> <span><spring:message code="subMenus.label.ServicePlan" text="ServicePlan"/> </span></a>
        <ul class="children">
            <li><a href="/throttle/manageService.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="serviceplan.subMenus.label.ManageServiceType" text="default text"/></a></li>
            <li><a href="/throttle/manageSection.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="serviceplan.subMenus.label.ManageSection" text="default text"/></a></li>
            <li><a href="/throttle/manageProblemAct.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="serviceplan.subMenus.label.ManageProblem" text="default text"/></a></li>
            <li><a href="/throttle/manageProblemActivity.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="serviceplan.subMenus.label.Activities" text="default text"/> </a></li>
            <li><a href="/throttle/handleCheckListConfigPage.do?menuClick=1"><i class="fa fa-cog"></i>checkList </a></li>
            <li><a href="/throttle/manageMaintenance.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="serviceplan.subMenus.label.PeriodicService" text="default text"/> </a></li>
                            <li><a href="/throttle/searchFreeService.do?menuClick=1"><i class="fa fa-cog"></i> Config Free Service</a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-wrench"></i> <span><spring:message code="subMenus.label.Service" text="Service"/></span></a>
        <ul class="children">
            <li><a href="/throttle/createJobcard.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="service.subMenus.label.CreateJobCard" text="default text"/></a></li>
            <li><a href="/throttle/vehicleComplaintHist.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="service.subMenus.label.VehComplaintHistory" text="default text"/></a></li>
            <li><a href="/throttle/supervisorView.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="service.subMenus.label.ViewJobCard" text="default text"/></a></li>
            <li><a href="/throttle/chooseJobCard.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="service.subMenus.label.GenerateMRS" text="default text"/></a></li>
            <li><a href="/throttle/closeJobCardView.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="service.subMenus.label.CloseJobCard" text="default text"/></a></li>
            <li><a href="/throttle/ViewJobCardsForBilling.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="service.subMenus.label.GenerateBill" text="default text"/></a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-chain-broken"></i> <span><spring:message code="subMenus.label.Recondition" text="Recondition"/></span></a>
        <ul class="children">
            <li><a href="/throttle/rcQueue.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="recondition.subMenus.label.RCQueue" text="default text"/> </a></li>
            <li><a href="/throttle/alterRcWo.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="recondition.subMenus.label.AlterRCWO" text="RC WO List"/></a></li>
            <li><a href="/throttle/modifyWO.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="recondition.subMenus.label.ModifyRCWorkOrder" text="default text"/></a></li>
            <li><a href="/throttle/receiveRcParts.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="recondition.subMenus.label.ReceiveRC" text="Receive RC"/></a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-building"></i> <span><spring:message code="subMenus.label.Stores" text="Stores"/></span></a>
        <ul class="children">
            <li><a href="/throttle/manageRacks.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="stores.subMenus.label.Rack" text="default text"/></a></li>
            <li><a href="/throttle/manageSubRack.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="stores.subMenus.label.SubRack" text="default text"/> </a></li>
            <li><a href="/throttle/manageParts.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="stores.subMenus.label.Parts" text="default text"/></a></li>
            <li><a href="/throttle/MRSApproval.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="stores.subMenus.label.MRSApproval" text="default text"/></a></li>
            <li><a href="/throttle/MRSList.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="stores.subMenus.label.View/IssueMRS" text="default text"/> </a></li>
            <li><a href="/throttle/localPurchaseRequest.do?menuClick=1"><i class="fa fa-cog"></i>Local Purchase Request </a></li>
            <li><a href="/throttle/localPurchase.do?menuClick=1"><i class="fa fa-cog"></i>Local Purchase  </a></li>
            <li><a href="/throttle/purchaseReceipt.do?menuClick=1"><i class="fa fa-cog"></i>Purchase Receipts </a></li>
            <li><a href="/throttle/handleBarcodeGenerationPage.do?menuClick=1"><i class="fa fa-cog"></i> Generate BarCode</a></li>
                            <li><a href="/throttle/barCodePrint.do?menuClick=1"><i class="fa fa-cog"></i>barCodePrint </a></li>
            <li><a href="/throttle/generateDirectMprPage.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="stores.subMenus.label.GenerateMPR" text="default text"/>  </a></li>
            <li><a href="/throttle/storesMprList.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="stores.subMenus.label.MPRList" text="default text"/> </a></li>
            <li><a href="/throttle/mprApprovalList.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="stores.subMenus.label.MPRApproval" text="default text"/>  </a></li>
            <li><a href="/throttle/modifyPO.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="stores.subMenus.label.ModifyPO" text="default text"/> </a></li>
            <li><a href="/throttle/requiredItems.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="stores.subMenus.label.RequeriedItems" text="default text"/> </a></li>
            <li><a href="/throttle/receiveInvoice.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="stores.subMenus.label.ReceiveDC" text="default text"/>  </a></li>
            <li><a href="/throttle/modifyGrnPage.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="stores.subMenus.label.ReceiveDCInvoice" text="default text"/>   </a></li>
        </ul>
    </li>-->

<!--
    <li class="nav-parent"><a href="#"><i class="fa fa-refresh"></i> <span><spring:message code="subMenus.label.PrimaryOperation" text="PrimaryOperation"/> </span></a>
        <ul class="children">

            <li><a href="/throttle/viewRouteDetails.do?menuClick=1"><i class="fa fa-cog"></i>Routes</a></li>
            <li><a href="/throttle/tripPlanning.do?tripType=1&menuClick=1"><i class="fa fa-cog"></i><spring:message code="primaryOperation.label.tripPlanning" text="tripPlanning"/></a></li>
            <li><a href="/throttle/viewTripSheets.do?statusId=8&tripType=1&admin=No&menuClick=1"><i class="fa fa-cog"></i><spring:message code="primaryOperation.label.tripStart" text="tripStart"/></a></li>
            <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=1&admin=No&menuClick=1"><i class="fa fa-cog"></i>View Trip Sheet</a></li>
            <li><a href="/throttle/viewTripSheets.do?statusId=10&admin=No&tripType=1&menuClick=1"><i class="fa fa-cog"></i>Trip End</a></li>
            <li><a href="/throttle/viewTripSheets.do?statusId=12&admin=No&tripType=1&menuClick=1"><i class="fa fa-cog"></i>Trip Closure</a></li>
            <li><a href="/throttle/viewTripSheets.do?statusId=100&admin=No&tripType=1&menuClick=1"><i class="fa fa-cog"></i>Trip POD</a></li>
            <li><a href="/throttle/blockAdvanceGr.do?statusId=100&admin=No&tripType=1&menuClick=1"><i class="fa fa-cog"></i>Block Advance GR(s)</a></li>
            <li><a href="/throttle/cancelBlockedGr.do?statusId=100&admin=No&tripType=1&menuClick=1"><i class="fa fa-cog"></i>Cancel Blocked GR(s)</a></li>
            <li><a href="/throttle/viewCanceleGr.do?statusId=100&admin=No&tripType=1&menuClick=1"><i class="fa fa-cog"></i>cancelled GR(s)</a></li>
            <li><a href="/throttle/handleViewPrimaryDriver.do?menuClick=1"><i class="fa fa-cog"></i>Manage Driver</a></li>
            <li><a href="/throttle/vehicleDriverPlanning.do?menuClick=1"><i class="fa fa-cog"></i>Driver Mapping</a></li>
            <li><a href="/throttle/vehicleBPCLCardMapping.do?menuClick=1"><i class="fa fa-cog"></i>BPCL Card Mapping</a></li>
            <li><a href="/throttle/viewVehicleAvailability.do?tripType=1&menuClick=1"><i class="fa fa-cog"></i>Vehicle Availability</a></li>
            <li><a href="/throttle/viewTripSheets.do?statusId=6&admin=No&menuClick=1"><i class="fa fa-cog"></i>Trip Assign Vehicle</a></li>
            <li><a href="/throttle/viewTripSheets.do?statusId=8&admin=No&menuClick=1"><i class="fa fa-cog"></i>Trip Un Freeze</a></li>
            <li><a href="/throttle/viewTripSheets.do?statusId=13&admin=No&menuClick=1"><i class="fa fa-cog"></i>Trip Settlement</a></li>
            <li><a href="/throttle/viewTripSheets.do?statusId=14&admin=No&menuClick=1"><i class="fa fa-cog"></i>View Settled Trips</a></li>
            <li><a href="/throttle/viewTripSheets.do?statusId=16&admin=No&menuClick=1"><i class="fa fa-cog"></i>Trip Billed</a></li>
            <li><a href="/throttle/handlePrimaryDriverSettlement.do?menuClick=1"><i class="fa fa-cog"></i>Driver Settlement</a></li>

        </ul>
    </li>

    <li class="nav-parent"><a href="#"><i class="fa fa-shopping-cart"></i> <span><spring:message code="subMenus.label.Billing" text="Billing"/></span></a>
        <ul class="children">

            <li><a href="/throttle/viewClosedOrderForBilling.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="billing.subMenus.label.GenerateBilling" text="default text"/></a></li>
            <li><a href="/throttle/viewClosedRepoOrderForBilling.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="billing.subMenus.label.GenerateBilling(Repo&DSO)" text="default text"/></a></li>
            <li><a href="/throttle/viewOrderBillForSubmit.do?tripType=1&submitStatus=0&menuClick=1" > <i class="fa fa-cog"></i><spring:message code="billing.subMenus.label.SubmitBill" text="default text"/> </a></li>
            <li><a href="/throttle/viewOrderBillForSubmit.do?tripType=1&submitStatus=1&menuClick=1" > <i class="fa fa-cog"></i><spring:message code="billing.subMenus.label.ViewBill" text="default text"/> </a></li>
            <li><a href="/throttle/invoiceReceipts.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="billing.subMenus.label.invoice" text="default text"/></a></li>

        </ul>
    </li>

    
    <li class="nav-parent"><a href="#"><i class="fa fa-money"></i> <span><spring:message code="subMenus.label.Finance" text="Finance"/></span></a>
        <ul class="children">
            <li><a href="/throttle/financeYear.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.FinanceYear" text="default text"/> </a></li>
            <li><a href="/throttle/financePrimaryBank.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.BankMaster" text="default text"/> </a></li>
            <li><a href="/throttle/financeBank.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.BankBranchMaster" text="default text"/>  </a></li>
            <li><a href="/throttle/financeGroup.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.PrimaryGroup" text="default text"/> </a></li>
            <li><a href="/throttle/levelMaster.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.GroupMaster" text="default text"/> </a></li>
            <li><a href="/throttle/financeLedger.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.LedgerMaster" text="default text"/> </a></li>
            	                <li><a href="/throttle/financeTax.do?menuClick=1"><i class="fa fa-cog"></i>Tax Master</a></li>
            <li><a href="/throttle/handleCRJPayment.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.CreditorJournalVoucher(CRJ)" text="default text"/> </a></li>
            <li><a href="/throttle/handlePaymentDetailsReport.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.Payments" text="default text"/></a></li>
            <li><a href="/throttle/handlePaymentPendingReport.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.PaymentsPendingReport" text="default text"/>  </a></li>
            <li><a href="/throttle/handlePaymentPaidReport.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.PaymentsPaidReport" text="default text"/>  </a></li>
            <li><a href="/throttle/invoiceReceipts.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.InvoiceReceipts" text="default text"/> </a></li>
            <li><a href="/throttle/invoicePendingReceipts.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.InvoicePendingReceipts" text="default text"/>  </a></li>
            <li><a href="/throttle/invoiceReceived.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.InvoiceReceived" text="default text"/> </a></li>
            <li><a href="/throttle/handleCreditNote.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.CreditNote" text="default text"/> </a></li>
            <li><a href="/throttle/handleCreditSearch.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.CreditSearch" text="default text"/> </a></li>
            <li><a href="/throttle/handleDebitNote.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.DebitNote" text="default text"/> </a></li>
            <li><a href="/throttle/handleDebitSearch.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.DebitSearch" text="default text"/> </a></li>
            <li><a href="/throttle/paymentEntry.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.CashPayments" text="default text"/> </a></li>
            <li><a href="/throttle/bankPaymentEntry.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.BankPayments" text="default text"/> </a></li>
            <li><a href="/throttle/contraEntry.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.ContraEntry" text="default text"/> </a></li>
            <li><a href="/throttle/journalEntry.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.JournalEntry" text="default text"/> </a></li>
            <li><a href="/throttle/bankPaymentClearance.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.BankTransaction" text="default text"/></a></li>
            <li><a href="/throttle/bankReconciliationStatement.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.BankReconditionStatament" text="default text"/></a></li>
            <li><a href="/throttle/ledgerReport.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.LedgerReport" text="default text"/></a></li>
            <li><a href="/throttle/dayBook.do?param=search"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.DayBook" text="default text"/></a></li>
            <li><a href="/throttle/dayBookSummary.do?param=search"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.DayBookSummary" text="default text"/></a></li>
            <li><a href="/throttle/trialBalanceNew.do?param=search"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.TrailBalance" text="default text"/></a></li>
            <li><a href="/throttle/profitAndLoss.do?param=search"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.P&L" text="default text"/></a></li>
            <li><a href="/throttle/balanceSheet.do?param=search"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.BalanceSheet" text="default text"/></a></li>
            <li><a href="/throttle/entriesReport.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.EntriesReport" text="default text"/></a></li>
            <li><a href="/throttle/costCenterReport.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.CostCenterReport" text="default text"/></a></li>
            <li><a href="/throttle/bankReconciliationReport.do?param=view&menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.BRSReport" text="default text"/></a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-envelope-o"></i> <span><spring:message code="subMenu.label.Reports"  text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/handleTripExpenseReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Trip Expense Report</a></li>
            <li><a href="/throttle/handleDailyTripPlanningDetails.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Transport Plan Report</a></li>
            <li><a href="/throttle/handleContainerMovement.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Container Movement Report</a></li>
            <li><a href="/throttle/handleVehicleVendorwiseTrip.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Vehicle VendorWise Trip Report</a></li>
            <li><a href="/throttle/handleDailyTripPlanningDetails.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Transport Plan Report</a></li>
            <li><a href="/throttle/handleTripReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Trip Report</a></li>
            <li><a href="/throttle/handleGRSummaryReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>GR Summary</a></li>
            <li><a href="/throttle/handleGRProfitability.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>GR Wise Profitability</a></li>
            <li><a href="/throttle/handleDailyCashReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Daily Cash Report(DCR)</a></li>
            <li><a href="/throttle/handleDailyVehicleStatusExcel.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>DMR Report</a></li>
            <li><a href="/throttle/handleVehicleDetainDetails.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Vehicle Detain/Idle Report</a></li>
            <li><a href="/throttle/handleInvoiceReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Invoice Report</a></li>
            <li><a href="/throttle/handlePendingGrForInvoiceReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Pending GR for Invoice Report</a></li>
            <li><a href="/throttle/handleInvoiceDetailsReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Invoice Details(Excel)</a></li>
            <li><a href="/throttle/handleInvoiceXMLDetailsReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Invoice Details(XML)</a></li>
            <li><a href="/throttle/searchContractRateLog.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Contract Rate Edit Log</a></li>
            <li><a href="/throttle/handleVehicleWiseProfitReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.VehicleWiseProfit"  text="default text"/></a></li>
            <li><a href="/throttle/handleVehicleUtilizationReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.VehicleUtilizationReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleViewTripSheet.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.TripSheetReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleAccountsReceivableReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.AccountsReceivable"  text="default text"/></a></li>
            <li><a href="/throttle/handleCustomerWiseProfitReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.CustomerwiseProfit"  text="default text"/>  </a></li>
            <li><a href="/throttle/handleTripWiseProfitReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.TripWiseProfitabilityReport(Closure)"  text="default text"/> </a></li>
            <li><a href="/throttle/handleTripWiseProfitReportEndStatus.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.TripWiseProfitabilityReport(End)"  text="default text"/> </a></li>
            <li><a href="/throttle/handleGPSLogReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.GPSLogReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleDriverSettlementDetails.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.DriverSettlementReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleBPCLTransactionDetails.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.BPCLTransactionReport"  text="default text"/> </a></li>
            <li><a href="/throttle/viewFinanceAdviceDetais.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.FinanceAdviceReport"  text="default text"/> </a></li>
            <li><a href="/throttle/viewGpsStatusDetais.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.GPSStatusReport"  text="default text"/> </a></li>
            <li><a href="/throttle/viewWFLWFUDetais.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.WFL&WFUMailSent"  text="default text"/> </a></li>
            <li><a href="/throttle/dashboardReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.Dashboard"  text="default text"/> </a></li>
            <li><a href="/throttle/handleViewTripSheetWfl.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.TripSheetReportWithWfl"  text="default text"/> </a></li>
            <li><a href="/throttle/handleFCWiseTripSummary.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.FCTripSummary"  text="default text"/> </a></li>
            <li><a href="/throttle/handleMarginWiseReport.do?param=search" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.MarginTripSummary"  text="default text"/> </a></li>
            <li><a href="/throttle/handleMonthWiseEmptyRunSummary.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.EmptyRunSummary"  text="default text"/> </a></li>
            <li><a href="/throttle/handleJobcardSumary.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.JobcardSummary"  text="default text"/> </a></li>
            <li><a href="/throttle/handleTripMergingDetails.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.TripMergingReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleVehicleOdometerReading.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.VehicleCurrentOdometerReadingReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleCustomerWiseMergingProfitReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.CustomerWiseProfitMerging"  text="default text"/> </a></li>
            <li><a href="/throttle/handleTripExtraExpenseReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.TripExtraExpenseReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleVehicleDriverAdvance.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.VehicleDriverAdvanceReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleToPayCustomerTripDetails.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.TopaycustomertripDetailsReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleTripVmrReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.TripVMRReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleTripBudgetReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.FCPerformanceReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleAccountMgrPerformanceReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.A/CMgrPerformanceReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleRNMExpenseReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.R&MSpentAnalysisReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleTyerExpenseReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.TyreAnalysisReport"  text="default text"/> </a></li>
            <li><a href="/throttle/report.do?menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.TruckDriverMappingReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleOrderExpenseRevenueReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.OrderExpenseRevenueReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handletripTrailerProfitReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.TripTrailerProfitReport"  text="default text"/> </a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-pie-chart"></i> <span><spring:message code="subMenu.label.MIS"  text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/handlecustomerrevenue.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.CustomerWiseRevenue"  text="default text"/></a></li>
            <li><a href="/throttle/customerrevenuebillwise.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.BillWiseRevenue"  text="default text"/> </a></li>
            <li><a href="/throttle/revenuefcwise.do?menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.FCWiseRevenue"  text="default text"/> </a></li>
            <li><a href="/throttle/vehiclerevenue.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.VehicleWiseRevenue"  text="default text"/></a></li>
            <li><a href="/throttle/vehicletyperevenue.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.VehicleTypeWiseRevenue"  text="default text"/></a></li>
            <li><a href="/throttle/accountreceivablerevenue.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.CustomerWiseA.R"  text="default text"/></a></li>
            <li><a href="/throttle/accountreceivablebillwiserevenue.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.BillWiseA.R"  text="default text"/> </a></li>
            <li><a href="/throttle/totaloperationcost.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.TotalOperationCost"  text="default text"/></a></li>
            <li><a href="/throttle/totaloperationcostperbill.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.TotalOperationCost(Bill)"  text="default text"/></a></li>
            <li><a href="/throttle/fuelcostperbill.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.FuelCost"  text="default text"/> </a></li>
            <li><a href="/throttle/customerwiseprofit.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.CustomerWiseProfit"  text="default text"/></a></li>
            <li><a href="/throttle/vehiclewiseprofit.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.VehicleWiseProfit"  text="default text"/></a></li>
            <li><a href="/throttle/fcwiseprofit.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.FCWiseProfit"  text="default text"/> </a></li>
            <li><a href="/throttle/consolidatedreportpermonth.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.MonthWiseConsolidated"  text="default text"/></a></li>
            <li><a href="/throttle/consolidatedreportperbill.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.MonthWiseConsolidated(Bill)"  text="default text"/></a></li>
        </ul>
    </li>


    <li class="nav-parent"><a href="#"><i class="fa fa-camera-retro"></i> <span><spring:message code="fmsvehicles.label.FMSVehicle" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/handleBillListPage.do?menuClick=1" ><spring:message code="fmsvehicles.label.BillReport" text="default text"/></a></li>
            <li><a href="/throttle/mechanicPerformance.do?menuClick=1" >Mechanic Performance</a></li>
            <li><a href="/throttle/handleBodyBillPage.do?menuClick=1" ><spring:message code="fmsvehicles.label.ContractorsReport" text="default text"/></a></li>
            <li><a href="/throttle/contractorActivityPage.do?menuClick=1" >Contractors Activity Report</a></li>
            <li><a href="/throttle/periodicServiceListPage.do?menuClick=1" ><spring:message code="fmsvehicles.label.ServiceDues" text="default text"/></a></li>
            <li><a href="/throttle/vehicleFCDue.do?menuClick=1" ><spring:message code="fmsvehicles.label.FCDues" text="default text"/></a></li>
            <li><a href="/throttle/vehicleInsuranceDue.do?menuClick=1" ><spring:message code="fmsvehicles.label.InsuranceDues" text="default text"/></a></li>
            <li><a href="/throttle/vehicleRoadTaxDue.do?menuClick=1" ><spring:message code="fmsvehicles.label.RoadTaxDues" text="default text"/></a></li>
            <li><a href="/throttle/vehiclePermitDue.do?menuClick=1" ><spring:message code="fmsvehicles.label.PermitDues" text="default text"/></a></li>
            <li><a href="/throttle/vehicleAMCDue.do?menuClick=1" ><spring:message code="fmsvehicles.label.AMCDues" text="default text"/></a></li>
            <li><a href="/throttle/vehicleServiceCost.do?menuClick=1" ><spring:message code="fmsvehicles.label.ServiceCostAnalyst" text="default text"/></a></li>

        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-shopping-basket"></i> <span><spring:message code="fmsstores.label.FMSStores" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/categorySearch.do?menuClick=1" ><spring:message code="fmsstores.label.CategorywiseReport" text="default text"/></a></li>
            <li><a href="/throttle/stkAvbPage.do?menuClick=1" ><spring:message code="fmsstores.label.StockAvailability" text="default text"/></a></li>
            <li><a href="/throttle/handleTaxWiseItemsPage.do?menuClick=1" >Taxwise Items</a></li>
                         <li><a href="/throttle/reqItemsPage.do?menuClick=1" ><spring:message code="fmsstores.label.RequiredItems" text="default text"/></a></li>
            <li><a href="/throttle/handleOrderListPage.do?menuClick=1" ><spring:message code="fmsstores.label.WO/POReport" text="default text"/></a></li>
            <li><a href="/throttle/stockWorthPage.do?menuClick=1" ><spring:message code="fmsstores.label.StockWorth" text="default text"/></a></li>
            <li><a href="/throttle/stockPurchaseRep.do?menuClick=1" ><spring:message code="fmsstores.label.PurchaseReport" text="default text"/></a></li>
            <li><a href="/throttle/stockIssueRep.do?menuClick=1" ><spring:message code="fmsstores.label.IssueReport" text="default text"/></a></li>
                    <li><a href="/throttle/storesEff.do?menuClick=1" ><spring:message code="fmsstores.label.StoresEfficiency" text="default text"/></a></li>
                    <li><a href="/throttle/movingAve.do?menuClick=1" ><spring:message code="fmsstores.label.MovingAverage" text="default text"/></a></li>
            <li><a href="/throttle/receivedStockRpt.do?menuClick=1" ><spring:message code="fmsstores.label.ReceiveStock" text="default text"/></a></li>
                            <li><a href="/throttle/stList.do?menuClick=1" >Stock Transfer </a></li>
                    <li><a href="/throttle/handleStockPurchasePage.do?menuClick=1" ><spring:message code="fmsstores.label.StockPurchaseReport" text="default text"/> </a></li>
                    <li><a href="/throttle/taxwisePOreport.do?menuClick=1" >TaxWise Purchase Report</a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-map"></i> <span><spring:message code="fmsRCParts.label.FMSRCParts" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/rcItemPage.do?menuClick=1" ><spring:message code="fmsRCParts.label.RCPartsReport" text="default text"/></a></li>
            <li><a href="/throttle/handleRcBillList.do?menuClick=1" ><spring:message code="fmsRCParts.label.RCWorkOrderBills" text="default text"/></a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-life-ring"></i> <span><spring:message code="fmsTyres.label.FMSTyres" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/tyrePoWoPage.do?menuClick=1" ><spring:message code="fmsTyres.label.TyrePo/WoReport" text="default text"/></a></li>
            <li><a href="/throttle/handleTyreLifeCyclePage.do?menuClick=1" ><spring:message code="fmsTyres.label.label.TyreLifeCycle" text="default text"/></a></li>
            <li><a href="/throttle/handleTyresRotationsList.do?menuClick=1"><i class="fa fa-cog"></i>Tyres Rotation</a></li>
            <li><a href="/throttle/tyresReportByVehicleNo.do?menuClick=1"><i class="fa fa-cog"></i>Tyres Report By Vehicle</a></li>
            <li><a href="/throttle/tyresReportByTyreNo.do?menuClick=1"><i class="fa fa-cog"></i>Tyres Report By Tyre No</a></li>
            <li><a href="/throttle/stockPurchaseRep.do?menuClick=1"><i class="fa fa-cog"></i>Tyres Purchase Report</a></li>
            <li><a href="/throttle/stockIssueRep.do?menuClick=1"><i class="fa fa-cog"></i>Tyres Issue Report</a></li>
            <li><a href="/throttle/stockWorthPage.do?menuClick=1"><i class="fa fa-cog"></i>Tyres Stock Worth Report</a></li>

        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-server"></i> <span><spring:message code="fmsRenderService.label.FMSRenderService" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/serviceSummaryPage.do?menuClick=1" ><spring:message code="fmsRenderService.label.ServiceSummary" text="default text"/></a></li>
            <li><a href="/throttle/serEff.do?menuClick=1" ><spring:message code="fmsRenderService.label.ServiceEfficiency" text="default text"/></a></li>
            <li><a href="/throttle/vehicleComplaintHist.do?menuClick=1" ><spring:message code="fmsRenderService.label.VehCompliantHistory" text="default text"/></a></li>
        </ul>
    </li>

    <li class="nav-parent"><a href="#"><i class="fa fa-bar-chart"></i> <span><spring:message code="FMSMIS.label.FMSMIS" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/handleServiceDailyMIS.do?menuClick=1" ><spring:message code="FMSMIS.label.JobcardRep" text="default text"/>1</a></li>
            <li><a href="/throttle/handleServiceGraphData.do?menuClick=1" ><spring:message code="FMSMIS.label.JobcardRep" text="default text"/>2</a></li>
            <li><a href="/throttle/vehicleComparisionReportPage.do?menuClick=1" ><spring:message code="FMSMIS.label.ManufacturerComparison" text="default text"/></a></li>
            <li><a href="/throttle/vehicleAgeComparisionReportPage.do?menuClick=1" ><spring:message code="FMSMIS.label.VehicleAgeDistribution" text="default text"/></a></li>
            <li><a href="/throttle/salesTrendSearch.do?menuClick=1" >Sales Bill Trend</a></li>
            <li><a href="/throttle/stockWorthSearch.do?menuClick=1" ><spring:message code="FMSMIS.label.StockWorth" text="default text"/></a></li>
            <li><a href="/throttle/vendorTrendSearch.do?menuClick=1" >Vendor Supply Trend</a></li>
            <li><a href="/throttle/problemDistributionSearch.do?menuClick=1" ><spring:message code="FMSMIS.label.ProblemDistribution" text="default text"/></a></li>
            <li><a href="/throttle/externalLabourBillGraphData.do?menuClick=1" >External Labour Bill Trend</a></li>
            <%--<li><a href="/throttle/frame/report/Valume.html"  >Volume of Vehicle Services</a></li>--%>
            <li><a href="/throttle/topProblemSearch.do?menuClick=1"  ><spring:message code="FMSMIS.label.TopProblem" text="default text"/></a></li>
                <%--<li><a href="/throttle/frame/report/consumption.html"  >Stock Consumption</a></li>--%>
            <li><a href="/throttle/scheduleDeviation.do?menuClick=1" >Schedule Deviation</a></li>
            <li><a href="/throttle/scrapGraphData.do?menuClick=1" >Scarp Value</a></li>
            <li><a href="/throttle/rcTrendGraphData.do?menuClick=1" ><spring:message code="FMSMIS.label.RCTrend" text="default text"/></a></li>
                <%--<li><a href="/throttle/frame/report/spareLife.html" >Spare Life</a></li>--%>
                <%--<li><a href="/throttle/frame/report/RCspareLife.html" >RC-Spare Life</a></li>--%>
            <li><a href="/throttle/RCexpense.do?menuClick=1" ><spring:message code="FMSMIS.label.RC-Expense" text="default text"/> </a></li>
            <li><a href="/throttle/RCsaving.do?menuClick=1" >Saving By RC</a></li>
            <li><a href="/throttle/mileageReport.do?menuClick=1" ><spring:message code="FMSMIS.label.MileageReport" text="default text"/></a></li>
            <li><a href="/throttle/vehicleServiceCostGraphData.do?menuClick=1" >Cost of Service</a></li>
            <%--bala--%>
            <%-- <li><a href="/throttle/usageTypewisereport.do?menuClick=1" >Usage Typewise Report</a></li>--%>
            <%--<li><a href="/throttle/frame/report/vehicleDeviation.html" >Vehicle Deviation </a></li>--%>
        </ul>
    </li>

    <li class="nav-parent"><a href="#"><i class="fa fa-table"></i> <span><spring:message code="TripAnalytics.label.TripAnalytics" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/content/RenderService/tripMisReport.jsp" >&nbsp;<img src="/throttle/images/icon_stand.png" alt="" /><spring:message code="TripAnalytics.label.TripMISReport" text="default text"/></a></li>
                  <li><a href="/throttle/content/report/driverSettlementReport.jsp" >Driver Settlement </a></li>
                  <li><a href="/throttle/vehicleCurrentStatus.do?menuClick=1" >Vehicle Status</a></li>
            <li><a href="/throttle/companyWiseReportPage.do?menuClick=1" ><spring:message code="TripAnalytics.label.CompanyWiseTrip" text="default text"/> </a></li>
            <!-- <li><a href="/throttle/vehiclePerformancePage.do?menuClick=1" >Vehicle Performance</a></li>
                 <li><a href="/throttle/driverPerformancePage.do?menuClick=1" >Driver Performance</a></li>    
                                                    <li><a href="/throttle/lpsReport.do?menuClick=1" >LPS</a></li>
            <li><a href="/throttle/vehicleProfitAndLossReport.do?menuClick=1" >Vehicle Profit And Loss Report</a></li> 
            <li><a href="/throttle/tripWisePandLReport.do?menuClick=1" ><spring:message code="TripAnalytics.label.TripWiseProfitAndLoss" text="default text"/>  </a></li>
            <li><a href="/throttle/VehicleWisePandL.do?menuClick=1" ><spring:message code="TripAnalytics.label.VehicleWiseProfitAndLoss" text="default text"/>  </a></li>
            <li><a href="/throttle/content/RenderService/ProfitAndLossReport.jsp" ><spring:message code="TripAnalytics.label.ProfitAndLoss" text="default text"/>  </a></li>
            <li><a href="/throttle/districtWiseReport.do?menuClick=1" ><spring:message code="TripAnalytics.label.DistrictWiseReport" text="default text"/> </a></li>
            <li><a href="/throttle/tripSheetDetailsReport.do?menuClick=1" ><spring:message code="TripAnalytics.label.TripSheetDetails" text="default text"/> </a></li>
            <li><a href="/throttle/consigneeWiseDetailsReport.do?menuClick=1" ><spring:message code="TripAnalytics.label.ConsigneeWiseDetails" text="default text"/> </a></li>
            <li><a href="/throttle/tripSettlementDetailsReport.do?menuClick=1" ><spring:message code="TripAnalytics.label.TripSettlementDetails" text="default text"/> </a></li>
            <li><a href="/throttle/dieselReport.do?menuClick=1" ><spring:message code="TripAnalytics.label.DieselReport" text="default text"/> </a></li>
        </ul>
    </li>-->





</ul>



</div><!-- leftpanelinner -->
</div><!-- leftpanel -->

<div class="mainpanel">

    <div class="headerbar">

        <a class="menutoggle"><i class="fa fa-bars"></i></a>

        <%--      <form class="searchform" action="/throttle/dashboardOperation.do?menuClick=1" method="post">
                <input type="text" class="form-control" name="keyword" placeholder="Search here..." />
        <%@ include file="../common/NewDesign/commonParameters.jsp" %></form>--%>


        <div class="header-right">
            <ul class="headermenu">

                <li>
                    <div class="btn-group">
                        <span >
                            <a href="/throttle/languageChangeLoginRequest.do?paramName=en">english</a> &nbsp;|&nbsp;
                            <!--<a href="/throttle/languageChangeLoginRequest.do?paramName=ar">Ø§Ù„Ø¹Ø±Ø¨ÙŠØ©</a>-->
                        </span>
                    </div>
                </li>
                <li>
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle tp-icon" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-globe"></i>
                            <span class="badge">1</span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-head pull-right">
                            <h5 class="title"><spring:message code="sidemenu.label.YouHave5NewNotifications" text="default text"/></h5>
                            <ul class="dropdown-list gen-list">
                                <li class="new">
                                    <a href="">
                                        <span class="thumb"><img src="images/photos/user4.png" alt="" /></span>
                                        <span class="desc">
                                            <span class="name"><spring:message code="sidemenu.label.NewJobcard1960created" text="default text"/> <span class="badge badge-success"><spring:message code="sidemenu.label.new" text="default text"/></span></span>
                                        </span>
                                    </a>
                                </li>

                                <li class="new"><a href=""><spring:message code="sidemenu.label.SeeAllNotifications" text="default text"/></a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <!--                <img src="/throttle/content/NewDesign/images/photos/loggeduser.png" alt="" />-->
                            <spring:message code="sidemenu.label.WelcomeMr" text="Welcome Mr"/>.<%= session.getAttribute("userName") %>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                            <!--                <li><a href="profile.html"><i class="glyphicon glyphicon-user"></i> My Profile</a></li>-->
                            <li><a href="#"><i class="glyphicon glyphicon-cog"></i> <spring:message code="sidemenu.label.AccountSettings" text="AccountSettings"/></a></li>
                            <li><a href="#"><i class="glyphicon glyphicon-question-sign"></i> <spring:message code="sidemenu.label.Help" text="Help"/></a></li>
                            <li><a href="/throttle/logout.do?menuClick=1"><i class="glyphicon glyphicon-log-out"></i><spring:message code="sidemenu.label.LogOut" text="LogOut"/></a></li>
                        </ul>
                    </div>
                </li>

            </ul>
        </div><!-- header-right -->

    </div><!-- headerbar -->

    <!--sidemenu.label.LogOut-->