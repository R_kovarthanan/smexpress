
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<!--      <h5 class="sidebartitle">Navigation</h5>-->

<% Object rolId = session.getAttribute("RoleId");%>
<ul class="nav nav-pills nav-stacked nav-bracket">

    <li class="nav-parent"><a href=""><i class="fa fa-user"></i> <span><spring:message code="subMenus.label.HRMS" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/viewCompany1.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="hrms.subMenus.label.Company" text="default text"/> </a></li>
            <li><a href="/throttle/manageDepartment.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="hrms.subMenus.label.Department" text="default text"/></a></li>
            <li><a href="/throttle/viewDesign.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="hrms.subMenus.label.Designation" text="default text"/></a></li>
            <li><a href="/throttle/handleEmpViewPage.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="hrms.subMenus.label.Employees" text="default text"/></a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href=""><i class="fa fa-gears"></i> <span><spring:message code="subMenus.label.Settings" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/alterPassword.do?menuClick=1" ><spring:message code="settings.subMenus.label.ChangePassword" text="default text"/></a></li>
            <li><a href="/throttle/recoverPassword.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManagePassword" text="default text"/></a></li>
            <li><a href="/throttle/viewuser.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManageUsers" text="default text"/></a></li>
            <li><a href="/throttle/allroles.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManageRoles" text="default text"/></a></li>
            <li><a href="/throttle/userroles.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManageUserRoles" text="default text"/></a></li>
            <li><a href="/throttle/activerole.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManageRoleFunctions" text="default text"/></a></li>

        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-bus"></i> <span><spring:message code="subMenus.label.Trucks" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/handleViewAxle.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.AxleMaster" text="default text"/></a></li>
            <li><a href="/throttle/viewType.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.VehicleType" text="default text"/></a></li>
            <li><a href="/throttle/viewMfr.do?fleetType=1&menuClick=1"><i class="fa fa-cog"></i><spring:message code="trucks.subMenus.label.Make" text="default text"/> </a></li>
            <li><a href="/throttle/viewModelPage.do?fleetType=1&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.Model" text="default text"/></a></li>
            <li><a href="/throttle/searchVehiclePage.do?fleetTypeId=1&listId=&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.Vehicles" text="default text"/></a></li>
            <li><a href="/throttle/resetVehicleKM.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="trucks.subMenus.label.ResetVehicleKM" text="default text"/> </a></li>
            <li><a href="/throttle/searchVehiclePage.do?listId=5&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.VehiclePurchase" text="default text"/> </a></li>
            <li><a href="/throttle/searchVehiclePage.do?listId=1&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.VehicleInsurance" text="default text"/></a></li>
            <li><a href="/throttle/searchVehiclePage.do?listId=3&menuClick=1"><i class="fa fa-cog"></i><spring:message code="trucks.subMenus.label.VehicleRegistration" text="default text"/> </a></li>
            <li><a href="/throttle/viewType.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="trucks.subMenus.label.VehicleInspection" text="default text"/> </a></li>
            <li><a href="/throttle/viewType.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.VehicleRoadPermit" text="default text"/></a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-truck"></i> <span><spring:message code="subMenus.label.Trailers" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/handleViewTrailerTypePage.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trailers.subMenus.label.TrailerType" text="default text"/></a></li>
            <li><a href="/throttle/viewMfr.do?fleetType=2&menuClick=2"><i class="fa fa-cog"></i> <spring:message code="trailers.subMenus.label.Make" text="default text"/></a></li>
            <li><a href="/throttle/viewModelPage.do?fleetType=2&menuClick=2"><i class="fa fa-cog"></i><spring:message code="trailers.subMenus.label.Model" text="default text"/> </a></li>
            <li><a href="/throttle/searchTrailerPage.do?fleetTypeId=2&listId=&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trailers.subMenus.label.Trailers" text="default text"/></a></li>
            <li><a href="/throttle/searchVehiclePage.do?listId=5&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trailers.subMenus.label.TrailerPurchase" text="default text"/> </a></li>
        </ul>
    </li>
    <!-- ==============================operation Incharge ================================-->


    <!-- =============================HR Section=============================================== -->
    <%if (rolId.equals(1039)) {%>
    <li class="nav-parent"><a href=""><i class="fa fa-user"></i> <span><spring:message code="subMenus.label.HRMS" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/viewCompany1.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="hrms.subMenus.label.Company" text="default text"/> </a></li>
            <li><a href="/throttle/manageDepartment.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="hrms.subMenus.label.Department" text="default text"/></a></li>
            <li><a href="/throttle/viewDesign.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="hrms.subMenus.label.Designation" text="default text"/></a></li>
            <li><a href="/throttle/handleEmpViewPage.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="hrms.subMenus.label.Employees" text="default text"/></a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href=""><i class="fa fa-gears"></i> <span><spring:message code="subMenus.label.Settings" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/alterPassword.do?menuClick=1" ><spring:message code="settings.subMenus.label.ChangePassword" text="default text"/></a></li>
            <li><a href="/throttle/recoverPassword.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManagePassword" text="default text"/></a></li>
            <li><a href="/throttle/viewuser.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManageUsers" text="default text"/></a></li>
            <li><a href="/throttle/allroles.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManageRoles" text="default text"/></a></li>
            <li><a href="/throttle/userroles.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManageUserRoles" text="default text"/></a></li>
            <li><a href="/throttle/activerole.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManageRoleFunctions" text="default text"/></a></li>

        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-bus"></i> <span><spring:message code="subMenus.label.Trucks" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/handleViewAxle.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.AxleMaster" text="default text"/></a></li>
            <li><a href="/throttle/viewType.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.VehicleType" text="default text"/></a></li>
            <li><a href="/throttle/viewMfr.do?fleetType=1&menuClick=1"><i class="fa fa-cog"></i><spring:message code="trucks.subMenus.label.Make" text="default text"/> </a></li>
            <li><a href="/throttle/viewModelPage.do?fleetType=1&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.Model" text="default text"/></a></li>
            <li><a href="/throttle/searchVehiclePage.do?fleetTypeId=1&listId=&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.Vehicles" text="default text"/></a></li>
            <li><a href="/throttle/resetVehicleKM.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="trucks.subMenus.label.ResetVehicleKM" text="default text"/> </a></li>
            <li><a href="/throttle/searchVehiclePage.do?listId=5&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.VehiclePurchase" text="default text"/> </a></li>
            <li><a href="/throttle/searchVehiclePage.do?listId=1&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.VehicleInsurance" text="default text"/></a></li>
            <li><a href="/throttle/searchVehiclePage.do?listId=3&menuClick=1"><i class="fa fa-cog"></i><spring:message code="trucks.subMenus.label.VehicleRegistration" text="default text"/> </a></li>
            <li><a href="/throttle/viewType.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="trucks.subMenus.label.VehicleInspection" text="default text"/> </a></li>
            <li><a href="/throttle/viewType.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.VehicleRoadPermit" text="default text"/></a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-truck"></i> <span><spring:message code="subMenus.label.Trailers" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/handleViewTrailerTypePage.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trailers.subMenus.label.TrailerType" text="default text"/></a></li>
            <li><a href="/throttle/viewMfr.do?fleetType=2&menuClick=2"><i class="fa fa-cog"></i> <spring:message code="trailers.subMenus.label.Make" text="default text"/></a></li>
            <li><a href="/throttle/viewModelPage.do?fleetType=2&menuClick=2"><i class="fa fa-cog"></i><spring:message code="trailers.subMenus.label.Model" text="default text"/> </a></li>
            <li><a href="/throttle/searchTrailerPage.do?fleetTypeId=2&listId=&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trailers.subMenus.label.Trailers" text="default text"/></a></li>
            <li><a href="/throttle/searchVehiclePage.do?listId=5&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trailers.subMenus.label.TrailerPurchase" text="default text"/> </a></li>
        </ul>
    </li>
    <!-- ==============================operation Incharge ================================-->
    <%}else if(rolId.equals(1045)){%>
    <li class="nav-parent"><a href="#"><i class="fa fa-dashboard"></i> <span><spring:message code="subMenus.label.Dashboard" text="default text"/></span></a>
        <ul class="children" >
            <li><a href="/throttle/dashboardOperation.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="dashboard.subMenus.label.Operations" text="default text"/></a></li>
            <li><a href="/throttle/dashboardWorkshop.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="dashboard.subMenus.label.Workshop" text="default text"/></a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href=""><i class="fa fa-gears"></i> <span><spring:message code="subMenus.label.Settings" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/alterPassword.do?menuClick=1" ><spring:message code="settings.subMenus.label.ChangePassword" text="default text"/></a></li>

        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-motorcycle"></i> <span><spring:message code="subMenus.label.ServicePlan" text="default text"/> </span></a>
        <ul class="children">
            <li><a href="/throttle/manageService.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="serviceplan.subMenus.label.ManageServiceType" text="default text"/></a></li>
            <li><a href="/throttle/manageSection.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="serviceplan.subMenus.label.ManageSection" text="default text"/></a></li>
            <li><a href="/throttle/manageProblemAct.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="serviceplan.subMenus.label.ManageProblem" text="default text"/></a></li>
            <li><a href="/throttle/manageProblemActivity.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="serviceplan.subMenus.label.Activities" text="default text"/> </a></li>
            <li><a href="/throttle/manageMaintenance.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="serviceplan.subMenus.label.PeriodicService" text="default text"/> </a></li>
            <!--                <li><a href="/throttle/searchFreeService.do?menuClick=1"><i class="fa fa-cog"></i> Config Free Service</a></li>-->
        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-wrench"></i> <span><spring:message code="subMenus.label.Service" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/createJobcard.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="service.subMenus.label.CreateJobCard" text="default text"/></a></li>
            <li><a href="/throttle/vehicleComplaintHist.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="service.subMenus.label.VehComplaintHistory" text="default text"/></a></li>
            <li><a href="/throttle/supervisorView.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="service.subMenus.label.ViewJobCard" text="default text"/></a></li>
            <li><a href="/throttle/chooseJobCard.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="service.subMenus.label.GenerateMRS" text="default text"/></a></li>
            <li><a href="/throttle/closeJobCardView.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="service.subMenus.label.CloseJobCard" text="default text"/></a></li>
            <li><a href="/throttle/ViewJobCardsForBilling.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="service.subMenus.label.GenerateBill" text="default text"/></a></li>
        </ul>
    </li>
<!--         <li class="nav-parent"><a href="#"><i class="fa fa-refresh"></i> <span><spring:message code="subMenus.label.LeasingOperation" text="default text"/> </span></a>


                <ul class="children">

                    <li><a href="/throttle/vehicleDriverPlanning.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.Vehicle&DriverMapping" text="default text"/></a></li>
                    <li><a href="/throttle/viewVehicleAvailability.do?tripType=1&menuClick=1"><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.VehicleAvailability" text="default text"/></a></li>
                    <li><a href="/throttle/viewTrailerAvailability.do?tripType=1&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.TrailerAvailability" text="default text"/> </a></li>
                    <li><a href="/throttle/secondaryLeasingSheduleView.do?tripType=2&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.VehicleSchedule" text="default text"/> </a></li>
                     <li><a href="/throttle/viewTripSheets.do?statusId=8&tripType=3&admin=No&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.TripStart" text="default text"/> </a></li>
                        <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=3&admin=No&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.ViewTripSheet" text="default text"/> </a></li>
                        <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=3&admin=No&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.TripEnd" text="default text"/> </a></li>
                        <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=3&admin=No&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.TripClosure" text="default text"/> </a></li>
                        <li><a href="/throttle/handlefinanceAdvice.do?tripType=3&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.FinanceAdvice" text="default text"/> </a></li>
                         <li><a href="/throttle/handlefinanceRequest.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.FinanceAdviceApproval" text="default text"/>  </a></li>
                        <li><a href="/throttle/viewTripSheets.do?statusId=13&tripType=3&admin=No&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.TripSettlement" text="default text"/> </a></li>
                        <li><a href="/throttle/viewTripSheets.do?statusId=14&tripType=3&admin=No&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.SettledTrips" text="default text"/> </a></li>
                </ul>


    </li>-->
<!--        <li class="nav-parent"><a href="#"><i class="fa fa-exchange"></i> <span><spring:message code="subMenus.label.DailyOperation" text="default text"/> </span></a>
                   <ul class="children">
                    <li><a href="/throttle/viewGatePass.do?statusId=14&tripType=3&admin=No&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="dailyoperations.subMenus.label.GatePass" text="default text"/> </a></li>
                        <li><a href="/throttle/viewGatePassPrint.do?statusId=14&tripType=3&admin=No&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="dailyoperations.subMenus.label.GatePassPrint" text="default text"/> </a></li>
                    <li><a href="/throttle/vehicleDriverPlanning.do?menuClick=2"><i class="fa fa-cog"></i><spring:message code="dailyoperations.subMenus.label.Vehicle&DriverMapping" text="default text"/></a></li>
                    <li><a href="/throttle/viewVehicleAvailability.do?tripType=1&menuClick=2"><i class="fa fa-cog"></i><spring:message code="dailyoperations.subMenus.label.VehicleAvailability" text="default text"/> </a></li>
                    <li><a href="/throttle/viewTrailerAvailability.do?tripType=1&menuClick=2" ><i class="fa fa-cog"></i><spring:message code="dailyoperations.subMenus.label.TrailerAvailability" text="default text"/> </a></li>
                    <li><a href="/throttle/tripPlanning.do?tripType=1&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="dailyoperations.subMenus.label.TripPlanning" text="default text"/> </a></li>
                    <li><a href="/throttle/viewTripSheets.do?statusId=8&admin=No&tripType=1&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="dailyoperations.subMenus.label.TripStart" text="default text"/></a></li>
                     <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=1&admin=No&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="dailyoperations.subMenus.label.ViewTripSheet" text="default text"/> </a></li>
                     <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=1&admin=No&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="dailyoperations.subMenus.label.TripEnd" text="default text"/> </a></li>
                     <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=1&admin=No&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="dailyoperations.subMenus.label.TripClosure" text="default text"/> </a></li>
                     <li><a href="/throttle/handlefinanceAdvice.do?tripType=1&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="dailyoperations.subMenus.label.FinanceAdvice" text="default text"/> </a></li>
                    <li><a href="/throttle/handlefinanceRequest.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="dailyoperations.subMenus.label.FinanceApproval" text="default text"/> </a></li>
                     <li><a href="/throttle/viewTripSheets.do?statusId=13&tripType=1&admin=No&menuClick=1" ><spring:message code="dailyoperations.subMenus.label.TripSettlement" text="default text"/> </a></li>
                      <li><a href="/throttle/viewTripSheets.do?statusId=14&tripType=1&admin=No&menuClick=1" ><spring:message code="dailyoperations.subMenus.label.SettledTrips" text="default text"/> </a></li>
                </ul>
    </li>-->
    <li class="nav-parent"><a href="#"><i class="fa fa-exchange"></i> <span><spring:message code="operations.label.Operation" text="default text"/> </span></a>
        <ul class="children">

            <li><a href="/throttle/consignmentNote.do&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.label.C-NoteCreate" text="default text"/> </a></li>
            <li><a href="/throttle/consignmentNoteView.do?admin=NomenuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.C-NoteView" text="default text"/> </a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-shopping-cart"></i> <span><spring:message code="subMenus.label.Billing" text="default text"/></span></a>
        <ul class="children">

            <li><a href="/throttle/viewClosedOrderForBilling.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="billing.subMenus.label.GenerateBilling" text="default text"/></a></li>
            <li><a href="/throttle/viewClosedRepoOrderForBilling.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="billing.subMenus.label.GenerateBilling(Repo&DSO)" text="default text"/></a></li>
            <li><a href="/throttle/viewOrderBillForSubmit.do?tripType=1&submitStatus=0&menuClick=1" > <i class="fa fa-cog"></i><spring:message code="billing.subMenus.label.SubmitBill" text="default text"/> </a></li>
            <li><a href="/throttle/viewOrderBillForSubmit.do?tripType=1&submitStatus=1&menuClick=1" > <i class="fa fa-cog"></i><spring:message code="billing.subMenus.label.ViewBill" text="default text"/> </a></li>
            <li><a href="/throttle/invoiceReceipts.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="billing.subMenus.label.invoice" text="default text"/></a></li>

        </ul>
    </li>
    <!-==========HSE manager======-->
    <%}else if(rolId.equals(1048)){%>
    <li class="nav-parent"><a href="#"><i class="fa fa-dashboard"></i> <span><spring:message code="subMenus.label.Dashboard" text="default text"/></span></a>
        <ul class="children" >
            <li><a href="/throttle/dashboardOperation.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="dashboard.subMenus.label.Operations" text="default text"/></a></li>
            <li><a href="/throttle/dashboardWorkshop.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="dashboard.subMenus.label.Workshop" text="default text"/></a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href=""><i class="fa fa-gears"></i> <span><spring:message code="subMenus.label.Settings" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/alterPassword.do?menuClick=1" ><spring:message code="settings.subMenus.label.ChangePassword" text="default text"/></a></li>

        </ul>
    </li>
    <li class="nav-parent"><a href=""><i class="fa fa-bell"></i> <span>Safety</span></a>
        <ul class="children">
            <li><a href="/throttle/handleCreateSafetyAlert.do?menuClick=1" >Create Alert</a></li>
            <li><a href="/throttle/handleViewSafetyAlert.do?menuClick=1" >View Alert</a></li>
        </ul>
    </li>
    <!-- ===================Sales Manager==============================-->
    <%}else if(rolId.equals(1030)){%>
    <li class="nav-parent"><a href="#"><i class="fa fa-dashboard"></i> <span><spring:message code="subMenus.label.Dashboard" text="default text"/></span></a>
        <ul class="children" >
            <li><a href="/throttle/dashboardOperation.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="dashboard.subMenus.label.Operations" text="default text"/></a></li>
            <li><a href="/throttle/dashboardWorkshop.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="dashboard.subMenus.label.Workshop" text="default text"/></a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href=""><i class="fa fa-gears"></i> <span><spring:message code="subMenus.label.Settings" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/alterPassword.do?menuClick=1" ><spring:message code="settings.subMenus.label.ChangePassword" text="default text"/></a></li>

        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-angle-double-up"></i> <span><spring:message code="subMenus.label.Sales" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/handleViewCustomer.do?menuClick=1" >&nbsp;Customer Master</a></li>
            <li><a href="/throttle/consignmentNote.do?menuClick=1" >&nbsp;C-Note Create</a></li>
            <li><a href="/throttle/consignmentNoteView.do?menuClick=1" >&nbsp;C-Note View</a></li>

        </ul>
    </li>
    <!-- ===================Store Manager==============================-->
    <%}else if(rolId.equals(1013)){%>
    <li class="nav-parent"><a href="#"><i class="fa fa-dashboard"></i> <span><spring:message code="subMenus.label.Dashboard" text="default text"/></span></a>
        <ul class="children" >
            <li><a href="/throttle/dashboardOperation.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="dashboard.subMenus.label.Operations" text="default text"/></a></li>
            <li><a href="/throttle/dashboardWorkshop.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="dashboard.subMenus.label.Workshop" text="default text"/></a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href=""><i class="fa fa-gears"></i> <span><spring:message code="subMenus.label.Settings" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/alterPassword.do?menuClick=1" ><spring:message code="settings.subMenus.label.ChangePassword" text="default text"/></a></li>

        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-building"></i> <span><spring:message code="subMenus.label.Stores" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/manageRacks.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="stores.subMenus.label.Rack" text="default text"/></a></li>
            <li><a href="/throttle/manageSubRack.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="stores.subMenus.label.SubRack" text="default text"/> </a></li>
            <li><a href="/throttle/manageParts.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="stores.subMenus.label.Parts" text="default text"/></a></li>
            <li><a href="/throttle/MRSApproval.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="stores.subMenus.label.MRSApproval" text="default text"/></a></li>
            <li><a href="/throttle/MRSList.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="stores.subMenus.label.View/IssueMRS" text="default text"/> </a></li>
            <li><a href="/throttle/localPurchaseRequest.do?menuClick=1"><i class="fa fa-cog"></i>Local Purchase Request </a></li>
            <li><a href="/throttle/localPurchase.do?menuClick=1"><i class="fa fa-cog"></i>Local Purchase  </a></li>
            <li><a href="/throttle/purchaseReceipt.do?menuClick=1"><i class="fa fa-cog"></i>Purchase Receipts </a></li>
            <!--                <li><a href="/throttle/barCodePrint.do?menuClick=1"><i class="fa fa-cog"></i>barCodePrint </a></li>-->
            <li><a href="/throttle/generateDirectMprPage.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="stores.subMenus.label.GenerateMPR" text="default text"/>  </a></li>
            <li><a href="/throttle/storesMprList.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="stores.subMenus.label.MPRList" text="default text"/> </a></li>
            <li><a href="/throttle/mprApprovalList.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="stores.subMenus.label.MPRApproval" text="default text"/>  </a></li>
            <li><a href="/throttle/modifyPO.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="stores.subMenus.label.ModifyPO" text="default text"/> </a></li>
            <li><a href="/throttle/requiredItems.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="stores.subMenus.label.RequeriedItems" text="default text"/> </a></li>
            <li><a href="/throttle/receiveInvoice.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="stores.subMenus.label.ReceiveDC" text="default text"/>  </a></li>
            <li><a href="/throttle/modifyGrnPage.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="stores.subMenus.label.ReceiveDCInvoice" text="default text"/>   </a></li>
        </ul>
    </li>
    <%}else{%>
    <li class="nav-parent"><a href="#"><i class="fa fa-dashboard"></i> <span><spring:message code="subMenus.label.Dashboard" text="default text"/></span></a>
        <ul class="children" >
            <li><a href="/throttle/dashboardOperation.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="dashboard.subMenus.label.Operations" text="default text"/></a></li>
            <li><a href="/throttle/dashboardWorkshop.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="dashboard.subMenus.label.Workshop" text="default text"/></a></li>
        </ul>
    </li>

    <li class="nav-parent"><a href=""><i class="fa fa-user"></i> <span><spring:message code="subMenus.label.HRMS" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/viewCompany1.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="hrms.subMenus.label.Company" text="default text"/> </a></li>
            <li><a href="/throttle/manageDepartment.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="hrms.subMenus.label.Department" text="default text"/></a></li>
            <li><a href="/throttle/viewDesign.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="hrms.subMenus.label.Designation" text="default text"/></a></li>
            <li><a href="/throttle/handleEmpViewPage.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="hrms.subMenus.label.Employees" text="default text"/></a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href=""><i class="fa fa-gears"></i> <span><spring:message code="subMenus.label.Settings" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/alterPassword.do?menuClick=1" ><spring:message code="settings.subMenus.label.ChangePassword" text="default text"/></a></li>
            <li><a href="/throttle/recoverPassword.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManagePassword" text="default text"/></a></li>
            <li><a href="/throttle/viewuser.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManageUsers" text="default text"/></a></li>
            <li><a href="/throttle/allroles.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManageRoles" text="default text"/></a></li>
            <li><a href="/throttle/userroles.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManageUserRoles" text="default text"/></a></li>
            <li><a href="/throttle/activerole.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManageRoleFunctions" text="default text"/></a></li>

        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-bus"></i> <span><spring:message code="subMenus.label.Trucks" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/handleViewAxle.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.AxleMaster" text="default text"/></a></li>
            <li><a href="/throttle/viewType.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.VehicleType" text="default text"/></a></li>
            <li><a href="/throttle/viewMfr.do?fleetType=1&menuClick=1"><i class="fa fa-cog"></i><spring:message code="trucks.subMenus.label.Make" text="default text"/> </a></li>
            <li><a href="/throttle/viewModelPage.do?fleetType=1&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.Model" text="default text"/></a></li>
            <li><a href="/throttle/searchVehiclePage.do?fleetTypeId=1&listId=&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.Vehicles" text="default text"/></a></li>
            <li><a href="/throttle/resetVehicleKM.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="trucks.subMenus.label.ResetVehicleKM" text="default text"/> </a></li>
            <li><a href="/throttle/searchVehiclePage.do?listId=5&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.VehiclePurchase" text="default text"/> </a></li>
            <li><a href="/throttle/searchVehiclePage.do?listId=1&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.VehicleInsurance" text="default text"/></a></li>
            <li><a href="/throttle/searchVehiclePage.do?listId=3&menuClick=1"><i class="fa fa-cog"></i><spring:message code="trucks.subMenus.label.VehicleRegistration" text="default text"/> </a></li>
            <li><a href="/throttle/viewType.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="trucks.subMenus.label.VehicleInspection" text="default text"/> </a></li>
            <li><a href="/throttle/viewType.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trucks.subMenus.label.VehicleRoadPermit" text="default text"/></a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-truck"></i> <span><spring:message code="subMenus.label.Trailers" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/handleViewTrailerTypePage.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trailers.subMenus.label.TrailerType" text="default text"/></a></li>
            <li><a href="/throttle/viewMfr.do?fleetType=2&menuClick=2"><i class="fa fa-cog"></i> <spring:message code="trailers.subMenus.label.Make" text="default text"/></a></li>
            <li><a href="/throttle/viewModelPage.do?fleetType=2&menuClick=2"><i class="fa fa-cog"></i><spring:message code="trailers.subMenus.label.Model" text="default text"/> </a></li>
            <li><a href="/throttle/searchTrailerPage.do?fleetTypeId=2&listId=&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trailers.subMenus.label.Trailers" text="default text"/></a></li>
            <li><a href="/throttle/searchVehiclePage.do?listId=5&menuClick=1"><i class="fa fa-cog"></i> <spring:message code="trailers.subMenus.label.TrailerPurchase" text="default text"/> </a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-retweet"></i> <span><spring:message code="subMenus.label.Accidents" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/handleVehicleAccident.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="accidents.subMenus.label.Accidents" text="default text"/> </a></li>
            <li><a href="/throttle/vehicleAccidentDetails.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="accidents.subMenus.label.AccidentDetails" text="default text"/> </a></li>
            <li><a href="/throttle/vehicleAccidentHistory.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="accidents.subMenus.label.AccidentHistory" text="default text"/>  </a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-users"></i> <span><spring:message code="subMenus.label.Vendors" text="default text"/></span></a>
        <ul class="children">

            <li><a href="/throttle/manageVendorPage.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="vendors.subMenus.label.Vendors" text="default text"/> </a></li>
            <li><a href="/throttle/manageVendorItemConfigPage.do?menuClick=1"><i class="fa fa-cog"></i>Config Vendor Items </a></li>
            <li><a href="/throttle/handleVendorPaymentPage.do?menuClick=1"><i class="fa fa-cog"></i>Vendor Payment </a></li>

            <li><a href="/throttle/manageFleetVendorPage.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="vendors.subMenus.label.FleetVendorContract" text="default text"/></a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-angle-double-up"></i> <span><spring:message code="subMenus.label.Sales" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/handleViewCustomer.do?menuClick=1" ><i class="fa fa-cog"></i>&nbsp;Customer Master</a></li>
            <li><a href="/throttle/consignmentNote.do?menuClick=1" ><i class="fa fa-cog"></i>&nbsp;C-Note Create</a></li>
            <li><a href="/throttle/consignmentNoteView.do?menuClick=1" ><i class="fa fa-cog"></i>&nbsp;C-Note View</a></li>

        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-motorcycle"></i> <span><spring:message code="subMenus.label.ServicePlan" text="default text"/> </span></a>
        <ul class="children">
            <li><a href="/throttle/manageService.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="serviceplan.subMenus.label.ManageServiceType" text="default text"/></a></li>
            <li><a href="/throttle/manageSection.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="serviceplan.subMenus.label.ManageSection" text="default text"/></a></li>
            <li><a href="/throttle/manageProblemAct.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="serviceplan.subMenus.label.ManageProblem" text="default text"/></a></li>
            <li><a href="/throttle/manageProblemActivity.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="serviceplan.subMenus.label.Activities" text="default text"/> </a></li>
            <li><a href="/throttle/handleCheckListConfigPage.do?menuClick=1"><i class="fa fa-cog"></i>checkList </a></li>
            <li><a href="/throttle/manageMaintenance.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="serviceplan.subMenus.label.PeriodicService" text="default text"/> </a></li>
            <!--                <li><a href="/throttle/searchFreeService.do?menuClick=1"><i class="fa fa-cog"></i> Config Free Service</a></li>-->
        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-wrench"></i> <span><spring:message code="subMenus.label.Service" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/createJobcard.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="service.subMenus.label.CreateJobCard" text="default text"/></a></li>
            <li><a href="/throttle/vehicleComplaintHist.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="service.subMenus.label.VehComplaintHistory" text="default text"/></a></li>
            <li><a href="/throttle/supervisorView.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="service.subMenus.label.ViewJobCard" text="default text"/></a></li>
            <li><a href="/throttle/chooseJobCard.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="service.subMenus.label.GenerateMRS" text="default text"/></a></li>
            <li><a href="/throttle/closeJobCardView.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="service.subMenus.label.CloseJobCard" text="default text"/></a></li>
            <li><a href="/throttle/ViewJobCardsForBilling.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="service.subMenus.label.GenerateBill" text="default text"/></a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-chain-broken"></i> <span><spring:message code="subMenus.label.Recondition" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/rcQueue.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="recondition.subMenus.label.RCQueue" text="default text"/> </a></li>
            <li><a href="/throttle/alterRcWo.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="recondition.subMenus.label.AlterRCWO" text="RC WO List"/></a></li>
            <li><a href="/throttle/modifyWO.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="recondition.subMenus.label.ModifyRCWorkOrder" text="default text"/></a></li>
            <li><a href="/throttle/receiveRcParts.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="recondition.subMenus.label.ReceiveRC" text="Receive RC"/></a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-building"></i> <span><spring:message code="subMenus.label.Stores" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/manageRacks.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="stores.subMenus.label.Rack" text="default text"/></a></li>
            <li><a href="/throttle/manageSubRack.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="stores.subMenus.label.SubRack" text="default text"/> </a></li>
            <li><a href="/throttle/manageParts.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="stores.subMenus.label.Parts" text="default text"/></a></li>
            <li><a href="/throttle/MRSApproval.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="stores.subMenus.label.MRSApproval" text="default text"/></a></li>
            <li><a href="/throttle/MRSList.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="stores.subMenus.label.View/IssueMRS" text="default text"/> </a></li>
            <li><a href="/throttle/localPurchaseRequest.do?menuClick=1"><i class="fa fa-cog"></i>Local Purchase Request </a></li>
            <li><a href="/throttle/localPurchase.do?menuClick=1"><i class="fa fa-cog"></i>Local Purchase  </a></li>
            <li><a href="/throttle/purchaseReceipt.do?menuClick=1"><i class="fa fa-cog"></i>Purchase Receipts </a></li>
            <li><a href="/throttle/handleBarcodeGenerationPage.do?menuClick=1"><i class="fa fa-cog"></i> Generate BarCode</a></li>
            <!--                <li><a href="/throttle/barCodePrint.do?menuClick=1"><i class="fa fa-cog"></i>barCodePrint </a></li>-->
            <li><a href="/throttle/generateDirectMprPage.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="stores.subMenus.label.GenerateMPR" text="default text"/>  </a></li>
            <li><a href="/throttle/storesMprList.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="stores.subMenus.label.MPRList" text="default text"/> </a></li>
            <li><a href="/throttle/mprApprovalList.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="stores.subMenus.label.MPRApproval" text="default text"/>  </a></li>
            <li><a href="/throttle/modifyPO.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="stores.subMenus.label.ModifyPO" text="default text"/> </a></li>
            <li><a href="/throttle/requiredItems.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="stores.subMenus.label.RequeriedItems" text="default text"/> </a></li>
            <li><a href="/throttle/receiveInvoice.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="stores.subMenus.label.ReceiveDC" text="default text"/>  </a></li>
            <li><a href="/throttle/modifyGrnPage.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="stores.subMenus.label.ReceiveDCInvoice" text="default text"/>   </a></li>
        </ul>
    </li>


    <li class="nav-parent"><a href="#"><i class="fa fa-refresh"></i> <span><spring:message code="subMenus.label.PrimaryOperation" text="default text"/> </span></a>
        <ul class="children">

            <li><a href="/throttle/viewRouteDetails.do?menuClick=1"><i class="fa fa-cog"></i>Routes</a></li>
            <li><a href="/throttle/tripPlanning.do?tripType=1&menuClick=1"><i class="fa fa-cog"></i><spring:message code="primaryOperation.label.tripPlanning" text="default text"/></a></li>
            <li><a href="/throttle/viewTripSheets.do?statusId=8&tripType=1&admin=No&menuClick=1"><i class="fa fa-cog"></i><spring:message code="primaryOperation.label.tripStart" text="default text"/></a></li>
            <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=1&admin=No&menuClick=1"><i class="fa fa-cog"></i>View Trip Sheet</a></li>
            <li><a href="/throttle/viewTripSheets.do?statusId=10&admin=No&tripType=1&menuClick=1"><i class="fa fa-cog"></i>Trip End</a></li>
            <li><a href="/throttle/viewTripSheets.do?statusId=12&admin=No&tripType=1&menuClick=1"><i class="fa fa-cog"></i>Trip Closure</a></li>
            <li><a href="/throttle/viewTripSheets.do?statusId=100&admin=No&tripType=1&menuClick=1"><i class="fa fa-cog"></i>Trip POD</a></li>
            <li><a href="/throttle/blockAdvanceGr.do?statusId=100&admin=No&tripType=1&menuClick=1"><i class="fa fa-cog"></i>Block Advance GR(s)</a></li>
            <li><a href="/throttle/cancelBlockedGr.do?statusId=100&admin=No&tripType=1&menuClick=1"><i class="fa fa-cog"></i>Cancel Blocked GR(s)</a></li>
            <li><a href="/throttle/viewCanceleGr.do?statusId=100&admin=No&tripType=1&menuClick=1"><i class="fa fa-cog"></i>cancelled GR(s)</a></li>
            <li><a href="/throttle/handleViewPrimaryDriver.do?menuClick=1"><i class="fa fa-cog"></i>Manage Driver</a></li>
            <li><a href="/throttle/vehicleDriverPlanning.do?menuClick=1"><i class="fa fa-cog"></i>Driver Mapping</a></li>
            <li><a href="/throttle/vehicleBPCLCardMapping.do?menuClick=1"><i class="fa fa-cog"></i>BPCL Card Mapping</a></li>
            <li><a href="/throttle/viewVehicleAvailability.do?tripType=1&menuClick=1"><i class="fa fa-cog"></i>Vehicle Availability</a></li>
            <li><a href="/throttle/viewTripSheets.do?statusId=6&admin=No&menuClick=1"><i class="fa fa-cog"></i>Trip Assign Vehicle</a></li>
            <li><a href="/throttle/viewTripSheets.do?statusId=8&admin=No&menuClick=1"><i class="fa fa-cog"></i>Trip Un Freeze</a></li>
            <li><a href="/throttle/viewTripSheets.do?statusId=13&admin=No&menuClick=1"><i class="fa fa-cog"></i>Trip Settlement</a></li>
            <li><a href="/throttle/viewTripSheets.do?statusId=14&admin=No&menuClick=1"><i class="fa fa-cog"></i>View Settled Trips</a></li>
            <li><a href="/throttle/viewTripSheets.do?statusId=16&admin=No&menuClick=1"><i class="fa fa-cog"></i>Trip Billed</a></li>
            <li><a href="/throttle/handlePrimaryDriverSettlement.do?menuClick=1"><i class="fa fa-cog"></i>Driver Settlement</a></li>

        </ul>
    </li>



<!--        <li class="nav-parent"><a href="#"><i class="fa fa-refresh"></i> <span><spring:message code="subMenus.label.LeasingOperation" text="default text"/> </span></a>


                    <ul class="children">

                        <li><a href="/throttle/vehicleDriverPlanning.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.Vehicle&DriverMapping" text="default text"/></a></li>
                        <li><a href="/throttle/viewVehicleAvailability.do?tripType=1&menuClick=1"><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.VehicleAvailability" text="default text"/></a></li>
                        <li><a href="/throttle/viewTrailerAvailability.do?tripType=1&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.TrailerAvailability" text="default text"/> </a></li>
                        <li><a href="/throttle/secondaryLeasingSheduleView.do?tripType=2&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.VehicleSchedule" text="default text"/> </a></li>
                         <li><a href="/throttle/viewTripSheets.do?statusId=8&tripType=3&admin=No&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.TripStart" text="default text"/> </a></li>
                            <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=3&admin=No&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.ViewTripSheet" text="default text"/> </a></li>
                            <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=3&admin=No&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.TripEnd" text="default text"/> </a></li>
                            <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=3&admin=No&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.TripClosure" text="default text"/> </a></li>
                            <li><a href="/throttle/handlefinanceAdvice.do?tripType=3&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.FinanceAdvice" text="default text"/> </a></li>
                             <li><a href="/throttle/handlefinanceRequest.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.FinanceAdviceApproval" text="default text"/>  </a></li>
                            <li><a href="/throttle/viewTripSheets.do?statusId=13&tripType=3&admin=No&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.TripSettlement" text="default text"/> </a></li>
                            <li><a href="/throttle/viewTripSheets.do?statusId=14&tripType=3&admin=No&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="leasingoperations.subMenus.label.SettledTrips" text="default text"/> </a></li>
                    </ul>


        </li>
        <li class="nav-parent"><a href="#"><i class="fa fa-exchange"></i> <span><spring:message code="subMenus.label.DailyOperation" text="default text"/> </span></a>
                    <ul class="children">
                        <li><a href="/throttle/viewGatePass.do?statusId=14&tripType=3&admin=No&menuClick=1" ><spring:message code="dailyoperations.subMenus.label.GatePass" text="default text"/> </a></li>
                            <li><a href="/throttle/viewGatePassPrint.do?statusId=14&tripType=3&admin=No&menuClick=1" ><spring:message code="dailyoperations.subMenus.label.GatePassPrint" text="default text"/> </a></li>
                        <li><a href="/throttle/vehicleDriverPlanning.do?menuClick=2"><i class="fa fa-cog"></i><spring:message code="dailyoperations.subMenus.label.Vehicle&DriverMapping" text="default text"/></a></li>
                        <li><a href="/throttle/viewVehicleAvailability.do?tripType=1&menuClick=2"><i class="fa fa-cog"></i><spring:message code="dailyoperations.subMenus.label.VehicleAvailability" text="default text"/> </a></li>
                        <li><a href="/throttle/viewTrailerAvailability.do?tripType=1&menuClick=2" ><i class="fa fa-cog"></i><spring:message code="dailyoperations.subMenus.label.TrailerAvailability" text="default text"/> </a></li>
                        <li><a href="/throttle/tripPlanning.do?tripType=1&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="dailyoperations.subMenus.label.TripPlanning" text="default text"/> </a></li>
                        <li><a href="/throttle/viewTripSheets.do?statusId=8&admin=No&tripType=1&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="dailyoperations.subMenus.label.TripStart" text="default text"/></a></li>
                        <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=1&admin=No&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="dailyoperations.subMenus.label.ViewTripSheet" text="default text"/> </a></li>
                         <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=1&admin=No&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="dailyoperations.subMenus.label.TripEnd" text="default text"/> </a></li>
                         <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=1&admin=No&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="dailyoperations.subMenus.label.TripClosure" text="default text"/> </a></li>
                         <li><a href="/throttle/handlefinanceAdvice.do?tripType=3&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="dailyoperations.subMenus.label.FinanceAdvice" text="default text"/> </a></li>
                             <li><a href="/throttle/handlefinanceRequest.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="dailyoperations.subMenus.label.FinanceApproval" text="default text"/> </a></li>
                            <li><a href="/throttle/viewTripSheets.do?statusId=13&tripType=3&admin=No&menuClick=1" ><spring:message code="dailyoperations.subMenus.label.TripSettlement" text="default text"/> </a></li>
                            <li><a href="/throttle/viewTripSheets.do?statusId=14&tripType=3&admin=No&menuClick=1" ><spring:message code="dailyoperations.subMenus.label.SettledTrips" text="default text"/> </a></li>
                    </ul>
        </li>

 <li class="nav-parent"><a href=""><i class="fa fa-bell"></i> <span>Safety</span></a>
                  <ul class="children">
                   <li><a href="/throttle/handleCreateSafetyAlert.do?menuClick=1" >Create Alert</a></li>
                   <li><a href="/throttle/handleViewSafetyAlert.do?menuClick=1" >View Alert</a></li>
                          </ul>
        </li>-->

    <li class="nav-parent"><a href="#"><i class="fa fa-shopping-cart"></i> <span><spring:message code="subMenus.label.Billing" text="default text"/></span></a>
        <ul class="children">

            <li><a href="/throttle/viewClosedOrderForBilling.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="billing.subMenus.label.GenerateBilling" text="default text"/></a></li>
            <li><a href="/throttle/viewClosedRepoOrderForBilling.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="billing.subMenus.label.GenerateBilling(Repo&DSO)" text="default text"/></a></li>
            <li><a href="/throttle/viewOrderBillForSubmit.do?tripType=1&submitStatus=0&menuClick=1" > <i class="fa fa-cog"></i><spring:message code="billing.subMenus.label.SubmitBill" text="default text"/> </a></li>
            <li><a href="/throttle/viewOrderBillForSubmit.do?tripType=1&submitStatus=1&menuClick=1" > <i class="fa fa-cog"></i><spring:message code="billing.subMenus.label.ViewBill" text="default text"/> </a></li>
            <li><a href="/throttle/invoiceReceipts.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="billing.subMenus.label.invoice" text="default text"/></a></li>

        </ul>
    </li>

    <li class="nav-parent"><a href="#"><i class="fa fa-beer"></i> <span><spring:message code="subMenus.label.Fuel" text="default text"/></span></a>
        <ul class="children">

            <li><a href="/throttle/viewVehicleMilleage.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="fuel.subMenus.label.MilleageConfiguration" text="default text"/>   </a></li>
            <li><a href="/throttle/viewFuelPriceMaster.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="fuel.subMenus.label.FuelPrice" text="default text"/>   </a></li>
            <li><a href="/throttle/vehicleTypeOperationgCostMaster.do?menuClick=1" ><i class="fa fa-cog"></i>VehicleTypeOperationgCost </a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-money"></i> <span><spring:message code="subMenus.label.Finance" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/financeYear.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.FinanceYear" text="default text"/> </a></li>
            <li><a href="/throttle/financePrimaryBank.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.BankMaster" text="default text"/> </a></li>
            <li><a href="/throttle/financeBank.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.BankBranchMaster" text="default text"/>  </a></li>
            <li><a href="/throttle/financeGroup.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.PrimaryGroup" text="default text"/> </a></li>
            <li><a href="/throttle/levelMaster.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.GroupMaster" text="default text"/> </a></li>
            <li><a href="/throttle/financeLedger.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.LedgerMaster" text="default text"/> </a></li>
            <!--	                <li><a href="/throttle/financeTax.do?menuClick=1"><i class="fa fa-cog"></i>Tax Master</a></li>-->
            <li><a href="/throttle/handleCRJPayment.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.CreditorJournalVoucher(CRJ)" text="default text"/> </a></li>
            <li><a href="/throttle/handlePaymentDetailsReport.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.Payments" text="default text"/></a></li>
            <li><a href="/throttle/handlePaymentPendingReport.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.PaymentsPendingReport" text="default text"/>  </a></li>
            <li><a href="/throttle/handlePaymentPaidReport.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.PaymentsPaidReport" text="default text"/>  </a></li>
            <li><a href="/throttle/invoiceReceipts.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.InvoiceReceipts" text="default text"/> </a></li>
            <li><a href="/throttle/invoicePendingReceipts.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.InvoicePendingReceipts" text="default text"/>  </a></li>
            <li><a href="/throttle/invoiceReceived.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.InvoiceReceived" text="default text"/> </a></li>
            <li><a href="/throttle/handleCreditNote.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.CreditNote" text="default text"/> </a></li>
            <li><a href="/throttle/handleCreditSearch.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.CreditSearch" text="default text"/> </a></li>
            <li><a href="/throttle/handleDebitNote.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.DebitNote" text="default text"/> </a></li>
            <li><a href="/throttle/handleDebitSearch.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.DebitSearch" text="default text"/> </a></li>
            <li><a href="/throttle/paymentEntry.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.CashPayments" text="default text"/> </a></li>
            <li><a href="/throttle/bankPaymentEntry.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.BankPayments" text="default text"/> </a></li>
            <li><a href="/throttle/contraEntry.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.ContraEntry" text="default text"/> </a></li>
            <li><a href="/throttle/journalEntry.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.JournalEntry" text="default text"/> </a></li>
            <li><a href="/throttle/bankPaymentClearance.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.BankTransaction" text="default text"/></a></li>
            <li><a href="/throttle/bankReconciliationStatement.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.BankReconditionStatament" text="default text"/></a></li>
            <li><a href="/throttle/ledgerReport.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.LedgerReport" text="default text"/></a></li>
            <li><a href="/throttle/dayBook.do?param=search"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.DayBook" text="default text"/></a></li>
            <li><a href="/throttle/dayBookSummary.do?param=search"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.DayBookSummary" text="default text"/></a></li>
            <li><a href="/throttle/trialBalanceNew.do?param=search"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.TrailBalance" text="default text"/></a></li>
            <li><a href="/throttle/profitAndLoss.do?param=search"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.P&L" text="default text"/></a></li>
            <li><a href="/throttle/balanceSheet.do?param=search"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.BalanceSheet" text="default text"/></a></li>
            <li><a href="/throttle/entriesReport.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.EntriesReport" text="default text"/></a></li>
            <li><a href="/throttle/costCenterReport.do?menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.CostCenterReport" text="default text"/></a></li>
            <li><a href="/throttle/bankReconciliationReport.do?param=view&menuClick=1"><i class="fa fa-cog"></i><spring:message code="finance.subMenus.label.BRSReport" text="default text"/></a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-envelope-o"></i> <span><spring:message code="subMenu.label.Reports"  text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/handleTripExpenseReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Trip Expense Report</a></li>
            <li><a href="/throttle/handleDailyTripPlanningDetails.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Transport Plan Report</a></li>
            <li><a href="/throttle/handleContainerMovement.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Container Movement Report</a></li>
            <li><a href="/throttle/handleVehicleVendorwiseTrip.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Vehicle VendorWise Trip Report</a></li>
            <li><a href="/throttle/handleDailyTripPlanningDetails.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Transport Plan Report</a></li>
            <li><a href="/throttle/handleTripReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Trip Report</a></li>
            <li><a href="/throttle/handleGRSummaryReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>GR Summary</a></li>
            <li><a href="/throttle/handleGRProfitability.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>GR Wise Profitability</a></li>
            <li><a href="/throttle/handleDailyCashReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Daily Cash Report(DCR)</a></li>
            <li><a href="/throttle/handleDailyVehicleStatusExcel.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>DMR Report</a></li>
            <li><a href="/throttle/handleVehicleDetainDetails.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Vehicle Detain/Idle Report</a></li>
            <li><a href="/throttle/handleInvoiceReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Invoice Report</a></li>
            <li><a href="/throttle/handlePendingGrForInvoiceReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Pending GR for Invoice Report</a></li>
            <li><a href="/throttle/handleInvoiceDetailsReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Invoice Details(Excel)</a></li>
            <li><a href="/throttle/handleInvoiceXMLDetailsReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Invoice Details(XML)</a></li>
            <li><a href="/throttle/searchContractRateLog.do?param=search&menuClick=1" ><i class="fa fa-cog"></i>Contract Rate Edit Log</a></li>
            <li><a href="/throttle/handleVehicleWiseProfitReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.VehicleWiseProfit"  text="default text"/></a></li>
            <li><a href="/throttle/handleVehicleUtilizationReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.VehicleUtilizationReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleViewTripSheet.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.TripSheetReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleAccountsReceivableReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.AccountsReceivable"  text="default text"/></a></li>
            <li><a href="/throttle/handleCustomerWiseProfitReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.CustomerwiseProfit"  text="default text"/>  </a></li>
            <li><a href="/throttle/handleTripWiseProfitReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.TripWiseProfitabilityReport(Closure)"  text="default text"/> </a></li>
            <li><a href="/throttle/handleTripWiseProfitReportEndStatus.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.TripWiseProfitabilityReport(End)"  text="default text"/> </a></li>
            <li><a href="/throttle/handleGPSLogReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.GPSLogReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleDriverSettlementDetails.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.DriverSettlementReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleBPCLTransactionDetails.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.BPCLTransactionReport"  text="default text"/> </a></li>
            <li><a href="/throttle/viewFinanceAdviceDetais.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.FinanceAdviceReport"  text="default text"/> </a></li>
            <li><a href="/throttle/viewGpsStatusDetais.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.GPSStatusReport"  text="default text"/> </a></li>
            <li><a href="/throttle/viewWFLWFUDetais.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.WFL&WFUMailSent"  text="default text"/> </a></li>
            <li><a href="/throttle/dashboardReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.Dashboard"  text="default text"/> </a></li>
            <li><a href="/throttle/handleViewTripSheetWfl.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.TripSheetReportWithWfl"  text="default text"/> </a></li>
            <li><a href="/throttle/handleFCWiseTripSummary.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.FCTripSummary"  text="default text"/> </a></li>
            <li><a href="/throttle/handleMarginWiseReport.do?param=search" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.MarginTripSummary"  text="default text"/> </a></li>
            <li><a href="/throttle/handleMonthWiseEmptyRunSummary.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.EmptyRunSummary"  text="default text"/> </a></li>
            <li><a href="/throttle/handleJobcardSumary.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.JobcardSummary"  text="default text"/> </a></li>
            <li><a href="/throttle/handleTripMergingDetails.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.TripMergingReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleVehicleOdometerReading.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.VehicleCurrentOdometerReadingReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleCustomerWiseMergingProfitReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.CustomerWiseProfitMerging"  text="default text"/> </a></li>
            <li><a href="/throttle/handleTripExtraExpenseReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.TripExtraExpenseReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleVehicleDriverAdvance.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.VehicleDriverAdvanceReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleToPayCustomerTripDetails.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.TopaycustomertripDetailsReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleTripVmrReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.TripVMRReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleTripBudgetReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.FCPerformanceReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleAccountMgrPerformanceReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.A/CMgrPerformanceReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleRNMExpenseReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.R&MSpentAnalysisReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleTyerExpenseReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.TyreAnalysisReport"  text="default text"/> </a></li>
            <li><a href="/throttle/report.do?menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.TruckDriverMappingReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handleOrderExpenseRevenueReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.OrderExpenseRevenueReport"  text="default text"/> </a></li>
            <li><a href="/throttle/handletripTrailerProfitReport.do?param=search&menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.TripTrailerProfitReport"  text="default text"/> </a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-pie-chart"></i> <span><spring:message code="subMenu.label.MIS"  text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/handlecustomerrevenue.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.CustomerWiseRevenue"  text="default text"/></a></li>
            <li><a href="/throttle/customerrevenuebillwise.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.BillWiseRevenue"  text="default text"/> </a></li>
            <li><a href="/throttle/revenuefcwise.do?menuClick=1" ><i class="fa fa-cog"></i> <spring:message code="subMenu.label.FCWiseRevenue"  text="default text"/> </a></li>
            <li><a href="/throttle/vehiclerevenue.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.VehicleWiseRevenue"  text="default text"/></a></li>
            <li><a href="/throttle/vehicletyperevenue.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.VehicleTypeWiseRevenue"  text="default text"/></a></li>
            <li><a href="/throttle/accountreceivablerevenue.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.CustomerWiseA.R"  text="default text"/></a></li>
            <li><a href="/throttle/accountreceivablebillwiserevenue.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.BillWiseA.R"  text="default text"/> </a></li>
            <li><a href="/throttle/totaloperationcost.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.TotalOperationCost"  text="default text"/></a></li>
            <li><a href="/throttle/totaloperationcostperbill.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.TotalOperationCost(Bill)"  text="default text"/></a></li>
            <li><a href="/throttle/fuelcostperbill.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.FuelCost"  text="default text"/> </a></li>
            <li><a href="/throttle/customerwiseprofit.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.CustomerWiseProfit"  text="default text"/></a></li>
            <li><a href="/throttle/vehiclewiseprofit.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.VehicleWiseProfit"  text="default text"/></a></li>
            <li><a href="/throttle/fcwiseprofit.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.FCWiseProfit"  text="default text"/> </a></li>
            <li><a href="/throttle/consolidatedreportpermonth.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.MonthWiseConsolidated"  text="default text"/></a></li>
            <li><a href="/throttle/consolidatedreportperbill.do?menuClick=1" ><i class="fa fa-cog"></i><spring:message code="subMenu.label.MonthWiseConsolidated(Bill)"  text="default text"/></a></li>
        </ul>
    </li>


    <li class="nav-parent"><a href="#"><i class="fa fa-camera-retro"></i> <span><spring:message code="fmsvehicles.label.FMSVehicle" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/handleBillListPage.do?menuClick=1" ><spring:message code="fmsvehicles.label.BillReport" text="default text"/></a></li>
            <li><a href="/throttle/mechanicPerformance.do?menuClick=1" >Mechanic Performance</a></li>
            <li><a href="/throttle/handleBodyBillPage.do?menuClick=1" ><spring:message code="fmsvehicles.label.ContractorsReport" text="default text"/></a></li>
            <!--<li><a href="/throttle/contractorActivityPage.do?menuClick=1" >Contractors Activity Report</a></li>-->
            <li><a href="/throttle/periodicServiceListPage.do?menuClick=1" ><spring:message code="fmsvehicles.label.ServiceDues" text="default text"/></a></li>
            <li><a href="/throttle/vehicleFCDue.do?menuClick=1" ><spring:message code="fmsvehicles.label.FCDues" text="default text"/></a></li>
            <li><a href="/throttle/vehicleInsuranceDue.do?menuClick=1" ><spring:message code="fmsvehicles.label.InsuranceDues" text="default text"/></a></li>
            <li><a href="/throttle/vehicleRoadTaxDue.do?menuClick=1" ><spring:message code="fmsvehicles.label.RoadTaxDues" text="default text"/></a></li>
            <li><a href="/throttle/vehiclePermitDue.do?menuClick=1" ><spring:message code="fmsvehicles.label.PermitDues" text="default text"/></a></li>
            <li><a href="/throttle/vehicleAMCDue.do?menuClick=1" ><spring:message code="fmsvehicles.label.AMCDues" text="default text"/></a></li>
            <li><a href="/throttle/vehicleServiceCost.do?menuClick=1" ><spring:message code="fmsvehicles.label.ServiceCostAnalyst" text="default text"/></a></li>

        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-shopping-basket"></i> <span><spring:message code="fmsstores.label.FMSStores" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/categorySearch.do?menuClick=1" ><spring:message code="fmsstores.label.CategorywiseReport" text="default text"/></a></li>
            <li><a href="/throttle/stkAvbPage.do?menuClick=1" ><spring:message code="fmsstores.label.StockAvailability" text="default text"/></a></li>
            <!--<li><a href="/throttle/handleTaxWiseItemsPage.do?menuClick=1" >Taxwise Items</a></li>
            -->             <li><a href="/throttle/reqItemsPage.do?menuClick=1" ><spring:message code="fmsstores.label.RequiredItems" text="default text"/></a></li>
            <li><a href="/throttle/handleOrderListPage.do?menuClick=1" ><spring:message code="fmsstores.label.WO/POReport" text="default text"/></a></li>
            <li><a href="/throttle/stockWorthPage.do?menuClick=1" ><spring:message code="fmsstores.label.StockWorth" text="default text"/></a></li>
            <li><a href="/throttle/stockPurchaseRep.do?menuClick=1" ><spring:message code="fmsstores.label.PurchaseReport" text="default text"/></a></li>
            <li><a href="/throttle/stockIssueRep.do?menuClick=1" ><spring:message code="fmsstores.label.IssueReport" text="default text"/></a></li><!--
            -->        <li><a href="/throttle/storesEff.do?menuClick=1" ><spring:message code="fmsstores.label.StoresEfficiency" text="default text"/></a></li><!--
            -->        <li><a href="/throttle/movingAve.do?menuClick=1" ><spring:message code="fmsstores.label.MovingAverage" text="default text"/></a></li>
            <li><a href="/throttle/receivedStockRpt.do?menuClick=1" ><spring:message code="fmsstores.label.ReceiveStock" text="default text"/></a></li>
            <!--                <li><a href="/throttle/stList.do?menuClick=1" >Stock Transfer </a></li>
            -->        <li><a href="/throttle/handleStockPurchasePage.do?menuClick=1" ><spring:message code="fmsstores.label.StockPurchaseReport" text="default text"/> </a></li><!--
                    <li><a href="/throttle/taxwisePOreport.do?menuClick=1" >TaxWise Purchase Report</a></li>-->
        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-map"></i> <span><spring:message code="fmsRCParts.label.FMSRCParts" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/rcItemPage.do?menuClick=1" ><spring:message code="fmsRCParts.label.RCPartsReport" text="default text"/></a></li>
            <li><a href="/throttle/handleRcBillList.do?menuClick=1" ><spring:message code="fmsRCParts.label.RCWorkOrderBills" text="default text"/></a></li>
        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-life-ring"></i> <span><spring:message code="fmsTyres.label.FMSTyres" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/tyrePoWoPage.do?menuClick=1" ><spring:message code="fmsTyres.label.TyrePo/WoReport" text="default text"/></a></li>
            <li><a href="/throttle/handleTyreLifeCyclePage.do?menuClick=1" ><spring:message code="fmsTyres.label.label.TyreLifeCycle" text="default text"/></a></li>
            <li><a href="/throttle/handleTyresRotationsList.do?menuClick=1"><i class="fa fa-cog"></i>Tyres Rotation</a></li>
            <li><a href="/throttle/tyresReportByVehicleNo.do?menuClick=1"><i class="fa fa-cog"></i>Tyres Report By Vehicle</a></li>
            <li><a href="/throttle/tyresReportByTyreNo.do?menuClick=1"><i class="fa fa-cog"></i>Tyres Report By Tyre No</a></li>
            <li><a href="/throttle/stockPurchaseRep.do?menuClick=1"><i class="fa fa-cog"></i>Tyres Purchase Report</a></li>
            <li><a href="/throttle/stockIssueRep.do?menuClick=1"><i class="fa fa-cog"></i>Tyres Issue Report</a></li>
            <li><a href="/throttle/stockWorthPage.do?menuClick=1"><i class="fa fa-cog"></i>Tyres Stock Worth Report</a></li>

        </ul>
    </li>
    <li class="nav-parent"><a href="#"><i class="fa fa-server"></i> <span><spring:message code="fmsRenderService.label.FMSRenderService" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/serviceSummaryPage.do?menuClick=1" ><spring:message code="fmsRenderService.label.ServiceSummary" text="default text"/></a></li>
            <li><a href="/throttle/serEff.do?menuClick=1" ><spring:message code="fmsRenderService.label.ServiceEfficiency" text="default text"/></a></li>
            <li><a href="/throttle/vehicleComplaintHist.do?menuClick=1" ><spring:message code="fmsRenderService.label.VehCompliantHistory" text="default text"/></a></li>
        </ul>
    </li>

    <li class="nav-parent"><a href="#"><i class="fa fa-bar-chart"></i> <span><spring:message code="FMSMIS.label.FMSMIS" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/handleServiceDailyMIS.do?menuClick=1" ><spring:message code="FMSMIS.label.JobcardRep" text="default text"/>1</a></li>
            <li><a href="/throttle/handleServiceGraphData.do?menuClick=1" ><spring:message code="FMSMIS.label.JobcardRep" text="default text"/>2</a></li>
            <li><a href="/throttle/vehicleComparisionReportPage.do?menuClick=1" ><spring:message code="FMSMIS.label.ManufacturerComparison" text="default text"/></a></li>
            <li><a href="/throttle/vehicleAgeComparisionReportPage.do?menuClick=1" ><spring:message code="FMSMIS.label.VehicleAgeDistribution" text="default text"/></a></li>
            <!--<li><a href="/throttle/salesTrendSearch.do?menuClick=1" >Sales Bill Trend</a></li>-->
            <li><a href="/throttle/stockWorthSearch.do?menuClick=1" ><spring:message code="FMSMIS.label.StockWorth" text="default text"/></a></li>
            <!--<li><a href="/throttle/vendorTrendSearch.do?menuClick=1" >Vendor Supply Trend</a></li>-->
            <li><a href="/throttle/problemDistributionSearch.do?menuClick=1" ><spring:message code="FMSMIS.label.ProblemDistribution" text="default text"/></a></li>
            <!--<li><a href="/throttle/externalLabourBillGraphData.do?menuClick=1" >External Labour Bill Trend</a></li>-->
            <%--<li><a href="/throttle/frame/report/Valume.html"  >Volume of Vehicle Services</a></li>--%>
            <li><a href="/throttle/topProblemSearch.do?menuClick=1"  ><spring:message code="FMSMIS.label.TopProblem" text="default text"/></a></li>
                <%--<li><a href="/throttle/frame/report/consumption.html"  >Stock Consumption</a></li>--%>
            <!--<li><a href="/throttle/scheduleDeviation.do?menuClick=1" >Schedule Deviation</a></li>
            <li><a href="/throttle/scrapGraphData.do?menuClick=1" >Scarp Value</a></li>-->
            <li><a href="/throttle/rcTrendGraphData.do?menuClick=1" ><spring:message code="FMSMIS.label.RCTrend" text="default text"/></a></li>
                <%--<li><a href="/throttle/frame/report/spareLife.html" >Spare Life</a></li>--%>
                <%--<li><a href="/throttle/frame/report/RCspareLife.html" >RC-Spare Life</a></li>--%>
            <li><a href="/throttle/RCexpense.do?menuClick=1" ><spring:message code="FMSMIS.label.RC-Expense" text="default text"/> </a></li>
            <!--<li><a href="/throttle/RCsaving.do?menuClick=1" >Saving By RC</a></li>-->
            <li><a href="/throttle/mileageReport.do?menuClick=1" ><spring:message code="FMSMIS.label.MileageReport" text="default text"/></a></li>
            <!--<li><a href="/throttle/vehicleServiceCostGraphData.do?menuClick=1" >Cost of Service</a></li>-->
            <%--bala--%>
            <%-- <li><a href="/throttle/usageTypewisereport.do?menuClick=1" >Usage Typewise Report</a></li>--%>
            <%--<li><a href="/throttle/frame/report/vehicleDeviation.html" >Vehicle Deviation </a></li>--%>
        </ul>
    </li>

    <li class="nav-parent"><a href="#"><i class="fa fa-table"></i> <span><spring:message code="TripAnalytics.label.TripAnalytics" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/content/RenderService/tripMisReport.jsp" >&nbsp;<img src="/throttle/images/icon_stand.png" alt="" /><spring:message code="TripAnalytics.label.TripMISReport" text="default text"/></a></li>
            <!--      <li><a href="/throttle/content/report/driverSettlementReport.jsp" >Driver Settlement </a></li>
                  <li><a href="/throttle/vehicleCurrentStatus.do?menuClick=1" >Vehicle Status</a></li>-->
            <li><a href="/throttle/companyWiseReportPage.do?menuClick=1" ><spring:message code="TripAnalytics.label.CompanyWiseTrip" text="default text"/> </a></li>
            <!--<!-- <li><a href="/throttle/vehiclePerformancePage.do?menuClick=1" >Vehicle Performance</a></li>
                 <li><a href="/throttle/driverPerformancePage.do?menuClick=1" >Driver Performance</a></li>    -->
            <!--                                        <li><a href="/throttle/lpsReport.do?menuClick=1" >LPS</a></li>-->
            <!--<li><a href="/throttle/vehicleProfitAndLossReport.do?menuClick=1" >Vehicle Profit And Loss Report</a></li> -->
            <li><a href="/throttle/tripWisePandLReport.do?menuClick=1" ><spring:message code="TripAnalytics.label.TripWiseProfitAndLoss" text="default text"/>  </a></li>
            <li><a href="/throttle/VehicleWisePandL.do?menuClick=1" ><spring:message code="TripAnalytics.label.VehicleWiseProfitAndLoss" text="default text"/>  </a></li>
            <li><a href="/throttle/content/RenderService/ProfitAndLossReport.jsp" ><spring:message code="TripAnalytics.label.ProfitAndLoss" text="default text"/>  </a></li>
            <li><a href="/throttle/districtWiseReport.do?menuClick=1" ><spring:message code="TripAnalytics.label.DistrictWiseReport" text="default text"/> </a></li>
            <li><a href="/throttle/tripSheetDetailsReport.do?menuClick=1" ><spring:message code="TripAnalytics.label.TripSheetDetails" text="default text"/> </a></li>
            <li><a href="/throttle/consigneeWiseDetailsReport.do?menuClick=1" ><spring:message code="TripAnalytics.label.ConsigneeWiseDetails" text="default text"/> </a></li>
            <li><a href="/throttle/tripSettlementDetailsReport.do?menuClick=1" ><spring:message code="TripAnalytics.label.TripSettlementDetails" text="default text"/> </a></li>
            <li><a href="/throttle/dieselReport.do?menuClick=1" ><spring:message code="TripAnalytics.label.DieselReport" text="default text"/> </a></li>
        </ul>
    </li>




    <%}%>


</ul>



</div><!-- leftpanelinner -->
</div><!-- leftpanel -->

<div class="mainpanel">

    <div class="headerbar">

        <a class="menutoggle"><i class="fa fa-bars"></i></a>

        <!--      <form class="searchform" action="/throttle/dashboardOperation.do?menuClick=1" method="post">
                <input type="text" class="form-control" name="keyword" placeholder="Search here..." />
        <%@ include file="../common/NewDesign/commonParameters.jsp" %></form>-->


        <div class="header-right">
            <ul class="headermenu">

                <li>
                    <div class="btn-group">
                        <span >
                            <a href="/throttle/languageChangeLoginRequest.do?paramName=en">english</a> &nbsp;|&nbsp;
                            <a href="/throttle/languageChangeLoginRequest.do?paramName=ar">العربية</a>
                        </span>
                    </div>
                </li>
                <li>
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle tp-icon" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-globe"></i>
                            <span class="badge">1</span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-head pull-right">
                            <h5 class="title"><spring:message code="sidemenu.label.YouHave5NewNotifications" text="default text"/></h5>
                            <ul class="dropdown-list gen-list">
                                <li class="new">
                                    <a href="">
                                        <span class="thumb"><img src="images/photos/user4.png" alt="" /></span>
                                        <span class="desc">
                                            <span class="name"><spring:message code="sidemenu.label.NewJobcard1960created" text="default text"/> <span class="badge badge-success"><spring:message code="sidemenu.label.new" text="default text"/></span></span>
                                        </span>
                                    </a>
                                </li>

                                <li class="new"><a href=""><spring:message code="sidemenu.label.SeeAllNotifications" text="default text"/></a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <!--                <img src="/throttle/content/NewDesign/images/photos/loggeduser.png" alt="" />-->
                            <spring:message code="sidemenu.label.WelcomeMr" text="default text"/>.<%= session.getAttribute("userName") %>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                            <!--                <li><a href="profile.html"><i class="glyphicon glyphicon-user"></i> My Profile</a></li>-->
                            <li><a href="#"><i class="glyphicon glyphicon-cog"></i> <spring:message code="sidemenu.label.AccountSettings" text="default text"/></a></li>
                            <li><a href="#"><i class="glyphicon glyphicon-question-sign"></i> <spring:message code="sidemenu.label.Help" text="default text"/></a></li>
                            <li><a href="/throttle/logout.do?menuClick=1"><i class="glyphicon glyphicon-log-out"></i><spring:message code="sidemenu.label.LogOut" text="default text"/></a></li>
                        </ul>
                    </div>
                </li>

            </ul>
        </div><!-- header-right -->

    </div><!-- headerbar -->

    <!--sidemenu.label.LogOut-->