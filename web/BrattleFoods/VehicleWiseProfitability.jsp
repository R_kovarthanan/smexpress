<%-- 
    Document   : sampleability
    Created on : Oct 30, 2013, 5:17:29 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

            <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
            <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
            <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
            <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
            <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
            <script src="/throttle/js/jquery.ui.core.js"></script>
            <script src="/throttle/js/jquery.ui.datepicker.js"></script>


    <script type="text/javascript">

            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });
  

        function submitPage(){
           
                 document.vehiclePerformance1.action="/throttle/BrattleFoods/searchvehicleReport.jsp";
                document.vehiclePerformance1.submit();
            }

        </script>

    </head>
    <%
     String menuPath = "Report >> Vehicle Wise Profitability";
     request.setAttribute("menuPath",menuPath);
     %>
    <body>
        <form name="vehiclePerformance1" method="post">
        <%@ include file="/content/common/path.jsp" %>
         <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="../images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="../images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="0" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Vehicle Wise Profitability Report</li>
                            </ul>
                            <div id="first">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                        <td><font color="red">*</font>Vehicle No</td>
                                        <td><input name="regno" id="regno" type="text" class="textbox" size="20" value="" autocomplete="off"></td>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  onclick="ressetDate(this);"></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" onclick="ressetDate(this);"></td>

                                        <td> <input type="hidden" name="days" id="days" value="" /></td>
                                        <td><input type="button" class="button"   value="Search" onclick="submitPage()"></td>
                                    </tr>


                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>
        </form>
      
    </body>
</html>
