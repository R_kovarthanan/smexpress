<%-- 
    Document   : gmaiAccountTest
    Created on : Nov 5, 2013, 2:45:15 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="stylesheet" href="/throttle/css/token-input.css" type="text/css" />
        <link rel="stylesheet" href="/throttle/css/token-input-facebook.css" type="text/css" />
        
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.tokeninput.js"></script>

    
        <title>JSP Page</title>
    </head>
    <body>
        <h2 id="theme">Facebook Theme</h2>
    <div>
        <input type="text" id="demo-input-facebook-theme" name="blah2" />
        <input type="button" value="Submit" />
        <script type="text/javascript">
        $(document).ready(function() {
            $("#demo-input-facebook-theme").tokenInput("http://shell.loopj.com/tokeninput/tvshows.php", {
                theme: "facebook"
            });
        });
        </script>
    </div>
    </body>
</html>
