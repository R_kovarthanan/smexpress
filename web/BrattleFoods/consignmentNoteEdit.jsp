
<!DOCTYPE html>
<%@page import="java.text.SimpleDateFormat" %>
<%@ include file="../common/NewDesign/header.jsp" %>
	<%@ include file="../common/NewDesign/sidemenu.jsp" %> 
	
<html>
    <head>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script type="text/javascript">

            //this autocomplete is used for consignee and consignor names
            $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#consignorName').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getConsignorName.do",
                            dataType: "json",
                            data: {
                                consignorName: request.term,
                                customerId: document.getElementById('customerId').value

                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                console.log(type);
                            }

                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        $('#consignorName').val(value);
                        $('#consignorPhoneNo').val(ui.item.Mobile);
                        $('#consignorAddress').val(ui.item.Address);
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    itemVal = '<font color="green">' + itemVal + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };

                $('#consigneeName').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getConsigneeName.do",
                            dataType: "json",
                            data: {
                                consigneeName: request.term,
                                customerId: document.getElementById('customerId').value
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        $('#consigneeName').val(value);
                        $('#consigneePhoneNo').val(ui.item.Mobile);
                        $('#consigneeAddress').val(ui.item.Address);
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    itemVal = '<font color="green">' + itemVal + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                }

            });

        </script>
        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>
        <script type="text/javascript">

            function convertStringToDate(y, m, d, h, mi, ss) {
                //1            
                //var dateString = "2013-08-09 20:02:03";
                var dateString = y + '-' + m + '-' + d + ' ' + h + ':' + mi + ':' + ss;
                //alert(dateString);
                var reggie = /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/;
                var dateArray = reggie.exec(dateString);
                var dateObject = new Date(
                        (+dateArray[1]),
                        (+dateArray[2]) - 1, // Careful, month starts at 0!
                        (+dateArray[3]),
                        (+dateArray[4]),
                        (+dateArray[5]),
                        (+dateArray[6])
                        );
                //alert(dateObject);
                return dateObject;
                /*
                 //2
                 var today = new Date();
                 var otherDay = new Date("12/30/2013");
                 alert(today >= otherDay);
                 
                 //3
                 var time1 = new Date();
                 alert(time1);
                 var time1ms= time1.getTime(time1); //i get the time in ms
                 var time2 = otherDay;
                 alert(time2);
                 var time2ms= time2.getTime(time2);
                 var difference= time2ms-time1ms;
                 var hours = Math.floor(difference / 36e5);
                 var minutes = Math.floor(difference % 36e5 / 60000);
                 var seconds = Math.floor(difference % 60000 / 1000);
                 alert(hours/24);
                 alert(minutes);
                 alert(seconds);
                 
                 var lapse = new Date(difference);
                 var tz_correction_minutes = new Date().getTimezoneOffset() - lapse.getTimezoneOffset();
                 lapse.setMinutes(time2.getMinutes() + tz_correction_minutes);
                 alert(lapse.getDate()-1+' days and '  +lapse.getHours()+':'+lapse.getMinutes()+':'+lapse.getSeconds());
                 */

            }

            function viewCustomerContract() {
                window.open('/throttle/viewCustomerContract.do?custId=' + $("#customerId").val(), 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
            }


            function setOriginDestination() {



                var tempVar = document.cNote.contractRoute.value;
                //alert(tempVar);
                var tempSplit = tempVar.split("-");
                document.cNote.vehTypeId.value = tempSplit[3];
                document.cNote.vehicleTypeId.value = tempSplit[3];
                //alert(document.cNote.vehTypeId.value);
                document.cNote.origin.value = tempSplit[1];
                document.cNote.destination.value = tempSplit[2];
                //alert(document.cNote.origin.value);
                //alert(document.cNote.destination.value);
                document.getElementById("routeContractId").value = tempSplit[0];
                var routeContractId = tempSplit[0];
                //fetch route course
                var pointTypeVal = "";
                var cntr = 0;
                $.ajax({
                    url: '/throttle/getContractRouteCourse.do',
                    data: {routeContractId: routeContractId},
                    dataType: 'json',
                    success: function(data) {
                        $.each(data, function(i, data) {
                            //data.Id).html(data.Name)
                            var tempId = data.Id.split("~");
                            var tempName = data.Name.split("~");
                            for (var x = 0; x < tempId.length; x++) {
                                cntr++;
                                if (x == 0) {
                                    pointTypeVal = 'Pick Up';
                                } else if (x == (tempId.length - 1)) {
                                    pointTypeVal = 'Drop';
                                } else {
                                    pointTypeVal = 'select';
                                }
                                //alert(tempId[x]);
                                var tempIdSplit = tempId[x].split("-");
                                addRouteCourse1a(tempIdSplit[0], tempName[x], cntr, pointTypeVal, tempIdSplit[1]);
                            }
                        });
                    }
                });



            }

            function checkRoute(val) {
                if (val > 1) {
                    var pointOrderValue = document.getElementById("pointOrder" + val).value
                    var pointIdValue = document.getElementById("pointId" + val).value
                    var prevVal = val - 1;
                    var nextVal = val + 1;
                    var pointIdValue = document.getElementById("pointId" + val).value
                    var pointIdValue = document.getElementById("pointId" + val).value
                }

            }
            function checkOrderSequence(val) {
                var pointOrderValue = document.getElementById("pointOrder" + val).value;
                if (pointOrderValue == '') {
                    alert("please fill in the route order sequence");
                    document.getElementById("pointOrder" + val).focus();
                    document.getElementById("pointName" + val).value = '';
                } else {
                    var pointOrderValue = document.getElementsByName("pointOrder");
                    var prevPointSeq = 0;
                    for (var m = 1; m <= pointOrderValue.length; m++) {
                        if (document.getElementById("pointOrder" + m).value == prevPointSeq) {
                            alert("please crosscheck and correct the  route order sequence");
                            document.getElementById("pointOrder" + val).focus();
                        }
                        prevPointSeq = document.getElementById("pointOrder" + m).value;
                    }
                }
            }

            function validateRoute(val) {
//            alert(val);
                var contractType = document.cNote.billingTypeId.value;
                //alert(contractType);
                if (contractType != '1' && contractType != '2') {

                    var pointName = document.getElementById("pointName" + val).value;
                    var pointIdValue = document.getElementById("pointId" + val).value;
                    var pointOrderValue = document.getElementById("pointOrder" + val).value;
                    var pointOrder = document.getElementsByName("pointOrder");
                    var pointNames = document.getElementsByName("pointName");
                    var pointIdPrev = 0;
                    var pointIdNext = 0;
                    var pointNamePrev = '';
                    var pointNameNext = '';
                    //            alert(pointOrderValue);
                    //            alert(pointOrder.length);
                    var prevPointOrderVal = parseInt(pointOrderValue) - 1;
                    var nextPointOrderVal = parseInt(pointOrderValue) + 1;
                    for (var m = 1; m <= pointOrder.length; m++) {
                        //                    alert("loop:"+m+" :"+document.getElementById("pointOrder"+m).value +"pointOrderValue:"+pointOrderValue+"prevPointOrderVal:"+prevPointOrderVal+"nextPointOrderVal:"+nextPointOrderVal);
                        if (document.getElementById("pointOrder" + m).value == prevPointOrderVal) {
                            pointIdPrev = document.getElementById("pointId" + m).value
                            pointNamePrev = document.getElementById("pointName" + m).value
                            //                        alert("pointIdPrev:"+pointIdPrev);
                        }
                        if (document.getElementById("pointOrder" + m).value == nextPointOrderVal) {
                            //                        alert("am here..");
                            pointIdNext = document.getElementById("pointId" + m).value
                            pointNameNext = document.getElementById("pointName" + m).value
                            //                        alert("pointIdNext:"+pointIdNext);
                        }
                    }
                    //            alert(pointIdValue);
                    //            alert(pointIdPrev);
                    //            alert(pointIdNext);

                    ajaxRouteCheck(pointIdValue, pointIdPrev, pointIdNext, val, pointName, pointNamePrev, pointNameNext);
                }
//            else{
//                var pointName = document.getElementById("pointName"+val).value;
//                var pointIdValue = document.getElementById("pointId"+val).value;
//                //alert(pointIdValue)
//                if(pointIdValue == ''){
//                    alert('please enter valid interim point');
//                    document.getElementById("pointName"+val).value = '';
//                    document.getElementById("pointName"+val).focus();
//                }
//            }

            }



            var podRowCount1 = 1;
            var podSno1 = 0;
            function addRouteCourse1(id, name, order, type, routeId, routeKm, routeReeferHr, startDate) {
                if (parseInt(podRowCount1) % 2 == 0)
                {
                    styl = "text2";
                } else {
                    styl = "text1";
                }
                podSno1++;
                var tab = document.getElementById("routePlan");
                var newrow = tab.insertRow(podRowCount1);

//            var cell = newrow.insertCell(0);
//            var cell0 = "<td class='text1' height='25' >" + podSno1 + "</td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;



                cell = newrow.insertCell(0);
                var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointOrder" + podSno1 + "' name='pointOrder' class='form-control' style='width:150px;height:40px' value='" + order + "' ></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

                cell = newrow.insertCell(1);
                var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointName" + podSno1 + "' name='pointName' class='form-control' style='width:150px;height:40px' value='" + name + "' ><input type='hidden' id='pointTransitHrs" + podSno1 + "'  name='pointTransitHrs' value='' ><input type='hidden' id='pointRouteKm" + podSno1 + "'  name='pointRouteKm' value='" + routeKm + "' ><input type='hidden' id='pointRouteReeferHr" + podSno1 + "'  name='pointRouteReeferHr' value='" + routeReeferHr + "' ><input type='hidden' id='pointRouteId" + podSno1 + "'  name='pointRouteId' value='" + routeId + "' ><input type='hidden' id='pointId" + podSno1 + "'  name='pointId' value='" + id + "' ></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

                if (type == 'select') {
                    cell = newrow.insertCell(2);
                    var cell0 = "<td class='text1' height='25' ><select name='pointType' id='pointType" + podSno1 + "' class='form-control' style='width:150px;height:40px'><option value='Pick Up'>Pick Up</option><option value='Drop'>Drop</option></select></td>";
                    cell.setAttribute("className", styl);
                    cell.innerHTML = cell0;
                } else {
                    cell = newrow.insertCell(2);
                    var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointType" + podSno1 + "'  name='pointType' class='form-control' style='width:150px;height:40px' value='" + type + "' ></td>";
                    cell.setAttribute("className", styl);
                    cell.innerHTML = cell0;
                }
                cell = newrow.insertCell(3);
                var cell0 = "<td class='text1' height='25' ><textarea name='pointAddresss' rows='2' cols='20' class='form-control' style='width:150px;height:40px' ></textarea></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

                cell = newrow.insertCell(4);
                var cell0 = "<td class='text1' height='25' ><input type='text' value='" + startDate + "'  readonly name='pointPlanDate'  id='pointPlanDate" + podSno1 + "'   class='datepicker' ></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

                cell = newrow.insertCell(5);
                var cell0 = "<td class='text1' height='25' >HH:<select name='pointPlanHour'  id='pointPlanHour" + podSno1 + "'   class='form-control' style='width:150px;height:40px'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select>MI:<select name='pointPlanMinute'   id='pointPlanMinute" + podSno1 + "'   class='form-control' style='width:150px;height:40px'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option><option value='32'>32</option><option value='33'>33</option><option value='34'>34</option><option value='35'>35</option><option value='36'>36</option><option value='37'>37</option><option value='38'>38</option><option value='39'>39</option><option value='40'>40</option><option value='41'>41</option><option value='42'>42</option><option value='43'>43</option><option value='44'>44</option><option value='45'>45</option><option value='46'>46</option><option value='47'>47</option><option value='48'>48</option><option value='49'>49</option><option value='50'>50</option><option value='51'>51</option><option value='52'>52</option><option value='53'>53</option><option value='54'>54</option><option value='55'>55</option><option value='56'>56</option><option value='57'>57</option><option value='58'>58</option><option value='59'>59</option><option value='60'>60</option></select></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

                cell = newrow.insertCell(6);
                var cell0 = "<td class='text1' height='25' >&nbsp;</td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

                podRowCount1++;

                $(document).ready(function() {
                    $("#datepicker").datepicker({
                        showOn: "button",
                        buttonImage: "calendar.gif",
                        buttonImageOnly: true

                    });
                });

                $(function() {
                    $(".datepicker").datepicker({
                        changeMonth: true, changeYear: true
                    });
                });
            }

            function addRouteCourse3(id, name, order, type, routeId, routeKm, routeReeferHr, startDate) {
                if (parseInt(podRowCount1) % 2 == 0)
                {
                    styl = "text2";
                } else {
                    styl = "text1";
                }
                podSno1++;
                var tab = document.getElementById("routePlan");
                var newrow = tab.insertRow(podRowCount1);


                cell = newrow.insertCell(0);
                var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointOrder" + podSno1 + "' name='pointOrder' class='form-control' style='width:150px;height:40px' value='" + order + "' ></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

                cell = newrow.insertCell(1);
                var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointName" + podSno1 + "' name='pointName' class='form-control' style='width:150px;height:40px' value='" + name + "' ><input type='hidden' id='pointTransitHrs" + podSno1 + "'  name='pointTransitHrs' value='' ><input type='hidden' id='pointRouteKm" + podSno1 + "'  name='pointRouteKm' value='" + routeKm + "' ><input type='hidden' id='pointRouteReeferHr" + podSno1 + "'  name='pointRouteReeferHr' value='" + routeReeferHr + "' ><input type='hidden' id='pointRouteId" + podSno1 + "'  name='pointRouteId' value='" + routeId + "' ><input type='hidden' id='pointId" + podSno1 + "'  name='pointId' value='" + id + "' ></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

                if (type == 'select') {
                    cell = newrow.insertCell(2);
                    var cell0 = "<td class='text1' height='25' ><select name='pointType' id='pointType" + podSno1 + "' class='form-control' style='width:150px;height:40px'><option value='Pick Up'>Pick Up</option><option value='Drop'>Drop</option></select></td>";
                    cell.setAttribute("className", styl);
                    cell.innerHTML = cell0;
                } else {
                    cell = newrow.insertCell(2);
                    var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointType" + podSno1 + "'  name='pointType' class='form-control' style='width:150px;height:40px' value='" + type + "' ></td>";
                    cell.setAttribute("className", styl);
                    cell.innerHTML = cell0;
                }
                cell = newrow.insertCell(3);
                var cell0 = "<td class='text1' height='25' ><textarea name='pointAddresss' rows='2' cols='20' class='form-control' style='width:150px;height:40px' ></textarea></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;
                if (type == 'Pick Up') {
                    cell = newrow.insertCell(4);
                    var cell0 = "<td class='text1' height='25' ><input type='text' onChange='validateTripSchedule();'  value='" + startDate + "'   readonly name='pointPlanDate'  id='pointPlanDate" + podSno1 + "'   class='datepicker' ></td>";
                    cell.setAttribute("className", styl);
                    cell.innerHTML = cell0;
                } else {
                    cell = newrow.insertCell(4);
                    var cell0 = "<td class='text1' height='25' ><input type='text' onChange='validateTransitTime(" + podSno1 + ");'  value='" + startDate + "'   readonly name='pointPlanDate'  id='pointPlanDate" + podSno1 + "'   class='datepicker' ></td>";
                    cell.setAttribute("className", styl);
                    cell.innerHTML = cell0;

                }
                if (type == 'Pick Up') {
                    cell = newrow.insertCell(5);
                    var cell0 = "<td class='text1' height='25' >HH:<select name='pointPlanHour' onChange='validateTripSchedule();'   id='pointPlanHour" + podSno1 + "'   class='form-control' style='width:150px;height:40px'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select>MI:<select name='pointPlanMinute'   id='pointPlanMinute" + podSno1 + "'   class='form-control' style='width:150px;height:40px'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option><option value='32'>32</option><option value='33'>33</option><option value='34'>34</option><option value='35'>35</option><option value='36'>36</option><option value='37'>37</option><option value='38'>38</option><option value='39'>39</option><option value='40'>40</option><option value='41'>41</option><option value='42'>42</option><option value='43'>43</option><option value='44'>44</option><option value='45'>45</option><option value='46'>46</option><option value='47'>47</option><option value='48'>48</option><option value='49'>49</option><option value='50'>50</option><option value='51'>51</option><option value='52'>52</option><option value='53'>53</option><option value='54'>54</option><option value='55'>55</option><option value='56'>56</option><option value='57'>57</option><option value='58'>58</option><option value='59'>59</option><option value='60'>60</option></select></td>";
                    cell.setAttribute("className", styl);
                    cell.innerHTML = cell0;
                } else {
                    cell = newrow.insertCell(5);
                    var cell0 = "<td class='text1' height='25' >HH:<select name='pointPlanHour' onChange='validateTransitTime(" + podSno1 + ");'   id='pointPlanHour" + podSno1 + "'   class='form-control' style='width:150px;height:40px'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select>MI:<select name='pointPlanMinute'   id='pointPlanMinute" + podSno1 + "'   class='form-control' style='width:150px;height:40px'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option><option value='32'>32</option><option value='33'>33</option><option value='34'>34</option><option value='35'>35</option><option value='36'>36</option><option value='37'>37</option><option value='38'>38</option><option value='39'>39</option><option value='40'>40</option><option value='41'>41</option><option value='42'>42</option><option value='43'>43</option><option value='44'>44</option><option value='45'>45</option><option value='46'>46</option><option value='47'>47</option><option value='48'>48</option><option value='49'>49</option><option value='50'>50</option><option value='51'>51</option><option value='52'>52</option><option value='53'>53</option><option value='54'>54</option><option value='55'>55</option><option value='56'>56</option><option value='57'>57</option><option value='58'>58</option><option value='59'>59</option><option value='60'>60</option></select></td>";
                    cell.setAttribute("className", styl);
                    cell.innerHTML = cell0;

                }



                podRowCount1++;

                $(document).ready(function() {
                    $("#datepicker").datepicker({
                        showOn: "button",
                        buttonImage: "calendar.gif",
                        buttonImageOnly: true

                    });
                });

                $(function() {
                    $(".datepicker").datepicker({
                        changeMonth: true, changeYear: true
                    });
                });
            }
            function addRouteCourse1a(id, name, order, type, routeId) {
                if (parseInt(podRowCount1) % 2 == 0)
                {
                    styl = "text2";
                } else {
                    styl = "text1";
                }
                podSno1++;
                var tab = document.getElementById("routePlan");
                var newrow = tab.insertRow(podRowCount1);

//            var cell = newrow.insertCell(0);
//            var cell0 = "<td class='text1' height='25' >" + podSno1 + "</td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;



                cell = newrow.insertCell(0);
                var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointOrder" + podSno1 + "' name='pointOrder' class='form-control' style='width:150px;height:40px' value='" + order + "' ></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

                cell = newrow.insertCell(1);
                var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointName" + podSno1 + "' name='pointName' class='form-control' style='width:150px;height:40px' value='" + name + "' ><input type='hidden' id='pointTransitHrs" + podSno1 + "'  name='pointTransitHrs' value='' ><input type='hidden' id='pointRouteKm" + podSno1 + "'  name='pointRouteKm' value='' ><input type='hidden' id='pointRouteReeferHr" + podSno1 + "'  name='pointRouteReeferHr' value='' ><input type='hidden' id='pointRouteId" + routeId + "'  name='pointRouteId' value='" + id + "' ><input type='hidden' id='pointId" + podSno1 + "'  readonly name='pointId' class='form-control' style='width:150px;height:40px' value='" + id + "' ></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

                if (type == 'select') {
                    cell = newrow.insertCell(2);
                    var cell0 = "<td class='text1' height='25' ><select name='pointType' id='pointType" + podSno1 + "' class='form-control' style='width:150px;height:40px'><option value='Pick Up'>Pick Up</option><option value='Drop'>Drop</option></select></td>";
                    cell.setAttribute("className", styl);
                    cell.innerHTML = cell0;
                } else {
                    cell = newrow.insertCell(2);
                    var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointType" + podSno1 + "'  name='pointType' class='form-control' style='width:150px;height:40px' value='" + type + "' ></td>";
                    cell.setAttribute("className", styl);
                    cell.innerHTML = cell0;
                }
                cell = newrow.insertCell(3);
                var cell0 = "<td class='text1' height='25' ><textarea name='pointAddresss' rows='2' cols='20' class='form-control' style='width:150px;height:40px' ></textarea></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

                cell = newrow.insertCell(4);
                var cell0 = "<td class='text1' height='25' ><input type='text'  value='" + startDate + "'   readonly name='pointPlanDate'  id='pointPlanDate" + podSno1 + "'   class='datepicker' ></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

                cell = newrow.insertCell(5);
                var cell0 = "<td class='text1' height='25' >HH:<select name='pointPlanHour'  id='pointPlanHour" + podSno1 + "'   class='form-control' style='width:150px;height:40px'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select>MI:<select name='pointPlanMinute'   id='pointPlanMinute" + podSno1 + "'   class='form-control' style='width:150px;height:40px'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option><option value='32'>32</option><option value='33'>33</option><option value='34'>34</option><option value='35'>35</option><option value='36'>36</option><option value='37'>37</option><option value='38'>38</option><option value='39'>39</option><option value='40'>40</option><option value='41'>41</option><option value='42'>42</option><option value='43'>43</option><option value='44'>44</option><option value='45'>45</option><option value='46'>46</option><option value='47'>47</option><option value='48'>48</option><option value='49'>49</option><option value='50'>50</option><option value='51'>51</option><option value='52'>52</option><option value='53'>53</option><option value='54'>54</option><option value='55'>55</option><option value='56'>56</option><option value='57'>57</option><option value='58'>58</option><option value='59'>59</option><option value='60'>60</option></select></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

                podRowCount1++;

                $(document).ready(function() {
                    $("#datepicker").datepicker({
                        showOn: "button",
                        buttonImage: "calendar.gif",
                        buttonImageOnly: true

                    });
                });

                $(function() {
                    $(".datepicker").datepicker({
                        changeMonth: true, changeYear: true
                    });
                });
            }

            function addRouteCourse2() {
                if (parseInt(podRowCount1) % 2 == 0)
                {
                    styl = "text2";
                } else {
                    styl = "text1";
                }

                var pointOrderValue = document.getElementById("pointOrder2").value;
                var prevRowRouteNameValue = document.getElementById("pointName" + parseInt(podRowCount1 - 1)).value;
                var prevRowPointIdValue = document.getElementById("pointId" + parseInt(podRowCount1 - 1)).value;
                var prevRowRouteOrderValue = document.getElementById("pointOrder" + parseInt(podRowCount1 - 1)).value;
                if (prevRowRouteNameValue.trim() == '' || prevRowPointIdValue == '') {
                    alert('please enter the enroute name for route order ' + prevRowRouteNameValue);
                    document.getElementById("pointName" + parseInt(podRowCount1 - 1)).focus();
                } else {

                    podSno1++;
                    var tab = document.getElementById("routePlan");
                    var newrow = tab.insertRow(podRowCount1 - 1);

                    //alert(pointOrderValue);
                    pointOrderValue = parseInt(pointOrderValue) + 1;
                    document.getElementById("pointOrder2").value = pointOrderValue;

                    pointOrderValue = parseInt(pointOrderValue) - 1;

                    //            var cell = newrow.insertCell(0);
                    //            var cell0 = "<td class='text1' height='25' >" + podSno1 + "</td>";
                    //            cell.setAttribute("className", styl);
                    //            cell.innerHTML = cell0;


                    cell = newrow.insertCell(0);
                    var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointOrder" + podSno1 + "' name='pointOrder' class='form-control' style='width:150px;height:40px' value='" + pointOrderValue + "' ></td>";
                    cell.setAttribute("className", styl);
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(1);
                    var cell0 = "<td class='text1' height='25' ><input type='text' id='pointName" + podSno1 + "' onChange='validateRoute(" + podSno1 + ");'  name='pointName' class='form-control' style='width:150px;height:40px' value='' ><input type='hidden' id='pointTransitHrs" + podSno1 + "'  name='pointTransitHrs' value='' ><input type='hidden' id='pointRouteKm" + podSno1 + "'  name='pointRouteKm' value='' ><input type='hidden' id='pointRouteReeferHr" + podSno1 + "'  name='pointRouteReeferHr' value='' ><input type='hidden' id='pointRouteId" + podSno1 + "'  name='pointRouteId' value='0' ><input type='hidden' id='pointId" + podSno1 + "'  readonly name='pointId' class='form-control' style='width:150px;height:40px' value='' ></td>";
                    cell.setAttribute("className", styl);
                    cell.innerHTML = cell0;
                    callAjax(podSno1);
                    cell = newrow.insertCell(2);
                    var cell0 = "<td class='text1' height='25' ><select name='pointType' id='pointType" + podSno1 + "' class='form-control' style='width:150px;height:40px'><option value='Pick Up'>Pick Up</option><option value='Drop'>Drop</option></select></td>";
                    cell.setAttribute("className", styl);
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(3);
                    var cell0 = "<td class='text1' height='25' ><textarea id='pointAddresss" + podSno1 + "' name='pointAddresss' rows='2' cols='20' class='form-control' style='width:150px;height:40px' ></textarea></td>";
                    cell.setAttribute("className", styl);
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(4);
                    var cell0 = "<td class='text1' height='25' ><input type='text'  value=''  onChange='validateTransitTime(" + podSno1 + ");'     readonly name='pointPlanDate'  id='pointPlanDate" + podSno1 + "'   class='datepicker' ></td>";
                    cell.setAttribute("className", styl);
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(5);
                    var cell0 = "<td class='text1' height='25' >HH:<select name='pointPlanHour'  onChange='validateTransitTime(" + podSno1 + ");'    id='pointPlanHour" + podSno1 + "'   class='form-control' style='width:150px;height:40px'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select>MI:<select name='pointPlanMinute'   id='pointPlanMinute" + podSno1 + "'   class='form-control' style='width:150px;height:40px'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option><option value='32'>32</option><option value='33'>33</option><option value='34'>34</option><option value='35'>35</option><option value='36'>36</option><option value='37'>37</option><option value='38'>38</option><option value='39'>39</option><option value='40'>40</option><option value='41'>41</option><option value='42'>42</option><option value='43'>43</option><option value='44'>44</option><option value='45'>45</option><option value='46'>46</option><option value='47'>47</option><option value='48'>48</option><option value='49'>49</option><option value='50'>50</option><option value='51'>51</option><option value='52'>52</option><option value='53'>53</option><option value='54'>54</option><option value='55'>55</option><option value='56'>56</option><option value='57'>57</option><option value='58'>58</option><option value='59'>59</option><option value='60'>60</option></select></td>";
                    cell.setAttribute("className", styl);
                    cell.innerHTML = cell0;

                    podRowCount1++;

                    document.getElementById('pointName' + podSno1).focus();

                    $(document).ready(function() {
                        $("#datepicker").datepicker({
                            showOn: "button",
                            buttonImage: "calendar.gif",
                            buttonImageOnly: true

                        });
                    });

                    $(function() {
                        $(".datepicker").datepicker({
                            changeMonth: true, changeYear: true
                        });
                    });
                }
            }





        </script>


        <script type="text/javascript">

            var httpReqRouteCheck;
            var temp = "";
            function ajaxRouteCheck(pointIdValue, pointIdPrev, pointIdNext, val, pointName, pointNamePrev, pointNameNext)
            {

                var url = "/throttle/checkForRoute.do?pointIdValue=" + pointIdValue + "&pointIdPrev=" + pointIdPrev + "&pointIdNext=" + pointIdNext;
                if (pointIdValue != '') {
                    if (window.ActiveXObject)
                    {
                        httpReqRouteCheck = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)
                    {
                        httpReqRouteCheck = new XMLHttpRequest();
                    }
                    httpReqRouteCheck.open("GET", url, true);
                    httpReqRouteCheck.onreadystatechange = function() {
                        processAjaxRouteCheck(val, pointName, pointNamePrev, pointNameNext);
                    };
                    httpReqRouteCheck.send(null);
                } else {
                    alert("invalid interim point: " + pointName);
                    document.getElementById("pointName" + val).value = '';
                    document.getElementById("pointName" + val).focus();
                }
            }

            function processAjaxRouteCheck(val, pointName, pointNamePrev, pointNameNext)
            {
                if (httpReqRouteCheck.readyState == 4)
                {
                    if (httpReqRouteCheck.status == 200)
                    {
                        temp = httpReqRouteCheck.responseText.valueOf();
                        //alert(temp);
                        var tempVal = temp.split('~');
                        if (tempVal[0] == 0) {
                            alert("valid route does not exists for the selected point: " + pointName);
                            document.getElementById("pointName" + val).value = '';
                            document.getElementById("pointName" + val).focus();
                        }
                        if (tempVal[0] == 1) {
                            alert("valid route does not exists between " + pointName + " and " + pointNameNext);
                            document.getElementById("pointName" + val).value = '';
                            document.getElementById("pointName" + val).focus();
                        }
                        if (tempVal[0] == 2) {
                            document.getElementById("pointOrder" + val).focus();
                            var tempValSplit = tempVal[1].split("-");
                            document.getElementById("pointRouteId" + (val - 1)).value = tempValSplit[0];
                            document.getElementById("pointRouteKm" + (val - 1)).value = tempValSplit[1];
                            document.getElementById("pointRouteReeferHr" + (val - 1)).value = tempValSplit[2];
                            document.getElementById("pointTransitHrs" + (val - 1)).value = tempValSplit[3];


                            tempValSplit = tempVal[2].split("-");
                            document.getElementById("pointRouteId" + (val + 1)).value = tempValSplit[0];
                            document.getElementById("pointRouteKm" + (val + 1)).value = tempValSplit[1];
                            document.getElementById("pointRouteReeferHr" + (val + 1)).value = tempValSplit[2];
                            document.getElementById("pointTransitHrs" + (val + 1)).value = tempValSplit[3];
                        }
                    }
                    else
                    {
                        alert("Error loading page\n" + httpReqRouteCheck.status + ":" + httpReqRouteCheck.statusText);
                    }
                }
            }




            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true
                });
            });
        </script>
        <script type="text/javascript" language="javascript">
            var poItems = 0;
            var rowCount = '';
            var snumber = '';
            function addAllowanceRow()
            {
                if (snumber < 19) {
                    var tab = document.getElementById("expenseTBL");
                    var rowCount = tab.rows.length;

                    snumber = parseInt(rowCount) - 1;

                    var newrow = tab.insertRow(parseInt(rowCount) - 1);
                    newrow.height = "30px";
                    // var temp = sno1-1;
                    var cell = newrow.insertCell(0);
                    var cell0 = "<td><input type='hidden'  name='itemId' /> " + snumber + "</td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(1);
                    cell0 = "<td class='text1'><input name='driName' type='text' class='form-control' style='width:150px;height:40px' id='driName' onkeyup='getDriverName(" + snumber + ")' /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(2);
                    cell0 = "<td class='text1'><input name='cleanerName' type='text' class='form-control' style='width:150px;height:40px' id='cleanerName' /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(3);
                    cell0 = "<td class='text1'><input name='date' type='text' id='date' class='datepicker' /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    // rowCount++;

                    $(".datepicker").datepicker({
                        /*altField: "#alternate",
                         altFormat: "DD, d MM, yy"*/
                        changeMonth: true, changeYear: true
                    });
                }
                document.cNote.expenseId.value = snumber;
            }

            function delAllowanceRow() {
                try {
                    var table = document.getElementById("expenseTBL");
                    rowCount = table.rows.length - 1;
                    for (var i = 2; i < rowCount; i++) {
                        var row = table.rows[i];
                        var checkbox = row.cells[6].childNodes[0];
                        if (null != checkbox && true == checkbox.checked) {
                            if (rowCount <= 1) {
                                alert("Cannot delete all the rows");
                                break;
                            }
                            table.deleteRow(i);
                            rowCount--;
                            i--;
                            sno--;
                        }
                    }
                    document.cNote.expenseId.value = sno;
                } catch (e) {
                    alert(e);
                }
            }
            function parseDouble(value) {
                if (typeof value == "string") {
                    value = value.match(/^-?\d*/)[0];
                }
                return !isNaN(parseInt(value)) ? value * 1 : NaN;
            }

            function estimateFreight() {
                var billingTypeId = document.getElementById("billingTypeId").value;
                var customerTypeId = document.getElementById("customerTypeId").value;
                var editCountValue = document.getElementById("editCountValue").value;
                //customertypeid 1-> contract; 2-> walk in
                //billling type id 1 -> point to point; 2 -> point to point based on wt; 3 -> actual weight
                var totalKm = 0;
                var totalHr = 0;
                var rateWithReefer = 0;
                var rateWithoutReefer = 0;
                if (editCountValue == 0) {
                    if (customerTypeId == 1 && billingTypeId == 1) {//point to point
                        var temp = document.cNote.vehTypeIdContractTemp.value;
                        if (temp == 0) {
                            alert("Freight cannot be estimated as the vehicle type is not chosen");
                        } else {
                            var temp = document.cNote.vehTypeIdContractTemp.value;
                            //alert(temp);
                            var tempSplit = temp.split("-");
                            rateWithReefer = tempSplit[5];
                            rateWithoutReefer = tempSplit[6];
                            totalKm = tempSplit[2];
                            totalHr = tempSplit[3];
                            if (document.cNote.reeferRequired.value == 'Yes') {
                                document.cNote.totFreightAmount.value = rateWithReefer;
                            } else {
                                document.cNote.totFreightAmount.value = rateWithoutReefer;
                            }
                            document.cNote.totalKm.value = totalKm;
                            document.cNote.totalHours.value = totalHr;
                        }
                    } else if (customerTypeId == 1 && billingTypeId == 2) {//point to point weight
                        var temp = document.cNote.vehTypeIdContractTemp.value;
                        if (temp == 0) {
                            alert("Freight cannot be estimated as the vehicle type is not chosen");
                        } else {
                            var tempSplit = temp.split("-");
                            rateWithReefer = tempSplit[7];
                            rateWithoutReefer = tempSplit[8];
                            totalKm = tempSplit[2];
                            totalHr = tempSplit[3];
                            var totalWeight = document.cNote.totalWeightage.value;
                            if (document.cNote.reeferRequired.value == 'Yes') {
                                document.cNote.totFreightAmount.value = (parseFloat(rateWithReefer) * parseFloat(totalWeight)).toFixed(2);
                            } else {
                                document.cNote.totFreightAmount.value = (parseFloat(rateWithoutReefer) * parseFloat(totalWeight)).toFixed(2);
                            }
                            document.cNote.totalKm.value = totalKm;
                            document.cNote.totalHours.value = totalHr;
                        }
                    } else if (customerTypeId == 1 && billingTypeId == 3) {//actual kms
                        var pointRouteKm = document.getElementsByName("pointRouteKm");
                        var pointRouteReeferHr = document.getElementsByName("pointRouteReeferHr");
                        //calculate total km and total hrs
                        for (var i = 0; i < pointRouteKm.length; i++) {
                            totalKm = totalKm + parseFloat(pointRouteKm[i].value);
                            totalHr = totalHr + parseFloat(pointRouteReeferHr[i].value);
                        }
                        var temp = document.cNote.vehTypeIdTemp.value;
                        if (temp == 0) {
                            alert("Freight cannot be estimated as the vehicle type is not chosen");
                        } else {
                            var tempSplit = temp.split("-");
                            var vehicleRatePerKm = tempSplit[1];
                            var reeferRatePerHour = tempSplit[2];


                            var vehicleFreight = parseFloat(totalKm) * parseFloat(vehicleRatePerKm);
                            var reeferFreight = parseFloat(totalHr) * parseFloat(reeferRatePerHour);

                            if (document.cNote.reeferRequired.value == 'Yes') {
                                document.cNote.totFreightAmount.value = (parseFloat(vehicleFreight) + parseFloat(reeferFreight)).toFixed(2);
                            } else {
                                document.cNote.totFreightAmount.value = parseFloat(vehicleFreight).toFixed(2);
                            }
                        }
                        document.cNote.totalKm.value = totalKm;
                        document.cNote.totalHours.value = totalHr;
                    } else if (customerTypeId == 2) { //walk in

                        var pointRouteKm = document.getElementsByName("pointRouteKm");
                        var pointRouteReeferHr = document.getElementsByName("pointRouteReeferHr");
                        var walkInBillingTypeId = document.cNote.walkInBillingTypeId.value;
                        //calculate total km and total hrs
                        for (var i = 0; i < pointRouteKm.length; i++) {
                            totalKm = totalKm + parseFloat(pointRouteKm[i].value);
                            totalHr = totalHr + parseFloat(pointRouteReeferHr[i].value);
                        }
                        if (walkInBillingTypeId == 1) {//fixed rate
                            rateWithReefer = document.cNote.walkinFreightWithReefer.value;
                            rateWithoutReefer = document.cNote.walkinFreightWithoutReefer.value;
                            if (document.cNote.reeferRequired.value == 'Yes') {
                                document.cNote.totFreightAmount.value = parseFloat(rateWithReefer).toFixed(2);
                            } else {
                                document.cNote.totFreightAmount.value = parseFloat(rateWithoutReefer).toFixed(2);
                            }
                        } else if (walkInBillingTypeId == 2) {//rate per km
                            rateWithReefer = document.cNote.walkinRateWithReeferPerKm.value;
                            rateWithoutReefer = document.cNote.walkinRateWithoutReeferPerKm.value;

                            if (document.cNote.reeferRequired.value == 'Yes') {
                                document.cNote.totFreightAmount.value = (parseFloat(totalKm) * parseFloat(rateWithReefer)).toFixed(2);
                            } else {
                                document.cNote.totFreightAmount.value = (parseFloat(totalKm) * parseFloat(rateWithoutReefer)).toFixed(2);

                            }
                        } else if (walkInBillingTypeId == 3) {// rate per kg
                            var totalWeight = document.cNote.totalWeightage.value;
                            rateWithReefer = document.cNote.walkinRateWithReeferPerKg.value;
                            rateWithoutReefer = document.cNote.walkinRateWithoutReeferPerKg.value;
                            if (document.cNote.reeferRequired.value == 'Yes') {
                                document.cNote.totFreightAmount.value = (parseFloat(rateWithReefer) * parseFloat(totalWeight)).toFixed(2);
                            } else {
                                document.cNote.totFreightAmount.value = (parseFloat(rateWithoutReefer) * parseFloat(totalWeight)).toFixed(2);
                            }

                        }
                        document.cNote.totFreightAmount1.value = document.cNote.totFreightAmount.value;
                        document.cNote.totalKm.value = totalKm;
                        document.cNote.totalHours.value = totalHr;
                    }

                } else {
                    // This else part for edit the consign freight rate

                    if (customerTypeId == 1 && billingTypeId == 1) {//point to point
                        var temp = document.cNote.editVehTypeIdContractTemp.value;
                        if (temp == 0) {
                            alert("Freight cannot be estimated as the vehicle type is not chosen");
                        } else {
                            var temp = document.cNote.editVehTypeIdContractTemp.value;
                            //alert(temp);
                            var tempSplit = temp.split("-");
                            rateWithReefer = tempSplit[5];
                            rateWithoutReefer = tempSplit[6];
                            totalKm = tempSplit[2];
                            totalHr = tempSplit[3];
                            if (document.cNote.reeferRequired.value == 'Yes') {
                                document.cNote.totFreightAmount.value = rateWithReefer;
                            } else {
                                document.cNote.totFreightAmount.value = rateWithoutReefer;
                            }
                            document.cNote.totalKm.value = totalKm;
                            document.cNote.totalHours.value = totalHr;
                        }
                    } else if (customerTypeId == 1 && billingTypeId == 2) {//point to point weight
                        var temp = document.cNote.editVehTypeIdContractTemp.value;
                        if (temp == 0) {
                            alert("Freight cannot be estimated as the vehicle type is not chosen");
                        } else {
                            var tempSplit = temp.split("-");
                            rateWithReefer = tempSplit[7];
                            rateWithoutReefer = tempSplit[8];
                            totalKm = tempSplit[2];
                            totalHr = tempSplit[3];
                            var totalWeight = document.cNote.totalWeightage.value;
                            if (document.cNote.reeferRequired.value == 'Yes') {
                                document.cNote.totFreightAmount.value = (parseFloat(rateWithReefer) * parseFloat(totalWeight)).toFixed(2);
                            } else {
                                document.cNote.totFreightAmount.value = (parseFloat(rateWithoutReefer) * parseFloat(totalWeight)).toFixed(2);
                            }
                            document.cNote.totalKm.value = totalKm;
                            document.cNote.totalHours.value = totalHr;
                        }
                    } else if (customerTypeId == 1 && billingTypeId == 3) {//actual kms
                        //calculate total km and total hrs
                        totalKm = document.getElementById('totalKm').value;
                        totalHr = document.getElementById('totalHours').value;
                        var temp = document.cNote.editVehTypeIdTemp.value;
                        //alert(temp);
                        if (temp == 0) {
                            alert("Freight cannot be estimated as the vehicle type is not chosen");
                        } else {
                            var tempSplit = temp.split("-");
                            var vehicleRatePerKm = tempSplit[1];
                            var reeferRatePerHour = tempSplit[2];


                            var vehicleFreight = parseFloat(totalKm) * parseFloat(vehicleRatePerKm);
                            var reeferFreight = parseFloat(totalHr) * parseFloat(reeferRatePerHour);

                            if (document.cNote.reeferRequired.value == 'Yes') {
                                document.cNote.totFreightAmount.value = (parseFloat(vehicleFreight) + parseFloat(reeferFreight)).toFixed(2);
                            } else {
                                document.cNote.totFreightAmount.value = parseFloat(vehicleFreight).toFixed(2);
                            }
                        }
                        document.cNote.totalKm.value = totalKm;
                        document.cNote.totalHours.value = totalHr;
                    } else if (customerTypeId == 2) { //walk in

                        var pointRouteKm = document.getElementsByName("pointRouteKm");
                        var pointRouteReeferHr = document.getElementsByName("pointRouteReeferHr");
                        var walkInBillingTypeId = document.cNote.walkInBillingTypeId.value;
                        //calculate total km and total hrs
                        for (var i = 0; i < pointRouteKm.length; i++) {
                            totalKm = totalKm + parseFloat(pointRouteKm[i].value);
                            totalHr = totalHr + parseFloat(pointRouteReeferHr[i].value);
                        }
                        if (walkInBillingTypeId == 1) {//fixed rate
                            rateWithReefer = document.cNote.walkinFreightWithReefer.value;
                            rateWithoutReefer = document.cNote.walkinFreightWithoutReefer.value;
                            if (document.cNote.reeferRequired.value == 'Yes') {
                                document.cNote.totFreightAmount.value = parseFloat(rateWithReefer).toFixed(2);
                            } else {
                                document.cNote.totFreightAmount.value = parseFloat(rateWithoutReefer).toFixed(2);
                            }
                        } else if (walkInBillingTypeId == 2) {//rate per km
                            rateWithReefer = document.cNote.walkinRateWithReeferPerKm.value;
                            rateWithoutReefer = document.cNote.walkinRateWithoutReeferPerKm.value;

                            if (document.cNote.reeferRequired.value == 'Yes') {
                                document.cNote.totFreightAmount.value = (parseFloat(totalKm) * parseFloat(rateWithReefer)).toFixed(2);
                            } else {
                                document.cNote.totFreightAmount.value = (parseFloat(totalKm) * parseFloat(rateWithoutReefer)).toFixed(2);

                            }
                        } else if (walkInBillingTypeId == 3) {// rate per kg
                            var totalWeight = document.cNote.totalWeightage.value;
                            rateWithReefer = document.cNote.walkinRateWithReeferPerKg.value;
                            rateWithoutReefer = document.cNote.walkinRateWithoutReeferPerKg.value;
                            if (document.cNote.reeferRequired.value == 'Yes') {
                                document.cNote.totFreightAmount.value = (parseFloat(rateWithReefer) * parseFloat(totalWeight)).toFixed(2);
                            } else {
                                document.cNote.totFreightAmount.value = (parseFloat(rateWithoutReefer) * parseFloat(totalWeight)).toFixed(2);
                            }

                        }
                        document.cNote.totFreightAmount1.value = document.cNote.totFreightAmount.value;
                        document.cNote.totalKm.value = totalKm;
                        document.cNote.totalHours.value = totalHr;
                    }



                }


            }

            function saveExp(obj) {
                document.cNote.buttonName.value = "save";
                obj.name = "none";
                if (document.cNote.expenseId.value != "") {
                    document.cNote.action = '/throttle/saveDriverExpenses.do';
                    document.cNote.submit();
                }
            }

            function getDriverName(sno) {
                var oTextbox = new AutoSuggestControl(document.getElementById("driName"), new ListSuggestions("driName", "/throttle/handleDriverSettlement.do?"));

            }
        </script>
        <script language="">
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
        </script>
    </head>

    <script type="text/javascript">
        var rowCount = 1;
        var sno = 0;
        var rowCount1 = 1;
        var sno1 = 0;
        var httpRequest;
        var httpReq;
        var styl = "";
        function addRow1() {
            if (parseInt(rowCount1) % 2 == 0)
            {
                styl = "text2";
            } else {
                styl = "text1";
            }
            var articlesCount = document.getElementById('articlesCount').value;
            //alert(articlesCount);
            if (sno1 == 0) {
                sno1 = articlesCount;
            } else {
                sno1++;
            }
            var tab = document.getElementById("addTyres1");
            //find current no of rows
            var rowCountNew = document.getElementById('addTyres1').rows.length;
            rowCountNew--;
            var newrow = tab.insertRow(rowCountNew);

            var cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' ><input type='hidden' name='consignmentArticleId' id='consignmentArticleId' value='0' class='form-control' style='width:150px;height:40px' > " + sno1 + "</td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            // Positions
            cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='30' ><input type='text' name='productCodes' class='form-control' style='width:150px;height:40px' >";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            // TyreIds
            var cell = newrow.insertCell(2);
            var cell0 = "<td class='text1' height='30' ><input type='text' name='batchCode' class='form-control' style='width:150px;height:40px' >";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            var cell = newrow.insertCell(3);
            var cell0 = "<td class='text1' height='30' ><input type='text' name='productNames' class='form-control' style='width:150px;height:40px' >";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(4);
            var cell1 = "<td class='text1' height='30' ><input type='text' name='packagesNos'   onKeyPress='return onKeyPressBlockCharacters(event);' id='packagesNos' class='form-control' style='width:150px;height:40px' value='' onkeyup='calcTotalPacks(this.value)'>";

            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;

            cell = newrow.insertCell(5);
            var cell1 = "<td class='text1' height='30' ><select name='uom' id='uom' class='form-control' style='width:80px;height:40px'><option value='1' selected >Box</option> <option value='2'>Bag</option><option value='3'>Pallet</option><option value='4'>Each</option></select> </td>";

            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;

            cell = newrow.insertCell(6);
            var cell0 = "<td class='text1' height='30' ><input type='text' name='weights'   onKeyPress='return onKeyPressBlockCharacters(event);' id='weights' class='form-control' style='width:150px;height:40px' value='' onkeyup='calcTotalWeights(this.value)' >";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            rowCount1++;
        }


        function calculateTravelHours(sno) {
            var endDates = document.getElementById('endDates' + sno).value;
            var endTimeIds = document.getElementById('endTimeIds' + sno).value;
            var tempDate1 = endDates.split("-");
            var tempTime1 = endTimeIds.split(":");
            var stDates = document.getElementById('stDates' + sno).value;
            var stTimeIds = document.getElementById('stTimeIds' + sno).value;
            var tempDate2 = stDates.split("-");
            var tempTime2 = stTimeIds.split(":");
            var prevTime = new Date(tempDate2[2], tempDate2[1], tempDate2[0], tempTime2[0], tempTime2[1]);  // Feb 1, 2011
            var thisTime = new Date(tempDate1[2], tempDate1[1], tempDate1[0], tempTime1[0], tempTime1[1]);              // now
            var difference = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
            var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
            document.getElementById('timeDifferences' + sno).value = hoursDifference;
        }

        function goToImportPage() {
            document.cNote.action = '/throttle/cnoteUploadPage.do';
            document.cNote.submit();
        }
    </script>
    <script>
        //endingPointIds
        function resetAddRow(sno) {
            if (document.getElementById('endingPointIds' + sno).value == document.getElementById('destination').value) {
//           addRow 
                document.getElementById('addRowDetails').style.display = 'none';
            }
        }
    </script> 

    <%--
        <script>
            var v = "Roseindia-1,Vhghg-2,hgdhjsgdjsd-3,hkjsdhksjdhk-4,jhdkjshdksd-5";
        </script>
        <% ArrayList list  new ArrayList();
            String st="<script>document.writeln(v)</script>";
            String[] temp = st.split(",");
            list.add(temp[0]);
            list.add(temp[1]);
            list.add(temp[2]);
            list.add(temp[3]);
            list.add(temp[4]);
            out.println("value="+list.size()); 
        %>
    --%>

    <script>
        //start ajax for vehicle Nos
        function callAjaxNew(val) {
            // Use the .autocomplete() method to compile the list based on input from user
            //alert(val);
            var pointNameId = 'pointName' + val;
            var pointIdId = 'pointId' + val;
            var prevPointId = 'pointId' + (parseInt(val) - 1);
            var pointOrder = 'pointOrder' + val;
            var pointOrderVal = $('#' + pointOrder).val();
            var prevPointOrderVal = parseInt(pointOrderVal) - 1;
            //alert(pointOrderVal);
            //alert(prevPointOrderVal);
            var prevPointId = 0;
            var pointIds = document.getElementsByName("pointId");
            var pointOrders = document.getElementsByName("pointOrder");
            for (var m = 0; m < pointIds.length; m++) {
                //alert("loop value:"+pointOrders[m].value +" : "+pointIds[m].value );
                if (pointOrders[m].value == prevPointOrderVal) {
                    //alert("am here:"+pointOrders[m].value +" : "+prevPointOrderVal);
                    prevPointId = pointIds[m].value;
                }
            }


            $.ajax({
                url: "/throttle/getCityList.do",
                dataType: "json",
                data: {
                    cityName: request.term,
                    prevPointId: prevPointId,
                    pointOrder: $('#' + pointOrder).val(),
                    billingTypeId: $('#billingTypeId').val(),
                    contractId: $('#contractId').val(),
                    textBox: 1
                },
                success: function(data) {
                    $.each(data, function(i, data) {
                        $('#originTemp').append(
                                $('<option style="width:150px"></option>').val(data.Id).html(data.Name)
                                )
                    });
                }
            });



        }
        function callAjax(val) {
            // Use the .autocomplete() method to compile the list based on input from user
            //alert(val);
            var pointNameId = 'pointName' + val;
            var pointIdId = 'pointId' + val;
            var prevPointId = 'pointId' + (parseInt(val) - 1);
            var pointOrder = 'pointOrder' + val;
            var pointOrderVal = $('#' + pointOrder).val();
            var prevPointOrderVal = parseInt(pointOrderVal) - 1;
            //alert(pointOrderVal);
            //alert(prevPointOrderVal);
            var prevPointId = 0;
            var pointIds = document.getElementsByName("pointId");
            var pointOrders = document.getElementsByName("pointOrder");
            for (var m = 0; m < pointIds.length; m++) {
                //alert("loop value:"+pointOrders[m].value +" : "+pointIds[m].value );
                if (pointOrders[m].value == prevPointOrderVal) {
                    //alert("am here:"+pointOrders[m].value +" : "+prevPointOrderVal);
                    prevPointId = pointIds[m].value;
                }
            }
            //alert(prevPointId);
            $('#' + pointNameId).autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCityList.do",
                        dataType: "json",
                        data: {
                            cityName: request.term,
                            prevPointId: prevPointId,
                            pointOrder: $('#' + pointOrder).val(),
                            billingTypeId: $('#billingTypeId').val(),
                            contractId: $('#contractId').val(),
                            textBox: 1
                        },
                        success: function(data, textStatus, jqXHR) {
                            var contractType = document.cNote.billingTypeId.value;
                            //alert(contractType);
                            if (contractType == '1' || contractType == '2') {
                                if (data == '') {
                                    $('#' + pointIdId).val(0);
                                    alert('please enter valid interim point');
                                    $('#' + pointNameId).val('');
                                    $('#' + pointNameId).focus();
                                }
                            }

                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {

                            //console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    var id = ui.item.Id;
                    //alert(id+" : "+value);
                    $('#' + pointIdId).val(id);
                    $('#' + pointNameId).val(value);
                    //validateRoute(val,value);

                    return false;
                }

                // Format the list menu output of the autocomplete
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                //alert(item);
                var itemVal = item.Name;
                itemVal = '<font color="green">' + itemVal + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };


        }
        //end ajax for vehicle Nos

        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#customerName').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCustomerDetails.do",
                        dataType: "json",
                        data: {
                            //alert(document.getElementById("customerName").value);
                            customerName: request.term,
                            customerCode: document.getElementById('customerCode').value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    $("#customerName").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('~');
                    $itemrow.find('#customerId').val(tmp[0]);
                    $itemrow.find('#customerName').val(tmp[1]);
                    $itemrow.find('#customerCode').val(tmp[2]);
                    $('#customerAddress').text(tmp[3]);
                    $('#pincode').val(tmp[4]);
                    $('#customerMobileNo').val(tmp[6]);
                    $('#customerPhoneNo').val(tmp[5]);
                    $('#mailId').val(tmp[7]);
                    $('#billingTypeId').val(tmp[8]);
                    $('#billingTypeName').text(tmp[9]);
                    $('#billTypeName').text(tmp[9]);
                    $('#contractNo').text(tmp[10]);
                    $('#contractExpDate').text(tmp[11]);
                    $('#contractId').val(tmp[12]);
                    //$('#ahref').attr('href', '/throttle/viewCustomerContract.do?custId=' + tmp[0]);
                    $("#contractDetails").show();
                    document.getElementById('customerCode').readOnly = true;
                    $('#origin').empty();
                    $('#origin').append(
                            $('<option style="width:150px"></option>').val(0).html('--Select--')
                            )
                    if (tmp[8] == '3') {//actual kms contract
                        setOriginList(tmp[12]);
                        setVehicleTypeForActualKM(tmp[12]);
                        $('#vehicleTypeDiv').show();
                        $('#vehicleTypeContractDiv').hide();
                        $('#contractRouteDiv2').show();
                        $('#contractRouteDiv1').hide();
                    } else {
                        getContractRoutes(tmp[12]);
                        getContractRoutesOrigin(tmp[12]);


                        $('#vehicleTypeDiv').hide();
                        $('#vehicleTypeContractDiv').show();
                        $('#contractRouteDiv1').show();
                        $('#contractRouteDiv2').hide();
                    }
                    fetchCustomerProducts(tmp[1]);
                    return false;

                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('~');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };


            $('#customerCode').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCustomerDetails.do",
                        dataType: "json",
                        data: {
                            customerCode: request.term,
                            customerName: document.getElementById('customerName').value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    $("#customerCode").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#customerId').val(tmp[0]);
                    $itemrow.find('#customerName').val(tmp[1]);
                    $itemrow.find('#customerCode').val(tmp[2]);
                    $itemrow.find('#customerAddress').val(tmp[3]);
                    $itemrow.find('#customerMobileNo').val(tmp[4]);
                    $itemrow.find('#customerPhoneNo').val(tmp[5]);
                    $itemrow.find('#mailId').val(tmp[6]);
                    $itemrow.find('#billingTypeId').val(tmp[7]);
                    $('#billingTypeName').val(tmp[8]);
                    $('#billTypeName').val(tmp[8]);
                    $("#contractDetails").show();
                    document.getElementById('customerName').readOnly = true;
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[2] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        });

        function setOriginList(val) {
            $.ajax({
                url: '/throttle/getContractRouteList.do',
                data: {contractId: val, billingTypeId: $('#billingTypeId').val(), customerTypeId: 0},
                dataType: 'json',
                success: function(data) {
                    $.each(data, function(i, data) {
                        $('#originTemp').append(
                                $('<option style="width:150px"></option>').val(data.Id).html(data.Name)
                                )
                    });
                }
            });

        }
        function fetchCustomerProducts(val) {
            var custId = document.cNote.customerId.value;
            //alert(custId + ":::" +val );
            if (custId != '' && custId != '0') {
                $.ajax({
                    url: '/throttle/fetchCustomerProducts.do',
                    data: {customerId: custId},
                    dataType: 'json',
                    success: function(data) {
                        if (data != '') {
                            $('#productCategoryIdTemp').empty();
                            if (document.getElementById('editProductCategoryId').value != '') {
                                $('#productCategoryIdTemp').append(
                                        $('<option ></option>').val(document.getElementById('editProductCategoryId').value).html(document.getElementById('productCategoryName').value)
                                        )

                                $.each(data, function(i, data) {
                                    if (document.getElementById('productCategoryName').value != data.Name) {
                                        $('#productCategoryIdTemp').append(
                                                $('<option></option>').val(data.Id).html(data.Name)
                                                )
                                    }

                                });

                            } else {
                                $('#productCategoryIdTemp').append(
                                        $('<option ></option>').val(0).html('--select--')
                                        )

                                $.each(data, function(i, data) {

                                    $('#productCategoryIdTemp').append(
                                            $('<option></option>').val(data.Id).html(data.Name)
                                            )

                                });

                            }

                        }
                    }
                });
            }

        }
        function setVehicleTypeForActualKM(val) {
            $('#vehTypeIdTemp').empty();
            $('#vehTypeIdTemp').append(
                    $('<option ></option>').val(0).html('--select--')
                    )
            $.ajax({
                url: '/throttle/getVehicleTypeForActualKM.do',
                data: {contractId: val},
                dataType: 'json',
                success: function(data) {
                    $.each(data, function(i, data) {
                        $('#vehTypeIdTemp').append(
                                $('<option ></option>').val(data.Id).html(data.Name)
                                )
                    });
                }
            });

        }

        function getContractRoutes(val) {
            $.ajax({
                url: '/throttle/getContractRoutes.do',
                data: {contractId: val, billingTypeId: $('#billingTypeId').val(), customerTypeId: 0},
                dataType: 'json',
                success: function(data) {
                    $.each(data, function(i, data) {
                        $('#contractRoute').append(
                                $('<option ></option>').val(data.Id).html(data.Name)
                                )
                    });
                }
            });
        }
        function getContractRoutesOrigin(val) {
            $.ajax({
                url: '/throttle/getContractRoutesOrigin.do',
                data: {contractId: val, billingTypeId: $('#billingTypeId').val(), customerTypeId: 0},
                dataType: 'json',
                success: function(data) {
                    $('#contractRouteOrigin').empty();
                    $('#contractRouteOrigin').append(
                            $('<option></option>').val(0).html('-select-')
                            )
                    $.each(data, function(i, data) {
                        $('#contractRouteOrigin').append(
                                $('<option ></option>').val(data.Id).html(data.Name)
                                )
                    });
                }
            });
        }
        function setContractOriginDestination() {

            if (document.cNote.contractRouteOrigin.value == '0') {
                $('#contractRouteDestination').empty();
                $('#contractRouteDestination').append(
                        $('<option></option>').val(0).html('-select-')
                        )
                setContractRouteDetails();
            } else {

                $.ajax({
                    url: '/throttle/getContractRoutesOriginDestination.do',
                    data: {contractId: $('#contractId').val(), contractRouteOrigin: $('#contractRouteOrigin').val(), customerTypeId: 0},
                    dataType: 'json',
                    success: function(data) {
                        $('#contractRouteDestination').empty();
                        $('#contractRouteDestination').append(
                                $('<option></option>').val(0).html('-select-')
                                )
                        $.each(data, function(i, data) {
                            $('#contractRouteDestination').append(
                                    $('<option ></option>').val(data.Id).html(data.Name)
                                    )
                        });
                        setContractRouteDetails();
                    }
                });
            }
        }


        var httpReq;
        var temp = "";
        function setDestination()
        {
            document.cNote.origin.value = document.cNote.originTemp.value;
            var billingTypeId = document.getElementById('billingTypeId').value;
            var url = "/throttle/getDestinationList.do?originId=" + document.cNote.originTemp.value + "&billingTypeId=" + document.getElementById('billingTypeId').value + "&customerTypeId=" + document.getElementById('customerTypeId').value + "&contractId=" + document.getElementById('contractId').value;
            if (window.ActiveXObject)
            {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest)
            {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() {
                processAjax();
            };
            httpReq.send(null);
        }

        function processAjax()
        {
            if (httpReq.readyState == 4)
            {
                if (httpReq.status == 200)
                {
                    temp = httpReq.responseText.valueOf();
                    setDestinationOptions(temp, document.cNote.destinationTemp);
                }
                else
                {
                    alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                }
            }
        }

        function setDestinationOptions(text, variab) {
            variab.options.length = 0;
            option0 = new Option("--select--", '0');
            variab.options[0] = option0;
            if (text != "") {
                var splt = text.split('~');
                var temp1;
                var id;
                var name;
                for (var i = 0; i < splt.length; i++) {
                    temp1 = splt[i].split('#');
//                    alert(temp1[0]);
//                    alert(temp1[1]);
                    /*
                     if (document.getElementById('customerTypeId').value == 2) {
                     id = temp1[0];
                     name = temp1[1];
                     //setVehicleType();
                     } else if (document.getElementById('billingTypeId').value == 3 && document.getElementById('customerTypeId').value == 1) {
                     id = temp1[0] + "-" + temp1[1];
                     name = temp1[1];
                     //setVehicleType();
                     } else if (document.getElementById('billingTypeId').value != 3 && document.getElementById('customerTypeId').value == 1) {
                     id = temp1[0];
                     name = temp1[1];
                     //setVehicleType();
                     }
                     */
//                    alert(name);
//                    alert(id);
                    id = temp1[0];
                    name = temp1[1];
                    option1 = new Option(name, id)
                    variab.options[i + 1] = option1;
                }
                setRouteDetails();
            }
        }

        function setVehicleType() {
            //alert();
            //alert();
            var url = "/throttle/getContractVehicleTypeList.do?contractId=" + document.getElementById('contractId').value + "&billingTypeId=" + document.getElementById('billingTypeId').value + "&customerTypeId=" + document.getElementById('customerTypeId').value + "&origin=" + document.getElementById('origin').value + "&destination=" + document.getElementById('destination').value;
            if (window.ActiveXObject) {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() {
                processVehicleTypeAjax();
            };
            httpReq.send(null);
        }
        function getContractVehicleType() {
            var contractType = document.cNote.billingTypeId.value;
            //alert(contractType);
            if (contractType == '1' || contractType == '2') {
                //alert("am here....");
                var pointIds = document.getElementsByName("pointId");
                //alert("am here...."+pointIds.length);
                var pointId = "";
                var j = 0;
                var statusCheck = true;
                for (var i = 0; i < pointIds.length; i++) {
                    //alert(pointIds[i].value);
                    j = i + 1;
                    if (i != 0 && i != pointIds.length) {
                        j = j + 1;
                    }
                    if (pointIds[i].value == '' || pointIds[i].value == 0) {
                        alert('please enter valid interim point');
                        //alert(document.getElementById("pointName"+j).value);
                        document.getElementById("pointName" + j).focus();
                        statusCheck = false;
                    }

                    if (i == 0) {
                        pointId = pointIds[i].value;
                    } else {
                        pointId = pointId + "~" + pointIds[i].value;
                    }
                }
                //alert(pointId);
                if (statusCheck) {
                    $.ajax({
                        url: '/throttle/getContractVehicleTypeForPointsList.do',
                        data: {contractId: $('#contractId').val(), pointId: pointId},
                        dataType: 'json',
                        success: function(data) {
                            if (data == '' || data == null) {
                                alert('chosen contract route does not exists. please check.');
                            } else {
                                $("#routePlanAddRow").hide();
                                $("#freezeRoute").hide();
                                $("#resetRoute").hide();
                                $("#unFreezetRoute").show();
                                $('#vehTypeIdContractTemp').empty();
                                $('#vehTypeIdContractTemp').append(
                                        $('<option></option>').val(0).html('-select-')
                                        )
                                $.each(data, function(i, data) {
                                    $('#vehTypeIdContractTemp').append(
                                            $('<option ></option>').val(data.Id).html(data.Name)
                                            )
                                });
                            }
                        }
                    });
                }
            } else {
                $("#routePlanAddRow").hide();
                $("#freezeRoute").hide();
                $("#resetRoute").hide();
                $("#unFreezetRoute").show();
            }

        }

        function unFreezetRouteFn() {
            $("#unFreezetRoute").hide();
            $("#routePlanAddRow").show();
            $("#freezeRoute").show();
            $("#resetRoute").show();

            var contractType = document.cNote.billingTypeId.value;
            //alert(contractType);
            if (contractType == '1' || contractType == '2') {
                $('#vehTypeIdContractTemp').empty();
                $('#vehTypeIdContractTemp').append(
                        $('<option></option>').val(0).html('-select-')
                        )
            }
        }
        function processVehicleTypeAjax() {
            if (httpReq.readyState == 4) {
                if (httpReq.status == 200) {
                    temp = httpReq.responseText.valueOf();
                    //alert(temp);
                    $('#vehTypeId').append(
                            $('<option style="width:150px"></option>').val(0).html('--Select--')
                            )
                    setVehicleTypeOptions(temp, document.cNote.vehTypeId);
                } else {
                    alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                }
            }
        }

        function setVehicleTypeOptions(text, variab) {
            variab.options.length = 0;
            option0 = new Option("--select--", '0');
            variab.options[0] = option0;
            if (text != "") {
                var splt = text.split('~');
                var temp1;
                variab.options[0] = option0;
                for (var i = 0; i < splt.length; i++) {
                    temp1 = splt[i].split('#');
                    //alert(temp1[1]+"-"+temp1[0]);
                    option0 = new Option(temp1[1], temp1[0])
                    variab.options[i + 1] = option0;
                }
            }
        }


        function multiPickupShow() {
            var billingTypeId = document.getElementById('billingTypeId').value;
            if (document.getElementById('multiPickup').checked == true) {
                document.getElementById('multiPickupCharge').readOnly = false;
                document.getElementById('multiPickup').value = "Y";
                //alert(document.getElementById('multiPickup').value);
//                if (billingTypeId == "3") {
//                    $("#routeDetail").show();
//                    $("#showRouteCourse").show();
//                }
            } else if (document.getElementById('multiPickup').checked == false) {
                document.getElementById('multiPickupCharge').readOnly = true;
                document.getElementById('multiPickup').value = "N";
                //alert(document.getElementById('multiPickup').value);
//                if (billingTypeId == "3") {
//                    $("#routeDetail").hide();
//                    $("#showRouteCourse").hide();
//                }
            }
        }
        function multiDeliveryShow() {
            var billingTypeId = document.getElementById('billingTypeId').value;
            if (document.getElementById('multiDelivery').checked == true) {
                document.getElementById('multiDeliveryCharge').readOnly = false;
                document.getElementById('multiDelivery').value = "Y";
//                if (billingTypeId == "3") {
//                    $("#routeDetail").show();
//                    $("#showRouteCourse").show();
//                }
            } else if (document.getElementById('multiDelivery').checked == false) {
                document.getElementById('multiDeliveryCharge').readOnly = true;
                document.getElementById('multiDelivery').value = "N";
//                if (billingTypeId == "3") {
//                    $("#routeDetail").hide();
//                    $("#showRouteCourse").hide();
//                }
            }
        }

        function calcTotalPacks(val) {
            var totVal = 0;
            var packagesNos = document.getElementsByName('packagesNos');
            for (var i = 0; i < packagesNos.length; i++) {
                if (packagesNos[i].value != '') {
                    totVal += parseInt(packagesNos[i].value);
                }
            }
            $('#totalPackages').text(totVal);
            $('#totalPackage').val(totVal);
        }


        function calcTotalWeights(val) {
            var totVal = 0;
            var reeferRequired = document.getElementById('reeferRequired').value;
            var weights = document.getElementsByName('weights');
            var billingTypeId = document.getElementById('billingTypeId').value;
            var rateWithReefer = document.getElementById('rateWithReefer').value;
            var rateWithoutReefer = document.getElementById('rateWithoutReefer').value;
            for (var i = 0; i < weights.length; i++) {
                if (weights[i].value != '') {
                    totVal += parseInt(weights[i].value);
                }
            }
            $('#totalWeight').text(totVal);
            $('#totalWeightage').val(totVal);
            var freigthTotal = 0;
            if (billingTypeId == 2 && reeferRequired == 'Yes') {
                freigthTotal = parseInt(rateWithReefer) * parseInt(totVal);
                $('#freightAmount').text(freigthTotal)
                $('#totalCharges').val(freigthTotal)
            } else if (billingTypeId == 2 && reeferRequired == 'No') {
                freigthTotal = parseInt(rateWithoutReefer) * parseInt(totVal);
                $('#freightAmount').text(freigthTotal)
                $('#totalCharges').val(freigthTotal)
            }
        }
        function resetRouteInfo() {
            var customerType = document.cNote.customerTypeId.value;
            var billingType = document.cNote.billingTypeId.value;
            if (customerType == '1' && (billingType == '1' || billingType == '2')) {
                setContractRouteDetails();
            } else {
                setRouteDetails();
            }
        }
        function validateTransitTime(val) {

            var transitHours = document.getElementById("pointTransitHrs" + val).value;

            if (transitHours == '') {
                var pointIdValue = document.getElementById("pointId" + val).value;
                var pointOrderValue = document.getElementById("pointOrder" + val).value;
                var pointOrder = document.getElementsByName("pointOrder");
                var pointNames = document.getElementsByName("pointName");
                var pointIdPrev = 0;
                var pointIdNext = 0;
                var prevPointOrderVal = parseInt(pointOrderValue) - 1;
                var nextPointOrderVal = parseInt(pointOrderValue) + 1;
                for (var m = 1; m <= pointOrder.length; m++) {
                    if (document.getElementById("pointOrder" + m).value == prevPointOrderVal) {
                        pointIdPrev = document.getElementById("pointId" + m).value;
                    }
                    if (document.getElementById("pointOrder" + m).value == nextPointOrderVal) {
                        pointIdNext = document.getElementById("pointId" + m).value;
                    }
                }
                //fetch transit hours via ajax for prepointid and current point id and set transit hours value to hidden field

            }

        }

        function validateTripSchedule() {

            document.cNote.vehicleRequiredDate.value = document.getElementById("pointPlanDate1").value;
            document.cNote.vehicleRequiredHour.value = document.getElementById("pointPlanHour1").value;
            document.cNote.vehicleRequiredMinute.value = document.getElementById("pointPlanMinute1").value;
            var scheduleDate = document.cNote.vehicleRequiredDate.value;
            var scheduleHour = document.cNote.vehicleRequiredHour.value;
            var scheduleMinute = document.cNote.vehicleRequiredMinute.value;

            var temp = scheduleDate.split("-");
            var tripScheduleTime = convertStringToDate(temp[2], temp[1], temp[0], scheduleHour, scheduleMinute, '00');

            var time1 = new Date();
            var time1ms = time1.getTime(time1); //i get the time in ms
            var time2 = tripScheduleTime;
            var time2ms = time2.getTime(time2);
            var difference = time2ms - time1ms;
            var hours = Math.floor(difference / 36e5);
            //alert(hours);
            if (hours < 10) {
                alert('Orders cannot be accepted as the cut off time has elapsed.');
//                return false;
                return true;
            } else {
                return true;
            }

        }
        function submitPage(value) {
            if (document.getElementById("editCountValue").value == 0) {

                if (!validateTripSchedule()) {
                    return;
                }

//                if (textValidation(document.cNote.orderReferenceNo, 'Customer Order Reference')) {
//                    return;
//                }
                if (document.cNote.productCategoryIdTemp.value == '0') {
                    alert('please select product category');
                    document.cNote.productCategoryIdTemp.focus();
                    return;
                }
                var customerType = document.cNote.customerTypeId.value;
                if (customerType == '2') {
                    if (textValidation(document.cNote.walkinCustomerName, 'Customer Name')) {
                        return;
                    }
                    if (textValidation(document.cNote.walkinCustomerCode, 'Customer Code')) {
                        return;
                    }
                    if (textValidation(document.cNote.walkinCustomerAddress, 'Customer Address')) {
                        return;
                    }
                    if (textValidation(document.cNote.walkinPincode, 'Customer pincode')) {
                        return;
                    }
                    if (textValidation(document.cNote.walkinCustomerMobileNo, 'Customer Mobile')) {
                        return;
                    }
                    if (document.cNote.walkinMailId.value == '') {
                        alert('please enter customer email id');
                        document.cNote.walkinMailId.focus();
                        return;
                    }
                }


//            if (isEmail(document.cNote.walkinMailId.value)) {
//                return;
//            }

                var billingType = document.cNote.billingTypeId.value;
                if (customerType == '1' && (billingType == '1' || billingType == '2')) {
                    if (document.cNote.contractRouteOrigin.value == '0') {
                        alert('please select consignment origin');
                        document.cNote.contractRouteOrigin.focus();
                        return;
                    }
                    if (document.cNote.contractRouteDestination.value == '0') {
                        alert('please select consignment destination');
                        document.cNote.contractRouteDestination.focus();
                        return;
                    }

                    if (document.cNote.vehTypeIdContractTemp.value == '0') {
                        alert('please select vehicle type');
                        document.cNote.vehTypeIdContractTemp.focus();
                        return;
                    }
                } else { //walk in or actual kms
                    if (document.cNote.originTemp.value == '0') {
                        alert('please select consignment origin');
                        document.cNote.originTemp.focus();
                        return;
                    }
                    if (document.cNote.destinationTemp.value == '0') {
                        alert('please select consignment destination');
                        document.cNote.destinationTemp.focus();
                        return;
                    }
                }
                document.cNote.vehicleRequiredDate.value = document.getElementById("pointPlanDate1").value;
                document.cNote.vehicleRequiredHour.value = document.getElementById("pointPlanHour1").value;
                document.cNote.vehicleRequiredMinute.value = document.getElementById("pointPlanMinute1").value;
                //var tripScheduleTime = convertStringToDate(2013,12,12,22,30,'00');

                if (isEmpty(document.cNote.vehicleRequiredDate.value)) {
                    alert('please enter vehicle required date');
                    document.getElementById("pointPlanDate1").focus();
                    return;
                }





                if (isEmpty(document.cNote.consignorName.value)) {
                    alert('please enter consignorName');
                    document.cNote.consignorName.focus();
                    return;
                }
                if (isEmpty(document.cNote.consignorPhoneNo.value)) {
                    alert('please enter consignorPhoneNo');
                    document.cNote.consignorPhoneNo.focus();
                    return;
                }
                if (isEmpty(document.cNote.consignorAddress.value)) {
                    alert('please enter consignorAddress');
                    document.cNote.consignorAddress.focus();
                    return;
                }
                if (isEmpty(document.cNote.consigneeName.value)) {
                    alert('please enter consigneeName');
                    document.cNote.consigneeName.focus();
                    return;
                }
                if (isEmpty(document.cNote.consigneePhoneNo.value)) {
                    alert('please enter consigneePhoneNo');
                    document.cNote.consigneePhoneNo.focus();
                    return;
                }
                if (isEmpty(document.cNote.consigneeAddress.value)) {
                    alert('please enter consigneeAddress');
                    document.cNote.consigneeAddress.focus();
                    return;
                }

                estimateFreight();
                document.cNote.action = "/throttle/editSaveConsignmentNote.do";
                document.cNote.submit();
            } else {

                if (textValidation(document.cNote.orderReferenceNo, 'Customer Order Reference')) {
                    return;
                }
                if (document.cNote.productCategoryIdTemp.value == '0') {
                    alert('please select product category');
                    document.cNote.productCategoryIdTemp.focus();
                    return;
                }

                if (isEmpty(document.cNote.consignorName.value)) {
                    alert('please enter consignorName');
                    document.cNote.consignorName.focus();
                    return;
                }
                if (isEmpty(document.cNote.consignorPhoneNo.value)) {
                    alert('please enter consignorPhoneNo');
                    document.cNote.consignorPhoneNo.focus();
                    return;
                }
                if (isEmpty(document.cNote.consignorAddress.value)) {
                    alert('please enter consignorAddress');
                    document.cNote.consignorAddress.focus();
                    return;
                }
                if (isEmpty(document.cNote.consigneeName.value)) {
                    alert('please enter consigneeName');
                    document.cNote.consigneeName.focus();
                    return;
                }
                if (isEmpty(document.cNote.consigneePhoneNo.value)) {
                    alert('please enter consigneePhoneNo');
                    document.cNote.consigneePhoneNo.focus();
                    return;
                }
                if (isEmpty(document.cNote.consigneeAddress.value)) {
                    alert('please enter consigneeAddress');
                    document.cNote.consigneeAddress.focus();
                    return;
                }
                var editVehicleTypeId = $("#editVehicleTypeId").val();
                var vehicleTypeId = $("#vehicleTypeId").val();
                //alert(editVehicleTypeId+"------"+vehicleTypeId);
                if (editVehicleTypeId != vehicleTypeId) {
                    estimateFreight();
                    document.cNote.action = "/throttle/editSaveConsignmentNote.do";
                    document.cNote.submit();
                } else {
                    document.cNote.action = "/throttle/editSaveConsignmentNote.do";
                    document.cNote.submit();
                }
            }

        }

        function setProductCategoryValues() {
            var temp = document.cNote.productCategoryIdTemp.value;
            //alert(temp);
            if (temp != 0) {
                var tempSplit = temp.split('~');
                document.getElementById("temperatureInfo").innerHTML = 'Reefer Temp (deg Celcius): Min ' + tempSplit[1] + ' Max ' + tempSplit[2];
                document.cNote.productCategoryId.value = tempSplit[0];
                var reeferRequired = tempSplit[3];
                if (reeferRequired == 'Y') {
                    reeferRequired = 'Yes';
                } else {
                    reeferRequired = 'No';
                }
                document.cNote.reeferRequired.value = reeferRequired;
            } else {
                document.getElementById("temperatureInfo").innerHTML = '';
                document.cNote.productCategoryId.value = 0;
                document.cNote.reeferRequired.value = '';
            }

        }

        $(document).ready(function() {
            var billingTypeId = $("#billingTypeId").val();
            
             if (billingTypeId == 3) {
                $("#editVehicleTypeDiv").show();
                $("#editVehicleTypeContractDiv").hide();
            } else {

                $("#editVehicleTypeDiv").hide();
                $("#editVehicleTypeContractDiv").show();
            }
            
            if (document.getElementById('editProductCategoryId').value != '') {
                //alert(document.getElementById('productCategoryIdTemp').value);
                var temp = document.cNote.editProductCategoryId.value;
                //alert(temp);
                if (temp != 0) {
                    var tempSplit = temp.split('~');
                    document.getElementById("temperatureInfo").innerHTML = 'Reefer Temp (deg Celcius): Min ' + tempSplit[1] + ' Max ' + tempSplit[2];
                    document.cNote.productCategoryId.value = tempSplit[0];
                    var reeferRequired = tempSplit[3];
                    if (reeferRequired == 'Y') {
                        reeferRequired = 'Yes';
                    } else {
                        reeferRequired = 'No';
                    }
                    document.cNote.reeferRequired.value = reeferRequired;
                } else {
                    document.getElementById("temperatureInfo").innerHTML = '';
                    document.cNote.productCategoryId.value = 0;
                    document.cNote.reeferRequired.value = '';
                }
                document.getElementById('productCategoryIdTemp').value = document.getElementById('editProductCategoryId').value;
                $('#productCategoryIdTemp').append(
                        $('<option ></option>').val(document.getElementById('editProductCategoryId').value).html('-ppppppp')
                        )
                //alert(document.getElementById('productCategoryIdTemp').value);
            }
        });

        function setCustomerInfo() {
            var billingTypeId = $("#billingTypeId").val();
            var contractId = $("#contractId").val();
            var customerId = $("#customerId").val();
            $("#contractDetails").show();
            document.getElementById('customerCode').readOnly = true;
            $('#origin').empty();
            $('#origin').append(
                    $('<option style="width:150px"></option>').val(0).html('--Select--')
                    )
            if (billingTypeId == '3') {//actual kms contract
                setOriginList(contractId);
                setVehicleTypeForActualKM(contractId);
                $('#vehicleTypeDiv').show();
                $('#vehicleTypeContractDiv').hide();
                $('#contractRouteDiv2').show();
                $('#contractRouteDiv1').hide();
            } else {
                getContractRoutes(contractId);
                getContractRoutesOrigin(contractId);
                $("#contractRouteOrigin").val(<c:out value="${consigmentOrigin}"/>);
                //alert("Return");
                setContractOriginDestination();
                setContractRouteDetails();
                $('#vehicleTypeDiv').hide();
                $('#vehicleTypeContractDiv').hide();
                $('#contractRouteDiv1').show();
                $('#contractRouteDiv2').hide();
            }
            fetchCustomerProducts(customerId);
        }
    </script>
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.C-Note Edit" text="C-Note Edit"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Sales/Ops" text="Sales/Ops"/></a></li>
		                    <li class=""><spring:message code="hrms.label.C-Note Edit" text="C-Note Edit"/></li>
		
		                </ol>
		            </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">

    <body onLoad="enableContract(1);
            fetchCustomerProducts(document.getElementById('customerId').value);

            addRow1();">
        <% String menuPath = "Operations >>  Create New Consignment Note";
            request.setAttribute("menuPath", menuPath);
        %>
        <form name="cNote" method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            
            <%@include file="/content/common/message.jsp" %>
            <%
            Date today = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String startDate = sdf.format(today);
            %>
            <input type="hidden" name="startDate" value='<%=startDate%>' />

           <table class="table table-info mb30 table-hover" >     
           <thead>
               <tr>
                    <th  colspan="6"><input type="hidden" name="consignmentOrderId" id="consignmentOrderId" value="<c:out value="${consignmentOrderId}"/>"/></th>
                </tr>
            </thead>
                <tr>
                    <td >Entry Option</td>
                    <c:if test="${entryType == 1}">
                        <td ><input type="hidden" name="entryType" value="<c:out value="${entryType}"/>">Manual</td>
                        </c:if>
                        <c:if test="${entryType == 2}">
                        <td ><input type="hidden" name="entryType" value="<c:out value="${entryType}"/>">Import</td>
                        </c:if>
                    <td >Consignment Date </td>
                    <td  ><input type="hidden" name="consignmentDate" id="consignmentDate" value="<c:out value="${consignmentOrderDate}"/>" class="form-control" style="width:250px;height:40px"readonly style="width: 120px"/><c:out value="${consignmentOrderDate}"/></td>
                </tr>
                <tr>
                    <td >Consignment Note </td>
                    <td ><input type="text" name="consignmentNoteNo" id="consignmentNoteNo" value="<c:out value="${consignmentNoteNo}"/>" class="form-control" style="width:250px;height:40px"readonly style="width: 120px"/></td>
                    <td ><font color="red">*</font>Customer Type</td>
                    <td >
                        <c:if test="${customerTypeList != null}">
                            <select name="customerTypeId" id="customerTypeId" onchange="enableContract(this.value);" class="form-control" style="width:250px;height:40px" style="width:120px;" >
                                <c:forEach items="${customerTypeList}" var="cusType">
                                    <option value="<c:out value="${cusType.customerTypeId}"/>"><c:out value="${cusType.customerTypeName}"/></option>
                                </c:forEach>
                            </select>

                        </c:if>
                    </td>

                </tr>
                <tr>
                    <td ><font color="red">*</font>Customer Reference No</td>
                    <td ><input type="text" name="orderReferenceNo" id="orderReferenceNo" value="<c:out value="${consignmentRefNo}"/>" class="form-control" style="width:250px;height:40px" style="width: 120px"/></td>
                    <td >Customer Reference  Remarks</td>
                    <td ><textarea name="orderReferenceRemarks" class="form-control" style="width:250px;height:40px" id="orderReferenceRemarks"><c:out value="${consignmentOrderRefRemarks}"/></textarea></td>

            </table>
            
            
            <script type="text/javascript">

                function enableContract(val) {
                    //alert(val);
                    if (val == 1) {
                        $("#contractCustomerTable").show();
                        $("#walkinCustomerTable").hide();
                        $("#contractFreightTable").show();
                        $("#walkinFreightTable").hide();
                        document.getElementById('customerAddress').readOnly = true;
                        document.getElementById('pincode').readOnly = true;
                        document.getElementById('customerMobileNo').readOnly = true;
                        document.getElementById('mailId').readOnly = true;
                        document.getElementById('customerPhoneNo').readOnly = true;
                        document.getElementById('walkinCustomerName').value = "";
                        document.getElementById('walkinCustomerCode').value = "";
                        document.getElementById('walkinCustomerAddress').value = "";
                        document.getElementById('walkinPincode').value = "";
                        document.getElementById('walkinCustomerMobileNo').value = "";
                        document.getElementById('walkinMailId').value = "";
                        document.getElementById('walkinCustomerPhoneNo').value = "";
                    } else {
                        //alert(val);
                        $("#contractCustomerTable").hide();
                        $("#walkinCustomerTable").show();
                        $("#contractFreightTable").hide();
                        $("#walkinFreightTable").show();
                        document.getElementById('customerId').value = "";
                        document.getElementById('customerName').value = "";
                        document.getElementById('customerCode').value = "";
                        document.getElementById('customerAddress').value = "";
                        document.getElementById('pincode').value = "";
                        document.getElementById('customerMobileNo').value = "";
                        document.getElementById('mailId').value = "";
                        document.getElementById('customerPhoneNo').value = "";
                        setOriginList(0);
                        $('#vehicleTypeDiv').hide();
                        $('#vehicleTypeContractDiv').hide();
                        $('#contractRouteDiv2').show();
                        $('#contractRouteDiv1').hide();
                    }
                }
            </script>
            <div id="tabs">
                <ul>
                    <li><a href="#customerDetail"><span>Order Details</span></a></li>
                    <!--                    <li><a href="#routeDetail" id="showRouteCourse"><span>Route Course</span></a></li>-->
                    <!--                    <li><a href="#paymentDetails"><span>Invoice Info</span></a></li>-->

                </ul>

                <div id="customerDetail">
                        <table class="table table-info mb30 table-hover" id="contractCustomerTable" >
                            <thead>
                        <tr>
                            <th colspan="4" >Contract Customer Details</th>
                        </tr>
                            </thead>
                        <tr>
                            <td ><font color="red">*</font>Customer Name</td>
                            <td ><input type="hidden" name="customerId" id="customerId" class="form-control" style="width:250px;height:40px" value="<c:out value="${customerId}"/>"/>
                                <input type="text" name="customerName" onKeyPress="return onKeyPressBlockNumbers(event);"  id="customerName" class="form-control" style="width:250px;height:40px"  value="<c:out value="${customerName}"/>" readonly=""/></td>
                            <td ><font color="red">*</font>Customer Code</td>
                            <td ><input type="text" name="customerCode" id="customerCode" class="form-control" style="width:250px;height:40px"  readonly="" value="<c:out value="${customerCode}"/>"/></td>
                        </tr>
                        <tr>
                            <td ><font color="red">*</font>Address</td>
                            <td ><textarea rows="1" cols="16" id="customerAddress" name="customerAddress" class="form-control" style="width:250px;height:40px" readonly=""><c:out value="${customerAddress}"/></textarea></td>
                            <td ><font color="red">*</font>Pincode</td>
                            <td ><input type="text"  name="pincode" id="pincode"  onKeyPress="return onKeyPressBlockCharacters(event);"  class="form-control" style="width:250px;height:40px" value="<c:out value="${customerPincode}"/>" readonly=""/></td>
                        </tr>
                        <tr>
                            <td ><font color="red">*</font>Mobile No</td>
                            <td ><input type="text"  name="customerMobileNo"  onKeyPress="return onKeyPressBlockCharacters(event);"  id="customerMobileNo" class="form-control" style="width:250px;height:40px" value="<c:out value="${customerMobile}"/>" readonly=""/></td>
                            <td ><font color="red">*</font>E-Mail Id</td>
                            <td ><input type="text"  name="mailId" id="mailId" class="form-control" style="width:250px;height:40px" value="<c:out value="${customerEmail}"/>" readonly=""/></td>
                        </tr>
                        <tr>
                            <td >Phone No</td>
                            <td ><input type="text" name="customerPhoneNo" id="customerPhoneNo"  onKeyPress="return onKeyPressBlockCharacters(event);"   class="form-control" style="width:250px;height:40px" maxlength="10" value="<c:out value="${customerPhone}"/>" readonly="" /></td>

                            <td >Billing Type</td>
                            <td  id="billingType"><input type="hidden" name="billingTypeId" id="billingTypeId" class="form-control" style="width:250px;height:40px" value="<c:out value="${customerBillingType}"/>"/><label id="billingTypeName"><c:out value="${billingTypeName}"/></label></td>

                        </tr>
                    </table>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="walkinCustomerTable">
                        <tr>
                            <td  colspan="4" >Walkin Customer Details</td>
                        </tr>
                        <tr>
                            <td ><font color="red">*</font> Customer Name</td>
                            <td ><input type="text" name="walkinCustomerName" id="walkinCustomerName" class="form-control" style="width:250px;height:40px"   /></td>
                            <td ><font color="red">*</font> Customer Code</td>
                            <td ><input type="text" name="walkinCustomerCode" id="walkinCustomerCode" class="form-control" style="width:250px;height:40px"  /></td>
                        </tr>
                        <tr>
                            <td ><font color="red">*</font>Address</td>
                            <td ><textarea rows="1" cols="16" id="walkinCustomerAddress" name="walkinCustomerAddress"></textarea></td>
                            <td ><font color="red">*</font>Pincode</td>
                            <td ><input type="text"    onKeyPress='return onKeyPressBlockCharacters(event);'   name="walkinPincode" id="walkinPincode" class="form-control" style="width:250px;height:40px" /></td>
                        </tr>
                        <tr>
                            <td ><font color="red">*</font>Mobile No</td>
                            <td ><input type="text"    onKeyPress='return onKeyPressBlockCharacters(event);'   name="walkinCustomerMobileNo" id="walkinCustomerMobileNo" class="form-control" style="width:250px;height:40px"  /></td>
                            <td ><font color="red">*</font>E-Mail Id</td>
                            <td ><input type="text"  name="walkinMailId" id="walkinMailId" class="form-control" style="width:250px;height:40px"  /></td>
                        </tr>
                        <tr>
                            <td >Phone No</td>
                            <td ><input type="text"     onKeyPress='return onKeyPressBlockCharacters(event);' name="walkinCustomerPhoneNo" id="walkinCustomerPhoneNo"  class="form-control" style="width:250px;height:40px" maxlength="10"   /></td>

                            <td  colspan="2">&nbsp</td>

                        </tr>
                    </table>
                    <div id="contractDetails" style="display: none">
                        <table>
                            <tr>
                                <td >Contract No :</td>
                                <td ><b><input type="hidden" name="contractId" id="contractId" class="form-control" style="width:250px;height:40px" value="<c:out value="${customerContractId}"/>" /><label id="contractNo"><c:out value="${contractNo}"/></label></b></td>
                                <td >Contract Expiry Date :</td>
                                <td ><font color="green"><b><label id="contractExpDate"><c:out value="${contractTo}"/></label></b></font></td>
                                <td >&nbsp;&nbsp;&nbsp;</td>
                                <!--                                <td ><a id="ahref" href="/throttle/BrattleFoods/customerContractMaster.jsp"><b>View Contract</b></a></td>-->
                                <td >
                                    <!--                                    <input type="button" class="btn btn-success"  value="View Contract" onclick="viewCustomerContract()">-->
                                    <a href="#" onclick="viewCustomerContract();">view</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    








                   <table class="table table-info mb30 table-hover" id="bg" >	
                       <thead>
                        <tr>
                            <th  colspan="6" >Product Info</th>
                        </tr>
                       </thead>
                        <tr>

                            <td ><font color="red">*</font>Product Category &nbsp;&nbsp;
                                <c:if test="${productCategoryList != null}">
                                    <input type="hidden" name="productCategoryId" id="productCategoryId" class="form-control" style="width:250px;height:40px" value="<c:out value="${productCategory}"/>" />
                                    <input type="hidden" name="editProductCategoryId" id="editProductCategoryId" class="form-control" style="width:250px;height:40px" value="<c:out value="${productCategoryId}"/>" />
                                    <input type="hidden" name="productCategoryName" id="productCategoryName" class="form-control" style="width:250px;height:40px" value="<c:out value="${productCategoryName}"/>" />
                                    <select name="productCategoryIdTemp" id="productCategoryIdTemp" onchange="setProductCategoryValues();"  class="form-control" style="width:250px;height:40px" >
                                        <option value="0">--Select--</option>
                                        <c:forEach items="${productCategoryList}" var="proList">
                                            <option value='<c:out value="${proList.productCategoryId}"/>'><c:out value="${proList.customerTypeName}"/></option>
                                        </c:forEach>
                                    </select>
                                </c:if>
                            </td>
                            <td  colspan="4" align="left" >&nbsp;<label id="temperatureInfo"></label></td>
                        </tr>
                    </table>
                    









                    <div id="hideEditRouteDetails" style="display:none;">
                       <table class="table table-info mb30 table-hover" id="bg" >	
                           <thead>
                            <tr>
                                <th  colspan="6" >Consignment / Route Details</th>
                            </tr>

                            <tr id="contractRouteDiv1" style="display:none;">
                                <th ><font color="red">*</font>Origin&nbsp;&nbsp;
                                    <select id="contractRouteOrigin" name="contractRouteOrigin" onchange='setContractOriginDestination();' style="width:100px">
                                        <option value="0">-Select-</option>
                                    </select>
                                </th>
                                <th ><font color="red">*</font>Destination&nbsp;&nbsp;
                                    <select id="contractRouteDestination" name="contractRouteDestination" onchange='setContractRouteDetails();' style="width:100px">
                                        <option value="0">-Select-</option>
                                    </select>
                                </th>
                           </thead>
                                <td  colspan="2" >&nbsp;</td>
                                <!--                                <td ><font color="red">*</font>Contract Route</td>
                                                                <td  colspan="3" >
                                                                    <select id="contractRoute" name="contractRoute" onchange='setOriginDestination();' style="width:350px">
                                                                        <option value="0">-Select-</option>
                                                                    </select>
                                                                </td>-->
                            </tr>
                            <input type="hidden" name="origin" value="<c:out value="${consigmentOrigin}"/>" />
                            <input type="hidden" name="destination" value="<c:out value="${consigmentDestination}"/>" />

                            <tr  id="contractRouteDiv2" style="display:none;">
                                <td ><font color="red">*</font>Origin</td>
                                <td >
                                    <select id="originTemp" name="originTemp" onchange='setDestination();' class="form-control" style="width:250px;height:40px" style="width:150px">
                                        <option value="0">-Select-</option>
                                    </select>
                                </td>
                                <td ><font color="red">*</font>Destination</td>
                                <td ><select id="destinationTemp" name="destinationTemp" class="form-control" style="width:250px;height:40px" onchange="setRouteDetails();" style="width:150px">
                                        <option value="0">-Select-</option>
                                    </select></td>                            
                            </tr>

                            <tr id="routeChart" style="display:none;" >
                                <td colspan="6" align="left" >
                                    <table class="table table-info mb30 table-hover" id="routePlan" >
                                  
                                       <thead>
                                        <tr>
                                            <th  height="30" ><font color="red">*</font>Order Sequence</th>
                                            <th  height="30" ><font color="red">*</font>Point Name</th>
                                            <th  height="30" ><font color="red">*</font>Point Type</th>
                                            <th  height="30" ><font color="red">*</font>Address</th>
                                            <th  height="30" ><font color="red">*</font>Required Date</th>
                                            <th  height="30" ><font color="red"></font>Required Time</th>
                                        </tr>
                                       </thead>

                                    </table>
                                    
                                    <table class="border" align="left" width="900" cellpadding="0" cellspacing="0" >
                                        <tr>
                                            <td>
                                                <input type="button" class="btn btn-success"  id="routePlanAddRow" style="display:none;" value="Interim Point" name="save" onClick="addRouteCourse2();">
                                                &nbsp;&nbsp;<input type="button" class="btn btn-success"  id="freezeRoute" style="display:none;"  value="Freeze Route" name="save" onClick="getContractVehicleType();">
                                                &nbsp;&nbsp;<input type="button" class="btn btn-success"  id="resetRoute" style="display:none;"  value="Reset" name="save" onClick="resetRouteInfo();">
                                                &nbsp;&nbsp;<input type="button" class="btn btn-success"  id="unFreezetRoute" style="display:none;"  value="UnFreeze Route" name="save" onClick="unFreezetRouteFn();">
                                            </td>
                                        </tr>
                                    </table>
                                    <!--                                    <a  class="previoustab" href="#"><input type="button" class="btn btn-success"  value="<back" name="Save" /></a>-->

                                    <!--                                    <a  class="nexttab" href="#"><input type="button" class="btn btn-success"  value="next>" name="Save" /></a>-->
                                    </center>
                                    


                                </td>
                            </tr>


                            <!--                                 <select name="businessType" class="form-control" style="width:250px;height:40px"  id="businessType" style="width:120px;">
                                                                <option value="1" selected> Primary  </option>
                                                                <option value="2"> Secondary  </option>
                                                            </select>-->
                           


                        </table>
                    </div>      





                    <!--This DIV is used show existing consignment route course details-->                       
                    <div id="showEditRouteDetails" style="display:block;">
                        <table class="table table-info mb30 table-hover" id="bg" >
                            <thead>
                            <tr>
                                <th  colspan="6" >Consignment / Route Details</th>
                            </tr>
                            </thead>

                            <tr id="routeChart" style="display:block;" >
                                <td colspan="6" align="left" >
                                   <table class="table table-info mb30 table-hover"  id="routePlan">
                                       <thead>
                                          <tr>
                                            <th><font color="red">*</font>Origin</th>
                                            <th ><c:out value="${origin}"/></th>
                                            <th  colspan="2">&nbsp;</th>
                                            <th ><font color="red">*</font>Destination</th>
                                            <th ><c:out value="${destination}"/></th>
                                        </tr>
                                        <tr>
                                            <th  height="30" ><font color="red">*</font>Order Sequence</th>
                                            <th  height="30" ><font color="red">*</font>Point Name</th>
                                            <th  height="30" ><font color="red">*</font>Point Type</th>
                                            <th  height="30" ><font color="red">*</font>Address</th>
                                            <th  height="30" ><font color="red">*</font>Required Date</th>
                                            <th  height="30" ><font color="red"></font>Required Time</th>
                                        </tr>
                                       </thead>
                                        <c:if test="${consignmentPoint != null}">
                                            <tbody>
                                                <c:forEach items="${consignmentPoint}" var="point">
                                                    <tr>
                                                        <td  height="30" ><label><c:out value="${point.pointSequence}"/></label></td>
                                                        <td  height="30" ><label><c:out value="${point.cityName}"/></label></td>
                                                        <td  height="30" ><label><c:out value="${point.consignmentPointType}"/></label></td>
                                                        <td  height="30" ><label><c:out value="${point.pointAddress}"/></label></td>
                                                        <td  height="30" ><label><c:out value="${point.pointDate}"/></label></td>
                                                        <td  height="30" ><label><c:out value="${point.pointPlanTime}"/></label></td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </c:if>
                                    </table>
                                    
                                    <table class="border" align="left" width="900" cellpadding="0" cellspacing="0" >
                                        <tr>
                                            <td>
                                                <input type="button" class="btn btn-success"  id="alterRoute" style="display:block;" value="Alter Route Details" name="alterRoute" onClick="alterRouteDetails();">
                                            </td>
                                        </tr>
                                        <script>
                                            function alterRouteDetails() {
                                                setCustomerInfo();
                                                $("#totFreightAmount").val(0);
                                                $("#editCountValue").val(0);
                                                $("#showEditRouteDetails").hide();
                                                $("#hideEditRouteDetails").show();
                                                $("#editVehicleTypeDiv").hide();
                                                $("#editVehicleTypeContractDiv").hide();
                                                var billingTypeId = $("#billingTypeId").val();
                                                if (billingTypeId == '3') {//actual kms contract
                                                    $('#vehicleTypeDiv').show();
                                                    $('#vehicleTypeContractDiv').hide();
                                                    $('#contractRouteDiv2').show();
                                                    $('#contractRouteDiv1').hide();
                                                } else {
                                                    $('#vehicleTypeDiv').hide();
                                                    $('#vehicleTypeContractDiv').show();
                                                    $('#contractRouteDiv1').show();
                                                    $('#contractRouteDiv2').hide();
                                                }
                                            }
                                        </script>
                                    </table>
                                    <!--                                    <a  class="previoustab" href="#"><input type="button" class="btn btn-success"  value="<back" name="Save" /></a>-->

                                    <!--                                    <a  class="nexttab" href="#"><input type="button" class="btn btn-success"  value="next>" name="Save" /></a>-->
                                    </center>
                                    


                                </td>
                            </tr>


                            <!--                                 <select name="businessType" class="form-control" style="width:250px;height:40px"  id="businessType" style="width:120px;">
                                                                <option value="1" selected> Primary  </option>
                                                                <option value="2"> Secondary  </option>
                                                            </select>-->
                            <input type="hidden" name="businessType" value="1" />
                            <tr>
                                <td >Special Instruction &nbsp;&nbsp;&nbsp;&nbsp;<textarea rows="1" cols="16" class="form-control" style="width:250px;height:40px" name="consignmentOrderInstruction" id="consignmentOrderInstruction"><c:out value="${consigmentInstruction}"/></textarea></td>
                                <!--                            <td >Multi Pickup</td>-->
                                <td ><input type="hidden" class="form-control" style="width:250px;height:40px" name="multiPickup" id="multiPickup" onclick="multiPickupShow()" value="N"></td>
                                <!--                            <td >Multi Delivery</td>-->
                                <td  colspan="4" ><input type="hidden" class="form-control" style="width:250px;height:40px" name="multiDelivery" id="multiDelivery" onclick="multiDeliveryShow()" value="N"></td>
                            </tr>


                        </table>
                    </div>   

                    

                    <table>
                        <tr>
                            <td colspan="4" >

                                    <table class="table table-info mb30 table-hover" id="addTyres1" >
                                        <thead>
                                    <tr>
                                        <th width="20"  align="center" height="30" >Sno</th>
                                        <th  height="30" >Product/Article Code</th>
                                        <th  height="30" >Product/Article Name </th>
                                        <th  height="30" >Batch </th>
                                        <th  height="30" ><font color='red'>*</font>No of Packages</th>
                                        <th  height="30" ><font color='red'>*</font>Uom</th>
                                        <th  height="30" ><font color='red'>*</font>Total Weight (in Kg)</th>
                                    </tr>
                                        </thead>
                                    <% int j = 1; int sno1 = 0;%>
                                    <c:if test="${consignmentArticles != null}">
                                        <tbody>
                                            <c:forEach items="${consignmentArticles}" var="article">
                                                <tr>
                                                    <td  height="30" ><input type="hidden" name="consignmentArticleId" class="form-control" style="width:250px;height:40px" id="consignmentArticleId<%=sno1%>" value="<c:out value="${article.consignmentArticleId}"/>"/><%=j++%></label></td>
                                                    <td  height="30" ><input type="text" name="productCodes" class="form-control" style="width:250px;height:40px" id="productCodes<%=sno1%>" value="<c:out value="${article.articleCode}"/>"/></td>
                                                    <td  height="30" ><input type="text" name="batchCode" class="form-control" style="width:250px;height:40px" id="batchCode<%=sno1%>" value="<c:out value="${article.batchName}"/>"/></td>
                                                    <td  height="30" ><input type="text" name="productNames" class="form-control" style="width:250px;height:40px" id="productNames<%=sno1%>" value="<c:out value="${article.articleName}"/>"/></td>
                                                    <td  height="30" ><input type="text" name="packagesNos" class="form-control" style="width:250px;height:40px" id="packagesNos<%=sno1%>" value="<c:out value="${article.packageNos}"/>" onKeyPress='return onKeyPressBlockCharacters(event);'  onkeyup='calcTotalPacks(this.value)'/></td>
                                                    <td  height="30" >
                                                        <select name='uom' id='uom<%=sno1%>'>
                                                            <option value='1'>Box</option> 
                                                            <option value='2'>Bag</option>
                                                            <option value='3'>Pallet</option>
                                                            <option value='4'>Each</option>
                                                        </select> 
                                                        <script>
                                                            document.getElementById('uom' +<%=sno1%>).value = <c:out value="${article.unitOfMeasurement}"/>;
                                                        </script>
                                                    </td>
                                                    <td  height="30" ><input type="text" name="weights" class="form-control" style="width:250px;height:40px" id="weights<%=sno1%>" value="<c:out value="${article.packageWeight}"/>"  onKeyPress='return onKeyPressBlockCharacters(event);'  onkeyup='calcTotalWeights(this.value)' /></td>
                                                </tr>
                                                <%sno1++;%>
                                            </c:forEach>
                                        </tbody>
                                    </c:if>
                                    <tr>
                                        <td colspan="5" align="left">
                                            <input type="hidden" name="articlesCount" id="articlesCount" value="<%=j%>"/>
                                            <input type="button" class="btn btn-success"  value="Add Row" name="save" onClick="addRow1()">

                                        </td>
                                    </tr>
                                </table>
                                
                                
                            </td>
                            <td colspan="2">
                               <table class="table table-info mb30 table-hover"  >	
                                   <thead>

                                    <tr>
                                        <th>
                                            <label >Total No Packages</label>
                                            <label id="totalPackages"><c:out value="${totalPackages}"/></label>
                                            <input type="hidden" id="totalPackage" name="totalPackage" value="<c:out value="${totalPackages}"/>"/>
                                        </th>
                                        <th>
                                            <label >Total Weight (Kg)</label>
                                            <label id="totalWeight"><c:out value="${totalWeight}"/></label>
                                            <input type="hidden" id="totalWeightage" name="totalWeightage" value="<c:out value="${totalWeight}"/>"/>
                                        </th>
                                    </tr>
                                   </thead>
                                </table>
                            </td>
                        </tr>
                    </table>





                    
                   <table class="table table-info mb30 table-hover" id="bg" >
                       <thead>

                        <tr>
                            <th colspan="6" >Vehicle (Required) Details</th>
                        </tr>
                       </thead>


                        <!--                            <td >Service Type</td>
                                                    <td >
                                                        <select name="serviceType" class="form-control" style="width:250px;height:40px"  id="paymentType" style="width:120px;">
                                                            <option value="0" selected>--Select--</option>
                                                            <option value="1"> FTL </option>
                                                            <option value="2"> LTL </option>
                                                        </select>
                                                    </td>-->
                        <input type="hidden" name="serviceType" value="1" />
                        <input type="hidden" name="vehTypeId" value="0" />
                        <tr id="vehicleTypeDiv" style="display:none;">
                            <td >Vehicle Type </td>
                            <td   colspan="5" > <select name="vehTypeIdTemp" id="vehTypeIdTemp" class="form-control" style="width:250px;height:40px"  style="width:120px;" onchange="setFreightRate();">
                                    <c:if test="${vehicleTypeList != null}">
                                        <option value="0" selected>--Select--</option>
                                        <c:forEach items="${vehicleTypeList}" var="vehicleType">
                                            <option value="<c:out value="${vehicleType.vehicleTypeId}"/>"><c:out value="${vehicleType.vehicleTypeName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select></td>
                        </tr>
                        <tr id="vehicleTypeContractDiv" style="display:none;">
                            <td >
                                Vehicle Type &nbsp;&nbsp;                                

                            </td>
                            <td   colspan="5" > <select name="vehTypeIdContractTemp" id="vehTypeIdContractTemp"  onchange="setFreightRate1();" class="form-control" style="width:250px;height:40px"  style="width:120px;">
                                    <option value="0" selected>--Select--</option>
                                </select></td>
                        </tr>

                       
                        <!--edit vehicle type list-->
                        <tr id="editVehicleTypeDiv" style="display:none;">
                            <td >Vehicle Type <input type="hidden" id="editVehicleTypeId" id="editVehicleTypeId" value="<c:out value="${vehicleTypeId}"/>" /></td>
                            <td   colspan="5" > <select name="editVehTypeIdTemp" id="editVehTypeIdTemp" class="form-control" style="width:250px;height:40px"  style="width:120px;" onchange="setFreightRateForEdit();">
                                    <c:if test="${vehicleTypeList != null}">
                                        <c:forEach items="${vehicleTypeList}" var="vehicleType">
                                            <option value="<c:out value="${vehicleType.vehicleTypeId}"/>"><c:out value="${vehicleType.vehicleTypeName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                                <script>
                                 document.getElementById('editVehTypeIdTemp').value = '<c:out value="${actualKmVehicleTypeId}"/>';
                                </script>
                            </td>
                            <td  colspan="2">&nbsp;</td>     
                        </tr>
                        <tr id="editVehicleTypeContractDiv" style="display:none;">
                            <td >Vehicle Type</td>
                            <td > <select name="editVehTypeIdContractTemp" id="editVehTypeIdContractTemp" class="form-control" style="width:250px;height:40px"  style="width:120px;" onchange="setFreightRate1ForEdit();">
                                    <c:if test="${editVehicleTypeList  != null}">
                                        <c:forEach items="${editVehicleTypeList }" var="vehicleType">
                                            <option value="<c:out value="${vehicleType.vehicleTypeId}"/>"><c:out value="${vehicleType.vehicleTypeName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                                <script>
                                 document.getElementById('editVehTypeIdContractTemp').value = '<c:out value="${contractVehicleTypeId}"/>';
                                </script>
                            </td>
                            <td  colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                            <td ><font color="red">*</font>Reefer Required</td>
                            <td  colspan="5" >
                                <input type="text" readonly  name="reeferRequired" id="reeferRequired"  class="form-control" style="width:250px;height:40px"  value="" />
                                <!--                                <select name="reeferRequired" id="reeferRequired"  class="form-control" style="width:250px;height:40px"  style="width:120px;" onchange="calculateFreightTotal()">
                                                                    <option value="Yes" > Yes </option>
                                                                    <option value="No"> No </option>
                                                                </select>-->
                            </td>
                        </tr>
                        <script>


                            //edit vehicle type freight chharge calculation
                            function setFreightRateForEdit() {
                                var temp = document.getElementById("editVehTypeIdTemp").value;
                                var tempVal = temp.split("-");
                                document.cNote.vehTypeId.value = tempVal[0];
                                document.cNote.vehicleTypeId.value = tempVal[0];
                                $("#totFreightAmount").val(0);

                                //alert(document.cNote.vehTypeId.value);
                                //alert(document.cNote.vehicleTypeId.value);

                                var billingTypeId = document.getElementById("billingTypeId").value;
                                var customerTypeId = document.getElementById("customerTypeId").value;
                                var temp;

                            }

                            function setFreightRate1ForEdit() {
                                var temp = document.getElementById("editVehTypeIdContractTemp").value;
                                //alert(temp);
                                var tempVal = temp.split("-");
                                document.cNote.vehTypeId.value = tempVal[1];
                                document.cNote.vehicleTypeId.value = tempVal[1];
                                document.cNote.routeContractId.value = tempVal[0];
                                $("#totFreightAmount").val(0);


                                //alert(document.cNote.vehTypeId.value);
                                //alert(document.cNote.vehicleTypeId.value);

                                var billingTypeId = document.getElementById("billingTypeId").value;
                                var customerTypeId = document.getElementById("customerTypeId").value;
                                var temp;

                            }
                            //edit vehicle type freight chharge calculation


                            function setRouteDetails() {
                                $("#routeChart").show();
                                var temp = document.getElementById('destinationTemp').value;
                                //alert(temp);
                                var destinationSelect = document.getElementById('destinationTemp');
                                var destinationName = destinationSelect.options[destinationSelect.selectedIndex].text;

                                var originSelect = document.getElementById('originTemp');
                                var originName = originSelect.options[originSelect.selectedIndex].text;

                                var tempVal = temp.split('-');
                                document.cNote.destination.value = tempVal[1];
                                //alert(document.cNote.destination.value);
                                document.cNote.routeId.value = tempVal[0];
                                document.cNote.routeBased.value = 'Y';
                                //remove table rows and reset values
                                var tab = document.getElementById("routePlan");
                                //alert(tab.rows.length);
                                //alert(podRowCount1);
                                if (podRowCount1 > 1) {
                                    for (var x = 1; x < podRowCount1; x++) {
                                        document.getElementById("routePlan").deleteRow(1);
                                    }
                                }
                                podRowCount1 = 1;
                                podSno1 = 0;
                                //alert(document.cNote.destinationTemp.value);
                                if ((document.cNote.destinationTemp.value != '0') && (document.cNote.destinationTemp.value != '')) {
                                    //alert("am here...");
                                    var startDate = document.cNote.startDate.value;
                                    addRouteCourse1(document.cNote.origin.value, originName, '1', 'Pick Up', 0, 0, 0, startDate);
                                    addRouteCourse1(document.cNote.destination.value, destinationName, '2', 'Drop', tempVal[0], tempVal[1], tempVal[2], '');
                                    $("#routePlanAddRow").show();
                                    $("#freezeRoute").show();
                                    $("#resetRoute").show();
                                } else {
                                    $("#routePlanAddRow").hide();
                                    $("#freezeRoute").hide();
                                    $("#resetRoute").hide();
                                }
                            }

                            function setContractRouteDetails() {

                                $("#routeChart").show();
                                var origin = document.getElementById('contractRouteOrigin').value;

                                var destination = document.getElementById('contractRouteDestination').value;
                                //alert(origin + " ::: " + destination);
                                var destinationSelect = document.getElementById('contractRouteDestination');
                                var destinationName = destinationSelect.options[destinationSelect.selectedIndex].text;

                                var originSelect = document.getElementById('contractRouteOrigin');
                                var originName = originSelect.options[originSelect.selectedIndex].text;

                                document.cNote.origin.value = origin;
                                document.cNote.destination.value = destination;
                                //remove table rows and reset values
                                var tab = document.getElementById("routePlan");
                                //alert(tab.rows.length);
                                //alert(podRowCount1);
                                if (podRowCount1 > 1) {
                                    for (var x = 1; x < podRowCount1; x++) {
                                        document.getElementById("routePlan").deleteRow(1);
                                    }
                                }
                                podRowCount1 = 1;
                                podSno1 = 0;
                                if (document.cNote.origin.value != '0' && document.cNote.destination.value != '0') {
                                    //alert("am here...");
                                    //function addRouteCourse1(id, name, order, type,routeId,routeKm,routeReeferHr) {
                                    var startDate = document.cNote.startDate.value;
                                    addRouteCourse3(document.cNote.origin.value, originName, '1', 'Pick Up', 0, 0, 0, startDate);
                                    addRouteCourse3(document.cNote.destination.value, destinationName, '2', 'Drop', 0, 0, 0, '');
                                    $("#routePlanAddRow").show();
                                    $("#freezeRoute").show();
                                    $("#resetRoute").show();
                                } else {
                                    $("#routePlanAddRow").hide();
                                    $("#freezeRoute").hide();
                                    $("#resetRoute").hide();
                                }


                            }

                            function setFreightRate() {
                                var temp = document.getElementById("vehTypeIdTemp").value;
                                var tempVal = temp.split("-");
                                document.cNote.vehTypeId.value = tempVal[0];
                                document.cNote.vehicleTypeId.value = tempVal[0];

                                //alert(document.cNote.vehTypeId.value);
                                //alert(document.cNote.vehicleTypeId.value);

                                var billingTypeId = document.getElementById("billingTypeId").value;
                                var customerTypeId = document.getElementById("customerTypeId").value;
                                var temp;

                            }
                            function setFreightRate1() {
                                var temp = document.getElementById("vehTypeIdContractTemp").value;
                                //alert(temp);
                                var tempVal = temp.split("-");
                                document.cNote.vehTypeId.value = tempVal[1];
                                document.cNote.vehicleTypeId.value = tempVal[1];
                                document.cNote.routeContractId.value = tempVal[0];


                                //alert(document.cNote.vehTypeId.value);
                                //alert(document.cNote.vehicleTypeId.value);

                                var billingTypeId = document.getElementById("billingTypeId").value;
                                var customerTypeId = document.getElementById("customerTypeId").value;
                                var temp;

                            }

                            function calculateFreightTotal() {
                                var reeferRequired = document.getElementById('reeferRequired').value;
                                var billingTypeId = document.getElementById('billingTypeId').value;
                                var rateWithReefer = document.getElementById('rateWithReefer').value;
                                var rateWithoutReefer = document.getElementById('rateWithoutReefer').value;
                                var totalKm = document.getElementById('totalKm').value;
                                var totalWeight = $("#totalWeight").text();
                                var totalAmount = 0;
                                if (reeferRequired == 'Yes' && billingTypeId == 1) {
                                    $('#freightAmount').text(rateWithReefer)
                                    $('#totFreightAmount').val(rateWithReefer)
                                    $('#totalCharges').val(rateWithReefer)
                                } else if (reeferRequired == 'No' && billingTypeId == 1) {
                                    $('#freightAmount').text(rateWithoutReefer)
                                    $('#totFreightAmount').val(rateWithoutReefer)
                                    $('#totalCharges').val(rateWithoutReefer)
                                } else if (reeferRequired == 'Yes' && billingTypeId == 2) {
                                    totalAmount = totalWeight * rateWithReefer;
                                    //alert(totalAmount);
                                    $('#freightAmount').text(totalAmount)
                                    $('#totFreightAmount').val(totalAmount)
                                    $('#totalCharges').val(totalAmount)
                                } else if (reeferRequired == 'No' && billingTypeId == 2) {
                                    totalAmount = totalWeight * rateWithReefer;
                                    //alert(totalAmount);
                                    $('#freightAmount').text(totalAmount)
                                    $('#totFreightAmount').val(totalAmount)
                                    $('#totalCharges').val(totalAmount)
                                } else if (reeferRequired == 'Yes' && billingTypeId == 3) {
                                    totalAmount = totalKm * rateWithReefer;
                                    //alert(totalAmount);
                                    $('#freightAmount').text(totalAmount)
                                    $('#totFreightAmount').val(totalAmount)
                                    $('#totalCharges').val(totalAmount)
                                } else if (reeferRequired == 'No' && billingTypeId == 3) {
                                    totalAmount = totalKm * rateWithoutReefer;
                                    //alert(totalAmount);
                                    $('#freightAmount').text(totalAmount)
                                    $('#totFreightAmount').val(totalAmount)
                                    $('#totalCharges').val(totalAmount)
                                }
                            }
                        </script>
                        <tr>
                            <!--                            <td ><font color="red">*</font>Vehicle Required Date</td>
                                                        <td >-->
                        <input type="hidden" class="form-control" style="width:250px;height:40px" name="destinationId" id="destinationId" value="<c:out value="${consigmentDestination}"/>" >
                        <input type="hidden" class="form-control" style="width:250px;height:40px" name="routeContractId" id="routeContractId" value="<c:out value="${routeContractId}"/>" />
                        <input type="hidden" class="form-control" style="width:250px;height:40px" name="routeId" id="routeId" value="<c:out value="${routeId}"/>" />
                        <input type="hidden" class="form-control" style="width:250px;height:40px" name="routeBased" id="routeBased" >
                        <input type="hidden" class="form-control" style="width:250px;height:40px" name="contractRateId" id="contractRateId" value="<c:out value="${contractRateId}"/>" / >
                               <input type="hidden" class="form-control" style="width:250px;height:40px" name="totalKm" id="totalKm" value="<c:out value="${totalDistance}"/>" />
                        <input type="hidden" class="form-control" style="width:250px;height:40px" name="totalHours" id="totalHours" value="<c:out value="${totalHours}"/>" />
                        <input type="hidden" class="form-control" style="width:250px;height:40px" name="totalMinutes" id="totalMinutes" value="<c:out value="${totalMinutes}"/>" />
                        <input type="hidden" class="form-control" style="width:250px;height:40px" name="totalPoints" id="totalPoints" value="" />
                        <input type="hidden" class="form-control" style="width:250px;height:40px" name="vehicleTypeId" id="vehicleTypeId" value="<c:out value="${vehicleTypeId}"/>" />
                        <input type="hidden" class="form-control" style="width:250px;height:40px" name="rateWithReefer" id="rateWithReefer" value="" />
                        <input type="hidden" class="form-control" style="width:250px;height:40px" name="rateWithoutReefer" id="rateWithoutReefer" value="" />
                        <input type="hidden" readonly class="datepicker" name="vehicleRequiredDate" id="vehicleRequiredDate" value="<c:out value="${vehicleRequiredDate}"/>" />

                        <input type="hidden" readonly  name="vehicleRequiredHour" id="vehicleRequiredHour" value="<c:out value="${vehicleRequiredTime}"/>" >
                        <input type="hidden" readonly  name="vehicleRequiredMinute" id="vehicleRequiredMinute"value="<c:out value="${vehicleRequiredTime}"/>"  >

                        <td >Special Instruction</td>
                        <td  colspan="5" ><textarea rows="1" cols="16" class="form-control" style="width:250px;height:40px" name="vehicleInstruction" id="vehicleInstruction"><c:out value="${vehicleInstruction}"/></textarea></td>
                        </tr>
                        <script>
                            function calculateTime() {
                                var date1 = new Date();
                                var tempDate = $("#vehicleRequiredDate").val();
                                var temp = tempDate.split("-");
                                var requiredDay = temp[0];
                                var requiredMonth = temp[1];
                                var requiredYear = temp[2];
                                var requiredHour = $("#vehicleRequiredHour").val();
                                if (requiredHour == "") {
                                    requiredHour = "00";
                                }
                                var requiredMinute = $("#vehicleRequiredMinute").val();
                                if (requiredMinute == "") {
                                    requiredMinute = "00";
                                }
                                var requiredSeconds = "00";
                                var date2 = new Date(requiredYear, requiredMonth, requiredDay, requiredHour, requiredMinute, requiredSeconds)
                                var diff = Math.abs(date1.getTime() - date2.getTime()) / 1000 / 60 / 60;
                                alert(diff);
                                var diffDays = diff.toFixed(2);
                                alert(diffDays);
                                if (diffDays < 10) {
                                    alert("No")
                                } else {
                                    alert("Yes");
                                }
                            }
                        </script>
                    </table>
                    
                    
                        <table class="table table-info mb30 table-hover" id="bg" >
                            <thead>
                        <tr>
                            <th   height="30" colspan="6">Consignor Details</th>
                        </tr>
                            </thead>
                        <tr>
                            <td ><font color="red">*</font>Consignor Name</td>
                            <td ><input type="text" class="form-control" style="width:250px;height:40px" id="consignorName" onKeyPress="return onKeyPressBlockNumbers(event);" name="consignorName"  value="<c:out value="${consignorName}"/>"></td>
                            <td ><font color="red">*</font>Mobile No</td>
                            <td ><input type="text" class="form-control" style="width:250px;height:40px" id="consignorPhoneNo" onKeyPress="return onKeyPressBlockCharacters(event);"  name="consignorPhoneNo" value="<c:out value="${consignorMobile}"/>"></td>
                            <td ><font color="red">*</font>Address</td>
                            <td ><textarea rows="1" cols="16" name="consignorAddress" class="form-control" style="width:250px;height:40px" id="consignorAddress"><c:out value="${consignorAddress}"/></textarea> </td>
                        </tr>
                    </table>
                    
                    
                        <table class="table table-info mb30 table-hover" id="bg" >
                            <thead>
                        <tr>
                            <th   height="30" colspan="6">Consignee Details</th>
                        </tr>
                            </thead>
                        <tr>
                            <td ><font color="red">*</font>Consignee Name</td>
                            <td ><input type="text" class="form-control" style="width:250px;height:40px" id="consigneeName" onKeyPress="return onKeyPressBlockNumbers(event);"  name="consigneeName" value="<c:out value="${consigneeName}"/>"></td>
                            <td ><font color="red">*</font>Mobile No</td>
                            <td ><input type="text" class="form-control" style="width:250px;height:40px" id="consigneePhoneNo" onKeyPress="return onKeyPressBlockCharacters(event);"   name="consigneePhoneNo" value="<c:out value="${consigneeMobile}"/>"></td>
                            <td ><font color="red">*</font>Address</td>
                            <td ><textarea rows="1" cols="16" name="consigneeAddress" class="form-control" style="width:250px;height:40px" id="consigneeAddress"><c:out value="${consigneeAddress}"/></textarea> </td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                    <!--                    <center>
                                            <a  class="nexttab" href="#"><input type="button" class="btn btn-success"  value="Next" name="Save" ></a>
                                        </center>-->
                </div>





                <script>
                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                    $(".previoustab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected - 1);
                    });
                </script>
                <!--                <div id="paymentDetails">-->

                <table  class="table table-info mb30 table-hover"  id="contractFreightTable">
                    <thead>
                    <tr>
                        <th   height="30" colspan="6" > Freight Details</th>
                    </tr>
                    </thead>
                    <tr>
                        <td >Billing Type</td>
                        <td ><label id="billTypeName"><c:out value="${billingTypeName}"/></label></td>
                        <td  align="right" >Estimated Freight Charges &nbsp; </td>
                        <td >INR.
                            <input type="text" readonly class="form-control" style="width:250px;height:40px" name="totFreightAmount" id="totFreightAmount" value="<c:out value="${freightCharges}"/>"/>
                        </td>
                    </tr>
                </table>
                <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="walkinFreightTable">
                    <tr>
                        <td   height="30" colspan="6" >Walkin Freight Details</td>
                    </tr>
                    <tr>
                        <td >Billing Type</td>
                        <td ><select name="walkInBillingTypeId" id="walkInBillingTypeId" class="texttbox" onchange="displayWalkIndetails(this.value)" style="width: 120px">                                    
                                <option value="0" selected>--Select--</option>
                                <option value="1" >Fixed Freight</option>
                                <option value="2" >Rate Per KM</option>
                                <option value="3" >Rate Per Kg</option>
                            </select></td>
                        <td >Freight Charges (INR.)</td>
                        <td >
                            <input type="text" readonly class="form-control" style="width:250px;height:40px" name="totFreightAmount1" id="totFreightAmount1" value="0"/>
                        </td>
                    </tr>
                    <tr id="WalkInptp" style="display: none">
                        <td >Freight With Reefer</td>
                        <td ><input type="text" name="walkinFreightWithReefer" id="walkinFreightWithReefer" class="form-control" style="width:250px;height:40px" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                        <td >Freight Without Reefer</td>
                        <td ><input type="text" name="walkinFreightWithoutReefer" id="walkinFreightWithoutReefer" class="form-control" style="width:250px;height:40px" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                    </tr>
                    <tr id="WalkInPtpw" style="display: none">
                        <td >Rate With Reefer/Kg</td>
                        <td ><input type="text" name="walkinRateWithReeferPerKg" id="walkinRateWithReeferPerKg" class="form-control" style="width:250px;height:40px" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                        <td >Rate Without Reefer/Kg</td>
                        <td ><input type="text" name="walkinRateWithoutReeferPerKg" id="walkinRateWithoutReeferPerKg" class="form-control" style="width:250px;height:40px" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                    </tr>
                    <tr id="WalkinKilometerBased" style="display: none">
                        <td >Rate With Reefer/Km</td>
                        <td ><input type="text" name="walkinRateWithReeferPerKm" id="walkinRateWithReeferPerKm" class="form-control" style="width:250px;height:40px" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                        <td >Rate Without Reefer/Km</td>
                        <td ><input type="text" name="walkinRateWithoutReeferPerKm" id="walkinRateWithoutReeferPerKm" class="form-control" style="width:250px;height:40px" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                    </tr>
                </table>
                <script type="text/javascript">
                    function displayWalkIndetails(val) {
                        var reeferRequired = document.getElementById('reeferRequired').value;
                        var vehicleTypeId = document.getElementById('vehTypeIdTemp').value;
                        //alert(vehicleTypeId);
                        if (vehicleTypeId == '0') {
                            alert("Please Choose Vehicle Type");
                            document.cNote.walkInBillingTypeId.value = 0;
                            document.getElementById('vehTypeIdTemp').focus();
                        } else if (reeferRequired == '0') {
                            alert("Please Choose Reefer Details");
                            document.getElementById('reeferRequired').focus();
                        } else {
                            if (val == 1) {
                                $("#WalkInptp").show();
                                $("#WalkInPtpw").hide();
                                $("#WalkinKilometerBased").hide();
                                if (reeferRequired == "Yes") {
                                    $('#walkinFreightWithReefer').attr('readonly', false);
                                    $('#walkinFreightWithoutReefer').attr('readonly', true);
                                } else if (reeferRequired == "No") {
                                    $('#walkinFreightWithReefer').attr('readonly', true);
                                    $('#walkinFreightWithoutReefer').attr('readonly', false);
                                }
                                $("#walkinRateWithReeferPerKm").val("");
                                $("#walkinRateWithoutReeferPerKm").val("");
                                $("#walkinRateWithReeferPerKg").val("");
                                $("#walkinRateWithoutReeferPerKg").val("");
                            } else if (val == 2) {
                                $("#WalkInptp").hide();
                                $("#WalkInPtpw").show();
                                $("#WalkinKilometerBased").hide();
                                if (reeferRequired == "Yes") {
                                    $('#walkinRateWithReeferPerKg').attr('readonly', false);
                                    $('#walkinRateWithoutReeferPerKg').attr('readonly', true);
                                } else if (reeferRequired == "No") {
                                    $('#walkinRateWithReeferPerKg').attr('readonly', true);
                                    $('#walkinRateWithoutReeferPerKg').attr('readonly', false);
                                }
                                $("#walkinFreightWithReefer").val("");
                                $("#walkinFreightWithoutReefer").val("");
                                $("#walkinRateWithReeferPerKm").val("");
                                $("#walkinRateWithoutReeferPerKm").val("");
                            } else if (val == 3) {
                                $("#WalkInptp").hide();
                                $("#WalkInPtpw").hide();
                                $("#WalkinKilometerBased").show();
                                if (reeferRequired == "Yes") {
                                    $('#walkinRateWithReeferPerKm').attr('readonly', false);
                                    $('#walkinRateWithoutReeferPerKm').attr('readonly', true);
                                } else if (reeferRequired == "No") {
                                    $('#walkinRateWithReeferPerKm').attr('readonly', true);
                                    $('#walkinRateWithoutReeferPerKm').attr('readonly', false);
                                }
                                $("#walkinFreightWithReefer").val("");
                                $("#walkinFreightWithoutReefer").val("");
                                $("#walkinRateWithReeferPerKg").val("");
                                $("#walkinRateWithoutReeferPerKg").val("");
                            }
                        }
                    }
                </script>
                <br/>
                <br/>
                <!--                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                        <tr>
                                            <td   height="30" colspan="6">Standard / Additional Charges</td>
                                        </tr>
                                        <tr>
                                            <td >Document Charges</td>
                                            <td ><input type="text" class="form-control" style="width:250px;height:40px" id="docCharges" name="docCharges" onkeyup="calculateSubTotal()"></td>
                                            <td >ODA Charges</td>
                                            <td ><input type="text" class="form-control" style="width:250px;height:40px" id="odaCharges" name="odaCharges" onkeyup="calculateSubTotal()"></td>
                                            <td >Multi Pickup Charges</td>
                                            <td ><input type="text" class="form-control" style="width:250px;height:40px" id="multiPickupCharge" name="multiPickupCharge" readonly onkeyup="calculateSubTotal()"></td>
                                        </tr>
                                        <tr>
                                            <td >Multi Delivery Charges</td>
                                            <td ><input type="text" class="form-control" style="width:250px;height:40px" id="multiDeliveryCharge" name="multiDeliveryCharge" readonly onkeyup="calculateSubTotal()"></td>
                                            <td >Handling Charges</td>
                                            <td ><input type="text" class="form-control" style="width:250px;height:40px" id="handleCharges" name="handleCharges" onkeyup="calculateSubTotal()"></td>
                                            <td >Other Charges</td>
                                            <td ><input type="text" class="form-control" style="width:250px;height:40px" id="otherCharges" name="otherCharges" onkeyup="calculateSubTotal()"></td>
                                        </tr>
                                        <tr>                            
                                            <td >Unloading Charges</td>
                                            <td ><input type="text" class="form-control" style="width:250px;height:40px" id="unloadingCharges" name="unloadingCharges" onkeyup="calculateSubTotal()"></td>
                                            <td >Loading Charges</td>
                                            <td ><input type="text" class="form-control" style="width:250px;height:40px" id="loadingCharges" name="loadingCharges" onkeyup="calculateSubTotal()"></td>
                                            <td >Remarks</td>
                                            <td ><textarea rows="3" cols="20" nmae="standardChargeRemarks" id="standardChargeRemarks"></textarea></td>
                                        </tr>
                                        <tr>
                                            <td  colspan="4"></td>
                                            <td >Sub Total</td>
                                            <td ><input type="text" class="form-control" style="width:250px;height:40px" id="subTotal" name="subTotal"  value="0" readonly=""></td>
                                        </tr>
                                    </table>-->
                <input type="hidden" class="form-control" style="width:250px;height:40px" id="subTotal" name="subTotal"  value="0" readonly="">
                <script type="text/javascript">
                    function calculateSubTotal1() {
                        var customerTypeId = document.getElementById("customerTypeId").value;
                        if (customerTypeId == 2) {
                            var fixedRateWithReefer = $("#walkinFreightWithReefer").val();
                            var fixedRateWithoutReefer = $("#walkinFreightWithoutReefer").val();
                            var rateWithReeferPerKm = $("#walkinRateWithReeferPerKm").val();
                            var rateWithoutReeferPerKm = $("#walkinRateWithoutReeferPerKm").val();
                            var rateWithReeferPerKg = $("#walkinRateWithReeferPerKg").val();
                            var rateWithoutReeferPerKg = $("#walkinRateWithoutReeferPerKg").val();
                        }
                        var freightAmount = $('#freightAmount').text();
                        var docCharges = document.getElementById('docCharges').value;
                        var odaCharges = document.getElementById('odaCharges').value;
                        var multiPickupCharge = document.getElementById('multiPickupCharge').value;
                        var multiDeliveryCharge = document.getElementById('multiDeliveryCharge').value;
                        var handleCharges = document.getElementById('handleCharges').value;
                        var otherCharges = document.getElementById('otherCharges').value;
                        var unloadingCharges = document.getElementById('unloadingCharges').value;
                        var loadingCharges = document.getElementById('loadingCharges').value;
                        var total = 0;
                        var walkInTotal = 0;
                        var kmRate = 0;
                        var kgRate = 0;
                        if (customerTypeId == 2) {
                            var billingTypeId = $("#walkInBillingTypeId").val();
                            var totalKm = $("#totalKm").val();
                            var totalWeight = $("#totalWeight").text();
                            if (fixedRateWithReefer != '' && billingTypeId == 1) {
                                walkInTotal += parseInt(fixedRateWithReefer);
                            } else {
                                walkInTotal += parseInt(0);
                            }
                            if (fixedRateWithoutReefer != '' && billingTypeId == 1) {
                                walkInTotal += parseInt(fixedRateWithoutReefer);
                            } else {
                                walkInTotal += parseInt(0);
                            }
                            if (rateWithReeferPerKm != '' && billingTypeId == 3) {
                                kmRate = parseInt(totalKm) * parseInt(rateWithReeferPerKm);
                                walkInTotal += parseInt(kmRate);
                            } else {
                                walkInTotal += parseInt(0);
                            }
                            if (rateWithoutReeferPerKm != '' && billingTypeId == 3) {
                                kmRate = parseInt(totalKm) * parseInt(rateWithoutReeferPerKm);
                                walkInTotal += parseInt(kmRate);
                            } else {
                                walkInTotal += parseInt(0);
                            }
                            if (rateWithReeferPerKg != '' && billingTypeId == 2) {
                                kgRate = parseInt(totalWeight) * parseInt(rateWithReeferPerKg);
                                walkInTotal += parseInt(kgRate);
                            } else {
                                walkInTotal += parseInt(0);
                            }
                            if (rateWithoutReeferPerKg != '' && billingTypeId == 2) {
                                kgRate = parseInt(totalWeight) * parseInt(rateWithoutReeferPerKg);
                                walkInTotal += parseInt(kgRate);
                            } else {
                                walkInTotal += parseInt(0);
                            }
                        }
                        if (docCharges != '') {
                            total += parseInt(docCharges);
                        } else {
                            total += parseInt(0);
                        }
                        if (odaCharges != '') {
                            total += parseInt(odaCharges);
                        } else {
                            total += parseInt(0);
                        }
                        if (multiPickupCharge != '') {
                            total += parseInt(multiPickupCharge);
                        } else {
                            total += parseInt(0);
                        }
                        if (multiDeliveryCharge != '') {
                            total += parseInt(multiDeliveryCharge);
                        } else {
                            total += parseInt(0);
                        }
                        if (handleCharges != '') {
                            total += parseInt(handleCharges);
                        } else {
                            total += parseInt(0);
                        }
                        if (otherCharges != '') {
                            total += parseInt(otherCharges);
                        } else {
                            total += parseInt(0);
                        }
                        if (unloadingCharges != '') {
                            total += parseInt(unloadingCharges);
                        } else {
                            total += parseInt(0);
                        }
                        if (loadingCharges != '') {
                            total += parseInt(loadingCharges);
                        } else {
                            total += parseInt(0);
                        }
                        document.getElementById('subTotal').value = total;
                        if (customerTypeId == 1) {
                            if (freightAmount != '') {
                                total += parseInt(freightAmount);
                            } else {
                                total += parseInt(0);
                            }
                        } else if (customerTypeId == 2) {
                            total += parseInt(walkInTotal);
                        }
                        document.getElementById('totalCharges').value = total;
                    }
                    function calculateSubTotal() {
                        estimateFreight();
                        var freightAmount = document.cNote.totFreightAmount.value;
                        var docCharges = document.getElementById('docCharges').value;
                        var odaCharges = document.getElementById('odaCharges').value;
                        var multiPickupCharge = document.getElementById('multiPickupCharge').value;
                        var multiDeliveryCharge = document.getElementById('multiDeliveryCharge').value;
                        var handleCharges = document.getElementById('handleCharges').value;
                        var otherCharges = document.getElementById('otherCharges').value;
                        var unloadingCharges = document.getElementById('unloadingCharges').value;
                        var loadingCharges = document.getElementById('loadingCharges').value;
                        var total = 0;
                        var walkInTotal = 0;
                        var kmRate = 0;
                        var kgRate = 0;

                        if (docCharges != '') {
                            total += parseInt(docCharges);
                        } else {
                            total += parseInt(0);
                        }
                        if (odaCharges != '') {
                            total += parseInt(odaCharges);
                        } else {
                            total += parseInt(0);
                        }
                        if (multiPickupCharge != '') {
                            total += parseInt(multiPickupCharge);
                        } else {
                            total += parseInt(0);
                        }
                        if (multiDeliveryCharge != '') {
                            total += parseInt(multiDeliveryCharge);
                        } else {
                            total += parseInt(0);
                        }
                        if (handleCharges != '') {
                            total += parseInt(handleCharges);
                        } else {
                            total += parseInt(0);
                        }
                        if (otherCharges != '') {
                            total += parseInt(otherCharges);
                        } else {
                            total += parseInt(0);
                        }
                        if (unloadingCharges != '') {
                            total += parseInt(unloadingCharges);
                        } else {
                            total += parseInt(0);
                        }
                        if (loadingCharges != '') {
                            total += parseInt(loadingCharges);
                        } else {
                            total += parseInt(0);
                        }
                        document.getElementById('subTotal').value = total.toFixed(2);
                        total = total + parseFloat(freightAmount);
                        document.getElementById('totalCharges').value = total.toFixed(2);
                    }
                </script>
                <br/>
                <!--                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                        <tr>
                                            <td   height="30" colspan="60">&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </td>
                                            <td   height="30" colspan="6" align="right">Total Charges</td>
                                            <td   height="30" align="right">INR.<input align="right" value="" type="text" readonly class="form-control" style="width:250px;height:40px" id="totalCharges" name="totalCharges" ></td>
                                        </tr>
                
                                    </table>-->
                <input align="right" value="" type="hidden" readonly class="form-control" style="width:250px;height:40px" id="totalCharges" name="totalCharges" >
                <br/>
                <br/>

                <center>
                    <!--                        <input type="button" class="btn btn-success"  name="Save" value="Estimate Freight" onclick="estimateFreight();" >-->
                    <input type="hidden" name="editCountValue"  id="editCountValue" value="1"/>
                    <input type="button" class="btn btn-success"  name="Save" value="Save Changes" onclick="submitPage(this.value);" >
                </center>
                <!--                </div>-->

            </div>
        </form>
    </body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>