
var elementName="";
var targetURL="";
function ListSuggestions(txtFieldName, url) {
	elementName = txtFieldName;	
	targetURL = url;
    
}

function getXmlHttpRequestObject() {
	if (window.XMLHttpRequest) {
		return new XMLHttpRequest();
	} else if(window.ActiveXObject) {
		return new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		alert("Your Browser does not support Ajax-ing!");
	}
	}

var suggestReq = getXmlHttpRequestObject();

/**
 * Request suggestions for the given autosuggest control. 
 * @scope protected
 * @param oAutoSuggestControl The autosuggest control to provide suggestions for.
 */
ListSuggestions.prototype.requestSuggestions = function (oAutoSuggestControl /*:AutoSuggestControl*/,
                                                          bTypeAhead /*:boolean*/) {
    //alert("am in sug.js");
	if (suggestReq.readyState == 4 || suggestReq.readyState == 0) {
		var elementValue = escape(document.getElementById(elementName).value);
		var targetURLFinal = targetURL + 'itemName'+'='+elementValue;
		//alert(targetURLFinal);
		suggestReq.open("POST", targetURLFinal, false);				
		suggestReq.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		suggestReq.send("");
	}    
     
 	var resp=suggestReq.responseText;     
    if(suggestReq.readyState==4) {
	    if (suggestReq.status == 200) { 
			var suggestions = [];
			resp=resp.split("~");
			var j=0;
			for (var i=0; i<resp.length-1; i++)	{			
		    	if(resp[i] !=""){
					suggestions[j] = resp[i];
					j++;
				}               
			}
		    var aSuggestions = [];
		    var sTextboxValue = oAutoSuggestControl.textbox.value;
                     
		    if (sTextboxValue.length > 0){
				for (var i=0; i < suggestions.length; i++) { 				
				    //if (suggestions[i].indexOf(sTextboxValue) == 0) {
						aSuggestions.push(suggestions[i]);
				    //} 
				}
		   	}
		    //provide suggestions to the control
		    oAutoSuggestControl.autosuggest(aSuggestions, bTypeAhead);		    
		}
   }    
  
};