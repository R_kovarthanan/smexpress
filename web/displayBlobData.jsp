<%-- 
    Document   : displayBlobData
    Created on : Feb 22, 2014, 10:57:49 PM
    Author     : Throttle
--%>

<%@page import="java.util.Properties"%>
<%@ page import="java.sql.*"%>

<%@ page import="java.io.*"%>
                    <%
                        Blob image = null;
                        Blob licenseImage = null;

                        Connection con = null;

                        byte[] imgData = null;
                        byte[] licenseImgData = null;

                        Statement stmt = null;

                        ResultSet rs = null;
                        String staffId = request.getParameter("staffId");
                        String empPhoto = request.getParameter("empPhoto");
                        
                        String fileName = "jdbc_url.properties";
                        Properties dbProps = new Properties();
                        //The forward slash "/" in front of in_filename will ensure that
                        //no package names are prepended to the filename when the Classloader
                        //search for the file in the classpath

                        InputStream is = getClass().getResourceAsStream("/"+fileName);
                        dbProps.load(is);//this may throw IOException
                        String dbClassName = dbProps.getProperty("jdbc.driverClassName");

                        String dbUrl = dbProps.getProperty("jdbc.url");
                        String dbUserName = dbProps.getProperty("jdbc.username");
                        String dbPassword = dbProps.getProperty("jdbc.password");
                        try {

                            Class.forName(dbClassName).newInstance();

                            con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);

                            stmt = con.createStatement();

                            if ("1".equals(empPhoto)) {
                                rs = stmt.executeQuery("select Emp_Photo from papl_emp_master where emp_id = " + staffId);

                                if (rs.next()) {

                                    image = rs.getBlob(1);

                                    imgData = image.getBytes(1, (int) image.length());

                                } else {

                                    //out.println("Display Blob Example");

                                    //out.println("image not found for given id>");

                                    return;

                                }

                                // display the image
                                response.setContentType("image/jpeg");

                                OutputStream o = response.getOutputStream();
                                o.write(imgData);
                                o.flush();

                                o.close();

                            } else {
                                rs = stmt.executeQuery("select License_Photo from papl_emp_master where emp_id = " + staffId);

                                if (rs.next()) {

                                    licenseImage = rs.getBlob(1);

                                    licenseImgData = licenseImage.getBytes(1, (int) licenseImage.length());

                                } else {

                                    //out.println("Display Blob Example");

                                    //out.println("image not found for given id>");

                                    return;

                                }

                                // display the image
                                response.setContentType("image/gif");

                                OutputStream o = response.getOutputStream();

                                o.write(licenseImgData);

                                o.flush();

                                o.close();

                            }

                        } catch (Exception e) {

                            out.println("Please Upload Image");

                            //out.println("Image Display Error=" + e.getMessage());

                            return;

                        } finally {

                            try {

                                rs.close();

                                stmt.close();

                                con.close();

                            } catch (SQLException e) {

                                e.printStackTrace();

                            }

                        }

                    %> 
