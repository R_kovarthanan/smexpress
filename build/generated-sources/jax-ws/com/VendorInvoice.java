
package com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="efsId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="efs_branchId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="invoice_no" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="invoice_date" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="invoice_amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="taxable_amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cgst_percentage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cgst_amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sgst_percentage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sgst_amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="igst_percentage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="igst_amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="net_amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="gst_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "efsId",
    "efsBranchId",
    "invoiceNo",
    "invoiceDate",
    "invoiceAmount",
    "taxableAmount",
    "cgstPercentage",
    "cgstAmount",
    "sgstPercentage",
    "sgstAmount",
    "igstPercentage",
    "igstAmount",
    "netAmount",
    "gstId",
    "username",
    "password"
})
@XmlRootElement(name = "VendorInvoice")
public class VendorInvoice {

    protected String efsId;
    @XmlElement(name = "efs_branchId")
    protected String efsBranchId;
    @XmlElement(name = "invoice_no")
    protected String invoiceNo;
    @XmlElement(name = "invoice_date")
    protected String invoiceDate;
    @XmlElement(name = "invoice_amount")
    protected String invoiceAmount;
    @XmlElement(name = "taxable_amount")
    protected String taxableAmount;
    @XmlElement(name = "cgst_percentage")
    protected String cgstPercentage;
    @XmlElement(name = "cgst_amount")
    protected String cgstAmount;
    @XmlElement(name = "sgst_percentage")
    protected String sgstPercentage;
    @XmlElement(name = "sgst_amount")
    protected String sgstAmount;
    @XmlElement(name = "igst_percentage")
    protected String igstPercentage;
    @XmlElement(name = "igst_amount")
    protected String igstAmount;
    @XmlElement(name = "net_amount")
    protected String netAmount;
    @XmlElement(name = "gst_id")
    protected String gstId;
    protected String username;
    protected String password;

    /**
     * Gets the value of the efsId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEfsId() {
        return efsId;
    }

    /**
     * Sets the value of the efsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEfsId(String value) {
        this.efsId = value;
    }

    /**
     * Gets the value of the efsBranchId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEfsBranchId() {
        return efsBranchId;
    }

    /**
     * Sets the value of the efsBranchId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEfsBranchId(String value) {
        this.efsBranchId = value;
    }

    /**
     * Gets the value of the invoiceNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * Sets the value of the invoiceNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceNo(String value) {
        this.invoiceNo = value;
    }

    /**
     * Gets the value of the invoiceDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceDate() {
        return invoiceDate;
    }

    /**
     * Sets the value of the invoiceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceDate(String value) {
        this.invoiceDate = value;
    }

    /**
     * Gets the value of the invoiceAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceAmount() {
        return invoiceAmount;
    }

    /**
     * Sets the value of the invoiceAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceAmount(String value) {
        this.invoiceAmount = value;
    }

    /**
     * Gets the value of the taxableAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxableAmount() {
        return taxableAmount;
    }

    /**
     * Sets the value of the taxableAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxableAmount(String value) {
        this.taxableAmount = value;
    }

    /**
     * Gets the value of the cgstPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCgstPercentage() {
        return cgstPercentage;
    }

    /**
     * Sets the value of the cgstPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCgstPercentage(String value) {
        this.cgstPercentage = value;
    }

    /**
     * Gets the value of the cgstAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCgstAmount() {
        return cgstAmount;
    }

    /**
     * Sets the value of the cgstAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCgstAmount(String value) {
        this.cgstAmount = value;
    }

    /**
     * Gets the value of the sgstPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSgstPercentage() {
        return sgstPercentage;
    }

    /**
     * Sets the value of the sgstPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSgstPercentage(String value) {
        this.sgstPercentage = value;
    }

    /**
     * Gets the value of the sgstAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSgstAmount() {
        return sgstAmount;
    }

    /**
     * Sets the value of the sgstAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSgstAmount(String value) {
        this.sgstAmount = value;
    }

    /**
     * Gets the value of the igstPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIgstPercentage() {
        return igstPercentage;
    }

    /**
     * Sets the value of the igstPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIgstPercentage(String value) {
        this.igstPercentage = value;
    }

    /**
     * Gets the value of the igstAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIgstAmount() {
        return igstAmount;
    }

    /**
     * Sets the value of the igstAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIgstAmount(String value) {
        this.igstAmount = value;
    }

    /**
     * Gets the value of the netAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetAmount() {
        return netAmount;
    }

    /**
     * Sets the value of the netAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetAmount(String value) {
        this.netAmount = value;
    }

    /**
     * Gets the value of the gstId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGstId() {
        return gstId;
    }

    /**
     * Sets the value of the gstId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGstId(String value) {
        this.gstId = value;
    }

    /**
     * Gets the value of the username property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the value of the username property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsername(String value) {
        this.username = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

}
