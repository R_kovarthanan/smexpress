
package com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="efsId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="efsBranchId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tripId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tripAdvanceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tripAdvanceAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tripAdvanceStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tripAdvanceRemarks" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "efsId",
    "efsBranchId",
    "tripId",
    "tripAdvanceId",
    "tripAdvanceAmount",
    "tripAdvanceStatus",
    "tripAdvanceRemarks",
    "username",
    "password"
})
@XmlRootElement(name = "LoadTripAdvanceDetail")
public class LoadTripAdvanceDetail {

    protected String efsId;
    protected String efsBranchId;
    protected String tripId;
    protected String tripAdvanceId;
    protected String tripAdvanceAmount;
    protected String tripAdvanceStatus;
    protected String tripAdvanceRemarks;
    protected String username;
    protected String password;

    /**
     * Gets the value of the efsId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEfsId() {
        return efsId;
    }

    /**
     * Sets the value of the efsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEfsId(String value) {
        this.efsId = value;
    }

    /**
     * Gets the value of the efsBranchId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEfsBranchId() {
        return efsBranchId;
    }

    /**
     * Sets the value of the efsBranchId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEfsBranchId(String value) {
        this.efsBranchId = value;
    }

    /**
     * Gets the value of the tripId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTripId() {
        return tripId;
    }

    /**
     * Sets the value of the tripId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTripId(String value) {
        this.tripId = value;
    }

    /**
     * Gets the value of the tripAdvanceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTripAdvanceId() {
        return tripAdvanceId;
    }

    /**
     * Sets the value of the tripAdvanceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTripAdvanceId(String value) {
        this.tripAdvanceId = value;
    }

    /**
     * Gets the value of the tripAdvanceAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTripAdvanceAmount() {
        return tripAdvanceAmount;
    }

    /**
     * Sets the value of the tripAdvanceAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTripAdvanceAmount(String value) {
        this.tripAdvanceAmount = value;
    }

    /**
     * Gets the value of the tripAdvanceStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTripAdvanceStatus() {
        return tripAdvanceStatus;
    }

    /**
     * Sets the value of the tripAdvanceStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTripAdvanceStatus(String value) {
        this.tripAdvanceStatus = value;
    }

    /**
     * Gets the value of the tripAdvanceRemarks property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTripAdvanceRemarks() {
        return tripAdvanceRemarks;
    }

    /**
     * Sets the value of the tripAdvanceRemarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTripAdvanceRemarks(String value) {
        this.tripAdvanceRemarks = value;
    }

    /**
     * Gets the value of the username property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the value of the username property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsername(String value) {
        this.username = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

}
