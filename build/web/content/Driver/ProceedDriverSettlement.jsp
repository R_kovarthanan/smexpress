
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>

        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    </head>

    <style type="text/css">
        .blink {
            font-family:Tahoma;
            font-size:11px;
            color:#333333;
            padding-left:10px;    
            background-color:#CC3333;
        }            
        .blink1 {
            font-family:Tahoma;
            font-size:11px;
            color:#333333;
            padding-left:10px;    
            background-color:#F2F2F2;
        }            
    </style>
    <script>
        function show_src() {
            document.getElementById('exp_table').style.display='none';
        }
        function show_exp() {
            document.getElementById('exp_table').style.display='block';
        }
        function show_close() {
            document.getElementById('exp_table').style.display='none';
        }

        function submitPage()
        {
            var fromDate=document.proceedDriverSettlement.fromDate.value;
            var toDate=document.proceedDriverSettlement.toDate.value;
            var regno=document.proceedDriverSettlement.regno.value;
            var driName=document.proceedDriverSettlement.driName.value;

            if((!(isEmpty(fromDate) && isEmpty(toDate)) || !isEmpty(regno) || !isEmpty(driName) ))  {           
                document.proceedDriverSettlement.action="/throttle/searchProDriverSettlement.do";
                document.getElementById('exp_table').style.display='none';
                document.proceedDriverSettlement.submit();
            }else {
                alert("Please enter any one value ");
            }
            /*if(obj.name=="Proceed"){
                document.proceedDriverSettlement.action="/throttle/ProceedToDriverSettlement.do";
                document.proceedDriverSettlement.submit();
                
            }*/
        }
        function setFocus()
        {
            var sdate='<%=request.getAttribute("fromDate")%>';
            var edate='<%=request.getAttribute("toDate")%>';
            var regno='<%=request.getAttribute("regNo")%>';
            var driName='<%=request.getAttribute("driName")%>';
            if(sdate!='null' && edate!='null'){
                document.proceedDriverSettlement.fromDate.value=sdate;
                document.proceedDriverSettlement.toDate.value=edate;
            } if(regno!='null'){
                document.proceedDriverSettlement.regno.value=regno;
            }
            if(driName!='null'){
                document.proceedDriverSettlement.driName.value=driName;
            }     
        }  
        
    </script>    
    <body onload="setFocus();getVehicleNos();">
        <form name="proceedDriverSettlement" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <!-- pointer table -->
            <!-- message table -->
            <%@ include file="/content/common/message.jsp"%>
            <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">                
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <b>Driver Settlement</b>
                            <div id="first">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4">
                                    <tr>
                                        <td><font color="red">*</font>Driver Name</td>
                                        <td height="30">
                                            <input name="driName" id="driName" type="text" class="textbox" size="20" value="">
                                        </td>
                                        <td><!--Vehicle No--></td>
                                        <td><input name="regno" id="regno" type="hidden" class="textbox" size="20" value=""></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="30"><font color="red">*</font>From Date</td>
                                        <td height="30"><input type="text" name="fromDate" class="textbox" ><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.proceedDriverSettlement.fromDate,'dd-mm-yyyy',this)"/></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td><input type="text" name="toDate" class="textbox" ><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.proceedDriverSettlement.toDate,'dd-mm-yyyy',this)"/></td>
                                        <td><input type="button" class="button"  onclick="submitPage();" value="Search"></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>            
        </form>
    </body>
</html>