<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });
            $(function() {
                $( ".datepicker" ).datepicker({
                    changeMonth: true,changeYear: true
                });
            });
        </script>
    </head>
    <style type="text/css">
        .blink {
            font-family:Tahoma;
            font-size:11px;
            color:#333333;
            padding-left:10px;
            background-color:#CC3333;
        }
        .blink1 {
            font-family:Tahoma;
            font-size:11px;
            color:#333333;
            padding-left:10px;
            background-color:#F2F2F2;
        }
    </style>
    <script type="text/javascript" language="javascript">
        $(document).ready(function() {
            $("#tabs").tabs();
        });
    </script>
    <script type="text/javascript" language="javascript">
        function savePage(){
            var cardId = document.cardVehicleDriverMap.cardId.value;
            var vehicleId = document.cardVehicleDriverMap.vehicleId.value;
            var driverId =document.cardVehicleDriverMap.driverId.value;
            var remark =document.cardVehicleDriverMap.remark.value;
            //alert(vehicleId);
            if((!(isEmpty(cardId) && isEmpty(vehicleId)) || !isEmpty(driverId) || !isEmpty(remark) ))  {
                document.cardVehicleDriverMap.action="/throttle/saveCardVehicleDriverMap.do";
                document.cardVehicleDriverMap.submit();                
            }
        }            
    </script>
    <body>
        <form name="cardVehicleDriverMap" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <td><input type="hidden" name="settlementId" id="settlementId" value=""/></td>
            <!-- pointer table -->
            <!-- message table -->
            <%@ include file="/content/common/message.jsp"%>            
            <br/>
            <c:if test="${cardDetails != null}">
                <c:forEach items="${cardDetails}" var="cardDt">
            <table width="700" id="addtable" cellpadding="0" cellspacing="2" border="0" align="center" class="table4">
                <tr>
                    <td>&nbsp;&nbsp;</td>
                    <td>Card No</td>
                    <td>
                        <select class='textbox' style="width:123px; " id="cardId"  name="cardId" >
                            <option selected  value="0">---Select---</option>
                            <c:if test = "${cardNos != null}" >
                                <c:forEach items="${cardNos}" var="card">
                                    <option  value='<c:out value="${card.identityNo}" />'>
                                        <c:out value="${card.identityNo}" />
                                    </c:forEach >
                                </c:if>
                        </select>
                        <script type="text/javascript">
                            document.getElementById('cardId').value = '<c:out value="${cardDt.identityNo}"/>';
                        </script>
                    </td>
                    <td>Vehicle No</td>
                    <td>
                        <select class='textbox' style="width:123px; " id="vehicleId"  name="vehicleId" >
                            <option selected  value="0">---Select---</option>
                            <c:if test = "${vehicleNos != null}" >
                                <c:forEach items="${vehicleNos}" var="vNos">
                                    <option  value='<c:out value="${vNos.vehicleId}" />'><c:out value="${vNos.regno}" />
                                    </c:forEach >
                                </c:if>
                        </select>                        
                        <script type="text/javascript">
                            document.getElementById('vehicleId').value = '<c:out value="${cardDt.vehicleId}"/>';
                        </script>
                    </td>
                    <td>Driver Name</td>
                    <td height="30">
                        <select class='textbox' style="width:123px; " id="driverId"  name="driverId" >
                            <option selected  value="0">---Select---</option>
                            <c:if test = "${driverName != null}" >
                                <c:forEach items="${driverName}" var="dri">
                                    <option  value='<c:out value="${dri.empId}" />'>
                                        <c:out value="${dri.empName}" />
                                    </c:forEach >
                                </c:if>
                        </select>
                        <script type="text/javascript">
                            document.getElementById('driverId').value = '<c:out value="${cardDt.driverId}"/>';
                        </script>
                    </td>                    
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;</td>
                    <td>Remarks</td>
                    <td colspan="4"><textarea cols="50%" name="remark" id="remark"></textarea></td>
                    <td>&nbsp;&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;</td>
                    <td colspan="5" align="center">&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;</td>
                    <td colspan="5" align="center"><input type="button" class="button"  onclick="savePage(0);" value="Save"></td>
                    <td>&nbsp;&nbsp;</td>
                </tr>
            </table>
            </c:forEach>
            </c:if>
        </form>
    </body>
</html>