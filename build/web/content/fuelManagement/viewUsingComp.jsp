
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
        
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    </head>
    <script language="javascript">
        function submitPage(value){
            
            if(value=='Add'){
                document.vehComp.action='/throttle/addUsingCompanyPage.do';
                document.vehComp.submit();
                
            }
                   
        }
       
        function submitAlter(indx){  
            var usingCompId=document.getElementsByName("usingCompIds");           
            document.vehComp.action='/throttle/alterUsingCompanyPage.do?usingCompId='+usingCompId[indx].value;
            document.vehComp.submit();
        }
        
       
         
    </script>
    <body >
        <form name="vehComp" method="post">
            <%@ include file="/content/common/path.jsp" %>                 
            <%@ include file="/content/common/message.jsp" %>
            <br>
                 
            
            <c:if test = "${UsingCompList!= null}" >
                
                <table align="center" width="350" class="border" border="0" cellspacing="0" cellpadding="0">
                    <tr height="30">
                        <td colspan="4" class="contenthead">Vehicle Using Company</td>
                    </tr>
                    
                    <tr height="30">
                        <td class="contentsub">SNo</td>                            
                        <td class="contentsub">Company Name</td>
                        <td class="contentsub">Status</td>
                        <td class="contentsub">Action</td>
                        
                        
                    </tr>
                    <% int index=0;%>
                    <c:forEach items="${UsingCompList}" var="veh"> 
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>	
                        <tr height="30">                                                                                                  
                            <td class="<%=classText %>"><%=index+1%></td>
                            <td class="<%=classText %>"><input type="hidden" name="usingCompIds" value='<c:out value="${veh.usingCompId}"/>'><c:out value="${veh.companyName}"/></td>
                            <c:if test="${veh.activeInd=='Y'}">
                            <td class="<%=classText %>">Active</td>
                        </c:if>
                            <c:if test="${veh.activeInd=='N'}">
                            <td class="<%=classText %>">INActive</td>
                        </c:if>
                            <td class="<%=classText %>"><a href="#" onclick="submitAlter(<%=index%>);">Alter</a></td>                                                        
                        </tr>
                        <%index++;%>
                    </c:forEach>
                </table>
            </c:if>
            <br><br>
            <center><input type="button" class="button" value="Add" onclick="submitPage(this.value);" />
                
                
            </center>
        </form>
    </body>
</html>
