
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
        
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    </head>
    <script language="javascript">
        function submitPage(value){
            
            if(value=='Add'){
                document.vehComp.action='/throttle/addVehicleCompanyPage.do';
                document.vehComp.submit();
                
            }
            else{
                
                document.vehComp.action='/throttle/deleteVehicleCompanyPage.do';
                document.vehComp.submit();
            }       
        }
        function submitGo(){
            document.vehComp.action='/throttle/viewVehicleCompany.do';
            document.vehComp.submit();
        }
        function submitAlter(indx){  
            var reg=document.getElementsByName("vehNo");           
            document.vehComp.action='/throttle/alterVehicleCompanyPage.do?regNo='+reg[indx].value;
            document.vehComp.submit();
        }
        function getVehicleNos(){
            
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
        }   
        function setValues(){
            if('<%=request.getAttribute("regNo")%>'!='null'){
              
                   document.vehComp.vehicleNo.value='<%=request.getAttribute("regNo")%>';
               
               }if('<%=request.getAttribute("companyId")%>'!='null'){
               
                   document.vehComp.companyId.value='<%=request.getAttribute("companyId")%>';
              
               }if('<%=request.getAttribute("typeId")%>'!='null'){
               
                   document.vehComp.typeId.value='<%=request.getAttribute("typeId")%>';
              
               }if('<%=request.getAttribute("servicePtId") %>'!='null'){
              
                   document.vehComp.servicePtId.value='<%=request.getAttribute("servicePtId")%>';
              
               }
           }
    </script>
    <body onload="getVehicleNos();setValues();">
        <form name="vehComp" method="post">
            <%@ include file="/content/common/path.jsp" %>                 
            <%@ include file="/content/common/message.jsp" %>
            <br>
                 
            <table  border="0" class="border" align="center" width="800" cellpadding="0" cellspacing="0" id="bg">
                <tr>
                    <td height="30" class="contenthead" colspan="4"><div class="contenthead">View Vehicle</div></td>
                </tr>
                <tr>
                    <td class="text2" height="30">Vehicle Number</td>
                    <td class="text2" height="30">Owning Company</td>
                    <td class="text2" height="30">Using Company</td>
                    <td class="text2" height="30">Vehicle Type</td>
                </tr>
                <tr>
                    <td class="text1" height="30"><input type="text" id="regno" name="vehicleNo"  class="textbox" ></td>
                    <td class="text1" height="30"><select class="textbox" name="owningCompId" style="width:125px">
                        <option value="0">---Select---</option>
                        <c:if test = "${OwningCompanyList != null}" >
                            <c:forEach items="${OwningCompanyList}" var="comp"> 
                                <option value='<c:out value="${comp.custId}" />'><c:out value="${comp.custName}" /></option>
                            </c:forEach>
                        </c:if>  	
                </select></td>
                 <td class="text1" height="30">
                       <select class="textbox" name="usingCompId" style="width:125px">
                        <option value="0">---Select---</option>
                        <c:if test = "${UsingCompanyList != null}" >
                            <c:forEach items="${UsingCompanyList}" var="comp"> 
                                <option value='<c:out value="${comp.usingCompId}" />'><c:out value="${comp.companyName}" /></option>
                            </c:forEach>
                        </c:if>  	
                </select>           
                    </td>
                    <td class="text1" height="30">
                        <select class="textbox" name="typeId" >
                            <option value="1">---Select---</option>
                            <c:if test = "${TypeList != null}" >
                                <c:forEach items="${TypeList}" var="Type"> 
                                    <option value='<c:out value="${Type.typeId}" />'><c:out value="${Type.typeName}" /></option>
                                </c:forEach >
                            </c:if>  	
                        </select>
                    </td>
                   
                </tr>
            </table>
            <br>
            <center> <input type="button" class="button" onclick="submitGo();" value="Go"></center>
            <br>
            <c:if test = "${VehicleCompList!= null}" >
                
                <table align="center" width="700" class="border" border="0" cellspacing="0" cellpadding="0">
                    <tr height="30">
                        <td colspan="4" class="contenthead">Vehicle Usage Configuration</td>
                    </tr>
                    
                    <tr height="30">
                        <td class="contentsub">Vehicle Number</td>    
                        <td class="contentsub">Vehicle Type</td>
                        <td class="contentsub">Mfr</td>
                        <td class="contentsub">Model</td>
                        <td class="contentsub">Usage Type</td>
                        <td class="contentsub">Owning Company</td>
                        <td class="contentsub">Using Company</td>
                        <td class="contentsub">Purchase Date</td>
                        
                        
                    </tr>
                    <% int index=0;%>
                    <c:forEach items="${VehicleCompList}" var="veh"> 
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>	
                        <tr height="30">
                            
                            <td class="<%=classText %>"><input type="hidden" name="vehNo" value='<c:out value="${veh.regNo}"/>'><c:out value="${veh.regNo}"/></td> 
                            <td class="<%=classText %>"><c:out value="${veh.typeName}" /></td>                            
                            <td class="<%=classText %>"><c:out value="${veh.mfr}" /></td>                            
                            <td class="<%=classText %>"><c:out value="${veh.model}" /></td>                            
                            <td class="<%=classText %>"><c:out value="${veh.usageType}" /></td>                            
                            <td class="<%=classText %>"><c:out value="${veh.servicePtName}"/></td>
                            <td class="<%=classText %>"><c:out value="${veh.companyName}" /></td> 
                            <td class="<%=classText %>"><c:out value="${veh.date}"/></td>
                            <td class="<%=classText %>"><a href="#" onclick="submitAlter(<%=index%>);">Alter</a></td>                                                        
                        </tr>
                        <%index++;%>
                    </c:forEach>
                </table>
            </c:if>
            <br><br>
            <center><input type="button" class="button" value="Add" onclick="submitPage(this.value);" />
                <input type="button" class="button" value="Delete" onclick="submitPage(this.value);" />
                
            </center>
        </form>
    </body>
</html>
