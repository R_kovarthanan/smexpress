<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
       
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
</head>
<script language="javascript">
    function submitPage(){
        if(!textValidation(document.fuelPrice.rate,'Fuel Price')){          
       document.fuelPrice.action='/throttle/alterFuelPrice.do';
       document.fuelPrice.submit();
        }
        }
        function setValues(){
           
            document.fuelPrice.date.value='<%=request.getAttribute("date")%>';
            document.fuelPrice.companyId.value='<%=request.getAttribute("companyId")%>';
            document.fuelPrice.rate.value='<%=request.getAttribute("rate")%>';
            
            }
</script>
<body onload="setValues();">
<form name="fuelPrice" method="post">
     <%@ include file="/content/common/path.jsp" %>                 
            <%@ include file="/content/common/message.jsp" %>
            <br>
<table align="center" width="500" border="0" cellspacing="0" cellpadding="0">
 <tr height="30">
  <Td colspan="4" class="contenthead">Fuel Price</Td>
 </tr>
 
  <tr>
    <td class="text1">Date</td>    
   <td class="text1"><input name="date" class="textbox" readonly type="textbox"  size="20"></td> 
        
  </tr>
  <tr>
    <td class="text2">Filling Station</td>    
   <td class="text2"><select class="textbox" name="companyId" style="width:125px">                        
                        <c:if test = "${CompanyList != null}" >
                            <c:forEach items="${CompanyList}" var="comp"> 
                            <c:if test="${comp.cmpId==companyId}">
                                <option value='<c:out value="${comp.cmpId}" />'><c:out value="${comp.name}" /></option>
                            </c:if>
                           </c:forEach>
                        </c:if>  	
                </select></td> 
    
    
  </tr>
  <tr height="30">
     <td class="text1">Fuel Price(per Ltr)</td>    
    <td class="text1"><input name="rate" class="textbox" type="textbox" value="" size="20"></td>   
  </tr>
 
</table>

<br><br>
<center><input type="button" class="button" value="Save" onclick="submitPage();" />


</center>
</form>
</body>
</html>
