<%-- 
Document   : manageuser
Created on : Jul 19, 2008, 11:12:18 AM
Author     : vidya
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
   <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import = "ets.domain.users.business.LoginTO" %>
<%@ page import="java.util.*" %>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript">
function submitPage(value){
if(value == 'search'){
document.manageuser.action ='/throttle/getuser.do';
document.manageuser.whatRequest.value='User-GetUser';
document.manageuser.submit();
}else if(value == 'add'){
document.manageuser.action ='/throttle/adduser.do';
document.manageuser.whatRequest.value='User-AddPage';
document.manageuser.submit();
}else if(value == 'moddel'){
document.manageuser.action ='/throttle/alteruser.do';
document.manageuser.whatRequest.value='User-AlterPage';
document.manageuser.submit();
}else {
//
}
}
function setName() {
document.manageuser.letters.focus();
var letter = '<%= request.getAttribute("Letter") %>';
if(letter != 'null'){
    document.manageuser.letters.value = letter;
    }
}
</script>


<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->
<div class="pageheader">
      <h2><i class="fa fa-edit"></i><spring:message code="settings.label.ManageUser" text="default text"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="header.label.YouAreHere" text="default text"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="header.label.Home" text="default text"/></a></li>
          <li><a href="general-forms.html"><spring:message code="header.label.Setting" text="default text"/></a></li>
          <li class=""><spring:message code="settings.label.ManageUser" text="default text"/> </li>
                  </ol>
      </div>
      </div>
<div class="contentpanel">
            <div class="panel panel-default">
             <div class="panel-body">
<body onLoad="setName();" >

<form method="post" name="manageuser">
<!-- copy there from end -->
<!-- copy there from end -->
<!-- pointer table -->



<!-- pointer table -->


<%@ include file="/content/common/message.jsp" %>


 <!--<table class="table table-bordered">-->

  <table  class="table table-info mb30 table-hover">
 <thead>
    
    <th colspan="4" ><spring:message code="settings.label.ManageUser" text="default text"/> </th>
</thead>
<tr>
<td  ><spring:message code="settings.label.Select User name starts with" text="Select User Name Starts With"/> </td>
<td    ><select name="letters" style="width:260px;height:40px;"  class="form-control" >
<option value="0"><spring:message code="settings.label.All" text="default text"/></option>
<option value="A"><spring:message code="settings.label.A" text="default text"/></option>
<option value="B"><spring:message code="settings.label.B" text="default text"/></option>
<option value="C"><spring:message code="settings.label.C" text="default text"/></option>
<option value="D"><spring:message code="settings.label.D" text="default text"/></option>
<option value="E"><spring:message code="settings.label.E" text="default text"/></option>
<option value="F"><spring:message code="settings.label.F" text="default text"/></option>
<option value="G"><spring:message code="settings.label.G" text="default text"/></option>
<option value="H"><spring:message code="settings.label.H" text="default text"/></option>
<option value="I"><spring:message code="settings.label.I" text="default text"/></option>
<option value="J"><spring:message code="settings.label.J" text="default text"/></option>
<option value="K"><spring:message code="settings.label.K" text="default text"/></option>
<option value="L"><spring:message code="settings.label.L" text="default text"/></option>
<option value="M"><spring:message code="settings.label.M" text="default text"/></option>
<option value="N"><spring:message code="settings.label.N" text="default text"/></option>
<option value="O"><spring:message code="settings.label.O" text="default text"/></option>
<option value="P"><spring:message code="settings.label.P" text="default text"/></option>
<option value="Q"><spring:message code="settings.label.Q" text="default text"/></option>
<option value="R"><spring:message code="settings.label.R" text="default text"/></option>
<option value="S"><spring:message code="settings.label.S" text="default text"/></option>
<option value="T"><spring:message code="settings.label.T" text="default text"/></option>
<option value="U"><spring:message code="settings.label.U" text="default text"/></option>
<option value="V"><spring:message code="settings.label.V" text="default text"/></option>
<option value="W"><spring:message code="settings.label.W" text="default text"/></option>
<option value="X"><spring:message code="settings.label.X" text="default text"/></option>
<option value="Y"><spring:message code="settings.label.Y" text="default text"/></option>
<option value="Z"><spring:message code="settings.label.Z" text="default text"/></option>                                   
</select> </td>
<td  style="border-top-color:#5BC0DE;"><input type="button" class="btn btn-success" align="right" name="search" value="<spring:message code="settings.label.GO" text="default text"/>" onClick="submitPage(this.name)" class="button"></td>
</tr>
</td>
</table>

<%
int index = 0;
ArrayList usernames = (ArrayList) request.getAttribute("userList");

%>
<c:if test = "${userList != null}">
<%
if (usernames.size() > 0) {
%>
<table class="table table-info mb30 table-hover" >
    <thead>
<tr>
<th  ><spring:message code="settings.label.SNo" text="default text"/></th>

<th  ><spring:message code="settings.label.UserId" text="default text"/></th>
<th  ><spring:message code="settings.label.Username" text="default text"/></th>
<th  ><spring:message code="settings.label.Status" text="default text"/></th>
<th  ><spring:message code="settings.label.CreatedBy" text="default text"/></th>
<th  ><spring:message code="settings.label.CreatedDate" text="default text"/></th>
</tr>
</thead>
<%
}
%>

<c:forEach items="${userList}" var="user"> 
<%

String classText = "";
int oddEven = index % 2;
if (oddEven > 0) {
classText = "text2";
} else {
classText = "text1";
}
%>

<tr>
<td  ><%= index + 1%></td>
<td  >
<input type="hidden" name="userid" value='<c:out value="${user.userId}" />' /> <div align="left"><c:out value="${user.userId}" /></div> </td>
<td  >
<input type="hidden" name="username" value='<c:out value="${user.userId}" />' /><div align="left"><c:out value="${user.userName}" /> </div></td>
<td  >
<input type="hidden" name="actind" value='<c:out value="${user.status}"/>'><div align="left"> <c:out value="${user.status}"/></div> </td>
<td  >
<input type="hidden" name="createdby" value='<c:out value="${user.createdBy}"/>'><div align="left"><c:out value="${user.createdBy}"/></div> </td>
<td  >
<input type="hidden" name="createddate" value='<c:out value="${user.createdon}"/>'><div align="left"><c:out value="${user.createdon}"/></div> </td>


</tr>
<%
index++;
%>

</c:forEach >
</c:if>  
</table>  <br>
<center>
<!--
<input type="button" name="add" value="Add" onClick="submitPage(this.name)" class="button">
-->

<%
if(request.getAttribute("userList") != null){
ArrayList userList = (ArrayList) request.getAttribute("userList");
if(userList.size() != 0){
%>
<input type="button" class="btn btn-success" name="moddel" value="<spring:message code="settings.label.Alter" text="default text"/>" onClick="submitPage(this.name)" class="button">
<%
}
}
%>

<input type="hidden" value="" name="whatRequest">
</center>
<br>
<center>

<tr>
<td align="center" height="40" colspan="5">

</td>
</tr>

</center>

<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
