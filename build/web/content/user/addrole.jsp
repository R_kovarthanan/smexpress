<%-- 
    Document   : Addrole
    Created on : Jul 18, 2008, 5:52:19 PM
    Author     : vidya
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
   <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import = "ets.domain.users.business.LoginTO" %>
<%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
     <script language="javascript">
           function submitPage(name){
                //alert(name);
                if(document.addrole.roletype.value=="") {
                    alert("Enter the Role type");
                    document.addrole.roletype.focus();
                }else if(name == "add") {
                    document.addrole.action='/throttle/insertrole.do';
                   // alert(document.addrole.action)
                    document.addrole.reqfor.value='insertrole';
                   // alert(document.addrole.reqfor.value);
                    document.addrole.submit();
                }
            }
        </script>
        
        <!--[if lte IE 7]>
	<style type="text/css">
	
	#fixme {display:block;
	top:0px; left:0px;  position:fixed;  }
	</style>
	<![endif]-->
	
	<!--[if lte IE 6]>
	<style type="text/css">
	body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
	#fixme {display:block;
	top:0px; left:0px;  position:fixed;  }
	* html #fixme  {position:absolute;}
	</style>
	<![endif]-->
	
	<!--[if lte IE 6]>
	<style type="text/css">
	/*<![CDATA[*/ 
	html {overflow-x:auto; overflow-y:hidden;}
	/*]]>*/
	</style>
<![endif]-->
     <div class="pageheader">
      <h2><i class="fa fa-edit"></i><spring:message code="settings.label.AddRole" text="default text"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="header.label.YouAreHere" text="default text"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="header.label.Home" text="default text"/></a></li>
          <li><a href="general-forms.html"><spring:message code="header.label.Setting" text="default text"/></a></li>
          <li class=""><spring:message code="settings.label.ManageRole" text="default text"/> </li>
          <li class=""><spring:message code="settings.label.AddRole" text="default text"/> </li>

                  </ol>
      </div>
      </div>
<div class="contentpanel">
            <div class="panel panel-default">
             <div class="panel-body">
        
   <body onLoad="document.addrole.roletype.focus()">
<form method="post" name="addrole" >

<!-- copy there from end -->
<!--<div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
<div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
 pointer table 
<table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
<tr>
<td >

</td></tr></table>
 pointer table 
 title table 

 title table 
</div>
</div>-->
<!-- message table -->

<%@ include file="/content/common/message.jsp" %>
<!-- message table -->
<!-- copy there  end -->



 <table  class="table">
<!--    <thead>
    
    <th colspan="6"  height="30"><spring:message code="settings.label.AddRole" text="default text"/> </th>
</thead>-->
<tr>
 <td  height="30"  ><font color="red">*</font><spring:message code="settings.label.Rolltype" text="default text"/> :</td>
 
<td  height="30" ><input type="text" style="width:260px;height:40px;"  class="form-control" name="roletype"/></td>

<td height="30" ><font color="red">*</font> <spring:message code="settings.label.Description" text="default text"/> :</td>

<td height="45" ><textarea cols="17"  name="roledesc" style="width:260px;height:40px;"  class="form-control"></textarea></td>
</tr>

</table>
  



<center>
<input type="button" class="btn btn-success" value="<spring:message code="settings.label.ADD" text="default text"/>" name="add" class="button" onClick="submitPage(this.name)" />
&emsp;<input type="reset" class="btn btn-success" value="<spring:message code="settings.label.CLEAR" text="default text"/>">
<input type="hidden" name="reqfor" value="">
</center>

<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
  
</body>
</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
