

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>          
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script> 
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"> </script> 
        <title>MRSList</title>
    </head>

        
        
        <script>
            function submitPag(){  
        if(document.mpr.toDate.value==''){
            alert("Please Enter From Date");
        }else if(document.mpr.fromDate.value==''){
            alert("Please Enter to Date");
        }                 
                document.mpr.action="/throttle/mprApprovalList.do";
                document.mpr.submit();
            }            
            
            function setDate(fDate,tDate){
                if(fDate != 'null'){
                document.mpr.fromDate.value=fDate;
                }
                if(tDate != 'null'){
                document.mpr.toDate.value=tDate;
                }
                                
            }
            
        </script>



    <body onLoad="setDate('<%= request.getAttribute("fromDate") %>','<%= request.getAttribute("toDate") %>')">        
        <form name="mpr" method="post" >                    
            <%@ include file="/content/common/path.jsp" %>            
            <!-- pointer table -->
       
            <!-- message table -->           
            <%@ include file="/content/common/message.jsp" %>    
            <table align="center" width="80%" border="0" cellspacing="0" cellpadding="0" class="border">
                
                <tr>
                    <td colspan="4" class="contenthead" height="30"><div class="contenthead">MPR / LPR Approval Queue</div></td>
                </tr>
                <tr>
                    <td class="text2" width="150" height="30">From Date</td>
                    <td width="196" height="30" class="text2"><input readonly name="fromDate" class="textbox" type="text" value="" size="20">
                    <img src="/throttle/images/cal.gif" name="Calendar"  readonly  onclick="displayCalendar(document.mpr.fromDate,'dd-mm-yyyy',this)"/></td>

                    <td class="text2" width="150" height="30">To Date</td>
                    <td width="196" height="30" class="text2"><input name="toDate" class="textbox"  type="text" value="" size="20">
                    <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.mpr.toDate,'dd-mm-yyyy',this)"/></td>
                </tr>
                <!--<tr>
                    <td class="text1" width="150"  height="30">Vendor</td>
                    <td width="196" class="text1" height="30" ><select class="textbox" name="vendorId">
                            <option value='0' selected>All</option>
                    <c:if test = "${vendorList != null}" >
                    <c:forEach items="${vendorList}" var="vend">                             
                            <option value=<c:out value="${vend.vendorId}"/> ><c:out value="${vend.vendorName}"/></option>
                    </c:forEach>
                    </c:if>                             
                    </select> </td>
                </tr>
                <tr>
                    <td class="text2"  height="30">Status</td>
                    <td height="30" class="text2"><select class="textbox" >
                            <option value="PENDING" selected> Pending</option>
                            <option value="APPROVED"> Approved</option>
                            <option value="REJECTED"> Rejected</option>
                        </select>
                    </td>
                </tr> -->
                <tr>
                    <td colspan="4" align="center"> <input class="button"  type="button" name="search" value="Search" onClick="submitPag();"> </td>
                </tr>
            </table> 
			<br>   
                <c:if test = "${mprList != null}" >            
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="80%" id="bg" class="border">
               
                <tr>
                    <td class="contentsub" height="30"><div class="contentsub">MPR/LPR NO</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Vendor Name</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Purchase Type</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Raised Date</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Elapsed Days</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Status</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">&nbsp;</div></td>
                </tr>
                <% int index = 0;
            String classText = "";
            int oddEven = 0;
                %>

                    <c:forEach items="${mprList}" var="mpr"> 
                        <c:if test="${mpr.status != 'APPROVED' &&  mpr.status != 'REJECTED'}" >
                        <%

            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr>
                            <td class="<%=classText %>" height="30"><c:out value="${mpr.mprId}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${mpr.vendorName}"/></td>                       
                            <td class="<%=classText %>" height="30"><c:out value="${mpr.purchaseType}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${mpr.mprDate}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${mpr.elapsedDays}"/></td>                                                                        
                            <td class="<%=classText %>" height="30"><c:out value="${mpr.status}"/></td>                                                                        
                            <td class="<%=classText %>" height="30"><a href="/throttle/approveMprPage.do?mprId=<c:out value='${mpr.mprId}'/>" > Approve/Reject </a> </td>                                                                        
                        </tr>
                        <%
            index++;
                        %>
                        </c:if>
                    </c:forEach>
                </c:if>                  
            </table>                        
        </form>
    </body>
</html>
