


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>          
        <script language="javascript" src="/throttle/js/validate.js"></script>
           <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>
        <title>Modify GRN</title>
    </head>
    <body>
        
        
        <script>
            function submitPage(val){ 
/*                if(val =='return'){
                    document.modifyGrn.action="/throttle/handleDirectPurchaseRetQty.do"                
                    document.modifyGrn.submit();                                    
                }
                else{*/
                if(validate()!='submit'){
                    return;
                }                         
                    document.modifyGrn.action="/throttle/handleDirectPurchaseRetQty.do"                
                    document.modifyGrn.submit();                
                }    
                
            function submitPage1(val){ 
                    var index = document.getElementsByName("selectedIndex");
                    if(document.modifyGrn.inVoiceNumber.value=="" ){
                        alert("Please Enter Invoice Number");
                        document.modifyGrn.inVoiceNumber.focus();
                        return;
                    }            
                    for(var i =0; i<index.length; i++ ){
                        if( calculateMRP(i)== 'fail' ){
                            return;
                        }    
                        if( calculateAmount(i)=='fail' ){
                            return;
                        }
                        totalBalance(i);
                    }     
                    document.getElementById("buttonStyle").style.visibility="hidden" ;
                    document.modifyGrn.action="/throttle/updateGrn.do"                
                    document.modifyGrn.submit();                
                }    

            
            function setAmount(val){
                var acceptedQtys = document.getElementsByName("acceptedQtys");
                var itemAmounts = document.getElementsByName("itemAmounts");
                var mrps = document.getElementsByName("mrps");
                if(floatValidation( acceptedQtys[val],"acceptedQty")) {
                    acceptedQtys[val].focus();
                    return;
                }                 
                itemAmounts[val].value = parseFloat( acceptedQtys[val].value)*parseFloat(mrps[val].value);
                itemAmounts[val].value = parseFloat(itemAmounts[val].value).toFixed(2);
            }  
            

       

            
            function validate(){
                var selectedInd = document.getElementsByName("selectedIndex");
                var retQty = document.getElementsByName("returnedQtys");
                var acceptedQtys = document.getElementsByName("acceptedQtys");
                var counter = 0;
                if(floatValidation(document.modifyGrn.inVoiceAmount,"InVoice Amount") ){
                    return;
                }    
                if(textValidation(document.modifyGrn.dcNumber,"DC Number") ){
                    return;
                }    
                /*
                if(textValidation(document.modifyGrn.inVoiceNumber,"Invoice Number") ){
                    return;
                }  
                */
                for(var i=0;i< acceptedQtys.length;i++){
                    if(selectedInd[i].checked==true){
                        counter++;
                        if(floatValidation(acceptedQtys[i],"acceptedQty")){
                            acceptedQtys[i].focus();
                            return "notSubmit";
                        }
                        if(floatValidation(retQty[i],"returned Qty")){
                            retQty[i].focus();
                            return "notSubmit";
                        }
                    }
                }
                if(counter==0){
                    alert('Please Select any Item');
                    return "notSubmit";
                }    
                return "submit"
            } 
            
            
        function calculateTotQty(val){
         
            if(calculateAmount(val)=='fail'){
                return 'fail';
            }    
            
            if(calculateMRP(val)=='fail'){
                return 'fail';
            }                
            totalBalance(val);
        }    
            
            
        function calculateAmount(val){                           
            var totalQtys = document.getElementsByName("totalQtys");  
            var returnedQty = document.getElementsByName("retQtys"); 
            var acceptedQty =  document.getElementsByName("acceptedQtys");
    
            var mrp=document.getElementsByName("mrps");                                       
            var index = document.getElementsByName("selectedIndex");            
            var itemAmounts = document.getElementsByName("itemAmounts");
                                    
            if( isFloat( returnedQty[val].value ) ){
                alert("Please Enter Valid Returned Qty");
                returnedQty[val].focus();
                returnedQty[val].select();
                return 'fail';
            }
            else if(returnedQty[val].value=='0'){
                totalQtys[val].value = acceptedQty[val].value;        
            }    
            else if( parseFloat( returnedQty[val].value ) > parseFloat(acceptedQty[val].value) ){
                alert("Returned quantity should not exceed accepted quantity");
                returnedQty[val].focus();
                returnedQty[val].select();
                return 'fail';        
            }

            totalQtys[val].value  = parseFloat( acceptedQty[val].value ) - parseFloat( returnedQty[val].value );            
                                                
            if( calculateMRP(val) == 'fail' ){
                return 'fail';
            }

            if( isFloat(mrp[val].value) ){
                alert("Please Enter Valid MRP");
                mrp[val].focus();
                mrp[val].select();
                return 'fail';
            }

            
            itemAmounts[val].value = parseFloat( totalQtys[val].value ) * parseFloat( mrp[val].value );
            itemAmounts[val].value = parseFloat( itemAmounts[val].value ).toFixed(2);            
            return 'pass';
    }            
            
            
            

    function calculateMRP(val)
    {            
            var mrp=document.getElementsByName("mrps");                
            var unitPrice = document.getElementsByName("unitPrice");
            var tax = document.getElementsByName("tax");
            var discount = document.getElementsByName("discount");
            var priceAfterDiscount = 0;
            
            if( isFloat(discount[val].value) ){
                alert("Please Enter Valid Discount");
                discount[val].focus();
                discount[val].select();
                return 'fail';
            }    
            if( isFloat(tax[val].value) ){
                alert("Please Enter Valid Tax");
                tax[val].focus();
                tax[val].select();
                return 'fail';
            }    
            if( isFloat(unitPrice[val].value) ){
                alert("Please Enter Valid Unit Price");
                unitPrice[val].focus();
                unitPrice[val].select();
                return 'fail';
            }
             
             priceAfterDiscount = parseFloat( unitPrice[val].value ) - ( parseFloat( unitPrice[val].value ) * parseFloat( discount[val].value )/100 );
             mrp[val].value = parseFloat( priceAfterDiscount ) + ( parseFloat( priceAfterDiscount ) * parseFloat( tax[val].value )/100 );
             mrp[val].value = parseFloat( mrp[val].value ).toFixed(2);
             return 'pass';
    }    


            
            
   
            

function totalBalance(val)
{
    var index = document.getElementsByName("selectedIndex");
    var itemAmounts = document.getElementsByName("itemAmounts");            
    var tot = 0;         
    var roundOff=0;
    calculateMRP(val);
    calculateAmount(val);

    for(var i =0; i<index.length; i++ ){
        if(itemAmounts[i].value != ''){
        tot = parseFloat( tot ) + parseFloat( itemAmounts[i].value);
        }
    }
            if(isFloat(document.modifyGrn.freight.value) ) {
                alert("Please Enter Valid Freight Charges");
                document.modifyGrn.freight.select();
                document.modifyGrn.freight.focus();
                return 'fail';
            }    
            
            tot = parseFloat(tot) + parseFloat(document.modifyGrn.freight.value);
            roundOff = parseFloat(tot).toFixed(0) - parseFloat(tot).toFixed(2);
            roundOff = parseFloat(roundOff).toFixed(2);

            if( parseFloat(roundOff) < 0 ){
                roundOff = -(roundOff);
            }    
            tot = parseFloat(tot).toFixed(0);
            document.getElementById("total").innerHTML= '<font>'+tot+'</font>'; 
            document.getElementById("roundOff").innerHTML= '<font>'+roundOff+'</font>'; 
            document.modifyGrn.inVoiceAmount.value=tot ;                                          
}  
            
            
        </script>
        
        <form name="modifyGrn" method="post" >                    
            <%@ include file="/content/common/path.jsp" %>            
            
            
            <!-- message table -->           
            <%@ include file="/content/common/message.jsp" %>    
            <br>
<% int index = 0; %>                
                <c:if test = "${supplyList != null}" >   
                
          <c:forEach items="${supplyList}" var="supply">  
          <% if(index==0){ %> 
         <table align="center" width="90%" border="0" cellspacing="0" cellpadding="0">
            <tr>
            <td width="90%"  align="left" >
                
            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" id="bg" class="border">
                <tr>
                    <td colspan="9" height="80" align="center" class="contenthead" >GRN Info</td>
                </tr>

         
                
                
                <tr>
                    <td class="text1" height="30">GRN No</td>
                    <input type="hidden" name="supplyId" value=<c:out value="${supply.supplyId}"/>>
                    <input type="hidden" name="poId" value=<c:out value="${supply.poId}"/>>
                    <td class="text1" height="30"> <c:out value="${supply.supplyId}"/> </td>

                    <td class="text1" height="30">Vendor Name</td>
                    <td class="text1" height="30"> <c:out value="${supply.vendorName}"/> </td>
                </tr>           
                <tr>
                    <td class="text2" height="30">DC No</td>
                    <td class="text2" height="30">  <input type="text" readonly name="dcNumber" class="textbox"  value=<c:out value="${supply.dcNumber}"/> >     </td>
                
                    <td class="text2" height="30">Invoice No</td>
                    <td class="text2" height="30"> <input type="text" name="inVoiceNumber" class="textbox"  value=<c:out value="${supply.inVoiceNumber}"/> >        </td>
                </tr>           
                <tr>
                    <td class="text1" height="30">Freight</td>
                    <td class="text1" height="30"> <input type="text" name="freight" class="textbox" onFocusOut="totalBalance('0');" value=<c:out value="${supply.freight}"/> >        </td>
                
                    <td class="text1" height="30">Bill Amount</td>
                    <td class="text1" height="30"> <input type="text" name="inVoiceAmount"  class="textbox" value=<c:out value="${supply.inVoiceAmount}"/>  ></td>
                </tr>
                <tr>
                  <td class="text2" height="30"><font color="red">*</font>Invoice date </td>
                    <td class="text2" height="30"><input name="inVoiceDate" readonly  class="textbox"  type="text" value=<c:out value="${currentDate}"/> size="20">
                    <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.modifyGrn.inVoiceDate,'dd-mm-yyyy',this)"/></td>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2" height="30">&nbsp;</td>
                    
                </tr>
                <c:set var="transactionType" value="${supply.inVoiceNumber}" />
            </table>
        </td>    
	<td>

		<table width="200" align="right" border="0" cellpadding="0" cellspacing="0" class="border">
		<tr> 
		<td colspan="2" height="30" class="blue" align="center" style="border:1px;border-color:#FFFFFF;border-bottom-style:dashed;">Total</td>
		</tr>

		<tr> 
		<td width="121" height="30"  class="bluetext">Rs.</td>
                 <td width="86" height="30" class="bluetext"><div id="total" ><c:out value="${supply.inVoiceAmount}"/></div>  </td>
                </tr>
		<tr> 
		<td width="121" height="30"  class="bluetext">Round Off</td>
                 <td width="86" height="30" class="bluetext"><div id="roundOff" ></div>  </td>
                </tr>                
                
                </table>
	</td>
 </tr>
  
</table>       
<% index++; } %>                
                    </c:forEach> 
            <br>

            <table align="center" border="0" cellpadding="0" cellspacing="0" width="90%"  id="bg" class="border">
                <%index = 0;
            String classText = "";
            int oddEven = 0;
                %>
                <c:if test = "${supplyList != null}" >

                        <tr>
                            <td class="contentsub" height="30"><b>S.No</b></td>
                            <td class="contentsub" height="30"><b>Mfr Code</b></td>
                            <td class="contentsub" height="30"><b>Papl Code</b></td>
                            <td class="contentsub" height="30"><b>Item Name</b></td>
                            <td class="contentsub" height="30"><b>Uom</b></td>
                            <td class="contentsub" height="30"><b>Dis(%)</b></td>
                            <td class="contentsub" height="30"><b>Tax(%)</b></td>
                            <td class="contentsub" height="30"><b>Unit Price</b></td>
                            <td class="contentsub" height="30"><b>Price(with tax)</b></td>
                            <td class="contentsub" height="30"><b>Accepted Qty</b> </td>
                            <td class="contentsub" height="30"><b>Returned Qty</b></td>
                            <td class="contentsub" height="30"><b>Total Qty</b></td>
                            <td class="contentsub" height="30"><b>Amount</b> </td>
                            <td class="contentsub" height="30"><b>Unused Quantity</b> </td>
                            <td class="contentsub" height="30"><b>Select</b> </td>
                        </tr>                
                    <c:forEach items="${supplyList}" var="supply"> 
                        <%

            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr>
                            <input type="hidden" name="itemIds" value=<c:out value="${supply.itemId}"/> >
                            <td class="<%=classText %>" height="30"> <%=index+1%></td>
                            <td class="<%=classText %>" height="30"> <c:out value="${supply.mfrCode}"/></td>
                            <td class="<%=classText %>" height="30"> <c:out value="${supply.paplCode}"/></td>                       
                            <td class="<%=classText %>" height="30"> <c:out value="${supply.itemName}"/></td>
                            <td class="<%=classText %>" height="30"> <c:out value="${supply.uomName}"/></td>
                            <td class="<%=classText %>" height="30"> <input type="text"  readonly name="discount" size="5" class="textbox" onFocusOut="calculateMRP('<%= index %>');"  onChange="setAmount('<%= index %>');calculateTotQty('<%= index %>');" value=<c:out value="${supply.discount}"/> > </td>
                            <td class="<%=classText %>" height="30"> <input type="text"  name="tax" size="5" readonly class="textbox" value=<c:out value="${supply.tax}"/> > </td>
                            <td class="<%=classText %>" height="30"> <input type="text"  readonly name="unitPrice" onFocusOut="calculateMRP('<%= index %>');" onChange="setAmount('<%= index %>');"  size="5" class="textbox" value=<c:out value="${supply.unitPrice}"/> > </td>
                            <td class="<%=classText %>" height="30"> <input type="text"  readonly  name="mrps" size="5" class="textbox" value=<c:out value="${supply.mrp}"/> > </td>                                                                                                                              
                            <td class="<%=classText %>" height="30"> <input type="text" readonly  name="acceptedQtys" size="5" onChange="setAmount('<%= index %>')" class="textbox" value=<c:out value="${supply.acceptedQty}"/> > </td>                                                                                                                              
                            <td class="<%=classText %>" height="30"> <input type="text"  name="retQtys" onFocusOut="calculateTotQty('<%= index %>');"  size="5" class="textbox" value='0' > </td>
                            <td class="<%=classText %>" height="30"> <input type="text" readonly   name="totalQtys" size="5" class="textbox" value='<c:out value="${supply.acceptedQty}"/>' > </td>
                            <td class="<%=classText %>" height="30"> <input type="text"  readonly  name="itemAmounts" size="5" class="textbox" value=<c:out value="${supply.itemAmount}"/> > </td>                                                                                      
                            <td class="<%=classText %>" height="30"> <input type="text" readonly name="returnedQtys" size="5" class="textbox" value=<c:out value="${supply.returnedQty}"/> > </td>
                            <td class="<%=classText %>" height="30"> <input type="checkbox" name="selectedIndex"  value='<%= index %>' > </td> 
                            <c:set var="inventoryStat" value="${supply.inventoryStatus}" />
                            
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach>
                </c:if>                  
            </table> 
<br>           
<br>           
                <input type="hidden" name="status" value="" >             

             <center>   
          <div id="buttonStyle" style="visibility:visible;" align="center" >
          <input type="button" class="button" name="approve" value="Modify GRN" onClick="submitPage1(this.name);" > &nbsp;
          </div>
          <!--<c:if test="${transactionType == ''}" >
              <input type="button" class="button" name="approve" value="Modify GRN" onClick="submitPage1(this.name);" > &nbsp;
            </c:if>-->
            
            &nbsp;&nbsp;&nbsp;
            <c:if test="${inventoryStat == 'N'}" >
            <input type="button" class="button" name="return" value="Unused Qty" onClick="submitPage(this.name);" > &nbsp;            
            </c:if>
            </center>
<br>            
<br>            
                </c:if>                  
        </form>
    </body>
</html>
